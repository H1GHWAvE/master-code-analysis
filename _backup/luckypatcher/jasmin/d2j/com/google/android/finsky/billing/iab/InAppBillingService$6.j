.bytecode 50.0
.class synchronized com/google/android/finsky/billing/iab/InAppBillingService$6
.super com/google/android/finsky/billing/iab/google/util/IInAppBillingService$Stub
.enclosing method com/google/android/finsky/billing/iab/InAppBillingService
.inner class inner com/google/android/finsky/billing/iab/InAppBillingService$6

.field final 'productIDinapp' Ljava/util/ArrayList; signature "Ljava/util/ArrayList<Ljava/lang/String;>;"

.field final 'productIDsubs' Ljava/util/ArrayList; signature "Ljava/util/ArrayList<Ljava/lang/String;>;"

.field final synthetic 'this$0' Lcom/google/android/finsky/billing/iab/InAppBillingService;

.method <init>(Lcom/google/android/finsky/billing/iab/InAppBillingService;)V
aload 0
aload 1
putfield com/google/android/finsky/billing/iab/InAppBillingService$6/this$0 Lcom/google/android/finsky/billing/iab/InAppBillingService;
aload 0
invokespecial com/google/android/finsky/billing/iab/google/util/IInAppBillingService$Stub/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield com/google/android/finsky/billing/iab/InAppBillingService$6/productIDinapp Ljava/util/ArrayList;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield com/google/android/finsky/billing/iab/InAppBillingService$6/productIDsubs Ljava/util/ArrayList;
return
.limit locals 2
.limit stack 3
.end method

.method public consumePurchase(ILjava/lang/String;Ljava/lang/String;)I
.throws android/os/RemoteException
ldc "BillingHack"
ldc "consumePurchase"
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
iconst_0
ireturn
.limit locals 4
.limit stack 2
.end method

.method public getBuyIntent(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
.throws android/os/RemoteException
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 6
aload 6
ldc "RESPONSE_CODE"
iconst_0
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
astore 7
aload 7
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$6/this$0 Lcom/google/android/finsky/billing/iab/InAppBillingService;
invokevirtual com/google/android/finsky/billing/iab/InAppBillingService/getApplicationContext()Landroid/content/Context;
ldc com/google/android/finsky/billing/iab/BuyActivity
invokevirtual android/content/Intent/setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;
pop
aload 7
ldc "com.google.android.finsky.billing.iab.BUY"
invokevirtual android/content/Intent/setAction(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 7
iconst_3
invokevirtual android/content/Intent/addFlags(I)Landroid/content/Intent;
pop
aload 7
ldc "packageName"
aload 2
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 7
ldc "product"
aload 3
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 7
ldc "payload"
aload 5
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 7
ldc "Type"
aload 4
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
new com/google/android/finsky/billing/iab/DbHelper
dup
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$6/this$0 Lcom/google/android/finsky/billing/iab/InAppBillingService;
getfield com/google/android/finsky/billing/iab/InAppBillingService/mContext Landroid/content/Context;
aload 2
invokespecial com/google/android/finsky/billing/iab/DbHelper/<init>(Landroid/content/Context;Ljava/lang/String;)V
invokevirtual com/google/android/finsky/billing/iab/DbHelper/getItems()Ljava/util/ArrayList;
astore 2
aload 2
invokevirtual java/util/ArrayList/size()I
ifeq L0
aload 2
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 2
L1:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/google/android/finsky/billing/iab/ItemsListItem
astore 4
aload 4
getfield com/google/android/finsky/billing/iab/ItemsListItem/pData Ljava/lang/String;
ldc "auto.repeat.LP"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L1
aload 4
getfield com/google/android/finsky/billing/iab/ItemsListItem/itemID Ljava/lang/String;
aload 3
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L1
aload 7
ldc "autorepeat"
aload 4
getfield com/google/android/finsky/billing/iab/ItemsListItem/pSignature Ljava/lang/String;
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
goto L1
L0:
aload 6
ldc "BUY_INTENT"
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$6/this$0 Lcom/google/android/finsky/billing/iab/InAppBillingService;
invokevirtual com/google/android/finsky/billing/iab/InAppBillingService/getApplicationContext()Landroid/content/Context;
iconst_0
aload 7
ldc_w 134217728
invokestatic android/app/PendingIntent/getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
invokevirtual android/os/Bundle/putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
aload 6
areturn
.limit locals 8
.limit stack 6
.end method

.method public getBuyIntentToReplaceSkus(ILjava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
.throws android/os/RemoteException
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "LuckyPatcher: use api 5 getBuyIntentToReplaceSkus"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 3
aload 3
ldc "RESPONSE_CODE"
iconst_0
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
astore 7
aload 7
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$6/this$0 Lcom/google/android/finsky/billing/iab/InAppBillingService;
invokevirtual com/google/android/finsky/billing/iab/InAppBillingService/getApplicationContext()Landroid/content/Context;
ldc com/google/android/finsky/billing/iab/BuyActivity
invokevirtual android/content/Intent/setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;
pop
aload 7
ldc "com.google.android.finsky.billing.iab.BUY"
invokevirtual android/content/Intent/setAction(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 7
iconst_3
invokevirtual android/content/Intent/addFlags(I)Landroid/content/Intent;
pop
aload 7
ldc "packageName"
aload 2
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 7
ldc "product"
aload 4
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 7
ldc "payload"
aload 6
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 7
ldc "Type"
aload 5
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 3
ldc "BUY_INTENT"
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$6/this$0 Lcom/google/android/finsky/billing/iab/InAppBillingService;
invokevirtual com/google/android/finsky/billing/iab/InAppBillingService/getApplicationContext()Landroid/content/Context;
iconst_0
aload 7
ldc_w 134217728
invokestatic android/app/PendingIntent/getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
invokevirtual android/os/Bundle/putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
aload 3
areturn
.limit locals 8
.limit stack 6
.end method

.method public getPurchases(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
.throws android/os/RemoteException
ldc "BillingHack"
ldc "getPurchases"
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
new com/google/android/finsky/billing/iab/DbHelper
dup
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$6/this$0 Lcom/google/android/finsky/billing/iab/InAppBillingService;
getfield com/google/android/finsky/billing/iab/InAppBillingService/mContext Landroid/content/Context;
aload 2
invokespecial com/google/android/finsky/billing/iab/DbHelper/<init>(Landroid/content/Context;Ljava/lang/String;)V
invokevirtual com/google/android/finsky/billing/iab/DbHelper/getItems()Ljava/util/ArrayList;
astore 5
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 2
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 3
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 4
aload 5
invokevirtual java/util/ArrayList/size()I
ifeq L0
aload 5
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 5
L1:
aload 5
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 5
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/google/android/finsky/billing/iab/ItemsListItem
astore 6
aload 6
getfield com/google/android/finsky/billing/iab/ItemsListItem/pData Ljava/lang/String;
ldc "auto.repeat.LP"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L1
aload 2
aload 6
getfield com/google/android/finsky/billing/iab/ItemsListItem/itemID Ljava/lang/String;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 3
aload 6
getfield com/google/android/finsky/billing/iab/ItemsListItem/pData Ljava/lang/String;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 6
getfield com/google/android/finsky/billing/iab/ItemsListItem/pSignature Ljava/lang/String;
ldc "1"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L2
aload 4
aload 6
getfield com/google/android/finsky/billing/iab/ItemsListItem/pData Ljava/lang/String;
invokestatic com/chelpus/Utils/gen_sha1withrsa(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
goto L1
L2:
aload 4
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
goto L1
L0:
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 5
aload 5
ldc "RESPONSE_CODE"
iconst_0
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
aload 5
ldc "INAPP_PURCHASE_ITEM_LIST"
aload 2
invokevirtual android/os/Bundle/putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V
aload 5
ldc "INAPP_PURCHASE_DATA_LIST"
aload 3
invokevirtual android/os/Bundle/putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V
aload 5
ldc "INAPP_DATA_SIGNATURE_LIST"
aload 4
invokevirtual android/os/Bundle/putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V
aload 5
areturn
.limit locals 7
.limit stack 4
.end method

.method public getSkuDetails(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
.throws android/os/RemoteException
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
.catch java/lang/Exception from L4 to L5 using L6
.catch java/lang/Exception from L7 to L8 using L6
.catch java/lang/Exception from L9 to L10 using L6
.catch java/lang/Exception from L11 to L12 using L6
.catch java/lang/Exception from L13 to L14 using L6
.catch java/lang/Exception from L14 to L15 using L6
.catch java/lang/Exception from L16 to L17 using L6
.catch java/lang/Exception from L18 to L19 using L2
.catch org/json/JSONException from L20 to L21 using L22
.catch org/json/JSONException from L23 to L24 using L25
ldc "BillingHack"
ldc "getSkuDetails"
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
L0:
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$6/this$0 Lcom/google/android/finsky/billing/iab/InAppBillingService;
getfield com/google/android/finsky/billing/iab/InAppBillingService/mSetupDone Z
ifne L1
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$6/this$0 Lcom/google/android/finsky/billing/iab/InAppBillingService;
getfield com/google/android/finsky/billing/iab/InAppBillingService/skipSetupDone Z
ifne L1
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$6/this$0 Lcom/google/android/finsky/billing/iab/InAppBillingService;
aload 2
invokevirtual com/google/android/finsky/billing/iab/InAppBillingService/connectToBilling(Ljava/lang/String;)V
L1:
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
ldc "RESPONSE_CODE"
iconst_0
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$6/this$0 Lcom/google/android/finsky/billing/iab/InAppBillingService;
getfield com/google/android/finsky/billing/iab/InAppBillingService/mSetupDone Z
ifeq L8
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$6/this$0 Lcom/google/android/finsky/billing/iab/InAppBillingService;
getfield com/google/android/finsky/billing/iab/InAppBillingService/mService Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;
ifnull L8
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$6/this$0 Lcom/google/android/finsky/billing/iab/InAppBillingService;
getfield com/google/android/finsky/billing/iab/InAppBillingService/skipSetupDone Z
istore 5
L3:
iload 5
ifne L8
L4:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Connect to google billing"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$6/this$0 Lcom/google/android/finsky/billing/iab/InAppBillingService;
getfield com/google/android/finsky/billing/iab/InAppBillingService/mService Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;
iload 1
aload 2
aload 3
aload 4
invokeinterface com/google/android/finsky/billing/iab/google/util/IInAppBillingService/getSkuDetails(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle; 4
astore 7
aload 7
ldc "DETAILS_LIST"
invokevirtual android/os/Bundle/getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;
astore 8
aload 4
ldc "ITEM_ID_LIST"
invokevirtual android/os/Bundle/getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;
pop
aload 7
ldc "RESPONSE_CODE"
invokevirtual android/os/Bundle/getInt(Ljava/lang/String;)I
istore 1
getstatic java/lang/System/out Ljava/io/PrintStream;
iload 1
invokevirtual java/io/PrintStream/println(I)V
L5:
iload 1
ifeq L26
L7:
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$6/this$0 Lcom/google/android/finsky/billing/iab/InAppBillingService;
iconst_1
putfield com/google/android/finsky/billing/iab/InAppBillingService/skipSetupDone Z
L8:
aload 4
ldc "ITEM_ID_LIST"
invokevirtual android/os/Bundle/getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;
astore 4
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 7
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$6/this$0 Lcom/google/android/finsky/billing/iab/InAppBillingService;
getfield com/google/android/finsky/billing/iab/InAppBillingService/mSetupDone Z
ifeq L27
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$6/this$0 Lcom/google/android/finsky/billing/iab/InAppBillingService;
getfield com/google/android/finsky/billing/iab/InAppBillingService/mService Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;
ifnull L27
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$6/this$0 Lcom/google/android/finsky/billing/iab/InAppBillingService;
getfield com/google/android/finsky/billing/iab/InAppBillingService/skipSetupDone Z
ifeq L28
L27:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Dont Connect to google billing"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 4
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 6
L29:
aload 6
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L30
aload 6
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 8
aload 3
ldc "subs"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L31
iconst_0
istore 1
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$6/productIDsubs Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 9
L32:
aload 9
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L33
aload 8
aload 9
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L32
iconst_1
istore 1
goto L32
L26:
aload 7
astore 6
aload 8
ifnull L34
aload 7
astore 6
L9:
aload 8
invokevirtual java/util/ArrayList/size()I
ifeq L34
aload 8
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 8
L10:
aload 7
astore 6
L11:
aload 8
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L34
new org/json/JSONObject
dup
aload 8
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
invokespecial org/json/JSONObject/<init>(Ljava/lang/String;)V
astore 6
aload 6
ldc "type"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
ldc "inapp"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L10
L12:
iconst_0
istore 1
L13:
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$6/productIDinapp Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 9
L14:
aload 9
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L35
aload 9
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 10
aload 6
ldc "productId"
invokevirtual org/json/JSONObject/get(Ljava/lang/String;)Ljava/lang/Object;
aload 10
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L14
L15:
iconst_1
istore 1
goto L14
L35:
iload 1
ifne L10
L16:
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$6/productIDinapp Ljava/util/ArrayList;
aload 6
ldc "productId"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L17:
goto L10
L6:
astore 6
L18:
aload 6
invokevirtual java/lang/Exception/printStackTrace()V
L19:
goto L8
L2:
astore 6
aload 6
invokevirtual java/lang/Exception/printStackTrace()V
goto L8
L33:
iload 1
ifne L31
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$6/productIDsubs Ljava/util/ArrayList;
aload 8
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L31:
aload 3
ldc "inapp"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L29
iconst_0
istore 1
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$6/productIDinapp Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 9
L36:
aload 9
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L37
aload 8
aload 9
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L36
iconst_1
istore 1
goto L36
L37:
iload 1
ifne L29
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$6/productIDinapp Ljava/util/ArrayList;
aload 8
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
goto L29
L30:
aload 4
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 8
L38:
aload 8
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L28
aload 8
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 9
aload 9
ldc "\\."
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 6
ldc ""
astore 4
aload 6
ifnull L39
aload 6
arraylength
ifeq L39
aload 6
arraylength
iconst_2
if_icmplt L40
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 6
aload 6
arraylength
iconst_2
isub
aaload
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
aload 6
arraylength
iconst_1
isub
aaload
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 4
L40:
aload 6
arraylength
iconst_1
if_icmpne L41
aload 6
iconst_0
aaload
ldc "_"
ldc " "
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 4
L41:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "$0."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lconst_0
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 6
new com/google/android/finsky/billing/iab/DbHelper
dup
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$6/this$0 Lcom/google/android/finsky/billing/iab/InAppBillingService;
getfield com/google/android/finsky/billing/iab/InAppBillingService/mContext Landroid/content/Context;
aload 2
invokespecial com/google/android/finsky/billing/iab/DbHelper/<init>(Landroid/content/Context;Ljava/lang/String;)V
invokevirtual com/google/android/finsky/billing/iab/DbHelper/getItems()Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 10
L42:
aload 10
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L43
aload 10
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/google/android/finsky/billing/iab/ItemsListItem
getfield com/google/android/finsky/billing/iab/ItemsListItem/itemID Ljava/lang/String;
aload 9
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L42
ldc "Purchased"
astore 6
goto L42
L39:
aload 9
ldc "_"
ldc " "
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 4
goto L41
L43:
aload 3
ldc "subs"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L44
iconst_0
istore 1
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$6/productIDinapp Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 10
L45:
aload 10
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L46
aload 10
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
aload 9
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L45
iconst_1
istore 1
goto L45
L46:
iload 1
ifne L47
new org/json/JSONObject
dup
invokespecial org/json/JSONObject/<init>()V
astore 10
L20:
aload 10
ldc "productId"
aload 9
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 10
ldc "type"
aload 3
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 10
ldc "price"
aload 6
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 10
ldc "title"
aload 4
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 10
ldc "description"
aload 4
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 10
ldc "price_amount_micros"
ldc2_w 1000000L
lconst_0
lmul
invokevirtual org/json/JSONObject/put(Ljava/lang/String;J)Lorg/json/JSONObject;
pop
aload 10
ldc "price_currency_code"
ldc "USD"
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
L21:
aload 7
aload 10
invokevirtual org/json/JSONObject/toString()Ljava/lang/String;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
goto L38
L22:
astore 4
aload 4
invokevirtual org/json/JSONException/printStackTrace()V
goto L21
L47:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "skip "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 9
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
goto L38
L44:
new org/json/JSONObject
dup
invokespecial org/json/JSONObject/<init>()V
astore 10
L23:
aload 10
ldc "productId"
aload 9
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 10
ldc "type"
aload 3
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 10
ldc "price"
aload 6
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 10
ldc "title"
aload 4
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 10
ldc "description"
aload 4
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 10
ldc "price_amount_micros"
ldc2_w 1000000L
lconst_0
lmul
invokevirtual org/json/JSONObject/put(Ljava/lang/String;J)Lorg/json/JSONObject;
pop
aload 10
ldc "price_currency_code"
ldc "USD"
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
L24:
aload 7
aload 10
invokevirtual org/json/JSONObject/toString()Ljava/lang/String;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
goto L38
L25:
astore 4
aload 4
invokevirtual org/json/JSONException/printStackTrace()V
goto L24
L28:
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 6
aload 6
ldc "RESPONSE_CODE"
iconst_0
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
aload 6
ldc "DETAILS_LIST"
aload 7
invokevirtual android/os/Bundle/putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V
L34:
aload 6
areturn
.limit locals 11
.limit stack 6
.end method

.method public isBillingSupported(ILjava/lang/String;Ljava/lang/String;)I
.throws android/os/RemoteException
ldc "BillingHack"
ldc "isBillingSupported"
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$6/this$0 Lcom/google/android/finsky/billing/iab/InAppBillingService;
invokevirtual com/google/android/finsky/billing/iab/InAppBillingService/startGoogleBilling()V
iconst_0
ireturn
.limit locals 4
.limit stack 2
.end method
