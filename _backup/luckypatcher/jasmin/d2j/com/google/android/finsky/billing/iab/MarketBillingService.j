.bytecode 50.0
.class public synchronized com/google/android/finsky/billing/iab/MarketBillingService
.super android/app/Service
.inner class inner com/google/android/finsky/billing/iab/MarketBillingService$1
.inner class public BillingNotifier inner com/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier outer com/google/android/finsky/billing/iab/MarketBillingService

.field public static final 'TAG' Ljava/lang/String; = "BillingHack"

.field private static 'context' Landroid/content/Context;

.field private static 'dev_pay' Ljava/lang/String;

.field private static 'item' Ljava/lang/String;

.field private static 'sRandom' Ljava/util/Random;

.field private final 'mBinder' Lcom/android/vending/billing/IMarketBillingService$Stub;

.field 'mNotifier' Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;

.field 'mPackageManager' Landroid/content/pm/PackageManager;

.method static <clinit>()V
ldc ""
putstatic com/google/android/finsky/billing/iab/MarketBillingService/item Ljava/lang/String;
ldc ""
putstatic com/google/android/finsky/billing/iab/MarketBillingService/dev_pay Ljava/lang/String;
new java/util/Random
dup
invokespecial java/util/Random/<init>()V
putstatic com/google/android/finsky/billing/iab/MarketBillingService/sRandom Ljava/util/Random;
new java/util/Random
dup
invokespecial java/util/Random/<init>()V
putstatic com/google/android/finsky/billing/iab/MarketBillingService/sRandom Ljava/util/Random;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial android/app/Service/<init>()V
aload 0
new com/google/android/finsky/billing/iab/MarketBillingService$1
dup
aload 0
invokespecial com/google/android/finsky/billing/iab/MarketBillingService$1/<init>(Lcom/google/android/finsky/billing/iab/MarketBillingService;)V
putfield com/google/android/finsky/billing/iab/MarketBillingService/mBinder Lcom/android/vending/billing/IMarketBillingService$Stub;
aload 0
new com/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier
dup
aload 0
aload 0
invokespecial com/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier/<init>(Lcom/google/android/finsky/billing/iab/MarketBillingService;Lcom/google/android/finsky/billing/iab/MarketBillingService;)V
putfield com/google/android/finsky/billing/iab/MarketBillingService/mNotifier Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;
return
.limit locals 1
.limit stack 5
.end method

.method static synthetic access$000()Ljava/lang/String;
getstatic com/google/android/finsky/billing/iab/MarketBillingService/item Ljava/lang/String;
areturn
.limit locals 0
.limit stack 1
.end method

.method static synthetic access$002(Ljava/lang/String;)Ljava/lang/String;
aload 0
putstatic com/google/android/finsky/billing/iab/MarketBillingService/item Ljava/lang/String;
aload 0
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$100()Ljava/lang/String;
getstatic com/google/android/finsky/billing/iab/MarketBillingService/dev_pay Ljava/lang/String;
areturn
.limit locals 0
.limit stack 1
.end method

.method static synthetic access$102(Ljava/lang/String;)Ljava/lang/String;
aload 0
putstatic com/google/android/finsky/billing/iab/MarketBillingService/dev_pay Ljava/lang/String;
aload 0
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$200()Ljava/util/Random;
getstatic com/google/android/finsky/billing/iab/MarketBillingService/sRandom Ljava/util/Random;
areturn
.limit locals 0
.limit stack 1
.end method

.method public static findReceiverName(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;)Landroid/content/Intent;
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
aload 2
iconst_0
invokevirtual android/content/pm/PackageManager/queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;
invokeinterface java/util/List/listIterator()Ljava/util/ListIterator; 0
astore 0
L0:
aload 0
invokeinterface java/util/ListIterator/hasNext()Z 0
ifeq L1
aload 0
invokeinterface java/util/ListIterator/next()Ljava/lang/Object; 0
astore 3
aload 3
checkcast android/content/pm/ResolveInfo
getfield android/content/pm/ResolveInfo/activityInfo Landroid/content/pm/ActivityInfo;
ifnull L0
aload 1
aload 3
checkcast android/content/pm/ResolveInfo
getfield android/content/pm/ResolveInfo/activityInfo Landroid/content/pm/ActivityInfo;
getfield android/content/pm/ActivityInfo/packageName Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
aload 2
aload 1
aload 3
checkcast android/content/pm/ResolveInfo
getfield android/content/pm/ResolveInfo/activityInfo Landroid/content/pm/ActivityInfo;
getfield android/content/pm/ActivityInfo/name Ljava/lang/String;
invokevirtual android/content/Intent/setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 2
aload 1
invokevirtual android/content/Intent/setPackage(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 2
areturn
L1:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Cannot find billing receiver in package '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "' for action: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual android/content/Intent/getAction()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aconst_null
areturn
.limit locals 4
.limit stack 3
.end method

.method public static sendResponseCode(Landroid/content/Context;Ljava/lang/String;JI)Z
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
aload 1
new android/content/Intent
dup
ldc "com.android.vending.billing.RESPONSE_CODE"
invokespecial android/content/Intent/<init>(Ljava/lang/String;)V
invokestatic com/google/android/finsky/billing/iab/MarketBillingService/findReceiverName(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;)Landroid/content/Intent;
astore 0
aload 0
ifnonnull L0
iconst_0
ireturn
L0:
aload 0
ldc "request_id"
lload 2
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;J)Landroid/content/Intent;
pop
aload 0
ldc "response_code"
iload 4
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;I)Landroid/content/Intent;
pop
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
aload 0
invokevirtual android/content/Context/sendBroadcast(Landroid/content/Intent;)V
iconst_1
ireturn
.limit locals 5
.limit stack 5
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
aload 0
getfield com/google/android/finsky/billing/iab/MarketBillingService/mBinder Lcom/android/vending/billing/IMarketBillingService$Stub;
areturn
.limit locals 2
.limit stack 1
.end method

.method public onCreate()V
aload 0
invokespecial android/app/Service/onCreate()V
aload 0
putstatic com/google/android/finsky/billing/iab/MarketBillingService/context Landroid/content/Context;
return
.limit locals 1
.limit stack 1
.end method
