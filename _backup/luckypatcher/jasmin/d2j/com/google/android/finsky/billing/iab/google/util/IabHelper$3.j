.bytecode 50.0
.class synchronized com/google/android/finsky/billing/iab/google/util/IabHelper$3
.super java/lang/Object
.implements java/lang/Runnable
.enclosing method com/google/android/finsky/billing/iab/google/util/IabHelper/consumeAsyncInternal(Ljava/util/List;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeFinishedListener;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeMultiFinishedListener;)V
.inner class inner com/google/android/finsky/billing/iab/google/util/IabHelper$3
.inner class inner com/google/android/finsky/billing/iab/google/util/IabHelper$3$1
.inner class inner com/google/android/finsky/billing/iab/google/util/IabHelper$3$2

.field final synthetic 'this$0' Lcom/google/android/finsky/billing/iab/google/util/IabHelper;

.field final synthetic 'val$handler' Landroid/os/Handler;

.field final synthetic 'val$multiListener' Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeMultiFinishedListener;

.field final synthetic 'val$purchases' Ljava/util/List;

.field final synthetic 'val$singleListener' Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeFinishedListener;

.method <init>(Lcom/google/android/finsky/billing/iab/google/util/IabHelper;Ljava/util/List;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeFinishedListener;Landroid/os/Handler;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeMultiFinishedListener;)V
aload 0
aload 1
putfield com/google/android/finsky/billing/iab/google/util/IabHelper$3/this$0 Lcom/google/android/finsky/billing/iab/google/util/IabHelper;
aload 0
aload 2
putfield com/google/android/finsky/billing/iab/google/util/IabHelper$3/val$purchases Ljava/util/List;
aload 0
aload 3
putfield com/google/android/finsky/billing/iab/google/util/IabHelper$3/val$singleListener Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeFinishedListener;
aload 0
aload 4
putfield com/google/android/finsky/billing/iab/google/util/IabHelper$3/val$handler Landroid/os/Handler;
aload 0
aload 5
putfield com/google/android/finsky/billing/iab/google/util/IabHelper$3/val$multiListener Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeMultiFinishedListener;
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 6
.limit stack 2
.end method

.method public run()V
.catch com/google/android/finsky/billing/iab/google/util/IabException from L0 to L1 using L2
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 1
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper$3/val$purchases Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 2
L3:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/google/android/finsky/billing/iab/google/util/Purchase
astore 3
L0:
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper$3/this$0 Lcom/google/android/finsky/billing/iab/google/util/IabHelper;
aload 3
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/consume(Lcom/google/android/finsky/billing/iab/google/util/Purchase;)V
aload 1
new com/google/android/finsky/billing/iab/google/util/IabResult
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Successful consume of sku "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual com/google/android/finsky/billing/iab/google/util/Purchase/getSku()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial com/google/android/finsky/billing/iab/google/util/IabResult/<init>(ILjava/lang/String;)V
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L1:
goto L3
L2:
astore 3
aload 1
aload 3
invokevirtual com/google/android/finsky/billing/iab/google/util/IabException/getResult()Lcom/google/android/finsky/billing/iab/google/util/IabResult;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
goto L3
L4:
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper$3/this$0 Lcom/google/android/finsky/billing/iab/google/util/IabHelper;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/flagEndAsync()V
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper$3/this$0 Lcom/google/android/finsky/billing/iab/google/util/IabHelper;
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mDisposed Z
ifne L5
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper$3/val$singleListener Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeFinishedListener;
ifnull L5
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper$3/val$handler Landroid/os/Handler;
new com/google/android/finsky/billing/iab/google/util/IabHelper$3$1
dup
aload 0
aload 1
invokespecial com/google/android/finsky/billing/iab/google/util/IabHelper$3$1/<init>(Lcom/google/android/finsky/billing/iab/google/util/IabHelper$3;Ljava/util/List;)V
invokevirtual android/os/Handler/post(Ljava/lang/Runnable;)Z
pop
L5:
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper$3/this$0 Lcom/google/android/finsky/billing/iab/google/util/IabHelper;
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mDisposed Z
ifne L6
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper$3/val$multiListener Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeMultiFinishedListener;
ifnull L6
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper$3/val$handler Landroid/os/Handler;
new com/google/android/finsky/billing/iab/google/util/IabHelper$3$2
dup
aload 0
aload 1
invokespecial com/google/android/finsky/billing/iab/google/util/IabHelper$3$2/<init>(Lcom/google/android/finsky/billing/iab/google/util/IabHelper$3;Ljava/util/List;)V
invokevirtual android/os/Handler/post(Ljava/lang/Runnable;)Z
pop
L6:
return
.limit locals 4
.limit stack 6
.end method
