.bytecode 50.0
.class synchronized com/google/android/finsky/services/LicensingService$1
.super java/lang/Object
.implements android/content/ServiceConnection
.enclosing method com/google/android/finsky/services/LicensingService/connectToLicensing(JLjava/lang/String;)V
.inner class inner com/google/android/finsky/services/LicensingService$1
.inner class inner com/google/android/finsky/services/LicensingService$1$1

.field final synthetic 'this$0' Lcom/google/android/finsky/services/LicensingService;

.field final synthetic 'val$nonce' J

.field final synthetic 'val$pkgName' Ljava/lang/String;

.method <init>(Lcom/google/android/finsky/services/LicensingService;Ljava/lang/String;J)V
aload 0
aload 1
putfield com/google/android/finsky/services/LicensingService$1/this$0 Lcom/google/android/finsky/services/LicensingService;
aload 0
aload 2
putfield com/google/android/finsky/services/LicensingService$1/val$pkgName Ljava/lang/String;
aload 0
lload 3
putfield com/google/android/finsky/services/LicensingService$1/val$nonce J
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 5
.limit stack 3
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
.catch android/os/RemoteException from L0 to L1 using L2
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Licensing service try to connect."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
getfield com/google/android/finsky/services/LicensingService$1/this$0 Lcom/google/android/finsky/services/LicensingService;
aload 2
invokestatic com/android/vending/licensing/ILicensingService$Stub/asInterface(Landroid/os/IBinder;)Lcom/android/vending/licensing/ILicensingService;
putfield com/google/android/finsky/services/LicensingService/mService Lcom/android/vending/licensing/ILicensingService;
L0:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Calling checkLicense on service for "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/google/android/finsky/services/LicensingService$1/val$pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
getfield com/google/android/finsky/services/LicensingService$1/this$0 Lcom/google/android/finsky/services/LicensingService;
getfield com/google/android/finsky/services/LicensingService/mService Lcom/android/vending/licensing/ILicensingService;
aload 0
getfield com/google/android/finsky/services/LicensingService$1/val$nonce J
aload 0
getfield com/google/android/finsky/services/LicensingService$1/val$pkgName Ljava/lang/String;
new com/google/android/finsky/services/LicensingService$1$1
dup
aload 0
invokespecial com/google/android/finsky/services/LicensingService$1$1/<init>(Lcom/google/android/finsky/services/LicensingService$1;)V
invokeinterface com/android/vending/licensing/ILicensingService/checkLicense(JLjava/lang/String;Lcom/android/vending/licensing/ILicenseResultListener;)V 4
L1:
return
L2:
astore 1
aload 1
invokevirtual android/os/RemoteException/printStackTrace()V
aload 0
getfield com/google/android/finsky/services/LicensingService$1/this$0 Lcom/google/android/finsky/services/LicensingService;
iconst_1
putfield com/google/android/finsky/services/LicensingService/mSetupDone Z
return
.limit locals 3
.limit stack 7
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Licensing service disconnected."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
getfield com/google/android/finsky/services/LicensingService$1/this$0 Lcom/google/android/finsky/services/LicensingService;
aconst_null
putfield com/google/android/finsky/services/LicensingService/mService Lcom/android/vending/licensing/ILicensingService;
return
.limit locals 2
.limit stack 2
.end method
