.bytecode 50.0
.class public synchronized com/google/android/finsky/billing/iab/google/util/Inventory
.super java/lang/Object

.field 'mPurchaseMap' Ljava/util/Map; signature "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/finsky/billing/iab/google/util/Purchase;>;"

.field 'mSkuMap' Ljava/util/Map; signature "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/finsky/billing/iab/google/util/SkuDetails;>;"

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield com/google/android/finsky/billing/iab/google/util/Inventory/mSkuMap Ljava/util/Map;
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield com/google/android/finsky/billing/iab/google/util/Inventory/mPurchaseMap Ljava/util/Map;
return
.limit locals 1
.limit stack 3
.end method

.method addPurchase(Lcom/google/android/finsky/billing/iab/google/util/Purchase;)V
aload 0
getfield com/google/android/finsky/billing/iab/google/util/Inventory/mPurchaseMap Ljava/util/Map;
aload 1
invokevirtual com/google/android/finsky/billing/iab/google/util/Purchase/getSku()Ljava/lang/String;
aload 1
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
return
.limit locals 2
.limit stack 3
.end method

.method addSkuDetails(Lcom/google/android/finsky/billing/iab/google/util/SkuDetails;)V
aload 0
getfield com/google/android/finsky/billing/iab/google/util/Inventory/mSkuMap Ljava/util/Map;
aload 1
invokevirtual com/google/android/finsky/billing/iab/google/util/SkuDetails/getSku()Ljava/lang/String;
aload 1
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
return
.limit locals 2
.limit stack 3
.end method

.method public erasePurchase(Ljava/lang/String;)V
aload 0
getfield com/google/android/finsky/billing/iab/google/util/Inventory/mPurchaseMap Ljava/util/Map;
aload 1
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ifeq L0
aload 0
getfield com/google/android/finsky/billing/iab/google/util/Inventory/mPurchaseMap Ljava/util/Map;
aload 1
invokeinterface java/util/Map/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
pop
L0:
return
.limit locals 2
.limit stack 2
.end method

.method getAllOwnedSkus()Ljava/util/List;
.signature "()Ljava/util/List<Ljava/lang/String;>;"
new java/util/ArrayList
dup
aload 0
getfield com/google/android/finsky/billing/iab/google/util/Inventory/mPurchaseMap Ljava/util/Map;
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
invokespecial java/util/ArrayList/<init>(Ljava/util/Collection;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method getAllOwnedSkus(Ljava/lang/String;)Ljava/util/List;
.signature "(Ljava/lang/String;)Ljava/util/List<Ljava/lang/String;>;"
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 2
aload 0
getfield com/google/android/finsky/billing/iab/google/util/Inventory/mPurchaseMap Ljava/util/Map;
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 3
L0:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/google/android/finsky/billing/iab/google/util/Purchase
astore 4
aload 4
invokevirtual com/google/android/finsky/billing/iab/google/util/Purchase/getItemType()Ljava/lang/String;
aload 1
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
aload 2
aload 4
invokevirtual com/google/android/finsky/billing/iab/google/util/Purchase/getSku()Ljava/lang/String;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
goto L0
L1:
aload 2
areturn
.limit locals 5
.limit stack 2
.end method

.method getAllPurchases()Ljava/util/List;
.signature "()Ljava/util/List<Lcom/google/android/finsky/billing/iab/google/util/Purchase;>;"
new java/util/ArrayList
dup
aload 0
getfield com/google/android/finsky/billing/iab/google/util/Inventory/mPurchaseMap Ljava/util/Map;
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokespecial java/util/ArrayList/<init>(Ljava/util/Collection;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public getPurchase(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/google/util/Purchase;
aload 0
getfield com/google/android/finsky/billing/iab/google/util/Inventory/mPurchaseMap Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/google/android/finsky/billing/iab/google/util/Purchase
areturn
.limit locals 2
.limit stack 2
.end method

.method public getSkuDetails(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/google/util/SkuDetails;
aload 0
getfield com/google/android/finsky/billing/iab/google/util/Inventory/mSkuMap Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/google/android/finsky/billing/iab/google/util/SkuDetails
areturn
.limit locals 2
.limit stack 2
.end method

.method public hasDetails(Ljava/lang/String;)Z
aload 0
getfield com/google/android/finsky/billing/iab/google/util/Inventory/mSkuMap Ljava/util/Map;
aload 1
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public hasPurchase(Ljava/lang/String;)Z
aload 0
getfield com/google/android/finsky/billing/iab/google/util/Inventory/mPurchaseMap Ljava/util/Map;
aload 1
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method
