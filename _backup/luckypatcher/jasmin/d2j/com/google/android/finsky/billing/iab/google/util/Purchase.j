.bytecode 50.0
.class public synchronized com/google/android/finsky/billing/iab/google/util/Purchase
.super java/lang/Object

.field 'mDeveloperPayload' Ljava/lang/String;

.field 'mItemType' Ljava/lang/String;

.field 'mOrderId' Ljava/lang/String;

.field 'mOriginalJson' Ljava/lang/String;

.field 'mPackageName' Ljava/lang/String;

.field 'mPurchaseState' I

.field 'mPurchaseTime' J

.field 'mSignature' Ljava/lang/String;

.field 'mSku' Ljava/lang/String;

.field 'mToken' Ljava/lang/String;

.method public <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.throws org/json/JSONException
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/google/android/finsky/billing/iab/google/util/Purchase/mItemType Ljava/lang/String;
aload 0
aload 2
putfield com/google/android/finsky/billing/iab/google/util/Purchase/mOriginalJson Ljava/lang/String;
new org/json/JSONObject
dup
aload 0
getfield com/google/android/finsky/billing/iab/google/util/Purchase/mOriginalJson Ljava/lang/String;
invokespecial org/json/JSONObject/<init>(Ljava/lang/String;)V
astore 1
aload 0
aload 1
ldc "orderId"
invokevirtual org/json/JSONObject/optString(Ljava/lang/String;)Ljava/lang/String;
putfield com/google/android/finsky/billing/iab/google/util/Purchase/mOrderId Ljava/lang/String;
aload 0
aload 1
ldc "packageName"
invokevirtual org/json/JSONObject/optString(Ljava/lang/String;)Ljava/lang/String;
putfield com/google/android/finsky/billing/iab/google/util/Purchase/mPackageName Ljava/lang/String;
aload 0
aload 1
ldc "productId"
invokevirtual org/json/JSONObject/optString(Ljava/lang/String;)Ljava/lang/String;
putfield com/google/android/finsky/billing/iab/google/util/Purchase/mSku Ljava/lang/String;
aload 0
aload 1
ldc "purchaseTime"
invokevirtual org/json/JSONObject/optLong(Ljava/lang/String;)J
putfield com/google/android/finsky/billing/iab/google/util/Purchase/mPurchaseTime J
aload 0
aload 1
ldc "purchaseState"
invokevirtual org/json/JSONObject/optInt(Ljava/lang/String;)I
putfield com/google/android/finsky/billing/iab/google/util/Purchase/mPurchaseState I
aload 0
aload 1
ldc "developerPayload"
invokevirtual org/json/JSONObject/optString(Ljava/lang/String;)Ljava/lang/String;
putfield com/google/android/finsky/billing/iab/google/util/Purchase/mDeveloperPayload Ljava/lang/String;
aload 0
aload 1
ldc "token"
aload 1
ldc "purchaseToken"
invokevirtual org/json/JSONObject/optString(Ljava/lang/String;)Ljava/lang/String;
invokevirtual org/json/JSONObject/optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
putfield com/google/android/finsky/billing/iab/google/util/Purchase/mToken Ljava/lang/String;
aload 0
aload 3
putfield com/google/android/finsky/billing/iab/google/util/Purchase/mSignature Ljava/lang/String;
return
.limit locals 4
.limit stack 5
.end method

.method public getDeveloperPayload()Ljava/lang/String;
aload 0
getfield com/google/android/finsky/billing/iab/google/util/Purchase/mDeveloperPayload Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getItemType()Ljava/lang/String;
aload 0
getfield com/google/android/finsky/billing/iab/google/util/Purchase/mItemType Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getOrderId()Ljava/lang/String;
aload 0
getfield com/google/android/finsky/billing/iab/google/util/Purchase/mOrderId Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getOriginalJson()Ljava/lang/String;
aload 0
getfield com/google/android/finsky/billing/iab/google/util/Purchase/mOriginalJson Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getPackageName()Ljava/lang/String;
aload 0
getfield com/google/android/finsky/billing/iab/google/util/Purchase/mPackageName Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getPurchaseState()I
aload 0
getfield com/google/android/finsky/billing/iab/google/util/Purchase/mPurchaseState I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getPurchaseTime()J
aload 0
getfield com/google/android/finsky/billing/iab/google/util/Purchase/mPurchaseTime J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getSignature()Ljava/lang/String;
aload 0
getfield com/google/android/finsky/billing/iab/google/util/Purchase/mSignature Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getSku()Ljava/lang/String;
aload 0
getfield com/google/android/finsky/billing/iab/google/util/Purchase/mSku Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getToken()Ljava/lang/String;
aload 0
getfield com/google/android/finsky/billing/iab/google/util/Purchase/mToken Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "PurchaseInfo(type:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/google/android/finsky/billing/iab/google/util/Purchase/mItemType Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "):"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/google/android/finsky/billing/iab/google/util/Purchase/mOriginalJson Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method
