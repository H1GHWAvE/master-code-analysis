.bytecode 50.0
.class synchronized com/google/android/finsky/billing/iab/google/util/IabHelper$1
.super java/lang/Object
.implements android/content/ServiceConnection
.enclosing method com/google/android/finsky/billing/iab/google/util/IabHelper/startSetup(Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabSetupFinishedListener;)V
.inner class inner com/google/android/finsky/billing/iab/google/util/IabHelper$1

.field final synthetic 'this$0' Lcom/google/android/finsky/billing/iab/google/util/IabHelper;

.field final synthetic 'val$listener' Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabSetupFinishedListener;

.method <init>(Lcom/google/android/finsky/billing/iab/google/util/IabHelper;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabSetupFinishedListener;)V
aload 0
aload 1
putfield com/google/android/finsky/billing/iab/google/util/IabHelper$1/this$0 Lcom/google/android/finsky/billing/iab/google/util/IabHelper;
aload 0
aload 2
putfield com/google/android/finsky/billing/iab/google/util/IabHelper$1/val$listener Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabSetupFinishedListener;
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 3
.limit stack 2
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
.catch android/os/RemoteException from L0 to L1 using L2
.catch android/os/RemoteException from L3 to L4 using L2
.catch android/os/RemoteException from L4 to L5 using L2
.catch android/os/RemoteException from L6 to L7 using L2
.catch android/os/RemoteException from L8 to L9 using L2
.catch android/os/RemoteException from L9 to L10 using L2
.catch android/os/RemoteException from L11 to L12 using L2
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper$1/this$0 Lcom/google/android/finsky/billing/iab/google/util/IabHelper;
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mDisposed Z
ifeq L13
L14:
return
L13:
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper$1/this$0 Lcom/google/android/finsky/billing/iab/google/util/IabHelper;
ldc "Billing service connected."
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper$1/this$0 Lcom/google/android/finsky/billing/iab/google/util/IabHelper;
aload 2
invokestatic com/google/android/finsky/billing/iab/google/util/IInAppBillingService$Stub/asInterface(Landroid/os/IBinder;)Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mService Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper$1/this$0 Lcom/google/android/finsky/billing/iab/google/util/IabHelper;
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mContext Landroid/content/Context;
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
astore 1
L0:
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper$1/this$0 Lcom/google/android/finsky/billing/iab/google/util/IabHelper;
ldc "Checking for in-app billing 3 support."
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper$1/this$0 Lcom/google/android/finsky/billing/iab/google/util/IabHelper;
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mService Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;
iconst_3
aload 1
ldc "inapp"
invokeinterface com/google/android/finsky/billing/iab/google/util/IInAppBillingService/isBillingSupported(ILjava/lang/String;Ljava/lang/String;)I 3
istore 3
L1:
iload 3
ifeq L6
L3:
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper$1/val$listener Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabSetupFinishedListener;
ifnull L4
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper$1/val$listener Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabSetupFinishedListener;
new com/google/android/finsky/billing/iab/google/util/IabResult
dup
iload 3
ldc "Error checking for billing v3 support."
invokespecial com/google/android/finsky/billing/iab/google/util/IabResult/<init>(ILjava/lang/String;)V
invokeinterface com/google/android/finsky/billing/iab/google/util/IabHelper$OnIabSetupFinishedListener/onIabSetupFinished(Lcom/google/android/finsky/billing/iab/google/util/IabResult;)V 1
L4:
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper$1/this$0 Lcom/google/android/finsky/billing/iab/google/util/IabHelper;
iconst_0
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mSubscriptionsSupported Z
L5:
return
L2:
astore 1
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper$1/val$listener Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabSetupFinishedListener;
ifnull L15
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper$1/val$listener Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabSetupFinishedListener;
new com/google/android/finsky/billing/iab/google/util/IabResult
dup
sipush -1001
ldc "RemoteException while setting up in-app billing."
invokespecial com/google/android/finsky/billing/iab/google/util/IabResult/<init>(ILjava/lang/String;)V
invokeinterface com/google/android/finsky/billing/iab/google/util/IabHelper$OnIabSetupFinishedListener/onIabSetupFinished(Lcom/google/android/finsky/billing/iab/google/util/IabResult;)V 1
L15:
aload 1
invokevirtual android/os/RemoteException/printStackTrace()V
return
L6:
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper$1/this$0 Lcom/google/android/finsky/billing/iab/google/util/IabHelper;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "In-app billing version 3 supported for "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper$1/this$0 Lcom/google/android/finsky/billing/iab/google/util/IabHelper;
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mService Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;
iconst_3
aload 1
ldc "subs"
invokeinterface com/google/android/finsky/billing/iab/google/util/IInAppBillingService/isBillingSupported(ILjava/lang/String;Ljava/lang/String;)I 3
istore 3
L7:
iload 3
ifne L11
L8:
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper$1/this$0 Lcom/google/android/finsky/billing/iab/google/util/IabHelper;
ldc "Subscriptions AVAILABLE."
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper$1/this$0 Lcom/google/android/finsky/billing/iab/google/util/IabHelper;
iconst_1
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mSubscriptionsSupported Z
L9:
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper$1/this$0 Lcom/google/android/finsky/billing/iab/google/util/IabHelper;
iconst_1
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mSetupDone Z
L10:
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper$1/val$listener Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabSetupFinishedListener;
ifnull L14
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper$1/val$listener Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabSetupFinishedListener;
new com/google/android/finsky/billing/iab/google/util/IabResult
dup
iconst_0
ldc "Setup successful."
invokespecial com/google/android/finsky/billing/iab/google/util/IabResult/<init>(ILjava/lang/String;)V
invokeinterface com/google/android/finsky/billing/iab/google/util/IabHelper$OnIabSetupFinishedListener/onIabSetupFinished(Lcom/google/android/finsky/billing/iab/google/util/IabResult;)V 1
return
L11:
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper$1/this$0 Lcom/google/android/finsky/billing/iab/google/util/IabHelper;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Subscriptions NOT AVAILABLE. Response: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 3
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
L12:
goto L9
.limit locals 4
.limit stack 5
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper$1/this$0 Lcom/google/android/finsky/billing/iab/google/util/IabHelper;
ldc "Billing service disconnected."
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper$1/this$0 Lcom/google/android/finsky/billing/iab/google/util/IabHelper;
aconst_null
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mService Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;
return
.limit locals 2
.limit stack 2
.end method
