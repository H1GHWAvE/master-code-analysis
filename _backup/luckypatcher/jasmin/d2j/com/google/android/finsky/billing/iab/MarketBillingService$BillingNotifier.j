.bytecode 50.0
.class public synchronized com/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier
.super java/lang/Object
.inner class public BillingNotifier inner com/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier outer com/google/android/finsky/billing/iab/MarketBillingService

.field private 'mService' Lcom/google/android/finsky/billing/iab/MarketBillingService;

.field final synthetic 'this$0' Lcom/google/android/finsky/billing/iab/MarketBillingService;

.method public <init>(Lcom/google/android/finsky/billing/iab/MarketBillingService;Lcom/google/android/finsky/billing/iab/MarketBillingService;)V
aload 0
aload 1
putfield com/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier/this$0 Lcom/google/android/finsky/billing/iab/MarketBillingService;
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 2
putfield com/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier/mService Lcom/google/android/finsky/billing/iab/MarketBillingService;
return
.limit locals 3
.limit stack 2
.end method

.method protected sendPurchaseStateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
aload 0
getfield com/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier/mService Lcom/google/android/finsky/billing/iab/MarketBillingService;
aload 1
new android/content/Intent
dup
ldc "com.android.vending.billing.PURCHASE_STATE_CHANGED"
invokespecial android/content/Intent/<init>(Ljava/lang/String;)V
invokestatic com/google/android/finsky/billing/iab/MarketBillingService/findReceiverName(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;)Landroid/content/Intent;
astore 1
aload 1
ifnonnull L0
iconst_0
ireturn
L0:
aload 1
ldc "inapp_signed_data"
aload 2
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 1
ldc "inapp_signature"
aload 3
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 0
getfield com/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier/mService Lcom/google/android/finsky/billing/iab/MarketBillingService;
aload 1
invokevirtual com/google/android/finsky/billing/iab/MarketBillingService/sendBroadcast(Landroid/content/Intent;)V
iconst_1
ireturn
.limit locals 4
.limit stack 5
.end method

.method protected sendResponseCode(Ljava/lang/String;JI)Z
aload 0
getfield com/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier/mService Lcom/google/android/finsky/billing/iab/MarketBillingService;
aload 1
lload 2
iload 4
invokestatic com/google/android/finsky/billing/iab/MarketBillingService/sendResponseCode(Landroid/content/Context;Ljava/lang/String;JI)Z
ireturn
.limit locals 5
.limit stack 5
.end method
