.bytecode 50.0
.class public synchronized com/google/android/finsky/billing/iab/google/util/IabException
.super java/lang/Exception

.field 'mResult' Lcom/google/android/finsky/billing/iab/google/util/IabResult;

.method public <init>(ILjava/lang/String;)V
aload 0
new com/google/android/finsky/billing/iab/google/util/IabResult
dup
iload 1
aload 2
invokespecial com/google/android/finsky/billing/iab/google/util/IabResult/<init>(ILjava/lang/String;)V
invokespecial com/google/android/finsky/billing/iab/google/util/IabException/<init>(Lcom/google/android/finsky/billing/iab/google/util/IabResult;)V
return
.limit locals 3
.limit stack 5
.end method

.method public <init>(ILjava/lang/String;Ljava/lang/Exception;)V
aload 0
new com/google/android/finsky/billing/iab/google/util/IabResult
dup
iload 1
aload 2
invokespecial com/google/android/finsky/billing/iab/google/util/IabResult/<init>(ILjava/lang/String;)V
aload 3
invokespecial com/google/android/finsky/billing/iab/google/util/IabException/<init>(Lcom/google/android/finsky/billing/iab/google/util/IabResult;Ljava/lang/Exception;)V
return
.limit locals 4
.limit stack 5
.end method

.method public <init>(Lcom/google/android/finsky/billing/iab/google/util/IabResult;)V
aload 0
aload 1
aconst_null
invokespecial com/google/android/finsky/billing/iab/google/util/IabException/<init>(Lcom/google/android/finsky/billing/iab/google/util/IabResult;Ljava/lang/Exception;)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Lcom/google/android/finsky/billing/iab/google/util/IabResult;Ljava/lang/Exception;)V
aload 0
aload 1
invokevirtual com/google/android/finsky/billing/iab/google/util/IabResult/getMessage()Ljava/lang/String;
aload 2
invokespecial java/lang/Exception/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
aload 0
aload 1
putfield com/google/android/finsky/billing/iab/google/util/IabException/mResult Lcom/google/android/finsky/billing/iab/google/util/IabResult;
return
.limit locals 3
.limit stack 3
.end method

.method public getResult()Lcom/google/android/finsky/billing/iab/google/util/IabResult;
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabException/mResult Lcom/google/android/finsky/billing/iab/google/util/IabResult;
areturn
.limit locals 1
.limit stack 1
.end method
