.bytecode 50.0
.class public synchronized com/google/android/finsky/billing/iab/InAppBillingService
.super android/app/Service
.inner class inner com/google/android/finsky/billing/iab/InAppBillingService$1
.inner class inner com/google/android/finsky/billing/iab/InAppBillingService$1$1
.inner class inner com/google/android/finsky/billing/iab/InAppBillingService$2
.inner class inner com/google/android/finsky/billing/iab/InAppBillingService$3
.inner class inner com/google/android/finsky/billing/iab/InAppBillingService$4
.inner class inner com/google/android/finsky/billing/iab/InAppBillingService$5
.inner class inner com/google/android/finsky/billing/iab/InAppBillingService$6

.field public static final 'BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE' I = 3


.field public static final 'BILLING_RESPONSE_RESULT_OK' I = 0


.field public static final 'ITEM_TYPE_INAPP' Ljava/lang/String; = "inapp"

.field public static final 'ITEM_TYPE_SUBS' Ljava/lang/String; = "subs"

.field public static final 'TAG' Ljava/lang/String; = "BillingHack"

.field static 'mServiceConn' Landroid/content/ServiceConnection;

.field 'googleBillingDisabled' Z

.field 'mB' Landroid/os/IBinder;

.field private final 'mBinder' Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService$Stub;

.field 'mContext' Landroid/content/Context;

.field 'mService' Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;

.field 'mSetupDone' Z

.field 'skipSetupDone' Z

.method public <init>()V
aload 0
invokespecial android/app/Service/<init>()V
aload 0
iconst_0
putfield com/google/android/finsky/billing/iab/InAppBillingService/mSetupDone Z
aload 0
iconst_0
putfield com/google/android/finsky/billing/iab/InAppBillingService/skipSetupDone Z
aload 0
iconst_0
putfield com/google/android/finsky/billing/iab/InAppBillingService/googleBillingDisabled Z
aload 0
aconst_null
putfield com/google/android/finsky/billing/iab/InAppBillingService/mB Landroid/os/IBinder;
aload 0
new com/google/android/finsky/billing/iab/InAppBillingService$6
dup
aload 0
invokespecial com/google/android/finsky/billing/iab/InAppBillingService$6/<init>(Lcom/google/android/finsky/billing/iab/InAppBillingService;)V
putfield com/google/android/finsky/billing/iab/InAppBillingService/mBinder Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService$Stub;
return
.limit locals 1
.limit stack 4
.end method

.method public connectToBilling(Ljava/lang/String;)V
aload 0
invokevirtual com/google/android/finsky/billing/iab/InAppBillingService/startGoogleBilling()V
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService/mSetupDone Z
ifeq L0
new java/lang/IllegalStateException
dup
ldc "IAB helper is already set up."
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
new com/google/android/finsky/billing/iab/InAppBillingService$5
dup
aload 0
aload 1
invokespecial com/google/android/finsky/billing/iab/InAppBillingService$5/<init>(Lcom/google/android/finsky/billing/iab/InAppBillingService;Ljava/lang/String;)V
putstatic com/google/android/finsky/billing/iab/InAppBillingService/mServiceConn Landroid/content/ServiceConnection;
new android/content/Intent
dup
ldc "com.android.vending.billing.InAppBillingService.BIND"
invokespecial android/content/Intent/<init>(Ljava/lang/String;)V
astore 1
aload 1
ldc "com.android.vending"
invokevirtual android/content/Intent/setPackage(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 1
ldc "xexe"
ldc "lp"
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
aload 1
iconst_0
invokevirtual android/content/pm/PackageManager/queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;
invokeinterface java/util/List/isEmpty()Z 0
ifeq L1
new com/chelpus/Utils
dup
ldc "w"
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
ldc2_w 2000L
invokevirtual com/chelpus/Utils/waitLP(J)V
L1:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
aload 1
iconst_0
invokevirtual android/content/pm/PackageManager/queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;
invokeinterface java/util/List/isEmpty()Z 0
ifne L2
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
aload 1
iconst_0
invokevirtual android/content/pm/PackageManager/queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 1
L3:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/content/pm/ResolveInfo
astore 4
aload 4
getfield android/content/pm/ResolveInfo/serviceInfo Landroid/content/pm/ServiceInfo;
getfield android/content/pm/ServiceInfo/packageName Ljava/lang/String;
ifnull L3
aload 4
getfield android/content/pm/ResolveInfo/serviceInfo Landroid/content/pm/ServiceInfo;
getfield android/content/pm/ServiceInfo/packageName Ljava/lang/String;
ldc "com.android.vending"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L3
new android/content/ComponentName
dup
aload 4
getfield android/content/pm/ResolveInfo/serviceInfo Landroid/content/pm/ServiceInfo;
getfield android/content/pm/ServiceInfo/packageName Ljava/lang/String;
aload 4
getfield android/content/pm/ResolveInfo/serviceInfo Landroid/content/pm/ServiceInfo;
getfield android/content/pm/ServiceInfo/name Ljava/lang/String;
invokespecial android/content/ComponentName/<init>(Ljava/lang/String;Ljava/lang/String;)V
astore 4
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
astore 5
aload 5
aload 4
invokevirtual android/content/Intent/setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
pop
aload 5
ldc "xexe"
ldc "lp"
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
aload 5
getstatic com/google/android/finsky/billing/iab/InAppBillingService/mServiceConn Landroid/content/ServiceConnection;
iconst_1
invokevirtual android/content/Context/bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
ifeq L3
iconst_0
istore 2
L5:
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService/mSetupDone Z
ifne L3
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService/skipSetupDone Z
ifne L3
new com/chelpus/Utils
dup
ldc "w"
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
ldc2_w 500L
invokevirtual com/chelpus/Utils/waitLP(J)V
iload 2
iconst_1
iadd
istore 3
iload 3
istore 2
iload 3
bipush 30
if_icmpne L5
goto L3
L2:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Billing service unavailable on device."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L4:
return
.limit locals 6
.limit stack 4
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
aload 1
ifnull L0
aload 1
ldc "xexe"
invokevirtual android/content/Intent/getStringExtra(Ljava/lang/String;)Ljava/lang/String;
ifnull L1
aload 1
ldc "xexe"
invokevirtual android/content/Intent/getStringExtra(Ljava/lang/String;)Ljava/lang/String;
ldc "lp"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L1
aload 1
invokevirtual android/content/Intent/getPackage()Ljava/lang/String;
ifnull L2
L1:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Connect from patch."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L0:
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService/mBinder Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService$Stub;
areturn
L2:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Connect from proxy."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
iconst_0
putfield com/google/android/finsky/billing/iab/InAppBillingService/mSetupDone Z
aload 0
iconst_1
putfield com/google/android/finsky/billing/iab/InAppBillingService/skipSetupDone Z
goto L0
.limit locals 2
.limit stack 2
.end method

.method public onCreate()V
aload 0
invokespecial android/app/Service/onCreate()V
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "create bill+skip:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService/skipSetupDone Z
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
aload 0
putfield com/google/android/finsky/billing/iab/InAppBillingService/mContext Landroid/content/Context;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/su Z
ifeq L0
aload 0
invokevirtual com/google/android/finsky/billing/iab/InAppBillingService/startGoogleBilling()V
L0:
return
.limit locals 1
.limit stack 3
.end method

.method public onDestroy()V
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "destroy billing"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
invokespecial android/app/Service/onDestroy()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/su Z
ifeq L0
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService/googleBillingDisabled Z
ifeq L0
new java/lang/Thread
dup
new com/google/android/finsky/billing/iab/InAppBillingService$3
dup
aload 0
invokespecial com/google/android/finsky/billing/iab/InAppBillingService$3/<init>(Lcom/google/android/finsky/billing/iab/InAppBillingService;)V
invokespecial java/lang/Thread/<init>(Ljava/lang/Runnable;)V
invokevirtual java/lang/Thread/start()V
L0:
return
.limit locals 1
.limit stack 5
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
aload 1
ifnull L0
aload 1
ldc "xexe"
invokevirtual android/content/Intent/getStringExtra(Ljava/lang/String;)Ljava/lang/String;
ifnull L1
aload 1
ldc "xexe"
invokevirtual android/content/Intent/getStringExtra(Ljava/lang/String;)Ljava/lang/String;
ldc "lp"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L2
L1:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Connect from app."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L0:
aload 0
aload 1
iload 2
iload 3
invokespecial android/app/Service/onStartCommand(Landroid/content/Intent;II)I
ireturn
L2:
aload 0
iconst_1
putfield com/google/android/finsky/billing/iab/InAppBillingService/skipSetupDone Z
goto L0
.limit locals 4
.limit stack 4
.end method

.method public onTaskRemoved(Landroid/content/Intent;)V
aload 0
aload 1
invokespecial android/app/Service/onTaskRemoved(Landroid/content/Intent;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "on Task Removed billing"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/su Z
ifeq L0
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService/googleBillingDisabled Z
ifeq L0
new java/lang/Thread
dup
new com/google/android/finsky/billing/iab/InAppBillingService$2
dup
aload 0
invokespecial com/google/android/finsky/billing/iab/InAppBillingService$2/<init>(Lcom/google/android/finsky/billing/iab/InAppBillingService;)V
invokespecial java/lang/Thread/<init>(Ljava/lang/Runnable;)V
invokevirtual java/lang/Thread/start()V
L0:
return
.limit locals 2
.limit stack 5
.end method

.method public onUnbind(Landroid/content/Intent;)Z
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "destroy billing"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/su Z
ifeq L0
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService/googleBillingDisabled Z
ifeq L0
new java/lang/Thread
dup
new com/google/android/finsky/billing/iab/InAppBillingService$4
dup
aload 0
invokespecial com/google/android/finsky/billing/iab/InAppBillingService$4/<init>(Lcom/google/android/finsky/billing/iab/InAppBillingService;)V
invokespecial java/lang/Thread/<init>(Ljava/lang/Runnable;)V
invokevirtual java/lang/Thread/start()V
L0:
aload 0
aload 1
invokespecial android/app/Service/onUnbind(Landroid/content/Intent;)Z
ireturn
.limit locals 2
.limit stack 5
.end method

.method startGoogleBilling()V
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
.catch java/lang/Exception from L4 to L6 using L5
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/init()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/su Z
ifeq L7
aconst_null
astore 2
L0:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
ldc "com.android.vending"
iconst_4
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
astore 3
L1:
aload 3
astore 2
L8:
aload 2
ifnull L7
aload 2
getfield android/content/pm/PackageInfo/services [Landroid/content/pm/ServiceInfo;
ifnull L7
aload 2
getfield android/content/pm/PackageInfo/services [Landroid/content/pm/ServiceInfo;
arraylength
ifeq L7
iconst_0
istore 1
L9:
iload 1
aload 2
getfield android/content/pm/PackageInfo/services [Landroid/content/pm/ServiceInfo;
arraylength
if_icmpge L7
L3:
aload 2
getfield android/content/pm/PackageInfo/services [Landroid/content/pm/ServiceInfo;
iload 1
aaload
getfield android/content/pm/ServiceInfo/name Ljava/lang/String;
ldc "InAppBillingService"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifne L4
aload 2
getfield android/content/pm/PackageInfo/services [Landroid/content/pm/ServiceInfo;
iload 1
aaload
getfield android/content/pm/ServiceInfo/name Ljava/lang/String;
ldc "MarketBillingService"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L6
L4:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
ldc "com.android.vending"
aload 2
getfield android/content/pm/PackageInfo/services [Landroid/content/pm/ServiceInfo;
iload 1
aaload
getfield android/content/pm/ServiceInfo/name Ljava/lang/String;
invokespecial android/content/ComponentName/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/content/pm/PackageManager/getComponentEnabledSetting(Landroid/content/ComponentName;)I
iconst_1
if_icmpeq L6
aload 0
iconst_1
putfield com/google/android/finsky/billing/iab/InAppBillingService/googleBillingDisabled Z
iconst_1
invokestatic com/chelpus/Utils/market_billing_services(Z)V
new java/util/Timer
dup
ldc "FirstRunTimer"
invokespecial java/util/Timer/<init>(Ljava/lang/String;)V
new com/google/android/finsky/billing/iab/InAppBillingService$1
dup
aload 0
invokespecial com/google/android/finsky/billing/iab/InAppBillingService$1/<init>(Lcom/google/android/finsky/billing/iab/InAppBillingService;)V
ldc2_w 60000L
invokevirtual java/util/Timer/schedule(Ljava/util/TimerTask;J)V
L6:
iload 1
iconst_1
iadd
istore 1
goto L9
L2:
astore 3
aload 3
invokevirtual android/content/pm/PackageManager$NameNotFoundException/printStackTrace()V
goto L8
L5:
astore 3
aload 3
invokevirtual java/lang/Exception/printStackTrace()V
goto L6
L7:
return
.limit locals 4
.limit stack 6
.end method
