.bytecode 50.0
.class public synchronized com/google/android/finsky/billing/iab/google/util/SkuDetails
.super java/lang/Object

.field 'mDescription' Ljava/lang/String;

.field 'mItemType' Ljava/lang/String;

.field 'mJson' Ljava/lang/String;

.field 'mPrice' Ljava/lang/String;

.field 'mSku' Ljava/lang/String;

.field 'mTitle' Ljava/lang/String;

.field 'mType' Ljava/lang/String;

.method public <init>(Ljava/lang/String;)V
.throws org/json/JSONException
aload 0
ldc "inapp"
aload 1
invokespecial com/google/android/finsky/billing/iab/google/util/SkuDetails/<init>(Ljava/lang/String;Ljava/lang/String;)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Ljava/lang/String;Ljava/lang/String;)V
.throws org/json/JSONException
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/google/android/finsky/billing/iab/google/util/SkuDetails/mItemType Ljava/lang/String;
aload 0
aload 2
putfield com/google/android/finsky/billing/iab/google/util/SkuDetails/mJson Ljava/lang/String;
new org/json/JSONObject
dup
aload 0
getfield com/google/android/finsky/billing/iab/google/util/SkuDetails/mJson Ljava/lang/String;
invokespecial org/json/JSONObject/<init>(Ljava/lang/String;)V
astore 1
aload 0
aload 1
ldc "productId"
invokevirtual org/json/JSONObject/optString(Ljava/lang/String;)Ljava/lang/String;
putfield com/google/android/finsky/billing/iab/google/util/SkuDetails/mSku Ljava/lang/String;
aload 0
aload 1
ldc "type"
invokevirtual org/json/JSONObject/optString(Ljava/lang/String;)Ljava/lang/String;
putfield com/google/android/finsky/billing/iab/google/util/SkuDetails/mType Ljava/lang/String;
aload 0
aload 1
ldc "price"
invokevirtual org/json/JSONObject/optString(Ljava/lang/String;)Ljava/lang/String;
putfield com/google/android/finsky/billing/iab/google/util/SkuDetails/mPrice Ljava/lang/String;
aload 0
aload 1
ldc "title"
invokevirtual org/json/JSONObject/optString(Ljava/lang/String;)Ljava/lang/String;
putfield com/google/android/finsky/billing/iab/google/util/SkuDetails/mTitle Ljava/lang/String;
aload 0
aload 1
ldc "description"
invokevirtual org/json/JSONObject/optString(Ljava/lang/String;)Ljava/lang/String;
putfield com/google/android/finsky/billing/iab/google/util/SkuDetails/mDescription Ljava/lang/String;
return
.limit locals 3
.limit stack 3
.end method

.method public getDescription()Ljava/lang/String;
aload 0
getfield com/google/android/finsky/billing/iab/google/util/SkuDetails/mDescription Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getPrice()Ljava/lang/String;
aload 0
getfield com/google/android/finsky/billing/iab/google/util/SkuDetails/mPrice Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getSku()Ljava/lang/String;
aload 0
getfield com/google/android/finsky/billing/iab/google/util/SkuDetails/mSku Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getTitle()Ljava/lang/String;
aload 0
getfield com/google/android/finsky/billing/iab/google/util/SkuDetails/mTitle Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getType()Ljava/lang/String;
aload 0
getfield com/google/android/finsky/billing/iab/google/util/SkuDetails/mType Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "SkuDetails:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/google/android/finsky/billing/iab/google/util/SkuDetails/mJson Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method
