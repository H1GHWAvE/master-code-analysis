.bytecode 50.0
.class public synchronized com/google/android/finsky/billing/iab/DbHelper
.super android/database/sqlite/SQLiteOpenHelper

.field public static 'contextdb' Landroid/content/Context;

.field static final 'dbName' Ljava/lang/String; = "BillingRestoreTransactions"

.field public static 'getPackage' Z = 0


.field static final 'itemID' Ljava/lang/String; = "itemID"

.field static final 'pData' Ljava/lang/String; = "Data"

.field static final 'pSignature' Ljava/lang/String; = "Signature"

.field static 'packageTable' Ljava/lang/String;

.field public static 'savePackage' Z

.method static <clinit>()V
ldc "Packages"
putstatic com/google/android/finsky/billing/iab/DbHelper/packageTable Ljava/lang/String;
aconst_null
putstatic com/google/android/finsky/billing/iab/DbHelper/contextdb Landroid/content/Context;
iconst_0
putstatic com/google/android/finsky/billing/iab/DbHelper/getPackage Z
iconst_0
putstatic com/google/android/finsky/billing/iab/DbHelper/savePackage Z
return
.limit locals 0
.limit stack 1
.end method

.method public <init>(Landroid/content/Context;)V
.catch android/database/sqlite/SQLiteException from L0 to L1 using L2
.catch android/database/sqlite/SQLiteException from L1 to L3 using L2
.catch android/database/sqlite/SQLiteException from L3 to L4 using L2
.catch android/database/sqlite/SQLiteException from L5 to L6 using L2
aload 0
aload 1
ldc "BillingRestoreTransactions"
aconst_null
bipush 41
invokespecial android/database/sqlite/SQLiteOpenHelper/<init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V
aload 1
putstatic com/google/android/finsky/billing/iab/DbHelper/contextdb Landroid/content/Context;
getstatic com/google/android/finsky/billing/iab/DbHelper/packageTable Ljava/lang/String;
putstatic com/google/android/finsky/billing/iab/DbHelper/packageTable Ljava/lang/String;
L0:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/billing_db Landroid/database/sqlite/SQLiteDatabase;
ifnonnull L5
aload 0
invokevirtual com/google/android/finsky/billing/iab/DbHelper/getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/billing_db Landroid/database/sqlite/SQLiteDatabase;
L1:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "SQLite base version is "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/billing_db Landroid/database/sqlite/SQLiteDatabase;
invokevirtual android/database/sqlite/SQLiteDatabase/getVersion()I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/billing_db Landroid/database/sqlite/SQLiteDatabase;
invokevirtual android/database/sqlite/SQLiteDatabase/getVersion()I
bipush 41
if_icmpeq L3
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "SQL delete and recreate."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/billing_db Landroid/database/sqlite/SQLiteDatabase;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "DROP TABLE IF EXISTS "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/google/android/finsky/billing/iab/DbHelper/packageTable Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/database/sqlite/SQLiteDatabase/execSQL(Ljava/lang/String;)V
aload 0
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/billing_db Landroid/database/sqlite/SQLiteDatabase;
invokevirtual com/google/android/finsky/billing/iab/DbHelper/onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
L3:
aload 0
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/billing_db Landroid/database/sqlite/SQLiteDatabase;
invokevirtual com/google/android/finsky/billing/iab/DbHelper/onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
L4:
return
L5:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/billing_db Landroid/database/sqlite/SQLiteDatabase;
invokevirtual android/database/sqlite/SQLiteDatabase/close()V
aload 0
invokevirtual com/google/android/finsky/billing/iab/DbHelper/getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/billing_db Landroid/database/sqlite/SQLiteDatabase;
L6:
goto L1
L2:
astore 1
aload 1
invokevirtual android/database/sqlite/SQLiteException/printStackTrace()V
return
.limit locals 2
.limit stack 5
.end method

.method public <init>(Landroid/content/Context;Ljava/lang/String;)V
.catch android/database/sqlite/SQLiteException from L0 to L1 using L2
.catch android/database/sqlite/SQLiteException from L1 to L3 using L2
.catch android/database/sqlite/SQLiteException from L3 to L4 using L2
.catch android/database/sqlite/SQLiteException from L5 to L6 using L2
aload 0
aload 1
ldc "BillingRestoreTransactions"
aconst_null
bipush 41
invokespecial android/database/sqlite/SQLiteOpenHelper/<init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V
aload 1
putstatic com/google/android/finsky/billing/iab/DbHelper/contextdb Landroid/content/Context;
aload 2
putstatic com/google/android/finsky/billing/iab/DbHelper/packageTable Ljava/lang/String;
L0:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/billing_db Landroid/database/sqlite/SQLiteDatabase;
ifnonnull L5
aload 0
invokevirtual com/google/android/finsky/billing/iab/DbHelper/getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/billing_db Landroid/database/sqlite/SQLiteDatabase;
L1:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/billing_db Landroid/database/sqlite/SQLiteDatabase;
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/billing_db Landroid/database/sqlite/SQLiteDatabase;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/billing_db Landroid/database/sqlite/SQLiteDatabase;
invokevirtual android/database/sqlite/SQLiteDatabase/getVersion()I
bipush 41
if_icmpeq L3
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "SQL delete and recreate."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/billing_db Landroid/database/sqlite/SQLiteDatabase;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "DROP TABLE IF EXISTS "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/database/sqlite/SQLiteDatabase/execSQL(Ljava/lang/String;)V
aload 0
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/billing_db Landroid/database/sqlite/SQLiteDatabase;
invokevirtual com/google/android/finsky/billing/iab/DbHelper/onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
L3:
aload 0
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/billing_db Landroid/database/sqlite/SQLiteDatabase;
invokevirtual com/google/android/finsky/billing/iab/DbHelper/onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
L4:
return
L5:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/billing_db Landroid/database/sqlite/SQLiteDatabase;
invokevirtual android/database/sqlite/SQLiteDatabase/close()V
aload 0
invokevirtual com/google/android/finsky/billing/iab/DbHelper/getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/billing_db Landroid/database/sqlite/SQLiteDatabase;
L6:
goto L1
L2:
astore 1
aload 1
invokevirtual android/database/sqlite/SQLiteException/printStackTrace()V
return
.limit locals 3
.limit stack 5
.end method

.method public deleteAll()V
return
.limit locals 1
.limit stack 0
.end method

.method public deleteItem(Lcom/google/android/finsky/billing/iab/ItemsListItem;)V
.catch java/lang/Exception from L0 to L1 using L2
L0:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/billing_db Landroid/database/sqlite/SQLiteDatabase;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/google/android/finsky/billing/iab/DbHelper/packageTable Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "itemID = '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
getfield com/google/android/finsky/billing/iab/ItemsListItem/itemID Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aconst_null
invokevirtual android/database/sqlite/SQLiteDatabase/delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
pop
L1:
return
L2:
astore 1
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher-Error: deletePackage "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
return
.limit locals 2
.limit stack 4
.end method

.method public deleteItem(Ljava/lang/String;)V
.catch java/lang/Exception from L0 to L1 using L2
L0:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/billing_db Landroid/database/sqlite/SQLiteDatabase;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/google/android/finsky/billing/iab/DbHelper/packageTable Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "itemID = '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aconst_null
invokevirtual android/database/sqlite/SQLiteDatabase/delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
pop
L1:
return
L2:
astore 1
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher-Error: deletePackage "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
return
.limit locals 2
.limit stack 4
.end method

.method public getItems()Ljava/util/ArrayList;
.signature "()Ljava/util/ArrayList<Lcom/google/android/finsky/billing/iab/ItemsListItem;>;"
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L4
.catch java/lang/IllegalArgumentException from L3 to L5 using L6
.catch java/lang/Exception from L3 to L5 using L4
.catch java/lang/Exception from L5 to L7 using L8
.catch java/lang/Exception from L7 to L9 using L2
.catch java/lang/Exception from L10 to L11 using L2
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 1
aload 1
invokevirtual java/util/ArrayList/clear()V
L0:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/billing_db Landroid/database/sqlite/SQLiteDatabase;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/google/android/finsky/billing/iab/DbHelper/packageTable Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "itemID"
aastore
dup
iconst_1
ldc "Data"
aastore
dup
iconst_2
ldc "Signature"
aastore
aconst_null
aconst_null
aconst_null
aconst_null
aconst_null
invokevirtual android/database/sqlite/SQLiteDatabase/query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
astore 2
aload 2
invokeinterface android/database/Cursor/moveToFirst()Z 0
pop
L1:
aload 2
aload 2
ldc "itemID"
invokeinterface android/database/Cursor/getColumnIndexOrThrow(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
astore 3
L3:
aload 1
new com/google/android/finsky/billing/iab/ItemsListItem
dup
aload 3
aload 2
aload 2
ldc "Data"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
aload 2
aload 2
ldc "Signature"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
invokespecial com/google/android/finsky/billing/iab/ItemsListItem/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L5:
aload 2
invokeinterface android/database/Cursor/moveToNext()Z 0
ifne L1
aload 2
invokeinterface android/database/Cursor/close()V 0
L7:
iconst_0
putstatic com/google/android/finsky/billing/iab/DbHelper/getPackage Z
L9:
aload 1
areturn
L8:
astore 3
L10:
aload 2
invokeinterface android/database/Cursor/close()V 0
L11:
goto L7
L2:
astore 2
iconst_0
putstatic com/google/android/finsky/billing/iab/DbHelper/getPackage Z
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher-Error: getPackage "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 1
areturn
L4:
astore 3
goto L5
L6:
astore 3
goto L5
.limit locals 4
.limit stack 8
.end method

.method public isOpen()Z
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/billing_db Landroid/database/sqlite/SQLiteDatabase;
invokevirtual android/database/sqlite/SQLiteDatabase/isOpen()Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
aload 1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "CREATE TABLE IF NOT EXISTS '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/google/android/finsky/billing/iab/DbHelper/packageTable Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "' ("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "itemID"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " TEXT PRIMARY KEY, "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "Data"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " TEXT, "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "Signature"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " TEXT"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ");"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/database/sqlite/SQLiteDatabase/execSQL(Ljava/lang/String;)V
return
.limit locals 2
.limit stack 3
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
aload 1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "DROP TABLE IF EXISTS '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/google/android/finsky/billing/iab/DbHelper/packageTable Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/database/sqlite/SQLiteDatabase/execSQL(Ljava/lang/String;)V
aload 0
aload 1
invokevirtual com/google/android/finsky/billing/iab/DbHelper/onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
return
.limit locals 4
.limit stack 3
.end method

.method public saveItem(Lcom/google/android/finsky/billing/iab/ItemsListItem;)V
.throws android/database/sqlite/SQLiteException
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L4
.catch java/lang/Exception from L3 to L5 using L2
.catch java/lang/Exception from L6 to L7 using L2
L0:
iconst_1
putstatic com/google/android/finsky/billing/iab/DbHelper/savePackage Z
new android/content/ContentValues
dup
invokespecial android/content/ContentValues/<init>()V
astore 2
aload 2
ldc "itemID"
aload 1
getfield com/google/android/finsky/billing/iab/ItemsListItem/itemID Ljava/lang/String;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 2
ldc "Data"
aload 1
getfield com/google/android/finsky/billing/iab/ItemsListItem/pData Ljava/lang/String;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 2
ldc "Signature"
aload 1
getfield com/google/android/finsky/billing/iab/ItemsListItem/pSignature Ljava/lang/String;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
L1:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/billing_db Landroid/database/sqlite/SQLiteDatabase;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/google/android/finsky/billing/iab/DbHelper/packageTable Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
ldc "itemID"
aload 2
invokevirtual android/database/sqlite/SQLiteDatabase/insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
pop2
L3:
iconst_0
putstatic com/google/android/finsky/billing/iab/DbHelper/savePackage Z
iconst_0
putstatic com/google/android/finsky/billing/iab/DbHelper/savePackage Z
L5:
return
L4:
astore 1
L6:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/billing_db Landroid/database/sqlite/SQLiteDatabase;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/google/android/finsky/billing/iab/DbHelper/packageTable Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aconst_null
aload 2
invokevirtual android/database/sqlite/SQLiteDatabase/replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
pop2
L7:
goto L3
L2:
astore 1
iconst_0
putstatic com/google/android/finsky/billing/iab/DbHelper/savePackage Z
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher-Error: savePackage "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
return
.limit locals 3
.limit stack 4
.end method

.method public updatePackage(Ljava/util/List;)V
.signature "(Ljava/util/List<Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;>;)V"
return
.limit locals 2
.limit stack 0
.end method
