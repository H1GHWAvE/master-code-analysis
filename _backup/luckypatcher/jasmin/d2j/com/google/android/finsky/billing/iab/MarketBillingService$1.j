.bytecode 50.0
.class synchronized com/google/android/finsky/billing/iab/MarketBillingService$1
.super com/android/vending/billing/IMarketBillingService$Stub
.enclosing method com/google/android/finsky/billing/iab/MarketBillingService
.inner class inner com/google/android/finsky/billing/iab/MarketBillingService$1

.field 'request_id' J

.field final synthetic 'this$0' Lcom/google/android/finsky/billing/iab/MarketBillingService;

.method <init>(Lcom/google/android/finsky/billing/iab/MarketBillingService;)V
aload 0
aload 1
putfield com/google/android/finsky/billing/iab/MarketBillingService$1/this$0 Lcom/google/android/finsky/billing/iab/MarketBillingService;
aload 0
invokespecial com/android/vending/billing/IMarketBillingService$Stub/<init>()V
aload 0
ldc2_w -1L
putfield com/google/android/finsky/billing/iab/MarketBillingService$1/request_id J
return
.limit locals 2
.limit stack 3
.end method

.method private getNextInAppRequestId()J
aload 0
invokestatic com/google/android/finsky/billing/iab/MarketBillingService/access$200()Ljava/util/Random;
invokevirtual java/util/Random/nextLong()J
ldc2_w 9223372036854775807L
land
putfield com/google/android/finsky/billing/iab/MarketBillingService$1/request_id J
aload 0
getfield com/google/android/finsky/billing/iab/MarketBillingService$1/request_id J
lreturn
.limit locals 1
.limit stack 5
.end method

.method private updateWithRequestId(Landroid/os/Bundle;J)I
aload 1
ldc "REQUEST_ID"
lload 2
invokevirtual android/os/Bundle/putLong(Ljava/lang/String;J)V
iconst_0
ireturn
.limit locals 4
.limit stack 4
.end method

.method public confirmNotifications(Ljava/lang/String;[Ljava/lang/String;)J
aload 0
invokespecial com/google/android/finsky/billing/iab/MarketBillingService$1/getNextInAppRequestId()J
lreturn
.limit locals 3
.limit stack 2
.end method

.method public restoreTransactions(Ljava/lang/String;J)J
aload 0
invokespecial com/google/android/finsky/billing/iab/MarketBillingService$1/getNextInAppRequestId()J
lreturn
.limit locals 4
.limit stack 2
.end method

.method public sendBillingRequest(Landroid/os/Bundle;)Landroid/os/Bundle;
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
.catch org/json/JSONException from L3 to L4 using L5
.catch org/json/JSONException from L6 to L7 using L5
.catch org/json/JSONException from L8 to L9 using L5
.catch org/json/JSONException from L10 to L11 using L5
.catch org/json/JSONException from L12 to L13 using L5
.catch org/json/JSONException from L14 to L15 using L5
.catch org/json/JSONException from L16 to L17 using L5
.catch org/json/JSONException from L18 to L19 using L5
.catch org/json/JSONException from L20 to L21 using L5
.catch org/json/JSONException from L22 to L23 using L5
.catch org/json/JSONException from L24 to L25 using L26
.catch org/json/JSONException from L27 to L28 using L29
aload 1
ldc "BILLING_REQUEST"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
astore 5
aload 1
ldc "API_VERSION"
iconst_m1
invokevirtual android/os/Bundle/getInt(Ljava/lang/String;I)I
pop
aload 1
ldc "PACKAGE_NAME"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
astore 10
aload 1
ldc "DEVELOPER_PAYLOAD"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
astore 7
aload 1
ldc "ITEM_ID"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
astore 6
aload 1
ldc "NONCE"
invokevirtual android/os/Bundle/getLong(Ljava/lang/String;)J
lstore 3
aload 1
ldc "NOTIFY_IDS"
invokevirtual android/os/Bundle/getStringArray(Ljava/lang/String;)[Ljava/lang/String;
astore 1
aload 6
ifnull L30
aload 6
invokestatic com/google/android/finsky/billing/iab/MarketBillingService/access$002(Ljava/lang/String;)Ljava/lang/String;
pop
L30:
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 9
ldc ""
astore 8
ldc "CHECK_BILLING_SUPPORTED"
aload 5
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L31
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "CHECK_BILLING_SUPPORTED"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 9
ldc "RESPONSE_CODE"
iconst_0
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
aload 9
areturn
L31:
ldc "REQUEST_PURCHASE"
aload 5
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L32
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "REQUEST_PURCHASE"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aconst_null
astore 1
L0:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
aload 10
iconst_0
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
astore 5
L1:
aload 5
astore 1
L33:
aconst_null
astore 5
aload 1
ifnonnull L34
aload 5
astore 1
L35:
aload 9
ldc "PURCHASE_INTENT"
aload 0
getfield com/google/android/finsky/billing/iab/MarketBillingService$1/this$0 Lcom/google/android/finsky/billing/iab/MarketBillingService;
iconst_0
aload 1
ldc_w 1073741824
invokestatic android/app/PendingIntent/getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
invokevirtual android/os/Bundle/putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
aload 9
ldc "RESPONSE_CODE"
iconst_0
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
aload 9
areturn
L2:
astore 5
aload 5
invokevirtual android/content/pm/PackageManager$NameNotFoundException/printStackTrace()V
goto L33
L34:
aload 0
invokespecial com/google/android/finsky/billing/iab/MarketBillingService$1/getNextInAppRequestId()J
lstore 3
aload 9
ldc "REQUEST_ID"
lload 3
invokevirtual android/os/Bundle/putLong(Ljava/lang/String;J)V
new android/content/Intent
dup
ldc "android.intent.action.VIEW"
invokespecial android/content/Intent/<init>(Ljava/lang/String;)V
astore 5
aload 5
aload 0
getfield com/google/android/finsky/billing/iab/MarketBillingService$1/this$0 Lcom/google/android/finsky/billing/iab/MarketBillingService;
ldc com/google/android/finsky/billing/iab/BuyMarketActivity
invokevirtual android/content/Intent/setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;
pop
aload 5
ldc "assetid"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "inapp:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 10
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 5
ldc "asset_package"
aload 10
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 6
invokestatic com/google/android/finsky/billing/iab/MarketBillingService/access$002(Ljava/lang/String;)Ljava/lang/String;
pop
aload 7
ifnull L36
aload 7
invokestatic com/google/android/finsky/billing/iab/MarketBillingService/access$102(Ljava/lang/String;)Ljava/lang/String;
pop
L37:
aload 5
ldc "asset_version_code"
aload 1
getfield android/content/pm/PackageInfo/versionCode I
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;I)Landroid/content/Intent;
pop
aload 5
ldc "request_id"
lload 3
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;J)Landroid/content/Intent;
pop
aload 5
ldc "packageName"
aload 10
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 5
ldc "product"
aload 6
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 5
ldc "payload"
aload 7
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 7
ifnull L38
aload 5
ldc "developer_payload"
aload 7
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
L38:
new com/google/android/finsky/billing/iab/DbHelper
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
aload 10
invokespecial com/google/android/finsky/billing/iab/DbHelper/<init>(Landroid/content/Context;Ljava/lang/String;)V
invokevirtual com/google/android/finsky/billing/iab/DbHelper/getItems()Ljava/util/ArrayList;
astore 7
aload 5
astore 1
aload 7
invokevirtual java/util/ArrayList/size()I
ifeq L35
aload 7
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 7
L39:
aload 5
astore 1
aload 7
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L35
aload 7
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/google/android/finsky/billing/iab/ItemsListItem
astore 1
aload 1
getfield com/google/android/finsky/billing/iab/ItemsListItem/pData Ljava/lang/String;
ldc "auto.repeat.LP"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L39
aload 1
getfield com/google/android/finsky/billing/iab/ItemsListItem/itemID Ljava/lang/String;
aload 6
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L39
aload 5
ldc "autorepeat"
aload 1
getfield com/google/android/finsky/billing/iab/ItemsListItem/pSignature Ljava/lang/String;
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
goto L39
L36:
ldc ""
invokestatic com/google/android/finsky/billing/iab/MarketBillingService/access$102(Ljava/lang/String;)Ljava/lang/String;
pop
goto L37
L32:
ldc "RESTORE_TRANSACTIONS"
aload 5
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L40
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "RESTORE_TRANSACTIONS"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new com/google/android/finsky/billing/iab/DbHelper
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
aload 10
invokespecial com/google/android/finsky/billing/iab/DbHelper/<init>(Landroid/content/Context;Ljava/lang/String;)V
invokevirtual com/google/android/finsky/billing/iab/DbHelper/getItems()Ljava/util/ArrayList;
astore 11
aload 11
invokevirtual java/util/ArrayList/size()I
ifeq L41
new org/json/JSONObject
dup
invokespecial org/json/JSONObject/<init>()V
astore 12
new org/json/JSONArray
dup
invokespecial org/json/JSONArray/<init>()V
astore 13
ldc ""
astore 1
iconst_0
istore 2
L42:
aload 8
astore 7
aload 1
astore 6
L3:
iload 2
aload 11
invokevirtual java/util/ArrayList/size()I
if_icmpge L43
L4:
aload 8
astore 7
aload 1
astore 6
aload 1
astore 5
L6:
aload 11
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/google/android/finsky/billing/iab/ItemsListItem
getfield com/google/android/finsky/billing/iab/ItemsListItem/pData Ljava/lang/String;
ldc "auto.repeat.LP"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L44
L7:
aload 8
astore 7
aload 1
astore 6
L8:
aload 13
new org/json/JSONObject
dup
aload 11
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/google/android/finsky/billing/iab/ItemsListItem
getfield com/google/android/finsky/billing/iab/ItemsListItem/pData Ljava/lang/String;
invokespecial org/json/JSONObject/<init>(Ljava/lang/String;)V
invokevirtual org/json/JSONArray/put(Ljava/lang/Object;)Lorg/json/JSONArray;
pop
L9:
aload 8
astore 7
aload 1
astore 6
L10:
aload 11
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/google/android/finsky/billing/iab/ItemsListItem
getfield com/google/android/finsky/billing/iab/ItemsListItem/pSignature Ljava/lang/String;
ldc "1"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L45
L11:
ldc "1"
astore 5
goto L44
L43:
aload 8
astore 7
aload 1
astore 6
L12:
aload 12
ldc "nonce"
lload 3
invokevirtual org/json/JSONObject/put(Ljava/lang/String;J)Lorg/json/JSONObject;
pop
L13:
aload 8
astore 7
aload 1
astore 6
L14:
aload 12
ldc "orders"
aload 13
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
L15:
aload 8
astore 7
aload 1
astore 6
L16:
aload 12
invokevirtual org/json/JSONObject/toString()Ljava/lang/String;
astore 8
L17:
aload 1
astore 5
aload 8
astore 7
aload 1
astore 6
L18:
aload 1
ldc "1"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L21
L19:
aload 8
astore 7
aload 1
astore 6
L20:
aload 8
invokestatic com/chelpus/Utils/gen_sha1withrsa(Ljava/lang/String;)Ljava/lang/String;
astore 5
L21:
aload 8
astore 7
aload 5
astore 6
L22:
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 8
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L23:
aload 8
astore 7
L46:
aload 0
getfield com/google/android/finsky/billing/iab/MarketBillingService$1/this$0 Lcom/google/android/finsky/billing/iab/MarketBillingService;
getfield com/google/android/finsky/billing/iab/MarketBillingService/mNotifier Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;
aload 10
aload 7
aload 5
invokevirtual com/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier/sendPurchaseStateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
pop
L41:
aload 10
ldc "com.dynamixsoftware.printhand"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L47
new org/json/JSONObject
dup
invokespecial org/json/JSONObject/<init>()V
astore 1
new org/json/JSONArray
dup
invokespecial org/json/JSONArray/<init>()V
astore 5
new org/json/JSONObject
dup
invokespecial org/json/JSONObject/<init>()V
astore 6
L24:
aload 1
ldc "nonce"
lload 3
invokevirtual org/json/JSONObject/put(Ljava/lang/String;J)Lorg/json/JSONObject;
pop
aload 6
ldc "notificationId"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc2_w 1000000000000000000L
ldc2_w 9223372036854775807L
invokestatic com/chelpus/Utils/getRandom(JJ)J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 6
ldc "orderId"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc2_w 1000000000000000000L
ldc2_w 9223372036854775807L
invokestatic com/chelpus/Utils/getRandom(JJ)J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
lconst_0
ldc2_w 9L
invokestatic com/chelpus/Utils/getRandom(JJ)J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc "."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc2_w 1000000000000000L
ldc2_w 9999999999999999L
invokestatic com/chelpus/Utils/getRandom(JJ)J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 6
ldc "packageName"
aload 10
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 6
ldc "productId"
ldc "printhand.premium"
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 6
ldc "purchaseTime"
new java/lang/Long
dup
invokestatic java/lang/System/currentTimeMillis()J
invokespecial java/lang/Long/<init>(J)V
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 6
ldc "purchaseState"
new java/lang/Integer
dup
iconst_0
invokespecial java/lang/Integer/<init>(I)V
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 6
ldc "developerPayload"
invokestatic com/google/android/finsky/billing/iab/MarketBillingService/access$100()Ljava/lang/String;
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 6
ldc "purchaseToken"
bipush 24
invokestatic com/chelpus/Utils/getRandomStringLowerCase(I)Ljava/lang/String;
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 5
aload 6
invokevirtual org/json/JSONArray/put(Ljava/lang/Object;)Lorg/json/JSONArray;
pop
aload 1
ldc "orders"
aload 5
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
L25:
aload 1
invokevirtual org/json/JSONObject/toString()Ljava/lang/String;
astore 1
aload 1
invokestatic com/chelpus/Utils/gen_sha1withrsa(Ljava/lang/String;)Ljava/lang/String;
astore 5
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 1
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
getfield com/google/android/finsky/billing/iab/MarketBillingService$1/this$0 Lcom/google/android/finsky/billing/iab/MarketBillingService;
getfield com/google/android/finsky/billing/iab/MarketBillingService/mNotifier Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;
aload 10
aload 1
aload 5
invokevirtual com/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier/sendPurchaseStateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
pop
L47:
aload 0
getfield com/google/android/finsky/billing/iab/MarketBillingService$1/this$0 Lcom/google/android/finsky/billing/iab/MarketBillingService;
getfield com/google/android/finsky/billing/iab/MarketBillingService/mNotifier Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;
aload 10
aload 0
getfield com/google/android/finsky/billing/iab/MarketBillingService$1/request_id J
iconst_0
invokevirtual com/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier/sendResponseCode(Ljava/lang/String;JI)Z
pop
aload 9
ldc "RESPONSE_CODE"
aload 0
aload 9
aload 0
aload 10
lload 3
invokevirtual com/google/android/finsky/billing/iab/MarketBillingService$1/restoreTransactions(Ljava/lang/String;J)J
invokespecial com/google/android/finsky/billing/iab/MarketBillingService$1/updateWithRequestId(Landroid/os/Bundle;J)I
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
aload 9
areturn
L5:
astore 1
aload 1
invokevirtual org/json/JSONException/printStackTrace()V
aload 6
astore 5
goto L46
L26:
astore 5
aload 5
invokevirtual org/json/JSONException/printStackTrace()V
goto L25
L40:
ldc "GET_PURCHASE_INFORMATION"
aload 5
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L48
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "GET_PURCHASE_INFORMATION"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
lconst_0
ldc2_w 2147483647L
invokestatic com/chelpus/Utils/getRandom(JJ)J
l2i
istore 2
new org/json/JSONObject
dup
invokespecial org/json/JSONObject/<init>()V
astore 5
new org/json/JSONArray
dup
invokespecial org/json/JSONArray/<init>()V
astore 7
new org/json/JSONObject
dup
invokespecial org/json/JSONObject/<init>()V
astore 6
L27:
aload 5
ldc "nonce"
lload 3
invokevirtual org/json/JSONObject/put(Ljava/lang/String;J)Lorg/json/JSONObject;
pop
aload 6
ldc "notificationId"
aload 1
iconst_0
aaload
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 6
ldc "orderId"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc2_w 1000000000000000000L
ldc2_w 9223372036854775807L
invokestatic com/chelpus/Utils/getRandom(JJ)J
lconst_0
ldc2_w 9L
invokestatic com/chelpus/Utils/getRandom(JJ)J
ladd
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc "."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc2_w 1000000000000000L
ldc2_w 9999999999999999L
invokestatic com/chelpus/Utils/getRandom(JJ)J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 6
ldc "packageName"
aload 10
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 6
ldc "productId"
invokestatic com/google/android/finsky/billing/iab/MarketBillingService/access$000()Ljava/lang/String;
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 6
ldc "purchaseTime"
new java/lang/Long
dup
invokestatic java/lang/System/currentTimeMillis()J
invokespecial java/lang/Long/<init>(J)V
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 6
ldc "purchaseState"
new java/lang/Integer
dup
iconst_0
invokespecial java/lang/Integer/<init>(I)V
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 6
ldc "developerPayload"
invokestatic com/google/android/finsky/billing/iab/MarketBillingService/access$100()Ljava/lang/String;
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 6
ldc "purchaseToken"
bipush 24
invokestatic com/chelpus/Utils/getRandomStringLowerCase(I)Ljava/lang/String;
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 7
aload 6
invokevirtual org/json/JSONArray/put(Ljava/lang/Object;)Lorg/json/JSONArray;
pop
aload 5
ldc "orders"
aload 7
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
L28:
aload 5
invokevirtual org/json/JSONObject/toString()Ljava/lang/String;
astore 7
ldc ""
astore 1
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "config"
iconst_4
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
ldc "UnSign"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifne L49
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "send sign"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 7
invokestatic com/chelpus/Utils/gen_sha1withrsa(Ljava/lang/String;)Ljava/lang/String;
astore 1
L49:
ldc ""
astore 5
aload 1
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L50
ldc "1"
astore 5
L50:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "config"
iconst_4
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
ldc "SavePurchase"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifeq L51
new com/google/android/finsky/billing/iab/DbHelper
dup
aload 0
getfield com/google/android/finsky/billing/iab/MarketBillingService$1/this$0 Lcom/google/android/finsky/billing/iab/MarketBillingService;
aload 10
invokespecial com/google/android/finsky/billing/iab/DbHelper/<init>(Landroid/content/Context;Ljava/lang/String;)V
new com/google/android/finsky/billing/iab/ItemsListItem
dup
invokestatic com/google/android/finsky/billing/iab/MarketBillingService/access$000()Ljava/lang/String;
aload 6
invokevirtual org/json/JSONObject/toString()Ljava/lang/String;
aload 5
invokespecial com/google/android/finsky/billing/iab/ItemsListItem/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
invokevirtual com/google/android/finsky/billing/iab/DbHelper/saveItem(Lcom/google/android/finsky/billing/iab/ItemsListItem;)V
L51:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "config"
iconst_4
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
ldc "AutoRepeat"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifeq L52
new com/google/android/finsky/billing/iab/DbHelper
dup
aload 0
getfield com/google/android/finsky/billing/iab/MarketBillingService$1/this$0 Lcom/google/android/finsky/billing/iab/MarketBillingService;
aload 10
invokespecial com/google/android/finsky/billing/iab/DbHelper/<init>(Landroid/content/Context;Ljava/lang/String;)V
new com/google/android/finsky/billing/iab/ItemsListItem
dup
invokestatic com/google/android/finsky/billing/iab/MarketBillingService/access$000()Ljava/lang/String;
ldc "auto.repeat.LP"
aload 5
invokespecial com/google/android/finsky/billing/iab/ItemsListItem/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
invokevirtual com/google/android/finsky/billing/iab/DbHelper/saveItem(Lcom/google/android/finsky/billing/iab/ItemsListItem;)V
L52:
aload 0
getfield com/google/android/finsky/billing/iab/MarketBillingService$1/this$0 Lcom/google/android/finsky/billing/iab/MarketBillingService;
getfield com/google/android/finsky/billing/iab/MarketBillingService/mNotifier Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;
aload 10
aload 7
aload 1
invokevirtual com/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier/sendPurchaseStateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
pop
aload 0
getfield com/google/android/finsky/billing/iab/MarketBillingService$1/this$0 Lcom/google/android/finsky/billing/iab/MarketBillingService;
getfield com/google/android/finsky/billing/iab/MarketBillingService/mNotifier Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;
aload 10
aload 0
getfield com/google/android/finsky/billing/iab/MarketBillingService$1/request_id J
iconst_0
invokevirtual com/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier/sendResponseCode(Ljava/lang/String;JI)Z
pop
aload 0
aload 9
aload 0
aload 10
lload 3
invokevirtual com/google/android/finsky/billing/iab/MarketBillingService$1/restoreTransactions(Ljava/lang/String;J)J
invokespecial com/google/android/finsky/billing/iab/MarketBillingService$1/updateWithRequestId(Landroid/os/Bundle;J)I
pop
aload 9
ldc "RESPONSE_CODE"
iconst_0
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
aload 9
areturn
L29:
astore 1
aload 1
invokevirtual org/json/JSONException/printStackTrace()V
goto L28
L48:
ldc "CONFIRM_NOTIFICATIONS"
aload 5
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L53
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "CONFIRM_NOTIFICATIONS"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 9
ldc "RESPONSE_CODE"
aload 0
aload 9
aload 0
aload 10
aload 1
invokevirtual com/google/android/finsky/billing/iab/MarketBillingService$1/confirmNotifications(Ljava/lang/String;[Ljava/lang/String;)J
invokespecial com/google/android/finsky/billing/iab/MarketBillingService$1/updateWithRequestId(Landroid/os/Bundle;J)I
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
aload 0
getfield com/google/android/finsky/billing/iab/MarketBillingService$1/this$0 Lcom/google/android/finsky/billing/iab/MarketBillingService;
getfield com/google/android/finsky/billing/iab/MarketBillingService/mNotifier Lcom/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier;
aload 10
aload 0
getfield com/google/android/finsky/billing/iab/MarketBillingService$1/request_id J
iconst_0
invokevirtual com/google/android/finsky/billing/iab/MarketBillingService$BillingNotifier/sendResponseCode(Ljava/lang/String;JI)Z
pop
aload 9
areturn
L53:
aload 9
ldc "RESPONSE_CODE"
iconst_0
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
aload 9
areturn
L44:
iload 2
iconst_1
iadd
istore 2
aload 5
astore 1
goto L42
L45:
ldc ""
astore 5
goto L44
.limit locals 14
.limit stack 9
.end method
