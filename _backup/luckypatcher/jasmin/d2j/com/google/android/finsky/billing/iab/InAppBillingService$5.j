.bytecode 50.0
.class synchronized com/google/android/finsky/billing/iab/InAppBillingService$5
.super java/lang/Object
.implements android/content/ServiceConnection
.enclosing method com/google/android/finsky/billing/iab/InAppBillingService/connectToBilling(Ljava/lang/String;)V
.inner class inner com/google/android/finsky/billing/iab/InAppBillingService$5

.field final synthetic 'this$0' Lcom/google/android/finsky/billing/iab/InAppBillingService;

.field final synthetic 'val$pkgName' Ljava/lang/String;

.method <init>(Lcom/google/android/finsky/billing/iab/InAppBillingService;Ljava/lang/String;)V
aload 0
aload 1
putfield com/google/android/finsky/billing/iab/InAppBillingService$5/this$0 Lcom/google/android/finsky/billing/iab/InAppBillingService;
aload 0
aload 2
putfield com/google/android/finsky/billing/iab/InAppBillingService$5/val$pkgName Ljava/lang/String;
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 3
.limit stack 2
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
.catch android/os/RemoteException from L0 to L1 using L2
.catch android/os/RemoteException from L3 to L4 using L2
.catch android/os/RemoteException from L5 to L6 using L2
.catch android/os/RemoteException from L6 to L7 using L2
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Billing service try to connect."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$5/this$0 Lcom/google/android/finsky/billing/iab/InAppBillingService;
aload 2
invokestatic com/google/android/finsky/billing/iab/google/util/IInAppBillingService$Stub/asInterface(Landroid/os/IBinder;)Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;
putfield com/google/android/finsky/billing/iab/InAppBillingService/mService Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;
L0:
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$5/this$0 Lcom/google/android/finsky/billing/iab/InAppBillingService;
getfield com/google/android/finsky/billing/iab/InAppBillingService/mService Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;
iconst_3
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$5/val$pkgName Ljava/lang/String;
ldc "inapp"
invokeinterface com/google/android/finsky/billing/iab/google/util/IInAppBillingService/isBillingSupported(ILjava/lang/String;Ljava/lang/String;)I 3
istore 3
L1:
iload 3
ifeq L5
L3:
getstatic java/lang/System/out Ljava/io/PrintStream;
iload 3
invokevirtual java/io/PrintStream/println(I)V
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$5/this$0 Lcom/google/android/finsky/billing/iab/InAppBillingService;
iconst_1
putfield com/google/android/finsky/billing/iab/InAppBillingService/skipSetupDone Z
L4:
return
L5:
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$5/this$0 Lcom/google/android/finsky/billing/iab/InAppBillingService;
getfield com/google/android/finsky/billing/iab/InAppBillingService/mService Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;
iconst_3
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$5/val$pkgName Ljava/lang/String;
ldc "subs"
invokeinterface com/google/android/finsky/billing/iab/google/util/IInAppBillingService/isBillingSupported(ILjava/lang/String;Ljava/lang/String;)I 3
ifne L6
L6:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Billing service connected."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$5/this$0 Lcom/google/android/finsky/billing/iab/InAppBillingService;
iconst_1
putfield com/google/android/finsky/billing/iab/InAppBillingService/mSetupDone Z
L7:
return
L2:
astore 1
aload 1
invokevirtual android/os/RemoteException/printStackTrace()V
return
.limit locals 4
.limit stack 4
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Billing service disconnected."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$5/this$0 Lcom/google/android/finsky/billing/iab/InAppBillingService;
aconst_null
putfield com/google/android/finsky/billing/iab/InAppBillingService/mService Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;
aload 0
getfield com/google/android/finsky/billing/iab/InAppBillingService$5/this$0 Lcom/google/android/finsky/billing/iab/InAppBillingService;
iconst_0
putfield com/google/android/finsky/billing/iab/InAppBillingService/mSetupDone Z
return
.limit locals 2
.limit stack 2
.end method
