.bytecode 50.0
.class public synchronized com/google/android/finsky/billing/iab/google/util/Security
.super java/lang/Object

.field private static final 'KEY_FACTORY_ALGORITHM' Ljava/lang/String; = "RSA"

.field private static final 'SIGNATURE_ALGORITHM' Ljava/lang/String; = "SHA1withRSA"

.field private static final 'TAG' Ljava/lang/String; = "IABUtil/Security"

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static generatePublicKey(Ljava/lang/String;)Ljava/security/PublicKey;
.catch java/security/NoSuchAlgorithmException from L0 to L1 using L2
.catch java/security/spec/InvalidKeySpecException from L0 to L1 using L3
.catch com/google/android/finsky/billing/iab/google/util/Base64DecoderException from L0 to L1 using L4
L0:
aload 0
invokestatic com/google/android/finsky/billing/iab/google/util/Base64/decode(Ljava/lang/String;)[B
astore 0
ldc "RSA"
invokestatic java/security/KeyFactory/getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;
new java/security/spec/X509EncodedKeySpec
dup
aload 0
invokespecial java/security/spec/X509EncodedKeySpec/<init>([B)V
invokevirtual java/security/KeyFactory/generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;
astore 0
L1:
aload 0
areturn
L2:
astore 0
new java/lang/RuntimeException
dup
aload 0
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
L3:
astore 0
ldc "IABUtil/Security"
ldc "Invalid key specification."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
new java/lang/IllegalArgumentException
dup
aload 0
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/Throwable;)V
athrow
L4:
astore 0
ldc "IABUtil/Security"
ldc "Base64 decoding failed."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
new java/lang/IllegalArgumentException
dup
aload 0
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 1
.limit stack 4
.end method

.method public static verify(Ljava/security/PublicKey;Ljava/lang/String;Ljava/lang/String;)Z
.catch java/security/NoSuchAlgorithmException from L0 to L1 using L2
.catch java/security/InvalidKeyException from L0 to L1 using L3
.catch java/security/SignatureException from L0 to L1 using L4
.catch com/google/android/finsky/billing/iab/google/util/Base64DecoderException from L0 to L1 using L5
L0:
ldc "SHA1withRSA"
invokestatic java/security/Signature/getInstance(Ljava/lang/String;)Ljava/security/Signature;
astore 3
aload 3
aload 0
invokevirtual java/security/Signature/initVerify(Ljava/security/PublicKey;)V
aload 3
aload 1
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/security/Signature/update([B)V
aload 3
aload 2
invokestatic com/google/android/finsky/billing/iab/google/util/Base64/decode(Ljava/lang/String;)[B
invokevirtual java/security/Signature/verify([B)Z
ifne L6
ldc "IABUtil/Security"
ldc "Signature verification failed."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
L1:
iconst_0
ireturn
L6:
iconst_1
ireturn
L2:
astore 0
ldc "IABUtil/Security"
ldc "NoSuchAlgorithmException."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
iconst_0
ireturn
L3:
astore 0
ldc "IABUtil/Security"
ldc "Invalid key specification."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
iconst_0
ireturn
L4:
astore 0
ldc "IABUtil/Security"
ldc "Signature exception."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
iconst_0
ireturn
L5:
astore 0
ldc "IABUtil/Security"
ldc "Base64 decoding failed."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
iconst_0
ireturn
.limit locals 4
.limit stack 2
.end method

.method public static verifyPurchase(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
aload 1
ifnonnull L0
ldc "IABUtil/Security"
ldc "data is null"
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
iconst_0
ireturn
L0:
aload 2
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L1
aload 0
invokestatic com/google/android/finsky/billing/iab/google/util/Security/generatePublicKey(Ljava/lang/String;)Ljava/security/PublicKey;
aload 1
aload 2
invokestatic com/google/android/finsky/billing/iab/google/util/Security/verify(Ljava/security/PublicKey;Ljava/lang/String;Ljava/lang/String;)Z
ifne L1
ldc "IABUtil/Security"
ldc "signature does not match data."
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
iconst_0
ireturn
L1:
iconst_1
ireturn
.limit locals 3
.limit stack 3
.end method
