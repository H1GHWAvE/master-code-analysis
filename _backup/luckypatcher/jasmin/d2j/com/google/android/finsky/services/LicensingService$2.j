.bytecode 50.0
.class synchronized com/google/android/finsky/services/LicensingService$2
.super com/android/vending/licensing/ILicensingService$Stub
.enclosing method com/google/android/finsky/services/LicensingService/<init>()V
.inner class inner com/google/android/finsky/services/LicensingService$2

.field final synthetic 'this$0' Lcom/google/android/finsky/services/LicensingService;

.method <init>(Lcom/google/android/finsky/services/LicensingService;)V
aload 0
aload 1
putfield com/google/android/finsky/services/LicensingService$2/this$0 Lcom/google/android/finsky/services/LicensingService;
aload 0
invokespecial com/android/vending/licensing/ILicensingService$Stub/<init>()V
return
.limit locals 2
.limit stack 2
.end method

.method public checkLicense(JLjava/lang/String;Lcom/android/vending/licensing/ILicenseResultListener;)V
.throws android/os/RemoteException
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch android/os/RemoteException from L5 to L6 using L7
L0:
aload 0
getfield com/google/android/finsky/services/LicensingService$2/this$0 Lcom/google/android/finsky/services/LicensingService;
invokevirtual com/google/android/finsky/services/LicensingService/getPackageManager()Landroid/content/pm/PackageManager;
aload 3
iconst_0
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
astore 5
aload 0
getfield com/google/android/finsky/services/LicensingService$2/this$0 Lcom/google/android/finsky/services/LicensingService;
lload 1
aload 3
invokevirtual com/google/android/finsky/services/LicensingService/connectToLicensing(JLjava/lang/String;)V
aload 0
getfield com/google/android/finsky/services/LicensingService$2/this$0 Lcom/google/android/finsky/services/LicensingService;
getfield com/google/android/finsky/services/LicensingService/responseCode I
sipush 255
if_icmpeq L3
aload 0
getfield com/google/android/finsky/services/LicensingService$2/this$0 Lcom/google/android/finsky/services/LicensingService;
getfield com/google/android/finsky/services/LicensingService/responseCode I
ifne L3
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Transfer license from Google Play"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 4
aload 0
getfield com/google/android/finsky/services/LicensingService$2/this$0 Lcom/google/android/finsky/services/LicensingService;
getfield com/google/android/finsky/services/LicensingService/responseCode I
aload 0
getfield com/google/android/finsky/services/LicensingService$2/this$0 Lcom/google/android/finsky/services/LicensingService;
getfield com/google/android/finsky/services/LicensingService/signedData Ljava/lang/String;
aload 0
getfield com/google/android/finsky/services/LicensingService$2/this$0 Lcom/google/android/finsky/services/LicensingService;
getfield com/google/android/finsky/services/LicensingService/signature Ljava/lang/String;
invokeinterface com/android/vending/licensing/ILicenseResultListener/verifyLicense(ILjava/lang/String;Ljava/lang/String;)V 3
L1:
return
L3:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "0|"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 1
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc "|"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "|"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
getfield android/content/pm/PackageInfo/versionCode I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc "|ANlOHQOShF3uJUwv3Ql+fbsgEG9FD35Hag==|"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokestatic java/lang/System/currentTimeMillis()J
ldc2_w 31536000000L
ladd
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ":GR=10&VT="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokestatic java/lang/System/currentTimeMillis()J
ldc "31622400000"
invokestatic java/lang/Long/valueOf(Ljava/lang/String;)Ljava/lang/Long;
invokevirtual java/lang/Long/longValue()J
ladd
invokestatic java/lang/String/valueOf(J)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "&GT="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokestatic java/lang/System/currentTimeMillis()J
ldc2_w 31536000000L
ladd
ldc2_w 31536000000L
ladd
invokestatic java/lang/String/valueOf(J)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 3
new com/chelpus/Utils
dup
ldc "w"
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
ldc2_w 4000L
invokevirtual com/chelpus/Utils/waitLP(J)V
aload 4
iconst_0
aload 3
ldc "hL9GqWwZL35OoLxZQN1EYmyylu3zmf8umnXW4P0EPqGjV0QcRYjD+NtiqoDEmxnnocvrqA7Z/0v+i0O4cwgOsD7/Tg3B1QI/ukA7ZUcibvFQUNoq7KjUWSg1Qn5MauaFFhAhZbuP840wnCuntxVDUkVJ6GDymDXLqhFG1LbZmNoPl6QjkschEBLVth1YtBxE4GnbVVI8Cq5LY7/F0N8d6EGLIISD6ekoD4lkhxq3nORsibX7kjmotyhLpO7THNMXvOeXeKhVp6dNSblOHp9tcL6l/NJY7sHPw/DBSxteW2hZ9y7yyaMxMAz+nTIN/V8gJXzeaRlmIXntJpQDEMz5pQ=="
invokeinterface com/android/vending/licensing/ILicenseResultListener/verifyLicense(ILjava/lang/String;Ljava/lang/String;)V 3
L4:
return
L2:
astore 3
L5:
aload 4
sipush 258
aconst_null
aconst_null
invokeinterface com/android/vending/licensing/ILicenseResultListener/verifyLicense(ILjava/lang/String;Ljava/lang/String;)V 3
L6:
return
L7:
astore 3
return
.limit locals 6
.limit stack 5
.end method
