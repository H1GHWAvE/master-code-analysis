.bytecode 50.0
.class public synchronized com/google/android/finsky/billing/iab/google/util/IabHelper
.super java/lang/Object
.inner class inner com/google/android/finsky/billing/iab/google/util/IabHelper$1
.inner class inner com/google/android/finsky/billing/iab/google/util/IabHelper$2
.inner class inner com/google/android/finsky/billing/iab/google/util/IabHelper$2$1
.inner class inner com/google/android/finsky/billing/iab/google/util/IabHelper$3
.inner class inner com/google/android/finsky/billing/iab/google/util/IabHelper$3$1
.inner class inner com/google/android/finsky/billing/iab/google/util/IabHelper$3$2
.inner class public static abstract interface OnConsumeFinishedListener inner com/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeFinishedListener outer com/google/android/finsky/billing/iab/google/util/IabHelper
.inner class public static abstract interface OnConsumeMultiFinishedListener inner com/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeMultiFinishedListener outer com/google/android/finsky/billing/iab/google/util/IabHelper
.inner class public static abstract interface OnIabPurchaseFinishedListener inner com/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener outer com/google/android/finsky/billing/iab/google/util/IabHelper
.inner class public static abstract interface OnIabSetupFinishedListener inner com/google/android/finsky/billing/iab/google/util/IabHelper$OnIabSetupFinishedListener outer com/google/android/finsky/billing/iab/google/util/IabHelper
.inner class public static abstract interface QueryInventoryFinishedListener inner com/google/android/finsky/billing/iab/google/util/IabHelper$QueryInventoryFinishedListener outer com/google/android/finsky/billing/iab/google/util/IabHelper

.field public static final 'BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE' I = 3


.field public static final 'BILLING_RESPONSE_RESULT_DEVELOPER_ERROR' I = 5


.field public static final 'BILLING_RESPONSE_RESULT_ERROR' I = 6


.field public static final 'BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED' I = 7


.field public static final 'BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED' I = 8


.field public static final 'BILLING_RESPONSE_RESULT_ITEM_UNAVAILABLE' I = 4


.field public static final 'BILLING_RESPONSE_RESULT_OK' I = 0


.field public static final 'BILLING_RESPONSE_RESULT_USER_CANCELED' I = 1


.field public static final 'GET_SKU_DETAILS_ITEM_LIST' Ljava/lang/String; = "ITEM_ID_LIST"

.field public static final 'GET_SKU_DETAILS_ITEM_TYPE_LIST' Ljava/lang/String; = "ITEM_TYPE_LIST"

.field public static final 'IABHELPER_BAD_RESPONSE' I = -1002


.field public static final 'IABHELPER_ERROR_BASE' I = -1000


.field public static final 'IABHELPER_INVALID_CONSUMPTION' I = -1010


.field public static final 'IABHELPER_MISSING_TOKEN' I = -1007


.field public static final 'IABHELPER_REMOTE_EXCEPTION' I = -1001


.field public static final 'IABHELPER_SEND_INTENT_FAILED' I = -1004


.field public static final 'IABHELPER_SUBSCRIPTIONS_NOT_AVAILABLE' I = -1009


.field public static final 'IABHELPER_UNKNOWN_ERROR' I = -1008


.field public static final 'IABHELPER_UNKNOWN_PURCHASE_RESPONSE' I = -1006


.field public static final 'IABHELPER_USER_CANCELLED' I = -1005


.field public static final 'IABHELPER_VERIFICATION_FAILED' I = -1003


.field public static final 'INAPP_CONTINUATION_TOKEN' Ljava/lang/String; = "INAPP_CONTINUATION_TOKEN"

.field public static final 'ITEM_TYPE_INAPP' Ljava/lang/String; = "inapp"

.field public static final 'ITEM_TYPE_SUBS' Ljava/lang/String; = "subs"

.field public static final 'RESPONSE_BUY_INTENT' Ljava/lang/String; = "BUY_INTENT"

.field public static final 'RESPONSE_CODE' Ljava/lang/String; = "RESPONSE_CODE"

.field public static final 'RESPONSE_GET_SKU_DETAILS_LIST' Ljava/lang/String; = "DETAILS_LIST"

.field public static final 'RESPONSE_INAPP_ITEM_LIST' Ljava/lang/String; = "INAPP_PURCHASE_ITEM_LIST"

.field public static final 'RESPONSE_INAPP_PURCHASE_DATA' Ljava/lang/String; = "INAPP_PURCHASE_DATA"

.field public static final 'RESPONSE_INAPP_PURCHASE_DATA_LIST' Ljava/lang/String; = "INAPP_PURCHASE_DATA_LIST"

.field public static final 'RESPONSE_INAPP_SIGNATURE' Ljava/lang/String; = "INAPP_DATA_SIGNATURE"

.field public static final 'RESPONSE_INAPP_SIGNATURE_LIST' Ljava/lang/String; = "INAPP_DATA_SIGNATURE_LIST"

.field 'mAsyncInProgress' Z

.field 'mAsyncOperation' Ljava/lang/String;

.field 'mContext' Landroid/content/Context;

.field 'mDebugLog' Z

.field 'mDebugTag' Ljava/lang/String;

.field 'mDisposed' Z

.field 'mPurchaseListener' Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;

.field 'mPurchasingItemType' Ljava/lang/String;

.field 'mRequestCode' I

.field 'mService' Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;

.field 'mServiceConn' Landroid/content/ServiceConnection;

.field 'mSetupDone' Z

.field 'mSignatureBase64' Ljava/lang/String;

.field 'mSubscriptionsSupported' Z

.method public <init>(Landroid/content/Context;Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mDebugLog Z
aload 0
ldc "IabHelper"
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mDebugTag Ljava/lang/String;
aload 0
iconst_0
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mSetupDone Z
aload 0
iconst_0
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mDisposed Z
aload 0
iconst_0
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mSubscriptionsSupported Z
aload 0
iconst_0
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mAsyncInProgress Z
aload 0
ldc ""
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mAsyncOperation Ljava/lang/String;
aload 0
aconst_null
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mSignatureBase64 Ljava/lang/String;
aload 0
aload 1
invokevirtual android/content/Context/getApplicationContext()Landroid/content/Context;
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mContext Landroid/content/Context;
aload 0
aload 2
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mSignatureBase64 Ljava/lang/String;
aload 0
ldc "IAB helper created."
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
return
.limit locals 3
.limit stack 2
.end method

.method private checkNotDisposed()V
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mDisposed Z
ifeq L0
new java/lang/IllegalStateException
dup
ldc "IabHelper was disposed of, so it cannot be used."
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
return
.limit locals 1
.limit stack 3
.end method

.method public static getResponseDesc(I)Ljava/lang/String;
ldc "0:OK/1:User Canceled/2:Unknown/3:Billing Unavailable/4:Item unavailable/5:Developer Error/6:Error/7:Item Already Owned/8:Item not owned"
ldc "/"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 2
ldc "0:OK/-1001:Remote exception during initialization/-1002:Bad response received/-1003:Purchase signature verification failed/-1004:Send intent failed/-1005:User cancelled/-1006:Unknown purchase response/-1007:Missing token/-1008:Unknown error/-1009:Subscriptions not available/-1010:Invalid consumption attempt"
ldc "/"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 3
iload 0
sipush -1000
if_icmpgt L0
sipush -1000
iload 0
isub
istore 1
iload 1
iflt L1
iload 1
aload 3
arraylength
if_icmpge L1
aload 3
iload 1
aaload
areturn
L1:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
iload 0
invokestatic java/lang/String/valueOf(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":Unknown IAB Helper Error"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L0:
iload 0
iflt L2
iload 0
aload 2
arraylength
if_icmplt L3
L2:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
iload 0
invokestatic java/lang/String/valueOf(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":Unknown"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L3:
aload 2
iload 0
aaload
areturn
.limit locals 4
.limit stack 2
.end method

.method checkSetupDone(Ljava/lang/String;)V
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mSetupDone Z
ifne L0
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Illegal state for operation ("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "): IAB helper is not set up."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logError(Ljava/lang/String;)V
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "IAB helper is not set up. Can't perform operation: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
return
.limit locals 2
.limit stack 4
.end method

.method consume(Lcom/google/android/finsky/billing/iab/google/util/Purchase;)V
.throws com/google/android/finsky/billing/iab/google/util/IabException
.catch android/os/RemoteException from L0 to L1 using L2
.catch android/os/RemoteException from L3 to L4 using L2
.catch android/os/RemoteException from L4 to L2 using L2
.catch android/os/RemoteException from L5 to L6 using L2
.catch android/os/RemoteException from L7 to L8 using L2
.catch android/os/RemoteException from L9 to L10 using L2
aload 0
invokespecial com/google/android/finsky/billing/iab/google/util/IabHelper/checkNotDisposed()V
aload 0
ldc "consume"
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/checkSetupDone(Ljava/lang/String;)V
aload 1
getfield com/google/android/finsky/billing/iab/google/util/Purchase/mItemType Ljava/lang/String;
ldc "inapp"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L0
new com/google/android/finsky/billing/iab/google/util/IabException
dup
sipush -1010
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Items of type '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
getfield com/google/android/finsky/billing/iab/google/util/Purchase/mItemType Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "' can't be consumed."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial com/google/android/finsky/billing/iab/google/util/IabException/<init>(ILjava/lang/String;)V
athrow
L0:
aload 1
invokevirtual com/google/android/finsky/billing/iab/google/util/Purchase/getToken()Ljava/lang/String;
astore 3
aload 1
invokevirtual com/google/android/finsky/billing/iab/google/util/Purchase/getSku()Ljava/lang/String;
astore 4
L1:
aload 3
ifnull L4
L3:
aload 3
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L5
L4:
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Can't consume "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ". No token."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logError(Ljava/lang/String;)V
new com/google/android/finsky/billing/iab/google/util/IabException
dup
sipush -1007
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "PurchaseInfo is missing token for sku: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial com/google/android/finsky/billing/iab/google/util/IabException/<init>(ILjava/lang/String;)V
athrow
L2:
astore 3
new com/google/android/finsky/billing/iab/google/util/IabException
dup
sipush -1001
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Remote exception while consuming. PurchaseInfo: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 3
invokespecial com/google/android/finsky/billing/iab/google/util/IabException/<init>(ILjava/lang/String;Ljava/lang/Exception;)V
athrow
L5:
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Consuming sku: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", token: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mService Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;
iconst_3
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mContext Landroid/content/Context;
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
aload 3
invokeinterface com/google/android/finsky/billing/iab/google/util/IInAppBillingService/consumePurchase(ILjava/lang/String;Ljava/lang/String;)I 3
istore 2
L6:
iload 2
ifne L9
L7:
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Successfully consumed sku: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
L8:
return
L9:
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Error consuming consuming sku "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ". "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 2
invokestatic com/google/android/finsky/billing/iab/google/util/IabHelper/getResponseDesc(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
new com/google/android/finsky/billing/iab/google/util/IabException
dup
iload 2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Error consuming sku "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial com/google/android/finsky/billing/iab/google/util/IabException/<init>(ILjava/lang/String;)V
athrow
L10:
.limit locals 5
.limit stack 5
.end method

.method public consumeAsync(Lcom/google/android/finsky/billing/iab/google/util/Purchase;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeFinishedListener;)V
aload 0
invokespecial com/google/android/finsky/billing/iab/google/util/IabHelper/checkNotDisposed()V
aload 0
ldc "consume"
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/checkSetupDone(Ljava/lang/String;)V
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 3
aload 3
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 0
aload 3
aload 2
aconst_null
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/consumeAsyncInternal(Ljava/util/List;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeFinishedListener;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeMultiFinishedListener;)V
return
.limit locals 4
.limit stack 4
.end method

.method public consumeAsync(Ljava/util/List;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeMultiFinishedListener;)V
.signature "(Ljava/util/List<Lcom/google/android/finsky/billing/iab/google/util/Purchase;>;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeMultiFinishedListener;)V"
aload 0
invokespecial com/google/android/finsky/billing/iab/google/util/IabHelper/checkNotDisposed()V
aload 0
ldc "consume"
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/checkSetupDone(Ljava/lang/String;)V
aload 0
aload 1
aconst_null
aload 2
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/consumeAsyncInternal(Ljava/util/List;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeFinishedListener;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeMultiFinishedListener;)V
return
.limit locals 3
.limit stack 4
.end method

.method consumeAsyncInternal(Ljava/util/List;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeFinishedListener;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeMultiFinishedListener;)V
.signature "(Ljava/util/List<Lcom/google/android/finsky/billing/iab/google/util/Purchase;>;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeFinishedListener;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeMultiFinishedListener;)V"
new android/os/Handler
dup
invokespecial android/os/Handler/<init>()V
astore 4
aload 0
ldc "consume"
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/flagStartAsync(Ljava/lang/String;)V
new java/lang/Thread
dup
new com/google/android/finsky/billing/iab/google/util/IabHelper$3
dup
aload 0
aload 1
aload 2
aload 4
aload 3
invokespecial com/google/android/finsky/billing/iab/google/util/IabHelper$3/<init>(Lcom/google/android/finsky/billing/iab/google/util/IabHelper;Ljava/util/List;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeFinishedListener;Landroid/os/Handler;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnConsumeMultiFinishedListener;)V
invokespecial java/lang/Thread/<init>(Ljava/lang/Runnable;)V
invokevirtual java/lang/Thread/start()V
return
.limit locals 5
.limit stack 9
.end method

.method public dispose()V
aload 0
ldc "Disposing."
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
aload 0
iconst_0
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mSetupDone Z
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mServiceConn Landroid/content/ServiceConnection;
ifnull L0
aload 0
ldc "Unbinding from service."
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mContext Landroid/content/Context;
ifnull L0
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mContext Landroid/content/Context;
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mServiceConn Landroid/content/ServiceConnection;
invokevirtual android/content/Context/unbindService(Landroid/content/ServiceConnection;)V
L0:
aload 0
iconst_1
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mDisposed Z
aload 0
aconst_null
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mContext Landroid/content/Context;
aload 0
aconst_null
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mServiceConn Landroid/content/ServiceConnection;
aload 0
aconst_null
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mService Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;
aload 0
aconst_null
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mPurchaseListener Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;
return
.limit locals 1
.limit stack 2
.end method

.method public enableDebugLogging(Z)V
aload 0
invokespecial com/google/android/finsky/billing/iab/google/util/IabHelper/checkNotDisposed()V
aload 0
iload 1
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mDebugLog Z
return
.limit locals 2
.limit stack 2
.end method

.method public enableDebugLogging(ZLjava/lang/String;)V
aload 0
invokespecial com/google/android/finsky/billing/iab/google/util/IabHelper/checkNotDisposed()V
aload 0
iload 1
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mDebugLog Z
aload 0
aload 2
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mDebugTag Ljava/lang/String;
return
.limit locals 3
.limit stack 2
.end method

.method flagEndAsync()V
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Ending async operation: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mAsyncOperation Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
aload 0
ldc ""
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mAsyncOperation Ljava/lang/String;
aload 0
iconst_0
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mAsyncInProgress Z
return
.limit locals 1
.limit stack 3
.end method

.method flagStartAsync(Ljava/lang/String;)V
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mAsyncInProgress Z
ifeq L0
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Can't start async operation ("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ") because another async operation("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mAsyncOperation Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ") is in progress."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mAsyncOperation Ljava/lang/String;
aload 0
iconst_1
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mAsyncInProgress Z
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Starting async operation: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
return
.limit locals 2
.limit stack 4
.end method

.method getResponseCodeFromBundle(Landroid/os/Bundle;)I
aload 1
ldc "RESPONSE_CODE"
invokevirtual android/os/Bundle/get(Ljava/lang/String;)Ljava/lang/Object;
astore 1
aload 1
ifnonnull L0
aload 0
ldc "Bundle with null response code, assuming OK (known issue)"
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
iconst_0
ireturn
L0:
aload 1
instanceof java/lang/Integer
ifeq L1
aload 1
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
ireturn
L1:
aload 1
instanceof java/lang/Long
ifeq L2
aload 1
checkcast java/lang/Long
invokevirtual java/lang/Long/longValue()J
l2i
ireturn
L2:
aload 0
ldc "Unexpected type for bundle response code."
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logError(Ljava/lang/String;)V
aload 0
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logError(Ljava/lang/String;)V
new java/lang/RuntimeException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Unexpected type for bundle response code: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
.limit locals 2
.limit stack 4
.end method

.method getResponseCodeFromIntent(Landroid/content/Intent;)I
aload 1
invokevirtual android/content/Intent/getExtras()Landroid/os/Bundle;
ldc "RESPONSE_CODE"
invokevirtual android/os/Bundle/get(Ljava/lang/String;)Ljava/lang/Object;
astore 1
aload 1
ifnonnull L0
aload 0
ldc "Intent with no response code, assuming OK (known issue)"
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logError(Ljava/lang/String;)V
iconst_0
ireturn
L0:
aload 1
instanceof java/lang/Integer
ifeq L1
aload 1
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
ireturn
L1:
aload 1
instanceof java/lang/Long
ifeq L2
aload 1
checkcast java/lang/Long
invokevirtual java/lang/Long/longValue()J
l2i
ireturn
L2:
aload 0
ldc "Unexpected type for intent response code."
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logError(Ljava/lang/String;)V
aload 0
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logError(Ljava/lang/String;)V
new java/lang/RuntimeException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Unexpected type for intent response code: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
.limit locals 2
.limit stack 4
.end method

.method public handleActivityResult(IILandroid/content/Intent;)Z
.catch org/json/JSONException from L0 to L1 using L2
.catch org/json/JSONException from L1 to L3 using L4
.catch org/json/JSONException from L5 to L6 using L4
iload 1
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mRequestCode I
if_icmpeq L7
iconst_0
ireturn
L7:
aload 0
invokespecial com/google/android/finsky/billing/iab/google/util/IabHelper/checkNotDisposed()V
aload 0
ldc "handleActivityResult"
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/checkSetupDone(Ljava/lang/String;)V
aload 0
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/flagEndAsync()V
aload 3
ifnonnull L8
aload 0
ldc "Null data in IAB activity result."
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logError(Ljava/lang/String;)V
new com/google/android/finsky/billing/iab/google/util/IabResult
dup
sipush -1002
ldc "Null data in IAB result"
invokespecial com/google/android/finsky/billing/iab/google/util/IabResult/<init>(ILjava/lang/String;)V
astore 3
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mPurchaseListener Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;
ifnull L9
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mPurchaseListener Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;
aload 3
aconst_null
invokeinterface com/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener/onIabPurchaseFinished(Lcom/google/android/finsky/billing/iab/google/util/IabResult;Lcom/google/android/finsky/billing/iab/google/util/Purchase;)V 2
L9:
iconst_1
ireturn
L8:
aload 0
aload 3
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/getResponseCodeFromIntent(Landroid/content/Intent;)I
istore 1
aload 3
ldc "INAPP_PURCHASE_DATA"
invokevirtual android/content/Intent/getStringExtra(Ljava/lang/String;)Ljava/lang/String;
astore 4
aload 3
ldc "INAPP_DATA_SIGNATURE"
invokevirtual android/content/Intent/getStringExtra(Ljava/lang/String;)Ljava/lang/String;
astore 5
iload 2
iconst_m1
if_icmpne L10
iload 1
ifne L10
aload 0
ldc "Successful resultcode from purchase activity."
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Purchase data: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Data signature: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Extras: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual android/content/Intent/getExtras()Landroid/os/Bundle;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Expected item type: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mPurchasingItemType Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
aload 4
ifnull L11
aload 5
ifnonnull L0
L11:
aload 0
ldc "BUG: either purchaseData or dataSignature is null."
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logError(Ljava/lang/String;)V
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Extras: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual android/content/Intent/getExtras()Landroid/os/Bundle;
invokevirtual android/os/Bundle/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
new com/google/android/finsky/billing/iab/google/util/IabResult
dup
sipush -1008
ldc "IAB returned null purchaseData or dataSignature"
invokespecial com/google/android/finsky/billing/iab/google/util/IabResult/<init>(ILjava/lang/String;)V
astore 3
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mPurchaseListener Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;
ifnull L12
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mPurchaseListener Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;
aload 3
aconst_null
invokeinterface com/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener/onIabPurchaseFinished(Lcom/google/android/finsky/billing/iab/google/util/IabResult;Lcom/google/android/finsky/billing/iab/google/util/Purchase;)V 2
L12:
iconst_1
ireturn
L0:
new com/google/android/finsky/billing/iab/google/util/Purchase
dup
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mPurchasingItemType Ljava/lang/String;
aload 4
aload 5
invokespecial com/google/android/finsky/billing/iab/google/util/Purchase/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
astore 3
L1:
aload 3
invokevirtual com/google/android/finsky/billing/iab/google/util/Purchase/getSku()Ljava/lang/String;
astore 6
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mSignatureBase64 Ljava/lang/String;
aload 4
aload 5
invokestatic com/google/android/finsky/billing/iab/google/util/Security/verifyPurchase(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
ifne L5
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Purchase signature verification FAILED for sku "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logError(Ljava/lang/String;)V
new com/google/android/finsky/billing/iab/google/util/IabResult
dup
sipush -1003
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Signature verification failed for sku "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial com/google/android/finsky/billing/iab/google/util/IabResult/<init>(ILjava/lang/String;)V
astore 4
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mPurchaseListener Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;
ifnull L13
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mPurchaseListener Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;
aload 4
aload 3
invokeinterface com/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener/onIabPurchaseFinished(Lcom/google/android/finsky/billing/iab/google/util/IabResult;Lcom/google/android/finsky/billing/iab/google/util/Purchase;)V 2
L3:
goto L13
L5:
aload 0
ldc "Purchase signature successfully verified."
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
L6:
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mPurchaseListener Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;
ifnull L14
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mPurchaseListener Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;
new com/google/android/finsky/billing/iab/google/util/IabResult
dup
iconst_0
ldc "Success"
invokespecial com/google/android/finsky/billing/iab/google/util/IabResult/<init>(ILjava/lang/String;)V
aload 3
invokeinterface com/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener/onIabPurchaseFinished(Lcom/google/android/finsky/billing/iab/google/util/IabResult;Lcom/google/android/finsky/billing/iab/google/util/Purchase;)V 2
L14:
iconst_1
ireturn
L2:
astore 3
L15:
aload 0
ldc "Failed to parse purchase data."
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logError(Ljava/lang/String;)V
aload 3
invokevirtual org/json/JSONException/printStackTrace()V
new com/google/android/finsky/billing/iab/google/util/IabResult
dup
sipush -1002
ldc "Failed to parse purchase data."
invokespecial com/google/android/finsky/billing/iab/google/util/IabResult/<init>(ILjava/lang/String;)V
astore 3
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mPurchaseListener Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;
ifnull L16
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mPurchaseListener Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;
aload 3
aconst_null
invokeinterface com/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener/onIabPurchaseFinished(Lcom/google/android/finsky/billing/iab/google/util/IabResult;Lcom/google/android/finsky/billing/iab/google/util/Purchase;)V 2
L16:
iconst_1
ireturn
L10:
iload 2
iconst_m1
if_icmpne L17
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Result code was OK but in-app billing response was not OK: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokestatic com/google/android/finsky/billing/iab/google/util/IabHelper/getResponseDesc(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mPurchaseListener Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;
ifnull L14
new com/google/android/finsky/billing/iab/google/util/IabResult
dup
iload 1
ldc "Problem purchashing item."
invokespecial com/google/android/finsky/billing/iab/google/util/IabResult/<init>(ILjava/lang/String;)V
astore 3
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mPurchaseListener Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;
aload 3
aconst_null
invokeinterface com/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener/onIabPurchaseFinished(Lcom/google/android/finsky/billing/iab/google/util/IabResult;Lcom/google/android/finsky/billing/iab/google/util/Purchase;)V 2
goto L14
L17:
iload 2
ifne L18
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Purchase canceled - Response: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokestatic com/google/android/finsky/billing/iab/google/util/IabHelper/getResponseDesc(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
new com/google/android/finsky/billing/iab/google/util/IabResult
dup
sipush -1005
ldc "User canceled."
invokespecial com/google/android/finsky/billing/iab/google/util/IabResult/<init>(ILjava/lang/String;)V
astore 3
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mPurchaseListener Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;
ifnull L14
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mPurchaseListener Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;
aload 3
aconst_null
invokeinterface com/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener/onIabPurchaseFinished(Lcom/google/android/finsky/billing/iab/google/util/IabResult;Lcom/google/android/finsky/billing/iab/google/util/Purchase;)V 2
goto L14
L18:
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Purchase failed. Result code: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 2
invokestatic java/lang/Integer/toString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ". Response: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokestatic com/google/android/finsky/billing/iab/google/util/IabHelper/getResponseDesc(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logError(Ljava/lang/String;)V
new com/google/android/finsky/billing/iab/google/util/IabResult
dup
sipush -1006
ldc "Unknown purchase response."
invokespecial com/google/android/finsky/billing/iab/google/util/IabResult/<init>(ILjava/lang/String;)V
astore 3
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mPurchaseListener Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;
ifnull L14
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mPurchaseListener Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;
aload 3
aconst_null
invokeinterface com/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener/onIabPurchaseFinished(Lcom/google/android/finsky/billing/iab/google/util/IabResult;Lcom/google/android/finsky/billing/iab/google/util/Purchase;)V 2
goto L14
L4:
astore 3
goto L15
L13:
iconst_1
ireturn
.limit locals 7
.limit stack 5
.end method

.method public launchPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;ILcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;)V
aload 0
aload 1
aload 2
iload 3
aload 4
ldc ""
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/launchPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;ILcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;Ljava/lang/String;)V
return
.limit locals 5
.limit stack 6
.end method

.method public launchPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;ILcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;Ljava/lang/String;)V
aload 0
aload 1
aload 2
ldc "inapp"
iload 3
aload 4
aload 5
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/launchPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;Ljava/lang/String;)V
return
.limit locals 6
.limit stack 7
.end method

.method public launchPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;Ljava/lang/String;)V
.catch android/content/IntentSender$SendIntentException from L0 to L1 using L2
.catch android/os/RemoteException from L0 to L1 using L3
.catch android/content/IntentSender$SendIntentException from L4 to L5 using L2
.catch android/os/RemoteException from L4 to L5 using L3
.catch android/content/IntentSender$SendIntentException from L6 to L7 using L2
.catch android/os/RemoteException from L6 to L7 using L3
.catch android/content/IntentSender$SendIntentException from L8 to L9 using L2
.catch android/os/RemoteException from L8 to L9 using L3
aload 0
invokespecial com/google/android/finsky/billing/iab/google/util/IabHelper/checkNotDisposed()V
aload 0
ldc "launchPurchaseFlow"
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/checkSetupDone(Ljava/lang/String;)V
aload 0
ldc "launchPurchaseFlow"
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/flagStartAsync(Ljava/lang/String;)V
aload 3
ldc "subs"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mSubscriptionsSupported Z
ifne L0
new com/google/android/finsky/billing/iab/google/util/IabResult
dup
sipush -1009
ldc "Subscriptions are not available."
invokespecial com/google/android/finsky/billing/iab/google/util/IabResult/<init>(ILjava/lang/String;)V
astore 1
aload 0
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/flagEndAsync()V
aload 5
ifnull L10
aload 5
aload 1
aconst_null
invokeinterface com/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener/onIabPurchaseFinished(Lcom/google/android/finsky/billing/iab/google/util/IabResult;Lcom/google/android/finsky/billing/iab/google/util/Purchase;)V 2
L10:
return
L0:
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Constructing buy intent for "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", item type: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mService Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;
iconst_3
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mContext Landroid/content/Context;
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
aload 2
aload 3
aload 6
invokeinterface com/google/android/finsky/billing/iab/google/util/IInAppBillingService/getBuyIntent(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle; 5
astore 6
aload 0
aload 6
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/getResponseCodeFromBundle(Landroid/os/Bundle;)I
istore 7
L1:
iload 7
ifeq L8
L4:
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Unable to buy item, Error response: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 7
invokestatic com/google/android/finsky/billing/iab/google/util/IabHelper/getResponseDesc(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logError(Ljava/lang/String;)V
aload 0
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/flagEndAsync()V
new com/google/android/finsky/billing/iab/google/util/IabResult
dup
iload 7
ldc "Unable to buy item"
invokespecial com/google/android/finsky/billing/iab/google/util/IabResult/<init>(ILjava/lang/String;)V
astore 1
L5:
aload 5
ifnull L10
L6:
aload 5
aload 1
aconst_null
invokeinterface com/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener/onIabPurchaseFinished(Lcom/google/android/finsky/billing/iab/google/util/IabResult;Lcom/google/android/finsky/billing/iab/google/util/Purchase;)V 2
L7:
return
L2:
astore 1
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "SendIntentException while launching purchase flow for sku "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logError(Ljava/lang/String;)V
aload 1
invokevirtual android/content/IntentSender$SendIntentException/printStackTrace()V
aload 0
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/flagEndAsync()V
new com/google/android/finsky/billing/iab/google/util/IabResult
dup
sipush -1004
ldc "Failed to send intent."
invokespecial com/google/android/finsky/billing/iab/google/util/IabResult/<init>(ILjava/lang/String;)V
astore 1
aload 5
ifnull L10
aload 5
aload 1
aconst_null
invokeinterface com/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener/onIabPurchaseFinished(Lcom/google/android/finsky/billing/iab/google/util/IabResult;Lcom/google/android/finsky/billing/iab/google/util/Purchase;)V 2
return
L8:
aload 6
ldc "BUY_INTENT"
invokevirtual android/os/Bundle/getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
checkcast android/app/PendingIntent
astore 6
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Launching buy intent for "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ". Request code: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 4
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
aload 0
iload 4
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mRequestCode I
aload 0
aload 5
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mPurchaseListener Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;
aload 0
aload 3
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mPurchasingItemType Ljava/lang/String;
aload 1
aload 6
invokevirtual android/app/PendingIntent/getIntentSender()Landroid/content/IntentSender;
iload 4
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
iconst_0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/lang/Integer/intValue()I
iconst_0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/lang/Integer/intValue()I
iconst_0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/lang/Integer/intValue()I
invokevirtual android/app/Activity/startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;III)V
L9:
return
L3:
astore 1
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "RemoteException while launching purchase flow for sku "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logError(Ljava/lang/String;)V
aload 1
invokevirtual android/os/RemoteException/printStackTrace()V
aload 0
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/flagEndAsync()V
new com/google/android/finsky/billing/iab/google/util/IabResult
dup
sipush -1001
ldc "Remote exception while starting purchase flow"
invokespecial com/google/android/finsky/billing/iab/google/util/IabResult/<init>(ILjava/lang/String;)V
astore 1
aload 5
ifnull L10
aload 5
aload 1
aconst_null
invokeinterface com/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener/onIabPurchaseFinished(Lcom/google/android/finsky/billing/iab/google/util/IabResult;Lcom/google/android/finsky/billing/iab/google/util/Purchase;)V 2
return
.limit locals 8
.limit stack 7
.end method

.method public launchSubscriptionPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;ILcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;)V
aload 0
aload 1
aload 2
iload 3
aload 4
ldc ""
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/launchSubscriptionPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;ILcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;Ljava/lang/String;)V
return
.limit locals 5
.limit stack 6
.end method

.method public launchSubscriptionPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;ILcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;Ljava/lang/String;)V
aload 0
aload 1
aload 2
ldc "subs"
iload 3
aload 4
aload 5
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/launchPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabPurchaseFinishedListener;Ljava/lang/String;)V
return
.limit locals 6
.limit stack 7
.end method

.method logDebug(Ljava/lang/String;)V
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mDebugLog Z
ifeq L0
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mDebugTag Ljava/lang/String;
aload 1
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
L0:
return
.limit locals 2
.limit stack 2
.end method

.method logError(Ljava/lang/String;)V
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mDebugTag Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "In-app billing error: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
return
.limit locals 2
.limit stack 3
.end method

.method logWarn(Ljava/lang/String;)V
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mDebugTag Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "In-app billing warning: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
return
.limit locals 2
.limit stack 3
.end method

.method public queryInventory(ZLjava/util/List;)Lcom/google/android/finsky/billing/iab/google/util/Inventory;
.signature "(ZLjava/util/List<Ljava/lang/String;>;)Lcom/google/android/finsky/billing/iab/google/util/Inventory;"
.throws com/google/android/finsky/billing/iab/google/util/IabException
aload 0
iload 1
aload 2
aconst_null
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/queryInventory(ZLjava/util/List;Ljava/util/List;)Lcom/google/android/finsky/billing/iab/google/util/Inventory;
areturn
.limit locals 3
.limit stack 4
.end method

.method public queryInventory(ZLjava/util/List;Ljava/util/List;)Lcom/google/android/finsky/billing/iab/google/util/Inventory;
.signature "(ZLjava/util/List<Ljava/lang/String;>;Ljava/util/List<Ljava/lang/String;>;)Lcom/google/android/finsky/billing/iab/google/util/Inventory;"
.throws com/google/android/finsky/billing/iab/google/util/IabException
.catch android/os/RemoteException from L0 to L1 using L2
.catch org/json/JSONException from L0 to L1 using L3
.catch android/os/RemoteException from L4 to L2 using L2
.catch org/json/JSONException from L4 to L2 using L3
.catch android/os/RemoteException from L5 to L6 using L2
.catch org/json/JSONException from L5 to L6 using L3
.catch android/os/RemoteException from L7 to L3 using L2
.catch org/json/JSONException from L7 to L3 using L3
.catch android/os/RemoteException from L8 to L9 using L2
.catch org/json/JSONException from L8 to L9 using L3
.catch android/os/RemoteException from L10 to L11 using L2
.catch org/json/JSONException from L10 to L11 using L3
.catch android/os/RemoteException from L12 to L13 using L2
.catch org/json/JSONException from L12 to L13 using L3
.catch android/os/RemoteException from L14 to L15 using L2
.catch org/json/JSONException from L14 to L15 using L3
aload 0
invokespecial com/google/android/finsky/billing/iab/google/util/IabHelper/checkNotDisposed()V
aload 0
ldc "queryInventory"
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/checkSetupDone(Ljava/lang/String;)V
L0:
new com/google/android/finsky/billing/iab/google/util/Inventory
dup
invokespecial com/google/android/finsky/billing/iab/google/util/Inventory/<init>()V
astore 3
aload 0
aload 3
ldc "inapp"
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/queryPurchases(Lcom/google/android/finsky/billing/iab/google/util/Inventory;Ljava/lang/String;)I
istore 4
L1:
iload 4
ifeq L16
L4:
new com/google/android/finsky/billing/iab/google/util/IabException
dup
iload 4
ldc "Error refreshing inventory (querying owned items)."
invokespecial com/google/android/finsky/billing/iab/google/util/IabException/<init>(ILjava/lang/String;)V
athrow
L2:
astore 2
new com/google/android/finsky/billing/iab/google/util/IabException
dup
sipush -1001
ldc "Remote exception while refreshing inventory."
aload 2
invokespecial com/google/android/finsky/billing/iab/google/util/IabException/<init>(ILjava/lang/String;Ljava/lang/Exception;)V
athrow
L16:
iload 1
ifeq L8
L5:
aload 0
ldc "inapp"
aload 3
aload 2
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/querySkuDetails(Ljava/lang/String;Lcom/google/android/finsky/billing/iab/google/util/Inventory;Ljava/util/List;)I
istore 4
L6:
iload 4
ifeq L8
L7:
new com/google/android/finsky/billing/iab/google/util/IabException
dup
iload 4
ldc "Error refreshing inventory (querying prices of items)."
invokespecial com/google/android/finsky/billing/iab/google/util/IabException/<init>(ILjava/lang/String;)V
athrow
L3:
astore 2
new com/google/android/finsky/billing/iab/google/util/IabException
dup
sipush -1002
ldc "Error parsing JSON response while refreshing inventory."
aload 2
invokespecial com/google/android/finsky/billing/iab/google/util/IabException/<init>(ILjava/lang/String;Ljava/lang/Exception;)V
athrow
L8:
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mSubscriptionsSupported Z
ifeq L15
aload 0
aload 3
ldc "subs"
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/queryPurchases(Lcom/google/android/finsky/billing/iab/google/util/Inventory;Ljava/lang/String;)I
istore 4
L9:
iload 4
ifeq L11
L10:
new com/google/android/finsky/billing/iab/google/util/IabException
dup
iload 4
ldc "Error refreshing inventory (querying owned subscriptions)."
invokespecial com/google/android/finsky/billing/iab/google/util/IabException/<init>(ILjava/lang/String;)V
athrow
L11:
iload 1
ifeq L15
L12:
aload 0
ldc "subs"
aload 3
aload 2
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/querySkuDetails(Ljava/lang/String;Lcom/google/android/finsky/billing/iab/google/util/Inventory;Ljava/util/List;)I
istore 4
L13:
iload 4
ifeq L15
L14:
new com/google/android/finsky/billing/iab/google/util/IabException
dup
iload 4
ldc "Error refreshing inventory (querying prices of subscriptions)."
invokespecial com/google/android/finsky/billing/iab/google/util/IabException/<init>(ILjava/lang/String;)V
athrow
L15:
aload 3
areturn
.limit locals 5
.limit stack 5
.end method

.method public queryInventoryAsync(Lcom/google/android/finsky/billing/iab/google/util/IabHelper$QueryInventoryFinishedListener;)V
aload 0
iconst_1
aconst_null
aload 1
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/queryInventoryAsync(ZLjava/util/List;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$QueryInventoryFinishedListener;)V
return
.limit locals 2
.limit stack 4
.end method

.method public queryInventoryAsync(ZLcom/google/android/finsky/billing/iab/google/util/IabHelper$QueryInventoryFinishedListener;)V
aload 0
iload 1
aconst_null
aload 2
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/queryInventoryAsync(ZLjava/util/List;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$QueryInventoryFinishedListener;)V
return
.limit locals 3
.limit stack 4
.end method

.method public queryInventoryAsync(ZLjava/util/List;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$QueryInventoryFinishedListener;)V
.signature "(ZLjava/util/List<Ljava/lang/String;>;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$QueryInventoryFinishedListener;)V"
new android/os/Handler
dup
invokespecial android/os/Handler/<init>()V
astore 4
aload 0
invokespecial com/google/android/finsky/billing/iab/google/util/IabHelper/checkNotDisposed()V
aload 0
ldc "queryInventory"
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/checkSetupDone(Ljava/lang/String;)V
aload 0
ldc "refresh inventory"
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/flagStartAsync(Ljava/lang/String;)V
new java/lang/Thread
dup
new com/google/android/finsky/billing/iab/google/util/IabHelper$2
dup
aload 0
iload 1
aload 2
aload 3
aload 4
invokespecial com/google/android/finsky/billing/iab/google/util/IabHelper$2/<init>(Lcom/google/android/finsky/billing/iab/google/util/IabHelper;ZLjava/util/List;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$QueryInventoryFinishedListener;Landroid/os/Handler;)V
invokespecial java/lang/Thread/<init>(Ljava/lang/Runnable;)V
invokevirtual java/lang/Thread/start()V
return
.limit locals 5
.limit stack 9
.end method

.method queryPurchases(Lcom/google/android/finsky/billing/iab/google/util/Inventory;Ljava/lang/String;)I
.throws org/json/JSONException
.throws android/os/RemoteException
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Querying owned items, item type: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Package name: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mContext Landroid/content/Context;
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
iconst_0
istore 3
aconst_null
astore 6
L0:
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Calling getPurchases with continuation token: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mService Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;
iconst_3
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mContext Landroid/content/Context;
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
aload 2
aload 6
invokeinterface com/google/android/finsky/billing/iab/google/util/IInAppBillingService/getPurchases(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle; 4
astore 6
aload 0
aload 6
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/getResponseCodeFromBundle(Landroid/os/Bundle;)I
istore 4
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Owned items response: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 4
invokestatic java/lang/String/valueOf(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
iload 4
ifeq L1
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "getPurchases() failed: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 4
invokestatic com/google/android/finsky/billing/iab/google/util/IabHelper/getResponseDesc(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
iload 4
ireturn
L1:
aload 6
ldc "INAPP_PURCHASE_ITEM_LIST"
invokevirtual android/os/Bundle/containsKey(Ljava/lang/String;)Z
ifeq L2
aload 6
ldc "INAPP_PURCHASE_DATA_LIST"
invokevirtual android/os/Bundle/containsKey(Ljava/lang/String;)Z
ifeq L2
aload 6
ldc "INAPP_DATA_SIGNATURE_LIST"
invokevirtual android/os/Bundle/containsKey(Ljava/lang/String;)Z
ifne L3
L2:
aload 0
ldc "Bundle returned from getPurchases() doesn't contain required fields."
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logError(Ljava/lang/String;)V
sipush -1002
ireturn
L3:
aload 6
ldc "INAPP_PURCHASE_ITEM_LIST"
invokevirtual android/os/Bundle/getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;
astore 7
aload 6
ldc "INAPP_PURCHASE_DATA_LIST"
invokevirtual android/os/Bundle/getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;
astore 8
aload 6
ldc "INAPP_DATA_SIGNATURE_LIST"
invokevirtual android/os/Bundle/getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;
astore 9
iconst_0
istore 5
iload 3
istore 4
iload 5
istore 3
L4:
iload 3
aload 8
invokevirtual java/util/ArrayList/size()I
if_icmpge L5
aload 8
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/String
astore 10
aload 9
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/String
astore 11
aload 7
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/String
astore 12
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mSignatureBase64 Ljava/lang/String;
aload 10
aload 11
invokestatic com/google/android/finsky/billing/iab/google/util/Security/verifyPurchase(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
ifeq L6
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Sku is owned: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 12
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
new com/google/android/finsky/billing/iab/google/util/Purchase
dup
aload 2
aload 10
aload 11
invokespecial com/google/android/finsky/billing/iab/google/util/Purchase/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
astore 11
aload 11
invokevirtual com/google/android/finsky/billing/iab/google/util/Purchase/getToken()Ljava/lang/String;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L7
aload 0
ldc "BUG: empty/null token!"
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logWarn(Ljava/lang/String;)V
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Purchase data: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 10
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
L7:
aload 1
aload 11
invokevirtual com/google/android/finsky/billing/iab/google/util/Inventory/addPurchase(Lcom/google/android/finsky/billing/iab/google/util/Purchase;)V
L8:
iload 3
iconst_1
iadd
istore 3
goto L4
L6:
aload 0
ldc "Purchase signature verification **FAILED**. Not adding item."
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logWarn(Ljava/lang/String;)V
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "   Purchase data: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 10
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "   Signature: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 11
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
iconst_1
istore 4
goto L8
L5:
aload 6
ldc "INAPP_CONTINUATION_TOKEN"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
astore 7
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Continuation token: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 7
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
aload 7
astore 6
iload 4
istore 3
aload 7
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L0
iload 4
ifeq L9
sipush -1003
istore 3
L10:
iload 3
ireturn
L9:
iconst_0
istore 3
goto L10
.limit locals 13
.limit stack 5
.end method

.method querySkuDetails(Ljava/lang/String;Lcom/google/android/finsky/billing/iab/google/util/Inventory;Ljava/util/List;)I
.signature "(Ljava/lang/String;Lcom/google/android/finsky/billing/iab/google/util/Inventory;Ljava/util/List<Ljava/lang/String;>;)I"
.throws android/os/RemoteException
.throws org/json/JSONException
aload 0
ldc "Querying SKU details."
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 5
aload 5
aload 2
aload 1
invokevirtual com/google/android/finsky/billing/iab/google/util/Inventory/getAllOwnedSkus(Ljava/lang/String;)Ljava/util/List;
invokevirtual java/util/ArrayList/addAll(Ljava/util/Collection;)Z
pop
aload 3
ifnull L0
aload 3
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 3
L1:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 6
aload 5
aload 6
invokevirtual java/util/ArrayList/contains(Ljava/lang/Object;)Z
ifne L1
aload 5
aload 6
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
goto L1
L0:
aload 5
invokevirtual java/util/ArrayList/size()I
ifne L2
aload 0
ldc "queryPrices: nothing to do because there are no SKUs."
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
L3:
iconst_0
ireturn
L2:
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 3
aload 3
ldc "ITEM_ID_LIST"
aload 5
invokevirtual android/os/Bundle/putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mService Lcom/google/android/finsky/billing/iab/google/util/IInAppBillingService;
iconst_3
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mContext Landroid/content/Context;
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
aload 1
aload 3
invokeinterface com/google/android/finsky/billing/iab/google/util/IInAppBillingService/getSkuDetails(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle; 4
astore 3
aload 3
ldc "DETAILS_LIST"
invokevirtual android/os/Bundle/containsKey(Ljava/lang/String;)Z
ifne L4
aload 0
aload 3
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/getResponseCodeFromBundle(Landroid/os/Bundle;)I
istore 4
iload 4
ifeq L5
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "getSkuDetails() failed: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 4
invokestatic com/google/android/finsky/billing/iab/google/util/IabHelper/getResponseDesc(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
iload 4
ireturn
L5:
aload 0
ldc "getSkuDetails() returned a bundle with neither an error nor a detail list."
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logError(Ljava/lang/String;)V
sipush -1002
ireturn
L4:
aload 3
ldc "DETAILS_LIST"
invokevirtual android/os/Bundle/getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 3
L6:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
new com/google/android/finsky/billing/iab/google/util/SkuDetails
dup
aload 1
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
invokespecial com/google/android/finsky/billing/iab/google/util/SkuDetails/<init>(Ljava/lang/String;Ljava/lang/String;)V
astore 5
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Got sku details: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
aload 2
aload 5
invokevirtual com/google/android/finsky/billing/iab/google/util/Inventory/addSkuDetails(Lcom/google/android/finsky/billing/iab/google/util/SkuDetails;)V
goto L6
.limit locals 7
.limit stack 5
.end method

.method public startSetup(Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabSetupFinishedListener;)V
aload 0
invokespecial com/google/android/finsky/billing/iab/google/util/IabHelper/checkNotDisposed()V
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mSetupDone Z
ifeq L0
new java/lang/IllegalStateException
dup
ldc "IAB helper is already set up."
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
ldc "Starting in-app billing setup."
invokevirtual com/google/android/finsky/billing/iab/google/util/IabHelper/logDebug(Ljava/lang/String;)V
aload 0
new com/google/android/finsky/billing/iab/google/util/IabHelper$1
dup
aload 0
aload 1
invokespecial com/google/android/finsky/billing/iab/google/util/IabHelper$1/<init>(Lcom/google/android/finsky/billing/iab/google/util/IabHelper;Lcom/google/android/finsky/billing/iab/google/util/IabHelper$OnIabSetupFinishedListener;)V
putfield com/google/android/finsky/billing/iab/google/util/IabHelper/mServiceConn Landroid/content/ServiceConnection;
new android/content/Intent
dup
ldc "com.android.vending.billing.InAppBillingService.BIND"
invokespecial android/content/Intent/<init>(Ljava/lang/String;)V
astore 2
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mContext Landroid/content/Context;
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
aload 2
iconst_0
invokevirtual android/content/pm/PackageManager/queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;
invokeinterface java/util/List/isEmpty()Z 0
ifne L1
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mContext Landroid/content/Context;
aload 2
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mServiceConn Landroid/content/ServiceConnection;
iconst_1
invokevirtual android/content/Context/bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
pop
L2:
return
L1:
aload 1
ifnull L2
aload 1
new com/google/android/finsky/billing/iab/google/util/IabResult
dup
iconst_3
ldc "Billing service unavailable on device."
invokespecial com/google/android/finsky/billing/iab/google/util/IabResult/<init>(ILjava/lang/String;)V
invokeinterface com/google/android/finsky/billing/iab/google/util/IabHelper$OnIabSetupFinishedListener/onIabSetupFinished(Lcom/google/android/finsky/billing/iab/google/util/IabResult;)V 1
return
.limit locals 3
.limit stack 5
.end method

.method public subscriptionsSupported()Z
aload 0
invokespecial com/google/android/finsky/billing/iab/google/util/IabHelper/checkNotDisposed()V
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabHelper/mSubscriptionsSupported Z
ireturn
.limit locals 1
.limit stack 1
.end method
