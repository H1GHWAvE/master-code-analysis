.bytecode 50.0
.class public synchronized com/google/android/finsky/billing/iab/BuyActivity
.super android/app/Activity
.inner class inner com/google/android/finsky/billing/iab/BuyActivity$1
.inner class inner com/google/android/finsky/billing/iab/BuyActivity$2

.field public static final 'BUY_INTENT' Ljava/lang/String; = "com.google.android.finsky.billing.iab.BUY"

.field public static final 'EXTRA_DEV_PAYLOAD' Ljava/lang/String; = "payload"

.field public static final 'EXTRA_PACKAGENAME' Ljava/lang/String; = "packageName"

.field public static final 'EXTRA_PRODUCT_ID' Ljava/lang/String; = "product"

.field public static final 'TAG' Ljava/lang/String; = "BillingHack"

.field 'bundle' Landroid/os/Bundle;

.field public 'context' Lcom/google/android/finsky/billing/iab/BuyActivity;

.field 'devPayload' Ljava/lang/String;

.field 'packageName' Ljava/lang/String;

.field 'productId' Ljava/lang/String;

.field 'type' Ljava/lang/String;

.method public <init>()V
aload 0
invokespecial android/app/Activity/<init>()V
aload 0
aconst_null
putfield com/google/android/finsky/billing/iab/BuyActivity/context Lcom/google/android/finsky/billing/iab/BuyActivity;
aload 0
ldc ""
putfield com/google/android/finsky/billing/iab/BuyActivity/packageName Ljava/lang/String;
aload 0
ldc ""
putfield com/google/android/finsky/billing/iab/BuyActivity/productId Ljava/lang/String;
aload 0
ldc ""
putfield com/google/android/finsky/billing/iab/BuyActivity/devPayload Ljava/lang/String;
aload 0
ldc ""
putfield com/google/android/finsky/billing/iab/BuyActivity/type Ljava/lang/String;
aload 0
aconst_null
putfield com/google/android/finsky/billing/iab/BuyActivity/bundle Landroid/os/Bundle;
return
.limit locals 1
.limit stack 2
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
aload 1
getfield android/content/res/Configuration/orientation I
iconst_2
if_icmpne L0
aload 0
iconst_0
invokevirtual com/google/android/finsky/billing/iab/BuyActivity/setRequestedOrientation(I)V
L1:
aload 0
aload 1
invokespecial android/app/Activity/onConfigurationChanged(Landroid/content/res/Configuration;)V
return
L0:
aload 0
iconst_1
invokevirtual com/google/android/finsky/billing/iab/BuyActivity/setRequestedOrientation(I)V
goto L1
.limit locals 2
.limit stack 2
.end method

.method protected onCreate(Landroid/os/Bundle;)V
.catch org/json/JSONException from L0 to L1 using L2
aload 0
aload 1
invokespecial android/app/Activity/onCreate(Landroid/os/Bundle;)V
aload 0
aload 0
putfield com/google/android/finsky/billing/iab/BuyActivity/context Lcom/google/android/finsky/billing/iab/BuyActivity;
ldc "com.google.android.finsky.billing.iab.BUY"
aload 0
invokevirtual com/google/android/finsky/billing/iab/BuyActivity/getIntent()Landroid/content/Intent;
invokevirtual android/content/Intent/getAction()Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L3
aload 1
ifnull L4
L3:
ldc "BillingHack"
ldc "Buy intent!"
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
invokevirtual com/google/android/finsky/billing/iab/BuyActivity/getRequestedOrientation()I
invokevirtual java/io/PrintStream/println(I)V
aload 1
ifnull L5
aload 0
aload 1
ldc "packageName"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
putfield com/google/android/finsky/billing/iab/BuyActivity/packageName Ljava/lang/String;
aload 0
aload 1
ldc "product"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
putfield com/google/android/finsky/billing/iab/BuyActivity/productId Ljava/lang/String;
aload 0
aload 1
ldc "payload"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
putfield com/google/android/finsky/billing/iab/BuyActivity/devPayload Ljava/lang/String;
aload 0
aload 1
ldc "Type"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
putfield com/google/android/finsky/billing/iab/BuyActivity/type Ljava/lang/String;
L6:
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
getfield com/google/android/finsky/billing/iab/BuyActivity/type Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new org/json/JSONObject
dup
invokespecial org/json/JSONObject/<init>()V
astore 1
L0:
aload 1
ldc "orderId"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc2_w 1000000000000000000L
ldc2_w 9223372036854775807L
invokestatic com/chelpus/Utils/getRandom(JJ)J
lconst_0
ldc2_w 9L
invokestatic com/chelpus/Utils/getRandom(JJ)J
ladd
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc "."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc2_w 1000000000000000L
ldc2_w 9999999999999999L
invokestatic com/chelpus/Utils/getRandom(JJ)J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 1
ldc "packageName"
aload 0
getfield com/google/android/finsky/billing/iab/BuyActivity/packageName Ljava/lang/String;
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 1
ldc "productId"
aload 0
getfield com/google/android/finsky/billing/iab/BuyActivity/productId Ljava/lang/String;
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 1
ldc "purchaseTime"
new java/lang/Long
dup
invokestatic java/lang/System/currentTimeMillis()J
invokespecial java/lang/Long/<init>(J)V
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 1
ldc "purchaseState"
new java/lang/Integer
dup
iconst_0
invokespecial java/lang/Integer/<init>(I)V
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 1
ldc "developerPayload"
aload 0
getfield com/google/android/finsky/billing/iab/BuyActivity/devPayload Ljava/lang/String;
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
aload 1
ldc "purchaseToken"
bipush 24
invokestatic com/chelpus/Utils/getRandomStringLowerCase(I)Ljava/lang/String;
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
pop
L1:
aload 1
invokevirtual org/json/JSONObject/toString()Ljava/lang/String;
astore 2
aload 0
invokevirtual com/google/android/finsky/billing/iab/BuyActivity/getIntent()Landroid/content/Intent;
invokevirtual android/content/Intent/getExtras()Landroid/os/Bundle;
ldc "autorepeat"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
astore 1
aload 1
ifnull L7
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
astore 3
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 4
aload 1
ldc "1"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L8
aload 2
invokestatic com/chelpus/Utils/gen_sha1withrsa(Ljava/lang/String;)Ljava/lang/String;
astore 1
L9:
aload 4
ldc "RESPONSE_CODE"
iconst_0
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
aload 4
ldc "INAPP_PURCHASE_DATA"
aload 2
invokevirtual android/os/Bundle/putString(Ljava/lang/String;Ljava/lang/String;)V
aload 4
ldc "INAPP_DATA_SIGNATURE"
aload 1
invokevirtual android/os/Bundle/putString(Ljava/lang/String;Ljava/lang/String;)V
aload 3
aload 4
invokevirtual android/content/Intent/putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
pop
aload 0
iconst_m1
aload 3
invokevirtual com/google/android/finsky/billing/iab/BuyActivity/setResult(ILandroid/content/Intent;)V
aload 0
invokevirtual com/google/android/finsky/billing/iab/BuyActivity/finish()V
L7:
aload 0
ldc_w 2130968594
invokevirtual com/google/android/finsky/billing/iab/BuyActivity/setContentView(I)V
aload 0
ldc_w 2131558407
invokevirtual com/google/android/finsky/billing/iab/BuyActivity/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
astore 1
aload 0
ldc_w 2131558408
invokevirtual com/google/android/finsky/billing/iab/BuyActivity/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
astore 3
aload 0
ldc_w 2131558483
invokevirtual com/google/android/finsky/billing/iab/BuyActivity/findViewById(I)Landroid/view/View;
checkcast android/widget/CheckBox
astore 4
invokestatic com/chelpus/Utils/checkCoreJarPatch11()Z
ifne L10
aload 0
getfield com/google/android/finsky/billing/iab/BuyActivity/packageName Ljava/lang/String;
aload 0
invokestatic com/chelpus/Utils/isRebuildedOrOdex(Ljava/lang/String;Landroid/content/Context;)Z
ifne L10
aload 4
iconst_1
invokevirtual android/widget/CheckBox/setChecked(Z)V
L10:
aload 0
ldc_w 2131558484
invokevirtual com/google/android/finsky/billing/iab/BuyActivity/findViewById(I)Landroid/view/View;
checkcast android/widget/CheckBox
astore 5
aload 0
ldc_w 2131558485
invokevirtual com/google/android/finsky/billing/iab/BuyActivity/findViewById(I)Landroid/view/View;
checkcast android/widget/CheckBox
astore 6
aload 0
ldc_w 2131558471
invokevirtual com/google/android/finsky/billing/iab/BuyActivity/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
astore 7
aload 7
ldc_w 2131165222
invokestatic com/chelpus/Utils/getText(I)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 7
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc_w 2131165223
invokestatic com/chelpus/Utils/getText(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/widget/TextView/append(Ljava/lang/CharSequence;)V
aload 5
iconst_0
invokevirtual android/widget/CheckBox/setChecked(Z)V
aload 6
iconst_0
invokevirtual android/widget/CheckBox/setChecked(Z)V
aload 1
new com/google/android/finsky/billing/iab/BuyActivity$1
dup
aload 0
aload 4
aload 2
aload 5
aload 6
invokespecial com/google/android/finsky/billing/iab/BuyActivity$1/<init>(Lcom/google/android/finsky/billing/iab/BuyActivity;Landroid/widget/CheckBox;Ljava/lang/String;Landroid/widget/CheckBox;Landroid/widget/CheckBox;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 3
new com/google/android/finsky/billing/iab/BuyActivity$2
dup
aload 0
invokespecial com/google/android/finsky/billing/iab/BuyActivity$2/<init>(Lcom/google/android/finsky/billing/iab/BuyActivity;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
return
L5:
aload 0
aload 0
invokevirtual com/google/android/finsky/billing/iab/BuyActivity/getIntent()Landroid/content/Intent;
invokevirtual android/content/Intent/getExtras()Landroid/os/Bundle;
ldc "packageName"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
putfield com/google/android/finsky/billing/iab/BuyActivity/packageName Ljava/lang/String;
aload 0
aload 0
invokevirtual com/google/android/finsky/billing/iab/BuyActivity/getIntent()Landroid/content/Intent;
invokevirtual android/content/Intent/getExtras()Landroid/os/Bundle;
ldc "product"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
putfield com/google/android/finsky/billing/iab/BuyActivity/productId Ljava/lang/String;
aload 0
aload 0
invokevirtual com/google/android/finsky/billing/iab/BuyActivity/getIntent()Landroid/content/Intent;
invokevirtual android/content/Intent/getExtras()Landroid/os/Bundle;
ldc "payload"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
putfield com/google/android/finsky/billing/iab/BuyActivity/devPayload Ljava/lang/String;
aload 0
aload 0
invokevirtual com/google/android/finsky/billing/iab/BuyActivity/getIntent()Landroid/content/Intent;
invokevirtual android/content/Intent/getExtras()Landroid/os/Bundle;
ldc "Type"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
putfield com/google/android/finsky/billing/iab/BuyActivity/type Ljava/lang/String;
goto L6
L2:
astore 2
aload 2
invokevirtual org/json/JSONException/printStackTrace()V
goto L1
L8:
ldc ""
astore 1
goto L9
L4:
aload 0
invokevirtual com/google/android/finsky/billing/iab/BuyActivity/finish()V
return
.limit locals 8
.limit stack 9
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "load instance"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
aload 1
ldc "packageName"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
putfield com/google/android/finsky/billing/iab/BuyActivity/packageName Ljava/lang/String;
aload 0
aload 1
invokespecial android/app/Activity/onRestoreInstanceState(Landroid/os/Bundle;)V
return
.limit locals 2
.limit stack 3
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "save instance"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 1
ldc "packageName"
aload 0
getfield com/google/android/finsky/billing/iab/BuyActivity/packageName Ljava/lang/String;
invokevirtual android/os/Bundle/putString(Ljava/lang/String;Ljava/lang/String;)V
aload 1
ldc "product"
aload 0
getfield com/google/android/finsky/billing/iab/BuyActivity/productId Ljava/lang/String;
invokevirtual android/os/Bundle/putString(Ljava/lang/String;Ljava/lang/String;)V
aload 1
ldc "payload"
aload 0
getfield com/google/android/finsky/billing/iab/BuyActivity/devPayload Ljava/lang/String;
invokevirtual android/os/Bundle/putString(Ljava/lang/String;Ljava/lang/String;)V
aload 1
ldc "Type"
aload 0
getfield com/google/android/finsky/billing/iab/BuyActivity/type Ljava/lang/String;
invokevirtual android/os/Bundle/putString(Ljava/lang/String;Ljava/lang/String;)V
aload 0
aload 1
invokespecial android/app/Activity/onSaveInstanceState(Landroid/os/Bundle;)V
return
.limit locals 2
.limit stack 3
.end method
