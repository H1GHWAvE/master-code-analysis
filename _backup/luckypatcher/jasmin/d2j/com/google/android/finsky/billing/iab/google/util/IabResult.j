.bytecode 50.0
.class public synchronized com/google/android/finsky/billing/iab/google/util/IabResult
.super java/lang/Object

.field 'mMessage' Ljava/lang/String;

.field 'mResponse' I

.method public <init>(ILjava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iload 1
putfield com/google/android/finsky/billing/iab/google/util/IabResult/mResponse I
aload 2
ifnull L0
aload 2
invokevirtual java/lang/String/trim()Ljava/lang/String;
invokevirtual java/lang/String/length()I
ifne L1
L0:
aload 0
iload 1
invokestatic com/google/android/finsky/billing/iab/google/util/IabHelper/getResponseDesc(I)Ljava/lang/String;
putfield com/google/android/finsky/billing/iab/google/util/IabResult/mMessage Ljava/lang/String;
return
L1:
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " (response: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokestatic com/google/android/finsky/billing/iab/google/util/IabHelper/getResponseDesc(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
putfield com/google/android/finsky/billing/iab/google/util/IabResult/mMessage Ljava/lang/String;
return
.limit locals 3
.limit stack 3
.end method

.method public getMessage()Ljava/lang/String;
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabResult/mMessage Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getResponse()I
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabResult/mResponse I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isFailure()Z
aload 0
invokevirtual com/google/android/finsky/billing/iab/google/util/IabResult/isSuccess()Z
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isSuccess()Z
aload 0
getfield com/google/android/finsky/billing/iab/google/util/IabResult/mResponse I
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "IabResult: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual com/google/android/finsky/billing/iab/google/util/IabResult/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method
