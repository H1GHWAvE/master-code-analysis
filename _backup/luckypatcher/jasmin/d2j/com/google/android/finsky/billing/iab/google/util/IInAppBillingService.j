.bytecode 50.0
.class public abstract interface com/google/android/finsky/billing/iab/google/util/IInAppBillingService
.super java/lang/Object
.implements android/os/IInterface
.inner class public static abstract Stub inner com/google/android/finsky/billing/iab/google/util/IInAppBillingService$Stub outer com/google/android/finsky/billing/iab/google/util/IInAppBillingService
.inner class private static Proxy inner com/google/android/finsky/billing/iab/google/util/IInAppBillingService$Stub$Proxy outer com/google/android/finsky/billing/iab/google/util/IInAppBillingService

.method public abstract consumePurchase(ILjava/lang/String;Ljava/lang/String;)I
.throws android/os/RemoteException
.end method

.method public abstract getBuyIntent(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
.throws android/os/RemoteException
.end method

.method public abstract getBuyIntentToReplaceSkus(ILjava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
.throws android/os/RemoteException
.end method

.method public abstract getPurchases(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
.throws android/os/RemoteException
.end method

.method public abstract getSkuDetails(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
.throws android/os/RemoteException
.end method

.method public abstract isBillingSupported(ILjava/lang/String;Ljava/lang/String;)I
.throws android/os/RemoteException
.end method
