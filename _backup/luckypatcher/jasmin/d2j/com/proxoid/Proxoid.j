.bytecode 50.0
.class public synchronized com/proxoid/Proxoid
.super java/lang/Object
.implements android/content/ServiceConnection

.field protected static final 'KEY_ONOFF' Ljava/lang/String; = "onoff"

.field protected static final 'KEY_PORT' Ljava/lang/String; = "port"

.field protected static final 'KEY_PREFS' Ljava/lang/String; = "proxoidv6"

.field protected static final 'KEY_USERAGENT' Ljava/lang/String; = "useragent"

.field private static final 'TAG' Ljava/lang/String; = "proxoid"

.field protected static final 'USERAGENT_ASIS' Ljava/lang/String; = "asis"

.field protected static final 'USERAGENT_RANDOM' Ljava/lang/String; = "random"

.field protected static final 'USERAGENT_REMOVE' Ljava/lang/String; = "remove"

.field protected static final 'USERAGENT_REPLACE' Ljava/lang/String; = "replace"

.field public 'mContext' Landroid/content/Context;

.field private 'proxoidControl' Lcom/proxoid/IProxoidControl;

.method public <init>(Landroid/content/Context;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aconst_null
putfield com/proxoid/Proxoid/proxoidControl Lcom/proxoid/IProxoidControl;
aload 0
aconst_null
putfield com/proxoid/Proxoid/mContext Landroid/content/Context;
aload 0
aload 1
putfield com/proxoid/Proxoid/mContext Landroid/content/Context;
aload 1
new android/content/Intent
dup
aload 1
ldc com/proxoid/ProxoidService
invokespecial android/content/Intent/<init>(Landroid/content/Context;Ljava/lang/Class;)V
aload 0
iconst_1
invokevirtual android/content/Context/bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
pop
aload 0
invokespecial com/proxoid/Proxoid/getSharedPreferences()Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "onoff"
iconst_1
invokeinterface android/content/SharedPreferences$Editor/putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
return
.limit locals 2
.limit stack 5
.end method

.method private getSharedPreferences()Landroid/content/SharedPreferences;
aload 0
getfield com/proxoid/Proxoid/mContext Landroid/content/Context;
ldc "proxoidv6"
iconst_4
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
areturn
.limit locals 1
.limit stack 3
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
.catch android/os/RemoteException from L0 to L1 using L2
aload 0
aload 2
checkcast com/proxoid/IProxoidControl
putfield com/proxoid/Proxoid/proxoidControl Lcom/proxoid/IProxoidControl;
aload 0
getfield com/proxoid/Proxoid/proxoidControl Lcom/proxoid/IProxoidControl;
ifnull L1
L0:
aload 0
getfield com/proxoid/Proxoid/proxoidControl Lcom/proxoid/IProxoidControl;
invokeinterface com/proxoid/IProxoidControl/update()Z 0
pop
L1:
return
L2:
astore 1
aload 1
invokevirtual android/os/RemoteException/printStackTrace()V
return
.limit locals 3
.limit stack 2
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
aload 0
aconst_null
putfield com/proxoid/Proxoid/proxoidControl Lcom/proxoid/IProxoidControl;
return
.limit locals 2
.limit stack 2
.end method
