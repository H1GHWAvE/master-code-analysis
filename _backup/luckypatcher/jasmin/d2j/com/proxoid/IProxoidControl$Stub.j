.bytecode 50.0
.class public synchronized abstract com/proxoid/IProxoidControl$Stub
.super android/os/Binder
.implements com/proxoid/IProxoidControl
.inner class public static abstract Stub inner com/proxoid/IProxoidControl$Stub outer com/proxoid/IProxoidControl
.inner class private static Proxy inner com/proxoid/IProxoidControl$Stub$Proxy outer com/proxoid/IProxoidControl$Stub

.field private static final 'DESCRIPTOR' Ljava/lang/String; = "com.proxoid.IProxoidControl"

.field static final 'TRANSACTION_update' I = 1


.method public <init>()V
aload 0
invokespecial android/os/Binder/<init>()V
aload 0
aload 0
ldc "com.proxoid.IProxoidControl"
invokevirtual com/proxoid/IProxoidControl$Stub/attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
return
.limit locals 1
.limit stack 3
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/proxoid/IProxoidControl;
aload 0
ifnonnull L0
aconst_null
areturn
L0:
aload 0
ldc "com.proxoid.IProxoidControl"
invokeinterface android/os/IBinder/queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface; 1
astore 1
aload 1
ifnull L1
aload 1
instanceof com/proxoid/IProxoidControl
ifeq L1
aload 1
checkcast com/proxoid/IProxoidControl
areturn
L1:
new com/proxoid/IProxoidControl$Stub$Proxy
dup
aload 0
invokespecial com/proxoid/IProxoidControl$Stub$Proxy/<init>(Landroid/os/IBinder;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public asBinder()Landroid/os/IBinder;
aload 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
.throws android/os/RemoteException
iload 1
lookupswitch
1 : L0
1598968902 : L1
default : L2
L2:
aload 0
iload 1
aload 2
aload 3
iload 4
invokespecial android/os/Binder/onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
ireturn
L1:
aload 3
ldc "com.proxoid.IProxoidControl"
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L0:
aload 2
ldc "com.proxoid.IProxoidControl"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/proxoid/IProxoidControl$Stub/update()Z
istore 5
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iload 5
ifeq L3
iconst_1
istore 1
L4:
aload 3
iload 1
invokevirtual android/os/Parcel/writeInt(I)V
iconst_1
ireturn
L3:
iconst_0
istore 1
goto L4
.limit locals 6
.limit stack 5
.end method
