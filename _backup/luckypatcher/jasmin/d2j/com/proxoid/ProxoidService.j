.bytecode 50.0
.class public synchronized com/proxoid/ProxoidService
.super android/app/Service
.inner class inner com/proxoid/ProxoidService$1
.inner class inner com/proxoid/ProxoidService$1$1
.inner class private UserAgentRequestFilter inner com/proxoid/ProxoidService$UserAgentRequestFilter outer com/proxoid/ProxoidService

.field private static 'ID' I = 0


.field private static final 'TAG' Ljava/lang/String; = "ProxoidService"

.field private 'proxy' Lcom/mba/proxylight/ProxyLight;

.field private 'randomUserAgent' Ljava/lang/String;

.field private 'useragent' Ljava/lang/String;

.method static <clinit>()V
ldc_w 2130837551
putstatic com/proxoid/ProxoidService/ID I
return
.limit locals 0
.limit stack 1
.end method

.method public <init>()V
aload 0
invokespecial android/app/Service/<init>()V
aload 0
aconst_null
putfield com/proxoid/ProxoidService/proxy Lcom/mba/proxylight/ProxyLight;
aload 0
aconst_null
putfield com/proxoid/ProxoidService/useragent Ljava/lang/String;
aload 0
invokestatic java/lang/System/currentTimeMillis()J
bipush 20
invokestatic java/lang/Long/toString(JI)Ljava/lang/String;
putfield com/proxoid/ProxoidService/randomUserAgent Ljava/lang/String;
return
.limit locals 1
.limit stack 4
.end method

.method static synthetic access$000(Lcom/proxoid/ProxoidService;)Landroid/content/SharedPreferences;
aload 0
invokespecial com/proxoid/ProxoidService/getSharedPreferences()Landroid/content/SharedPreferences;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$102(Lcom/proxoid/ProxoidService;Ljava/lang/String;)Ljava/lang/String;
aload 0
aload 1
putfield com/proxoid/ProxoidService/useragent Ljava/lang/String;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic access$200(Lcom/proxoid/ProxoidService;)V
aload 0
invokespecial com/proxoid/ProxoidService/doStop()V
return
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$300(Lcom/proxoid/ProxoidService;)Lcom/mba/proxylight/ProxyLight;
aload 0
getfield com/proxoid/ProxoidService/proxy Lcom/mba/proxylight/ProxyLight;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$302(Lcom/proxoid/ProxoidService;Lcom/mba/proxylight/ProxyLight;)Lcom/mba/proxylight/ProxyLight;
aload 0
aload 1
putfield com/proxoid/ProxoidService/proxy Lcom/mba/proxylight/ProxyLight;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic access$500()I
getstatic com/proxoid/ProxoidService/ID I
ireturn
.limit locals 0
.limit stack 1
.end method

.method private doStop()V
aload 0
getfield com/proxoid/ProxoidService/proxy Lcom/mba/proxylight/ProxyLight;
ifnull L0
aload 0
getfield com/proxoid/ProxoidService/proxy Lcom/mba/proxylight/ProxyLight;
invokevirtual com/mba/proxylight/ProxyLight/isRunning()Z
ifeq L0
ldc "ProxoidService"
ldc "stopping"
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/proxoid/ProxoidService/proxy Lcom/mba/proxylight/ProxyLight;
invokevirtual com/mba/proxylight/ProxyLight/stop()V
aload 0
ldc "notification"
invokevirtual com/proxoid/ProxoidService/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/app/NotificationManager
getstatic com/proxoid/ProxoidService/ID I
invokevirtual android/app/NotificationManager/cancel(I)V
aload 0
ldc "Proxy stopped."
iconst_0
invokestatic android/widget/Toast/makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;
invokevirtual android/widget/Toast/show()V
aload 0
invokespecial com/proxoid/ProxoidService/getSharedPreferences()Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
astore 1
aload 1
ldc "onoff"
iconst_0
invokeinterface android/content/SharedPreferences$Editor/putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor; 2
pop
aload 1
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
L0:
return
.limit locals 2
.limit stack 3
.end method

.method private getSharedPreferences()Landroid/content/SharedPreferences;
aload 0
ldc "proxoidv6"
iconst_4
invokespecial android/app/Service/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
areturn
.limit locals 1
.limit stack 3
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
new com/proxoid/ProxoidService$1
dup
aload 0
invokespecial com/proxoid/ProxoidService$1/<init>(Lcom/proxoid/ProxoidService;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public onCreate()V
aload 0
invokespecial android/app/Service/onCreate()V
return
.limit locals 1
.limit stack 1
.end method

.method public onDestroy()V
aload 0
invokespecial com/proxoid/ProxoidService/doStop()V
aload 0
invokespecial android/app/Service/onDestroy()V
return
.limit locals 1
.limit stack 1
.end method
