.bytecode 50.0
.class public synchronized com/chelpus/root/utils/runpatchads
.super java/lang/Object
.inner class static final inner com/chelpus/root/utils/runpatchads$1

.field private static 'ART' Z

.field public static 'AdsBlockFile' Ljava/lang/String;

.field public static 'appdir' Ljava/lang/String;

.field public static 'classesFiles' Ljava/util/ArrayList; signature "Ljava/util/ArrayList<Ljava/io/File;>;"

.field private static 'copyDC' Z

.field private static 'createAPK' Z

.field public static 'crkapk' Ljava/io/File;

.field private static 'dependencies' Z

.field public static 'dir' Ljava/lang/String;

.field public static 'dir2' Ljava/lang/String;

.field public static 'dirapp' Ljava/lang/String;

.field private static 'fileblock' Z

.field public static 'filestopatch' Ljava/util/ArrayList; signature "Ljava/util/ArrayList<Ljava/io/File;>;"

.field private static 'full_offline' Z

.field private static 'pattern1' Z

.field private static 'pattern2' Z

.field private static 'pattern3' Z

.field private static 'pattern4' Z

.field private static 'pattern5' Z

.field private static 'pattern6' Z

.field public static 'print' Ljava/io/PrintStream;

.field public static 'result' Ljava/lang/String;

.field public static 'sddir' Ljava/lang/String;

.field public static 'sites' [[B

.field public static 'system' Z

.field public static 'uid' Ljava/lang/String;

.method static <clinit>()V
iconst_0
putstatic com/chelpus/root/utils/runpatchads/createAPK Z
iconst_1
putstatic com/chelpus/root/utils/runpatchads/fileblock Z
iconst_1
putstatic com/chelpus/root/utils/runpatchads/pattern1 Z
iconst_1
putstatic com/chelpus/root/utils/runpatchads/pattern2 Z
iconst_1
putstatic com/chelpus/root/utils/runpatchads/pattern3 Z
iconst_1
putstatic com/chelpus/root/utils/runpatchads/pattern4 Z
iconst_1
putstatic com/chelpus/root/utils/runpatchads/pattern5 Z
iconst_1
putstatic com/chelpus/root/utils/runpatchads/pattern6 Z
iconst_1
putstatic com/chelpus/root/utils/runpatchads/dependencies Z
iconst_1
putstatic com/chelpus/root/utils/runpatchads/full_offline Z
iconst_0
putstatic com/chelpus/root/utils/runpatchads/copyDC Z
iconst_0
putstatic com/chelpus/root/utils/runpatchads/ART Z
ldc "/data/app/"
putstatic com/chelpus/root/utils/runpatchads/dirapp Ljava/lang/String;
iconst_0
putstatic com/chelpus/root/utils/runpatchads/system Z
ldc ""
putstatic com/chelpus/root/utils/runpatchads/uid Ljava/lang/String;
ldc "/sdcard/"
putstatic com/chelpus/root/utils/runpatchads/dir Ljava/lang/String;
ldc "/sdcard/"
putstatic com/chelpus/root/utils/runpatchads/dir2 Ljava/lang/String;
aconst_null
putstatic com/chelpus/root/utils/runpatchads/filestopatch Ljava/util/ArrayList;
ldc "/sdcard/"
putstatic com/chelpus/root/utils/runpatchads/sddir Ljava/lang/String;
ldc "/sdcard/"
putstatic com/chelpus/root/utils/runpatchads/appdir Ljava/lang/String;
ldc ""
putstatic com/chelpus/root/utils/runpatchads/AdsBlockFile Ljava/lang/String;
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putstatic com/chelpus/root/utils/runpatchads/classesFiles Ljava/util/ArrayList;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static byteverify(Ljava/nio/MappedByteBuffer;IB[B[B[B[BLjava/lang/String;Z)Z
iload 2
aload 3
iconst_0
baload
if_icmpne L0
iload 8
ifeq L0
aload 6
iconst_0
baload
ifne L1
aload 5
iconst_0
iload 2
bastore
L1:
iconst_1
istore 9
aload 0
iload 1
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 0
invokevirtual java/nio/MappedByteBuffer/get()B
istore 2
L2:
iload 2
aload 3
iload 9
baload
if_icmpeq L3
aload 4
iload 9
baload
iconst_1
if_icmpne L4
L3:
aload 6
iload 9
baload
ifne L5
aload 5
iload 9
iload 2
bastore
L5:
iload 9
iconst_1
iadd
istore 9
iload 9
aload 3
arraylength
if_icmpne L6
aload 0
iload 1
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 0
aload 5
invokevirtual java/nio/MappedByteBuffer/put([B)Ljava/nio/ByteBuffer;
pop
aload 0
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 7
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
iconst_1
ireturn
L6:
aload 0
invokevirtual java/nio/MappedByteBuffer/get()B
istore 2
goto L2
L4:
aload 0
iload 1
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L0:
iconst_0
ireturn
.limit locals 10
.limit stack 3
.end method

.method private static final calcChecksum([BI)V
new java/util/zip/Adler32
dup
invokespecial java/util/zip/Adler32/<init>()V
astore 3
aload 3
aload 0
bipush 12
aload 0
arraylength
iload 1
bipush 12
iadd
isub
invokevirtual java/util/zip/Adler32/update([BII)V
aload 3
invokevirtual java/util/zip/Adler32/getValue()J
l2i
istore 2
aload 0
iload 1
bipush 8
iadd
iload 2
i2b
bastore
aload 0
iload 1
bipush 9
iadd
iload 2
bipush 8
ishr
i2b
bastore
aload 0
iload 1
bipush 10
iadd
iload 2
bipush 16
ishr
i2b
bastore
aload 0
iload 1
bipush 11
iadd
iload 2
bipush 24
ishr
i2b
bastore
return
.limit locals 4
.limit stack 6
.end method

.method private static final calcSignature([BI)V
.catch java/security/NoSuchAlgorithmException from L0 to L1 using L2
.catch java/security/DigestException from L3 to L4 using L5
.catch java/security/DigestException from L6 to L5 using L5
L0:
ldc "SHA-1"
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
astore 2
L1:
aload 2
aload 0
bipush 32
aload 0
arraylength
iload 1
bipush 32
iadd
isub
invokevirtual java/security/MessageDigest/update([BII)V
L3:
aload 2
aload 0
iload 1
bipush 12
iadd
bipush 20
invokevirtual java/security/MessageDigest/digest([BII)I
istore 1
L4:
iload 1
bipush 20
if_icmpeq L7
L6:
new java/lang/RuntimeException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "unexpected digest write:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc "bytes"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L5:
astore 0
new java/lang/RuntimeException
dup
aload 0
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
L2:
astore 0
new java/lang/RuntimeException
dup
aload 0
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
L7:
return
.limit locals 3
.limit stack 6
.end method

.method public static clearTemp()V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L2
.catch java/lang/Exception from L6 to L7 using L2
L0:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchads/dir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/AndroidManifest.xml"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
aload 0
invokevirtual java/io/File/exists()Z
ifeq L1
aload 0
invokevirtual java/io/File/delete()Z
pop
L1:
getstatic com/chelpus/root/utils/runpatchads/classesFiles Ljava/util/ArrayList;
ifnull L5
getstatic com/chelpus/root/utils/runpatchads/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifle L5
getstatic com/chelpus/root/utils/runpatchads/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 0
L3:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L5
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
astore 1
aload 1
invokevirtual java/io/File/exists()Z
ifeq L3
aload 1
invokevirtual java/io/File/delete()Z
pop
L4:
goto L3
L2:
astore 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
L8:
return
L5:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchads/dir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/classes.dex"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
aload 0
invokevirtual java/io/File/exists()Z
ifeq L6
aload 0
invokevirtual java/io/File/delete()Z
pop
L6:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchads/dir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/classes.dex.apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
aload 0
invokevirtual java/io/File/exists()Z
ifeq L8
aload 0
invokevirtual java/io/File/delete()Z
pop
L7:
return
.limit locals 2
.limit stack 4
.end method

.method public static clearTempSD()V
.catch java/lang/Exception from L0 to L1 using L2
L0:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchads/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/classes.dex.apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
aload 0
invokevirtual java/io/File/exists()Z
ifeq L1
aload 0
invokevirtual java/io/File/delete()Z
pop
L1:
return
L2:
astore 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
return
.limit locals 1
.limit stack 4
.end method

.method public static fixadler(Ljava/io/File;)V
.catch java/lang/Exception from L0 to L1 using L2
L0:
new java/io/FileInputStream
dup
aload 0
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
astore 2
aload 2
invokevirtual java/io/FileInputStream/available()I
newarray byte
astore 1
aload 2
aload 1
invokevirtual java/io/FileInputStream/read([B)I
pop
aload 1
iconst_0
invokestatic com/chelpus/root/utils/runpatchads/calcSignature([BI)V
aload 1
iconst_0
invokestatic com/chelpus/root/utils/runpatchads/calcChecksum([BI)V
aload 2
invokevirtual java/io/FileInputStream/close()V
new java/io/FileOutputStream
dup
aload 0
invokespecial java/io/FileOutputStream/<init>(Ljava/io/File;)V
astore 0
aload 0
aload 1
invokevirtual java/io/FileOutputStream/write([B)V
aload 0
invokevirtual java/io/FileOutputStream/close()V
L1:
return
L2:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
return
.limit locals 3
.limit stack 3
.end method

.method public static main([Ljava/lang/String;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/NullPointerException from L5 to L6 using L7
.catch java/lang/Exception from L5 to L6 using L8
.catch java/lang/NullPointerException from L6 to L9 using L7
.catch java/lang/Exception from L6 to L9 using L8
.catch java/lang/NullPointerException from L9 to L10 using L7
.catch java/lang/Exception from L9 to L10 using L8
.catch java/lang/NullPointerException from L10 to L11 using L7
.catch java/lang/Exception from L10 to L11 using L8
.catch java/lang/NullPointerException from L11 to L12 using L7
.catch java/lang/Exception from L11 to L12 using L8
.catch java/lang/NullPointerException from L12 to L13 using L7
.catch java/lang/Exception from L12 to L13 using L8
.catch java/lang/NullPointerException from L13 to L14 using L7
.catch java/lang/Exception from L13 to L14 using L8
.catch java/lang/NullPointerException from L14 to L15 using L7
.catch java/lang/Exception from L14 to L15 using L8
.catch java/lang/NullPointerException from L15 to L16 using L7
.catch java/lang/Exception from L15 to L16 using L8
.catch java/lang/NullPointerException from L17 to L18 using L7
.catch java/lang/Exception from L17 to L18 using L8
.catch java/lang/NullPointerException from L19 to L20 using L7
.catch java/lang/Exception from L19 to L20 using L8
.catch java/lang/NullPointerException from L21 to L22 using L7
.catch java/lang/Exception from L21 to L22 using L8
.catch java/lang/NullPointerException from L23 to L24 using L7
.catch java/lang/Exception from L23 to L24 using L8
.catch java/lang/NullPointerException from L25 to L26 using L7
.catch java/lang/Exception from L25 to L26 using L8
.catch java/lang/NullPointerException from L26 to L27 using L28
.catch java/lang/Exception from L26 to L27 using L29
.catch java/lang/Exception from L30 to L31 using L32
.catch java/lang/Exception from L31 to L33 using L32
.catch java/lang/Exception from L34 to L35 using L32
.catch java/lang/Exception from L36 to L37 using L38
.catch java/lang/Exception from L37 to L39 using L38
.catch java/lang/Exception from L40 to L41 using L38
.catch java/io/FileNotFoundException from L42 to L43 using L44
.catch java/lang/Exception from L42 to L43 using L45
.catch java/io/FileNotFoundException from L43 to L46 using L44
.catch java/lang/Exception from L43 to L46 using L45
.catch java/io/FileNotFoundException from L46 to L47 using L44
.catch java/lang/Exception from L46 to L47 using L45
.catch java/io/FileNotFoundException from L47 to L48 using L44
.catch java/lang/Exception from L47 to L48 using L45
.catch java/io/FileNotFoundException from L49 to L50 using L44
.catch java/lang/Exception from L49 to L50 using L45
.catch java/io/FileNotFoundException from L50 to L44 using L44
.catch java/lang/Exception from L50 to L44 using L45
.catch java/lang/NullPointerException from L51 to L52 using L7
.catch java/lang/Exception from L51 to L52 using L8
.catch java/lang/Exception from L53 to L54 using L32
.catch java/lang/Exception from L55 to L56 using L38
.catch java/io/FileNotFoundException from L57 to L58 using L44
.catch java/lang/Exception from L57 to L58 using L45
.catch java/io/FileNotFoundException from L58 to L45 using L44
.catch java/lang/Exception from L58 to L45 using L45
.catch java/io/FileNotFoundException from L59 to L60 using L44
.catch java/lang/Exception from L59 to L60 using L45
.catch java/io/FileNotFoundException from L61 to L62 using L44
.catch java/lang/Exception from L61 to L62 using L45
.catch java/io/FileNotFoundException from L62 to L63 using L44
.catch java/lang/Exception from L62 to L63 using L45
.catch java/io/FileNotFoundException from L63 to L64 using L44
.catch java/lang/Exception from L63 to L64 using L45
.catch java/io/FileNotFoundException from L64 to L65 using L44
.catch java/lang/Exception from L64 to L65 using L45
.catch java/io/FileNotFoundException from L65 to L66 using L44
.catch java/lang/Exception from L65 to L66 using L45
.catch java/io/FileNotFoundException from L67 to L68 using L44
.catch java/lang/Exception from L67 to L68 using L45
.catch java/io/FileNotFoundException from L68 to L69 using L44
.catch java/lang/Exception from L68 to L69 using L45
.catch java/io/FileNotFoundException from L69 to L70 using L44
.catch java/lang/Exception from L69 to L70 using L45
.catch java/io/FileNotFoundException from L70 to L71 using L44
.catch java/lang/Exception from L70 to L71 using L45
.catch java/io/FileNotFoundException from L72 to L73 using L44
.catch java/lang/Exception from L72 to L73 using L45
.catch java/io/FileNotFoundException from L74 to L75 using L44
.catch java/lang/Exception from L74 to L75 using L45
.catch java/io/FileNotFoundException from L75 to L76 using L44
.catch java/lang/Exception from L75 to L76 using L45
.catch java/io/FileNotFoundException from L76 to L77 using L44
.catch java/lang/Exception from L76 to L77 using L45
.catch java/io/FileNotFoundException from L77 to L78 using L44
.catch java/lang/Exception from L77 to L78 using L45
.catch java/io/FileNotFoundException from L78 to L79 using L44
.catch java/lang/Exception from L78 to L79 using L45
.catch java/io/FileNotFoundException from L80 to L81 using L44
.catch java/lang/Exception from L80 to L81 using L45
.catch java/io/FileNotFoundException from L82 to L83 using L44
.catch java/lang/Exception from L82 to L83 using L45
.catch java/io/FileNotFoundException from L83 to L84 using L44
.catch java/lang/Exception from L83 to L84 using L45
.catch java/io/FileNotFoundException from L84 to L85 using L44
.catch java/lang/Exception from L84 to L85 using L45
.catch java/io/FileNotFoundException from L85 to L86 using L44
.catch java/lang/Exception from L85 to L86 using L45
.catch java/io/FileNotFoundException from L86 to L87 using L44
.catch java/lang/Exception from L86 to L87 using L45
.catch java/io/FileNotFoundException from L88 to L89 using L44
.catch java/lang/Exception from L88 to L89 using L45
.catch java/io/FileNotFoundException from L90 to L91 using L44
.catch java/lang/Exception from L90 to L91 using L45
.catch java/io/FileNotFoundException from L92 to L93 using L44
.catch java/lang/Exception from L92 to L93 using L45
.catch java/io/FileNotFoundException from L93 to L94 using L44
.catch java/lang/Exception from L93 to L94 using L45
.catch java/io/FileNotFoundException from L94 to L95 using L44
.catch java/lang/Exception from L94 to L95 using L45
.catch java/io/FileNotFoundException from L96 to L97 using L44
.catch java/lang/Exception from L96 to L97 using L45
.catch java/io/FileNotFoundException from L98 to L99 using L44
.catch java/lang/Exception from L98 to L99 using L45
.catch java/io/FileNotFoundException from L99 to L100 using L44
.catch java/lang/Exception from L99 to L100 using L45
.catch java/io/FileNotFoundException from L101 to L102 using L44
.catch java/lang/Exception from L101 to L102 using L45
.catch java/io/FileNotFoundException from L102 to L103 using L44
.catch java/lang/Exception from L102 to L103 using L45
.catch java/io/FileNotFoundException from L104 to L105 using L44
.catch java/lang/Exception from L104 to L105 using L45
.catch java/io/FileNotFoundException from L106 to L107 using L44
.catch java/lang/Exception from L106 to L107 using L45
.catch java/io/FileNotFoundException from L108 to L109 using L44
.catch java/lang/Exception from L108 to L109 using L45
.catch java/lang/Exception from L110 to L111 using L112
.catch java/io/FileNotFoundException from L110 to L111 using L44
.catch java/lang/Exception from L113 to L114 using L112
.catch java/io/FileNotFoundException from L113 to L114 using L44
.catch java/lang/Exception from L115 to L116 using L112
.catch java/io/FileNotFoundException from L115 to L116 using L44
.catch java/lang/Exception from L116 to L117 using L112
.catch java/io/FileNotFoundException from L116 to L117 using L44
.catch java/lang/Exception from L118 to L119 using L112
.catch java/io/FileNotFoundException from L118 to L119 using L44
.catch java/lang/Exception from L120 to L121 using L112
.catch java/io/FileNotFoundException from L120 to L121 using L44
.catch java/lang/Exception from L122 to L123 using L112
.catch java/io/FileNotFoundException from L122 to L123 using L44
.catch java/lang/Exception from L124 to L125 using L112
.catch java/io/FileNotFoundException from L124 to L125 using L44
.catch java/lang/Exception from L126 to L127 using L112
.catch java/io/FileNotFoundException from L126 to L127 using L44
.catch java/lang/Exception from L127 to L128 using L112
.catch java/io/FileNotFoundException from L127 to L128 using L44
.catch java/lang/Exception from L129 to L130 using L112
.catch java/io/FileNotFoundException from L129 to L130 using L44
.catch java/lang/Exception from L130 to L131 using L112
.catch java/io/FileNotFoundException from L130 to L131 using L44
.catch java/lang/Exception from L131 to L132 using L112
.catch java/io/FileNotFoundException from L131 to L132 using L44
.catch java/lang/Exception from L132 to L133 using L112
.catch java/io/FileNotFoundException from L132 to L133 using L44
.catch java/lang/Exception from L134 to L135 using L112
.catch java/io/FileNotFoundException from L134 to L135 using L44
.catch java/lang/Exception from L135 to L136 using L112
.catch java/io/FileNotFoundException from L135 to L136 using L44
.catch java/io/FileNotFoundException from L137 to L138 using L44
.catch java/lang/Exception from L137 to L138 using L45
.catch java/io/FileNotFoundException from L138 to L139 using L44
.catch java/lang/Exception from L138 to L139 using L45
.catch java/lang/Exception from L140 to L141 using L112
.catch java/io/FileNotFoundException from L140 to L141 using L44
.catch java/lang/Exception from L141 to L142 using L112
.catch java/io/FileNotFoundException from L141 to L142 using L44
.catch java/lang/Exception from L142 to L143 using L112
.catch java/io/FileNotFoundException from L142 to L143 using L44
.catch java/lang/Exception from L144 to L145 using L112
.catch java/io/FileNotFoundException from L144 to L145 using L44
.catch java/lang/Exception from L145 to L146 using L112
.catch java/io/FileNotFoundException from L145 to L146 using L44
.catch java/lang/Exception from L146 to L147 using L112
.catch java/io/FileNotFoundException from L146 to L147 using L44
.catch java/lang/Exception from L147 to L148 using L112
.catch java/io/FileNotFoundException from L147 to L148 using L44
.catch java/lang/Exception from L148 to L149 using L112
.catch java/io/FileNotFoundException from L148 to L149 using L44
.catch java/lang/Exception from L150 to L151 using L112
.catch java/io/FileNotFoundException from L150 to L151 using L44
.catch java/lang/Exception from L151 to L152 using L112
.catch java/io/FileNotFoundException from L151 to L152 using L44
.catch java/lang/Exception from L153 to L154 using L112
.catch java/io/FileNotFoundException from L153 to L154 using L44
.catch java/lang/Exception from L155 to L156 using L112
.catch java/io/FileNotFoundException from L155 to L156 using L44
.catch java/lang/Exception from L156 to L157 using L112
.catch java/io/FileNotFoundException from L156 to L157 using L44
.catch java/lang/Exception from L158 to L159 using L112
.catch java/io/FileNotFoundException from L158 to L159 using L44
.catch java/lang/Exception from L160 to L161 using L112
.catch java/io/FileNotFoundException from L160 to L161 using L44
.catch java/lang/Exception from L162 to L163 using L112
.catch java/io/FileNotFoundException from L162 to L163 using L44
.catch java/lang/Exception from L164 to L165 using L112
.catch java/io/FileNotFoundException from L164 to L165 using L44
new com/android/vending/billing/InAppBillingService/LUCK/LogOutputStream
dup
ldc "System.out"
invokespecial com/android/vending/billing/InAppBillingService/LUCK/LogOutputStream/<init>(Ljava/lang/String;)V
astore 11
new java/io/PrintStream
dup
aload 11
invokespecial java/io/PrintStream/<init>(Ljava/io/OutputStream;)V
putstatic com/chelpus/root/utils/runpatchads/print Ljava/io/PrintStream;
getstatic com/chelpus/root/utils/runpatchads/print Ljava/io/PrintStream;
ldc "Ads-Code Running!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new com/chelpus/root/utils/runpatchads$1
dup
invokespecial com/chelpus/root/utils/runpatchads$1/<init>()V
invokestatic com/chelpus/Utils/startRootJava(Ljava/lang/Object;)V
ldc "Ads-Code Running!"
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
aload 0
iconst_0
aaload
invokestatic com/chelpus/Utils/kill(Ljava/lang/String;)V
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 12
iconst_1
putstatic com/chelpus/root/utils/runpatchads/fileblock Z
iconst_1
putstatic com/chelpus/root/utils/runpatchads/pattern1 Z
iconst_1
putstatic com/chelpus/root/utils/runpatchads/pattern2 Z
iconst_1
putstatic com/chelpus/root/utils/runpatchads/pattern3 Z
iconst_1
putstatic com/chelpus/root/utils/runpatchads/pattern4 Z
iconst_1
putstatic com/chelpus/root/utils/runpatchads/pattern5 Z
iconst_1
putstatic com/chelpus/root/utils/runpatchads/pattern6 Z
iconst_1
putstatic com/chelpus/root/utils/runpatchads/dependencies Z
iconst_1
putstatic com/chelpus/root/utils/runpatchads/full_offline Z
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putstatic com/chelpus/root/utils/runpatchads/filestopatch Ljava/util/ArrayList;
L0:
new java/io/File
dup
aload 0
iconst_3
aaload
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/listFiles()[Ljava/io/File;
astore 13
aload 13
arraylength
istore 4
L1:
iconst_0
istore 3
L166:
iload 3
iload 4
if_icmpge L5
aload 13
iload 3
aaload
astore 14
L3:
aload 14
invokevirtual java/io/File/isFile()Z
ifeq L4
aload 14
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc "busybox"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L4
aload 14
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc "reboot"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L4
aload 14
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc "dalvikvm"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L4
aload 14
invokevirtual java/io/File/delete()Z
pop
L4:
iload 3
iconst_1
iadd
istore 3
goto L166
L2:
astore 13
aload 13
invokevirtual java/lang/Exception/printStackTrace()V
L5:
aload 0
iconst_1
aaload
ldc "pattern0"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L6
iconst_0
putstatic com/chelpus/root/utils/runpatchads/fileblock Z
L6:
aload 0
iconst_1
aaload
ldc "pattern1"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L9
iconst_0
putstatic com/chelpus/root/utils/runpatchads/pattern1 Z
L9:
aload 0
iconst_1
aaload
ldc "pattern2"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L10
iconst_0
putstatic com/chelpus/root/utils/runpatchads/pattern2 Z
L10:
aload 0
iconst_1
aaload
ldc "pattern3"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L11
iconst_0
putstatic com/chelpus/root/utils/runpatchads/pattern3 Z
L11:
aload 0
iconst_1
aaload
ldc "pattern4"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L12
iconst_0
putstatic com/chelpus/root/utils/runpatchads/pattern4 Z
L12:
aload 0
iconst_1
aaload
ldc "pattern5"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L13
iconst_0
putstatic com/chelpus/root/utils/runpatchads/pattern5 Z
L13:
aload 0
iconst_1
aaload
ldc "pattern6"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L14
iconst_0
putstatic com/chelpus/root/utils/runpatchads/pattern6 Z
L14:
aload 0
iconst_1
aaload
ldc "dependencies"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L15
iconst_0
putstatic com/chelpus/root/utils/runpatchads/dependencies Z
L15:
aload 0
iconst_1
aaload
ldc "fulloffline"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L16
iconst_0
putstatic com/chelpus/root/utils/runpatchads/full_offline Z
L16:
aload 0
bipush 6
aaload
ifnull L18
L17:
aload 0
bipush 6
aaload
ldc "createAPK"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L18
iconst_1
putstatic com/chelpus/root/utils/runpatchads/createAPK Z
L18:
aload 0
bipush 6
aaload
ifnull L20
L19:
aload 0
bipush 6
aaload
ldc "ART"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L20
iconst_1
putstatic com/chelpus/root/utils/runpatchads/ART Z
L20:
aload 0
bipush 6
aaload
ifnull L22
L21:
aload 0
bipush 6
aaload
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
L22:
aload 0
bipush 7
aaload
ifnull L24
L23:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "1 "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
bipush 7
aaload
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
aload 0
bipush 7
aaload
putstatic com/chelpus/root/utils/runpatchads/AdsBlockFile Ljava/lang/String;
L24:
aload 0
bipush 8
aaload
ifnull L51
L25:
aload 0
bipush 8
aaload
putstatic com/chelpus/root/utils/runpatchads/uid Ljava/lang/String;
L26:
aload 0
iconst_5
aaload
ldc "copyDC"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L27
iconst_1
putstatic com/chelpus/root/utils/runpatchads/copyDC Z
L27:
getstatic com/chelpus/root/utils/runpatchads/createAPK Z
ifeq L167
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
L167:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 13
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 14
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 15
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 16
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 17
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 18
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 19
aload 14
ldc "1A ?? FF FF"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "1A ?? ?? ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern5 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "(offline intekekt 0)"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc "search_offline"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "1B ?? FF FF FF FF"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "1B ?? ?? ?? ?? ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern5 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "(offline intekekt 0)"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc "search_offline"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "6E 10 FF FF ?? 00 0C ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "6E 10 ?? ?? ?? 00 12 S0"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads5 Fixed!\noffline (sha intekekt 4)"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc "search_offline"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "6E 10 FF FF ?? 00 0C ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "6E 10 ?? ?? ?? 00 12 S0"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/full_offline Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads5 Fixed!\nfull_offline (sha intekekt 4)"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "12 F1 12 E2 1A ?? ?? ?? 70 ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "12 31 12 32 1A ?? ?? ?? 70 ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern1 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads1 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "13 ?? d8 02 13 ?? 5a 00 1A ?? ?? ?? 70 ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "13 ?? 03 00 13 ?? 03 00 1A ?? ?? ?? 70 ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern1 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads1 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "13 ?? d4 01 13 ?? 3c 00 1A ?? ?? ?? 70 ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "13 ?? 03 00 13 ?? 03 00 1A ?? ?? ?? 70 ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern1 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads1 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "13 ?? 2c 01 13 ?? fa 00 1A ?? ?? ?? 70 ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "13 ?? 03 00 13 ?? 03 00 1A ?? ?? ?? 70 ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern1 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads1 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "13 ?? 40 01 13 ?? 32 00 1A ?? ?? ?? 70 ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "13 ?? 03 00 13 ?? 03 00 1A ?? ?? ?? 70 ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern1 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads1 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "13 ?? 40 01 ?? ?? ?? ?? 13 ?? 32 00 1A ?? ?? ?? 70 ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "13 ?? 03 00 ?? ?? ?? ?? 13 ?? 03 00 1A ?? ?? ?? 70 ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern1 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads1 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "13 ?? A0 00 13 ?? 58 02 1A ?? ?? ?? 70 ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "13 ?? 03 00 13 ?? 03 00 1A ?? ?? ?? 70 ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern1 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads1 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "13 00 11 00 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 38 ?? 0D 00 12 E1 12 E2 6E ?? ?? ?? ?? ?? 12 E1 12 E2 6E ?? ?? ?? ?? ?? 0E 00 12 E0 12 E1 6E 40"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "13 00 11 00 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 38 ?? 0D 00 12 11 12 12 6E ?? ?? ?? ?? ?? 12 11 12 12 6E ?? ?? ?? ?? ?? 0E 00 12 10 12 11 6E 40"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern1 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads1 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "13 00 11 00 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 38 ?? 0D 00 12 E0 12 E1 6E ?? ?? ?? ?? ?? 12 E0 12 E1 6E ?? ?? ?? ?? ?? 0E 00 12 E0 12 E1 6E 40"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "13 00 11 00 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 38 ?? 0D 00 12 10 12 11 6E ?? ?? ?? ?? ?? 12 10 12 11 6E ?? ?? ?? ?? ?? 0E 00 12 10 12 11 6E 40"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern1 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads1 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "6E ?? ?? ?? ?? ?? 39 ?? ?? ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? ?? ?? ?? ?? ?? ?? 0A ?? 6E ?? ?? ?? ?? ?? 0A ?? ?? ?? ?? ?? ?? ?? 0A ?? D8 ?? ?? FE D8 ?? ?? FE 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0E 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "6E ?? ?? ?? ?? ?? 39 ?? ?? ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 12 ?? ?? ?? ?? ?? ?? ?? 0A ?? 6E ?? ?? ?? ?? ?? 12 ?? ?? ?? ?? ?? ?? ?? 0A ?? 12 ?? 00 00 12 ?? 00 00 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0E 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern1 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads1 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "12 04 39 06 0A 00 22 00 ?? ?? 1A 01 ?? ?? 70 20 ?? ?? ?? ?? 27 00 71 10 ?? ?? ?? ?? 70 20 ?? ?? ?? ?? 71 10 ?? ?? ?? ?? 5B ?? ?? ?? 59 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 5B"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "0E 00 39 06 0A 00 22 00 ?? ?? 1A 01 ?? ?? 70 20 ?? ?? ?? ?? 27 00 71 10 ?? ?? ?? ?? 70 20 ?? ?? ?? ?? 71 10 ?? ?? ?? ?? 5B ?? ?? ?? 59 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 5B"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern1 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads1 Fixed!\nIMAdView"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "12 F1 12 E2 1A ?? ?? ?? 70 ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "12 01 12 02 1A ?? ?? ?? 70 ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern1 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads1 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "13 ?? d4 01 13 ?? 3c 00 1A ?? ?? ?? 70 ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "13 ?? 00 00 13 ?? 00 00 1A ?? ?? ?? 70 ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern2 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads2 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "13 ?? 2c 01 13 ?? fa 00 1A ?? ?? ?? 70 ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "13 ?? 00 00 13 ?? 00 00 1A ?? ?? ?? 70 ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern2 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads2 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "13 ?? 40 01 13 ?? 32 00 1A ?? ?? ?? 70 ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "13 ?? 00 00 13 ?? 00 00 1A ?? ?? ?? 70 ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern2 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads2 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "13 ?? 40 01 ?? ?? ?? ?? 13 ?? 32 00 1A ?? ?? ?? 70 ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "13 ?? 00 00 ?? ?? ?? ?? 13 ?? 00 00 1A ?? ?? ?? 70 ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern2 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads2 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "13 ?? d8 02 13 ?? 5a 00 1A ?? ?? ?? 70 ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "13 ?? 00 00 13 ?? 00 00 1A ?? ?? ?? 70 ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern2 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads2 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "13 ?? A0 00 13 ?? 58 02 1A ?? ?? ?? 70 ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "13 ?? 00 00 13 ?? 00 00 1A ?? ?? ?? 70 ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern2 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads2 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "13 00 11 00 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 38 ?? 0D 00 12 E1 12 E2 6E ?? ?? ?? ?? ?? 12 E1 12 E2 6E ?? ?? ?? ?? ?? 0E 00 12 E0 12 E1 6E 40"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "13 00 11 00 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 38 ?? 0D 00 12 11 12 12 6E ?? ?? ?? ?? ?? 12 11 12 12 6E ?? ?? ?? ?? ?? 0E 00 12 10 12 11 6E 40"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern2 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads2 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "13 00 11 00 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 38 ?? 0D 00 12 E0 12 E1 6E ?? ?? ?? ?? ?? 12 E0 12 E1 6E ?? ?? ?? ?? ?? 0E 00 12 E0 12 E1 6E 40"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "13 00 11 00 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 38 ?? 0D 00 12 10 12 11 6E ?? ?? ?? ?? ?? 12 10 12 11 6E ?? ?? ?? ?? ?? 0E 00 12 10 12 11 6E 40"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern2 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads2 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "6E ?? ?? ?? ?? ?? 39 ?? ?? ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? ?? ?? ?? ?? ?? ?? 0A ?? 6E ?? ?? ?? ?? ?? 0A ?? ?? ?? ?? ?? ?? ?? 0A ?? D8 ?? ?? FE D8 ?? ?? FE 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0E 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "6E ?? ?? ?? ?? ?? 39 ?? ?? ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 12 ?? ?? ?? ?? ?? ?? ?? 0A ?? 6E ?? ?? ?? ?? ?? 12 ?? ?? ?? ?? ?? ?? ?? 0A ?? 12 ?? 00 00 12 ?? 00 00 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0E 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern2 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads2 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "12 04 39 06 0A 00 22 00 ?? ?? 1A 01 ?? ?? 70 20 ?? ?? ?? ?? 27 00 71 10 ?? ?? ?? ?? 70 20 ?? ?? ?? ?? 71 10 ?? ?? ?? ?? 5B ?? ?? ?? 59 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 5B"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "0E 00 39 06 0A 00 22 00 ?? ?? 1A 01 ?? ?? 70 20 ?? ?? ?? ?? 27 00 71 10 ?? ?? ?? ?? 70 20 ?? ?? ?? ?? 71 10 ?? ?? ?? ?? 5B ?? ?? ?? 59 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 5B"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern2 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads2 Fixed!\nIMAdView"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "55 ?? ?? ?? 39 00 ?? ?? 22 ?? ?? ?? 54 ?? ?? ?? 70 ?? ?? ?? ?? ?? 54 ?? ?? ?? 72 ?? ?? ?? ?? ?? 0C 01 54"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "55 ?? ?? ?? 32 00 ?? ?? 22 ?? ?? ?? 54 ?? ?? ?? 70 ?? ?? ?? ?? ?? 54 ?? ?? ?? 72 ?? ?? ?? ?? ?? 0C 01 54"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern3 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads3 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "1D ?? 54 ?? ?? ?? 39 ?? ?? ?? 5B ?? ?? ?? 12 00 5C ?? ?? ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 5B ?? ?? ?? 54 ?? ?? ?? 6E 10"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "1D ?? 54 ?? ?? ?? 39 ?? ?? ?? 5B ?? ?? ?? 12 10 5C ?? ?? ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 5B ?? ?? ?? 54 ?? ?? ?? 6E 10"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern3 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads3 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "1D ?? 70 ?? ?? ?? ?? ?? 0A ?? 38 ?? ?? ?? 1A ?? ?? ?? 71 ?? ?? ?? ?? ?? 1E ?? 0E 00 71"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "1D ?? 70 ?? ?? ?? ?? ?? 0A ?? 33 00 ?? ?? 1A ?? ?? ?? 71 ?? ?? ?? ?? ?? 1E ?? 0E 00 71"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern3 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads3 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "1D 01 54 10 ?? ?? 38 00 ?? ?? 12 10 1E 01 0F 00 12 00 28 ?? ?? ?? 1E 01 27 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "1D 01 54 10 ?? ?? 33 00 ?? ?? 12 10 1E 01 0F 00 12 00 28 ?? ?? ?? 1E 01 27 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern3 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads3 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "71 10 ?? ?? ?? ?? 15 03 FF FF 07 60 07 71 07 82 07 94 07 A5 76 06"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "71 10 ?? ?? ?? ?? 15 03 00 FF 07 60 07 71 07 82 07 94 07 A5 76 06"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern4 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads4 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "71 10 ?? ?? ?? ?? 15 03 FF FF 07 60 07 71 07 82 07 94 07 A5 74 06"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "71 10 ?? ?? ?? ?? 15 03 00 FF 07 60 07 71 07 82 07 94 07 A5 74 06"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern4 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads4 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "12 ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 33 ?? ?? ?? 1A ?? ?? ?? 71"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "12 ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 33 00 ?? ?? 1A ?? ?? ?? 71"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern4 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads4 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "1A 00 ?? ?? 6E 20 ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E 10 ?? ?? ?? ?? 0C 00 39 00 04 00 12 00 11 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "1A 00 ?? ?? 6E 20 ?? ?? ?? ?? 0C 01 1F ?? ?? ?? 6E 10 ?? ?? ?? ?? 0C 00 33 00 ?? ?? 12 00 11 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern5 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads5 Fixed!\noffline"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? 00 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A ?? 33 ?? ?? 00 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "6E ?? ?? ?? ?? ?? 0C ?? 32 00 ?? 00 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A ?? 33 ?? ?? 00 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern5 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads5 Fixed!\noffline"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "12 ?? 12 ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 39 ?? ?? ?? 0F ?? 6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A ?? 33 ?? ?? ?? 01 ?? 28 ?? 01 ?? 28"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "12 ?? 12 ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 33 00 ?? ?? 0F ?? 6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A ?? 33 ?? ?? ?? 01 ?? 28 ?? 01 ?? 28"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern5 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads5 Fixed!\noffline"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "12 ?? 12 ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 39 ?? ?? ?? 01 ?? 0F ?? 6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A ?? 33 ?? ?? ?? 01 ?? 28 ?? 01 ?? 28"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "12 ?? 12 ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 33 00 ?? ?? 01 ?? 0F ?? 6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A ?? 33 ?? ?? ?? 01 ?? 28 ?? 01 ?? 28"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern5 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads5 Fixed!\noffline"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "12 15 6E 10 ?? ?? ?? ?? 0C 01 1A ?? ?? ?? 6E 20 ?? ?? ?? ?? 0A 04 12 ?? 33 ?? ?? ?? 0F 05 6E 10"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "12 05 6E 10 ?? ?? ?? ?? 0C 01 1A ?? ?? ?? 6E 20 ?? ?? ?? ?? 0A 04 12 ?? 33 ?? ?? ?? 0F 05 6E 10"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern5 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads5 Fixed!\noffline"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "54 ?? ?? ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 1A ?? ?? ?? 54 ?? ?? ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 38 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 38"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "54 ?? ?? ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 1A ?? ?? ?? 54 ?? ?? ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 32 00 ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 38"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern5 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads5 Fixed!\noffline"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "12 01 39 04 04 00 01 10 0F 00 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 39 ?? ?? ?? 01 10 28"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "12 01 33 00 ?? ?? 01 10 0F 00 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 39 ?? ?? ?? 01 10 28"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern5 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads5 Fixed!\noffline"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "12 01 6E ?? ?? ?? ?? ?? 0C ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 38"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "12 01 6E ?? ?? ?? ?? ?? 0C ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 32 00 ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 38"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern5 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads5 Fixed!\noffline"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "6E ?? ?? ?? ?? ?? 0C ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 38 ?? ?? ?? 12 ?? 0F ?? 12 00 28"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "6E ?? ?? ?? ?? ?? 0C ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 32 00 ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 32 00 ?? ?? 12 ?? 0F ?? 12 00 28"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern5 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads5 Fixed!\noffline"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "54 ?? ?? ?? 39 ?? ?? ?? 1A ?? ?? ?? 1A ?? ?? ?? 71 ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0E 00 1A"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "54 ?? ?? ?? 33 00 ?? ?? 1A ?? ?? ?? 1A ?? ?? ?? 71 ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0E 00 1A"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern6 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads6 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "12 00 62 ?? ?? ?? 38 ?? ?? 00 62 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 39 ?? ?? 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "12 00 62 ?? ?? ?? 32 00 ?? 00 62 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 39 ?? ?? 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern6 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads6 Fixed!\nangry offline"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
iconst_0
aaload
ldc "com.buak.Link2SD"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L168
aload 14
ldc "00 05 2E 6F 64 65 78 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "00 05 2E 6F 64 65 79 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern6 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads6 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L168:
aload 14
ldc "00 04 6F 64 65 78 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "00 04 6F 64 65 79 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern6 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads6 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "54 ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 22 ?? ?? ?? 12 E3 12 E4 70 ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 54"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "54 ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 22 ?? ?? ?? 12 03 12 04 70 ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 54"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern6 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads6 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "12 08 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 39 ?? 04 00 01 ?? 0F 05 1A ?? ?? ?? 12"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "12 08 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 33 00 ?? ?? 01 ?? 0F 05 1A ?? ?? ?? 12"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/pattern6 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "ads6 Fixed!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
getstatic com/chelpus/root/utils/runpatchads/ART Z
ifne L169
aload 14
ldc "13 ?? 09 00 12 ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "12 00 0F 00 12 ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/dependencies Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "com.android.vending dependencies removed\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "12 ?? 12 ?? 13 ?? 09 00 6E ?? ?? ?? ?? ?? 0C ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "12 ?? 12 ?? 12 00 0F 00 6E ?? ?? ?? ?? ?? 0C ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/dependencies Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "com.android.vending dependencies removed\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L170:
aload 12
aload 14
aload 15
aload 16
aload 17
aload 18
aload 19
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokestatic com/chelpus/Utils/convertToPatchItemAuto(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/Boolean;)V
getstatic com/chelpus/root/utils/runpatchads/fileblock Z
ifeq L42
L30:
getstatic com/chelpus/root/utils/runpatchads/AdsBlockFile Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
new java/io/FileInputStream
dup
getstatic com/chelpus/root/utils/runpatchads/AdsBlockFile Ljava/lang/String;
invokespecial java/io/FileInputStream/<init>(Ljava/lang/String;)V
astore 14
new java/io/BufferedReader
dup
new java/io/InputStreamReader
dup
aload 14
invokespecial java/io/InputStreamReader/<init>(Ljava/io/InputStream;)V
invokespecial java/io/BufferedReader/<init>(Ljava/io/Reader;)V
astore 15
getstatic com/chelpus/root/utils/runpatchads/fileblock Z
ifeq L53
L31:
aload 15
invokevirtual java/io/BufferedReader/readLine()Ljava/lang/String;
astore 16
L33:
aload 16
ifnull L53
L34:
aload 16
invokevirtual java/lang/String/trim()Ljava/lang/String;
astore 16
aload 16
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L31
aload 13
aload 16
ldc "UTF-8"
invokevirtual java/lang/String/getBytes(Ljava/lang/String;)[B
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L35:
goto L31
L32:
astore 14
aload 14
invokevirtual java/lang/Exception/printStackTrace()V
L36:
new java/io/FileInputStream
dup
getstatic com/chelpus/root/utils/runpatchads/AdsBlockFile Ljava/lang/String;
ldc "AdsBlockList.txt"
ldc "AdsBlockList_user_edit.txt"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokespecial java/io/FileInputStream/<init>(Ljava/lang/String;)V
astore 14
new java/io/BufferedReader
dup
new java/io/InputStreamReader
dup
aload 14
invokespecial java/io/InputStreamReader/<init>(Ljava/io/InputStream;)V
invokespecial java/io/BufferedReader/<init>(Ljava/io/Reader;)V
astore 15
L37:
aload 15
invokevirtual java/io/BufferedReader/readLine()Ljava/lang/String;
astore 16
L39:
aload 16
ifnull L55
L40:
aload 16
invokevirtual java/lang/String/trim()Ljava/lang/String;
astore 16
aload 16
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L37
aload 13
aload 16
ldc "UTF-8"
invokevirtual java/lang/String/getBytes(Ljava/lang/String;)[B
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L41:
goto L37
L38:
astore 14
aload 14
invokevirtual java/lang/Exception/printStackTrace()V
L171:
aload 13
invokevirtual java/util/ArrayList/isEmpty()Z
ifne L42
aload 13
invokevirtual java/util/ArrayList/size()I
anewarray [B
putstatic com/chelpus/root/utils/runpatchads/sites [[B
aload 13
getstatic com/chelpus/root/utils/runpatchads/sites [[B
invokevirtual java/util/ArrayList/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
checkcast [[B
checkcast [[B
putstatic com/chelpus/root/utils/runpatchads/sites [[B
L42:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L43
aload 0
iconst_2
aaload
ldc "RW"
invokestatic com/chelpus/Utils/remount(Ljava/lang/String;Ljava/lang/String;)Z
pop
L43:
getstatic com/chelpus/root/utils/runpatchads/createAPK Z
ifne L72
getstatic com/chelpus/root/utils/runpatchads/ART Z
ifne L72
aload 0
iconst_3
aaload
putstatic com/chelpus/root/utils/runpatchads/dir Ljava/lang/String;
aload 0
iconst_2
aaload
putstatic com/chelpus/root/utils/runpatchads/dirapp Ljava/lang/String;
invokestatic com/chelpus/root/utils/runpatchads/clearTemp()V
aload 0
iconst_4
aaload
ldc "not_system"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L46
iconst_0
putstatic com/chelpus/root/utils/runpatchads/system Z
L46:
aload 0
iconst_4
aaload
ldc "system"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L47
iconst_1
putstatic com/chelpus/root/utils/runpatchads/system Z
L47:
getstatic com/chelpus/root/utils/runpatchads/filestopatch Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
ldc "CLASSES mode create odex enabled."
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
L48:
aload 0
iconst_0
aaload
astore 13
L49:
aload 0
iconst_2
aaload
putstatic com/chelpus/root/utils/runpatchads/appdir Ljava/lang/String;
aload 0
iconst_3
aaload
putstatic com/chelpus/root/utils/runpatchads/sddir Ljava/lang/String;
invokestatic com/chelpus/root/utils/runpatchads/clearTempSD()V
new java/io/File
dup
getstatic com/chelpus/root/utils/runpatchads/appdir Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 13
ldc "Get classes.dex."
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
getstatic com/chelpus/root/utils/runpatchads/print Ljava/io/PrintStream;
ldc "Get classes.dex."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 13
invokestatic com/chelpus/root/utils/runpatchads/unzipART(Ljava/io/File;)V
getstatic com/chelpus/root/utils/runpatchads/classesFiles Ljava/util/ArrayList;
ifnull L50
getstatic com/chelpus/root/utils/runpatchads/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifne L57
L50:
new java/io/FileNotFoundException
dup
invokespecial java/io/FileNotFoundException/<init>()V
athrow
L44:
astore 12
ldc "Error: Program files are not found!\n\nCheck the location dalvik-cache to solve problems!\n\nDefault: /data/dalvik-cache/*"
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
L172:
getstatic com/chelpus/root/utils/runpatchads/filestopatch Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 12
L173:
aload 12
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L174
aload 12
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
invokestatic com/chelpus/Utils/fixadler(Ljava/io/File;)V
invokestatic com/chelpus/root/utils/runpatchads/clearTempSD()V
goto L173
L51:
ldc "fignya"
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
L52:
goto L26
L7:
astore 13
goto L26
L169:
aload 14
ldc "13 ?? 09 00 12 ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "12 S0 00 00 12 S0 12 S0 6E ?? ?? ?? ?? ?? 0C ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/dependencies Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "com.android.vending dependencies removed\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 14
ldc "12 ?? 12 ?? 13 ?? 09 00 6E ?? ?? ?? ?? ?? 0C ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "12 S0 12 S0 12 S0 00 00 6E ?? ?? ?? ?? ?? 0C ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
getstatic com/chelpus/root/utils/runpatchads/dependencies Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 17
ldc "com.android.vending dependencies removed\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
goto L170
L53:
aload 14
invokevirtual java/io/FileInputStream/close()V
L54:
goto L36
L55:
aload 14
invokevirtual java/io/FileInputStream/close()V
L56:
goto L171
L57:
getstatic com/chelpus/root/utils/runpatchads/filestopatch Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
getstatic com/chelpus/root/utils/runpatchads/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 13
L58:
aload 13
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L61
aload 13
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
astore 14
aload 14
invokevirtual java/io/File/exists()Z
ifne L59
new java/io/FileNotFoundException
dup
invokespecial java/io/FileNotFoundException/<init>()V
athrow
L45:
astore 12
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Patch Process Exception: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 12
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
goto L172
L59:
getstatic com/chelpus/root/utils/runpatchads/filestopatch Ljava/util/ArrayList;
aload 14
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L60:
goto L58
L61:
aload 0
iconst_2
aaload
iconst_1
invokestatic com/chelpus/Utils/getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;
astore 13
new java/io/File
dup
aload 13
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 14
aload 14
invokevirtual java/io/File/exists()Z
ifeq L62
aload 14
invokevirtual java/io/File/delete()Z
pop
L62:
new java/io/File
dup
aload 13
ldc "-1"
ldc "-2"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 14
aload 14
invokevirtual java/io/File/exists()Z
ifeq L63
aload 14
invokevirtual java/io/File/delete()Z
pop
L63:
new java/io/File
dup
aload 13
ldc "-2"
ldc "-1"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 13
aload 13
invokevirtual java/io/File/exists()Z
ifeq L64
aload 13
invokevirtual java/io/File/delete()Z
pop
L64:
getstatic com/chelpus/root/utils/runpatchads/filestopatch Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 13
L65:
aload 13
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L172
aload 13
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
astore 14
ldc "Find string id."
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 15
aload 15
ldc "phone"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "Landroid/net/ConnectivityManager;"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 15
ldc "getActiveNetworkInfo"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
ldc "String analysis."
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
getstatic com/chelpus/root/utils/runpatchads/print Ljava/io/PrintStream;
ldc "String analysis."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 14
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aload 15
iconst_0
invokestatic com/chelpus/Utils/getStringIds(Ljava/lang/String;Ljava/util/ArrayList;Z)Ljava/util/ArrayList;
astore 16
L66:
iconst_0
istore 3
L67:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 15
aload 15
new com/android/vending/billing/InAppBillingService/LUCK/CommandItem
dup
ldc "Landroid/net/ConnectivityManager;"
ldc "getActiveNetworkInfo"
invokespecial com/android/vending/billing/InAppBillingService/LUCK/CommandItem/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 16
L68:
aload 16
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L175
aload 16
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/StringItem
astore 17
aload 15
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 18
L69:
aload 18
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L88
aload 18
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/CommandItem
astore 19
aload 19
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/object Ljava/lang/String;
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/str Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L70
aload 19
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
putfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/Object [B
L70:
aload 19
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/method Ljava/lang/String;
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/str Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L69
aload 19
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
putfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/Method [B
L71:
goto L69
L72:
getstatic com/chelpus/root/utils/runpatchads/createAPK Z
ifeq L80
L73:
aload 0
iconst_0
aaload
astore 13
L74:
aload 0
iconst_2
aaload
putstatic com/chelpus/root/utils/runpatchads/appdir Ljava/lang/String;
aload 0
iconst_5
aaload
putstatic com/chelpus/root/utils/runpatchads/sddir Ljava/lang/String;
invokestatic com/chelpus/root/utils/runpatchads/clearTempSD()V
new java/io/File
dup
getstatic com/chelpus/root/utils/runpatchads/appdir Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 14
aload 14
invokestatic com/chelpus/root/utils/runpatchads/unzipSD(Ljava/io/File;)V
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchads/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 13
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
putstatic com/chelpus/root/utils/runpatchads/crkapk Ljava/io/File;
aload 14
getstatic com/chelpus/root/utils/runpatchads/crkapk Ljava/io/File;
invokestatic com/chelpus/Utils/copyFile(Ljava/io/File;Ljava/io/File;)V
getstatic com/chelpus/root/utils/runpatchads/classesFiles Ljava/util/ArrayList;
ifnull L75
getstatic com/chelpus/root/utils/runpatchads/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifne L76
L75:
new java/io/FileNotFoundException
dup
invokespecial java/io/FileNotFoundException/<init>()V
athrow
L76:
getstatic com/chelpus/root/utils/runpatchads/filestopatch Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
getstatic com/chelpus/root/utils/runpatchads/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 13
L77:
aload 13
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L80
aload 13
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
astore 14
aload 14
invokevirtual java/io/File/exists()Z
ifne L78
new java/io/FileNotFoundException
dup
invokespecial java/io/FileNotFoundException/<init>()V
athrow
L78:
getstatic com/chelpus/root/utils/runpatchads/filestopatch Ljava/util/ArrayList;
aload 14
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L79:
goto L77
L80:
getstatic com/chelpus/root/utils/runpatchads/ART Z
ifeq L64
ldc "ART mode create dex enabled."
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
L81:
aload 0
iconst_0
aaload
astore 13
L82:
aload 0
iconst_2
aaload
putstatic com/chelpus/root/utils/runpatchads/appdir Ljava/lang/String;
aload 0
iconst_3
aaload
putstatic com/chelpus/root/utils/runpatchads/sddir Ljava/lang/String;
invokestatic com/chelpus/root/utils/runpatchads/clearTempSD()V
new java/io/File
dup
getstatic com/chelpus/root/utils/runpatchads/appdir Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokestatic com/chelpus/root/utils/runpatchads/unzipART(Ljava/io/File;)V
getstatic com/chelpus/root/utils/runpatchads/classesFiles Ljava/util/ArrayList;
ifnull L83
getstatic com/chelpus/root/utils/runpatchads/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifne L84
L83:
new java/io/FileNotFoundException
dup
invokespecial java/io/FileNotFoundException/<init>()V
athrow
L84:
getstatic com/chelpus/root/utils/runpatchads/filestopatch Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
getstatic com/chelpus/root/utils/runpatchads/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 13
L85:
aload 13
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L64
aload 13
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
astore 14
aload 14
invokevirtual java/io/File/exists()Z
ifne L86
new java/io/FileNotFoundException
dup
invokespecial java/io/FileNotFoundException/<init>()V
athrow
L86:
getstatic com/chelpus/root/utils/runpatchads/filestopatch Ljava/util/ArrayList;
aload 14
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L87:
goto L85
L88:
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/str Ljava/lang/String;
ldc "phone"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L68
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/bits32 Z
ifne L90
aload 12
iconst_0
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_2
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_0
baload
bastore
aload 12
iconst_0
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_3
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_1
baload
bastore
aload 12
iconst_1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/pattern Z
L89:
goto L176
L90:
aload 12
iconst_0
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/pattern Z
aload 12
iconst_1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_2
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_0
baload
bastore
aload 12
iconst_1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_3
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_1
baload
bastore
aload 12
iconst_1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_4
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_2
baload
bastore
aload 12
iconst_1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_5
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_3
baload
bastore
L91:
goto L176
L175:
iload 3
ifne L93
L92:
getstatic com/chelpus/root/utils/runpatchads/full_offline Z
ifeq L99
L93:
ldc "Parse data for patch."
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
getstatic com/chelpus/root/utils/runpatchads/print Ljava/io/PrintStream;
ldc "Parse data for patch."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 14
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aload 15
iconst_0
invokestatic com/chelpus/Utils/getMethodsIds(Ljava/lang/String;Ljava/util/ArrayList;Z)Z
pop
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 15
iconst_0
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/CommandItem
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/index_command [B
iconst_0
baload
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 15
iconst_0
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/CommandItem
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/index_command [B
iconst_1
baload
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 15
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 15
L94:
aload 15
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L177
aload 15
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/CommandItem
astore 16
aload 16
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/object Ljava/lang/String;
ldc "Landroid/net/ConnectivityManager;"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L94
aload 16
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/found_index_command Z
ifeq L96
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "save to command"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 12
iconst_2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_2
aload 16
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/index_command [B
iconst_0
baload
bastore
aload 12
iconst_2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_3
aload 16
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/index_command [B
iconst_1
baload
bastore
aload 12
iconst_3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_2
aload 16
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/index_command [B
iconst_0
baload
bastore
aload 12
iconst_3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_3
aload 16
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/index_command [B
iconst_1
baload
bastore
L95:
goto L94
L96:
aload 12
iconst_2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/pattern Z
aload 12
iconst_3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/pattern Z
L97:
goto L94
L177:
iload 3
ifne L99
L98:
aload 12
iconst_0
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/pattern Z
aload 12
iconst_1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/pattern Z
aload 12
iconst_2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/pattern Z
L99:
aload 12
invokevirtual java/util/ArrayList/size()I
anewarray com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
astore 15
L100:
iconst_0
istore 3
L101:
aload 12
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 16
L102:
aload 16
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L178
aload 15
iload 3
aload 16
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
aastore
L103:
iload 3
iconst_1
iadd
istore 3
goto L102
L178:
iconst_0
istore 3
L104:
getstatic com/chelpus/root/utils/runpatchads/fileblock Z
ifeq L105
ldc "String analysis."
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
getstatic com/chelpus/root/utils/runpatchads/print Ljava/io/PrintStream;
ldc "String analysis."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 14
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
getstatic com/chelpus/root/utils/runpatchads/sites [[B
iconst_0
bipush 64
invokestatic com/chelpus/Utils/setStringIds(Ljava/lang/String;[[BZB)I
istore 3
L105:
iload 3
ifle L108
iload 3
iconst_1
isub
istore 3
L106:
ldc "Site from AdsBlockList blocked!"
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
getstatic com/chelpus/root/utils/runpatchads/print Ljava/io/PrintStream;
ldc "Site from AdsBlockList blocked!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L107:
goto L105
L108:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Relaced strings:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 14
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "com.google.android.gms.ads.identifier.service.START"
aastore
iconst_0
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "com.google.android.gms.ads.identifier.service.STAPT"
aastore
invokestatic com/chelpus/Utils/replaceStringIds(Ljava/lang/String;[Ljava/lang/String;Z[Ljava/lang/String;)I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
invokestatic java/lang/System/currentTimeMillis()J
lstore 9
new java/io/RandomAccessFile
dup
aload 14
ldc "rw"
invokespecial java/io/RandomAccessFile/<init>(Ljava/io/File;Ljava/lang/String;)V
invokevirtual java/io/RandomAccessFile/getChannel()Ljava/nio/channels/FileChannel;
astore 14
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Size file:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 14
invokevirtual java/nio/channels/FileChannel/size()J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
aload 14
getstatic java/nio/channels/FileChannel$MapMode/READ_WRITE Ljava/nio/channels/FileChannel$MapMode;
lconst_0
aload 14
invokevirtual java/nio/channels/FileChannel/size()J
l2i
i2l
invokevirtual java/nio/channels/FileChannel/map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
astore 16
L109:
iconst_0
istore 3
iconst_0
istore 5
L110:
aload 16
invokevirtual java/nio/MappedByteBuffer/hasRemaining()Z
ifeq L138
L111:
iload 5
istore 4
L113:
getstatic com/chelpus/root/utils/runpatchads/createAPK Z
ifne L116
L114:
iload 5
istore 4
L115:
aload 16
invokevirtual java/nio/MappedByteBuffer/position()I
iload 5
isub
ldc_w 149999
if_icmple L116
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Progress size:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 16
invokevirtual java/nio/MappedByteBuffer/position()I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
aload 16
invokevirtual java/nio/MappedByteBuffer/position()I
istore 4
L116:
aload 16
invokevirtual java/nio/MappedByteBuffer/position()I
istore 8
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
istore 2
L117:
iconst_0
istore 5
iload 3
istore 6
L118:
iload 5
aload 15
arraylength
if_icmpge L179
L119:
aload 15
iload 5
aaload
astore 17
L120:
aload 16
iload 8
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L121:
iload 6
istore 3
L122:
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
ifeq L142
L123:
iload 6
istore 3
iload 5
iconst_2
if_icmpne L142
iload 6
iconst_1
iadd
istore 7
iload 7
sipush 574
if_icmpge L155
iload 7
istore 3
L124:
iload 2
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_0
baload
if_icmpne L141
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iconst_0
iaload
ifne L125
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iconst_0
iload 2
bastore
L125:
iconst_1
istore 6
L126:
aload 16
iload 8
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
L127:
iload 1
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iload 6
baload
if_icmpeq L130
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 6
iaload
iconst_1
if_icmpeq L130
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 6
iaload
bipush 20
if_icmpeq L130
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 6
iaload
bipush 21
if_icmpeq L130
L128:
iload 7
istore 3
L129:
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 6
iaload
bipush 23
if_icmpne L141
L130:
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iload 6
iaload
ifne L131
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iload 6
iload 1
bastore
L131:
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iload 6
iaload
bipush 20
if_icmpne L132
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iload 6
iload 1
bipush 15
iand
i2b
bastore
L132:
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iload 6
iaload
bipush 21
if_icmpne L133
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iload 6
iload 1
bipush 15
iand
bipush 16
iadd
i2b
bastore
L133:
iload 6
iconst_1
iadd
istore 6
L134:
iload 6
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
arraylength
if_icmpne L153
aload 16
iload 8
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 16
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
invokevirtual java/nio/MappedByteBuffer/put([B)Ljava/nio/ByteBuffer;
pop
aload 16
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/resultText Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
getstatic com/chelpus/root/utils/runpatchads/print Ljava/io/PrintStream;
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/resultText Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 17
iconst_1
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/result Z
aload 17
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
aload 12
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 18
L135:
aload 18
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L180
aload 18
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
astore 19
aload 19
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L135
aload 19
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
L136:
goto L135
L112:
astore 15
L137:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 15
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
L138:
aload 14
invokevirtual java/nio/channels/FileChannel/close()V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokestatic java/lang/System/currentTimeMillis()J
lload 9
lsub
ldc2_w 1000L
ldiv
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
ldc "Analise Results:"
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
L139:
goto L65
L180:
iconst_0
istore 3
L140:
aload 16
iload 8
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L141:
aload 16
iload 8
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L142:
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
ifne L181
iload 2
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_0
baload
if_icmpne L181
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/pattern Z
ifeq L181
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iconst_0
iaload
ifne L143
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iconst_0
iload 2
bastore
L143:
iconst_1
istore 6
L144:
aload 16
iload 8
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
L145:
iload 1
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iload 6
baload
if_icmpeq L146
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 6
iaload
iconst_1
if_icmpeq L146
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 6
iaload
bipush 20
if_icmpeq L146
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 6
iaload
bipush 21
if_icmpeq L146
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 6
iaload
bipush 23
if_icmpne L162
L146:
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iload 6
iaload
ifne L147
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iload 6
iload 1
bastore
L147:
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iload 6
iaload
bipush 20
if_icmpne L148
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iload 6
iload 1
bipush 15
iand
i2b
bastore
L148:
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iload 6
iaload
bipush 21
if_icmpne L149
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iload 6
iload 1
bipush 15
iand
bipush 16
iadd
i2b
bastore
L149:
iload 6
iconst_1
iadd
istore 6
L150:
iload 6
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
arraylength
if_icmpne L160
aload 16
iload 8
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 16
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
invokevirtual java/nio/MappedByteBuffer/put([B)Ljava/nio/ByteBuffer;
pop
aload 16
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/resultText Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
getstatic com/chelpus/root/utils/runpatchads/print Ljava/io/PrintStream;
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/resultText Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 17
iconst_1
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/result Z
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L162
aload 17
iconst_1
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
aload 12
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 18
L151:
aload 18
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L162
aload 18
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
astore 19
aload 19
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L151
aload 19
iconst_1
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
L152:
goto L151
L153:
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
L154:
goto L127
L155:
aload 17
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
aload 12
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 18
L156:
aload 18
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L182
aload 18
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
astore 19
aload 19
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L156
aload 19
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
L157:
goto L156
L182:
iconst_0
istore 3
L158:
aload 16
iload 8
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L159:
goto L142
L160:
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
L161:
goto L145
L162:
aload 16
iload 8
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L163:
goto L181
L179:
iload 4
istore 5
iload 6
istore 3
iconst_0
ifne L110
L164:
aload 16
iload 8
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L165:
iload 4
istore 5
iload 6
istore 3
goto L110
L174:
getstatic com/chelpus/root/utils/runpatchads/createAPK Z
ifne L183
aload 0
iconst_3
aaload
getstatic com/chelpus/root/utils/runpatchads/classesFiles Ljava/util/ArrayList;
aload 0
iconst_2
aaload
getstatic com/chelpus/root/utils/runpatchads/uid Ljava/lang/String;
aload 0
iconst_2
aaload
getstatic com/chelpus/root/utils/runpatchads/uid Ljava/lang/String;
invokestatic com/chelpus/Utils/getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokestatic com/chelpus/Utils/create_ODEX_root(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
istore 3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chelpus_return_"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 3
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
iload 3
ifne L183
getstatic com/chelpus/root/utils/runpatchads/ART Z
ifne L183
aload 0
iconst_1
aaload
aload 0
iconst_2
aaload
aload 0
iconst_2
aaload
iconst_1
invokestatic com/chelpus/Utils/getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;
getstatic com/chelpus/root/utils/runpatchads/uid Ljava/lang/String;
aload 0
iconst_3
aaload
invokestatic com/chelpus/Utils/afterPatch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
L183:
getstatic com/chelpus/root/utils/runpatchads/createAPK Z
ifne L184
invokestatic com/chelpus/Utils/exitFromRootJava()V
L184:
aload 11
getfield com/android/vending/billing/InAppBillingService/LUCK/LogOutputStream/allresult Ljava/lang/String;
putstatic com/chelpus/root/utils/runpatchads/result Ljava/lang/String;
return
L29:
astore 13
goto L27
L28:
astore 13
goto L27
L8:
astore 13
goto L26
L176:
iconst_1
istore 3
goto L68
L181:
iload 5
iconst_1
iadd
istore 5
iload 3
istore 6
goto L118
.limit locals 20
.limit stack 8
.end method

.method public static unzipART(Ljava/io/File;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L4 to L5 using L2
.catch java/lang/Exception from L6 to L7 using L2
.catch net/lingala/zip4j/exception/ZipException from L8 to L9 using L10
.catch java/lang/Exception from L8 to L9 using L11
.catch java/lang/Exception from L12 to L13 using L2
.catch java/lang/Exception from L13 to L14 using L2
.catch java/lang/Exception from L14 to L15 using L2
.catch java/lang/Exception from L16 to L17 using L2
.catch java/lang/Exception from L18 to L19 using L2
.catch java/lang/Exception from L20 to L21 using L2
.catch java/lang/Exception from L22 to L23 using L2
iconst_0
istore 1
L0:
new java/io/FileInputStream
dup
aload 0
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
astore 4
new java/util/zip/ZipInputStream
dup
aload 4
invokespecial java/util/zip/ZipInputStream/<init>(Ljava/io/InputStream;)V
astore 5
aload 5
invokevirtual java/util/zip/ZipInputStream/getNextEntry()Ljava/util/zip/ZipEntry;
astore 3
L1:
aload 3
ifnull L20
iconst_1
ifeq L20
L3:
aload 3
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
astore 3
aload 3
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "classes"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L13
aload 3
ldc ".dex"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L13
aload 3
ldc "/"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L13
new java/io/FileOutputStream
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchads/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
astore 6
sipush 2048
newarray byte
astore 7
L4:
aload 5
aload 7
invokevirtual java/util/zip/ZipInputStream/read([B)I
istore 2
L5:
iload 2
iconst_m1
if_icmpeq L12
L6:
aload 6
aload 7
iconst_0
iload 2
invokevirtual java/io/FileOutputStream/write([BII)V
L7:
goto L4
L2:
astore 3
L8:
new net/lingala/zip4j/core/ZipFile
dup
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/<init>(Ljava/io/File;)V
astore 0
aload 0
ldc "classes.dex"
getstatic com/chelpus/root/utils/runpatchads/sddir Ljava/lang/String;
invokevirtual net/lingala/zip4j/core/ZipFile/extractFile(Ljava/lang/String;Ljava/lang/String;)V
getstatic com/chelpus/root/utils/runpatchads/classesFiles Ljava/util/ArrayList;
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchads/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "classes.dex"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchads/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "classes.dex"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
aload 0
ldc "AndroidManifest.xml"
getstatic com/chelpus/root/utils/runpatchads/sddir Ljava/lang/String;
invokevirtual net/lingala/zip4j/core/ZipFile/extractFile(Ljava/lang/String;Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchads/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "AndroidManifest.xml"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L9:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception e"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
return
L12:
aload 5
invokevirtual java/util/zip/ZipInputStream/closeEntry()V
aload 6
invokevirtual java/io/FileOutputStream/close()V
getstatic com/chelpus/root/utils/runpatchads/classesFiles Ljava/util/ArrayList;
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchads/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchads/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L13:
aload 3
ldc "AndroidManifest.xml"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L24
new java/io/FileOutputStream
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchads/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "AndroidManifest.xml"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
astore 3
sipush 2048
newarray byte
astore 6
L14:
aload 5
aload 6
invokevirtual java/util/zip/ZipInputStream/read([B)I
istore 1
L15:
iload 1
iconst_m1
if_icmpeq L18
L16:
aload 3
aload 6
iconst_0
iload 1
invokevirtual java/io/FileOutputStream/write([BII)V
L17:
goto L14
L18:
aload 5
invokevirtual java/util/zip/ZipInputStream/closeEntry()V
aload 3
invokevirtual java/io/FileOutputStream/close()V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchads/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "AndroidManifest.xml"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L19:
iconst_1
istore 1
goto L24
L20:
aload 5
invokevirtual java/util/zip/ZipInputStream/close()V
aload 4
invokevirtual java/io/FileInputStream/close()V
L21:
return
L22:
aload 5
invokevirtual java/util/zip/ZipInputStream/getNextEntry()Ljava/util/zip/ZipEntry;
astore 3
L23:
goto L1
L10:
astore 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Error classes.dex decompress! "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception e1"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
goto L9
L11:
astore 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Error classes.dex decompress! "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception e1"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
goto L9
L24:
iconst_0
ifeq L22
iload 1
ifeq L22
goto L20
.limit locals 8
.limit stack 5
.end method

.method public static unzipSD(Ljava/io/File;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
.catch java/lang/Exception from L4 to L5 using L2
.catch java/lang/Exception from L5 to L6 using L2
.catch java/lang/Exception from L7 to L8 using L2
.catch net/lingala/zip4j/exception/ZipException from L9 to L10 using L11
.catch java/lang/Exception from L9 to L10 using L12
.catch java/lang/Exception from L13 to L14 using L2
.catch java/lang/Exception from L15 to L16 using L2
L0:
new java/io/FileInputStream
dup
aload 0
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
astore 2
new java/util/zip/ZipInputStream
dup
aload 2
invokespecial java/util/zip/ZipInputStream/<init>(Ljava/io/InputStream;)V
astore 3
L1:
aload 3
invokevirtual java/util/zip/ZipInputStream/getNextEntry()Ljava/util/zip/ZipEntry;
astore 4
L3:
aload 4
ifnull L15
L4:
aload 4
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "classes"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L1
aload 4
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
ldc ".dex"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L1
aload 4
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
ldc "/"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L1
new java/io/FileOutputStream
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchads/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
astore 5
sipush 1024
newarray byte
astore 6
L5:
aload 3
aload 6
invokevirtual java/util/zip/ZipInputStream/read([B)I
istore 1
L6:
iload 1
iconst_m1
if_icmpeq L13
L7:
aload 5
aload 6
iconst_0
iload 1
invokevirtual java/io/FileOutputStream/write([BII)V
L8:
goto L5
L2:
astore 2
L9:
new net/lingala/zip4j/core/ZipFile
dup
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/<init>(Ljava/io/File;)V
ldc "classes.dex"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchads/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual net/lingala/zip4j/core/ZipFile/extractFile(Ljava/lang/String;Ljava/lang/String;)V
getstatic com/chelpus/root/utils/runpatchads/classesFiles Ljava/util/ArrayList;
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchads/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "classes.dex"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L10:
return
L13:
aload 3
invokevirtual java/util/zip/ZipInputStream/closeEntry()V
aload 5
invokevirtual java/io/FileOutputStream/close()V
getstatic com/chelpus/root/utils/runpatchads/classesFiles Ljava/util/ArrayList;
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchads/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L14:
goto L1
L15:
aload 3
invokevirtual java/util/zip/ZipInputStream/close()V
aload 2
invokevirtual java/io/FileInputStream/close()V
L16:
return
L11:
astore 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Error classes.dex decompress! "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception e1"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
return
L12:
astore 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Error classes.dex decompress! "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception e1"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
return
.limit locals 7
.limit stack 5
.end method
