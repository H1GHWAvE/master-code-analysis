.bytecode 50.0
.class public synchronized com/chelpus/root/utils/install_to_system
.super java/lang/Object
.inner class static final inner com/chelpus/root/utils/install_to_system$1
.inner class public static Decompress inner com/chelpus/root/utils/install_to_system$Decompress outer com/chelpus/root/utils/install_to_system

.field public static 'CPU_ABI' Ljava/lang/String;

.field public static 'CPU_ABI2' Ljava/lang/String;

.field public static 'appfile' Ljava/lang/String;

.field public static 'datadir' Ljava/lang/String;

.field public static 'pkgName' Ljava/lang/String;

.field public static 'toolsfiles' Ljava/lang/String;

.method static <clinit>()V
ldc "/sdcard/app.apk"
putstatic com/chelpus/root/utils/install_to_system/appfile Ljava/lang/String;
ldc "/data/data/"
putstatic com/chelpus/root/utils/install_to_system/datadir Ljava/lang/String;
ldc ""
putstatic com/chelpus/root/utils/install_to_system/toolsfiles Ljava/lang/String;
ldc ""
putstatic com/chelpus/root/utils/install_to_system/pkgName Ljava/lang/String;
ldc ""
putstatic com/chelpus/root/utils/install_to_system/CPU_ABI Ljava/lang/String;
ldc ""
putstatic com/chelpus/root/utils/install_to_system/CPU_ABI2 Ljava/lang/String;
return
.limit locals 0
.limit stack 1
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static getLibs(Ljava/io/File;)Ljava/util/ArrayList;
.signature "(Ljava/io/File;)Ljava/util/ArrayList<Ljava/io/File;>;"
.catch java/io/IOException from L0 to L1 using L2
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 5
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/install_to_system/toolsfiles Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/tmp/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 6
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/install_to_system/toolsfiles Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/tmp/lib/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 7
aload 7
invokevirtual java/io/File/exists()Z
ifeq L1
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
astore 8
L0:
aload 8
aload 7
invokevirtual com/chelpus/Utils/deleteFolder(Ljava/io/File;)V
L1:
aload 7
invokevirtual java/io/File/exists()Z
ifne L3
new com/chelpus/root/utils/install_to_system$Decompress
dup
aload 0
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aload 6
invokespecial com/chelpus/root/utils/install_to_system$Decompress/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual com/chelpus/root/utils/install_to_system$Decompress/unzip()V
L3:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/install_to_system/toolsfiles Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/tmp/lib"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
aload 0
invokevirtual java/io/File/exists()Z
ifeq L4
aload 0
invokevirtual java/io/File/listFiles()[Ljava/io/File;
astore 0
aload 0
arraylength
istore 3
iconst_0
istore 1
L5:
iload 1
iload 3
if_icmpge L6
aload 0
iload 1
aaload
astore 6
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher: directory in lib found - "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 6
invokevirtual java/io/File/isDirectory()Z
ifeq L7
aload 6
invokevirtual java/io/File/getName()Ljava/lang/String;
getstatic com/chelpus/root/utils/install_to_system/CPU_ABI Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L7
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher: - "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/install_to_system/CPU_ABI Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 6
invokevirtual java/io/File/listFiles()[Ljava/io/File;
astore 6
aload 6
arraylength
istore 4
iconst_0
istore 2
L8:
iload 2
iload 4
if_icmpge L7
aload 6
iload 2
aaload
astore 7
aload 7
invokevirtual java/io/File/isFile()Z
ifeq L9
aload 7
invokevirtual java/io/File/toString()Ljava/lang/String;
ldc ".so"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L9
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 7
invokevirtual java/io/PrintStream/println(Ljava/lang/Object;)V
aload 5
aload 7
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher: found lib file - "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/install_to_system/CPU_ABI Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 7
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L9:
iload 2
iconst_1
iadd
istore 2
goto L8
L2:
astore 8
aload 8
invokevirtual java/io/IOException/printStackTrace()V
goto L1
L7:
iload 1
iconst_1
iadd
istore 1
goto L5
L6:
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 5
invokevirtual java/util/ArrayList/size()I
invokevirtual java/io/PrintStream/println(I)V
aload 5
invokevirtual java/util/ArrayList/size()I
ifne L4
aload 0
arraylength
istore 3
iconst_0
istore 1
L10:
iload 1
iload 3
if_icmpge L4
aload 0
iload 1
aaload
astore 6
aload 6
invokevirtual java/io/File/isDirectory()Z
ifeq L11
aload 6
invokevirtual java/io/File/getName()Ljava/lang/String;
getstatic com/chelpus/root/utils/install_to_system/CPU_ABI2 Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L11
aload 6
invokevirtual java/io/File/listFiles()[Ljava/io/File;
astore 6
aload 6
arraylength
istore 4
iconst_0
istore 2
L12:
iload 2
iload 4
if_icmpge L11
aload 6
iload 2
aaload
astore 7
aload 7
invokevirtual java/io/File/isFile()Z
ifeq L13
aload 7
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc ".so"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L13
aload 5
aload 7
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher: found lib file - "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/install_to_system/CPU_ABI2 Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 7
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L13:
iload 2
iconst_1
iadd
istore 2
goto L12
L11:
iload 1
iconst_1
iadd
istore 1
goto L10
L4:
aload 5
areturn
.limit locals 9
.limit stack 4
.end method

.method public static main([Ljava/lang/String;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
.catch java/lang/Exception from L6 to L7 using L5
.catch java/lang/Exception from L8 to L9 using L2
.catch java/lang/Exception from L10 to L11 using L2
.catch java/lang/Exception from L12 to L13 using L2
.catch java/lang/Exception from L14 to L15 using L2
.catch java/lang/Exception from L16 to L17 using L2
.catch java/lang/Exception from L17 to L18 using L2
.catch java/lang/Exception from L18 to L19 using L2
.catch java/lang/Exception from L19 to L20 using L2
.catch java/lang/Exception from L20 to L21 using L2
.catch java/lang/Exception from L22 to L23 using L2
.catch java/lang/Exception from L24 to L25 using L2
.catch java/lang/Exception from L25 to L26 using L27
.catch java/lang/Exception from L26 to L28 using L27
.catch java/lang/Exception from L28 to L29 using L2
.catch java/lang/Exception from L30 to L31 using L2
.catch java/lang/Exception from L32 to L33 using L2
.catch java/lang/Exception from L33 to L34 using L2
.catch java/lang/Exception from L35 to L36 using L2
.catch java/lang/Exception from L37 to L38 using L39
.catch java/lang/Exception from L38 to L40 using L39
.catch java/lang/Exception from L40 to L41 using L2
.catch java/lang/Exception from L41 to L42 using L2
.catch java/io/IOException from L42 to L43 using L44
.catch java/lang/Exception from L42 to L43 using L2
.catch java/lang/Exception from L43 to L45 using L2
.catch java/lang/Exception from L46 to L47 using L2
.catch java/lang/Exception from L48 to L49 using L2
.catch java/lang/Exception from L49 to L50 using L2
.catch java/lang/Exception from L51 to L52 using L2
.catch java/lang/Exception from L53 to L54 using L2
.catch java/lang/Exception from L55 to L56 using L2
.catch java/lang/Exception from L56 to L57 using L2
.catch java/lang/Exception from L58 to L59 using L2
.catch java/lang/Exception from L60 to L61 using L2
iconst_0
istore 2
L0:
new com/chelpus/root/utils/install_to_system$1
dup
invokespecial com/chelpus/root/utils/install_to_system$1/<init>()V
invokestatic com/chelpus/Utils/startRootJava(Ljava/lang/Object;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
iconst_1
aaload
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
iconst_1
aaload
putstatic com/chelpus/root/utils/install_to_system/appfile Ljava/lang/String;
aload 0
iconst_2
aaload
putstatic com/chelpus/root/utils/install_to_system/datadir Ljava/lang/String;
aload 0
iconst_3
aaload
putstatic com/chelpus/root/utils/install_to_system/toolsfiles Ljava/lang/String;
aload 0
iconst_0
aaload
putstatic com/chelpus/root/utils/install_to_system/pkgName Ljava/lang/String;
aload 0
iconst_4
aaload
putstatic com/chelpus/root/utils/install_to_system/CPU_ABI Ljava/lang/String;
aload 0
iconst_5
aaload
putstatic com/chelpus/root/utils/install_to_system/CPU_ABI2 Ljava/lang/String;
L1:
ldc "/system/app/"
astore 3
aload 3
astore 0
L3:
new java/io/File
dup
ldc "/system/priv-app"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L62
L4:
aload 3
astore 0
L6:
new java/io/File
dup
ldc "/system/priv-app"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/list()[Ljava/lang/String;
ifnull L62
L7:
ldc "/system/priv-app/"
astore 0
L62:
aload 0
astore 3
L8:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/api I
bipush 21
if_icmplt L12
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/system/priv-app/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/install_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
L9:
aload 0
astore 3
L10:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifne L12
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/mkdirs()Z
pop
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "755"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/system/priv-app/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/install_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L11:
aload 0
astore 3
L12:
new java/io/File
dup
getstatic com/chelpus/root/utils/install_to_system/appfile Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 5
aload 5
invokestatic com/chelpus/root/utils/install_to_system/getLibs(Ljava/io/File;)Ljava/util/ArrayList;
astore 6
L13:
ldc "/system/lib/"
astore 4
aload 4
astore 0
L14:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/api I
bipush 21
if_icmplt L21
getstatic com/chelpus/root/utils/install_to_system/CPU_ABI Ljava/lang/String;
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "x86"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L17
L15:
aload 4
astore 0
L16:
getstatic com/chelpus/root/utils/install_to_system/CPU_ABI2 Ljava/lang/String;
ldc "x86"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L18
L17:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/system/priv-app/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/install_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/lib/x86/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
L18:
getstatic com/chelpus/root/utils/install_to_system/CPU_ABI Ljava/lang/String;
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "arm"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L20
L19:
getstatic com/chelpus/root/utils/install_to_system/CPU_ABI2 Ljava/lang/String;
ldc "arm"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L21
L20:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/system/priv-app/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/install_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/lib/arm/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
L21:
iload 2
istore 1
L22:
aload 6
invokevirtual java/util/ArrayList/size()I
ifle L63
aload 6
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 4
L23:
iload 2
istore 1
L24:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L63
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
astore 7
L25:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifne L26
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/mkdirs()Z
pop
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "755"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/system/priv-app/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/install_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/lib"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L26:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/system/priv-app/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/install_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 0
ldc "755"
invokestatic com/chelpus/Utils/setPermissionDir(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
aload 7
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 7
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokestatic com/chelpus/Utils/copyFile(Ljava/io/File;Ljava/io/File;)V
L28:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chmod 0644 "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 7
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/root/utils/install_to_system/run_all(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chown 0.0 "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 7
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/root/utils/install_to_system/run_all(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chown 0:0 "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 7
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/root/utils/install_to_system/run_all(Ljava/lang/String;)V
aload 7
invokevirtual java/io/File/length()J
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 7
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
lcmp
ifne L48
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher: copy lib "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 7
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L29:
goto L23
L2:
astore 0
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher Error move to System: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L64:
invokestatic com/chelpus/Utils/exitFromRootJava()V
return
L5:
astore 0
L30:
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L31:
aload 3
astore 0
goto L62
L27:
astore 4
L32:
aload 6
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 4
L33:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L35
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
astore 7
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 7
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L34:
goto L33
L35:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "In /system space not found!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L36:
iconst_1
istore 1
L63:
iload 1
ifne L58
L37:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/install_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".odex"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L38
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/install_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".odex"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L38:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/install_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 4
getstatic com/chelpus/root/utils/install_to_system/appfile Ljava/lang/String;
astore 7
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
aload 7
aload 4
invokestatic com/chelpus/Utils/dalvikvm_copyFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
ifne L40
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "In /system space not found!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/install_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L40:
aload 5
invokevirtual java/io/File/length()J
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/install_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
lcmp
ifne L55
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chmod 0644 "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/install_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/root/utils/install_to_system/run_all(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chown 0.0 "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/install_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/root/utils/install_to_system/run_all(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chown 0:0 "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/install_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/root/utils/install_to_system/run_all(Ljava/lang/String;)V
L41:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/install_to_system/toolsfiles Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/tmp"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
aload 0
invokevirtual java/io/File/exists()Z
ifeq L43
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
astore 4
L42:
aload 4
aload 0
invokevirtual com/chelpus/Utils/deleteFolder(Ljava/io/File;)V
aload 4
new java/io/File
dup
getstatic com/chelpus/root/utils/install_to_system/datadir Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual com/chelpus/Utils/deleteFolder(Ljava/io/File;)V
L43:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/install_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/getFileDalvikCache(Ljava/lang/String;)Ljava/io/File;
astore 0
L45:
aload 0
ifnull L64
L46:
aload 0
invokevirtual java/io/File/delete()Z
pop
L47:
goto L64
L48:
aload 6
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 4
L49:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L51
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
astore 7
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 7
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L50:
goto L49
L51:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "In /system space not found!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L52:
iconst_1
istore 1
goto L63
L39:
astore 4
L53:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "In /system space not found!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/install_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
aload 4
invokevirtual java/lang/Exception/printStackTrace()V
L54:
goto L40
L55:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/install_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "In /system space not found!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 6
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 4
L56:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L41
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
astore 5
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L57:
goto L56
L58:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/install_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L59:
goto L41
L44:
astore 0
L60:
aload 0
invokevirtual java/io/IOException/printStackTrace()V
L61:
goto L43
.limit locals 8
.limit stack 6
.end method

.method private static run_all(Ljava/lang/String;)V
.catch java/io/UnsupportedEncodingException from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
.catch java/lang/Exception from L4 to L6 using L7
.catch java/lang/Exception from L6 to L8 using L9
.catch java/lang/Exception from L8 to L10 using L11
.catch java/lang/Exception from L10 to L12 using L13
L0:
new java/lang/String
dup
aload 0
invokevirtual java/lang/String/getBytes()[B
ldc "ISO-8859-1"
invokespecial java/lang/String/<init>([BLjava/lang/String;)V
astore 1
L1:
aload 1
astore 0
L3:
invokestatic java/lang/Runtime/getRuntime()Ljava/lang/Runtime;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/Runtime/exec(Ljava/lang/String;)Ljava/lang/Process;
astore 1
aload 1
invokevirtual java/lang/Process/waitFor()I
pop
aload 1
invokevirtual java/lang/Process/destroy()V
L4:
invokestatic java/lang/Runtime/getRuntime()Ljava/lang/Runtime;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "toolbox "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/Runtime/exec(Ljava/lang/String;)Ljava/lang/Process;
astore 1
aload 1
invokevirtual java/lang/Process/waitFor()I
pop
aload 1
invokevirtual java/lang/Process/destroy()V
L6:
invokestatic java/lang/Runtime/getRuntime()Ljava/lang/Runtime;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/system/bin/failsafe/toolbox "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/Runtime/exec(Ljava/lang/String;)Ljava/lang/Process;
astore 1
aload 1
invokevirtual java/lang/Process/waitFor()I
pop
aload 1
invokevirtual java/lang/Process/destroy()V
L8:
invokestatic java/lang/Runtime/getRuntime()Ljava/lang/Runtime;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "busybox "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/Runtime/exec(Ljava/lang/String;)Ljava/lang/Process;
astore 1
aload 1
invokevirtual java/lang/Process/waitFor()I
pop
aload 1
invokevirtual java/lang/Process/destroy()V
L10:
invokestatic java/lang/Runtime/getRuntime()Ljava/lang/Runtime;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/install_to_system/toolsfiles Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/busybox "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/Runtime/exec(Ljava/lang/String;)Ljava/lang/Process;
astore 0
aload 0
invokevirtual java/lang/Process/waitFor()I
pop
aload 0
invokevirtual java/lang/Process/destroy()V
L12:
return
L2:
astore 1
aload 1
invokevirtual java/io/UnsupportedEncodingException/printStackTrace()V
goto L3
L13:
astore 0
return
L11:
astore 1
goto L10
L9:
astore 1
goto L8
L7:
astore 1
goto L6
L5:
astore 1
goto L4
.limit locals 2
.limit stack 4
.end method
