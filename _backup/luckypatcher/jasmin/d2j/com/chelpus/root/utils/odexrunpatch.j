.bytecode 50.0
.class public synchronized com/chelpus/root/utils/odexrunpatch
.super java/lang/Object
.inner class static final inner com/chelpus/root/utils/odexrunpatch$1

.field public static 'ART' Z

.field private static 'amazon' Z

.field public static 'appdir' Ljava/lang/String;

.field public static 'classesFiles' Ljava/util/ArrayList; signature "Ljava/util/ArrayList<Ljava/io/File;>;"

.field public static 'copyDC' Z

.field private static 'createAPK' Z

.field public static 'crkapk' Ljava/io/File;

.field private static 'dependencies' Z

.field public static 'dir' Ljava/lang/String;

.field public static 'dirapp' Ljava/lang/String;

.field public static 'filestopatch' Ljava/util/ArrayList; signature "Ljava/util/ArrayList<Ljava/io/File;>;"

.field private static 'pattern1' Z

.field private static 'pattern2' Z

.field private static 'pattern3' Z

.field private static 'pattern4' Z

.field private static 'pattern5' Z

.field private static 'pattern6' Z

.field public static 'print' Ljava/io/PrintStream;

.field public static 'result' Ljava/lang/String;

.field private static 'samsung' Z

.field public static 'sddir' Ljava/lang/String;

.field public static 'system' Z

.field public static 'uid' Ljava/lang/String;

.method static <clinit>()V
iconst_0
putstatic com/chelpus/root/utils/odexrunpatch/createAPK Z
iconst_1
putstatic com/chelpus/root/utils/odexrunpatch/pattern1 Z
iconst_1
putstatic com/chelpus/root/utils/odexrunpatch/pattern2 Z
iconst_1
putstatic com/chelpus/root/utils/odexrunpatch/pattern3 Z
iconst_1
putstatic com/chelpus/root/utils/odexrunpatch/pattern4 Z
iconst_1
putstatic com/chelpus/root/utils/odexrunpatch/pattern5 Z
iconst_1
putstatic com/chelpus/root/utils/odexrunpatch/pattern6 Z
iconst_1
putstatic com/chelpus/root/utils/odexrunpatch/dependencies Z
iconst_1
putstatic com/chelpus/root/utils/odexrunpatch/amazon Z
iconst_1
putstatic com/chelpus/root/utils/odexrunpatch/samsung Z
ldc "/sdcard/"
putstatic com/chelpus/root/utils/odexrunpatch/dir Ljava/lang/String;
ldc ""
putstatic com/chelpus/root/utils/odexrunpatch/uid Ljava/lang/String;
aconst_null
putstatic com/chelpus/root/utils/odexrunpatch/filestopatch Ljava/util/ArrayList;
iconst_0
putstatic com/chelpus/root/utils/odexrunpatch/system Z
iconst_0
putstatic com/chelpus/root/utils/odexrunpatch/copyDC Z
iconst_0
putstatic com/chelpus/root/utils/odexrunpatch/ART Z
ldc "/data/app/"
putstatic com/chelpus/root/utils/odexrunpatch/dirapp Ljava/lang/String;
ldc "/sdcard/"
putstatic com/chelpus/root/utils/odexrunpatch/sddir Ljava/lang/String;
ldc "/sdcard/"
putstatic com/chelpus/root/utils/odexrunpatch/appdir Ljava/lang/String;
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putstatic com/chelpus/root/utils/odexrunpatch/classesFiles Ljava/util/ArrayList;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static byteverify(Ljava/nio/MappedByteBuffer;IB[B[B[B[BLjava/lang/String;Z)Z
iload 2
aload 3
iconst_0
baload
if_icmpne L0
iload 8
ifeq L0
aload 6
iconst_0
baload
ifne L1
aload 5
iconst_0
iload 2
bastore
L1:
iconst_1
istore 9
aload 0
iload 1
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 0
invokevirtual java/nio/MappedByteBuffer/get()B
istore 2
L2:
iload 2
aload 3
iload 9
baload
if_icmpeq L3
aload 4
iload 9
baload
iconst_1
if_icmpne L4
L3:
aload 6
iload 9
baload
ifne L5
aload 5
iload 9
iload 2
bastore
L5:
iload 9
iconst_1
iadd
istore 9
iload 9
aload 3
arraylength
if_icmpne L6
aload 0
iload 1
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 0
aload 5
invokevirtual java/nio/MappedByteBuffer/put([B)Ljava/nio/ByteBuffer;
pop
aload 0
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 7
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
iconst_1
ireturn
L6:
aload 0
invokevirtual java/nio/MappedByteBuffer/get()B
istore 2
goto L2
L4:
aload 0
iload 1
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L0:
iconst_0
ireturn
.limit locals 10
.limit stack 3
.end method

.method public static clearTemp()V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L2
.catch java/lang/Exception from L6 to L7 using L2
L0:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/odexrunpatch/dir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/AndroidManifest.xml"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
aload 0
invokevirtual java/io/File/exists()Z
ifeq L1
aload 0
invokevirtual java/io/File/delete()Z
pop
L1:
getstatic com/chelpus/root/utils/odexrunpatch/classesFiles Ljava/util/ArrayList;
ifnull L5
getstatic com/chelpus/root/utils/odexrunpatch/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifle L5
getstatic com/chelpus/root/utils/odexrunpatch/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 0
L3:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L5
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
astore 1
aload 1
invokevirtual java/io/File/exists()Z
ifeq L3
aload 1
invokevirtual java/io/File/delete()Z
pop
L4:
goto L3
L2:
astore 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
L8:
return
L5:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/odexrunpatch/dir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/classes.dex"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
aload 0
invokevirtual java/io/File/exists()Z
ifeq L6
aload 0
invokevirtual java/io/File/delete()Z
pop
L6:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/odexrunpatch/dir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/classes.dex.apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
aload 0
invokevirtual java/io/File/exists()Z
ifeq L8
aload 0
invokevirtual java/io/File/delete()Z
pop
L7:
return
.limit locals 2
.limit stack 4
.end method

.method public static clearTempSD()V
.catch java/lang/Exception from L0 to L1 using L2
L0:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/odexrunpatch/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/classes.dex.apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
aload 0
invokevirtual java/io/File/exists()Z
ifeq L1
aload 0
invokevirtual java/io/File/delete()Z
pop
L1:
return
L2:
astore 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
return
.limit locals 1
.limit stack 4
.end method

.method public static main([Ljava/lang/String;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/NullPointerException from L5 to L6 using L7
.catch java/lang/Exception from L5 to L6 using L8
.catch java/lang/NullPointerException from L6 to L9 using L7
.catch java/lang/Exception from L6 to L9 using L8
.catch java/lang/NullPointerException from L9 to L10 using L7
.catch java/lang/Exception from L9 to L10 using L8
.catch java/lang/NullPointerException from L10 to L11 using L7
.catch java/lang/Exception from L10 to L11 using L8
.catch java/lang/NullPointerException from L11 to L12 using L7
.catch java/lang/Exception from L11 to L12 using L8
.catch java/lang/NullPointerException from L12 to L13 using L7
.catch java/lang/Exception from L12 to L13 using L8
.catch java/lang/NullPointerException from L13 to L14 using L7
.catch java/lang/Exception from L13 to L14 using L8
.catch java/lang/NullPointerException from L14 to L15 using L7
.catch java/lang/Exception from L14 to L15 using L8
.catch java/lang/NullPointerException from L15 to L16 using L7
.catch java/lang/Exception from L15 to L16 using L8
.catch java/lang/NullPointerException from L16 to L17 using L7
.catch java/lang/Exception from L16 to L17 using L8
.catch java/lang/NullPointerException from L18 to L19 using L7
.catch java/lang/Exception from L18 to L19 using L8
.catch java/lang/NullPointerException from L20 to L21 using L7
.catch java/lang/Exception from L20 to L21 using L8
.catch java/lang/NullPointerException from L21 to L22 using L7
.catch java/lang/Exception from L21 to L22 using L8
.catch java/lang/NullPointerException from L22 to L23 using L24
.catch java/lang/Exception from L22 to L23 using L25
.catch java/io/FileNotFoundException from L26 to L27 using L28
.catch java/lang/Exception from L26 to L27 using L29
.catch java/io/FileNotFoundException from L27 to L30 using L28
.catch java/lang/Exception from L27 to L30 using L29
.catch java/io/FileNotFoundException from L30 to L31 using L28
.catch java/lang/Exception from L30 to L31 using L29
.catch java/io/FileNotFoundException from L31 to L32 using L28
.catch java/lang/Exception from L31 to L32 using L29
.catch java/io/FileNotFoundException from L33 to L34 using L28
.catch java/lang/Exception from L33 to L34 using L29
.catch java/io/FileNotFoundException from L34 to L28 using L28
.catch java/lang/Exception from L34 to L28 using L29
.catch java/io/FileNotFoundException from L35 to L36 using L28
.catch java/lang/Exception from L35 to L36 using L29
.catch java/io/FileNotFoundException from L36 to L29 using L28
.catch java/lang/Exception from L36 to L29 using L29
.catch java/io/FileNotFoundException from L37 to L38 using L28
.catch java/lang/Exception from L37 to L38 using L29
.catch java/io/FileNotFoundException from L39 to L40 using L28
.catch java/lang/Exception from L39 to L40 using L29
.catch java/io/FileNotFoundException from L40 to L41 using L28
.catch java/lang/Exception from L40 to L41 using L29
.catch java/io/FileNotFoundException from L41 to L42 using L28
.catch java/lang/Exception from L41 to L42 using L29
.catch java/io/FileNotFoundException from L42 to L43 using L28
.catch java/lang/Exception from L42 to L43 using L29
.catch java/io/FileNotFoundException from L43 to L44 using L28
.catch java/lang/Exception from L43 to L44 using L29
.catch java/io/FileNotFoundException from L45 to L46 using L28
.catch java/lang/Exception from L45 to L46 using L29
.catch java/io/FileNotFoundException from L46 to L47 using L28
.catch java/lang/Exception from L46 to L47 using L29
.catch java/io/FileNotFoundException from L47 to L48 using L28
.catch java/lang/Exception from L47 to L48 using L29
.catch java/io/FileNotFoundException from L48 to L49 using L28
.catch java/lang/Exception from L48 to L49 using L29
.catch java/io/FileNotFoundException from L50 to L51 using L28
.catch java/lang/Exception from L50 to L51 using L29
.catch java/io/FileNotFoundException from L52 to L53 using L28
.catch java/lang/Exception from L52 to L53 using L29
.catch java/io/FileNotFoundException from L53 to L54 using L28
.catch java/lang/Exception from L53 to L54 using L29
.catch java/io/FileNotFoundException from L54 to L55 using L28
.catch java/lang/Exception from L54 to L55 using L29
.catch java/io/FileNotFoundException from L55 to L56 using L28
.catch java/lang/Exception from L55 to L56 using L29
.catch java/io/FileNotFoundException from L56 to L57 using L28
.catch java/lang/Exception from L56 to L57 using L29
.catch java/io/FileNotFoundException from L58 to L59 using L28
.catch java/lang/Exception from L58 to L59 using L29
.catch java/io/FileNotFoundException from L60 to L61 using L28
.catch java/lang/Exception from L60 to L61 using L29
.catch java/io/FileNotFoundException from L61 to L62 using L28
.catch java/lang/Exception from L61 to L62 using L29
.catch java/io/FileNotFoundException from L62 to L63 using L28
.catch java/lang/Exception from L62 to L63 using L29
.catch java/io/FileNotFoundException from L63 to L64 using L28
.catch java/lang/Exception from L63 to L64 using L29
.catch java/io/FileNotFoundException from L64 to L65 using L28
.catch java/lang/Exception from L64 to L65 using L29
.catch java/io/FileNotFoundException from L66 to L67 using L28
.catch java/lang/Exception from L66 to L67 using L29
.catch java/io/FileNotFoundException from L68 to L69 using L28
.catch java/lang/Exception from L68 to L69 using L29
.catch java/io/FileNotFoundException from L70 to L71 using L28
.catch java/lang/Exception from L70 to L71 using L29
.catch java/io/FileNotFoundException from L72 to L73 using L28
.catch java/lang/Exception from L72 to L73 using L29
.catch java/io/FileNotFoundException from L73 to L74 using L28
.catch java/lang/Exception from L73 to L74 using L29
.catch java/io/FileNotFoundException from L74 to L75 using L28
.catch java/lang/Exception from L74 to L75 using L29
.catch java/io/FileNotFoundException from L76 to L77 using L28
.catch java/lang/Exception from L76 to L77 using L29
.catch java/io/FileNotFoundException from L78 to L79 using L28
.catch java/lang/Exception from L78 to L79 using L29
.catch java/io/FileNotFoundException from L79 to L80 using L28
.catch java/lang/Exception from L79 to L80 using L29
.catch java/io/FileNotFoundException from L81 to L82 using L28
.catch java/lang/Exception from L81 to L82 using L29
.catch java/lang/Exception from L83 to L84 using L85
.catch java/io/FileNotFoundException from L83 to L84 using L28
.catch java/lang/Exception from L86 to L87 using L85
.catch java/io/FileNotFoundException from L86 to L87 using L28
.catch java/lang/Exception from L88 to L89 using L85
.catch java/io/FileNotFoundException from L88 to L89 using L28
.catch java/lang/Exception from L89 to L90 using L85
.catch java/io/FileNotFoundException from L89 to L90 using L28
.catch java/lang/Exception from L91 to L92 using L85
.catch java/io/FileNotFoundException from L91 to L92 using L28
.catch java/lang/Exception from L93 to L94 using L85
.catch java/io/FileNotFoundException from L93 to L94 using L28
.catch java/lang/Exception from L95 to L96 using L85
.catch java/io/FileNotFoundException from L95 to L96 using L28
.catch java/lang/Exception from L97 to L98 using L85
.catch java/io/FileNotFoundException from L97 to L98 using L28
.catch java/lang/Exception from L99 to L100 using L85
.catch java/io/FileNotFoundException from L99 to L100 using L28
.catch java/lang/Exception from L100 to L101 using L85
.catch java/io/FileNotFoundException from L100 to L101 using L28
.catch java/lang/Exception from L102 to L103 using L85
.catch java/io/FileNotFoundException from L102 to L103 using L28
.catch java/lang/Exception from L104 to L105 using L85
.catch java/io/FileNotFoundException from L104 to L105 using L28
.catch java/lang/Exception from L106 to L107 using L85
.catch java/io/FileNotFoundException from L106 to L107 using L28
.catch java/lang/Exception from L108 to L109 using L85
.catch java/io/FileNotFoundException from L108 to L109 using L28
.catch java/lang/Exception from L110 to L111 using L85
.catch java/io/FileNotFoundException from L110 to L111 using L28
.catch java/lang/Exception from L111 to L112 using L85
.catch java/io/FileNotFoundException from L111 to L112 using L28
.catch java/lang/Exception from L113 to L114 using L85
.catch java/io/FileNotFoundException from L113 to L114 using L28
.catch java/lang/Exception from L114 to L115 using L85
.catch java/io/FileNotFoundException from L114 to L115 using L28
.catch java/lang/Exception from L115 to L116 using L85
.catch java/io/FileNotFoundException from L115 to L116 using L28
.catch java/lang/Exception from L116 to L117 using L85
.catch java/io/FileNotFoundException from L116 to L117 using L28
.catch java/lang/Exception from L117 to L118 using L85
.catch java/io/FileNotFoundException from L117 to L118 using L28
.catch java/lang/Exception from L118 to L119 using L85
.catch java/io/FileNotFoundException from L118 to L119 using L28
.catch java/lang/Exception from L120 to L121 using L85
.catch java/io/FileNotFoundException from L120 to L121 using L28
.catch java/lang/Exception from L122 to L123 using L85
.catch java/io/FileNotFoundException from L122 to L123 using L28
.catch java/lang/Exception from L124 to L125 using L85
.catch java/io/FileNotFoundException from L124 to L125 using L28
.catch java/lang/Exception from L126 to L127 using L85
.catch java/io/FileNotFoundException from L126 to L127 using L28
.catch java/lang/Exception from L127 to L128 using L85
.catch java/io/FileNotFoundException from L127 to L128 using L28
.catch java/lang/Exception from L128 to L129 using L85
.catch java/io/FileNotFoundException from L128 to L129 using L28
.catch java/io/FileNotFoundException from L130 to L131 using L28
.catch java/lang/Exception from L130 to L131 using L29
.catch java/io/FileNotFoundException from L131 to L132 using L28
.catch java/lang/Exception from L131 to L132 using L29
.catch java/lang/Exception from L133 to L134 using L85
.catch java/io/FileNotFoundException from L133 to L134 using L28
.catch java/lang/Exception from L134 to L135 using L85
.catch java/io/FileNotFoundException from L134 to L135 using L28
.catch java/lang/Exception from L136 to L137 using L85
.catch java/io/FileNotFoundException from L136 to L137 using L28
.catch java/lang/Exception from L138 to L139 using L85
.catch java/io/FileNotFoundException from L138 to L139 using L28
.catch java/lang/Exception from L140 to L141 using L85
.catch java/io/FileNotFoundException from L140 to L141 using L28
.catch java/lang/Exception from L141 to L142 using L85
.catch java/io/FileNotFoundException from L141 to L142 using L28
.catch java/lang/Exception from L143 to L144 using L85
.catch java/io/FileNotFoundException from L143 to L144 using L28
.catch java/lang/Exception from L144 to L145 using L85
.catch java/io/FileNotFoundException from L144 to L145 using L28
.catch java/lang/Exception from L145 to L146 using L85
.catch java/io/FileNotFoundException from L145 to L146 using L28
.catch java/lang/Exception from L146 to L147 using L85
.catch java/io/FileNotFoundException from L146 to L147 using L28
.catch java/lang/Exception from L147 to L148 using L85
.catch java/io/FileNotFoundException from L147 to L148 using L28
.catch java/lang/Exception from L148 to L149 using L85
.catch java/io/FileNotFoundException from L148 to L149 using L28
.catch java/lang/Exception from L150 to L151 using L85
.catch java/io/FileNotFoundException from L150 to L151 using L28
.catch java/lang/Exception from L152 to L153 using L85
.catch java/io/FileNotFoundException from L152 to L153 using L28
.catch java/lang/Exception from L154 to L155 using L85
.catch java/io/FileNotFoundException from L154 to L155 using L28
.catch java/lang/Exception from L156 to L157 using L85
.catch java/io/FileNotFoundException from L156 to L157 using L28
.catch java/lang/Exception from L157 to L158 using L85
.catch java/io/FileNotFoundException from L157 to L158 using L28
.catch java/lang/Exception from L159 to L160 using L85
.catch java/io/FileNotFoundException from L159 to L160 using L28
.catch java/lang/Exception from L161 to L162 using L85
.catch java/io/FileNotFoundException from L161 to L162 using L28
.catch java/lang/Exception from L162 to L163 using L85
.catch java/io/FileNotFoundException from L162 to L163 using L28
.catch java/lang/Exception from L163 to L164 using L85
.catch java/io/FileNotFoundException from L163 to L164 using L28
.catch java/lang/Exception from L165 to L166 using L85
.catch java/io/FileNotFoundException from L165 to L166 using L28
.catch java/lang/Exception from L166 to L167 using L85
.catch java/io/FileNotFoundException from L166 to L167 using L28
.catch java/lang/Exception from L167 to L168 using L85
.catch java/io/FileNotFoundException from L167 to L168 using L28
.catch java/lang/Exception from L169 to L170 using L85
.catch java/io/FileNotFoundException from L169 to L170 using L28
.catch java/lang/Exception from L170 to L171 using L85
.catch java/io/FileNotFoundException from L170 to L171 using L28
.catch java/lang/Exception from L171 to L172 using L85
.catch java/io/FileNotFoundException from L171 to L172 using L28
.catch java/lang/Exception from L172 to L173 using L85
.catch java/io/FileNotFoundException from L172 to L173 using L28
.catch java/lang/Exception from L173 to L174 using L85
.catch java/io/FileNotFoundException from L173 to L174 using L28
.catch java/lang/Exception from L174 to L175 using L85
.catch java/io/FileNotFoundException from L174 to L175 using L28
.catch java/lang/Exception from L175 to L176 using L85
.catch java/io/FileNotFoundException from L175 to L176 using L28
.catch java/lang/Exception from L177 to L178 using L85
.catch java/io/FileNotFoundException from L177 to L178 using L28
.catch java/lang/Exception from L179 to L180 using L85
.catch java/io/FileNotFoundException from L179 to L180 using L28
.catch java/lang/Exception from L181 to L182 using L85
.catch java/io/FileNotFoundException from L181 to L182 using L28
.catch java/lang/Exception from L183 to L184 using L85
.catch java/io/FileNotFoundException from L183 to L184 using L28
.catch java/lang/Exception from L184 to L185 using L85
.catch java/io/FileNotFoundException from L184 to L185 using L28
.catch java/lang/Exception from L186 to L187 using L85
.catch java/io/FileNotFoundException from L186 to L187 using L28
.catch java/lang/Exception from L188 to L189 using L85
.catch java/io/FileNotFoundException from L188 to L189 using L28
.catch java/lang/Exception from L189 to L190 using L85
.catch java/io/FileNotFoundException from L189 to L190 using L28
.catch java/lang/Exception from L190 to L191 using L85
.catch java/io/FileNotFoundException from L190 to L191 using L28
.catch java/lang/Exception from L192 to L193 using L85
.catch java/io/FileNotFoundException from L192 to L193 using L28
.catch java/lang/Exception from L194 to L195 using L85
.catch java/io/FileNotFoundException from L194 to L195 using L28
.catch java/lang/Exception from L196 to L197 using L85
.catch java/io/FileNotFoundException from L196 to L197 using L28
.catch java/lang/Exception from L198 to L199 using L85
.catch java/io/FileNotFoundException from L198 to L199 using L28
.catch java/lang/Exception from L200 to L201 using L85
.catch java/io/FileNotFoundException from L200 to L201 using L28
.catch java/lang/Exception from L202 to L203 using L85
.catch java/io/FileNotFoundException from L202 to L203 using L28
.catch java/lang/Exception from L204 to L205 using L85
.catch java/io/FileNotFoundException from L204 to L205 using L28
.catch java/lang/Exception from L206 to L207 using L85
.catch java/io/FileNotFoundException from L206 to L207 using L28
.catch java/lang/Exception from L208 to L209 using L85
.catch java/io/FileNotFoundException from L208 to L209 using L28
.catch java/lang/Exception from L210 to L211 using L85
.catch java/io/FileNotFoundException from L210 to L211 using L28
.catch java/lang/Exception from L212 to L213 using L85
.catch java/io/FileNotFoundException from L212 to L213 using L28
new com/android/vending/billing/InAppBillingService/LUCK/LogOutputStream
dup
ldc "System.out"
invokespecial com/android/vending/billing/InAppBillingService/LUCK/LogOutputStream/<init>(Ljava/lang/String;)V
astore 20
new java/io/PrintStream
dup
aload 20
invokespecial java/io/PrintStream/<init>(Ljava/io/OutputStream;)V
putstatic com/chelpus/root/utils/odexrunpatch/print Ljava/io/PrintStream;
new com/chelpus/root/utils/odexrunpatch$1
dup
invokespecial com/chelpus/root/utils/odexrunpatch$1/<init>()V
invokestatic com/chelpus/Utils/startRootJava(Ljava/lang/Object;)V
aload 0
iconst_0
aaload
invokestatic com/chelpus/Utils/kill(Ljava/lang/String;)V
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 21
iconst_1
putstatic com/chelpus/root/utils/odexrunpatch/pattern1 Z
iconst_1
putstatic com/chelpus/root/utils/odexrunpatch/pattern2 Z
iconst_1
putstatic com/chelpus/root/utils/odexrunpatch/pattern3 Z
iconst_1
putstatic com/chelpus/root/utils/odexrunpatch/pattern4 Z
iconst_1
putstatic com/chelpus/root/utils/odexrunpatch/pattern5 Z
iconst_1
putstatic com/chelpus/root/utils/odexrunpatch/pattern6 Z
iconst_1
putstatic com/chelpus/root/utils/odexrunpatch/dependencies Z
iconst_1
putstatic com/chelpus/root/utils/odexrunpatch/amazon Z
iconst_1
putstatic com/chelpus/root/utils/odexrunpatch/samsung Z
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putstatic com/chelpus/root/utils/odexrunpatch/filestopatch Ljava/util/ArrayList;
L0:
new java/io/File
dup
aload 0
iconst_3
aaload
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/listFiles()[Ljava/io/File;
astore 22
aload 22
arraylength
istore 7
L1:
iconst_0
istore 6
L214:
iload 6
iload 7
if_icmpge L5
aload 22
iload 6
aaload
astore 23
L3:
aload 23
invokevirtual java/io/File/isFile()Z
ifeq L4
aload 23
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc "busybox"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L4
aload 23
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc "reboot"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L4
aload 23
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc "dalvikvm"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L4
aload 23
invokevirtual java/io/File/delete()Z
pop
L4:
iload 6
iconst_1
iadd
istore 6
goto L214
L2:
astore 22
aload 22
invokevirtual java/lang/Exception/printStackTrace()V
L5:
aload 0
iconst_1
aaload
ldc "pattern1"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L6
iconst_0
putstatic com/chelpus/root/utils/odexrunpatch/pattern1 Z
L6:
aload 0
iconst_1
aaload
ldc "pattern2"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L9
iconst_0
putstatic com/chelpus/root/utils/odexrunpatch/pattern2 Z
L9:
aload 0
iconst_1
aaload
ldc "pattern3"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L10
iconst_0
putstatic com/chelpus/root/utils/odexrunpatch/pattern3 Z
L10:
aload 0
iconst_1
aaload
ldc "pattern4"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L11
iconst_0
putstatic com/chelpus/root/utils/odexrunpatch/pattern4 Z
L11:
aload 0
iconst_1
aaload
ldc "pattern5"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L12
iconst_0
putstatic com/chelpus/root/utils/odexrunpatch/pattern5 Z
L12:
aload 0
iconst_1
aaload
ldc "pattern6"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L13
iconst_0
putstatic com/chelpus/root/utils/odexrunpatch/pattern6 Z
L13:
aload 0
iconst_1
aaload
ldc "dependencies"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L14
iconst_0
putstatic com/chelpus/root/utils/odexrunpatch/dependencies Z
L14:
aload 0
iconst_1
aaload
ldc "amazon"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L15
iconst_0
putstatic com/chelpus/root/utils/odexrunpatch/amazon Z
L15:
aload 0
iconst_1
aaload
ldc "samsung"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L16
iconst_0
putstatic com/chelpus/root/utils/odexrunpatch/samsung Z
L16:
aload 0
bipush 6
aaload
ldc "createAPK"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L17
iconst_1
putstatic com/chelpus/root/utils/odexrunpatch/createAPK Z
L17:
aload 0
bipush 6
aaload
ifnull L19
L18:
aload 0
bipush 6
aaload
ldc "ART"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L19
iconst_1
putstatic com/chelpus/root/utils/odexrunpatch/ART Z
L19:
aload 0
bipush 7
aaload
ifnull L21
L20:
aload 0
bipush 7
aaload
putstatic com/chelpus/root/utils/odexrunpatch/uid Ljava/lang/String;
L21:
aload 0
bipush 6
aaload
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
L22:
aload 0
iconst_5
aaload
ldc "copyDC"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L23
iconst_1
putstatic com/chelpus/root/utils/odexrunpatch/copyDC Z
L23:
getstatic com/chelpus/root/utils/odexrunpatch/createAPK Z
ifeq L215
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
L215:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 31
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 32
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 33
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 34
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 35
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 36
bipush 29
newarray byte
astore 22
aload 22
dup
iconst_0
ldc_w 5
bastore
dup
iconst_1
ldc_w 0
bastore
dup
iconst_2
ldc_w 0
bastore
dup
iconst_3
ldc_w 0
bastore
dup
iconst_4
ldc_w 1
bastore
dup
iconst_5
ldc_w 1
bastore
dup
bipush 6
ldc_w 0
bastore
dup
bipush 7
ldc_w 0
bastore
dup
bipush 8
ldc_w 2
bastore
dup
bipush 9
ldc_w 1
bastore
dup
bipush 10
ldc_w 0
bastore
dup
bipush 11
ldc_w 0
bastore
dup
bipush 12
ldc_w 3
bastore
dup
bipush 13
ldc_w 1
bastore
dup
bipush 14
ldc_w 0
bastore
dup
bipush 15
ldc_w 0
bastore
dup
bipush 16
ldc_w 15
bastore
dup
bipush 17
ldc_w 0
bastore
dup
bipush 18
ldc_w 0
bastore
dup
bipush 19
ldc_w 0
bastore
dup
bipush 20
ldc_w 26
bastore
dup
bipush 21
ldc_w 0
bastore
dup
bipush 22
ldc_w 0
bastore
dup
bipush 23
ldc_w 0
bastore
dup
bipush 24
ldc_w 15
bastore
dup
bipush 25
ldc_w 0
bastore
dup
bipush 26
ldc_w 0
bastore
dup
bipush 27
ldc_w 0
bastore
dup
bipush 28
ldc_w 89
bastore
pop
bipush 29
newarray byte
astore 23
aload 23
dup
iconst_0
ldc_w 5
bastore
dup
iconst_1
ldc_w 0
bastore
dup
iconst_2
ldc_w 0
bastore
dup
iconst_3
ldc_w 0
bastore
dup
iconst_4
ldc_w 1
bastore
dup
iconst_5
ldc_w 1
bastore
dup
bipush 6
ldc_w 0
bastore
dup
bipush 7
ldc_w 0
bastore
dup
bipush 8
ldc_w 2
bastore
dup
bipush 9
ldc_w 1
bastore
dup
bipush 10
ldc_w 0
bastore
dup
bipush 11
ldc_w 0
bastore
dup
bipush 12
ldc_w 3
bastore
dup
bipush 13
ldc_w 1
bastore
dup
bipush 14
ldc_w 0
bastore
dup
bipush 15
ldc_w 0
bastore
dup
bipush 16
ldc_w 15
bastore
dup
bipush 17
ldc_w 0
bastore
dup
bipush 18
ldc_w 0
bastore
dup
bipush 19
ldc_w 0
bastore
dup
bipush 20
ldc_w 15
bastore
dup
bipush 21
ldc_w 0
bastore
dup
bipush 22
ldc_w 0
bastore
dup
bipush 23
ldc_w 0
bastore
dup
bipush 24
ldc_w 15
bastore
dup
bipush 25
ldc_w 0
bastore
dup
bipush 26
ldc_w 0
bastore
dup
bipush 27
ldc_w 0
bastore
dup
bipush 28
ldc_w 89
bastore
pop
bipush 29
newarray byte
astore 24
aload 24
dup
iconst_0
ldc_w 0
bastore
dup
iconst_1
ldc_w 0
bastore
dup
iconst_2
ldc_w 0
bastore
dup
iconst_3
ldc_w 0
bastore
dup
iconst_4
ldc_w 0
bastore
dup
iconst_5
ldc_w 0
bastore
dup
bipush 6
ldc_w 0
bastore
dup
bipush 7
ldc_w 0
bastore
dup
bipush 8
ldc_w 0
bastore
dup
bipush 9
ldc_w 0
bastore
dup
bipush 10
ldc_w 0
bastore
dup
bipush 11
ldc_w 0
bastore
dup
bipush 12
ldc_w 0
bastore
dup
bipush 13
ldc_w 0
bastore
dup
bipush 14
ldc_w 0
bastore
dup
bipush 15
ldc_w 0
bastore
dup
bipush 16
ldc_w 2
bastore
dup
bipush 17
ldc_w 0
bastore
dup
bipush 18
ldc_w 0
bastore
dup
bipush 19
ldc_w 0
bastore
dup
bipush 20
ldc_w 3
bastore
dup
bipush 21
ldc_w 0
bastore
dup
bipush 22
ldc_w 0
bastore
dup
bipush 23
ldc_w 0
bastore
dup
bipush 24
ldc_w 0
bastore
dup
bipush 25
ldc_w 0
bastore
dup
bipush 26
ldc_w 0
bastore
dup
bipush 27
ldc_w 0
bastore
dup
bipush 28
ldc_w 0
bastore
pop
aload 31
ldc "1A ?? FF FF"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "1A ?? ?? ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "(pak intekekt 0)"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc "search_pack"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "1B ?? FF FF FF FF"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "1B ?? ?? ?? ?? ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "(pak intekekt 0)"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc "search_pack"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "1A ?? FF FF"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "1A ?? ?? ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "(sha intekekt 2)"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc "search"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "1B ?? FF FF FF FF"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "1B ?? ?? ?? ?? ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "(sha intekekt 2 32 bit)"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc "search"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "0A ?? 39 ?? ?? 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "12 S1 39 ?? ?? 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "lvl patch N2!\n(sha intekekt 3)"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc "search"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "6E 20 FF FF ?? 00 0A ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "6E 20 ?? ?? ?? 00 12 S1"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "support2 Fixed!\n(sha intekekt 4)"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "70 ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? ?? 62 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 44 00 01 00 2B 00 ?? ?? ?? ?? 62 ?? ?? ?? 11"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "70 ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? ?? 62 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 12 10 00 00 2B 00 ?? ?? ?? ?? 62 ?? ?? ?? 11"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/pattern3 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "lvl patch N2!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "12 ?? 12 ?? 71 ?? ?? ?? ?? ?? 0B ?? 52 ?? ?? ?? 39 ?? ?? ?? 53 ?? ?? ?? 31"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "12 S1 12 S1 71 ?? ?? ?? ?? ?? 0B ?? 52 ?? ?? ?? 39 ?? ?? ?? 53 ?? ?? ?? 31"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/pattern5 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "lvl patch N3!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "0a ?? 38 ?? 0e 00 1a ?? ?? ?? 1A ?? ?? ?? 71 ?? ?? ?? ?? ?? 72"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "0a ?? 33 00 ?? ?? 1a ?? ?? ?? 1A ?? ?? ?? 71 ?? ?? ?? ?? ?? 72"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/pattern2 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "lvl patch N4!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "1A ?? ?? ?? 70 ?? ?? ?? ?? ?? 27 ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 12 ?? 46 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0A ?? ?? ?? ?? ?? 12"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "1A ?? ?? ?? 70 ?? ?? ?? ?? ?? 27 ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 12 ?? 46 ?? ?? ?? 71 ?? ?? ?? ?? ?? 12 ?? ?? ?? ?? ?? 12"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/pattern4 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "lvl patch N5!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 5B ?? ?? ?? 12 ?? 46 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0A ?? ?? ?? ?? ?? 12 ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 5B ?? ?? ?? 12 ?? 46 ?? ?? ?? 71 ?? ?? ?? ?? ?? 12 ?? ?? ?? ?? ?? 12 ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/pattern4 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "lvl patch N5!\nparse response code"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc "patch5"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "12 ?? 12 ?? 71 ?? ?? ?? ?? ?? 0B ?? ?? ?? ?? ?? ?? ?? 31 ?? ?? ?? 3B ?? ?? ?? 01"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "12 S1 12 S1 71 ?? ?? ?? ?? ?? 0B ?? ?? ?? ?? ?? ?? ?? 31 ?? ?? ?? 3B ?? ?? ?? 01"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/pattern5 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "lvl patch N3!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "2C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? 0F ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 ?? 28 ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 01 ?? 28"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "2C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? 0F ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 ?? 28 ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 12 ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 01 ?? 28"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/pattern4 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "lvl patch N7!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "2C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? 0F ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 01 ?? 28"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "2C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? 0F ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 12 ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 01 ?? 28"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/pattern4 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "lvl patch N7!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "2C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? 0F ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 28"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "2C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? 0F ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 12 ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 28"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/pattern4 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "lvl patch N7!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "2C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? 0F ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "2C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? 0F ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 12"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/pattern4 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "lvl patch N7!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "0C ?? 71 ?? ?? ?? ?? ?? 0C ?? 21 ?? 12 ?? 35 ?? ?? ?? 22 ?? ?? ?? 1A ?? ?? ?? 70 ?? ?? ?? ?? ?? 27 ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 5B ?? ?? ?? 12 ?? 46 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0A"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "0C ?? 71 ?? ?? ?? ?? ?? 0C ?? 21 ?? 12 ?? 35 ?? ?? ?? 22 ?? ?? ?? 1A ?? ?? ?? 70 ?? ?? ?? ?? ?? 27 ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 5B ?? ?? ?? 12 ?? 46 ?? ?? ?? 71 ?? ?? ?? ?? ?? 12"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/pattern4 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "lvl patch N5!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc "patch5"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
bipush 32
newarray byte
astore 25
aload 25
dup
iconst_0
ldc_w 26
bastore
dup
iconst_1
ldc_w 102
bastore
dup
iconst_2
ldc_w 102
bastore
dup
iconst_3
ldc_w 102
bastore
dup
iconst_4
ldc_w 113
bastore
dup
iconst_5
ldc_w 102
bastore
dup
bipush 6
ldc_w 102
bastore
dup
bipush 7
ldc_w 102
bastore
dup
bipush 8
ldc_w 102
bastore
dup
bipush 9
ldc_w 102
bastore
dup
bipush 10
ldc_w 12
bastore
dup
bipush 11
ldc_w 102
bastore
dup
bipush 12
ldc_w 113
bastore
dup
bipush 13
ldc_w 102
bastore
dup
bipush 14
ldc_w 102
bastore
dup
bipush 15
ldc_w 102
bastore
dup
bipush 16
ldc_w 102
bastore
dup
bipush 17
ldc_w 102
bastore
dup
bipush 18
ldc_w 12
bastore
dup
bipush 19
ldc_w 102
bastore
dup
bipush 20
ldc_w 33
bastore
dup
bipush 21
ldc_w 102
bastore
dup
bipush 22
ldc_w 102
bastore
dup
bipush 23
ldc_w 102
bastore
dup
bipush 24
ldc_w 53
bastore
dup
bipush 25
ldc_w 102
bastore
dup
bipush 26
ldc_w 102
bastore
dup
bipush 27
ldc_w 102
bastore
dup
bipush 28
ldc_w 34
bastore
dup
bipush 29
ldc_w 102
bastore
dup
bipush 30
ldc_w 102
bastore
dup
bipush 31
ldc_w 102
bastore
pop
bipush 32
newarray byte
astore 26
aload 26
dup
iconst_0
ldc_w 26
bastore
dup
iconst_1
ldc_w 102
bastore
dup
iconst_2
ldc_w 102
bastore
dup
iconst_3
ldc_w 102
bastore
dup
iconst_4
ldc_w 113
bastore
dup
iconst_5
ldc_w 102
bastore
dup
bipush 6
ldc_w 102
bastore
dup
bipush 7
ldc_w 102
bastore
dup
bipush 8
ldc_w 102
bastore
dup
bipush 9
ldc_w 102
bastore
dup
bipush 10
ldc_w 12
bastore
dup
bipush 11
ldc_w 102
bastore
dup
bipush 12
ldc_w 113
bastore
dup
bipush 13
ldc_w 102
bastore
dup
bipush 14
ldc_w 102
bastore
dup
bipush 15
ldc_w 102
bastore
dup
bipush 16
ldc_w 102
bastore
dup
bipush 17
ldc_w 102
bastore
dup
bipush 18
ldc_w 12
bastore
dup
bipush 19
ldc_w 102
bastore
dup
bipush 20
ldc_w 33
bastore
dup
bipush 21
ldc_w 102
bastore
dup
bipush 22
ldc_w 102
bastore
dup
bipush 23
ldc_w 102
bastore
dup
bipush 24
ldc_w 53
bastore
dup
bipush 25
ldc_w 102
bastore
dup
bipush 26
ldc_w 102
bastore
dup
bipush 27
ldc_w 102
bastore
dup
bipush 28
ldc_w 34
bastore
dup
bipush 29
ldc_w 102
bastore
dup
bipush 30
ldc_w 102
bastore
dup
bipush 31
ldc_w 102
bastore
pop
bipush 32
newarray byte
astore 27
aload 27
dup
iconst_0
ldc_w 0
bastore
dup
iconst_1
ldc_w 0
bastore
dup
iconst_2
ldc_w 0
bastore
dup
iconst_3
ldc_w 0
bastore
dup
iconst_4
ldc_w 0
bastore
dup
iconst_5
ldc_w 0
bastore
dup
bipush 6
ldc_w 0
bastore
dup
bipush 7
ldc_w 0
bastore
dup
bipush 8
ldc_w 0
bastore
dup
bipush 9
ldc_w 0
bastore
dup
bipush 10
ldc_w 0
bastore
dup
bipush 11
ldc_w 0
bastore
dup
bipush 12
ldc_w 0
bastore
dup
bipush 13
ldc_w 0
bastore
dup
bipush 14
ldc_w 0
bastore
dup
bipush 15
ldc_w 0
bastore
dup
bipush 16
ldc_w 0
bastore
dup
bipush 17
ldc_w 0
bastore
dup
bipush 18
ldc_w 0
bastore
dup
bipush 19
ldc_w 0
bastore
dup
bipush 20
ldc_w 0
bastore
dup
bipush 21
ldc_w 0
bastore
dup
bipush 22
ldc_w 0
bastore
dup
bipush 23
ldc_w 0
bastore
dup
bipush 24
ldc_w 0
bastore
dup
bipush 25
ldc_w 0
bastore
dup
bipush 26
ldc_w 0
bastore
dup
bipush 27
ldc_w 0
bastore
dup
bipush 28
ldc_w 0
bastore
dup
bipush 29
ldc_w 0
bastore
dup
bipush 30
ldc_w 0
bastore
dup
bipush 31
ldc_w 0
bastore
pop
bipush 13
newarray byte
astore 28
aload 28
dup
iconst_0
ldc_w 0
bastore
dup
iconst_1
ldc_w 70
bastore
dup
iconst_2
ldc_w 102
bastore
dup
iconst_3
ldc_w 102
bastore
dup
iconst_4
ldc_w 102
bastore
dup
iconst_5
ldc_w 113
bastore
dup
bipush 6
ldc_w 102
bastore
dup
bipush 7
ldc_w 102
bastore
dup
bipush 8
ldc_w 102
bastore
dup
bipush 9
ldc_w 102
bastore
dup
bipush 10
ldc_w 102
bastore
dup
bipush 11
ldc_w 10
bastore
dup
bipush 12
ldc_w 102
bastore
pop
bipush 13
newarray byte
astore 29
aload 29
dup
iconst_0
ldc_w 0
bastore
dup
iconst_1
ldc_w 70
bastore
dup
iconst_2
ldc_w 102
bastore
dup
iconst_3
ldc_w 102
bastore
dup
iconst_4
ldc_w 102
bastore
dup
iconst_5
ldc_w 113
bastore
dup
bipush 6
ldc_w 102
bastore
dup
bipush 7
ldc_w 102
bastore
dup
bipush 8
ldc_w 102
bastore
dup
bipush 9
ldc_w 102
bastore
dup
bipush 10
ldc_w 102
bastore
dup
bipush 11
ldc_w 18
bastore
dup
bipush 12
ldc_w 102
bastore
pop
bipush 13
newarray byte
astore 30
aload 30
dup
iconst_0
ldc_w 0
bastore
dup
iconst_1
ldc_w 0
bastore
dup
iconst_2
ldc_w 0
bastore
dup
iconst_3
ldc_w 0
bastore
dup
iconst_4
ldc_w 0
bastore
dup
iconst_5
ldc_w 0
bastore
dup
bipush 6
ldc_w 0
bastore
dup
bipush 7
ldc_w 0
bastore
dup
bipush 8
ldc_w 0
bastore
dup
bipush 9
ldc_w 0
bastore
dup
bipush 10
ldc_w 0
bastore
dup
bipush 11
ldc_w 1
bastore
dup
bipush 12
ldc_w 0
bastore
pop
aload 31
ldc "2C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? 0F ?? 62 ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 ?? 62 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "2C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? 0F ?? 62 ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 ?? 62 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 12"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/pattern4 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "lvl patch N7!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "12 ?? 12 ?? 71 ?? ?? ?? ?? ?? 0B ?? ?? ?? ?? ?? ?? ?? ?? ?? 33 ?? ?? ?? 53"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "12 S1 12 S1 71 ?? ?? ?? ?? ?? 0B ?? ?? ?? ?? ?? ?? ?? ?? ?? 33 ?? ?? ?? 53"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/pattern5 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "lvl patch N3!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "6C 61 63 6B 79 70 61 74 63 68"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "6C 75 63 75 79 70 75 74 63 68"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "6C 75 63 6B 79 70 61 74 63 68"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "6C 75 63 75 79 70 75 74 75 68"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "64 69 6D 6F 6E 76 69 64 65 6F 2E"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "64 69 6D 69 6E 69 69 64 65 6F 2E"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "12 ?? 12 ?? 71 ?? ?? ?? ?? ?? 0B ?? 52 ?? ?? ?? 33 ?? ?? ?? 53 ?? ?? ?? 31"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "12 S1 12 S1 71 ?? ?? ?? ?? ?? 0B ?? 52 ?? ?? ?? 33 ?? ?? ?? 53 ?? ?? ?? 31"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/pattern5 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "lvl patch N3!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "6E 10 ?? ?? ?? ?? 0A ?? 6E 10 ?? ?? ?? ?? 0C ?? 6E 10 ?? ?? ?? ?? 0C ?? 6E 40"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "6E 10 ?? ?? ?? ?? 12 ?? 6E 10 ?? ?? ?? ?? 0C ?? 6E 10 ?? ?? ?? ?? 0C ?? 6E 40"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/pattern4 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "lvl patch N7!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "62 01 ?? ?? 6E 20 ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A 01 71 10 ?? ?? ?? ?? 0C 02 71 10 ?? ?? ?? ?? 6E 10 ?? ?? ?? ?? 0C 02 6E 10 ?? ?? ?? ?? 0C 03 6E 40 ?? ?? ?? ?? 28"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "62 01 ?? ?? 6E 20 ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 12 01 71 10 ?? ?? ?? ?? 0C 02 71 10 ?? ?? ?? ?? 6E 10 ?? ?? ?? ?? 0C 02 6E 10 ?? ?? ?? ?? 0C 03 6E 40 ?? ?? ?? ?? 28"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/pattern4 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "lvl patch N7!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "12 ?? 12 ?? 71 ?? ?? ?? ?? ?? 0B ?? 52 ?? ?? ?? 39 ?? ?? ?? 53 ?? ?? ?? 31"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "12 S0 12 S0 71 ?? ?? ?? ?? ?? 0B ?? 52 ?? ?? ?? 39 ?? ?? ?? 53 ?? ?? ?? 31"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/pattern6 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "lvl patch N3!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "12 ?? 12 ?? 71 ?? ?? ?? ?? ?? 0B ?? 52 ?? ?? ?? 33 ?? ?? ?? 53 ?? ?? ?? 31"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "12 S0 12 S0 71 ?? ?? ?? ?? ?? 0B ?? 52 ?? ?? ?? 33 ?? ?? ?? 53 ?? ?? ?? 31"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/pattern6 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "lvl patch N3!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "12 ?? 12 ?? 71 ?? ?? ?? ?? ?? 0B ?? ?? ?? ?? ?? ?? ?? ?? ?? 33 ?? ?? ?? 53"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "12 S0 12 S0 71 ?? ?? ?? ?? ?? 0B ?? ?? ?? ?? ?? ?? ?? ?? ?? 33 ?? ?? ?? 53"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/pattern6 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "lvl patch N3!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "12 ?? 12 ?? 71 ?? ?? ?? ?? ?? 0B ?? ?? ?? ?? ?? ?? ?? 31 ?? ?? ?? 3B ?? ?? ?? 01"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "12 S0 12 S0 71 ?? ?? ?? ?? ?? 0B ?? ?? ?? ?? ?? ?? ?? 31 ?? ?? ?? 3B ?? ?? ?? 01"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/pattern6 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "lvl patch N3!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "13 00 00 01 33 R0 ?? ?? 54 ?? ?? ?? 71 10 ?? ?? ?? ?? 0C 01"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "13 W0 00 01 33 00 00 01 54 ?? ?? ?? 71 10 ?? ?? ?? ?? 0C 01"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/pattern4 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "lvl patch N3!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "1A 05 ?? ?? 63 00 ?? ?? 38 00 09 00 62 00 ?? ?? 1A 01 ?? ?? 6E 20 ?? ?? ?? ?? 54 60 ?? ?? 1A 01 ?? ?? 6E 20 ?? ?? ?? ?? ?? ?? 38 00 0A 00 62 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "1A 05 ?? ?? 63 00 ?? ?? 38 00 09 00 62 00 ?? ?? 1A 01 ?? ?? 6E 20 ?? ?? ?? ?? 54 60 ?? ?? 1A 01 ?? ?? 6E 20 ?? ?? ?? ?? ?? ?? 33 00 ?? ?? 62 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/amazon Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "amazon patch N1!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "6E 20 ?? ?? ?? ?? 0C 00 38 00 05 00 12 10 ?? ?? 0F 00 12 00 ?? ?? 0D 00 ?? ?? 27 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "6E 20 ?? ?? ?? ?? 0C 00 33 00 ?? ?? 12 10 ?? ?? 0F 00 12 00 ?? ?? 0D 00 ?? ?? 27 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/amazon Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "amazon patch N1!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "13 ?? 09 00 6E ?? ?? ?? ?? ?? 0C ?? 71 ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A ?? 32 ?? ?? ?? 59 ?? ?? ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 27"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "13 ?? 09 00 6E ?? ?? ?? ?? ?? 0C ?? 71 ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A ?? 32 00 ?? ?? 59 ?? ?? ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 27"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/samsung Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "samsung patch N1!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "13 ?? 09 00 13 ?? 0B 00 6E ?? ?? ?? ?? ?? 0C ?? 71 ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A ?? 32 ?? ?? ?? 59 ?? ?? ?? 22 ?? ?? ?? 12 ?? 70 ?? ?? ?? ?? ?? 27"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "13 ?? 09 00 13 ?? 0B 00 6E ?? ?? ?? ?? ?? 0C ?? 71 ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A ?? 32 00 ?? ?? 59 ?? ?? ?? 22 ?? ?? ?? 12 ?? 70 ?? ?? ?? ?? ?? 27"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/samsung Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "samsung patch N1!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 71 ?? ?? ?? ?? ?? 0A ?? 0F ?? 00 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 71 ?? ?? ?? ?? ?? 12 S1 0F ?? 00 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/samsung Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "samsung patch N2!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "54 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0A ?? 0F ?? 00 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "54 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 12 S1 0F ?? 00 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/samsung Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "samsung patch N2!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 71 ?? ?? ?? ?? ?? 0A ?? 0F ?? 00 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 71 ?? ?? ?? ?? ?? 12 S1 0F ?? 00 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/samsung Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "samsung patch N3!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "54 ?? ?? ?? 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 54 ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0A ?? 38 ?? ?? ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "54 ?? ?? ?? 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 54 ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0A ?? 33 00 ?? ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/samsung Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "samsung patch N3!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "13 ?? 32 00 33 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 38 ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 12"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "13 ?? 32 00 32 00 ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 38 ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 12"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/samsung Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "samsung patch N4!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "13 ?? 32 00 33 ?? ?? ?? 70 ?? ?? ?? ?? ?? 0A ?? 38 ?? ?? ?? 54 ?? ?? ?? 59 ?? ?? ?? ?? ?? 28"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "13 ?? 32 00 32 00 ?? ?? 70 ?? ?? ?? ?? ?? 0A ?? 38 ?? ?? ?? 54 ?? ?? ?? 59 ?? ?? ?? ?? ?? 28"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/samsung Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "samsung patch N4!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "38 ?? 06 00 32 ?? 04 00 33 ?? ?? ?? 1A ?? ?? ?? 71"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "12 ?? 00 00 32 00 04 00 33 ?? ?? ?? ?? ?? ?? ?? ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/pattern4 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "lvl patch N6!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
iconst_0
aaload
ldc "com.buak.Link2SD"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L216
aload 31
ldc "00 05 2E 6F 64 65 78 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "00 05 2E 6F 64 65 79 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/pattern4 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "lvl patch N7!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L216:
aload 31
ldc "00 04 6F 64 65 78 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "00 04 6F 64 65 79 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/pattern4 Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "lvl patch N7!\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "2F 4C 75 63 6B 79 50 61 74 63 68 65 72"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "2F 4C 75 63 6B 79 50 79 74 63 68 65 72"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
getstatic com/chelpus/root/utils/odexrunpatch/ART Z
ifne L217
aload 31
ldc "13 ?? 09 00 12 ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "12 00 0F 00 12 ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/dependencies Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "com.android.vending dependencies removed\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "12 ?? 12 ?? 13 ?? 09 00 6E ?? ?? ?? ?? ?? 0C ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "12 ?? 12 ?? 12 00 0F 00 6E ?? ?? ?? ?? ?? 0C ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/dependencies Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "com.android.vending dependencies removed\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L218:
aload 21
aload 31
aload 32
aload 33
aload 34
aload 35
aload 36
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokestatic com/chelpus/Utils/convertToPatchItemAuto(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/Boolean;)V
L26:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L27
aload 0
iconst_2
aaload
ldc "RW"
invokestatic com/chelpus/Utils/remount(Ljava/lang/String;Ljava/lang/String;)Z
pop
L27:
getstatic com/chelpus/root/utils/odexrunpatch/createAPK Z
ifne L50
getstatic com/chelpus/root/utils/odexrunpatch/ART Z
ifne L50
aload 0
iconst_3
aaload
putstatic com/chelpus/root/utils/odexrunpatch/dir Ljava/lang/String;
aload 0
iconst_2
aaload
putstatic com/chelpus/root/utils/odexrunpatch/dirapp Ljava/lang/String;
invokestatic com/chelpus/root/utils/odexrunpatch/clearTemp()V
aload 0
iconst_4
aaload
ldc "not_system"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L30
iconst_0
putstatic com/chelpus/root/utils/odexrunpatch/system Z
L30:
aload 0
iconst_4
aaload
ldc "system"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L31
iconst_1
putstatic com/chelpus/root/utils/odexrunpatch/system Z
L31:
getstatic com/chelpus/root/utils/odexrunpatch/filestopatch Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
ldc "CLASSES mode create odex enabled."
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
L32:
aload 0
iconst_0
aaload
astore 31
L33:
aload 0
iconst_2
aaload
putstatic com/chelpus/root/utils/odexrunpatch/appdir Ljava/lang/String;
aload 0
iconst_3
aaload
putstatic com/chelpus/root/utils/odexrunpatch/sddir Ljava/lang/String;
invokestatic com/chelpus/root/utils/odexrunpatch/clearTempSD()V
new java/io/File
dup
getstatic com/chelpus/root/utils/odexrunpatch/appdir Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 31
ldc "Get classes.dex."
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
getstatic com/chelpus/root/utils/odexrunpatch/print Ljava/io/PrintStream;
ldc "Get classes.dex."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 31
invokestatic com/chelpus/root/utils/odexrunpatch/unzipART(Ljava/io/File;)V
getstatic com/chelpus/root/utils/odexrunpatch/classesFiles Ljava/util/ArrayList;
ifnull L34
getstatic com/chelpus/root/utils/odexrunpatch/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifne L35
L34:
new java/io/FileNotFoundException
dup
invokespecial java/io/FileNotFoundException/<init>()V
athrow
L28:
astore 21
ldc "Error: Program files are not found!\n\nCheck the location dalvik-cache to solve problems!\n\nDefault: /data/dalvik-cache/*"
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
aload 21
invokevirtual java/io/FileNotFoundException/printStackTrace()V
L219:
getstatic com/chelpus/root/utils/odexrunpatch/filestopatch Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 21
L220:
aload 21
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L221
aload 21
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
invokestatic com/chelpus/Utils/fixadler(Ljava/io/File;)V
invokestatic com/chelpus/root/utils/odexrunpatch/clearTempSD()V
goto L220
L217:
aload 31
ldc "13 ?? 09 00 12 ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "12 S0 00 00 12 S0 12 S0 6E ?? ?? ?? ?? ?? 0C ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/dependencies Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "com.android.vending dependencies removed\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 31
ldc "12 ?? 12 ?? 13 ?? 09 00 6E ?? ?? ?? ?? ?? 0C ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "12 S0 12 S0 12 S0 00 00 6E ?? ?? ?? ?? ?? 0C ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 33
getstatic com/chelpus/root/utils/odexrunpatch/dependencies Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
ldc "com.android.vending dependencies removed\n"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 35
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 36
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
goto L218
L35:
getstatic com/chelpus/root/utils/odexrunpatch/filestopatch Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
getstatic com/chelpus/root/utils/odexrunpatch/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 31
L36:
aload 31
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L39
aload 31
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
astore 32
aload 32
invokevirtual java/io/File/exists()Z
ifne L37
new java/io/FileNotFoundException
dup
invokespecial java/io/FileNotFoundException/<init>()V
athrow
L29:
astore 21
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception e"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 21
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
aload 21
invokevirtual java/lang/Exception/printStackTrace()V
aload 21
invokevirtual java/lang/Exception/printStackTrace()V
goto L219
L37:
getstatic com/chelpus/root/utils/odexrunpatch/filestopatch Ljava/util/ArrayList;
aload 32
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L38:
goto L36
L39:
aload 0
iconst_2
aaload
iconst_1
invokestatic com/chelpus/Utils/getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;
astore 31
new java/io/File
dup
aload 31
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 32
aload 32
invokevirtual java/io/File/exists()Z
ifeq L40
aload 32
invokevirtual java/io/File/delete()Z
pop
L40:
new java/io/File
dup
aload 31
ldc "-1"
ldc "-2"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 32
aload 32
invokevirtual java/io/File/exists()Z
ifeq L41
aload 32
invokevirtual java/io/File/delete()Z
pop
L41:
new java/io/File
dup
aload 31
ldc "-2"
ldc "-1"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 31
aload 31
invokevirtual java/io/File/exists()Z
ifeq L42
aload 31
invokevirtual java/io/File/delete()Z
pop
L42:
getstatic com/chelpus/root/utils/odexrunpatch/filestopatch Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 31
L43:
aload 31
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L219
aload 31
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
astore 33
ldc "Find string id."
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 32
aload 32
ldc "com.android.vending"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "SHA1withRSA"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "com.android.vending.billing.InAppBillingService.BIND"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "Ljava/security/Signature;"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 32
ldc "verify"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
ldc "String analysis."
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
getstatic com/chelpus/root/utils/odexrunpatch/print Ljava/io/PrintStream;
ldc "String analysis."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 33
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aload 32
iconst_0
invokestatic com/chelpus/Utils/getStringIds(Ljava/lang/String;Ljava/util/ArrayList;Z)Ljava/util/ArrayList;
astore 34
L44:
iconst_0
istore 16
iconst_0
istore 6
L45:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 32
aload 32
new com/android/vending/billing/InAppBillingService/LUCK/CommandItem
dup
ldc "Ljava/security/Signature;"
ldc "verify"
invokespecial com/android/vending/billing/InAppBillingService/LUCK/CommandItem/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 34
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 34
L46:
aload 34
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L222
aload 34
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/StringItem
astore 35
aload 32
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 36
L47:
aload 36
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L223
aload 36
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/CommandItem
astore 37
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/object Ljava/lang/String;
aload 35
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/str Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L48
aload 37
aload 35
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
putfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/Object [B
L48:
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/method Ljava/lang/String;
aload 35
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/str Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L47
aload 37
aload 35
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
putfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/Method [B
L49:
goto L47
L50:
getstatic com/chelpus/root/utils/odexrunpatch/createAPK Z
ifeq L58
L51:
aload 0
iconst_0
aaload
astore 31
L52:
aload 0
iconst_2
aaload
putstatic com/chelpus/root/utils/odexrunpatch/appdir Ljava/lang/String;
aload 0
iconst_5
aaload
putstatic com/chelpus/root/utils/odexrunpatch/sddir Ljava/lang/String;
invokestatic com/chelpus/root/utils/odexrunpatch/clearTempSD()V
new java/io/File
dup
getstatic com/chelpus/root/utils/odexrunpatch/appdir Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 32
aload 32
invokestatic com/chelpus/root/utils/odexrunpatch/unzipSD(Ljava/io/File;)V
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/odexrunpatch/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 31
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
putstatic com/chelpus/root/utils/odexrunpatch/crkapk Ljava/io/File;
aload 32
getstatic com/chelpus/root/utils/odexrunpatch/crkapk Ljava/io/File;
invokestatic com/chelpus/Utils/copyFile(Ljava/io/File;Ljava/io/File;)V
getstatic com/chelpus/root/utils/odexrunpatch/classesFiles Ljava/util/ArrayList;
ifnull L53
getstatic com/chelpus/root/utils/odexrunpatch/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifne L54
L53:
new java/io/FileNotFoundException
dup
invokespecial java/io/FileNotFoundException/<init>()V
athrow
L54:
getstatic com/chelpus/root/utils/odexrunpatch/filestopatch Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
getstatic com/chelpus/root/utils/odexrunpatch/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 31
L55:
aload 31
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L58
aload 31
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
astore 32
aload 32
invokevirtual java/io/File/exists()Z
ifne L56
new java/io/FileNotFoundException
dup
invokespecial java/io/FileNotFoundException/<init>()V
athrow
L56:
getstatic com/chelpus/root/utils/odexrunpatch/filestopatch Ljava/util/ArrayList;
aload 32
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L57:
goto L55
L58:
getstatic com/chelpus/root/utils/odexrunpatch/ART Z
ifeq L42
L59:
aload 0
iconst_0
aaload
astore 31
L60:
aload 0
iconst_2
aaload
putstatic com/chelpus/root/utils/odexrunpatch/appdir Ljava/lang/String;
aload 0
iconst_3
aaload
putstatic com/chelpus/root/utils/odexrunpatch/sddir Ljava/lang/String;
invokestatic com/chelpus/root/utils/odexrunpatch/clearTempSD()V
new java/io/File
dup
getstatic com/chelpus/root/utils/odexrunpatch/appdir Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokestatic com/chelpus/root/utils/odexrunpatch/unzipART(Ljava/io/File;)V
getstatic com/chelpus/root/utils/odexrunpatch/classesFiles Ljava/util/ArrayList;
ifnull L61
getstatic com/chelpus/root/utils/odexrunpatch/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifne L62
L61:
new java/io/FileNotFoundException
dup
invokespecial java/io/FileNotFoundException/<init>()V
athrow
L62:
getstatic com/chelpus/root/utils/odexrunpatch/filestopatch Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
getstatic com/chelpus/root/utils/odexrunpatch/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 31
L63:
aload 31
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L42
aload 31
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
astore 32
aload 32
invokevirtual java/io/File/exists()Z
ifne L64
new java/io/FileNotFoundException
dup
invokespecial java/io/FileNotFoundException/<init>()V
athrow
L64:
getstatic com/chelpus/root/utils/odexrunpatch/filestopatch Ljava/util/ArrayList;
aload 32
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L65:
goto L63
L223:
iload 16
istore 17
L66:
aload 35
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/str Ljava/lang/String;
ldc "com.android.vending.billing.InAppBillingService.BIND"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L224
aload 21
iconst_0
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_2
aload 35
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_0
baload
bastore
aload 21
iconst_0
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_3
aload 35
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_1
baload
bastore
aload 21
iconst_1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_2
aload 35
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_0
baload
bastore
aload 21
iconst_1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_3
aload 35
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_1
baload
bastore
aload 21
iconst_1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_4
aload 35
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_2
baload
bastore
aload 21
iconst_1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_5
aload 35
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_3
baload
bastore
L67:
iconst_0
istore 17
L224:
iload 17
istore 16
L68:
aload 35
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/str Ljava/lang/String;
ldc "SHA1withRSA"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L46
aload 21
iconst_2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_2
aload 35
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_0
baload
bastore
aload 21
iconst_2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_3
aload 35
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_1
baload
bastore
aload 21
iconst_3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_2
aload 35
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_0
baload
bastore
aload 21
iconst_3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_3
aload 35
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_1
baload
bastore
aload 21
iconst_3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_4
aload 35
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_2
baload
bastore
aload 21
iconst_3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_5
aload 35
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_3
baload
bastore
L69:
iconst_1
istore 6
iload 17
istore 16
goto L46
L70:
aload 21
iconst_0
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/pattern Z
aload 21
iconst_1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/pattern Z
L71:
iload 6
ifne L73
L72:
aload 21
iconst_2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/pattern Z
aload 21
iconst_3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/pattern Z
aload 21
iconst_4
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/pattern Z
L73:
ldc "Parse data for patch."
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
getstatic com/chelpus/root/utils/odexrunpatch/print Ljava/io/PrintStream;
ldc "Parse data for patch."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 33
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aload 32
iconst_0
invokestatic com/chelpus/Utils/getMethodsIds(Ljava/lang/String;Ljava/util/ArrayList;Z)Z
pop
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 32
iconst_0
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/CommandItem
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/index_command [B
iconst_0
baload
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 32
iconst_0
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/CommandItem
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/index_command [B
iconst_1
baload
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 32
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 32
L74:
aload 32
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L76
aload 32
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/CommandItem
astore 34
aload 34
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/found_index_command Z
ifeq L74
aload 34
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/object Ljava/lang/String;
ldc "Ljava/security/Signature;"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L74
aload 21
bipush 11
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_2
aload 34
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/index_command [B
iconst_0
baload
bastore
aload 21
bipush 11
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_3
aload 34
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/index_command [B
iconst_1
baload
bastore
aload 21
bipush 11
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
iconst_1
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/pattern Z
L75:
goto L74
L76:
aload 21
invokevirtual java/util/ArrayList/size()I
anewarray com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
astore 32
L77:
iconst_0
istore 6
L78:
aload 21
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 34
L79:
aload 34
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L81
aload 32
iload 6
aload 34
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
aastore
L80:
iload 6
iconst_1
iadd
istore 6
goto L79
L81:
invokestatic java/lang/System/currentTimeMillis()J
lstore 18
new java/io/RandomAccessFile
dup
aload 33
ldc "rw"
invokespecial java/io/RandomAccessFile/<init>(Ljava/io/File;Ljava/lang/String;)V
invokevirtual java/io/RandomAccessFile/getChannel()Ljava/nio/channels/FileChannel;
astore 33
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Size file:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 33
invokevirtual java/nio/channels/FileChannel/size()J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
aload 33
getstatic java/nio/channels/FileChannel$MapMode/READ_WRITE Ljava/nio/channels/FileChannel$MapMode;
lconst_0
aload 33
invokevirtual java/nio/channels/FileChannel/size()J
l2i
i2l
invokevirtual java/nio/channels/FileChannel/map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
astore 34
L82:
iconst_0
istore 8
iconst_0
istore 13
iconst_0
istore 9
iconst_0
istore 7
iconst_0
istore 10
L83:
aload 34
invokevirtual java/nio/MappedByteBuffer/hasRemaining()Z
ifeq L131
L84:
iload 10
istore 12
L86:
getstatic com/chelpus/root/utils/odexrunpatch/createAPK Z
ifne L89
L87:
iload 10
istore 12
L88:
aload 34
invokevirtual java/nio/MappedByteBuffer/position()I
iload 10
isub
ldc_w 149999
if_icmple L89
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Progress size:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 34
invokevirtual java/nio/MappedByteBuffer/position()I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
aload 34
invokevirtual java/nio/MappedByteBuffer/position()I
istore 12
L89:
aload 34
invokevirtual java/nio/MappedByteBuffer/position()I
istore 15
aload 34
invokevirtual java/nio/MappedByteBuffer/get()B
istore 5
L90:
iload 8
ifne L225
iload 7
istore 6
iconst_0
ifeq L226
goto L225
L227:
iload 5
aload 22
iconst_0
baload
if_icmpne L100
L91:
getstatic com/chelpus/root/utils/odexrunpatch/pattern1 Z
ifeq L100
L92:
aload 24
iconst_0
baload
ifne L228
aload 23
iconst_0
iload 5
bastore
L228:
iconst_1
istore 6
sipush 1000
istore 11
sipush 1000
istore 10
L93:
aload 34
iload 15
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 34
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
L94:
iconst_0
istore 4
iconst_0
istore 3
goto L229
L230:
iload 10
istore 14
iload 4
istore 2
aload 24
iload 6
baload
iconst_2
if_icmpne L231
iload 10
istore 14
iload 4
istore 2
L95:
iload 1
aload 34
aload 34
invokevirtual java/nio/MappedByteBuffer/position()I
bipush 7
iadd
invokevirtual java/nio/MappedByteBuffer/get(I)B
if_icmpne L231
L96:
iload 6
istore 14
iload 1
istore 2
goto L231
L232:
iload 6
iconst_1
iadd
istore 6
L97:
iload 6
aload 22
arraylength
if_icmpne L124
L98:
iload 11
sipush 1000
if_icmpge L100
iload 3
ifeq L100
aload 23
iload 11
iload 2
bastore
iload 14
bipush 7
iadd
sipush 1000
if_icmpge L100
iload 2
ifeq L100
aload 23
iload 14
bipush 8
iadd
iload 3
bastore
L99:
aload 34
iload 15
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 34
aload 23
invokevirtual java/nio/MappedByteBuffer/put([B)Ljava/nio/ByteBuffer;
pop
aload 34
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
ldc "lvl patch N1!\n"
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
getstatic com/chelpus/root/utils/odexrunpatch/print Ljava/io/PrintStream;
ldc "lvl patch N1!\n"
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
L100:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 35
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 36
L101:
iconst_0
istore 11
iconst_0
istore 14
iload 9
istore 6
L102:
iload 14
aload 32
arraylength
if_icmpge L233
L103:
aload 32
iload 14
aaload
astore 37
L104:
aload 34
iload 15
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L105:
iload 11
istore 10
iload 6
istore 9
L106:
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
ifeq L234
L107:
iload 14
iconst_5
if_icmpeq L235
iload 14
bipush 6
if_icmpeq L235
iload 14
bipush 7
if_icmpeq L235
iload 14
bipush 8
if_icmpeq L235
iload 14
bipush 9
if_icmpeq L235
iload 11
istore 10
iload 6
istore 9
iload 14
bipush 10
if_icmpne L234
goto L235
L236:
iload 9
bipush 40
if_icmpge L156
iload 9
istore 6
L108:
iload 5
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_0
baload
if_icmpne L134
aload 35
invokevirtual java/util/ArrayList/clear()V
aload 36
invokevirtual java/util/ArrayList/clear()V
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iconst_0
iaload
ifne L109
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iconst_0
iload 5
bastore
L109:
iconst_1
istore 11
L110:
aload 34
iload 15
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 34
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
L111:
iload 1
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iload 11
baload
if_icmpeq L114
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 11
iaload
iconst_1
if_icmpeq L114
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 11
iaload
bipush 20
if_icmpeq L114
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 11
iaload
bipush 21
if_icmpeq L114
L112:
iload 9
istore 6
L113:
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 11
iaload
bipush 23
if_icmpne L134
L114:
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iload 11
iaload
ifne L115
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iload 11
iload 1
bastore
L115:
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iload 11
iaload
bipush 20
if_icmpne L116
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iload 11
iload 1
bipush 15
iand
i2b
bastore
L116:
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iload 11
iaload
bipush 21
if_icmpne L117
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iload 11
iload 1
bipush 15
iand
bipush 16
iadd
i2b
bastore
L117:
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 11
iaload
bipush 23
if_icmpne L118
aload 35
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iload 11
baload
iload 1
invokestatic java/lang/Byte/valueOf(B)Ljava/lang/Byte;
invokevirtual java/util/ArrayList/add(ILjava/lang/Object;)V
L118:
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iload 11
iaload
bipush 23
if_icmpne L119
aload 36
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iload 11
baload
iload 11
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/ArrayList/add(ILjava/lang/Object;)V
L119:
iload 11
iconst_1
iadd
istore 11
L120:
iload 11
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
arraylength
if_icmpne L154
aload 35
invokevirtual java/util/ArrayList/size()I
ifgt L237
aload 36
invokevirtual java/util/ArrayList/size()I
ifle L127
L121:
goto L237
L122:
iload 6
aload 35
invokevirtual java/util/ArrayList/size()I
if_icmpge L126
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
aload 36
iload 6
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
aload 35
iload 6
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/Byte
invokevirtual java/lang/Byte/byteValue()B
bastore
L123:
iload 6
iconst_1
iadd
istore 6
goto L122
L124:
aload 34
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
L125:
iload 14
istore 10
iload 2
istore 4
goto L229
L126:
aload 35
invokevirtual java/util/ArrayList/clear()V
aload 36
invokevirtual java/util/ArrayList/clear()V
L127:
aload 34
iload 15
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 34
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
invokevirtual java/nio/MappedByteBuffer/put([B)Ljava/nio/ByteBuffer;
pop
aload 34
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/resultText Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
getstatic com/chelpus/root/utils/odexrunpatch/print Ljava/io/PrintStream;
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/resultText Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 37
iconst_1
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/result Z
aload 37
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
aload 21
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 38
L128:
aload 38
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L238
aload 38
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
astore 39
aload 39
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L128
aload 39
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
L129:
goto L128
L85:
astore 32
L130:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 32
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
aload 32
invokevirtual java/lang/Exception/printStackTrace()V
L131:
aload 33
invokevirtual java/nio/channels/FileChannel/close()V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokestatic java/lang/System/currentTimeMillis()J
lload 18
lsub
ldc2_w 1000L
ldiv
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
ldc "Analise Results:"
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
L132:
goto L43
L238:
iconst_0
istore 6
L133:
aload 34
iload 15
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L134:
aload 34
iload 15
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L135:
iload 6
istore 9
L234:
iload 13
istore 6
L136:
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
ifeq L167
L137:
iload 13
istore 6
iload 14
iconst_4
if_icmpne L167
iload 13
iconst_1
iadd
istore 13
iload 13
bipush 90
if_icmpge L183
iload 13
istore 6
L138:
iload 5
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_0
baload
if_icmpne L166
aload 35
invokevirtual java/util/ArrayList/clear()V
aload 36
invokevirtual java/util/ArrayList/clear()V
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iconst_0
iaload
ifne L139
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iconst_0
iload 5
bastore
L139:
iconst_1
istore 11
L140:
aload 34
iload 15
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 34
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
L141:
iload 1
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iload 11
baload
if_icmpeq L144
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 11
iaload
iconst_1
if_icmpeq L144
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 11
iaload
bipush 20
if_icmpeq L144
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 11
iaload
bipush 21
if_icmpeq L144
L142:
iload 13
istore 6
L143:
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 11
iaload
bipush 23
if_icmpne L166
L144:
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iload 11
iaload
ifne L145
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iload 11
iload 1
bastore
L145:
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iload 11
iaload
bipush 20
if_icmpne L146
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iload 11
iload 1
bipush 15
iand
i2b
bastore
L146:
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iload 11
iaload
bipush 21
if_icmpne L147
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iload 11
iload 1
bipush 15
iand
bipush 16
iadd
i2b
bastore
L147:
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 11
iaload
bipush 23
if_icmpne L148
aload 35
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iload 11
baload
iload 1
invokestatic java/lang/Byte/valueOf(B)Ljava/lang/Byte;
invokevirtual java/util/ArrayList/add(ILjava/lang/Object;)V
L148:
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iload 11
iaload
bipush 23
if_icmpne L149
aload 36
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iload 11
baload
iload 11
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/ArrayList/add(ILjava/lang/Object;)V
L149:
iload 11
iconst_1
iadd
istore 11
L150:
iload 11
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
arraylength
if_icmpne L181
aload 35
invokevirtual java/util/ArrayList/size()I
ifgt L239
aload 36
invokevirtual java/util/ArrayList/size()I
ifle L162
L151:
goto L239
L152:
iload 6
aload 35
invokevirtual java/util/ArrayList/size()I
if_icmpge L161
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
aload 36
iload 6
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
aload 35
iload 6
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/Byte
invokevirtual java/lang/Byte/byteValue()B
bastore
L153:
iload 6
iconst_1
iadd
istore 6
goto L152
L154:
aload 34
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
L155:
goto L111
L156:
aload 37
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
aload 21
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 38
L157:
aload 38
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L240
aload 38
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
astore 39
aload 39
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L157
aload 39
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
L158:
goto L157
L240:
iconst_0
istore 9
L159:
aload 34
iload 15
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L160:
goto L234
L161:
aload 35
invokevirtual java/util/ArrayList/clear()V
aload 36
invokevirtual java/util/ArrayList/clear()V
L162:
aload 34
iload 15
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 34
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
invokevirtual java/nio/MappedByteBuffer/put([B)Ljava/nio/ByteBuffer;
pop
aload 34
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/resultText Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
getstatic com/chelpus/root/utils/odexrunpatch/print Ljava/io/PrintStream;
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/resultText Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 37
iconst_1
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/result Z
aload 37
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
aload 21
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 38
L163:
aload 38
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L241
aload 38
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
astore 39
aload 39
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L163
aload 39
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
L164:
goto L163
L241:
iconst_0
istore 6
L165:
aload 34
iload 15
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L166:
aload 34
iload 15
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L167:
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
ifne L242
iload 5
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_0
baload
if_icmpne L242
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/pattern Z
ifeq L242
aload 35
invokevirtual java/util/ArrayList/clear()V
aload 36
invokevirtual java/util/ArrayList/clear()V
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iconst_0
iaload
ifne L168
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iconst_0
iload 5
bastore
L168:
iconst_1
istore 11
L169:
aload 34
iload 15
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 34
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
L170:
iload 1
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iload 11
baload
if_icmpeq L171
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 11
iaload
iconst_1
if_icmpeq L171
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 11
iaload
bipush 20
if_icmpeq L171
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 11
iaload
bipush 21
if_icmpeq L171
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 11
iaload
bipush 23
if_icmpne L194
L171:
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iload 11
iaload
ifne L172
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iload 11
iload 1
bastore
L172:
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iload 11
iaload
bipush 20
if_icmpne L173
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iload 11
iload 1
bipush 15
iand
i2b
bastore
L173:
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iload 11
iaload
bipush 21
if_icmpne L174
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iload 11
iload 1
bipush 15
iand
bipush 16
iadd
i2b
bastore
L174:
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 11
iaload
bipush 23
if_icmpne L175
aload 35
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iload 11
baload
iload 1
invokestatic java/lang/Byte/valueOf(B)Ljava/lang/Byte;
invokevirtual java/util/ArrayList/add(ILjava/lang/Object;)V
L175:
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iload 11
iaload
bipush 23
if_icmpne L176
aload 36
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iload 11
baload
iload 11
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/ArrayList/add(ILjava/lang/Object;)V
L176:
iload 11
iconst_1
iadd
istore 11
L177:
iload 11
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
arraylength
if_icmpne L192
aload 35
invokevirtual java/util/ArrayList/size()I
ifgt L243
aload 36
invokevirtual java/util/ArrayList/size()I
ifle L189
L178:
goto L243
L179:
iload 11
aload 35
invokevirtual java/util/ArrayList/size()I
if_icmpge L188
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
aload 36
iload 11
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
aload 35
iload 11
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/Byte
invokevirtual java/lang/Byte/byteValue()B
bastore
L180:
iload 11
iconst_1
iadd
istore 11
goto L179
L181:
aload 34
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
L182:
goto L141
L183:
aload 37
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
aload 21
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 38
L184:
aload 38
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L244
aload 38
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
astore 39
aload 39
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L184
aload 39
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
L185:
goto L184
L244:
iconst_0
istore 6
L186:
aload 34
iload 15
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L187:
goto L167
L188:
aload 35
invokevirtual java/util/ArrayList/clear()V
aload 36
invokevirtual java/util/ArrayList/clear()V
L189:
aload 34
iload 15
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 34
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
invokevirtual java/nio/MappedByteBuffer/put([B)Ljava/nio/ByteBuffer;
pop
aload 34
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/resultText Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
getstatic com/chelpus/root/utils/odexrunpatch/print Ljava/io/PrintStream;
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/resultText Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 37
iconst_1
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/result Z
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L194
aload 37
iconst_1
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
aload 21
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 38
L190:
aload 38
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L194
aload 38
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
astore 39
aload 39
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
aload 37
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L190
aload 39
iconst_1
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
L191:
goto L190
L192:
aload 34
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
L193:
goto L170
L194:
aload 34
iload 15
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L195:
goto L242
L233:
iload 15
istore 10
iload 8
istore 9
iload 5
aload 25
iconst_0
baload
if_icmpne L245
iload 15
istore 10
iload 8
istore 9
L196:
getstatic com/chelpus/root/utils/odexrunpatch/pattern4 Z
ifeq L245
L197:
aload 27
iconst_0
baload
ifne L246
aload 26
iconst_0
iload 5
bastore
L246:
iconst_1
istore 11
L198:
aload 34
iload 15
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 34
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
L199:
goto L247
L248:
iload 11
iconst_1
iadd
istore 11
L200:
iload 11
aload 25
arraylength
if_icmpne L210
aload 34
invokevirtual java/nio/MappedByteBuffer/position()I
istore 10
L201:
iconst_1
istore 9
L245:
iload 9
istore 8
iload 5
bipush 16
if_icmpge L208
iload 9
istore 8
iload 9
ifeq L208
iload 9
istore 8
L202:
getstatic com/chelpus/root/utils/odexrunpatch/pattern4 Z
ifeq L208
L203:
aload 30
iconst_0
baload
ifne L249
aload 29
iconst_0
iload 5
bastore
L249:
iconst_1
istore 11
L204:
aload 34
iload 10
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 34
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
L205:
goto L250
L251:
iload 11
iconst_1
iadd
istore 11
L206:
iload 11
aload 28
arraylength
if_icmpne L212
aload 34
iload 10
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 34
aload 29
invokevirtual java/nio/MappedByteBuffer/put([B)Ljava/nio/ByteBuffer;
pop
aload 34
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
ldc "lvl patch N5!\n"
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
L207:
iconst_0
istore 8
L208:
aload 34
iload 10
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L209:
iload 12
istore 10
iload 6
istore 9
goto L83
L210:
aload 34
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
L211:
goto L247
L212:
aload 34
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
L213:
goto L250
L221:
getstatic com/chelpus/root/utils/odexrunpatch/createAPK Z
ifne L252
aload 0
iconst_3
aaload
getstatic com/chelpus/root/utils/odexrunpatch/classesFiles Ljava/util/ArrayList;
aload 0
iconst_2
aaload
getstatic com/chelpus/root/utils/odexrunpatch/uid Ljava/lang/String;
aload 0
iconst_2
aaload
getstatic com/chelpus/root/utils/odexrunpatch/uid Ljava/lang/String;
invokestatic com/chelpus/Utils/getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokestatic com/chelpus/Utils/create_ODEX_root(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
istore 6
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chelpus_return_"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 6
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
iload 6
ifne L252
getstatic com/chelpus/root/utils/odexrunpatch/ART Z
ifne L252
aload 0
iconst_1
aaload
aload 0
iconst_2
aaload
aload 0
iconst_2
aaload
iconst_1
invokestatic com/chelpus/Utils/getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;
getstatic com/chelpus/root/utils/odexrunpatch/uid Ljava/lang/String;
aload 0
iconst_3
aaload
invokestatic com/chelpus/Utils/afterPatch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
L252:
getstatic com/chelpus/root/utils/odexrunpatch/createAPK Z
ifne L253
invokestatic com/chelpus/Utils/exitFromRootJava()V
L253:
aload 20
getfield com/android/vending/billing/InAppBillingService/LUCK/LogOutputStream/allresult Ljava/lang/String;
putstatic com/chelpus/root/utils/odexrunpatch/result Ljava/lang/String;
return
L25:
astore 22
goto L23
L24:
astore 22
goto L23
L8:
astore 22
goto L22
L7:
astore 22
goto L22
L222:
iconst_0
ifeq L70
iload 16
ifne L71
goto L70
L225:
iload 7
iconst_1
iadd
istore 6
L226:
iload 6
istore 7
iload 6
sipush 380
if_icmple L227
iconst_0
istore 8
iconst_0
istore 7
goto L227
L229:
iload 1
aload 22
iload 6
baload
if_icmpeq L230
bipush 29
newarray byte
dup
iconst_0
ldc_w 0
bastore
dup
iconst_1
ldc_w 0
bastore
dup
iconst_2
ldc_w 0
bastore
dup
iconst_3
ldc_w 0
bastore
dup
iconst_4
ldc_w 0
bastore
dup
iconst_5
ldc_w 0
bastore
dup
bipush 6
ldc_w 0
bastore
dup
bipush 7
ldc_w 0
bastore
dup
bipush 8
ldc_w 0
bastore
dup
bipush 9
ldc_w 0
bastore
dup
bipush 10
ldc_w 0
bastore
dup
bipush 11
ldc_w 0
bastore
dup
bipush 12
ldc_w 0
bastore
dup
bipush 13
ldc_w 0
bastore
dup
bipush 14
ldc_w 0
bastore
dup
bipush 15
ldc_w 0
bastore
dup
bipush 16
ldc_w 1
bastore
dup
bipush 17
ldc_w 0
bastore
dup
bipush 18
ldc_w 0
bastore
dup
bipush 19
ldc_w 0
bastore
dup
bipush 20
ldc_w 1
bastore
dup
bipush 21
ldc_w 0
bastore
dup
bipush 22
ldc_w 0
bastore
dup
bipush 23
ldc_w 0
bastore
dup
bipush 24
ldc_w 1
bastore
dup
bipush 25
ldc_w 0
bastore
dup
bipush 26
ldc_w 0
bastore
dup
bipush 27
ldc_w 0
bastore
dup
bipush 28
ldc_w 1
bastore
iload 6
baload
ifeq L100
goto L230
L231:
aload 24
iload 6
baload
ifeq L254
aload 24
iload 6
baload
iconst_2
if_icmpne L255
L254:
aload 23
iload 6
iload 1
bastore
L255:
aload 24
iload 6
baload
iconst_3
if_icmpne L232
iload 6
istore 11
iload 1
istore 3
goto L232
L235:
iload 11
istore 10
iload 6
istore 9
iload 11
ifne L236
iload 6
iconst_1
iadd
istore 9
iconst_1
istore 10
goto L236
L237:
iconst_0
istore 6
goto L122
L239:
iconst_0
istore 6
goto L152
L243:
iconst_0
istore 11
goto L179
L242:
iload 14
iconst_1
iadd
istore 14
iload 10
istore 11
iload 6
istore 13
iload 9
istore 6
goto L102
L247:
iload 1
aload 25
iload 11
baload
if_icmpeq L256
iload 15
istore 10
iload 8
istore 9
bipush 32
newarray byte
dup
iconst_0
ldc_w 0
bastore
dup
iconst_1
ldc_w 1
bastore
dup
iconst_2
ldc_w 1
bastore
dup
iconst_3
ldc_w 1
bastore
dup
iconst_4
ldc_w 0
bastore
dup
iconst_5
ldc_w 1
bastore
dup
bipush 6
ldc_w 1
bastore
dup
bipush 7
ldc_w 1
bastore
dup
bipush 8
ldc_w 1
bastore
dup
bipush 9
ldc_w 1
bastore
dup
bipush 10
ldc_w 0
bastore
dup
bipush 11
ldc_w 1
bastore
dup
bipush 12
ldc_w 0
bastore
dup
bipush 13
ldc_w 1
bastore
dup
bipush 14
ldc_w 1
bastore
dup
bipush 15
ldc_w 1
bastore
dup
bipush 16
ldc_w 1
bastore
dup
bipush 17
ldc_w 1
bastore
dup
bipush 18
ldc_w 0
bastore
dup
bipush 19
ldc_w 1
bastore
dup
bipush 20
ldc_w 0
bastore
dup
bipush 21
ldc_w 1
bastore
dup
bipush 22
ldc_w 1
bastore
dup
bipush 23
ldc_w 1
bastore
dup
bipush 24
ldc_w 0
bastore
dup
bipush 25
ldc_w 1
bastore
dup
bipush 26
ldc_w 1
bastore
dup
bipush 27
ldc_w 1
bastore
dup
bipush 28
ldc_w 0
bastore
dup
bipush 29
ldc_w 1
bastore
dup
bipush 30
ldc_w 1
bastore
dup
bipush 31
ldc_w 1
bastore
iload 11
baload
iconst_1
if_icmpne L245
L256:
aload 27
iload 11
baload
ifne L248
aload 26
iload 11
iload 1
bastore
goto L248
L250:
iload 1
aload 28
iload 11
baload
if_icmpeq L257
iload 9
istore 8
bipush 13
newarray byte
dup
iconst_0
ldc_w 1
bastore
dup
iconst_1
ldc_w 0
bastore
dup
iconst_2
ldc_w 1
bastore
dup
iconst_3
ldc_w 1
bastore
dup
iconst_4
ldc_w 1
bastore
dup
iconst_5
ldc_w 0
bastore
dup
bipush 6
ldc_w 1
bastore
dup
bipush 7
ldc_w 1
bastore
dup
bipush 8
ldc_w 1
bastore
dup
bipush 9
ldc_w 1
bastore
dup
bipush 10
ldc_w 1
bastore
dup
bipush 11
ldc_w 0
bastore
dup
bipush 12
ldc_w 1
bastore
iload 11
baload
iconst_1
if_icmpne L208
L257:
aload 30
iload 11
baload
ifne L251
aload 29
iload 11
iload 1
bastore
goto L251
.limit locals 40
.limit stack 8
.end method

.method public static unzipART(Ljava/io/File;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L4 to L5 using L2
.catch java/lang/Exception from L6 to L7 using L2
.catch net/lingala/zip4j/exception/ZipException from L8 to L9 using L10
.catch java/lang/Exception from L8 to L9 using L11
.catch java/lang/Exception from L12 to L13 using L2
.catch java/lang/Exception from L13 to L14 using L2
.catch java/lang/Exception from L14 to L15 using L2
.catch java/lang/Exception from L16 to L17 using L2
.catch java/lang/Exception from L18 to L19 using L2
.catch java/lang/Exception from L20 to L21 using L2
.catch java/lang/Exception from L22 to L23 using L2
iconst_0
istore 1
L0:
new java/io/FileInputStream
dup
aload 0
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
astore 4
new java/util/zip/ZipInputStream
dup
aload 4
invokespecial java/util/zip/ZipInputStream/<init>(Ljava/io/InputStream;)V
astore 5
aload 5
invokevirtual java/util/zip/ZipInputStream/getNextEntry()Ljava/util/zip/ZipEntry;
astore 3
L1:
aload 3
ifnull L20
iconst_1
ifeq L20
L3:
aload 3
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
astore 3
aload 3
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "classes"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L13
aload 3
ldc ".dex"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L13
aload 3
ldc "/"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L13
new java/io/FileOutputStream
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/odexrunpatch/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
astore 6
sipush 2048
newarray byte
astore 7
L4:
aload 5
aload 7
invokevirtual java/util/zip/ZipInputStream/read([B)I
istore 2
L5:
iload 2
iconst_m1
if_icmpeq L12
L6:
aload 6
aload 7
iconst_0
iload 2
invokevirtual java/io/FileOutputStream/write([BII)V
L7:
goto L4
L2:
astore 3
L8:
new net/lingala/zip4j/core/ZipFile
dup
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/<init>(Ljava/io/File;)V
astore 0
aload 0
ldc "classes.dex"
getstatic com/chelpus/root/utils/odexrunpatch/sddir Ljava/lang/String;
invokevirtual net/lingala/zip4j/core/ZipFile/extractFile(Ljava/lang/String;Ljava/lang/String;)V
getstatic com/chelpus/root/utils/odexrunpatch/classesFiles Ljava/util/ArrayList;
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/odexrunpatch/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "classes.dex"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/odexrunpatch/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "classes.dex"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
aload 0
ldc "AndroidManifest.xml"
getstatic com/chelpus/root/utils/odexrunpatch/sddir Ljava/lang/String;
invokevirtual net/lingala/zip4j/core/ZipFile/extractFile(Ljava/lang/String;Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/odexrunpatch/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "AndroidManifest.xml"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L9:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception e"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
return
L12:
aload 5
invokevirtual java/util/zip/ZipInputStream/closeEntry()V
aload 6
invokevirtual java/io/FileOutputStream/close()V
getstatic com/chelpus/root/utils/odexrunpatch/classesFiles Ljava/util/ArrayList;
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/odexrunpatch/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/odexrunpatch/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L13:
aload 3
ldc "AndroidManifest.xml"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L24
new java/io/FileOutputStream
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/odexrunpatch/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "AndroidManifest.xml"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
astore 3
sipush 2048
newarray byte
astore 6
L14:
aload 5
aload 6
invokevirtual java/util/zip/ZipInputStream/read([B)I
istore 1
L15:
iload 1
iconst_m1
if_icmpeq L18
L16:
aload 3
aload 6
iconst_0
iload 1
invokevirtual java/io/FileOutputStream/write([BII)V
L17:
goto L14
L18:
aload 5
invokevirtual java/util/zip/ZipInputStream/closeEntry()V
aload 3
invokevirtual java/io/FileOutputStream/close()V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/odexrunpatch/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "AndroidManifest.xml"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L19:
iconst_1
istore 1
goto L24
L20:
aload 5
invokevirtual java/util/zip/ZipInputStream/close()V
aload 4
invokevirtual java/io/FileInputStream/close()V
L21:
return
L22:
aload 5
invokevirtual java/util/zip/ZipInputStream/getNextEntry()Ljava/util/zip/ZipEntry;
astore 3
L23:
goto L1
L10:
astore 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Error classes.dex decompress! "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception e1"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
goto L9
L11:
astore 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Error classes.dex decompress! "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception e1"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
goto L9
L24:
iconst_0
ifeq L22
iload 1
ifeq L22
goto L20
.limit locals 8
.limit stack 5
.end method

.method public static unzipSD(Ljava/io/File;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
.catch java/lang/Exception from L4 to L5 using L2
.catch java/lang/Exception from L5 to L6 using L2
.catch java/lang/Exception from L7 to L8 using L2
.catch net/lingala/zip4j/exception/ZipException from L9 to L10 using L11
.catch java/lang/Exception from L9 to L10 using L12
.catch java/lang/Exception from L13 to L14 using L2
.catch java/lang/Exception from L15 to L16 using L2
L0:
new java/io/FileInputStream
dup
aload 0
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
astore 2
new java/util/zip/ZipInputStream
dup
aload 2
invokespecial java/util/zip/ZipInputStream/<init>(Ljava/io/InputStream;)V
astore 3
L1:
aload 3
invokevirtual java/util/zip/ZipInputStream/getNextEntry()Ljava/util/zip/ZipEntry;
astore 4
L3:
aload 4
ifnull L15
L4:
aload 4
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "classes"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L1
aload 4
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
ldc ".dex"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L1
aload 4
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
ldc "/"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L1
new java/io/FileOutputStream
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/odexrunpatch/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
astore 5
sipush 1024
newarray byte
astore 6
L5:
aload 3
aload 6
invokevirtual java/util/zip/ZipInputStream/read([B)I
istore 1
L6:
iload 1
iconst_m1
if_icmpeq L13
L7:
aload 5
aload 6
iconst_0
iload 1
invokevirtual java/io/FileOutputStream/write([BII)V
L8:
goto L5
L2:
astore 2
L9:
new net/lingala/zip4j/core/ZipFile
dup
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/<init>(Ljava/io/File;)V
ldc "classes.dex"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/odexrunpatch/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual net/lingala/zip4j/core/ZipFile/extractFile(Ljava/lang/String;Ljava/lang/String;)V
getstatic com/chelpus/root/utils/odexrunpatch/classesFiles Ljava/util/ArrayList;
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/odexrunpatch/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "classes.dex"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L10:
return
L13:
aload 3
invokevirtual java/util/zip/ZipInputStream/closeEntry()V
aload 5
invokevirtual java/io/FileOutputStream/close()V
getstatic com/chelpus/root/utils/odexrunpatch/classesFiles Ljava/util/ArrayList;
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/odexrunpatch/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L14:
goto L1
L15:
aload 3
invokevirtual java/util/zip/ZipInputStream/close()V
aload 2
invokevirtual java/io/FileInputStream/close()V
L16:
return
L11:
astore 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Error classes.dex decompress! "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception e1"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
return
L12:
astore 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Error classes.dex decompress! "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception e1"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
return
.limit locals 7
.limit stack 5
.end method
