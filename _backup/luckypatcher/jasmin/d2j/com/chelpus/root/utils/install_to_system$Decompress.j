.bytecode 50.0
.class public synchronized com/chelpus/root/utils/install_to_system$Decompress
.super java/lang/Object
.inner class public static Decompress inner com/chelpus/root/utils/install_to_system$Decompress outer com/chelpus/root/utils/install_to_system

.field private '_location' Ljava/lang/String;

.field private '_zipFile' Ljava/lang/String;

.method public <init>(Ljava/lang/String;Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/chelpus/root/utils/install_to_system$Decompress/_zipFile Ljava/lang/String;
aload 0
aload 2
putfield com/chelpus/root/utils/install_to_system$Decompress/_location Ljava/lang/String;
aload 0
ldc ""
invokespecial com/chelpus/root/utils/install_to_system$Decompress/_dirChecker(Ljava/lang/String;)V
return
.limit locals 3
.limit stack 2
.end method

.method private _dirChecker(Ljava/lang/String;)V
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/chelpus/root/utils/install_to_system$Decompress/_location Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
aload 1
invokevirtual java/io/File/isDirectory()Z
ifne L0
aload 1
invokevirtual java/io/File/mkdirs()Z
pop
L0:
return
.limit locals 2
.limit stack 4
.end method

.method public unzip()V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
.catch java/lang/Exception from L4 to L5 using L2
.catch net/lingala/zip4j/exception/ZipException from L6 to L7 using L8
.catch java/lang/Exception from L6 to L7 using L9
.catch net/lingala/zip4j/exception/ZipException from L7 to L10 using L8
.catch java/lang/Exception from L7 to L10 using L9
.catch java/lang/Exception from L11 to L12 using L2
.catch java/lang/Exception from L13 to L14 using L2
.catch java/lang/Exception from L15 to L16 using L2
.catch java/lang/Exception from L17 to L18 using L2
.catch java/lang/Exception from L18 to L19 using L2
.catch java/lang/Exception from L20 to L21 using L2
.catch java/lang/Exception from L22 to L23 using L2
.catch java/lang/Exception from L24 to L25 using L2
L0:
new java/io/FileInputStream
dup
aload 0
getfield com/chelpus/root/utils/install_to_system$Decompress/_zipFile Ljava/lang/String;
invokespecial java/io/FileInputStream/<init>(Ljava/lang/String;)V
astore 4
new java/util/zip/ZipInputStream
dup
aload 4
invokespecial java/util/zip/ZipInputStream/<init>(Ljava/io/InputStream;)V
astore 5
L1:
aload 5
invokevirtual java/util/zip/ZipInputStream/getNextEntry()Ljava/util/zip/ZipEntry;
astore 6
L3:
aload 6
ifnull L24
L4:
aload 6
invokevirtual java/util/zip/ZipEntry/isDirectory()Z
ifeq L11
aload 0
aload 6
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
invokespecial com/chelpus/root/utils/install_to_system$Decompress/_dirChecker(Ljava/lang/String;)V
L5:
goto L1
L2:
astore 2
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Decompressunzip "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/Exception/printStackTrace()V
L6:
new net/lingala/zip4j/core/ZipFile
dup
aload 0
getfield com/chelpus/root/utils/install_to_system$Decompress/_zipFile Ljava/lang/String;
invokespecial net/lingala/zip4j/core/ZipFile/<init>(Ljava/lang/String;)V
astore 2
aload 2
invokevirtual net/lingala/zip4j/core/ZipFile/getFileHeaders()Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 3
L7:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L26
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast net/lingala/zip4j/model/FileHeader
astore 4
aload 4
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
ldc ".so"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L7
aload 4
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
ldc "libjnigraphics.so"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L7
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 4
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 2
aload 4
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
aload 0
getfield com/chelpus/root/utils/install_to_system$Decompress/_location Ljava/lang/String;
invokevirtual net/lingala/zip4j/core/ZipFile/extractFile(Ljava/lang/String;Ljava/lang/String;)V
L10:
goto L7
L8:
astore 2
aload 2
invokevirtual net/lingala/zip4j/exception/ZipException/printStackTrace()V
L26:
return
L11:
aload 6
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
ldc ".so"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L1
aload 6
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
ldc "libjnigraphics.so"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L1
aload 6
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
ldc "\\/+"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 7
L12:
ldc ""
astore 2
iconst_0
istore 1
L13:
iload 1
aload 7
arraylength
iconst_1
isub
if_icmpge L17
L14:
aload 2
astore 3
L15:
aload 7
iload 1
aaload
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L27
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 7
iload 1
aaload
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 3
L16:
goto L27
L17:
aload 0
aload 2
invokespecial com/chelpus/root/utils/install_to_system$Decompress/_dirChecker(Ljava/lang/String;)V
new java/io/FileOutputStream
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/chelpus/root/utils/install_to_system$Decompress/_location Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
astore 2
sipush 1024
newarray byte
astore 3
L18:
aload 5
aload 3
invokevirtual java/util/zip/ZipInputStream/read([B)I
istore 1
L19:
iload 1
iconst_m1
if_icmpeq L22
L20:
aload 2
aload 3
iconst_0
iload 1
invokevirtual java/io/FileOutputStream/write([BII)V
L21:
goto L18
L22:
aload 5
invokevirtual java/util/zip/ZipInputStream/closeEntry()V
aload 2
invokevirtual java/io/FileOutputStream/close()V
L23:
goto L1
L24:
aload 5
invokevirtual java/util/zip/ZipInputStream/close()V
aload 4
invokevirtual java/io/FileInputStream/close()V
L25:
return
L9:
astore 2
aload 2
invokevirtual java/lang/Exception/printStackTrace()V
return
L27:
iload 1
iconst_1
iadd
istore 1
aload 3
astore 2
goto L13
.limit locals 8
.limit stack 4
.end method
