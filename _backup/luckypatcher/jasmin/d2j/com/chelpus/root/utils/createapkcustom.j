.bytecode 50.0
.class public synchronized com/chelpus/root/utils/createapkcustom
.super java/lang/Object
.inner class public static Decompress inner com/chelpus/root/utils/createapkcustom$Decompress outer com/chelpus/root/utils/createapkcustom

.field static final 'BUFFER' I = 2048


.field private static final 'all' I = 4


.field public static 'appdir' Ljava/lang/String;

.field private static final 'armeabi' I = 0


.field private static final 'armeabiv7a' I = 1


.field private static final 'beginTag' I = 0


.field public static 'classesFiles' Ljava/util/ArrayList; signature "Ljava/util/ArrayList<Ljava/io/File;>;"

.field private static final 'classesTag' I = 1


.field public static 'crkapk' Ljava/io/File;

.field public static 'dir' Ljava/lang/String;

.field public static 'dir2' Ljava/lang/String;

.field private static final 'endTag' I = 4


.field private static final 'fileInApkTag' I = 10


.field public static 'goodResult' Z = 0


.field private static 'group' Ljava/lang/String;

.field private static final 'libTagALL' I = 2


.field private static final 'libTagARMEABI' I = 6


.field private static final 'libTagARMEABIV7A' I = 7


.field private static final 'libTagMIPS' I = 8


.field private static final 'libTagx86' I = 9


.field private static 'libs' Ljava/util/ArrayList; signature "Ljava/util/ArrayList<Ljava/lang/String;>;"

.field public static 'localFile2' Ljava/io/File;

.field public static 'manualpatch' Z = 0


.field private static final 'mips' I = 2


.field public static 'multidex' Z = 0


.field public static 'multilib_patch' Z = 0


.field public static 'packageName' Ljava/lang/String;

.field private static final 'packageTag' I = 5


.field private static 'pat' Ljava/util/ArrayList; signature "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;>;"

.field private static 'patchedLibs' Ljava/util/ArrayList; signature "Ljava/util/ArrayList<Ljava/lang/String;>;"

.field public static 'patchteil' Z = 0


.field private static 'print' Ljava/io/PrintStream;

.field public static 'sddir' Ljava/lang/String;

.field private static 'search' Ljava/util/ArrayList; signature "Ljava/util/ArrayList<Ljava/lang/Byte;>;"

.field private static 'ser' Ljava/util/ArrayList; signature "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;>;"

.field public static 'tag' I = 0


.field public static 'tooldir' Ljava/lang/String;

.field public static 'unpack' Z = 0


.field private static final 'x86' I = 3


.method static <clinit>()V
aconst_null
putstatic com/chelpus/root/utils/createapkcustom/pat Ljava/util/ArrayList;
aconst_null
putstatic com/chelpus/root/utils/createapkcustom/ser Ljava/util/ArrayList;
aconst_null
putstatic com/chelpus/root/utils/createapkcustom/search Ljava/util/ArrayList;
iconst_0
putstatic com/chelpus/root/utils/createapkcustom/patchteil Z
iconst_0
putstatic com/chelpus/root/utils/createapkcustom/unpack Z
iconst_0
putstatic com/chelpus/root/utils/createapkcustom/manualpatch Z
ldc "/sdcard/"
putstatic com/chelpus/root/utils/createapkcustom/dir Ljava/lang/String;
ldc "/sdcard/"
putstatic com/chelpus/root/utils/createapkcustom/dir2 Ljava/lang/String;
ldc "/sdcard/"
putstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
ldc "/sdcard/"
putstatic com/chelpus/root/utils/createapkcustom/appdir Ljava/lang/String;
ldc "/sdcard/"
putstatic com/chelpus/root/utils/createapkcustom/tooldir Ljava/lang/String;
ldc ""
putstatic com/chelpus/root/utils/createapkcustom/packageName Ljava/lang/String;
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putstatic com/chelpus/root/utils/createapkcustom/libs Ljava/util/ArrayList;
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putstatic com/chelpus/root/utils/createapkcustom/patchedLibs Ljava/util/ArrayList;
ldc ""
putstatic com/chelpus/root/utils/createapkcustom/group Ljava/lang/String;
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putstatic com/chelpus/root/utils/createapkcustom/classesFiles Ljava/util/ArrayList;
iconst_0
putstatic com/chelpus/root/utils/createapkcustom/multidex Z
iconst_0
putstatic com/chelpus/root/utils/createapkcustom/goodResult Z
iconst_0
putstatic com/chelpus/root/utils/createapkcustom/multilib_patch Z
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$000()Ljava/io/PrintStream;
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static final calcChecksum([BI)V
new java/util/zip/Adler32
dup
invokespecial java/util/zip/Adler32/<init>()V
astore 3
aload 3
aload 0
bipush 12
aload 0
arraylength
iload 1
bipush 12
iadd
isub
invokevirtual java/util/zip/Adler32/update([BII)V
aload 3
invokevirtual java/util/zip/Adler32/getValue()J
l2i
istore 2
aload 0
iload 1
bipush 8
iadd
iload 2
i2b
bastore
aload 0
iload 1
bipush 9
iadd
iload 2
bipush 8
ishr
i2b
bastore
aload 0
iload 1
bipush 10
iadd
iload 2
bipush 16
ishr
i2b
bastore
aload 0
iload 1
bipush 11
iadd
iload 2
bipush 24
ishr
i2b
bastore
return
.limit locals 4
.limit stack 6
.end method

.method private static final calcSignature([BI)V
.catch java/security/NoSuchAlgorithmException from L0 to L1 using L2
.catch java/security/DigestException from L3 to L4 using L5
.catch java/security/DigestException from L6 to L5 using L5
L0:
ldc "SHA-1"
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
astore 2
L1:
aload 2
aload 0
bipush 32
aload 0
arraylength
iload 1
bipush 32
iadd
isub
invokevirtual java/security/MessageDigest/update([BII)V
L3:
aload 2
aload 0
iload 1
bipush 12
iadd
bipush 20
invokevirtual java/security/MessageDigest/digest([BII)I
istore 1
L4:
iload 1
bipush 20
if_icmpeq L7
L6:
new java/lang/RuntimeException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "unexpected digest write:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc "bytes"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L5:
astore 0
new java/lang/RuntimeException
dup
aload 0
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
L2:
astore 0
new java/lang/RuntimeException
dup
aload 0
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
L7:
return
.limit locals 3
.limit stack 6
.end method

.method public static clearTemp()V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
L0:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/classes.dex.apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
aload 0
invokevirtual java/io/File/exists()Z
ifeq L1
aload 0
invokevirtual java/io/File/delete()Z
pop
L1:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/tmp/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
new com/chelpus/Utils
dup
ldc "createcustompatch"
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
aload 0
invokevirtual com/chelpus/Utils/deleteFolder(Ljava/io/File;)V
L3:
return
L2:
astore 0
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
return
.limit locals 1
.limit stack 4
.end method

.method public static extractFile(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;
new com/chelpus/root/utils/createapkcustom$Decompress
dup
aload 0
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/tmp/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial com/chelpus/root/utils/createapkcustom$Decompress/<init>(Ljava/lang/String;Ljava/lang/String;)V
aload 1
invokevirtual com/chelpus/root/utils/createapkcustom$Decompress/unzip(Ljava/lang/String;)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 5
.end method

.method public static extractLibs(Ljava/io/File;)V
aload 0
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
astore 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/tmp/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/tmp/lib/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifne L0
new com/chelpus/root/utils/createapkcustom$Decompress
dup
aload 0
aload 1
invokespecial com/chelpus/root/utils/createapkcustom$Decompress/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual com/chelpus/root/utils/createapkcustom$Decompress/unzip()V
L0:
return
.limit locals 2
.limit stack 4
.end method

.method public static fixadler(Ljava/io/File;)V
.catch java/lang/Exception from L0 to L1 using L2
L0:
new java/io/FileInputStream
dup
aload 0
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
astore 2
aload 2
invokevirtual java/io/FileInputStream/available()I
newarray byte
astore 1
aload 2
aload 1
invokevirtual java/io/FileInputStream/read([B)I
pop
aload 1
iconst_0
invokestatic com/chelpus/root/utils/createapkcustom/calcSignature([BI)V
aload 1
iconst_0
invokestatic com/chelpus/root/utils/createapkcustom/calcChecksum([BI)V
aload 2
invokevirtual java/io/FileInputStream/close()V
new java/io/FileOutputStream
dup
aload 0
invokespecial java/io/FileOutputStream/<init>(Ljava/io/File;)V
astore 0
aload 0
aload 1
invokevirtual java/io/FileOutputStream/write([B)V
aload 0
invokevirtual java/io/FileOutputStream/close()V
L1:
return
L2:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
return
.limit locals 3
.limit stack 3
.end method

.method public static getClassesDex()V
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
.catch java/io/FileNotFoundException from L1 to L2 using L2
.catch java/lang/Exception from L1 to L2 using L3
.catch java/io/FileNotFoundException from L4 to L5 using L2
.catch java/lang/Exception from L4 to L5 using L3
.catch java/io/FileNotFoundException from L5 to L3 using L2
.catch java/lang/Exception from L5 to L3 using L3
L0:
new java/io/File
dup
getstatic com/chelpus/root/utils/createapkcustom/appdir Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/createapkcustom/packageName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
putstatic com/chelpus/root/utils/createapkcustom/crkapk Ljava/io/File;
aload 0
getstatic com/chelpus/root/utils/createapkcustom/crkapk Ljava/io/File;
invokestatic com/chelpus/Utils/copyFile(Ljava/io/File;Ljava/io/File;)V
getstatic com/chelpus/root/utils/createapkcustom/crkapk Ljava/io/File;
invokestatic com/chelpus/root/utils/createapkcustom/unzip(Ljava/io/File;)V
getstatic com/chelpus/root/utils/createapkcustom/classesFiles Ljava/util/ArrayList;
ifnull L1
getstatic com/chelpus/root/utils/createapkcustom/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifne L4
L1:
new java/io/FileNotFoundException
dup
invokespecial java/io/FileNotFoundException/<init>()V
athrow
L2:
astore 0
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "Error LP: unzip classes.dex fault!\n\n"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L6:
return
L4:
getstatic com/chelpus/root/utils/createapkcustom/classesFiles Ljava/util/ArrayList;
ifnull L6
getstatic com/chelpus/root/utils/createapkcustom/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifle L6
getstatic com/chelpus/root/utils/createapkcustom/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 0
L5:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L6
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
invokevirtual java/io/File/exists()Z
ifne L5
new java/io/FileNotFoundException
dup
invokespecial java/io/FileNotFoundException/<init>()V
athrow
L3:
astore 0
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Extract classes.dex error: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
return
.limit locals 1
.limit stack 4
.end method

.method public static getFileFromApk(Ljava/lang/String;)Ljava/lang/String;
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
L0:
new java/io/File
dup
getstatic com/chelpus/root/utils/createapkcustom/appdir Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/createapkcustom/packageName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
putstatic com/chelpus/root/utils/createapkcustom/crkapk Ljava/io/File;
getstatic com/chelpus/root/utils/createapkcustom/crkapk Ljava/io/File;
invokevirtual java/io/File/exists()Z
ifne L1
aload 1
getstatic com/chelpus/root/utils/createapkcustom/crkapk Ljava/io/File;
invokestatic com/chelpus/Utils/copyFile(Ljava/io/File;Ljava/io/File;)V
L1:
getstatic com/chelpus/root/utils/createapkcustom/crkapk Ljava/io/File;
aload 0
invokestatic com/chelpus/root/utils/createapkcustom/extractFile(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;
astore 0
L3:
aload 0
areturn
L2:
astore 0
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Lib select error: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
ldc ""
areturn
.limit locals 2
.limit stack 4
.end method

.method public static main([Ljava/lang/String;)Ljava/lang/String;
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch java/io/IOException from L0 to L1 using L3
.catch java/lang/InterruptedException from L0 to L1 using L4
.catch java/io/FileNotFoundException from L5 to L6 using L2
.catch java/io/IOException from L5 to L6 using L3
.catch java/lang/InterruptedException from L5 to L6 using L4
.catch java/io/FileNotFoundException from L7 to L8 using L2
.catch java/io/IOException from L7 to L8 using L3
.catch java/lang/InterruptedException from L7 to L8 using L4
.catch java/io/FileNotFoundException from L9 to L10 using L2
.catch java/io/IOException from L9 to L10 using L3
.catch java/lang/InterruptedException from L9 to L10 using L4
.catch java/io/FileNotFoundException from L11 to L12 using L2
.catch java/io/IOException from L11 to L12 using L3
.catch java/lang/InterruptedException from L11 to L12 using L4
.catch java/io/FileNotFoundException from L13 to L14 using L2
.catch java/io/IOException from L13 to L14 using L3
.catch java/lang/InterruptedException from L13 to L14 using L4
.catch java/io/FileNotFoundException from L14 to L15 using L2
.catch java/io/IOException from L14 to L15 using L3
.catch java/lang/InterruptedException from L14 to L15 using L4
.catch java/io/FileNotFoundException from L16 to L17 using L2
.catch java/io/IOException from L16 to L17 using L3
.catch java/lang/InterruptedException from L16 to L17 using L4
.catch java/io/FileNotFoundException from L18 to L19 using L2
.catch java/io/IOException from L18 to L19 using L3
.catch java/lang/InterruptedException from L18 to L19 using L4
.catch java/io/FileNotFoundException from L20 to L21 using L2
.catch java/io/IOException from L20 to L21 using L3
.catch java/lang/InterruptedException from L20 to L21 using L4
.catch java/io/FileNotFoundException from L22 to L23 using L2
.catch java/io/IOException from L22 to L23 using L3
.catch java/lang/InterruptedException from L22 to L23 using L4
.catch java/io/FileNotFoundException from L24 to L25 using L2
.catch java/io/IOException from L24 to L25 using L3
.catch java/lang/InterruptedException from L24 to L25 using L4
.catch java/io/FileNotFoundException from L26 to L27 using L2
.catch java/io/IOException from L26 to L27 using L3
.catch java/lang/InterruptedException from L26 to L27 using L4
.catch java/io/FileNotFoundException from L27 to L28 using L2
.catch java/io/IOException from L27 to L28 using L3
.catch java/lang/InterruptedException from L27 to L28 using L4
.catch java/io/FileNotFoundException from L28 to L29 using L2
.catch java/io/IOException from L28 to L29 using L3
.catch java/lang/InterruptedException from L28 to L29 using L4
.catch java/io/FileNotFoundException from L30 to L31 using L2
.catch java/io/IOException from L30 to L31 using L3
.catch java/lang/InterruptedException from L30 to L31 using L4
.catch org/json/JSONException from L31 to L32 using L33
.catch java/io/FileNotFoundException from L31 to L32 using L2
.catch java/io/IOException from L31 to L32 using L3
.catch java/lang/InterruptedException from L31 to L32 using L4
.catch java/io/FileNotFoundException from L34 to L35 using L2
.catch java/io/IOException from L34 to L35 using L3
.catch java/lang/InterruptedException from L34 to L35 using L4
.catch java/io/FileNotFoundException from L36 to L37 using L2
.catch java/io/IOException from L36 to L37 using L3
.catch java/lang/InterruptedException from L36 to L37 using L4
.catch org/json/JSONException from L37 to L38 using L39
.catch java/io/FileNotFoundException from L37 to L38 using L2
.catch java/io/IOException from L37 to L38 using L3
.catch java/lang/InterruptedException from L37 to L38 using L4
.catch java/io/FileNotFoundException from L38 to L40 using L2
.catch java/io/IOException from L38 to L40 using L3
.catch java/lang/InterruptedException from L38 to L40 using L4
.catch java/io/FileNotFoundException from L41 to L42 using L2
.catch java/io/IOException from L41 to L42 using L3
.catch java/lang/InterruptedException from L41 to L42 using L4
.catch java/io/FileNotFoundException from L43 to L44 using L2
.catch java/io/IOException from L43 to L44 using L3
.catch java/lang/InterruptedException from L43 to L44 using L4
.catch java/io/FileNotFoundException from L45 to L46 using L2
.catch java/io/IOException from L45 to L46 using L3
.catch java/lang/InterruptedException from L45 to L46 using L4
.catch java/io/FileNotFoundException from L47 to L48 using L2
.catch java/io/IOException from L47 to L48 using L3
.catch java/lang/InterruptedException from L47 to L48 using L4
.catch java/io/FileNotFoundException from L49 to L50 using L2
.catch java/io/IOException from L49 to L50 using L3
.catch java/lang/InterruptedException from L49 to L50 using L4
.catch java/io/FileNotFoundException from L51 to L52 using L2
.catch java/io/IOException from L51 to L52 using L3
.catch java/lang/InterruptedException from L51 to L52 using L4
.catch java/io/FileNotFoundException from L53 to L54 using L2
.catch java/io/IOException from L53 to L54 using L3
.catch java/lang/InterruptedException from L53 to L54 using L4
.catch org/json/JSONException from L55 to L56 using L57
.catch java/io/FileNotFoundException from L55 to L56 using L2
.catch java/io/IOException from L55 to L56 using L3
.catch java/lang/InterruptedException from L55 to L56 using L4
.catch java/io/FileNotFoundException from L58 to L59 using L2
.catch java/io/IOException from L58 to L59 using L3
.catch java/lang/InterruptedException from L58 to L59 using L4
.catch java/io/FileNotFoundException from L60 to L61 using L2
.catch java/io/IOException from L60 to L61 using L3
.catch java/lang/InterruptedException from L60 to L61 using L4
.catch org/json/JSONException from L62 to L63 using L64
.catch java/io/FileNotFoundException from L62 to L63 using L2
.catch java/io/IOException from L62 to L63 using L3
.catch java/lang/InterruptedException from L62 to L63 using L4
.catch java/io/FileNotFoundException from L65 to L66 using L2
.catch java/io/IOException from L65 to L66 using L3
.catch java/lang/InterruptedException from L65 to L66 using L4
.catch java/lang/Exception from L67 to L68 using L69
.catch java/io/FileNotFoundException from L67 to L68 using L2
.catch java/io/IOException from L67 to L68 using L3
.catch java/lang/InterruptedException from L67 to L68 using L4
.catch java/lang/Exception from L70 to L71 using L69
.catch java/io/FileNotFoundException from L70 to L71 using L2
.catch java/io/IOException from L70 to L71 using L3
.catch java/lang/InterruptedException from L70 to L71 using L4
.catch java/lang/Exception from L72 to L73 using L69
.catch java/io/FileNotFoundException from L72 to L73 using L2
.catch java/io/IOException from L72 to L73 using L3
.catch java/lang/InterruptedException from L72 to L73 using L4
.catch java/lang/Exception from L74 to L75 using L69
.catch java/io/FileNotFoundException from L74 to L75 using L2
.catch java/io/IOException from L74 to L75 using L3
.catch java/lang/InterruptedException from L74 to L75 using L4
.catch java/lang/Exception from L76 to L77 using L69
.catch java/io/FileNotFoundException from L76 to L77 using L2
.catch java/io/IOException from L76 to L77 using L3
.catch java/lang/InterruptedException from L76 to L77 using L4
.catch java/lang/Exception from L78 to L79 using L69
.catch java/io/FileNotFoundException from L78 to L79 using L2
.catch java/io/IOException from L78 to L79 using L3
.catch java/lang/InterruptedException from L78 to L79 using L4
.catch java/lang/Exception from L80 to L81 using L69
.catch java/io/FileNotFoundException from L80 to L81 using L2
.catch java/io/IOException from L80 to L81 using L3
.catch java/lang/InterruptedException from L80 to L81 using L4
.catch java/lang/Exception from L82 to L83 using L69
.catch java/io/FileNotFoundException from L82 to L83 using L2
.catch java/io/IOException from L82 to L83 using L3
.catch java/lang/InterruptedException from L82 to L83 using L4
.catch java/lang/Exception from L84 to L85 using L69
.catch java/io/FileNotFoundException from L84 to L85 using L2
.catch java/io/IOException from L84 to L85 using L3
.catch java/lang/InterruptedException from L84 to L85 using L4
.catch java/lang/Exception from L86 to L87 using L69
.catch java/io/FileNotFoundException from L86 to L87 using L2
.catch java/io/IOException from L86 to L87 using L3
.catch java/lang/InterruptedException from L86 to L87 using L4
.catch java/lang/Exception from L88 to L89 using L69
.catch java/io/FileNotFoundException from L88 to L89 using L2
.catch java/io/IOException from L88 to L89 using L3
.catch java/lang/InterruptedException from L88 to L89 using L4
.catch java/io/FileNotFoundException from L90 to L91 using L2
.catch java/io/IOException from L90 to L91 using L3
.catch java/lang/InterruptedException from L90 to L91 using L4
.catch java/io/FileNotFoundException from L92 to L93 using L2
.catch java/io/IOException from L92 to L93 using L3
.catch java/lang/InterruptedException from L92 to L93 using L4
.catch java/io/FileNotFoundException from L94 to L95 using L2
.catch java/io/IOException from L94 to L95 using L3
.catch java/lang/InterruptedException from L94 to L95 using L4
.catch java/io/FileNotFoundException from L95 to L96 using L2
.catch java/io/IOException from L95 to L96 using L3
.catch java/lang/InterruptedException from L95 to L96 using L4
.catch java/io/FileNotFoundException from L97 to L98 using L2
.catch java/io/IOException from L97 to L98 using L3
.catch java/lang/InterruptedException from L97 to L98 using L4
.catch java/io/FileNotFoundException from L99 to L100 using L2
.catch java/io/IOException from L99 to L100 using L3
.catch java/lang/InterruptedException from L99 to L100 using L4
.catch java/io/FileNotFoundException from L101 to L102 using L2
.catch java/io/IOException from L101 to L102 using L3
.catch java/lang/InterruptedException from L101 to L102 using L4
.catch java/io/IOException from L103 to L104 using L105
.catch java/io/FileNotFoundException from L106 to L107 using L2
.catch java/io/IOException from L106 to L107 using L3
.catch java/lang/InterruptedException from L106 to L107 using L4
.catch java/io/FileNotFoundException from L107 to L108 using L2
.catch java/io/IOException from L107 to L108 using L3
.catch java/lang/InterruptedException from L107 to L108 using L4
.catch java/io/FileNotFoundException from L109 to L110 using L2
.catch java/io/IOException from L109 to L110 using L3
.catch java/lang/InterruptedException from L109 to L110 using L4
.catch java/io/FileNotFoundException from L111 to L112 using L2
.catch java/io/IOException from L111 to L112 using L3
.catch java/lang/InterruptedException from L111 to L112 using L4
.catch java/io/FileNotFoundException from L112 to L113 using L2
.catch java/io/IOException from L112 to L113 using L3
.catch java/lang/InterruptedException from L112 to L113 using L4
.catch java/io/FileNotFoundException from L113 to L114 using L2
.catch java/io/IOException from L113 to L114 using L3
.catch java/lang/InterruptedException from L113 to L114 using L4
.catch java/io/FileNotFoundException from L115 to L116 using L2
.catch java/io/IOException from L115 to L116 using L3
.catch java/lang/InterruptedException from L115 to L116 using L4
.catch java/io/FileNotFoundException from L117 to L118 using L2
.catch java/io/IOException from L117 to L118 using L3
.catch java/lang/InterruptedException from L117 to L118 using L4
.catch java/io/FileNotFoundException from L119 to L120 using L2
.catch java/io/IOException from L119 to L120 using L3
.catch java/lang/InterruptedException from L119 to L120 using L4
.catch java/io/FileNotFoundException from L120 to L121 using L2
.catch java/io/IOException from L120 to L121 using L3
.catch java/lang/InterruptedException from L120 to L121 using L4
.catch java/io/FileNotFoundException from L121 to L122 using L2
.catch java/io/IOException from L121 to L122 using L3
.catch java/lang/InterruptedException from L121 to L122 using L4
.catch java/io/FileNotFoundException from L123 to L124 using L2
.catch java/io/IOException from L123 to L124 using L3
.catch java/lang/InterruptedException from L123 to L124 using L4
.catch java/io/FileNotFoundException from L125 to L126 using L2
.catch java/io/IOException from L125 to L126 using L3
.catch java/lang/InterruptedException from L125 to L126 using L4
.catch java/io/FileNotFoundException from L127 to L128 using L2
.catch java/io/IOException from L127 to L128 using L3
.catch java/lang/InterruptedException from L127 to L128 using L4
.catch java/io/FileNotFoundException from L128 to L129 using L2
.catch java/io/IOException from L128 to L129 using L3
.catch java/lang/InterruptedException from L128 to L129 using L4
.catch java/io/FileNotFoundException from L129 to L130 using L2
.catch java/io/IOException from L129 to L130 using L3
.catch java/lang/InterruptedException from L129 to L130 using L4
.catch java/io/FileNotFoundException from L131 to L132 using L2
.catch java/io/IOException from L131 to L132 using L3
.catch java/lang/InterruptedException from L131 to L132 using L4
.catch java/io/FileNotFoundException from L133 to L134 using L2
.catch java/io/IOException from L133 to L134 using L3
.catch java/lang/InterruptedException from L133 to L134 using L4
.catch java/io/FileNotFoundException from L135 to L136 using L2
.catch java/io/IOException from L135 to L136 using L3
.catch java/lang/InterruptedException from L135 to L136 using L4
.catch java/io/FileNotFoundException from L136 to L137 using L2
.catch java/io/IOException from L136 to L137 using L3
.catch java/lang/InterruptedException from L136 to L137 using L4
.catch java/io/FileNotFoundException from L137 to L138 using L2
.catch java/io/IOException from L137 to L138 using L3
.catch java/lang/InterruptedException from L137 to L138 using L4
.catch java/io/FileNotFoundException from L139 to L140 using L2
.catch java/io/IOException from L139 to L140 using L3
.catch java/lang/InterruptedException from L139 to L140 using L4
.catch java/io/FileNotFoundException from L141 to L142 using L2
.catch java/io/IOException from L141 to L142 using L3
.catch java/lang/InterruptedException from L141 to L142 using L4
.catch java/io/FileNotFoundException from L143 to L144 using L2
.catch java/io/IOException from L143 to L144 using L3
.catch java/lang/InterruptedException from L143 to L144 using L4
.catch java/io/FileNotFoundException from L144 to L145 using L2
.catch java/io/IOException from L144 to L145 using L3
.catch java/lang/InterruptedException from L144 to L145 using L4
.catch java/io/FileNotFoundException from L145 to L146 using L2
.catch java/io/IOException from L145 to L146 using L3
.catch java/lang/InterruptedException from L145 to L146 using L4
.catch java/io/FileNotFoundException from L147 to L148 using L2
.catch java/io/IOException from L147 to L148 using L3
.catch java/lang/InterruptedException from L147 to L148 using L4
.catch java/io/FileNotFoundException from L149 to L150 using L2
.catch java/io/IOException from L149 to L150 using L3
.catch java/lang/InterruptedException from L149 to L150 using L4
.catch java/io/FileNotFoundException from L151 to L152 using L2
.catch java/io/IOException from L151 to L152 using L3
.catch java/lang/InterruptedException from L151 to L152 using L4
.catch java/io/FileNotFoundException from L153 to L154 using L2
.catch java/io/IOException from L153 to L154 using L3
.catch java/lang/InterruptedException from L153 to L154 using L4
.catch java/io/FileNotFoundException from L155 to L156 using L2
.catch java/io/IOException from L155 to L156 using L3
.catch java/lang/InterruptedException from L155 to L156 using L4
.catch java/io/FileNotFoundException from L157 to L158 using L2
.catch java/io/IOException from L157 to L158 using L3
.catch java/lang/InterruptedException from L157 to L158 using L4
.catch java/io/FileNotFoundException from L159 to L160 using L2
.catch java/io/IOException from L159 to L160 using L3
.catch java/lang/InterruptedException from L159 to L160 using L4
.catch java/io/FileNotFoundException from L161 to L162 using L2
.catch java/io/IOException from L161 to L162 using L3
.catch java/lang/InterruptedException from L161 to L162 using L4
.catch java/io/FileNotFoundException from L163 to L164 using L2
.catch java/io/IOException from L163 to L164 using L3
.catch java/lang/InterruptedException from L163 to L164 using L4
.catch java/io/FileNotFoundException from L165 to L166 using L2
.catch java/io/IOException from L165 to L166 using L3
.catch java/lang/InterruptedException from L165 to L166 using L4
.catch java/io/FileNotFoundException from L167 to L168 using L2
.catch java/io/IOException from L167 to L168 using L3
.catch java/lang/InterruptedException from L167 to L168 using L4
.catch java/io/FileNotFoundException from L169 to L170 using L2
.catch java/io/IOException from L169 to L170 using L3
.catch java/lang/InterruptedException from L169 to L170 using L4
.catch java/io/FileNotFoundException from L171 to L172 using L2
.catch java/io/IOException from L171 to L172 using L3
.catch java/lang/InterruptedException from L171 to L172 using L4
.catch java/io/FileNotFoundException from L173 to L174 using L2
.catch java/io/IOException from L173 to L174 using L3
.catch java/lang/InterruptedException from L173 to L174 using L4
.catch org/json/JSONException from L175 to L176 using L177
.catch java/io/FileNotFoundException from L175 to L176 using L2
.catch java/io/IOException from L175 to L176 using L3
.catch java/lang/InterruptedException from L175 to L176 using L4
.catch java/io/FileNotFoundException from L178 to L179 using L2
.catch java/io/IOException from L178 to L179 using L3
.catch java/lang/InterruptedException from L178 to L179 using L4
.catch java/io/FileNotFoundException from L180 to L181 using L2
.catch java/io/IOException from L180 to L181 using L3
.catch java/lang/InterruptedException from L180 to L181 using L4
.catch java/io/FileNotFoundException from L182 to L183 using L2
.catch java/io/IOException from L182 to L183 using L3
.catch java/lang/InterruptedException from L182 to L183 using L4
.catch org/json/JSONException from L184 to L185 using L186
.catch java/io/FileNotFoundException from L184 to L185 using L2
.catch java/io/IOException from L184 to L185 using L3
.catch java/lang/InterruptedException from L184 to L185 using L4
.catch java/io/FileNotFoundException from L185 to L187 using L2
.catch java/io/IOException from L185 to L187 using L3
.catch java/lang/InterruptedException from L185 to L187 using L4
.catch java/lang/Exception from L188 to L189 using L190
.catch java/io/FileNotFoundException from L188 to L189 using L2
.catch java/io/IOException from L188 to L189 using L3
.catch java/lang/InterruptedException from L188 to L189 using L4
.catch java/lang/Exception from L191 to L192 using L190
.catch java/io/FileNotFoundException from L191 to L192 using L2
.catch java/io/IOException from L191 to L192 using L3
.catch java/lang/InterruptedException from L191 to L192 using L4
.catch java/lang/Exception from L193 to L194 using L190
.catch java/io/FileNotFoundException from L193 to L194 using L2
.catch java/io/IOException from L193 to L194 using L3
.catch java/lang/InterruptedException from L193 to L194 using L4
.catch java/lang/Exception from L195 to L196 using L190
.catch java/io/FileNotFoundException from L195 to L196 using L2
.catch java/io/IOException from L195 to L196 using L3
.catch java/lang/InterruptedException from L195 to L196 using L4
.catch java/lang/Exception from L197 to L198 using L190
.catch java/io/FileNotFoundException from L197 to L198 using L2
.catch java/io/IOException from L197 to L198 using L3
.catch java/lang/InterruptedException from L197 to L198 using L4
.catch java/lang/Exception from L199 to L200 using L190
.catch java/io/FileNotFoundException from L199 to L200 using L2
.catch java/io/IOException from L199 to L200 using L3
.catch java/lang/InterruptedException from L199 to L200 using L4
.catch java/lang/Exception from L201 to L202 using L190
.catch java/io/FileNotFoundException from L201 to L202 using L2
.catch java/io/IOException from L201 to L202 using L3
.catch java/lang/InterruptedException from L201 to L202 using L4
.catch java/lang/Exception from L203 to L204 using L190
.catch java/io/FileNotFoundException from L203 to L204 using L2
.catch java/io/IOException from L203 to L204 using L3
.catch java/lang/InterruptedException from L203 to L204 using L4
.catch java/io/FileNotFoundException from L205 to L206 using L2
.catch java/io/IOException from L205 to L206 using L3
.catch java/lang/InterruptedException from L205 to L206 using L4
.catch java/io/FileNotFoundException from L207 to L208 using L2
.catch java/io/IOException from L207 to L208 using L3
.catch java/lang/InterruptedException from L207 to L208 using L4
.catch java/io/FileNotFoundException from L209 to L210 using L2
.catch java/io/IOException from L209 to L210 using L3
.catch java/lang/InterruptedException from L209 to L210 using L4
.catch java/io/FileNotFoundException from L211 to L212 using L2
.catch java/io/IOException from L211 to L212 using L3
.catch java/lang/InterruptedException from L211 to L212 using L4
.catch java/io/FileNotFoundException from L213 to L214 using L2
.catch java/io/IOException from L213 to L214 using L3
.catch java/lang/InterruptedException from L213 to L214 using L4
.catch java/io/FileNotFoundException from L215 to L216 using L2
.catch java/io/IOException from L215 to L216 using L3
.catch java/lang/InterruptedException from L215 to L216 using L4
.catch org/json/JSONException from L217 to L218 using L219
.catch java/io/FileNotFoundException from L217 to L218 using L2
.catch java/io/IOException from L217 to L218 using L3
.catch java/lang/InterruptedException from L217 to L218 using L4
.catch java/io/FileNotFoundException from L220 to L221 using L2
.catch java/io/IOException from L220 to L221 using L3
.catch java/lang/InterruptedException from L220 to L221 using L4
.catch java/lang/Exception from L222 to L223 using L224
.catch java/io/FileNotFoundException from L222 to L223 using L2
.catch java/io/IOException from L222 to L223 using L3
.catch java/lang/InterruptedException from L222 to L223 using L4
.catch java/lang/Exception from L225 to L226 using L224
.catch java/io/FileNotFoundException from L225 to L226 using L2
.catch java/io/IOException from L225 to L226 using L3
.catch java/lang/InterruptedException from L225 to L226 using L4
.catch java/lang/Exception from L227 to L228 using L224
.catch java/io/FileNotFoundException from L227 to L228 using L2
.catch java/io/IOException from L227 to L228 using L3
.catch java/lang/InterruptedException from L227 to L228 using L4
.catch java/lang/Exception from L229 to L230 using L224
.catch java/io/FileNotFoundException from L229 to L230 using L2
.catch java/io/IOException from L229 to L230 using L3
.catch java/lang/InterruptedException from L229 to L230 using L4
.catch java/lang/Exception from L231 to L232 using L224
.catch java/io/FileNotFoundException from L231 to L232 using L2
.catch java/io/IOException from L231 to L232 using L3
.catch java/lang/InterruptedException from L231 to L232 using L4
.catch java/lang/Exception from L233 to L234 using L224
.catch java/io/FileNotFoundException from L233 to L234 using L2
.catch java/io/IOException from L233 to L234 using L3
.catch java/lang/InterruptedException from L233 to L234 using L4
.catch java/lang/Exception from L235 to L236 using L224
.catch java/io/FileNotFoundException from L235 to L236 using L2
.catch java/io/IOException from L235 to L236 using L3
.catch java/lang/InterruptedException from L235 to L236 using L4
.catch java/lang/Exception from L237 to L238 using L224
.catch java/io/FileNotFoundException from L237 to L238 using L2
.catch java/io/IOException from L237 to L238 using L3
.catch java/lang/InterruptedException from L237 to L238 using L4
.catch java/lang/Exception from L239 to L240 using L224
.catch java/io/FileNotFoundException from L239 to L240 using L2
.catch java/io/IOException from L239 to L240 using L3
.catch java/lang/InterruptedException from L239 to L240 using L4
.catch java/lang/Exception from L241 to L242 using L224
.catch java/io/FileNotFoundException from L241 to L242 using L2
.catch java/io/IOException from L241 to L242 using L3
.catch java/lang/InterruptedException from L241 to L242 using L4
.catch java/lang/Exception from L243 to L244 using L224
.catch java/io/FileNotFoundException from L243 to L244 using L2
.catch java/io/IOException from L243 to L244 using L3
.catch java/lang/InterruptedException from L243 to L244 using L4
.catch java/lang/Exception from L245 to L246 using L224
.catch java/io/FileNotFoundException from L245 to L246 using L2
.catch java/io/IOException from L245 to L246 using L3
.catch java/lang/InterruptedException from L245 to L246 using L4
.catch java/lang/Exception from L247 to L248 using L224
.catch java/io/FileNotFoundException from L247 to L248 using L2
.catch java/io/IOException from L247 to L248 using L3
.catch java/lang/InterruptedException from L247 to L248 using L4
.catch java/lang/Exception from L249 to L250 using L224
.catch java/io/FileNotFoundException from L249 to L250 using L2
.catch java/io/IOException from L249 to L250 using L3
.catch java/lang/InterruptedException from L249 to L250 using L4
.catch java/lang/Exception from L251 to L252 using L224
.catch java/io/FileNotFoundException from L251 to L252 using L2
.catch java/io/IOException from L251 to L252 using L3
.catch java/lang/InterruptedException from L251 to L252 using L4
.catch java/lang/Exception from L253 to L254 using L224
.catch java/io/FileNotFoundException from L253 to L254 using L2
.catch java/io/IOException from L253 to L254 using L3
.catch java/lang/InterruptedException from L253 to L254 using L4
.catch java/lang/Exception from L255 to L256 using L257
.catch java/io/FileNotFoundException from L255 to L256 using L2
.catch java/io/IOException from L255 to L256 using L3
.catch java/lang/InterruptedException from L255 to L256 using L4
.catch java/io/FileNotFoundException from L258 to L259 using L2
.catch java/io/IOException from L258 to L259 using L3
.catch java/lang/InterruptedException from L258 to L259 using L4
.catch java/io/FileNotFoundException from L260 to L261 using L2
.catch java/io/IOException from L260 to L261 using L3
.catch java/lang/InterruptedException from L260 to L261 using L4
.catch java/io/FileNotFoundException from L262 to L263 using L2
.catch java/io/IOException from L262 to L263 using L3
.catch java/lang/InterruptedException from L262 to L263 using L4
.catch java/io/FileNotFoundException from L264 to L265 using L2
.catch java/io/IOException from L264 to L265 using L3
.catch java/lang/InterruptedException from L264 to L265 using L4
.catch java/io/FileNotFoundException from L266 to L267 using L2
.catch java/io/IOException from L266 to L267 using L3
.catch java/lang/InterruptedException from L266 to L267 using L4
.catch java/io/FileNotFoundException from L268 to L269 using L2
.catch java/io/IOException from L268 to L269 using L3
.catch java/lang/InterruptedException from L268 to L269 using L4
.catch java/io/FileNotFoundException from L270 to L271 using L2
.catch java/io/IOException from L270 to L271 using L3
.catch java/lang/InterruptedException from L270 to L271 using L4
.catch java/io/FileNotFoundException from L272 to L273 using L2
.catch java/io/IOException from L272 to L273 using L3
.catch java/lang/InterruptedException from L272 to L273 using L4
.catch org/json/JSONException from L274 to L275 using L276
.catch java/io/FileNotFoundException from L274 to L275 using L2
.catch java/io/IOException from L274 to L275 using L3
.catch java/lang/InterruptedException from L274 to L275 using L4
.catch java/io/FileNotFoundException from L275 to L277 using L2
.catch java/io/IOException from L275 to L277 using L3
.catch java/lang/InterruptedException from L275 to L277 using L4
.catch java/lang/Exception from L278 to L279 using L280
.catch java/io/FileNotFoundException from L278 to L279 using L2
.catch java/io/IOException from L278 to L279 using L3
.catch java/lang/InterruptedException from L278 to L279 using L4
.catch java/lang/Exception from L281 to L282 using L280
.catch java/io/FileNotFoundException from L281 to L282 using L2
.catch java/io/IOException from L281 to L282 using L3
.catch java/lang/InterruptedException from L281 to L282 using L4
.catch java/lang/Exception from L283 to L284 using L280
.catch java/io/FileNotFoundException from L283 to L284 using L2
.catch java/io/IOException from L283 to L284 using L3
.catch java/lang/InterruptedException from L283 to L284 using L4
.catch java/lang/Exception from L285 to L286 using L280
.catch java/io/FileNotFoundException from L285 to L286 using L2
.catch java/io/IOException from L285 to L286 using L3
.catch java/lang/InterruptedException from L285 to L286 using L4
.catch java/lang/Exception from L287 to L288 using L280
.catch java/io/FileNotFoundException from L287 to L288 using L2
.catch java/io/IOException from L287 to L288 using L3
.catch java/lang/InterruptedException from L287 to L288 using L4
.catch java/lang/Exception from L289 to L290 using L280
.catch java/io/FileNotFoundException from L289 to L290 using L2
.catch java/io/IOException from L289 to L290 using L3
.catch java/lang/InterruptedException from L289 to L290 using L4
.catch java/lang/Exception from L291 to L292 using L280
.catch java/io/FileNotFoundException from L291 to L292 using L2
.catch java/io/IOException from L291 to L292 using L3
.catch java/lang/InterruptedException from L291 to L292 using L4
.catch java/lang/Exception from L293 to L294 using L280
.catch java/io/FileNotFoundException from L293 to L294 using L2
.catch java/io/IOException from L293 to L294 using L3
.catch java/lang/InterruptedException from L293 to L294 using L4
.catch java/lang/Exception from L295 to L296 using L280
.catch java/io/FileNotFoundException from L295 to L296 using L2
.catch java/io/IOException from L295 to L296 using L3
.catch java/lang/InterruptedException from L295 to L296 using L4
.catch java/lang/Exception from L297 to L298 using L280
.catch java/io/FileNotFoundException from L297 to L298 using L2
.catch java/io/IOException from L297 to L298 using L3
.catch java/lang/InterruptedException from L297 to L298 using L4
.catch java/lang/Exception from L299 to L300 using L280
.catch java/io/FileNotFoundException from L299 to L300 using L2
.catch java/io/IOException from L299 to L300 using L3
.catch java/lang/InterruptedException from L299 to L300 using L4
.catch java/lang/Exception from L301 to L302 using L280
.catch java/io/FileNotFoundException from L301 to L302 using L2
.catch java/io/IOException from L301 to L302 using L3
.catch java/lang/InterruptedException from L301 to L302 using L4
.catch java/lang/Exception from L303 to L304 using L280
.catch java/io/FileNotFoundException from L303 to L304 using L2
.catch java/io/IOException from L303 to L304 using L3
.catch java/lang/InterruptedException from L303 to L304 using L4
.catch java/lang/Exception from L305 to L306 using L280
.catch java/io/FileNotFoundException from L305 to L306 using L2
.catch java/io/IOException from L305 to L306 using L3
.catch java/lang/InterruptedException from L305 to L306 using L4
.catch java/lang/Exception from L307 to L308 using L280
.catch java/io/FileNotFoundException from L307 to L308 using L2
.catch java/io/IOException from L307 to L308 using L3
.catch java/lang/InterruptedException from L307 to L308 using L4
.catch java/lang/Exception from L309 to L310 using L280
.catch java/io/FileNotFoundException from L309 to L310 using L2
.catch java/io/IOException from L309 to L310 using L3
.catch java/lang/InterruptedException from L309 to L310 using L4
.catch java/io/FileNotFoundException from L311 to L312 using L2
.catch java/io/IOException from L311 to L312 using L3
.catch java/lang/InterruptedException from L311 to L312 using L4
.catch java/io/FileNotFoundException from L313 to L314 using L2
.catch java/io/IOException from L313 to L314 using L3
.catch java/lang/InterruptedException from L313 to L314 using L4
.catch java/io/FileNotFoundException from L315 to L316 using L2
.catch java/io/IOException from L315 to L316 using L3
.catch java/lang/InterruptedException from L315 to L316 using L4
.catch java/io/FileNotFoundException from L317 to L318 using L2
.catch java/io/IOException from L317 to L318 using L3
.catch java/lang/InterruptedException from L317 to L318 using L4
.catch java/io/FileNotFoundException from L319 to L320 using L2
.catch java/io/IOException from L319 to L320 using L3
.catch java/lang/InterruptedException from L319 to L320 using L4
.catch java/io/FileNotFoundException from L321 to L322 using L2
.catch java/io/IOException from L321 to L322 using L3
.catch java/lang/InterruptedException from L321 to L322 using L4
.catch java/io/FileNotFoundException from L322 to L323 using L2
.catch java/io/IOException from L322 to L323 using L3
.catch java/lang/InterruptedException from L322 to L323 using L4
.catch java/io/FileNotFoundException from L323 to L324 using L2
.catch java/io/IOException from L323 to L324 using L3
.catch java/lang/InterruptedException from L323 to L324 using L4
.catch java/io/FileNotFoundException from L325 to L326 using L2
.catch java/io/IOException from L325 to L326 using L3
.catch java/lang/InterruptedException from L325 to L326 using L4
.catch org/json/JSONException from L327 to L328 using L329
.catch java/io/FileNotFoundException from L327 to L328 using L2
.catch java/io/IOException from L327 to L328 using L3
.catch java/lang/InterruptedException from L327 to L328 using L4
.catch java/io/FileNotFoundException from L330 to L331 using L2
.catch java/io/IOException from L330 to L331 using L3
.catch java/lang/InterruptedException from L330 to L331 using L4
.catch java/lang/Exception from L331 to L332 using L333
.catch java/io/FileNotFoundException from L331 to L332 using L2
.catch java/io/IOException from L331 to L332 using L3
.catch java/lang/InterruptedException from L331 to L332 using L4
.catch java/lang/Exception from L332 to L334 using L333
.catch java/io/FileNotFoundException from L332 to L334 using L2
.catch java/io/IOException from L332 to L334 using L3
.catch java/lang/InterruptedException from L332 to L334 using L4
.catch java/io/FileNotFoundException from L335 to L336 using L2
.catch java/io/IOException from L335 to L336 using L3
.catch java/lang/InterruptedException from L335 to L336 using L4
.catch java/io/FileNotFoundException from L337 to L338 using L2
.catch java/io/IOException from L337 to L338 using L3
.catch java/lang/InterruptedException from L337 to L338 using L4
.catch java/io/FileNotFoundException from L339 to L340 using L2
.catch java/io/IOException from L339 to L340 using L3
.catch java/lang/InterruptedException from L339 to L340 using L4
.catch java/io/FileNotFoundException from L341 to L342 using L2
.catch java/io/IOException from L341 to L342 using L3
.catch java/lang/InterruptedException from L341 to L342 using L4
.catch java/io/FileNotFoundException from L342 to L343 using L2
.catch java/io/IOException from L342 to L343 using L3
.catch java/lang/InterruptedException from L342 to L343 using L4
.catch java/io/FileNotFoundException from L344 to L345 using L2
.catch java/io/IOException from L344 to L345 using L3
.catch java/lang/InterruptedException from L344 to L345 using L4
.catch java/io/FileNotFoundException from L346 to L347 using L2
.catch java/io/IOException from L346 to L347 using L3
.catch java/lang/InterruptedException from L346 to L347 using L4
.catch java/io/FileNotFoundException from L348 to L349 using L2
.catch java/io/IOException from L348 to L349 using L3
.catch java/lang/InterruptedException from L348 to L349 using L4
.catch java/io/FileNotFoundException from L350 to L351 using L2
.catch java/io/IOException from L350 to L351 using L3
.catch java/lang/InterruptedException from L350 to L351 using L4
.catch java/io/FileNotFoundException from L352 to L353 using L2
.catch java/io/IOException from L352 to L353 using L3
.catch java/lang/InterruptedException from L352 to L353 using L4
.catch java/io/FileNotFoundException from L354 to L355 using L2
.catch java/io/IOException from L354 to L355 using L3
.catch java/lang/InterruptedException from L354 to L355 using L4
.catch java/io/FileNotFoundException from L355 to L356 using L2
.catch java/io/IOException from L355 to L356 using L3
.catch java/lang/InterruptedException from L355 to L356 using L4
.catch java/io/FileNotFoundException from L357 to L358 using L2
.catch java/io/IOException from L357 to L358 using L3
.catch java/lang/InterruptedException from L357 to L358 using L4
new com/android/vending/billing/InAppBillingService/LUCK/LogOutputStream
dup
ldc "System.out"
invokespecial com/android/vending/billing/InAppBillingService/LUCK/LogOutputStream/<init>(Ljava/lang/String;)V
astore 27
new java/io/PrintStream
dup
aload 27
invokespecial java/io/PrintStream/<init>(Ljava/io/OutputStream;)V
putstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "SU Java-Code Running!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic com/chelpus/root/utils/createapkcustom/patchedLibs Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
aload 0
iconst_0
aaload
putstatic com/chelpus/root/utils/createapkcustom/packageName Ljava/lang/String;
aload 0
iconst_2
aaload
putstatic com/chelpus/root/utils/createapkcustom/appdir Ljava/lang/String;
aload 0
iconst_3
aaload
putstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
aload 0
iconst_4
aaload
putstatic com/chelpus/root/utils/createapkcustom/tooldir Ljava/lang/String;
invokestatic com/chelpus/root/utils/createapkcustom/clearTemp()V
iconst_0
putstatic com/chelpus/root/utils/createapkcustom/manualpatch Z
ldc ""
astore 23
ldc ""
astore 26
iconst_0
istore 2
iconst_0
istore 10
invokestatic com/chelpus/root/utils/createapkcustom/getClassesDex()V
L0:
new java/io/FileInputStream
dup
aload 0
iconst_1
aaload
invokespecial java/io/FileInputStream/<init>(Ljava/lang/String;)V
astore 28
new java/io/BufferedReader
dup
new java/io/InputStreamReader
dup
aload 28
ldc "UTF-8"
invokespecial java/io/InputStreamReader/<init>(Ljava/io/InputStream;Ljava/lang/String;)V
invokespecial java/io/BufferedReader/<init>(Ljava/io/Reader;)V
astore 29
sipush 1000
anewarray java/lang/String
astore 30
L1:
aconst_null
astore 21
aconst_null
astore 22
iconst_1
istore 12
iconst_1
istore 13
iconst_0
istore 5
iconst_0
istore 4
iconst_0
istore 3
iconst_0
istore 6
ldc ""
astore 18
ldc ""
astore 16
ldc ""
astore 17
L5:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putstatic com/chelpus/root/utils/createapkcustom/pat Ljava/util/ArrayList;
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putstatic com/chelpus/root/utils/createapkcustom/ser Ljava/util/ArrayList;
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putstatic com/chelpus/root/utils/createapkcustom/search Ljava/util/ArrayList;
L6:
iconst_0
istore 9
L7:
aload 29
invokevirtual java/io/BufferedReader/readLine()Ljava/lang/String;
astore 19
L8:
aload 19
ifnull L350
aload 19
astore 24
L9:
aload 19
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L10
aload 19
getstatic com/chelpus/root/utils/createapkcustom/packageName Ljava/lang/String;
invokestatic com/chelpus/Utils/apply_TAGS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 24
L10:
aload 30
iload 9
aload 24
aastore
iload 4
istore 1
iload 4
ifeq L359
L11:
aload 30
iload 9
aaload
ldc "["
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L14
aload 30
iload 9
aaload
ldc "]"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L14
L12:
iload 4
istore 1
L13:
aload 30
iload 9
aaload
ldc "{"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L359
L14:
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 26
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L15:
iconst_0
istore 1
L359:
aload 26
astore 25
iload 1
ifeq L17
L16:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 26
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 30
iload 9
aaload
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 25
L17:
iload 12
istore 15
iload 13
istore 14
L18:
aload 30
iload 9
aaload
ldc "["
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L23
L19:
iload 12
istore 15
iload 13
istore 14
L20:
aload 30
iload 9
aaload
ldc "]"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L23
L21:
iload 12
istore 15
iload 13
istore 14
L22:
getstatic com/chelpus/root/utils/createapkcustom/tag I
tableswitch 1
L360
L111
L23
L23
L23
L119
L127
L135
L143
L106
default : L361
L23:
iload 1
istore 4
L24:
aload 30
iload 9
aaload
ldc "[BEGIN]"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L26
iconst_0
putstatic com/chelpus/root/utils/createapkcustom/tag I
L25:
iconst_1
istore 4
L26:
aload 30
iload 9
aaload
ldc "[CLASSES]"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L27
aload 30
iload 9
aaload
ldc "[ODEX]"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L28
L27:
iconst_1
putstatic com/chelpus/root/utils/createapkcustom/tag I
L28:
aload 30
iload 9
aaload
ldc "[PACKAGE]"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L29
iconst_5
putstatic com/chelpus/root/utils/createapkcustom/tag I
L29:
iload 5
istore 1
aload 18
astore 19
iload 5
ifeq L35
L30:
getstatic com/chelpus/root/utils/createapkcustom/ser Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
getstatic com/chelpus/root/utils/createapkcustom/pat Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
L31:
new org/json/JSONObject
dup
aload 30
iload 9
aaload
invokespecial org/json/JSONObject/<init>(Ljava/lang/String;)V
ldc "name"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
astore 19
L32:
aload 19
astore 18
L34:
getstatic com/chelpus/root/utils/createapkcustom/tag I
tableswitch 2
L153
L362
L362
L362
L155
L157
L159
L161
default : L362
L35:
iload 6
istore 5
aload 19
astore 18
iload 6
ifeq L41
L36:
getstatic com/chelpus/root/utils/createapkcustom/ser Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
getstatic com/chelpus/root/utils/createapkcustom/pat Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
L37:
new org/json/JSONObject
dup
aload 30
iload 9
aaload
invokespecial org/json/JSONObject/<init>(Ljava/lang/String;)V
ldc "name"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
astore 18
L38:
aload 18
invokestatic com/chelpus/root/utils/createapkcustom/getFileFromApk(Ljava/lang/String;)Ljava/lang/String;
astore 19
new java/io/File
dup
aload 19
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L165
new java/io/File
dup
aload 19
invokespecial java/io/File/<init>(Ljava/lang/String;)V
putstatic com/chelpus/root/utils/createapkcustom/localFile2 Ljava/io/File;
L40:
goto L363
L41:
aload 30
iload 9
aaload
ldc "[LIB-ARMEABI]"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L43
bipush 6
putstatic com/chelpus/root/utils/createapkcustom/tag I
iconst_0
putstatic com/chelpus/root/utils/createapkcustom/unpack Z
L42:
iconst_0
istore 5
iconst_1
istore 1
L43:
aload 30
iload 9
aaload
ldc "[LIB-ARMEABI-V7A]"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L45
bipush 7
putstatic com/chelpus/root/utils/createapkcustom/tag I
iconst_0
putstatic com/chelpus/root/utils/createapkcustom/unpack Z
L44:
iconst_0
istore 5
iconst_1
istore 1
L45:
aload 30
iload 9
aaload
ldc "[LIB-MIPS]"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L47
bipush 8
putstatic com/chelpus/root/utils/createapkcustom/tag I
iconst_0
putstatic com/chelpus/root/utils/createapkcustom/unpack Z
L46:
iconst_0
istore 5
iconst_1
istore 1
L47:
aload 30
iload 9
aaload
ldc "[LIB-X86]"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L49
bipush 9
putstatic com/chelpus/root/utils/createapkcustom/tag I
iconst_0
putstatic com/chelpus/root/utils/createapkcustom/unpack Z
L48:
iconst_0
istore 5
iconst_1
istore 1
L49:
aload 30
iload 9
aaload
ldc "[LIB]"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L364
iconst_2
putstatic com/chelpus/root/utils/createapkcustom/tag I
iconst_0
putstatic com/chelpus/root/utils/createapkcustom/unpack Z
L50:
iconst_0
istore 5
iconst_1
istore 1
L364:
iload 5
istore 6
iload 1
istore 5
L51:
aload 30
iload 9
aaload
ldc "[FILE_IN_APK]"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L53
bipush 10
putstatic com/chelpus/root/utils/createapkcustom/tag I
iconst_0
putstatic com/chelpus/root/utils/createapkcustom/unpack Z
L52:
iconst_0
istore 5
iconst_1
istore 6
L53:
aload 30
iload 9
aaload
ldc "group"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
istore 12
L54:
iload 12
ifeq L56
L55:
new org/json/JSONObject
dup
aload 30
iload 9
aaload
invokespecial org/json/JSONObject/<init>(Ljava/lang/String;)V
ldc "group"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
putstatic com/chelpus/root/utils/createapkcustom/group Ljava/lang/String;
L56:
iload 2
istore 1
iload 3
istore 7
iload 14
istore 12
aload 18
astore 19
L58:
aload 30
iload 9
aaload
ldc "original"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L173
L59:
iload 3
istore 8
iload 3
ifeq L62
L60:
getstatic com/chelpus/root/utils/createapkcustom/ser Ljava/util/ArrayList;
invokestatic com/chelpus/root/utils/createapkcustom/searchProcess(Ljava/util/ArrayList;)Z
istore 14
L61:
iconst_0
istore 8
L62:
new org/json/JSONObject
dup
aload 30
iload 9
aaload
invokespecial org/json/JSONObject/<init>(Ljava/lang/String;)V
ldc "original"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
astore 19
L63:
aload 19
astore 18
L65:
aload 18
invokevirtual java/lang/String/trim()Ljava/lang/String;
astore 18
aload 18
ldc "[ \u0009]+"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
arraylength
anewarray java/lang/String
astore 19
aload 18
ldc "[ \u0009]+"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 31
aload 31
arraylength
newarray int
astore 20
aload 31
arraylength
newarray byte
astore 26
L66:
iconst_0
istore 11
L365:
iload 2
istore 3
aload 26
astore 21
aload 20
astore 22
iload 2
istore 1
iload 8
istore 7
iload 14
istore 12
aload 18
astore 19
L67:
iload 11
aload 31
arraylength
if_icmpge L173
L68:
iload 2
istore 1
iload 2
istore 3
L70:
aload 31
iload 11
aaload
ldc "*"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L366
L71:
iload 2
istore 1
iload 2
istore 3
L72:
aload 31
iload 11
aaload
ldc "**"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L366
L73:
iconst_1
istore 1
aload 31
iload 11
ldc "60"
aastore
L366:
iload 1
istore 3
L74:
aload 31
iload 11
aaload
ldc "**"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L367
L75:
iload 1
istore 3
L76:
aload 31
iload 11
aaload
ldc "\\?+"
invokevirtual java/lang/String/matches(Ljava/lang/String;)Z
ifeq L368
L77:
goto L367
L369:
iload 1
istore 3
L78:
aload 31
iload 11
aaload
ldc "W"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L85
L79:
iload 1
istore 3
L80:
aload 31
iload 11
aaload
ldc "w"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L85
L81:
iload 1
istore 3
L82:
aload 31
iload 11
aaload
ldc "R"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L85
L83:
iload 1
istore 3
L84:
aload 31
iload 11
aaload
ldc "r"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L370
L85:
iload 1
istore 3
L86:
aload 20
iload 11
aload 31
iload 11
aaload
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "w"
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
ldc "r"
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokestatic java/lang/Integer/valueOf(Ljava/lang/String;)Ljava/lang/Integer;
invokevirtual java/lang/Integer/intValue()I
iconst_2
iadd
iastore
L87:
aload 31
iload 11
ldc "60"
aastore
L370:
iload 1
istore 3
L88:
aload 26
iload 11
aload 31
iload 11
aaload
bipush 16
invokestatic java/lang/Integer/valueOf(Ljava/lang/String;I)Ljava/lang/Integer;
invokevirtual java/lang/Integer/byteValue()B
bastore
L89:
iload 11
iconst_1
iadd
istore 11
iload 1
istore 2
goto L365
L360:
iload 12
istore 15
iload 13
istore 14
L90:
getstatic com/chelpus/root/utils/createapkcustom/pat Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifle L23
invokestatic com/chelpus/root/utils/createapkcustom/getClassesDex()V
L91:
iload 12
istore 15
iload 13
istore 14
L92:
getstatic com/chelpus/root/utils/createapkcustom/classesFiles Ljava/util/ArrayList;
ifnull L101
L93:
iload 12
istore 15
iload 13
istore 14
L94:
getstatic com/chelpus/root/utils/createapkcustom/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifle L101
getstatic com/chelpus/root/utils/createapkcustom/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
if_icmple L95
iconst_1
putstatic com/chelpus/root/utils/createapkcustom/multidex Z
L95:
getstatic com/chelpus/root/utils/createapkcustom/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 19
L96:
iload 12
istore 15
iload 13
istore 14
L97:
aload 19
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L101
aload 19
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
astore 20
aload 20
putstatic com/chelpus/root/utils/createapkcustom/localFile2 Ljava/io/File;
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "---------------------------------"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Patch for "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 20
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "---------------------------------\n"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L98:
iload 12
istore 14
L99:
getstatic com/chelpus/root/utils/createapkcustom/manualpatch Z
ifne L371
getstatic com/chelpus/root/utils/createapkcustom/pat Ljava/util/ArrayList;
invokestatic com/chelpus/root/utils/createapkcustom/patchProcess(Ljava/util/ArrayList;)Z
istore 14
L100:
goto L371
L101:
iconst_0
putstatic com/chelpus/root/utils/createapkcustom/multidex Z
iconst_0
putstatic com/chelpus/root/utils/createapkcustom/goodResult Z
getstatic com/chelpus/root/utils/createapkcustom/ser Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
getstatic com/chelpus/root/utils/createapkcustom/pat Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
sipush 200
putstatic com/chelpus/root/utils/createapkcustom/tag I
L102:
goto L23
L2:
astore 0
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "Custom Patch not Found!\n"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L372:
invokestatic com/chelpus/root/utils/createapkcustom/clearTemp()V
aload 27
getfield com/android/vending/billing/InAppBillingService/LUCK/LogOutputStream/allresult Ljava/lang/String;
astore 0
L103:
aload 27
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/LogOutputStream/close()V
L104:
aload 0
areturn
L106:
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "---------------------------"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Patch for file from apk\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/createapkcustom/localFile2 Ljava/io/File;
invokevirtual java/io/File/getPath()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/tmp"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "---------------------------\n"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L107:
getstatic com/chelpus/root/utils/createapkcustom/manualpatch Z
ifne L373
getstatic com/chelpus/root/utils/createapkcustom/pat Ljava/util/ArrayList;
invokestatic com/chelpus/root/utils/createapkcustom/patchProcess(Ljava/util/ArrayList;)Z
istore 12
L108:
goto L373
L109:
getstatic com/chelpus/root/utils/createapkcustom/patchedLibs Ljava/util/ArrayList;
getstatic com/chelpus/root/utils/createapkcustom/localFile2 Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
getstatic com/chelpus/root/utils/createapkcustom/ser Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
getstatic com/chelpus/root/utils/createapkcustom/pat Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
sipush 200
putstatic com/chelpus/root/utils/createapkcustom/tag I
L110:
iload 12
istore 15
iload 13
istore 14
goto L23
L3:
astore 0
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Patch process Error LP: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
goto L372
L111:
getstatic com/chelpus/root/utils/createapkcustom/libs Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 19
L112:
aload 19
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L117
aload 19
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 20
new java/io/File
dup
aload 20
invokespecial java/io/File/<init>(Ljava/lang/String;)V
putstatic com/chelpus/root/utils/createapkcustom/localFile2 Ljava/io/File;
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "---------------------------"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Patch for libraries \n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/createapkcustom/localFile2 Ljava/io/File;
invokevirtual java/io/File/getPath()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/tmp"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "---------------------------\n"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L113:
getstatic com/chelpus/root/utils/createapkcustom/manualpatch Z
ifne L374
getstatic com/chelpus/root/utils/createapkcustom/pat Ljava/util/ArrayList;
invokestatic com/chelpus/root/utils/createapkcustom/patchProcess(Ljava/util/ArrayList;)Z
istore 12
L114:
goto L374
L115:
getstatic com/chelpus/root/utils/createapkcustom/patchedLibs Ljava/util/ArrayList;
aload 20
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L116:
goto L112
L4:
astore 0
aload 0
invokevirtual java/lang/InterruptedException/printStackTrace()V
goto L372
L117:
iconst_0
putstatic com/chelpus/root/utils/createapkcustom/multilib_patch Z
iconst_0
putstatic com/chelpus/root/utils/createapkcustom/goodResult Z
getstatic com/chelpus/root/utils/createapkcustom/ser Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
getstatic com/chelpus/root/utils/createapkcustom/pat Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
sipush 200
putstatic com/chelpus/root/utils/createapkcustom/tag I
L118:
iload 12
istore 15
iload 13
istore 14
goto L23
L119:
getstatic com/chelpus/root/utils/createapkcustom/libs Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 19
L120:
aload 19
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L125
aload 19
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 20
new java/io/File
dup
aload 20
invokespecial java/io/File/<init>(Ljava/lang/String;)V
putstatic com/chelpus/root/utils/createapkcustom/localFile2 Ljava/io/File;
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "--------------------------------"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Patch for (armeabi) libraries \n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/createapkcustom/localFile2 Ljava/io/File;
invokevirtual java/io/File/getPath()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/tmp"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "--------------------------------\n"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L121:
getstatic com/chelpus/root/utils/createapkcustom/manualpatch Z
ifne L375
getstatic com/chelpus/root/utils/createapkcustom/pat Ljava/util/ArrayList;
invokestatic com/chelpus/root/utils/createapkcustom/patchProcess(Ljava/util/ArrayList;)Z
istore 12
L122:
goto L375
L123:
getstatic com/chelpus/root/utils/createapkcustom/patchedLibs Ljava/util/ArrayList;
aload 20
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L124:
goto L120
L125:
iconst_0
putstatic com/chelpus/root/utils/createapkcustom/multilib_patch Z
iconst_0
putstatic com/chelpus/root/utils/createapkcustom/goodResult Z
getstatic com/chelpus/root/utils/createapkcustom/ser Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
getstatic com/chelpus/root/utils/createapkcustom/pat Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
sipush 200
putstatic com/chelpus/root/utils/createapkcustom/tag I
L126:
iload 12
istore 15
iload 13
istore 14
goto L23
L127:
getstatic com/chelpus/root/utils/createapkcustom/libs Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 19
L128:
aload 19
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L133
aload 19
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 20
new java/io/File
dup
aload 20
invokespecial java/io/File/<init>(Ljava/lang/String;)V
putstatic com/chelpus/root/utils/createapkcustom/localFile2 Ljava/io/File;
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "---------------------------------------"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Patch for (armeabi-v7a) libraries \n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/createapkcustom/localFile2 Ljava/io/File;
invokevirtual java/io/File/getPath()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/tmp"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "---------------------------------------\n"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L129:
getstatic com/chelpus/root/utils/createapkcustom/manualpatch Z
ifne L376
getstatic com/chelpus/root/utils/createapkcustom/pat Ljava/util/ArrayList;
invokestatic com/chelpus/root/utils/createapkcustom/patchProcess(Ljava/util/ArrayList;)Z
istore 12
L130:
goto L376
L131:
getstatic com/chelpus/root/utils/createapkcustom/patchedLibs Ljava/util/ArrayList;
aload 20
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L132:
goto L128
L133:
iconst_0
putstatic com/chelpus/root/utils/createapkcustom/multilib_patch Z
iconst_0
putstatic com/chelpus/root/utils/createapkcustom/goodResult Z
getstatic com/chelpus/root/utils/createapkcustom/ser Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
getstatic com/chelpus/root/utils/createapkcustom/pat Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
sipush 200
putstatic com/chelpus/root/utils/createapkcustom/tag I
L134:
iload 12
istore 15
iload 13
istore 14
goto L23
L135:
getstatic com/chelpus/root/utils/createapkcustom/libs Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 19
L136:
aload 19
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L141
aload 19
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 20
new java/io/File
dup
aload 20
invokespecial java/io/File/<init>(Ljava/lang/String;)V
putstatic com/chelpus/root/utils/createapkcustom/localFile2 Ljava/io/File;
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "---------------------------"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Patch for (MIPS) libraries \n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/createapkcustom/localFile2 Ljava/io/File;
invokevirtual java/io/File/getPath()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/tmp"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "---------------------------\n"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L137:
getstatic com/chelpus/root/utils/createapkcustom/manualpatch Z
ifne L377
getstatic com/chelpus/root/utils/createapkcustom/pat Ljava/util/ArrayList;
invokestatic com/chelpus/root/utils/createapkcustom/patchProcess(Ljava/util/ArrayList;)Z
istore 12
L138:
goto L377
L139:
getstatic com/chelpus/root/utils/createapkcustom/patchedLibs Ljava/util/ArrayList;
aload 20
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L140:
goto L136
L141:
iconst_0
putstatic com/chelpus/root/utils/createapkcustom/multilib_patch Z
iconst_0
putstatic com/chelpus/root/utils/createapkcustom/goodResult Z
getstatic com/chelpus/root/utils/createapkcustom/ser Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
getstatic com/chelpus/root/utils/createapkcustom/pat Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
sipush 200
putstatic com/chelpus/root/utils/createapkcustom/tag I
L142:
iload 12
istore 15
iload 13
istore 14
goto L23
L143:
getstatic com/chelpus/root/utils/createapkcustom/libs Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 19
L144:
aload 19
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L149
aload 19
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 20
new java/io/File
dup
aload 20
invokespecial java/io/File/<init>(Ljava/lang/String;)V
putstatic com/chelpus/root/utils/createapkcustom/localFile2 Ljava/io/File;
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "---------------------------"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Patch for (x86) libraries \n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/createapkcustom/localFile2 Ljava/io/File;
invokevirtual java/io/File/getPath()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/tmp"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "---------------------------\n"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L145:
getstatic com/chelpus/root/utils/createapkcustom/manualpatch Z
ifne L378
getstatic com/chelpus/root/utils/createapkcustom/pat Ljava/util/ArrayList;
invokestatic com/chelpus/root/utils/createapkcustom/patchProcess(Ljava/util/ArrayList;)Z
istore 12
L146:
goto L378
L147:
getstatic com/chelpus/root/utils/createapkcustom/patchedLibs Ljava/util/ArrayList;
aload 20
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L148:
goto L144
L149:
iconst_0
putstatic com/chelpus/root/utils/createapkcustom/multilib_patch Z
iconst_0
putstatic com/chelpus/root/utils/createapkcustom/goodResult Z
getstatic com/chelpus/root/utils/createapkcustom/ser Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
getstatic com/chelpus/root/utils/createapkcustom/pat Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
sipush 200
putstatic com/chelpus/root/utils/createapkcustom/tag I
L150:
iload 12
istore 15
iload 13
istore 14
goto L23
L33:
astore 19
L151:
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "Error LP: Error name of libraries read!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L152:
goto L34
L153:
getstatic com/chelpus/root/utils/createapkcustom/libs Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
iconst_4
aload 18
invokestatic com/chelpus/root/utils/createapkcustom/searchlib(ILjava/lang/String;)Ljava/util/ArrayList;
putstatic com/chelpus/root/utils/createapkcustom/libs Ljava/util/ArrayList;
L154:
goto L362
L155:
getstatic com/chelpus/root/utils/createapkcustom/libs Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
iconst_0
aload 18
invokestatic com/chelpus/root/utils/createapkcustom/searchlib(ILjava/lang/String;)Ljava/util/ArrayList;
putstatic com/chelpus/root/utils/createapkcustom/libs Ljava/util/ArrayList;
L156:
goto L362
L157:
getstatic com/chelpus/root/utils/createapkcustom/libs Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
iconst_1
aload 18
invokestatic com/chelpus/root/utils/createapkcustom/searchlib(ILjava/lang/String;)Ljava/util/ArrayList;
putstatic com/chelpus/root/utils/createapkcustom/libs Ljava/util/ArrayList;
L158:
goto L362
L159:
getstatic com/chelpus/root/utils/createapkcustom/libs Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
iconst_2
aload 18
invokestatic com/chelpus/root/utils/createapkcustom/searchlib(ILjava/lang/String;)Ljava/util/ArrayList;
putstatic com/chelpus/root/utils/createapkcustom/libs Ljava/util/ArrayList;
L160:
goto L362
L161:
getstatic com/chelpus/root/utils/createapkcustom/libs Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
iconst_3
aload 18
invokestatic com/chelpus/root/utils/createapkcustom/searchlib(ILjava/lang/String;)Ljava/util/ArrayList;
putstatic com/chelpus/root/utils/createapkcustom/libs Ljava/util/ArrayList;
L162:
goto L362
L39:
astore 18
L163:
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "Error LP: Error name of file from apk read!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L164:
aload 19
astore 18
goto L38
L165:
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "file for patch not found in apk."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L166:
goto L363
L57:
astore 19
L167:
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "Error LP: Error original hex read!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
ldc ""
putstatic com/chelpus/root/utils/createapkcustom/group Ljava/lang/String;
L168:
goto L56
L64:
astore 19
L169:
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "Error LP: Error original hex read!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L170:
goto L65
L368:
aload 20
iload 11
iconst_0
iastore
goto L369
L69:
astore 19
L171:
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 19
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L172:
aload 18
astore 19
iload 14
istore 12
iload 8
istore 7
iload 3
istore 1
aload 20
astore 22
aload 26
astore 21
L173:
aload 30
iload 9
aaload
ldc "\"object\""
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
istore 13
L174:
iload 12
istore 14
aload 17
astore 18
iload 13
ifeq L182
L175:
new org/json/JSONObject
dup
aload 30
iload 9
aaload
invokespecial org/json/JSONObject/<init>(Ljava/lang/String;)V
ldc "object"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
astore 18
L176:
aload 18
astore 17
L178:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "dalvikvm -Xverify:none -Xdexopt:none -cp "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
iconst_5
aaload
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
bipush 6
aaload
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".createnerorunpatch "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
iconst_0
aaload
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "object"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 17
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/createapkcustom/tooldir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 18
invokestatic java/lang/Runtime/getRuntime()Ljava/lang/Runtime;
aload 18
invokevirtual java/lang/Runtime/exec(Ljava/lang/String;)Ljava/lang/Process;
astore 18
aload 18
invokevirtual java/lang/Process/waitFor()I
pop
new java/io/DataInputStream
dup
aload 18
invokevirtual java/lang/Process/getInputStream()Ljava/io/InputStream;
invokespecial java/io/DataInputStream/<init>(Ljava/io/InputStream;)V
astore 20
aload 20
invokevirtual java/io/DataInputStream/available()I
newarray byte
astore 26
aload 20
aload 26
invokevirtual java/io/DataInputStream/read([B)I
pop
new java/lang/String
dup
aload 26
invokespecial java/lang/String/<init>([B)V
astore 20
aload 18
invokevirtual java/lang/Process/destroy()V
aload 20
ldc "Done"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L207
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "Object patched!\n\n"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L179:
iconst_1
istore 14
L180:
getstatic com/chelpus/root/utils/createapkcustom/localFile2 Ljava/io/File;
invokestatic com/chelpus/root/utils/createapkcustom/fixadler(Ljava/io/File;)V
iconst_1
putstatic com/chelpus/root/utils/createapkcustom/manualpatch Z
L181:
aload 17
astore 18
L182:
aload 30
iload 9
aaload
ldc "search"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
istore 12
L183:
iload 1
istore 2
iload 7
istore 3
iload 15
istore 13
aload 18
astore 20
iload 12
ifeq L215
L184:
new org/json/JSONObject
dup
aload 30
iload 9
aaload
invokespecial org/json/JSONObject/<init>(Ljava/lang/String;)V
ldc "search"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
astore 17
L185:
aload 17
invokevirtual java/lang/String/trim()Ljava/lang/String;
astore 20
aload 20
ldc "[ \u0009]+"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
arraylength
anewarray java/lang/String
astore 17
aload 20
ldc "[ \u0009]+"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 17
aload 17
arraylength
newarray int
astore 22
aload 17
arraylength
newarray byte
astore 21
L187:
iconst_0
istore 8
L379:
iload 1
istore 3
iload 1
istore 2
L188:
iload 8
aload 17
arraylength
if_icmpge L380
L189:
iload 1
istore 2
iload 1
istore 3
L191:
aload 17
iload 8
aaload
ldc "*"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L381
L192:
iload 1
istore 2
iload 1
istore 3
L193:
aload 17
iload 8
aaload
ldc "**"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L381
L194:
iconst_1
istore 2
aload 17
iload 8
ldc "60"
aastore
L381:
iload 2
istore 3
L195:
aload 17
iload 8
aaload
ldc "**"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L382
L196:
iload 2
istore 3
L197:
aload 17
iload 8
aaload
ldc "\\?+"
invokevirtual java/lang/String/matches(Ljava/lang/String;)Z
ifeq L383
L198:
goto L382
L384:
iload 2
istore 3
L199:
aload 17
iload 8
aaload
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
ldc "R"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L385
L200:
iload 2
istore 3
L201:
aload 22
iload 8
aload 17
iload 8
aaload
ldc "R"
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokestatic java/lang/Integer/valueOf(Ljava/lang/String;)Ljava/lang/Integer;
invokevirtual java/lang/Integer/intValue()I
iconst_2
iadd
iastore
L202:
aload 17
iload 8
ldc "60"
aastore
L385:
iload 2
istore 3
L203:
aload 21
iload 8
aload 17
iload 8
aaload
bipush 16
invokestatic java/lang/Integer/valueOf(Ljava/lang/String;I)Ljava/lang/Integer;
invokevirtual java/lang/Integer/byteValue()B
bastore
L204:
iload 8
iconst_1
iadd
istore 8
iload 2
istore 1
goto L379
L177:
astore 18
L205:
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "Error LP: Error number by object!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L206:
goto L178
L207:
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "Object not found!\n\n"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L208:
iconst_0
istore 14
goto L180
L186:
astore 17
L209:
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "Error LP: Error search hex read!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L210:
aload 18
astore 17
goto L185
L383:
aload 22
iload 8
iconst_0
iastore
goto L384
L190:
astore 17
L211:
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "search pattern read: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 17
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L212:
iload 3
istore 2
L380:
iload 2
ifeq L386
iconst_0
istore 13
L213:
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "Error LP: Patterns to search not valid!\n"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L214:
iload 7
istore 3
L215:
aload 30
iload 9
aaload
ldc "replaced"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
istore 15
L216:
iload 2
istore 1
iload 13
istore 12
aload 16
astore 17
iload 15
ifeq L272
L217:
new org/json/JSONObject
dup
aload 30
iload 9
aaload
invokespecial org/json/JSONObject/<init>(Ljava/lang/String;)V
ldc "replaced"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
astore 17
L218:
aload 17
astore 16
L220:
aload 16
invokevirtual java/lang/String/trim()Ljava/lang/String;
astore 16
aload 16
ldc "[ \u0009]+"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
arraylength
anewarray java/lang/String
astore 17
aload 16
ldc "[ \u0009]+"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 17
aload 17
arraylength
newarray int
astore 18
aload 17
arraylength
newarray byte
astore 26
L221:
iconst_0
istore 8
L387:
iload 2
istore 7
iload 2
istore 1
L222:
iload 8
aload 17
arraylength
if_icmpge L264
L223:
iload 2
istore 1
iload 2
istore 7
L225:
aload 17
iload 8
aaload
ldc "*"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L388
L226:
iload 2
istore 1
iload 2
istore 7
L227:
aload 17
iload 8
aaload
ldc "**"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L388
L228:
iconst_1
istore 1
aload 17
iload 8
ldc "60"
aastore
L388:
iload 1
istore 7
L229:
aload 17
iload 8
aaload
ldc "**"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L389
L230:
iload 1
istore 7
L231:
aload 17
iload 8
aaload
ldc "\\?+"
invokevirtual java/lang/String/matches(Ljava/lang/String;)Z
ifeq L390
L232:
goto L389
L391:
iload 1
istore 7
L233:
aload 17
iload 8
aaload
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "sq"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L392
L234:
aload 17
iload 8
ldc "60"
aastore
aload 18
iload 8
sipush 253
iastore
L392:
iload 1
istore 7
L235:
aload 17
iload 8
aaload
ldc "s1"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L393
L236:
iload 1
istore 7
L237:
aload 17
iload 8
aaload
ldc "S1"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L394
L238:
goto L393
L394:
iload 1
istore 7
L239:
aload 17
iload 8
aaload
ldc "s0"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L395
L240:
iload 1
istore 7
L241:
aload 17
iload 8
aaload
ldc "S0"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L396
L242:
goto L395
L396:
iload 1
istore 7
L243:
aload 17
iload 8
aaload
ldc "W"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L250
L244:
iload 1
istore 7
L245:
aload 17
iload 8
aaload
ldc "w"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L250
L246:
iload 1
istore 7
L247:
aload 17
iload 8
aaload
ldc "R"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L250
L248:
iload 1
istore 7
L249:
aload 17
iload 8
aaload
ldc "R"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L397
L250:
iload 1
istore 7
L251:
aload 18
iload 8
aload 17
iload 8
aaload
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "w"
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
ldc "r"
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokestatic java/lang/Integer/valueOf(Ljava/lang/String;)Ljava/lang/Integer;
invokevirtual java/lang/Integer/intValue()I
iconst_2
iadd
iastore
L252:
aload 17
iload 8
ldc "60"
aastore
L397:
iload 1
istore 7
L253:
aload 26
iload 8
aload 17
iload 8
aaload
bipush 16
invokestatic java/lang/Integer/valueOf(Ljava/lang/String;I)Ljava/lang/Integer;
invokevirtual java/lang/Integer/byteValue()B
bastore
L254:
iload 8
iconst_1
iadd
istore 8
iload 1
istore 2
goto L387
L386:
iconst_1
istore 3
L255:
new com/android/vending/billing/InAppBillingService/LUCK/SearchItem
dup
aload 21
aload 22
invokespecial com/android/vending/billing/InAppBillingService/LUCK/SearchItem/<init>([B[I)V
astore 17
aload 17
aload 21
arraylength
newarray byte
putfield com/android/vending/billing/InAppBillingService/LUCK/SearchItem/repByte [B
getstatic com/chelpus/root/utils/createapkcustom/ser Ljava/util/ArrayList;
aload 17
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L256:
iload 15
istore 13
goto L215
L257:
astore 17
L258:
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 17
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L259:
iload 15
istore 13
goto L215
L219:
astore 17
L260:
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "Error LP: Error replaced hex read!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L261:
goto L220
L390:
aload 18
iload 8
iconst_1
iastore
goto L391
L224:
astore 17
L262:
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 17
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L263:
iload 7
istore 1
L264:
aload 18
arraylength
aload 22
arraylength
if_icmpne L398
aload 21
arraylength
aload 26
arraylength
if_icmpne L398
aload 26
arraylength
iconst_4
if_icmplt L398
L265:
iload 1
istore 2
L266:
aload 21
arraylength
iconst_4
if_icmpge L399
L267:
goto L398
L399:
iload 2
ifeq L269
iconst_0
istore 13
L268:
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "Error LP: Patterns original and replaced not valid!\n- Dimensions of the original hex-string and repleced must be >3.\n- Dimensions of the original hex-string and repleced must be equal.\n- Pattern hex must be: AF 11 4B ** AA **\nCheck the template file and try again!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L269:
iload 2
istore 1
iload 13
istore 12
aload 16
astore 17
iload 2
ifne L272
L270:
getstatic com/chelpus/root/utils/createapkcustom/pat Ljava/util/ArrayList;
new com/android/vending/billing/InAppBillingService/LUCK/PatchesItem
dup
aload 21
aload 22
aload 26
aload 18
getstatic com/chelpus/root/utils/createapkcustom/group Ljava/lang/String;
iconst_0
invokespecial com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/<init>([B[I[B[ILjava/lang/String;Z)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
ldc ""
putstatic com/chelpus/root/utils/createapkcustom/group Ljava/lang/String;
L271:
aload 16
astore 17
iload 13
istore 12
iload 2
istore 1
L272:
aload 30
iload 9
aaload
ldc "insert"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
istore 15
L273:
iload 1
istore 2
iload 12
istore 13
aload 17
astore 16
iload 15
ifeq L325
L274:
new org/json/JSONObject
dup
aload 30
iload 9
aaload
invokespecial org/json/JSONObject/<init>(Ljava/lang/String;)V
ldc "insert"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
astore 16
L275:
aload 16
invokevirtual java/lang/String/trim()Ljava/lang/String;
astore 17
aload 17
ldc "[ \u0009]+"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
arraylength
anewarray java/lang/String
astore 16
aload 17
ldc "[ \u0009]+"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 16
aload 16
arraylength
newarray int
astore 18
aload 16
arraylength
newarray byte
astore 26
L277:
iconst_0
istore 8
L400:
iload 1
istore 7
iload 1
istore 2
L278:
iload 8
aload 16
arraylength
if_icmpge L315
L279:
iload 1
istore 2
iload 1
istore 7
L281:
aload 16
iload 8
aaload
ldc "*"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L401
L282:
iload 1
istore 2
iload 1
istore 7
L283:
aload 16
iload 8
aaload
ldc "**"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L401
L284:
iconst_1
istore 2
aload 16
iload 8
ldc "60"
aastore
L401:
iload 2
istore 7
L285:
aload 16
iload 8
aaload
ldc "**"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L402
L286:
iload 2
istore 7
L287:
aload 16
iload 8
aaload
ldc "\\?+"
invokevirtual java/lang/String/matches(Ljava/lang/String;)Z
ifeq L403
L288:
goto L402
L404:
iload 2
istore 7
L289:
aload 16
iload 8
aaload
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "sq"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L405
L290:
aload 16
iload 8
ldc "60"
aastore
aload 18
iload 8
sipush 253
iastore
L405:
iload 2
istore 7
L291:
aload 16
iload 8
aaload
ldc "s1"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L406
L292:
iload 2
istore 7
L293:
aload 16
iload 8
aaload
ldc "S1"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L407
L294:
goto L406
L407:
iload 2
istore 7
L295:
aload 16
iload 8
aaload
ldc "s0"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L408
L296:
iload 2
istore 7
L297:
aload 16
iload 8
aaload
ldc "S0"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L409
L298:
goto L408
L409:
iload 2
istore 7
L299:
aload 16
iload 8
aaload
ldc "W"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L306
L300:
iload 2
istore 7
L301:
aload 16
iload 8
aaload
ldc "w"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L306
L302:
iload 2
istore 7
L303:
aload 16
iload 8
aaload
ldc "R"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L306
L304:
iload 2
istore 7
L305:
aload 16
iload 8
aaload
ldc "R"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L410
L306:
iload 2
istore 7
L307:
aload 18
iload 8
aload 16
iload 8
aaload
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "w"
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
ldc "r"
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokestatic java/lang/Integer/valueOf(Ljava/lang/String;)Ljava/lang/Integer;
invokevirtual java/lang/Integer/intValue()I
iconst_2
iadd
iastore
L308:
aload 16
iload 8
ldc "60"
aastore
L410:
iload 2
istore 7
L309:
aload 26
iload 8
aload 16
iload 8
aaload
bipush 16
invokestatic java/lang/Integer/valueOf(Ljava/lang/String;I)Ljava/lang/Integer;
invokevirtual java/lang/Integer/byteValue()B
bastore
L310:
iload 8
iconst_1
iadd
istore 8
iload 2
istore 1
goto L400
L276:
astore 16
L311:
ldc "Error LP: Error insert hex read!"
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
L312:
aload 17
astore 16
goto L275
L403:
aload 18
iload 8
iconst_1
iastore
goto L404
L280:
astore 16
L313:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 16
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
L314:
iload 7
istore 2
L315:
aload 26
arraylength
iconst_4
if_icmplt L411
L316:
iload 2
istore 1
L317:
aload 21
arraylength
iconst_4
if_icmpge L412
L318:
goto L411
L412:
iload 1
ifeq L320
iconst_0
istore 12
L319:
ldc "Error LP: Dimensions of the original hex-string and repleced must be >3.\n- Pattern hex must be: AF 11 4B ** AA **\nCheck the template file and try again!"
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
L320:
iload 1
istore 2
iload 12
istore 13
aload 17
astore 16
iload 1
ifne L325
L321:
getstatic com/chelpus/root/utils/createapkcustom/multilib_patch Z
ifne L322
getstatic com/chelpus/root/utils/createapkcustom/multidex Z
ifeq L344
L322:
getstatic com/chelpus/root/utils/createapkcustom/pat Ljava/util/ArrayList;
new com/android/vending/billing/InAppBillingService/LUCK/PatchesItem
dup
aload 21
aload 22
aload 26
aload 18
ldc "all_lib"
iconst_1
invokespecial com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/<init>([B[I[B[ILjava/lang/String;Z)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L323:
ldc ""
putstatic com/chelpus/root/utils/createapkcustom/group Ljava/lang/String;
L324:
aload 17
astore 16
iload 12
istore 13
iload 1
istore 2
L325:
aload 30
iload 9
aaload
ldc "replace_from_file"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
istore 15
L326:
iload 13
istore 12
aload 16
astore 17
iload 15
ifeq L413
L327:
new org/json/JSONObject
dup
aload 30
iload 9
aaload
invokespecial org/json/JSONObject/<init>(Ljava/lang/String;)V
ldc "replace_from_file"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
astore 17
L328:
aload 17
astore 16
L330:
aload 16
invokevirtual java/lang/String/trim()Ljava/lang/String;
astore 16
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
new java/io/File
dup
aload 0
iconst_1
aaload
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 16
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 17
aload 17
invokevirtual java/io/File/length()J
l2i
istore 1
iload 1
newarray byte
astore 18
L331:
new java/io/FileInputStream
dup
aload 17
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
astore 17
L332:
aload 17
aload 18
invokevirtual java/io/FileInputStream/read([B)I
istore 7
L334:
iload 7
ifgt L332
L335:
iload 1
newarray int
astore 26
aload 26
iconst_1
invokestatic java/util/Arrays/fill([II)V
L336:
iload 2
ifeq L338
iconst_0
istore 13
L337:
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "Error LP: Patterns original and replaced not valid!\n- Dimensions of the original hex-string and repleced must be >3.\n- Dimensions of the original hex-string and repleced must be equal.\n- Pattern hex must be: AF 11 4B ** AA **\nCheck the template file and try again!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L338:
iload 13
istore 12
aload 16
astore 17
iload 2
ifne L413
L339:
getstatic com/chelpus/root/utils/createapkcustom/pat Ljava/util/ArrayList;
new com/android/vending/billing/InAppBillingService/LUCK/PatchesItem
dup
aload 21
aload 22
aload 18
aload 26
getstatic com/chelpus/root/utils/createapkcustom/group Ljava/lang/String;
iconst_0
invokespecial com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/<init>([B[I[B[ILjava/lang/String;Z)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
ldc ""
putstatic com/chelpus/root/utils/createapkcustom/group Ljava/lang/String;
L340:
aload 16
astore 17
iload 13
istore 12
L413:
aload 23
astore 16
iload 10
ifeq L342
L341:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 23
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 30
iload 9
aaload
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 16
L342:
aload 24
ldc "[END]"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L414
iconst_4
putstatic com/chelpus/root/utils/createapkcustom/tag I
L343:
iconst_1
istore 10
goto L414
L344:
getstatic com/chelpus/root/utils/createapkcustom/pat Ljava/util/ArrayList;
new com/android/vending/billing/InAppBillingService/LUCK/PatchesItem
dup
aload 21
aload 22
aload 26
aload 18
getstatic com/chelpus/root/utils/createapkcustom/group Ljava/lang/String;
iconst_1
invokespecial com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/<init>([B[I[B[ILjava/lang/String;Z)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L345:
goto L323
L329:
astore 17
L346:
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "Error LP: Error replaced hex read!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L347:
goto L330
L333:
astore 17
L348:
aload 17
invokevirtual java/lang/Exception/printStackTrace()V
L349:
goto L335
L350:
getstatic com/chelpus/root/utils/createapkcustom/patchedLibs Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifle L351
getstatic com/chelpus/root/utils/createapkcustom/patchedLibs Ljava/util/ArrayList;
invokestatic com/chelpus/root/utils/createapkcustom/zipLib(Ljava/util/ArrayList;)V
L351:
iload 13
ifeq L353
L352:
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 23
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L353:
iload 13
ifne L355
L354:
getstatic com/chelpus/root/utils/createapkcustom/patchteil Z
ifeq L357
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "Not all patterns are replaced, but the program can work, test it!\nCustom Patch not valid for this Version of the Programm or already patched. "
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L355:
invokestatic com/chelpus/root/utils/createapkcustom/clearTemp()V
aload 28
invokevirtual java/io/FileInputStream/close()V
L356:
goto L372
L357:
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "Custom Patch not valid for this Version of the Programm or already patched. "
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L358:
goto L355
L105:
astore 16
aload 16
invokevirtual java/io/IOException/printStackTrace()V
aload 0
areturn
L361:
iload 12
istore 15
iload 13
istore 14
goto L23
L362:
iconst_0
istore 1
aload 18
astore 19
goto L35
L363:
iconst_0
istore 5
goto L41
L367:
aload 31
iload 11
ldc "60"
aastore
aload 20
iload 11
iconst_1
iastore
goto L369
L371:
iload 14
istore 12
iload 14
ifne L96
iconst_0
istore 13
iload 14
istore 12
goto L96
L373:
iload 12
ifne L109
iconst_0
istore 13
goto L109
L374:
iload 12
ifne L115
iconst_0
istore 13
goto L115
L375:
iload 12
ifne L123
iconst_0
istore 13
goto L123
L376:
iload 12
ifne L131
iconst_0
istore 13
goto L131
L377:
iload 12
ifne L139
iconst_0
istore 13
goto L139
L378:
iload 12
ifne L147
iconst_0
istore 13
goto L147
L382:
aload 17
iload 8
ldc "60"
aastore
aload 22
iload 8
iconst_1
iastore
goto L384
L389:
aload 17
iload 8
ldc "60"
aastore
aload 18
iload 8
iconst_0
iastore
goto L391
L393:
aload 17
iload 8
ldc "60"
aastore
aload 18
iload 8
sipush 254
iastore
goto L394
L395:
aload 17
iload 8
ldc "60"
aastore
aload 18
iload 8
sipush 255
iastore
goto L396
L398:
iconst_1
istore 2
goto L399
L402:
aload 16
iload 8
ldc "60"
aastore
aload 18
iload 8
iconst_0
iastore
goto L404
L406:
aload 16
iload 8
ldc "60"
aastore
aload 18
iload 8
sipush 254
iastore
goto L407
L408:
aload 16
iload 8
ldc "60"
aastore
aload 18
iload 8
sipush 255
iastore
goto L409
L411:
iconst_1
istore 1
goto L412
L414:
iload 9
iconst_1
iadd
istore 9
aload 25
astore 26
aload 16
astore 23
iload 14
istore 13
aload 19
astore 18
aload 17
astore 16
aload 20
astore 17
goto L7
.limit locals 32
.limit stack 9
.end method

.method public static patchProcess(Ljava/util/ArrayList;)Z
.signature "(Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;>;)Z"
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch java/nio/BufferUnderflowException from L0 to L1 using L3
.catch java/lang/Exception from L0 to L1 using L4
.catch java/lang/IndexOutOfBoundsException from L5 to L6 using L7
.catch java/nio/BufferUnderflowException from L5 to L6 using L8
.catch java/lang/Exception from L5 to L6 using L9
.catch java/io/FileNotFoundException from L5 to L6 using L2
.catch java/lang/IndexOutOfBoundsException from L10 to L11 using L7
.catch java/nio/BufferUnderflowException from L10 to L11 using L8
.catch java/lang/Exception from L10 to L11 using L9
.catch java/io/FileNotFoundException from L10 to L11 using L2
.catch java/lang/IndexOutOfBoundsException from L12 to L13 using L7
.catch java/nio/BufferUnderflowException from L12 to L13 using L8
.catch java/lang/Exception from L12 to L13 using L9
.catch java/io/FileNotFoundException from L12 to L13 using L2
.catch java/lang/IndexOutOfBoundsException from L14 to L15 using L7
.catch java/nio/BufferUnderflowException from L14 to L15 using L8
.catch java/lang/Exception from L14 to L15 using L9
.catch java/io/FileNotFoundException from L14 to L15 using L2
.catch java/lang/IndexOutOfBoundsException from L15 to L16 using L7
.catch java/nio/BufferUnderflowException from L15 to L16 using L8
.catch java/lang/Exception from L15 to L16 using L9
.catch java/io/FileNotFoundException from L15 to L16 using L2
.catch java/lang/Exception from L16 to L17 using L18
.catch java/lang/IndexOutOfBoundsException from L16 to L17 using L7
.catch java/nio/BufferUnderflowException from L16 to L17 using L8
.catch java/io/FileNotFoundException from L16 to L17 using L2
.catch java/lang/IndexOutOfBoundsException from L17 to L19 using L7
.catch java/nio/BufferUnderflowException from L17 to L19 using L8
.catch java/lang/Exception from L17 to L19 using L9
.catch java/io/FileNotFoundException from L17 to L19 using L2
.catch java/lang/IndexOutOfBoundsException from L19 to L20 using L7
.catch java/nio/BufferUnderflowException from L19 to L20 using L8
.catch java/lang/Exception from L19 to L20 using L9
.catch java/io/FileNotFoundException from L19 to L20 using L2
.catch java/lang/IndexOutOfBoundsException from L20 to L21 using L7
.catch java/nio/BufferUnderflowException from L20 to L21 using L8
.catch java/lang/Exception from L20 to L21 using L9
.catch java/io/FileNotFoundException from L20 to L21 using L2
.catch java/lang/IndexOutOfBoundsException from L22 to L23 using L7
.catch java/nio/BufferUnderflowException from L22 to L23 using L8
.catch java/lang/Exception from L22 to L23 using L9
.catch java/io/FileNotFoundException from L22 to L23 using L2
.catch java/lang/IndexOutOfBoundsException from L23 to L24 using L7
.catch java/nio/BufferUnderflowException from L23 to L24 using L8
.catch java/lang/Exception from L23 to L24 using L9
.catch java/io/FileNotFoundException from L23 to L24 using L2
.catch java/lang/IndexOutOfBoundsException from L24 to L25 using L7
.catch java/nio/BufferUnderflowException from L24 to L25 using L8
.catch java/lang/Exception from L24 to L25 using L9
.catch java/io/FileNotFoundException from L24 to L25 using L2
.catch java/lang/Exception from L26 to L27 using L28
.catch java/lang/IndexOutOfBoundsException from L26 to L27 using L7
.catch java/nio/BufferUnderflowException from L26 to L27 using L8
.catch java/io/FileNotFoundException from L26 to L27 using L2
.catch java/lang/Exception from L27 to L29 using L28
.catch java/lang/IndexOutOfBoundsException from L27 to L29 using L7
.catch java/nio/BufferUnderflowException from L27 to L29 using L8
.catch java/io/FileNotFoundException from L27 to L29 using L2
.catch java/lang/IndexOutOfBoundsException from L29 to L30 using L7
.catch java/nio/BufferUnderflowException from L29 to L30 using L8
.catch java/lang/Exception from L29 to L30 using L9
.catch java/io/FileNotFoundException from L29 to L30 using L2
.catch java/lang/IndexOutOfBoundsException from L30 to L31 using L7
.catch java/nio/BufferUnderflowException from L30 to L31 using L8
.catch java/lang/Exception from L30 to L31 using L9
.catch java/io/FileNotFoundException from L30 to L31 using L2
.catch java/lang/IndexOutOfBoundsException from L31 to L32 using L7
.catch java/nio/BufferUnderflowException from L31 to L32 using L8
.catch java/lang/Exception from L31 to L32 using L9
.catch java/io/FileNotFoundException from L31 to L32 using L2
.catch java/lang/IndexOutOfBoundsException from L33 to L34 using L7
.catch java/nio/BufferUnderflowException from L33 to L34 using L8
.catch java/lang/Exception from L33 to L34 using L9
.catch java/io/FileNotFoundException from L33 to L34 using L2
.catch java/lang/IndexOutOfBoundsException from L35 to L36 using L7
.catch java/nio/BufferUnderflowException from L35 to L36 using L8
.catch java/lang/Exception from L35 to L36 using L9
.catch java/io/FileNotFoundException from L35 to L36 using L2
.catch java/lang/IndexOutOfBoundsException from L36 to L37 using L7
.catch java/nio/BufferUnderflowException from L36 to L37 using L8
.catch java/lang/Exception from L36 to L37 using L9
.catch java/io/FileNotFoundException from L36 to L37 using L2
.catch java/lang/IndexOutOfBoundsException from L38 to L39 using L7
.catch java/nio/BufferUnderflowException from L38 to L39 using L8
.catch java/lang/Exception from L38 to L39 using L9
.catch java/io/FileNotFoundException from L38 to L39 using L2
.catch java/io/FileNotFoundException from L40 to L41 using L2
.catch java/nio/BufferUnderflowException from L40 to L41 using L3
.catch java/lang/Exception from L40 to L41 using L4
.catch java/io/FileNotFoundException from L41 to L42 using L2
.catch java/nio/BufferUnderflowException from L41 to L42 using L3
.catch java/lang/Exception from L41 to L42 using L4
.catch java/io/FileNotFoundException from L43 to L44 using L2
.catch java/nio/BufferUnderflowException from L43 to L44 using L3
.catch java/lang/Exception from L43 to L44 using L4
.catch java/io/FileNotFoundException from L45 to L46 using L2
.catch java/nio/BufferUnderflowException from L45 to L46 using L3
.catch java/lang/Exception from L45 to L46 using L4
.catch java/io/FileNotFoundException from L47 to L48 using L2
.catch java/nio/BufferUnderflowException from L47 to L48 using L3
.catch java/lang/Exception from L47 to L48 using L4
.catch java/io/FileNotFoundException from L49 to L50 using L2
.catch java/nio/BufferUnderflowException from L49 to L50 using L3
.catch java/lang/Exception from L49 to L50 using L4
.catch java/io/FileNotFoundException from L51 to L52 using L2
.catch java/nio/BufferUnderflowException from L51 to L52 using L3
.catch java/lang/Exception from L51 to L52 using L4
.catch java/io/FileNotFoundException from L52 to L53 using L2
.catch java/nio/BufferUnderflowException from L52 to L53 using L3
.catch java/lang/Exception from L52 to L53 using L4
.catch java/lang/IndexOutOfBoundsException from L54 to L55 using L7
.catch java/nio/BufferUnderflowException from L54 to L55 using L8
.catch java/lang/Exception from L54 to L55 using L9
.catch java/io/FileNotFoundException from L54 to L55 using L2
.catch java/lang/IndexOutOfBoundsException from L56 to L57 using L7
.catch java/nio/BufferUnderflowException from L56 to L57 using L8
.catch java/lang/Exception from L56 to L57 using L9
.catch java/io/FileNotFoundException from L56 to L57 using L2
.catch java/io/FileNotFoundException from L58 to L59 using L2
.catch java/nio/BufferUnderflowException from L58 to L59 using L3
.catch java/lang/Exception from L58 to L59 using L4
.catch java/io/FileNotFoundException from L60 to L61 using L2
.catch java/nio/BufferUnderflowException from L60 to L61 using L3
.catch java/lang/Exception from L60 to L61 using L4
.catch java/io/FileNotFoundException from L62 to L63 using L2
.catch java/nio/BufferUnderflowException from L62 to L63 using L3
.catch java/lang/Exception from L62 to L63 using L4
.catch java/io/FileNotFoundException from L64 to L65 using L2
.catch java/nio/BufferUnderflowException from L64 to L65 using L3
.catch java/lang/Exception from L64 to L65 using L4
.catch java/io/FileNotFoundException from L65 to L66 using L2
.catch java/nio/BufferUnderflowException from L65 to L66 using L3
.catch java/lang/Exception from L65 to L66 using L4
L0:
new java/io/RandomAccessFile
dup
getstatic com/chelpus/root/utils/createapkcustom/localFile2 Ljava/io/File;
ldc "rw"
invokespecial java/io/RandomAccessFile/<init>(Ljava/io/File;Ljava/lang/String;)V
invokevirtual java/io/RandomAccessFile/getChannel()Ljava/nio/channels/FileChannel;
astore 8
aload 8
getstatic java/nio/channels/FileChannel$MapMode/READ_WRITE Ljava/nio/channels/FileChannel$MapMode;
lconst_0
aload 8
invokevirtual java/nio/channels/FileChannel/size()J
l2i
i2l
invokevirtual java/nio/channels/FileChannel/map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
astore 7
aload 0
invokevirtual java/util/ArrayList/toArray()[Ljava/lang/Object;
arraylength
anewarray com/android/vending/billing/InAppBillingService/LUCK/PatchesItem
astore 9
aload 0
aload 0
invokevirtual java/util/ArrayList/size()I
anewarray com/android/vending/billing/InAppBillingService/LUCK/PatchesItem
invokevirtual java/util/ArrayList/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
checkcast [Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;
checkcast [Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;
astore 9
L1:
iconst_m1
istore 4
L5:
aload 7
invokevirtual java/nio/MappedByteBuffer/hasRemaining()Z
ifeq L41
aload 7
iload 4
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 7
invokevirtual java/nio/MappedByteBuffer/position()I
istore 5
aload 7
invokevirtual java/nio/MappedByteBuffer/get()B
istore 2
L6:
iconst_0
istore 3
aload 7
astore 0
L67:
iload 5
istore 4
aload 0
astore 7
L10:
iload 3
aload 9
arraylength
if_icmpge L5
aload 0
iload 5
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
iload 2
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/origByte [B
iconst_0
baload
if_icmpeq L15
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/origMask [I
iconst_0
iaload
iconst_1
if_icmpeq L15
L11:
aload 0
astore 7
L12:
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/origMask [I
iconst_0
iaload
iconst_1
if_icmple L68
L13:
aload 0
astore 7
L14:
iload 2
getstatic com/chelpus/root/utils/createapkcustom/search Ljava/util/ArrayList;
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/origMask [I
iconst_0
iaload
iconst_2
isub
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/Byte
invokevirtual java/lang/Byte/byteValue()B
if_icmpne L68
L15:
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/repMask [I
iconst_0
iaload
ifne L16
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/repByte [B
iconst_0
iload 2
bastore
L16:
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/repMask [I
iconst_0
iaload
iconst_1
if_icmple L17
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/repMask [I
iconst_0
iaload
sipush 253
if_icmpge L17
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/repByte [B
iconst_0
getstatic com/chelpus/root/utils/createapkcustom/search Ljava/util/ArrayList;
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/repMask [I
iconst_0
iaload
iconst_2
isub
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/Byte
invokevirtual java/lang/Byte/byteValue()B
bastore
L17:
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/repMask [I
iconst_0
iaload
sipush 253
if_icmpne L19
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/repByte [B
iconst_0
iload 2
bipush 15
iand
iload 2
bipush 15
iand
bipush 16
imul
iadd
i2b
bastore
L19:
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/repMask [I
iconst_0
iaload
sipush 254
if_icmpne L20
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/repByte [B
iconst_0
iload 2
bipush 15
iand
bipush 16
iadd
i2b
bastore
L20:
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/repMask [I
iconst_0
iaload
sipush 255
if_icmpne L21
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/repByte [B
iconst_0
iload 2
bipush 15
iand
i2b
bastore
L21:
iconst_1
istore 4
L22:
aload 0
iload 5
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 0
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
L23:
iload 1
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/origByte [B
iload 4
baload
if_icmpeq L26
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/origMask [I
iload 4
iaload
iconst_1
if_icmple L24
iload 1
getstatic com/chelpus/root/utils/createapkcustom/search Ljava/util/ArrayList;
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/origMask [I
iload 4
iaload
iconst_2
isub
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/Byte
invokevirtual java/lang/Byte/byteValue()B
if_icmpeq L26
L24:
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/origMask [I
iload 4
iaload
istore 6
L25:
aload 0
astore 7
iload 6
iconst_1
if_icmpne L68
L26:
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/repMask [I
iload 4
iaload
ifne L27
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/repByte [B
iload 4
iload 1
bastore
L27:
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/repMask [I
iload 4
iaload
iconst_1
if_icmple L29
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/repMask [I
iload 4
iaload
sipush 253
if_icmpge L29
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/repMask [I
iload 4
iaload
istore 6
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/repByte [B
iload 4
getstatic com/chelpus/root/utils/createapkcustom/search Ljava/util/ArrayList;
iload 6
iconst_2
isub
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/Byte
invokevirtual java/lang/Byte/byteValue()B
bastore
L29:
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/repMask [I
iconst_0
iaload
sipush 253
if_icmpne L30
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/repByte [B
iconst_0
iload 2
bipush 15
iand
iload 2
bipush 15
iand
bipush 16
imul
iadd
i2b
bastore
L30:
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/repMask [I
iload 4
iaload
sipush 254
if_icmpne L31
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/repByte [B
iload 4
iload 1
bipush 15
iand
bipush 16
iadd
i2b
bastore
L31:
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/repMask [I
iload 4
iaload
sipush 255
if_icmpne L32
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/repByte [B
iload 4
iload 1
bipush 15
iand
i2b
bastore
L32:
iload 4
iconst_1
iadd
istore 4
L33:
iload 4
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/origByte [B
arraylength
if_icmpne L56
L34:
aload 0
astore 7
L35:
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/insert Z
ifeq L36
aload 0
invokevirtual java/nio/MappedByteBuffer/position()I
istore 4
aload 8
invokevirtual java/nio/channels/FileChannel/size()J
l2i
iload 4
isub
istore 6
iload 6
newarray byte
astore 7
aload 0
aload 7
iconst_0
iload 6
invokevirtual java/nio/MappedByteBuffer/get([BII)Ljava/nio/ByteBuffer;
pop
aload 7
invokestatic java/nio/ByteBuffer/wrap([B)Ljava/nio/ByteBuffer;
astore 0
aload 8
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/repByte [B
arraylength
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/origByte [B
arraylength
isub
iload 4
iadd
i2l
invokevirtual java/nio/channels/FileChannel/position(J)Ljava/nio/channels/FileChannel;
pop
aload 8
aload 0
invokevirtual java/nio/channels/FileChannel/write(Ljava/nio/ByteBuffer;)I
pop
aload 8
getstatic java/nio/channels/FileChannel$MapMode/READ_WRITE Ljava/nio/channels/FileChannel$MapMode;
lconst_0
aload 8
invokevirtual java/nio/channels/FileChannel/size()J
l2i
i2l
invokevirtual java/nio/channels/FileChannel/map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
astore 7
aload 7
iload 4
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L36:
aload 8
iload 5
i2l
invokevirtual java/nio/channels/FileChannel/position(J)Ljava/nio/channels/FileChannel;
pop
aload 8
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/repByte [B
invokestatic java/nio/ByteBuffer/wrap([B)Ljava/nio/ByteBuffer;
invokevirtual java/nio/channels/FileChannel/write(Ljava/nio/ByteBuffer;)I
pop
aload 7
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "\nPattern N"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 3
iconst_1
iadd
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ": Patch done! \n(Offset: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 5
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ")\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 9
iload 3
aaload
iconst_1
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/result Z
iconst_1
putstatic com/chelpus/root/utils/createapkcustom/patchteil Z
L37:
goto L68
L18:
astore 7
L38:
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/repMask [I
iconst_0
iaload
iconst_2
isub
istore 4
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Byte N"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 4
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " not found! Please edit search pattern for byte "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 4
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc "."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L39:
goto L17
L7:
astore 0
L40:
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "Byte by search not found! Please edit pattern for search.\n"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L41:
aload 8
invokevirtual java/nio/channels/FileChannel/close()V
L42:
iconst_0
istore 4
L43:
iload 4
aload 9
arraylength
if_icmpge L69
aload 9
iload 4
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/result Z
ifne L64
L44:
iconst_0
istore 6
iconst_0
istore 3
L45:
aload 9
iload 4
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/group Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L70
L46:
iconst_0
istore 5
L71:
iload 3
istore 6
L47:
iload 5
aload 9
arraylength
if_icmpge L70
L48:
iload 3
istore 6
L49:
aload 9
iload 4
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/group Ljava/lang/String;
aload 9
iload 5
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/group Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L72
L50:
iload 3
istore 6
L51:
aload 9
iload 5
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/result Z
ifeq L72
getstatic com/chelpus/root/utils/createapkcustom/multidex Z
ifne L52
getstatic com/chelpus/root/utils/createapkcustom/multilib_patch Z
ifeq L53
L52:
iconst_1
putstatic com/chelpus/root/utils/createapkcustom/goodResult Z
L53:
iconst_1
istore 6
L72:
iload 5
iconst_1
iadd
istore 5
iload 6
istore 3
goto L71
L28:
astore 7
L54:
aload 9
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/repMask [I
iload 4
iaload
iconst_2
isub
istore 6
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Byte N"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 6
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " not found! Please edit search pattern for byte "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 6
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc "."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L55:
goto L29
L56:
aload 0
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
L57:
goto L23
L70:
iload 6
ifne L73
L58:
getstatic com/chelpus/root/utils/createapkcustom/goodResult Z
ifne L73
getstatic com/chelpus/root/utils/createapkcustom/multidex Z
ifeq L60
getstatic com/chelpus/root/utils/createapkcustom/goodResult Z
ifne L73
getstatic com/chelpus/root/utils/createapkcustom/localFile2 Ljava/io/File;
getstatic com/chelpus/root/utils/createapkcustom/classesFiles Ljava/util/ArrayList;
getstatic com/chelpus/root/utils/createapkcustom/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
invokevirtual java/io/File/equals(Ljava/lang/Object;)Z
ifeq L73
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "\nPattern N"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 4
iconst_1
iadd
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ":\nError LP: Pattern not found!\nor patch is already applied?!\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L59:
goto L73
L60:
getstatic com/chelpus/root/utils/createapkcustom/multilib_patch Z
ifeq L62
getstatic com/chelpus/root/utils/createapkcustom/goodResult Z
ifne L73
getstatic com/chelpus/root/utils/createapkcustom/localFile2 Ljava/io/File;
new java/io/File
dup
getstatic com/chelpus/root/utils/createapkcustom/libs Ljava/util/ArrayList;
getstatic com/chelpus/root/utils/createapkcustom/libs Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/String
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/equals(Ljava/lang/Object;)Z
ifeq L73
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "\nPattern N"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 4
iconst_1
iadd
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ":\nError LP: Pattern not found!\nor patch is already applied?!\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L61:
goto L73
L62:
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "\nPattern N"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 4
iconst_1
iadd
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ":\nError LP: Pattern not found!\nor patch is already applied?!\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L63:
goto L73
L64:
getstatic com/chelpus/root/utils/createapkcustom/multidex Z
ifne L65
getstatic com/chelpus/root/utils/createapkcustom/multilib_patch Z
ifeq L73
L65:
aload 9
iload 4
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItem/group Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L73
iconst_1
putstatic com/chelpus/root/utils/createapkcustom/goodResult Z
L66:
goto L73
L2:
astore 0
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "Error LP: Program files are not found!\nMove Program to internal storage."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L69:
getstatic com/chelpus/root/utils/createapkcustom/tag I
iconst_1
if_icmpne L74
getstatic com/chelpus/root/utils/createapkcustom/localFile2 Ljava/io/File;
invokestatic com/chelpus/root/utils/createapkcustom/fixadler(Ljava/io/File;)V
L74:
iconst_1
ireturn
L4:
astore 0
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception e"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
goto L69
L3:
astore 0
goto L69
L9:
astore 0
goto L41
L68:
iload 3
iconst_1
iadd
istore 3
aload 7
astore 0
goto L67
L8:
astore 0
goto L41
L73:
iload 4
iconst_1
iadd
istore 4
goto L43
.limit locals 10
.limit stack 6
.end method

.method public static searchProcess(Ljava/util/ArrayList;)Z
.signature "(Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;>;)Z"
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch java/nio/BufferUnderflowException from L0 to L1 using L3
.catch java/lang/Exception from L0 to L1 using L4
.catch java/io/FileNotFoundException from L5 to L6 using L2
.catch java/nio/BufferUnderflowException from L5 to L6 using L3
.catch java/lang/Exception from L5 to L6 using L4
.catch java/io/FileNotFoundException from L7 to L8 using L2
.catch java/nio/BufferUnderflowException from L7 to L8 using L3
.catch java/lang/Exception from L7 to L8 using L4
.catch java/io/FileNotFoundException from L9 to L10 using L2
.catch java/nio/BufferUnderflowException from L9 to L10 using L3
.catch java/lang/Exception from L9 to L10 using L4
.catch java/lang/Exception from L11 to L12 using L13
.catch java/io/FileNotFoundException from L11 to L12 using L2
.catch java/nio/BufferUnderflowException from L11 to L12 using L3
.catch java/lang/Exception from L14 to L15 using L13
.catch java/io/FileNotFoundException from L14 to L15 using L2
.catch java/nio/BufferUnderflowException from L14 to L15 using L3
.catch java/lang/Exception from L16 to L17 using L13
.catch java/io/FileNotFoundException from L16 to L17 using L2
.catch java/nio/BufferUnderflowException from L16 to L17 using L3
.catch java/lang/Exception from L18 to L19 using L13
.catch java/io/FileNotFoundException from L18 to L19 using L2
.catch java/nio/BufferUnderflowException from L18 to L19 using L3
.catch java/lang/Exception from L20 to L21 using L13
.catch java/io/FileNotFoundException from L20 to L21 using L2
.catch java/nio/BufferUnderflowException from L20 to L21 using L3
.catch java/lang/Exception from L22 to L23 using L13
.catch java/io/FileNotFoundException from L22 to L23 using L2
.catch java/nio/BufferUnderflowException from L22 to L23 using L3
.catch java/lang/Exception from L24 to L25 using L13
.catch java/io/FileNotFoundException from L24 to L25 using L2
.catch java/nio/BufferUnderflowException from L24 to L25 using L3
.catch java/lang/Exception from L26 to L27 using L13
.catch java/io/FileNotFoundException from L26 to L27 using L2
.catch java/nio/BufferUnderflowException from L26 to L27 using L3
.catch java/lang/Exception from L28 to L29 using L13
.catch java/io/FileNotFoundException from L28 to L29 using L2
.catch java/nio/BufferUnderflowException from L28 to L29 using L3
.catch java/lang/Exception from L30 to L31 using L13
.catch java/io/FileNotFoundException from L30 to L31 using L2
.catch java/nio/BufferUnderflowException from L30 to L31 using L3
.catch java/lang/Exception from L32 to L33 using L13
.catch java/io/FileNotFoundException from L32 to L33 using L2
.catch java/nio/BufferUnderflowException from L32 to L33 using L3
.catch java/lang/Exception from L34 to L35 using L13
.catch java/io/FileNotFoundException from L34 to L35 using L2
.catch java/nio/BufferUnderflowException from L34 to L35 using L3
.catch java/lang/Exception from L36 to L37 using L13
.catch java/io/FileNotFoundException from L36 to L37 using L2
.catch java/nio/BufferUnderflowException from L36 to L37 using L3
.catch java/lang/Exception from L38 to L39 using L13
.catch java/io/FileNotFoundException from L38 to L39 using L2
.catch java/nio/BufferUnderflowException from L38 to L39 using L3
.catch java/lang/Exception from L40 to L41 using L13
.catch java/io/FileNotFoundException from L40 to L41 using L2
.catch java/nio/BufferUnderflowException from L40 to L41 using L3
.catch java/lang/Exception from L42 to L43 using L13
.catch java/io/FileNotFoundException from L42 to L43 using L2
.catch java/nio/BufferUnderflowException from L42 to L43 using L3
.catch java/lang/Exception from L44 to L45 using L13
.catch java/io/FileNotFoundException from L44 to L45 using L2
.catch java/nio/BufferUnderflowException from L44 to L45 using L3
.catch java/lang/Exception from L46 to L47 using L13
.catch java/io/FileNotFoundException from L46 to L47 using L2
.catch java/nio/BufferUnderflowException from L46 to L47 using L3
.catch java/lang/Exception from L48 to L49 using L13
.catch java/io/FileNotFoundException from L48 to L49 using L2
.catch java/nio/BufferUnderflowException from L48 to L49 using L3
.catch java/lang/Exception from L50 to L51 using L13
.catch java/io/FileNotFoundException from L50 to L51 using L2
.catch java/nio/BufferUnderflowException from L50 to L51 using L3
.catch java/lang/Exception from L52 to L53 using L13
.catch java/io/FileNotFoundException from L52 to L53 using L2
.catch java/nio/BufferUnderflowException from L52 to L53 using L3
.catch java/lang/Exception from L54 to L55 using L13
.catch java/io/FileNotFoundException from L54 to L55 using L2
.catch java/nio/BufferUnderflowException from L54 to L55 using L3
.catch java/io/FileNotFoundException from L56 to L57 using L2
.catch java/nio/BufferUnderflowException from L56 to L57 using L3
.catch java/lang/Exception from L56 to L57 using L4
.catch java/io/FileNotFoundException from L58 to L59 using L2
.catch java/nio/BufferUnderflowException from L58 to L59 using L3
.catch java/lang/Exception from L58 to L59 using L4
.catch java/io/FileNotFoundException from L60 to L61 using L2
.catch java/nio/BufferUnderflowException from L60 to L61 using L3
.catch java/lang/Exception from L60 to L61 using L4
.catch java/io/FileNotFoundException from L62 to L63 using L2
.catch java/nio/BufferUnderflowException from L62 to L63 using L3
.catch java/lang/Exception from L62 to L63 using L4
.catch java/io/FileNotFoundException from L64 to L65 using L2
.catch java/nio/BufferUnderflowException from L64 to L65 using L3
.catch java/lang/Exception from L64 to L65 using L4
.catch java/io/FileNotFoundException from L66 to L67 using L2
.catch java/nio/BufferUnderflowException from L66 to L67 using L3
.catch java/lang/Exception from L66 to L67 using L4
.catch java/io/FileNotFoundException from L68 to L69 using L2
.catch java/nio/BufferUnderflowException from L68 to L69 using L3
.catch java/lang/Exception from L68 to L69 using L4
.catch java/io/FileNotFoundException from L70 to L71 using L2
.catch java/nio/BufferUnderflowException from L70 to L71 using L3
.catch java/lang/Exception from L70 to L71 using L4
.catch java/io/FileNotFoundException from L72 to L73 using L2
.catch java/nio/BufferUnderflowException from L72 to L73 using L3
.catch java/lang/Exception from L72 to L73 using L4
.catch java/io/FileNotFoundException from L74 to L75 using L2
.catch java/nio/BufferUnderflowException from L74 to L75 using L3
.catch java/lang/Exception from L74 to L75 using L4
.catch java/io/FileNotFoundException from L76 to L77 using L2
.catch java/nio/BufferUnderflowException from L76 to L77 using L3
.catch java/lang/Exception from L76 to L77 using L4
.catch java/lang/Exception from L78 to L79 using L80
.catch java/io/FileNotFoundException from L78 to L79 using L2
.catch java/nio/BufferUnderflowException from L78 to L79 using L3
.catch java/io/FileNotFoundException from L81 to L82 using L2
.catch java/nio/BufferUnderflowException from L81 to L82 using L3
.catch java/lang/Exception from L81 to L82 using L4
.catch java/io/FileNotFoundException from L83 to L84 using L2
.catch java/nio/BufferUnderflowException from L83 to L84 using L3
.catch java/lang/Exception from L83 to L84 using L4
.catch java/io/FileNotFoundException from L85 to L86 using L2
.catch java/nio/BufferUnderflowException from L85 to L86 using L3
.catch java/lang/Exception from L85 to L86 using L4
.catch java/io/FileNotFoundException from L87 to L88 using L2
.catch java/nio/BufferUnderflowException from L87 to L88 using L3
.catch java/lang/Exception from L87 to L88 using L4
iconst_1
istore 10
iconst_1
istore 11
iconst_1
istore 12
iconst_1
istore 6
iload 10
istore 7
iload 11
istore 8
iload 12
istore 9
L0:
new java/io/RandomAccessFile
dup
getstatic com/chelpus/root/utils/createapkcustom/localFile2 Ljava/io/File;
ldc "rw"
invokespecial java/io/RandomAccessFile/<init>(Ljava/io/File;Ljava/lang/String;)V
invokevirtual java/io/RandomAccessFile/getChannel()Ljava/nio/channels/FileChannel;
astore 15
L1:
iload 10
istore 7
iload 11
istore 8
iload 12
istore 9
L5:
aload 15
getstatic java/nio/channels/FileChannel$MapMode/READ_WRITE Ljava/nio/channels/FileChannel$MapMode;
lconst_0
aload 15
invokevirtual java/nio/channels/FileChannel/size()J
l2i
i2l
invokevirtual java/nio/channels/FileChannel/map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
astore 16
L6:
iload 10
istore 7
iload 11
istore 8
iload 12
istore 9
L7:
aload 0
invokevirtual java/util/ArrayList/toArray()[Ljava/lang/Object;
arraylength
anewarray com/android/vending/billing/InAppBillingService/LUCK/SearchItem
astore 17
L8:
iload 10
istore 7
iload 11
istore 8
iload 12
istore 9
L9:
aload 0
aload 0
invokevirtual java/util/ArrayList/size()I
anewarray com/android/vending/billing/InAppBillingService/LUCK/SearchItem
invokevirtual java/util/ArrayList/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
checkcast [Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;
checkcast [Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;
astore 0
L10:
lconst_0
lstore 13
L89:
iload 10
istore 7
iload 11
istore 8
L11:
aload 16
invokevirtual java/nio/MappedByteBuffer/hasRemaining()Z
ifeq L57
L12:
iload 10
istore 7
iload 11
istore 8
L14:
aload 16
invokevirtual java/nio/MappedByteBuffer/position()I
istore 5
L15:
iload 10
istore 7
iload 11
istore 8
L16:
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
istore 2
L17:
iconst_0
istore 3
L90:
iload 10
istore 7
iload 11
istore 8
L18:
iload 3
aload 0
arraylength
if_icmpge L91
L19:
iload 10
istore 7
iload 11
istore 8
L20:
aload 16
iload 5
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L21:
iload 10
istore 7
iload 11
istore 8
L22:
aload 0
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/SearchItem/result Z
ifne L92
L23:
iload 10
istore 7
iload 11
istore 8
L24:
iload 2
aload 0
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/SearchItem/origByte [B
iconst_0
baload
if_icmpeq L27
L25:
iload 10
istore 7
iload 11
istore 8
L26:
aload 0
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/SearchItem/origMask [I
iconst_0
iaload
ifeq L92
L27:
iload 10
istore 7
iload 11
istore 8
L28:
aload 0
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/SearchItem/origMask [I
iconst_0
iaload
ifeq L31
L29:
iload 10
istore 7
iload 11
istore 8
L30:
aload 0
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/SearchItem/repByte [B
iconst_0
iload 2
bastore
L31:
iconst_1
istore 4
iload 10
istore 7
iload 11
istore 8
L32:
aload 16
iload 5
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L33:
iload 10
istore 7
iload 11
istore 8
L34:
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
L35:
iload 10
istore 7
iload 11
istore 8
L36:
aload 0
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/SearchItem/result Z
ifne L39
L37:
iload 10
istore 7
iload 11
istore 8
L38:
iload 1
aload 0
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/SearchItem/origByte [B
iload 4
baload
if_icmpeq L41
L39:
iload 10
istore 7
iload 11
istore 8
L40:
aload 0
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/SearchItem/origMask [I
iload 4
iaload
ifeq L92
L41:
iload 10
istore 7
iload 11
istore 8
L42:
aload 0
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/SearchItem/origMask [I
iload 4
iaload
ifle L45
L43:
iload 10
istore 7
iload 11
istore 8
L44:
aload 0
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/SearchItem/repByte [B
iload 4
iload 1
bastore
L45:
iload 4
iconst_1
iadd
istore 4
iload 10
istore 7
iload 11
istore 8
L46:
iload 4
aload 0
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/SearchItem/origByte [B
arraylength
if_icmpne L93
L47:
iload 10
istore 7
iload 11
istore 8
L48:
aload 0
iload 3
aaload
iconst_1
putfield com/android/vending/billing/InAppBillingService/LUCK/SearchItem/result Z
L49:
iload 10
istore 7
iload 11
istore 8
L50:
iconst_1
putstatic com/chelpus/root/utils/createapkcustom/patchteil Z
L51:
goto L92
L93:
iload 10
istore 7
iload 11
istore 8
L52:
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
L53:
goto L35
L91:
iload 10
istore 7
iload 11
istore 8
L54:
aload 16
iload 5
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L55:
lload 13
lconst_1
ladd
lstore 13
goto L89
L13:
astore 16
iload 10
istore 7
iload 11
istore 8
iload 12
istore 9
L56:
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 16
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L57:
iload 10
istore 7
iload 11
istore 8
iload 12
istore 9
L58:
aload 15
invokevirtual java/nio/channels/FileChannel/close()V
L59:
iconst_0
istore 3
L94:
iload 6
istore 7
iload 6
istore 8
iload 6
istore 9
L60:
iload 3
aload 0
arraylength
if_icmpge L95
L61:
iload 6
istore 7
iload 6
istore 8
iload 6
istore 9
iload 6
istore 10
L62:
aload 0
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/SearchItem/result Z
ifne L96
L63:
iload 6
istore 7
iload 6
istore 8
iload 6
istore 9
L64:
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Bytes by serach N"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 3
iconst_1
iadd
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ":\nError LP: Bytes not found!"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L65:
iconst_0
istore 10
goto L96
L97:
iload 6
istore 7
iload 6
istore 10
iload 6
istore 8
iload 6
istore 9
L66:
iload 3
aload 0
arraylength
if_icmpge L98
L67:
iload 6
istore 7
iload 6
istore 8
iload 6
istore 9
L68:
aload 0
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/SearchItem/result Z
ifeq L99
L69:
iload 6
istore 7
iload 6
istore 8
iload 6
istore 9
L70:
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "\nBytes by search N"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 3
iconst_1
iadd
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L71:
goto L99
L100:
iload 6
istore 7
iload 6
istore 8
iload 6
istore 9
L72:
iload 4
aload 0
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/SearchItem/origMask [I
arraylength
if_icmpge L101
L73:
iload 6
istore 7
iload 6
istore 8
iload 6
istore 9
L74:
aload 0
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/SearchItem/origMask [I
iload 4
iaload
iconst_1
if_icmple L102
L75:
iload 6
istore 7
iload 6
istore 8
iload 6
istore 9
L76:
aload 0
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/SearchItem/origMask [I
iload 4
iaload
istore 5
L77:
iload 5
iconst_2
isub
istore 5
iload 6
istore 7
iload 6
istore 8
L78:
getstatic com/chelpus/root/utils/createapkcustom/search Ljava/util/ArrayList;
iload 5
aload 0
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/SearchItem/repByte [B
iload 4
baload
invokestatic java/lang/Byte/valueOf(B)Ljava/lang/Byte;
invokevirtual java/util/ArrayList/set(ILjava/lang/Object;)Ljava/lang/Object;
pop
L79:
iload 6
istore 7
iload 6
istore 8
iload 6
istore 9
L81:
aload 0
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/SearchItem/result Z
ifeq L102
L82:
iload 6
istore 7
iload 6
istore 8
iload 6
istore 9
L83:
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "R"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 5
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc "="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/createapkcustom/search Ljava/util/ArrayList;
iload 5
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/Byte
invokevirtual java/lang/Byte/byteValue()B
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
L84:
goto L102
L80:
astore 15
iload 6
istore 7
iload 6
istore 8
iload 6
istore 9
L85:
getstatic com/chelpus/root/utils/createapkcustom/search Ljava/util/ArrayList;
iload 5
aload 0
iload 3
aaload
getfield com/android/vending/billing/InAppBillingService/LUCK/SearchItem/repByte [B
iload 4
baload
invokestatic java/lang/Byte/valueOf(B)Ljava/lang/Byte;
invokevirtual java/util/ArrayList/add(ILjava/lang/Object;)V
L86:
goto L79
L2:
astore 0
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc "Error LP: Program files are not found!\nMove Program to internal storage."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iload 7
istore 10
L98:
iload 10
ireturn
L101:
iload 6
istore 7
iload 6
istore 8
iload 6
istore 9
L87:
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
ldc ""
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L88:
iload 3
iconst_1
iadd
istore 3
goto L97
L3:
astore 0
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception e"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/nio/BufferUnderflowException/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iload 8
ireturn
L4:
astore 0
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception e"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iload 9
ireturn
L92:
iload 3
iconst_1
iadd
istore 3
goto L90
L96:
iload 3
iconst_1
iadd
istore 3
iload 10
istore 6
goto L94
L95:
iconst_0
istore 3
goto L97
L99:
iconst_0
istore 4
goto L100
L102:
iload 4
iconst_1
iadd
istore 4
goto L100
.limit locals 18
.limit stack 6
.end method

.method public static searchlib(ILjava/lang/String;)Ljava/util/ArrayList;
.signature "(ILjava/lang/String;)Ljava/util/ArrayList<Ljava/lang/String;>;"
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
.catch java/io/FileNotFoundException from L1 to L4 using L2
.catch java/lang/Exception from L1 to L4 using L3
.catch java/io/FileNotFoundException from L4 to L5 using L2
.catch java/lang/Exception from L4 to L5 using L3
.catch java/io/FileNotFoundException from L6 to L7 using L2
.catch java/lang/Exception from L6 to L7 using L3
.catch java/io/FileNotFoundException from L8 to L9 using L2
.catch java/lang/Exception from L8 to L9 using L3
.catch java/io/FileNotFoundException from L9 to L10 using L2
.catch java/lang/Exception from L9 to L10 using L3
.catch java/io/FileNotFoundException from L11 to L12 using L2
.catch java/lang/Exception from L11 to L12 using L3
.catch java/io/FileNotFoundException from L12 to L13 using L2
.catch java/lang/Exception from L12 to L13 using L3
.catch java/io/FileNotFoundException from L14 to L15 using L2
.catch java/lang/Exception from L14 to L15 using L3
.catch java/io/FileNotFoundException from L15 to L16 using L2
.catch java/lang/Exception from L15 to L16 using L3
.catch java/io/FileNotFoundException from L17 to L18 using L2
.catch java/lang/Exception from L17 to L18 using L3
.catch java/io/FileNotFoundException from L18 to L19 using L2
.catch java/lang/Exception from L18 to L19 using L3
.catch java/io/FileNotFoundException from L19 to L20 using L2
.catch java/lang/Exception from L19 to L20 using L3
.catch java/io/FileNotFoundException from L20 to L21 using L2
.catch java/lang/Exception from L20 to L21 using L3
.catch java/io/FileNotFoundException from L21 to L22 using L2
.catch java/lang/Exception from L21 to L22 using L3
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 2
L0:
new java/io/File
dup
getstatic com/chelpus/root/utils/createapkcustom/appdir Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 3
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/createapkcustom/packageName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
putstatic com/chelpus/root/utils/createapkcustom/crkapk Ljava/io/File;
getstatic com/chelpus/root/utils/createapkcustom/crkapk Ljava/io/File;
invokevirtual java/io/File/exists()Z
ifne L1
aload 3
getstatic com/chelpus/root/utils/createapkcustom/crkapk Ljava/io/File;
invokestatic com/chelpus/Utils/copyFile(Ljava/io/File;Ljava/io/File;)V
L1:
getstatic com/chelpus/root/utils/createapkcustom/crkapk Ljava/io/File;
invokestatic com/chelpus/root/utils/createapkcustom/extractLibs(Ljava/io/File;)V
aload 1
invokevirtual java/lang/String/trim()Ljava/lang/String;
ldc "*"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L23
iconst_1
putstatic com/chelpus/root/utils/createapkcustom/multilib_patch Z
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 1
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/tmp/lib/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
ldc ".so"
aload 1
invokevirtual com/chelpus/Utils/findFileEndText(Ljava/io/File;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;
pop
aload 1
invokevirtual java/util/ArrayList/size()I
ifle L24
aload 1
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 1
L4:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L24
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
astore 3
aload 3
invokevirtual java/io/File/length()J
lconst_0
lcmp
ifle L4
aload 2
aload 3
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L5:
goto L4
L2:
astore 1
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Lib not found!"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/io/FileNotFoundException/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L24:
aload 2
areturn
L23:
iload 0
tableswitch 0
L6
L9
L12
L15
L18
default : L25
L25:
aload 2
areturn
L6:
aload 2
invokevirtual java/util/ArrayList/clear()V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/tmp/lib/armeabi/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 3
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/tmp/lib/armeabi/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L8
aload 2
aload 3
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L7:
aload 2
areturn
L3:
astore 1
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Lib select error: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 2
areturn
L8:
new java/io/FileNotFoundException
dup
invokespecial java/io/FileNotFoundException/<init>()V
athrow
L9:
aload 2
invokevirtual java/util/ArrayList/clear()V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/tmp/lib/armeabi-v7a/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 3
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/tmp/lib/armeabi-v7a/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L11
aload 2
aload 3
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L10:
aload 2
areturn
L11:
new java/io/FileNotFoundException
dup
invokespecial java/io/FileNotFoundException/<init>()V
athrow
L12:
aload 2
invokevirtual java/util/ArrayList/clear()V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/tmp/lib/mips/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 3
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/tmp/lib/mips/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L14
aload 2
aload 3
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L13:
aload 2
areturn
L14:
new java/io/FileNotFoundException
dup
invokespecial java/io/FileNotFoundException/<init>()V
athrow
L15:
aload 2
invokevirtual java/util/ArrayList/clear()V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/tmp/lib/x86/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 3
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/tmp/lib/x86/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L17
aload 2
aload 3
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L16:
aload 2
areturn
L17:
new java/io/FileNotFoundException
dup
invokespecial java/io/FileNotFoundException/<init>()V
athrow
L18:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/tmp/lib/armeabi/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 3
new java/io/File
dup
aload 3
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L19
aload 2
aload 3
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L19:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/tmp/lib/armeabi-v7a/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 3
new java/io/File
dup
aload 3
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L20
aload 2
aload 3
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L20:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/tmp/lib/mips/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 3
new java/io/File
dup
aload 3
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L21
aload 2
aload 3
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L21:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/tmp/lib/x86/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L24
aload 2
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L22:
aload 2
areturn
.limit locals 4
.limit stack 5
.end method

.method public static unzip(Ljava/io/File;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
.catch java/lang/Exception from L4 to L5 using L2
.catch java/lang/Exception from L5 to L6 using L2
.catch java/lang/Exception from L7 to L8 using L2
.catch net/lingala/zip4j/exception/ZipException from L9 to L10 using L11
.catch java/lang/Exception from L9 to L10 using L12
.catch java/lang/Exception from L13 to L14 using L2
.catch java/lang/Exception from L15 to L16 using L2
getstatic com/chelpus/root/utils/createapkcustom/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
L0:
new java/io/FileInputStream
dup
aload 0
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
astore 2
new java/util/zip/ZipInputStream
dup
aload 2
invokespecial java/util/zip/ZipInputStream/<init>(Ljava/io/InputStream;)V
astore 3
L1:
aload 3
invokevirtual java/util/zip/ZipInputStream/getNextEntry()Ljava/util/zip/ZipEntry;
astore 4
L3:
aload 4
ifnull L15
L4:
aload 4
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "classes"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L1
aload 4
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
ldc ".dex"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L1
aload 4
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
ldc "/"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L1
new java/io/FileOutputStream
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
astore 5
sipush 1024
newarray byte
astore 6
L5:
aload 3
aload 6
invokevirtual java/util/zip/ZipInputStream/read([B)I
istore 1
L6:
iload 1
iconst_m1
if_icmpeq L13
L7:
aload 5
aload 6
iconst_0
iload 1
invokevirtual java/io/FileOutputStream/write([BII)V
L8:
goto L5
L2:
astore 2
L9:
new net/lingala/zip4j/core/ZipFile
dup
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/<init>(Ljava/io/File;)V
ldc "classes.dex"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual net/lingala/zip4j/core/ZipFile/extractFile(Ljava/lang/String;Ljava/lang/String;)V
getstatic com/chelpus/root/utils/createapkcustom/classesFiles Ljava/util/ArrayList;
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "classes.dex"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L10:
return
L13:
getstatic com/chelpus/root/utils/createapkcustom/classesFiles Ljava/util/ArrayList;
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 3
invokevirtual java/util/zip/ZipInputStream/closeEntry()V
aload 5
invokevirtual java/io/FileOutputStream/close()V
L14:
goto L1
L15:
aload 3
invokevirtual java/util/zip/ZipInputStream/close()V
aload 2
invokevirtual java/io/FileInputStream/close()V
L16:
return
L11:
astore 0
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Error LP: Error classes.dex decompress! "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception e1"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
return
L12:
astore 0
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Error LP: Error classes.dex decompress! "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception e1"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
return
.limit locals 7
.limit stack 5
.end method

.method public static zipLib(Ljava/util/ArrayList;)V
.signature "(Ljava/util/ArrayList<Ljava/lang/String;>;)V"
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
.catch java/lang/Exception from L4 to L5 using L2
L0:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 1
aload 0
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 0
L1:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 1
new com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem
dup
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/tmp/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L3:
goto L1
L2:
astore 0
getstatic com/chelpus/root/utils/createapkcustom/print Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Error LP: Error libs compress! "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L6:
return
L4:
getstatic com/chelpus/root/utils/createapkcustom/crkapk Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/crkapk Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "checlpis.zip"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 1
invokestatic com/chelpus/Utils/addFilesToZip(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
getstatic com/chelpus/root/utils/createapkcustom/crkapk Ljava/io/File;
invokevirtual java/io/File/delete()Z
pop
getstatic com/chelpus/root/utils/createapkcustom/crkapk Ljava/io/File;
invokevirtual java/io/File/exists()Z
ifne L6
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/createapkcustom/crkapk Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "checlpis.zip"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
getstatic com/chelpus/root/utils/createapkcustom/crkapk Ljava/io/File;
invokevirtual java/io/File/renameTo(Ljava/io/File;)Z
pop
L5:
return
.limit locals 2
.limit stack 6
.end method
