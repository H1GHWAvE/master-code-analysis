.bytecode 50.0
.class public synchronized com/chelpus/root/utils/runpatchsupport
.super java/lang/Object
.inner class static final inner com/chelpus/root/utils/runpatchsupport$1

.field private static 'ART' Z

.field public static 'appdir' Ljava/lang/String;

.field public static 'classesFiles' Ljava/util/ArrayList; signature "Ljava/util/ArrayList<Ljava/io/File;>;"

.field private static 'copyDC' Z

.field private static 'createAPK' Z

.field public static 'crkapk' Ljava/io/File;

.field public static 'dir' Ljava/lang/String;

.field public static 'dir2' Ljava/lang/String;

.field public static 'dirapp' Ljava/lang/String;

.field public static 'filestopatch' Ljava/util/ArrayList; signature "Ljava/util/ArrayList<Ljava/io/File;>;"

.field private static 'pattern1' Z

.field private static 'pattern2' Z

.field private static 'pattern3' Z

.field public static 'print' Ljava/io/PrintStream;

.field public static 'result' Ljava/lang/String;

.field public static 'sddir' Ljava/lang/String;

.field public static 'system' Z

.field public static 'uid' Ljava/lang/String;

.method static <clinit>()V
iconst_0
putstatic com/chelpus/root/utils/runpatchsupport/createAPK Z
iconst_1
putstatic com/chelpus/root/utils/runpatchsupport/pattern1 Z
iconst_1
putstatic com/chelpus/root/utils/runpatchsupport/pattern2 Z
iconst_1
putstatic com/chelpus/root/utils/runpatchsupport/pattern3 Z
iconst_0
putstatic com/chelpus/root/utils/runpatchsupport/copyDC Z
iconst_0
putstatic com/chelpus/root/utils/runpatchsupport/ART Z
ldc "/data/app/"
putstatic com/chelpus/root/utils/runpatchsupport/dirapp Ljava/lang/String;
iconst_0
putstatic com/chelpus/root/utils/runpatchsupport/system Z
ldc ""
putstatic com/chelpus/root/utils/runpatchsupport/uid Ljava/lang/String;
ldc "/sdcard/"
putstatic com/chelpus/root/utils/runpatchsupport/dir Ljava/lang/String;
ldc "/sdcard/"
putstatic com/chelpus/root/utils/runpatchsupport/dir2 Ljava/lang/String;
aconst_null
putstatic com/chelpus/root/utils/runpatchsupport/filestopatch Ljava/util/ArrayList;
ldc "/sdcard/"
putstatic com/chelpus/root/utils/runpatchsupport/sddir Ljava/lang/String;
ldc "/sdcard/"
putstatic com/chelpus/root/utils/runpatchsupport/appdir Ljava/lang/String;
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putstatic com/chelpus/root/utils/runpatchsupport/classesFiles Ljava/util/ArrayList;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static byteverify(Ljava/nio/MappedByteBuffer;IB[B[B[B[BLjava/lang/String;Z)Z
iload 2
aload 3
iconst_0
baload
if_icmpne L0
iload 8
ifeq L0
aload 6
iconst_0
baload
ifne L1
aload 5
iconst_0
iload 2
bastore
L1:
iconst_1
istore 9
aload 0
iload 1
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 0
invokevirtual java/nio/MappedByteBuffer/get()B
istore 2
L2:
iload 2
aload 3
iload 9
baload
if_icmpeq L3
aload 4
iload 9
baload
iconst_1
if_icmpne L4
L3:
aload 6
iload 9
baload
ifne L5
aload 5
iload 9
iload 2
bastore
L5:
iload 9
iconst_1
iadd
istore 9
iload 9
aload 3
arraylength
if_icmpne L6
aload 0
iload 1
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 0
aload 5
invokevirtual java/nio/MappedByteBuffer/put([B)Ljava/nio/ByteBuffer;
pop
aload 0
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 7
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
iconst_1
ireturn
L6:
aload 0
invokevirtual java/nio/MappedByteBuffer/get()B
istore 2
goto L2
L4:
aload 0
iload 1
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L0:
iconst_0
ireturn
.limit locals 10
.limit stack 3
.end method

.method private static final calcChecksum([BI)V
new java/util/zip/Adler32
dup
invokespecial java/util/zip/Adler32/<init>()V
astore 3
aload 3
aload 0
bipush 12
aload 0
arraylength
iload 1
bipush 12
iadd
isub
invokevirtual java/util/zip/Adler32/update([BII)V
aload 3
invokevirtual java/util/zip/Adler32/getValue()J
l2i
istore 2
aload 0
iload 1
bipush 8
iadd
iload 2
i2b
bastore
aload 0
iload 1
bipush 9
iadd
iload 2
bipush 8
ishr
i2b
bastore
aload 0
iload 1
bipush 10
iadd
iload 2
bipush 16
ishr
i2b
bastore
aload 0
iload 1
bipush 11
iadd
iload 2
bipush 24
ishr
i2b
bastore
return
.limit locals 4
.limit stack 6
.end method

.method private static final calcSignature([BI)V
.catch java/security/NoSuchAlgorithmException from L0 to L1 using L2
.catch java/security/DigestException from L3 to L4 using L5
.catch java/security/DigestException from L6 to L5 using L5
L0:
ldc "SHA-1"
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
astore 2
L1:
aload 2
aload 0
bipush 32
aload 0
arraylength
iload 1
bipush 32
iadd
isub
invokevirtual java/security/MessageDigest/update([BII)V
L3:
aload 2
aload 0
iload 1
bipush 12
iadd
bipush 20
invokevirtual java/security/MessageDigest/digest([BII)I
istore 1
L4:
iload 1
bipush 20
if_icmpeq L7
L6:
new java/lang/RuntimeException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "unexpected digest write:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc "bytes"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L5:
astore 0
new java/lang/RuntimeException
dup
aload 0
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
L2:
astore 0
new java/lang/RuntimeException
dup
aload 0
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
L7:
return
.limit locals 3
.limit stack 6
.end method

.method public static clearTemp()V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L2
.catch java/lang/Exception from L6 to L7 using L2
L0:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchsupport/dir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/AndroidManifest.xml"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
aload 0
invokevirtual java/io/File/exists()Z
ifeq L1
aload 0
invokevirtual java/io/File/delete()Z
pop
L1:
getstatic com/chelpus/root/utils/runpatchsupport/classesFiles Ljava/util/ArrayList;
ifnull L5
getstatic com/chelpus/root/utils/runpatchsupport/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifle L5
getstatic com/chelpus/root/utils/runpatchsupport/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 0
L3:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L5
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
astore 1
aload 1
invokevirtual java/io/File/exists()Z
ifeq L3
aload 1
invokevirtual java/io/File/delete()Z
pop
L4:
goto L3
L2:
astore 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
L8:
return
L5:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchsupport/dir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/classes.dex"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
aload 0
invokevirtual java/io/File/exists()Z
ifeq L6
aload 0
invokevirtual java/io/File/delete()Z
pop
L6:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchsupport/dir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/classes.dex.apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
aload 0
invokevirtual java/io/File/exists()Z
ifeq L8
aload 0
invokevirtual java/io/File/delete()Z
pop
L7:
return
.limit locals 2
.limit stack 4
.end method

.method public static clearTempSD()V
.catch java/lang/Exception from L0 to L1 using L2
L0:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchsupport/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/classes.dex.apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
aload 0
invokevirtual java/io/File/exists()Z
ifeq L1
aload 0
invokevirtual java/io/File/delete()Z
pop
L1:
return
L2:
astore 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
return
.limit locals 1
.limit stack 4
.end method

.method public static fixadler(Ljava/io/File;)V
.catch java/lang/Exception from L0 to L1 using L2
L0:
new java/io/FileInputStream
dup
aload 0
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
astore 2
aload 2
invokevirtual java/io/FileInputStream/available()I
newarray byte
astore 1
aload 2
aload 1
invokevirtual java/io/FileInputStream/read([B)I
pop
aload 1
iconst_0
invokestatic com/chelpus/root/utils/runpatchsupport/calcSignature([BI)V
aload 1
iconst_0
invokestatic com/chelpus/root/utils/runpatchsupport/calcChecksum([BI)V
aload 2
invokevirtual java/io/FileInputStream/close()V
new java/io/FileOutputStream
dup
aload 0
invokespecial java/io/FileOutputStream/<init>(Ljava/io/File;)V
astore 0
aload 0
aload 1
invokevirtual java/io/FileOutputStream/write([B)V
aload 0
invokevirtual java/io/FileOutputStream/close()V
L1:
return
L2:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
return
.limit locals 3
.limit stack 3
.end method

.method public static main([Ljava/lang/String;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/NullPointerException from L5 to L6 using L7
.catch java/lang/Exception from L5 to L6 using L8
.catch java/lang/NullPointerException from L6 to L9 using L7
.catch java/lang/Exception from L6 to L9 using L8
.catch java/lang/NullPointerException from L9 to L10 using L7
.catch java/lang/Exception from L9 to L10 using L8
.catch java/lang/NullPointerException from L11 to L12 using L7
.catch java/lang/Exception from L11 to L12 using L8
.catch java/lang/NullPointerException from L13 to L14 using L7
.catch java/lang/Exception from L13 to L14 using L8
.catch java/lang/NullPointerException from L15 to L16 using L7
.catch java/lang/Exception from L15 to L16 using L8
.catch java/lang/NullPointerException from L17 to L18 using L7
.catch java/lang/Exception from L17 to L18 using L8
.catch java/lang/NullPointerException from L18 to L19 using L7
.catch java/lang/Exception from L18 to L19 using L8
.catch java/lang/NullPointerException from L19 to L20 using L21
.catch java/lang/Exception from L19 to L20 using L22
.catch java/io/FileNotFoundException from L23 to L24 using L25
.catch java/lang/Exception from L23 to L24 using L26
.catch java/io/FileNotFoundException from L24 to L27 using L25
.catch java/lang/Exception from L24 to L27 using L26
.catch java/io/FileNotFoundException from L27 to L28 using L25
.catch java/lang/Exception from L27 to L28 using L26
.catch java/io/FileNotFoundException from L28 to L29 using L25
.catch java/lang/Exception from L28 to L29 using L26
.catch java/io/FileNotFoundException from L30 to L31 using L25
.catch java/lang/Exception from L30 to L31 using L26
.catch java/io/FileNotFoundException from L31 to L25 using L25
.catch java/lang/Exception from L31 to L25 using L26
.catch java/io/FileNotFoundException from L32 to L33 using L25
.catch java/lang/Exception from L32 to L33 using L26
.catch java/io/FileNotFoundException from L33 to L26 using L25
.catch java/lang/Exception from L33 to L26 using L26
.catch java/io/FileNotFoundException from L34 to L35 using L25
.catch java/lang/Exception from L34 to L35 using L26
.catch java/io/FileNotFoundException from L36 to L37 using L25
.catch java/lang/Exception from L36 to L37 using L26
.catch java/io/FileNotFoundException from L37 to L38 using L25
.catch java/lang/Exception from L37 to L38 using L26
.catch java/io/FileNotFoundException from L38 to L39 using L25
.catch java/lang/Exception from L38 to L39 using L26
.catch java/io/FileNotFoundException from L39 to L40 using L25
.catch java/lang/Exception from L39 to L40 using L26
.catch java/io/FileNotFoundException from L40 to L41 using L25
.catch java/lang/Exception from L40 to L41 using L26
.catch java/io/FileNotFoundException from L42 to L43 using L25
.catch java/lang/Exception from L42 to L43 using L26
.catch java/io/FileNotFoundException from L43 to L44 using L25
.catch java/lang/Exception from L43 to L44 using L26
.catch java/io/FileNotFoundException from L44 to L45 using L25
.catch java/lang/Exception from L44 to L45 using L26
.catch java/io/FileNotFoundException from L46 to L47 using L25
.catch java/lang/Exception from L46 to L47 using L26
.catch java/io/FileNotFoundException from L48 to L49 using L25
.catch java/lang/Exception from L48 to L49 using L26
.catch java/lang/Exception from L49 to L50 using L51
.catch java/io/FileNotFoundException from L49 to L50 using L25
.catch java/io/FileNotFoundException from L50 to L52 using L25
.catch java/lang/Exception from L50 to L52 using L26
.catch java/io/FileNotFoundException from L52 to L51 using L25
.catch java/lang/Exception from L52 to L51 using L26
.catch java/io/FileNotFoundException from L53 to L54 using L25
.catch java/lang/Exception from L53 to L54 using L26
.catch java/io/FileNotFoundException from L55 to L56 using L25
.catch java/lang/Exception from L55 to L56 using L26
.catch java/io/FileNotFoundException from L56 to L57 using L25
.catch java/lang/Exception from L56 to L57 using L26
.catch java/io/FileNotFoundException from L57 to L58 using L25
.catch java/lang/Exception from L57 to L58 using L26
.catch java/io/FileNotFoundException from L59 to L60 using L25
.catch java/lang/Exception from L59 to L60 using L26
.catch java/io/FileNotFoundException from L61 to L62 using L25
.catch java/lang/Exception from L61 to L62 using L26
.catch java/io/FileNotFoundException from L62 to L63 using L25
.catch java/lang/Exception from L62 to L63 using L26
.catch java/io/FileNotFoundException from L63 to L64 using L25
.catch java/lang/Exception from L63 to L64 using L26
.catch java/io/FileNotFoundException from L64 to L65 using L25
.catch java/lang/Exception from L64 to L65 using L26
.catch java/io/FileNotFoundException from L65 to L66 using L25
.catch java/lang/Exception from L65 to L66 using L26
.catch java/io/FileNotFoundException from L67 to L68 using L25
.catch java/lang/Exception from L67 to L68 using L26
.catch java/io/FileNotFoundException from L69 to L70 using L25
.catch java/lang/Exception from L69 to L70 using L26
.catch java/io/FileNotFoundException from L70 to L71 using L25
.catch java/lang/Exception from L70 to L71 using L26
.catch java/io/FileNotFoundException from L71 to L72 using L25
.catch java/lang/Exception from L71 to L72 using L26
.catch java/io/FileNotFoundException from L73 to L74 using L25
.catch java/lang/Exception from L73 to L74 using L26
.catch java/io/FileNotFoundException from L75 to L76 using L25
.catch java/lang/Exception from L75 to L76 using L26
.catch java/io/FileNotFoundException from L77 to L78 using L25
.catch java/lang/Exception from L77 to L78 using L26
.catch java/io/FileNotFoundException from L79 to L80 using L25
.catch java/lang/Exception from L79 to L80 using L26
.catch java/io/FileNotFoundException from L81 to L82 using L25
.catch java/lang/Exception from L81 to L82 using L26
.catch java/io/FileNotFoundException from L83 to L84 using L25
.catch java/lang/Exception from L83 to L84 using L26
.catch java/io/FileNotFoundException from L85 to L86 using L25
.catch java/lang/Exception from L85 to L86 using L26
.catch java/io/FileNotFoundException from L87 to L88 using L25
.catch java/lang/Exception from L87 to L88 using L26
.catch java/io/FileNotFoundException from L88 to L89 using L25
.catch java/lang/Exception from L88 to L89 using L26
.catch java/io/FileNotFoundException from L89 to L90 using L25
.catch java/lang/Exception from L89 to L90 using L26
.catch java/io/FileNotFoundException from L90 to L91 using L25
.catch java/lang/Exception from L90 to L91 using L26
.catch java/io/FileNotFoundException from L92 to L93 using L25
.catch java/lang/Exception from L92 to L93 using L26
.catch java/io/FileNotFoundException from L93 to L94 using L25
.catch java/lang/Exception from L93 to L94 using L26
.catch java/io/FileNotFoundException from L95 to L96 using L25
.catch java/lang/Exception from L95 to L96 using L26
.catch java/io/FileNotFoundException from L97 to L98 using L25
.catch java/lang/Exception from L97 to L98 using L26
.catch java/io/FileNotFoundException from L98 to L99 using L25
.catch java/lang/Exception from L98 to L99 using L26
.catch java/io/FileNotFoundException from L100 to L101 using L25
.catch java/lang/Exception from L100 to L101 using L26
.catch java/io/FileNotFoundException from L102 to L103 using L25
.catch java/lang/Exception from L102 to L103 using L26
.catch java/lang/Exception from L104 to L105 using L106
.catch java/io/FileNotFoundException from L104 to L105 using L25
.catch java/lang/Exception from L107 to L108 using L106
.catch java/io/FileNotFoundException from L107 to L108 using L25
.catch java/lang/Exception from L109 to L110 using L106
.catch java/io/FileNotFoundException from L109 to L110 using L25
.catch java/lang/Exception from L110 to L111 using L106
.catch java/io/FileNotFoundException from L110 to L111 using L25
.catch java/lang/Exception from L112 to L113 using L106
.catch java/io/FileNotFoundException from L112 to L113 using L25
.catch java/lang/Exception from L114 to L115 using L106
.catch java/io/FileNotFoundException from L114 to L115 using L25
.catch java/lang/Exception from L116 to L117 using L106
.catch java/io/FileNotFoundException from L116 to L117 using L25
.catch java/lang/Exception from L118 to L119 using L106
.catch java/io/FileNotFoundException from L118 to L119 using L25
.catch java/lang/Exception from L120 to L121 using L106
.catch java/io/FileNotFoundException from L120 to L121 using L25
.catch java/lang/Exception from L121 to L122 using L106
.catch java/io/FileNotFoundException from L121 to L122 using L25
.catch java/lang/Exception from L123 to L124 using L106
.catch java/io/FileNotFoundException from L123 to L124 using L25
.catch java/lang/Exception from L124 to L125 using L106
.catch java/io/FileNotFoundException from L124 to L125 using L25
.catch java/lang/Exception from L125 to L126 using L106
.catch java/io/FileNotFoundException from L125 to L126 using L25
.catch java/lang/Exception from L126 to L127 using L106
.catch java/io/FileNotFoundException from L126 to L127 using L25
.catch java/lang/Exception from L128 to L129 using L106
.catch java/io/FileNotFoundException from L128 to L129 using L25
.catch java/lang/Exception from L129 to L130 using L106
.catch java/io/FileNotFoundException from L129 to L130 using L25
.catch java/io/FileNotFoundException from L131 to L132 using L25
.catch java/lang/Exception from L131 to L132 using L26
.catch java/io/FileNotFoundException from L132 to L133 using L25
.catch java/lang/Exception from L132 to L133 using L26
.catch java/lang/Exception from L134 to L135 using L106
.catch java/io/FileNotFoundException from L134 to L135 using L25
.catch java/lang/Exception from L135 to L136 using L106
.catch java/io/FileNotFoundException from L135 to L136 using L25
.catch java/lang/Exception from L137 to L138 using L106
.catch java/io/FileNotFoundException from L137 to L138 using L25
.catch java/lang/Exception from L139 to L140 using L106
.catch java/io/FileNotFoundException from L139 to L140 using L25
.catch java/lang/Exception from L141 to L142 using L106
.catch java/io/FileNotFoundException from L141 to L142 using L25
.catch java/lang/Exception from L143 to L144 using L106
.catch java/io/FileNotFoundException from L143 to L144 using L25
.catch java/lang/Exception from L144 to L145 using L106
.catch java/io/FileNotFoundException from L144 to L145 using L25
.catch java/lang/Exception from L146 to L147 using L106
.catch java/io/FileNotFoundException from L146 to L147 using L25
.catch java/lang/Exception from L147 to L148 using L106
.catch java/io/FileNotFoundException from L147 to L148 using L25
.catch java/lang/Exception from L148 to L149 using L106
.catch java/io/FileNotFoundException from L148 to L149 using L25
.catch java/lang/Exception from L149 to L150 using L106
.catch java/io/FileNotFoundException from L149 to L150 using L25
.catch java/lang/Exception from L151 to L152 using L106
.catch java/io/FileNotFoundException from L151 to L152 using L25
.catch java/lang/Exception from L152 to L153 using L106
.catch java/io/FileNotFoundException from L152 to L153 using L25
.catch java/lang/Exception from L154 to L155 using L106
.catch java/io/FileNotFoundException from L154 to L155 using L25
.catch java/lang/Exception from L156 to L157 using L106
.catch java/io/FileNotFoundException from L156 to L157 using L25
.catch java/lang/Exception from L157 to L158 using L106
.catch java/io/FileNotFoundException from L157 to L158 using L25
.catch java/lang/Exception from L159 to L160 using L106
.catch java/io/FileNotFoundException from L159 to L160 using L25
.catch java/lang/Exception from L161 to L162 using L106
.catch java/io/FileNotFoundException from L161 to L162 using L25
.catch java/lang/Exception from L162 to L163 using L106
.catch java/io/FileNotFoundException from L162 to L163 using L25
.catch java/lang/Exception from L164 to L165 using L106
.catch java/io/FileNotFoundException from L164 to L165 using L25
.catch java/lang/Exception from L166 to L167 using L106
.catch java/io/FileNotFoundException from L166 to L167 using L25
.catch java/lang/Exception from L168 to L169 using L106
.catch java/io/FileNotFoundException from L168 to L169 using L25
.catch java/lang/Exception from L169 to L170 using L106
.catch java/io/FileNotFoundException from L169 to L170 using L25
.catch java/lang/Exception from L171 to L172 using L106
.catch java/io/FileNotFoundException from L171 to L172 using L25
.catch java/lang/Exception from L172 to L173 using L106
.catch java/io/FileNotFoundException from L172 to L173 using L25
.catch java/lang/Exception from L173 to L174 using L106
.catch java/io/FileNotFoundException from L173 to L174 using L25
.catch java/lang/Exception from L174 to L175 using L106
.catch java/io/FileNotFoundException from L174 to L175 using L25
.catch java/lang/Exception from L176 to L177 using L106
.catch java/io/FileNotFoundException from L176 to L177 using L25
.catch java/lang/Exception from L177 to L178 using L106
.catch java/io/FileNotFoundException from L177 to L178 using L25
.catch java/lang/Exception from L179 to L180 using L106
.catch java/io/FileNotFoundException from L179 to L180 using L25
.catch java/lang/Exception from L181 to L182 using L106
.catch java/io/FileNotFoundException from L181 to L182 using L25
.catch java/lang/Exception from L182 to L183 using L106
.catch java/io/FileNotFoundException from L182 to L183 using L25
.catch java/lang/Exception from L184 to L185 using L106
.catch java/io/FileNotFoundException from L184 to L185 using L25
.catch java/lang/Exception from L186 to L187 using L106
.catch java/io/FileNotFoundException from L186 to L187 using L25
.catch java/lang/Exception from L187 to L188 using L106
.catch java/io/FileNotFoundException from L187 to L188 using L25
.catch java/lang/Exception from L188 to L189 using L106
.catch java/io/FileNotFoundException from L188 to L189 using L25
.catch java/lang/Exception from L190 to L191 using L106
.catch java/io/FileNotFoundException from L190 to L191 using L25
.catch java/lang/Exception from L191 to L192 using L106
.catch java/io/FileNotFoundException from L191 to L192 using L25
.catch java/lang/Exception from L192 to L193 using L106
.catch java/io/FileNotFoundException from L192 to L193 using L25
.catch java/lang/Exception from L193 to L194 using L106
.catch java/io/FileNotFoundException from L193 to L194 using L25
.catch java/lang/Exception from L194 to L195 using L106
.catch java/io/FileNotFoundException from L194 to L195 using L25
.catch java/lang/Exception from L196 to L197 using L106
.catch java/io/FileNotFoundException from L196 to L197 using L25
.catch java/lang/Exception from L197 to L198 using L106
.catch java/io/FileNotFoundException from L197 to L198 using L25
.catch java/lang/Exception from L199 to L200 using L106
.catch java/io/FileNotFoundException from L199 to L200 using L25
.catch java/lang/Exception from L201 to L202 using L106
.catch java/io/FileNotFoundException from L201 to L202 using L25
.catch java/lang/Exception from L202 to L203 using L106
.catch java/io/FileNotFoundException from L202 to L203 using L25
.catch java/lang/Exception from L204 to L205 using L106
.catch java/io/FileNotFoundException from L204 to L205 using L25
.catch java/lang/Exception from L206 to L207 using L106
.catch java/io/FileNotFoundException from L206 to L207 using L25
.catch java/lang/Exception from L208 to L209 using L106
.catch java/io/FileNotFoundException from L208 to L209 using L25
.catch java/lang/Exception from L210 to L211 using L106
.catch java/io/FileNotFoundException from L210 to L211 using L25
new com/android/vending/billing/InAppBillingService/LUCK/LogOutputStream
dup
ldc "System.out"
invokespecial com/android/vending/billing/InAppBillingService/LUCK/LogOutputStream/<init>(Ljava/lang/String;)V
astore 17
new java/io/PrintStream
dup
aload 17
invokespecial java/io/PrintStream/<init>(Ljava/io/OutputStream;)V
putstatic com/chelpus/root/utils/runpatchsupport/print Ljava/io/PrintStream;
new com/chelpus/root/utils/runpatchsupport$1
dup
invokespecial com/chelpus/root/utils/runpatchsupport$1/<init>()V
invokestatic com/chelpus/Utils/startRootJava(Ljava/lang/Object;)V
aload 0
iconst_0
aaload
invokestatic com/chelpus/Utils/kill(Ljava/lang/String;)V
getstatic com/chelpus/root/utils/runpatchsupport/print Ljava/io/PrintStream;
ldc "Support-Code Running!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 18
iconst_1
putstatic com/chelpus/root/utils/runpatchsupport/pattern1 Z
iconst_1
putstatic com/chelpus/root/utils/runpatchsupport/pattern2 Z
iconst_1
putstatic com/chelpus/root/utils/runpatchsupport/pattern3 Z
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putstatic com/chelpus/root/utils/runpatchsupport/filestopatch Ljava/util/ArrayList;
L0:
new java/io/File
dup
aload 0
iconst_3
aaload
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/listFiles()[Ljava/io/File;
astore 16
aload 16
arraylength
istore 4
L1:
iconst_0
istore 3
L212:
iload 3
iload 4
if_icmpge L5
aload 16
iload 3
aaload
astore 19
L3:
aload 19
invokevirtual java/io/File/isFile()Z
ifeq L4
aload 19
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc "busybox"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L4
aload 19
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc "reboot"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L4
aload 19
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc "dalvikvm"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L4
aload 19
invokevirtual java/io/File/delete()Z
pop
L4:
iload 3
iconst_1
iadd
istore 3
goto L212
L2:
astore 16
aload 16
invokevirtual java/lang/Exception/printStackTrace()V
L5:
aload 0
iconst_1
aaload
ldc "pattern0"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L6
iconst_0
putstatic com/chelpus/root/utils/runpatchsupport/pattern1 Z
L6:
aload 0
iconst_1
aaload
ldc "pattern1"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L9
iconst_0
putstatic com/chelpus/root/utils/runpatchsupport/pattern2 Z
L9:
aload 0
iconst_1
aaload
ldc "pattern2"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L10
iconst_0
putstatic com/chelpus/root/utils/runpatchsupport/pattern3 Z
L10:
aload 0
bipush 6
aaload
ifnull L12
L11:
aload 0
bipush 6
aaload
ldc "createAPK"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L12
iconst_1
putstatic com/chelpus/root/utils/runpatchsupport/createAPK Z
L12:
aload 0
bipush 6
aaload
ifnull L14
L13:
aload 0
bipush 6
aaload
ldc "ART"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L14
iconst_1
putstatic com/chelpus/root/utils/runpatchsupport/ART Z
L14:
aload 0
bipush 6
aaload
ifnull L16
L15:
aload 0
bipush 6
aaload
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
L16:
aload 0
bipush 7
aaload
ifnull L18
L17:
aload 0
bipush 7
aaload
putstatic com/chelpus/root/utils/runpatchsupport/uid Ljava/lang/String;
L18:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "uid:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/runpatchsupport/uid Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L19:
aload 0
iconst_5
aaload
ldc "copyDC"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L20
iconst_1
putstatic com/chelpus/root/utils/runpatchsupport/copyDC Z
L20:
getstatic com/chelpus/root/utils/runpatchsupport/createAPK Z
ifeq L213
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
L213:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 16
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 19
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 20
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 21
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 22
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 23
aload 16
ldc "1A ?? FF FF"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
ldc "1A ?? ?? ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 20
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 21
ldc "(pak intekekt 0)"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 22
ldc "search_pack"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 23
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
ldc "1B ?? FF FF FF FF"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
ldc "1B ?? ?? ?? ?? ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 20
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 21
ldc "(pak intekekt 0)"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 22
ldc "search_pack"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 23
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
ldc "1A ?? FF FF"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
ldc "1A ?? ?? ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 20
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 21
ldc "(sha intekekt 2)"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 22
ldc "search"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 23
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
ldc "1B ?? FF FF FF FF"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
ldc "1B ?? ?? ?? ?? ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 20
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 21
ldc "(sha intekekt 2 32 bit)"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 22
ldc "search"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 23
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
ldc "0A ?? 39 ?? ?? 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
ldc "12 S1 39 ?? ?? 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 20
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 21
ldc "support2 Fixed!\n(sha intekekt 3)"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 22
ldc "search"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 23
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
ldc "1A ?? FF FF"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
ldc "1A ?? FF FF"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 20
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 21
ldc "support1 Fixed!\n(pak intekekt)"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 22
ldc "search_pack"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 23
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
ldc "1B ?? FF FF FF FF"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
ldc "1B ?? FF FF FF FF"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 20
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 21
ldc "support1 Fixed!\n(pak intekekt 32 bit)"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 22
ldc "search_pack"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 23
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
ldc "6E 20 FF FF ?? 00 0A ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
ldc "6E 20 ?? ?? ?? 00 12 S1"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 20
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 21
ldc "support2 Fixed!\n(sha intekekt 4)"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 22
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 23
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
ldc "6E 20 FF FF ?? 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
ldc "00 00 00 00 00 00"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 20
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 21
ldc "support3 Fixed!\n(intent for free)"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 22
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 23
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
ldc "0A ?? 39 ?? ?? ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
ldc "12 S1 39 ?? ?? ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 20
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 21
ldc "support1 Fixed!\n(ev1)"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 22
ldc "search_sign_ver"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 23
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
ldc "0A ?? 38 ?? ?? ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
ldc "12 S1 38 ?? ?? ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 20
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 21
ldc "support1 Fixed!\n(ev1)"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 22
ldc "search_sign_ver"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 23
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
ldc "1C ?? FF FF"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
ldc "1C ?? ?? ??"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 20
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 21
ldc "support1 Fixed!\n(si)"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 22
ldc "search_sign_ver"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 23
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
ldc "23 ?? ?? ?? 1C ?? ?? ?? 12 ?? 4D ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 39 ?? ?? ?? 1A ?? ?? ?? 1A"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 19
ldc "23 ?? ?? ?? 1C ?? ?? ?? 12 ?? 4D ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 12 S1 39 ?? ?? ?? 1A ?? ?? ?? 1A"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 20
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 21
ldc "support3 Fixed!\n(s)"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 22
ldc ""
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 23
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 18
aload 16
aload 19
aload 20
aload 21
aload 22
aload 23
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokestatic com/chelpus/Utils/convertToPatchItemAuto(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/Boolean;)V
L23:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L24
aload 0
iconst_2
aaload
ldc "RW"
invokestatic com/chelpus/Utils/remount(Ljava/lang/String;Ljava/lang/String;)Z
pop
L24:
getstatic com/chelpus/root/utils/runpatchsupport/createAPK Z
ifne L46
getstatic com/chelpus/root/utils/runpatchsupport/ART Z
ifne L46
aload 0
iconst_3
aaload
putstatic com/chelpus/root/utils/runpatchsupport/dir Ljava/lang/String;
aload 0
iconst_2
aaload
putstatic com/chelpus/root/utils/runpatchsupport/dirapp Ljava/lang/String;
invokestatic com/chelpus/root/utils/runpatchsupport/clearTemp()V
aload 0
iconst_4
aaload
ldc "not_system"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L27
iconst_0
putstatic com/chelpus/root/utils/runpatchsupport/system Z
L27:
aload 0
iconst_4
aaload
ldc "system"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L28
iconst_1
putstatic com/chelpus/root/utils/runpatchsupport/system Z
L28:
getstatic com/chelpus/root/utils/runpatchsupport/filestopatch Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
ldc "CLASSES mode create odex enabled."
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
L29:
aload 0
iconst_0
aaload
astore 16
L30:
aload 0
iconst_2
aaload
putstatic com/chelpus/root/utils/runpatchsupport/appdir Ljava/lang/String;
aload 0
iconst_3
aaload
putstatic com/chelpus/root/utils/runpatchsupport/sddir Ljava/lang/String;
invokestatic com/chelpus/root/utils/runpatchsupport/clearTempSD()V
new java/io/File
dup
getstatic com/chelpus/root/utils/runpatchsupport/appdir Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 16
ldc "Get classes.dex."
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
getstatic com/chelpus/root/utils/runpatchsupport/print Ljava/io/PrintStream;
ldc "Get classes.dex."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 16
invokestatic com/chelpus/root/utils/runpatchsupport/unzipART(Ljava/io/File;)V
getstatic com/chelpus/root/utils/runpatchsupport/classesFiles Ljava/util/ArrayList;
ifnull L31
getstatic com/chelpus/root/utils/runpatchsupport/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifne L32
L31:
new java/io/FileNotFoundException
dup
invokespecial java/io/FileNotFoundException/<init>()V
athrow
L25:
astore 16
ldc "Error: Program files are not found!\n\nCheck the location dalvik-cache to solve problems!\n\nDefault: /data/dalvik-cache/*"
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
L214:
getstatic com/chelpus/root/utils/runpatchsupport/filestopatch Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 16
L215:
aload 16
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L216
aload 16
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
invokestatic com/chelpus/Utils/fixadler(Ljava/io/File;)V
invokestatic com/chelpus/root/utils/runpatchsupport/clearTempSD()V
goto L215
L32:
getstatic com/chelpus/root/utils/runpatchsupport/filestopatch Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
getstatic com/chelpus/root/utils/runpatchsupport/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 16
L33:
aload 16
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L36
aload 16
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
astore 19
aload 19
invokevirtual java/io/File/exists()Z
ifne L34
new java/io/FileNotFoundException
dup
invokespecial java/io/FileNotFoundException/<init>()V
athrow
L26:
astore 16
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Patch Process Exception: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 16
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
goto L214
L34:
getstatic com/chelpus/root/utils/runpatchsupport/filestopatch Ljava/util/ArrayList;
aload 19
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L35:
goto L33
L36:
aload 0
iconst_2
aaload
iconst_1
invokestatic com/chelpus/Utils/getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;
astore 16
new java/io/File
dup
aload 16
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 19
aload 19
invokevirtual java/io/File/exists()Z
ifeq L37
aload 19
invokevirtual java/io/File/delete()Z
pop
L37:
new java/io/File
dup
aload 16
ldc "-1"
ldc "-2"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 19
aload 19
invokevirtual java/io/File/exists()Z
ifeq L38
aload 19
invokevirtual java/io/File/delete()Z
pop
L38:
new java/io/File
dup
aload 16
ldc "-2"
ldc "-1"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 16
aload 16
invokevirtual java/io/File/exists()Z
ifeq L39
aload 16
invokevirtual java/io/File/delete()Z
pop
L39:
getstatic com/chelpus/root/utils/runpatchsupport/filestopatch Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 19
L40:
aload 19
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L214
aload 19
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
astore 20
ldc "Find string id."
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 16
aload 16
ldc "com.android.vending"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
ldc "SHA1withRSA"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
ldc "com.android.vending.billing.InAppBillingService.BIND"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
ldc "Ljava/security/Signature;"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
ldc "verify"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
ldc "Landroid/content/Intent;"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
ldc "setPackage"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
ldc "engineVerify"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
ldc "Ljava/security/SignatureSpi;"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
ldc "String analysis."
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
getstatic com/chelpus/root/utils/runpatchsupport/print Ljava/io/PrintStream;
ldc "String analysis."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic com/chelpus/root/utils/runpatchsupport/copyDC Z
ifne L67
aload 20
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aload 16
iconst_0
invokestatic com/chelpus/Utils/getStringIds(Ljava/lang/String;Ljava/util/ArrayList;Z)Ljava/util/ArrayList;
astore 16
L41:
iconst_0
istore 9
iconst_0
istore 6
iconst_0
istore 4
iconst_0
istore 5
iconst_0
istore 3
L42:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 21
aload 21
new com/android/vending/billing/InAppBillingService/LUCK/TypesItem
dup
ldc "Ljava/security/Signature;"
invokespecial com/android/vending/billing/InAppBillingService/LUCK/TypesItem/<init>(Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 21
new com/android/vending/billing/InAppBillingService/LUCK/TypesItem
dup
ldc "Ljava/security/SignatureSpi;"
invokespecial com/android/vending/billing/InAppBillingService/LUCK/TypesItem/<init>(Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 22
aload 22
new com/android/vending/billing/InAppBillingService/LUCK/CommandItem
dup
ldc "Ljava/security/Signature;"
ldc "verify"
invokespecial com/android/vending/billing/InAppBillingService/LUCK/CommandItem/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 22
new com/android/vending/billing/InAppBillingService/LUCK/CommandItem
dup
ldc "Landroid/content/Intent;"
ldc "setPackage"
invokespecial com/android/vending/billing/InAppBillingService/LUCK/CommandItem/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 16
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 16
L43:
aload 16
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L217
aload 16
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/StringItem
astore 23
aload 21
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 24
L44:
aload 24
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L69
aload 24
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/TypesItem
astore 25
aload 25
getfield com/android/vending/billing/InAppBillingService/LUCK/TypesItem/type Ljava/lang/String;
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/str Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L44
aload 25
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
putfield com/android/vending/billing/InAppBillingService/LUCK/TypesItem/Type [B
L45:
goto L44
L46:
getstatic com/chelpus/root/utils/runpatchsupport/createAPK Z
ifeq L59
L47:
aload 0
iconst_0
aaload
astore 16
L48:
aload 0
iconst_2
aaload
putstatic com/chelpus/root/utils/runpatchsupport/appdir Ljava/lang/String;
aload 0
iconst_5
aaload
putstatic com/chelpus/root/utils/runpatchsupport/sddir Ljava/lang/String;
invokestatic com/chelpus/root/utils/runpatchsupport/clearTempSD()V
new java/io/File
dup
getstatic com/chelpus/root/utils/runpatchsupport/appdir Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 19
aload 19
invokestatic com/chelpus/root/utils/runpatchsupport/unzipSD(Ljava/io/File;)V
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchsupport/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 16
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
putstatic com/chelpus/root/utils/runpatchsupport/crkapk Ljava/io/File;
aload 19
getstatic com/chelpus/root/utils/runpatchsupport/crkapk Ljava/io/File;
invokestatic com/chelpus/Utils/copyFile(Ljava/io/File;Ljava/io/File;)V
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchsupport/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/AndroidManifest.xml"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 16
aload 16
invokevirtual java/io/File/exists()Z
ifne L49
new java/io/FileNotFoundException
dup
invokespecial java/io/FileNotFoundException/<init>()V
athrow
L49:
new com/android/vending/billing/InAppBillingService/LUCK/AxmlExample
dup
invokespecial com/android/vending/billing/InAppBillingService/LUCK/AxmlExample/<init>()V
aload 16
ldc "19"
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/AxmlExample/changeTargetApi(Ljava/io/File;Ljava/lang/String;)Z
ifne L50
aload 16
invokevirtual java/io/File/delete()Z
pop
L50:
getstatic com/chelpus/root/utils/runpatchsupport/classesFiles Ljava/util/ArrayList;
ifnull L52
getstatic com/chelpus/root/utils/runpatchsupport/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifne L55
L52:
new java/io/FileNotFoundException
dup
invokespecial java/io/FileNotFoundException/<init>()V
athrow
L51:
astore 16
L53:
aload 16
invokevirtual java/lang/Exception/printStackTrace()V
L54:
goto L50
L55:
getstatic com/chelpus/root/utils/runpatchsupport/filestopatch Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
getstatic com/chelpus/root/utils/runpatchsupport/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 16
L56:
aload 16
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L59
aload 16
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
astore 19
aload 19
invokevirtual java/io/File/exists()Z
ifne L57
new java/io/FileNotFoundException
dup
invokespecial java/io/FileNotFoundException/<init>()V
athrow
L57:
getstatic com/chelpus/root/utils/runpatchsupport/filestopatch Ljava/util/ArrayList;
aload 19
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L58:
goto L56
L59:
getstatic com/chelpus/root/utils/runpatchsupport/ART Z
ifeq L39
ldc "ART mode create dex enabled."
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
L60:
aload 0
iconst_0
aaload
astore 16
L61:
aload 0
iconst_2
aaload
putstatic com/chelpus/root/utils/runpatchsupport/appdir Ljava/lang/String;
aload 0
iconst_3
aaload
putstatic com/chelpus/root/utils/runpatchsupport/sddir Ljava/lang/String;
invokestatic com/chelpus/root/utils/runpatchsupport/clearTempSD()V
new java/io/File
dup
getstatic com/chelpus/root/utils/runpatchsupport/appdir Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokestatic com/chelpus/root/utils/runpatchsupport/unzipART(Ljava/io/File;)V
getstatic com/chelpus/root/utils/runpatchsupport/classesFiles Ljava/util/ArrayList;
ifnull L62
getstatic com/chelpus/root/utils/runpatchsupport/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifne L63
L62:
new java/io/FileNotFoundException
dup
invokespecial java/io/FileNotFoundException/<init>()V
athrow
L63:
getstatic com/chelpus/root/utils/runpatchsupport/filestopatch Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
getstatic com/chelpus/root/utils/runpatchsupport/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 16
L64:
aload 16
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L39
aload 16
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
astore 19
aload 19
invokevirtual java/io/File/exists()Z
ifne L65
new java/io/FileNotFoundException
dup
invokespecial java/io/FileNotFoundException/<init>()V
athrow
L65:
getstatic com/chelpus/root/utils/runpatchsupport/filestopatch Ljava/util/ArrayList;
aload 19
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L66:
goto L64
L67:
aload 20
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aload 16
iconst_0
invokestatic com/chelpus/Utils/getStringIds(Ljava/lang/String;Ljava/util/ArrayList;Z)Ljava/util/ArrayList;
astore 16
L68:
goto L41
L69:
aload 22
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 24
L70:
aload 24
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L218
aload 24
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/CommandItem
astore 25
aload 25
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/object Ljava/lang/String;
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/str Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L71
aload 25
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
putfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/Object [B
L71:
aload 25
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/method Ljava/lang/String;
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/str Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L70
aload 25
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
putfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/Method [B
L72:
goto L70
L218:
iload 9
istore 7
L73:
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/str Ljava/lang/String;
ldc "com.android.vending"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L219
aload 18
iconst_5
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_2
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_0
baload
bastore
aload 18
iconst_5
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_3
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_1
baload
bastore
aload 18
bipush 6
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_2
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_0
baload
bastore
aload 18
bipush 6
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_3
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_1
baload
bastore
aload 18
bipush 6
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_4
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_2
baload
bastore
aload 18
bipush 6
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_5
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_3
baload
bastore
L74:
iconst_1
istore 7
L219:
iload 6
istore 8
L75:
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/str Ljava/lang/String;
ldc "com.android.vending.billing.InAppBillingService.BIND"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L220
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "c.a.v.b.i "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/bits32 Z
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_0
baload
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_1
baload
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_2
baload
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_3
baload
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 18
iconst_0
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_2
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_0
baload
bastore
aload 18
iconst_0
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_3
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_1
baload
bastore
aload 18
iconst_5
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iconst_2
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_0
baload
bastore
aload 18
iconst_5
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iconst_3
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_1
baload
bastore
aload 18
iconst_1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_2
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_0
baload
bastore
aload 18
iconst_1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_3
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_1
baload
bastore
aload 18
iconst_1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_4
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_2
baload
bastore
aload 18
iconst_1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_5
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_3
baload
bastore
aload 18
bipush 6
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iconst_2
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_0
baload
bastore
aload 18
bipush 6
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iconst_3
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_1
baload
bastore
aload 18
bipush 6
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iconst_4
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_2
baload
bastore
aload 18
bipush 6
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iconst_5
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_3
baload
bastore
L76:
iconst_1
istore 8
L220:
iload 4
istore 10
L77:
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/str Ljava/lang/String;
ldc "SHA1withRSA"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L221
aload 18
iconst_2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_2
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_0
baload
bastore
aload 18
iconst_2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_3
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_1
baload
bastore
aload 18
iconst_3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_2
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_0
baload
bastore
aload 18
iconst_3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_3
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_1
baload
bastore
aload 18
iconst_3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_4
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_2
baload
bastore
aload 18
iconst_3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_5
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/offset [B
iconst_3
baload
bastore
L78:
iconst_1
istore 10
L221:
iload 5
istore 11
L79:
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/str Ljava/lang/String;
ldc "engineVerify"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L222
L80:
iconst_1
istore 11
L222:
iload 11
istore 5
iload 7
istore 9
iload 8
istore 6
iload 10
istore 4
L81:
aload 23
getfield com/android/vending/billing/InAppBillingService/LUCK/StringItem/str Ljava/lang/String;
ldc "Ljava/security/SignatureSpi;"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L43
L82:
iconst_1
istore 3
iload 11
istore 5
iload 7
istore 9
iload 8
istore 6
iload 10
istore 4
goto L43
L83:
aload 18
iconst_0
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/pattern Z
aload 18
iconst_1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/pattern Z
aload 18
iconst_5
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/pattern Z
aload 18
bipush 6
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/pattern Z
L84:
iload 4
ifne L86
L85:
aload 18
iconst_2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/pattern Z
aload 18
iconst_3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/pattern Z
aload 18
iconst_4
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/pattern Z
L86:
iload 6
ifeq L223
iload 9
ifeq L223
L87:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Relaced strings:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
astore 16
aload 20
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
astore 23
ldc com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment
invokevirtual java/lang/Class/getPackage()Ljava/lang/Package;
invokevirtual java/lang/Package/getName()Ljava/lang/String;
astore 24
aload 16
aload 23
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "com.android.vending.billing.InAppBillingService.BIND"
aastore
iconst_0
iconst_1
anewarray java/lang/String
dup
iconst_0
aload 24
aastore
invokestatic com/chelpus/Utils/replaceStringIds(Ljava/lang/String;[Ljava/lang/String;Z[Ljava/lang/String;)I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
L88:
ldc "Parse data for patch."
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
getstatic com/chelpus/root/utils/runpatchsupport/print Ljava/io/PrintStream;
ldc "Parse data for patch."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 20
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aload 22
iconst_0
invokestatic com/chelpus/Utils/getMethodsIds(Ljava/lang/String;Ljava/util/ArrayList;Z)Z
pop
aload 22
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 16
L89:
aload 16
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L224
aload 16
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/CommandItem
astore 22
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/found_index_command Z
ifeq L89
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/object Ljava/lang/String;
ldc "Ljava/security/Signature;"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L90
aload 18
bipush 7
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_2
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/index_command [B
iconst_0
baload
bastore
aload 18
bipush 7
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_3
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/index_command [B
iconst_1
baload
bastore
aload 18
bipush 7
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
iconst_1
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/pattern Z
L90:
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/object Ljava/lang/String;
ldc "Landroid/content/Intent;"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L89
getstatic com/chelpus/root/utils/runpatchsupport/pattern3 Z
ifeq L89
aload 18
bipush 8
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_2
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/index_command [B
iconst_0
baload
bastore
aload 18
bipush 8
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_3
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/index_command [B
iconst_1
baload
bastore
aload 18
bipush 8
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
iconst_1
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/pattern Z
L91:
goto L89
L224:
iload 5
ifeq L95
iload 3
ifeq L95
L92:
aload 20
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aload 21
iconst_0
invokestatic com/chelpus/Utils/getTypesIds(Ljava/lang/String;Ljava/util/ArrayList;Z)Z
pop
aload 21
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 16
L93:
aload 16
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L95
aload 16
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/TypesItem
astore 21
aload 21
getfield com/android/vending/billing/InAppBillingService/LUCK/TypesItem/found_id_type Z
ifeq L93
aload 21
getfield com/android/vending/billing/InAppBillingService/LUCK/TypesItem/type Ljava/lang/String;
ldc "Ljava/security/SignatureSpi;"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L93
aload 18
bipush 11
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_2
aload 21
getfield com/android/vending/billing/InAppBillingService/LUCK/TypesItem/id_type [B
iconst_0
baload
bastore
aload 18
bipush 11
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_3
aload 21
getfield com/android/vending/billing/InAppBillingService/LUCK/TypesItem/id_type [B
iconst_1
baload
bastore
aload 18
bipush 11
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
iconst_1
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/pattern Z
L94:
goto L93
L95:
aload 18
invokevirtual java/util/ArrayList/size()I
anewarray com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
astore 16
L96:
iconst_0
istore 3
L97:
aload 18
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 21
L98:
aload 21
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L225
aload 16
iload 3
aload 21
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
aastore
L99:
iload 3
iconst_1
iadd
istore 3
goto L98
L226:
iload 3
ifle L102
iload 3
iconst_1
isub
istore 3
L100:
ldc "Reworked inapp string!"
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
getstatic com/chelpus/root/utils/runpatchsupport/print Ljava/io/PrintStream;
ldc "Reworked inapp string!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L101:
goto L226
L102:
invokestatic java/lang/System/currentTimeMillis()J
lstore 14
new java/io/RandomAccessFile
dup
aload 20
ldc "rw"
invokespecial java/io/RandomAccessFile/<init>(Ljava/io/File;Ljava/lang/String;)V
invokevirtual java/io/RandomAccessFile/getChannel()Ljava/nio/channels/FileChannel;
astore 20
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Size file:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 20
invokevirtual java/nio/channels/FileChannel/size()J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
aload 20
getstatic java/nio/channels/FileChannel$MapMode/READ_WRITE Ljava/nio/channels/FileChannel$MapMode;
lconst_0
aload 20
invokevirtual java/nio/channels/FileChannel/size()J
l2i
i2l
invokevirtual java/nio/channels/FileChannel/map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
astore 21
L103:
iconst_0
istore 10
iconst_0
istore 7
iconst_0
istore 6
iconst_0
istore 3
lconst_0
lstore 12
L104:
aload 21
invokevirtual java/nio/MappedByteBuffer/hasRemaining()Z
ifeq L132
L105:
iload 3
istore 8
L107:
getstatic com/chelpus/root/utils/runpatchsupport/createAPK Z
ifne L110
L108:
iload 3
istore 8
L109:
aload 21
invokevirtual java/nio/MappedByteBuffer/position()I
iload 3
isub
ldc_w 149999
if_icmple L110
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Progress size:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 21
invokevirtual java/nio/MappedByteBuffer/position()I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
aload 21
invokevirtual java/nio/MappedByteBuffer/position()I
istore 8
L110:
aload 21
invokevirtual java/nio/MappedByteBuffer/position()I
istore 11
aload 21
invokevirtual java/nio/MappedByteBuffer/get()B
istore 2
L111:
iconst_0
istore 3
iconst_0
istore 9
L112:
iload 9
aload 16
arraylength
if_icmpge L210
L113:
aload 16
iload 9
aaload
astore 22
L114:
aload 21
iload 11
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L115:
iload 3
istore 4
iload 7
istore 5
L116:
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
ifeq L227
L117:
iload 9
iconst_5
if_icmpeq L228
iload 3
istore 4
iload 7
istore 5
iload 9
bipush 6
if_icmpne L227
goto L228
L229:
iload 5
bipush 60
if_icmpge L156
iload 5
istore 3
L118:
iload 2
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_0
baload
if_icmpne L135
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iconst_0
iaload
ifne L119
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iconst_0
iload 2
bastore
L119:
iconst_1
istore 7
L120:
aload 21
iload 11
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 21
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
L121:
iload 1
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iload 7
baload
if_icmpeq L124
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 7
iaload
iconst_1
if_icmpeq L124
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 7
iaload
bipush 20
if_icmpeq L124
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 7
iaload
bipush 21
if_icmpeq L124
L122:
iload 5
istore 3
L123:
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 7
iaload
bipush 23
if_icmpne L135
L124:
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iload 7
iaload
ifne L125
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iload 7
iload 1
bastore
L125:
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iload 7
iaload
bipush 20
if_icmpne L126
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iload 7
iload 1
bipush 15
iand
i2b
bastore
L126:
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iload 7
iaload
bipush 21
if_icmpne L127
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iload 7
iload 1
bipush 15
iand
bipush 16
iadd
i2b
bastore
L127:
iload 7
iconst_1
iadd
istore 7
L128:
iload 7
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
arraylength
if_icmpne L154
aload 21
iload 11
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 21
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
invokevirtual java/nio/MappedByteBuffer/put([B)Ljava/nio/ByteBuffer;
pop
aload 21
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/resultText Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
getstatic com/chelpus/root/utils/runpatchsupport/print Ljava/io/PrintStream;
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/resultText Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 22
iconst_1
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/result Z
aload 22
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
aload 18
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 23
L129:
aload 23
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L230
aload 23
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
astore 24
aload 24
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L129
aload 24
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
L130:
goto L129
L106:
astore 16
L131:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 16
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
L132:
aload 20
invokevirtual java/nio/channels/FileChannel/close()V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokestatic java/lang/System/currentTimeMillis()J
lload 14
lsub
ldc2_w 1000L
ldiv
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
ldc "Analise Results:"
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
L133:
goto L40
L230:
iconst_0
istore 3
L134:
aload 21
iload 11
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L135:
aload 21
iload 11
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L136:
iload 3
istore 5
L227:
iload 4
istore 3
iload 6
istore 7
L137:
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
ifeq L231
L138:
iload 9
bipush 9
if_icmpeq L139
iload 4
istore 3
iload 6
istore 7
iload 9
bipush 10
if_icmpne L231
L139:
getstatic com/chelpus/root/utils/runpatchsupport/print Ljava/io/PrintStream;
ldc "search jump"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L140:
iload 4
istore 3
iload 6
istore 7
iload 4
ifne L232
iload 6
iconst_1
iadd
istore 7
iconst_1
istore 3
L232:
iload 7
bipush 90
if_icmpge L181
iload 7
istore 4
L141:
iload 2
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_0
baload
if_icmpne L162
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iconst_0
iaload
ifne L142
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iconst_0
iload 2
bastore
L142:
iconst_1
istore 6
L143:
aload 21
iload 11
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 21
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
L144:
iload 1
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iload 6
baload
if_icmpeq L147
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 6
iaload
iconst_1
if_icmpeq L147
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 6
iaload
bipush 20
if_icmpeq L147
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 6
iaload
bipush 21
if_icmpeq L147
L145:
iload 7
istore 4
L146:
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 6
iaload
bipush 23
if_icmpne L162
L147:
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iload 6
iaload
ifne L148
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iload 6
iload 1
bastore
L148:
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iload 6
iaload
bipush 20
if_icmpne L149
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iload 6
iload 1
bipush 15
iand
i2b
bastore
L149:
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iload 6
iaload
bipush 21
if_icmpne L150
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iload 6
iload 1
bipush 15
iand
bipush 16
iadd
i2b
bastore
L150:
iload 6
iconst_1
iadd
istore 6
L151:
iload 6
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
arraylength
if_icmpne L179
aload 21
iload 11
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 21
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
invokevirtual java/nio/MappedByteBuffer/put([B)Ljava/nio/ByteBuffer;
pop
aload 21
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/resultText Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
getstatic com/chelpus/root/utils/runpatchsupport/print Ljava/io/PrintStream;
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/resultText Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 22
iconst_1
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/result Z
aload 22
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
aload 18
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 23
L152:
aload 23
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L233
aload 23
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
astore 24
aload 24
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L152
aload 24
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
L153:
goto L152
L154:
aload 21
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
L155:
goto L121
L156:
aload 22
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
aload 18
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 23
L157:
aload 23
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L234
aload 23
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
astore 24
aload 24
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L157
aload 24
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
L158:
goto L157
L234:
iconst_0
istore 5
L159:
aload 21
iload 11
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L160:
goto L227
L233:
iconst_0
istore 4
L161:
aload 21
iload 11
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L162:
aload 21
iload 11
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L163:
iload 4
istore 7
L231:
iload 10
istore 4
L164:
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
ifeq L188
L165:
iload 10
istore 4
iload 9
iconst_4
if_icmpne L188
iload 10
iconst_1
iadd
istore 10
iload 10
bipush 90
if_icmpge L201
iload 10
istore 4
L166:
iload 2
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_0
baload
if_icmpne L187
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iconst_0
iaload
ifne L167
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iconst_0
iload 2
bastore
L167:
iconst_1
istore 6
L168:
aload 21
iload 11
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 21
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
L169:
iload 1
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iload 6
baload
if_icmpeq L172
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 6
iaload
iconst_1
if_icmpeq L172
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 6
iaload
bipush 20
if_icmpeq L172
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 6
iaload
bipush 21
if_icmpeq L172
L170:
iload 10
istore 4
L171:
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 6
iaload
bipush 23
if_icmpne L187
L172:
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iload 6
iaload
ifne L173
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iload 6
iload 1
bastore
L173:
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iload 6
iaload
bipush 20
if_icmpne L174
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iload 6
iload 1
bipush 15
iand
i2b
bastore
L174:
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iload 6
iaload
bipush 21
if_icmpne L175
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iload 6
iload 1
bipush 15
iand
bipush 16
iadd
i2b
bastore
L175:
iload 6
iconst_1
iadd
istore 6
L176:
iload 6
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
arraylength
if_icmpne L199
aload 21
iload 11
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 21
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
invokevirtual java/nio/MappedByteBuffer/put([B)Ljava/nio/ByteBuffer;
pop
aload 21
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/resultText Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
getstatic com/chelpus/root/utils/runpatchsupport/print Ljava/io/PrintStream;
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/resultText Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 22
iconst_1
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/result Z
aload 22
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
aload 18
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 23
L177:
aload 23
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L235
aload 23
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
astore 24
aload 24
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L177
aload 24
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
L178:
goto L177
L179:
aload 21
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
L180:
goto L144
L181:
aload 22
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
aload 18
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 23
L182:
aload 23
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L236
aload 23
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
astore 24
aload 24
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L182
aload 24
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
L183:
goto L182
L236:
iconst_0
istore 7
L184:
aload 21
iload 11
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L185:
goto L231
L235:
iconst_0
istore 4
L186:
aload 21
iload 11
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L187:
aload 21
iload 11
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L188:
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
ifne L237
iload 2
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iconst_0
baload
if_icmpne L237
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/pattern Z
ifeq L237
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iconst_0
iaload
ifne L189
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iconst_0
iload 2
bastore
L189:
iconst_1
istore 6
L190:
aload 21
iload 11
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 21
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
L191:
iload 1
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
iload 6
baload
if_icmpeq L192
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 6
iaload
iconst_1
if_icmpeq L192
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 6
iaload
bipush 20
if_icmpeq L192
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 6
iaload
bipush 21
if_icmpeq L192
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origMask [I
iload 6
iaload
bipush 23
if_icmpne L208
L192:
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iload 6
iaload
ifne L193
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iload 6
iload 1
bastore
L193:
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iload 6
iaload
bipush 20
if_icmpne L194
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iload 6
iload 1
bipush 15
iand
i2b
bastore
L194:
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repMask [I
iload 6
iaload
bipush 21
if_icmpne L195
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
iload 6
iload 1
bipush 15
iand
bipush 16
iadd
i2b
bastore
L195:
iload 6
iconst_1
iadd
istore 6
L196:
iload 6
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/origByte [B
arraylength
if_icmpne L206
aload 21
iload 11
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 21
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/repByte [B
invokevirtual java/nio/MappedByteBuffer/put([B)Ljava/nio/ByteBuffer;
pop
aload 21
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/resultText Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
getstatic com/chelpus/root/utils/runpatchsupport/print Ljava/io/PrintStream;
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/resultText Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 22
iconst_1
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/result Z
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L208
aload 22
iconst_1
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
aload 18
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 23
L197:
aload 23
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L208
aload 23
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
astore 24
aload 24
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L197
aload 24
iconst_1
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
L198:
goto L197
L199:
aload 21
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
L200:
goto L169
L201:
aload 22
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
aload 18
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 23
L202:
aload 23
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L238
aload 23
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
astore 24
aload 24
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
aload 22
getfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/marker Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L202
aload 24
iconst_0
putfield com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/markerTrig Z
L203:
goto L202
L238:
iconst_0
istore 4
L204:
aload 21
iload 11
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L205:
goto L188
L206:
aload 21
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
L207:
goto L191
L208:
aload 21
iload 11
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L209:
goto L237
L210:
aload 21
iload 11
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L211:
lload 12
lconst_1
ladd
lstore 12
iload 8
istore 3
goto L104
L216:
getstatic com/chelpus/root/utils/runpatchsupport/createAPK Z
ifne L239
ldc "Create ODEX:"
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
aload 0
iconst_3
aaload
getstatic com/chelpus/root/utils/runpatchsupport/classesFiles Ljava/util/ArrayList;
aload 0
iconst_2
aaload
getstatic com/chelpus/root/utils/runpatchsupport/uid Ljava/lang/String;
aload 0
iconst_2
aaload
getstatic com/chelpus/root/utils/runpatchsupport/uid Ljava/lang/String;
invokestatic com/chelpus/Utils/getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokestatic com/chelpus/Utils/create_ODEX_root(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
istore 3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chelpus_return_"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 3
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
iload 3
ifne L239
getstatic com/chelpus/root/utils/runpatchsupport/ART Z
ifne L239
aload 0
iconst_1
aaload
aload 0
iconst_2
aaload
aload 0
iconst_2
aaload
iconst_1
invokestatic com/chelpus/Utils/getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;
getstatic com/chelpus/root/utils/runpatchsupport/uid Ljava/lang/String;
aload 0
iconst_3
aaload
invokestatic com/chelpus/Utils/afterPatch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
L239:
ldc "Optional Steps After Patch:"
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
getstatic com/chelpus/root/utils/runpatchsupport/createAPK Z
ifne L240
invokestatic com/chelpus/Utils/exitFromRootJava()V
L240:
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/LogOutputStream/allresult Ljava/lang/String;
putstatic com/chelpus/root/utils/runpatchsupport/result Ljava/lang/String;
return
L22:
astore 16
goto L20
L21:
astore 16
goto L20
L8:
astore 16
goto L19
L7:
astore 16
goto L19
L217:
iload 9
ifeq L83
iload 6
ifne L84
goto L83
L223:
goto L88
L225:
iconst_0
istore 3
goto L226
L228:
iload 3
istore 4
iload 7
istore 5
iload 3
ifne L229
iload 7
iconst_1
iadd
istore 5
iconst_1
istore 4
goto L229
L237:
iload 9
iconst_1
iadd
istore 9
iload 4
istore 10
iload 7
istore 6
iload 5
istore 7
goto L112
.limit locals 26
.limit stack 8
.end method

.method public static unzipART(Ljava/io/File;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L4 to L5 using L2
.catch java/lang/Exception from L6 to L7 using L2
.catch net/lingala/zip4j/exception/ZipException from L8 to L9 using L10
.catch java/lang/Exception from L8 to L9 using L11
.catch java/lang/Exception from L12 to L13 using L2
.catch java/lang/Exception from L13 to L14 using L2
.catch java/lang/Exception from L14 to L15 using L2
.catch java/lang/Exception from L16 to L17 using L2
.catch java/lang/Exception from L18 to L19 using L2
.catch java/lang/Exception from L20 to L21 using L2
.catch java/lang/Exception from L22 to L23 using L2
iconst_0
istore 1
L0:
new java/io/FileInputStream
dup
aload 0
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
astore 4
new java/util/zip/ZipInputStream
dup
aload 4
invokespecial java/util/zip/ZipInputStream/<init>(Ljava/io/InputStream;)V
astore 5
aload 5
invokevirtual java/util/zip/ZipInputStream/getNextEntry()Ljava/util/zip/ZipEntry;
astore 3
L1:
aload 3
ifnull L20
iconst_1
ifeq L20
L3:
aload 3
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
astore 3
aload 3
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "classes"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L13
aload 3
ldc ".dex"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L13
aload 3
ldc "/"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L13
new java/io/FileOutputStream
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchsupport/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
astore 6
sipush 2048
newarray byte
astore 7
L4:
aload 5
aload 7
invokevirtual java/util/zip/ZipInputStream/read([B)I
istore 2
L5:
iload 2
iconst_m1
if_icmpeq L12
L6:
aload 6
aload 7
iconst_0
iload 2
invokevirtual java/io/FileOutputStream/write([BII)V
L7:
goto L4
L2:
astore 3
L8:
new net/lingala/zip4j/core/ZipFile
dup
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/<init>(Ljava/io/File;)V
astore 0
aload 0
ldc "classes.dex"
getstatic com/chelpus/root/utils/runpatchsupport/sddir Ljava/lang/String;
invokevirtual net/lingala/zip4j/core/ZipFile/extractFile(Ljava/lang/String;Ljava/lang/String;)V
getstatic com/chelpus/root/utils/runpatchsupport/classesFiles Ljava/util/ArrayList;
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchsupport/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "classes.dex"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchsupport/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "classes.dex"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
aload 0
ldc "AndroidManifest.xml"
getstatic com/chelpus/root/utils/runpatchsupport/sddir Ljava/lang/String;
invokevirtual net/lingala/zip4j/core/ZipFile/extractFile(Ljava/lang/String;Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchsupport/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "AndroidManifest.xml"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L9:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception e"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
return
L12:
aload 5
invokevirtual java/util/zip/ZipInputStream/closeEntry()V
aload 6
invokevirtual java/io/FileOutputStream/close()V
getstatic com/chelpus/root/utils/runpatchsupport/classesFiles Ljava/util/ArrayList;
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchsupport/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchsupport/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L13:
aload 3
ldc "AndroidManifest.xml"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L24
new java/io/FileOutputStream
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchsupport/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "AndroidManifest.xml"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
astore 3
sipush 2048
newarray byte
astore 6
L14:
aload 5
aload 6
invokevirtual java/util/zip/ZipInputStream/read([B)I
istore 1
L15:
iload 1
iconst_m1
if_icmpeq L18
L16:
aload 3
aload 6
iconst_0
iload 1
invokevirtual java/io/FileOutputStream/write([BII)V
L17:
goto L14
L18:
aload 5
invokevirtual java/util/zip/ZipInputStream/closeEntry()V
aload 3
invokevirtual java/io/FileOutputStream/close()V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchsupport/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "AndroidManifest.xml"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L19:
iconst_1
istore 1
goto L24
L20:
aload 5
invokevirtual java/util/zip/ZipInputStream/close()V
aload 4
invokevirtual java/io/FileInputStream/close()V
L21:
return
L22:
aload 5
invokevirtual java/util/zip/ZipInputStream/getNextEntry()Ljava/util/zip/ZipEntry;
astore 3
L23:
goto L1
L10:
astore 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Error classes.dex decompress! "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception e1"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
goto L9
L11:
astore 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Error classes.dex decompress! "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception e1"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
goto L9
L24:
iconst_0
ifeq L22
iload 1
ifeq L22
goto L20
.limit locals 8
.limit stack 5
.end method

.method public static unzipSD(Ljava/io/File;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L2
.catch java/lang/Exception from L7 to L8 using L2
.catch java/lang/Exception from L9 to L10 using L2
.catch java/lang/Exception from L10 to L11 using L2
.catch java/lang/Exception from L12 to L13 using L2
.catch net/lingala/zip4j/exception/ZipException from L14 to L15 using L16
.catch java/lang/Exception from L14 to L15 using L17
.catch java/lang/Exception from L18 to L19 using L2
.catch java/lang/Exception from L20 to L21 using L2
.catch java/lang/Exception from L22 to L23 using L2
.catch java/lang/Exception from L23 to L24 using L2
.catch java/lang/Exception from L25 to L26 using L2
.catch java/lang/Exception from L27 to L28 using L2
.catch java/lang/Exception from L29 to L30 using L2
L0:
new java/io/FileInputStream
dup
aload 0
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
astore 3
new java/util/zip/ZipInputStream
dup
aload 3
invokespecial java/util/zip/ZipInputStream/<init>(Ljava/io/InputStream;)V
astore 4
L1:
iconst_0
istore 2
L3:
aload 4
invokevirtual java/util/zip/ZipInputStream/getNextEntry()Ljava/util/zip/ZipEntry;
astore 5
L4:
aload 5
ifnull L29
iload 2
istore 1
L5:
aload 5
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "classes"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L31
L6:
iload 2
istore 1
L7:
aload 5
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
ldc ".dex"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L31
L8:
iload 2
istore 1
L9:
aload 5
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
ldc "/"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L31
new java/io/FileOutputStream
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchsupport/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
astore 6
sipush 1024
newarray byte
astore 7
L10:
aload 4
aload 7
invokevirtual java/util/zip/ZipInputStream/read([B)I
istore 1
L11:
iload 1
iconst_m1
if_icmpeq L18
L12:
aload 6
aload 7
iconst_0
iload 1
invokevirtual java/io/FileOutputStream/write([BII)V
L13:
goto L10
L2:
astore 3
L14:
new net/lingala/zip4j/core/ZipFile
dup
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/<init>(Ljava/io/File;)V
astore 0
aload 0
ldc "classes.dex"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchsupport/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual net/lingala/zip4j/core/ZipFile/extractFile(Ljava/lang/String;Ljava/lang/String;)V
getstatic com/chelpus/root/utils/runpatchsupport/classesFiles Ljava/util/ArrayList;
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchsupport/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "classes.dex"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
ldc "AndroidManifest.xml"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchsupport/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual net/lingala/zip4j/core/ZipFile/extractFile(Ljava/lang/String;Ljava/lang/String;)V
L15:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Decompressunzip "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
return
L18:
getstatic com/chelpus/root/utils/runpatchsupport/classesFiles Ljava/util/ArrayList;
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchsupport/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L19:
iconst_1
istore 2
iload 2
istore 1
L20:
getstatic com/chelpus/root/utils/runpatchsupport/createAPK Z
ifne L31
aload 4
invokevirtual java/util/zip/ZipInputStream/closeEntry()V
aload 6
invokevirtual java/io/FileOutputStream/close()V
L21:
iload 2
istore 1
L31:
iload 1
istore 2
L22:
aload 5
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
ldc "AndroidManifest.xml"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L3
new java/io/FileOutputStream
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/runpatchsupport/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "AndroidManifest.xml"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
astore 5
sipush 1024
newarray byte
astore 6
L23:
aload 4
aload 6
invokevirtual java/util/zip/ZipInputStream/read([B)I
istore 2
L24:
iload 2
iconst_m1
if_icmpeq L32
L25:
aload 5
aload 6
iconst_0
iload 2
invokevirtual java/io/FileOutputStream/write([BII)V
L26:
goto L23
L32:
iload 1
istore 2
iload 1
ifeq L3
L27:
aload 4
invokevirtual java/util/zip/ZipInputStream/closeEntry()V
aload 5
invokevirtual java/io/FileOutputStream/close()V
L28:
iload 1
istore 2
goto L3
L29:
aload 4
invokevirtual java/util/zip/ZipInputStream/close()V
aload 3
invokevirtual java/io/FileInputStream/close()V
L30:
return
L16:
astore 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Error classes.dex decompress! "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception e1"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
goto L15
L17:
astore 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Error classes.dex decompress! "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception e1"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
goto L15
.limit locals 8
.limit stack 5
.end method
