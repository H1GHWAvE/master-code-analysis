.bytecode 50.0
.class public synchronized com/chelpus/root/utils/live_restorelib
.super java/lang/Object
.inner class static final inner com/chelpus/root/utils/live_restorelib$1

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static main([Ljava/lang/String;)V
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
.catch java/io/FileNotFoundException from L1 to L4 using L2
.catch java/lang/Exception from L1 to L4 using L3
.catch java/io/FileNotFoundException from L5 to L6 using L2
.catch java/lang/Exception from L5 to L6 using L3
.catch java/io/FileNotFoundException from L7 to L8 using L2
.catch java/lang/Exception from L7 to L8 using L3
.catch java/io/FileNotFoundException from L8 to L2 using L2
.catch java/lang/Exception from L8 to L2 using L3
.catch java/io/FileNotFoundException from L9 to L3 using L2
.catch java/lang/Exception from L9 to L3 using L3
.catch java/io/FileNotFoundException from L10 to L11 using L2
.catch java/lang/Exception from L10 to L11 using L3
new com/chelpus/root/utils/live_restorelib$1
dup
invokespecial com/chelpus/root/utils/live_restorelib$1/<init>()V
invokestatic com/chelpus/Utils/startRootJava(Ljava/lang/Object;)V
aload 0
iconst_0
aaload
astore 2
L0:
new java/io/File
dup
aload 2
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
new java/io/File
dup
aload 2
ldc "/data/data/"
ldc "/mnt/asec/"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
L1:
aload 1
invokevirtual java/io/File/exists()Z
ifeq L12
L4:
aload 1
astore 0
L12:
aload 0
astore 1
L5:
aload 0
invokevirtual java/io/File/exists()Z
ifne L6
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "-1"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
L6:
aload 1
astore 0
L7:
aload 1
invokevirtual java/io/File/exists()Z
ifne L8
new java/io/File
dup
aload 2
ldc "-1"
ldc "-2"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
L8:
aload 0
invokevirtual java/io/File/exists()Z
ifne L9
new java/io/FileNotFoundException
dup
invokespecial java/io/FileNotFoundException/<init>()V
athrow
L2:
astore 0
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Error: Backup files are not found!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L13:
invokestatic com/chelpus/Utils/exitFromRootJava()V
return
L9:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".backup"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
aload 1
invokevirtual java/io/File/exists()Z
ifne L10
new java/io/FileNotFoundException
dup
invokespecial java/io/FileNotFoundException/<init>()V
athrow
L3:
astore 0
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception e"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
goto L13
L10:
aload 1
aload 0
invokestatic com/chelpus/Utils/copyFile(Ljava/io/File;Ljava/io/File;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Restore - done!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L11:
goto L13
.limit locals 3
.limit stack 5
.end method
