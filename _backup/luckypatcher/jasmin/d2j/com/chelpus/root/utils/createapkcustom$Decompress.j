.bytecode 50.0
.class public synchronized com/chelpus/root/utils/createapkcustom$Decompress
.super java/lang/Object
.inner class public static Decompress inner com/chelpus/root/utils/createapkcustom$Decompress outer com/chelpus/root/utils/createapkcustom

.field private '_location' Ljava/lang/String;

.field private '_zipFile' Ljava/lang/String;

.method public <init>(Ljava/lang/String;Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/chelpus/root/utils/createapkcustom$Decompress/_zipFile Ljava/lang/String;
aload 0
aload 2
putfield com/chelpus/root/utils/createapkcustom$Decompress/_location Ljava/lang/String;
aload 0
ldc ""
invokespecial com/chelpus/root/utils/createapkcustom$Decompress/_dirChecker(Ljava/lang/String;)V
return
.limit locals 3
.limit stack 2
.end method

.method private _dirChecker(Ljava/lang/String;)V
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/chelpus/root/utils/createapkcustom$Decompress/_location Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
aload 1
invokevirtual java/io/File/isFile()Z
ifeq L0
aload 1
invokevirtual java/io/File/delete()Z
pop
L0:
aload 1
invokevirtual java/io/File/exists()Z
ifne L1
aload 1
invokevirtual java/io/File/mkdirs()Z
pop
L1:
return
.limit locals 2
.limit stack 4
.end method

.method public unzip(Ljava/lang/String;)Ljava/lang/String;
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L2
.catch java/lang/Exception from L7 to L8 using L2
.catch java/lang/Exception from L9 to L10 using L2
.catch net/lingala/zip4j/exception/ZipException from L11 to L12 using L13
.catch java/lang/Exception from L11 to L12 using L14
.catch net/lingala/zip4j/exception/ZipException from L15 to L16 using L13
.catch java/lang/Exception from L15 to L16 using L14
.catch java/lang/Exception from L17 to L18 using L2
.catch java/lang/Exception from L19 to L20 using L2
.catch java/lang/Exception from L21 to L22 using L2
.catch java/lang/Exception from L23 to L24 using L2
.catch java/lang/Exception from L25 to L26 using L2
.catch java/lang/Exception from L27 to L28 using L2
.catch java/lang/Exception from L29 to L30 using L2
.catch java/lang/Exception from L31 to L32 using L2
.catch java/lang/Exception from L33 to L34 using L2
.catch java/lang/Exception from L35 to L36 using L2
.catch java/lang/Exception from L37 to L38 using L2
.catch java/lang/Exception from L39 to L40 using L2
.catch java/lang/Exception from L41 to L42 using L2
.catch java/lang/Exception from L43 to L44 using L2
.catch java/lang/Exception from L45 to L46 using L2
.catch java/lang/Exception from L47 to L48 using L2
.catch java/lang/Exception from L49 to L50 using L2
.catch java/lang/Exception from L51 to L52 using L2
.catch java/lang/Exception from L53 to L54 using L2
aload 1
astore 3
L0:
new java/io/FileInputStream
dup
aload 0
getfield com/chelpus/root/utils/createapkcustom$Decompress/_zipFile Ljava/lang/String;
invokespecial java/io/FileInputStream/<init>(Ljava/lang/String;)V
astore 6
L1:
aload 1
astore 3
L3:
new java/util/zip/ZipInputStream
dup
aload 6
invokespecial java/util/zip/ZipInputStream/<init>(Ljava/io/InputStream;)V
astore 7
L4:
aload 1
astore 4
L55:
aload 4
astore 3
L5:
aload 7
invokevirtual java/util/zip/ZipInputStream/getNextEntry()Ljava/util/zip/ZipEntry;
astore 8
L6:
aload 8
ifnull L50
aload 4
astore 3
L7:
aload 8
invokevirtual java/util/zip/ZipEntry/isDirectory()Z
ifeq L56
L8:
aload 4
astore 3
L9:
aload 0
aload 8
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
invokespecial com/chelpus/root/utils/createapkcustom$Decompress/_dirChecker(Ljava/lang/String;)V
L10:
goto L55
L2:
astore 1
invokestatic com/chelpus/root/utils/createapkcustom/access$000()Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Decompressunzip "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L11:
new net/lingala/zip4j/core/ZipFile
dup
aload 0
getfield com/chelpus/root/utils/createapkcustom$Decompress/_zipFile Ljava/lang/String;
invokespecial net/lingala/zip4j/core/ZipFile/<init>(Ljava/lang/String;)V
astore 1
aload 1
invokevirtual net/lingala/zip4j/core/ZipFile/getFileHeaders()Ljava/util/List;
astore 4
L12:
iconst_0
istore 2
L15:
iload 2
aload 4
invokeinterface java/util/List/size()I 0
if_icmpge L54
aload 4
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast net/lingala/zip4j/model/FileHeader
astore 5
aload 5
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
aload 3
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L57
invokestatic com/chelpus/root/utils/createapkcustom/access$000()Ljava/io/PrintStream;
aload 5
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 1
aload 5
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
aload 0
getfield com/chelpus/root/utils/createapkcustom$Decompress/_location Ljava/lang/String;
invokevirtual net/lingala/zip4j/core/ZipFile/extractFile(Ljava/lang/String;Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/chelpus/root/utils/createapkcustom$Decompress/_location Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
L16:
aload 1
areturn
L56:
aload 4
astore 3
aload 4
astore 1
L17:
aload 4
ldc "/"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L20
L18:
aload 4
astore 3
L19:
aload 4
ldc "/"
ldc ""
invokevirtual java/lang/String/replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 1
L20:
aload 1
astore 4
aload 1
astore 3
L21:
aload 8
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
aload 1
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L55
L22:
aload 1
astore 3
L23:
aload 8
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
ldc "\\/+"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 9
L24:
ldc ""
astore 4
iconst_0
istore 2
L58:
aload 1
astore 3
L25:
iload 2
aload 9
arraylength
iconst_1
isub
if_icmpge L59
L26:
aload 1
astore 3
aload 4
astore 5
L27:
aload 9
iload 2
aaload
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L60
L28:
aload 1
astore 3
L29:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 9
iload 2
aaload
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 5
L30:
goto L60
L59:
aload 1
astore 3
L31:
aload 0
aload 4
invokespecial com/chelpus/root/utils/createapkcustom$Decompress/_dirChecker(Ljava/lang/String;)V
L32:
aload 1
astore 3
L33:
new java/io/FileOutputStream
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/chelpus/root/utils/createapkcustom$Decompress/_location Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 8
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
astore 4
L34:
aload 1
astore 3
L35:
sipush 1024
newarray byte
astore 5
L36:
aload 1
astore 3
L37:
aload 7
aload 5
invokevirtual java/util/zip/ZipInputStream/read([B)I
istore 2
L38:
iload 2
iconst_m1
if_icmpeq L61
aload 1
astore 3
L39:
aload 4
aload 5
iconst_0
iload 2
invokevirtual java/io/FileOutputStream/write([BII)V
L40:
goto L36
L61:
aload 1
astore 3
L41:
aload 7
invokevirtual java/util/zip/ZipInputStream/closeEntry()V
L42:
aload 1
astore 3
L43:
aload 4
invokevirtual java/io/FileOutputStream/close()V
L44:
aload 1
astore 3
L45:
aload 7
invokevirtual java/util/zip/ZipInputStream/close()V
L46:
aload 1
astore 3
L47:
aload 6
invokevirtual java/io/FileInputStream/close()V
L48:
aload 1
astore 3
L49:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/chelpus/root/utils/createapkcustom$Decompress/_location Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 8
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L50:
aload 4
astore 3
L51:
aload 7
invokevirtual java/util/zip/ZipInputStream/close()V
L52:
aload 4
astore 3
L53:
aload 6
invokevirtual java/io/FileInputStream/close()V
L54:
ldc ""
areturn
L57:
iload 2
iconst_1
iadd
istore 2
goto L15
L13:
astore 1
aload 1
invokevirtual net/lingala/zip4j/exception/ZipException/printStackTrace()V
goto L54
L14:
astore 1
aload 1
invokevirtual java/lang/Exception/printStackTrace()V
goto L54
L60:
iload 2
iconst_1
iadd
istore 2
aload 5
astore 4
goto L58
.limit locals 10
.limit stack 4
.end method

.method public unzip()V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
.catch java/lang/Exception from L4 to L5 using L2
.catch net/lingala/zip4j/exception/ZipException from L6 to L7 using L8
.catch java/lang/Exception from L6 to L7 using L9
.catch net/lingala/zip4j/exception/ZipException from L10 to L11 using L8
.catch java/lang/Exception from L10 to L11 using L9
.catch java/lang/Exception from L12 to L13 using L2
.catch java/lang/Exception from L14 to L15 using L2
.catch java/lang/Exception from L16 to L17 using L2
.catch java/lang/Exception from L18 to L19 using L2
.catch java/lang/Exception from L19 to L20 using L2
.catch java/lang/Exception from L21 to L22 using L2
.catch java/lang/Exception from L23 to L24 using L2
.catch java/lang/Exception from L25 to L26 using L2
L0:
new java/io/FileInputStream
dup
aload 0
getfield com/chelpus/root/utils/createapkcustom$Decompress/_zipFile Ljava/lang/String;
invokespecial java/io/FileInputStream/<init>(Ljava/lang/String;)V
astore 4
new java/util/zip/ZipInputStream
dup
aload 4
invokespecial java/util/zip/ZipInputStream/<init>(Ljava/io/InputStream;)V
astore 5
L1:
aload 5
invokevirtual java/util/zip/ZipInputStream/getNextEntry()Ljava/util/zip/ZipEntry;
astore 6
L3:
aload 6
ifnull L25
L4:
aload 6
invokevirtual java/util/zip/ZipEntry/isDirectory()Z
ifeq L12
aload 0
aload 6
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
invokespecial com/chelpus/root/utils/createapkcustom$Decompress/_dirChecker(Ljava/lang/String;)V
L5:
goto L1
L2:
astore 2
invokestatic com/chelpus/root/utils/createapkcustom/access$000()Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Decompressunzip "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L6:
new net/lingala/zip4j/core/ZipFile
dup
aload 0
getfield com/chelpus/root/utils/createapkcustom$Decompress/_zipFile Ljava/lang/String;
invokespecial net/lingala/zip4j/core/ZipFile/<init>(Ljava/lang/String;)V
astore 2
aload 2
invokevirtual net/lingala/zip4j/core/ZipFile/getFileHeaders()Ljava/util/List;
astore 3
L7:
iconst_0
istore 1
L10:
iload 1
aload 3
invokeinterface java/util/List/size()I 0
if_icmpge L26
aload 3
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast net/lingala/zip4j/model/FileHeader
astore 4
aload 4
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
ldc ".so"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L11
invokestatic com/chelpus/root/utils/createapkcustom/access$000()Ljava/io/PrintStream;
aload 4
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 2
aload 4
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
aload 0
getfield com/chelpus/root/utils/createapkcustom$Decompress/_location Ljava/lang/String;
invokevirtual net/lingala/zip4j/core/ZipFile/extractFile(Ljava/lang/String;Ljava/lang/String;)V
L11:
iload 1
iconst_1
iadd
istore 1
goto L10
L12:
aload 6
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
ldc ".so"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L1
aload 6
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
ldc "\\/+"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 7
L13:
ldc ""
astore 2
iconst_0
istore 1
L14:
iload 1
aload 7
arraylength
iconst_1
isub
if_icmpge L18
L15:
aload 2
astore 3
L16:
aload 7
iload 1
aaload
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L27
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 7
iload 1
aaload
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 3
L17:
goto L27
L18:
aload 0
aload 2
invokespecial com/chelpus/root/utils/createapkcustom$Decompress/_dirChecker(Ljava/lang/String;)V
new java/io/FileOutputStream
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/chelpus/root/utils/createapkcustom$Decompress/_location Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
astore 2
sipush 1024
newarray byte
astore 3
L19:
aload 5
aload 3
invokevirtual java/util/zip/ZipInputStream/read([B)I
istore 1
L20:
iload 1
iconst_m1
if_icmpeq L23
L21:
aload 2
aload 3
iconst_0
iload 1
invokevirtual java/io/FileOutputStream/write([BII)V
L22:
goto L19
L23:
aload 5
invokevirtual java/util/zip/ZipInputStream/closeEntry()V
aload 2
invokevirtual java/io/FileOutputStream/close()V
L24:
goto L1
L25:
aload 5
invokevirtual java/util/zip/ZipInputStream/close()V
aload 4
invokevirtual java/io/FileInputStream/close()V
L26:
return
L8:
astore 2
aload 2
invokevirtual net/lingala/zip4j/exception/ZipException/printStackTrace()V
return
L9:
astore 2
aload 2
invokevirtual java/lang/Exception/printStackTrace()V
return
L27:
iload 1
iconst_1
iadd
istore 1
aload 3
astore 2
goto L14
.limit locals 8
.limit stack 4
.end method
