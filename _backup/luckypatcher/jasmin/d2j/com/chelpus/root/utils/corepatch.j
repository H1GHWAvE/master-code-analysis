.bytecode 50.0
.class public synchronized com/chelpus/root/utils/corepatch
.super java/lang/Object
.inner class static final inner com/chelpus/root/utils/corepatch$1

.field public static final 'MAGIC' [B

.field public static 'adler' I

.field public static 'fileBytes' Ljava/nio/MappedByteBuffer;

.field public static 'lastByteReplace' [B

.field public static 'lastPatchPosition' I

.field public static 'not_found_bytes_for_patch' Z

.field public static 'onlyDalvik' Z

.field public static 'toolfilesdir' Ljava/lang/String;

.method static <clinit>()V
bipush 8
newarray byte
dup
iconst_0
ldc_w 100
bastore
dup
iconst_1
ldc_w 101
bastore
dup
iconst_2
ldc_w 121
bastore
dup
iconst_3
ldc_w 10
bastore
dup
iconst_4
ldc_w 48
bastore
dup
iconst_5
ldc_w 51
bastore
dup
bipush 6
ldc_w 53
bastore
dup
bipush 7
ldc_w 0
bastore
putstatic com/chelpus/root/utils/corepatch/MAGIC [B
ldc ""
putstatic com/chelpus/root/utils/corepatch/toolfilesdir Ljava/lang/String;
iconst_0
putstatic com/chelpus/root/utils/corepatch/onlyDalvik Z
iconst_0
putstatic com/chelpus/root/utils/corepatch/not_found_bytes_for_patch Z
aconst_null
putstatic com/chelpus/root/utils/corepatch/fileBytes Ljava/nio/MappedByteBuffer;
iconst_0
putstatic com/chelpus/root/utils/corepatch/lastPatchPosition I
aconst_null
putstatic com/chelpus/root/utils/corepatch/lastByteReplace [B
return
.limit locals 0
.limit stack 4
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static applyPatch(IB[B[B[B[BZ)Z
aload 2
ifnull L0
iload 1
aload 2
iconst_0
baload
if_icmpne L0
iload 6
ifeq L0
aload 5
iconst_0
baload
ifne L1
aload 4
iconst_0
iload 1
bastore
L1:
iconst_1
istore 7
getstatic com/chelpus/root/utils/corepatch/fileBytes Ljava/nio/MappedByteBuffer;
iload 0
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
getstatic com/chelpus/root/utils/corepatch/fileBytes Ljava/nio/MappedByteBuffer;
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
L2:
iload 1
aload 2
iload 7
baload
if_icmpeq L3
aload 3
iload 7
baload
iconst_1
if_icmpne L4
L3:
aload 5
iload 7
baload
ifne L5
aload 4
iload 7
iload 1
bastore
L5:
aload 5
iload 7
baload
iconst_3
if_icmpne L6
aload 4
iload 7
iload 1
bipush 15
iand
i2b
bastore
L6:
aload 5
iload 7
baload
iconst_2
if_icmpne L7
aload 4
iload 7
iload 1
bipush 15
iand
iload 1
bipush 15
iand
bipush 16
imul
iadd
i2b
bastore
L7:
iload 7
iconst_1
iadd
istore 7
iload 7
aload 2
arraylength
if_icmpne L8
getstatic com/chelpus/root/utils/corepatch/fileBytes Ljava/nio/MappedByteBuffer;
iload 0
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
getstatic com/chelpus/root/utils/corepatch/fileBytes Ljava/nio/MappedByteBuffer;
aload 4
invokevirtual java/nio/MappedByteBuffer/put([B)Ljava/nio/ByteBuffer;
pop
getstatic com/chelpus/root/utils/corepatch/fileBytes Ljava/nio/MappedByteBuffer;
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
iconst_1
ireturn
L8:
getstatic com/chelpus/root/utils/corepatch/fileBytes Ljava/nio/MappedByteBuffer;
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
goto L2
L4:
getstatic com/chelpus/root/utils/corepatch/fileBytes Ljava/nio/MappedByteBuffer;
iload 0
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L0:
iconst_0
ireturn
.limit locals 8
.limit stack 5
.end method

.method private static applyPatchCounter(IB[B[B[B[BIIZ)Z
aload 2
ifnull L0
iload 1
aload 2
iconst_0
baload
if_icmpne L0
iload 8
ifeq L0
aload 5
iconst_0
baload
ifne L1
aload 4
iconst_0
iload 1
bastore
L1:
iconst_1
istore 9
getstatic com/chelpus/root/utils/corepatch/fileBytes Ljava/nio/MappedByteBuffer;
iload 0
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
getstatic com/chelpus/root/utils/corepatch/fileBytes Ljava/nio/MappedByteBuffer;
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
L2:
iload 1
aload 2
iload 9
baload
if_icmpeq L3
aload 3
iload 9
baload
iconst_1
if_icmpne L4
L3:
aload 5
iload 9
baload
ifne L5
aload 4
iload 9
iload 1
bastore
L5:
aload 5
iload 9
baload
iconst_3
if_icmpne L6
aload 4
iload 9
iload 1
bipush 15
iand
i2b
bastore
L6:
aload 5
iload 9
baload
iconst_2
if_icmpne L7
aload 4
iload 9
iload 1
bipush 15
iand
iload 1
bipush 15
iand
bipush 16
imul
iadd
i2b
bastore
L7:
iload 9
iconst_1
iadd
istore 9
iload 9
aload 2
arraylength
if_icmpne L8
iload 6
iload 7
if_icmplt L9
iload 0
putstatic com/chelpus/root/utils/corepatch/lastPatchPosition I
aload 4
arraylength
newarray byte
putstatic com/chelpus/root/utils/corepatch/lastByteReplace [B
aload 4
iconst_0
getstatic com/chelpus/root/utils/corepatch/lastByteReplace [B
iconst_0
aload 4
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L9:
iconst_1
ireturn
L8:
getstatic com/chelpus/root/utils/corepatch/fileBytes Ljava/nio/MappedByteBuffer;
invokevirtual java/nio/MappedByteBuffer/get()B
istore 1
goto L2
L4:
getstatic com/chelpus/root/utils/corepatch/fileBytes Ljava/nio/MappedByteBuffer;
iload 0
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L0:
iconst_0
ireturn
.limit locals 10
.limit stack 5
.end method

.method public static main([Ljava/lang/String;)V
new java/lang/RuntimeException
dup
ldc "d2j fail translate: java.lang.RuntimeException: Method code too large!\n\u0009at org.objectweb.asm.MethodWriter.getSize(MethodWriter.java:1872)\n\u0009at org.objectweb.asm.AsmBridge.sizeOfMethodWriter(AsmBridge.java:28)\n\u0009at com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:55)\n\u0009at com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\u0009at com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\u0009at com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\u0009at com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\u0009at com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\u0009at com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\u0009at com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\u0009at com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\u0009at com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n"
invokespecial java/lang/Runtime/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method public static unzip(Ljava/io/File;Ljava/lang/String;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L4 to L5 using L2
.catch java/lang/Exception from L6 to L7 using L2
.catch net/lingala/zip4j/exception/ZipException from L8 to L9 using L10
.catch java/lang/Exception from L8 to L9 using L11
.catch java/lang/Exception from L12 to L13 using L2
.catch java/lang/Exception from L14 to L15 using L2
.catch java/lang/Exception from L16 to L17 using L2
iconst_0
istore 2
L0:
new java/io/FileInputStream
dup
aload 0
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
astore 4
new java/util/zip/ZipInputStream
dup
aload 4
invokespecial java/util/zip/ZipInputStream/<init>(Ljava/io/InputStream;)V
astore 5
aload 5
invokevirtual java/util/zip/ZipInputStream/getNextEntry()Ljava/util/zip/ZipEntry;
astore 3
L1:
aload 3
ifnull L14
iconst_1
ifeq L14
L3:
aload 3
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
ldc "classes.dex"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L18
new java/io/FileOutputStream
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "classes.dex"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
astore 3
sipush 2048
newarray byte
astore 6
L4:
aload 5
aload 6
invokevirtual java/util/zip/ZipInputStream/read([B)I
istore 2
L5:
iload 2
iconst_m1
if_icmpeq L12
L6:
aload 3
aload 6
iconst_0
iload 2
invokevirtual java/io/FileOutputStream/write([BII)V
L7:
goto L4
L2:
astore 3
L8:
new net/lingala/zip4j/core/ZipFile
dup
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/<init>(Ljava/io/File;)V
ldc "classes.dex"
aload 1
invokevirtual net/lingala/zip4j/core/ZipFile/extractFile(Ljava/lang/String;Ljava/lang/String;)V
L9:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception e"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
return
L12:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "classes.dex"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
ldc "0.0"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "classes.dex"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
ldc "0:0"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "classes.dex"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
aload 5
invokevirtual java/util/zip/ZipInputStream/closeEntry()V
aload 3
invokevirtual java/io/FileOutputStream/close()V
L13:
iconst_1
istore 2
goto L18
L14:
aload 5
invokevirtual java/util/zip/ZipInputStream/close()V
aload 4
invokevirtual java/io/FileInputStream/close()V
L15:
return
L16:
aload 5
invokevirtual java/util/zip/ZipInputStream/getNextEntry()Ljava/util/zip/ZipEntry;
astore 3
L17:
goto L1
L10:
astore 0
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Error classes.dex decompress! "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception e1"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
goto L9
L11:
astore 0
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Error classes.dex decompress! "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception e1"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
goto L9
L18:
iload 2
ifeq L16
goto L14
.limit locals 7
.limit stack 5
.end method
