.bytecode 50.0
.class public synchronized com/chelpus/root/utils/cloneApp
.super java/lang/Object
.inner class static final inner com/chelpus/root/utils/cloneApp$1

.field public static 'appdir' Ljava/lang/String;

.field public static 'classesFiles' Ljava/util/ArrayList; signature "Ljava/util/ArrayList<Ljava/io/File;>;"

.field public static 'crkapk' Ljava/io/File;

.field public static 'dir' Ljava/lang/String;

.field public static 'dir2' Ljava/lang/String;

.field public static 'dirapp' Ljava/lang/String;

.field public static 'filestopatch' Ljava/util/ArrayList; signature "Ljava/util/ArrayList<Ljava/io/File;>;"

.field public static 'print' Ljava/io/PrintStream;

.field public static 'result' Ljava/lang/String;

.field public static 'sddir' Ljava/lang/String;

.field public static 'system' Z

.method static <clinit>()V
ldc "/data/app/"
putstatic com/chelpus/root/utils/cloneApp/dirapp Ljava/lang/String;
iconst_0
putstatic com/chelpus/root/utils/cloneApp/system Z
ldc "/sdcard/"
putstatic com/chelpus/root/utils/cloneApp/dir Ljava/lang/String;
ldc "/sdcard/"
putstatic com/chelpus/root/utils/cloneApp/dir2 Ljava/lang/String;
aconst_null
putstatic com/chelpus/root/utils/cloneApp/filestopatch Ljava/util/ArrayList;
ldc "/sdcard/"
putstatic com/chelpus/root/utils/cloneApp/sddir Ljava/lang/String;
ldc "/sdcard/"
putstatic com/chelpus/root/utils/cloneApp/appdir Ljava/lang/String;
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putstatic com/chelpus/root/utils/cloneApp/classesFiles Ljava/util/ArrayList;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static clearTempSD()V
.catch java/lang/Exception from L0 to L1 using L2
L0:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/cloneApp/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/classes.dex.apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
aload 0
invokevirtual java/io/File/exists()Z
ifeq L1
aload 0
invokevirtual java/io/File/delete()Z
pop
L1:
return
L2:
astore 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
return
.limit locals 1
.limit stack 4
.end method

.method public static main([Ljava/lang/String;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/NullPointerException from L5 to L6 using L7
.catch java/lang/Exception from L5 to L6 using L8
.catch java/lang/Exception from L9 to L10 using L11
.catch java/lang/Exception from L12 to L13 using L11
.catch java/lang/Exception from L14 to L11 using L11
.catch java/lang/Exception from L15 to L16 using L17
.catch java/lang/Exception from L16 to L18 using L11
.catch java/lang/Exception from L18 to L17 using L11
.catch java/lang/Exception from L19 to L20 using L11
.catch java/lang/Exception from L21 to L22 using L11
.catch java/lang/Exception from L22 to L23 using L11
.catch java/lang/Exception from L23 to L24 using L11
.catch java/lang/Exception from L25 to L26 using L11
.catch java/lang/Exception from L26 to L27 using L11
new com/android/vending/billing/InAppBillingService/LUCK/LogOutputStream
dup
ldc "System.out"
invokespecial com/android/vending/billing/InAppBillingService/LUCK/LogOutputStream/<init>(Ljava/lang/String;)V
astore 3
new java/io/PrintStream
dup
aload 3
invokespecial java/io/PrintStream/<init>(Ljava/io/OutputStream;)V
putstatic com/chelpus/root/utils/cloneApp/print Ljava/io/PrintStream;
new com/chelpus/root/utils/cloneApp$1
dup
invokespecial com/chelpus/root/utils/cloneApp$1/<init>()V
invokestatic com/chelpus/Utils/startRootJava(Ljava/lang/Object;)V
aload 0
iconst_0
aaload
invokestatic com/chelpus/Utils/kill(Ljava/lang/String;)V
getstatic com/chelpus/root/utils/cloneApp/print Ljava/io/PrintStream;
ldc "Support-Code Running!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
pop
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putstatic com/chelpus/root/utils/cloneApp/filestopatch Ljava/util/ArrayList;
L0:
new java/io/File
dup
aload 0
iconst_2
aaload
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/listFiles()[Ljava/io/File;
astore 4
aload 4
arraylength
istore 2
L1:
iconst_0
istore 1
L28:
iload 1
iload 2
if_icmpge L29
aload 4
iload 1
aaload
astore 5
L3:
aload 5
invokevirtual java/io/File/isFile()Z
ifeq L4
aload 5
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc "busybox"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L4
aload 5
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc "reboot"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L4
aload 5
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc "dalvikvm"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L4
aload 5
invokevirtual java/io/File/delete()Z
pop
L4:
iload 1
iconst_1
iadd
istore 1
goto L28
L2:
astore 4
aload 4
invokevirtual java/lang/Exception/printStackTrace()V
L29:
aload 0
bipush 6
aaload
ifnull L6
L5:
aload 0
bipush 6
aaload
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
L6:
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
aload 0
iconst_0
aaload
astore 4
L9:
aload 0
iconst_1
aaload
putstatic com/chelpus/root/utils/cloneApp/appdir Ljava/lang/String;
aload 0
iconst_3
aaload
putstatic com/chelpus/root/utils/cloneApp/sddir Ljava/lang/String;
invokestatic com/chelpus/root/utils/cloneApp/clearTempSD()V
new java/io/File
dup
getstatic com/chelpus/root/utils/cloneApp/appdir Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
aload 0
invokestatic com/chelpus/root/utils/cloneApp/unzipSD(Ljava/io/File;)V
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/cloneApp/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
putstatic com/chelpus/root/utils/cloneApp/crkapk Ljava/io/File;
aload 0
getstatic com/chelpus/root/utils/cloneApp/crkapk Ljava/io/File;
invokestatic com/chelpus/Utils/copyFile(Ljava/io/File;Ljava/io/File;)V
aload 4
invokevirtual java/lang/String/getBytes()[B
astore 0
L10:
iconst_0
istore 1
L12:
iload 1
aload 0
arraylength
if_icmpge L14
iload 1
aload 0
arraylength
iconst_1
isub
if_icmpne L30
L13:
aload 0
iload 1
aload 0
iload 1
baload
iconst_1
iadd
i2b
bastore
goto L30
L14:
new java/lang/String
dup
aload 0
invokespecial java/lang/String/<init>([B)V
astore 0
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/cloneApp/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/AndroidManifest.xml"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 5
aload 5
invokevirtual java/io/File/exists()Z
ifne L15
new java/io/FileNotFoundException
dup
invokespecial java/io/FileNotFoundException/<init>()V
athrow
L11:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L31:
ldc "Optional Steps After Patch:"
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
aload 3
getfield com/android/vending/billing/InAppBillingService/LUCK/LogOutputStream/allresult Ljava/lang/String;
putstatic com/chelpus/root/utils/cloneApp/result Ljava/lang/String;
return
L15:
new com/android/vending/billing/InAppBillingService/LUCK/AxmlExample
dup
invokespecial com/android/vending/billing/InAppBillingService/LUCK/AxmlExample/<init>()V
aload 5
aload 4
aload 0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/AxmlExample/changePackageName(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Z
ifne L16
aload 5
invokevirtual java/io/File/delete()Z
pop
L16:
getstatic com/chelpus/root/utils/cloneApp/classesFiles Ljava/util/ArrayList;
ifnull L18
getstatic com/chelpus/root/utils/cloneApp/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifne L21
L18:
new java/io/FileNotFoundException
dup
invokespecial java/io/FileNotFoundException/<init>()V
athrow
L17:
astore 0
L19:
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L20:
goto L16
L21:
getstatic com/chelpus/root/utils/cloneApp/filestopatch Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
getstatic com/chelpus/root/utils/cloneApp/classesFiles Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 0
L22:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L25
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
astore 4
aload 4
invokevirtual java/io/File/exists()Z
ifne L23
new java/io/FileNotFoundException
dup
invokespecial java/io/FileNotFoundException/<init>()V
athrow
L23:
getstatic com/chelpus/root/utils/cloneApp/filestopatch Ljava/util/ArrayList;
aload 4
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L24:
goto L22
L25:
getstatic com/chelpus/root/utils/cloneApp/filestopatch Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 0
L26:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L31
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
astore 4
ldc "Find string id."
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
ldc "String analysis."
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
getstatic com/chelpus/root/utils/cloneApp/print Ljava/io/PrintStream;
ldc "String analysis."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
ldc "Analise Results:"
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
aload 4
invokestatic com/chelpus/Utils/fixadler(Ljava/io/File;)V
L27:
goto L26
L8:
astore 4
goto L6
L7:
astore 4
goto L6
L30:
iload 1
iconst_1
iadd
istore 1
goto L12
.limit locals 6
.limit stack 4
.end method

.method public static unzipSD(Ljava/io/File;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L2
.catch java/lang/Exception from L7 to L8 using L2
.catch java/lang/Exception from L9 to L10 using L2
.catch java/lang/Exception from L10 to L11 using L2
.catch java/lang/Exception from L12 to L13 using L2
.catch net/lingala/zip4j/exception/ZipException from L14 to L15 using L16
.catch java/lang/Exception from L14 to L15 using L17
.catch java/lang/Exception from L18 to L19 using L2
.catch java/lang/Exception from L20 to L21 using L2
.catch java/lang/Exception from L21 to L22 using L2
.catch java/lang/Exception from L23 to L24 using L2
.catch java/lang/Exception from L25 to L26 using L2
.catch java/lang/Exception from L27 to L28 using L2
L0:
new java/io/FileInputStream
dup
aload 0
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
astore 3
new java/util/zip/ZipInputStream
dup
aload 3
invokespecial java/util/zip/ZipInputStream/<init>(Ljava/io/InputStream;)V
astore 4
L1:
iconst_0
istore 2
L3:
aload 4
invokevirtual java/util/zip/ZipInputStream/getNextEntry()Ljava/util/zip/ZipEntry;
astore 5
L4:
aload 5
ifnull L27
iload 2
istore 1
L5:
aload 5
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
ldc "classes"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L29
L6:
iload 2
istore 1
L7:
aload 5
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
ldc ".dex"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L29
L8:
iload 2
istore 1
L9:
aload 5
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
ldc "/"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L29
new java/io/FileOutputStream
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/cloneApp/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
astore 6
sipush 1024
newarray byte
astore 7
L10:
aload 4
aload 7
invokevirtual java/util/zip/ZipInputStream/read([B)I
istore 1
L11:
iload 1
iconst_m1
if_icmpeq L18
L12:
aload 6
aload 7
iconst_0
iload 1
invokevirtual java/io/FileOutputStream/write([BII)V
L13:
goto L10
L2:
astore 3
L14:
new net/lingala/zip4j/core/ZipFile
dup
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/<init>(Ljava/io/File;)V
astore 0
aload 0
ldc "classes.dex"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/cloneApp/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual net/lingala/zip4j/core/ZipFile/extractFile(Ljava/lang/String;Ljava/lang/String;)V
getstatic com/chelpus/root/utils/cloneApp/classesFiles Ljava/util/ArrayList;
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/cloneApp/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "classes.dex"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
ldc "AndroidManifest.xml"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/cloneApp/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual net/lingala/zip4j/core/ZipFile/extractFile(Ljava/lang/String;Ljava/lang/String;)V
L15:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Decompressunzip "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
return
L18:
getstatic com/chelpus/root/utils/cloneApp/classesFiles Ljava/util/ArrayList;
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/cloneApp/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L19:
iconst_1
istore 1
L29:
iload 1
istore 2
L20:
aload 5
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
ldc "AndroidManifest.xml"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L3
new java/io/FileOutputStream
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/cloneApp/sddir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "AndroidManifest.xml"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
astore 5
sipush 1024
newarray byte
astore 6
L21:
aload 4
aload 6
invokevirtual java/util/zip/ZipInputStream/read([B)I
istore 2
L22:
iload 2
iconst_m1
if_icmpeq L30
L23:
aload 5
aload 6
iconst_0
iload 2
invokevirtual java/io/FileOutputStream/write([BII)V
L24:
goto L21
L30:
iload 1
istore 2
iload 1
ifeq L3
L25:
aload 4
invokevirtual java/util/zip/ZipInputStream/closeEntry()V
aload 5
invokevirtual java/io/FileOutputStream/close()V
L26:
iload 1
istore 2
goto L3
L27:
aload 4
invokevirtual java/util/zip/ZipInputStream/close()V
aload 3
invokevirtual java/io/FileInputStream/close()V
L28:
return
L16:
astore 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Error classes.dex decompress! "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception e1"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
goto L15
L17:
astore 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Error classes.dex decompress! "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception e1"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
goto L15
.limit locals 8
.limit stack 5
.end method
