.bytecode 50.0
.class public synchronized com/chelpus/root/utils/copyFile
.super java/lang/Object
.inner class static final inner com/chelpus/root/utils/copyFile$1

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static main([Ljava/lang/String;)V
.catch java/lang/IllegalArgumentException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
new com/chelpus/root/utils/copyFile$1
dup
invokespecial com/chelpus/root/utils/copyFile$1/<init>()V
invokestatic com/chelpus/Utils/startRootJava(Ljava/lang/Object;)V
new java/io/File
dup
aload 0
iconst_0
aaload
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
new java/io/File
dup
aload 0
iconst_1
aaload
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 1
invokevirtual java/io/PrintStream/println(Ljava/lang/Object;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
invokevirtual java/io/PrintStream/println(Ljava/lang/Object;)V
aload 1
invokevirtual java/io/File/exists()Z
ifeq L4
L0:
aload 1
aload 0
invokestatic com/chelpus/Utils/copyFile(Ljava/io/File;Ljava/io/File;)V
L1:
aload 1
invokevirtual java/io/File/length()J
aload 0
invokevirtual java/io/File/length()J
lcmp
ifeq L5
aload 1
invokevirtual java/io/File/length()J
lconst_0
lcmp
ifeq L5
aload 0
invokevirtual java/io/File/delete()Z
pop
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Length of Files not equals. Destination deleted!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L6:
invokestatic com/chelpus/Utils/exitFromRootJava()V
return
L2:
astore 2
aload 2
invokevirtual java/lang/IllegalArgumentException/printStackTrace()V
goto L1
L3:
astore 2
aload 2
invokevirtual java/lang/Exception/printStackTrace()V
goto L1
L5:
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
invokevirtual java/io/File/length()J
invokevirtual java/io/PrintStream/println(J)V
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "File copied!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
goto L6
L4:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Source File not Found!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
goto L6
.limit locals 3
.limit stack 4
.end method
