.bytecode 50.0
.class public synchronized com/chelpus/root/utils/move_to_system
.super java/lang/Object
.inner class static final inner com/chelpus/root/utils/move_to_system$1

.field public static 'datadir' Ljava/lang/String;

.field public static 'dirapp' Ljava/lang/String;

.field public static 'index' Ljava/lang/String;

.field public static 'libDir' Ljava/lang/String;

.field public static 'pkgName' Ljava/lang/String;

.field public static 'toolsfiles' Ljava/lang/String;

.method static <clinit>()V
ldc "/data/app/"
putstatic com/chelpus/root/utils/move_to_system/dirapp Ljava/lang/String;
ldc "/data/data/"
putstatic com/chelpus/root/utils/move_to_system/datadir Ljava/lang/String;
ldc ""
putstatic com/chelpus/root/utils/move_to_system/toolsfiles Ljava/lang/String;
ldc ""
putstatic com/chelpus/root/utils/move_to_system/pkgName Ljava/lang/String;
ldc ""
putstatic com/chelpus/root/utils/move_to_system/libDir Ljava/lang/String;
ldc ""
putstatic com/chelpus/root/utils/move_to_system/index Ljava/lang/String;
return
.limit locals 0
.limit stack 1
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static getLibs(Ljava/io/File;Ljava/lang/String;I)Ljava/util/ArrayList;
.signature "(Ljava/io/File;Ljava/lang/String;I)Ljava/util/ArrayList<Ljava/lang/String;>;"
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 8
aload 1
ldc "/mnt/"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifne L0
iload 2
bipush 21
if_icmplt L1
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
astore 0
aload 0
invokevirtual java/io/File/exists()Z
ifeq L2
aload 0
invokevirtual java/io/File/listFiles()[Ljava/io/File;
astore 1
aload 1
arraylength
istore 5
iconst_0
istore 2
L3:
iload 2
iload 5
if_icmpge L2
aload 1
iload 2
aaload
astore 9
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher: file found in data dir - "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 9
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 9
invokevirtual java/io/File/isDirectory()Z
ifeq L4
aload 9
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "lib"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L4
aload 9
invokevirtual java/io/File/listFiles()[Ljava/io/File;
astore 9
aload 9
ifnull L4
aload 9
arraylength
ifle L4
aload 9
arraylength
istore 6
iconst_0
istore 3
L5:
iload 3
iload 6
if_icmpge L4
aload 9
iload 3
aaload
invokevirtual java/io/File/listFiles()[Ljava/io/File;
astore 10
aload 10
ifnull L6
aload 10
arraylength
ifle L6
aload 10
arraylength
istore 7
iconst_0
istore 4
L7:
iload 4
iload 7
if_icmpge L6
aload 10
iload 4
aaload
astore 11
aload 8
aload 11
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 11
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aload 0
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
putstatic com/chelpus/root/utils/move_to_system/index Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/system/priv-app/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/move_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/move_to_system/index Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
putstatic com/chelpus/root/utils/move_to_system/libDir Ljava/lang/String;
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "libdir"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/move_to_system/libDir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/io/File
dup
getstatic com/chelpus/root/utils/move_to_system/libDir Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/mkdirs()Z
pop
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher: found lib - "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 11
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iload 4
iconst_1
iadd
istore 4
goto L7
L6:
iload 3
iconst_1
iadd
istore 3
goto L5
L4:
iload 2
iconst_1
iadd
istore 2
goto L3
L1:
aload 0
invokevirtual java/io/File/exists()Z
ifeq L2
aload 0
invokevirtual java/io/File/list()[Ljava/lang/String;
astore 1
aload 1
arraylength
istore 4
iconst_0
istore 2
L8:
iload 2
iload 4
if_icmpge L2
aload 1
iload 2
aaload
astore 9
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher: file found in data dir - "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 9
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 9
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/isDirectory()Z
ifeq L9
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 9
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "lib"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L9
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 9
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/list()[Ljava/lang/String;
astore 10
aload 10
arraylength
istore 5
iconst_0
istore 3
L10:
iload 3
iload 5
if_icmpge L9
aload 10
iload 3
aaload
astore 11
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 9
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 11
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/isFile()Z
ifeq L11
aload 11
ldc "libjnigraphics.so"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L11
aload 8
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 9
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 11
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher: found lib - "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 9
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 11
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L11:
iload 3
iconst_1
iadd
istore 3
goto L10
L9:
iload 2
iconst_1
iadd
istore 2
goto L8
L0:
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
astore 0
aload 0
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
ldc "rw"
invokestatic com/chelpus/Utils/remount(Ljava/lang/String;Ljava/lang/String;)Z
pop
aload 0
invokevirtual java/io/File/exists()Z
ifeq L2
aload 0
invokevirtual java/io/File/list()[Ljava/lang/String;
astore 1
aload 1
arraylength
istore 4
iconst_0
istore 2
L12:
iload 2
iload 4
if_icmpge L2
aload 1
iload 2
aaload
astore 9
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher: file found in data dir - "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 9
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 9
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/isDirectory()Z
ifeq L13
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 9
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "lib"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L13
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 9
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/list()[Ljava/lang/String;
astore 10
aload 10
arraylength
istore 5
iconst_0
istore 3
L14:
iload 3
iload 5
if_icmpge L13
aload 10
iload 3
aaload
astore 11
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 9
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 11
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/isFile()Z
ifeq L15
aload 11
ldc "libjnigraphics.so"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L15
aload 8
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 9
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 11
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher: found lib - "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 9
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 11
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L15:
iload 3
iconst_1
iadd
istore 3
goto L14
L13:
iload 2
iconst_1
iadd
istore 2
goto L12
L2:
aload 8
areturn
.limit locals 12
.limit stack 4
.end method

.method public static main([Ljava/lang/String;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L7
.catch java/lang/Exception from L6 to L8 using L7
.catch java/lang/Exception from L9 to L10 using L7
.catch java/lang/Exception from L11 to L12 using L7
.catch java/lang/Exception from L13 to L14 using L7
.catch java/lang/Exception from L15 to L16 using L7
.catch java/lang/Exception from L16 to L17 using L7
.catch java/lang/Exception from L18 to L19 using L7
.catch java/lang/Exception from L19 to L20 using L7
.catch java/lang/Exception from L21 to L22 using L7
.catch java/lang/Exception from L23 to L24 using L7
.catch java/lang/Exception from L25 to L26 using L27
.catch java/lang/Exception from L26 to L28 using L27
.catch java/lang/Exception from L28 to L29 using L27
.catch java/lang/Exception from L29 to L30 using L27
.catch java/lang/Exception from L30 to L31 using L27
.catch java/lang/Exception from L31 to L32 using L27
.catch java/lang/Exception from L32 to L33 using L27
.catch java/lang/Exception from L33 to L34 using L27
.catch java/lang/Exception from L34 to L35 using L27
.catch java/lang/Exception from L35 to L36 using L27
.catch java/lang/Exception from L36 to L37 using L27
.catch java/lang/Exception from L37 to L38 using L7
.catch java/lang/Exception from L38 to L39 using L40
.catch java/lang/Exception from L39 to L41 using L7
.catch java/lang/Exception from L42 to L43 using L7
.catch java/lang/Exception from L44 to L45 using L7
.catch java/lang/Exception from L46 to L47 using L7
.catch java/lang/Exception from L48 to L49 using L7
.catch java/lang/Exception from L49 to L50 using L7
.catch java/lang/Exception from L51 to L52 using L7
iconst_0
istore 2
ldc "/system/app/"
astore 4
aload 4
astore 3
L0:
new java/io/File
dup
ldc "/system/priv-app"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L5
L1:
aload 4
astore 3
L3:
new java/io/File
dup
ldc "/system/priv-app"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/list()[Ljava/lang/String;
ifnull L5
L4:
ldc "/system/priv-app/"
astore 3
L5:
new com/chelpus/root/utils/move_to_system$1
dup
invokespecial com/chelpus/root/utils/move_to_system$1/<init>()V
invokestatic com/chelpus/Utils/startRootJava(Ljava/lang/Object;)V
aload 0
iconst_1
aaload
putstatic com/chelpus/root/utils/move_to_system/dirapp Ljava/lang/String;
aload 0
iconst_2
aaload
putstatic com/chelpus/root/utils/move_to_system/datadir Ljava/lang/String;
aload 0
iconst_3
aaload
putstatic com/chelpus/root/utils/move_to_system/toolsfiles Ljava/lang/String;
aload 0
iconst_0
aaload
putstatic com/chelpus/root/utils/move_to_system/pkgName Ljava/lang/String;
new java/io/File
dup
getstatic com/chelpus/root/utils/move_to_system/dirapp Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 4
L6:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/api I
bipush 21
if_icmplt L11
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/system/priv-app/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/move_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
L8:
aload 0
astore 3
L9:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifne L11
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/mkdirs()Z
pop
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "755"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/system/priv-app/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/move_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L10:
aload 0
astore 3
L11:
ldc "/system/lib/"
putstatic com/chelpus/root/utils/move_to_system/libDir Ljava/lang/String;
new java/io/File
dup
getstatic com/chelpus/root/utils/move_to_system/datadir Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
getstatic com/chelpus/root/utils/move_to_system/dirapp Ljava/lang/String;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/api I
invokestatic com/chelpus/root/utils/move_to_system/getLibs(Ljava/io/File;Ljava/lang/String;I)Ljava/util/ArrayList;
astore 0
L12:
iload 2
istore 1
L13:
aload 0
invokevirtual java/util/ArrayList/size()I
ifle L53
aload 0
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 5
L14:
iload 2
istore 1
L15:
aload 5
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L53
new java/io/File
dup
aload 5
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 6
new java/io/File
dup
getstatic com/chelpus/root/utils/move_to_system/libDir Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifne L16
new java/io/File
dup
getstatic com/chelpus/root/utils/move_to_system/libDir Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/mkdirs()Z
pop
L16:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/system/priv-app/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/move_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/system/priv-app/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/move_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/move_to_system/index Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
ldc "755"
invokestatic com/chelpus/Utils/setPermissionDir(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
aload 6
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/move_to_system/libDir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokestatic com/chelpus/Utils/copyFile(Ljava/io/File;Ljava/io/File;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chmod 0644 "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/move_to_system/libDir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/root/utils/move_to_system/run_all(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chown 0.0 "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/move_to_system/libDir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/root/utils/move_to_system/run_all(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chown 0:0 "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/move_to_system/libDir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/root/utils/move_to_system/run_all(Ljava/lang/String;)V
aload 6
invokevirtual java/io/File/length()J
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/move_to_system/libDir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
lcmp
ifne L18
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher: copy lib "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/move_to_system/libDir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L17:
goto L14
L7:
astore 0
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher Error move to System: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L54:
invokestatic com/chelpus/Utils/exitFromRootJava()V
return
L2:
astore 3
aload 3
invokevirtual java/lang/Exception/printStackTrace()V
aload 4
astore 3
goto L5
L18:
aload 0
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 5
L19:
aload 5
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L23
new java/io/File
dup
aload 5
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 6
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/api I
bipush 21
if_icmpge L21
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/move_to_system/libDir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L20:
goto L19
L21:
getstatic com/chelpus/root/utils/move_to_system/libDir Ljava/lang/String;
ldc "/system/priv-app/"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L19
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
new java/io/File
dup
getstatic com/chelpus/root/utils/move_to_system/libDir Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual com/chelpus/Utils/deleteFolder(Ljava/io/File;)V
L22:
goto L19
L23:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "In /system space not found!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L24:
iconst_1
istore 1
L53:
iload 1
ifne L51
L25:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/move_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".odex"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L26
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/move_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".odex"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L26:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/move_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 5
getstatic com/chelpus/root/utils/move_to_system/dirapp Ljava/lang/String;
astore 6
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "dd"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "if="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "of="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
new java/io/File
dup
aload 5
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L28
aload 4
invokevirtual java/io/File/length()J
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/move_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
lcmp
ifeq L29
L28:
iconst_4
anewarray java/lang/String
dup
iconst_0
ldc "toolbox"
aastore
dup
iconst_1
ldc "dd"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "if="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "of="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L29:
new java/io/File
dup
aload 5
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L30
aload 4
invokevirtual java/io/File/length()J
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/move_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
lcmp
ifeq L31
L30:
iconst_4
anewarray java/lang/String
dup
iconst_0
ldc "busybox"
aastore
dup
iconst_1
ldc "dd"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "if="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "of="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L31:
new java/io/File
dup
aload 5
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L32
aload 4
invokevirtual java/io/File/length()J
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/move_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
lcmp
ifeq L33
L32:
iconst_4
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/move_to_system/toolsfiles Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/busybox"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_1
ldc "dd"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "if="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "of="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L33:
new java/io/File
dup
aload 5
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L34
aload 4
invokevirtual java/io/File/length()J
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/move_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
lcmp
ifeq L35
L34:
iconst_5
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/move_to_system/toolsfiles Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/busybox"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_1
ldc "cp"
aastore
dup
iconst_2
ldc "-fp"
aastore
dup
iconst_3
aload 6
aastore
dup
iconst_4
aload 5
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L35:
new java/io/File
dup
aload 5
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L36
aload 4
invokevirtual java/io/File/length()J
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/move_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
lcmp
ifeq L37
L36:
aload 4
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/move_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokestatic com/chelpus/Utils/copyFile(Ljava/io/File;Ljava/io/File;)V
L37:
aload 4
invokevirtual java/io/File/length()J
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/move_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
lcmp
ifne L48
getstatic com/chelpus/root/utils/move_to_system/dirapp Ljava/lang/String;
ldc "/mnt/"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifne L46
aload 4
invokevirtual java/io/File/delete()Z
pop
L38:
new java/io/File
dup
getstatic com/chelpus/root/utils/move_to_system/datadir Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
getstatic com/chelpus/root/utils/move_to_system/dirapp Ljava/lang/String;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/api I
invokestatic com/chelpus/root/utils/move_to_system/removeLibs(Ljava/io/File;Ljava/lang/String;I)V
L39:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chmod 0644 "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/move_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/root/utils/move_to_system/run_all(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chown 0.0 "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/move_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/root/utils/move_to_system/run_all(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chown 0:0 "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/move_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/root/utils/move_to_system/run_all(Ljava/lang/String;)V
L41:
goto L54
L27:
astore 5
L42:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "In /system space not found!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/move_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
aload 5
invokevirtual java/lang/Exception/printStackTrace()V
L43:
goto L37
L40:
astore 0
L44:
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L45:
goto L39
L46:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "pm"
aastore
dup
iconst_1
ldc "uninstall"
aastore
dup
iconst_2
getstatic com/chelpus/root/utils/move_to_system/pkgName Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L47:
goto L39
L48:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/move_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "In /system space not found!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 0
L49:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L54
new java/io/File
dup
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 3
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/move_to_system/libDir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L50:
goto L49
L51:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/move_to_system/pkgName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L52:
goto L54
.limit locals 7
.limit stack 6
.end method

.method public static removeLibs(Ljava/io/File;Ljava/lang/String;I)V
.catch java/io/IOException from L0 to L1 using L2
iconst_0
istore 3
iload 2
bipush 21
if_icmpge L3
aload 1
ldc "/mnt/"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifne L4
aload 0
invokevirtual java/io/File/exists()Z
ifeq L4
aload 0
invokevirtual java/io/File/list()[Ljava/lang/String;
astore 1
aload 1
arraylength
istore 4
iconst_0
istore 2
L5:
iload 2
iload 4
if_icmpge L4
aload 1
iload 2
aaload
astore 6
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher: file found in data dir - "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/isDirectory()Z
ifeq L6
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "lib"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L6
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/list()[Ljava/lang/String;
astore 7
aload 7
ifnull L7
aload 7
arraylength
ifle L7
aload 7
arraylength
istore 5
iconst_0
istore 3
L8:
iload 3
iload 5
if_icmpge L7
aload 7
iload 3
aaload
astore 8
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 8
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/isFile()Z
ifeq L9
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 8
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher: remove lib - "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 8
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L9:
iload 3
iconst_1
iadd
istore 3
goto L8
L7:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L6:
iload 2
iconst_1
iadd
istore 2
goto L5
L3:
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
astore 0
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/io/File/exists()Z
ifeq L4
aload 0
invokevirtual java/io/File/listFiles()[Ljava/io/File;
astore 1
aload 1
arraylength
istore 4
iload 3
istore 2
L10:
iload 2
iload 4
if_icmpge L4
aload 1
iload 2
aaload
astore 6
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher: file found in data dir - "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 6
invokevirtual java/io/File/isDirectory()Z
ifeq L1
aload 6
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
ldc "/lib"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L1
L0:
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
aload 6
invokevirtual com/chelpus/Utils/deleteFolder(Ljava/io/File;)V
L1:
iload 2
iconst_1
iadd
istore 2
goto L10
L2:
astore 6
aload 6
invokevirtual java/io/IOException/printStackTrace()V
goto L1
L4:
return
.limit locals 9
.limit stack 4
.end method

.method private static run_all(Ljava/lang/String;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L4
.catch java/lang/Exception from L3 to L5 using L6
.catch java/lang/Exception from L5 to L7 using L8
.catch java/lang/Exception from L7 to L9 using L10
L0:
invokestatic java/lang/Runtime/getRuntime()Ljava/lang/Runtime;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/Runtime/exec(Ljava/lang/String;)Ljava/lang/Process;
astore 1
aload 1
invokevirtual java/lang/Process/waitFor()I
pop
aload 1
invokevirtual java/lang/Process/destroy()V
L1:
invokestatic java/lang/Runtime/getRuntime()Ljava/lang/Runtime;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "toolbox "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/Runtime/exec(Ljava/lang/String;)Ljava/lang/Process;
astore 1
aload 1
invokevirtual java/lang/Process/waitFor()I
pop
aload 1
invokevirtual java/lang/Process/destroy()V
L3:
invokestatic java/lang/Runtime/getRuntime()Ljava/lang/Runtime;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/system/bin/failsafe/toolbox "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/Runtime/exec(Ljava/lang/String;)Ljava/lang/Process;
astore 1
aload 1
invokevirtual java/lang/Process/waitFor()I
pop
aload 1
invokevirtual java/lang/Process/destroy()V
L5:
invokestatic java/lang/Runtime/getRuntime()Ljava/lang/Runtime;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "busybox "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/Runtime/exec(Ljava/lang/String;)Ljava/lang/Process;
astore 1
aload 1
invokevirtual java/lang/Process/waitFor()I
pop
aload 1
invokevirtual java/lang/Process/destroy()V
L7:
invokestatic java/lang/Runtime/getRuntime()Ljava/lang/Runtime;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/move_to_system/toolsfiles Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/busybox "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/Runtime/exec(Ljava/lang/String;)Ljava/lang/Process;
astore 0
aload 0
invokevirtual java/lang/Process/waitFor()I
pop
aload 0
invokevirtual java/lang/Process/destroy()V
L9:
return
L10:
astore 0
return
L8:
astore 1
goto L7
L6:
astore 1
goto L5
L4:
astore 1
goto L3
L2:
astore 1
goto L1
.limit locals 2
.limit stack 3
.end method
