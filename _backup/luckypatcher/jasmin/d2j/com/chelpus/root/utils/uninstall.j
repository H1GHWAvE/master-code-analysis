.bytecode 50.0
.class public synchronized com/chelpus/root/utils/uninstall
.super java/lang/Object
.inner class static final inner com/chelpus/root/utils/uninstall$1

.field public static 'datadir' Ljava/lang/String;

.field public static 'dirapp' Ljava/lang/String;

.field public static 'odexpatch' Z

.field public static 'system' Z

.method static <clinit>()V
ldc "/data/app/"
putstatic com/chelpus/root/utils/uninstall/dirapp Ljava/lang/String;
ldc "/data/data/"
putstatic com/chelpus/root/utils/uninstall/datadir Ljava/lang/String;
iconst_0
putstatic com/chelpus/root/utils/uninstall/system Z
iconst_0
putstatic com/chelpus/root/utils/uninstall/odexpatch Z
return
.limit locals 0
.limit stack 1
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static getLibs(Ljava/io/File;)Ljava/util/ArrayList;
.signature "(Ljava/io/File;)Ljava/util/ArrayList<Ljava/lang/String;>;"
.catch java/io/IOException from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/io/IOException from L1 to L4 using L5
.catch all from L1 to L4 using L6
.catch java/io/IOException from L4 to L7 using L8
.catch all from L4 to L7 using L9
.catch java/io/IOException from L10 to L11 using L8
.catch all from L10 to L11 using L9
.catch java/io/IOException from L11 to L12 using L8
.catch all from L11 to L12 using L9
.catch java/io/IOException from L12 to L13 using L8
.catch all from L12 to L13 using L9
.catch java/io/IOException from L14 to L15 using L16
.catch java/io/IOException from L17 to L18 using L19
.catch net/lingala/zip4j/exception/ZipException from L20 to L21 using L22
.catch java/lang/Exception from L20 to L21 using L23
.catch all from L20 to L21 using L3
.catch net/lingala/zip4j/exception/ZipException from L24 to L25 using L22
.catch java/lang/Exception from L24 to L25 using L23
.catch all from L24 to L25 using L3
.catch net/lingala/zip4j/exception/ZipException from L26 to L27 using L22
.catch java/lang/Exception from L26 to L27 using L23
.catch all from L26 to L27 using L3
.catch net/lingala/zip4j/exception/ZipException from L28 to L29 using L22
.catch java/lang/Exception from L28 to L29 using L23
.catch all from L28 to L29 using L3
.catch net/lingala/zip4j/exception/ZipException from L30 to L31 using L22
.catch java/lang/Exception from L30 to L31 using L23
.catch all from L30 to L31 using L3
.catch net/lingala/zip4j/exception/ZipException from L32 to L33 using L22
.catch java/lang/Exception from L32 to L33 using L23
.catch all from L32 to L33 using L3
.catch net/lingala/zip4j/exception/ZipException from L34 to L35 using L22
.catch java/lang/Exception from L34 to L35 using L23
.catch all from L34 to L35 using L3
.catch net/lingala/zip4j/exception/ZipException from L36 to L37 using L22
.catch java/lang/Exception from L36 to L37 using L23
.catch all from L36 to L37 using L3
.catch net/lingala/zip4j/exception/ZipException from L38 to L39 using L22
.catch java/lang/Exception from L38 to L39 using L23
.catch all from L38 to L39 using L3
.catch net/lingala/zip4j/exception/ZipException from L40 to L41 using L22
.catch java/lang/Exception from L40 to L41 using L23
.catch all from L40 to L41 using L3
.catch net/lingala/zip4j/exception/ZipException from L42 to L43 using L22
.catch java/lang/Exception from L42 to L43 using L23
.catch all from L42 to L43 using L3
.catch net/lingala/zip4j/exception/ZipException from L44 to L45 using L22
.catch java/lang/Exception from L44 to L45 using L23
.catch all from L44 to L45 using L3
.catch net/lingala/zip4j/exception/ZipException from L46 to L47 using L22
.catch java/lang/Exception from L46 to L47 using L23
.catch all from L46 to L47 using L3
.catch net/lingala/zip4j/exception/ZipException from L48 to L49 using L22
.catch java/lang/Exception from L48 to L49 using L23
.catch all from L48 to L49 using L3
.catch net/lingala/zip4j/exception/ZipException from L50 to L51 using L22
.catch java/lang/Exception from L50 to L51 using L23
.catch all from L50 to L51 using L3
.catch all from L52 to L53 using L3
.catch java/io/IOException from L54 to L55 using L56
.catch java/io/IOException from L57 to L58 using L59
.catch all from L60 to L61 using L3
.catch java/io/IOException from L62 to L63 using L64
.catch java/io/IOException from L65 to L66 using L67
aconst_null
astore 3
aconst_null
astore 5
aconst_null
astore 4
aconst_null
astore 2
aconst_null
astore 6
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 7
L0:
new java/io/FileInputStream
dup
aload 0
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
astore 1
L1:
new java/util/zip/ZipInputStream
dup
aload 1
invokespecial java/util/zip/ZipInputStream/<init>(Ljava/io/InputStream;)V
astore 2
L4:
aload 2
invokevirtual java/util/zip/ZipInputStream/getNextEntry()Ljava/util/zip/ZipEntry;
astore 3
L7:
aload 3
ifnull L68
L10:
aload 3
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
ldc "lib/"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L12
aload 3
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
ldc "/"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 3
aload 3
aload 3
arraylength
iconst_1
isub
aaload
astore 3
aload 7
invokevirtual java/util/ArrayList/isEmpty()Z
ifeq L11
aload 3
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L11
aload 3
ldc "libjnigraphics.so"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L11
aload 7
aload 3
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L11:
aload 7
aload 3
invokevirtual java/util/ArrayList/contains(Ljava/lang/Object;)Z
ifne L12
aload 3
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L12
aload 3
ldc "libjnigraphics.so"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L12
aload 7
aload 3
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L12:
aload 2
invokevirtual java/util/zip/ZipInputStream/getNextEntry()Ljava/util/zip/ZipEntry;
astore 3
L13:
goto L7
L68:
aload 2
ifnull L15
L14:
aload 2
invokevirtual java/util/zip/ZipInputStream/close()V
L15:
aload 1
ifnull L18
L17:
aload 1
invokevirtual java/io/FileInputStream/close()V
L18:
aload 7
areturn
L2:
astore 1
aload 6
astore 1
L69:
aload 1
astore 2
aload 4
astore 3
L20:
new net/lingala/zip4j/core/ZipFile
dup
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/<init>(Ljava/io/File;)V
invokevirtual net/lingala/zip4j/core/ZipFile/getFileHeaders()Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 0
L21:
aload 1
astore 2
aload 4
astore 3
L24:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L53
L25:
aload 1
astore 2
aload 4
astore 3
L26:
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast net/lingala/zip4j/model/FileHeader
astore 5
L27:
aload 1
astore 2
aload 4
astore 3
L28:
aload 5
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
ldc ".so"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L21
L29:
aload 1
astore 2
aload 4
astore 3
L30:
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 5
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L31:
aload 1
astore 2
aload 4
astore 3
L32:
aload 5
invokevirtual net/lingala/zip4j/model/FileHeader/getFileName()Ljava/lang/String;
ldc "/"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 5
L33:
aload 1
astore 2
aload 4
astore 3
L34:
aload 5
aload 5
arraylength
iconst_1
isub
aaload
astore 5
L35:
aload 1
astore 2
aload 4
astore 3
L36:
aload 7
invokevirtual java/util/ArrayList/isEmpty()Z
ifeq L43
L37:
aload 1
astore 2
aload 4
astore 3
L38:
aload 5
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L43
L39:
aload 1
astore 2
aload 4
astore 3
L40:
aload 5
ldc "libjnigraphics.so"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L43
L41:
aload 1
astore 2
aload 4
astore 3
L42:
aload 7
aload 5
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L43:
aload 1
astore 2
aload 4
astore 3
L44:
aload 7
aload 5
invokevirtual java/util/ArrayList/contains(Ljava/lang/Object;)Z
ifne L21
L45:
aload 1
astore 2
aload 4
astore 3
L46:
aload 5
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L21
L47:
aload 1
astore 2
aload 4
astore 3
L48:
aload 5
ldc "libjnigraphics.so"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L21
L49:
aload 1
astore 2
aload 4
astore 3
L50:
aload 7
aload 5
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L51:
goto L21
L22:
astore 0
aload 1
astore 2
aload 4
astore 3
L52:
aload 0
invokevirtual net/lingala/zip4j/exception/ZipException/printStackTrace()V
L53:
aload 4
ifnull L55
L54:
aload 4
invokevirtual java/util/zip/ZipInputStream/close()V
L55:
aload 1
ifnull L58
L57:
aload 1
invokevirtual java/io/FileInputStream/close()V
L58:
aload 7
areturn
L23:
astore 0
aload 1
astore 2
aload 4
astore 3
L60:
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L61:
goto L53
L3:
astore 0
L70:
aload 3
ifnull L63
L62:
aload 3
invokevirtual java/util/zip/ZipInputStream/close()V
L63:
aload 2
ifnull L66
L65:
aload 2
invokevirtual java/io/FileInputStream/close()V
L66:
aload 0
athrow
L16:
astore 0
goto L15
L19:
astore 0
goto L18
L56:
astore 0
goto L55
L59:
astore 0
goto L58
L64:
astore 1
goto L63
L67:
astore 1
goto L66
L6:
astore 0
aload 1
astore 2
aload 5
astore 3
goto L70
L9:
astore 0
aload 2
astore 3
aload 1
astore 2
goto L70
L5:
astore 2
goto L69
L8:
astore 3
aload 2
astore 4
goto L69
.limit locals 8
.limit stack 3
.end method

.method public static main([Ljava/lang/String;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
.catch java/lang/Exception from L4 to L5 using L2
.catch java/lang/Exception from L5 to L6 using L7
.catch java/lang/Exception from L6 to L8 using L2
.catch java/lang/Exception from L8 to L9 using L10
.catch java/lang/Exception from L11 to L12 using L10
.catch java/lang/Exception from L12 to L13 using L10
.catch java/lang/Exception from L13 to L14 using L2
.catch java/lang/Exception from L15 to L16 using L2
.catch java/lang/Exception from L17 to L18 using L10
.catch java/lang/Exception from L19 to L20 using L2
L0:
new com/chelpus/root/utils/uninstall$1
dup
invokespecial com/chelpus/root/utils/uninstall$1/<init>()V
invokestatic com/chelpus/Utils/startRootJava(Ljava/lang/Object;)V
aload 0
iconst_1
aaload
putstatic com/chelpus/root/utils/uninstall/dirapp Ljava/lang/String;
iconst_1
putstatic com/chelpus/root/utils/uninstall/system Z
aload 0
iconst_2
aaload
putstatic com/chelpus/root/utils/uninstall/datadir Ljava/lang/String;
new java/io/File
dup
getstatic com/chelpus/root/utils/uninstall/dirapp Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
new java/io/File
dup
getstatic com/chelpus/root/utils/uninstall/dirapp Ljava/lang/String;
iconst_1
invokestatic com/chelpus/Utils/getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Start getLibs!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 1
invokestatic com/chelpus/root/utils/uninstall/getLibs(Ljava/io/File;)Ljava/util/ArrayList;
astore 1
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Start delete lib!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 1
invokevirtual java/util/ArrayList/isEmpty()Z
ifne L4
aload 1
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 1
L1:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 2
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/system/lib/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 2
aload 2
invokevirtual java/io/File/exists()Z
ifeq L1
aload 2
invokevirtual java/io/File/delete()Z
pop
L3:
goto L1
L2:
astore 0
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher Error uninstall: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L21:
invokestatic com/chelpus/Utils/exitFromRootJava()V
return
L4:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Start delete data directory!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new com/chelpus/Utils
dup
ldc "uninstall system"
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
astore 1
L5:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Start delete dir!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 1
new java/io/File
dup
getstatic com/chelpus/root/utils/uninstall/datadir Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual com/chelpus/Utils/deleteFolder(Ljava/io/File;)V
aload 1
new java/io/File
dup
getstatic com/chelpus/root/utils/uninstall/datadir Ljava/lang/String;
ldc "/data/data/"
ldc "/dbdata/databases/"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual com/chelpus/Utils/deleteFolder(Ljava/io/File;)V
L6:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Start delete dc!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L8:
getstatic com/chelpus/root/utils/uninstall/dirapp Ljava/lang/String;
invokestatic com/chelpus/Utils/getFileDalvikCache(Ljava/lang/String;)Ljava/io/File;
astore 1
L9:
aload 1
ifnull L17
L11:
aload 1
invokevirtual java/io/File/delete()Z
pop
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Dalvik-cache "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " deleted."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L12:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Start delete odex."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
invokevirtual java/io/File/exists()Z
ifeq L13
aload 0
invokevirtual java/io/File/delete()Z
pop
L13:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Start delete apk!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/io/File
dup
getstatic com/chelpus/root/utils/uninstall/dirapp Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
aload 0
invokevirtual java/io/File/exists()Z
ifeq L21
aload 0
invokevirtual java/io/File/delete()Z
pop
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Delete apk:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/chelpus/root/utils/uninstall/dirapp Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L14:
goto L21
L7:
astore 1
L15:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher Error uninstall: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/Exception/printStackTrace()V
L16:
goto L6
L17:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "dalvik-cache not found."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L18:
goto L12
L10:
astore 0
L19:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Error: Exception e"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L20:
goto L13
.limit locals 3
.limit stack 6
.end method
