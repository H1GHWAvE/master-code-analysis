.bytecode 50.0
.class public synchronized com/chelpus/root/utils/pinfo
.super java/lang/Object
.inner class static final inner com/chelpus/root/utils/pinfo$1

.field public static 'classesFiles' Ljava/util/ArrayList; signature "Ljava/util/ArrayList<Ljava/io/File;>;"

.field public static 'toolfilesdir' Ljava/lang/String;

.method static <clinit>()V
ldc ""
putstatic com/chelpus/root/utils/pinfo/toolfilesdir Ljava/lang/String;
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putstatic com/chelpus/root/utils/pinfo/classesFiles Ljava/util/ArrayList;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static main([Ljava/lang/String;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L4 to L5 using L2
.catch java/lang/Exception from L6 to L7 using L8
.catch java/lang/Exception from L9 to L10 using L2
new com/chelpus/root/utils/pinfo$1
dup
invokespecial com/chelpus/root/utils/pinfo$1/<init>()V
invokestatic com/chelpus/Utils/startRootJava(Ljava/lang/Object;)V
aload 0
iconst_0
aaload
astore 1
aload 0
iconst_1
aaload
putstatic com/chelpus/root/utils/pinfo/toolfilesdir Ljava/lang/String;
aload 0
iconst_2
aaload
astore 2
aload 0
iconst_3
aaload
ifnull L11
aload 0
iconst_3
aaload
ldc "recovery"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L11
L0:
aload 1
aload 2
invokestatic com/chelpus/Utils/getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 0
aload 0
ldc "rw"
invokestatic com/chelpus/Utils/remount(Ljava/lang/String;Ljava/lang/String;)Z
pop
aload 1
invokestatic com/chelpus/Utils/getFileDalvikCache(Ljava/lang/String;)Ljava/io/File;
astore 3
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "_lpbackup"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 4
L1:
aload 4
ifnull L6
L3:
aload 4
invokevirtual java/io/File/exists()Z
ifeq L6
aload 4
aload 3
invokevirtual java/io/File/renameTo(Ljava/io/File;)Z
pop
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L4:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "_lpbackup"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L5
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "_lpbackup"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/renameTo(Ljava/io/File;)Z
pop
L5:
invokestatic com/chelpus/Utils/exitFromRootJava()V
return
L6:
getstatic com/chelpus/root/utils/pinfo/toolfilesdir Ljava/lang/String;
aload 1
aload 2
aload 1
aload 2
invokestatic com/chelpus/Utils/getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokestatic com/chelpus/Utils/create_DC_root(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
pop
L7:
goto L4
L8:
astore 1
L9:
aload 1
invokevirtual java/lang/Exception/printStackTrace()V
L10:
goto L4
L2:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
goto L5
L11:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/pinfo/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/p.apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokestatic com/chelpus/root/utils/pinfo/unzipART(Ljava/io/File;)V
aload 1
aload 2
invokestatic com/chelpus/Utils/getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 0
aload 1
invokestatic com/chelpus/Utils/getFileDalvikCache(Ljava/lang/String;)Ljava/io/File;
astore 3
aload 3
ifnull L12
aload 3
invokevirtual java/io/File/exists()Z
ifeq L12
aload 3
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "_lpbackup"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/renameTo(Ljava/io/File;)Z
pop
getstatic com/chelpus/root/utils/pinfo/toolfilesdir Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "_lpbackup"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 3
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokestatic com/chelpus/Utils/dalvikvm_copyFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
pop
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
aload 3
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
ldc "0:0"
aastore
dup
iconst_2
aload 3
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
ldc "0.0"
aastore
dup
iconst_2
aload 3
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L12:
aload 0
ldc "rw"
invokestatic com/chelpus/Utils/remount(Ljava/lang/String;Ljava/lang/String;)Z
pop
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L13
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "_lpbackup"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/renameTo(Ljava/io/File;)Z
pop
L13:
getstatic com/chelpus/root/utils/pinfo/toolfilesdir Ljava/lang/String;
getstatic com/chelpus/root/utils/pinfo/classesFiles Ljava/util/ArrayList;
aload 1
aload 2
aload 0
invokestatic com/chelpus/Utils/create_ODEX_root(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
pop
goto L5
.limit locals 5
.limit stack 5
.end method

.method public static unzipART(Ljava/io/File;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L4 to L5 using L2
.catch java/lang/Exception from L6 to L7 using L2
.catch net/lingala/zip4j/exception/ZipException from L8 to L9 using L10
.catch java/lang/Exception from L8 to L9 using L11
.catch java/lang/Exception from L12 to L13 using L2
.catch java/lang/Exception from L13 to L14 using L2
.catch java/lang/Exception from L14 to L15 using L2
.catch java/lang/Exception from L16 to L17 using L2
.catch java/lang/Exception from L18 to L19 using L2
.catch java/lang/Exception from L20 to L21 using L2
.catch java/lang/Exception from L22 to L23 using L2
iconst_0
istore 1
L0:
new java/io/FileInputStream
dup
aload 0
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
astore 4
new java/util/zip/ZipInputStream
dup
aload 4
invokespecial java/util/zip/ZipInputStream/<init>(Ljava/io/InputStream;)V
astore 5
aload 5
invokevirtual java/util/zip/ZipInputStream/getNextEntry()Ljava/util/zip/ZipEntry;
astore 3
L1:
aload 3
ifnull L20
iconst_1
ifeq L20
L3:
aload 3
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
astore 3
aload 3
ldc "classes"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L13
aload 3
ldc ".dex"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L13
aload 3
ldc "/"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L13
new java/io/FileOutputStream
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/pinfo/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
astore 6
sipush 2048
newarray byte
astore 7
L4:
aload 5
aload 7
invokevirtual java/util/zip/ZipInputStream/read([B)I
istore 2
L5:
iload 2
iconst_m1
if_icmpeq L12
L6:
aload 6
aload 7
iconst_0
iload 2
invokevirtual java/io/FileOutputStream/write([BII)V
L7:
goto L4
L2:
astore 3
L8:
new net/lingala/zip4j/core/ZipFile
dup
aload 0
invokespecial net/lingala/zip4j/core/ZipFile/<init>(Ljava/io/File;)V
astore 0
aload 0
ldc "classes.dex"
getstatic com/chelpus/root/utils/pinfo/toolfilesdir Ljava/lang/String;
invokevirtual net/lingala/zip4j/core/ZipFile/extractFile(Ljava/lang/String;Ljava/lang/String;)V
getstatic com/chelpus/root/utils/pinfo/classesFiles Ljava/util/ArrayList;
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/pinfo/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "classes.dex"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/pinfo/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "classes.dex"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
aload 0
ldc "AndroidManifest.xml"
getstatic com/chelpus/root/utils/pinfo/toolfilesdir Ljava/lang/String;
invokevirtual net/lingala/zip4j/core/ZipFile/extractFile(Ljava/lang/String;Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/pinfo/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "AndroidManifest.xml"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L9:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception e"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
return
L12:
aload 5
invokevirtual java/util/zip/ZipInputStream/closeEntry()V
aload 6
invokevirtual java/io/FileOutputStream/close()V
getstatic com/chelpus/root/utils/pinfo/classesFiles Ljava/util/ArrayList;
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/pinfo/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/pinfo/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L13:
aload 3
ldc "AndroidManifest.xml"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L24
new java/io/FileOutputStream
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/pinfo/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "AndroidManifest.xml"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
astore 3
sipush 2048
newarray byte
astore 6
L14:
aload 5
aload 6
invokevirtual java/util/zip/ZipInputStream/read([B)I
istore 1
L15:
iload 1
iconst_m1
if_icmpeq L18
L16:
aload 3
aload 6
iconst_0
iload 1
invokevirtual java/io/FileOutputStream/write([BII)V
L17:
goto L14
L18:
aload 5
invokevirtual java/util/zip/ZipInputStream/closeEntry()V
aload 3
invokevirtual java/io/FileOutputStream/close()V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/root/utils/pinfo/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "AndroidManifest.xml"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L19:
iconst_1
istore 1
goto L24
L20:
aload 5
invokevirtual java/util/zip/ZipInputStream/close()V
aload 4
invokevirtual java/io/FileInputStream/close()V
L21:
return
L22:
aload 5
invokevirtual java/util/zip/ZipInputStream/getNextEntry()Ljava/util/zip/ZipEntry;
astore 3
L23:
goto L1
L10:
astore 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Error classes.dex decompress! "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception e1"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
goto L9
L11:
astore 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Error classes.dex decompress! "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception e1"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/Exception/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/sendFromRoot(Ljava/lang/String;)Z
pop
goto L9
L24:
iconst_0
ifeq L22
iload 1
ifeq L22
goto L20
.limit locals 8
.limit stack 5
.end method
