.bytecode 50.0
.class public synchronized com/chelpus/GradientTextView
.super android/widget/TextView

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
aconst_null
iconst_m1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return
.limit locals 2
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
iconst_m1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return
.limit locals 3
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
aload 1
aload 2
iload 3
invokespecial android/widget/TextView/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return
.limit locals 4
.limit stack 4
.end method

.method protected onLayout(ZIIII)V
aload 0
iload 1
iload 2
iload 3
iload 4
iload 5
invokespecial android/widget/TextView/onLayout(ZIIII)V
iload 1
ifeq L0
aload 0
invokevirtual com/chelpus/GradientTextView/getPaint()Landroid/text/TextPaint;
new android/graphics/LinearGradient
dup
fconst_0
fconst_0
aload 0
invokevirtual com/chelpus/GradientTextView/getWidth()I
i2f
fconst_0
ldc "#FF6699cc"
invokestatic android/graphics/Color/parseColor(Ljava/lang/String;)I
ldc "#906699cc"
invokestatic android/graphics/Color/parseColor(Ljava/lang/String;)I
getstatic android/graphics/Shader$TileMode/CLAMP Landroid/graphics/Shader$TileMode;
invokespecial android/graphics/LinearGradient/<init>(FFFFIILandroid/graphics/Shader$TileMode;)V
invokevirtual android/text/TextPaint/setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;
pop
L0:
return
.limit locals 6
.limit stack 10
.end method
