.bytecode 50.0
.class public synchronized com/chelpus/Utils
.super java/lang/Object
.inner class static final inner com/chelpus/Utils$1
.inner class static final inner com/chelpus/Utils$10
.inner class static final inner com/chelpus/Utils$11
.inner class static final inner com/chelpus/Utils$12
.inner class static final inner com/chelpus/Utils$13
.inner class static final inner com/chelpus/Utils$14
.inner class static final inner com/chelpus/Utils$15
.inner class inner com/chelpus/Utils$2
.inner class inner com/chelpus/Utils$3
.inner class inner com/chelpus/Utils$4
.inner class inner com/chelpus/Utils$5
.inner class inner com/chelpus/Utils$6
.inner class inner com/chelpus/Utils$7
.inner class inner com/chelpus/Utils$8
.inner class inner com/chelpus/Utils$9
.inner class RootTimerTask inner com/chelpus/Utils$RootTimerTask outer com/chelpus/Utils
.inner class private Worker inner com/chelpus/Utils$Worker outer com/chelpus/Utils

.field static final 'AB' Ljava/lang/String; = "abcdefghijklmnopqrstuvwxyz"

.field static final 'AB2' Ljava/lang/String; = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

.field private static final 'LIB_ART' Ljava/lang/String; = "libart.so"

.field private static final 'LIB_ART_D' Ljava/lang/String; = "libartd.so"

.field private static final 'LIB_DALVIK' Ljava/lang/String; = "libdvm.so"

.field public static final 'ROOT_NOT_FOUND' Ljava/lang/String; = "lucky patcher root not found!"

.field private static final 'SELECT_RUNTIME_PROPERTY' Ljava/lang/String; = "persist.sys.dalvik.vm.lib"

.field protected static final 'hexArray' [C

.field private static 'internalBusybox' Ljava/lang/String;

.field static 'mService' Lcom/android/vending/billing/InAppBillingService/LUCK/ITestServiceInterface;

.field static 'mServiceConn' Landroid/content/ServiceConnection;

.field public static 'pattern_check' Ljava/lang/String;

.field static 'rnd' Ljava/util/Random;

.field 'folder_size' F

.method static <clinit>()V
ldc "297451286"
putstatic com/chelpus/Utils/pattern_check Ljava/lang/String;
ldc ""
putstatic com/chelpus/Utils/internalBusybox Ljava/lang/String;
ldc "0123456789ABCDEF"
invokevirtual java/lang/String/toCharArray()[C
putstatic com/chelpus/Utils/hexArray [C
aconst_null
putstatic com/chelpus/Utils/mServiceConn Landroid/content/ServiceConnection;
aconst_null
putstatic com/chelpus/Utils/mService Lcom/android/vending/billing/InAppBillingService/LUCK/ITestServiceInterface;
new java/util/Random
dup
invokespecial java/util/Random/<init>()V
putstatic com/chelpus/Utils/rnd Ljava/util/Random;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>(Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
fconst_0
putfield com/chelpus/Utils/folder_size F
return
.limit locals 2
.limit stack 2
.end method

.method public static XZCompress(Ljava/io/File;Ljava/io/File;)Z
.throws java/lang/OutOfMemoryError
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch org/tukaani/xz/UnsupportedOptionsException from L0 to L1 using L3
.catch java/io/IOException from L0 to L1 using L4
.catch java/lang/Exception from L0 to L1 using L5
.catch java/io/FileNotFoundException from L1 to L6 using L2
.catch org/tukaani/xz/UnsupportedOptionsException from L1 to L6 using L3
.catch java/io/IOException from L1 to L6 using L4
.catch java/lang/Exception from L1 to L6 using L5
.catch java/io/FileNotFoundException from L7 to L8 using L2
.catch org/tukaani/xz/UnsupportedOptionsException from L7 to L8 using L3
.catch java/io/IOException from L7 to L8 using L4
.catch java/lang/Exception from L7 to L8 using L5
.catch java/io/FileNotFoundException from L9 to L10 using L2
.catch org/tukaani/xz/UnsupportedOptionsException from L9 to L10 using L3
.catch java/io/IOException from L9 to L10 using L4
.catch java/lang/Exception from L9 to L10 using L5
L0:
new java/io/FileInputStream
dup
aload 0
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
astore 0
new org/tukaani/xz/XZOutputStream
dup
new java/io/FileOutputStream
dup
aload 1
invokespecial java/io/FileOutputStream/<init>(Ljava/io/File;)V
new org/tukaani/xz/LZMA2Options
dup
invokespecial org/tukaani/xz/LZMA2Options/<init>()V
invokespecial org/tukaani/xz/XZOutputStream/<init>(Ljava/io/OutputStream;Lorg/tukaani/xz/FilterOptions;)V
astore 3
sipush 2048
newarray byte
astore 4
L1:
aload 0
aload 4
invokevirtual java/io/FileInputStream/read([B)I
istore 2
L6:
iload 2
iconst_m1
if_icmpeq L9
L7:
aload 3
aload 4
iconst_0
iload 2
invokevirtual org/tukaani/xz/XZOutputStream/write([BII)V
L8:
goto L1
L2:
astore 0
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "File not found for xz compress."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
invokevirtual java/io/FileNotFoundException/printStackTrace()V
L11:
iconst_0
ireturn
L9:
aload 3
invokevirtual org/tukaani/xz/XZOutputStream/finish()V
L10:
aload 1
invokevirtual java/io/File/exists()Z
ifeq L11
iconst_1
ireturn
L3:
astore 0
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Unsupported options for xz compress."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
invokevirtual org/tukaani/xz/UnsupportedOptionsException/printStackTrace()V
iconst_0
ireturn
L4:
astore 0
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "IO Error for xz compress."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
invokevirtual java/io/IOException/printStackTrace()V
iconst_0
ireturn
L5:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
iconst_0
ireturn
.limit locals 5
.limit stack 5
.end method

.method public static XZDecompress(Ljava/io/File;Ljava/lang/String;)Z
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch java/io/EOFException from L0 to L1 using L3
.catch java/io/IOException from L0 to L1 using L4
.catch java/io/FileNotFoundException from L5 to L6 using L2
.catch java/io/EOFException from L5 to L6 using L3
.catch java/io/IOException from L5 to L6 using L4
.catch java/io/FileNotFoundException from L7 to L8 using L2
.catch java/io/EOFException from L7 to L8 using L3
.catch java/io/IOException from L7 to L8 using L4
.catch java/io/FileNotFoundException from L9 to L10 using L2
.catch java/io/EOFException from L9 to L10 using L3
.catch java/io/IOException from L9 to L10 using L4
.catch java/io/FileNotFoundException from L11 to L12 using L2
.catch java/io/EOFException from L11 to L12 using L3
.catch java/io/IOException from L11 to L12 using L4
sipush 2048
newarray byte
astore 7
aconst_null
astore 5
aconst_null
astore 6
aconst_null
astore 4
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 3
aload 3
invokevirtual java/io/File/exists()Z
ifeq L13
aload 3
invokevirtual java/io/File/isFile()Z
ifeq L14
L13:
aload 3
invokevirtual java/io/File/isFile()Z
ifeq L15
aload 3
invokevirtual java/io/File/delete()Z
pop
L15:
aload 3
invokevirtual java/io/File/mkdirs()Z
pop
L14:
aload 3
invokevirtual java/io/File/exists()Z
ifne L16
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "not found dir for ectract xz."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iconst_0
ireturn
L16:
aload 1
ldc "/"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L17
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/io/File/getName()Ljava/lang/String;
invokestatic com/chelpus/Utils/removeExtension(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 3
L0:
aload 0
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
astore 1
L1:
aload 1
astore 4
aload 1
astore 5
aload 1
astore 6
L5:
new org/tukaani/xz/XZInputStream
dup
new java/io/FileInputStream
dup
aload 0
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
invokespecial org/tukaani/xz/XZInputStream/<init>(Ljava/io/InputStream;)V
astore 0
L6:
aload 1
astore 4
aload 1
astore 5
aload 1
astore 6
L7:
new java/io/FileOutputStream
dup
aload 3
invokespecial java/io/FileOutputStream/<init>(Ljava/io/File;)V
astore 8
L8:
aload 1
astore 4
aload 1
astore 5
aload 1
astore 6
L9:
aload 0
aload 7
invokevirtual java/io/InputStream/read([B)I
istore 2
L10:
iload 2
iconst_m1
if_icmpeq L18
aload 1
astore 4
aload 1
astore 5
aload 1
astore 6
L11:
aload 8
aload 7
iconst_0
iload 2
invokevirtual java/io/FileOutputStream/write([BII)V
L12:
goto L8
L2:
astore 0
getstatic java/lang/System/err Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "XZDec: Cannot open "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ": "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/io/FileNotFoundException/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 3
invokevirtual java/io/File/delete()Z
pop
iconst_0
ireturn
L17:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/io/File/getName()Ljava/lang/String;
invokestatic com/chelpus/Utils/removeExtension(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 3
goto L0
L3:
astore 0
getstatic java/lang/System/err Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "XZDec: Unexpected end of input on "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 3
invokevirtual java/io/File/delete()Z
pop
iconst_0
ireturn
L4:
astore 0
getstatic java/lang/System/err Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "XZDec: Error decompressing from "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ": "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/io/IOException/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 3
invokevirtual java/io/File/delete()Z
pop
iconst_0
ireturn
L18:
aload 3
invokevirtual java/io/File/exists()Z
ifeq L19
iconst_1
ireturn
L19:
aload 3
invokevirtual java/io/File/delete()Z
pop
iconst_0
ireturn
.limit locals 9
.limit stack 5
.end method

.method static synthetic access$400()Ljava/lang/String;
getstatic com/chelpus/Utils/internalBusybox Ljava/lang/String;
areturn
.limit locals 0
.limit stack 1
.end method

.method public static final activityToFront()V
new android/content/Intent
dup
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/frag Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getContext()Landroid/support/v4/app/FragmentActivity;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/frag Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getActivity()Landroid/support/v4/app/FragmentActivity;
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokespecial android/content/Intent/<init>(Landroid/content/Context;Ljava/lang/Class;)V
astore 0
aload 0
ldc_w 131072
invokevirtual android/content/Intent/setFlags(I)Landroid/content/Intent;
pop
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/frag Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getContext()Landroid/support/v4/app/FragmentActivity;
aload 0
invokevirtual android/support/v4/app/FragmentActivity/startActivity(Landroid/content/Intent;)V
return
.limit locals 1
.limit stack 4
.end method

.method public static addFileToList(Ljava/io/File;Ljava/util/ArrayList;)V
.signature "(Ljava/io/File;Ljava/util/ArrayList<Ljava/io/File;>;)V"
aload 1
ifnull L0
aload 1
invokevirtual java/util/ArrayList/size()I
ifle L0
iconst_0
istore 2
aload 1
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 3
L1:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
invokevirtual java/io/File/length()J
aload 0
invokevirtual java/io/File/length()J
lcmp
ifne L1
iconst_1
istore 2
goto L1
L2:
iload 2
ifne L3
aload 1
aload 0
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L3:
return
L0:
aload 1
ifnull L3
aload 1
aload 0
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
return
.limit locals 4
.limit stack 4
.end method

.method public static addFilesToZip(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
.signature "(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;)V"
.throws java/io/IOException
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L2
.catch java/lang/Exception from L7 to L8 using L2
.catch java/lang/Exception from L9 to L10 using L2
.catch java/lang/Exception from L11 to L12 using L2
.catch java/lang/Exception from L13 to L14 using L2
.catch java/lang/Exception from L15 to L16 using L2
.catch java/lang/Exception from L17 to L18 using L2
.catch java/lang/Exception from L19 to L20 using L2
.catch java/lang/Exception from L21 to L22 using L2
.catch java/lang/Exception from L23 to L24 using L2
.catch java/lang/Exception from L25 to L26 using L2
.catch java/lang/Exception from L27 to L28 using L2
.catch java/lang/Exception from L29 to L30 using L2
.catch java/lang/Exception from L31 to L32 using L2
.catch java/lang/Exception from L33 to L34 using L2
.catch java/lang/Exception from L35 to L36 using L2
aload 0
invokestatic kellinwood/zipio/ZipInput/read(Ljava/lang/String;)Lkellinwood/zipio/ZipInput;
astore 6
new kellinwood/zipio/ZipOutput
dup
new java/io/FileOutputStream
dup
aload 1
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
invokespecial kellinwood/zipio/ZipOutput/<init>(Ljava/io/OutputStream;)V
astore 0
aload 6
invokevirtual kellinwood/zipio/ZipInput/getEntries()Ljava/util/Map;
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 1
L37:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L38
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast kellinwood/zipio/ZioEntry
astore 6
aload 6
invokevirtual kellinwood/zipio/ZioEntry/getName()Ljava/lang/String;
astore 7
iconst_0
istore 3
aload 2
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 8
L39:
aload 8
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L40
aload 8
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem
astore 9
aload 7
aload 9
getfield com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem/fileName Ljava/lang/String;
aload 9
getfield com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem/basePath Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L39
iload 3
istore 4
L0:
new java/io/File
dup
aload 9
getfield com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem/fileName Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 10
L1:
iload 3
istore 4
L3:
sipush 2048
newarray byte
astore 11
L4:
iload 3
istore 4
L5:
new java/io/FileInputStream
dup
aload 9
getfield com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem/fileName Ljava/lang/String;
invokespecial java/io/FileInputStream/<init>(Ljava/lang/String;)V
astore 12
L6:
iload 3
istore 4
L7:
new kellinwood/zipio/ZioEntry
dup
aload 7
invokespecial kellinwood/zipio/ZioEntry/<init>(Ljava/lang/String;)V
astore 13
L8:
iload 3
istore 4
L9:
aload 13
aload 6
invokevirtual kellinwood/zipio/ZioEntry/getCompression()S
invokevirtual kellinwood/zipio/ZioEntry/setCompression(I)V
L10:
iload 3
istore 4
L11:
aload 13
aload 6
invokevirtual kellinwood/zipio/ZioEntry/getTime()J
invokevirtual kellinwood/zipio/ZioEntry/setTime(J)V
L12:
iload 3
istore 4
L13:
aload 13
invokevirtual kellinwood/zipio/ZioEntry/getOutputStream()Ljava/io/OutputStream;
astore 14
L14:
iload 3
istore 4
L15:
new java/util/zip/CRC32
dup
invokespecial java/util/zip/CRC32/<init>()V
astore 15
L16:
iload 3
istore 4
L17:
aload 15
invokevirtual java/util/zip/CRC32/reset()V
L18:
iload 3
istore 4
L19:
aload 12
aload 11
invokevirtual java/io/FileInputStream/read([B)I
istore 5
L20:
iload 5
ifle L41
iload 3
istore 4
L21:
aload 14
aload 11
iconst_0
iload 5
invokevirtual java/io/OutputStream/write([BII)V
L22:
iload 3
istore 4
L23:
aload 15
aload 11
iconst_0
iload 5
invokevirtual java/util/zip/CRC32/update([BII)V
L24:
goto L18
L2:
astore 9
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 9
invokevirtual java/io/PrintStream/println(Ljava/lang/Object;)V
iload 4
istore 3
goto L39
L41:
iload 3
istore 4
L25:
aload 14
invokevirtual java/io/OutputStream/flush()V
L26:
iload 3
istore 4
L27:
aload 0
aload 13
invokevirtual kellinwood/zipio/ZipOutput/write(Lkellinwood/zipio/ZioEntry;)V
L28:
iconst_1
istore 5
iconst_1
istore 3
iload 5
istore 4
L29:
aload 10
invokevirtual java/io/File/delete()Z
pop
L30:
iload 5
istore 4
L31:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher (signer): Additional files added! "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 9
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L32:
iload 5
istore 4
L33:
aload 12
invokevirtual java/io/FileInputStream/close()V
L34:
iload 5
istore 4
L35:
aload 14
invokevirtual java/io/OutputStream/close()V
L36:
goto L39
L40:
iload 3
ifne L37
aload 0
aload 6
invokevirtual kellinwood/zipio/ZipOutput/write(Lkellinwood/zipio/ZioEntry;)V
goto L37
L38:
aload 0
invokevirtual kellinwood/zipio/ZipOutput/close()V
return
.limit locals 16
.limit stack 5
.end method

.method public static afterPatch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "uid:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
ldc "copyDC"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc "deleteDC"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L1
L0:
aload 1
invokestatic com/chelpus/Utils/getFileDalvikCache(Ljava/lang/String;)Ljava/io/File;
astore 4
aload 4
ifnull L2
aload 4
invokevirtual java/io/File/delete()Z
pop
aload 0
ldc "copyDC"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L1
new java/io/File
dup
aload 2
invokespecial java/io/File/<init>(Ljava/lang/String;)V
aload 4
invokestatic com/chelpus/Utils/copyFile(Ljava/io/File;Ljava/io/File;)V
aload 4
invokevirtual java/io/File/exists()Z
ifeq L1
aload 4
invokevirtual java/io/File/length()J
new java/io/File
dup
aload 2
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
lcmp
ifne L1
new java/io/File
dup
aload 2
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "644"
aastore
dup
iconst_2
aload 4
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "1000:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
aload 4
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "1000."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
aload 4
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L1:
return
L2:
aload 0
ldc "copyDC"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L1
aload 1
invokestatic com/chelpus/Utils/getFileDalvikCacheName(Ljava/lang/String;)Ljava/io/File;
astore 0
new java/io/File
dup
aload 2
invokespecial java/io/File/<init>(Ljava/lang/String;)V
aload 0
invokestatic com/chelpus/Utils/copyFile(Ljava/io/File;Ljava/io/File;)V
aload 0
invokevirtual java/io/File/exists()Z
ifeq L1
aload 0
invokevirtual java/io/File/length()J
new java/io/File
dup
aload 2
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
lcmp
ifne L1
new java/io/File
dup
aload 2
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "644"
aastore
dup
iconst_2
aload 0
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "1000:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
aload 0
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "1000."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
aload 0
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
return
.limit locals 5
.limit stack 5
.end method

.method public static apply_TAGS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
aload 0
ldc "%PACKAGE_NAME%"
aload 1
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 3
.end method

.method public static bytesToHex([B)Ljava/lang/String;
aload 0
arraylength
iconst_2
imul
newarray char
astore 3
iconst_0
istore 1
L0:
iload 1
aload 0
arraylength
if_icmpge L1
aload 0
iload 1
baload
sipush 255
iand
istore 2
aload 3
iload 1
iconst_2
imul
getstatic com/chelpus/Utils/hexArray [C
iload 2
iconst_4
iushr
caload
castore
aload 3
iload 1
iconst_2
imul
iconst_1
iadd
getstatic com/chelpus/Utils/hexArray [C
iload 2
bipush 15
iand
caload
castore
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
new java/lang/String
dup
aload 3
invokespecial java/lang/String/<init>([C)V
areturn
.limit locals 4
.limit stack 5
.end method

.method private static final calcChecksum(Ljava/io/File;)V
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch java/io/IOException from L0 to L1 using L3
.catch java/io/FileNotFoundException from L1 to L4 using L2
.catch java/io/IOException from L1 to L4 using L3
.catch java/io/FileNotFoundException from L5 to L6 using L2
.catch java/io/IOException from L5 to L6 using L3
L0:
new java/io/RandomAccessFile
dup
aload 0
ldc "rw"
invokespecial java/io/RandomAccessFile/<init>(Ljava/io/File;Ljava/lang/String;)V
invokevirtual java/io/RandomAccessFile/getChannel()Ljava/nio/channels/FileChannel;
astore 0
aload 0
getstatic java/nio/channels/FileChannel$MapMode/READ_WRITE Ljava/nio/channels/FileChannel$MapMode;
lconst_0
aload 0
invokevirtual java/nio/channels/FileChannel/size()J
l2i
i2l
invokevirtual java/nio/channels/FileChannel/map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
astore 2
new java/util/zip/Adler32
dup
invokespecial java/util/zip/Adler32/<init>()V
astore 3
aload 2
bipush 12
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L1:
aload 2
invokevirtual java/nio/MappedByteBuffer/hasRemaining()Z
ifeq L5
aload 3
aload 2
invokevirtual java/nio/MappedByteBuffer/get()B
invokevirtual java/util/zip/Adler32/update(I)V
L4:
goto L1
L2:
astore 0
aload 0
invokevirtual java/io/FileNotFoundException/printStackTrace()V
return
L5:
aload 3
invokevirtual java/util/zip/Adler32/getValue()J
l2i
istore 1
aload 2
bipush 8
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 2
iload 1
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 2
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 2
bipush 9
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 2
iload 1
bipush 8
ishr
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 2
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 2
bipush 10
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 2
iload 1
bipush 16
ishr
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 2
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 2
bipush 11
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 2
iload 1
bipush 24
ishr
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 2
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 0
invokevirtual java/nio/channels/FileChannel/close()V
L6:
return
L3:
astore 0
aload 0
invokevirtual java/io/IOException/printStackTrace()V
return
.limit locals 4
.limit stack 6
.end method

.method private static calcChecksumOdexFly(IILjava/io/File;)V
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch java/io/IOException from L0 to L1 using L3
.catch java/io/FileNotFoundException from L4 to L5 using L2
.catch java/io/IOException from L4 to L5 using L3
.catch java/io/FileNotFoundException from L6 to L7 using L2
.catch java/io/IOException from L6 to L7 using L3
L0:
new java/io/RandomAccessFile
dup
aload 2
ldc "rw"
invokespecial java/io/RandomAccessFile/<init>(Ljava/io/File;Ljava/lang/String;)V
invokevirtual java/io/RandomAccessFile/getChannel()Ljava/nio/channels/FileChannel;
astore 2
aload 2
getstatic java/nio/channels/FileChannel$MapMode/READ_WRITE Ljava/nio/channels/FileChannel$MapMode;
lconst_0
aload 2
invokevirtual java/nio/channels/FileChannel/size()J
l2i
i2l
invokevirtual java/nio/channels/FileChannel/map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
astore 3
new java/util/zip/Adler32
dup
invokespecial java/util/zip/Adler32/<init>()V
astore 4
aload 3
iload 0
bipush 12
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L1:
iload 1
ifle L6
L4:
aload 4
aload 3
invokevirtual java/nio/MappedByteBuffer/get()B
invokevirtual java/util/zip/Adler32/update(I)V
L5:
iload 1
iconst_1
isub
istore 1
goto L1
L6:
aload 4
invokevirtual java/util/zip/Adler32/getValue()J
l2i
istore 1
aload 3
iload 0
bipush 8
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 3
iload 1
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 3
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 3
iload 0
bipush 9
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 3
iload 1
bipush 8
ishr
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 3
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 3
iload 0
bipush 10
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 3
iload 1
bipush 16
ishr
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 3
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 3
iload 0
bipush 11
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 3
iload 1
bipush 24
ishr
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 3
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 2
invokevirtual java/nio/channels/FileChannel/close()V
L7:
return
L2:
astore 2
aload 2
invokevirtual java/io/FileNotFoundException/printStackTrace()V
return
L3:
astore 2
aload 2
invokevirtual java/io/IOException/printStackTrace()V
return
.limit locals 5
.limit stack 6
.end method

.method private static final calcSignature(Ljava/io/File;)V
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch java/io/IOException from L0 to L1 using L3
.catch java/security/NoSuchAlgorithmException from L1 to L4 using L5
.catch java/io/FileNotFoundException from L1 to L4 using L2
.catch java/io/IOException from L1 to L4 using L3
.catch java/io/FileNotFoundException from L4 to L6 using L2
.catch java/io/IOException from L4 to L6 using L3
.catch java/io/FileNotFoundException from L6 to L7 using L2
.catch java/io/IOException from L6 to L7 using L3
.catch java/io/FileNotFoundException from L8 to L3 using L2
.catch java/io/IOException from L8 to L3 using L3
.catch java/security/DigestException from L9 to L10 using L11
.catch java/io/FileNotFoundException from L9 to L10 using L2
.catch java/io/IOException from L9 to L10 using L3
.catch java/security/DigestException from L12 to L11 using L11
.catch java/io/FileNotFoundException from L12 to L11 using L2
.catch java/io/IOException from L12 to L11 using L3
.catch java/io/FileNotFoundException from L13 to L14 using L2
.catch java/io/IOException from L13 to L14 using L3
.catch java/io/FileNotFoundException from L14 to L15 using L2
.catch java/io/IOException from L14 to L15 using L3
L0:
new java/io/RandomAccessFile
dup
aload 0
ldc "rw"
invokespecial java/io/RandomAccessFile/<init>(Ljava/io/File;Ljava/lang/String;)V
invokevirtual java/io/RandomAccessFile/getChannel()Ljava/nio/channels/FileChannel;
astore 0
aload 0
getstatic java/nio/channels/FileChannel$MapMode/READ_WRITE Ljava/nio/channels/FileChannel$MapMode;
lconst_0
aload 0
invokevirtual java/nio/channels/FileChannel/size()J
l2i
i2l
invokevirtual java/nio/channels/FileChannel/map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
astore 2
L1:
ldc "SHA-1"
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
astore 3
L4:
bipush 20
newarray byte
astore 4
aload 2
bipush 32
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L6:
aload 2
invokevirtual java/nio/MappedByteBuffer/hasRemaining()Z
ifeq L9
aload 3
aload 2
invokevirtual java/nio/MappedByteBuffer/get()B
invokevirtual java/security/MessageDigest/update(B)V
L7:
goto L6
L2:
astore 0
aload 0
invokevirtual java/io/FileNotFoundException/printStackTrace()V
return
L5:
astore 0
L8:
new java/lang/RuntimeException
dup
aload 0
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
L3:
astore 0
aload 0
invokevirtual java/io/IOException/printStackTrace()V
return
L9:
aload 3
aload 4
iconst_0
bipush 20
invokevirtual java/security/MessageDigest/digest([BII)I
istore 1
L10:
iload 1
bipush 20
if_icmpeq L14
L12:
new java/lang/RuntimeException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "unexpected digest write:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc "bytes"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L11:
astore 0
L13:
new java/lang/RuntimeException
dup
aload 0
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
L14:
aload 2
bipush 12
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 2
aload 4
invokevirtual java/nio/MappedByteBuffer/put([B)Ljava/nio/ByteBuffer;
pop
aload 2
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 0
invokevirtual java/nio/channels/FileChannel/close()V
L15:
return
.limit locals 5
.limit stack 6
.end method

.method public static changeExtension(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
ldc ""
astore 4
aload 4
astore 3
aload 0
ifnull L0
aload 0
ldc "\\."
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 5
iconst_0
istore 2
aload 4
astore 0
L1:
aload 0
astore 3
iload 2
aload 5
arraylength
if_icmpge L0
iload 2
aload 5
arraylength
iconst_1
isub
if_icmpge L2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
iload 2
aaload
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
L3:
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
goto L3
L0:
aload 3
areturn
.limit locals 6
.limit stack 3
.end method

.method public static changePackageNameIds(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L7
.catch java/lang/Exception from L8 to L9 using L10
.catch java/lang/Exception from L11 to L12 using L10
.catch java/lang/Exception from L13 to L14 using L10
.catch java/lang/Exception from L15 to L16 using L10
.catch java/lang/Exception from L17 to L18 using L10
.catch java/lang/Exception from L19 to L20 using L10
.catch java/lang/Exception from L21 to L22 using L10
.catch java/lang/Exception from L23 to L24 using L10
.catch java/lang/Exception from L25 to L26 using L10
.catch java/lang/Exception from L27 to L28 using L10
.catch java/lang/Exception from L29 to L30 using L10
.catch java/lang/Exception from L31 to L32 using L10
.catch java/lang/Exception from L33 to L34 using L10
.catch java/lang/Exception from L35 to L36 using L10
.catch java/lang/Exception from L37 to L38 using L10
.catch java/lang/Exception from L39 to L40 using L10
.catch java/lang/Exception from L41 to L42 using L10
.catch java/lang/Exception from L43 to L44 using L10
.catch java/lang/Exception from L45 to L46 using L10
.catch java/lang/Exception from L47 to L48 using L10
.catch java/lang/Exception from L49 to L50 using L10
.catch java/lang/Exception from L51 to L52 using L10
.catch java/lang/Exception from L53 to L54 using L10
.catch java/lang/Exception from L55 to L56 using L10
.catch java/lang/Exception from L57 to L58 using L10
.catch java/lang/Exception from L59 to L60 using L10
.catch java/lang/Exception from L61 to L62 using L10
.catch java/lang/Exception from L63 to L64 using L10
.catch java/lang/Exception from L65 to L66 using L10
.catch java/lang/Exception from L67 to L68 using L10
.catch java/lang/Exception from L69 to L70 using L10
.catch java/lang/Exception from L71 to L72 using L10
.catch java/lang/Exception from L73 to L74 using L10
.catch java/lang/Exception from L75 to L76 using L10
.catch java/lang/Exception from L77 to L78 using L10
.catch java/lang/Exception from L79 to L80 using L10
.catch java/lang/Exception from L81 to L82 using L10
.catch java/lang/Exception from L83 to L84 using L10
.catch java/lang/Exception from L85 to L86 using L7
.catch java/lang/Exception from L87 to L88 using L2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "L"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
ldc "\\."
ldc "/"
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 14
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "L"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
ldc "\\."
ldc "/"
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 15
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 14
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 15
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "scan: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iconst_0
istore 7
iconst_0
istore 6
iconst_0
istore 8
iconst_0
istore 9
iconst_0
istore 5
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
aload 0
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
pop
iload 7
istore 3
aload 0
ifnull L84
iload 7
istore 3
iload 9
istore 4
L0:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L84
L1:
iload 9
istore 4
L3:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
lstore 11
L4:
iload 7
istore 3
lload 11
lconst_0
lcmp
ifeq L84
iload 8
istore 3
L5:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
istore 13
L6:
iload 7
istore 3
iload 13
ifeq L84
iload 6
istore 4
L8:
new java/io/RandomAccessFile
dup
aload 0
ldc "rw"
invokespecial java/io/RandomAccessFile/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual java/io/RandomAccessFile/getChannel()Ljava/nio/channels/FileChannel;
astore 0
L9:
iload 6
istore 4
L11:
aload 0
getstatic java/nio/channels/FileChannel$MapMode/READ_WRITE Ljava/nio/channels/FileChannel$MapMode;
lconst_0
aload 0
invokevirtual java/nio/channels/FileChannel/size()J
l2i
i2l
invokevirtual java/nio/channels/FileChannel/map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
astore 16
L12:
iload 6
istore 4
L13:
aload 16
bipush 56
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L14:
iload 6
istore 4
L15:
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
invokestatic com/chelpus/Utils/convertFourBytesToInt(BBBB)I
istore 7
L16:
iload 6
istore 4
L17:
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
invokestatic com/chelpus/Utils/convertFourBytesToInt(BBBB)I
istore 3
L18:
iload 6
istore 4
L19:
iload 7
iconst_4
idiv
newarray byte
astore 17
L20:
iload 6
istore 4
L21:
aload 16
iload 3
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L22:
iload 6
istore 4
L23:
iload 7
newarray int
astore 17
L24:
iconst_0
istore 3
L89:
iload 3
iload 7
if_icmpge L90
iload 6
istore 4
L25:
aload 17
iload 3
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
invokestatic com/chelpus/Utils/convertFourBytesToInt(BBBB)I
iastore
L26:
iload 3
iconst_1
iadd
istore 3
goto L89
L90:
iload 6
istore 4
L27:
aload 17
arraylength
istore 7
L28:
iconst_0
istore 6
iload 5
istore 3
L91:
iload 6
iload 7
if_icmpge L92
aload 17
iload 6
iaload
istore 8
iload 3
istore 4
L29:
aload 16
iload 8
invokevirtual java/nio/MappedByteBuffer/get(I)B
invokestatic com/chelpus/Utils/convertByteToInt(B)I
istore 9
L30:
iload 3
istore 4
L31:
iload 9
newarray byte
astore 18
L32:
iload 8
iconst_1
iadd
istore 10
iload 3
istore 4
L33:
aload 16
iload 10
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L34:
iconst_0
istore 5
L93:
iload 3
istore 4
L35:
iload 5
aload 18
arraylength
if_icmpge L94
L36:
iload 3
istore 4
L37:
aload 18
iload 5
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
bastore
L38:
iload 5
iconst_1
iadd
istore 5
goto L93
L94:
iload 3
istore 5
iload 3
istore 4
L39:
iload 9
aload 2
invokevirtual java/lang/String/length()I
if_icmplt L95
L40:
iload 3
istore 5
iload 3
istore 4
L41:
new java/lang/String
dup
aload 18
invokespecial java/lang/String/<init>([B)V
aload 1
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L95
L42:
iload 3
istore 4
L43:
new java/lang/String
dup
aload 18
invokespecial java/lang/String/<init>([B)V
aload 1
aload 2
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/String/getBytes()[B
astore 19
L44:
iload 3
istore 5
iload 3
istore 4
L45:
aload 19
arraylength
iload 9
if_icmpgt L95
L46:
iload 3
istore 4
L47:
aload 16
iload 8
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L48:
iload 3
istore 4
L49:
aload 16
aload 19
arraylength
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
L50:
iload 3
istore 4
L51:
aload 16
iload 10
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L52:
iload 3
istore 4
L53:
aload 16
aload 19
invokevirtual java/nio/MappedByteBuffer/put([B)Ljava/nio/ByteBuffer;
pop
L54:
iload 3
istore 4
L55:
aload 16
iconst_0
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
L56:
iload 3
istore 4
L57:
aload 16
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
L58:
iload 3
istore 4
L59:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Replace string:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
new java/lang/String
dup
aload 18
invokespecial java/lang/String/<init>([B)V
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L60:
iload 3
iconst_1
iadd
istore 5
L95:
iload 5
istore 4
iload 5
istore 3
L61:
iload 9
aload 14
invokevirtual java/lang/String/length()I
if_icmplt L96
L62:
iload 5
istore 4
iload 5
istore 3
L63:
new java/lang/String
dup
aload 18
invokespecial java/lang/String/<init>([B)V
aload 14
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L96
L64:
iload 5
istore 4
L65:
new java/lang/String
dup
aload 18
invokespecial java/lang/String/<init>([B)V
aload 14
aload 15
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/String/getBytes()[B
astore 19
L66:
iload 5
istore 4
iload 5
istore 3
L67:
aload 19
arraylength
iload 9
if_icmpgt L96
L68:
iload 5
istore 4
L69:
aload 16
iload 8
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L70:
iload 5
istore 4
L71:
aload 16
aload 19
arraylength
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
L72:
iload 5
istore 4
L73:
aload 16
iload 10
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L74:
iload 5
istore 4
L75:
aload 16
aload 19
invokevirtual java/nio/MappedByteBuffer/put([B)Ljava/nio/ByteBuffer;
pop
L76:
iload 5
istore 4
L77:
aload 16
iconst_0
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
L78:
iload 5
istore 4
L79:
aload 16
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
L80:
iload 5
istore 4
L81:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Replace string:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
new java/lang/String
dup
aload 18
invokespecial java/lang/String/<init>([B)V
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L82:
iload 5
iconst_1
iadd
istore 3
goto L96
L92:
iload 3
istore 4
L83:
aload 0
invokevirtual java/nio/channels/FileChannel/close()V
L84:
iload 3
ireturn
L10:
astore 0
iload 4
istore 3
L85:
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L86:
iload 4
ireturn
L7:
astore 0
iload 3
istore 4
L87:
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L88:
iload 3
ireturn
L2:
astore 0
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
invokevirtual java/io/PrintStream/println(Ljava/lang/Object;)V
iload 4
ireturn
L96:
iload 6
iconst_1
iadd
istore 6
goto L91
.limit locals 20
.limit stack 6
.end method

.method public static checkBind(Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;)Z
.catch java/io/IOException from L0 to L1 using L2
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/BindItem/TargetDir Ljava/lang/String;
invokevirtual java/lang/String/trim()Ljava/lang/String;
ldc "~chelpus_disabled~"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L3
L4:
iconst_0
ireturn
L3:
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/BindItem/SourceDir Ljava/lang/String;
invokevirtual java/lang/String/trim()Ljava/lang/String;
astore 2
aload 0
getfield com/android/vending/billing/InAppBillingService/LUCK/BindItem/TargetDir Ljava/lang/String;
invokevirtual java/lang/String/trim()Ljava/lang/String;
astore 3
aload 2
ldc "/"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifne L5
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual java/lang/String/trim()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
L6:
aload 3
ldc "/"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifne L7
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/String/trim()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
L8:
new java/io/File
dup
aload 3
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/mkdirs()Z
pop
new java/io/File
dup
aload 2
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/mkdirs()Z
pop
new java/io/File
dup
aload 3
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifne L9
ldc "mkdir"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "-p '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/verify_and_run(Ljava/lang/String;Ljava/lang/String;)V
L9:
new java/io/File
dup
aload 2
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifne L0
ldc "mkdir"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "-p '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/verify_and_run(Ljava/lang/String;Ljava/lang/String;)V
L0:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "test.txt"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/createNewFile()Z
pop
L1:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "echo '' >'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "test.txt'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "test.txt"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifne L10
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "test.txt"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "test.txt"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifeq L4
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "rm '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "test.txt'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
iconst_0
ireturn
L5:
aload 2
astore 0
goto L6
L7:
aload 3
astore 1
goto L8
L2:
astore 2
aload 2
invokevirtual java/io/IOException/printStackTrace()V
goto L1
L10:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "test.txt"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "test.txt"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifeq L11
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "rm '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "test.txt'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
L11:
iconst_1
ireturn
.limit locals 4
.limit stack 4
.end method

.method public static checkCoreJarPatch11()Z
.catch java/security/NoSuchAlgorithmException from L0 to L1 using L2
.catch java/security/InvalidKeyException from L0 to L1 using L3
.catch java/security/SignatureException from L0 to L1 using L4
.catch java/security/spec/InvalidKeySpecException from L0 to L1 using L5
L0:
ldc "SHA1withRSA"
invokestatic java/security/Signature/getInstance(Ljava/lang/String;)Ljava/security/Signature;
astore 1
new java/security/spec/RSAPublicKeySpec
dup
new java/math/BigInteger
dup
ldc "12345678"
bipush 16
invokespecial java/math/BigInteger/<init>(Ljava/lang/String;I)V
new java/math/BigInteger
dup
ldc "11"
bipush 16
invokespecial java/math/BigInteger/<init>(Ljava/lang/String;I)V
invokespecial java/security/spec/RSAPublicKeySpec/<init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V
astore 2
aload 1
ldc "RSA"
invokestatic java/security/KeyFactory/getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;
aload 2
invokevirtual java/security/KeyFactory/generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;
invokevirtual java/security/Signature/initVerify(Ljava/security/PublicKey;)V
aload 1
ldc "367"
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/security/Signature/update([B)V
aload 1
ldc "123098"
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/security/Signature/verify([B)Z
istore 0
L1:
iload 0
ifne L6
iconst_0
ireturn
L6:
iconst_1
ireturn
L2:
astore 1
iconst_0
ireturn
L3:
astore 1
iconst_0
ireturn
L4:
astore 1
iconst_0
ireturn
L5:
astore 1
aload 1
invokevirtual java/security/spec/InvalidKeySpecException/printStackTrace()V
iconst_0
ireturn
.limit locals 3
.limit stack 7
.end method

.method public static checkCoreJarPatch12()Z
.catch java/security/NoSuchAlgorithmException from L0 to L1 using L2
.catch java/security/InvalidKeyException from L0 to L1 using L3
.catch java/security/SignatureException from L0 to L1 using L4
.catch java/security/spec/InvalidKeySpecException from L0 to L1 using L5
L0:
ldc "SHA1withRSA"
invokestatic java/security/Signature/getInstance(Ljava/lang/String;)Ljava/security/Signature;
astore 1
new java/security/spec/RSAPublicKeySpec
dup
new java/math/BigInteger
dup
ldc "12345678"
bipush 16
invokespecial java/math/BigInteger/<init>(Ljava/lang/String;I)V
new java/math/BigInteger
dup
ldc "11"
bipush 16
invokespecial java/math/BigInteger/<init>(Ljava/lang/String;I)V
invokespecial java/security/spec/RSAPublicKeySpec/<init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V
astore 2
aload 1
ldc "RSA"
invokestatic java/security/KeyFactory/getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;
aload 2
invokevirtual java/security/KeyFactory/generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;
invokevirtual java/security/Signature/initVerify(Ljava/security/PublicKey;)V
aload 1
ldc "367"
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/security/Signature/update([B)V
aload 1
ldc "123098"
invokevirtual java/lang/String/getBytes()[B
iconst_1
iconst_5
invokevirtual java/security/Signature/verify([BII)Z
istore 0
L1:
iload 0
ifne L6
iconst_0
ireturn
L6:
iconst_1
ireturn
L2:
astore 1
iconst_0
ireturn
L3:
astore 1
iconst_0
ireturn
L4:
astore 1
iconst_0
ireturn
L5:
astore 1
aload 1
invokevirtual java/security/spec/InvalidKeySpecException/printStackTrace()V
iconst_0
ireturn
.limit locals 3
.limit stack 7
.end method

.method public static checkCoreJarPatch20()Z
ldc "12"
invokevirtual java/lang/String/getBytes()[B
ldc "45"
invokevirtual java/lang/String/getBytes()[B
invokestatic java/security/MessageDigest/isEqual([B[B)Z
ireturn
.limit locals 0
.limit stack 2
.end method

.method public static checkCoreJarPatch30(Landroid/content/pm/PackageManager;)Z
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L4
.catch android/content/pm/PackageManager$NameNotFoundException from L5 to L6 using L7
.catch android/content/pm/PackageManager$NameNotFoundException from L8 to L9 using L7
.catch android/content/pm/PackageManager$NameNotFoundException from L10 to L11 using L7
L0:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
ldc "android"
bipush 64
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
getfield android/content/pm/PackageInfo/signatures [Landroid/content/pm/Signature;
iconst_0
aaload
invokevirtual android/content/pm/Signature/toByteArray()[B
invokestatic com/google/android/finsky/billing/iab/google/util/Base64/encode([B)Ljava/lang/String;
ldc "\n"
ldc ""
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 5
L1:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
ldc com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment
invokevirtual java/lang/Class/getPackage()Ljava/lang/Package;
invokevirtual java/lang/Package/getName()Ljava/lang/String;
bipush 64
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
getfield android/content/pm/PackageInfo/signatures [Landroid/content/pm/Signature;
iconst_0
aaload
invokevirtual android/content/pm/Signature/toByteArray()[B
invokestatic com/google/android/finsky/billing/iab/google/util/Base64/encode([B)Ljava/lang/String;
pop
aload 5
ldc "\n"
ldc ""
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 6
L3:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPackages()[Ljava/lang/String;
astore 7
aload 7
ifnull L12
aload 7
arraylength
ifle L12
aload 7
arraylength
istore 3
iconst_0
istore 1
L13:
iload 1
iload 3
if_icmpge L12
aload 7
iload 1
aaload
astore 8
aload 8
ldc "android"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L14
aload 8
ldc com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment
invokevirtual java/lang/Class/getPackage()Ljava/lang/Package;
invokevirtual java/lang/Package/getName()Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L14
L5:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
aload 8
bipush 64
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
getfield android/content/pm/PackageInfo/signatures [Landroid/content/pm/Signature;
astore 9
L6:
aload 9
ifnull L14
L8:
aload 9
arraylength
iconst_1
if_icmpne L14
aload 9
arraylength
istore 4
L9:
iconst_0
istore 2
L15:
iload 2
iload 4
if_icmpge L14
L10:
aload 9
iload 2
aaload
invokevirtual android/content/pm/Signature/toByteArray()[B
invokestatic com/google/android/finsky/billing/iab/google/util/Base64/encode([B)Ljava/lang/String;
ldc "\n"
ldc ""
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 10
aload 10
aload 5
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L16
aload 10
aload 6
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L16
aload 0
aload 8
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
invokevirtual android/content/pm/PackageManager/checkSignatures(Ljava/lang/String;Ljava/lang/String;)I
istore 2
L11:
iload 2
ifne L17
iconst_1
ireturn
L2:
astore 5
ldc "chelpa"
astore 5
goto L1
L4:
astore 6
ldc "chelpa"
astore 6
goto L3
L17:
iconst_0
ireturn
L16:
iload 2
iconst_1
iadd
istore 2
goto L15
L7:
astore 8
aload 8
invokevirtual android/content/pm/PackageManager$NameNotFoundException/printStackTrace()V
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "dont get Android signature"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L14:
iload 1
iconst_1
iadd
istore 1
goto L13
L12:
iconst_0
ireturn
.limit locals 11
.limit stack 3
.end method

.method public static checkCoreJarPatch40()Z
.catch java/lang/SecurityException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
.catch java/lang/SecurityException from L4 to L5 using L2
.catch java/lang/Exception from L4 to L5 using L3
.catch java/lang/SecurityException from L6 to L7 using L2
.catch java/lang/Exception from L6 to L7 using L3
iconst_0
istore 1
new com/chelpus/Utils$12
dup
invokespecial com/chelpus/Utils$12/<init>()V
putstatic com/chelpus/Utils/mServiceConn Landroid/content/ServiceConnection;
iload 1
istore 0
getstatic com/chelpus/Utils/mService Lcom/android/vending/billing/InAppBillingService/LUCK/ITestServiceInterface;
ifnonnull L8
L0:
new android/content/Intent
dup
ldc "com.android.vending.billing.InAppBillingService.LUCK.ITestServiceInterface.BIND"
invokespecial android/content/Intent/<init>(Ljava/lang/String;)V
astore 2
aload 2
ldc "com.android.vending"
invokevirtual android/content/Intent/setPackage(Ljava/lang/String;)Landroid/content/Intent;
pop
L1:
iload 1
istore 0
L4:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
aload 2
iconst_0
invokevirtual android/content/pm/PackageManager/queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;
invokeinterface java/util/List/isEmpty()Z 0
ifne L8
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
aload 2
iconst_0
invokevirtual android/content/pm/PackageManager/queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 2
L5:
iload 1
istore 0
L6:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L8
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/content/pm/ResolveInfo
astore 3
aload 3
getfield android/content/pm/ResolveInfo/serviceInfo Landroid/content/pm/ServiceInfo;
getfield android/content/pm/ServiceInfo/packageName Ljava/lang/String;
ifnull L5
aload 3
getfield android/content/pm/ResolveInfo/serviceInfo Landroid/content/pm/ServiceInfo;
getfield android/content/pm/ServiceInfo/packageName Ljava/lang/String;
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
istore 0
L7:
iload 0
ifeq L5
iconst_1
istore 0
L8:
iload 0
ireturn
L2:
astore 2
aload 2
invokevirtual java/lang/SecurityException/printStackTrace()V
iconst_0
ireturn
L3:
astore 2
aload 2
invokevirtual java/lang/Exception/printStackTrace()V
iconst_0
ireturn
.limit locals 4
.limit stack 3
.end method

.method public static checkRoot(Ljava/lang/Boolean;Ljava/lang/String;)Z
aload 1
putstatic com/chelpus/Utils/internalBusybox Ljava/lang/String;
new java/lang/Thread
dup
new com/chelpus/Utils$10
dup
invokespecial com/chelpus/Utils$10/<init>()V
invokespecial java/lang/Thread/<init>(Ljava/lang/Runnable;)V
invokevirtual java/lang/Thread/run()V
iconst_1
ireturn
.limit locals 2
.limit stack 4
.end method

.method public static checkRuntimeFromCache(Ljava/lang/String;)Ljava/lang/String;
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch java/io/IOException from L0 to L1 using L3
.catch java/lang/Exception from L0 to L1 using L4
.catch java/io/FileNotFoundException from L5 to L6 using L2
.catch java/io/IOException from L5 to L6 using L3
.catch java/lang/Exception from L5 to L6 using L4
.catch java/lang/Exception from L7 to L8 using L9
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/api I
bipush 19
if_icmplt L10
L0:
new java/io/FileInputStream
dup
aload 0
invokestatic com/chelpus/Utils/getFileDalvikCache(Ljava/lang/String;)Ljava/io/File;
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
astore 0
bipush 7
newarray byte
astore 2
aload 0
aload 2
invokevirtual java/io/FileInputStream/read([B)I
pop
aload 0
invokevirtual java/io/FileInputStream/close()V
L1:
iconst_0
istore 1
L11:
iload 1
iconst_3
if_icmpge L12
L5:
aload 2
iload 1
baload
iconst_3
newarray byte
dup
iconst_0
ldc_w 100
bastore
dup
iconst_1
ldc_w 101
bastore
dup
iconst_2
ldc_w 120
bastore
iload 1
baload
if_icmpeq L13
aload 2
iload 1
baload
iconst_3
newarray byte
dup
iconst_0
ldc_w 100
bastore
dup
iconst_1
ldc_w 101
bastore
dup
iconst_2
ldc_w 121
bastore
iload 1
baload
if_icmpeq L13
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "The magic value is not the expected value "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
new java/lang/String
dup
aload 2
invokespecial java/lang/String/<init>([B)V
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L6:
ldc "ART"
areturn
L12:
ldc "DALVIK"
areturn
L2:
astore 0
aload 0
invokevirtual java/io/FileNotFoundException/printStackTrace()V
L14:
ldc "UNKNOWN"
areturn
L3:
astore 0
aload 0
invokevirtual java/io/IOException/printStackTrace()V
goto L14
L4:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L7:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Althernative runtime check with java.vm.version"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
ldc "java.vm.version"
invokestatic java/lang/System/getProperty(Ljava/lang/String;)Ljava/lang/String;
astore 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
iconst_0
invokevirtual java/lang/String/charAt(I)C
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
iconst_1
if_icmple L15
L8:
ldc "ART"
areturn
L15:
ldc "DALVIK"
areturn
L9:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/api I
bipush 21
if_icmplt L16
ldc "ART"
areturn
L16:
ldc "DALVIK"
areturn
L10:
ldc "DALVIK"
areturn
L13:
iload 1
iconst_1
iadd
istore 1
goto L11
.limit locals 3
.limit stack 5
.end method

.method public static chmod(Ljava/io/File;I)I
.throws java/lang/Exception
ldc "android.os.FileUtils"
invokestatic java/lang/Class/forName(Ljava/lang/String;)Ljava/lang/Class;
ldc "setPermissions"
iconst_4
anewarray java/lang/Class
dup
iconst_0
ldc java/lang/String
aastore
dup
iconst_1
getstatic java/lang/Integer/TYPE Ljava/lang/Class;
aastore
dup
iconst_2
getstatic java/lang/Integer/TYPE Ljava/lang/Class;
aastore
dup
iconst_3
getstatic java/lang/Integer/TYPE Ljava/lang/Class;
aastore
invokevirtual java/lang/Class/getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
aconst_null
iconst_4
anewarray java/lang/Object
dup
iconst_0
aload 0
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aastore
dup
iconst_1
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_2
iconst_m1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_3
iconst_m1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
ireturn
.limit locals 2
.limit stack 6
.end method

.method public static classes_test(Ljava/io/File;)Z
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L2
.catch java/lang/Exception from L7 to L8 using L2
.catch java/io/IOException from L9 to L10 using L11
.catch java/io/IOException from L10 to L12 using L11
L0:
new java/io/FileInputStream
dup
aload 0
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
astore 3
new java/util/zip/ZipInputStream
dup
aload 3
invokespecial java/util/zip/ZipInputStream/<init>(Ljava/io/InputStream;)V
astore 4
aload 4
invokevirtual java/util/zip/ZipInputStream/getNextEntry()Ljava/util/zip/ZipEntry;
astore 2
L1:
aload 2
ifnull L7
L3:
aload 2
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "classes.dex"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L5
aload 4
invokevirtual java/util/zip/ZipInputStream/closeEntry()V
L4:
iconst_1
ireturn
L5:
aload 4
invokevirtual java/util/zip/ZipInputStream/getNextEntry()Ljava/util/zip/ZipEntry;
astore 2
L6:
goto L1
L7:
aload 4
invokevirtual java/util/zip/ZipInputStream/close()V
aload 3
invokevirtual java/io/FileInputStream/close()V
L8:
iconst_0
ireturn
L2:
astore 2
L9:
aload 0
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokestatic kellinwood/zipio/ZipInput/read(Ljava/lang/String;)Lkellinwood/zipio/ZipInput;
invokevirtual kellinwood/zipio/ZipInput/getEntries()Ljava/util/Map;
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 0
L10:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L8
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast kellinwood/zipio/ZioEntry
invokevirtual kellinwood/zipio/ZioEntry/getName()Ljava/lang/String;
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "classes.dex"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
istore 1
L12:
iload 1
ifeq L10
iconst_1
ireturn
L11:
astore 0
aload 0
invokevirtual java/io/IOException/printStackTrace()V
goto L8
.limit locals 5
.limit stack 3
.end method

.method public static transient cmd([Ljava/lang/String;)Ljava/lang/String;
.catch java/io/IOException from L0 to L1 using L2
.catch java/lang/InterruptedException from L0 to L1 using L3
.catch java/io/IOException from L4 to L5 using L2
.catch java/lang/InterruptedException from L4 to L5 using L3
.catch java/io/IOException from L6 to L7 using L2
.catch java/lang/InterruptedException from L6 to L7 using L3
.catch java/io/IOException from L8 to L9 using L2
.catch java/lang/InterruptedException from L8 to L9 using L3
.catch java/io/IOException from L10 to L11 using L2
.catch java/lang/InterruptedException from L10 to L11 using L3
.catch java/io/IOException from L12 to L13 using L2
.catch java/lang/InterruptedException from L12 to L13 using L3
.catch java/io/IOException from L14 to L15 using L2
.catch java/lang/InterruptedException from L14 to L15 using L3
iconst_0
istore 3
ldc ""
astore 5
aconst_null
astore 6
iconst_0
istore 1
aload 0
arraylength
istore 4
iconst_0
istore 2
L16:
iload 2
iload 4
if_icmpge L17
aload 0
iload 2
aaload
ldc "skipOut"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L18
iconst_1
istore 1
L18:
iload 2
iconst_1
iadd
istore 2
goto L16
L17:
aload 0
arraylength
istore 4
iload 3
istore 2
L19:
iload 2
iload 4
if_icmpge L20
aload 0
iload 2
aaload
astore 11
aload 6
astore 9
aload 5
astore 7
aload 6
astore 10
aload 5
astore 8
L0:
aload 11
ldc "skipOut"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L21
L1:
goto L22
L21:
aload 6
astore 9
aload 5
astore 7
aload 6
astore 10
aload 5
astore 8
L4:
invokestatic java/lang/Runtime/getRuntime()Ljava/lang/Runtime;
new java/lang/String
dup
aload 11
invokevirtual java/lang/String/getBytes()[B
ldc "ISO-8859-1"
invokespecial java/lang/String/<init>([BLjava/lang/String;)V
invokevirtual java/lang/Runtime/exec(Ljava/lang/String;)Ljava/lang/Process;
astore 6
L5:
aload 6
astore 9
aload 5
astore 7
aload 6
astore 10
aload 5
astore 8
L6:
new java/io/BufferedReader
dup
new java/io/InputStreamReader
dup
aload 6
invokevirtual java/lang/Process/getInputStream()Ljava/io/InputStream;
invokespecial java/io/InputStreamReader/<init>(Ljava/io/InputStream;)V
invokespecial java/io/BufferedReader/<init>(Ljava/io/Reader;)V
astore 11
L7:
iload 1
ifne L23
L24:
aload 6
astore 9
aload 5
astore 7
aload 6
astore 10
aload 5
astore 8
L8:
aload 11
invokevirtual java/io/BufferedReader/readLine()Ljava/lang/String;
astore 12
L9:
aload 12
ifnull L25
aload 6
astore 9
aload 5
astore 7
aload 6
astore 10
aload 5
astore 8
L10:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 12
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 5
L11:
goto L24
L25:
aload 6
astore 9
aload 5
astore 7
aload 6
astore 10
aload 5
astore 8
L12:
aload 6
invokevirtual java/lang/Process/waitFor()I
pop
L13:
goto L22
L2:
astore 5
aload 5
invokevirtual java/io/IOException/printStackTrace()V
aload 9
astore 6
aload 7
astore 5
goto L22
L23:
aload 6
astore 9
aload 5
astore 7
aload 6
astore 10
aload 5
astore 8
L14:
new com/chelpus/Utils
dup
ldc "w"
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
ldc2_w 2000L
invokevirtual com/chelpus/Utils/waitLP(J)V
L15:
goto L22
L3:
astore 5
aload 5
invokevirtual java/lang/InterruptedException/printStackTrace()V
aload 10
astore 6
aload 8
astore 5
goto L22
L20:
aload 6
invokevirtual java/lang/Process/destroy()V
aload 5
areturn
L22:
iload 2
iconst_1
iadd
istore 2
goto L19
.limit locals 13
.limit stack 5
.end method

.method public static transient cmdParam([Ljava/lang/String;)Ljava/lang/String;
.catch java/io/IOException from L0 to L1 using L2
.catch java/lang/InterruptedException from L0 to L1 using L3
.catch java/lang/Exception from L0 to L1 using L4
.catch java/io/IOException from L5 to L6 using L2
.catch java/lang/InterruptedException from L5 to L6 using L3
.catch java/lang/Exception from L5 to L6 using L4
.catch java/io/IOException from L7 to L8 using L2
.catch java/lang/InterruptedException from L7 to L8 using L3
.catch java/lang/Exception from L7 to L8 using L4
.catch java/io/IOException from L9 to L10 using L2
.catch java/lang/InterruptedException from L9 to L10 using L3
.catch java/lang/Exception from L9 to L10 using L4
.catch java/io/IOException from L11 to L12 using L2
.catch java/lang/InterruptedException from L11 to L12 using L3
.catch java/lang/Exception from L11 to L12 using L4
.catch java/lang/Exception from L13 to L14 using L15
.catch java/io/IOException from L13 to L14 using L2
.catch java/lang/InterruptedException from L13 to L14 using L3
.catch java/lang/Exception from L16 to L17 using L15
.catch java/io/IOException from L16 to L17 using L2
.catch java/lang/InterruptedException from L16 to L17 using L3
.catch java/lang/Exception from L18 to L19 using L15
.catch java/io/IOException from L18 to L19 using L2
.catch java/lang/InterruptedException from L18 to L19 using L3
.catch java/lang/Exception from L20 to L21 using L15
.catch java/io/IOException from L20 to L21 using L2
.catch java/lang/InterruptedException from L20 to L21 using L3
.catch java/lang/Exception from L22 to L23 using L15
.catch java/io/IOException from L22 to L23 using L2
.catch java/lang/InterruptedException from L22 to L23 using L3
.catch java/lang/Exception from L24 to L25 using L15
.catch java/io/IOException from L24 to L25 using L2
.catch java/lang/InterruptedException from L24 to L25 using L3
.catch java/lang/Exception from L26 to L27 using L15
.catch java/io/IOException from L26 to L27 using L2
.catch java/lang/InterruptedException from L26 to L27 using L3
.catch java/lang/Exception from L28 to L29 using L15
.catch java/io/IOException from L28 to L29 using L2
.catch java/lang/InterruptedException from L28 to L29 using L3
.catch java/io/IOException from L30 to L31 using L2
.catch java/lang/InterruptedException from L30 to L31 using L3
.catch java/lang/Exception from L30 to L31 using L4
ldc ""
astore 2
aconst_null
astore 6
aconst_null
astore 8
aconst_null
astore 4
aload 2
astore 5
aload 2
astore 7
aload 2
astore 3
L0:
invokestatic java/lang/Runtime/getRuntime()Ljava/lang/Runtime;
aload 0
invokevirtual java/lang/Runtime/exec([Ljava/lang/String;)Ljava/lang/Process;
astore 1
L1:
aload 1
astore 4
aload 2
astore 5
aload 1
astore 6
aload 2
astore 7
aload 1
astore 8
aload 2
astore 3
L5:
new java/io/BufferedReader
dup
new java/io/InputStreamReader
dup
aload 1
invokevirtual java/lang/Process/getInputStream()Ljava/io/InputStream;
invokespecial java/io/InputStreamReader/<init>(Ljava/io/InputStream;)V
invokespecial java/io/BufferedReader/<init>(Ljava/io/Reader;)V
astore 9
L6:
aload 2
astore 0
L32:
aload 1
astore 4
aload 0
astore 5
aload 1
astore 6
aload 0
astore 7
aload 1
astore 8
aload 0
astore 3
L7:
aload 9
invokevirtual java/io/BufferedReader/readLine()Ljava/lang/String;
astore 2
L8:
aload 2
ifnull L33
aload 1
astore 4
aload 0
astore 5
aload 1
astore 6
aload 0
astore 7
aload 1
astore 8
aload 0
astore 3
L9:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
L10:
goto L32
L33:
aload 1
astore 4
aload 0
astore 5
aload 1
astore 6
aload 0
astore 7
aload 1
astore 8
aload 0
astore 3
L11:
aload 1
invokevirtual java/lang/Process/waitFor()I
pop
L12:
aload 1
astore 4
aload 0
astore 5
aload 1
astore 6
aload 0
astore 7
L13:
new java/io/DataInputStream
dup
aload 1
invokevirtual java/lang/Process/getErrorStream()Ljava/io/InputStream;
invokespecial java/io/DataInputStream/<init>(Ljava/io/InputStream;)V
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suErrorInputStream Ljava/io/DataInputStream;
L14:
aload 1
astore 2
aload 0
astore 3
aload 1
astore 4
aload 0
astore 5
aload 1
astore 6
aload 0
astore 7
L16:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suErrorInputStream Ljava/io/DataInputStream;
ifnull L34
L17:
aload 1
astore 4
aload 0
astore 5
aload 1
astore 6
aload 0
astore 7
L18:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suErrorInputStream Ljava/io/DataInputStream;
invokevirtual java/io/DataInputStream/available()I
newarray byte
astore 2
L19:
aload 1
astore 4
aload 0
astore 5
aload 1
astore 6
aload 0
astore 7
L20:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suErrorInputStream Ljava/io/DataInputStream;
aload 2
invokevirtual java/io/DataInputStream/read([B)I
pop
L21:
aload 1
astore 4
aload 0
astore 5
aload 1
astore 6
aload 0
astore 7
L22:
new java/lang/String
dup
aload 2
invokespecial java/lang/String/<init>([B)V
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/errorOutput Ljava/lang/String;
L23:
aload 1
astore 4
aload 0
astore 5
aload 1
astore 6
aload 0
astore 7
L24:
new java/lang/String
dup
aload 2
invokespecial java/lang/String/<init>([B)V
invokevirtual java/lang/String/trim()Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L35
L25:
aload 1
astore 4
aload 0
astore 5
aload 1
astore 6
aload 0
astore 7
L26:
new java/lang/String
dup
aload 2
invokespecial java/lang/String/<init>([B)V
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/errorOutput Ljava/lang/String;
L27:
aload 0
astore 3
aload 1
astore 2
L34:
aload 2
ifnull L36
aload 2
invokevirtual java/lang/Process/destroy()V
L36:
aload 3
areturn
L35:
aload 1
astore 4
aload 0
astore 5
aload 1
astore 6
aload 0
astore 7
L28:
ldc ""
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/errorOutput Ljava/lang/String;
L29:
aload 1
astore 2
aload 0
astore 3
goto L34
L15:
astore 2
aload 1
astore 4
aload 0
astore 5
aload 1
astore 6
aload 0
astore 7
aload 1
astore 8
aload 0
astore 3
L30:
aload 2
invokevirtual java/lang/Exception/printStackTrace()V
L31:
aload 1
astore 2
aload 0
astore 3
goto L34
L2:
astore 0
aload 0
invokevirtual java/io/IOException/toString()Ljava/lang/String;
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/errorOutput Ljava/lang/String;
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
invokevirtual java/io/IOException/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 4
astore 2
aload 5
astore 3
goto L34
L3:
astore 0
aload 0
invokevirtual java/lang/InterruptedException/printStackTrace()V
aload 6
astore 2
aload 7
astore 3
goto L34
L4:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
aload 8
astore 2
goto L34
.limit locals 10
.limit stack 5
.end method

.method public static convertByteToInt(B)I
iload 0
sipush 255
iand
ireturn
.limit locals 1
.limit stack 2
.end method

.method public static convertFourBytesToInt(BBBB)I
iload 3
bipush 24
ishl
iload 2
sipush 255
iand
bipush 16
ishl
ior
iload 1
sipush 255
iand
bipush 8
ishl
ior
iload 0
sipush 255
iand
ior
ireturn
.limit locals 4
.limit stack 3
.end method

.method public static convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L4 to L5 using L2
.catch java/lang/Exception from L6 to L7 using L8
.catch java/lang/Exception from L9 to L10 using L8
.catch java/lang/Exception from L11 to L12 using L8
.catch java/lang/Exception from L13 to L14 using L8
.catch java/lang/Exception from L14 to L15 using L8
iconst_0
istore 7
aload 0
invokevirtual java/lang/String/trim()Ljava/lang/String;
astore 0
aload 0
ldc "[ \u0009]+"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
arraylength
anewarray java/lang/String
astore 8
aload 0
ldc "[ \u0009]+"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 0
iconst_0
istore 6
L0:
iload 6
aload 0
arraylength
if_icmpge L16
aload 0
iload 6
aaload
ldc "\\?+"
invokevirtual java/lang/String/matches(Ljava/lang/String;)Z
ifeq L17
L1:
aload 0
iload 6
ldc "60"
aastore
aload 3
iload 6
iconst_1
bastore
L3:
aload 0
iload 6
aaload
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
ldc "R"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L4
aload 2
iload 6
aload 0
iload 6
aaload
bipush 16
invokestatic java/lang/Integer/valueOf(Ljava/lang/String;I)Ljava/lang/Integer;
invokevirtual java/lang/Integer/byteValue()B
bastore
L4:
aload 0
iload 6
aaload
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
ldc "R"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L18
aload 2
iload 6
aload 0
iload 6
aaload
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
ldc "R"
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
bipush 16
invokestatic java/lang/Integer/valueOf(Ljava/lang/String;I)Ljava/lang/Integer;
invokevirtual java/lang/Integer/byteValue()B
bastore
L5:
aload 3
iload 6
bipush 23
bastore
goto L18
L17:
aload 3
iload 6
iconst_0
bastore
goto L3
L2:
astore 0
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L16:
aload 1
invokevirtual java/lang/String/trim()Ljava/lang/String;
astore 0
aload 0
ldc "[ \u0009]+"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
arraylength
anewarray java/lang/String
astore 1
aload 0
ldc "[ \u0009]+"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 0
iconst_0
istore 6
L6:
iload 6
aload 0
arraylength
if_icmpge L19
aload 0
iload 6
aaload
ldc "\\?+"
invokevirtual java/lang/String/matches(Ljava/lang/String;)Z
ifeq L20
L7:
aload 0
iload 6
ldc "60"
aastore
aload 5
iload 6
iconst_0
bastore
L9:
aload 0
iload 6
aaload
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
ldc "S1"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L11
L10:
aload 0
iload 6
ldc "60"
aastore
aload 5
iload 6
bipush 21
bastore
L11:
aload 0
iload 6
aaload
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
ldc "S0"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L13
L12:
aload 0
iload 6
ldc "60"
aastore
aload 5
iload 6
bipush 20
bastore
L13:
aload 0
iload 6
aaload
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
ldc "W"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L14
aload 4
iload 6
aload 0
iload 6
aaload
bipush 16
invokestatic java/lang/Integer/valueOf(Ljava/lang/String;I)Ljava/lang/Integer;
invokevirtual java/lang/Integer/byteValue()B
bastore
L14:
aload 0
iload 6
aaload
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
ldc "W"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L21
aload 4
iload 6
aload 0
iload 6
aaload
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
ldc "W"
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
bipush 16
invokestatic java/lang/Integer/valueOf(Ljava/lang/String;I)Ljava/lang/Integer;
invokevirtual java/lang/Integer/byteValue()B
bastore
L15:
aload 5
iload 6
bipush 23
bastore
goto L21
L20:
aload 5
iload 6
iconst_1
bastore
goto L9
L8:
astore 0
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L19:
aload 5
arraylength
aload 3
arraylength
if_icmpne L22
aload 2
arraylength
aload 4
arraylength
if_icmpne L22
aload 4
arraylength
iconst_4
if_icmplt L22
iload 7
istore 6
aload 2
arraylength
iconst_4
if_icmpge L23
L22:
iconst_1
istore 6
L23:
iload 6
ifeq L24
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Error: Patterns original and replaced not valid!\n- Dimensions of the original hex-string and repleced must be >3.\n- Dimensions of the original hex-string and repleced must be equal.\n- Pattern hex must be: AF 11 4B ** AA **\nCheck the template file and try again!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L24:
return
L18:
iload 6
iconst_1
iadd
istore 6
goto L0
L21:
iload 6
iconst_1
iadd
istore 6
goto L6
.limit locals 9
.limit stack 5
.end method

.method public static convertToPatchItemAuto(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/Boolean;)V
.signature "(Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;>;Ljava/util/ArrayList<Ljava/lang/String;>;Ljava/util/ArrayList<Ljava/lang/String;>;Ljava/util/ArrayList<Ljava/lang/Boolean;>;Ljava/util/ArrayList<Ljava/lang/String;>;Ljava/util/ArrayList<Ljava/lang/String;>;Ljava/util/ArrayList<Ljava/lang/Boolean;>;Ljava/lang/Boolean;)V"
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L4 to L5 using L2
.catch java/lang/Exception from L6 to L7 using L8
.catch java/lang/Exception from L9 to L10 using L8
.catch java/lang/Exception from L11 to L12 using L8
.catch java/lang/Exception from L13 to L14 using L8
.catch java/lang/Exception from L14 to L15 using L8
iconst_0
istore 8
L16:
iload 8
aload 1
invokevirtual java/util/ArrayList/size()I
if_icmpge L17
aload 1
iload 8
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/String
astore 13
aload 2
iload 8
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/String
astore 14
aload 13
astore 12
aload 14
astore 11
aload 6
iload 8
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/Boolean
invokevirtual java/lang/Boolean/booleanValue()Z
ifne L18
aload 13
astore 12
aload 14
astore 11
aload 7
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L18
aload 1
iload 8
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/String
invokestatic com/chelpus/Utils/rework(Ljava/lang/String;)Ljava/lang/String;
astore 12
aload 2
iload 8
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/String
invokestatic com/chelpus/Utils/rework(Ljava/lang/String;)Ljava/lang/String;
astore 11
L18:
aload 3
iload 8
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/Boolean
astore 13
aload 4
iload 8
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/String
astore 14
iconst_0
istore 10
aload 12
invokevirtual java/lang/String/trim()Ljava/lang/String;
astore 12
aload 12
ldc "[ \u0009]+"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
arraylength
anewarray java/lang/String
astore 15
aload 12
ldc "[ \u0009]+"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 16
aload 16
arraylength
newarray int
astore 12
aload 16
arraylength
newarray byte
astore 15
iconst_0
istore 9
L0:
iload 9
aload 16
arraylength
if_icmpge L19
aload 16
iload 9
aaload
ldc "\\?+"
invokevirtual java/lang/String/matches(Ljava/lang/String;)Z
ifeq L20
L1:
aload 16
iload 9
ldc "60"
aastore
aload 12
iload 9
iconst_1
iastore
L3:
aload 16
iload 9
aaload
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
ldc "R"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L4
aload 15
iload 9
aload 16
iload 9
aaload
bipush 16
invokestatic java/lang/Integer/valueOf(Ljava/lang/String;I)Ljava/lang/Integer;
invokevirtual java/lang/Integer/byteValue()B
bastore
L4:
aload 16
iload 9
aaload
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
ldc "R"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L21
aload 15
iload 9
aload 16
iload 9
aaload
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
ldc "R"
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
bipush 16
invokestatic java/lang/Integer/valueOf(Ljava/lang/String;I)Ljava/lang/Integer;
invokevirtual java/lang/Integer/byteValue()B
bastore
L5:
aload 12
iload 9
bipush 23
iastore
goto L21
L20:
aload 12
iload 9
iconst_0
iastore
goto L3
L2:
astore 16
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 16
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L19:
aload 11
invokevirtual java/lang/String/trim()Ljava/lang/String;
astore 11
aload 11
ldc "[ \u0009]+"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
arraylength
anewarray java/lang/String
astore 16
aload 11
ldc "[ \u0009]+"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 17
aload 17
arraylength
newarray int
astore 11
aload 17
arraylength
newarray byte
astore 16
iconst_0
istore 9
L6:
iload 9
aload 17
arraylength
if_icmpge L22
aload 17
iload 9
aaload
ldc "\\?+"
invokevirtual java/lang/String/matches(Ljava/lang/String;)Z
ifeq L23
L7:
aload 17
iload 9
ldc "60"
aastore
aload 11
iload 9
iconst_0
iastore
L9:
aload 17
iload 9
aaload
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
ldc "S1"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L11
L10:
aload 17
iload 9
ldc "60"
aastore
aload 11
iload 9
bipush 21
iastore
L11:
aload 17
iload 9
aaload
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
ldc "S0"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L13
L12:
aload 17
iload 9
ldc "60"
aastore
aload 11
iload 9
bipush 20
iastore
L13:
aload 17
iload 9
aaload
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
ldc "W"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L14
aload 16
iload 9
aload 17
iload 9
aaload
bipush 16
invokestatic java/lang/Integer/valueOf(Ljava/lang/String;I)Ljava/lang/Integer;
invokevirtual java/lang/Integer/byteValue()B
bastore
L14:
aload 17
iload 9
aaload
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
ldc "W"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L24
aload 16
iload 9
aload 17
iload 9
aaload
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
ldc "W"
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
bipush 16
invokestatic java/lang/Integer/valueOf(Ljava/lang/String;I)Ljava/lang/Integer;
invokevirtual java/lang/Integer/byteValue()B
bastore
L15:
aload 11
iload 9
bipush 23
iastore
goto L24
L23:
aload 11
iload 9
iconst_1
iastore
goto L9
L8:
astore 17
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 17
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L22:
aload 11
arraylength
aload 12
arraylength
if_icmpne L25
aload 15
arraylength
aload 16
arraylength
if_icmpne L25
aload 16
arraylength
iconst_4
if_icmplt L25
iload 10
istore 9
aload 15
arraylength
iconst_4
if_icmpge L26
L25:
iconst_1
istore 9
L26:
iload 9
ifeq L27
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Error: Patterns original and replaced not valid!\n- Dimensions of the original hex-string and repleced must be >3.\n- Dimensions of the original hex-string and repleced must be equal.\n- Pattern hex must be: AF 11 4B ** AA **\nCheck the template file and try again!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L27:
iload 9
ifne L28
aload 0
new com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto
dup
aload 15
aload 12
aload 16
aload 11
aload 13
invokevirtual java/lang/Boolean/booleanValue()Z
aload 14
aload 5
iload 8
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/String
invokespecial com/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto/<init>([B[I[B[IZLjava/lang/String;Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L28:
iload 8
iconst_1
iadd
istore 8
goto L16
L17:
return
L21:
iload 9
iconst_1
iadd
istore 9
goto L0
L24:
iload 9
iconst_1
iadd
istore 9
goto L6
.limit locals 18
.limit stack 11
.end method

.method public static convertTwoBytesToInt(BB)I
iload 1
sipush 255
iand
bipush 8
ishl
iload 0
sipush 255
iand
ior
ireturn
.limit locals 2
.limit stack 3
.end method

.method public static copyArchFiles(Ljava/util/zip/ZipInputStream;Ljava/util/jar/JarOutputStream;Ljava/util/ArrayList;)V
.signature "(Ljava/util/zip/ZipInputStream;Ljava/util/jar/JarOutputStream;Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;)V"
.throws java/io/IOException
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L4
.catch java/lang/Exception from L5 to L6 using L7
.catch java/lang/Exception from L6 to L8 using L7
.catch java/lang/Exception from L9 to L10 using L7
.catch java/lang/Exception from L11 to L12 using L7
iconst_0
istore 3
aload 2
aload 2
invokevirtual java/util/ArrayList/size()I
anewarray com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem
invokevirtual java/util/ArrayList/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
checkcast [Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;
astore 8
sipush 8192
newarray byte
astore 9
new java/util/zip/CRC32
dup
invokespecial java/util/zip/CRC32/<init>()V
astore 10
aload 10
invokevirtual java/util/zip/CRC32/reset()V
aconst_null
astore 2
L13:
aload 0
invokevirtual java/util/zip/ZipInputStream/getNextEntry()Ljava/util/zip/ZipEntry;
astore 11
aload 11
ifnull L14
aload 8
arraylength
istore 5
iconst_0
istore 4
L15:
iload 4
iload 5
if_icmpge L16
aload 8
iload 4
aaload
astore 7
aload 11
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
aload 7
getfield com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem/fileName Ljava/lang/String;
aload 7
getfield com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem/basePath Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L17
L0:
new java/util/zip/ZipEntry
dup
aload 11
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
invokespecial java/util/zip/ZipEntry/<init>(Ljava/lang/String;)V
astore 6
L1:
aload 6
aload 11
invokevirtual java/util/zip/ZipEntry/getTime()J
invokevirtual java/util/zip/ZipEntry/setTime(J)V
new java/io/File
dup
aload 7
getfield com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem/fileName Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 2
new java/io/FileInputStream
dup
aload 7
getfield com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem/fileName Ljava/lang/String;
invokespecial java/io/FileInputStream/<init>(Ljava/lang/String;)V
astore 7
aload 2
invokevirtual java/io/File/length()J
l2i
newarray byte
astore 12
aload 7
aload 12
invokevirtual java/io/FileInputStream/read([B)I
pop
aload 6
aload 2
invokevirtual java/io/File/length()J
invokevirtual java/util/zip/ZipEntry/setSize(J)V
aload 10
aload 12
invokevirtual java/util/zip/CRC32/update([B)V
aload 6
aload 10
invokevirtual java/util/zip/CRC32/getValue()J
invokevirtual java/util/zip/ZipEntry/setCrc(J)V
aload 6
aload 11
invokevirtual java/util/zip/ZipEntry/getMethod()I
invokevirtual java/util/zip/ZipEntry/setMethod(I)V
aload 7
invokevirtual java/io/FileInputStream/close()V
L3:
iconst_1
istore 3
aload 6
astore 2
L18:
iload 4
iconst_1
iadd
istore 4
goto L15
L2:
astore 7
L19:
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 7
invokevirtual java/io/PrintStream/println(Ljava/lang/Object;)V
goto L18
L16:
aload 11
invokevirtual java/util/zip/ZipEntry/getMethod()I
ifne L20
iload 3
ifne L21
new java/util/zip/ZipEntry
dup
aload 11
invokespecial java/util/zip/ZipEntry/<init>(Ljava/util/zip/ZipEntry;)V
astore 2
aload 2
iconst_0
invokevirtual java/util/zip/ZipEntry/setMethod(I)V
aload 2
aload 11
invokevirtual java/util/zip/ZipEntry/getTime()J
invokevirtual java/util/zip/ZipEntry/setTime(J)V
aload 2
aload 11
invokevirtual java/util/zip/ZipEntry/getSize()J
invokevirtual java/util/zip/ZipEntry/setCompressedSize(J)V
aload 2
aload 11
invokevirtual java/util/zip/ZipEntry/getSize()J
invokevirtual java/util/zip/ZipEntry/setSize(J)V
aload 1
aload 2
invokevirtual java/util/jar/JarOutputStream/putNextEntry(Ljava/util/zip/ZipEntry;)V
aload 10
invokevirtual java/util/zip/CRC32/reset()V
L22:
iload 3
ifeq L23
aload 8
arraylength
istore 4
iconst_0
istore 3
L24:
iload 3
iload 4
if_icmpge L25
aload 8
iload 3
aaload
astore 7
aload 11
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
aload 7
getfield com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem/fileName Ljava/lang/String;
aload 7
getfield com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem/basePath Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L26
L5:
new java/io/File
dup
aload 7
getfield com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem/fileName Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 6
new java/io/FileInputStream
dup
aload 7
getfield com/android/vending/billing/InAppBillingService/LUCK/AddFilesItem/fileName Ljava/lang/String;
invokespecial java/io/FileInputStream/<init>(Ljava/lang/String;)V
astore 7
aload 1
aload 2
invokevirtual java/util/jar/JarOutputStream/putNextEntry(Ljava/util/zip/ZipEntry;)V
L6:
aload 7
aload 9
invokevirtual java/io/FileInputStream/read([B)I
istore 5
L8:
iload 5
ifle L11
L9:
aload 1
aload 9
iconst_0
iload 5
invokevirtual java/util/jar/JarOutputStream/write([BII)V
L10:
goto L6
L7:
astore 6
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 6
invokevirtual java/io/PrintStream/println(Ljava/lang/Object;)V
L26:
iload 3
iconst_1
iadd
istore 3
goto L24
L20:
iload 3
ifne L21
new java/util/jar/JarEntry
dup
aload 11
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
invokespecial java/util/jar/JarEntry/<init>(Ljava/lang/String;)V
astore 2
aload 1
aload 2
invokevirtual java/util/jar/JarOutputStream/putNextEntry(Ljava/util/zip/ZipEntry;)V
aload 2
aload 11
invokevirtual java/util/zip/ZipEntry/getTime()J
invokevirtual java/util/zip/ZipEntry/setTime(J)V
goto L22
L11:
aload 1
invokevirtual java/util/jar/JarOutputStream/flush()V
aload 6
invokevirtual java/io/File/delete()Z
pop
aload 7
invokevirtual java/io/FileInputStream/close()V
L12:
goto L26
L25:
iconst_0
istore 3
goto L13
L23:
aload 0
aload 9
invokevirtual java/util/zip/ZipInputStream/read([B)I
istore 4
iload 4
ifle L27
aload 1
aload 9
iconst_0
iload 4
invokevirtual java/util/jar/JarOutputStream/write([BII)V
aload 10
aload 9
iconst_0
iload 4
invokevirtual java/util/zip/CRC32/update([BII)V
goto L23
L27:
aload 1
invokevirtual java/util/jar/JarOutputStream/flush()V
aload 2
aload 10
invokevirtual java/util/zip/CRC32/getValue()J
invokevirtual java/util/zip/ZipEntry/setCrc(J)V
goto L13
L14:
aload 1
invokevirtual java/util/jar/JarOutputStream/finish()V
aload 1
invokevirtual java/util/jar/JarOutputStream/close()V
return
L4:
astore 7
aload 6
astore 2
goto L19
L21:
goto L22
L17:
goto L18
.limit locals 13
.limit stack 4
.end method

.method public static copyFile(Ljava/io/File;Ljava/io/File;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/io/IOException from L1 to L3 using L4
.catch all from L1 to L3 using L5
.catch java/io/IOException from L6 to L7 using L4
.catch all from L6 to L7 using L5
.catch java/lang/Exception from L8 to L9 using L2
.catch java/lang/Exception from L10 to L11 using L2
.catch java/lang/Exception from L12 to L13 using L2
.catch java/lang/Exception from L14 to L15 using L2
.catch java/lang/Exception from L15 to L16 using L2
.catch java/lang/Exception from L16 to L17 using L2
.catch java/lang/Exception from L17 to L18 using L2
.catch java/lang/Exception from L18 to L19 using L2
.catch all from L20 to L21 using L5
.catch all from L22 to L23 using L5
.catch all from L24 to L25 using L5
.catch all from L25 to L26 using L5
.catch all from L26 to L27 using L5
.catch all from L27 to L28 using L5
.catch all from L28 to L29 using L5
.catch java/lang/Exception from L30 to L31 using L2
.catch java/lang/Exception from L32 to L33 using L2
.catch all from L34 to L35 using L5
.catch java/lang/Exception from L36 to L37 using L2
.catch java/lang/Exception from L38 to L39 using L2
.catch java/lang/Exception from L39 to L40 using L2
.catch all from L40 to L41 using L5
.catch java/lang/Exception from L42 to L43 using L2
.catch java/lang/Exception from L44 to L45 using L2
iconst_0
istore 3
iconst_0
istore 2
L0:
new java/io/FileInputStream
dup
aload 0
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
invokevirtual java/io/FileInputStream/getChannel()Ljava/nio/channels/FileChannel;
astore 10
new java/io/FileOutputStream
dup
aload 1
invokespecial java/io/FileOutputStream/<init>(Ljava/io/File;)V
invokevirtual java/io/FileOutputStream/getChannel()Ljava/nio/channels/FileChannel;
astore 11
L1:
aload 10
invokevirtual java/nio/channels/FileChannel/size()J
lstore 6
L3:
lconst_0
lstore 4
L46:
lload 4
lload 6
lcmp
ifge L47
L6:
aload 10
lload 4
ldc_w 67076096
i2l
aload 11
invokevirtual java/nio/channels/FileChannel/transferTo(JJLjava/nio/channels/WritableByteChannel;)J
lstore 8
L7:
lload 4
lload 8
ladd
lstore 4
goto L46
L47:
aload 10
ifnull L9
L8:
aload 10
invokevirtual java/nio/channels/FileChannel/close()V
L9:
iload 2
istore 3
aload 11
ifnull L12
L10:
aload 11
invokevirtual java/nio/channels/FileChannel/close()V
L11:
iload 2
istore 3
L12:
aload 0
invokevirtual java/io/File/length()J
aload 1
invokevirtual java/io/File/length()J
lcmp
ifeq L48
aload 1
invokevirtual java/io/File/delete()Z
pop
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/su Z
ifeq L48
L13:
iload 3
ifne L48
L14:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L44
aload 0
invokevirtual java/io/File/getAbsoluteFile()Ljava/io/File;
invokevirtual java/io/File/toString()Ljava/lang/String;
astore 10
aload 1
invokevirtual java/io/File/getAbsoluteFile()Ljava/io/File;
invokevirtual java/io/File/toString()Ljava/lang/String;
astore 11
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "dd"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "if="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 10
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "of="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 11
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
new java/io/File
dup
aload 11
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L15
aload 0
invokevirtual java/io/File/length()J
aload 1
invokevirtual java/io/File/length()J
lcmp
ifeq L16
L15:
iconst_4
anewarray java/lang/String
dup
iconst_0
ldc "toolbox"
aastore
dup
iconst_1
ldc "dd"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "if="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 10
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "of="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 11
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L16:
new java/io/File
dup
aload 11
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L17
aload 0
invokevirtual java/io/File/length()J
aload 1
invokevirtual java/io/File/length()J
lcmp
ifeq L18
L17:
iconst_4
anewarray java/lang/String
dup
iconst_0
ldc "busybox"
aastore
dup
iconst_1
ldc "dd"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "if="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 10
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "of="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 11
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L18:
aload 0
invokevirtual java/io/File/length()J
lstore 4
aload 1
invokevirtual java/io/File/length()J
lstore 6
L19:
lload 4
lload 6
lcmp
ifne L42
L48:
return
L4:
astore 12
L20:
aload 12
invokevirtual java/io/IOException/printStackTrace()V
L21:
iload 3
istore 2
L22:
aload 12
invokevirtual java/io/IOException/toString()Ljava/lang/String;
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "no space left"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L24
L23:
iconst_1
istore 2
L24:
aload 12
invokevirtual java/io/IOException/toString()Ljava/lang/String;
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "no space left"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L49
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "try copy with root"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/su Z
ifeq L49
invokestatic com/chelpus/Utils/getRootUid()Z
ifeq L40
aload 0
invokevirtual java/io/File/getAbsoluteFile()Ljava/io/File;
invokevirtual java/io/File/toString()Ljava/lang/String;
astore 12
aload 1
invokevirtual java/io/File/getAbsoluteFile()Ljava/io/File;
invokevirtual java/io/File/toString()Ljava/lang/String;
astore 13
iconst_1
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "dd if="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 12
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " of="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 13
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmd([Ljava/lang/String;)Ljava/lang/String;
pop
new java/io/File
dup
aload 13
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L25
aload 0
invokevirtual java/io/File/length()J
aload 1
invokevirtual java/io/File/length()J
lcmp
ifeq L26
L25:
iconst_1
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "toolbox dd if="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 12
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " of="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 13
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmd([Ljava/lang/String;)Ljava/lang/String;
pop
L26:
new java/io/File
dup
aload 13
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L27
aload 0
invokevirtual java/io/File/length()J
aload 1
invokevirtual java/io/File/length()J
lcmp
ifeq L28
L27:
iconst_1
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "busybox dd if="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 12
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " of="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 13
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmd([Ljava/lang/String;)Ljava/lang/String;
pop
L28:
aload 0
invokevirtual java/io/File/length()J
lstore 4
aload 1
invokevirtual java/io/File/length()J
lstore 6
L29:
lload 4
lload 6
lcmp
ifne L34
L49:
aload 10
ifnull L31
L30:
aload 10
invokevirtual java/nio/channels/FileChannel/close()V
L31:
iload 2
istore 3
aload 11
ifnull L12
L32:
aload 11
invokevirtual java/nio/channels/FileChannel/close()V
L33:
iload 2
istore 3
goto L12
L2:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
return
L34:
aload 1
invokevirtual java/io/File/delete()Z
pop
L35:
goto L49
L5:
astore 0
aload 10
ifnull L37
L36:
aload 10
invokevirtual java/nio/channels/FileChannel/close()V
L37:
aload 11
ifnull L39
L38:
aload 11
invokevirtual java/nio/channels/FileChannel/close()V
L39:
aload 0
athrow
L40:
aload 0
invokevirtual java/io/File/getAbsoluteFile()Ljava/io/File;
invokevirtual java/io/File/toString()Ljava/lang/String;
aload 1
invokevirtual java/io/File/getAbsoluteFile()Ljava/io/File;
invokevirtual java/io/File/toString()Ljava/lang/String;
iconst_1
iconst_1
invokestatic com/chelpus/Utils/copyFile(Ljava/lang/String;Ljava/lang/String;ZZ)Z
pop
L41:
goto L49
L42:
aload 1
invokevirtual java/io/File/delete()Z
pop
L43:
return
L44:
aload 0
invokevirtual java/io/File/getAbsoluteFile()Ljava/io/File;
invokevirtual java/io/File/toString()Ljava/lang/String;
aload 1
invokevirtual java/io/File/getAbsoluteFile()Ljava/io/File;
invokevirtual java/io/File/toString()Ljava/lang/String;
iconst_1
iconst_1
invokestatic com/chelpus/Utils/copyFile(Ljava/lang/String;Ljava/lang/String;ZZ)Z
pop
L45:
return
.limit locals 14
.limit stack 6
.end method

.method public static copyFile(Ljava/lang/String;Ljava/lang/String;ZZ)Z
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L2
.catch java/lang/Exception from L7 to L8 using L9
.catch java/lang/Exception from L10 to L11 using L2
.catch java/lang/Exception from L11 to L12 using L2
.catch java/lang/Exception from L12 to L13 using L2
.catch java/lang/Exception from L13 to L14 using L2
.catch java/lang/Exception from L15 to L16 using L2
.catch java/lang/Exception from L17 to L18 using L19
iconst_1
istore 5
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifne L20
iload 5
istore 4
L0:
aload 0
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifeq L21
L1:
iload 2
ifeq L4
L3:
aload 1
ldc "RW"
invokestatic com/chelpus/Utils/remount(Ljava/lang/String;Ljava/lang/String;)Z
pop
L4:
ldc ""
astore 6
iload 3
ifeq L10
L5:
aload 0
invokestatic com/chelpus/Utils/getPermissions(Ljava/lang/String;)Ljava/lang/String;
astore 6
L6:
aload 6
ifnull L8
L7:
aload 6
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L22
L8:
ldc "777"
astore 6
L10:
invokestatic com/chelpus/Utils/exitRoot()V
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "dd if="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " of="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
pop
aload 1
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifne L11
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "toolbox dd if="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " of="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
pop
L11:
aload 1
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifne L12
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "busybox dd if="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " of="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
pop
L12:
aload 1
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifne L13
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/busybox dd if="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " of="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
pop
L13:
aload 1
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifne L14
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/busybox cp -fp "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
pop
L14:
iload 5
istore 4
iload 3
ifeq L21
L15:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chmod "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
L16:
iload 5
istore 4
goto L21
L9:
astore 6
ldc "777"
astore 6
goto L10
L2:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
iconst_0
istore 4
goto L21
L20:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 7
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 8
iload 2
ifeq L23
aload 1
ldc "RW"
invokestatic com/chelpus/Utils/remount(Ljava/lang/String;Ljava/lang/String;)Z
pop
L23:
ldc ""
astore 6
iload 3
ifeq L24
aload 0
invokestatic com/chelpus/Utils/getPermissions(Ljava/lang/String;)Ljava/lang/String;
astore 6
aload 6
ifnull L18
L17:
aload 6
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L25
L18:
ldc "777"
astore 6
L24:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "dd"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "if="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "of="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L26
aload 7
invokevirtual java/io/File/length()J
aload 8
invokevirtual java/io/File/length()J
lcmp
ifeq L27
L26:
iconst_4
anewarray java/lang/String
dup
iconst_0
ldc "toolbox"
aastore
dup
iconst_1
ldc "dd"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "if="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "of="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L27:
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L28
aload 7
invokevirtual java/io/File/length()J
aload 8
invokevirtual java/io/File/length()J
lcmp
ifeq L29
L28:
iconst_4
anewarray java/lang/String
dup
iconst_0
ldc "busybox"
aastore
dup
iconst_1
ldc "dd"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "if="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "of="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L29:
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L30
aload 7
invokevirtual java/io/File/length()J
aload 8
invokevirtual java/io/File/length()J
lcmp
ifeq L31
L30:
iconst_4
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/busybox"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_1
ldc "dd"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "if="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "of="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L31:
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L32
aload 7
invokevirtual java/io/File/length()J
aload 8
invokevirtual java/io/File/length()J
lcmp
ifeq L33
L32:
aload 7
aload 8
invokestatic com/chelpus/Utils/copyFile(Ljava/io/File;Ljava/io/File;)V
L33:
aload 7
invokevirtual java/io/File/length()J
aload 8
invokevirtual java/io/File/length()J
lcmp
ifne L34
iload 3
ifeq L35
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
aload 6
aastore
dup
iconst_2
aload 1
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L35:
iconst_1
ireturn
L25:
goto L24
L19:
astore 6
ldc "777"
astore 6
goto L24
L34:
aload 8
invokevirtual java/io/File/delete()Z
pop
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "LuckyPatcher (cmdCopy): error copy... not enougth space to destination."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iconst_0
ireturn
L21:
iload 4
ireturn
L22:
goto L10
.limit locals 9
.limit stack 6
.end method

.method public static copyFolder(Ljava/io/File;Ljava/io/File;)V
.throws java/lang/Exception
aload 0
invokevirtual java/io/File/isDirectory()Z
ifeq L0
aload 1
invokevirtual java/io/File/exists()Z
ifne L1
aload 1
invokevirtual java/io/File/mkdir()Z
pop
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Directory copied from "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "  to "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L1:
aload 0
invokevirtual java/io/File/list()[Ljava/lang/String;
astore 4
aload 4
arraylength
ifle L2
aload 4
arraylength
istore 3
iconst_0
istore 2
L3:
iload 2
iload 3
if_icmpge L2
aload 4
iload 2
aaload
astore 5
new java/io/File
dup
aload 0
aload 5
invokespecial java/io/File/<init>(Ljava/io/File;Ljava/lang/String;)V
new java/io/File
dup
aload 1
aload 5
invokespecial java/io/File/<init>(Ljava/io/File;Ljava/lang/String;)V
invokestatic com/chelpus/Utils/copyFolder(Ljava/io/File;Ljava/io/File;)V
iload 2
iconst_1
iadd
istore 2
goto L3
L0:
aload 0
aload 1
invokestatic com/chelpus/Utils/copyFile(Ljava/io/File;Ljava/io/File;)V
L2:
return
.limit locals 6
.limit stack 5
.end method

.method public static create_DC_root(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
.catch java/lang/Exception from L6 to L7 using L5
.catch java/lang/Exception from L8 to L9 using L5
.catch java/lang/Exception from L10 to L11 using L5
.catch java/lang/Exception from L11 to L12 using L13
.catch java/lang/Exception from L14 to L15 using L13
.catch java/lang/Exception from L16 to L17 using L13
.catch java/lang/Exception from L18 to L19 using L13
.catch java/lang/Exception from L20 to L21 using L13
.catch java/lang/Exception from L21 to L22 using L13
.catch java/lang/Exception from L23 to L24 using L5
.catch java/lang/Exception from L25 to L26 using L5
.catch java/lang/Exception from L27 to L28 using L5
.catch java/lang/Exception from L29 to L30 using L5
.catch java/io/IOException from L31 to L32 using L33
.catch java/lang/Exception from L34 to L35 using L13
.catch java/lang/Exception from L36 to L37 using L13
.catch java/lang/Exception from L38 to L39 using L5
.catch java/lang/RuntimeException from L40 to L41 using L42
.catch java/lang/Exception from L40 to L41 using L43
.catch java/lang/Exception from L44 to L45 using L5
.catch java/lang/Exception from L46 to L47 using L5
.catch java/lang/Exception from L48 to L49 using L5
.catch java/lang/Exception from L50 to L51 using L5
.catch java/lang/Exception from L52 to L53 using L5
.catch java/lang/Exception from L54 to L55 using L5
.catch java/lang/Exception from L56 to L57 using L5
.catch java/lang/Exception from L58 to L59 using L5
.catch java/lang/Exception from L60 to L61 using L5
.catch java/lang/Exception from L62 to L63 using L5
.catch java/lang/Exception from L64 to L65 using L5
iconst_0
istore 9
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
aload 0
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
ldc "0:0"
aastore
dup
iconst_2
aload 0
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
ldc "0.0"
aastore
dup
iconst_2
aload 0
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
aconst_null
astore 12
L0:
aload 1
invokestatic com/chelpus/Utils/getFileDalvikCacheName(Ljava/lang/String;)Ljava/io/File;
astore 11
L1:
aload 11
astore 12
L66:
aload 3
astore 11
aload 3
ifnonnull L67
aload 1
aload 2
invokestatic com/chelpus/Utils/getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 11
L67:
aload 11
ifnull L68
new java/io/File
dup
aload 11
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L68
aload 11
ldc "RW"
invokestatic com/chelpus/Utils/remount(Ljava/lang/String;Ljava/lang/String;)Z
pop
new java/io/File
dup
aload 11
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L68:
iconst_0
istore 10
iconst_0
istore 6
iconst_0
istore 7
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/getName()Ljava/lang/String;
pop
aload 1
aload 2
invokestatic com/chelpus/Utils/getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
pop
aload 1
invokestatic com/chelpus/Utils/getFileDalvikCacheName(Ljava/lang/String;)Ljava/io/File;
pop
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc "odex"
invokestatic com/chelpus/Utils/changeExtension(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
pop
aconst_null
astore 11
aconst_null
astore 14
aconst_null
astore 13
iconst_0
istore 8
aload 11
astore 3
iload 10
istore 5
L3:
invokestatic com/chelpus/Utils/getCurrentRuntimeValue()Ljava/lang/String;
ldc "ART"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L69
L4:
iconst_1
istore 4
L70:
iload 4
istore 8
aload 11
astore 3
iload 10
istore 5
L6:
aload 12
invokevirtual java/io/File/exists()Z
ifeq L9
L7:
iload 4
istore 8
aload 11
astore 3
iload 10
istore 5
L8:
aload 12
invokevirtual java/io/File/delete()Z
pop
L9:
iload 4
ifeq L40
iload 4
istore 8
aload 11
astore 3
iload 10
istore 5
L10:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "dex-opt-art"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L11:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "try create oat with DexFile:"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L12:
ldc ""
astore 0
L14:
aload 12
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
ldc "/arm/"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L16
L15:
ldc "arm"
astore 0
L16:
aload 12
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
ldc "/arm64/"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L18
L17:
ldc "arm64"
astore 0
L18:
aload 12
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
ldc "/x86/"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L20
L19:
ldc "x86"
astore 0
L20:
aload 12
invokevirtual java/io/File/delete()Z
pop
aload 12
invokevirtual java/io/File/exists()Z
ifeq L21
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "rm"
aastore
dup
iconst_1
aload 12
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L21:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "try create oat with dex2oat:"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
iconst_4
anewarray java/lang/String
dup
iconst_0
ldc "dex2oat"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "--dex-file="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "--oat-file="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 12
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "--instruction-set="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "end"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 12
invokevirtual java/io/File/exists()Z
ifeq L34
aload 12
invokevirtual java/io/File/length()J
lconst_0
lcmp
ifeq L34
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "oat created with dex2oat - length="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 12
invokevirtual java/io/File/length()J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L22:
iload 7
istore 6
aload 13
astore 0
L71:
iload 4
istore 8
aload 0
astore 3
iload 6
istore 5
L23:
aload 1
ldc "/system"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L72
L24:
iload 4
istore 8
aload 0
astore 3
iload 6
istore 5
L25:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "0644"
aastore
dup
iconst_2
aload 12
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L26:
iload 4
istore 8
aload 0
astore 3
iload 6
istore 5
L27:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
ldc "0.0"
aastore
dup
iconst_2
aload 12
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L28:
iload 4
istore 8
aload 0
astore 3
iload 6
istore 5
L29:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
ldc "0:0"
aastore
dup
iconst_2
aload 12
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L30:
iload 6
istore 5
L73:
aload 0
ifnull L32
L31:
aload 0
invokevirtual dalvik/system/DexFile/close()V
L32:
iconst_1
ifne L74
iconst_3
istore 6
L75:
iload 6
ireturn
L2:
astore 11
aload 11
invokevirtual java/lang/Exception/printStackTrace()V
goto L66
L69:
iconst_0
istore 4
goto L70
L34:
aload 12
invokevirtual java/io/File/delete()Z
pop
L35:
aload 13
astore 0
iload 7
istore 6
L36:
aload 12
invokevirtual java/io/File/exists()Z
ifeq L71
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "rm"
aastore
dup
iconst_1
aload 12
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L37:
aload 13
astore 0
iload 7
istore 6
goto L71
L13:
astore 0
iload 4
istore 8
aload 11
astore 3
iload 10
istore 5
L38:
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L39:
aload 13
astore 0
iload 7
istore 6
goto L71
L5:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
iload 8
istore 4
aload 3
astore 0
goto L73
L40:
aload 12
invokevirtual java/io/File/delete()Z
pop
aload 1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/temp.dex"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
iconst_0
invokestatic dalvik/system/DexFile/loadDex(Ljava/lang/String;Ljava/lang/String;I)Ldalvik/system/DexFile;
astore 3
L41:
aload 3
astore 11
L76:
iload 4
istore 8
aload 11
astore 3
iload 10
istore 5
iload 6
istore 7
L44:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/temp.dex"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 12
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokestatic com/chelpus/Utils/dalvikvm_copyFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
ifeq L77
L45:
iload 4
istore 8
aload 11
astore 3
iload 10
istore 5
L46:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Free space for dex enougth."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L47:
iconst_1
istore 7
L77:
iload 4
istore 8
aload 11
astore 3
iload 7
istore 5
L48:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/temp.dex"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L49:
aload 11
astore 0
iload 7
istore 6
iload 4
istore 8
aload 11
astore 3
iload 7
istore 5
L50:
aload 12
invokevirtual java/io/File/exists()Z
ifeq L71
L51:
aload 11
astore 0
iload 7
istore 6
iload 4
istore 8
aload 11
astore 3
iload 7
istore 5
L52:
aload 12
invokevirtual java/io/File/length()J
lconst_0
lcmp
ifne L71
L53:
iload 4
istore 8
aload 11
astore 3
iload 7
istore 5
L54:
aload 12
invokevirtual java/io/File/delete()Z
pop
L55:
aload 11
astore 0
iload 7
istore 6
goto L71
L42:
astore 13
iload 4
istore 8
aload 11
astore 3
iload 10
istore 5
L56:
aload 13
invokevirtual java/lang/RuntimeException/printStackTrace()V
L57:
aload 14
astore 11
goto L76
L43:
astore 13
iload 4
istore 8
aload 11
astore 3
iload 10
istore 5
L58:
aload 13
invokevirtual java/lang/Exception/printStackTrace()V
L59:
aload 14
astore 11
goto L76
L72:
iload 4
istore 8
aload 0
astore 3
iload 6
istore 5
L60:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "0644"
aastore
dup
iconst_2
aload 12
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L61:
iload 4
istore 8
aload 0
astore 3
iload 6
istore 5
L62:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "1000."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
aload 12
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L63:
iload 4
istore 8
aload 0
astore 3
iload 6
istore 5
L64:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "1000:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
aload 12
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L65:
iload 6
istore 5
goto L73
L33:
astore 0
aload 0
invokevirtual java/io/IOException/printStackTrace()V
goto L32
L74:
iload 4
ifeq L78
iconst_0
ifeq L78
iconst_2
ireturn
L78:
iload 4
ifeq L79
iload 5
ifne L79
iconst_1
ireturn
L79:
iload 4
ifeq L80
iload 5
ifeq L80
iconst_0
ireturn
L80:
iload 4
ifne L81
iload 5
ifne L81
iconst_1
ireturn
L81:
iload 9
istore 6
iload 4
ifne L75
iload 9
istore 6
iload 5
ifeq L75
iconst_0
ireturn
.limit locals 15
.limit stack 6
.end method

.method public static create_ODEX_root(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.signature "(Ljava/lang/String;Ljava/util/ArrayList<Ljava/io/File;>;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I"
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L2
.catch java/lang/Exception from L7 to L8 using L2
.catch java/lang/Exception from L9 to L10 using L2
.catch java/lang/Exception from L11 to L12 using L2
.catch java/io/IOException from L13 to L14 using L15
.catch java/lang/Exception from L13 to L14 using L2
.catch java/lang/Exception from L16 to L17 using L2
.catch java/io/IOException from L18 to L19 using L20
.catch java/lang/Exception from L21 to L22 using L2
.catch java/lang/Exception from L23 to L24 using L2
.catch java/lang/Exception from L25 to L26 using L2
.catch java/lang/Exception from L27 to L28 using L2
.catch java/lang/Exception from L29 to L30 using L2
.catch java/lang/Exception from L31 to L32 using L2
.catch java/lang/Exception from L33 to L34 using L2
.catch java/lang/Exception from L35 to L36 using L2
.catch java/lang/Exception from L37 to L38 using L2
.catch java/lang/Exception from L39 to L40 using L2
.catch java/lang/Exception from L41 to L42 using L2
.catch java/lang/Exception from L43 to L44 using L2
.catch java/lang/Exception from L45 to L46 using L2
.catch java/lang/Exception from L47 to L48 using L2
.catch java/lang/Exception from L49 to L50 using L51
.catch java/lang/Exception from L52 to L53 using L51
.catch java/lang/Exception from L54 to L55 using L51
.catch java/lang/Exception from L56 to L57 using L51
.catch java/lang/Exception from L57 to L58 using L51
.catch java/lang/Exception from L59 to L60 using L2
.catch java/lang/Exception from L61 to L62 using L2
.catch java/lang/Exception from L63 to L64 using L2
.catch java/lang/Exception from L65 to L66 using L2
.catch java/lang/Exception from L67 to L68 using L2
.catch java/lang/Exception from L69 to L70 using L2
.catch java/lang/Exception from L71 to L72 using L2
.catch java/lang/Exception from L73 to L74 using L2
.catch java/lang/Exception from L75 to L76 using L2
.catch java/lang/Exception from L77 to L78 using L2
.catch java/lang/Exception from L79 to L80 using L2
.catch java/lang/Exception from L81 to L82 using L2
.catch java/lang/Exception from L83 to L84 using L2
.catch java/lang/Exception from L85 to L86 using L2
.catch java/lang/Exception from L87 to L88 using L2
.catch java/lang/Exception from L89 to L90 using L2
.catch java/lang/Exception from L91 to L92 using L2
.catch java/lang/Exception from L93 to L94 using L2
.catch java/lang/Exception from L95 to L96 using L2
.catch java/lang/Exception from L97 to L98 using L2
.catch java/lang/Exception from L99 to L100 using L2
.catch java/lang/Exception from L100 to L101 using L102
.catch java/lang/Exception from L103 to L104 using L102
.catch java/lang/Exception from L105 to L106 using L102
.catch java/lang/Exception from L107 to L108 using L2
.catch java/lang/Exception from L109 to L110 using L51
.catch java/lang/Exception from L111 to L112 using L2
.catch java/lang/Exception from L113 to L114 using L2
.catch java/lang/Exception from L115 to L116 using L2
.catch java/lang/Exception from L117 to L118 using L2
.catch java/lang/Exception from L119 to L120 using L2
.catch java/lang/Exception from L121 to L122 using L2
.catch java/lang/Exception from L123 to L124 using L2
.catch java/lang/Exception from L125 to L126 using L2
.catch java/lang/Exception from L127 to L128 using L2
.catch java/lang/Exception from L129 to L130 using L2
.catch java/lang/Exception from L131 to L132 using L2
.catch java/lang/Exception from L133 to L134 using L2
.catch java/lang/Exception from L135 to L136 using L2
.catch java/lang/Exception from L137 to L138 using L2
.catch java/lang/Exception from L139 to L140 using L2
.catch java/lang/Exception from L141 to L142 using L2
.catch java/lang/Exception from L143 to L144 using L2
.catch java/lang/Exception from L145 to L146 using L2
.catch java/lang/Exception from L147 to L148 using L2
.catch java/lang/Exception from L149 to L150 using L2
.catch java/lang/Exception from L151 to L152 using L2
.catch java/lang/Exception from L153 to L154 using L2
.catch java/lang/Exception from L155 to L156 using L2
.catch java/lang/Exception from L157 to L158 using L2
.catch java/lang/Exception from L159 to L160 using L2
.catch java/lang/Exception from L161 to L162 using L2
.catch java/lang/Exception from L163 to L164 using L2
.catch java/lang/Exception from L165 to L166 using L2
.catch java/lang/Exception from L167 to L168 using L2
.catch java/lang/Exception from L169 to L170 using L2
.catch java/lang/Exception from L171 to L172 using L2
.catch java/lang/Exception from L173 to L174 using L2
.catch java/lang/Exception from L175 to L176 using L2
.catch java/lang/Exception from L177 to L178 using L2
.catch java/lang/Exception from L179 to L180 using L2
.catch java/lang/Exception from L181 to L182 using L2
.catch java/lang/Exception from L183 to L184 using L2
.catch java/lang/Exception from L185 to L186 using L2
.catch java/lang/Exception from L187 to L188 using L2
.catch java/lang/Exception from L189 to L190 using L2
.catch java/lang/Exception from L191 to L192 using L2
.catch java/lang/Exception from L193 to L194 using L2
.catch java/lang/Exception from L195 to L196 using L2
.catch java/lang/Exception from L197 to L198 using L2
.catch java/lang/Exception from L199 to L200 using L2
.catch java/lang/Exception from L201 to L202 using L2
.catch java/lang/Exception from L203 to L204 using L2
.catch java/lang/Exception from L205 to L206 using L2
.catch java/lang/Exception from L207 to L208 using L2
.catch java/lang/Exception from L209 to L210 using L2
.catch java/lang/Exception from L211 to L212 using L2
.catch java/lang/Exception from L213 to L214 using L2
.catch java/lang/Exception from L215 to L216 using L2
.catch java/lang/Exception from L217 to L218 using L2
.catch java/lang/Exception from L219 to L220 using L2
.catch java/lang/Exception from L221 to L222 using L2
.catch java/lang/Exception from L223 to L224 using L2
.catch java/lang/Exception from L225 to L226 using L2
.catch java/lang/Exception from L227 to L228 using L2
.catch java/lang/Exception from L229 to L230 using L2
.catch java/lang/Exception from L231 to L232 using L2
.catch java/lang/Exception from L233 to L234 using L2
.catch java/lang/Exception from L235 to L236 using L2
.catch java/lang/Exception from L237 to L238 using L2
.catch java/lang/Exception from L239 to L240 using L2
.catch java/lang/Exception from L241 to L242 using L2
.catch java/lang/Exception from L243 to L244 using L2
.catch java/lang/RuntimeException from L244 to L245 using L246
.catch java/lang/Exception from L244 to L245 using L247
.catch java/lang/Exception from L248 to L249 using L2
.catch java/lang/Exception from L250 to L251 using L2
.catch java/lang/Exception from L252 to L253 using L2
.catch java/lang/Exception from L254 to L255 using L2
.catch java/lang/Exception from L256 to L257 using L2
.catch java/lang/Exception from L258 to L259 using L2
.catch java/lang/Exception from L260 to L261 using L2
.catch java/lang/Exception from L262 to L263 using L2
.catch java/lang/Exception from L264 to L265 using L2
.catch java/lang/Exception from L266 to L267 using L2
.catch java/lang/Exception from L268 to L269 using L2
.catch java/lang/Exception from L270 to L271 using L2
.catch java/lang/Exception from L272 to L273 using L2
.catch java/lang/Exception from L274 to L275 using L2
.catch java/lang/Exception from L276 to L277 using L2
.catch java/lang/Exception from L278 to L279 using L2
.catch java/lang/Exception from L280 to L281 using L2
.catch java/lang/Exception from L282 to L283 using L2
.catch java/lang/Exception from L284 to L285 using L2
.catch java/lang/Exception from L286 to L287 using L2
.catch java/lang/Exception from L288 to L289 using L2
.catch java/lang/Exception from L290 to L291 using L2
.catch java/lang/Exception from L292 to L293 using L2
.catch java/lang/Exception from L294 to L295 using L2
.catch java/lang/Exception from L296 to L297 using L2
.catch java/lang/Exception from L298 to L299 using L2
.catch java/lang/Exception from L300 to L301 using L2
.catch java/lang/Exception from L302 to L303 using L2
.catch java/lang/Exception from L304 to L305 using L2
.catch java/lang/Exception from L306 to L307 using L2
.catch java/lang/Exception from L308 to L309 using L2
.catch java/lang/Exception from L310 to L311 using L2
.catch java/lang/Exception from L312 to L313 using L2
.catch java/lang/Exception from L314 to L315 using L2
.catch java/lang/Exception from L316 to L317 using L2
.catch java/lang/Exception from L318 to L319 using L2
.catch java/lang/Exception from L320 to L321 using L2
.catch java/lang/Exception from L322 to L323 using L2
.catch java/lang/Exception from L324 to L325 using L2
.catch java/lang/Exception from L326 to L327 using L2
.catch java/lang/Exception from L328 to L329 using L2
.catch java/lang/Exception from L330 to L331 using L2
.catch java/lang/Exception from L332 to L333 using L2
.catch java/lang/Exception from L334 to L335 using L2
.catch java/lang/Exception from L336 to L337 using L2
.catch java/lang/Exception from L338 to L339 using L2
.catch java/lang/Exception from L340 to L341 using L2
iconst_0
istore 17
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
aload 0
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
ldc "0:0"
aastore
dup
iconst_2
aload 0
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
ldc "0.0"
aastore
dup
iconst_2
aload 0
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
aload 4
astore 21
aload 4
ifnonnull L342
aload 2
aload 3
invokestatic com/chelpus/Utils/getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 21
L342:
iconst_0
istore 6
iconst_0
istore 14
iconst_0
istore 13
iconst_1
istore 12
iconst_1
istore 19
iconst_1
istore 16
iconst_0
istore 8
iconst_0
istore 18
iconst_0
istore 7
new java/io/File
dup
aload 2
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/getName()Ljava/lang/String;
astore 25
aload 2
aload 3
invokestatic com/chelpus/Utils/getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 27
aload 2
invokestatic com/chelpus/Utils/getFileDalvikCacheName(Ljava/lang/String;)Ljava/io/File;
astore 26
new java/io/File
dup
aload 2
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/getName()Ljava/lang/String;
astore 22
aload 22
ldc "odex"
invokestatic com/chelpus/Utils/changeExtension(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 4
aload 22
ldc "dex"
invokestatic com/chelpus/Utils/changeExtension(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
pop
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 30
aconst_null
astore 24
aconst_null
astore 22
aconst_null
astore 23
iconst_0
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L0:
invokestatic com/chelpus/Utils/getCurrentRuntimeValue()Ljava/lang/String;
ldc "ART"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L343
L1:
iconst_1
istore 9
L344:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L3:
aload 1
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 29
L4:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L5:
aload 29
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L345
L6:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L7:
aload 29
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
astore 28
L8:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L9:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 28
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L10:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L11:
aload 28
invokevirtual java/io/File/exists()Z
istore 20
L12:
iload 20
ifne L346
iconst_0
ifeq L14
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L13:
new java/lang/NullPointerException
dup
invokespecial java/lang/NullPointerException/<init>()V
athrow
L14:
iconst_4
istore 5
L347:
iload 5
ireturn
L343:
iconst_0
istore 9
goto L344
L15:
astore 0
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L16:
aload 0
invokevirtual java/io/IOException/printStackTrace()V
L17:
goto L14
L2:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
iload 10
istore 16
iload 11
istore 8
iload 5
istore 7
aload 4
astore 1
iload 15
istore 6
L348:
aload 1
ifnull L19
L18:
aload 1
invokevirtual dalvik/system/DexFile/close()V
L19:
iload 8
ifne L349
iconst_3
ireturn
L346:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L21:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 28
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L22:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L23:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "1000."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 28
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L24:
goto L4
L345:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L25:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/AndroidManifest.xml"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L26:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L27:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "1000."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/AndroidManifest.xml"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L28:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L29:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 25
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L30:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L31:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 25
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L34
L32:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L33:
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "rm"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 25
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L34:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L35:
aload 2
invokestatic com/chelpus/Utils/getFileDalvikCacheName(Ljava/lang/String;)Ljava/io/File;
astore 28
L36:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L37:
aload 28
invokevirtual java/io/File/getName()Ljava/lang/String;
pop
L38:
iload 9
ifeq L350
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L39:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "dex-opt-art"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L40:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L41:
aload 25
aload 0
aload 1
aload 2
invokestatic com/chelpus/Utils/zipART(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/lang/String;
astore 29
L42:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L43:
aload 29
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L46
L44:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L45:
new java/io/File
dup
aload 29
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifne L351
L46:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L47:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Error: dont create rebuild apk to /data/tmp/"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L48:
ldc ""
astore 4
L49:
aload 28
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
ldc "/arm/"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L52
L50:
ldc "arm"
astore 4
L52:
aload 28
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
ldc "/arm64/"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L54
L53:
ldc "arm64"
astore 4
L54:
aload 28
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
ldc "/x86/"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L56
L55:
ldc "x86"
astore 4
L56:
new java/io/File
dup
aload 30
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
new java/io/File
dup
aload 30
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L57
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "rm"
aastore
dup
iconst_1
aload 30
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L57:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "try create oat with dex2oat:"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
iconst_4
anewarray java/lang/String
dup
iconst_0
ldc "dex2oat"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "--dex-file="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 29
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "--oat-file="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 30
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "--instruction-set="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "end"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/io/File
dup
aload 30
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L109
new java/io/File
dup
aload 30
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
lconst_0
lcmp
ifeq L109
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "oat created with dex2oat - length="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
new java/io/File
dup
aload 30
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/io/File
dup
aload 30
invokespecial java/io/File/<init>(Ljava/lang/String;)V
aload 1
aload 2
aload 29
invokestatic com/chelpus/Utils/fixCRCart(Ljava/io/File;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V
L58:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L59:
new java/io/File
dup
aload 30
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 22
L60:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L61:
aload 22
invokevirtual java/io/File/exists()Z
ifeq L66
L62:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L63:
aload 22
invokevirtual java/io/File/length()J
lconst_0
lcmp
ifne L66
L64:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L65:
aload 22
invokevirtual java/io/File/delete()Z
pop
L66:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L67:
new java/io/File
dup
aload 22
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L352
L68:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L69:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "oat file found. try copy and permission apply."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L70:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L71:
new java/io/File
dup
aload 27
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L72:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L73:
new java/io/File
dup
aload 27
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L76
L74:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L75:
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "rm"
aastore
dup
iconst_1
aload 27
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L76:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L77:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
aload 22
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aload 21
invokestatic com/chelpus/Utils/dalvikvm_copyFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
ifeq L353
L78:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L79:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Free space for odex enougth."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L80:
iconst_1
istore 6
iload 9
istore 15
aload 23
astore 4
iload 6
istore 5
iload 16
istore 11
iload 7
istore 10
L81:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
aload 30
aload 26
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokestatic com/chelpus/Utils/dalvikvm_copyFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
ifeq L354
L82:
iload 9
istore 15
aload 23
astore 4
iload 6
istore 5
iload 16
istore 11
iload 7
istore 10
L83:
aload 26
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
astore 1
L84:
iload 9
istore 15
aload 23
astore 4
iload 6
istore 5
iload 16
istore 11
iload 7
istore 10
L85:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".art"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L88
L86:
iload 9
istore 15
aload 23
astore 4
iload 6
istore 5
iload 16
istore 11
iload 7
istore 10
L87:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".art"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L88:
iconst_1
istore 6
iconst_1
istore 13
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L89:
aload 2
ldc "/system"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L355
L90:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L91:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "0644"
aastore
dup
iconst_2
aload 1
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L92:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L93:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
ldc "0.0"
aastore
dup
iconst_2
aload 1
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L94:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L95:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
ldc "0:0"
aastore
dup
iconst_2
aload 1
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L96:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L97:
aload 2
aload 3
invokestatic com/chelpus/Utils/getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 1
L98:
iload 12
istore 7
L356:
iload 9
istore 15
aload 23
astore 4
iload 6
istore 5
iload 7
istore 11
iload 8
istore 10
L99:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
ldc "/data/tmp"
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L100:
new java/io/File
dup
ldc "/data/tmp"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/listFiles()[Ljava/io/File;
astore 4
L101:
aload 24
astore 21
iload 6
istore 14
iload 7
istore 13
iload 8
istore 12
aload 1
astore 22
aload 4
ifnull L357
L103:
aload 4
arraylength
istore 10
L104:
iconst_0
istore 5
L358:
aload 24
astore 21
iload 6
istore 14
iload 7
istore 13
iload 8
istore 12
aload 1
astore 22
iload 5
iload 10
if_icmpge L357
aload 4
iload 5
aaload
astore 22
L105:
new com/chelpus/Utils
dup
ldc "1"
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
astore 21
aload 22
invokevirtual java/io/File/delete()Z
pop
aload 22
invokevirtual java/io/File/exists()Z
ifeq L106
aload 21
aload 22
invokevirtual com/chelpus/Utils/deleteFolder(Ljava/io/File;)V
L106:
iload 5
iconst_1
iadd
istore 5
goto L358
L351:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L107:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "apk found and copy created apk size "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
new java/io/File
dup
aload 29
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L108:
goto L48
L109:
new java/io/File
dup
aload 30
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
new java/io/File
dup
aload 30
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L58
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "rm"
aastore
dup
iconst_1
aload 30
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L110:
goto L58
L51:
astore 22
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L111:
aload 22
invokevirtual java/lang/Exception/printStackTrace()V
L112:
goto L58
L355:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L113:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "0644"
aastore
dup
iconst_2
aload 1
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L114:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L115:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "1000."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
aload 1
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L116:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L117:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "1000:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
aload 1
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L118:
goto L96
L354:
iload 9
istore 15
aload 23
astore 4
iload 6
istore 5
iload 16
istore 11
iload 7
istore 10
L119:
new java/io/File
dup
aload 27
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L120:
iload 9
istore 15
aload 23
astore 4
iload 6
istore 5
iload 16
istore 11
iload 7
istore 10
L121:
new java/io/File
dup
aload 27
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L359
L122:
iload 9
istore 15
aload 23
astore 4
iload 6
istore 5
iload 16
istore 11
iload 7
istore 10
L123:
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "rm"
aastore
dup
iconst_1
aload 27
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L124:
goto L359
L353:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L125:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Error:Free space for odex not enougth."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L126:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L127:
aload 22
invokevirtual java/io/File/exists()Z
ifeq L130
L128:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L129:
aload 22
invokevirtual java/io/File/delete()Z
pop
L130:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L131:
aload 22
invokevirtual java/io/File/exists()Z
ifeq L134
L132:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L133:
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "rm"
aastore
dup
iconst_1
aload 22
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L134:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L135:
new java/io/File
dup
aload 27
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L136:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L137:
new java/io/File
dup
aload 27
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L140
L138:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L139:
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "rm"
aastore
dup
iconst_1
aload 27
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L140:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L141:
new java/io/File
dup
aload 2
aload 3
invokestatic com/chelpus/Utils/getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L144
L142:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L143:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Error:dont delete corrupt odex."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L144:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L145:
aload 26
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
astore 22
L146:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L147:
aload 29
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L150
L148:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L149:
new java/io/File
dup
aload 29
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifne L360
L150:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L151:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Error: dont create rebuild apk to /data/tmp/"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L152:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L153:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "try create oat with DexFile2:"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L154:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L155:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 26
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 30
L156:
ldc ""
astore 21
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L157:
aload 28
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
ldc "/arm/"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L361
L158:
ldc "arm"
astore 21
L361:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L159:
aload 28
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
ldc "/arm64/"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L362
L160:
ldc "arm64"
astore 21
L362:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L161:
aload 28
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
ldc "/x86/"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L363
L162:
ldc "x86"
astore 21
L363:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L163:
new java/io/File
dup
aload 30
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L164:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L165:
new java/io/File
dup
aload 30
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L168
L166:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L167:
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "rm"
aastore
dup
iconst_1
aload 30
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L168:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L169:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "try create oat with dex2oat:"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L170:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L171:
getstatic java/lang/System/out Ljava/io/PrintStream;
iconst_4
anewarray java/lang/String
dup
iconst_0
ldc "dex2oat"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "--dex-file="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 29
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "--oat-file="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 30
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "--instruction-set="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 21
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L172:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L173:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "end"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L174:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L175:
new java/io/File
dup
aload 30
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L180
L176:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L177:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "oat2 created with dex2oat - length="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
new java/io/File
dup
aload 30
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L178:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L179:
new java/io/File
dup
aload 30
invokespecial java/io/File/<init>(Ljava/lang/String;)V
aload 1
aload 2
aload 29
invokestatic com/chelpus/Utils/fixCRCart(Ljava/io/File;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V
L180:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L181:
new java/io/File
dup
aload 30
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
L182:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L183:
aload 1
invokevirtual java/io/File/exists()Z
ifeq L188
L184:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L185:
aload 1
invokevirtual java/io/File/length()J
lconst_0
lcmp
ifne L188
L186:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L187:
aload 1
invokevirtual java/io/File/delete()Z
pop
L188:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L189:
new java/io/File
dup
aload 1
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L364
L190:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L191:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "oat file found. try copy and permission apply."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L192:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L193:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
aload 30
aload 26
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokestatic com/chelpus/Utils/dalvikvm_copyFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
ifeq L365
L194:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L195:
aload 26
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
astore 1
L196:
iconst_0
istore 6
iload 12
istore 7
goto L356
L360:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L197:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "apk found and copy created apk2"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L198:
goto L152
L365:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L199:
new java/io/File
dup
aload 27
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L200:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L201:
new java/io/File
dup
aload 27
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L366
L202:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L203:
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "rm"
aastore
dup
iconst_1
aload 27
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L204:
goto L366
L102:
astore 21
iload 9
istore 15
aload 23
astore 4
iload 6
istore 5
iload 7
istore 11
iload 8
istore 10
L205:
aload 21
invokevirtual java/lang/Exception/printStackTrace()V
L206:
aload 1
astore 22
iload 8
istore 12
iload 7
istore 13
iload 6
istore 14
aload 24
astore 21
L357:
iload 9
istore 15
aload 21
astore 4
iload 14
istore 5
iload 13
istore 11
iload 12
istore 10
L207:
aload 2
ldc "/system"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L367
L208:
iload 9
istore 15
aload 21
astore 4
iload 14
istore 5
iload 13
istore 11
iload 12
istore 10
L209:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "0644"
aastore
dup
iconst_2
aload 22
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L210:
iload 9
istore 15
aload 21
astore 4
iload 14
istore 5
iload 13
istore 11
iload 12
istore 10
L211:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
ldc "0.0"
aastore
dup
iconst_2
aload 22
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L212:
iload 9
istore 15
aload 21
astore 4
iload 14
istore 5
iload 13
istore 11
iload 12
istore 10
L213:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
ldc "0:0"
aastore
dup
iconst_2
aload 22
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L214:
iload 9
istore 15
aload 21
astore 4
iload 14
istore 5
iload 13
istore 11
iload 12
istore 10
L215:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 26
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L216:
iload 9
istore 15
aload 21
astore 4
iload 14
istore 5
iload 13
istore 11
iload 12
istore 10
L217:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 26
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L218:
iload 9
istore 15
aload 21
astore 4
iload 14
istore 5
iload 13
istore 11
iload 12
istore 10
L219:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 25
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L220:
iload 9
istore 15
aload 21
astore 4
iload 14
istore 5
iload 13
istore 11
iload 12
istore 10
iload 9
istore 6
aload 21
astore 1
iload 14
istore 7
iload 13
istore 8
iload 12
istore 16
L221:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 25
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L348
L222:
iload 9
istore 15
aload 21
astore 4
iload 14
istore 5
iload 13
istore 11
iload 12
istore 10
L223:
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "rm"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 25
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L224:
iload 9
istore 6
aload 21
astore 1
iload 14
istore 7
iload 13
istore 8
iload 12
istore 16
goto L348
L350:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L225:
aload 25
aload 0
aload 1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 25
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/zip(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)V
L226:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L227:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 25
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L228:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L229:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "1000."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 25
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L230:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L231:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "1000:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 25
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L232:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L233:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 25
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L238
L234:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L235:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 25
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
lconst_0
lcmp
ifeq L238
L236:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L237:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher (CustomPatch): foundreworked apk "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 25
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L238:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L239:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 25
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L244
L240:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L241:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 25
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
lconst_0
lcmp
ifne L244
L242:
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L243:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 25
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L244:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 26
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 25
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 26
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
iconst_0
invokestatic dalvik/system/DexFile/loadDex(Ljava/lang/String;Ljava/lang/String;I)Ldalvik/system/DexFile;
astore 1
L245:
iload 9
istore 15
aload 1
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L248:
aload 26
invokevirtual java/io/File/getName()Ljava/lang/String;
astore 24
L249:
iload 9
istore 15
aload 1
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L250:
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 24
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L251:
iload 9
istore 15
aload 1
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L252:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 24
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L253:
iload 9
istore 15
aload 1
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L254:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 24
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 22
L255:
iload 9
istore 15
aload 1
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L256:
aload 22
invokevirtual java/io/File/exists()Z
ifeq L261
L257:
iload 9
istore 15
aload 1
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L258:
aload 22
invokevirtual java/io/File/length()J
lconst_0
lcmp
ifne L261
L259:
iload 9
istore 15
aload 1
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L260:
aload 22
invokevirtual java/io/File/delete()Z
pop
L261:
iload 9
istore 15
aload 1
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
iload 14
istore 6
aload 21
astore 23
L262:
new java/io/File
dup
aload 22
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L368
L263:
iload 9
istore 15
aload 1
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L264:
aload 22
aload 2
invokestatic com/chelpus/Utils/fixadlerOdex(Ljava/io/File;Ljava/lang/String;)V
L265:
iload 9
istore 15
aload 1
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L266:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 24
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 21
invokestatic com/chelpus/Utils/dalvikvm_copyFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
ifeq L369
L267:
iload 9
istore 15
aload 1
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L268:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Free space for odex enougth."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L269:
iconst_1
istore 6
aload 21
astore 23
L368:
aload 1
astore 21
iload 6
istore 14
iload 19
istore 13
iload 18
istore 12
aload 23
astore 22
iload 6
ifeq L357
aload 1
astore 21
iload 6
istore 14
iload 19
istore 13
iload 18
istore 12
aload 23
astore 22
iload 9
ifne L357
iload 9
istore 15
aload 1
astore 4
iload 6
istore 5
iload 16
istore 11
iload 7
istore 10
aload 1
astore 21
iload 6
istore 14
iload 19
istore 13
iload 18
istore 12
aload 23
astore 22
L270:
new java/io/File
dup
aload 27
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifne L357
L271:
iload 9
istore 15
aload 1
astore 4
iload 6
istore 5
iload 16
istore 11
iload 7
istore 10
L272:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "lackypatch: dexopt-wrapper used!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L273:
iload 9
istore 15
aload 1
astore 4
iload 6
istore 5
iload 16
istore 11
iload 7
istore 10
L274:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
ldc "0.0"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/dexopt-wrapper"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L275:
iload 9
istore 15
aload 1
astore 4
iload 6
istore 5
iload 16
istore 11
iload 7
istore 10
L276:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
ldc "0:0"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/dexopt-wrapper"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L277:
iload 9
istore 15
aload 1
astore 4
iload 6
istore 5
iload 16
istore 11
iload 7
istore 10
L278:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/dexopt-wrapper"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L279:
iload 9
istore 15
aload 1
astore 4
iload 6
istore 5
iload 16
istore 11
iload 7
istore 10
L280:
iconst_3
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/dexopt-wrapper"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 25
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
aload 27
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
astore 21
L281:
iload 9
istore 15
aload 1
astore 4
iload 6
istore 5
iload 16
istore 11
iload 7
istore 10
L282:
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 21
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L283:
iload 9
istore 15
aload 1
astore 4
iload 6
istore 5
iload 16
istore 11
iload 7
istore 10
L284:
aload 21
ldc "succes"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L370
L285:
iload 9
istore 15
aload 1
astore 4
iload 6
istore 5
iload 16
istore 11
iload 7
istore 10
L286:
aload 21
ldc "failed"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L370
L287:
iload 9
istore 15
aload 1
astore 4
iload 6
istore 5
iload 16
istore 11
iload 7
istore 10
L288:
new java/io/File
dup
aload 27
invokespecial java/io/File/<init>(Ljava/lang/String;)V
aload 2
invokestatic com/chelpus/Utils/fixadlerOdex(Ljava/io/File;Ljava/lang/String;)V
L289:
aload 1
astore 21
iload 6
istore 14
iload 19
istore 13
iload 18
istore 12
aload 23
astore 22
goto L357
L246:
astore 1
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L290:
aload 1
invokevirtual java/lang/RuntimeException/printStackTrace()V
L291:
aload 22
astore 1
goto L245
L247:
astore 1
iload 9
istore 15
aload 23
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L292:
aload 1
invokevirtual java/lang/Exception/printStackTrace()V
L293:
aload 22
astore 1
goto L245
L369:
iload 9
istore 15
aload 1
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L294:
new java/io/File
dup
aload 27
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L295:
iload 9
istore 15
aload 1
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L296:
new java/io/File
dup
aload 27
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L299
L297:
iload 9
istore 15
aload 1
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L298:
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "rm"
aastore
dup
iconst_1
aload 27
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L299:
iload 9
istore 15
aload 1
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L300:
new java/io/File
dup
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 24
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;Ljava/lang/String;)V
aload 2
invokestatic com/chelpus/Utils/fixadlerOdex(Ljava/io/File;Ljava/lang/String;)V
L301:
iload 9
istore 15
aload 1
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L302:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 24
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 26
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokestatic com/chelpus/Utils/dalvikvm_copyFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
ifeq L371
L303:
iload 9
istore 15
aload 1
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L304:
aload 26
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
astore 23
L305:
iconst_0
istore 6
goto L368
L371:
iload 9
istore 15
aload 1
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L306:
new java/io/File
dup
aload 27
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L307:
iload 9
istore 15
aload 1
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L308:
new java/io/File
dup
aload 27
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L372
L309:
iload 9
istore 15
aload 1
astore 4
iload 13
istore 5
iload 16
istore 11
iload 7
istore 10
L310:
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "rm"
aastore
dup
iconst_1
aload 27
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L311:
goto L372
L370:
iload 9
istore 15
aload 1
astore 4
iload 6
istore 5
iload 16
istore 11
iload 7
istore 10
L312:
new java/io/File
dup
aload 27
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L313:
iload 9
istore 15
aload 1
astore 4
iload 6
istore 5
iload 16
istore 11
iload 7
istore 10
L314:
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "rm"
aastore
dup
iconst_1
aload 27
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L315:
iload 9
istore 15
aload 1
astore 4
iload 6
istore 5
iload 16
istore 11
iload 7
istore 10
L316:
aload 26
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
astore 22
L317:
iload 9
istore 15
aload 1
astore 4
iload 6
istore 5
iload 16
istore 11
iload 7
istore 10
L318:
new java/io/File
dup
aload 27
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L321
L319:
iload 9
istore 15
aload 1
astore 4
iload 6
istore 5
iload 16
istore 11
iload 7
istore 10
L320:
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "rm"
aastore
dup
iconst_1
aload 22
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L321:
iload 9
istore 15
aload 1
astore 4
iload 6
istore 5
iload 16
istore 11
iload 7
istore 10
L322:
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "rm"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 24
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L323:
iload 9
istore 15
aload 1
astore 4
iload 6
istore 5
iload 16
istore 11
iload 7
istore 10
L324:
iconst_3
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/dexopt-wrapper"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 25
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 24
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L325:
iload 9
istore 15
aload 1
astore 4
iload 6
istore 5
iload 16
istore 11
iload 7
istore 10
L326:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 24
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L327:
iload 9
istore 15
aload 1
astore 4
iload 6
istore 5
iload 16
istore 11
iload 7
istore 10
L328:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
ldc "0.0"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 24
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L329:
iload 9
istore 15
aload 1
astore 4
iload 6
istore 5
iload 16
istore 11
iload 7
istore 10
L330:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
ldc "0:0"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 24
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L331:
iload 9
istore 15
aload 1
astore 4
iload 6
istore 5
iload 16
istore 11
iload 7
istore 10
L332:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 24
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
aload 2
invokestatic com/chelpus/Utils/fixadlerOdex(Ljava/io/File;Ljava/lang/String;)V
L333:
iload 9
istore 15
aload 1
astore 4
iload 6
istore 5
iload 16
istore 11
iload 7
istore 10
L334:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 24
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 22
iconst_0
iconst_1
invokestatic com/chelpus/Utils/copyFile(Ljava/lang/String;Ljava/lang/String;ZZ)Z
pop
L335:
aload 1
astore 21
iload 6
istore 14
iload 19
istore 13
iload 18
istore 12
goto L357
L367:
iload 9
istore 15
aload 21
astore 4
iload 14
istore 5
iload 13
istore 11
iload 12
istore 10
L336:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "0644"
aastore
dup
iconst_2
aload 22
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L337:
iload 9
istore 15
aload 21
astore 4
iload 14
istore 5
iload 13
istore 11
iload 12
istore 10
L338:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "1000."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
aload 22
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L339:
iload 9
istore 15
aload 21
astore 4
iload 14
istore 5
iload 13
istore 11
iload 12
istore 10
L340:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "1000:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
aload 22
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L341:
goto L214
L20:
astore 0
aload 0
invokevirtual java/io/IOException/printStackTrace()V
goto L19
L349:
iload 6
ifeq L373
iload 16
ifeq L373
iconst_2
ireturn
L373:
iload 6
ifeq L374
iload 7
ifne L374
iconst_1
ireturn
L374:
iload 6
ifeq L375
iload 7
ifeq L375
iconst_0
ireturn
L375:
iload 6
ifne L376
iload 7
ifne L376
iconst_1
ireturn
L376:
iload 17
istore 5
iload 6
ifne L347
iload 17
istore 5
iload 7
ifeq L347
iconst_0
ireturn
L359:
iconst_0
istore 6
iconst_0
istore 7
aload 21
astore 1
goto L356
L366:
iconst_0
istore 6
iconst_0
istore 7
aload 22
astore 1
goto L356
L364:
iconst_1
istore 8
iload 12
istore 7
aload 22
astore 1
goto L356
L352:
iconst_1
istore 8
iload 12
istore 7
aload 21
astore 1
goto L356
L372:
iconst_0
istore 6
aload 21
astore 23
goto L368
.limit locals 31
.limit stack 6
.end method

.method public static dalvikvm_copyFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
new java/io/File
dup
aload 2
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 3
aload 0
invokevirtual java/io/File/exists()Z
ifeq L0
aload 2
ldc "RW"
invokestatic com/chelpus/Utils/remount(Ljava/lang/String;Ljava/lang/String;)Z
pop
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "dd"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "if="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "of="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
new java/io/File
dup
aload 2
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L1
aload 0
invokevirtual java/io/File/length()J
aload 3
invokevirtual java/io/File/length()J
lcmp
ifeq L2
L1:
iconst_4
anewarray java/lang/String
dup
iconst_0
ldc "busybox"
aastore
dup
iconst_1
ldc "dd"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "if="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "of="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L2:
new java/io/File
dup
aload 2
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L3
aload 0
invokevirtual java/io/File/length()J
aload 3
invokevirtual java/io/File/length()J
lcmp
ifeq L4
L3:
iconst_4
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/busybox"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_1
ldc "dd"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "if="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "of="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L4:
aload 0
invokevirtual java/io/File/length()J
aload 3
invokevirtual java/io/File/length()J
lcmp
ifeq L5
aload 0
invokevirtual java/io/File/length()J
lconst_0
lcmp
ifeq L5
aload 3
invokevirtual java/io/File/delete()Z
pop
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Length of Files not equals. Destination deleted!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iconst_0
ireturn
L5:
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 3
invokevirtual java/io/File/length()J
invokevirtual java/io/PrintStream/println(J)V
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "File copied!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iconst_1
ireturn
L0:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Source File not Found!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iconst_0
ireturn
.limit locals 4
.limit stack 5
.end method

.method public static dexopt(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)I
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L2
.catch java/lang/Exception from L7 to L8 using L2
.catch java/lang/Exception from L9 to L10 using L2
.catch java/lang/Exception from L10 to L11 using L12
.catch java/lang/Exception from L13 to L14 using L12
.catch java/lang/Exception from L15 to L16 using L12
.catch java/lang/Exception from L17 to L18 using L12
.catch java/lang/Exception from L19 to L20 using L12
.catch java/lang/Exception from L20 to L21 using L12
.catch java/lang/Exception from L22 to L23 using L12
.catch java/lang/Exception from L24 to L25 using L2
.catch java/lang/Exception from L26 to L27 using L2
.catch java/lang/Exception from L28 to L29 using L2
.catch java/lang/Exception from L30 to L31 using L2
.catch java/lang/Exception from L32 to L33 using L2
.catch java/lang/Exception from L34 to L35 using L2
.catch java/lang/Exception from L36 to L37 using L2
.catch java/lang/Exception from L38 to L39 using L2
.catch java/lang/Exception from L40 to L41 using L2
.catch java/lang/Exception from L42 to L43 using L2
.catch java/lang/Exception from L44 to L45 using L2
.catch java/lang/Exception from L46 to L47 using L2
.catch java/lang/Exception from L48 to L49 using L2
.catch java/lang/Exception from L50 to L51 using L2
.catch java/lang/Exception from L52 to L53 using L2
.catch java/lang/Exception from L54 to L55 using L2
.catch java/lang/Exception from L56 to L57 using L2
.catch java/lang/Exception from L58 to L59 using L12
.catch java/lang/Exception from L60 to L61 using L2
.catch java/lang/Exception from L62 to L63 using L12
.catch java/lang/Exception from L63 to L64 using L65
.catch java/lang/UnsatisfiedLinkError from L63 to L64 using L66
.catch java/lang/Exception from L64 to L67 using L12
.catch java/lang/Exception from L67 to L68 using L12
.catch java/lang/Exception from L68 to L69 using L12
.catch java/lang/Exception from L69 to L70 using L12
.catch java/lang/Exception from L70 to L71 using L12
.catch java/lang/Exception from L72 to L73 using L12
.catch java/lang/Exception from L73 to L74 using L12
.catch java/lang/Exception from L75 to L76 using L12
.catch java/lang/Exception from L77 to L78 using L12
.catch java/lang/Exception from L79 to L80 using L12
.catch java/lang/Exception from L81 to L82 using L12
.catch java/lang/Exception from L83 to L84 using L2
.catch java/lang/Exception from L85 to L86 using L2
.catch java/lang/Exception from L87 to L88 using L2
.catch java/lang/Exception from L89 to L90 using L2
.catch java/lang/Exception from L91 to L92 using L2
.catch java/lang/Exception from L93 to L94 using L2
.catch java/lang/Exception from L95 to L96 using L2
.catch java/lang/Exception from L97 to L98 using L2
.catch java/lang/Exception from L99 to L100 using L2
.catch java/lang/Exception from L101 to L102 using L2
.catch java/lang/Exception from L103 to L104 using L2
.catch java/lang/Exception from L105 to L106 using L2
.catch java/lang/Exception from L107 to L108 using L2
iconst_0
istore 8
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 1
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/mkdirs()Z
pop
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
aload 0
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
ldc "0:0"
aastore
dup
iconst_2
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
ldc "0.0"
aastore
dup
iconst_2
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
aload 1
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
ldc "0:0"
aastore
dup
iconst_2
aload 1
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
ldc "0.0"
aastore
dup
iconst_2
aload 1
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
aload 2
astore 10
aload 2
ifnonnull L109
aload 0
aload 3
invokestatic com/chelpus/Utils/getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 10
L109:
aload 10
ifnull L110
aload 10
ldc "rw"
invokestatic com/chelpus/Utils/remount(Ljava/lang/String;Ljava/lang/String;)Z
pop
L110:
iconst_0
istore 9
iconst_0
istore 6
aload 0
aload 3
invokestatic com/chelpus/Utils/getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 11
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/getName()Ljava/lang/String;
astore 2
aload 2
ldc "odex"
invokestatic com/chelpus/Utils/changeExtension(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
pop
aload 2
ldc "dex"
invokestatic com/chelpus/Utils/changeExtension(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 12
iload 9
istore 5
L0:
aload 0
invokestatic com/chelpus/Utils/getFileDalvikCacheName(Ljava/lang/String;)Ljava/io/File;
astore 13
L1:
iload 9
istore 5
L3:
aload 13
invokevirtual java/io/File/getName()Ljava/lang/String;
pop
L4:
iload 9
istore 5
L5:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "dex-opt-art"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L6:
iload 9
istore 5
L7:
new java/io/File
dup
aload 12
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L10
L8:
iload 9
istore 5
L9:
new java/io/File
dup
aload 12
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L10:
aload 4
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L62
L11:
ldc ""
astore 2
L13:
aload 13
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
ldc "/arm/"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L15
L14:
ldc "arm"
astore 2
L15:
aload 13
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
ldc "/arm64/"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L17
L16:
ldc "arm64"
astore 2
L17:
aload 13
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
ldc "/x86/"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L19
L18:
ldc "x86"
astore 2
L19:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "try create oat with dex2oat:"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
iconst_4
anewarray java/lang/String
dup
iconst_0
ldc "dex2oat"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "--dex-file="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "--oat-file="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 12
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "--instruction-set="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "end"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/io/File
dup
aload 12
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L20
new java/io/File
dup
aload 12
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
lconst_0
lcmp
ifne L58
L20:
new java/io/File
dup
aload 12
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L21:
iload 8
istore 7
L22:
new java/io/File
dup
aload 12
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L111
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "rm"
aastore
dup
iconst_1
aload 12
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L23:
iload 8
istore 7
L111:
iload 9
istore 5
L24:
new java/io/File
dup
aload 12
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 2
L25:
iload 9
istore 5
L26:
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 12
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L27:
iload 9
istore 5
L28:
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 10
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L29:
iload 9
istore 5
L30:
aload 2
invokevirtual java/io/File/exists()Z
ifeq L35
L31:
iload 9
istore 5
L32:
aload 2
invokevirtual java/io/File/length()J
lconst_0
lcmp
ifne L35
L33:
iload 9
istore 5
L34:
aload 2
invokevirtual java/io/File/delete()Z
pop
L35:
iload 7
istore 8
iload 9
istore 5
L36:
aload 2
invokevirtual java/io/File/exists()Z
ifeq L112
L37:
iload 9
istore 5
L38:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "oat file found. try copy and permission apply."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L39:
iload 9
istore 5
L40:
new java/io/File
dup
aload 11
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L41:
iload 9
istore 5
L42:
new java/io/File
dup
aload 11
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L45
L43:
iload 9
istore 5
L44:
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "rm"
aastore
dup
iconst_1
aload 11
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L45:
iload 9
istore 5
L46:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
aload 2
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aload 10
invokestatic com/chelpus/Utils/dalvikvm_copyFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
ifeq L113
L47:
iload 9
istore 5
L48:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Free space for odex enougth."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L49:
iconst_1
istore 6
L114:
iload 6
istore 5
L50:
aload 0
ldc "/system"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L115
L51:
iload 6
istore 5
L52:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "0644"
aastore
dup
iconst_2
aload 10
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L53:
iload 6
istore 5
L54:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
ldc "0.0"
aastore
dup
iconst_2
aload 10
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L55:
iload 6
istore 5
L56:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
ldc "0:0"
aastore
dup
iconst_2
aload 10
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L57:
iload 7
istore 8
L112:
new java/io/File
dup
aload 12
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L116
new java/io/File
dup
aload 12
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L116:
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/listFiles()[Ljava/io/File;
astore 2
aload 2
ifnull L117
aload 2
arraylength
ifle L117
aload 2
arraylength
istore 7
iconst_0
istore 5
L118:
iload 5
iload 7
if_icmpge L117
aload 2
iload 5
aaload
invokevirtual java/io/File/delete()Z
pop
iload 5
iconst_1
iadd
istore 5
goto L118
L58:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "odex file create to tmp dir."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L59:
iload 8
istore 7
goto L111
L12:
astore 2
iload 9
istore 5
L60:
aload 2
invokevirtual java/lang/Exception/printStackTrace()V
L61:
iconst_4
istore 7
goto L111
L62:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "try create oat with DexFile."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L63:
new java/io/File
dup
aload 12
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
aload 0
aload 12
iconst_0
invokestatic dalvik/system/DexFile/loadDex(Ljava/lang/String;Ljava/lang/String;I)Ldalvik/system/DexFile;
invokevirtual dalvik/system/DexFile/close()V
L64:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "end. check file. "
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/io/File
dup
aload 12
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L67
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "found file "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 12
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
new java/io/File
dup
aload 12
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L67:
new java/io/File
dup
aload 10
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L68
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "found file "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 10
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
new java/io/File
dup
aload 10
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L68:
new java/io/File
dup
aload 12
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L69
new java/io/File
dup
aload 12
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
lconst_0
lcmp
ifne L81
L69:
new java/io/File
dup
aload 12
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
new java/io/File
dup
aload 12
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L70
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "rm"
aastore
dup
iconst_1
aload 12
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L70:
new java/io/File
dup
aload 12
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L73
L71:
iload 8
istore 7
L72:
new java/io/File
dup
aload 12
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
lconst_0
lcmp
ifne L111
L73:
new java/io/File
dup
aload 12
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L74:
iload 8
istore 7
L75:
new java/io/File
dup
aload 12
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L111
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "rm"
aastore
dup
iconst_1
aload 12
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L76:
iload 8
istore 7
goto L111
L65:
astore 2
L77:
aload 2
invokevirtual java/lang/Exception/printStackTrace()V
L78:
goto L64
L66:
astore 2
L79:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "withoutFramework"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L80:
goto L64
L81:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "odex file create to tmp dir."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L82:
iload 8
istore 7
goto L111
L113:
iconst_0
istore 9
iconst_0
istore 8
iload 9
istore 5
L83:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Error:Free space for odex not enougth."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L84:
iload 9
istore 5
L85:
aload 2
invokevirtual java/io/File/exists()Z
ifeq L88
L86:
iload 9
istore 5
L87:
aload 2
invokevirtual java/io/File/delete()Z
pop
L88:
iload 9
istore 5
L89:
aload 2
invokevirtual java/io/File/exists()Z
ifeq L92
L90:
iload 9
istore 5
L91:
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "rm"
aastore
dup
iconst_1
aload 2
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L92:
iload 9
istore 5
L93:
new java/io/File
dup
aload 11
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L94:
iload 9
istore 5
L95:
new java/io/File
dup
aload 11
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L98
L96:
iload 9
istore 5
L97:
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "rm"
aastore
dup
iconst_1
aload 11
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L98:
iload 8
istore 6
iload 9
istore 5
L99:
new java/io/File
dup
aload 0
aload 3
invokestatic com/chelpus/Utils/getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L114
L100:
iload 9
istore 5
L101:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Error:dont delete corrupt odex."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L102:
iload 8
istore 6
goto L114
L2:
astore 2
aload 2
invokevirtual java/lang/Exception/printStackTrace()V
iconst_4
istore 8
iload 5
istore 6
goto L112
L115:
iload 6
istore 5
L103:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "0644"
aastore
dup
iconst_2
aload 10
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L104:
iload 6
istore 5
L105:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "1000."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
aload 10
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L106:
iload 6
istore 5
L107:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "1000:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
aload 10
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L108:
iload 7
istore 8
goto L112
L117:
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L119
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L119:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "644"
aastore
dup
iconst_2
aload 0
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
iload 6
ifne L120
iconst_1
istore 8
L120:
iload 8
ireturn
.limit locals 14
.limit stack 6
.end method

.method public static dexoptcopy()V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L4
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "dexopt-wrapper"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
getstatic android/os/Build/CPU_ABI Ljava/lang/String;
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
invokevirtual java/lang/String/trim()Ljava/lang/String;
ldc "x86"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L5
ldc_w 2131099654
istore 0
L6:
aload 1
invokevirtual java/io/File/exists()Z
ifeq L0
aload 1
invokevirtual java/io/File/length()J
iload 0
invokestatic com/chelpus/Utils/getRawLength(I)J
lcmp
ifeq L7
L0:
iload 0
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/dexopt-wrapper"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokestatic com/chelpus/Utils/getRawToFile(ILjava/io/File;)Z
pop
L1:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "dexopt-wrapper"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
sipush 777
invokestatic com/chelpus/Utils/chmod(Ljava/io/File;I)I
pop
L3:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chmod 777 "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "dexopt-wrapper"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chown 0.0 "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "dexopt-wrapper"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chmod 0:0 "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "dexopt-wrapper"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
return
L5:
getstatic android/os/Build/CPU_ABI Ljava/lang/String;
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
invokevirtual java/lang/String/trim()Ljava/lang/String;
ldc "MIPS"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L8
ldc_w 2131099653
istore 0
goto L6
L8:
ldc_w 2131099652
istore 0
goto L6
L4:
astore 1
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 1
invokevirtual java/io/PrintStream/println(Ljava/lang/Object;)V
aload 1
invokevirtual java/lang/Exception/printStackTrace()V
goto L3
L7:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chmod 777 "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "dexopt-wrapper"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chown 0.0 "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "dexopt-wrapper"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chmod 0:0 "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "dexopt-wrapper"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
return
L2:
astore 1
goto L1
.limit locals 2
.limit stack 5
.end method

.method public static exists(Ljava/lang/String;)Z
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L0
iconst_1
ireturn
L0:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/su Z
ifne L1
iconst_0
ireturn
L1:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifne L2
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "ls "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
astore 1
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 1
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 1
aload 0
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L2
iconst_1
ireturn
L2:
iconst_0
ireturn
.limit locals 2
.limit stack 6
.end method

.method public static exit()V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getConfig()Landroid/content/SharedPreferences;
ldc "OnBootService"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifne L0
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/patchOnBoot Z
ifne L0
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/desktop_launch Z
ifne L0
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/appDisabler Z
ifne L0
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/binderWidget Z
ifne L0
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "LP - exit."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iconst_0
invokestatic java/lang/System/exit(I)V
L0:
return
.limit locals 0
.limit stack 3
.end method

.method public static exitFromRootJava()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
istore 0
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
iload 0
ifeq L0
iconst_0
invokestatic java/lang/System/exit(I)V
L0:
return
.limit locals 1
.limit stack 1
.end method

.method public static exitRoot()V
.catch java/io/IOException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
.catch java/lang/Exception from L1 to L4 using L5
.catch java/lang/Exception from L4 to L6 using L5
.catch java/lang/Exception from L6 to L7 using L5
.catch java/lang/Exception from L7 to L8 using L5
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifne L9
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "LuckyPatcher: exit root."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L0:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suOutputStream Ljava/io/DataOutputStream;
ifnull L1
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suOutputStream Ljava/io/DataOutputStream;
ldc "exit\n"
invokevirtual java/io/DataOutputStream/writeBytes(Ljava/lang/String;)V
L1:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suProcess Ljava/lang/Process;
ifnull L4
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suProcess Ljava/lang/Process;
invokevirtual java/lang/Process/destroy()V
L4:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suOutputStream Ljava/io/DataOutputStream;
ifnull L6
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suOutputStream Ljava/io/DataOutputStream;
invokevirtual java/io/DataOutputStream/close()V
L6:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suInputStream Ljava/io/DataInputStream;
ifnull L7
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suInputStream Ljava/io/DataInputStream;
invokevirtual java/io/DataInputStream/close()V
L7:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suErrorInputStream Ljava/io/DataInputStream;
ifnull L8
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suErrorInputStream Ljava/io/DataInputStream;
invokevirtual java/io/DataInputStream/close()V
L8:
aconst_null
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suProcess Ljava/lang/Process;
aconst_null
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suOutputStream Ljava/io/DataOutputStream;
aconst_null
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suInputStream Ljava/io/DataInputStream;
aconst_null
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suErrorInputStream Ljava/io/DataInputStream;
L9:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/semaphoreRoot Ljava/util/concurrent/Semaphore;
invokevirtual java/util/concurrent/Semaphore/release()V
return
L2:
astore 0
aload 0
invokevirtual java/io/IOException/printStackTrace()V
goto L1
L3:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
goto L1
L5:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
goto L8
.limit locals 1
.limit stack 2
.end method

.method private static findMountPointRecursive(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/Mount;
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L4 to L5 using L2
.catch java/lang/Exception from L6 to L7 using L2
.catch java/lang/Exception from L7 to L8 using L2
.catch java/lang/Exception from L9 to L10 using L2
.catch java/lang/Exception from L10 to L11 using L2
.catch java/lang/Exception from L12 to L13 using L2
.catch java/lang/Exception from L14 to L15 using L2
L0:
invokestatic com/chelpus/Utils/getMounts()Ljava/util/ArrayList;
astore 1
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 2
L1:
aload 2
ifnull L16
L3:
aload 1
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 3
L4:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L6
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/Mount
astore 4
aload 4
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/Mount/getMountPoint()Ljava/io/File;
aload 2
invokevirtual java/io/File/equals(Ljava/lang/Object;)Z
ifeq L4
L5:
aload 4
areturn
L6:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 2
aload 1
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 1
L7:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L17
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/Mount
astore 3
aload 0
aload 3
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/Mount/getMountPoint()Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L7
aload 2
aload 3
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L8:
goto L7
L2:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
aconst_null
areturn
L17:
aconst_null
astore 0
L9:
aload 2
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 3
L10:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L18
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/Mount
astore 1
L11:
aload 0
astore 2
aload 0
ifnonnull L19
aload 1
astore 2
L19:
aload 2
astore 0
L12:
aload 2
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/Mount/getMountPoint()Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/String/length()I
aload 1
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/Mount/getMountPoint()Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/String/length()I
if_icmpge L10
L13:
aload 1
astore 0
goto L10
L18:
aload 0
ifnull L15
L14:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "recursive mount "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/Mount/getMountPoint()Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L15:
aload 0
areturn
L16:
aconst_null
areturn
.limit locals 5
.limit stack 3
.end method

.method public static fixCRCart(Ljava/io/File;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V
.signature "(Ljava/io/File;Ljava/util/ArrayList<Ljava/io/File;>;Ljava/lang/String;Ljava/lang/String;)V"
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
.catch java/lang/Exception from L4 to L6 using L7
.catch java/lang/Exception from L8 to L9 using L7
.catch java/lang/Exception from L10 to L11 using L7
.catch java/lang/Exception from L12 to L13 using L14
.catch java/lang/Exception from L15 to L16 using L14
.catch java/lang/Exception from L16 to L17 using L14
.catch java/lang/Exception from L18 to L19 using L14
.catch java/lang/Exception from L20 to L21 using L7
.catch java/lang/Exception from L22 to L23 using L7
.catch java/lang/Exception from L23 to L24 using L7
.catch java/lang/Exception from L25 to L26 using L14
.catch java/lang/Exception from L27 to L28 using L7
.catch java/lang/Exception from L28 to L29 using L30
.catch java/lang/Exception from L31 to L32 using L30
.catch java/lang/Exception from L32 to L33 using L34
.catch java/lang/Exception from L35 to L36 using L34
.catch java/lang/Exception from L37 to L38 using L34
.catch java/lang/Exception from L39 to L40 using L34
.catch java/lang/Exception from L41 to L42 using L34
.catch java/lang/Exception from L43 to L44 using L34
.catch java/lang/Exception from L45 to L46 using L34
.catch java/lang/Exception from L47 to L48 using L34
.catch java/lang/Exception from L49 to L50 using L34
.catch java/lang/Exception from L51 to L52 using L34
.catch java/lang/Exception from L52 to L53 using L34
.catch java/lang/Exception from L53 to L54 using L34
.catch java/lang/Exception from L55 to L56 using L5
.catch java/lang/Exception from L56 to L57 using L58
.catch java/lang/Exception from L57 to L59 using L58
.catch java/lang/Exception from L60 to L61 using L58
.catch java/lang/Exception from L62 to L63 using L64
.catch java/lang/Exception from L65 to L66 using L64
.catch java/lang/Exception from L66 to L67 using L58
.catch java/lang/Exception from L68 to L69 using L5
.catch java/lang/Exception from L70 to L71 using L2
.catch java/lang/Exception from L72 to L73 using L58
.catch java/lang/Exception from L74 to L75 using L34
.catch java/lang/Exception from L76 to L77 using L34
.catch java/lang/Exception from L78 to L79 using L34
.catch java/lang/Exception from L80 to L81 using L34
.catch java/lang/Exception from L81 to L82 using L34
.catch java/lang/Exception from L83 to L84 using L34
.catch java/lang/Exception from L85 to L86 using L34
.catch java/lang/Exception from L87 to L88 using L34
.catch java/lang/Exception from L88 to L89 using L34
.catch java/lang/Exception from L90 to L91 using L30
.catch java/lang/Exception from L91 to L92 using L30
.catch java/lang/Exception from L93 to L94 using L5
.catch java/lang/Exception from L95 to L96 using L34
new java/util/ArrayList
dup
aload 1
invokevirtual java/util/ArrayList/size()I
invokespecial java/util/ArrayList/<init>(I)V
astore 16
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
aload 0
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
aload 2
ifnull L71
L0:
new java/io/File
dup
aload 2
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L71
new java/io/File
dup
aload 2
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
lstore 12
L1:
lload 12
lconst_0
lcmp
ifeq L71
L3:
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 2
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "644"
aastore
dup
iconst_2
aload 2
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L4:
new java/io/FileInputStream
dup
aload 2
invokespecial java/io/FileInputStream/<init>(Ljava/lang/String;)V
astore 17
new java/util/zip/ZipInputStream
dup
aload 17
invokespecial java/util/zip/ZipInputStream/<init>(Ljava/io/InputStream;)V
astore 18
aload 18
invokevirtual java/util/zip/ZipInputStream/getNextEntry()Ljava/util/zip/ZipEntry;
astore 15
L6:
aload 15
ifnull L27
L8:
aload 15
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
astore 19
aload 19
ldc "classes"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L23
aload 19
ldc ".dex"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L23
L9:
iconst_0
istore 5
L10:
iload 5
aload 1
invokevirtual java/util/ArrayList/size()I
if_icmpge L23
aload 1
iload 5
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/io/File
invokevirtual java/io/File/getName()Ljava/lang/String;
aload 19
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
istore 14
L11:
iload 14
ifeq L97
L12:
aload 15
invokevirtual java/util/zip/ZipEntry/getCrc()J
l2i
istore 6
L13:
iload 6
istore 4
iload 6
iconst_m1
if_icmpne L22
L15:
new java/util/zip/CRC32
dup
invokespecial java/util/zip/CRC32/<init>()V
astore 15
aload 15
invokevirtual java/util/zip/CRC32/reset()V
sipush 4096
newarray byte
astore 19
L16:
aload 18
aload 19
invokevirtual java/util/zip/ZipInputStream/read([B)I
istore 4
L17:
iload 4
iconst_m1
if_icmpeq L25
L18:
aload 15
aload 19
iconst_0
iload 4
invokevirtual java/util/zip/CRC32/update([BII)V
L19:
goto L16
L14:
astore 15
L20:
aload 15
invokevirtual java/lang/Exception/printStackTrace()V
L21:
iconst_0
istore 4
L22:
aload 16
iload 5
iload 4
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/ArrayList/add(ILjava/lang/Object;)V
aload 18
invokevirtual java/util/zip/ZipInputStream/closeEntry()V
L23:
aload 18
invokevirtual java/util/zip/ZipInputStream/getNextEntry()Ljava/util/zip/ZipEntry;
astore 15
L24:
goto L6
L25:
aload 15
invokevirtual java/util/zip/CRC32/getValue()J
lstore 12
L26:
lload 12
l2i
istore 4
goto L22
L97:
iload 5
iconst_1
iadd
istore 5
goto L10
L27:
aload 18
invokevirtual java/util/zip/ZipInputStream/close()V
aload 17
invokevirtual java/io/FileInputStream/close()V
L28:
new java/io/RandomAccessFile
dup
aload 0
ldc "rw"
invokespecial java/io/RandomAccessFile/<init>(Ljava/io/File;Ljava/lang/String;)V
invokevirtual java/io/RandomAccessFile/getChannel()Ljava/nio/channels/FileChannel;
astore 15
aload 15
getstatic java/nio/channels/FileChannel$MapMode/READ_WRITE Ljava/nio/channels/FileChannel$MapMode;
lconst_0
aload 15
invokevirtual java/nio/channels/FileChannel/size()J
l2i
i2l
invokevirtual java/nio/channels/FileChannel/map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
astore 17
aload 17
sipush 4096
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L29:
iconst_m1
istore 5
L31:
aload 3
invokevirtual java/lang/String/getBytes()[B
astore 18
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":classes"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/getBytes()[B
astore 3
aload 2
invokevirtual java/lang/String/getBytes()[B
astore 19
L32:
aload 17
invokevirtual java/nio/MappedByteBuffer/hasRemaining()Z
ifeq L91
aload 17
iload 5
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 17
invokevirtual java/nio/MappedByteBuffer/position()I
istore 5
aload 17
invokevirtual java/nio/MappedByteBuffer/get()B
istore 6
L33:
iload 6
istore 7
iload 5
istore 4
iload 6
aload 3
iconst_0
baload
if_icmpne L98
iconst_1
istore 8
L35:
aload 17
iload 5
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 17
invokevirtual java/nio/MappedByteBuffer/get()B
istore 9
L36:
iload 6
istore 7
iload 5
istore 4
iload 9
aload 3
iload 8
baload
if_icmpne L98
iload 8
iconst_1
iadd
istore 8
L37:
iload 8
aload 3
arraylength
if_icmpne L74
L38:
iconst_0
istore 8
L39:
ldc ".dex"
invokevirtual java/lang/String/getBytes()[B
astore 20
L40:
ldc "classes"
astore 0
L99:
iload 6
istore 7
iload 5
istore 4
iload 8
bipush 7
if_icmpge L98
iload 8
iconst_1
iadd
istore 11
L41:
aload 17
invokevirtual java/nio/MappedByteBuffer/get()B
istore 4
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 4
i2c
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 2
L42:
aload 2
astore 0
iload 11
istore 8
iload 4
aload 20
iconst_0
baload
if_icmpne L99
iconst_1
istore 10
L43:
aload 17
invokevirtual java/nio/MappedByteBuffer/get()B
istore 9
L44:
iload 5
istore 7
iload 6
istore 4
L100:
aload 2
astore 0
iload 4
istore 6
iload 7
istore 5
iload 11
istore 8
L45:
iload 10
aload 20
arraylength
if_icmpge L99
L46:
aload 2
astore 0
iload 4
istore 6
iload 7
istore 5
iload 11
istore 8
iload 9
aload 20
iload 10
baload
if_icmpne L99
L47:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 9
i2c
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 2
L48:
iload 10
iconst_1
iadd
istore 10
iload 4
istore 6
iload 7
istore 5
L49:
iload 10
aload 20
arraylength
if_icmpne L53
L50:
iconst_0
istore 8
L101:
iload 4
istore 6
iload 7
istore 5
L51:
iload 8
aload 1
invokevirtual java/util/ArrayList/size()I
if_icmpge L53
aload 1
iload 8
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/io/File
invokevirtual java/io/File/getName()Ljava/lang/String;
aload 2
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L102
aload 16
iload 8
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
ifeq L52
aload 17
aload 16
iload 8
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/byteValue()B
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 17
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 17
aload 16
iload 8
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
bipush 8
ishr
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 17
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 17
aload 16
iload 8
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
bipush 16
ishr
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 17
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 17
aload 16
iload 8
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
bipush 24
ishr
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 17
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
L52:
aload 17
iload 7
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 17
aload 19
invokevirtual java/nio/MappedByteBuffer/put([B)Ljava/nio/ByteBuffer;
pop
aload 17
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 17
invokevirtual java/nio/MappedByteBuffer/position()I
istore 5
aload 17
invokevirtual java/nio/MappedByteBuffer/get()B
istore 6
L53:
aload 17
invokevirtual java/nio/MappedByteBuffer/get()B
istore 9
L54:
iload 6
istore 4
iload 5
istore 7
goto L100
L7:
astore 15
L55:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Start alternative method."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L56:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "start"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 2
invokestatic kellinwood/zipio/ZipInput/read(Ljava/lang/String;)Lkellinwood/zipio/ZipInput;
astore 15
aload 15
invokevirtual kellinwood/zipio/ZipInput/getEntries()Ljava/util/Map;
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 17
L57:
aload 17
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L28
aload 17
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast kellinwood/zipio/ZioEntry
astore 19
aload 19
invokevirtual kellinwood/zipio/ZioEntry/getName()Ljava/lang/String;
ldc "classes"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L57
aload 19
invokevirtual kellinwood/zipio/ZioEntry/getName()Ljava/lang/String;
ldc ".dex"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L57
L59:
iconst_0
istore 5
L60:
iload 5
aload 1
invokevirtual java/util/ArrayList/size()I
if_icmpge L57
aload 1
iload 5
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/io/File
invokevirtual java/io/File/getName()Ljava/lang/String;
aload 19
invokevirtual kellinwood/zipio/ZioEntry/getName()Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
istore 14
L61:
iload 14
ifeq L103
L62:
aload 19
invokevirtual kellinwood/zipio/ZioEntry/getCrc32()I
istore 6
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "CRC32:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 6
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L63:
iload 6
istore 4
iload 6
iconst_m1
if_icmpne L66
L65:
new java/util/zip/CRC32
dup
invokespecial java/util/zip/CRC32/<init>()V
astore 18
aload 18
invokevirtual java/util/zip/CRC32/reset()V
aload 19
invokevirtual kellinwood/zipio/ZioEntry/getData()[B
astore 19
aload 18
aload 19
iconst_0
aload 19
arraylength
invokevirtual java/util/zip/CRC32/update([BII)V
aload 18
invokevirtual java/util/zip/CRC32/getValue()J
l2i
istore 4
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "CRC32:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 4
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L66:
aload 16
iload 5
iload 4
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/ArrayList/add(ILjava/lang/Object;)V
aload 15
invokevirtual kellinwood/zipio/ZipInput/close()V
L67:
goto L57
L58:
astore 15
L68:
aload 15
invokevirtual java/lang/Exception/printStackTrace()V
L69:
goto L28
L5:
astore 0
L70:
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L71:
return
L64:
astore 18
L72:
aload 18
invokevirtual java/lang/Exception/printStackTrace()V
L73:
iconst_0
istore 4
goto L66
L103:
iload 5
iconst_1
iadd
istore 5
goto L60
L102:
iload 8
iconst_1
iadd
istore 8
goto L101
L74:
aload 17
invokevirtual java/nio/MappedByteBuffer/get()B
istore 9
L75:
goto L36
L98:
iload 4
istore 5
iload 7
aload 18
iconst_0
baload
if_icmpne L32
iconst_1
istore 6
L76:
aload 17
iload 4
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 17
invokevirtual java/nio/MappedByteBuffer/get()B
istore 7
L77:
iload 4
istore 5
iload 7
aload 18
iload 6
baload
if_icmpne L32
iload 6
iconst_1
iadd
istore 6
L78:
iload 6
aload 18
arraylength
if_icmpne L95
L79:
sipush 255
istore 6
iconst_0
istore 5
L80:
iload 5
aload 1
invokevirtual java/util/ArrayList/size()I
if_icmpge L104
L81:
aload 1
iload 5
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/io/File
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc "classes.dex"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L105
L82:
iload 5
istore 6
goto L105
L104:
iload 4
istore 5
iload 6
sipush 255
if_icmpeq L32
iload 4
istore 5
L83:
aload 17
aload 17
invokevirtual java/nio/MappedByteBuffer/position()I
invokevirtual java/nio/MappedByteBuffer/get(I)B
bipush 32
if_icmpeq L32
L84:
iload 4
istore 5
L85:
aload 17
aload 17
invokevirtual java/nio/MappedByteBuffer/position()I
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/get(I)B
bipush 45
if_icmpeq L32
L86:
iload 4
istore 5
L87:
aload 17
aload 17
invokevirtual java/nio/MappedByteBuffer/position()I
iconst_2
iadd
invokevirtual java/nio/MappedByteBuffer/get(I)B
bipush 45
if_icmpeq L32
aload 16
iload 6
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
ifeq L88
aload 17
aload 16
iload 6
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/byteValue()B
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 17
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 17
aload 16
iload 6
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
bipush 8
ishr
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 17
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 17
aload 16
iload 6
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
bipush 16
ishr
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 17
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 17
aload 16
iload 6
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
bipush 24
ishr
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 17
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
L88:
aload 17
iload 4
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 17
aload 19
invokevirtual java/nio/MappedByteBuffer/put([B)Ljava/nio/ByteBuffer;
pop
aload 17
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
L89:
iload 4
istore 5
goto L32
L34:
astore 0
L90:
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L91:
aload 15
invokevirtual java/nio/channels/FileChannel/close()V
L92:
return
L30:
astore 0
L93:
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L94:
return
L95:
aload 17
invokevirtual java/nio/MappedByteBuffer/get()B
istore 7
L96:
goto L77
L2:
astore 0
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
invokevirtual java/io/PrintStream/println(Ljava/lang/Object;)V
return
L105:
iload 5
iconst_1
iadd
istore 5
goto L80
.limit locals 21
.limit stack 6
.end method

.method public static fixadler(Ljava/io/File;)V
.catch java/lang/Exception from L0 to L1 using L2
L0:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
aload 0
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
aload 0
invokestatic com/chelpus/Utils/calcSignature(Ljava/io/File;)V
aload 0
invokestatic com/chelpus/Utils/calcChecksum(Ljava/io/File;)V
L1:
return
L2:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
return
.limit locals 1
.limit stack 4
.end method

.method public static fixadlerOdex(Ljava/io/File;Ljava/lang/String;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L7
.catch java/lang/Exception from L8 to L9 using L7
.catch java/lang/Exception from L10 to L11 using L12
.catch java/lang/Exception from L13 to L14 using L12
.catch java/lang/Exception from L15 to L16 using L17
.catch java/lang/Exception from L18 to L19 using L20
.catch java/lang/Exception from L21 to L22 using L20
.catch java/lang/Exception from L22 to L23 using L20
.catch java/lang/Exception from L24 to L25 using L20
.catch java/lang/Exception from L26 to L27 using L12
.catch java/io/FileNotFoundException from L28 to L29 using L30
.catch java/io/IOException from L28 to L29 using L31
.catch java/lang/Exception from L28 to L29 using L12
.catch java/io/FileNotFoundException from L32 to L33 using L30
.catch java/io/IOException from L32 to L33 using L31
.catch java/lang/Exception from L32 to L33 using L12
.catch java/io/FileNotFoundException from L34 to L35 using L30
.catch java/io/IOException from L34 to L35 using L31
.catch java/lang/Exception from L34 to L35 using L12
.catch java/io/FileNotFoundException from L35 to L36 using L30
.catch java/io/IOException from L35 to L36 using L31
.catch java/lang/Exception from L35 to L36 using L12
.catch java/lang/Exception from L36 to L37 using L12
.catch java/lang/Exception from L37 to L38 using L12
.catch java/lang/Exception from L39 to L40 using L7
.catch java/lang/Exception from L40 to L41 using L42
.catch java/lang/Exception from L41 to L43 using L42
.catch java/lang/Exception from L44 to L45 using L46
.catch java/lang/Exception from L45 to L47 using L48
.catch java/lang/Exception from L49 to L50 using L48
.catch java/io/FileNotFoundException from L50 to L51 using L52
.catch java/io/IOException from L50 to L51 using L53
.catch java/lang/Exception from L50 to L51 using L42
.catch java/io/FileNotFoundException from L54 to L55 using L52
.catch java/io/IOException from L54 to L55 using L53
.catch java/lang/Exception from L54 to L55 using L42
.catch java/io/FileNotFoundException from L56 to L57 using L52
.catch java/io/IOException from L56 to L57 using L53
.catch java/lang/Exception from L56 to L57 using L42
.catch java/io/FileNotFoundException from L57 to L58 using L52
.catch java/io/IOException from L57 to L58 using L53
.catch java/lang/Exception from L57 to L58 using L42
.catch java/lang/Exception from L58 to L59 using L42
.catch java/lang/Exception from L60 to L61 using L7
.catch java/lang/Exception from L62 to L63 using L2
.catch java/lang/Exception from L64 to L65 using L7
.catch java/lang/Exception from L66 to L67 using L12
.catch java/lang/Exception from L68 to L69 using L20
.catch java/lang/Exception from L70 to L71 using L12
.catch java/lang/Exception from L72 to L73 using L12
.catch java/lang/Exception from L74 to L75 using L12
.catch java/lang/Exception from L76 to L77 using L42
.catch java/lang/Exception from L78 to L79 using L42
.catch java/lang/Exception from L80 to L81 using L42
.catch java/lang/Exception from L82 to L83 using L42
L0:
new java/io/FileInputStream
dup
aload 0
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
astore 9
bipush 40
newarray byte
astore 10
aload 9
aload 10
invokevirtual java/io/FileInputStream/read([B)I
pop
aload 9
invokevirtual java/io/FileInputStream/close()V
L1:
iconst_0
istore 2
L84:
iload 2
iconst_4
if_icmpge L5
L3:
aload 10
iload 2
baload
bipush 8
newarray byte
dup
iconst_0
ldc_w 100
bastore
dup
iconst_1
ldc_w 101
bastore
dup
iconst_2
ldc_w 121
bastore
dup
iconst_3
ldc_w 10
bastore
dup
iconst_4
ldc_w 48
bastore
dup
iconst_5
ldc_w 51
bastore
dup
bipush 6
ldc_w 53
bastore
dup
bipush 7
ldc_w 0
bastore
iload 2
baload
if_icmpeq L85
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "The magic value is not the expected value "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
new java/lang/String
dup
aload 10
invokespecial java/lang/String/<init>([B)V
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L4:
return
L85:
iload 2
iconst_1
iadd
istore 2
goto L84
L5:
aload 10
invokestatic java/nio/ByteBuffer/wrap([B)Ljava/nio/ByteBuffer;
astore 9
aload 9
getstatic java/nio/ByteOrder/LITTLE_ENDIAN Ljava/nio/ByteOrder;
invokevirtual java/nio/ByteBuffer/order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
pop
aload 9
bipush 8
invokevirtual java/nio/ByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 9
invokevirtual java/nio/ByteBuffer/getInt()I
istore 2
aload 9
bipush 12
invokevirtual java/nio/ByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 9
invokevirtual java/nio/ByteBuffer/getInt()I
istore 3
aload 9
bipush 16
invokevirtual java/nio/ByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 9
invokevirtual java/nio/ByteBuffer/getInt()I
istore 5
aload 9
bipush 20
invokevirtual java/nio/ByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 9
invokevirtual java/nio/ByteBuffer/getInt()I
pop
aload 9
bipush 24
invokevirtual java/nio/ByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 9
invokevirtual java/nio/ByteBuffer/getInt()I
pop
aload 9
bipush 28
invokevirtual java/nio/ByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 9
invokevirtual java/nio/ByteBuffer/getInt()I
pop
aload 9
bipush 32
invokevirtual java/nio/ByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 9
invokevirtual java/nio/ByteBuffer/getInt()I
pop
iload 2
iload 3
aload 0
invokestatic com/chelpus/Utils/calcChecksumOdexFly(IILjava/io/File;)V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L64
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "644"
aastore
dup
iconst_2
aload 1
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L6:
aload 1
ifnull L4
L8:
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L4
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
lstore 6
L9:
lload 6
lconst_0
lcmp
ifeq L4
L10:
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 1
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/io/FileInputStream
dup
aload 1
invokespecial java/io/FileInputStream/<init>(Ljava/lang/String;)V
astore 10
new java/util/zip/ZipInputStream
dup
aload 10
invokespecial java/util/zip/ZipInputStream/<init>(Ljava/io/InputStream;)V
astore 11
aload 11
invokevirtual java/util/zip/ZipInputStream/getNextEntry()Ljava/util/zip/ZipEntry;
astore 9
L11:
aload 9
ifnull L37
L13:
aload 9
invokevirtual java/util/zip/ZipEntry/getName()Ljava/lang/String;
ldc "classes.dex"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
istore 8
L14:
iload 8
ifeq L74
L15:
aload 9
invokevirtual java/util/zip/ZipEntry/getTime()J
invokestatic com/chelpus/Utils/javaToDosTime(J)J
lstore 6
L16:
lload 6
l2i
istore 3
L18:
aload 9
invokevirtual java/util/zip/ZipEntry/getCrc()J
l2i
istore 4
L19:
iload 4
istore 2
iload 4
iconst_m1
if_icmpne L28
L21:
new java/util/zip/CRC32
dup
invokespecial java/util/zip/CRC32/<init>()V
astore 9
aload 9
invokevirtual java/util/zip/CRC32/reset()V
sipush 4096
newarray byte
astore 12
L22:
aload 11
aload 12
invokevirtual java/util/zip/ZipInputStream/read([B)I
istore 2
L23:
iload 2
iconst_m1
if_icmpeq L68
L24:
aload 9
aload 12
iconst_0
iload 2
invokevirtual java/util/zip/CRC32/update([BII)V
L25:
goto L22
L20:
astore 9
L26:
aload 9
invokevirtual java/lang/Exception/printStackTrace()V
L27:
iconst_0
istore 2
L28:
new java/io/RandomAccessFile
dup
aload 0
ldc "rw"
invokespecial java/io/RandomAccessFile/<init>(Ljava/io/File;Ljava/lang/String;)V
invokevirtual java/io/RandomAccessFile/getChannel()Ljava/nio/channels/FileChannel;
astore 9
aload 9
getstatic java/nio/channels/FileChannel$MapMode/READ_WRITE Ljava/nio/channels/FileChannel$MapMode;
lconst_0
aload 9
invokevirtual java/nio/channels/FileChannel/size()J
l2i
i2l
invokevirtual java/nio/channels/FileChannel/map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
astore 12
L29:
iload 3
ifeq L33
L32:
aload 12
iload 5
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 12
iload 3
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 12
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 12
iload 5
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 12
iload 3
bipush 8
ishr
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 12
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 12
iload 5
iconst_2
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 12
iload 3
bipush 16
ishr
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 12
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 12
iload 5
iconst_3
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 12
iload 3
bipush 24
ishr
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 12
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
L33:
iload 2
ifeq L35
L34:
aload 12
iload 5
iconst_4
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 12
iload 2
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 12
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 12
iload 5
iconst_5
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 12
iload 2
bipush 8
ishr
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 12
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 12
iload 5
bipush 6
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 12
iload 2
bipush 16
ishr
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 12
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 12
iload 5
bipush 7
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 12
iload 2
bipush 24
ishr
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 12
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
L35:
aload 9
invokevirtual java/nio/channels/FileChannel/close()V
L36:
aload 11
invokevirtual java/util/zip/ZipInputStream/closeEntry()V
L37:
aload 11
invokevirtual java/util/zip/ZipInputStream/close()V
aload 10
invokevirtual java/io/FileInputStream/close()V
L38:
return
L12:
astore 9
L39:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "alternative method"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 9
invokevirtual java/lang/Exception/printStackTrace()V
L40:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "start"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 1
invokestatic kellinwood/zipio/ZipInput/read(Ljava/lang/String;)Lkellinwood/zipio/ZipInput;
astore 1
aload 1
invokevirtual kellinwood/zipio/ZipInput/getEntries()Ljava/util/Map;
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 11
L41:
aload 11
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 11
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast kellinwood/zipio/ZioEntry
astore 10
aload 10
invokevirtual kellinwood/zipio/ZioEntry/getName()Ljava/lang/String;
ldc "classes.dex"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
istore 8
L43:
iload 8
ifeq L41
L44:
aload 10
invokevirtual kellinwood/zipio/ZioEntry/getTime()J
invokestatic com/chelpus/Utils/javaToDosTime(J)J
l2i
istore 3
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Time:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 3
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L45:
aload 10
invokevirtual kellinwood/zipio/ZioEntry/getCrc32()I
istore 4
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "CRC32:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 4
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L47:
iload 4
istore 2
iload 4
iconst_m1
if_icmpne L50
L49:
new java/util/zip/CRC32
dup
invokespecial java/util/zip/CRC32/<init>()V
astore 11
aload 11
invokevirtual java/util/zip/CRC32/reset()V
aload 10
invokevirtual kellinwood/zipio/ZioEntry/getData()[B
astore 10
aload 11
aload 10
iconst_0
aload 10
arraylength
invokevirtual java/util/zip/CRC32/update([BII)V
aload 11
invokevirtual java/util/zip/CRC32/getValue()J
l2i
istore 2
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "CRC32:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L50:
new java/io/RandomAccessFile
dup
aload 0
ldc "rw"
invokespecial java/io/RandomAccessFile/<init>(Ljava/io/File;Ljava/lang/String;)V
invokevirtual java/io/RandomAccessFile/getChannel()Ljava/nio/channels/FileChannel;
astore 0
aload 0
getstatic java/nio/channels/FileChannel$MapMode/READ_WRITE Ljava/nio/channels/FileChannel$MapMode;
lconst_0
aload 0
invokevirtual java/nio/channels/FileChannel/size()J
l2i
i2l
invokevirtual java/nio/channels/FileChannel/map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
astore 10
L51:
iload 3
ifeq L55
L54:
aload 10
iload 5
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 10
iload 3
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 10
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 10
iload 5
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 10
iload 3
bipush 8
ishr
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 10
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 10
iload 5
iconst_2
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 10
iload 3
bipush 16
ishr
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 10
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 10
iload 5
iconst_3
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 10
iload 3
bipush 24
ishr
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 10
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
L55:
iload 2
ifeq L57
L56:
aload 10
iload 5
iconst_4
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 10
iload 2
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 10
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 10
iload 5
iconst_5
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 10
iload 2
bipush 8
ishr
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 10
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 10
iload 5
bipush 6
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 10
iload 2
bipush 16
ishr
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 10
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
aload 10
iload 5
bipush 7
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 10
iload 2
bipush 24
ishr
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 10
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
L57:
aload 0
invokevirtual java/nio/channels/FileChannel/close()V
L58:
aload 1
invokevirtual kellinwood/zipio/ZipInput/close()V
L59:
return
L42:
astore 0
L60:
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L61:
return
L7:
astore 0
L62:
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
invokevirtual java/io/PrintStream/println(Ljava/lang/Object;)V
L63:
return
L2:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
return
L64:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chmod 644 "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
L65:
goto L6
L17:
astore 12
L66:
aload 12
invokevirtual java/lang/Exception/printStackTrace()V
L67:
iconst_0
istore 3
goto L18
L68:
aload 9
invokevirtual java/util/zip/CRC32/getValue()J
lstore 6
L69:
lload 6
l2i
istore 2
goto L28
L30:
astore 9
L70:
aload 9
invokevirtual java/io/FileNotFoundException/printStackTrace()V
L71:
goto L36
L31:
astore 9
L72:
aload 9
invokevirtual java/io/IOException/printStackTrace()V
L73:
goto L36
L74:
aload 11
invokevirtual java/util/zip/ZipInputStream/getNextEntry()Ljava/util/zip/ZipEntry;
astore 9
L75:
goto L11
L46:
astore 11
L76:
aload 11
invokevirtual java/lang/Exception/printStackTrace()V
L77:
iconst_0
istore 3
goto L45
L48:
astore 10
L78:
aload 10
invokevirtual java/lang/Exception/printStackTrace()V
L79:
iconst_0
istore 2
goto L50
L52:
astore 0
L80:
aload 9
invokevirtual java/lang/Exception/printStackTrace()V
L81:
goto L58
L53:
astore 0
L82:
aload 9
invokevirtual java/lang/Exception/printStackTrace()V
L83:
goto L58
.limit locals 13
.limit stack 6
.end method

.method public static gen_sha1withrsa(Ljava/lang/String;)Ljava/lang/String;
aload 0
invokevirtual java/lang/String/length()I
newarray byte
astore 0
getstatic com/chelpus/Utils/rnd Ljava/util/Random;
aload 0
invokevirtual java/util/Random/nextBytes([B)V
aload 0
invokestatic com/google/android/finsky/billing/iab/google/util/Base64/encode([B)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public static getApkIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
astore 1
aload 1
aload 0
iconst_1
invokevirtual android/content/pm/PackageManager/getPackageArchiveInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
astore 2
aload 2
ifnull L0
aload 2
getfield android/content/pm/PackageInfo/applicationInfo Landroid/content/pm/ApplicationInfo;
astore 2
getstatic android/os/Build$VERSION/SDK_INT I
bipush 8
if_icmplt L1
aload 2
aload 0
putfield android/content/pm/ApplicationInfo/sourceDir Ljava/lang/String;
aload 2
aload 0
putfield android/content/pm/ApplicationInfo/publicSourceDir Ljava/lang/String;
L1:
aload 2
aload 1
invokevirtual android/content/pm/ApplicationInfo/loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;
areturn
L0:
aconst_null
areturn
.limit locals 3
.limit stack 3
.end method

.method public static getApkInfo(Ljava/lang/String;)Landroid/content/pm/ApplicationInfo;
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
aload 0
iconst_1
invokevirtual android/content/pm/PackageManager/getPackageArchiveInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
astore 1
aload 1
ifnull L0
aload 1
getfield android/content/pm/PackageInfo/applicationInfo Landroid/content/pm/ApplicationInfo;
astore 1
getstatic android/os/Build$VERSION/SDK_INT I
bipush 8
if_icmplt L1
aload 1
aload 0
putfield android/content/pm/ApplicationInfo/sourceDir Ljava/lang/String;
aload 1
aload 0
putfield android/content/pm/ApplicationInfo/publicSourceDir Ljava/lang/String;
L1:
aload 1
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 3
.end method

.method public static getApkLabelName(Ljava/lang/String;)Ljava/lang/String;
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
astore 1
aload 1
aload 0
iconst_1
invokevirtual android/content/pm/PackageManager/getPackageArchiveInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
astore 2
aload 2
ifnull L0
aload 2
getfield android/content/pm/PackageInfo/applicationInfo Landroid/content/pm/ApplicationInfo;
astore 2
getstatic android/os/Build$VERSION/SDK_INT I
bipush 8
if_icmplt L1
aload 2
aload 0
putfield android/content/pm/ApplicationInfo/sourceDir Ljava/lang/String;
aload 2
aload 0
putfield android/content/pm/ApplicationInfo/publicSourceDir Ljava/lang/String;
L1:
aload 2
aload 1
invokevirtual android/content/pm/ApplicationInfo/loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
invokeinterface java/lang/CharSequence/toString()Ljava/lang/String; 0
areturn
L0:
aconst_null
areturn
.limit locals 3
.limit stack 3
.end method

.method public static getApkPackageInfo(Ljava/lang/String;)Landroid/content/pm/PackageInfo;
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
aload 0
iconst_1
invokevirtual android/content/pm/PackageManager/getPackageArchiveInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
astore 0
aload 0
ifnull L0
aload 0
areturn
L0:
aconst_null
areturn
.limit locals 1
.limit stack 3
.end method

.method public static getAssets(Ljava/lang/String;Ljava/lang/String;)V
.throws java/io/IOException
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/mkdirs()Z
pop
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getRes()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getAssets()Landroid/content/res/AssetManager;
aload 0
invokevirtual android/content/res/AssetManager/open(Ljava/lang/String;)Ljava/io/InputStream;
astore 3
new java/io/FileOutputStream
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
astore 0
sipush 8192
newarray byte
astore 1
L0:
aload 3
aload 1
invokevirtual java/io/InputStream/read([B)I
istore 2
iload 2
iconst_m1
if_icmpeq L1
aload 0
aload 1
iconst_0
iload 2
invokevirtual java/io/OutputStream/write([BII)V
goto L0
L1:
aload 3
invokevirtual java/io/InputStream/close()V
aload 0
invokevirtual java/io/OutputStream/flush()V
aload 0
invokevirtual java/io/OutputStream/close()V
return
.limit locals 4
.limit stack 4
.end method

.method public static getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L2
.catch java/lang/Exception from L7 to L8 using L2
new android/text/SpannableString
dup
aload 0
invokespecial android/text/SpannableString/<init>(Ljava/lang/CharSequence;)V
astore 4
iconst_0
istore 3
L0:
aload 2
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "bold"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L3
L1:
iconst_1
istore 3
L3:
aload 2
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "bold_italic"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L5
L4:
iconst_3
istore 3
L5:
aload 2
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "italic"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L7
L6:
iconst_2
istore 3
L7:
aload 4
new android/text/style/ForegroundColorSpan
dup
iload 1
invokespecial android/text/style/ForegroundColorSpan/<init>(I)V
iconst_0
aload 0
invokevirtual java/lang/String/length()I
iconst_0
invokevirtual android/text/SpannableString/setSpan(Ljava/lang/Object;III)V
aload 4
new android/text/style/StyleSpan
dup
iload 3
invokespecial android/text/style/StyleSpan/<init>(I)V
iconst_0
aload 0
invokevirtual java/lang/String/length()I
iconst_0
invokevirtual android/text/SpannableString/setSpan(Ljava/lang/Object;III)V
L8:
aload 4
areturn
L2:
astore 2
aload 2
invokevirtual java/lang/Exception/printStackTrace()V
aload 0
astore 2
aload 0
invokevirtual java/lang/String/length()I
ifne L9
ldc " "
astore 2
L9:
new android/text/SpannableString
dup
aload 2
invokespecial android/text/SpannableString/<init>(Ljava/lang/CharSequence;)V
areturn
.limit locals 5
.limit stack 5
.end method

.method public static getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L2
.catch java/lang/Exception from L7 to L8 using L2
.catch java/lang/Exception from L8 to L9 using L2
new android/text/SpannableString
dup
aload 0
invokespecial android/text/SpannableString/<init>(Ljava/lang/CharSequence;)V
astore 4
iconst_0
istore 3
L0:
aload 2
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "bold"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L3
L1:
iconst_1
istore 3
L3:
aload 2
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "bold_italic"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L5
L4:
iconst_3
istore 3
L5:
aload 2
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "italic"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L7
L6:
iconst_2
istore 3
L7:
aload 1
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L8
aload 4
new android/text/style/ForegroundColorSpan
dup
aload 1
invokestatic android/graphics/Color/parseColor(Ljava/lang/String;)I
invokespecial android/text/style/ForegroundColorSpan/<init>(I)V
iconst_0
aload 0
invokevirtual java/lang/String/length()I
iconst_0
invokevirtual android/text/SpannableString/setSpan(Ljava/lang/Object;III)V
L8:
aload 4
new android/text/style/StyleSpan
dup
iload 3
invokespecial android/text/style/StyleSpan/<init>(I)V
iconst_0
aload 0
invokevirtual java/lang/String/length()I
iconst_0
invokevirtual android/text/SpannableString/setSpan(Ljava/lang/Object;III)V
L9:
aload 4
areturn
L2:
astore 1
aload 1
invokevirtual java/lang/Exception/printStackTrace()V
aload 0
astore 1
aload 0
invokevirtual java/lang/String/length()I
ifne L10
ldc " "
astore 1
L10:
new android/text/SpannableString
dup
aload 1
invokespecial android/text/SpannableString/<init>(Ljava/lang/CharSequence;)V
areturn
.limit locals 5
.limit stack 5
.end method

.method public static getCurrentRuntimeValue()Ljava/lang/String;
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/ClassNotFoundException from L5 to L6 using L7
.catch java/lang/Exception from L5 to L6 using L8
.catch java/lang/NoSuchMethodException from L6 to L9 using L10
.catch java/lang/ClassNotFoundException from L6 to L9 using L7
.catch java/lang/Exception from L6 to L9 using L8
.catch java/lang/IllegalAccessException from L11 to L12 using L13
.catch java/lang/IllegalArgumentException from L11 to L12 using L14
.catch java/lang/reflect/InvocationTargetException from L11 to L12 using L15
.catch java/lang/NoSuchMethodException from L11 to L12 using L10
.catch java/lang/ClassNotFoundException from L11 to L12 using L7
.catch java/lang/Exception from L11 to L12 using L8
.catch java/lang/IllegalAccessException from L16 to L17 using L13
.catch java/lang/IllegalArgumentException from L16 to L17 using L14
.catch java/lang/reflect/InvocationTargetException from L16 to L17 using L15
.catch java/lang/NoSuchMethodException from L16 to L17 using L10
.catch java/lang/ClassNotFoundException from L16 to L17 using L7
.catch java/lang/Exception from L16 to L17 using L8
.catch java/lang/IllegalAccessException from L18 to L19 using L13
.catch java/lang/IllegalArgumentException from L18 to L19 using L14
.catch java/lang/reflect/InvocationTargetException from L18 to L19 using L15
.catch java/lang/NoSuchMethodException from L18 to L19 using L10
.catch java/lang/ClassNotFoundException from L18 to L19 using L7
.catch java/lang/Exception from L18 to L19 using L8
L0:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/runtime Ljava/lang/String;
ldc "ART"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L3
L1:
ldc "ART"
areturn
L3:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/runtime Ljava/lang/String;
ldc "DALVIK"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L20
L4:
ldc "Dalvik"
areturn
L20:
ldc "ART"
areturn
L2:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L5:
ldc "android.os.SystemProperties"
invokestatic java/lang/Class/forName(Ljava/lang/String;)Ljava/lang/Class;
astore 0
L6:
aload 0
ldc "get"
iconst_2
anewarray java/lang/Class
dup
iconst_0
ldc java/lang/String
aastore
dup
iconst_1
ldc java/lang/String
aastore
invokevirtual java/lang/Class/getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
astore 1
L9:
aload 1
ifnonnull L11
ldc "WTF?!"
areturn
L11:
aload 1
aload 0
iconst_2
anewarray java/lang/Object
dup
iconst_0
ldc "persist.sys.dalvik.vm.lib"
aastore
dup
iconst_1
ldc "Dalvik"
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
astore 1
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 1
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
ldc "libdvm.so"
aload 1
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L16
L12:
ldc "Dalvik"
areturn
L16:
ldc "libart.so"
aload 1
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L21
L17:
ldc "ART"
areturn
L21:
aload 1
astore 0
L18:
ldc "libartd.so"
aload 1
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L22
L19:
ldc "ART debug build"
areturn
L13:
astore 0
ldc "IllegalAccessException"
areturn
L15:
astore 0
ldc "InvocationTargetException"
areturn
L10:
astore 0
ldc "SystemProperties.get(String key, String def) method is not found"
areturn
L7:
astore 0
aload 0
invokevirtual java/lang/ClassNotFoundException/printStackTrace()V
ldc "Dalvik"
areturn
L8:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
ldc "Dalvik"
astore 0
L22:
aload 0
areturn
L14:
astore 0
ldc "IllegalArgumentException"
areturn
.limit locals 2
.limit stack 6
.end method

.method public static getCustomPatchesForPkg(Ljava/lang/String;)Ljava/util/ArrayList;
.signature "(Ljava/lang/String;)Ljava/util/ArrayList<Ljava/io/File;>;"
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
.catch java/lang/Exception from L3 to L4 using L5
.catch java/lang/Exception from L6 to L7 using L5
.catch java/lang/Exception from L8 to L9 using L5
.catch java/lang/Exception from L9 to L10 using L2
.catch java/lang/Exception from L11 to L12 using L2
.catch java/lang/Exception from L12 to L13 using L2
.catch java/lang/Exception from L14 to L15 using L2
.catch java/lang/Exception from L16 to L17 using L2
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 2
L0:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
ifnull L1
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
arraylength
ifne L9
L1:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/init()V
L3:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getConfig()Landroid/content/SharedPreferences;
ldc "basepath"
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/basepath Ljava/lang/String;
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/basepath Ljava/lang/String;
new java/io/File
dup
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/basepath Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/listFiles()[Ljava/io/File;
arraylength
anewarray java/io/File
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
new java/io/File
dup
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/basepath Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/listFiles()[Ljava/io/File;
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 3
aload 3
invokevirtual java/util/ArrayList/clear()V
L4:
iconst_0
istore 1
L6:
iload 1
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
arraylength
if_icmpge L8
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
iload 1
aaload
invokevirtual java/io/File/isFile()Z
ifeq L18
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
iload 1
aaload
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc ".txt"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L18
aload 3
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
iload 1
aaload
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L7:
goto L18
L8:
aload 3
invokevirtual java/util/ArrayList/size()I
ifle L9
aload 3
invokevirtual java/util/ArrayList/size()I
anewarray java/io/File
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
aload 3
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
invokevirtual java/util/ArrayList/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
checkcast [Ljava/io/File;
checkcast [Ljava/io/File;
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
L9:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
ifnull L19
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
arraylength
ifle L19
L10:
iconst_0
istore 1
L11:
iload 1
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
arraylength
if_icmpge L19
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
iload 1
aaload
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc "_%ALL%.txt"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L12
aload 0
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
iload 1
aaload
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc "_%ALL%.txt"
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L16
aload 2
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
iload 1
aaload
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L12:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
iload 1
aaload
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc ".txt"
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
aload 0
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L20
aload 2
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
iload 1
aaload
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L13:
goto L20
L5:
astore 3
L14:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Not found dir by Lucky Patcher. Custom patch not found."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L15:
goto L9
L2:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L19:
aload 2
areturn
L16:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
iload 1
aaload
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc "_%ALL%.txt"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L12
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
iload 1
aaload
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc "%ALL%_"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L12
aload 0
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
iload 1
aaload
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc "%ALL%_"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
iconst_1
aaload
ldc "_%ALL%.txt"
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L12
aload 2
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
iload 1
aaload
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L17:
goto L12
L18:
iload 1
iconst_1
iadd
istore 1
goto L6
L20:
iload 1
iconst_1
iadd
istore 1
goto L11
.limit locals 4
.limit stack 4
.end method

.method public static getDirs(Ljava/io/File;)Ljava/io/File;
aload 0
invokevirtual java/io/File/toString()Ljava/lang/String;
getstatic java/io/File/separator Ljava/lang/String;
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 3
ldc ""
astore 0
iconst_0
istore 1
L0:
iload 1
aload 3
arraylength
if_icmpge L1
aload 0
astore 2
iload 1
aload 3
arraylength
iconst_1
isub
if_icmpeq L2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
iload 1
aaload
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic java/io/File/separator Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 2
L2:
iload 1
iconst_1
iadd
istore 1
aload 2
astore 0
goto L0
L1:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
areturn
.limit locals 4
.limit stack 3
.end method

.method public static getFileDalvikCache(Ljava/lang/String;)Ljava/io/File;
.catch java/io/IOException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
.catch java/io/IOException from L4 to L5 using L2
.catch java/lang/Exception from L4 to L5 using L3
.catch java/io/IOException from L6 to L7 using L2
.catch java/lang/Exception from L6 to L7 using L3
.catch java/io/IOException from L8 to L9 using L2
.catch java/lang/Exception from L8 to L9 using L3
aload 0
ldc "/"
ldc "@"
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 0
L10:
aload 0
ldc "@"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L11
aload 0
ldc "@"
ldc ""
invokevirtual java/lang/String/replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 0
goto L10
L11:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "@classes.dex"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 4
ldc ""
astore 2
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L12
aload 2
astore 1
L0:
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
new java/io/File
dup
ldc "/data/dalvik-cache"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
aload 4
invokevirtual com/chelpus/Utils/findFileContainText(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;
astore 3
L1:
aload 3
astore 0
aload 3
astore 1
aload 3
astore 2
L4:
aload 3
ldc "\"/data/dalvik-cache/arm/\""
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L9
L5:
aload 3
astore 0
aload 3
astore 1
aload 3
astore 2
L6:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/data/dalvik-cache/arm64/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L9
L7:
aload 3
astore 1
aload 3
astore 2
L8:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/data/dalvik-cache/arm64/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
L9:
aload 0
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L13
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L13
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
areturn
L2:
astore 0
aload 0
invokevirtual java/io/IOException/printStackTrace()V
aload 1
astore 0
goto L9
L3:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
aload 2
astore 0
goto L9
L12:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/data/dalvik-cache/x86/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L14
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/data/dalvik-cache/x86/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
areturn
L14:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/data/dalvik-cache/arm64/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L15
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/data/dalvik-cache/arm64/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
areturn
L15:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/data/dalvik-cache/arm/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L16
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/data/dalvik-cache/arm/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
areturn
L16:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/data/dalvik-cache/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L13
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/data/dalvik-cache/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
areturn
L13:
aconst_null
areturn
.limit locals 5
.limit stack 4
.end method

.method public static getFileDalvikCacheName(Ljava/lang/String;)Ljava/io/File;
iconst_0
istore 1
new java/io/File
dup
ldc "/data/art-cache/"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L0
aload 0
ldc "/"
ldc "@"
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 3
L1:
aload 3
ldc "@"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L2
aload 3
ldc "@"
ldc ""
invokevirtual java/lang/String/replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 3
goto L1
L2:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".oat"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 3
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/data/art-cache/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L0
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "\nLuckyPatcher: found dalvik-cache! "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/data/art-cache/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/data/art-cache/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 3
L3:
aload 3
areturn
L0:
bipush 8
anewarray java/lang/String
astore 3
aload 3
iconst_0
ldc "/data/dalvik-cache/"
aastore
aload 3
iconst_1
ldc "/data/dalvik-cache/arm64/"
aastore
aload 3
iconst_2
ldc "/data/dalvik-cache/arm/"
aastore
aload 3
iconst_3
ldc "/data/dalvik-cache/x86/"
aastore
aload 3
iconst_4
ldc "/sd-ext/data/dalvik-cache/"
aastore
aload 3
iconst_5
ldc "/cache/dalvik-cache/"
aastore
aload 3
bipush 6
ldc "/sd-ext/data/cache/dalvik-cache/"
aastore
aload 3
bipush 7
ldc "/data/cache/dalvik-cache/"
aastore
aload 0
ldc "/"
ldc "@"
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 0
L4:
aload 0
ldc "@"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L5
aload 0
ldc "@"
ldc ""
invokevirtual java/lang/String/replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 0
goto L4
L5:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "@classes.dex"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 4
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "dalvikfile: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aconst_null
astore 0
aload 3
arraylength
istore 2
L6:
iload 1
iload 2
if_icmpge L7
aload 3
iload 1
aaload
astore 5
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L8
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "\nLuckyPatcher: found dalvik-cache! "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
L8:
iload 1
iconst_1
iadd
istore 1
goto L6
L7:
aload 0
astore 3
aload 0
ifnonnull L3
ldc "/data/dalvik-cache/arm"
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifne L9
ldc "/data/dalvik-cache/arm64"
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifne L9
ldc "/data/dalvik-cache/x86"
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifeq L10
L9:
ldc "/data/dalvik-cache/arm"
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifeq L11
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/data/dalvik-cache/arm/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
L11:
ldc "/data/dalvik-cache/arm64"
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifeq L12
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/data/dalvik-cache/arm64/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
L12:
aload 0
astore 3
ldc "/data/dalvik-cache/x86"
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifeq L3
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/data/dalvik-cache/x86/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
areturn
L10:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/data/dalvik-cache/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
areturn
.limit locals 6
.limit stack 4
.end method

.method public static getFilesWithPkgName(Ljava/lang/String;Ljava/io/File;Ljava/util/ArrayList;)V
.signature "(Ljava/lang/String;Ljava/io/File;Ljava/util/ArrayList<Ljava/io/File;>;)V"
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
.catch java/lang/Exception from L6 to L7 using L5
.catch java/lang/Exception from L8 to L9 using L5
.catch java/lang/Exception from L10 to L11 using L12
.catch java/lang/Exception from L13 to L14 using L5
ldc "/system"
ldc "rw"
invokestatic com/chelpus/Utils/remount(Ljava/lang/String;Ljava/lang/String;)Z
pop
aload 1
invokevirtual java/io/File/listFiles()[Ljava/io/File;
astore 8
aload 8
ifnull L15
aload 8
arraylength
ifeq L15
aload 8
arraylength
istore 4
iconst_0
istore 3
L16:
iload 3
iload 4
if_icmpge L17
aload 8
iload 3
aaload
astore 1
aload 1
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
ldc ".apk"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L1
L0:
aload 0
new com/android/vending/billing/InAppBillingService/LUCK/FileApkListItem
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
aload 1
iconst_0
invokespecial com/android/vending/billing/InAppBillingService/LUCK/FileApkListItem/<init>(Landroid/content/Context;Ljava/io/File;Z)V
getfield com/android/vending/billing/InAppBillingService/LUCK/FileApkListItem/pkgName Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L1
aload 2
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L1:
iload 3
iconst_1
iadd
istore 3
goto L16
L2:
astore 1
aload 1
invokevirtual java/lang/Exception/printStackTrace()V
goto L1
L17:
aload 8
arraylength
istore 5
iconst_0
istore 3
L18:
iload 3
iload 5
if_icmpge L19
aload 8
iload 3
aaload
astore 1
aload 1
invokevirtual java/io/File/isDirectory()Z
ifeq L20
L3:
aload 1
invokevirtual java/io/File/listFiles()[Ljava/io/File;
astore 1
L4:
aload 1
ifnull L20
L6:
aload 1
arraylength
ifeq L20
aload 1
arraylength
istore 6
L7:
iconst_0
istore 4
L21:
iload 4
iload 6
if_icmpge L20
aload 1
iload 4
aaload
astore 9
L8:
aload 9
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
ldc ".apk"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
istore 7
L9:
iload 7
ifeq L11
L10:
aload 0
new com/android/vending/billing/InAppBillingService/LUCK/FileApkListItem
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
aload 9
iconst_0
invokespecial com/android/vending/billing/InAppBillingService/LUCK/FileApkListItem/<init>(Landroid/content/Context;Ljava/io/File;Z)V
getfield com/android/vending/billing/InAppBillingService/LUCK/FileApkListItem/pkgName Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L11
aload 2
aload 9
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L11:
iload 4
iconst_1
iadd
istore 4
goto L21
L12:
astore 9
L13:
aload 9
invokevirtual java/lang/Exception/printStackTrace()V
L14:
goto L11
L5:
astore 1
aload 1
invokevirtual java/lang/Exception/printStackTrace()V
L20:
iload 3
iconst_1
iadd
istore 3
goto L18
L15:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher: 0 packages found in "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L19:
return
.limit locals 10
.limit stack 6
.end method

.method public static getMethodsIds(Ljava/lang/String;Ljava/util/ArrayList;Z)Z
.signature "(Ljava/lang/String;Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;>;Z)Z"
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L7
.catch java/lang/Exception from L8 to L9 using L10
.catch java/lang/Exception from L11 to L12 using L10
.catch java/lang/Exception from L13 to L14 using L10
.catch java/lang/Exception from L15 to L16 using L10
.catch java/lang/Exception from L17 to L18 using L10
.catch java/lang/Exception from L19 to L20 using L10
.catch java/lang/Exception from L21 to L22 using L10
.catch java/lang/Exception from L23 to L24 using L10
.catch java/lang/Exception from L25 to L26 using L10
.catch java/lang/Exception from L27 to L28 using L10
.catch java/lang/Exception from L29 to L30 using L10
.catch java/lang/Exception from L31 to L32 using L10
.catch java/lang/Exception from L33 to L34 using L10
.catch java/lang/Exception from L35 to L36 using L10
.catch java/lang/Exception from L37 to L38 using L10
.catch java/lang/Exception from L39 to L40 using L10
.catch java/lang/Exception from L41 to L42 using L10
.catch java/lang/Exception from L43 to L44 using L10
.catch java/lang/Exception from L45 to L46 using L10
.catch java/lang/Exception from L47 to L48 using L10
.catch java/lang/Exception from L49 to L50 using L10
.catch java/lang/Exception from L51 to L52 using L10
.catch java/lang/Exception from L53 to L54 using L7
.catch java/lang/Exception from L55 to L56 using L10
.catch java/lang/Exception from L57 to L58 using L10
.catch java/lang/Exception from L59 to L60 using L10
.catch java/lang/Exception from L61 to L62 using L10
.catch java/lang/Exception from L63 to L64 using L10
.catch java/lang/Exception from L65 to L66 using L10
.catch java/lang/Exception from L67 to L68 using L10
.catch java/lang/Exception from L69 to L70 using L10
.catch java/lang/Exception from L71 to L72 using L10
.catch java/lang/Exception from L73 to L74 using L10
.catch java/lang/Exception from L75 to L76 using L10
.catch java/lang/Exception from L77 to L78 using L10
.catch java/lang/Exception from L79 to L80 using L10
.catch java/lang/Exception from L81 to L82 using L10
.catch java/lang/Exception from L83 to L84 using L10
.catch java/lang/Exception from L85 to L86 using L10
.catch java/lang/Exception from L87 to L88 using L10
.catch java/lang/Exception from L89 to L90 using L10
.catch java/lang/Exception from L91 to L92 using L10
.catch java/lang/Exception from L93 to L94 using L10
.catch java/lang/Exception from L95 to L96 using L10
.catch java/lang/Exception from L97 to L98 using L10
.catch java/lang/Exception from L99 to L100 using L10
.catch java/lang/Exception from L101 to L102 using L10
.catch java/lang/Exception from L103 to L104 using L10
.catch java/lang/Exception from L105 to L106 using L10
.catch java/lang/Exception from L107 to L108 using L10
.catch java/lang/Exception from L109 to L110 using L10
.catch java/lang/Exception from L111 to L112 using L10
.catch java/lang/Exception from L113 to L114 using L10
.catch java/lang/Exception from L115 to L116 using L2
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "scan: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iconst_0
istore 10
iconst_0
istore 9
iconst_0
istore 7
iconst_0
istore 11
iconst_0
istore 12
iconst_0
istore 8
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
aload 0
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
pop
iload 10
istore 6
iload 2
ifne L117
iload 10
istore 6
aload 0
ifnull L117
iload 10
istore 6
iload 12
istore 2
L0:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L117
L1:
iload 12
istore 2
L3:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
lstore 13
L4:
iload 10
istore 6
lload 13
lconst_0
lcmp
ifeq L117
iload 11
istore 6
L5:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
istore 2
L6:
iload 10
istore 6
iload 2
ifeq L117
iload 8
istore 2
L8:
new java/io/RandomAccessFile
dup
aload 0
ldc "r"
invokespecial java/io/RandomAccessFile/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual java/io/RandomAccessFile/getChannel()Ljava/nio/channels/FileChannel;
astore 0
L9:
iload 8
istore 2
L11:
aload 0
getstatic java/nio/channels/FileChannel$MapMode/READ_ONLY Ljava/nio/channels/FileChannel$MapMode;
lconst_0
aload 0
invokevirtual java/nio/channels/FileChannel/size()J
l2i
i2l
invokevirtual java/nio/channels/FileChannel/map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
astore 15
L12:
iload 8
istore 2
L13:
aload 15
bipush 64
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L14:
iload 8
istore 2
L15:
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
invokestatic com/chelpus/Utils/convertFourBytesToInt(BBBB)I
istore 5
L16:
iload 8
istore 2
L17:
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
invokestatic com/chelpus/Utils/convertFourBytesToInt(BBBB)I
istore 3
L18:
iload 8
istore 2
L19:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher offset_to_data="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 3
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L20:
iload 8
istore 2
L21:
aload 15
iload 3
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L22:
iconst_0
istore 4
iconst_0
istore 3
L118:
iload 3
iload 5
if_icmpge L119
iload 8
istore 2
L23:
iconst_4
newarray byte
astore 16
L24:
iload 8
istore 2
L25:
aload 16
iconst_0
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
bastore
L26:
iload 8
istore 2
L27:
aload 16
iconst_1
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
bastore
L28:
iload 8
istore 2
L29:
aload 16
iconst_2
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
bastore
L30:
iload 8
istore 2
L31:
aload 16
iconst_3
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
bastore
L32:
iload 8
istore 2
L33:
aload 1
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 17
L34:
iload 8
istore 2
L35:
aload 17
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L120
L36:
iload 8
istore 2
L37:
aload 17
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/CommandItem
astore 18
L38:
iload 8
istore 2
L39:
aload 16
iconst_0
baload
aload 18
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/Object [B
iconst_0
baload
if_icmpne L34
L40:
iload 8
istore 2
L41:
aload 16
iconst_1
baload
aload 18
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/Object [B
iconst_1
baload
if_icmpne L34
L42:
iload 8
istore 2
L43:
aload 16
iconst_2
baload
aload 18
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/Object [B
iconst_2
baload
if_icmpne L34
L44:
iload 8
istore 2
L45:
aload 16
iconst_3
baload
aload 18
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/Object [B
iconst_3
baload
if_icmpne L34
L46:
iload 8
istore 2
L47:
aload 18
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/id_object [B
iconst_0
iload 4
i2b
bastore
L48:
iload 8
istore 2
L49:
aload 18
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/id_object [B
iconst_1
iload 4
bipush 8
ishr
i2b
bastore
L50:
iload 8
istore 2
L51:
aload 18
iconst_1
putfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/found_id_object Z
L52:
goto L34
L10:
astore 0
iload 2
istore 6
L53:
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L54:
iload 2
istore 6
L117:
iload 6
ireturn
L120:
iload 4
iconst_1
iadd
istore 4
iload 3
iconst_1
iadd
istore 3
goto L118
L119:
iconst_0
istore 3
iload 8
istore 2
L55:
aload 1
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 16
L56:
iload 8
istore 2
L57:
aload 16
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L121
L58:
iload 8
istore 2
L59:
aload 16
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/CommandItem
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/found_id_object Z
ifeq L56
L60:
iconst_1
istore 3
goto L56
L121:
iload 3
ifeq L122
iload 8
istore 2
L61:
aload 15
bipush 88
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L62:
iload 8
istore 2
L63:
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
invokestatic com/chelpus/Utils/convertFourBytesToInt(BBBB)I
istore 3
L64:
iload 8
istore 2
L65:
aload 15
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
invokestatic com/chelpus/Utils/convertFourBytesToInt(BBBB)I
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L66:
iload 8
istore 2
L67:
iload 3
newarray byte
astore 16
L68:
iconst_0
istore 4
iconst_0
istore 3
iload 9
istore 6
L123:
iload 6
istore 2
iload 6
istore 7
L69:
iload 3
aload 16
arraylength
if_icmpge L122
L70:
iload 6
istore 2
L71:
bipush 8
newarray byte
astore 17
L72:
iload 6
istore 2
L73:
aload 17
iconst_0
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
bastore
L74:
iload 6
istore 2
L75:
aload 17
iconst_1
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
bastore
L76:
iload 6
istore 2
L77:
aload 17
iconst_2
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
bastore
L78:
iload 6
istore 2
L79:
aload 17
iconst_3
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
bastore
L80:
iload 6
istore 2
L81:
aload 17
iconst_4
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
bastore
L82:
iload 6
istore 2
L83:
aload 17
iconst_5
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
bastore
L84:
iload 6
istore 2
L85:
aload 17
bipush 6
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
bastore
L86:
iload 6
istore 2
L87:
aload 17
bipush 7
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
bastore
L88:
iload 6
istore 2
L89:
aload 1
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 18
L90:
iload 6
istore 2
L91:
aload 18
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L124
L92:
iload 6
istore 2
L93:
aload 18
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/CommandItem
astore 19
L94:
iload 6
istore 2
L95:
aload 17
iconst_0
baload
aload 19
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/id_object [B
iconst_0
baload
if_icmpne L90
L96:
iload 6
istore 2
L97:
aload 17
iconst_1
baload
aload 19
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/id_object [B
iconst_1
baload
if_icmpne L90
L98:
iload 6
istore 2
L99:
aload 17
iconst_4
baload
aload 19
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/Method [B
iconst_0
baload
if_icmpne L90
L100:
iload 6
istore 2
L101:
aload 17
iconst_5
baload
aload 19
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/Method [B
iconst_1
baload
if_icmpne L90
L102:
iload 6
istore 2
L103:
aload 17
bipush 6
baload
aload 19
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/Method [B
iconst_2
baload
if_icmpne L90
L104:
iload 6
istore 2
L105:
aload 17
bipush 7
baload
aload 19
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/Method [B
iconst_3
baload
if_icmpne L90
L106:
iload 6
istore 2
L107:
aload 19
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/index_command [B
iconst_0
iload 4
i2b
bastore
L108:
iload 6
istore 2
L109:
aload 19
getfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/index_command [B
iconst_1
iload 4
bipush 8
ishr
i2b
bastore
L110:
iload 6
istore 2
L111:
aload 19
iconst_1
putfield com/android/vending/billing/InAppBillingService/LUCK/CommandItem/found_index_command Z
L112:
iconst_1
istore 6
goto L90
L122:
iload 7
istore 2
L113:
aload 0
invokevirtual java/nio/channels/FileChannel/close()V
L114:
iload 7
ireturn
L7:
astore 0
iload 6
istore 2
L115:
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L116:
iload 6
ireturn
L2:
astore 0
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
invokevirtual java/io/PrintStream/println(Ljava/lang/Object;)V
iload 2
ireturn
L124:
iload 4
iconst_1
iadd
istore 4
iload 3
iconst_1
iadd
istore 3
goto L123
.limit locals 20
.limit stack 6
.end method

.method public static getMounts()Ljava/util/ArrayList;
.signature "()Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/Mount;>;"
.catch java/io/IOException from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
.catch java/lang/Exception from L6 to L7 using L5
.catch java/lang/Exception from L8 to L9 using L5
.catch java/lang/Exception from L10 to L11 using L5
.catch java/lang/Exception from L12 to L13 using L14
.catch java/lang/Exception from L13 to L15 using L16
.catch java/lang/Exception from L15 to L17 using L18
.catch java/lang/Exception from L19 to L20 using L18
.catch java/lang/Exception from L21 to L22 using L18
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifne L23
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "mount"
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
astore 6
aload 6
astore 5
aload 6
ldc "~"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L24
invokestatic com/chelpus/Utils/exitRoot()V
L0:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getSu()Ljava/lang/Process;
pop
L1:
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/busybox mount"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
astore 5
aload 5
astore 6
aload 5
ldc "~"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L25
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "busybox mount"
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
astore 6
L25:
aload 6
astore 5
aload 6
ldc "~"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L24
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "toolbox mount"
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
astore 5
L24:
aload 5
ldc "\n"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 5
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 6
iconst_0
istore 1
iconst_0
istore 0
L3:
aload 5
arraylength
istore 4
L4:
iconst_0
istore 3
L26:
iload 0
istore 1
iload 3
iload 4
if_icmpge L27
iload 0
istore 1
L6:
aload 5
iload 3
aaload
ldc " on "
ldc " "
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
ldc " type "
ldc " "
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
ldc "\\s+"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 7
L7:
iload 0
istore 2
iload 0
istore 1
L8:
aload 7
iconst_1
aaload
ldc "/system"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L28
L9:
iconst_1
istore 2
L28:
iload 2
istore 1
L10:
aload 6
new com/android/vending/billing/InAppBillingService/LUCK/Mount
dup
new java/io/File
dup
aload 7
iconst_0
aaload
invokespecial java/io/File/<init>(Ljava/lang/String;)V
new java/io/File
dup
aload 7
iconst_1
aaload
invokespecial java/io/File/<init>(Ljava/lang/String;)V
aload 7
iconst_2
aaload
aload 7
iconst_3
aaload
invokespecial com/android/vending/billing/InAppBillingService/LUCK/Mount/<init>(Ljava/io/File;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L11:
iload 3
iconst_1
iadd
istore 3
iload 2
istore 0
goto L26
L2:
astore 5
aload 5
invokevirtual java/io/IOException/printStackTrace()V
goto L1
L23:
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "mount"
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
astore 6
aload 6
astore 5
aload 6
ldc "~"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L24
iconst_2
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/busybox"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_1
ldc "mount"
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
astore 5
aload 5
astore 6
aload 5
ldc "~"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L29
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "busybox"
aastore
dup
iconst_1
ldc "mount"
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
astore 6
L29:
aload 6
astore 5
aload 6
ldc "~"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L24
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "toolbox"
aastore
dup
iconst_1
ldc "mount"
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
astore 5
goto L24
L5:
astore 5
aload 5
invokevirtual java/lang/Exception/printStackTrace()V
L27:
aload 6
astore 5
iload 1
ifne L30
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "LuckyPatcher: get mounts from /proc/mounts"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifne L31
ldc "/proc/mounts"
ldc "/data/local/RootToolsMounts"
iconst_0
iconst_1
invokestatic com/chelpus/Utils/copyFile(Ljava/lang/String;Ljava/lang/String;ZZ)Z
pop
ldc "chmod 777 /data/local/RootToolsMounts"
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
L32:
aload 6
invokevirtual java/util/ArrayList/clear()V
L12:
new java/io/LineNumberReader
dup
new java/io/FileReader
dup
ldc "/data/local/RootToolsMounts"
invokespecial java/io/FileReader/<init>(Ljava/lang/String;)V
invokespecial java/io/LineNumberReader/<init>(Ljava/io/Reader;)V
astore 7
L13:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 5
L15:
aload 7
invokevirtual java/io/LineNumberReader/readLine()Ljava/lang/String;
astore 6
L17:
aload 6
ifnull L21
L19:
aload 6
ldc " "
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 6
aload 5
new com/android/vending/billing/InAppBillingService/LUCK/Mount
dup
new java/io/File
dup
aload 6
iconst_0
aaload
invokespecial java/io/File/<init>(Ljava/lang/String;)V
new java/io/File
dup
aload 6
iconst_1
aaload
invokespecial java/io/File/<init>(Ljava/lang/String;)V
aload 6
iconst_2
aaload
aload 6
iconst_3
aaload
invokespecial com/android/vending/billing/InAppBillingService/LUCK/Mount/<init>(Ljava/io/File;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L20:
goto L15
L18:
astore 7
aload 5
astore 6
aload 7
astore 5
L33:
aload 5
invokevirtual java/lang/Exception/printStackTrace()V
aload 6
astore 5
L30:
aload 5
areturn
L31:
ldc "/proc/mounts"
ldc "/data/local/RootToolsMounts"
iconst_0
iconst_1
invokestatic com/chelpus/Utils/copyFile(Ljava/lang/String;Ljava/lang/String;ZZ)Z
pop
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
ldc "/data/local/RootToolsMounts"
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
goto L32
L21:
aload 7
invokevirtual java/io/LineNumberReader/close()V
L22:
aload 5
areturn
L14:
astore 5
goto L33
L16:
astore 5
goto L33
.limit locals 8
.limit stack 8
.end method

.method public static getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
ifnull L0
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L0
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Start under Root"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L1:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 6
iconst_0
istore 3
ldc ""
astore 5
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L2
aload 5
astore 4
new java/io/File
dup
ldc "/data/dalvik-cache/arm"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L3
aload 5
astore 4
new java/io/File
dup
ldc "/data/dalvik-cache/arm"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/isDirectory()Z
ifeq L3
ldc "/arm"
astore 4
L3:
aload 4
astore 5
new java/io/File
dup
ldc "/data/dalvik-cache/arm64"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L4
aload 4
astore 5
new java/io/File
dup
ldc "/data/dalvik-cache/arm64"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/isDirectory()Z
ifeq L4
ldc "/arm64"
astore 5
L4:
aload 5
astore 4
new java/io/File
dup
ldc "/data/dalvik-cache/x86"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L5
aload 5
astore 4
new java/io/File
dup
ldc "/data/dalvik-cache/x86"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/isDirectory()Z
ifeq L5
ldc "/x86"
astore 4
L5:
aload 4
astore 5
iload 3
istore 2
aload 4
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L6
aload 4
astore 5
iload 3
istore 2
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/data/dalvik-cache"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L6
aload 4
astore 5
iload 3
istore 2
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/data/dalvik-cache"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/isDirectory()Z
ifeq L6
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " to dalvik cache found"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "check "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 6
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L7
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 6
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/isDirectory()Z
ifne L8
L7:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 6
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/mkdirs()Z
pop
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "try make dirs"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 6
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L8
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "dirs created"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 1
ldc "0"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L9
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "755"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 6
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "1000."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 6
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "1000:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 6
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L8:
iconst_1
istore 2
aload 4
astore 5
L6:
iload 2
ifeq L10
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 6
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc "odex"
invokestatic com/chelpus/Utils/changeExtension(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L0:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
ifnonnull L11
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "uderRoot not defined"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L11:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifne L1
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "uderRoot false"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
goto L1
L9:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "755"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 6
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "0."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 6
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chown"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "0:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 6
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
goto L8
L2:
aload 5
astore 4
ldc "/data/dalvik-cache/arm"
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifeq L12
ldc "/arm"
astore 4
L12:
ldc "/data/dalvik-cache/arm64"
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifeq L13
ldc "/arm64"
astore 4
L13:
ldc "/data/dalvik-cache/x86"
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifeq L14
ldc "/x86"
astore 4
L14:
aload 4
astore 5
iload 3
istore 2
aload 4
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L6
aload 4
astore 5
iload 3
istore 2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/data/dalvik-cache"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifeq L6
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 6
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifne L15
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "mkdir -p '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 6
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifeq L15
aload 1
ldc "0"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L16
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chmod 755 "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chown 1000."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chown 1000:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
L15:
iconst_1
istore 2
aload 4
astore 5
goto L6
L16:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chmod 755 "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chown 0."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chown 0:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
goto L15
L10:
aload 0
ldc "odex"
invokestatic com/chelpus/Utils/changeExtension(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
areturn
.limit locals 7
.limit stack 5
.end method

.method public static getPermissions(Ljava/lang/String;)Ljava/lang/String;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifne L0
iconst_5
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/busybox"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_1
ldc "stat"
aastore
dup
iconst_2
ldc "-c"
aastore
dup
iconst_3
ldc "%a"
aastore
dup
iconst_4
aload 0
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
ldc "\n"
ldc ""
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
ldc "\r"
ldc ""
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/String/trim()Ljava/lang/String;
astore 2
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 2
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 2
astore 1
aload 2
ldc "(\\d+)"
invokevirtual java/lang/String/matches(Ljava/lang/String;)Z
ifne L1
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "try get permission again"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iconst_5
anewarray java/lang/String
dup
iconst_0
ldc "busybox"
aastore
dup
iconst_1
ldc "stat"
aastore
dup
iconst_2
ldc "-c"
aastore
dup
iconst_3
ldc "%a"
aastore
dup
iconst_4
aload 0
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
ldc "\n"
ldc ""
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
ldc "\r"
ldc ""
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/String/trim()Ljava/lang/String;
astore 1
L1:
aload 1
ldc "(\\d+)"
invokevirtual java/lang/String/matches(Ljava/lang/String;)Z
ifne L2
ldc ""
areturn
L2:
aload 1
areturn
L0:
iconst_5
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/busybox"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_1
ldc "stat"
aastore
dup
iconst_2
ldc "-c"
aastore
dup
iconst_3
ldc "%a"
aastore
dup
iconst_4
aload 0
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
ldc "\n"
ldc ""
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
ldc "\r"
ldc ""
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/String/trim()Ljava/lang/String;
astore 2
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 2
astore 1
aload 2
ldc "(\\d+)"
invokevirtual java/lang/String/matches(Ljava/lang/String;)Z
ifne L3
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "try get permission again"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iconst_5
anewarray java/lang/String
dup
iconst_0
ldc "busybox"
aastore
dup
iconst_1
ldc "stat"
aastore
dup
iconst_2
ldc "-c"
aastore
dup
iconst_3
ldc "%a"
aastore
dup
iconst_4
aload 0
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
ldc "\n"
ldc ""
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
ldc "\r"
ldc ""
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/String/trim()Ljava/lang/String;
astore 1
L3:
aload 1
ldc "(\\d+)"
invokevirtual java/lang/String/matches(Ljava/lang/String;)Z
ifne L4
ldc ""
areturn
L4:
aload 1
areturn
.limit locals 3
.limit stack 5
.end method

.method public static getPkgInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
.catch java/lang/IllegalArgumentException from L0 to L1 using L3
L0:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
aload 0
iload 1
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
astore 0
L1:
aload 0
areturn
L2:
astore 0
aload 0
invokevirtual android/content/pm/PackageManager$NameNotFoundException/printStackTrace()V
aconst_null
areturn
L3:
astore 0
aload 0
invokevirtual java/lang/IllegalArgumentException/printStackTrace()V
aconst_null
areturn
.limit locals 2
.limit stack 3
.end method

.method public static getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 6
iconst_0
istore 3
ldc ""
astore 5
iload 1
ifeq L0
aload 5
astore 4
new java/io/File
dup
ldc "/data/dalvik-cache/arm"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L1
aload 5
astore 4
new java/io/File
dup
ldc "/data/dalvik-cache/arm"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/isDirectory()Z
ifeq L1
ldc "/arm"
astore 4
L1:
aload 4
astore 5
new java/io/File
dup
ldc "/data/dalvik-cache/arm64"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L2
aload 4
astore 5
new java/io/File
dup
ldc "/data/dalvik-cache/arm64"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/isDirectory()Z
ifeq L2
ldc "/arm64"
astore 5
L2:
aload 5
astore 4
new java/io/File
dup
ldc "/data/dalvik-cache/x86"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L3
aload 5
astore 4
new java/io/File
dup
ldc "/data/dalvik-cache/x86"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/isDirectory()Z
ifeq L3
ldc "/x86"
astore 4
L3:
aload 4
astore 5
iload 3
istore 2
aload 4
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L4
aload 4
astore 5
iload 3
istore 2
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/data/dalvik-cache"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L4
aload 4
astore 5
iload 3
istore 2
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/data/dalvik-cache"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/isDirectory()Z
ifeq L4
iconst_1
istore 2
aload 4
astore 5
L4:
iload 2
ifeq L5
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 6
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc "odex"
invokestatic com/chelpus/Utils/changeExtension(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L0:
aload 5
astore 4
ldc "/data/dalvik-cache/arm"
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifeq L6
ldc "/arm"
astore 4
L6:
ldc "/data/dalvik-cache/arm64"
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifeq L7
ldc "/arm64"
astore 4
L7:
ldc "/data/dalvik-cache/x86"
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifeq L8
ldc "/x86"
astore 4
L8:
aload 4
astore 5
iload 3
istore 2
aload 4
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L4
aload 4
astore 5
iload 3
istore 2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/data/dalvik-cache"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifeq L4
iconst_1
istore 2
aload 4
astore 5
goto L4
L5:
aload 0
ldc "odex"
invokestatic com/chelpus/Utils/changeExtension(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
areturn
.limit locals 7
.limit stack 4
.end method

.method public static getRandom(JJ)J
new java/util/Random
dup
invokespecial java/util/Random/<init>()V
astore 8
L0:
aload 8
invokevirtual java/util/Random/nextLong()J
iconst_1
lshl
iconst_1
lushr
lstore 4
lload 4
lload 2
lload 0
lsub
lconst_1
ladd
lrem
lstore 6
lload 4
lload 6
lsub
lload 2
lload 0
lsub
ladd
lconst_0
lcmp
iflt L0
lload 6
lload 0
ladd
lreturn
.limit locals 9
.limit stack 6
.end method

.method public static getRandomStringLowerCase(I)Ljava/lang/String;
new java/lang/StringBuilder
dup
iload 0
invokespecial java/lang/StringBuilder/<init>(I)V
astore 2
iconst_0
istore 1
L0:
iload 1
iload 0
if_icmpge L1
aload 2
ldc "abcdefghijklmnopqrstuvwxyz"
getstatic com/chelpus/Utils/rnd Ljava/util/Random;
ldc "abcdefghijklmnopqrstuvwxyz"
invokevirtual java/lang/String/length()I
invokevirtual java/util/Random/nextInt(I)I
invokevirtual java/lang/String/charAt(I)C
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 2
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 3
.limit stack 4
.end method

.method public static getRandomStringUpperLowerCase(I)Ljava/lang/String;
new java/lang/StringBuilder
dup
iload 0
invokespecial java/lang/StringBuilder/<init>(I)V
astore 2
iconst_0
istore 1
L0:
iload 1
iload 0
if_icmpge L1
aload 2
ldc "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
getstatic com/chelpus/Utils/rnd Ljava/util/Random;
ldc "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
invokevirtual java/lang/String/length()I
invokevirtual java/util/Random/nextInt(I)I
invokevirtual java/lang/String/charAt(I)C
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 2
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 3
.limit stack 4
.end method

.method public static getRawLength(I)J
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L3 to L4 using L2
.catch java/io/IOException from L4 to L5 using L2
.catch java/io/IOException from L6 to L7 using L2
L0:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getRes()Landroid/content/res/Resources;
iload 0
invokevirtual android/content/res/Resources/openRawResource(I)Ljava/io/InputStream;
astore 3
L1:
lconst_0
lstore 1
L3:
sipush 8192
newarray byte
astore 4
L4:
aload 3
aload 4
invokevirtual java/io/InputStream/read([B)I
istore 0
L5:
iload 0
iconst_m1
if_icmpeq L6
lload 1
iload 0
i2l
ladd
lstore 1
goto L4
L6:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher (RAW): length = "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 1
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L7:
lload 1
lreturn
L2:
astore 3
lconst_0
lreturn
.limit locals 5
.limit stack 4
.end method

.method public static getRawToFile(ILjava/io/File;)Z
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L3 to L4 using L2
.catch java/io/IOException from L5 to L6 using L2
aload 1
invokevirtual java/io/File/exists()Z
ifeq L7
aload 1
invokevirtual java/io/File/delete()Z
pop
L8:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "try get file from raw"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L0:
sipush 8192
newarray byte
astore 2
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getRes()Landroid/content/res/Resources;
iload 0
invokevirtual android/content/res/Resources/openRawResource(I)Ljava/io/InputStream;
astore 3
new java/io/FileOutputStream
dup
aload 1
invokespecial java/io/FileOutputStream/<init>(Ljava/io/File;)V
astore 1
aload 3
aload 2
invokevirtual java/io/InputStream/read([B)I
istore 0
L1:
iload 0
ifle L5
L3:
aload 1
aload 2
iconst_0
iload 0
invokevirtual java/io/FileOutputStream/write([BII)V
aload 3
aload 2
invokevirtual java/io/InputStream/read([B)I
istore 0
L4:
goto L1
L7:
aload 1
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/io/File/mkdirs()Z
pop
goto L8
L5:
aload 1
invokevirtual java/io/FileOutputStream/flush()V
aload 1
invokevirtual java/io/FileOutputStream/close()V
aload 3
invokevirtual java/io/InputStream/close()V
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "get file from raw"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L6:
iconst_1
ireturn
L2:
astore 1
iconst_0
ireturn
.limit locals 4
.limit stack 4
.end method

.method public static getRawToString(I)Ljava/lang/String;
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L3 to L4 using L2
.catch java/io/IOException from L5 to L6 using L2
ldc ""
astore 1
L0:
sipush 2048
newarray byte
astore 2
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getRes()Landroid/content/res/Resources;
iload 0
invokevirtual android/content/res/Resources/openRawResource(I)Ljava/io/InputStream;
astore 3
L1:
iconst_0
istore 0
L7:
iload 0
iconst_m1
if_icmpeq L5
L3:
aload 3
aload 2
invokevirtual java/io/InputStream/read([B)I
istore 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
new java/lang/String
dup
aload 2
ldc "UTF-8"
invokespecial java/lang/String/<init>([BLjava/lang/String;)V
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
L4:
goto L7
L5:
aload 3
invokevirtual java/io/InputStream/close()V
L6:
aload 1
areturn
L2:
astore 1
ldc ""
areturn
.limit locals 4
.limit stack 5
.end method

.method public static getRoot()V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
.catch java/lang/Exception from L6 to L7 using L2
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifne L1
L0:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suProcess Ljava/lang/Process;
ifnonnull L3
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "LuckyPatcher: GET ROOT."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
invokestatic java/lang/Runtime/getRuntime()Ljava/lang/Runtime;
ldc "su"
invokevirtual java/lang/Runtime/exec(Ljava/lang/String;)Ljava/lang/Process;
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suProcess Ljava/lang/Process;
new java/io/DataOutputStream
dup
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suProcess Ljava/lang/Process;
invokevirtual java/lang/Process/getOutputStream()Ljava/io/OutputStream;
invokespecial java/io/DataOutputStream/<init>(Ljava/io/OutputStream;)V
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suOutputStream Ljava/io/DataOutputStream;
new java/io/DataInputStream
dup
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suProcess Ljava/lang/Process;
invokevirtual java/lang/Process/getInputStream()Ljava/io/InputStream;
invokespecial java/io/DataInputStream/<init>(Ljava/io/InputStream;)V
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suInputStream Ljava/io/DataInputStream;
new java/io/DataInputStream
dup
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suProcess Ljava/lang/Process;
invokevirtual java/lang/Process/getErrorStream()Ljava/io/InputStream;
invokespecial java/io/DataInputStream/<init>(Ljava/io/InputStream;)V
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suErrorInputStream Ljava/io/DataInputStream;
L1:
return
L3:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suOutputStream Ljava/io/DataOutputStream;
ldc "echo chelpusstart!\n"
invokevirtual java/io/DataOutputStream/writeBytes(Ljava/lang/String;)V
L4:
return
L5:
astore 0
L6:
invokestatic com/chelpus/Utils/exitRoot()V
invokestatic java/lang/Runtime/getRuntime()Ljava/lang/Runtime;
ldc "su"
invokevirtual java/lang/Runtime/exec(Ljava/lang/String;)Ljava/lang/Process;
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suProcess Ljava/lang/Process;
new java/io/DataOutputStream
dup
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suProcess Ljava/lang/Process;
invokevirtual java/lang/Process/getOutputStream()Ljava/io/OutputStream;
invokespecial java/io/DataOutputStream/<init>(Ljava/io/OutputStream;)V
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suOutputStream Ljava/io/DataOutputStream;
new java/io/DataInputStream
dup
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suProcess Ljava/lang/Process;
invokevirtual java/lang/Process/getInputStream()Ljava/io/InputStream;
invokespecial java/io/DataInputStream/<init>(Ljava/io/InputStream;)V
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suInputStream Ljava/io/DataInputStream;
new java/io/DataInputStream
dup
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suProcess Ljava/lang/Process;
invokevirtual java/lang/Process/getErrorStream()Ljava/io/InputStream;
invokespecial java/io/DataInputStream/<init>(Ljava/io/InputStream;)V
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suErrorInputStream Ljava/io/DataInputStream;
L7:
return
L2:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
return
.limit locals 1
.limit stack 3
.end method

.method public static getRootUid()Z
.catch java/lang/Exception from L0 to L1 using L2
iconst_0
istore 0
L0:
ldc "user.name"
invokestatic java/lang/System/getProperty(Ljava/lang/String;)Ljava/lang/String;
ldc "root"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
istore 1
L1:
iload 1
ifeq L3
iconst_1
istore 0
L3:
iload 0
ireturn
L2:
astore 2
aload 2
invokevirtual java/lang/Exception/printStackTrace()V
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method private static getSignatureKeys()V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L2
L0:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "zipalign"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L1
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "zipalign"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
ldc_w 2131099663
invokestatic com/chelpus/Utils/getRawLength(I)J
lcmp
ifeq L5
L1:
ldc_w 2131099663
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/zipalign"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokestatic com/chelpus/Utils/getRawToFile(ILjava/io/File;)Z
pop
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
invokevirtual android/content/Context/getFilesDir()Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "zipalign"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
sipush 777
invokestatic com/chelpus/Utils/chmod(Ljava/io/File;I)I
pop
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chmod 777 "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
invokevirtual android/content/Context/getFilesDir()Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "zipalign"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
L3:
ldc "testkey.pk8"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/basepath Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/Keys"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/getAssets(Ljava/lang/String;Ljava/lang/String;)V
ldc "testkey.sbt"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/basepath Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/Keys"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/getAssets(Ljava/lang/String;Ljava/lang/String;)V
ldc "testkey.x509.pem"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/basepath Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Modified/Keys"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/getAssets(Ljava/lang/String;Ljava/lang/String;)V
L4:
return
L5:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "zipalign"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
sipush 777
invokestatic com/chelpus/Utils/chmod(Ljava/io/File;I)I
pop
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chmod 777 "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "zipalign"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
L6:
goto L3
L2:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
return
.limit locals 1
.limit stack 5
.end method

.method public static getSimulink(Ljava/lang/String;)Ljava/lang/String;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L0
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "ls"
aastore
dup
iconst_1
ldc "-l"
aastore
dup
iconst_2
aload 0
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
astore 1
ldc "^.*?\\-\\>\\s+(.*)$"
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
aload 1
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
astore 1
aload 1
invokevirtual java/util/regex/Matcher/find()Z
ifeq L1
aload 1
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
astore 0
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Symlink found: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
areturn
L1:
iconst_4
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/busybox"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_1
ldc "ls"
aastore
dup
iconst_2
ldc "-l"
aastore
dup
iconst_3
aload 0
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
astore 0
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
ldc "^.*?\\-\\>\\s+(.*)$"
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
aload 0
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
astore 0
aload 0
invokevirtual java/util/regex/Matcher/find()Z
ifeq L2
aload 0
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
astore 0
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Symlink found: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
areturn
L2:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "No symlink found!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
ldc ""
areturn
L0:
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "ls -l "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
astore 1
ldc "^.*?\\-\\>\\s+(.*)$"
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
aload 1
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
astore 1
aload 1
invokevirtual java/util/regex/Matcher/find()Z
ifeq L3
aload 1
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
astore 0
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Symlink found: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
areturn
L3:
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/busybox ls -l "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
astore 0
ldc "^.*?\\-\\>\\s+(.*)$"
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
aload 0
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
astore 0
aload 0
invokevirtual java/util/regex/Matcher/find()Z
ifeq L4
aload 0
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
astore 0
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Symlink found: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
areturn
L4:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "No symlink found!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
ldc ""
areturn
.limit locals 2
.limit stack 6
.end method

.method public static getStringIds(Ljava/lang/String;Ljava/util/ArrayList;Z)Ljava/util/ArrayList;
.signature "(Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/String;>;Z)Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;>;"
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
.catch java/lang/Exception from L6 to L7 using L8
.catch java/lang/Exception from L9 to L10 using L8
.catch java/lang/Exception from L11 to L12 using L8
.catch java/lang/Exception from L13 to L14 using L8
.catch java/lang/Exception from L15 to L16 using L8
.catch java/lang/Exception from L17 to L18 using L8
.catch java/lang/Exception from L19 to L20 using L8
.catch java/lang/Exception from L21 to L22 using L8
.catch java/lang/Exception from L23 to L24 using L8
.catch java/lang/Exception from L25 to L26 using L8
.catch java/lang/Exception from L27 to L28 using L8
.catch java/lang/Exception from L29 to L30 using L8
.catch java/lang/Exception from L31 to L32 using L5
.catch java/lang/Exception from L33 to L34 using L2
.catch java/lang/Exception from L35 to L36 using L2
.catch java/lang/Exception from L37 to L38 using L39
.catch java/lang/Exception from L40 to L41 using L39
.catch java/lang/Exception from L42 to L43 using L39
.catch java/lang/Exception from L44 to L45 using L39
.catch java/lang/Exception from L46 to L47 using L39
.catch java/lang/Exception from L48 to L49 using L39
.catch java/lang/Exception from L50 to L51 using L39
.catch java/lang/Exception from L52 to L53 using L39
.catch java/lang/Exception from L54 to L55 using L39
.catch java/lang/Exception from L56 to L57 using L39
.catch java/lang/Exception from L58 to L59 using L39
.catch java/lang/Exception from L60 to L61 using L39
.catch java/lang/Exception from L62 to L63 using L2
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "scan: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
aload 0
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 15
iload 2
ifne L35
aload 0
ifnull L30
L0:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L30
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
lstore 13
L1:
lload 13
lconst_0
lcmp
ifeq L30
L3:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
istore 2
L4:
iload 2
ifeq L30
L6:
new java/io/RandomAccessFile
dup
aload 0
ldc "r"
invokespecial java/io/RandomAccessFile/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual java/io/RandomAccessFile/getChannel()Ljava/nio/channels/FileChannel;
astore 0
aload 0
getstatic java/nio/channels/FileChannel$MapMode/READ_ONLY Ljava/nio/channels/FileChannel$MapMode;
lconst_0
aload 0
invokevirtual java/nio/channels/FileChannel/size()J
l2i
i2l
invokevirtual java/nio/channels/FileChannel/map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
astore 16
aload 16
bipush 56
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
invokestatic com/chelpus/Utils/convertFourBytesToInt(BBBB)I
istore 8
aload 16
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
invokestatic com/chelpus/Utils/convertFourBytesToInt(BBBB)I
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
iload 8
newarray int
astore 17
L7:
iconst_0
istore 7
L64:
iload 7
iload 8
if_icmpge L65
L9:
aload 17
iload 7
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
invokestatic com/chelpus/Utils/convertFourBytesToInt(BBBB)I
iastore
L10:
iload 7
iconst_1
iadd
istore 7
goto L64
L65:
iconst_0
istore 8
L11:
aload 1
invokevirtual java/util/ArrayList/size()I
newarray int
astore 18
aload 1
invokevirtual java/util/ArrayList/size()I
anewarray java/lang/String
astore 19
L12:
iconst_0
istore 7
L13:
iload 7
aload 18
arraylength
if_icmpge L15
aload 18
iload 7
aload 1
iload 7
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/String
invokevirtual java/lang/String/length()I
iastore
aload 19
iload 7
aload 1
iload 7
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/String
aastore
L14:
iload 7
iconst_1
iadd
istore 7
goto L13
L15:
aload 17
arraylength
istore 11
L16:
iconst_0
istore 7
goto L66
L17:
iload 9
aload 18
arraylength
if_icmpge L67
aload 16
iload 12
invokevirtual java/nio/MappedByteBuffer/get(I)B
invokestatic com/chelpus/Utils/convertByteToInt(B)I
istore 10
L18:
iload 10
aload 18
iload 9
iaload
if_icmpne L68
L19:
iload 10
newarray byte
astore 1
aload 16
iload 12
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L20:
iconst_0
istore 10
L21:
iload 10
aload 1
arraylength
if_icmpge L23
aload 1
iload 10
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
bastore
L22:
iload 10
iconst_1
iadd
istore 10
goto L21
L23:
aload 19
iload 9
aaload
new java/lang/String
dup
aload 1
invokespecial java/lang/String/<init>([B)V
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L68
L24:
iload 8
i2b
istore 3
iload 8
bipush 8
ishr
i2b
istore 4
iload 8
bipush 16
ishr
i2b
istore 5
iload 8
bipush 24
ishr
i2b
istore 6
iconst_0
istore 2
L25:
aload 17
arraylength
ldc_w 65535
if_icmple L27
L26:
iconst_1
istore 2
L27:
aload 15
new com/android/vending/billing/InAppBillingService/LUCK/StringItem
dup
aload 19
iload 9
aaload
iconst_4
newarray byte
dup
iconst_0
iload 3
bastore
dup
iconst_1
iload 4
bastore
dup
iconst_2
iload 5
bastore
dup
iconst_3
iload 6
bastore
iload 2
invokespecial com/android/vending/billing/InAppBillingService/LUCK/StringItem/<init>(Ljava/lang/String;[BZ)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L28:
goto L68
L29:
aload 0
invokevirtual java/nio/channels/FileChannel/close()V
L30:
aload 15
areturn
L8:
astore 0
L31:
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L32:
aload 15
areturn
L5:
astore 0
L33:
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L34:
aload 15
areturn
L2:
astore 0
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
invokevirtual java/io/PrintStream/println(Ljava/lang/Object;)V
aload 15
areturn
L35:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
istore 2
L36:
iload 2
ifeq L30
L37:
new java/io/RandomAccessFile
dup
aload 0
ldc "r"
invokespecial java/io/RandomAccessFile/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual java/io/RandomAccessFile/getChannel()Ljava/nio/channels/FileChannel;
astore 0
aload 0
getstatic java/nio/channels/FileChannel$MapMode/READ_ONLY Ljava/nio/channels/FileChannel$MapMode;
lconst_0
aload 0
invokevirtual java/nio/channels/FileChannel/size()J
l2i
i2l
invokevirtual java/nio/channels/FileChannel/map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
astore 16
aload 16
bipush 8
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
invokestatic com/chelpus/Utils/convertFourBytesToInt(BBBB)I
istore 8
aload 16
iload 8
bipush 56
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
invokestatic com/chelpus/Utils/convertFourBytesToInt(BBBB)I
istore 9
aload 16
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
invokestatic com/chelpus/Utils/convertFourBytesToInt(BBBB)I
iload 8
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
iload 9
newarray int
astore 17
L38:
iconst_0
istore 7
L69:
iload 7
iload 9
if_icmpge L70
L40:
aload 17
iload 7
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
invokestatic com/chelpus/Utils/convertFourBytesToInt(BBBB)I
iload 8
iadd
iastore
L41:
iload 7
iconst_1
iadd
istore 7
goto L69
L70:
iconst_0
istore 8
L42:
aload 1
invokevirtual java/util/ArrayList/size()I
newarray int
astore 18
aload 1
invokevirtual java/util/ArrayList/size()I
anewarray java/lang/String
astore 19
L43:
iconst_0
istore 7
L44:
iload 7
aload 18
arraylength
if_icmpge L46
aload 18
iload 7
aload 1
iload 7
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/String
invokevirtual java/lang/String/length()I
iastore
aload 19
iload 7
aload 1
iload 7
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/String
aastore
L45:
iload 7
iconst_1
iadd
istore 7
goto L44
L46:
aload 17
arraylength
istore 11
L47:
iconst_0
istore 7
goto L71
L48:
iload 9
aload 18
arraylength
if_icmpge L72
aload 16
iload 12
invokevirtual java/nio/MappedByteBuffer/get(I)B
invokestatic com/chelpus/Utils/convertByteToInt(B)I
istore 10
L49:
iload 10
aload 18
iload 9
iaload
if_icmpne L73
L50:
iload 10
newarray byte
astore 1
aload 16
iload 12
iconst_1
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L51:
iconst_0
istore 10
L52:
iload 10
aload 1
arraylength
if_icmpge L54
aload 1
iload 10
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
bastore
L53:
iload 10
iconst_1
iadd
istore 10
goto L52
L54:
aload 19
iload 9
aaload
new java/lang/String
dup
aload 1
invokespecial java/lang/String/<init>([B)V
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L73
L55:
iload 8
i2b
istore 3
iload 8
bipush 8
ishr
i2b
istore 4
iload 8
bipush 16
ishr
i2b
istore 5
iload 8
bipush 24
ishr
i2b
istore 6
iconst_0
istore 2
L56:
aload 17
arraylength
ldc_w 65535
if_icmple L58
L57:
iconst_1
istore 2
L58:
aload 15
new com/android/vending/billing/InAppBillingService/LUCK/StringItem
dup
aload 19
iload 9
aaload
iconst_4
newarray byte
dup
iconst_0
iload 3
bastore
dup
iconst_1
iload 4
bastore
dup
iconst_2
iload 5
bastore
dup
iconst_3
iload 6
bastore
iload 2
invokespecial com/android/vending/billing/InAppBillingService/LUCK/StringItem/<init>(Ljava/lang/String;[BZ)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L59:
goto L73
L60:
aload 0
invokevirtual java/nio/channels/FileChannel/close()V
L61:
aload 15
areturn
L39:
astore 0
L62:
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L63:
aload 15
areturn
L66:
iload 7
iload 11
if_icmpge L29
aload 17
iload 7
iaload
istore 12
iconst_0
istore 9
goto L17
L68:
iload 9
iconst_1
iadd
istore 9
goto L17
L67:
iload 8
iconst_1
iadd
istore 8
iload 7
iconst_1
iadd
istore 7
goto L66
L71:
iload 7
iload 11
if_icmpge L60
aload 17
iload 7
iaload
istore 12
iconst_0
istore 9
goto L48
L73:
iload 9
iconst_1
iadd
istore 9
goto L48
L72:
iload 8
iconst_1
iadd
istore 8
iload 7
iconst_1
iadd
istore 7
goto L71
.limit locals 20
.limit stack 8
.end method

.method public static getText(I)Ljava/lang/String;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/resources Landroid/content/res/Resources;
ifnonnull L0
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getRes()Landroid/content/res/Resources;
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/resources Landroid/content/res/Resources;
L0:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/resources Landroid/content/res/Resources;
iload 0
invokevirtual android/content/res/Resources/getString(I)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public static getTypesIds(Ljava/lang/String;Ljava/util/ArrayList;Z)Z
.signature "(Ljava/lang/String;Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;>;Z)Z"
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L7
.catch java/lang/Exception from L8 to L9 using L10
.catch java/lang/Exception from L11 to L12 using L10
.catch java/lang/Exception from L13 to L14 using L10
.catch java/lang/Exception from L15 to L16 using L10
.catch java/lang/Exception from L17 to L18 using L10
.catch java/lang/Exception from L19 to L20 using L10
.catch java/lang/Exception from L21 to L22 using L10
.catch java/lang/Exception from L23 to L24 using L10
.catch java/lang/Exception from L25 to L26 using L10
.catch java/lang/Exception from L27 to L28 using L10
.catch java/lang/Exception from L29 to L30 using L10
.catch java/lang/Exception from L31 to L32 using L10
.catch java/lang/Exception from L33 to L34 using L10
.catch java/lang/Exception from L35 to L36 using L10
.catch java/lang/Exception from L37 to L38 using L10
.catch java/lang/Exception from L39 to L40 using L10
.catch java/lang/Exception from L41 to L42 using L10
.catch java/lang/Exception from L43 to L44 using L10
.catch java/lang/Exception from L45 to L46 using L10
.catch java/lang/Exception from L47 to L48 using L10
.catch java/lang/Exception from L49 to L50 using L10
.catch java/lang/Exception from L51 to L52 using L10
.catch java/lang/Exception from L53 to L54 using L7
.catch java/lang/Exception from L55 to L56 using L10
.catch java/lang/Exception from L57 to L58 using L10
.catch java/lang/Exception from L59 to L60 using L10
.catch java/lang/Exception from L61 to L62 using L10
.catch java/lang/Exception from L63 to L64 using L2
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "scan: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iconst_0
istore 9
iconst_0
istore 8
iconst_0
istore 10
iconst_0
istore 11
iconst_0
istore 7
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
aload 0
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
pop
iload 9
istore 6
iload 2
ifne L65
iload 9
istore 6
aload 0
ifnull L65
iload 9
istore 6
iload 11
istore 2
L0:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L65
L1:
iload 11
istore 2
L3:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
lstore 12
L4:
iload 9
istore 6
lload 12
lconst_0
lcmp
ifeq L65
iload 10
istore 6
L5:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
istore 2
L6:
iload 9
istore 6
iload 2
ifeq L65
iload 7
istore 2
L8:
new java/io/RandomAccessFile
dup
aload 0
ldc "r"
invokespecial java/io/RandomAccessFile/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual java/io/RandomAccessFile/getChannel()Ljava/nio/channels/FileChannel;
astore 0
L9:
iload 7
istore 2
L11:
aload 0
getstatic java/nio/channels/FileChannel$MapMode/READ_ONLY Ljava/nio/channels/FileChannel$MapMode;
lconst_0
aload 0
invokevirtual java/nio/channels/FileChannel/size()J
l2i
i2l
invokevirtual java/nio/channels/FileChannel/map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
astore 14
L12:
iload 7
istore 2
L13:
aload 14
bipush 64
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L14:
iload 7
istore 2
L15:
aload 14
invokevirtual java/nio/MappedByteBuffer/get()B
aload 14
invokevirtual java/nio/MappedByteBuffer/get()B
aload 14
invokevirtual java/nio/MappedByteBuffer/get()B
aload 14
invokevirtual java/nio/MappedByteBuffer/get()B
invokestatic com/chelpus/Utils/convertFourBytesToInt(BBBB)I
istore 5
L16:
iload 7
istore 2
L17:
aload 14
invokevirtual java/nio/MappedByteBuffer/get()B
aload 14
invokevirtual java/nio/MappedByteBuffer/get()B
aload 14
invokevirtual java/nio/MappedByteBuffer/get()B
aload 14
invokevirtual java/nio/MappedByteBuffer/get()B
invokestatic com/chelpus/Utils/convertFourBytesToInt(BBBB)I
istore 3
L18:
iload 7
istore 2
L19:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher offset_to_data="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 3
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L20:
iload 7
istore 2
L21:
aload 14
iload 3
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L22:
iconst_0
istore 4
iconst_0
istore 3
L66:
iload 3
iload 5
if_icmpge L67
iload 7
istore 2
L23:
iconst_4
newarray byte
astore 15
L24:
iload 7
istore 2
L25:
aload 15
iconst_0
aload 14
invokevirtual java/nio/MappedByteBuffer/get()B
bastore
L26:
iload 7
istore 2
L27:
aload 15
iconst_1
aload 14
invokevirtual java/nio/MappedByteBuffer/get()B
bastore
L28:
iload 7
istore 2
L29:
aload 15
iconst_2
aload 14
invokevirtual java/nio/MappedByteBuffer/get()B
bastore
L30:
iload 7
istore 2
L31:
aload 15
iconst_3
aload 14
invokevirtual java/nio/MappedByteBuffer/get()B
bastore
L32:
iload 7
istore 2
L33:
aload 1
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 16
L34:
iload 7
istore 2
L35:
aload 16
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L68
L36:
iload 7
istore 2
L37:
aload 16
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/TypesItem
astore 17
L38:
iload 7
istore 2
L39:
aload 15
iconst_0
baload
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/TypesItem/Type [B
iconst_0
baload
if_icmpne L34
L40:
iload 7
istore 2
L41:
aload 15
iconst_1
baload
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/TypesItem/Type [B
iconst_1
baload
if_icmpne L34
L42:
iload 7
istore 2
L43:
aload 15
iconst_2
baload
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/TypesItem/Type [B
iconst_2
baload
if_icmpne L34
L44:
iload 7
istore 2
L45:
aload 15
iconst_3
baload
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/TypesItem/Type [B
iconst_3
baload
if_icmpne L34
L46:
iload 7
istore 2
L47:
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/TypesItem/id_type [B
iconst_0
iload 4
i2b
bastore
L48:
iload 7
istore 2
L49:
aload 17
getfield com/android/vending/billing/InAppBillingService/LUCK/TypesItem/id_type [B
iconst_1
iload 4
bipush 8
ishr
i2b
bastore
L50:
iload 7
istore 2
L51:
aload 17
iconst_1
putfield com/android/vending/billing/InAppBillingService/LUCK/TypesItem/found_id_type Z
L52:
goto L34
L10:
astore 0
iload 2
istore 6
L53:
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L54:
iload 2
istore 6
L65:
iload 6
ireturn
L68:
iload 4
iconst_1
iadd
istore 4
iload 3
iconst_1
iadd
istore 3
goto L66
L67:
iload 7
istore 2
L55:
aload 1
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 1
L56:
iload 8
istore 6
L69:
iload 6
istore 2
L57:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L70
L58:
iload 6
istore 2
L59:
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/android/vending/billing/InAppBillingService/LUCK/TypesItem
getfield com/android/vending/billing/InAppBillingService/LUCK/TypesItem/found_id_type Z
ifeq L69
L60:
iconst_1
istore 6
goto L69
L70:
iload 6
istore 2
L61:
aload 0
invokevirtual java/nio/channels/FileChannel/close()V
L62:
iload 6
ireturn
L7:
astore 0
iload 6
istore 2
L63:
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L64:
iload 6
ireturn
L2:
astore 0
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
invokevirtual java/io/PrintStream/println(Ljava/lang/Object;)V
iload 2
ireturn
.limit locals 18
.limit stack 6
.end method

.method public static getXmlAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.catch javax/xml/parsers/ParserConfigurationException from L0 to L1 using L2
.catch org/xml/sax/SAXException from L0 to L1 using L3
.catch java/io/IOException from L0 to L1 using L4
invokestatic javax/xml/parsers/DocumentBuilderFactory/newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;
astore 3
L0:
aload 3
invokevirtual javax/xml/parsers/DocumentBuilderFactory/newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;
new org/xml/sax/InputSource
dup
new java/io/StringReader
dup
aload 0
invokespecial java/io/StringReader/<init>(Ljava/lang/String;)V
invokespecial org/xml/sax/InputSource/<init>(Ljava/io/Reader;)V
invokevirtual javax/xml/parsers/DocumentBuilder/parse(Lorg/xml/sax/InputSource;)Lorg/w3c/dom/Document;
aload 1
invokeinterface org/w3c/dom/Document/getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList; 1
astore 0
aload 0
invokeinterface org/w3c/dom/NodeList/getLength()I 0
ifge L5
aload 0
iconst_0
invokeinterface org/w3c/dom/NodeList/item(I)Lorg/w3c/dom/Node; 1
checkcast org/w3c/dom/Element
aload 2
invokeinterface org/w3c/dom/Element/getAttribute(Ljava/lang/String;)Ljava/lang/String; 1
astore 0
L1:
aload 0
areturn
L2:
astore 0
aload 0
invokevirtual javax/xml/parsers/ParserConfigurationException/printStackTrace()V
L5:
ldc ""
areturn
L3:
astore 0
aload 0
invokevirtual org/xml/sax/SAXException/printStackTrace()V
goto L5
L4:
astore 0
aload 0
invokevirtual java/io/IOException/printStackTrace()V
goto L5
.limit locals 4
.limit stack 6
.end method

.method public static getfirstInstallTime(Ljava/lang/String;Z)J
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
.catch android/content/pm/PackageManager$NameNotFoundException from L3 to L4 using L5
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/api I
bipush 8
if_icmple L3
iload 1
ifne L3
L0:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
aload 0
iconst_0
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
getfield android/content/pm/PackageInfo/lastUpdateTime J
lstore 2
L1:
lload 2
lreturn
L2:
astore 0
aload 0
invokevirtual android/content/pm/PackageManager$NameNotFoundException/printStackTrace()V
lconst_0
lreturn
L3:
new java/io/File
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
aload 0
iconst_0
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
getfield android/content/pm/PackageInfo/applicationInfo Landroid/content/pm/ApplicationInfo;
getfield android/content/pm/ApplicationInfo/sourceDir Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/lastModified()J
lstore 2
L4:
lload 2
lreturn
L5:
astore 0
aload 0
invokevirtual android/content/pm/PackageManager$NameNotFoundException/printStackTrace()V
lconst_0
lreturn
.limit locals 4
.limit stack 5
.end method

.method public static hasXposed()Z
ldc "CLASSPATH"
invokestatic java/lang/System/getenv(Ljava/lang/String;)Ljava/lang/String;
ifnull L0
ldc "CLASSPATH"
invokestatic java/lang/System/getenv(Ljava/lang/String;)Ljava/lang/String;
ldc "Xposed"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 0
.limit stack 2
.end method

.method public static initXposedParam()Z
.catch org/json/JSONException from L0 to L1 using L2
.catch org/json/JSONException from L3 to L4 using L5
.catch org/json/JSONException from L4 to L6 using L7
.catch org/json/JSONException from L6 to L8 using L9
.catch org/json/JSONException from L10 to L11 using L12
.catch org/json/JSONException from L13 to L14 using L15
new java/io/File
dup
ldc "/data/lp/xposed"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifne L16
new java/io/File
dup
ldc "/data/lp"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifne L17
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L18
new java/io/File
dup
ldc "/data/lp"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/mkdirs()Z
pop
new java/io/File
dup
ldc "/data/lp"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
iconst_1
iconst_0
invokevirtual java/io/File/setWritable(ZZ)Z
pop
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
ldc "/data/lp"
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L17:
new org/json/JSONObject
dup
invokespecial org/json/JSONObject/<init>()V
astore 0
L0:
aload 0
ldc "patch1"
iconst_1
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Z)Lorg/json/JSONObject;
pop
aload 0
ldc "patch2"
iconst_1
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Z)Lorg/json/JSONObject;
pop
aload 0
ldc "patch3"
iconst_1
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Z)Lorg/json/JSONObject;
pop
aload 0
ldc "patch4"
iconst_1
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Z)Lorg/json/JSONObject;
pop
aload 0
ldc "hide"
iconst_0
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Z)Lorg/json/JSONObject;
pop
aload 0
ldc "module_on"
iconst_1
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Z)Lorg/json/JSONObject;
pop
L1:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L19
new java/io/File
dup
ldc "/data/lp/xposed"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
aload 0
invokevirtual org/json/JSONObject/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/save_text_to_file(Ljava/io/File;Ljava/lang/String;)Z
pop
new java/io/File
dup
ldc "/data/lp/xposed"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
iconst_1
iconst_0
invokevirtual java/io/File/setWritable(ZZ)Z
pop
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
ldc "/data/lp/xposed"
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L3:
new org/json/JSONObject
dup
new java/io/File
dup
ldc "/data/lp/xposed"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokestatic com/chelpus/Utils/read_from_file(Ljava/io/File;)Ljava/lang/String;
invokespecial org/json/JSONObject/<init>(Ljava/lang/String;)V
pop
L4:
new org/json/JSONObject
dup
new java/io/File
dup
ldc "/data/lp/xposed"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokestatic com/chelpus/Utils/read_from_file(Ljava/io/File;)Ljava/lang/String;
invokespecial org/json/JSONObject/<init>(Ljava/lang/String;)V
astore 0
L6:
aload 0
ldc "module_on"
invokevirtual org/json/JSONObject/getBoolean(Ljava/lang/String;)Z
pop
L8:
iconst_1
ireturn
L18:
ldc "mkdir /data/lp"
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
ldc "chmod 777 /data/lp"
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
goto L17
L2:
astore 0
aload 0
invokevirtual org/json/JSONException/printStackTrace()V
iconst_0
ireturn
L19:
new java/io/File
dup
ldc "/data/lp/xposed"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
aload 0
invokevirtual org/json/JSONObject/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/save_text_to_file(Ljava/io/File;Ljava/lang/String;)Z
pop
new java/io/File
dup
ldc "/data/lp/xposed"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
iconst_1
iconst_0
invokevirtual java/io/File/setWritable(ZZ)Z
pop
ldc "chmod 777 /data/lp/xposed"
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
goto L3
L16:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L20
new java/io/File
dup
ldc "/data/lp/xposed"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
iconst_1
iconst_0
invokevirtual java/io/File/setWritable(ZZ)Z
pop
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
ldc "/data/lp/xposed"
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
goto L3
L20:
new java/io/File
dup
ldc "/data/lp/xposed"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
iconst_1
iconst_0
invokevirtual java/io/File/setWritable(ZZ)Z
pop
ldc "chmod 777 /data/lp/xposed"
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
goto L3
L5:
astore 0
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L21
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
ldc "/data/lp/xposed"
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
goto L4
L21:
ldc "chmod 777 /data/lp/xposed"
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
goto L4
L7:
astore 0
aload 0
invokevirtual org/json/JSONException/printStackTrace()V
new org/json/JSONObject
dup
invokespecial org/json/JSONObject/<init>()V
astore 0
L10:
aload 0
ldc "patch1"
iconst_1
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Z)Lorg/json/JSONObject;
pop
aload 0
ldc "patch2"
iconst_1
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Z)Lorg/json/JSONObject;
pop
aload 0
ldc "patch3"
iconst_1
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Z)Lorg/json/JSONObject;
pop
aload 0
ldc "patch4"
iconst_1
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Z)Lorg/json/JSONObject;
pop
aload 0
ldc "hide"
iconst_0
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Z)Lorg/json/JSONObject;
pop
aload 0
ldc "module_on"
iconst_1
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Z)Lorg/json/JSONObject;
pop
L11:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L22
new java/io/File
dup
ldc "/data/lp/xposed"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
aload 0
invokevirtual org/json/JSONObject/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/save_text_to_file(Ljava/io/File;Ljava/lang/String;)Z
pop
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
ldc "/data/lp/xposed"
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L13:
new org/json/JSONObject
dup
new java/io/File
dup
ldc "/data/lp/xposed"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokestatic com/chelpus/Utils/read_from_file(Ljava/io/File;)Ljava/lang/String;
invokespecial org/json/JSONObject/<init>(Ljava/lang/String;)V
astore 0
L14:
goto L6
L12:
astore 0
aload 0
invokevirtual org/json/JSONException/printStackTrace()V
iconst_0
ireturn
L22:
new java/io/File
dup
ldc "/data/lp/xposed"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
aload 0
invokevirtual org/json/JSONObject/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/save_text_to_file(Ljava/io/File;Ljava/lang/String;)Z
pop
ldc "chmod 777 /data/lp/xposed"
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
goto L13
L15:
astore 0
aload 0
invokevirtual org/json/JSONException/printStackTrace()V
iconst_0
ireturn
L9:
astore 0
aload 0
invokevirtual org/json/JSONException/printStackTrace()V
iconst_0
ireturn
.limit locals 1
.limit stack 5
.end method

.method public static final isAds(Ljava/lang/String;)Z
aload 0
ldc ".ads."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc "adwhirl"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc "amobee"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc "burstly"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc "com.adknowledge."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc "cauly.android.ad."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc ".greystripe."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc "inmobi."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc "inneractive.api.ads."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc ".jumptap.adtag."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc ".mdotm.android.ads."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc "medialets.advertising."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc ".millennialmedia.android."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc ".mobclix.android.sdk."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc ".mobfox.sdk."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc ".adserver.adview."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc ".mopub.mobileads."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc "com.oneriot."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc ".papaya.offer."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc "pontiflex.mobile.webview.sdk.activities"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc ".qwapi.adclient.android.view."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc ".smaato.SOMA."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc ".vdopia.client.android."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc ".zestadz.android."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc "com.appenda."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc "com.airpush.android."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc "com.Leadbolt."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc "com.moolah."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc "com.tapit.adview.notif."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc "com.urbanairship.push."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc "com.xtify.android.sdk."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc "MediaPlayerWrapper"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc ".vungle."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc ".tapjoy."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc ".nbpcorp."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc "com.appenda."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc ".plus1.sdk."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc ".adsdk."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc ".mdotm."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc "AdView"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
ldc "mad.ad."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public static isCustomPatchesForPkg(Ljava/lang/String;)Z
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
.catch java/lang/Exception from L3 to L4 using L5
.catch java/lang/Exception from L6 to L7 using L5
.catch java/lang/Exception from L8 to L9 using L5
.catch java/lang/Exception from L9 to L10 using L2
.catch java/lang/Exception from L11 to L12 using L2
.catch java/lang/Exception from L13 to L14 using L2
.catch java/lang/Exception from L15 to L16 using L2
.catch java/lang/Exception from L17 to L18 using L2
L0:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
ifnull L1
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
arraylength
ifne L9
L1:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/init()V
L3:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getConfig()Landroid/content/SharedPreferences;
ldc "basepath"
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/basepath Ljava/lang/String;
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/basepath Ljava/lang/String;
new java/io/File
dup
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/basepath Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/listFiles()[Ljava/io/File;
arraylength
anewarray java/io/File
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
new java/io/File
dup
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/basepath Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/listFiles()[Ljava/io/File;
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 3
aload 3
invokevirtual java/util/ArrayList/clear()V
L4:
iconst_0
istore 1
L6:
iload 1
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
arraylength
if_icmpge L8
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
iload 1
aaload
invokevirtual java/io/File/isFile()Z
ifeq L19
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
iload 1
aaload
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc ".txt"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L19
aload 3
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
iload 1
aaload
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L7:
goto L19
L8:
aload 3
invokevirtual java/util/ArrayList/size()I
ifle L9
aload 3
invokevirtual java/util/ArrayList/size()I
anewarray java/io/File
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
aload 3
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
invokevirtual java/util/ArrayList/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
checkcast [Ljava/io/File;
checkcast [Ljava/io/File;
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
L9:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
ifnull L20
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
arraylength
ifle L20
L10:
iconst_0
istore 1
L11:
iload 1
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
arraylength
if_icmpge L21
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
iload 1
aaload
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc "_%ALL%.txt"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L17
aload 0
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
iload 1
aaload
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc "_%ALL%.txt"
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L15
L12:
iconst_1
ireturn
L5:
astore 3
L13:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Not found dir by Lucky Patcher. Custom patch not found."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L14:
goto L9
L2:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L21:
iconst_0
ireturn
L15:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
iload 1
aaload
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc "_%ALL%.txt"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L17
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
iload 1
aaload
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc "%ALL%_"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L17
aload 0
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
iload 1
aaload
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc "%ALL%_"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
checkcast [Ljava/lang/String;
iconst_1
aaload
ldc "_%ALL%.txt"
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L17
L16:
iconst_1
ireturn
L17:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/customlist [Ljava/io/File;
iload 1
aaload
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc ".txt"
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
aload 0
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
istore 2
L18:
iload 2
ifeq L22
iconst_1
ireturn
L22:
iload 1
iconst_1
iadd
istore 1
goto L11
L20:
iconst_0
ireturn
L19:
iload 1
iconst_1
iadd
istore 1
goto L6
.limit locals 4
.limit stack 4
.end method

.method public static isELFfiles(Ljava/io/File;)Z
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch java/io/IOException from L0 to L1 using L3
.catch java/io/FileNotFoundException from L4 to L5 using L2
.catch java/io/IOException from L4 to L5 using L3
L0:
new java/io/RandomAccessFile
dup
aload 0
ldc "r"
invokespecial java/io/RandomAccessFile/<init>(Ljava/io/File;Ljava/lang/String;)V
invokevirtual java/io/RandomAccessFile/getChannel()Ljava/nio/channels/FileChannel;
astore 0
aload 0
getstatic java/nio/channels/FileChannel$MapMode/READ_ONLY Ljava/nio/channels/FileChannel$MapMode;
lconst_0
aload 0
invokevirtual java/nio/channels/FileChannel/size()J
l2i
i2l
invokevirtual java/nio/channels/FileChannel/map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
astore 1
aload 1
iconst_0
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 1
invokevirtual java/nio/MappedByteBuffer/get()B
bipush 127
if_icmpne L4
aload 1
invokevirtual java/nio/MappedByteBuffer/get()B
bipush 69
if_icmpne L4
aload 1
invokevirtual java/nio/MappedByteBuffer/get()B
bipush 76
if_icmpne L4
aload 1
invokevirtual java/nio/MappedByteBuffer/get()B
bipush 70
if_icmpne L4
aload 0
invokevirtual java/nio/channels/FileChannel/close()V
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Check file: is ELF."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L1:
iconst_1
ireturn
L4:
aload 0
invokevirtual java/nio/channels/FileChannel/close()V
L5:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Check file: is not ELF."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iconst_0
ireturn
L2:
astore 0
aload 0
invokevirtual java/io/FileNotFoundException/printStackTrace()V
goto L5
L3:
astore 0
aload 0
invokevirtual java/io/IOException/printStackTrace()V
goto L5
.limit locals 2
.limit stack 6
.end method

.method public static isInstalledOnSdCard(Ljava/lang/String;)Z
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
.catch java/lang/Throwable from L3 to L4 using L5
.catch java/lang/Throwable from L6 to L7 using L5
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/api I
bipush 7
if_icmple L3
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
astore 3
L0:
aload 3
aload 0
iconst_0
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
getfield android/content/pm/PackageInfo/applicationInfo Landroid/content/pm/ApplicationInfo;
getfield android/content/pm/ApplicationInfo/flags I
istore 1
L1:
iload 1
ldc_w 262144
iand
ldc_w 262144
if_icmpne L8
L9:
iconst_1
ireturn
L8:
iconst_0
ireturn
L2:
astore 3
L3:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
aload 0
iconst_0
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
getfield android/content/pm/PackageInfo/applicationInfo Landroid/content/pm/ApplicationInfo;
getfield android/content/pm/ApplicationInfo/sourceDir Ljava/lang/String;
astore 0
aload 0
ldc "/data/"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L6
L4:
iconst_0
ireturn
L6:
aload 0
ldc "/mnt/"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L9
aload 0
ldc "/sdcard/"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
istore 2
L7:
iload 2
ifne L9
L10:
iconst_0
ireturn
L5:
astore 0
goto L10
.limit locals 4
.limit stack 3
.end method

.method public static isMarketIntent(Ljava/lang/String;)Z
aload 0
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "com.android.vending.billing.inappbillingservice.bind"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L0
aload 0
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "ir.cafebazaar.pardakht.inappbillingservice.bind"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L0
aload 0
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "com.nokia.payment.iapenabler.inappbillingservice.bind"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L0
aload 0
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "net.jhoobin.jhub.inappbillingservice.bind"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L0
aload 0
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "net.jhoobin.jhub.billing.iinappbillingservice"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L0
aload 0
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment
invokevirtual java/lang/Class/getPackage()Ljava/lang/Package;
invokevirtual java/lang/Package/getName()Ljava/lang/String;
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public static isModified(Ljava/lang/String;Landroid/content/Context;)Z
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
.catch android/content/pm/PackageManager$NameNotFoundException from L3 to L4 using L2
aconst_null
astore 1
L0:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
aload 0
bipush 64
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
getfield android/content/pm/PackageInfo/signatures [Landroid/content/pm/Signature;
iconst_0
aaload
invokevirtual android/content/pm/Signature/toByteArray()[B
astore 3
L1:
aload 3
astore 1
L3:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getConfig()Landroid/content/SharedPreferences;
aload 0
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
istore 2
L4:
aload 3
astore 1
iload 2
ifeq L5
L6:
iconst_1
ireturn
L2:
astore 0
aload 0
invokevirtual android/content/pm/PackageManager$NameNotFoundException/printStackTrace()V
L5:
aload 1
invokestatic com/google/android/finsky/billing/iab/google/util/Base64/encode([B)Ljava/lang/String;
ldc "\n"
ldc ""
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
ldc "MIIEqDCCA5CgAwIBAgIJAJNurL4H8gHfMA0GCSqGSIb3DQEBBQUAMIGUMQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEQMA4GA1UEChMHQW5kcm9pZDEQMA4GA1UECxMHQW5kcm9pZDEQMA4GA1UEAxMHQW5kcm9pZDEiMCAGCSqGSIb3DQEJARYTYW5kcm9pZEBhbmRyb2lkLmNvbTAeFw0wODAyMjkwMTMzNDZaFw0zNTA3MTcwMTMzNDZaMIGUMQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEQMA4GA1UEChMHQW5kcm9pZDEQMA4GA1UECxMHQW5kcm9pZDEQMA4GA1UEAxMHQW5kcm9pZDEiMCAGCSqGSIb3DQEJARYTYW5kcm9pZEBhbmRyb2lkLmNvbTCCASAwDQYJKoZIhvcNAQEBBQADggENADCCAQgCggEBANaTGQTexgskse3HYuDZ2CU+Ps1s6x3i/waMqOi8qM1r03hupwqnbOYOuw+ZNVn/2T53qUPn6D1LZLjk/qLT5lbx4meoG7+yMLV4wgRDvkxyGLhG9SEVhvA4oU6Jwr44f46+z4/Kw9oe4zDJ6pPQp8PcSvNQIg1QCAcy4ICXF+5qBTNZ5qaU7Cyz8oSgpGbIepTYOzEJOmc3Li9kEsBubULxWBjf/gOBzAzURNps3cO4JFgZSAGzJWQTT7/emMkod0jb9WdqVA2BVMi7yge54kdVMxHEa5r3b97szI5p58ii0I54JiCUP5lyfTwE/nKZHZnfm644oLIXf6MdW2r+6R8CAQOjgfwwgfkwHQYDVR0OBBYEFEhZAFY9JyxGrhGGBaR0GawJyowRMIHJBgNVHSMEgcEwgb6AFEhZAFY9JyxGrhGGBaR0GawJyowRoYGapIGXMIGUMQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEQMA4GA1UEChMHQW5kcm9pZDEQMA4GA1UECxMHQW5kcm9pZDEQMA4GA1UEAxMHQW5kcm9pZDEiMCAGCSqGSIb3DQEJARYTYW5kcm9pZEBhbmRyb2lkLmNvbYIJAJNurL4H8gHfMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQEFBQADggEBAHqvlozrUMRBBVEY0NqrrwFbinZaJ6cVosK0TyIUFf/azgMJWr+kLfcHCHJsIGnlw27drgQAvilFLAhLwn62oX6snb4YLCBOsVMR9FXYJLZW2+TcIkCRLXWG/oiVHQGo/rWuWkJgU134NDEFJCJGjDbiLCpe+ZTWHdcwauTJ9pUbo8EvHRkU3cYfGmLaLfgn9gP+pWA7LFQNvXwBnDa6sppCccEX31I828XzgXpJ4O+mDL1/dBd+ek8ZPUP0IgdyZm5MTYPhvVqGCHzzTy3sIeJFymwrsBbmg2OAUNLEMO6nwmocSdN2ClirfxqCzJOLSDE4QyS9BAH6EhY6UFcOaE0="
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L6
iconst_0
ireturn
.limit locals 4
.limit stack 3
.end method

.method public static final isNetworkAvailable()Z
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "connectivity"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/net/ConnectivityManager
invokevirtual android/net/ConnectivityManager/getActiveNetworkInfo()Landroid/net/NetworkInfo;
astore 0
aload 0
ifnull L0
aload 0
invokevirtual android/net/NetworkInfo/isConnected()Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public static isOdex(Ljava/lang/String;)Z
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
L0:
new java/io/File
dup
aload 0
ldc "odex"
invokestatic com/chelpus/Utils/changeExtension(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L3
L1:
iconst_1
ireturn
L3:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/arm/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc "odex"
invokestatic com/chelpus/Utils/changeExtension(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifne L5
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/arm64/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc "odex"
invokestatic com/chelpus/Utils/changeExtension(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifne L5
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/x86/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/io/File/getName()Ljava/lang/String;
ldc "odex"
invokestatic com/chelpus/Utils/changeExtension(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
istore 1
L4:
iload 1
ifne L5
L6:
iconst_0
ireturn
L2:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
goto L6
L5:
iconst_1
ireturn
.limit locals 2
.limit stack 5
.end method

.method public static isRebuildedOrOdex(Ljava/lang/String;Landroid/content/Context;)Z
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
.catch android/content/pm/PackageManager$NameNotFoundException from L3 to L4 using L2
.catch android/content/pm/PackageManager$NameNotFoundException from L5 to L6 using L2
.catch android/content/pm/PackageManager$NameNotFoundException from L7 to L8 using L2
aconst_null
astore 3
aload 3
astore 1
L0:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
aload 0
bipush 64
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
astore 4
L1:
aload 3
astore 1
L3:
aload 4
getfield android/content/pm/PackageInfo/signatures [Landroid/content/pm/Signature;
iconst_0
aaload
invokevirtual android/content/pm/Signature/toByteArray()[B
astore 3
L4:
aload 3
astore 1
L5:
aload 4
getfield android/content/pm/PackageInfo/applicationInfo Landroid/content/pm/ApplicationInfo;
getfield android/content/pm/ApplicationInfo/sourceDir Ljava/lang/String;
invokestatic com/chelpus/Utils/isOdex(Ljava/lang/String;)Z
ifeq L9
L6:
iconst_1
ireturn
L9:
aload 3
astore 1
L7:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getConfig()Landroid/content/SharedPreferences;
aload 0
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
istore 2
L8:
iload 2
ifne L10
aload 3
astore 1
L11:
aload 1
invokestatic com/google/android/finsky/billing/iab/google/util/Base64/encode([B)Ljava/lang/String;
ldc "\n"
ldc ""
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
ldc "MIIEqDCCA5CgAwIBAgIJAJNurL4H8gHfMA0GCSqGSIb3DQEBBQUAMIGUMQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEQMA4GA1UEChMHQW5kcm9pZDEQMA4GA1UECxMHQW5kcm9pZDEQMA4GA1UEAxMHQW5kcm9pZDEiMCAGCSqGSIb3DQEJARYTYW5kcm9pZEBhbmRyb2lkLmNvbTAeFw0wODAyMjkwMTMzNDZaFw0zNTA3MTcwMTMzNDZaMIGUMQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEQMA4GA1UEChMHQW5kcm9pZDEQMA4GA1UECxMHQW5kcm9pZDEQMA4GA1UEAxMHQW5kcm9pZDEiMCAGCSqGSIb3DQEJARYTYW5kcm9pZEBhbmRyb2lkLmNvbTCCASAwDQYJKoZIhvcNAQEBBQADggENADCCAQgCggEBANaTGQTexgskse3HYuDZ2CU+Ps1s6x3i/waMqOi8qM1r03hupwqnbOYOuw+ZNVn/2T53qUPn6D1LZLjk/qLT5lbx4meoG7+yMLV4wgRDvkxyGLhG9SEVhvA4oU6Jwr44f46+z4/Kw9oe4zDJ6pPQp8PcSvNQIg1QCAcy4ICXF+5qBTNZ5qaU7Cyz8oSgpGbIepTYOzEJOmc3Li9kEsBubULxWBjf/gOBzAzURNps3cO4JFgZSAGzJWQTT7/emMkod0jb9WdqVA2BVMi7yge54kdVMxHEa5r3b97szI5p58ii0I54JiCUP5lyfTwE/nKZHZnfm644oLIXf6MdW2r+6R8CAQOjgfwwgfkwHQYDVR0OBBYEFEhZAFY9JyxGrhGGBaR0GawJyowRMIHJBgNVHSMEgcEwgb6AFEhZAFY9JyxGrhGGBaR0GawJyowRoYGapIGXMIGUMQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEQMA4GA1UEChMHQW5kcm9pZDEQMA4GA1UECxMHQW5kcm9pZDEQMA4GA1UEAxMHQW5kcm9pZDEiMCAGCSqGSIb3DQEJARYTYW5kcm9pZEBhbmRyb2lkLmNvbYIJAJNurL4H8gHfMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQEFBQADggEBAHqvlozrUMRBBVEY0NqrrwFbinZaJ6cVosK0TyIUFf/azgMJWr+kLfcHCHJsIGnlw27drgQAvilFLAhLwn62oX6snb4YLCBOsVMR9FXYJLZW2+TcIkCRLXWG/oiVHQGo/rWuWkJgU134NDEFJCJGjDbiLCpe+ZTWHdcwauTJ9pUbo8EvHRkU3cYfGmLaLfgn9gP+pWA7LFQNvXwBnDa6sppCccEX31I828XzgXpJ4O+mDL1/dBd+ek8ZPUP0IgdyZm5MTYPhvVqGCHzzTy3sIeJFymwrsBbmg2OAUNLEMO6nwmocSdN2ClirfxqCzJOLSDE4QyS9BAH6EhY6UFcOaE0="
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L10
iconst_0
ireturn
L2:
astore 0
aload 0
invokevirtual android/content/pm/PackageManager$NameNotFoundException/printStackTrace()V
goto L11
L10:
iconst_1
ireturn
.limit locals 5
.limit stack 3
.end method

.method public static isServiceRunning(Ljava/lang/String;)Z
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "activity"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/app/ActivityManager
ldc_w 2147483647
invokevirtual android/app/ActivityManager/getRunningServices(I)Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/app/ActivityManager$RunningServiceInfo
getfield android/app/ActivityManager$RunningServiceInfo/service Landroid/content/ComponentName;
invokevirtual android/content/ComponentName/getClassName()Ljava/lang/String;
aload 0
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public static isWithFramework()Z
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/dalvikruncommandWithFramework Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".checkWithFramework 123"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
astore 0
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
aload 0
ldc "withoutFramework"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/errorOutput Ljava/lang/String;
ldc "java.lang.ClassNotFoundException:"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L1
L0:
iconst_0
ireturn
L1:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "WithFramework support"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iconst_1
ireturn
.limit locals 1
.limit stack 6
.end method

.method public static isXposedEnabled()Z
iconst_0
ireturn
.limit locals 0
.limit stack 1
.end method

.method public static javaToDosTime(J)J
new java/util/Date
dup
lload 0
invokespecial java/util/Date/<init>(J)V
astore 3
aload 3
invokevirtual java/util/Date/getYear()I
sipush 1900
iadd
istore 2
iload 2
sipush 1980
if_icmpge L0
ldc2_w 2162688L
lreturn
L0:
iload 2
sipush 1980
isub
bipush 25
ishl
aload 3
invokevirtual java/util/Date/getMonth()I
iconst_1
iadd
bipush 21
ishl
ior
aload 3
invokevirtual java/util/Date/getDate()I
bipush 16
ishl
ior
aload 3
invokevirtual java/util/Date/getHours()I
bipush 11
ishl
ior
aload 3
invokevirtual java/util/Date/getMinutes()I
iconst_5
ishl
ior
aload 3
invokevirtual java/util/Date/getSeconds()I
iconst_1
ishr
ior
i2l
lreturn
.limit locals 4
.limit stack 4
.end method

.method public static kill(Ljava/lang/String;)V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifne L0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "killall "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
aload 0
invokestatic com/chelpus/Utils/killAll(Ljava/lang/String;)Z
pop
return
L0:
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "killall"
aastore
dup
iconst_1
aload 0
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
aload 0
invokestatic com/chelpus/Utils/killAll(Ljava/lang/String;)Z
pop
return
.limit locals 1
.limit stack 4
.end method

.method public static killAll(Ljava/lang/String;)Z
.catch java/io/IOException from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
.catch java/lang/Exception from L6 to L7 using L8
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifne L9
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "^\\S+\\s+([0-9]+).*"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokestatic java/util/regex/Pattern/quote(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "$"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
astore 5
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "ps "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
astore 3
aload 3
astore 4
aload 3
ldc "~"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L10
invokestatic com/chelpus/Utils/exitRoot()V
L0:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getSu()Ljava/lang/Process;
pop
L1:
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "ps "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
astore 4
aload 4
astore 3
aload 4
ldc "~"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L11
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "busybox ps "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
astore 3
L11:
aload 3
astore 4
aload 3
ldc "~"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L10
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "toolbox ps "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
astore 4
L10:
aload 4
ldc "~"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L12
aload 4
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L12
aload 4
ldc "\n"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 3
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 4
aload 3
arraylength
iconst_1
if_icmple L12
aload 3
arraylength
istore 2
iconst_0
istore 1
L13:
iload 1
iload 2
if_icmpge L14
aload 3
iload 1
aaload
astore 6
aload 6
aload 0
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L4
aload 5
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
astore 6
L3:
aload 6
invokevirtual java/util/regex/Matcher/find()Z
ifeq L4
aload 6
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
astore 6
aload 4
aload 6
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Found pid: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " for "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L4:
iload 1
iconst_1
iadd
istore 1
goto L13
L2:
astore 3
aload 3
invokevirtual java/io/IOException/printStackTrace()V
goto L1
L5:
astore 6
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Error with regex! "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
goto L4
L14:
aload 4
invokevirtual java/util/ArrayList/size()I
ifle L12
aload 4
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 3
L15:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L16
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 4
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Kill: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " for "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "kill -9 "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
pop
goto L15
L16:
iconst_1
ireturn
L12:
iconst_0
ireturn
L9:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "^\\S+\\s+([0-9]+).*"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokestatic java/util/regex/Pattern/quote(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "$"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
astore 5
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "ps"
aastore
dup
iconst_1
aload 0
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
astore 4
aload 4
astore 3
aload 4
ldc "~"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L17
aload 4
astore 3
aload 4
ldc "~"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L18
iconst_3
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/busybox"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_1
ldc "ps"
aastore
dup
iconst_2
aload 0
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
astore 3
L18:
aload 3
astore 4
aload 3
ldc "~"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L19
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "busybox"
aastore
dup
iconst_1
ldc "ps"
aastore
dup
iconst_2
aload 0
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
astore 4
L19:
aload 4
astore 3
aload 4
ldc "~"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L17
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "toolbox"
aastore
dup
iconst_1
ldc "ps"
aastore
dup
iconst_2
aload 0
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
astore 3
L17:
aload 3
ldc "~"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L20
aload 3
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L20
aload 3
ldc "\n"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 3
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 4
aload 3
arraylength
iconst_1
if_icmple L20
aload 3
arraylength
istore 2
iconst_0
istore 1
L21:
iload 1
iload 2
if_icmpge L22
aload 3
iload 1
aaload
astore 6
aload 6
aload 0
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L7
aload 5
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
astore 6
L6:
aload 6
invokevirtual java/util/regex/Matcher/find()Z
ifeq L7
aload 6
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
astore 6
aload 4
aload 6
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Found pid: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " for "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L7:
iload 1
iconst_1
iadd
istore 1
goto L21
L8:
astore 6
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Error with regex! "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
goto L7
L22:
aload 4
invokevirtual java/util/ArrayList/size()I
ifle L20
aload 4
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 3
L23:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L24
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 4
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Kill: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " for "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "kill"
aastore
dup
iconst_1
ldc "-9"
aastore
dup
iconst_2
aload 4
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
goto L23
L24:
iconst_1
ireturn
L20:
iconst_0
ireturn
.limit locals 7
.limit stack 6
.end method

.method public static market_billing_services(Z)V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/su Z
ifeq L0
new java/lang/Thread
dup
new com/chelpus/Utils$13
dup
iload 0
invokespecial com/chelpus/Utils$13/<init>(Z)V
invokespecial java/lang/Thread/<init>(Ljava/lang/Runnable;)V
invokevirtual java/lang/Thread/start()V
L0:
return
.limit locals 1
.limit stack 5
.end method

.method public static market_billing_services_to_main_stream(Z)V
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
.catch java/lang/Exception from L4 to L6 using L5
.catch java/lang/Exception from L7 to L8 using L5
.catch java/lang/Exception from L8 to L9 using L5
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/su Z
ifeq L10
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
pop
aconst_null
astore 2
L0:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
ldc "com.android.vending"
iconst_4
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
astore 3
L1:
aload 3
astore 2
L11:
aload 2
ifnull L10
aload 2
getfield android/content/pm/PackageInfo/services [Landroid/content/pm/ServiceInfo;
ifnull L10
aload 2
getfield android/content/pm/PackageInfo/services [Landroid/content/pm/ServiceInfo;
arraylength
ifeq L10
iconst_0
istore 1
L12:
iload 1
aload 2
getfield android/content/pm/PackageInfo/services [Landroid/content/pm/ServiceInfo;
arraylength
if_icmpge L10
iload 0
ifne L7
L3:
aload 2
getfield android/content/pm/PackageInfo/services [Landroid/content/pm/ServiceInfo;
iload 1
aaload
getfield android/content/pm/ServiceInfo/name Ljava/lang/String;
ldc "InAppBillingService"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifne L4
aload 2
getfield android/content/pm/PackageInfo/services [Landroid/content/pm/ServiceInfo;
iload 1
aaload
getfield android/content/pm/ServiceInfo/name Ljava/lang/String;
ldc "MarketBillingService"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L6
L4:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
ldc "com.android.vending"
aload 2
getfield android/content/pm/PackageInfo/services [Landroid/content/pm/ServiceInfo;
iload 1
aaload
getfield android/content/pm/ServiceInfo/name Ljava/lang/String;
invokespecial android/content/ComponentName/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/content/pm/PackageManager/getComponentEnabledSetting(Landroid/content/ComponentName;)I
iconst_2
if_icmpeq L6
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_2
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "pm disable 'com.android.vending/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
getfield android/content/pm/PackageInfo/services [Landroid/content/pm/ServiceInfo;
iload 1
aaload
getfield android/content/pm/ServiceInfo/name Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_1
ldc "skipOut"
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
pop
L6:
iload 1
iconst_1
iadd
istore 1
goto L12
L2:
astore 3
aload 3
invokevirtual android/content/pm/PackageManager$NameNotFoundException/printStackTrace()V
goto L11
L7:
aload 2
getfield android/content/pm/PackageInfo/services [Landroid/content/pm/ServiceInfo;
iload 1
aaload
getfield android/content/pm/ServiceInfo/name Ljava/lang/String;
ldc "InAppBillingService"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifne L8
aload 2
getfield android/content/pm/PackageInfo/services [Landroid/content/pm/ServiceInfo;
iload 1
aaload
getfield android/content/pm/ServiceInfo/name Ljava/lang/String;
ldc "MarketBillingService"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L6
L8:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
ldc "com.android.vending"
aload 2
getfield android/content/pm/PackageInfo/services [Landroid/content/pm/ServiceInfo;
iload 1
aaload
getfield android/content/pm/ServiceInfo/name Ljava/lang/String;
invokespecial android/content/ComponentName/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/content/pm/PackageManager/getComponentEnabledSetting(Landroid/content/ComponentName;)I
iconst_1
if_icmpeq L6
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_2
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "pm enable 'com.android.vending/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
getfield android/content/pm/PackageInfo/services [Landroid/content/pm/ServiceInfo;
iload 1
aaload
getfield android/content/pm/ServiceInfo/name Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_1
ldc "skipOut"
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
pop
L9:
goto L6
L5:
astore 3
aload 3
invokevirtual java/lang/Exception/printStackTrace()V
goto L6
L10:
return
.limit locals 4
.limit stack 7
.end method

.method public static market_licensing_services(Z)V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/su Z
ifeq L0
new java/lang/Thread
dup
new com/chelpus/Utils$14
dup
iload 0
invokespecial com/chelpus/Utils$14/<init>(Z)V
invokespecial java/lang/Thread/<init>(Ljava/lang/Runnable;)V
invokevirtual java/lang/Thread/start()V
L0:
return
.limit locals 1
.limit stack 5
.end method

.method public static market_licensing_services_to_main_stream(Z)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/su Z
ifeq L5
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
pop
ldc "com.android.vending"
iconst_4
invokestatic com/chelpus/Utils/getPkgInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
astore 2
aload 2
ifnull L5
aload 2
getfield android/content/pm/PackageInfo/services [Landroid/content/pm/ServiceInfo;
ifnull L5
aload 2
getfield android/content/pm/PackageInfo/services [Landroid/content/pm/ServiceInfo;
arraylength
ifeq L5
iconst_0
istore 1
L6:
iload 1
aload 2
getfield android/content/pm/PackageInfo/services [Landroid/content/pm/ServiceInfo;
arraylength
if_icmpge L5
iload 0
ifne L3
L0:
aload 2
getfield android/content/pm/PackageInfo/services [Landroid/content/pm/ServiceInfo;
iload 1
aaload
getfield android/content/pm/ServiceInfo/name Ljava/lang/String;
ldc "LicensingService"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L7
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
ldc "com.android.vending"
aload 2
getfield android/content/pm/PackageInfo/services [Landroid/content/pm/ServiceInfo;
iload 1
aaload
getfield android/content/pm/ServiceInfo/name Ljava/lang/String;
invokespecial android/content/ComponentName/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/content/pm/PackageManager/getComponentEnabledSetting(Landroid/content/ComponentName;)I
iconst_2
if_icmpeq L7
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_2
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "pm disable 'com.android.vending/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
getfield android/content/pm/PackageInfo/services [Landroid/content/pm/ServiceInfo;
iload 1
aaload
getfield android/content/pm/ServiceInfo/name Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_1
ldc "skipOut"
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
pop
L1:
goto L7
L3:
aload 2
getfield android/content/pm/PackageInfo/services [Landroid/content/pm/ServiceInfo;
iload 1
aaload
getfield android/content/pm/ServiceInfo/name Ljava/lang/String;
ldc "LicensingService"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L7
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
ldc "com.android.vending"
aload 2
getfield android/content/pm/PackageInfo/services [Landroid/content/pm/ServiceInfo;
iload 1
aaload
getfield android/content/pm/ServiceInfo/name Ljava/lang/String;
invokespecial android/content/ComponentName/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/content/pm/PackageManager/getComponentEnabledSetting(Landroid/content/ComponentName;)I
iconst_1
if_icmpeq L7
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_2
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "pm enable 'com.android.vending/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
getfield android/content/pm/PackageInfo/services [Landroid/content/pm/ServiceInfo;
iload 1
aaload
getfield android/content/pm/ServiceInfo/name Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_1
ldc "skipOut"
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
pop
L4:
goto L7
L2:
astore 3
aload 3
invokevirtual java/lang/Exception/printStackTrace()V
goto L7
L5:
return
L7:
iload 1
iconst_1
iadd
istore 1
goto L6
.limit locals 4
.limit stack 7
.end method

.method public static onMainThread()Z
invokestatic android/os/Looper/myLooper()Landroid/os/Looper;
ifnull L0
invokestatic android/os/Looper/myLooper()Landroid/os/Looper;
invokestatic android/os/Looper/getMainLooper()Landroid/os/Looper;
if_acmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 0
.limit stack 2
.end method

.method public static pattern_checker(Landroid/app/Activity;)Z
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
.catch android/content/pm/PackageManager$NameNotFoundException from L3 to L4 using L2
iconst_0
istore 1
aload 0
invokevirtual android/app/Activity/getApplicationInfo()Landroid/content/pm/ApplicationInfo;
getfield android/content/pm/ApplicationInfo/sourceDir Ljava/lang/String;
astore 2
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 2
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aconst_null
astore 2
L0:
aload 0
invokevirtual android/app/Activity/getPackageManager()Landroid/content/pm/PackageManager;
aload 0
invokevirtual android/app/Activity/getPackageName()Ljava/lang/String;
bipush 64
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
getfield android/content/pm/PackageInfo/signatures [Landroid/content/pm/Signature;
iconst_0
aaload
invokevirtual android/content/pm/Signature/toByteArray()[B
astore 0
L1:
aload 0
astore 2
L3:
getstatic java/lang/System/out Ljava/io/PrintStream;
invokevirtual java/io/PrintStream/println()V
L4:
aload 0
invokestatic com/google/android/finsky/billing/iab/google/util/Base64/encode([B)Ljava/lang/String;
ldc "\n"
ldc ""
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
ldc "MIIDDTCCAfWgAwIBAgIEeR8eUDANBgkqhkiG9w0BAQsFADA3MQswCQYDVQQGEwJVUzEQMA4GA1UEChMHQW5kcm9pZDEWMBQGA1UEAxMNQW5kcm9pZCBEZWJ1ZzAeFw0xMTEyMDgwNjA1MTBaFw00MTExMzAwNjA1MTBaMDcxCzAJBgNVBAYTAlVTMRAwDgYDVQQKEwdBbmRyb2lkMRYwFAYDVQQDEw1BbmRyb2lkIERlYnVnMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhENqFp22Dq9M/CEU4on3/xGfoWggUk4tahTdC/okxdVO/nB27odddvB+zDiMSq+mGFprWxk31pzu+W31pbdq4tnBF6aqzhLanAjxVoeuqNUDzXfqNhxjQDJjZ9Q9zntEHNllIkfJclYyADf1GHjQs9vpgo58EXQ4Wt8REG9P+8My5ENmVkfTA3L7yryyTnplRn7d+jVtIcJEKY0s/kCFfRMNJnM2vYYWGpXrmEJFMNPtjvPGvnNgMojHLgWqY7z7foplBjGfEItX/huYZqp7+ZaGWyrksXHStEUXUa7TJJiW++R4e4VL6jIDwTHGOAgYaVA/ZarfLquQhXP28vBNhwIDAQABoyEwHzAdBgNVHQ4EFgQUNrZ//EPQx9WdAor2L5dvsy6i9eYwDQYJKoZIhvcNAQELBQADggEBAH09ZytGQmSrbGNjbCMnuZ+UuKOP+nN5j0U0hbMisC+2rcox36S23hVDPEc7rcBMo/Aep4kY/CZCO9UnRVP5NG3YugQU2mwimM2po4pZZbOBCDx4dEjA4ymJpKlS4fEPQ1qp5p9um8wmMVg5Yl5y9dGpxNF/USDW5jq+H8SBhfcrro+m4V+G/jPGWSN/0QwJpb0dmsD2MLgw7/HyJPnymvSEzom6e7Oe4aJDzOKuRM5hrfvsNyH+WTq+f+IElEVMg1zwo0JHhFTppxEFROPHTYO2FjMdrA26KdPcLTS07pzpP00/0n+4R7SPoAHzMBptlvNZws9KvaQEiOc0ObXhjL0="
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L5
iconst_1
istore 1
L5:
iload 1
ireturn
L2:
astore 0
aload 0
invokevirtual android/content/pm/PackageManager$NameNotFoundException/printStackTrace()V
aload 2
astore 0
goto L4
.limit locals 3
.limit stack 3
.end method

.method public static readLine(Ljava/io/InputStream;)Ljava/lang/String;
.throws java/io/IOException
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
astore 2
L0:
aload 0
invokevirtual java/io/InputStream/read()I
istore 1
iload 1
ifge L1
aconst_null
areturn
L1:
iload 1
bipush 10
if_icmpne L2
new java/lang/String
dup
aload 2
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
ldc "UTF-8"
invokespecial java/lang/String/<init>([BLjava/lang/String;)V
areturn
L2:
aload 2
iload 1
invokevirtual java/io/ByteArrayOutputStream/write(I)V
goto L0
.limit locals 3
.limit stack 4
.end method

.method public static readXposedParamBoolean()Lorg/json/JSONObject;
.throws org/json/JSONException
new org/json/JSONObject
dup
new java/io/File
dup
ldc "/data/lp/xposed"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokestatic com/chelpus/Utils/read_from_file(Ljava/io/File;)Ljava/lang/String;
invokespecial org/json/JSONObject/<init>(Ljava/lang/String;)V
areturn
.limit locals 0
.limit stack 5
.end method

.method public static read_from_file(Ljava/io/File;)Ljava/lang/String;
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L4
aload 0
invokevirtual java/io/File/length()J
l2i
newarray byte
astore 1
L0:
new java/io/RandomAccessFile
dup
aload 0
ldc "r"
invokespecial java/io/RandomAccessFile/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 0
L1:
aload 0
lconst_0
invokevirtual java/io/RandomAccessFile/seek(J)V
aload 0
aload 1
invokevirtual java/io/RandomAccessFile/read([B)I
pop
aload 0
invokevirtual java/io/RandomAccessFile/close()V
L3:
new java/lang/String
dup
aload 1
invokespecial java/lang/String/<init>([B)V
areturn
L2:
astore 0
L5:
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
goto L3
L4:
astore 0
goto L5
.limit locals 2
.limit stack 4
.end method

.method public static reboot()V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L4
.catch java/lang/Exception from L3 to L5 using L6
.catch java/lang/Exception from L5 to L7 using L8
.catch java/lang/Exception from L7 to L9 using L10
.catch java/lang/Exception from L9 to L11 using L12
.catch java/lang/Exception from L11 to L13 using L14
.catch java/lang/Exception from L15 to L16 using L17
.catch java/lang/Exception from L18 to L19 using L20
.catch java/lang/Exception from L19 to L21 using L22
.catch java/lang/Exception from L21 to L23 using L24
.catch java/lang/Exception from L23 to L25 using L26
.catch java/lang/Exception from L25 to L27 using L28
.catch java/lang/Exception from L27 to L29 using L30
.catch java/lang/Exception from L29 to L31 using L32
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifne L18
L0:
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "reboot"
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
pop
L1:
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "/system/bin/reboot"
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
pop
L3:
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "/system/xbin/reboot"
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
pop
L5:
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "busybox reboot"
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
pop
L7:
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "reboot"
aastore
invokestatic com/chelpus/Utils/cmd([Ljava/lang/String;)Ljava/lang/String;
pop
L9:
ldc "reboot"
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
L11:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/patchAct Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;
new com/chelpus/Utils$15
dup
invokespecial com/chelpus/Utils$15/<init>()V
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/patchActivity/runOnUiThread(Ljava/lang/Runnable;)V
L13:
new com/chelpus/Utils
dup
ldc "w"
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
ldc2_w 5000L
invokevirtual com/chelpus/Utils/waitLP(J)V
L15:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
invokevirtual android/content/Context/getFilesDir()Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/reboot"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
L16:
return
L2:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
goto L1
L4:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
goto L3
L6:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
goto L5
L8:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
goto L7
L10:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
goto L9
L12:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
goto L11
L14:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
goto L13
L17:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
return
L18:
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "reboot"
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L19:
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "/system/bin/reboot"
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L21:
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "/system/xbin/reboot"
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L23:
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "busybox reboot"
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L25:
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "reboot"
aastore
invokestatic com/chelpus/Utils/cmd([Ljava/lang/String;)Ljava/lang/String;
pop
L27:
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "reboot"
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L29:
iconst_1
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/reboot"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L31:
return
L32:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
return
L20:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
goto L19
L22:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
goto L21
L24:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
goto L23
L26:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
goto L25
L28:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
goto L27
L30:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
goto L29
.limit locals 1
.limit stack 5
.end method

.method public static remount(Ljava/lang/String;Ljava/lang/String;)Z
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifne L6
aload 0
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "/system"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L7
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "stop ric"
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
pop
ldc "/sbin/ric"
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifne L8
ldc "/system/bin/ric"
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifeq L7
L8:
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "stop ric"
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
pop
ldc "pkill -f /sbin/ric"
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
ldc "pkill -f /system/bin/ric"
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
L7:
aload 0
astore 2
aload 0
ldc "/"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L9
aload 0
astore 2
aload 0
ldc "/"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L9
aload 0
iconst_0
aload 0
ldc "/"
invokevirtual java/lang/String/lastIndexOf(Ljava/lang/String;)I
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
astore 2
L9:
aload 2
invokestatic com/chelpus/Utils/findMountPointRecursive(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/Mount;
astore 3
aload 3
ifnull L10
aload 3
astore 0
aload 3
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/Mount/getFlags()Ljava/util/Set;
aload 1
getstatic java/util/Locale/US Ljava/util/Locale;
invokevirtual java/lang/String/toLowerCase(Ljava/util/Locale;)Ljava/lang/String;
invokeinterface java/util/Set/contains(Ljava/lang/Object;)Z 1
ifne L11
L0:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "mount -o remount,"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
getstatic java/util/Locale/US Ljava/util/Locale;
invokevirtual java/lang/String/toLowerCase(Ljava/util/Locale;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/Mount/getDevice()Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/Mount/getMountPoint()Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
aload 2
invokestatic com/chelpus/Utils/findMountPointRecursive(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/Mount;
astore 0
aload 0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/Mount/getFlags()Ljava/util/Set;
aload 1
getstatic java/util/Locale/US Ljava/util/Locale;
invokevirtual java/lang/String/toLowerCase(Ljava/util/Locale;)Ljava/lang/String;
invokeinterface java/util/Set/contains(Ljava/lang/Object;)Z 1
ifne L1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "mount -o remount "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
getstatic java/util/Locale/US Ljava/util/Locale;
invokevirtual java/lang/String/toLowerCase(Ljava/util/Locale;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/Mount/getDevice()Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/Mount/getMountPoint()Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
L1:
aload 2
invokestatic com/chelpus/Utils/findMountPointRecursive(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/Mount;
astore 0
L11:
aload 0
ifnull L10
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/Mount/getFlags()Ljava/util/Set;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " AND "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
getstatic java/util/Locale/US Ljava/util/Locale;
invokevirtual java/lang/String/toLowerCase(Ljava/util/Locale;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/Mount/getFlags()Ljava/util/Set;
aload 1
getstatic java/util/Locale/US Ljava/util/Locale;
invokevirtual java/lang/String/toLowerCase(Ljava/util/Locale;)Ljava/lang/String;
invokeinterface java/util/Set/contains(Ljava/lang/Object;)Z 1
ifeq L10
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/Mount/getFlags()Ljava/util/Set;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iconst_1
ireturn
L10:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher:not remount "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " to "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iconst_0
ireturn
L6:
aload 0
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
ldc "/system"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L12
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "stop"
aastore
dup
iconst_1
ldc "ric"
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
new java/io/File
dup
ldc "/sbin/ric"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifne L13
new java/io/File
dup
ldc "/system/bin/ric"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L12
L13:
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "stop"
aastore
dup
iconst_1
ldc "ric"
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "pkill"
aastore
dup
iconst_1
ldc "-f"
aastore
dup
iconst_2
ldc "/sbin/ric"
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "pkill"
aastore
dup
iconst_1
ldc "-f"
aastore
dup
iconst_2
ldc "/system/bin/ric"
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L12:
aload 0
astore 2
aload 0
ldc "/"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L14
aload 0
astore 2
aload 0
ldc "/"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L14
aload 0
iconst_0
aload 0
ldc "/"
invokevirtual java/lang/String/lastIndexOf(Ljava/lang/String;)I
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
astore 2
L14:
aload 2
invokestatic com/chelpus/Utils/findMountPointRecursive(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/Mount;
astore 3
aload 3
ifnull L15
aload 3
astore 0
aload 3
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/Mount/getFlags()Ljava/util/Set;
aload 1
getstatic java/util/Locale/US Ljava/util/Locale;
invokevirtual java/lang/String/toLowerCase(Ljava/util/Locale;)Ljava/lang/String;
invokeinterface java/util/Set/contains(Ljava/lang/Object;)Z 1
ifne L16
L3:
iconst_5
anewarray java/lang/String
dup
iconst_0
ldc "mount"
aastore
dup
iconst_1
ldc "-o"
aastore
dup
iconst_2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "remount,"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
getstatic java/util/Locale/US Ljava/util/Locale;
invokevirtual java/lang/String/toLowerCase(Ljava/util/Locale;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_3
aload 3
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/Mount/getDevice()Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aastore
dup
iconst_4
aload 3
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/Mount/getMountPoint()Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
aload 2
invokestatic com/chelpus/Utils/findMountPointRecursive(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/Mount;
astore 0
aload 0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/Mount/getFlags()Ljava/util/Set;
aload 1
getstatic java/util/Locale/US Ljava/util/Locale;
invokevirtual java/lang/String/toLowerCase(Ljava/util/Locale;)Ljava/lang/String;
invokeinterface java/util/Set/contains(Ljava/lang/Object;)Z 1
ifne L4
bipush 6
anewarray java/lang/String
dup
iconst_0
ldc "mount"
aastore
dup
iconst_1
ldc "-o"
aastore
dup
iconst_2
ldc "remount"
aastore
dup
iconst_3
aload 1
getstatic java/util/Locale/US Ljava/util/Locale;
invokevirtual java/lang/String/toLowerCase(Ljava/util/Locale;)Ljava/lang/String;
aastore
dup
iconst_4
aload 0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/Mount/getDevice()Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aastore
dup
iconst_5
aload 0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/Mount/getMountPoint()Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L4:
aload 2
invokestatic com/chelpus/Utils/findMountPointRecursive(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/Mount;
astore 0
L16:
aload 0
ifnull L15
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/Mount/getFlags()Ljava/util/Set;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " AND "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
getstatic java/util/Locale/US Ljava/util/Locale;
invokevirtual java/lang/String/toLowerCase(Ljava/util/Locale;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/Mount/getFlags()Ljava/util/Set;
aload 1
getstatic java/util/Locale/US Ljava/util/Locale;
invokevirtual java/lang/String/toLowerCase(Ljava/util/Locale;)Ljava/lang/String;
invokeinterface java/util/Set/contains(Ljava/lang/Object;)Z 1
ifeq L15
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/Mount/getFlags()Ljava/util/Set;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iconst_1
ireturn
L15:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher:not remount "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " to "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iconst_0
ireturn
L5:
astore 0
goto L4
L2:
astore 0
goto L1
.limit locals 4
.limit stack 6
.end method

.method public static removeExtension(Ljava/lang/String;)Ljava/lang/String;
ldc ""
astore 3
aload 3
astore 2
aload 0
ifnull L0
aload 0
ldc "\\."
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 4
iconst_0
istore 1
aload 3
astore 0
L1:
aload 0
astore 2
iload 1
aload 4
arraylength
if_icmpge L0
iload 1
aload 4
arraylength
iconst_2
isub
if_icmpge L2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
iload 1
aaload
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
iload 1
aaload
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 2
L0:
aload 2
areturn
.limit locals 5
.limit stack 3
.end method

.method public static removePkgFromSystem(Ljava/lang/String;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
.catch java/lang/Exception from L6 to L7 using L5
.catch java/lang/Exception from L8 to L9 using L5
.catch java/lang/Exception from L10 to L11 using L12
.catch java/lang/Exception from L13 to L14 using L5
ldc "/system"
ldc "rw"
invokestatic com/chelpus/Utils/remount(Ljava/lang/String;Ljava/lang/String;)Z
pop
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 6
aload 6
new java/io/File
dup
ldc "/system/app"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 6
new java/io/File
dup
ldc "/system/priv-app"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 6
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 6
L15:
aload 6
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L16
aload 6
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
astore 8
aload 8
invokevirtual java/io/File/listFiles()[Ljava/io/File;
astore 7
aload 7
ifnull L17
aload 7
arraylength
ifeq L17
aload 7
arraylength
istore 2
iconst_0
istore 1
L18:
iload 1
iload 2
if_icmpge L19
aload 7
iload 1
aaload
astore 8
aload 8
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
ldc ".apk"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L1
L0:
aload 0
new com/android/vending/billing/InAppBillingService/LUCK/FileApkListItem
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
aload 8
iconst_0
invokespecial com/android/vending/billing/InAppBillingService/LUCK/FileApkListItem/<init>(Landroid/content/Context;Ljava/io/File;Z)V
getfield com/android/vending/billing/InAppBillingService/LUCK/FileApkListItem/pkgName Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chmod 777 '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 8
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "rm '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 8
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "rm '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 8
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
iconst_0
invokestatic com/chelpus/Utils/getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
L1:
iload 1
iconst_1
iadd
istore 1
goto L18
L2:
astore 8
aload 8
invokevirtual java/lang/Exception/printStackTrace()V
goto L1
L19:
aload 7
arraylength
istore 3
iconst_0
istore 1
L20:
iload 1
iload 3
if_icmpge L15
aload 7
iload 1
aaload
astore 8
aload 8
invokevirtual java/io/File/isDirectory()Z
ifeq L21
L3:
aload 8
invokevirtual java/io/File/listFiles()[Ljava/io/File;
astore 9
L4:
aload 9
ifnull L21
L6:
aload 9
arraylength
ifeq L21
aload 9
arraylength
istore 4
L7:
iconst_0
istore 2
L22:
iload 2
iload 4
if_icmpge L21
aload 9
iload 2
aaload
astore 10
L8:
aload 10
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
ldc ".apk"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
istore 5
L9:
iload 5
ifeq L11
L10:
aload 0
new com/android/vending/billing/InAppBillingService/LUCK/FileApkListItem
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
aload 10
iconst_0
invokespecial com/android/vending/billing/InAppBillingService/LUCK/FileApkListItem/<init>(Landroid/content/Context;Ljava/io/File;Z)V
getfield com/android/vending/billing/InAppBillingService/LUCK/FileApkListItem/pkgName Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L11
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "chmod 777 '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 10
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "rm '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 10
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "rm '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 10
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
iconst_0
invokestatic com/chelpus/Utils/getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "rm -rf '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 8
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
L11:
iload 2
iconst_1
iadd
istore 2
goto L22
L12:
astore 10
L13:
aload 10
invokevirtual java/lang/Exception/printStackTrace()V
L14:
goto L11
L5:
astore 8
aload 8
invokevirtual java/lang/Exception/printStackTrace()V
L21:
iload 1
iconst_1
iadd
istore 1
goto L20
L17:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher: 0 packages found in "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 8
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
goto L15
L16:
return
.limit locals 11
.limit stack 6
.end method

.method public static replaceStringIds(Ljava/lang/String;[Ljava/lang/String;Z[Ljava/lang/String;)I
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L7
.catch java/lang/Exception from L8 to L9 using L10
.catch java/lang/Exception from L11 to L12 using L10
.catch java/lang/Exception from L13 to L14 using L10
.catch java/lang/Exception from L15 to L16 using L10
.catch java/lang/Exception from L17 to L18 using L10
.catch java/lang/Exception from L19 to L20 using L10
.catch java/lang/Exception from L21 to L22 using L10
.catch java/lang/Exception from L23 to L24 using L10
.catch java/lang/Exception from L25 to L26 using L10
.catch java/lang/Exception from L27 to L28 using L10
.catch java/lang/Exception from L29 to L30 using L10
.catch java/lang/Exception from L31 to L32 using L10
.catch java/lang/Exception from L33 to L34 using L10
.catch java/lang/Exception from L35 to L36 using L10
.catch java/lang/Exception from L37 to L38 using L10
.catch java/lang/Exception from L39 to L40 using L10
.catch java/lang/Exception from L41 to L42 using L10
.catch java/lang/Exception from L43 to L44 using L10
.catch java/lang/Exception from L45 to L46 using L10
.catch java/lang/Exception from L47 to L48 using L10
.catch java/lang/Exception from L49 to L50 using L10
.catch java/lang/Exception from L51 to L52 using L10
.catch java/lang/Exception from L53 to L54 using L10
.catch java/lang/Exception from L55 to L56 using L10
.catch java/lang/Exception from L57 to L58 using L10
.catch java/lang/Exception from L59 to L60 using L10
.catch java/lang/Exception from L61 to L62 using L10
.catch java/lang/Exception from L63 to L64 using L7
.catch java/lang/Exception from L65 to L66 using L2
.catch java/lang/Exception from L67 to L68 using L2
.catch java/lang/Exception from L69 to L70 using L71
.catch java/lang/Exception from L72 to L73 using L71
.catch java/lang/Exception from L74 to L75 using L71
.catch java/lang/Exception from L76 to L77 using L71
.catch java/lang/Exception from L78 to L79 using L71
.catch java/lang/Exception from L80 to L81 using L71
.catch java/lang/Exception from L82 to L83 using L71
.catch java/lang/Exception from L84 to L85 using L71
.catch java/lang/Exception from L86 to L87 using L71
.catch java/lang/Exception from L88 to L89 using L71
.catch java/lang/Exception from L90 to L91 using L71
.catch java/lang/Exception from L92 to L93 using L71
.catch java/lang/Exception from L94 to L95 using L71
.catch java/lang/Exception from L96 to L97 using L71
.catch java/lang/Exception from L98 to L99 using L71
.catch java/lang/Exception from L100 to L101 using L71
.catch java/lang/Exception from L102 to L103 using L71
.catch java/lang/Exception from L104 to L105 using L71
.catch java/lang/Exception from L106 to L107 using L71
.catch java/lang/Exception from L108 to L109 using L71
.catch java/lang/Exception from L110 to L111 using L71
.catch java/lang/Exception from L112 to L113 using L71
.catch java/lang/Exception from L114 to L115 using L71
.catch java/lang/Exception from L116 to L117 using L71
.catch java/lang/Exception from L118 to L119 using L71
.catch java/lang/Exception from L120 to L121 using L71
.catch java/lang/Exception from L122 to L123 using L71
.catch java/lang/Exception from L124 to L125 using L71
.catch java/lang/Exception from L126 to L127 using L71
.catch java/lang/Exception from L128 to L129 using L2
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "scan: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iconst_0
istore 10
iconst_0
istore 8
iconst_0
istore 11
iconst_0
istore 12
iconst_0
istore 6
iconst_0
istore 9
iconst_0
istore 7
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
aload 0
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
pop
iload 2
ifne L130
iload 10
istore 4
aload 0
ifnull L131
iload 10
istore 4
iload 12
istore 5
L0:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L131
L1:
iload 12
istore 5
L3:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
lstore 13
L4:
iload 10
istore 4
lload 13
lconst_0
lcmp
ifeq L131
iload 11
istore 6
L5:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
istore 2
L6:
iload 10
istore 4
iload 2
ifeq L131
iload 8
istore 4
L8:
new java/io/RandomAccessFile
dup
aload 0
ldc "rw"
invokespecial java/io/RandomAccessFile/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual java/io/RandomAccessFile/getChannel()Ljava/nio/channels/FileChannel;
astore 0
L9:
iload 8
istore 4
L11:
aload 0
getstatic java/nio/channels/FileChannel$MapMode/READ_WRITE Ljava/nio/channels/FileChannel$MapMode;
lconst_0
aload 0
invokevirtual java/nio/channels/FileChannel/size()J
l2i
i2l
invokevirtual java/nio/channels/FileChannel/map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
astore 15
L12:
iload 8
istore 4
L13:
aload 15
bipush 56
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L14:
iload 8
istore 4
L15:
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
invokestatic com/chelpus/Utils/convertFourBytesToInt(BBBB)I
istore 6
L16:
iload 8
istore 4
L17:
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
invokestatic com/chelpus/Utils/convertFourBytesToInt(BBBB)I
istore 5
L18:
iload 8
istore 4
L19:
iload 6
iconst_4
idiv
newarray byte
astore 16
L20:
iload 8
istore 4
L21:
aload 15
iload 5
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L22:
iload 8
istore 4
L23:
iload 6
newarray int
astore 16
L24:
iconst_0
istore 5
L132:
iload 5
iload 6
if_icmpge L133
iload 8
istore 4
L25:
aload 16
iload 5
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
invokestatic com/chelpus/Utils/convertFourBytesToInt(BBBB)I
iastore
L26:
iload 5
iconst_1
iadd
istore 5
goto L132
L133:
iload 8
istore 4
L27:
aload 16
arraylength
istore 9
L28:
iconst_0
istore 6
iload 7
istore 5
L134:
iload 6
iload 9
if_icmpge L135
aload 16
iload 6
iaload
istore 10
iload 5
istore 4
L29:
aload 15
iload 10
invokevirtual java/nio/MappedByteBuffer/get(I)B
invokestatic com/chelpus/Utils/convertByteToInt(B)I
istore 11
L30:
iload 5
istore 4
L31:
iload 11
newarray byte
astore 17
L32:
iload 10
iconst_1
iadd
istore 12
iload 5
istore 4
L33:
aload 15
iload 12
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L34:
iconst_0
istore 7
L136:
iload 5
istore 4
L35:
iload 7
aload 17
arraylength
if_icmpge L137
L36:
iload 5
istore 4
L37:
aload 17
iload 7
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
bastore
L38:
iload 7
iconst_1
iadd
istore 7
goto L136
L138:
iload 5
istore 4
L39:
iload 7
aload 1
arraylength
if_icmpge L139
L40:
iload 5
istore 4
iload 5
istore 8
L41:
new java/lang/String
dup
aload 17
invokespecial java/lang/String/<init>([B)V
aload 1
iload 7
aaload
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L140
L42:
iload 5
istore 4
L43:
aload 3
iload 7
aaload
invokevirtual java/lang/String/getBytes()[B
astore 18
L44:
iload 5
istore 4
iload 5
istore 8
L45:
aload 18
arraylength
iload 11
if_icmpgt L140
L46:
iload 5
istore 4
L47:
aload 15
iload 10
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L48:
iload 5
istore 4
L49:
aload 15
aload 18
arraylength
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
L50:
iload 5
istore 4
L51:
aload 15
iload 12
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L52:
iload 5
istore 4
L53:
aload 15
aload 18
invokevirtual java/nio/MappedByteBuffer/put([B)Ljava/nio/ByteBuffer;
pop
L54:
iload 5
istore 4
L55:
aload 15
iconst_0
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
L56:
iload 5
istore 4
L57:
aload 15
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
L58:
iload 5
istore 4
L59:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Replace string:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
iload 7
aaload
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L60:
iload 5
iconst_1
iadd
istore 8
goto L140
L135:
iload 5
istore 4
L61:
aload 0
invokevirtual java/nio/channels/FileChannel/close()V
L62:
iload 5
istore 4
L131:
iload 4
ireturn
L10:
astore 0
iload 4
istore 6
L63:
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L64:
iload 4
ireturn
L7:
astore 0
iload 6
istore 5
L65:
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L66:
iload 6
ireturn
L2:
astore 0
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
invokevirtual java/io/PrintStream/println(Ljava/lang/Object;)V
iload 5
ireturn
L130:
iload 12
istore 5
L67:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
istore 2
L68:
iload 10
istore 4
iload 2
ifeq L131
iload 9
istore 4
L69:
new java/io/RandomAccessFile
dup
aload 0
ldc "rw"
invokespecial java/io/RandomAccessFile/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual java/io/RandomAccessFile/getChannel()Ljava/nio/channels/FileChannel;
astore 0
L70:
iload 9
istore 4
L72:
aload 0
getstatic java/nio/channels/FileChannel$MapMode/READ_WRITE Ljava/nio/channels/FileChannel$MapMode;
lconst_0
aload 0
invokevirtual java/nio/channels/FileChannel/size()J
l2i
i2l
invokevirtual java/nio/channels/FileChannel/map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
astore 15
L73:
iload 9
istore 4
L74:
aload 15
bipush 8
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L75:
iload 9
istore 4
L76:
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
invokestatic com/chelpus/Utils/convertFourBytesToInt(BBBB)I
istore 7
L77:
iload 9
istore 4
L78:
aload 15
iload 7
bipush 56
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L79:
iload 9
istore 4
L80:
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
invokestatic com/chelpus/Utils/convertFourBytesToInt(BBBB)I
istore 8
L81:
iload 9
istore 4
L82:
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
invokestatic com/chelpus/Utils/convertFourBytesToInt(BBBB)I
iload 7
iadd
istore 5
L83:
iload 9
istore 4
L84:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher offset_to_data="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 5
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L85:
iload 9
istore 4
L86:
aload 15
iload 5
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L87:
iload 9
istore 4
L88:
iload 8
newarray int
astore 16
L89:
iconst_0
istore 5
L141:
iload 5
iload 8
if_icmpge L142
iload 9
istore 4
L90:
aload 16
iload 5
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
invokestatic com/chelpus/Utils/convertFourBytesToInt(BBBB)I
iload 7
iadd
iastore
L91:
iload 5
iconst_1
iadd
istore 5
goto L141
L142:
iload 9
istore 4
L92:
aload 16
arraylength
istore 9
L93:
iconst_0
istore 4
iload 6
istore 5
iload 4
istore 6
L143:
iload 6
iload 9
if_icmpge L144
aload 16
iload 6
iaload
istore 10
iload 5
istore 4
L94:
aload 15
iload 10
invokevirtual java/nio/MappedByteBuffer/get(I)B
invokestatic com/chelpus/Utils/convertByteToInt(B)I
istore 11
L95:
iload 5
istore 4
L96:
iload 11
newarray byte
astore 17
L97:
iload 10
iconst_1
iadd
istore 12
iload 5
istore 4
L98:
aload 15
iload 12
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L99:
iconst_0
istore 7
L145:
iload 5
istore 4
L100:
iload 7
aload 17
arraylength
if_icmpge L146
L101:
iload 5
istore 4
L102:
aload 17
iload 7
aload 15
invokevirtual java/nio/MappedByteBuffer/get()B
bastore
L103:
iload 7
iconst_1
iadd
istore 7
goto L145
L147:
iload 5
istore 4
L104:
iload 7
aload 1
arraylength
if_icmpge L148
L105:
iload 5
istore 4
iload 5
istore 8
L106:
new java/lang/String
dup
aload 17
invokespecial java/lang/String/<init>([B)V
aload 1
iload 7
aaload
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L149
L107:
iload 5
istore 4
L108:
aload 3
iload 7
aaload
invokevirtual java/lang/String/getBytes()[B
astore 18
L109:
iload 5
istore 4
iload 5
istore 8
L110:
aload 18
arraylength
iload 11
if_icmpgt L149
L111:
iload 5
istore 4
L112:
aload 15
iload 10
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L113:
iload 5
istore 4
L114:
aload 15
aload 18
arraylength
i2b
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
L115:
iload 5
istore 4
L116:
aload 15
iload 12
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L117:
iload 5
istore 4
L118:
aload 15
aload 18
invokevirtual java/nio/MappedByteBuffer/put([B)Ljava/nio/ByteBuffer;
pop
L119:
iload 5
istore 4
L120:
aload 15
iconst_0
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
L121:
iload 5
istore 4
L122:
aload 15
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
L123:
iload 5
istore 4
L124:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Replace string:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
iload 7
aaload
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L125:
iload 5
iconst_1
iadd
istore 8
goto L149
L144:
iload 5
istore 4
L126:
aload 0
invokevirtual java/nio/channels/FileChannel/close()V
L127:
iload 5
ireturn
L71:
astore 0
iload 4
istore 5
L128:
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L129:
iload 4
ireturn
L137:
iconst_0
istore 7
goto L138
L140:
iload 7
iconst_1
iadd
istore 7
iload 8
istore 5
goto L138
L139:
iload 6
iconst_1
iadd
istore 6
goto L134
L146:
iconst_0
istore 7
goto L147
L149:
iload 7
iconst_1
iadd
istore 7
iload 8
istore 5
goto L147
L148:
iload 6
iconst_1
iadd
istore 6
goto L143
.limit locals 19
.limit stack 6
.end method

.method public static rework(Ljava/lang/String;)Ljava/lang/String;
aload 0
astore 1
aload 0
ldc "52"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L0
aload 0
ldc "52"
ldc "f2"
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 1
L0:
aload 1
astore 0
aload 1
ldc "53"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L1
aload 1
ldc "53"
ldc "f3"
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 0
L1:
aload 0
astore 1
aload 0
ldc "54"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L2
aload 0
ldc "54"
ldc "f4"
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 1
L2:
aload 1
astore 0
aload 1
ldc "55"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L3
aload 1
ldc "55"
ldc "f2"
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 0
L3:
aload 0
astore 1
aload 0
ldc "59"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L4
aload 0
ldc "59"
ldc "f5"
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 1
L4:
aload 1
astore 0
aload 1
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
ldc "5A"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L5
aload 1
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
ldc "5A"
ldc "F6"
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 0
L5:
aload 0
astore 1
aload 0
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
ldc "5B"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L6
aload 0
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
ldc "5B"
ldc "F7"
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 1
L6:
aload 1
astore 0
aload 1
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
ldc "5C"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L7
aload 1
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
ldc "5C"
ldc "F5"
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 0
L7:
aload 0
astore 1
aload 0
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
ldc "5D"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L8
aload 0
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
ldc "5D"
ldc "F5"
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 1
L8:
aload 1
astore 0
aload 1
ldc "74"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L9
aload 1
ldc "74"
ldc "f9"
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 0
L9:
aload 0
astore 1
aload 0
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
ldc "6E"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L10
aload 0
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
ldc "6E"
ldc "F8"
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 1
L10:
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method public static run_all(Ljava/lang/String;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L4 to L5 using L2
.catch java/lang/Exception from L5 to L6 using L2
.catch java/lang/Exception from L7 to L8 using L2
.catch java/lang/Exception from L9 to L10 using L2
.catch java/lang/Exception from L11 to L12 using L2
.catch java/lang/Exception from L13 to L14 using L2
L0:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/su Z
ifeq L5
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/checktools Z
ifne L5
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 4
aload 4
invokevirtual java/util/ArrayList/clear()V
aload 4
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/busybox"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
ldc "/system/bin/failsafe/toolbox"
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifeq L1
aload 4
ldc "/system/bin/failsafe/toolbox"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L1:
ldc ""
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/errorOutput Ljava/lang/String;
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "busybox chmod 777 "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/busybox"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
pop
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/errorOutput Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L3
aload 4
ldc "busybox"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L3:
ldc ""
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/errorOutput Ljava/lang/String;
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "toolbox chmod 777 "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/busybox"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
pop
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/errorOutput Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L11
aload 4
ldc "toolbox"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L4:
aload 4
invokevirtual java/util/ArrayList/size()I
anewarray java/lang/String
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/tools [Ljava/lang/String;
aload 4
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/tools [Ljava/lang/String;
invokevirtual java/util/ArrayList/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
checkcast [Ljava/lang/String;
checkcast [Ljava/lang/String;
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/tools [Ljava/lang/String;
iconst_1
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/checktools Z
L5:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/su Z
ifeq L15
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/tools [Ljava/lang/String;
arraylength
iconst_1
iadd
anewarray java/lang/String
astore 4
L6:
iconst_1
istore 2
aload 4
iconst_0
aload 0
aastore
L7:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/tools [Ljava/lang/String;
astore 5
aload 5
arraylength
istore 3
L8:
iconst_0
istore 1
L16:
iload 1
iload 3
if_icmpge L13
aload 5
iload 1
aaload
astore 6
L9:
aload 4
iload 2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
L10:
iload 2
iconst_1
iadd
istore 2
iload 1
iconst_1
iadd
istore 1
goto L16
L11:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "skip toolbox in tools"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L12:
goto L4
L2:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L15:
return
L13:
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
aload 4
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
pop
L14:
return
.limit locals 7
.limit stack 6
.end method

.method public static transient run_all_no_root([Ljava/lang/String;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L4 to L5 using L2
.catch java/lang/Exception from L5 to L6 using L2
.catch java/lang/Exception from L7 to L8 using L2
.catch java/lang/Exception from L9 to L10 using L2
.catch java/lang/Exception from L11 to L12 using L2
.catch java/lang/Exception from L13 to L14 using L2
.catch java/lang/Exception from L15 to L16 using L2
L0:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/checktools Z
ifne L5
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 4
aload 4
invokevirtual java/util/ArrayList/clear()V
aload 4
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/busybox"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
ldc "/system/bin/failsafe/toolbox"
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifeq L1
aload 4
ldc "/system/bin/failsafe/toolbox"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L1:
ldc ""
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/errorOutput Ljava/lang/String;
iconst_4
anewarray java/lang/String
dup
iconst_0
ldc "busybox"
aastore
dup
iconst_1
ldc "chmod"
aastore
dup
iconst_2
ldc "777"
aastore
dup
iconst_3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/busybox"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/errorOutput Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L9
aload 4
ldc "busybox"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L3:
ldc ""
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/errorOutput Ljava/lang/String;
iconst_4
anewarray java/lang/String
dup
iconst_0
ldc "toolbox"
aastore
dup
iconst_1
ldc "chmod"
aastore
dup
iconst_2
ldc "777"
aastore
dup
iconst_3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/busybox"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/errorOutput Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L11
aload 4
ldc "toolbox"
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L4:
aload 4
invokevirtual java/util/ArrayList/size()I
anewarray java/lang/String
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/tools [Ljava/lang/String;
aload 4
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/tools [Ljava/lang/String;
invokevirtual java/util/ArrayList/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
checkcast [Ljava/lang/String;
checkcast [Ljava/lang/String;
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/tools [Ljava/lang/String;
iconst_1
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/checktools Z
L5:
aload 0
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/tools [Ljava/lang/String;
ifnull L17
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/tools [Ljava/lang/String;
arraylength
ifle L17
aload 0
arraylength
iconst_1
iadd
anewarray java/lang/String
astore 4
L6:
iconst_1
istore 1
L7:
aload 0
arraylength
istore 3
L8:
iconst_0
istore 2
goto L18
L9:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "skip busybox in tools"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L10:
goto L3
L2:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L17:
return
L11:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "skip toolbox in tools"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L12:
goto L4
L13:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/tools [Ljava/lang/String;
astore 0
aload 0
arraylength
istore 2
L14:
iconst_0
istore 1
L19:
iload 1
iload 2
if_icmpge L17
aload 4
iconst_0
aload 0
iload 1
aaload
aastore
L15:
aload 4
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L16:
iload 1
iconst_1
iadd
istore 1
goto L19
L18:
iload 2
iload 3
if_icmpge L13
aload 4
iload 1
aload 0
iload 2
aaload
aastore
iload 1
iconst_1
iadd
istore 1
iload 2
iconst_1
iadd
istore 2
goto L18
.limit locals 5
.limit stack 5
.end method

.method public static saveXposedParamBoolean(Ljava/lang/String;ZZ)Z
.catch org/json/JSONException from L0 to L1 using L2
.catch org/json/JSONException from L1 to L3 using L4
.catch java/io/IOException from L5 to L6 using L7
.catch java/io/IOException from L8 to L9 using L10
L0:
new org/json/JSONObject
dup
new java/io/File
dup
ldc "/data/lp/xposed"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokestatic com/chelpus/Utils/read_from_file(Ljava/io/File;)Ljava/lang/String;
invokespecial org/json/JSONObject/<init>(Ljava/lang/String;)V
astore 3
L1:
aload 3
aload 0
iload 1
invokevirtual org/json/JSONObject/put(Ljava/lang/String;Z)Lorg/json/JSONObject;
pop
L3:
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 3
invokevirtual org/json/JSONObject/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/io/File
dup
ldc "/data/lp/xposed"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
aload 3
invokevirtual org/json/JSONObject/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/save_text_to_file(Ljava/io/File;Ljava/lang/String;)Z
pop
iload 2
ifeq L6
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L11
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
ldc "/data/lp/xposed"
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L5:
new java/io/File
dup
ldc "/data/lp/settings_android_changed"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/createNewFile()Z
pop
new java/io/File
dup
ldc "/data/lp/settings_android_changed"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
iconst_1
iconst_0
invokevirtual java/io/File/setWritable(ZZ)Z
pop
new java/io/File
dup
ldc "/data/lp/settings_changed"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/createNewFile()Z
pop
new java/io/File
dup
ldc "/data/lp/settings_changed"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
iconst_1
iconst_0
invokevirtual java/io/File/setWritable(ZZ)Z
pop
L6:
iconst_1
ireturn
L2:
astore 0
aload 0
invokevirtual org/json/JSONException/printStackTrace()V
iconst_0
ireturn
L4:
astore 0
aload 0
invokevirtual org/json/JSONException/printStackTrace()V
goto L3
L7:
astore 0
aload 0
invokevirtual java/io/IOException/printStackTrace()V
goto L6
L11:
ldc "chmod 777 /data/lp/xposed"
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
L8:
new java/io/File
dup
ldc "/data/lp/settings_android_changed"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/createNewFile()Z
pop
new java/io/File
dup
ldc "/data/lp/settings_android_changed"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
iconst_1
iconst_0
invokevirtual java/io/File/setWritable(ZZ)Z
pop
new java/io/File
dup
ldc "/data/lp/settings_changed"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/createNewFile()Z
pop
new java/io/File
dup
ldc "/data/lp/settings_changed"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
iconst_1
iconst_0
invokevirtual java/io/File/setWritable(ZZ)Z
pop
L9:
goto L6
L10:
astore 0
aload 0
invokevirtual java/io/IOException/printStackTrace()V
goto L6
.limit locals 4
.limit stack 5
.end method

.method public static save_text_to_end_file(Ljava/io/File;Ljava/lang/String;)Z
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
.catch java/lang/Exception from L3 to L4 using L5
L0:
aload 0
invokevirtual java/io/File/exists()Z
ifne L1
aload 0
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/io/File/mkdirs()Z
pop
aload 0
invokevirtual java/io/File/createNewFile()Z
pop
L1:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "...rrunning my app..."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/io/RandomAccessFile
dup
aload 0
ldc "rw"
invokespecial java/io/RandomAccessFile/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 7
L3:
aload 0
invokevirtual java/io/File/length()J
lstore 3
aload 7
aload 0
invokevirtual java/io/File/length()J
invokevirtual java/io/RandomAccessFile/seek(J)V
aload 7
aload 1
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/RandomAccessFile/write([B)V
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "...file length..."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 7
invokevirtual java/io/RandomAccessFile/length()J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 7
invokevirtual java/io/RandomAccessFile/close()V
aload 0
invokevirtual java/io/File/length()J
lstore 5
aload 1
invokevirtual java/lang/String/length()I
istore 2
L4:
lload 5
iload 2
i2l
lload 3
ladd
lcmp
ifeq L6
iconst_0
ireturn
L6:
iconst_1
ireturn
L2:
astore 0
L7:
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
iconst_0
ireturn
L5:
astore 0
goto L7
.limit locals 8
.limit stack 6
.end method

.method public static save_text_to_end_file_from_file(Ljava/io/File;Ljava/io/File;)Z
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L4
.catch java/lang/Exception from L3 to L5 using L6
.catch java/io/IOException from L5 to L7 using L8
.catch java/lang/Exception from L5 to L7 using L6
.catch java/io/IOException from L7 to L9 using L8
.catch java/lang/Exception from L7 to L9 using L6
.catch java/io/IOException from L10 to L11 using L8
.catch java/lang/Exception from L10 to L11 using L6
.catch java/lang/Exception from L12 to L13 using L6
.catch java/io/IOException from L14 to L15 using L8
.catch java/lang/Exception from L14 to L15 using L6
.catch java/lang/Exception from L15 to L16 using L6
L0:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "...rrunning my app..."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/io/RandomAccessFile
dup
aload 0
ldc "rw"
invokespecial java/io/RandomAccessFile/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 3
L1:
new java/io/RandomAccessFile
dup
aload 1
ldc "r"
invokespecial java/io/RandomAccessFile/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 1
L3:
aload 0
invokevirtual java/io/File/length()J
pop2
aload 3
aload 0
invokevirtual java/io/File/length()J
invokevirtual java/io/RandomAccessFile/seek(J)V
L5:
aload 3
ldc "#Lucky Patcher block Ads start#\n"
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/RandomAccessFile/write([B)V
sipush 4096
newarray byte
astore 0
L7:
aload 1
aload 0
invokevirtual java/io/RandomAccessFile/read([B)I
istore 2
L9:
iload 2
ifle L14
L10:
aload 3
aload 0
iconst_0
iload 2
invokevirtual java/io/RandomAccessFile/write([BII)V
L11:
goto L7
L8:
astore 0
L12:
aload 0
invokevirtual java/io/IOException/printStackTrace()V
L13:
iconst_0
ireturn
L14:
aload 3
ldc "#Lucky Patcher block Ads finish#\n\n\n\r\n"
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/RandomAccessFile/write([B)V
aload 1
invokevirtual java/io/RandomAccessFile/close()V
L15:
aload 3
invokevirtual java/io/RandomAccessFile/close()V
L16:
iconst_1
ireturn
L2:
astore 0
L17:
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
iconst_0
ireturn
L4:
astore 0
goto L17
L6:
astore 0
goto L17
.limit locals 4
.limit stack 4
.end method

.method public static save_text_to_file(Ljava/io/File;Ljava/lang/String;)Z
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
.catch java/lang/Exception from L3 to L4 using L5
L0:
aload 0
invokevirtual java/io/File/exists()Z
ifne L1
aload 0
invokestatic com/chelpus/Utils/getDirs(Ljava/io/File;)Ljava/io/File;
invokevirtual java/io/File/mkdirs()Z
pop
aload 0
invokevirtual java/io/File/createNewFile()Z
pop
L1:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "...rrunning my app..."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
new java/io/RandomAccessFile
dup
aload 0
ldc "rw"
invokespecial java/io/RandomAccessFile/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 5
L3:
aload 5
lconst_0
invokevirtual java/io/RandomAccessFile/setLength(J)V
aload 5
lconst_0
invokevirtual java/io/RandomAccessFile/seek(J)V
aload 5
aload 1
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/RandomAccessFile/write([B)V
aload 5
invokevirtual java/io/RandomAccessFile/close()V
aload 0
invokevirtual java/io/File/length()J
lstore 3
aload 1
invokevirtual java/lang/String/length()I
istore 2
L4:
lload 3
iload 2
i2l
lcmp
ifeq L6
iconst_0
ireturn
L6:
iconst_1
ireturn
L2:
astore 0
L7:
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
iconst_0
ireturn
L5:
astore 0
goto L7
.limit locals 6
.limit stack 4
.end method

.method public static sendFromRoot(Ljava/lang/String;)Z
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public static sendFromRootCP(Ljava/lang/String;)Z
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public static final setIcon(I)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
.catch java/lang/Exception from L6 to L7 using L8
.catch java/lang/Exception from L9 to L10 using L11
.catch java/lang/Exception from L12 to L13 using L14
.catch java/lang/Exception from L15 to L16 using L17
iload 0
tableswitch 0
L18
L19
L20
L21
L22
L23
default : L24
L24:
return
L18:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-Original"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_1
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-Flint"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_2
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-2"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_2
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-3"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_2
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-4"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_2
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-5"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_2
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
L0:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/frag Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
ifnull L24
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/api I
bipush 10
if_icmple L24
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/frag Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getActivity()Landroid/support/v4/app/FragmentActivity;
invokevirtual android/support/v4/app/FragmentActivity/getActionBar()Landroid/app/ActionBar;
ldc_w 2130837551
invokevirtual android/app/ActionBar/setIcon(I)V
L1:
return
L2:
astore 1
return
L19:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-Original"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_2
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-Flint"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_1
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-2"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_2
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-3"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_2
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-4"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_2
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-5"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_2
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
L3:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/frag Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
ifnull L24
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/api I
bipush 10
if_icmple L24
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/frag Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getActivity()Landroid/support/v4/app/FragmentActivity;
invokevirtual android/support/v4/app/FragmentActivity/getActionBar()Landroid/app/ActionBar;
ldc_w 2130903046
invokevirtual android/app/ActionBar/setIcon(I)V
L4:
return
L5:
astore 1
return
L20:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-Original"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_2
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-Flint"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_2
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-2"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_1
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-3"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_2
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-4"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_2
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-5"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_2
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
L6:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/frag Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
ifnull L24
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/api I
bipush 10
if_icmple L24
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/frag Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getActivity()Landroid/support/v4/app/FragmentActivity;
invokevirtual android/support/v4/app/FragmentActivity/getActionBar()Landroid/app/ActionBar;
ldc_w 2130903047
invokevirtual android/app/ActionBar/setIcon(I)V
L7:
return
L8:
astore 1
return
L21:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-Original"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_2
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-Flint"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_2
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-2"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_2
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-3"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_1
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-4"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_2
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-5"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_2
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
L9:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/frag Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
ifnull L24
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/api I
bipush 10
if_icmple L24
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/frag Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getActivity()Landroid/support/v4/app/FragmentActivity;
invokevirtual android/support/v4/app/FragmentActivity/getActionBar()Landroid/app/ActionBar;
ldc_w 2130903048
invokevirtual android/app/ActionBar/setIcon(I)V
L10:
return
L11:
astore 1
return
L22:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-Original"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_2
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-Flint"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_2
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-2"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_2
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-3"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_2
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-4"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_1
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-5"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_2
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
L12:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/frag Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
ifnull L24
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/api I
bipush 10
if_icmple L24
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/frag Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getActivity()Landroid/support/v4/app/FragmentActivity;
invokevirtual android/support/v4/app/FragmentActivity/getActionBar()Landroid/app/ActionBar;
ldc_w 2130903049
invokevirtual android/app/ActionBar/setIcon(I)V
L13:
return
L14:
astore 1
return
L23:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-Original"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_2
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-Flint"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_2
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-2"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_2
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-3"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_2
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-4"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_2
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getPkgMng()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "com.android.vending.billing.InAppBillingService.LUCK.MainActivity-5"
invokespecial android/content/ComponentName/<init>(Landroid/content/Context;Ljava/lang/String;)V
iconst_1
iconst_1
invokevirtual android/content/pm/PackageManager/setComponentEnabledSetting(Landroid/content/ComponentName;II)V
L15:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/frag Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
ifnull L24
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/api I
bipush 10
if_icmple L24
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/frag Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getActivity()Landroid/support/v4/app/FragmentActivity;
invokevirtual android/support/v4/app/FragmentActivity/getActionBar()Landroid/app/ActionBar;
ldc_w 2130903050
invokevirtual android/app/ActionBar/setIcon(I)V
L16:
return
L17:
astore 1
return
.limit locals 2
.limit stack 5
.end method

.method public static setPermissionDir(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L0
aload 1
getstatic java/io/File/separator Ljava/lang/String;
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 5
ldc "/"
astore 4
iconst_0
istore 3
L1:
iload 3
aload 5
arraylength
if_icmpge L0
aload 4
astore 1
aload 5
iload 3
aaload
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
iload 3
aaload
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
L2:
aload 1
aload 0
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifne L3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 0
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L4
L3:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
aload 2
aastore
dup
iconst_2
aload 1
aastore
invokestatic com/chelpus/Utils/cmdParam([Ljava/lang/String;)Ljava/lang/String;
pop
L4:
aload 1
astore 4
aload 5
iload 3
aaload
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L5
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 4
L5:
iload 3
iconst_1
iadd
istore 3
goto L1
L0:
return
.limit locals 6
.limit stack 4
.end method

.method public static setStringIds(Ljava/lang/String;[[BZB)I
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L7
.catch java/lang/Exception from L8 to L9 using L10
.catch java/lang/Exception from L11 to L12 using L10
.catch java/lang/Exception from L13 to L14 using L10
.catch java/lang/Exception from L15 to L16 using L10
.catch java/lang/Exception from L17 to L18 using L10
.catch java/lang/Exception from L19 to L20 using L10
.catch java/lang/Exception from L21 to L22 using L10
.catch java/lang/Exception from L23 to L24 using L10
.catch java/lang/Exception from L25 to L26 using L10
.catch java/lang/Exception from L27 to L28 using L10
.catch java/lang/Exception from L29 to L30 using L10
.catch java/lang/Exception from L31 to L32 using L10
.catch java/lang/Exception from L33 to L34 using L10
.catch java/lang/Exception from L35 to L36 using L10
.catch java/lang/Exception from L37 to L38 using L10
.catch java/lang/Exception from L39 to L40 using L10
.catch java/lang/Exception from L41 to L42 using L10
.catch java/lang/Exception from L43 to L44 using L10
.catch java/lang/Exception from L45 to L46 using L10
.catch java/lang/Exception from L47 to L48 using L10
.catch java/lang/Exception from L49 to L50 using L10
.catch java/lang/Exception from L51 to L52 using L10
.catch java/lang/Exception from L53 to L54 using L10
.catch java/lang/Exception from L55 to L56 using L10
.catch java/lang/Exception from L57 to L58 using L10
.catch java/lang/Exception from L59 to L60 using L10
.catch java/lang/Exception from L61 to L62 using L10
.catch java/lang/Exception from L63 to L64 using L10
.catch java/lang/Exception from L65 to L66 using L7
.catch java/lang/Exception from L67 to L68 using L2
.catch java/lang/Exception from L69 to L70 using L2
.catch java/lang/Exception from L71 to L72 using L73
.catch java/lang/Exception from L74 to L75 using L73
.catch java/lang/Exception from L76 to L77 using L73
.catch java/lang/Exception from L78 to L79 using L73
.catch java/lang/Exception from L80 to L81 using L73
.catch java/lang/Exception from L82 to L83 using L73
.catch java/lang/Exception from L84 to L85 using L73
.catch java/lang/Exception from L86 to L87 using L73
.catch java/lang/Exception from L88 to L89 using L73
.catch java/lang/Exception from L90 to L91 using L73
.catch java/lang/Exception from L92 to L93 using L73
.catch java/lang/Exception from L94 to L95 using L73
.catch java/lang/Exception from L96 to L97 using L73
.catch java/lang/Exception from L98 to L99 using L73
.catch java/lang/Exception from L100 to L101 using L73
.catch java/lang/Exception from L102 to L103 using L73
.catch java/lang/Exception from L104 to L105 using L73
.catch java/lang/Exception from L106 to L107 using L73
.catch java/lang/Exception from L108 to L109 using L73
.catch java/lang/Exception from L110 to L111 using L73
.catch java/lang/Exception from L112 to L113 using L73
.catch java/lang/Exception from L114 to L115 using L73
.catch java/lang/Exception from L116 to L117 using L73
.catch java/lang/Exception from L118 to L119 using L73
.catch java/lang/Exception from L120 to L121 using L73
.catch java/lang/Exception from L122 to L123 using L73
.catch java/lang/Exception from L124 to L125 using L73
.catch java/lang/Exception from L126 to L127 using L73
.catch java/lang/Exception from L128 to L129 using L73
.catch java/lang/Exception from L130 to L131 using L73
.catch java/lang/Exception from L132 to L133 using L2
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "scan: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iconst_0
istore 10
iconst_0
istore 8
iconst_0
istore 11
iconst_0
istore 12
iconst_0
istore 6
iconst_0
istore 9
iconst_0
istore 7
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
aload 0
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
pop
iload 2
ifne L134
iload 10
istore 4
aload 0
ifnull L135
iload 10
istore 4
iload 12
istore 5
L0:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L135
L1:
iload 12
istore 5
L3:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
lstore 14
L4:
iload 10
istore 4
lload 14
lconst_0
lcmp
ifeq L135
iload 11
istore 6
L5:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
istore 2
L6:
iload 10
istore 4
iload 2
ifeq L135
iload 8
istore 4
L8:
new java/io/RandomAccessFile
dup
aload 0
ldc "rw"
invokespecial java/io/RandomAccessFile/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual java/io/RandomAccessFile/getChannel()Ljava/nio/channels/FileChannel;
astore 0
L9:
iload 8
istore 4
L11:
aload 0
getstatic java/nio/channels/FileChannel$MapMode/READ_WRITE Ljava/nio/channels/FileChannel$MapMode;
lconst_0
aload 0
invokevirtual java/nio/channels/FileChannel/size()J
l2i
i2l
invokevirtual java/nio/channels/FileChannel/map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
astore 16
L12:
iload 8
istore 4
L13:
aload 16
bipush 56
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L14:
iload 8
istore 4
L15:
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
invokestatic com/chelpus/Utils/convertFourBytesToInt(BBBB)I
istore 6
L16:
iload 8
istore 4
L17:
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
invokestatic com/chelpus/Utils/convertFourBytesToInt(BBBB)I
istore 5
L18:
iload 8
istore 4
L19:
iload 6
iconst_4
idiv
newarray byte
astore 17
L20:
iload 8
istore 4
L21:
aload 16
iload 5
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L22:
iload 8
istore 4
L23:
iload 6
newarray int
astore 17
L24:
iconst_0
istore 5
L136:
iload 5
iload 6
if_icmpge L137
iload 8
istore 4
L25:
aload 17
iload 5
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
invokestatic com/chelpus/Utils/convertFourBytesToInt(BBBB)I
iastore
L26:
iload 5
iconst_1
iadd
istore 5
goto L136
L137:
iload 8
istore 4
L27:
aload 17
arraylength
istore 11
L28:
iconst_0
istore 6
iload 7
istore 5
L138:
iload 6
iload 11
if_icmpge L139
aload 17
iload 6
iaload
istore 7
iload 5
istore 4
L29:
aload 16
iload 7
invokevirtual java/nio/MappedByteBuffer/get(I)B
invokestatic com/chelpus/Utils/convertByteToInt(B)I
istore 12
L30:
iload 5
istore 4
L31:
iload 12
newarray byte
astore 18
L32:
iload 7
iconst_1
iadd
istore 13
iload 5
istore 4
L33:
aload 16
iload 13
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L34:
iconst_0
istore 7
L140:
iload 5
istore 4
L35:
iload 7
aload 18
arraylength
if_icmpge L141
L36:
iload 5
istore 4
L37:
aload 18
iload 7
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
bastore
L38:
iload 7
iconst_1
iadd
istore 7
goto L140
L142:
iload 5
istore 4
L39:
iload 7
aload 1
arraylength
if_icmpge L143
L40:
iload 5
istore 4
L41:
aload 1
iload 7
invokestatic java/lang/reflect/Array/get(Ljava/lang/Object;I)Ljava/lang/Object;
checkcast [B
checkcast [B
astore 19
L42:
iload 5
istore 4
iload 5
istore 9
L43:
aload 19
arraylength
iload 12
if_icmpgt L144
L44:
iload 5
istore 4
iload 5
istore 9
L45:
aload 19
arraylength
iconst_2
if_icmple L144
L46:
iconst_0
istore 8
L145:
iload 5
istore 4
iload 5
istore 9
L47:
iload 8
aload 18
arraylength
if_icmpge L144
L48:
iload 5
istore 10
aload 18
iload 8
baload
aload 19
iconst_0
baload
if_icmpne L146
iconst_1
istore 9
L147:
iload 5
istore 4
iload 5
istore 10
L49:
iload 9
aload 19
arraylength
if_icmpge L146
L50:
iload 5
istore 4
iload 5
istore 10
L51:
iload 8
iload 9
iadd
aload 18
arraylength
if_icmpge L146
L52:
iload 5
istore 10
aload 18
iload 8
iload 9
iadd
baload
aload 19
iload 9
baload
if_icmpne L146
iload 9
iconst_1
iadd
istore 10
iload 10
istore 9
iload 5
istore 4
L53:
iload 10
aload 19
arraylength
if_icmpne L147
L54:
iload 5
istore 4
L55:
aload 19
arraylength
iconst_2
if_icmple L60
L56:
iload 5
istore 4
L57:
aload 16
iload 13
iload 8
iadd
aload 19
arraylength
iconst_1
isub
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L58:
iload 5
istore 4
L59:
aload 16
iload 3
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
L60:
iload 5
istore 4
L61:
aload 16
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
L62:
iload 5
iconst_1
iadd
istore 5
iload 10
istore 9
goto L147
L139:
iload 5
istore 4
L63:
aload 0
invokevirtual java/nio/channels/FileChannel/close()V
L64:
iload 5
istore 4
L135:
iload 4
ireturn
L10:
astore 0
iload 4
istore 6
L65:
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L66:
iload 4
ireturn
L7:
astore 0
iload 6
istore 5
L67:
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L68:
iload 6
ireturn
L2:
astore 0
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
invokevirtual java/io/PrintStream/println(Ljava/lang/Object;)V
iload 5
ireturn
L134:
iload 12
istore 5
L69:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
istore 2
L70:
iload 10
istore 4
iload 2
ifeq L135
iload 9
istore 4
L71:
new java/io/RandomAccessFile
dup
aload 0
ldc "rw"
invokespecial java/io/RandomAccessFile/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual java/io/RandomAccessFile/getChannel()Ljava/nio/channels/FileChannel;
astore 0
L72:
iload 9
istore 4
L74:
aload 0
getstatic java/nio/channels/FileChannel$MapMode/READ_WRITE Ljava/nio/channels/FileChannel$MapMode;
lconst_0
aload 0
invokevirtual java/nio/channels/FileChannel/size()J
l2i
i2l
invokevirtual java/nio/channels/FileChannel/map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
astore 16
L75:
iload 9
istore 4
L76:
aload 16
bipush 8
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L77:
iload 9
istore 4
L78:
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
invokestatic com/chelpus/Utils/convertFourBytesToInt(BBBB)I
istore 7
L79:
iload 9
istore 4
L80:
aload 16
iload 7
bipush 56
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L81:
iload 9
istore 4
L82:
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
invokestatic com/chelpus/Utils/convertFourBytesToInt(BBBB)I
istore 8
L83:
iload 9
istore 4
L84:
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
invokestatic com/chelpus/Utils/convertFourBytesToInt(BBBB)I
iload 7
iadd
istore 5
L85:
iload 9
istore 4
L86:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher offset_to_data="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 5
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L87:
iload 9
istore 4
L88:
aload 16
iload 5
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L89:
iload 9
istore 4
L90:
iload 8
newarray int
astore 17
L91:
iconst_0
istore 5
L148:
iload 5
iload 8
if_icmpge L149
iload 9
istore 4
L92:
aload 17
iload 5
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
invokestatic com/chelpus/Utils/convertFourBytesToInt(BBBB)I
iload 7
iadd
iastore
L93:
iload 5
iconst_1
iadd
istore 5
goto L148
L149:
iload 9
istore 4
L94:
aload 17
arraylength
istore 11
L95:
iconst_0
istore 4
iload 6
istore 5
iload 4
istore 6
L150:
iload 6
iload 11
if_icmpge L151
aload 17
iload 6
iaload
istore 7
iload 5
istore 4
L96:
aload 16
iload 7
invokevirtual java/nio/MappedByteBuffer/get(I)B
invokestatic com/chelpus/Utils/convertByteToInt(B)I
istore 12
L97:
iload 5
istore 4
L98:
iload 12
newarray byte
astore 18
L99:
iload 7
iconst_1
iadd
istore 13
iload 5
istore 4
L100:
aload 16
iload 13
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L101:
iconst_0
istore 7
L152:
iload 5
istore 4
L102:
iload 7
aload 18
arraylength
if_icmpge L153
L103:
iload 5
istore 4
L104:
aload 18
iload 7
aload 16
invokevirtual java/nio/MappedByteBuffer/get()B
bastore
L105:
iload 7
iconst_1
iadd
istore 7
goto L152
L154:
iload 5
istore 4
L106:
iload 7
aload 1
arraylength
if_icmpge L155
L107:
iload 5
istore 4
L108:
aload 1
iload 7
invokestatic java/lang/reflect/Array/get(Ljava/lang/Object;I)Ljava/lang/Object;
checkcast [B
checkcast [B
astore 19
L109:
iload 5
istore 4
iload 5
istore 9
L110:
aload 19
arraylength
iload 12
if_icmpge L156
L111:
iload 5
istore 4
iload 5
istore 9
L112:
aload 19
arraylength
iconst_2
if_icmple L156
L113:
iconst_0
istore 8
L157:
iload 5
istore 4
iload 5
istore 9
L114:
iload 8
aload 18
arraylength
if_icmpge L156
L115:
iload 5
istore 10
aload 18
iload 8
baload
aload 19
iconst_0
baload
if_icmpne L158
iconst_1
istore 9
L159:
iload 5
istore 4
iload 5
istore 10
L116:
iload 9
aload 19
arraylength
if_icmpge L158
L117:
iload 5
istore 4
iload 5
istore 10
L118:
iload 8
iload 9
iadd
aload 18
arraylength
if_icmpge L158
L119:
iload 5
istore 10
aload 18
iload 8
iload 9
iadd
baload
aload 19
iload 9
baload
if_icmpne L158
iload 9
iconst_1
iadd
istore 10
iload 10
istore 9
iload 5
istore 4
L120:
iload 10
aload 19
arraylength
if_icmpne L159
L121:
iload 5
istore 4
L122:
aload 19
arraylength
iconst_2
if_icmple L127
L123:
iload 5
istore 4
L124:
aload 16
iload 13
iload 8
iadd
aload 19
arraylength
iconst_1
isub
iadd
invokevirtual java/nio/MappedByteBuffer/position(I)Ljava/nio/Buffer;
pop
L125:
iload 5
istore 4
L126:
aload 16
iload 3
invokevirtual java/nio/MappedByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
L127:
iload 5
istore 4
L128:
aload 16
invokevirtual java/nio/MappedByteBuffer/force()Ljava/nio/MappedByteBuffer;
pop
L129:
iload 5
iconst_1
iadd
istore 5
iload 10
istore 9
goto L159
L151:
iload 5
istore 4
L130:
aload 0
invokevirtual java/nio/channels/FileChannel/close()V
L131:
iload 5
ireturn
L73:
astore 0
iload 4
istore 5
L132:
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L133:
iload 4
ireturn
L141:
iconst_0
istore 7
goto L142
L146:
iload 8
iconst_1
iadd
istore 8
iload 10
istore 5
goto L145
L144:
iload 7
iconst_1
iadd
istore 7
iload 9
istore 5
goto L142
L143:
iload 6
iconst_1
iadd
istore 6
goto L138
L153:
iconst_0
istore 7
goto L154
L158:
iload 8
iconst_1
iadd
istore 8
iload 10
istore 5
goto L157
L156:
iload 7
iconst_1
iadd
istore 7
iload 9
istore 5
goto L154
L155:
iload 6
iconst_1
iadd
istore 6
goto L150
.limit locals 20
.limit stack 6
.end method

.method public static setTitle(Landroid/app/AlertDialog$Builder;ILjava/lang/String;)V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/frag Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getContext()Landroid/support/v4/app/FragmentActivity;
ldc_w 2130968610
aconst_null
invokestatic android/view/View/inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;
checkcast android/widget/LinearLayout
astore 3
aload 3
ldc_w 2131558434
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
astore 4
aload 3
ldc_w 2131558433
invokevirtual android/widget/LinearLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/ImageView
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getRes()Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/ImageView/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
aload 4
aload 2
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
aload 3
invokevirtual android/app/AlertDialog$Builder/setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;
pop
return
.limit locals 5
.limit stack 3
.end method

.method public static sha1withrsa_sign(Ljava/lang/String;)Ljava/lang/String;
.catch java/security/NoSuchAlgorithmException from L0 to L1 using L2
.catch java/security/SignatureException from L0 to L1 using L3
.catch java/security/InvalidKeyException from L0 to L1 using L4
.catch java/security/NoSuchAlgorithmException from L5 to L6 using L2
.catch java/security/SignatureException from L5 to L6 using L3
.catch java/security/InvalidKeyException from L5 to L6 using L4
.catch java/security/NoSuchAlgorithmException from L7 to L8 using L2
.catch java/security/SignatureException from L7 to L8 using L3
.catch java/security/InvalidKeyException from L7 to L8 using L4
.catch java/security/NoSuchAlgorithmException from L9 to L10 using L2
.catch java/security/SignatureException from L9 to L10 using L3
.catch java/security/InvalidKeyException from L9 to L10 using L4
.catch java/security/NoSuchAlgorithmException from L11 to L12 using L2
.catch java/security/SignatureException from L11 to L12 using L3
.catch java/security/InvalidKeyException from L11 to L12 using L4
.catch java/security/NoSuchAlgorithmException from L13 to L14 using L2
.catch java/security/SignatureException from L13 to L14 using L3
.catch java/security/InvalidKeyException from L13 to L14 using L4
.catch java/security/NoSuchAlgorithmException from L15 to L16 using L2
.catch java/security/SignatureException from L15 to L16 using L3
.catch java/security/InvalidKeyException from L15 to L16 using L4
.catch java/security/NoSuchAlgorithmException from L17 to L18 using L2
.catch java/security/SignatureException from L17 to L18 using L3
.catch java/security/InvalidKeyException from L17 to L18 using L4
ldc ""
astore 1
aload 1
astore 2
aload 1
astore 3
aload 1
astore 4
L0:
ldc "RSA"
invokestatic java/security/KeyPairGenerator/getInstance(Ljava/lang/String;)Ljava/security/KeyPairGenerator;
invokevirtual java/security/KeyPairGenerator/generateKeyPair()Ljava/security/KeyPair;
invokevirtual java/security/KeyPair/getPrivate()Ljava/security/PrivateKey;
astore 5
L1:
aload 1
astore 2
aload 1
astore 3
aload 1
astore 4
L5:
ldc "SHA1withRSA"
invokestatic java/security/Signature/getInstance(Ljava/lang/String;)Ljava/security/Signature;
astore 6
L6:
aload 1
astore 2
aload 1
astore 3
aload 1
astore 4
L7:
aload 6
aload 5
invokevirtual java/security/Signature/initSign(Ljava/security/PrivateKey;)V
L8:
aload 1
astore 2
aload 1
astore 3
aload 1
astore 4
L9:
aload 6
aload 0
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/security/Signature/update([B)V
L10:
aload 1
astore 2
aload 1
astore 3
aload 1
astore 4
L11:
aload 6
invokevirtual java/security/Signature/sign()[B
astore 5
L12:
aload 1
astore 2
aload 1
astore 3
aload 1
astore 4
L13:
aload 5
invokestatic com/google/android/finsky/billing/iab/google/util/Base64/encode([B)Ljava/lang/String;
astore 0
L14:
aload 0
astore 2
aload 0
astore 3
aload 0
astore 4
L15:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "b64: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokestatic com/google/android/finsky/billing/iab/google/util/Base64/encode([B)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L16:
aload 0
astore 2
aload 0
astore 3
aload 0
astore 4
L17:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Signature: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
new java/lang/String
dup
aload 5
invokespecial java/lang/String/<init>([B)V
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L18:
aload 0
areturn
L2:
astore 0
aload 0
invokevirtual java/security/NoSuchAlgorithmException/printStackTrace()V
aload 2
areturn
L3:
astore 0
aload 0
invokevirtual java/security/SignatureException/printStackTrace()V
aload 3
areturn
L4:
astore 0
aload 0
invokevirtual java/security/InvalidKeyException/printStackTrace()V
aload 4
areturn
.limit locals 7
.limit stack 5
.end method

.method public static showDialog(Landroid/app/Dialog;)V
.catch java/lang/Exception from L0 to L1 using L2
L0:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/patchAct Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;
ifnull L1
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/patchAct Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/patchActivity/isFinishing()Z
ifne L1
aload 0
invokevirtual android/app/Dialog/show()V
L1:
return
L2:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
return
.limit locals 1
.limit stack 1
.end method

.method public static showDialogCustomYes(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V
.catch java/lang/Exception from L0 to L1 using L2
L0:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/patchAct Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;
ifnull L1
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/patchAct Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/patchActivity/isFinishing()Z
ifne L1
new com/android/vending/billing/InAppBillingService/LUCK/AlertDlg
dup
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/frag Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getContext()Landroid/support/v4/app/FragmentActivity;
invokespecial com/android/vending/billing/InAppBillingService/LUCK/AlertDlg/<init>(Landroid/content/Context;)V
aload 0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/AlertDlg/setTitle(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
aload 1
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/AlertDlg/setMessage(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
ldc_w 2130837554
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/AlertDlg/setIcon(I)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
aload 2
aload 3
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/AlertDlg/setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
ldc_w 2131165563
invokestatic com/chelpus/Utils/getText(I)Ljava/lang/String;
aload 4
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/AlertDlg/setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
aload 5
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/AlertDlg/setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/AlertDlg/create()Landroid/app/Dialog;
astore 0
aload 0
invokestatic com/chelpus/Utils/showDialog(Landroid/app/Dialog;)V
aload 0
ldc_w 16908299
invokevirtual android/app/Dialog/findViewById(I)Landroid/view/View;
pop
L1:
return
L2:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
return
.limit locals 6
.limit stack 3
.end method

.method public static showDialogCustomYesNo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V
.catch java/lang/Exception from L0 to L1 using L2
L0:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/patchAct Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;
ifnull L1
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/patchAct Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/patchActivity/isFinishing()Z
ifne L1
new com/android/vending/billing/InAppBillingService/LUCK/AlertDlg
dup
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/frag Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getContext()Landroid/support/v4/app/FragmentActivity;
invokespecial com/android/vending/billing/InAppBillingService/LUCK/AlertDlg/<init>(Landroid/content/Context;)V
aload 0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/AlertDlg/setTitle(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
aload 1
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/AlertDlg/setMessage(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
ldc_w 2130837554
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/AlertDlg/setIcon(I)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
aload 2
aload 3
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/AlertDlg/setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
aload 4
aload 5
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/AlertDlg/setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
aload 6
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/AlertDlg/setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/AlertDlg/create()Landroid/app/Dialog;
astore 0
aload 0
invokestatic com/chelpus/Utils/showDialog(Landroid/app/Dialog;)V
aload 0
ldc_w 16908299
invokevirtual android/app/Dialog/findViewById(I)Landroid/view/View;
pop
L1:
return
L2:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
return
.limit locals 7
.limit stack 3
.end method

.method public static showDialogYesNo(Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V
.catch java/lang/Exception from L0 to L1 using L2
L0:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/patchAct Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;
ifnull L1
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/patchAct Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/patchActivity/isFinishing()Z
ifne L1
new com/android/vending/billing/InAppBillingService/LUCK/AlertDlg
dup
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/frag Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getContext()Landroid/support/v4/app/FragmentActivity;
invokespecial com/android/vending/billing/InAppBillingService/LUCK/AlertDlg/<init>(Landroid/content/Context;)V
aload 0
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/AlertDlg/setTitle(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
aload 1
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/AlertDlg/setMessage(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
ldc_w 2130837554
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/AlertDlg/setIcon(I)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
ldc_w 2131165187
invokestatic com/chelpus/Utils/getText(I)Ljava/lang/String;
aload 2
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/AlertDlg/setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
ldc_w 2131165563
invokestatic com/chelpus/Utils/getText(I)Ljava/lang/String;
aload 3
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/AlertDlg/setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
aload 4
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/AlertDlg/setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/AlertDlg/create()Landroid/app/Dialog;
astore 0
aload 0
invokestatic com/chelpus/Utils/showDialog(Landroid/app/Dialog;)V
aload 0
ldc_w 16908299
invokevirtual android/app/Dialog/findViewById(I)Landroid/view/View;
pop
L1:
return
L2:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
return
.limit locals 5
.limit stack 3
.end method

.method public static showMessage(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V
aload 0
new com/chelpus/Utils$1
dup
aload 0
aload 1
aload 2
invokespecial com/chelpus/Utils$1/<init>(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/app/Activity/runOnUiThread(Ljava/lang/Runnable;)V
return
.limit locals 3
.limit stack 6
.end method

.method public static showSystemWindow(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc "window"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/view/WindowManager
astore 4
new android/view/WindowManager$LayoutParams
dup
invokespecial android/view/WindowManager$LayoutParams/<init>()V
astore 5
aload 5
bipush 17
putfield android/view/WindowManager$LayoutParams/gravity I
aload 5
sipush 2003
putfield android/view/WindowManager$LayoutParams/type I
aload 5
bipush -2
putfield android/view/WindowManager$LayoutParams/width I
aload 5
bipush -2
putfield android/view/WindowManager$LayoutParams/height I
aload 5
fconst_1
putfield android/view/WindowManager$LayoutParams/alpha F
aload 5
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
putfield android/view/WindowManager$LayoutParams/packageName Ljava/lang/String;
aload 5
fconst_1
putfield android/view/WindowManager$LayoutParams/buttonBrightness F
aload 5
ldc_w 16973826
putfield android/view/WindowManager$LayoutParams/windowAnimations I
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getInstance()Landroid/content/Context;
ldc_w 2130968634
aconst_null
invokestatic android/view/View/inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;
astore 6
aload 6
ldc_w 2131558407
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
astore 7
aload 6
ldc_w 2131558408
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
astore 8
aload 6
ldc_w 2131558624
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
astore 9
aload 6
ldc_w 2131558481
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
aload 0
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 9
aload 1
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 7
aload 2
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 8
aload 3
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 7
new com/chelpus/Utils$11
dup
aload 4
invokespecial com/chelpus/Utils$11/<init>(Landroid/view/WindowManager;)V
invokevirtual android/widget/Button/setOnKeyListener(Landroid/view/View$OnKeyListener;)V
aload 4
aload 6
aload 5
invokeinterface android/view/WindowManager/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V 2
return
.limit locals 10
.limit stack 4
.end method

.method public static startRootJava()V
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "SU Java-Code Running!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
ldc ""
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
new java/io/File
dup
ldc "/data/lp"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
pop
new java/io/File
dup
ldc "/data/lp/lp_utils"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
aload 1
invokevirtual java/io/File/exists()Z
ifne L0
L1:
return
L0:
aload 1
invokestatic com/chelpus/Utils/read_from_file(Ljava/io/File;)Ljava/lang/String;
ldc "%chelpus%"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 1
aload 1
ifnull L1
aload 1
arraylength
ifle L1
iconst_0
istore 0
L2:
iload 0
aload 1
arraylength
if_icmpge L1
iload 0
tableswitch 0
L3
L4
L5
default : L6
L6:
iload 0
iconst_1
iadd
istore 0
goto L2
L3:
aload 1
iload 0
aaload
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
goto L6
L4:
aload 1
iload 0
aaload
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/api I
L5:
aload 1
iload 0
aaload
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/runtime Ljava/lang/String;
goto L6
.limit locals 2
.limit stack 3
.end method

.method public static startRootJava(Ljava/lang/Object;)V
aload 0
ifnull L0
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "SU Java-Code Running! "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getEnclosingClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L0:
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
ldc ""
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
new java/io/File
dup
ldc "/data/lp"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
pop
new java/io/File
dup
ldc "/data/lp/lp_utils"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 2
aload 2
invokevirtual java/io/File/exists()Z
ifne L1
aload 0
ifnull L2
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Lucky Patcher not found utils."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L2:
return
L1:
aload 2
invokestatic com/chelpus/Utils/read_from_file(Ljava/io/File;)Ljava/lang/String;
ldc "%chelpus%"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 2
aload 2
ifnull L3
aload 2
arraylength
ifle L3
iconst_0
istore 1
L4:
iload 1
aload 2
arraylength
if_icmpge L3
iload 1
tableswitch 0
L5
L6
L7
default : L8
L8:
iload 1
iconst_1
iadd
istore 1
goto L4
L5:
aload 2
iload 1
aaload
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
goto L8
L6:
aload 2
iload 1
aaload
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/api I
L7:
aload 2
iload 1
aaload
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/runtime Ljava/lang/String;
goto L8
L3:
aload 0
ifnull L9
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "tools read:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/busybox"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L9:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/busybox"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L2
aload 0
ifnull L10
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Lucky AppManager found utils."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
return
L10:
aload 0
ifnull L2
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Lucky AppManager not found busybox util."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
return
.limit locals 3
.limit stack 4
.end method

.method public static final turn_off_patch_on_boot(Ljava/lang/String;)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getConfig()Landroid/content/SharedPreferences;
ldc "patch_dalvik_on_boot_patterns"
ldc ""
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
astore 1
aload 0
ldc "patch1"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L0
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getConfig()Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "patch_dalvik_on_boot_patterns"
aload 1
ldc "patch1"
ldc ""
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokeinterface android/content/SharedPreferences$Editor/putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
L0:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getConfig()Landroid/content/SharedPreferences;
ldc "patch_dalvik_on_boot_patterns"
ldc ""
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
astore 1
aload 0
ldc "patch2"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L1
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getConfig()Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "patch_dalvik_on_boot_patterns"
aload 1
ldc "patch2"
ldc ""
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokeinterface android/content/SharedPreferences$Editor/putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
L1:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getConfig()Landroid/content/SharedPreferences;
ldc "patch_dalvik_on_boot_patterns"
ldc ""
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
astore 1
aload 0
ldc "patch3"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L2
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getConfig()Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "patch_dalvik_on_boot_patterns"
aload 1
ldc "patch3"
ldc ""
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokeinterface android/content/SharedPreferences$Editor/putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
L2:
return
.limit locals 2
.limit stack 5
.end method

.method public static final turn_off_patch_on_boot_all()V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getConfig()Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "patch_dalvik_on_boot_patterns"
ldc ""
invokeinterface android/content/SharedPreferences$Editor/putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
return
.limit locals 0
.limit stack 3
.end method

.method public static final turn_on_patch_on_boot(Ljava/lang/String;)V
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getConfig()Landroid/content/SharedPreferences;
ldc "patch_dalvik_on_boot_patterns"
ldc ""
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
astore 1
aload 0
ldc "patch1"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L0
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getConfig()Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "patch_dalvik_on_boot_patterns"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
ldc "patch1"
ldc ""
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "patch1"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface android/content/SharedPreferences$Editor/putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
L0:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getConfig()Landroid/content/SharedPreferences;
ldc "patch_dalvik_on_boot_patterns"
ldc ""
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
astore 1
aload 0
ldc "patch2"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L1
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getConfig()Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "patch_dalvik_on_boot_patterns"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
ldc "patch2"
ldc ""
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "patch2"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface android/content/SharedPreferences$Editor/putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
L1:
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getConfig()Landroid/content/SharedPreferences;
ldc "patch_dalvik_on_boot_patterns"
ldc ""
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
astore 1
aload 0
ldc "patch3"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L2
invokestatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/getConfig()Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "patch_dalvik_on_boot_patterns"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
ldc "patch3"
ldc ""
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "patch3"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface android/content/SharedPreferences$Editor/putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
L2:
return
.limit locals 2
.limit stack 6
.end method

.method public static verify_and_run(Ljava/lang/String;Ljava/lang/String;)V
ldc ""
putstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/errorOutput Ljava/lang/String;
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
pop
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/errorOutput Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L0
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "busybox "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
pop
L0:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/errorOutput Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L1
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/busybox "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
pop
L1:
return
.limit locals 2
.limit stack 6
.end method

.method public static verify_bind_and_run(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L3 to L4 using L5
.catch java/io/IOException from L6 to L7 using L8
.catch java/io/IOException from L9 to L10 using L11
aload 3
invokevirtual java/lang/String/trim()Ljava/lang/String;
ldc "~chelpus_disabled~"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L12
L13:
return
L12:
aload 2
invokevirtual java/lang/String/trim()Ljava/lang/String;
astore 5
aload 3
invokevirtual java/lang/String/trim()Ljava/lang/String;
astore 4
aload 5
ldc "/"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifne L14
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 5
invokevirtual java/lang/String/trim()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 2
L15:
aload 4
ldc "/"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifne L16
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 4
invokevirtual java/lang/String/trim()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 3
L17:
new java/io/File
dup
aload 4
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/mkdirs()Z
pop
new java/io/File
dup
aload 5
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/mkdirs()Z
pop
new java/io/File
dup
aload 4
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifne L18
ldc "mkdir"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "-p '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/verify_and_run(Ljava/lang/String;Ljava/lang/String;)V
L18:
new java/io/File
dup
aload 5
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifne L0
ldc "mkdir"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "-p '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/verify_and_run(Ljava/lang/String;Ljava/lang/String;)V
L0:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "test.txt"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/createNewFile()Z
pop
L1:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "echo '' >'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "test.txt'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "test.txt"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifne L19
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "umount '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "data: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "test.txt"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "target: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "test.txt"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L3:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "test.txt"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/createNewFile()Z
pop
L4:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "echo '' >'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "test.txt'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "test.txt"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifne L20
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "busybox "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
pop
L6:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "test.txt"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/createNewFile()Z
pop
L7:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "echo '' >'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "test.txt'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "test.txt"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifne L20
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/toolfilesdir Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/busybox "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
pop
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "busybox "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
pop
L9:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "test.txt"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/createNewFile()Z
pop
L10:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "echo '' >'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "test.txt'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "test.txt"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifne L20
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "LuckyPatcher(Binder error): bind not created!"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L20:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "test.txt"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifeq L13
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "test.txt"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifeq L21
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher(Binder): "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " binded!"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L22:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "test.txt"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "test.txt"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifeq L13
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "rm '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "test.txt'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/chelpus/Utils/run_all(Ljava/lang/String;)V
return
L14:
aload 5
astore 2
goto L15
L16:
aload 4
astore 3
goto L17
L2:
astore 5
aload 5
invokevirtual java/io/IOException/printStackTrace()V
goto L1
L5:
astore 5
aload 5
invokevirtual java/io/IOException/printStackTrace()V
goto L4
L8:
astore 5
aload 5
invokevirtual java/io/IOException/printStackTrace()V
goto L7
L11:
astore 0
aload 0
invokevirtual java/io/IOException/printStackTrace()V
goto L10
L19:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher(Binder): "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " exists!"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
goto L20
L21:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher(Binder error): "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " not binded!"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
goto L22
.limit locals 6
.limit stack 6
.end method

.method public static zip(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)V
.signature "(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList<Ljava/io/File;>;Ljava/lang/String;)V"
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
.catch java/lang/Exception from L4 to L6 using L2
.catch java/lang/Exception from L6 to L7 using L2
.catch java/lang/Exception from L8 to L9 using L2
.catch java/lang/Exception from L10 to L11 using L2
.catch java/lang/Exception from L12 to L13 using L5
.catch java/lang/Exception from L13 to L14 using L2
.catch java/lang/Exception from L14 to L15 using L2
.catch java/lang/Exception from L16 to L17 using L2
.catch java/lang/Exception from L18 to L19 using L2
L0:
new java/util/zip/ZipOutputStream
dup
new java/io/BufferedOutputStream
dup
new java/io/FileOutputStream
dup
aload 3
iconst_0
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;Z)V
invokespecial java/io/BufferedOutputStream/<init>(Ljava/io/OutputStream;)V
invokespecial java/util/zip/ZipOutputStream/<init>(Ljava/io/OutputStream;)V
astore 3
sipush 4096
newarray byte
astore 5
aload 2
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 6
L1:
aconst_null
astore 0
L3:
aload 6
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L12
aload 6
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
astore 7
new java/io/FileInputStream
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 7
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileInputStream/<init>(Ljava/lang/String;)V
astore 8
new java/io/BufferedInputStream
dup
aload 8
sipush 4096
invokespecial java/io/BufferedInputStream/<init>(Ljava/io/InputStream;I)V
astore 0
L4:
aload 3
new java/util/zip/ZipEntry
dup
aload 7
invokevirtual java/io/File/getName()Ljava/lang/String;
invokespecial java/util/zip/ZipEntry/<init>(Ljava/lang/String;)V
invokevirtual java/util/zip/ZipOutputStream/putNextEntry(Ljava/util/zip/ZipEntry;)V
L6:
aload 0
aload 5
iconst_0
sipush 4096
invokevirtual java/io/BufferedInputStream/read([BII)I
istore 4
L7:
iload 4
iconst_m1
if_icmpeq L10
L8:
aload 3
aload 5
iconst_0
iload 4
invokevirtual java/util/zip/ZipOutputStream/write([BII)V
L9:
goto L6
L2:
astore 0
L20:
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
L21:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/AndroidManifest.xml"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
aload 2
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 0
L22:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L23
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
astore 2
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
goto L22
L10:
aload 0
invokevirtual java/io/BufferedInputStream/close()V
aload 3
invokevirtual java/util/zip/ZipOutputStream/closeEntry()V
aload 8
invokevirtual java/io/FileInputStream/close()V
L11:
goto L3
L12:
new java/io/FileInputStream
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/AndroidManifest.xml"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileInputStream/<init>(Ljava/lang/String;)V
astore 0
new java/io/BufferedInputStream
dup
aload 0
sipush 4096
invokespecial java/io/BufferedInputStream/<init>(Ljava/io/InputStream;I)V
astore 6
L13:
aload 3
new java/util/zip/ZipEntry
dup
ldc "AndroidManifest.xml"
invokespecial java/util/zip/ZipEntry/<init>(Ljava/lang/String;)V
invokevirtual java/util/zip/ZipOutputStream/putNextEntry(Ljava/util/zip/ZipEntry;)V
L14:
aload 6
aload 5
iconst_0
sipush 4096
invokevirtual java/io/BufferedInputStream/read([BII)I
istore 4
L15:
iload 4
iconst_m1
if_icmpeq L18
L16:
aload 3
aload 5
iconst_0
iload 4
invokevirtual java/util/zip/ZipOutputStream/write([BII)V
L17:
goto L14
L18:
aload 6
invokevirtual java/io/BufferedInputStream/close()V
aload 3
invokevirtual java/util/zip/ZipOutputStream/closeEntry()V
aload 3
invokevirtual java/util/zip/ZipOutputStream/close()V
aload 0
invokevirtual java/io/FileInputStream/close()V
L19:
goto L21
L23:
return
L5:
astore 0
goto L20
.limit locals 9
.limit stack 8
.end method

.method public static zipART(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/lang/String;
.signature "(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList<Ljava/io/File;>;Ljava/lang/String;)Ljava/lang/String;"
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L2
.catch java/lang/Exception from L7 to L8 using L2
.catch java/lang/Exception from L9 to L10 using L2
.catch java/lang/Exception from L11 to L12 using L2
.catch java/lang/Exception from L13 to L14 using L2
.catch java/lang/Exception from L15 to L16 using L2
.catch java/lang/Exception from L17 to L18 using L2
.catch java/lang/Exception from L19 to L20 using L2
.catch java/lang/Exception from L21 to L22 using L2
.catch java/lang/Exception from L23 to L24 using L2
.catch java/lang/Exception from L25 to L26 using L2
.catch java/lang/Exception from L27 to L28 using L2
.catch java/lang/Exception from L29 to L30 using L2
.catch java/lang/Exception from L30 to L31 using L32
.catch java/lang/Exception from L33 to L34 using L32
.catch java/lang/Exception from L35 to L36 using L32
.catch java/lang/Exception from L37 to L38 using L2
.catch java/lang/Exception from L39 to L40 using L2
.catch java/lang/Exception from L41 to L42 using L2
.catch java/lang/Exception from L43 to L44 using L2
.catch java/lang/Exception from L45 to L46 using L2
.catch java/lang/Exception from L47 to L48 using L2
.catch java/lang/Exception from L49 to L50 using L2
.catch java/lang/Exception from L51 to L52 using L2
.catch java/lang/Exception from L53 to L54 using L2
.catch java/lang/Exception from L55 to L56 using L2
.catch java/lang/Exception from L57 to L58 using L2
.catch java/lang/Exception from L59 to L60 using L2
.catch java/lang/Exception from L61 to L62 using L2
.catch java/lang/Exception from L63 to L64 using L2
.catch java/lang/Exception from L65 to L66 using L2
.catch java/lang/Exception from L67 to L68 using L2
.catch java/lang/Exception from L69 to L70 using L2
.catch java/lang/Exception from L71 to L72 using L2
.catch java/lang/Exception from L73 to L74 using L2
.catch java/lang/Exception from L75 to L76 using L2
.catch java/lang/Exception from L77 to L78 using L2
.catch java/lang/Exception from L79 to L80 using L2
.catch java/lang/Exception from L81 to L82 using L2
.catch java/lang/Exception from L83 to L84 using L2
.catch java/lang/Exception from L85 to L86 using L2
.catch java/lang/Exception from L87 to L88 using L2
ldc ""
astore 9
ldc ""
astore 8
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
ldc "/data/tmp"
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
aload 9
astore 6
aload 8
astore 7
L0:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Try create tmp."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L1:
aload 9
astore 6
aload 8
astore 7
L3:
new java/io/File
dup
ldc "/data/tmp"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/mkdir()Z
pop
L4:
aload 9
astore 6
aload 8
astore 7
L5:
new java/io/File
dup
ldc "/data/tmp"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifne L89
L6:
aload 9
astore 6
aload 8
astore 7
L7:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "tmp dir not found. Try create with root."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L8:
aload 9
astore 6
aload 8
astore 7
L9:
new java/io/File
dup
ldc "/data/tmp"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/mkdirs()Z
pop
L10:
aload 9
astore 6
aload 8
astore 7
L11:
new java/io/File
dup
ldc "/data/tmp"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifne L14
L12:
aload 9
astore 6
aload 8
astore 7
L13:
iconst_2
anewarray java/lang/String
dup
iconst_0
ldc "mkdir"
aastore
dup
iconst_1
ldc "/data/tmp"
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L14:
aload 9
astore 6
aload 8
astore 7
L15:
new java/io/File
dup
ldc "/data/tmp"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifne L90
L16:
aload 9
astore 6
aload 8
astore 7
L17:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "tmp dir not created."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L18:
aload 9
astore 6
aload 8
astore 7
aload 9
astore 11
aload 8
astore 10
L19:
aload 3
invokevirtual java/lang/String/length()I
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/data/tmp/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/length()I
if_icmple L52
L20:
aload 9
astore 6
aload 8
astore 7
aload 9
astore 11
aload 8
astore 10
L21:
aload 3
invokevirtual java/lang/String/length()I
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/data/tmp/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/length()I
isub
iconst_1
if_icmple L52
L22:
aload 9
astore 6
aload 8
astore 7
L23:
aload 3
invokevirtual java/lang/String/length()I
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/data/tmp/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/length()I
isub
iconst_1
isub
newarray char
astore 12
L24:
iconst_0
istore 4
L91:
aload 9
astore 6
aload 8
astore 7
L25:
iload 4
aload 12
arraylength
if_icmpge L92
L26:
aload 12
iload 4
bipush 49
castore
iload 4
iconst_1
iadd
istore 4
goto L91
L90:
aload 9
astore 6
aload 8
astore 7
L27:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
ldc "/data/tmp"
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L28:
goto L18
L2:
astore 0
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
aload 6
astore 9
aload 7
astore 8
new java/io/File
dup
aload 6
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L93
new java/io/File
dup
aload 6
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
aload 7
astore 8
aload 6
astore 9
L93:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/AndroidManifest.xml"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
aload 2
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 0
L94:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L95
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
astore 2
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
goto L94
L89:
aload 9
astore 6
aload 8
astore 7
L29:
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "chmod"
aastore
dup
iconst_1
ldc "777"
aastore
dup
iconst_2
ldc "/data/tmp"
aastore
invokestatic com/chelpus/Utils/run_all_no_root([Ljava/lang/String;)V
L30:
new java/io/File
dup
ldc "/data/tmp"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/listFiles()[Ljava/io/File;
astore 6
L31:
aload 6
ifnull L18
L33:
aload 6
arraylength
istore 5
L34:
iconst_0
istore 4
L96:
iload 4
iload 5
if_icmpge L18
aload 6
iload 4
aaload
astore 7
L35:
new com/chelpus/Utils
dup
ldc "1"
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
astore 10
aload 7
invokevirtual java/io/File/delete()Z
pop
aload 7
invokevirtual java/io/File/exists()Z
ifeq L36
aload 10
aload 7
invokevirtual com/chelpus/Utils/deleteFolder(Ljava/io/File;)V
L36:
iload 4
iconst_1
iadd
istore 4
goto L96
L32:
astore 10
aload 9
astore 6
aload 8
astore 7
L37:
aload 10
invokevirtual java/lang/Exception/printStackTrace()V
L38:
goto L18
L92:
aload 9
astore 6
aload 8
astore 7
L39:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/data/tmp/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
new java/lang/String
dup
aload 12
invokespecial java/lang/String/<init>([C)V
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 11
L40:
aload 11
astore 6
aload 8
astore 7
L41:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/data/tmp/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
new java/lang/String
dup
aload 12
invokespecial java/lang/String/<init>([C)V
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 10
L42:
aload 11
astore 6
aload 10
astore 7
L43:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/data/tmp/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
new java/lang/String
dup
aload 12
invokespecial java/lang/String/<init>([C)V
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/mkdirs()Z
pop
L44:
aload 11
astore 6
aload 10
astore 7
L45:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/data/tmp/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
new java/lang/String
dup
aload 12
invokespecial java/lang/String/<init>([C)V
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L48
L46:
aload 11
astore 6
aload 10
astore 7
L47:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Dir delta created."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L48:
aload 11
astore 6
aload 10
astore 7
L49:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Path to create zip: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 11
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L50:
aload 11
astore 6
aload 10
astore 7
L51:
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Path to origin zip: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L52:
aload 11
astore 6
aload 10
astore 7
aload 11
astore 9
aload 10
astore 8
L53:
aload 3
invokevirtual java/lang/String/length()I
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/data/tmp/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/length()I
if_icmpne L97
L54:
aload 11
astore 6
aload 10
astore 7
L55:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/data/tmp/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 9
L56:
ldc ""
astore 8
L97:
aload 9
astore 6
aload 8
astore 7
L57:
new java/io/File
dup
aload 9
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L60
L58:
aload 9
astore 6
aload 8
astore 7
L59:
new java/io/File
dup
aload 9
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L60:
aload 9
astore 6
aload 8
astore 7
L61:
new java/util/zip/ZipOutputStream
dup
new java/io/BufferedOutputStream
dup
new java/io/FileOutputStream
dup
aload 9
iconst_0
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;Z)V
invokespecial java/io/BufferedOutputStream/<init>(Ljava/io/OutputStream;)V
invokespecial java/util/zip/ZipOutputStream/<init>(Ljava/io/OutputStream;)V
astore 0
L62:
aload 9
astore 6
aload 8
astore 7
L63:
sipush 4096
newarray byte
astore 3
L64:
aload 9
astore 6
aload 8
astore 7
L65:
aload 2
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 10
L66:
aload 9
astore 6
aload 8
astore 7
L67:
aload 10
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L98
L68:
aload 9
astore 6
aload 8
astore 7
L69:
aload 10
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/io/File
astore 11
L70:
aload 9
astore 6
aload 8
astore 7
L71:
new java/io/FileInputStream
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 11
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileInputStream/<init>(Ljava/lang/String;)V
astore 12
L72:
aload 9
astore 6
aload 8
astore 7
L73:
new java/io/BufferedInputStream
dup
aload 12
sipush 4096
invokespecial java/io/BufferedInputStream/<init>(Ljava/io/InputStream;I)V
astore 13
L74:
aload 9
astore 6
aload 8
astore 7
L75:
aload 0
new java/util/zip/ZipEntry
dup
aload 11
invokevirtual java/io/File/getName()Ljava/lang/String;
invokespecial java/util/zip/ZipEntry/<init>(Ljava/lang/String;)V
invokevirtual java/util/zip/ZipOutputStream/putNextEntry(Ljava/util/zip/ZipEntry;)V
L76:
aload 9
astore 6
aload 8
astore 7
L77:
aload 13
aload 3
iconst_0
sipush 4096
invokevirtual java/io/BufferedInputStream/read([BII)I
istore 4
L78:
iload 4
iconst_m1
if_icmpeq L99
aload 9
astore 6
aload 8
astore 7
L79:
aload 0
aload 3
iconst_0
iload 4
invokevirtual java/util/zip/ZipOutputStream/write([BII)V
L80:
goto L76
L99:
aload 9
astore 6
aload 8
astore 7
L81:
aload 13
invokevirtual java/io/BufferedInputStream/close()V
L82:
aload 9
astore 6
aload 8
astore 7
L83:
aload 0
invokevirtual java/util/zip/ZipOutputStream/closeEntry()V
L84:
aload 9
astore 6
aload 8
astore 7
L85:
aload 12
invokevirtual java/io/FileInputStream/close()V
L86:
goto L66
L98:
aload 9
astore 6
aload 8
astore 7
L87:
aload 0
invokevirtual java/util/zip/ZipOutputStream/close()V
L88:
goto L93
L95:
new java/io/File
dup
aload 9
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L100
aload 9
astore 0
new java/io/File
dup
aload 9
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
lconst_0
lcmp
ifne L101
L100:
new java/io/File
dup
aload 9
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L102
new java/io/File
dup
aload 9
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L102:
new java/io/File
dup
aload 8
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/exists()Z
ifeq L103
aload 8
ldc "1"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L103
new java/io/File
dup
aload 8
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L103:
ldc ""
astore 0
L101:
aload 0
areturn
.limit locals 14
.limit stack 8
.end method

.method public transient cmdRoot([Ljava/lang/String;)Ljava/lang/String;
.catch com/android/vending/billing/InAppBillingService/LUCK/ShellOnMainThreadException from L0 to L1 using L1
.catch com/android/vending/billing/InAppBillingService/LUCK/ShellOnMainThreadException from L2 to L3 using L3
.catch java/lang/InterruptedException from L4 to L5 using L6
.catch all from L4 to L5 using L7
.catch java/lang/InterruptedException from L8 to L9 using L6
.catch all from L8 to L9 using L7
.catch java/lang/InterruptedException from L9 to L10 using L6
.catch all from L9 to L10 using L7
.catch java/lang/InterruptedException from L11 to L12 using L6
.catch all from L11 to L12 using L7
.catch java/lang/InterruptedException from L13 to L14 using L6
.catch all from L13 to L14 using L7
.catch all from L15 to L16 using L7
iconst_0
istore 2
L0:
invokestatic com/chelpus/Utils/onMainThread()Z
ifeq L2
new com/android/vending/billing/InAppBillingService/LUCK/ShellOnMainThreadException
dup
ldc "Application attempted to run a shell command from the main thread"
invokespecial com/android/vending/billing/InAppBillingService/LUCK/ShellOnMainThreadException/<init>(Ljava/lang/String;)V
athrow
L1:
astore 5
aload 5
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/ShellOnMainThreadException/printStackTrace()V
L2:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/startUnderRoot Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L17
new com/android/vending/billing/InAppBillingService/LUCK/ShellOnMainThreadException
dup
ldc "Application attempted to run a shell command from the main thread"
invokespecial com/android/vending/billing/InAppBillingService/LUCK/ShellOnMainThreadException/<init>(Ljava/lang/String;)V
athrow
L3:
astore 5
aload 5
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/ShellOnMainThreadException/printStackTrace()V
L17:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/su Z
ifne L18
aload 1
iconst_0
aaload
ldc "checkRoot"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L18
ldc "lucky patcher root not found!"
areturn
L18:
new com/chelpus/Utils$Worker
dup
aload 0
aconst_null
invokespecial com/chelpus/Utils$Worker/<init>(Lcom/chelpus/Utils;Lcom/chelpus/Utils$1;)V
astore 5
aload 5
aload 1
invokestatic com/chelpus/Utils$Worker/access$102(Lcom/chelpus/Utils$Worker;[Ljava/lang/String;)[Ljava/lang/String;
pop
aload 5
invokevirtual com/chelpus/Utils$Worker/start()V
iconst_0
istore 3
L4:
aload 1
arraylength
istore 4
L5:
iload 2
iload 4
if_icmpge L19
aload 1
iload 2
aaload
astore 6
L8:
aload 6
ldc "-Xbootclasspath:"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L20
aload 6
ldc "dd "
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L20
aload 6
ldc "cp "
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L20
aload 6
ldc "cat "
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L20
L9:
aload 6
ldc "pm "
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L21
L10:
goto L20
L19:
iload 3
ifeq L13
L11:
aload 5
invokevirtual com/chelpus/Utils$Worker/join()V
L12:
aload 5
invokestatic com/chelpus/Utils$Worker/access$300(Lcom/chelpus/Utils$Worker;)Ljava/lang/String;
areturn
L13:
aload 5
invokevirtual com/chelpus/Utils$Worker/join()V
aload 5
invokestatic com/chelpus/Utils$Worker/access$200(Lcom/chelpus/Utils$Worker;)Ljava/lang/Integer;
astore 1
L14:
aload 1
ifnonnull L12
goto L12
L6:
astore 1
L15:
aload 1
invokevirtual java/lang/InterruptedException/printStackTrace()V
aload 5
invokevirtual com/chelpus/Utils$Worker/interrupt()V
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
invokestatic com/chelpus/Utils/exitRoot()V
L16:
goto L12
L7:
astore 1
aload 1
athrow
L20:
iconst_1
istore 3
L21:
iload 2
iconst_1
iadd
istore 2
goto L5
.limit locals 7
.limit stack 4
.end method

.method public deleteFolder(Ljava/io/File;)V
.throws java/io/IOException
aload 1
invokevirtual java/io/File/exists()Z
ifne L0
return
L0:
aload 1
invokevirtual java/io/File/isDirectory()Z
ifeq L1
aload 1
invokevirtual java/io/File/listFiles()[Ljava/io/File;
astore 4
aload 4
arraylength
istore 3
iconst_0
istore 2
L2:
iload 2
iload 3
if_icmpge L1
aload 0
aload 4
iload 2
aaload
invokevirtual com/chelpus/Utils/deleteFolder(Ljava/io/File;)V
iload 2
iconst_1
iadd
istore 2
goto L2
L1:
new java/io/File
dup
aload 1
invokevirtual java/io/File/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
return
.limit locals 5
.limit stack 3
.end method

.method public findFile(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;
.throws java/io/IOException
aload 1
invokevirtual java/io/File/exists()Z
ifne L0
ldc ""
astore 5
L1:
aload 5
areturn
L0:
aload 1
invokevirtual java/io/File/isDirectory()Z
ifeq L2
aload 1
invokevirtual java/io/File/listFiles()[Ljava/io/File;
astore 7
aload 7
arraylength
istore 4
iconst_0
istore 3
L3:
iload 3
iload 4
if_icmpge L2
aload 0
aload 7
iload 3
aaload
aload 2
invokevirtual com/chelpus/Utils/findFile(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;
astore 6
aload 6
astore 5
aload 6
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L1
iload 3
iconst_1
iadd
istore 3
goto L3
L2:
aload 1
invokevirtual java/io/File/getName()Ljava/lang/String;
aload 2
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L4
aload 1
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
areturn
L4:
ldc ""
areturn
.limit locals 8
.limit stack 3
.end method

.method public findFileContainText(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;
.throws java/io/IOException
aload 1
invokevirtual java/io/File/isDirectory()Z
ifeq L0
aload 1
invokevirtual java/io/File/listFiles()[Ljava/io/File;
astore 5
aload 5
arraylength
istore 4
iconst_0
istore 3
L1:
iload 3
iload 4
if_icmpge L0
aload 0
aload 5
iload 3
aaload
aload 2
invokevirtual com/chelpus/Utils/findFileContainText(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;
astore 6
aload 6
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L2
aload 6
areturn
L2:
iload 3
iconst_1
iadd
istore 3
goto L1
L0:
aload 1
invokevirtual java/io/File/getName()Ljava/lang/String;
aload 2
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L3
aload 1
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
areturn
L3:
ldc ""
areturn
.limit locals 7
.limit stack 3
.end method

.method public findFileEndText(Ljava/io/File;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;
.signature "(Ljava/io/File;Ljava/lang/String;Ljava/util/ArrayList<Ljava/io/File;>;)Ljava/lang/String;"
.throws java/io/IOException
aload 1
invokevirtual java/io/File/exists()Z
ifne L0
ldc ""
areturn
L0:
aload 1
invokevirtual java/io/File/isDirectory()Z
ifeq L1
aload 1
invokevirtual java/io/File/listFiles()[Ljava/io/File;
astore 6
aload 6
arraylength
istore 5
iconst_0
istore 4
L2:
iload 4
iload 5
if_icmpge L1
aload 0
aload 6
iload 4
aaload
aload 2
aload 3
invokevirtual com/chelpus/Utils/findFileEndText(Ljava/io/File;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;
astore 7
aload 7
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L3
new java/io/File
dup
aload 7
invokespecial java/io/File/<init>(Ljava/lang/String;)V
aload 3
invokestatic com/chelpus/Utils/addFileToList(Ljava/io/File;Ljava/util/ArrayList;)V
L3:
iload 4
iconst_1
iadd
istore 4
goto L2
L1:
aload 1
invokevirtual java/io/File/getName()Ljava/lang/String;
aload 2
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L4
aload 1
aload 3
invokestatic com/chelpus/Utils/addFileToList(Ljava/io/File;Ljava/util/ArrayList;)V
aload 1
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
areturn
L4:
ldc ""
areturn
.limit locals 8
.limit stack 4
.end method

.method public getInput(ZLcom/chelpus/Utils$Worker;)Ljava/lang/String;
.throws java/lang/Exception
.catch java/lang/Exception from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/lang/Exception from L4 to L5 using L2
.catch all from L4 to L5 using L3
.catch java/lang/Exception from L6 to L7 using L2
.catch all from L6 to L7 using L3
.catch java/lang/Exception from L8 to L9 using L2
.catch all from L8 to L9 using L3
.catch java/lang/Exception from L10 to L11 using L2
.catch all from L10 to L11 using L3
.catch java/lang/Exception from L12 to L13 using L2
.catch all from L12 to L13 using L3
.catch java/lang/Exception from L14 to L15 using L2
.catch all from L14 to L15 using L3
.catch java/lang/Exception from L16 to L17 using L2
.catch all from L16 to L17 using L3
.catch java/lang/Exception from L18 to L19 using L2
.catch all from L18 to L19 using L3
.catch java/lang/Exception from L20 to L21 using L2
.catch all from L20 to L21 using L3
.catch java/lang/Exception from L22 to L23 using L2
.catch all from L22 to L23 using L3
.catch java/lang/Exception from L24 to L25 using L2
.catch all from L24 to L25 using L3
.catch java/lang/Exception from L26 to L27 using L2
.catch all from L26 to L27 using L3
.catch java/lang/Exception from L28 to L29 using L2
.catch all from L28 to L29 using L3
.catch java/lang/Exception from L30 to L31 using L2
.catch all from L30 to L31 using L3
.catch java/lang/Exception from L32 to L33 using L2
.catch all from L32 to L33 using L3
.catch java/lang/Exception from L34 to L35 using L2
.catch all from L34 to L35 using L3
.catch java/lang/Exception from L36 to L37 using L2
.catch all from L36 to L37 using L3
.catch java/lang/Exception from L38 to L39 using L2
.catch all from L38 to L39 using L3
.catch java/lang/Exception from L40 to L41 using L2
.catch all from L40 to L41 using L3
.catch java/lang/Exception from L42 to L43 using L2
.catch all from L42 to L43 using L3
.catch java/lang/Exception from L44 to L45 using L2
.catch all from L44 to L45 using L3
.catch java/lang/Exception from L46 to L47 using L2
.catch all from L46 to L47 using L3
.catch java/lang/Exception from L48 to L49 using L2
.catch all from L48 to L49 using L3
.catch java/lang/Exception from L50 to L51 using L2
.catch all from L50 to L51 using L3
.catch all from L52 to L53 using L3
new java/util/Timer
dup
invokespecial java/util/Timer/<init>()V
astore 7
new com/chelpus/Utils$RootTimerTask
dup
aload 0
aload 2
invokespecial com/chelpus/Utils$RootTimerTask/<init>(Lcom/chelpus/Utils;Lcom/chelpus/Utils$Worker;)V
astore 4
aload 2
invokestatic com/chelpus/Utils$Worker/access$100(Lcom/chelpus/Utils$Worker;)[Ljava/lang/String;
iconst_0
aaload
ldc ".corepatch "
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L54
aload 7
aload 4
ldc2_w 600000L
invokevirtual java/util/Timer/schedule(Ljava/util/TimerTask;J)V
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Input a string within 3 minuten: "
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L54:
ldc ""
astore 4
aload 4
astore 5
L0:
aload 2
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/suInputStream Ljava/io/DataInputStream;
putfield com/chelpus/Utils$Worker/input Ljava/io/DataInputStream;
L1:
aload 4
astore 5
L4:
aload 2
getfield com/chelpus/Utils$Worker/input Ljava/io/DataInputStream;
invokestatic com/chelpus/Utils/readLine(Ljava/io/InputStream;)Ljava/lang/String;
astore 8
L5:
aload 4
astore 5
aload 8
ifnull L55
iload 1
ifeq L41
aload 4
astore 5
L6:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/progress_loading Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;
ifnull L41
L7:
aload 4
astore 5
L8:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/progress_loading Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading/isShowing()Z
ifeq L41
L9:
aload 4
astore 5
L10:
aload 8
ldc "Get classes.dex."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L13
L11:
aload 4
astore 5
L12:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/frag Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
new com/chelpus/Utils$2
dup
aload 0
invokespecial com/chelpus/Utils$2/<init>(Lcom/chelpus/Utils;)V
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/runToMain(Ljava/lang/Runnable;)V
L13:
aload 4
astore 5
L14:
aload 8
ldc "String analysis."
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L17
L15:
aload 4
astore 5
L16:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/frag Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
new com/chelpus/Utils$3
dup
aload 0
invokespecial com/chelpus/Utils$3/<init>(Lcom/chelpus/Utils;)V
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/runToMain(Ljava/lang/Runnable;)V
L17:
aload 4
astore 5
L18:
aload 8
ldc "Parse data for patch."
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L21
L19:
aload 4
astore 5
L20:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/frag Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
new com/chelpus/Utils$4
dup
aload 0
invokespecial com/chelpus/Utils$4/<init>(Lcom/chelpus/Utils;)V
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/runToMain(Ljava/lang/Runnable;)V
L21:
aload 4
astore 5
L22:
aload 8
ldc "Progress size:"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L25
L23:
aload 4
astore 5
L24:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/frag Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
new com/chelpus/Utils$5
dup
aload 0
aload 8
invokespecial com/chelpus/Utils$5/<init>(Lcom/chelpus/Utils;Ljava/lang/String;)V
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/runToMain(Ljava/lang/Runnable;)V
L25:
aload 4
astore 5
L26:
aload 8
ldc "Size file:"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L29
L27:
aload 4
astore 5
L28:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/frag Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
new com/chelpus/Utils$6
dup
aload 0
aload 8
invokespecial com/chelpus/Utils$6/<init>(Lcom/chelpus/Utils;Ljava/lang/String;)V
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/runToMain(Ljava/lang/Runnable;)V
L29:
aload 4
astore 5
L30:
aload 8
ldc "Analise Results:"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L33
L31:
aload 4
astore 5
L32:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/frag Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
new com/chelpus/Utils$7
dup
aload 0
invokespecial com/chelpus/Utils$7/<init>(Lcom/chelpus/Utils;)V
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/runToMain(Ljava/lang/Runnable;)V
L33:
aload 4
astore 5
L34:
aload 8
ldc "Create ODEX:"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L37
L35:
aload 4
astore 5
L36:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/frag Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
new com/chelpus/Utils$8
dup
aload 0
invokespecial com/chelpus/Utils$8/<init>(Lcom/chelpus/Utils;)V
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/runToMain(Ljava/lang/Runnable;)V
L37:
aload 4
astore 5
L38:
aload 8
ldc "Optional Steps After Patch:"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L41
L39:
aload 4
astore 5
L40:
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/frag Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
new com/chelpus/Utils$9
dup
aload 0
invokespecial com/chelpus/Utils$9/<init>(Lcom/chelpus/Utils;)V
invokevirtual com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/runToMain(Ljava/lang/Runnable;)V
L41:
aload 4
astore 6
aload 4
astore 5
L42:
aload 8
ldc "com.chelpus.root.utils.custompatch"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L56
L43:
aload 4
astore 5
L44:
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 4
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L45:
ldc ""
astore 6
L56:
aload 6
astore 5
L46:
aload 8
ldc "chelpus done!"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
istore 3
L47:
iload 3
ifeq L57
aload 6
astore 5
L55:
aload 7
invokevirtual java/util/Timer/cancel()V
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "you have entered: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 5
areturn
L57:
aload 6
astore 4
aload 6
astore 5
L48:
aload 8
ldc "chelpusstart!"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L1
L49:
aload 6
astore 5
L50:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 8
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 4
L51:
goto L1
L2:
astore 2
L52:
aload 2
invokevirtual java/lang/Exception/printStackTrace()V
L53:
goto L55
L3:
astore 2
aload 2
athrow
.limit locals 9
.limit stack 5
.end method

.method public sizeFolder(Ljava/io/File;Z)F
.throws java/io/IOException
iload 2
ifeq L0
aload 0
fconst_0
putfield com/chelpus/Utils/folder_size F
L0:
aload 1
invokevirtual java/io/File/isDirectory()Z
ifeq L1
aload 1
invokevirtual java/io/File/listFiles()[Ljava/io/File;
astore 5
aload 5
arraylength
istore 4
iconst_0
istore 3
L2:
iload 3
iload 4
if_icmpge L1
aload 0
aload 5
iload 3
aaload
iconst_0
invokevirtual com/chelpus/Utils/sizeFolder(Ljava/io/File;Z)F
pop
iload 3
iconst_1
iadd
istore 3
goto L2
L1:
aload 0
aload 0
getfield com/chelpus/Utils/folder_size F
new java/io/File
dup
aload 1
invokevirtual java/io/File/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/length()J
l2f
fadd
putfield com/chelpus/Utils/folder_size F
aload 0
getfield com/chelpus/Utils/folder_size F
ldc_w 1048576.0F
fdiv
freturn
.limit locals 6
.limit stack 5
.end method

.method public waitLP(J)V
.catch java/lang/Exception from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch all from L1 to L4 using L3
.catch all from L5 to L6 using L3
invokestatic java/lang/System/currentTimeMillis()J
lload 1
ladd
lstore 1
L7:
invokestatic java/lang/System/currentTimeMillis()J
lload 1
lcmp
ifge L8
aload 0
monitorenter
L0:
aload 0
lload 1
invokestatic java/lang/System/currentTimeMillis()J
lsub
invokevirtual java/lang/Object/wait(J)V
L1:
aload 0
monitorexit
L4:
goto L7
L3:
astore 3
L5:
aload 0
monitorexit
L6:
aload 3
athrow
L8:
return
L2:
astore 3
goto L1
.limit locals 4
.limit stack 5
.end method
