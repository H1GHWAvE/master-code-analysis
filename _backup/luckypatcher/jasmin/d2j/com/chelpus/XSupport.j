.bytecode 50.0
.class public synchronized com/chelpus/XSupport
.super java/lang/Object
.implements de/robv/android/xposed/IXposedHookZygoteInit
.implements de/robv/android/xposed/IXposedHookLoadPackage
.inner class inner com/chelpus/XSupport$1
.inner class inner com/chelpus/XSupport$10
.inner class inner com/chelpus/XSupport$11
.inner class inner com/chelpus/XSupport$12
.inner class inner com/chelpus/XSupport$13
.inner class inner com/chelpus/XSupport$14
.inner class inner com/chelpus/XSupport$15
.inner class inner com/chelpus/XSupport$16
.inner class inner com/chelpus/XSupport$17
.inner class inner com/chelpus/XSupport$18
.inner class inner com/chelpus/XSupport$19
.inner class inner com/chelpus/XSupport$2
.inner class inner com/chelpus/XSupport$20
.inner class inner com/chelpus/XSupport$21
.inner class inner com/chelpus/XSupport$22
.inner class inner com/chelpus/XSupport$23
.inner class inner com/chelpus/XSupport$24
.inner class inner com/chelpus/XSupport$25
.inner class inner com/chelpus/XSupport$26
.inner class inner com/chelpus/XSupport$27
.inner class inner com/chelpus/XSupport$28
.inner class inner com/chelpus/XSupport$29
.inner class inner com/chelpus/XSupport$3
.inner class inner com/chelpus/XSupport$30
.inner class inner com/chelpus/XSupport$31
.inner class inner com/chelpus/XSupport$32
.inner class inner com/chelpus/XSupport$33
.inner class inner com/chelpus/XSupport$34
.inner class inner com/chelpus/XSupport$35
.inner class inner com/chelpus/XSupport$36
.inner class inner com/chelpus/XSupport$37
.inner class inner com/chelpus/XSupport$38
.inner class inner com/chelpus/XSupport$39
.inner class inner com/chelpus/XSupport$4
.inner class inner com/chelpus/XSupport$5
.inner class inner com/chelpus/XSupport$6
.inner class inner com/chelpus/XSupport$7
.inner class inner com/chelpus/XSupport$8
.inner class inner com/chelpus/XSupport$9

.field public static 'enable' Z

.field public static 'hide' Z

.field public static 'patch1' Z

.field public static 'patch2' Z

.field public static 'patch3' Z

.field public static 'patch4' Z

.field 'PMcontext' Landroid/content/Context;

.field public 'checkDuplicatedPermissions' Z

.field public 'checkPermissions' Z

.field public 'checkSignatures' Z

.field 'ctx' Landroid/content/Context;

.field public 'currentPkgInApp' Ljava/lang/String;

.field public 'disableCheckSignatures' Z

.field 'forHide' Ljava/lang/Boolean;

.field 'hideFromThisApp' Ljava/lang/Boolean;

.field public 'initialize' J

.field public 'installUnsignedApps' Z

.field 'mContext' Landroid/content/Context;

.field public 'prefs' Lde/robv/android/xposed/XSharedPreferences;

.field 'skip1' Z

.field 'skip2' Z

.field 'skip3' Z

.field 'skipGB' Z

.field public 'verifyApps' Z

.method static <clinit>()V
iconst_1
putstatic com/chelpus/XSupport/patch1 Z
iconst_1
putstatic com/chelpus/XSupport/patch2 Z
iconst_1
putstatic com/chelpus/XSupport/patch3 Z
iconst_1
putstatic com/chelpus/XSupport/patch4 Z
iconst_0
putstatic com/chelpus/XSupport/hide Z
iconst_1
putstatic com/chelpus/XSupport/enable Z
return
.limit locals 0
.limit stack 1
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aconst_null
putfield com/chelpus/XSupport/currentPkgInApp Ljava/lang/String;
aload 0
lconst_0
putfield com/chelpus/XSupport/initialize J
aload 0
aconst_null
putfield com/chelpus/XSupport/ctx Landroid/content/Context;
aload 0
aconst_null
putfield com/chelpus/XSupport/forHide Ljava/lang/Boolean;
aload 0
aconst_null
putfield com/chelpus/XSupport/mContext Landroid/content/Context;
aload 0
aconst_null
putfield com/chelpus/XSupport/PMcontext Landroid/content/Context;
aload 0
aconst_null
putfield com/chelpus/XSupport/hideFromThisApp Ljava/lang/Boolean;
aload 0
iconst_0
putfield com/chelpus/XSupport/skip1 Z
aload 0
iconst_0
putfield com/chelpus/XSupport/skip2 Z
aload 0
iconst_0
putfield com/chelpus/XSupport/skip3 Z
aload 0
iconst_0
putfield com/chelpus/XSupport/skipGB Z
return
.limit locals 1
.limit stack 3
.end method

.method public checkForHide(Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;)Z
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
invokestatic android/os/Binder/getCallingUid()I
istore 2
aload 1
getfield de/robv/android/xposed/XC_MethodHook$MethodHookParam/thisObject Ljava/lang/Object;
ldc "getNameForUid"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic de/robv/android/xposed/XposedHelpers/callMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
astore 1
aload 1
ldc ":"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L3
aload 1
ldc com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment
invokevirtual java/lang/Class/getPackage()Ljava/lang/Package;
invokevirtual java/lang/Package/getName()Ljava/lang/String;
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L3
aload 1
ldc "de.robv.android.xposed.installer"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L3
aload 1
ldc "supersu"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L3
aload 1
ldc "superuser"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L3
aload 1
ldc "pro.burgerz.wsm.manager"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L4
L3:
iconst_0
ireturn
L4:
aload 0
getfield com/chelpus/XSupport/PMcontext Landroid/content/Context;
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
astore 3
L0:
aload 3
aload 1
iconst_0
invokevirtual android/content/pm/PackageManager/getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
astore 4
L1:
aload 4
ifnull L5
aload 4
getfield android/content/pm/ApplicationInfo/flags I
iconst_1
iand
ifeq L5
iconst_0
ireturn
L2:
astore 1
aload 1
invokevirtual android/content/pm/PackageManager$NameNotFoundException/printStackTrace()V
iconst_0
ireturn
L5:
iconst_1
ifeq L6
new android/content/Intent
dup
ldc "android.intent.action.MAIN"
invokespecial android/content/Intent/<init>(Ljava/lang/String;)V
astore 4
aload 4
ldc "android.intent.category.HOME"
invokevirtual android/content/Intent/addCategory(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 4
ldc "android.intent.category.DEFAULT"
invokevirtual android/content/Intent/addCategory(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 3
aload 4
iconst_0
invokevirtual android/content/pm/PackageManager/queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 3
L7:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L8
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/content/pm/ResolveInfo
getfield android/content/pm/ResolveInfo/activityInfo Landroid/content/pm/ActivityInfo;
getfield android/content/pm/ActivityInfo/packageName Ljava/lang/String;
aload 1
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L7
iconst_0
ireturn
L8:
iconst_1
ifeq L6
iconst_1
ireturn
L6:
iconst_0
ireturn
.limit locals 5
.limit stack 6
.end method

.method public checkForHideApp(Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;)Z
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
invokestatic android/os/Binder/getCallingUid()I
istore 2
aload 1
getfield de/robv/android/xposed/XC_MethodHook$MethodHookParam/thisObject Ljava/lang/Object;
ldc "getNameForUid"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic de/robv/android/xposed/XposedHelpers/callMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
astore 1
aload 1
ldc ":"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L3
aload 1
ldc com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment
invokevirtual java/lang/Class/getPackage()Ljava/lang/Package;
invokevirtual java/lang/Package/getName()Ljava/lang/String;
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L3
aload 1
ldc "de.robv.android.xposed.installer"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L3
aload 1
ldc "supersu"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L3
aload 1
ldc "superuser"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L3
aload 1
ldc "pro.burgerz.wsm.manager"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L4
L3:
iconst_0
ireturn
L4:
aload 0
getfield com/chelpus/XSupport/PMcontext Landroid/content/Context;
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
astore 3
L0:
aload 3
aload 1
iconst_0
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
astore 4
L1:
aload 4
ifnull L5
aload 4
getfield android/content/pm/PackageInfo/applicationInfo Landroid/content/pm/ApplicationInfo;
getfield android/content/pm/ApplicationInfo/flags I
iconst_1
iand
ifeq L5
iconst_0
ireturn
L2:
astore 1
aload 1
invokevirtual android/content/pm/PackageManager$NameNotFoundException/printStackTrace()V
iconst_0
ireturn
L5:
iconst_1
ifeq L6
new android/content/Intent
dup
ldc "android.intent.action.MAIN"
invokespecial android/content/Intent/<init>(Ljava/lang/String;)V
astore 4
aload 4
ldc "android.intent.category.HOME"
invokevirtual android/content/Intent/addCategory(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 4
ldc "android.intent.category.DEFAULT"
invokevirtual android/content/Intent/addCategory(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 3
aload 4
iconst_0
invokevirtual android/content/pm/PackageManager/queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 3
L7:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L8
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/content/pm/ResolveInfo
getfield android/content/pm/ResolveInfo/activityInfo Landroid/content/pm/ActivityInfo;
getfield android/content/pm/ActivityInfo/packageName Ljava/lang/String;
aload 1
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L7
iconst_0
ireturn
L8:
iconst_1
ifeq L6
iconst_1
ireturn
L6:
iconst_0
ireturn
.limit locals 5
.limit stack 6
.end method

.method public checkIntentRework(Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;Landroid/content/Intent;II)Z
.catch java/lang/ClassCastException from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
.catch java/lang/Exception from L6 to L7 using L8
.catch java/lang/Exception from L9 to L10 using L11
.catch java/lang/Exception from L12 to L13 using L14
iload 4
ifne L15
L0:
aload 1
getfield de/robv/android/xposed/XC_MethodHook$MethodHookParam/thisObject Ljava/lang/Object;
ldc "getBaseContext"
iconst_0
anewarray java/lang/Object
invokestatic de/robv/android/xposed/XposedHelpers/callMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/content/Context
astore 5
L1:
aload 5
ifnull L15
iload 3
ifne L16
aload 5
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
getstatic com/chelpus/Common/PACKAGE_NAME Ljava/lang/String;
ldc com/google/android/finsky/billing/iab/InAppBillingService
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokespecial android/content/ComponentName/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/content/pm/PackageManager/getComponentEnabledSetting(Landroid/content/ComponentName;)I
iconst_2
if_icmpne L17
aload 5
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
getstatic com/chelpus/Common/PACKAGE_NAME Ljava/lang/String;
ldc com/google/android/finsky/billing/iab/MarketBillingService
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokespecial android/content/ComponentName/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/content/pm/PackageManager/getComponentEnabledSetting(Landroid/content/ComponentName;)I
iconst_2
if_icmpeq L16
L17:
aload 2
ldc "xexe"
invokevirtual android/content/Intent/getStringExtra(Ljava/lang/String;)Ljava/lang/String;
ifnull L18
aload 2
ldc "xexe"
invokevirtual android/content/Intent/getStringExtra(Ljava/lang/String;)Ljava/lang/String;
ldc "lp"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L19
L18:
iconst_1
ireturn
L2:
astore 5
aload 1
getfield de/robv/android/xposed/XC_MethodHook$MethodHookParam/thisObject Ljava/lang/Object;
ldc "mBase"
invokestatic de/robv/android/xposed/XposedHelpers/getObjectField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
checkcast android/content/Context
astore 5
goto L1
L19:
iconst_0
ireturn
L16:
iload 3
iconst_1
if_icmpne L15
aload 5
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
getstatic com/chelpus/Common/PACKAGE_NAME Ljava/lang/String;
ldc com/google/android/finsky/services/LicensingService
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokespecial android/content/ComponentName/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/content/pm/PackageManager/getComponentEnabledSetting(Landroid/content/ComponentName;)I
iconst_2
if_icmpeq L15
aload 2
ldc "xexe"
invokevirtual android/content/Intent/getStringExtra(Ljava/lang/String;)Ljava/lang/String;
ifnull L20
aload 2
ldc "xexe"
invokevirtual android/content/Intent/getStringExtra(Ljava/lang/String;)Ljava/lang/String;
ldc "lp"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L21
L20:
iconst_1
ireturn
L21:
iconst_0
ireturn
L15:
iload 4
iconst_1
if_icmpne L22
iload 3
ifne L23
aload 1
getfield de/robv/android/xposed/XC_MethodHook$MethodHookParam/thisObject Ljava/lang/Object;
ldc "getPackageManager"
iconst_0
anewarray java/lang/Object
invokestatic de/robv/android/xposed/XposedHelpers/callMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/content/pm/PackageManager
new android/content/ComponentName
dup
getstatic com/chelpus/Common/PACKAGE_NAME Ljava/lang/String;
ldc com/google/android/finsky/billing/iab/InAppBillingService
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokespecial android/content/ComponentName/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/content/pm/PackageManager/getComponentEnabledSetting(Landroid/content/ComponentName;)I
iconst_2
if_icmpne L3
aload 1
getfield de/robv/android/xposed/XC_MethodHook$MethodHookParam/thisObject Ljava/lang/Object;
ldc "getPackageManager"
iconst_0
anewarray java/lang/Object
invokestatic de/robv/android/xposed/XposedHelpers/callMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/content/pm/PackageManager
new android/content/ComponentName
dup
getstatic com/chelpus/Common/PACKAGE_NAME Ljava/lang/String;
ldc com/google/android/finsky/billing/iab/MarketBillingService
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokespecial android/content/ComponentName/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/content/pm/PackageManager/getComponentEnabledSetting(Landroid/content/ComponentName;)I
iconst_2
if_icmpeq L23
L3:
aload 2
ldc "xexe"
invokevirtual android/content/Intent/getStringExtra(Ljava/lang/String;)Ljava/lang/String;
astore 1
L4:
aload 1
ifnull L24
aload 1
ldc "lp"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L25
L24:
iconst_1
ireturn
L5:
astore 1
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "skip inapp xposed queryIntentServices"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aconst_null
astore 1
goto L4
L25:
iconst_0
ireturn
L23:
iload 3
iconst_1
if_icmpne L22
aload 1
getfield de/robv/android/xposed/XC_MethodHook$MethodHookParam/thisObject Ljava/lang/Object;
ldc "getPackageManager"
iconst_0
anewarray java/lang/Object
invokestatic de/robv/android/xposed/XposedHelpers/callMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/content/pm/PackageManager
new android/content/ComponentName
dup
getstatic com/chelpus/Common/PACKAGE_NAME Ljava/lang/String;
ldc com/google/android/finsky/services/LicensingService
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokespecial android/content/ComponentName/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/content/pm/PackageManager/getComponentEnabledSetting(Landroid/content/ComponentName;)I
iconst_2
if_icmpeq L22
L6:
aload 2
ldc "xexe"
invokevirtual android/content/Intent/getStringExtra(Ljava/lang/String;)Ljava/lang/String;
astore 1
L7:
aload 1
ifnull L26
aload 1
ldc "lp"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L27
L26:
iconst_1
ireturn
L8:
astore 1
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "skip inapp xposed queryIntentServices"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aconst_null
astore 1
goto L7
L27:
iconst_0
ireturn
L22:
iload 4
iconst_2
if_icmpne L28
iload 3
ifne L29
aload 0
getfield com/chelpus/XSupport/PMcontext Landroid/content/Context;
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
getstatic com/chelpus/Common/PACKAGE_NAME Ljava/lang/String;
ldc com/google/android/finsky/billing/iab/InAppBillingService
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokespecial android/content/ComponentName/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/content/pm/PackageManager/getComponentEnabledSetting(Landroid/content/ComponentName;)I
iconst_2
if_icmpne L9
aload 0
getfield com/chelpus/XSupport/PMcontext Landroid/content/Context;
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
getstatic com/chelpus/Common/PACKAGE_NAME Ljava/lang/String;
ldc com/google/android/finsky/billing/iab/MarketBillingService
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokespecial android/content/ComponentName/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/content/pm/PackageManager/getComponentEnabledSetting(Landroid/content/ComponentName;)I
iconst_2
if_icmpeq L29
L9:
aload 2
ldc "xexe"
invokevirtual android/content/Intent/getStringExtra(Ljava/lang/String;)Ljava/lang/String;
astore 1
L10:
aload 1
ifnull L30
aload 1
ldc "lp"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L31
L30:
iconst_1
ireturn
L11:
astore 1
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "skip inapp xposed queryIntentServices"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aconst_null
astore 1
goto L10
L31:
iconst_0
ireturn
L29:
iload 3
iconst_1
if_icmpne L28
aload 0
getfield com/chelpus/XSupport/PMcontext Landroid/content/Context;
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
new android/content/ComponentName
dup
getstatic com/chelpus/Common/PACKAGE_NAME Ljava/lang/String;
ldc com/google/android/finsky/services/LicensingService
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokespecial android/content/ComponentName/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/content/pm/PackageManager/getComponentEnabledSetting(Landroid/content/ComponentName;)I
iconst_2
if_icmpeq L28
L12:
aload 2
ldc "xexe"
invokevirtual android/content/Intent/getStringExtra(Ljava/lang/String;)Ljava/lang/String;
astore 1
L13:
aload 1
ifnull L32
aload 1
ldc "lp"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L33
L32:
iconst_1
ireturn
L14:
astore 1
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "skip inapp xposed queryIntentServices"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aconst_null
astore 1
goto L13
L33:
iconst_0
ireturn
L28:
iconst_0
ireturn
.limit locals 6
.limit stack 5
.end method

.method public handleLoadPackage(Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;)V
.throws java/lang/Throwable
ldc com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment
invokevirtual java/lang/Class/getPackage()Ljava/lang/Package;
invokevirtual java/lang/Package/getName()Ljava/lang/String;
aload 1
getfield de/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam/packageName Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
ldc "com.chelpus.Utils"
aload 1
getfield de/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam/classLoader Ljava/lang/ClassLoader;
ldc "isXposedEnabled"
iconst_1
anewarray java/lang/Object
dup
iconst_0
new com/chelpus/XSupport$7
dup
aload 0
invokespecial com/chelpus/XSupport$7/<init>(Lcom/chelpus/XSupport;)V
aastore
invokestatic de/robv/android/xposed/XposedHelpers/findAndHookMethod(Ljava/lang/String;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Object;)Lde/robv/android/xposed/XC_MethodHook$Unhook;
pop
L0:
ldc "android.content.ContextWrapper"
aconst_null
ldc "bindService"
iconst_4
anewarray java/lang/Object
dup
iconst_0
ldc android/content/Intent
aastore
dup
iconst_1
ldc android/content/ServiceConnection
aastore
dup
iconst_2
getstatic java/lang/Integer/TYPE Ljava/lang/Class;
aastore
dup
iconst_3
new com/chelpus/XSupport$8
dup
aload 0
invokespecial com/chelpus/XSupport$8/<init>(Lcom/chelpus/XSupport;)V
aastore
invokestatic de/robv/android/xposed/XposedHelpers/findAndHookMethod(Ljava/lang/String;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Object;)Lde/robv/android/xposed/XC_MethodHook$Unhook;
pop
ldc "android.app.ContextImpl"
aload 1
getfield de/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam/classLoader Ljava/lang/ClassLoader;
invokestatic de/robv/android/xposed/XposedHelpers/findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;
ldc "queryIntentServices"
new com/chelpus/XSupport$9
dup
aload 0
invokespecial com/chelpus/XSupport$9/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
ldc "android"
aload 1
getfield de/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam/packageName Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L1
aload 1
getfield de/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam/processName Ljava/lang/String;
ldc "android"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L1
getstatic android/os/Build$VERSION/SDK_INT I
bipush 10
if_icmple L2
ldc "com.android.server.pm.PackageManagerService"
aload 1
getfield de/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam/classLoader Ljava/lang/ClassLoader;
invokestatic de/robv/android/xposed/XposedHelpers/findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;
astore 2
ldc "com.android.server.pm.PackageManagerService"
aload 1
getfield de/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam/classLoader Ljava/lang/ClassLoader;
invokestatic de/robv/android/xposed/XposedHelpers/findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;
new com/chelpus/XSupport$10
dup
aload 0
invokespecial com/chelpus/XSupport$10/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllConstructors(Ljava/lang/Class;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
getstatic com/chelpus/Common/LOLLIPOP_NEWER Z
ifeq L3
aload 2
ldc "installPackageAsUser"
new com/chelpus/XSupport$11
dup
aload 0
invokespecial com/chelpus/XSupport$11/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
L4:
ldc "com.android.server.pm.PackageManagerService"
aload 1
getfield de/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam/classLoader Ljava/lang/ClassLoader;
invokestatic de/robv/android/xposed/XposedHelpers/findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;
ldc "queryIntentServices"
new com/chelpus/XSupport$14
dup
aload 0
invokespecial com/chelpus/XSupport$14/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
getstatic com/chelpus/Common/LOLLIPOP_NEWER Z
ifeq L5
aload 2
ldc "checkUpgradeKeySetLP"
new com/chelpus/XSupport$15
dup
aload 0
invokespecial com/chelpus/XSupport$15/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
L5:
aload 2
ldc "verifySignaturesLP"
new com/chelpus/XSupport$16
dup
aload 0
invokespecial com/chelpus/XSupport$16/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
aload 2
ldc "compareSignatures"
new com/chelpus/XSupport$17
dup
aload 0
invokespecial com/chelpus/XSupport$17/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
aload 2
ldc "getPackageInfo"
new com/chelpus/XSupport$18
dup
aload 0
invokespecial com/chelpus/XSupport$18/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
aload 2
ldc "getApplicationInfo"
new com/chelpus/XSupport$19
dup
aload 0
invokespecial com/chelpus/XSupport$19/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
aload 2
ldc "generatePackageInfo"
new com/chelpus/XSupport$20
dup
aload 0
invokespecial com/chelpus/XSupport$20/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
aload 2
ldc "getInstalledApplications"
new com/chelpus/XSupport$21
dup
aload 0
invokespecial com/chelpus/XSupport$21/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
aload 2
ldc "getInstalledPackages"
new com/chelpus/XSupport$22
dup
aload 0
invokespecial com/chelpus/XSupport$22/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
aload 2
ldc "getPreferredPackages"
new com/chelpus/XSupport$23
dup
aload 0
invokespecial com/chelpus/XSupport$23/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
L1:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 10
if_icmple L6
ldc "android.app.ApplicationPackageManager"
aload 1
getfield de/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam/classLoader Ljava/lang/ClassLoader;
invokestatic de/robv/android/xposed/XposedHelpers/findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;
new com/chelpus/XSupport$34
dup
aload 0
invokespecial com/chelpus/XSupport$34/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllConstructors(Ljava/lang/Class;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
ldc "android.app.ApplicationPackageManager"
aload 1
getfield de/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam/classLoader Ljava/lang/ClassLoader;
invokestatic de/robv/android/xposed/XposedHelpers/findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;
ldc "getPackageInfo"
new com/chelpus/XSupport$35
dup
aload 0
invokespecial com/chelpus/XSupport$35/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
ldc "android.app.ApplicationPackageManager"
aload 1
getfield de/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam/classLoader Ljava/lang/ClassLoader;
invokestatic de/robv/android/xposed/XposedHelpers/findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;
ldc "getApplicationInfo"
new com/chelpus/XSupport$36
dup
aload 0
invokespecial com/chelpus/XSupport$36/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
ldc "android.app.ApplicationPackageManager"
aload 1
getfield de/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam/classLoader Ljava/lang/ClassLoader;
invokestatic de/robv/android/xposed/XposedHelpers/findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;
ldc "getInstalledApplications"
new com/chelpus/XSupport$37
dup
aload 0
invokespecial com/chelpus/XSupport$37/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
ldc "android.app.ApplicationPackageManager"
aload 1
getfield de/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam/classLoader Ljava/lang/ClassLoader;
invokestatic de/robv/android/xposed/XposedHelpers/findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;
ldc "getInstalledPackages"
new com/chelpus/XSupport$38
dup
aload 0
invokespecial com/chelpus/XSupport$38/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
ldc "android.app.ApplicationPackageManager"
aload 1
getfield de/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam/classLoader Ljava/lang/ClassLoader;
invokestatic de/robv/android/xposed/XposedHelpers/findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;
ldc "getPreferredPackages"
new com/chelpus/XSupport$39
dup
aload 0
invokespecial com/chelpus/XSupport$39/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
L6:
return
L3:
getstatic com/chelpus/Common/JB_MR1_NEWER Z
ifeq L7
aload 2
ldc "installPackageWithVerificationAndEncryption"
new com/chelpus/XSupport$12
dup
aload 0
invokespecial com/chelpus/XSupport$12/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
goto L4
L7:
aload 2
ldc "installPackageWithVerification"
new com/chelpus/XSupport$13
dup
aload 0
invokespecial com/chelpus/XSupport$13/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
goto L4
L2:
ldc "com.android.server.PackageManagerService"
aload 1
getfield de/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam/classLoader Ljava/lang/ClassLoader;
invokestatic de/robv/android/xposed/XposedHelpers/findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;
new com/chelpus/XSupport$24
dup
aload 0
invokespecial com/chelpus/XSupport$24/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllConstructors(Ljava/lang/Class;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
ldc "com.android.server.PackageManagerService"
aload 1
getfield de/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam/classLoader Ljava/lang/ClassLoader;
invokestatic de/robv/android/xposed/XposedHelpers/findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;
ldc "getPackageInfo"
new com/chelpus/XSupport$25
dup
aload 0
invokespecial com/chelpus/XSupport$25/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
ldc "com.android.server.PackageManagerService"
aload 1
getfield de/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam/classLoader Ljava/lang/ClassLoader;
invokestatic de/robv/android/xposed/XposedHelpers/findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;
ldc "getApplicationInfo"
new com/chelpus/XSupport$26
dup
aload 0
invokespecial com/chelpus/XSupport$26/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
ldc "android.content.pm.PackageParser"
aload 1
getfield de/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam/classLoader Ljava/lang/ClassLoader;
invokestatic de/robv/android/xposed/XposedHelpers/findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;
ldc "generatePackageInfo"
new com/chelpus/XSupport$27
dup
aload 0
invokespecial com/chelpus/XSupport$27/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
ldc "android.content.pm.PackageParser"
aload 1
getfield de/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam/classLoader Ljava/lang/ClassLoader;
invokestatic de/robv/android/xposed/XposedHelpers/findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;
ldc "generateApplicationInfo"
new com/chelpus/XSupport$28
dup
aload 0
invokespecial com/chelpus/XSupport$28/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
ldc "com.android.server.PackageManagerService"
aload 1
getfield de/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam/classLoader Ljava/lang/ClassLoader;
invokestatic de/robv/android/xposed/XposedHelpers/findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;
ldc "getInstalledApplications"
new com/chelpus/XSupport$29
dup
aload 0
invokespecial com/chelpus/XSupport$29/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
ldc "com.android.server.PackageManagerService"
aload 1
getfield de/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam/classLoader Ljava/lang/ClassLoader;
invokestatic de/robv/android/xposed/XposedHelpers/findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;
ldc "getInstalledPackages"
new com/chelpus/XSupport$30
dup
aload 0
invokespecial com/chelpus/XSupport$30/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
ldc "com.android.server.PackageManagerService"
aload 1
getfield de/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam/classLoader Ljava/lang/ClassLoader;
invokestatic de/robv/android/xposed/XposedHelpers/findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;
ldc "getPreferredPackages"
new com/chelpus/XSupport$31
dup
aload 0
invokespecial com/chelpus/XSupport$31/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
ldc "com.android.server.PackageManagerService"
aload 1
getfield de/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam/classLoader Ljava/lang/ClassLoader;
invokestatic de/robv/android/xposed/XposedHelpers/findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;
ldc "checkSignaturesLP"
new com/chelpus/XSupport$32
dup
aload 0
invokespecial com/chelpus/XSupport$32/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
ldc "com.android.server.PackageManagerService"
aload 1
getfield de/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam/classLoader Ljava/lang/ClassLoader;
invokestatic de/robv/android/xposed/XposedHelpers/findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;
ldc "queryIntentServices"
new com/chelpus/XSupport$33
dup
aload 0
invokespecial com/chelpus/XSupport$33/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
goto L1
.limit locals 3
.limit stack 9
.end method

.method public initZygote(Lde/robv/android/xposed/IXposedHookZygoteInit$StartupParam;)V
.throws java/lang/Throwable
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
.catch java/lang/ClassNotFoundException from L6 to L7 using L8
ldc "android.content.pm.PackageParser"
aconst_null
invokestatic de/robv/android/xposed/XposedHelpers/findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;
pop
ldc "java.util.jar.JarVerifier$VerifierEntry"
aconst_null
invokestatic de/robv/android/xposed/XposedHelpers/findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;
pop
aload 0
iconst_1
putfield com/chelpus/XSupport/disableCheckSignatures Z
getstatic android/os/Build$VERSION/SDK_INT I
bipush 18
if_icmple L1
L0:
ldc "com.android.org.conscrypt.OpenSSLSignature"
aconst_null
invokestatic de/robv/android/xposed/XposedHelpers/findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;
ldc "engineVerify"
new com/chelpus/XSupport$1
dup
aload 0
invokespecial com/chelpus/XSupport$1/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
L1:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmple L4
getstatic android/os/Build$VERSION/SDK_INT I
bipush 19
if_icmpge L4
L3:
ldc "org.apache.harmony.xnet.provider.jsse.OpenSSLSignature"
aconst_null
invokestatic de/robv/android/xposed/XposedHelpers/findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;
ldc "engineVerify"
new com/chelpus/XSupport$2
dup
aload 0
invokespecial com/chelpus/XSupport$2/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
L4:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 10
if_icmpne L7
L6:
ldc "org.bouncycastle.jce.provider.JDKDigestSignature"
aconst_null
invokestatic de/robv/android/xposed/XposedHelpers/findClass(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Class;
ldc "engineVerify"
new com/chelpus/XSupport$3
dup
aload 0
invokespecial com/chelpus/XSupport$3/<init>(Lcom/chelpus/XSupport;)V
invokestatic de/robv/android/xposed/XposedBridge/hookAllMethods(Ljava/lang/Class;Ljava/lang/String;Lde/robv/android/xposed/XC_MethodHook;)Ljava/util/Set;
pop
L7:
ldc "java.security.MessageDigest"
aconst_null
ldc "isEqual"
iconst_3
anewarray java/lang/Object
dup
iconst_0
ldc [B
aastore
dup
iconst_1
ldc [B
aastore
dup
iconst_2
new com/chelpus/XSupport$4
dup
aload 0
invokespecial com/chelpus/XSupport$4/<init>(Lcom/chelpus/XSupport;)V
aastore
invokestatic de/robv/android/xposed/XposedHelpers/findAndHookMethod(Ljava/lang/String;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Object;)Lde/robv/android/xposed/XC_MethodHook$Unhook;
pop
ldc "java.security.Signature"
aconst_null
ldc "verify"
iconst_2
anewarray java/lang/Object
dup
iconst_0
ldc [B
aastore
dup
iconst_1
new com/chelpus/XSupport$5
dup
aload 0
invokespecial com/chelpus/XSupport$5/<init>(Lcom/chelpus/XSupport;)V
aastore
invokestatic de/robv/android/xposed/XposedHelpers/findAndHookMethod(Ljava/lang/String;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Object;)Lde/robv/android/xposed/XC_MethodHook$Unhook;
pop
ldc "java.security.Signature"
aconst_null
ldc "verify"
iconst_4
anewarray java/lang/Object
dup
iconst_0
ldc [B
aastore
dup
iconst_1
getstatic java/lang/Integer/TYPE Ljava/lang/Class;
aastore
dup
iconst_2
getstatic java/lang/Integer/TYPE Ljava/lang/Class;
aastore
dup
iconst_3
new com/chelpus/XSupport$6
dup
aload 0
invokespecial com/chelpus/XSupport$6/<init>(Lcom/chelpus/XSupport;)V
aastore
invokestatic de/robv/android/xposed/XposedHelpers/findAndHookMethod(Ljava/lang/String;Ljava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Object;)Lde/robv/android/xposed/XC_MethodHook$Unhook;
pop
return
L8:
astore 1
goto L7
L5:
astore 1
goto L4
L2:
astore 1
goto L1
.limit locals 2
.limit stack 9
.end method

.method public loadPrefs()V
.catch org/json/JSONException from L0 to L1 using L2
aload 0
getfield com/chelpus/XSupport/initialize J
lconst_0
lcmp
ifeq L3
aload 0
getfield com/chelpus/XSupport/initialize J
new java/io/File
dup
ldc "/data/lp/xposed"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/lastModified()J
lcmp
ifeq L4
L3:
aload 0
getfield com/chelpus/XSupport/initialize J
lconst_0
lcmp
ifeq L5
aload 0
getfield com/chelpus/XSupport/initialize J
new java/io/File
dup
ldc "/data/lp/xposed"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/lastModified()J
lcmp
ifeq L5
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Update settings xposed"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L5:
aload 0
new java/io/File
dup
ldc "/data/lp/xposed"
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/lastModified()J
putfield com/chelpus/XSupport/initialize J
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
pop
aconst_null
astore 1
L0:
invokestatic com/chelpus/Utils/readXposedParamBoolean()Lorg/json/JSONObject;
astore 2
L1:
aload 2
astore 1
L6:
aload 1
ifnull L4
aload 1
ldc "patch1"
iconst_1
invokevirtual org/json/JSONObject/optBoolean(Ljava/lang/String;Z)Z
putstatic com/chelpus/XSupport/patch1 Z
aload 1
ldc "patch2"
iconst_1
invokevirtual org/json/JSONObject/optBoolean(Ljava/lang/String;Z)Z
putstatic com/chelpus/XSupport/patch2 Z
aload 1
ldc "patch3"
iconst_1
invokevirtual org/json/JSONObject/optBoolean(Ljava/lang/String;Z)Z
putstatic com/chelpus/XSupport/patch3 Z
aload 1
ldc "patch4"
iconst_1
invokevirtual org/json/JSONObject/optBoolean(Ljava/lang/String;Z)Z
putstatic com/chelpus/XSupport/patch4 Z
aload 1
ldc "hide"
iconst_0
invokevirtual org/json/JSONObject/optBoolean(Ljava/lang/String;Z)Z
putstatic com/chelpus/XSupport/hide Z
aload 1
ldc "module_on"
iconst_1
invokevirtual org/json/JSONObject/optBoolean(Ljava/lang/String;Z)Z
putstatic com/chelpus/XSupport/enable Z
L4:
return
L2:
astore 2
aload 2
invokevirtual org/json/JSONException/printStackTrace()V
goto L6
.limit locals 3
.limit stack 5
.end method
