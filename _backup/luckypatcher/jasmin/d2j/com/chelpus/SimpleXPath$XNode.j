.bytecode 50.0
.class public synchronized com/chelpus/SimpleXPath$XNode
.super java/lang/Object
.inner class public XNode inner com/chelpus/SimpleXPath$XNode outer com/chelpus/SimpleXPath

.field 'mNode' Lorg/w3c/dom/Node;

.field final synthetic 'this$0' Lcom/chelpus/SimpleXPath;

.method public <init>(Lcom/chelpus/SimpleXPath;Lorg/w3c/dom/Node;)V
aload 0
aload 1
putfield com/chelpus/SimpleXPath$XNode/this$0 Lcom/chelpus/SimpleXPath;
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 2
putfield com/chelpus/SimpleXPath$XNode/mNode Lorg/w3c/dom/Node;
return
.limit locals 3
.limit stack 2
.end method

.method getAttribute(Ljava/lang/String;)Ljava/lang/String;
.throws com/chelpus/XNodeException
aload 0
getfield com/chelpus/SimpleXPath$XNode/mNode Lorg/w3c/dom/Node;
checkcast org/w3c/dom/Element
aload 1
invokeinterface org/w3c/dom/Element/getAttribute(Ljava/lang/String;)Ljava/lang/String; 1
astore 2
aload 2
invokevirtual java/lang/String/length()I
ifne L0
new com/chelpus/XNodeException
dup
ldc "Node [%s] has no [%s] attribute!"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
invokevirtual com/chelpus/SimpleXPath$XNode/getName()Ljava/lang/String;
aastore
dup
iconst_1
aload 1
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokespecial com/chelpus/XNodeException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 2
areturn
.limit locals 3
.limit stack 7
.end method

.method getChild(Ljava/lang/String;)Lcom/chelpus/SimpleXPath$XNode;
.throws com/chelpus/XNodeException
aload 0
getfield com/chelpus/SimpleXPath$XNode/this$0 Lcom/chelpus/SimpleXPath;
aload 0
getfield com/chelpus/SimpleXPath$XNode/mNode Lorg/w3c/dom/Node;
aload 1
invokevirtual com/chelpus/SimpleXPath/getXPathNodes(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/util/List;
astore 2
aload 2
invokeinterface java/util/List/size()I 0
ifne L0
new com/chelpus/XNodeException
dup
ldc "Node [%s] has no child at path [%s]!"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/chelpus/SimpleXPath$XNode/mNode Lorg/w3c/dom/Node;
invokeinterface org/w3c/dom/Node/getNodeName()Ljava/lang/String; 0
aastore
dup
iconst_1
aload 1
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokespecial com/chelpus/XNodeException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 2
iconst_0
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/chelpus/SimpleXPath$XNode
areturn
.limit locals 3
.limit stack 7
.end method

.method getChildren(Ljava/lang/String;)Ljava/util/List;
.signature "(Ljava/lang/String;)Ljava/util/List<Lcom/chelpus/SimpleXPath$XNode;>;"
.throws com/chelpus/XNodeException
aload 0
getfield com/chelpus/SimpleXPath$XNode/this$0 Lcom/chelpus/SimpleXPath;
aload 0
getfield com/chelpus/SimpleXPath$XNode/mNode Lorg/w3c/dom/Node;
aload 1
invokevirtual com/chelpus/SimpleXPath/getXPathNodes(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/util/List;
astore 2
aload 2
invokeinterface java/util/List/isEmpty()Z 0
ifeq L0
new com/chelpus/XNodeException
dup
ldc "Node [%s] has no children at path [%s]!"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/chelpus/SimpleXPath$XNode/mNode Lorg/w3c/dom/Node;
invokeinterface org/w3c/dom/Node/getNodeName()Ljava/lang/String; 0
aastore
dup
iconst_1
aload 1
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokespecial com/chelpus/XNodeException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 2
areturn
.limit locals 3
.limit stack 7
.end method

.method getFloatAttribute(Ljava/lang/String;)F
.throws com/chelpus/XNodeException
aload 0
aload 1
invokevirtual com/chelpus/SimpleXPath$XNode/getAttribute(Ljava/lang/String;)Ljava/lang/String;
invokestatic java/lang/Float/parseFloat(Ljava/lang/String;)F
freturn
.limit locals 2
.limit stack 2
.end method

.method public getIntAttribute(Ljava/lang/String;)I
.throws com/chelpus/XNodeException
aload 0
aload 1
invokevirtual com/chelpus/SimpleXPath$XNode/getAttribute(Ljava/lang/String;)Ljava/lang/String;
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
ireturn
.limit locals 2
.limit stack 2
.end method

.method public getName()Ljava/lang/String;
aload 0
getfield com/chelpus/SimpleXPath$XNode/mNode Lorg/w3c/dom/Node;
invokeinterface org/w3c/dom/Node/getNodeName()Ljava/lang/String; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public getStringAttribute(Ljava/lang/String;)Ljava/lang/String;
.throws com/chelpus/XNodeException
aload 0
aload 1
invokevirtual com/chelpus/SimpleXPath$XNode/getAttribute(Ljava/lang/String;)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method public getValue()Ljava/lang/String;
aload 0
getfield com/chelpus/SimpleXPath$XNode/mNode Lorg/w3c/dom/Node;
invokeinterface org/w3c/dom/Node/getChildNodes()Lorg/w3c/dom/NodeList; 0
iconst_0
invokeinterface org/w3c/dom/NodeList/item(I)Lorg/w3c/dom/Node; 1
invokeinterface org/w3c/dom/Node/getNodeValue()Ljava/lang/String; 0
areturn
.limit locals 1
.limit stack 2
.end method
