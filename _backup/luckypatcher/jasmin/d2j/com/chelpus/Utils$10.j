.bytecode 50.0
.class final synchronized com/chelpus/Utils$10
.super java/lang/Object
.implements java/lang/Runnable
.enclosing method com/chelpus/Utils/checkRoot(Ljava/lang/Boolean;Ljava/lang/String;)Z
.inner class static final inner com/chelpus/Utils$10

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public run()V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L2
.catch java/lang/Exception from L7 to L8 using L2
.catch java/lang/Exception from L9 to L10 using L2
.catch java/lang/Exception from L11 to L12 using L2
L0:
ldc "/system/bin/su"
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifeq L7
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/su Z
ifeq L3
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "LuckyPatcher: skip root test."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L1:
return
L3:
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "stat -c %a /system/bin/su"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
invokestatic com/chelpus/Utils/access$400()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " stat -c %a /system/bin/su"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
ldc "busybox stat -c %a /system/bin/su"
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
astore 1
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher (chek root): get permissions "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " /system/bin/su"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 1
ldc "6755"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L5
aload 1
ldc "[0-9]"
invokevirtual java/lang/String/matches(Ljava/lang/String;)Z
ifeq L5
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "LuckyPatcher (chek root): Permissions /system/bin/su not correct."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
ldc "/system"
ldc "rw"
invokestatic com/chelpus/Utils/remount(Ljava/lang/String;Ljava/lang/String;)Z
pop
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "chmod 06755 /system/bin/su"
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
pop
ldc "/system"
ldc "ro"
invokestatic com/chelpus/Utils/remount(Ljava/lang/String;Ljava/lang/String;)Z
pop
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "stat -c %a /system/bin/su"
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
astore 1
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher (chek root): "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " /system/bin/su"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 1
ldc "6755"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L12
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "LuckyPatcher (chek root): permission /system/bin/su set 06755"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L4:
return
L2:
astore 1
aload 1
invokevirtual java/lang/Exception/printStackTrace()V
return
L5:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "LuckyPatcher (chek root): Permissions is true.(/system/bin/su)"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L6:
return
L7:
ldc "/system/xbin/su"
invokestatic com/chelpus/Utils/exists(Ljava/lang/String;)Z
ifeq L12
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "chmod 06777 internalBusybox"
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
pop
getstatic com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment/su Z
ifeq L9
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "LuckyPatcher: skip root test."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L8:
return
L9:
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_3
anewarray java/lang/String
dup
iconst_0
ldc "stat -c %a /system/xbin/su"
aastore
dup
iconst_1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
invokestatic com/chelpus/Utils/access$400()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " stat -c %a /system/xbin/su"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aastore
dup
iconst_2
ldc "busybox stat -c %a /system/xbin/su"
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
astore 1
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher (chek root): get permissions "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " /system/xbin/su"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 1
ldc "6755"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L11
aload 1
ldc "[0-9]"
invokevirtual java/lang/String/matches(Ljava/lang/String;)Z
ifeq L11
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "LuckyPatcher (chek root): Permissions /system/xbin/su not correct."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
ldc "/system"
ldc "rw"
invokestatic com/chelpus/Utils/remount(Ljava/lang/String;Ljava/lang/String;)Z
pop
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "chmod 06755 /system/xbin/su"
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
pop
ldc "/system"
ldc "ro"
invokestatic com/chelpus/Utils/remount(Ljava/lang/String;Ljava/lang/String;)Z
pop
new com/chelpus/Utils
dup
ldc ""
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "stat -c %a /system/xbin/su"
aastore
invokevirtual com/chelpus/Utils/cmdRoot([Ljava/lang/String;)Ljava/lang/String;
astore 1
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "LuckyPatcher (chek root): "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " /system/xbin/su"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 1
ldc "6755"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L12
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "LuckyPatcher (chek root): permission /system/xbin/su set 06755"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L10:
return
L11:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "LuckyPatcher (chek root): Permissions is true.(/system/xbin/su)"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L12:
return
.limit locals 2
.limit stack 6
.end method
