.bytecode 50.0
.class public synchronized com/chelpus/Common
.super java/lang/Object

.field public static final 'ACTION_BACKUP_APK_FILE' Ljava/lang/String; = "xinstaller.intent.action.BACKUP_APK_FILE"

.field public static final 'ACTION_BACKUP_PREFERENCES' Ljava/lang/String; = "xinstaller.intent.action.BACKUP_PREFERENCES"

.field public static final 'ACTION_DELETE_APK_FILE' Ljava/lang/String; = "xinstaller.intent.action.DELETE_APK_FILE"

.field public static final 'ACTION_RESET_PREFERENCES' Ljava/lang/String; = "xinstaller.intent.action.RESET_PREFERENCES"

.field public static final 'ACTION_RESTORE_PREFERENCES' Ljava/lang/String; = "xinstaller.intent.action.RESTORE_PREFERENCES"

.field public static final 'ACTION_UNINSTALL_SYSTEM_APP' Ljava/lang/String; = "xinstaller.intent.action.UNINSTALL_SYSTEM_APP"

.field public static final 'ANDROID_PKG' Ljava/lang/String; = "android"

.field public static final 'BACKUPCONFIRM_PKG' Ljava/lang/String; = "com.android.backupconfirm"

.field public static final 'BACKUPRESTORECONFIRMATION' Ljava/lang/String; = "com.android.backupconfirm.BackupRestoreConfirmation"

.field public static final 'CANBEONSDCARDCHECKER' Ljava/lang/String; = "com.android.settings.applications.CanBeOnSdCardChecker"

.field public static final 'DEBUG_ENABLE_DEBUGGER' I = 1


.field public static final 'DELETE_KEEP_DATA' I = 1


.field public static final 'DEVICEPOLICYMANAGERSERVICE' Ljava/lang/String;

.field public static final 'FDROIDAPPDETAILS' Ljava/lang/String; = "org.fdroid.fdroid.AppDetails"

.field public static final 'FDROID_PKG' Ljava/lang/String; = "org.fdroid.fdroid"

.field public static final 'FILE' Ljava/lang/String; = "file"

.field public static final 'GOOGLEPLAY_PKG' Ljava/lang/String; = "com.android.vending"

.field public static final 'INSTALLAPPPROGRESS' Ljava/lang/String; = "com.android.packageinstaller.InstallAppProgress"

.field public static final 'INSTALLEDAPPDETAILS' Ljava/lang/String; = "com.android.settings.applications.InstalledAppDetails"

.field public static final 'INSTALL_ALLOW_DOWNGRADE' I = 128


.field public static final 'INSTALL_EXTERNAL' I = 8


.field public static final 'INSTALL_FORWARD_LOCK' I = 1


.field public static final 'INSTALL_SUCCEEDED' I = 1


.field public static final 'InAppServ' I = 0


.field public static final 'JARVERIFIER' Ljava/lang/String; = "java.util.jar.JarVerifier$VerifierEntry"

.field public static final 'JB_MR1_NEWER' Z

.field public static final 'JB_MR2_NEWER' Z

.field public static final 'JB_NEWER' Z

.field public static final 'KITKAT_NEWER' Z

.field public static final 'LATEST_ANDROID_RELEASE' I = 21


.field public static final 'LOLLIPOP_NEWER' Z

.field public static final 'LicServ' I = 1


.field public static final 'PACKAGE' Ljava/lang/String; = "package"

.field public static final 'PACKAGEINSTALLERACTIVITY' Ljava/lang/String; = "com.android.packageinstaller.PackageInstallerActivity"

.field public static final 'PACKAGEINSTALLER_PKG' Ljava/lang/String; = "com.android.packageinstaller"

.field public static final 'PACKAGEMANAGERREPOSITORY' Ljava/lang/String; = "com.google.android.finsky.appstate.PackageManagerRepository"

.field public static final 'PACKAGEMANAGERSERVICE' Ljava/lang/String; = "com.android.server.pm.PackageManagerService"

.field public static final 'PACKAGEPARSER' Ljava/lang/String; = "android.content.pm.PackageParser"

.field public static final 'PACKAGE_NAME' Ljava/lang/String;

.field public static final 'PACKAGE_PREFERENCES' Ljava/lang/String;

.field public static final 'PACKAGE_TAG' Ljava/lang/String; = "XInstaller"

.field public static final 'PMcontext' I = 2


.field public static final 'PREFERENCE' Ljava/lang/String; = "preference"

.field public static final 'PREF_DISABLE_CHECK_DUPLICATED_PERMISSION' Ljava/lang/String; = "disable_check_duplicated_permissions"

.field public static final 'PREF_DISABLE_CHECK_PERMISSION' Ljava/lang/String; = "disable_check_permissions"

.field public static final 'PREF_DISABLE_CHECK_SDK_VERSION' Ljava/lang/String; = "disable_check_sdk_version"

.field public static final 'PREF_DISABLE_CHECK_SIGNATURE' Ljava/lang/String; = "disable_check_signatures"

.field public static final 'PREF_DISABLE_CHECK_SIGNATURE_FDROID' Ljava/lang/String; = "disable_check_signatures_fdroid"

.field public static final 'PREF_DISABLE_FORWARD_LOCK' Ljava/lang/String; = "disable_forward_lock"

.field public static final 'PREF_DISABLE_INSTALL_BACKGROUND' Ljava/lang/String; = "disable_install_background"

.field public static final 'PREF_DISABLE_UNINSTALL_BACKGROUND' Ljava/lang/String; = "disable_uninstall_background"

.field public static final 'PREF_DISABLE_VERIFY_APP' Ljava/lang/String; = "disable_verify_apps"

.field public static final 'PREF_DISABLE_VERIFY_JAR' Ljava/lang/String; = "disable_verify_jar"

.field public static final 'PREF_DISABLE_VERIFY_SIGNATURE' Ljava/lang/String; = "disable_verify_signatures"

.field public static final 'PREF_ENABLED_DOWNGRADE_APP' Ljava/lang/String; = "enable_downgrade_apps"

.field public static final 'PREF_ENABLE_AUTO_BACKUP' Ljava/lang/String; = "enable_auto_backup"

.field public static final 'PREF_ENABLE_AUTO_CLOSE_INSTALL' Ljava/lang/String; = "enable_auto_close_install"

.field public static final 'PREF_ENABLE_AUTO_CLOSE_UNINSTALL' Ljava/lang/String; = "enable_auto_close_uninstall"

.field public static final 'PREF_ENABLE_AUTO_ENABLE_CLEAR_BUTTON' Ljava/lang/String; = "enable_auto_enable_clear_buttons"

.field public static final 'PREF_ENABLE_AUTO_HIDE_INSTALL' Ljava/lang/String; = "enable_auto_hide_install"

.field public static final 'PREF_ENABLE_AUTO_INSTALL' Ljava/lang/String; = "enable_auto_install"

.field public static final 'PREF_ENABLE_AUTO_UNINSTALL' Ljava/lang/String; = "enable_auto_uninstall"

.field public static final 'PREF_ENABLE_BACKUP_APK_FILE' Ljava/lang/String; = "enable_backup_apk_files"

.field public static final 'PREF_ENABLE_BACKUP_APP_PACKAGE' Ljava/lang/String; = "enable_backup_app_packages"

.field public static final 'PREF_ENABLE_DEBUG_APP' Ljava/lang/String; = "enable_debug_apps"

.field public static final 'PREF_ENABLE_DELETE_APK_FILE_INSTALL' Ljava/lang/String; = "enable_delete_apk_files_install"

.field public static final 'PREF_ENABLE_DISABLE_SYSTEM_APP' Ljava/lang/String; = "enable_disable_system_apps"

.field public static final 'PREF_ENABLE_EXPERT_MODE' Ljava/lang/String; = "enable_expert_mode"

.field public static final 'PREF_ENABLE_EXPORT_APP' Ljava/lang/String; = "enable_export_apps"

.field public static final 'PREF_ENABLE_INSTALL_EXTERNAL_STORAGE' Ljava/lang/String; = "enable_install_external_storage"

.field public static final 'PREF_ENABLE_INSTALL_UNKNOWN_APP' Ljava/lang/String; = "enable_install_unknown_apps"

.field public static final 'PREF_ENABLE_INSTALL_UNSIGNED_APP' Ljava/lang/String; = "enable_install_unsigned_apps"

.field public static final 'PREF_ENABLE_KEEP_APP_DATA' Ljava/lang/String; = "enable_keep_apps_data"

.field public static final 'PREF_ENABLE_LAUNCH_APP' Ljava/lang/String; = "enable_launch_apps"

.field public static final 'PREF_ENABLE_LAUNCH_INSTALL' Ljava/lang/String; = "enable_auto_launch_install"

.field public static final 'PREF_ENABLE_MODULE' Ljava/lang/String; = "enable_module"

.field public static final 'PREF_ENABLE_MOVE_APP' Ljava/lang/String; = "enable_move_apps"

.field public static final 'PREF_ENABLE_OPEN_APP_GOOGLE_PLAY' Ljava/lang/String; = "enable_open_apps_google_play"

.field public static final 'PREF_ENABLE_SHOW_BUTTON' Ljava/lang/String; = "enable_show_buttons"

.field public static final 'PREF_ENABLE_SHOW_PACKAGE_NAME' Ljava/lang/String; = "enable_show_package_name"

.field public static final 'PREF_ENABLE_SHOW_VERSION' Ljava/lang/String; = "enable_show_version"

.field public static final 'PREF_ENABLE_UNINSTALL_DEVICE_ADMIN' Ljava/lang/String; = "enable_uninstall_device_admins"

.field public static final 'PREF_ENABLE_UNINSTALL_SYSTEM_APP' Ljava/lang/String; = "enable_uninstall_system_apps"

.field public static final 'ROOT_UID' I = 0


.field public static final 'SDK' I

.field public static final 'SETTINGS_PKG' Ljava/lang/String; = "com.android.settings"

.field public static final 'SIGNATURE' Ljava/lang/String; = "java.security.Signature"

.field public static final 'UNINSTALLAPPPROGRESS' Ljava/lang/String; = "com.android.packageinstaller.UninstallAppProgress"

.field public static final 'UNINSTALLERACTIVITY' Ljava/lang/String; = "com.android.packageinstaller.UninstallerActivity"

.field public static final 'UTILS' Ljava/lang/String; = "com.android.settings.Utils"

.field public static final 'VALUE' Ljava/lang/String; = "value"

.field public static final 'getPackageManagerF' I = 1


.field public static final 'mBase' I = 0


.method static <clinit>()V
iconst_1
istore 1
ldc com/android/vending/billing/InAppBillingService/LUCK/listAppsFragment
invokevirtual java/lang/Class/getPackage()Ljava/lang/Package;
invokevirtual java/lang/Package/getName()Ljava/lang/String;
putstatic com/chelpus/Common/PACKAGE_NAME Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/chelpus/Common/PACKAGE_NAME Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "_preferences"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
putstatic com/chelpus/Common/PACKAGE_PREFERENCES Ljava/lang/String;
getstatic android/os/Build$VERSION/SDK_INT I
putstatic com/chelpus/Common/SDK I
getstatic com/chelpus/Common/SDK I
bipush 16
if_icmplt L0
iconst_1
istore 0
L1:
iload 0
putstatic com/chelpus/Common/JB_NEWER Z
getstatic com/chelpus/Common/SDK I
bipush 17
if_icmplt L2
iconst_1
istore 0
L3:
iload 0
putstatic com/chelpus/Common/JB_MR1_NEWER Z
getstatic com/chelpus/Common/SDK I
bipush 18
if_icmplt L4
iconst_1
istore 0
L5:
iload 0
putstatic com/chelpus/Common/JB_MR2_NEWER Z
getstatic com/chelpus/Common/SDK I
bipush 19
if_icmplt L6
iconst_1
istore 0
L7:
iload 0
putstatic com/chelpus/Common/KITKAT_NEWER Z
getstatic com/chelpus/Common/SDK I
bipush 21
if_icmplt L8
iload 1
istore 0
L9:
iload 0
putstatic com/chelpus/Common/LOLLIPOP_NEWER Z
getstatic com/chelpus/Common/LOLLIPOP_NEWER Z
ifeq L10
ldc "com.android.server.devicepolicy.DevicePolicyManagerService"
astore 2
L11:
aload 2
putstatic com/chelpus/Common/DEVICEPOLICYMANAGERSERVICE Ljava/lang/String;
return
L0:
iconst_0
istore 0
goto L1
L2:
iconst_0
istore 0
goto L3
L4:
iconst_0
istore 0
goto L5
L6:
iconst_0
istore 0
goto L7
L8:
iconst_0
istore 0
goto L9
L10:
ldc "com.android.server.DevicePolicyManagerService"
astore 2
goto L11
.limit locals 3
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method
