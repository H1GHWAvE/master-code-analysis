.bytecode 50.0
.class public synchronized com/chelpus/SimpleXPath
.super java/lang/Object
.inner class public XNode inner com/chelpus/SimpleXPath$XNode outer com/chelpus/SimpleXPath

.field public static final 'XPATH_SEPARATOR' Ljava/lang/String; = "/"

.field 'mDocument' Lorg/w3c/dom/Document;

.method public <init>(Ljava/io/File;)V
.throws javax/xml/parsers/ParserConfigurationException
.throws org/xml/sax/SAXException
.throws java/io/IOException
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
invokestatic javax/xml/parsers/DocumentBuilderFactory/newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;
invokevirtual javax/xml/parsers/DocumentBuilderFactory/newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;
new java/io/FileInputStream
dup
aload 1
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
invokevirtual javax/xml/parsers/DocumentBuilder/parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;
putfield com/chelpus/SimpleXPath/mDocument Lorg/w3c/dom/Document;
return
.limit locals 2
.limit stack 5
.end method

.method private getNodes(Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/util/List;)V
.signature "(Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/util/List<Lcom/chelpus/SimpleXPath$XNode;>;)V"
.catch java/lang/Exception from L0 to L1 using L2
aload 2
ldc "/"
iconst_2
invokevirtual java/lang/String/split(Ljava/lang/String;I)[Ljava/lang/String;
astore 2
aload 2
arraylength
iconst_1
if_icmpne L3
iconst_1
istore 4
L4:
aload 1
invokeinterface org/w3c/dom/Node/getFirstChild()Lorg/w3c/dom/Node; 0
astore 1
L5:
aload 1
ifnull L6
aload 2
iconst_0
aaload
aload 1
invokeinterface org/w3c/dom/Node/getNodeName()Ljava/lang/String; 0
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
iload 4
ifeq L7
aload 3
new com/chelpus/SimpleXPath$XNode
dup
aload 0
aload 1
invokespecial com/chelpus/SimpleXPath$XNode/<init>(Lcom/chelpus/SimpleXPath;Lorg/w3c/dom/Node;)V
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L0:
aload 1
invokeinterface org/w3c/dom/Node/getNextSibling()Lorg/w3c/dom/Node; 0
astore 1
L1:
goto L5
L3:
iconst_0
istore 4
goto L4
L7:
aload 0
aload 1
aload 2
iconst_1
aaload
aload 3
invokespecial com/chelpus/SimpleXPath/getNodes(Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/util/List;)V
goto L0
L2:
astore 1
aconst_null
astore 1
goto L5
L6:
return
.limit locals 5
.limit stack 5
.end method

.method public getRoot()Lorg/w3c/dom/Node;
aload 0
getfield com/chelpus/SimpleXPath/mDocument Lorg/w3c/dom/Document;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getXPathNode(Lorg/w3c/dom/Node;Ljava/lang/String;)Lcom/chelpus/SimpleXPath$XNode;
.catch java/lang/Exception from L0 to L1 using L2
aconst_null
astore 4
aload 2
ldc "/"
iconst_2
invokevirtual java/lang/String/split(Ljava/lang/String;I)[Ljava/lang/String;
astore 5
aload 5
arraylength
iconst_1
if_icmpne L3
iconst_1
istore 3
L4:
aload 1
invokeinterface org/w3c/dom/Node/getFirstChild()Lorg/w3c/dom/Node; 0
astore 1
L5:
aload 4
astore 2
aload 1
ifnull L6
aload 5
iconst_0
aaload
aload 1
invokeinterface org/w3c/dom/Node/getNodeName()Ljava/lang/String; 0
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
iload 3
ifeq L7
new com/chelpus/SimpleXPath$XNode
dup
aload 0
aload 1
invokespecial com/chelpus/SimpleXPath$XNode/<init>(Lcom/chelpus/SimpleXPath;Lorg/w3c/dom/Node;)V
astore 2
L6:
aload 2
areturn
L3:
iconst_0
istore 3
goto L4
L7:
aload 0
aload 1
aload 5
iconst_1
aaload
invokevirtual com/chelpus/SimpleXPath/getXPathNode(Lorg/w3c/dom/Node;Ljava/lang/String;)Lcom/chelpus/SimpleXPath$XNode;
areturn
L0:
aload 1
invokeinterface org/w3c/dom/Node/getNextSibling()Lorg/w3c/dom/Node; 0
astore 1
L1:
goto L5
L2:
astore 1
aconst_null
astore 1
goto L5
.limit locals 6
.limit stack 4
.end method

.method public getXPathNodes(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/util/List;
.signature "(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/util/List<Lcom/chelpus/SimpleXPath$XNode;>;"
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 3
aload 0
aload 1
aload 2
aload 3
invokespecial com/chelpus/SimpleXPath/getNodes(Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/util/List;)V
aload 3
areturn
.limit locals 4
.limit stack 4
.end method
