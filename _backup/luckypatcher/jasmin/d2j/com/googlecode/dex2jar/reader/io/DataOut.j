.bytecode 50.0
.class public abstract interface com/googlecode/dex2jar/reader/io/DataOut
.super java/lang/Object

.method public abstract writeByte(I)V
.throws java/io/IOException
.end method

.method public abstract writeBytes([B)V
.throws java/io/IOException
.end method

.method public abstract writeInt(I)V
.throws java/io/IOException
.end method

.method public abstract writeShort(I)V
.throws java/io/IOException
.end method
