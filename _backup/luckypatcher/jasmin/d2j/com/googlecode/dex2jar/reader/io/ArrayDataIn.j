.bytecode 50.0
.class public synchronized com/googlecode/dex2jar/reader/io/ArrayDataIn
.super java/io/ByteArrayInputStream
.implements com/googlecode/dex2jar/reader/io/DataIn

.field private 'isLE' Z

.field private 'stack' Ljava/util/Stack; signature "Ljava/util/Stack<Ljava/lang/Integer;>;"

.method public <init>([BIIZ)V
aload 0
aload 1
iload 2
iload 3
invokespecial java/io/ByteArrayInputStream/<init>([BII)V
aload 0
new java/util/Stack
dup
invokespecial java/util/Stack/<init>()V
putfield com/googlecode/dex2jar/reader/io/ArrayDataIn/stack Ljava/util/Stack;
aload 0
iload 4
putfield com/googlecode/dex2jar/reader/io/ArrayDataIn/isLE Z
return
.limit locals 5
.limit stack 4
.end method

.method public <init>([BZ)V
aload 0
aload 1
invokespecial java/io/ByteArrayInputStream/<init>([B)V
aload 0
new java/util/Stack
dup
invokespecial java/util/Stack/<init>()V
putfield com/googlecode/dex2jar/reader/io/ArrayDataIn/stack Ljava/util/Stack;
aload 0
iload 2
putfield com/googlecode/dex2jar/reader/io/ArrayDataIn/isLE Z
return
.limit locals 3
.limit stack 3
.end method

.method public static be([B)Lcom/googlecode/dex2jar/reader/io/ArrayDataIn;
new com/googlecode/dex2jar/reader/io/ArrayDataIn
dup
aload 0
iconst_0
invokespecial com/googlecode/dex2jar/reader/io/ArrayDataIn/<init>([BZ)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public static be([BII)Lcom/googlecode/dex2jar/reader/io/ArrayDataIn;
new com/googlecode/dex2jar/reader/io/ArrayDataIn
dup
aload 0
iload 1
iload 2
iconst_0
invokespecial com/googlecode/dex2jar/reader/io/ArrayDataIn/<init>([BIIZ)V
areturn
.limit locals 3
.limit stack 6
.end method

.method public static le([B)Lcom/googlecode/dex2jar/reader/io/ArrayDataIn;
new com/googlecode/dex2jar/reader/io/ArrayDataIn
dup
aload 0
iconst_1
invokespecial com/googlecode/dex2jar/reader/io/ArrayDataIn/<init>([BZ)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public static le([BII)Lcom/googlecode/dex2jar/reader/io/ArrayDataIn;
new com/googlecode/dex2jar/reader/io/ArrayDataIn
dup
aload 0
iload 1
iload 2
iconst_1
invokespecial com/googlecode/dex2jar/reader/io/ArrayDataIn/<init>([BIIZ)V
areturn
.limit locals 3
.limit stack 6
.end method

.method public getCurrentPosition()I
aload 0
getfield java/io/ByteArrayInputStream/pos I
aload 0
getfield java/io/ByteArrayInputStream/mark I
isub
ireturn
.limit locals 1
.limit stack 2
.end method

.method public move(I)V
aload 0
aload 0
getfield java/io/ByteArrayInputStream/mark I
iload 1
iadd
putfield java/io/ByteArrayInputStream/pos I
return
.limit locals 2
.limit stack 3
.end method

.method public pop()V
aload 0
aload 0
getfield com/googlecode/dex2jar/reader/io/ArrayDataIn/stack Ljava/util/Stack;
invokevirtual java/util/Stack/pop()Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
putfield java/io/ByteArrayInputStream/pos I
return
.limit locals 1
.limit stack 2
.end method

.method public push()V
aload 0
getfield com/googlecode/dex2jar/reader/io/ArrayDataIn/stack Ljava/util/Stack;
aload 0
getfield java/io/ByteArrayInputStream/pos I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
pop
return
.limit locals 1
.limit stack 2
.end method

.method public pushMove(I)V
aload 0
invokevirtual com/googlecode/dex2jar/reader/io/ArrayDataIn/push()V
aload 0
iload 1
invokevirtual com/googlecode/dex2jar/reader/io/ArrayDataIn/move(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public readByte()I
aload 0
invokevirtual com/googlecode/dex2jar/reader/io/ArrayDataIn/readUByte()I
i2b
ireturn
.limit locals 1
.limit stack 1
.end method

.method public readBytes(I)[B
.catch java/io/IOException from L0 to L1 using L2
iload 1
newarray byte
astore 2
L0:
aload 0
aload 2
invokespecial java/io/ByteArrayInputStream/read([B)I
pop
L1:
aload 2
areturn
L2:
astore 2
new java/lang/RuntimeException
dup
aload 2
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method public readIntx()I
aload 0
invokevirtual com/googlecode/dex2jar/reader/io/ArrayDataIn/readUIntx()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public readLeb128()J
iconst_0
istore 1
lconst_0
lstore 6
L0:
aload 0
invokevirtual com/googlecode/dex2jar/reader/io/ArrayDataIn/readUByte()I
istore 3
lload 6
iload 3
bipush 127
iand
i2l
iload 1
lshl
lor
lstore 4
iload 1
bipush 7
iadd
istore 2
iload 2
istore 1
lload 4
lstore 6
iload 3
sipush 128
iand
ifne L0
lload 4
lstore 6
lconst_1
iload 2
iconst_1
isub
lshl
lload 4
land
lconst_0
lcmp
ifeq L1
lload 4
lconst_1
iload 2
lshl
lsub
lstore 6
L1:
lload 6
lreturn
.limit locals 8
.limit stack 5
.end method

.method public readShortx()I
aload 0
invokevirtual com/googlecode/dex2jar/reader/io/ArrayDataIn/readUShortx()I
i2s
ireturn
.limit locals 1
.limit stack 1
.end method

.method public readUByte()I
aload 0
getfield java/io/ByteArrayInputStream/pos I
aload 0
getfield java/io/ByteArrayInputStream/count I
if_icmplt L0
new java/lang/RuntimeException
dup
ldc "EOF"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
invokespecial java/io/ByteArrayInputStream/read()I
ireturn
.limit locals 1
.limit stack 3
.end method

.method public readUIntx()I
aload 0
getfield com/googlecode/dex2jar/reader/io/ArrayDataIn/isLE Z
ifeq L0
aload 0
invokevirtual com/googlecode/dex2jar/reader/io/ArrayDataIn/readUByte()I
aload 0
invokevirtual com/googlecode/dex2jar/reader/io/ArrayDataIn/readUByte()I
bipush 8
ishl
ior
aload 0
invokevirtual com/googlecode/dex2jar/reader/io/ArrayDataIn/readUByte()I
bipush 16
ishl
ior
aload 0
invokevirtual com/googlecode/dex2jar/reader/io/ArrayDataIn/readUByte()I
bipush 24
ishl
ior
ireturn
L0:
aload 0
invokevirtual com/googlecode/dex2jar/reader/io/ArrayDataIn/readUByte()I
bipush 24
ishl
aload 0
invokevirtual com/googlecode/dex2jar/reader/io/ArrayDataIn/readUByte()I
bipush 16
ishl
ior
aload 0
invokevirtual com/googlecode/dex2jar/reader/io/ArrayDataIn/readUByte()I
bipush 8
ishl
ior
aload 0
invokevirtual com/googlecode/dex2jar/reader/io/ArrayDataIn/readUByte()I
ior
ireturn
.limit locals 1
.limit stack 3
.end method

.method public readULeb128()J
lconst_0
lstore 3
iconst_0
istore 2
aload 0
invokevirtual com/googlecode/dex2jar/reader/io/ArrayDataIn/readUByte()I
istore 1
L0:
iload 1
sipush 128
iand
ifeq L1
lload 3
iload 1
bipush 127
iand
iload 2
ishl
i2l
lor
lstore 3
iload 2
bipush 7
iadd
istore 2
aload 0
invokevirtual com/googlecode/dex2jar/reader/io/ArrayDataIn/readUByte()I
istore 1
goto L0
L1:
lload 3
iload 1
bipush 127
iand
iload 2
ishl
i2l
lor
lreturn
.limit locals 5
.limit stack 4
.end method

.method public readUShortx()I
aload 0
getfield com/googlecode/dex2jar/reader/io/ArrayDataIn/isLE Z
ifeq L0
aload 0
invokevirtual com/googlecode/dex2jar/reader/io/ArrayDataIn/readUByte()I
aload 0
invokevirtual com/googlecode/dex2jar/reader/io/ArrayDataIn/readUByte()I
bipush 8
ishl
ior
ireturn
L0:
aload 0
invokevirtual com/googlecode/dex2jar/reader/io/ArrayDataIn/readUByte()I
bipush 8
ishl
aload 0
invokevirtual com/googlecode/dex2jar/reader/io/ArrayDataIn/readUByte()I
ior
ireturn
.limit locals 1
.limit stack 3
.end method

.method public skip(I)V
aload 0
iload 1
i2l
invokespecial java/io/ByteArrayInputStream/skip(J)J
pop2
return
.limit locals 2
.limit stack 3
.end method
