.bytecode 50.0
.class public synchronized com/googlecode/dex2jar/reader/io/LeDataOut
.super java/lang/Object
.implements com/googlecode/dex2jar/reader/io/DataOut

.field private 'os' Ljava/io/OutputStream;

.method public <init>(Ljava/io/OutputStream;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/googlecode/dex2jar/reader/io/LeDataOut/os Ljava/io/OutputStream;
return
.limit locals 2
.limit stack 2
.end method

.method public writeByte(I)V
.throws java/io/IOException
aload 0
getfield com/googlecode/dex2jar/reader/io/LeDataOut/os Ljava/io/OutputStream;
iload 1
invokevirtual java/io/OutputStream/write(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public writeBytes([B)V
.throws java/io/IOException
aload 0
getfield com/googlecode/dex2jar/reader/io/LeDataOut/os Ljava/io/OutputStream;
aload 1
invokevirtual java/io/OutputStream/write([B)V
return
.limit locals 2
.limit stack 2
.end method

.method public writeInt(I)V
.throws java/io/IOException
aload 0
getfield com/googlecode/dex2jar/reader/io/LeDataOut/os Ljava/io/OutputStream;
iload 1
invokevirtual java/io/OutputStream/write(I)V
aload 0
getfield com/googlecode/dex2jar/reader/io/LeDataOut/os Ljava/io/OutputStream;
iload 1
bipush 8
ishr
invokevirtual java/io/OutputStream/write(I)V
aload 0
getfield com/googlecode/dex2jar/reader/io/LeDataOut/os Ljava/io/OutputStream;
iload 1
bipush 16
ishr
invokevirtual java/io/OutputStream/write(I)V
aload 0
getfield com/googlecode/dex2jar/reader/io/LeDataOut/os Ljava/io/OutputStream;
iload 1
bipush 24
iushr
invokevirtual java/io/OutputStream/write(I)V
return
.limit locals 2
.limit stack 3
.end method

.method public writeShort(I)V
.throws java/io/IOException
aload 0
getfield com/googlecode/dex2jar/reader/io/LeDataOut/os Ljava/io/OutputStream;
iload 1
invokevirtual java/io/OutputStream/write(I)V
aload 0
getfield com/googlecode/dex2jar/reader/io/LeDataOut/os Ljava/io/OutputStream;
iload 1
bipush 8
ishr
invokevirtual java/io/OutputStream/write(I)V
return
.limit locals 2
.limit stack 3
.end method
