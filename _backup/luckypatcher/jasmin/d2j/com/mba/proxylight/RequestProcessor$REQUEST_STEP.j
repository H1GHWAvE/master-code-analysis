.bytecode 50.0
.class final synchronized enum com/mba/proxylight/RequestProcessor$REQUEST_STEP
.super java/lang/Enum
.signature "Ljava/lang/Enum<Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;>;"
.inner class private static final enum REQUEST_STEP inner com/mba/proxylight/RequestProcessor$REQUEST_STEP outer com/mba/proxylight/RequestProcessor

.field private static final synthetic '$VALUES' [Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

.field public static final enum 'REQUEST_CONTENT' Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

.field public static final enum 'REQUEST_HEADERS' Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

.field public static final enum 'STATUS_LINE' Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

.field public static final enum 'TRANSFER' Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;

.method static <clinit>()V
new com/mba/proxylight/RequestProcessor$REQUEST_STEP
dup
ldc "STATUS_LINE"
iconst_0
invokespecial com/mba/proxylight/RequestProcessor$REQUEST_STEP/<init>(Ljava/lang/String;I)V
putstatic com/mba/proxylight/RequestProcessor$REQUEST_STEP/STATUS_LINE Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;
new com/mba/proxylight/RequestProcessor$REQUEST_STEP
dup
ldc "REQUEST_HEADERS"
iconst_1
invokespecial com/mba/proxylight/RequestProcessor$REQUEST_STEP/<init>(Ljava/lang/String;I)V
putstatic com/mba/proxylight/RequestProcessor$REQUEST_STEP/REQUEST_HEADERS Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;
new com/mba/proxylight/RequestProcessor$REQUEST_STEP
dup
ldc "REQUEST_CONTENT"
iconst_2
invokespecial com/mba/proxylight/RequestProcessor$REQUEST_STEP/<init>(Ljava/lang/String;I)V
putstatic com/mba/proxylight/RequestProcessor$REQUEST_STEP/REQUEST_CONTENT Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;
new com/mba/proxylight/RequestProcessor$REQUEST_STEP
dup
ldc "TRANSFER"
iconst_3
invokespecial com/mba/proxylight/RequestProcessor$REQUEST_STEP/<init>(Ljava/lang/String;I)V
putstatic com/mba/proxylight/RequestProcessor$REQUEST_STEP/TRANSFER Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;
iconst_4
anewarray com/mba/proxylight/RequestProcessor$REQUEST_STEP
dup
iconst_0
getstatic com/mba/proxylight/RequestProcessor$REQUEST_STEP/STATUS_LINE Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;
aastore
dup
iconst_1
getstatic com/mba/proxylight/RequestProcessor$REQUEST_STEP/REQUEST_HEADERS Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;
aastore
dup
iconst_2
getstatic com/mba/proxylight/RequestProcessor$REQUEST_STEP/REQUEST_CONTENT Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;
aastore
dup
iconst_3
getstatic com/mba/proxylight/RequestProcessor$REQUEST_STEP/TRANSFER Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;
aastore
putstatic com/mba/proxylight/RequestProcessor$REQUEST_STEP/$VALUES [Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;
return
.limit locals 0
.limit stack 4
.end method

.method private <init>(Ljava/lang/String;I)V
.signature "()V"
aload 0
aload 1
iload 2
invokespecial java/lang/Enum/<init>(Ljava/lang/String;I)V
return
.limit locals 3
.limit stack 3
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;
ldc com/mba/proxylight/RequestProcessor$REQUEST_STEP
aload 0
invokestatic java/lang/Enum/valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
checkcast com/mba/proxylight/RequestProcessor$REQUEST_STEP
areturn
.limit locals 1
.limit stack 2
.end method

.method public static values()[Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;
getstatic com/mba/proxylight/RequestProcessor$REQUEST_STEP/$VALUES [Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;
invokevirtual [Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;/clone()Ljava/lang/Object;
checkcast [Lcom/mba/proxylight/RequestProcessor$REQUEST_STEP;
areturn
.limit locals 0
.limit stack 1
.end method
