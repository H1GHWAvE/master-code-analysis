.bytecode 50.0
.class public synchronized com/mba/proxylight/ProxyLight
.super java/lang/Object
.inner class inner com/mba/proxylight/ProxyLight$1
.inner class inner com/mba/proxylight/ProxyLight$1$1

.field private 'filters' Ljava/util/List; signature "Ljava/util/List<Lcom/mba/proxylight/RequestFilter;>;"

.field private 'ipCache' Ljava/util/Map; signature "Ljava/util/Map<Ljava/lang/String;Ljava/net/InetAddress;>;"

.field private 'port' I

.field private 'processors' Ljava/util/Stack; signature "Ljava/util/Stack<Lcom/mba/proxylight/RequestProcessor;>;"

.field private 'remoteProxyHost' Ljava/lang/String;

.field private 'remoteProxyPort' I

.field 'running' Z

.field private 'selector' Ljava/nio/channels/Selector;

.field 'server' Ljava/nio/channels/ServerSocketChannel;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
sipush 8080
putfield com/mba/proxylight/ProxyLight/port I
aload 0
iconst_0
putfield com/mba/proxylight/ProxyLight/running Z
aload 0
new java/util/Stack
dup
invokespecial java/util/Stack/<init>()V
putfield com/mba/proxylight/ProxyLight/processors Ljava/util/Stack;
aload 0
aconst_null
putfield com/mba/proxylight/ProxyLight/selector Ljava/nio/channels/Selector;
aload 0
aconst_null
putfield com/mba/proxylight/ProxyLight/remoteProxyHost Ljava/lang/String;
aload 0
sipush 8080
putfield com/mba/proxylight/ProxyLight/remoteProxyPort I
aload 0
aconst_null
putfield com/mba/proxylight/ProxyLight/server Ljava/nio/channels/ServerSocketChannel;
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield com/mba/proxylight/ProxyLight/ipCache Ljava/util/Map;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield com/mba/proxylight/ProxyLight/filters Ljava/util/List;
return
.limit locals 1
.limit stack 3
.end method

.method static synthetic access$000(Lcom/mba/proxylight/ProxyLight;)Ljava/nio/channels/Selector;
aload 0
getfield com/mba/proxylight/ProxyLight/selector Ljava/nio/channels/Selector;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$002(Lcom/mba/proxylight/ProxyLight;Ljava/nio/channels/Selector;)Ljava/nio/channels/Selector;
aload 0
aload 1
putfield com/mba/proxylight/ProxyLight/selector Ljava/nio/channels/Selector;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic access$100(Lcom/mba/proxylight/ProxyLight;)Ljava/util/Stack;
aload 0
getfield com/mba/proxylight/ProxyLight/processors Ljava/util/Stack;
areturn
.limit locals 1
.limit stack 1
.end method

.method public debug(Ljava/lang/String;)V
aload 1
ifnull L0
getstatic java/lang/System/err Ljava/io/PrintStream;
aload 1
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public error(Ljava/lang/String;Ljava/lang/Throwable;)V
aload 1
ifnull L0
getstatic java/lang/System/err Ljava/io/PrintStream;
aload 1
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L0:
aload 2
ifnull L1
L1:
return
.limit locals 3
.limit stack 2
.end method

.method public getPort()I
aload 0
getfield com/mba/proxylight/ProxyLight/port I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getRemoteProxyHost()Ljava/lang/String;
aload 0
getfield com/mba/proxylight/ProxyLight/remoteProxyHost Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getRemoteProxyPort()I
aload 0
getfield com/mba/proxylight/ProxyLight/remoteProxyPort I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getRequestFilters()Ljava/util/List;
.signature "()Ljava/util/List<Lcom/mba/proxylight/RequestFilter;>;"
aload 0
getfield com/mba/proxylight/ProxyLight/filters Ljava/util/List;
areturn
.limit locals 1
.limit stack 1
.end method

.method public isRunning()Z
aload 0
getfield com/mba/proxylight/ProxyLight/running Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public recycle(Lcom/mba/proxylight/RequestProcessor;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/mba/proxylight/ProxyLight/processors Ljava/util/Stack;
astore 2
aload 2
monitorenter
L0:
aload 0
getfield com/mba/proxylight/ProxyLight/processors Ljava/util/Stack;
aload 1
invokevirtual java/util/Stack/add(Ljava/lang/Object;)Z
pop
aload 2
monitorexit
L1:
return
L2:
astore 1
L3:
aload 2
monitorexit
L4:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method protected resolve(Ljava/lang/String;)Ljava/net/InetAddress;
.catch java/net/UnknownHostException from L0 to L1 using L2
.catch java/lang/Throwable from L0 to L1 using L3
.catch java/net/UnknownHostException from L4 to L5 using L2
.catch java/lang/Throwable from L4 to L5 using L3
aload 0
getfield com/mba/proxylight/ProxyLight/ipCache Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/net/InetAddress
astore 3
aload 3
astore 2
aload 3
ifnonnull L6
aload 3
astore 2
L0:
aload 1
invokestatic java/net/InetAddress/getByName(Ljava/lang/String;)Ljava/net/InetAddress;
astore 3
L1:
aload 3
astore 2
L4:
aload 0
getfield com/mba/proxylight/ProxyLight/ipCache Ljava/util/Map;
aload 1
aload 3
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L5:
aload 3
astore 2
L6:
aload 2
areturn
L2:
astore 1
aconst_null
areturn
L3:
astore 1
aload 0
ldc ""
aload 1
invokevirtual com/mba/proxylight/ProxyLight/error(Ljava/lang/String;Ljava/lang/Throwable;)V
goto L6
.limit locals 4
.limit stack 3
.end method

.method public setPort(I)V
aload 0
iload 1
putfield com/mba/proxylight/ProxyLight/port I
return
.limit locals 2
.limit stack 2
.end method

.method public setRemoteProxy(Ljava/lang/String;I)V
aload 0
aload 1
putfield com/mba/proxylight/ProxyLight/remoteProxyHost Ljava/lang/String;
aload 0
iload 2
putfield com/mba/proxylight/ProxyLight/remoteProxyPort I
return
.limit locals 3
.limit stack 2
.end method

.method public start()V
.throws java/lang/Exception
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
monitorenter
L0:
aload 0
getfield com/mba/proxylight/ProxyLight/running Z
istore 1
L1:
iload 1
ifeq L3
L5:
aload 0
monitorexit
return
L3:
aload 0
iconst_1
putfield com/mba/proxylight/ProxyLight/running Z
new java/lang/Thread
dup
new com/mba/proxylight/ProxyLight$1
dup
aload 0
invokespecial com/mba/proxylight/ProxyLight$1/<init>(Lcom/mba/proxylight/ProxyLight;)V
invokespecial java/lang/Thread/<init>(Ljava/lang/Runnable;)V
astore 2
aload 2
iconst_0
invokevirtual java/lang/Thread/setDaemon(Z)V
aload 2
ldc "ProxyLight server"
invokevirtual java/lang/Thread/setName(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/Thread/start()V
L4:
goto L5
L2:
astore 2
aload 0
monitorexit
aload 2
athrow
.limit locals 3
.limit stack 5
.end method

.method public stop()V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
aload 0
getfield com/mba/proxylight/ProxyLight/running Z
ifne L0
L4:
return
L0:
aload 0
getfield com/mba/proxylight/ProxyLight/server Ljava/nio/channels/ServerSocketChannel;
invokevirtual java/nio/channels/ServerSocketChannel/close()V
aload 0
aconst_null
putfield com/mba/proxylight/ProxyLight/server Ljava/nio/channels/ServerSocketChannel;
aload 0
getfield com/mba/proxylight/ProxyLight/selector Ljava/nio/channels/Selector;
invokevirtual java/nio/channels/Selector/wakeup()Ljava/nio/channels/Selector;
pop
aload 0
aconst_null
putfield com/mba/proxylight/ProxyLight/selector Ljava/nio/channels/Selector;
L1:
aload 0
getfield com/mba/proxylight/ProxyLight/processors Ljava/util/Stack;
invokevirtual java/util/Stack/size()I
ifle L4
aload 0
getfield com/mba/proxylight/ProxyLight/processors Ljava/util/Stack;
invokevirtual java/util/Stack/pop()Ljava/lang/Object;
checkcast com/mba/proxylight/RequestProcessor
invokevirtual com/mba/proxylight/RequestProcessor/shutdown()V
L3:
goto L1
L2:
astore 1
aload 0
aconst_null
aload 1
invokevirtual com/mba/proxylight/ProxyLight/error(Ljava/lang/String;Ljava/lang/Throwable;)V
return
.limit locals 2
.limit stack 3
.end method
