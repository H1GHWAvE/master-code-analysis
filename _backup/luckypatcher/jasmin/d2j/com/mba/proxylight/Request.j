.bytecode 50.0
.class public synchronized com/mba/proxylight/Request
.super java/lang/Object

.field private static 'CONNECT_PATTERN' Ljava/util/regex/Pattern;

.field private static 'GETPOST_PATTERN' Ljava/util/regex/Pattern;

.field private 'headers' Ljava/util/Map; signature "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"

.field private 'host' Ljava/lang/String;

.field private 'method' Ljava/lang/String;

.field private 'port' I

.field private 'protocol' Ljava/lang/String;

.field private 'statusline' Ljava/lang/String;

.field private 'url' Ljava/lang/String;

.method static <clinit>()V
ldc "(.*):([\\d]+)"
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
putstatic com/mba/proxylight/Request/CONNECT_PATTERN Ljava/util/regex/Pattern;
ldc "(https?)://([^:/]+)(:[\\d]+])?/.*"
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
putstatic com/mba/proxylight/Request/GETPOST_PATTERN Ljava/util/regex/Pattern;
return
.limit locals 0
.limit stack 1
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aconst_null
putfield com/mba/proxylight/Request/statusline Ljava/lang/String;
aload 0
aconst_null
putfield com/mba/proxylight/Request/method Ljava/lang/String;
aload 0
aconst_null
putfield com/mba/proxylight/Request/url Ljava/lang/String;
aload 0
aconst_null
putfield com/mba/proxylight/Request/protocol Ljava/lang/String;
aload 0
new java/util/LinkedHashMap
dup
invokespecial java/util/LinkedHashMap/<init>()V
putfield com/mba/proxylight/Request/headers Ljava/util/Map;
aload 0
aconst_null
putfield com/mba/proxylight/Request/host Ljava/lang/String;
aload 0
iconst_m1
putfield com/mba/proxylight/Request/port I
return
.limit locals 1
.limit stack 3
.end method

.method public addHeader(Ljava/lang/String;)V
iconst_1
istore 2
L0:
aload 1
iload 2
invokevirtual java/lang/String/charAt(I)C
bipush 58
if_icmpeq L1
aload 1
iload 2
invokevirtual java/lang/String/charAt(I)C
bipush 32
if_icmpeq L1
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 1
iconst_0
iload 2
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
astore 4
L2:
iload 2
iconst_1
iadd
istore 3
iload 3
istore 2
aload 1
iload 3
invokevirtual java/lang/String/charAt(I)C
bipush 58
if_icmpeq L2
iload 3
istore 2
aload 1
iload 3
invokevirtual java/lang/String/charAt(I)C
bipush 32
if_icmpeq L2
aload 1
iload 3
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
astore 1
aload 0
getfield com/mba/proxylight/Request/headers Ljava/util/Map;
aload 4
aload 1
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
return
.limit locals 5
.limit stack 3
.end method

.method public dump()V
return
.limit locals 1
.limit stack 0
.end method

.method public getHeaders()Ljava/util/Map;
.signature "()Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
aload 0
getfield com/mba/proxylight/Request/headers Ljava/util/Map;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getHost()Ljava/lang/String;
aload 0
getfield com/mba/proxylight/Request/host Ljava/lang/String;
ifnull L0
aload 0
getfield com/mba/proxylight/Request/host Ljava/lang/String;
areturn
L0:
aload 0
invokevirtual com/mba/proxylight/Request/getUrl()Ljava/lang/String;
ifnull L1
ldc "CONNECT"
aload 0
getfield com/mba/proxylight/Request/method Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L2
getstatic com/mba/proxylight/Request/CONNECT_PATTERN Ljava/util/regex/Pattern;
aload 0
invokevirtual com/mba/proxylight/Request/getUrl()Ljava/lang/String;
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
astore 2
aload 2
invokevirtual java/util/regex/Matcher/matches()Z
ifeq L3
aload 0
aload 2
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
putfield com/mba/proxylight/Request/host Ljava/lang/String;
aload 0
aload 2
iconst_2
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
putfield com/mba/proxylight/Request/port I
L3:
aload 0
getfield com/mba/proxylight/Request/host Ljava/lang/String;
ifnonnull L1
aload 0
aload 0
invokevirtual com/mba/proxylight/Request/getHeaders()Ljava/util/Map;
ldc "Host"
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
putfield com/mba/proxylight/Request/host Ljava/lang/String;
aload 0
getfield com/mba/proxylight/Request/host Ljava/lang/String;
bipush 58
invokevirtual java/lang/String/indexOf(I)I
istore 1
iload 1
iconst_m1
if_icmple L4
aload 0
aload 0
getfield com/mba/proxylight/Request/host Ljava/lang/String;
iload 1
iconst_1
iadd
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
putfield com/mba/proxylight/Request/port I
aload 0
aload 0
getfield com/mba/proxylight/Request/host Ljava/lang/String;
iconst_0
iload 1
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
putfield com/mba/proxylight/Request/host Ljava/lang/String;
L1:
aload 0
getfield com/mba/proxylight/Request/host Ljava/lang/String;
areturn
L2:
getstatic com/mba/proxylight/Request/GETPOST_PATTERN Ljava/util/regex/Pattern;
aload 0
invokevirtual com/mba/proxylight/Request/getUrl()Ljava/lang/String;
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
astore 2
aload 2
invokevirtual java/util/regex/Matcher/matches()Z
ifeq L3
aload 0
aload 2
iconst_2
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
putfield com/mba/proxylight/Request/host Ljava/lang/String;
aload 2
iconst_3
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
ifnull L5
aload 2
iconst_3
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
iconst_1
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
pop
goto L3
L5:
ldc "http"
aload 2
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L6
aload 0
bipush 80
putfield com/mba/proxylight/Request/port I
goto L3
L6:
aload 0
sipush 443
putfield com/mba/proxylight/Request/port I
goto L3
L4:
aload 0
bipush 80
putfield com/mba/proxylight/Request/port I
goto L1
.limit locals 3
.limit stack 4
.end method

.method public getMethod()Ljava/lang/String;
aload 0
getfield com/mba/proxylight/Request/method Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getPort()I
aload 0
invokevirtual com/mba/proxylight/Request/getHost()Ljava/lang/String;
pop
aload 0
getfield com/mba/proxylight/Request/port I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getProtocol()Ljava/lang/String;
aload 0
getfield com/mba/proxylight/Request/protocol Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getStatusline()Ljava/lang/String;
aload 0
getfield com/mba/proxylight/Request/statusline Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getUrl()Ljava/lang/String;
aload 0
getfield com/mba/proxylight/Request/url Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public setMethod(Ljava/lang/String;)V
aload 0
aload 1
putfield com/mba/proxylight/Request/method Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method public setProtocol(Ljava/lang/String;)V
aload 0
aload 1
putfield com/mba/proxylight/Request/protocol Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method public setStatusline(Ljava/lang/String;)V
aload 0
aload 1
putfield com/mba/proxylight/Request/statusline Ljava/lang/String;
aload 1
bipush 32
invokevirtual java/lang/String/indexOf(I)I
istore 2
iload 2
iconst_m1
if_icmpeq L0
iload 2
iconst_3
if_icmpge L1
L0:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "statusline: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
aload 1
iconst_0
iload 2
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
putfield com/mba/proxylight/Request/method Ljava/lang/String;
L2:
iload 2
iconst_1
iadd
istore 3
iload 3
istore 2
aload 1
iload 3
invokevirtual java/lang/String/charAt(I)C
bipush 32
if_icmpeq L2
aload 1
bipush 32
iload 3
invokevirtual java/lang/String/indexOf(II)I
istore 2
iload 2
iconst_m1
if_icmpne L3
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "statusline: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 0
aload 1
iload 3
iload 2
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
putfield com/mba/proxylight/Request/url Ljava/lang/String;
L4:
iload 2
iconst_1
iadd
istore 3
iload 3
istore 2
aload 1
iload 3
invokevirtual java/lang/String/charAt(I)C
bipush 32
if_icmpeq L4
aload 0
aload 1
iload 3
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
putfield com/mba/proxylight/Request/protocol Ljava/lang/String;
return
.limit locals 4
.limit stack 4
.end method

.method public setUrl(Ljava/lang/String;)V
aload 0
aload 1
putfield com/mba/proxylight/Request/url Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method
