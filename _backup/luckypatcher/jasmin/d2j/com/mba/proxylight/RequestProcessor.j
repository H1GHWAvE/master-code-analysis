.bytecode 50.0
.class public synchronized abstract com/mba/proxylight/RequestProcessor
.super java/lang/Object
.inner class inner com/mba/proxylight/RequestProcessor$1
.inner class private static final enum REQUEST_STEP inner com/mba/proxylight/RequestProcessor$REQUEST_STEP outer com/mba/proxylight/RequestProcessor

.field private static final 'CONNECT_OK' [B

.field private static final 'CRLF' Ljava/lang/String; = "\r\n"

.field private static 'SOCKET_TIMEOUT' J

.field private static 'processorsCount' I

.field private static 'processorsCpt' I

.field private 'alive' Z

.field private 'currentOutSocket' Lcom/mba/proxylight/Socket;

.field private 'inData' Ljava/lang/String;

.field private 'inSocket' Lcom/mba/proxylight/Socket;

.field private 'outData' Ljava/lang/String;

.field private 'outSockets' Ljava/util/Map; signature "Ljava/util/Map<Ljava/lang/String;Lcom/mba/proxylight/Socket;>;"

.field private 'processorIdx' I

.field private 'readBuffer' Ljava/nio/ByteBuffer;

.field private 'read_buf' [C

.field 'read_offset' I

.field private 'selector' Ljava/nio/channels/Selector;

.field private 'shutdown' Z

.field private 't' Ljava/lang/Thread;

.method static <clinit>()V
iconst_1
putstatic com/mba/proxylight/RequestProcessor/processorsCpt I
iconst_0
putstatic com/mba/proxylight/RequestProcessor/processorsCount I
ldc2_w 15000L
putstatic com/mba/proxylight/RequestProcessor/SOCKET_TIMEOUT J
ldc "HTTP/1.0 200 Connection established\r\nProxy-agent: ProxyLight\r\n\r\n"
invokevirtual java/lang/String/getBytes()[B
putstatic com/mba/proxylight/RequestProcessor/CONNECT_OK [B
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
.throws java/io/IOException
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aconst_null
putfield com/mba/proxylight/RequestProcessor/t Ljava/lang/Thread;
aload 0
iconst_0
putfield com/mba/proxylight/RequestProcessor/alive Z
aload 0
iconst_0
putfield com/mba/proxylight/RequestProcessor/shutdown Z
aload 0
iconst_1
putfield com/mba/proxylight/RequestProcessor/processorIdx I
aload 0
aconst_null
putfield com/mba/proxylight/RequestProcessor/selector Ljava/nio/channels/Selector;
aload 0
sipush 128
invokestatic java/nio/ByteBuffer/allocate(I)Ljava/nio/ByteBuffer;
putfield com/mba/proxylight/RequestProcessor/readBuffer Ljava/nio/ByteBuffer;
aload 0
aconst_null
putfield com/mba/proxylight/RequestProcessor/inSocket Lcom/mba/proxylight/Socket;
aload 0
ldc ""
putfield com/mba/proxylight/RequestProcessor/inData Ljava/lang/String;
aload 0
ldc ""
putfield com/mba/proxylight/RequestProcessor/outData Ljava/lang/String;
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield com/mba/proxylight/RequestProcessor/outSockets Ljava/util/Map;
aload 0
aconst_null
putfield com/mba/proxylight/RequestProcessor/currentOutSocket Lcom/mba/proxylight/Socket;
aload 0
sipush 128
newarray char
putfield com/mba/proxylight/RequestProcessor/read_buf [C
aload 0
iconst_0
putfield com/mba/proxylight/RequestProcessor/read_offset I
aload 0
new java/lang/Thread
dup
new com/mba/proxylight/RequestProcessor$1
dup
aload 0
invokespecial com/mba/proxylight/RequestProcessor$1/<init>(Lcom/mba/proxylight/RequestProcessor;)V
invokespecial java/lang/Thread/<init>(Ljava/lang/Runnable;)V
putfield com/mba/proxylight/RequestProcessor/t Ljava/lang/Thread;
aload 0
getfield com/mba/proxylight/RequestProcessor/t Ljava/lang/Thread;
astore 2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "ProxyLight processor - "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
astore 3
getstatic com/mba/proxylight/RequestProcessor/processorsCpt I
istore 1
iload 1
iconst_1
iadd
putstatic com/mba/proxylight/RequestProcessor/processorsCpt I
aload 0
iload 1
putfield com/mba/proxylight/RequestProcessor/processorIdx I
aload 2
aload 3
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/Thread/setName(Ljava/lang/String;)V
aload 0
getfield com/mba/proxylight/RequestProcessor/t Ljava/lang/Thread;
iconst_1
invokevirtual java/lang/Thread/setDaemon(Z)V
aload 0
getfield com/mba/proxylight/RequestProcessor/t Ljava/lang/Thread;
invokevirtual java/lang/Thread/start()V
L0:
aload 0
invokevirtual com/mba/proxylight/RequestProcessor/isAlive()Z
ifne L1
new com/chelpus/Utils
dup
ldc "w"
invokespecial com/chelpus/Utils/<init>(Ljava/lang/String;)V
ldc2_w 50L
invokevirtual com/chelpus/Utils/waitLP(J)V
goto L0
L1:
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Processeur "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/mba/proxylight/RequestProcessor/processorIdx I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " demarre."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/mba/proxylight/RequestProcessor/debug(Ljava/lang/String;)V
return
.limit locals 4
.limit stack 6
.end method

.method static synthetic access$008()I
getstatic com/mba/proxylight/RequestProcessor/processorsCount I
istore 0
iload 0
iconst_1
iadd
putstatic com/mba/proxylight/RequestProcessor/processorsCount I
iload 0
ireturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic access$010()I
getstatic com/mba/proxylight/RequestProcessor/processorsCount I
istore 0
iload 0
iconst_1
isub
putstatic com/mba/proxylight/RequestProcessor/processorsCount I
iload 0
ireturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic access$1000(Lcom/mba/proxylight/RequestProcessor;)Ljava/nio/ByteBuffer;
aload 0
getfield com/mba/proxylight/RequestProcessor/readBuffer Ljava/nio/ByteBuffer;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$102(Lcom/mba/proxylight/RequestProcessor;Z)Z
aload 0
iload 1
putfield com/mba/proxylight/RequestProcessor/alive Z
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic access$1100(Lcom/mba/proxylight/RequestProcessor;Lcom/mba/proxylight/Socket;Ljava/nio/ByteBuffer;J)I
.throws java/io/IOException
aload 0
aload 1
aload 2
lload 3
invokespecial com/mba/proxylight/RequestProcessor/read(Lcom/mba/proxylight/Socket;Ljava/nio/ByteBuffer;J)I
ireturn
.limit locals 5
.limit stack 5
.end method

.method static synthetic access$1200(Lcom/mba/proxylight/RequestProcessor;)Ljava/lang/String;
aload 0
getfield com/mba/proxylight/RequestProcessor/inData Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$1202(Lcom/mba/proxylight/RequestProcessor;Ljava/lang/String;)Ljava/lang/String;
aload 0
aload 1
putfield com/mba/proxylight/RequestProcessor/inData Ljava/lang/String;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic access$1300(Lcom/mba/proxylight/RequestProcessor;Ljava/nio/ByteBuffer;)Ljava/lang/String;
.throws java/io/IOException
aload 0
aload 1
invokespecial com/mba/proxylight/RequestProcessor/readNext(Ljava/nio/ByteBuffer;)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic access$1400(Lcom/mba/proxylight/RequestProcessor;Lcom/mba/proxylight/Request;)Z
aload 0
aload 1
invokespecial com/mba/proxylight/RequestProcessor/filterRequest(Lcom/mba/proxylight/Request;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic access$1500()[B
getstatic com/mba/proxylight/RequestProcessor/CONNECT_OK [B
areturn
.limit locals 0
.limit stack 1
.end method

.method static synthetic access$1600(Lcom/mba/proxylight/RequestProcessor;Lcom/mba/proxylight/Socket;Ljava/nio/ByteBuffer;J)V
.throws java/io/IOException
aload 0
aload 1
aload 2
lload 3
invokespecial com/mba/proxylight/RequestProcessor/write(Lcom/mba/proxylight/Socket;Ljava/nio/ByteBuffer;J)V
return
.limit locals 5
.limit stack 5
.end method

.method static synthetic access$1700(Lcom/mba/proxylight/RequestProcessor;Lcom/mba/proxylight/Socket;)V
aload 0
aload 1
invokespecial com/mba/proxylight/RequestProcessor/closeOutSocket(Lcom/mba/proxylight/Socket;)V
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic access$1800(Lcom/mba/proxylight/RequestProcessor;Lcom/mba/proxylight/Socket;Lcom/mba/proxylight/Socket;J)Z
.throws java/io/IOException
aload 0
aload 1
aload 2
lload 3
invokespecial com/mba/proxylight/RequestProcessor/transfer(Lcom/mba/proxylight/Socket;Lcom/mba/proxylight/Socket;J)Z
ireturn
.limit locals 5
.limit stack 5
.end method

.method static synthetic access$1900(Lcom/mba/proxylight/RequestProcessor;)Ljava/lang/String;
aload 0
getfield com/mba/proxylight/RequestProcessor/outData Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$1902(Lcom/mba/proxylight/RequestProcessor;Ljava/lang/String;)Ljava/lang/String;
aload 0
aload 1
putfield com/mba/proxylight/RequestProcessor/outData Ljava/lang/String;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic access$200(Lcom/mba/proxylight/RequestProcessor;)Ljava/nio/channels/Selector;
aload 0
getfield com/mba/proxylight/RequestProcessor/selector Ljava/nio/channels/Selector;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$300(Lcom/mba/proxylight/RequestProcessor;)Z
aload 0
getfield com/mba/proxylight/RequestProcessor/shutdown Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$400(Lcom/mba/proxylight/RequestProcessor;)Lcom/mba/proxylight/Socket;
aload 0
getfield com/mba/proxylight/RequestProcessor/inSocket Lcom/mba/proxylight/Socket;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$500()J
getstatic com/mba/proxylight/RequestProcessor/SOCKET_TIMEOUT J
lreturn
.limit locals 0
.limit stack 2
.end method

.method static synthetic access$600(Lcom/mba/proxylight/RequestProcessor;)Ljava/util/Map;
aload 0
getfield com/mba/proxylight/RequestProcessor/outSockets Ljava/util/Map;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$700(Lcom/mba/proxylight/RequestProcessor;)I
aload 0
getfield com/mba/proxylight/RequestProcessor/processorIdx I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$800(Lcom/mba/proxylight/RequestProcessor;)V
aload 0
invokespecial com/mba/proxylight/RequestProcessor/closeAll()V
return
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$900(Lcom/mba/proxylight/RequestProcessor;)Lcom/mba/proxylight/Socket;
aload 0
getfield com/mba/proxylight/RequestProcessor/currentOutSocket Lcom/mba/proxylight/Socket;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$902(Lcom/mba/proxylight/RequestProcessor;Lcom/mba/proxylight/Socket;)Lcom/mba/proxylight/Socket;
aload 0
aload 1
putfield com/mba/proxylight/RequestProcessor/currentOutSocket Lcom/mba/proxylight/Socket;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method private closeAll()V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
.catch java/lang/Exception from L6 to L7 using L8
aload 0
getfield com/mba/proxylight/RequestProcessor/inSocket Lcom/mba/proxylight/Socket;
ifnull L9
L0:
aload 0
getfield com/mba/proxylight/RequestProcessor/inSocket Lcom/mba/proxylight/Socket;
getfield com/mba/proxylight/Socket/socket Ljava/nio/channels/SocketChannel;
invokevirtual java/nio/channels/SocketChannel/close()V
L1:
aload 0
aconst_null
putfield com/mba/proxylight/RequestProcessor/inSocket Lcom/mba/proxylight/Socket;
L9:
aload 0
getfield com/mba/proxylight/RequestProcessor/outSockets Ljava/util/Map;
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 1
L10:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L11
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/mba/proxylight/Socket
astore 2
L3:
aload 2
getfield com/mba/proxylight/Socket/socket Ljava/nio/channels/SocketChannel;
invokevirtual java/nio/channels/SocketChannel/close()V
L4:
goto L10
L5:
astore 2
aload 0
aconst_null
aload 2
invokevirtual com/mba/proxylight/RequestProcessor/error(Ljava/lang/String;Ljava/lang/Throwable;)V
goto L10
L2:
astore 1
aload 0
aconst_null
aload 1
invokevirtual com/mba/proxylight/RequestProcessor/error(Ljava/lang/String;Ljava/lang/Throwable;)V
goto L1
L11:
aload 0
getfield com/mba/proxylight/RequestProcessor/outSockets Ljava/util/Map;
invokeinterface java/util/Map/clear()V 0
aload 0
aconst_null
putfield com/mba/proxylight/RequestProcessor/currentOutSocket Lcom/mba/proxylight/Socket;
aload 0
getfield com/mba/proxylight/RequestProcessor/selector Ljava/nio/channels/Selector;
ifnull L12
L6:
aload 0
getfield com/mba/proxylight/RequestProcessor/selector Ljava/nio/channels/Selector;
invokevirtual java/nio/channels/Selector/wakeup()Ljava/nio/channels/Selector;
pop
L7:
aload 0
aconst_null
putfield com/mba/proxylight/RequestProcessor/selector Ljava/nio/channels/Selector;
L12:
return
L8:
astore 1
aload 0
aconst_null
aload 1
invokevirtual com/mba/proxylight/RequestProcessor/error(Ljava/lang/String;Ljava/lang/Throwable;)V
goto L7
.limit locals 3
.limit stack 3
.end method

.method private closeOutSocket(Lcom/mba/proxylight/Socket;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
.catch java/lang/Exception from L3 to L4 using L2
L0:
aload 0
getfield com/mba/proxylight/RequestProcessor/outSockets Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 2
L1:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 3
aload 3
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
aload 1
if_acmpne L1
aload 0
getfield com/mba/proxylight/RequestProcessor/outSockets Ljava/util/Map;
aload 3
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
invokeinterface java/util/Map/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
pop
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Fermeture de la socket vers "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
checkcast java/lang/String
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/mba/proxylight/RequestProcessor/debug(Ljava/lang/String;)V
L3:
aload 1
getfield com/mba/proxylight/Socket/socket Ljava/nio/channels/SocketChannel;
invokevirtual java/nio/channels/SocketChannel/isOpen()Z
ifeq L4
aload 1
getfield com/mba/proxylight/Socket/socket Ljava/nio/channels/SocketChannel;
invokevirtual java/nio/channels/SocketChannel/close()V
L4:
return
L2:
astore 1
aload 0
ldc ""
aload 1
invokevirtual com/mba/proxylight/RequestProcessor/error(Ljava/lang/String;Ljava/lang/Throwable;)V
return
.limit locals 4
.limit stack 3
.end method

.method private filterRequest(Lcom/mba/proxylight/Request;)Z
aload 0
invokevirtual com/mba/proxylight/RequestProcessor/getRequestFilters()Ljava/util/List;
astore 3
aload 3
invokeinterface java/util/List/size()I 0
ifle L0
iconst_0
istore 2
L1:
iload 2
aload 3
invokeinterface java/util/List/size()I 0
if_icmpge L0
aload 3
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/mba/proxylight/RequestFilter
aload 1
invokeinterface com/mba/proxylight/RequestFilter/filter(Lcom/mba/proxylight/Request;)Z 1
ifeq L2
iconst_1
ireturn
L2:
iload 2
iconst_1
iadd
istore 2
goto L1
L0:
iconst_0
ireturn
.limit locals 4
.limit stack 2
.end method

.method private read(Lcom/mba/proxylight/Socket;Ljava/nio/ByteBuffer;J)I
.throws java/io/IOException
aload 1
getfield com/mba/proxylight/Socket/socket Ljava/nio/channels/SocketChannel;
aload 2
invokevirtual java/nio/channels/SocketChannel/read(Ljava/nio/ByteBuffer;)I
istore 5
iload 5
ifle L0
aload 1
lload 3
putfield com/mba/proxylight/Socket/lastWrite J
L0:
iload 5
ireturn
.limit locals 6
.limit stack 3
.end method

.method private readNext(Ljava/nio/ByteBuffer;)Ljava/lang/String;
.throws java/io/IOException
iconst_0
istore 3
L0:
iload 3
istore 2
aload 1
invokevirtual java/nio/ByteBuffer/remaining()I
ifle L1
aload 1
invokevirtual java/nio/ByteBuffer/get()B
istore 2
iload 2
iconst_m1
if_icmpeq L2
iload 2
bipush 10
if_icmpne L3
L2:
iconst_1
istore 2
L1:
iload 2
ifne L4
aconst_null
areturn
L3:
iload 2
bipush 13
if_icmpeq L0
aload 0
getfield com/mba/proxylight/RequestProcessor/read_offset I
aload 0
getfield com/mba/proxylight/RequestProcessor/read_buf [C
arraylength
if_icmpne L5
aload 0
getfield com/mba/proxylight/RequestProcessor/read_buf [C
astore 5
aload 0
aload 5
arraylength
iconst_2
imul
newarray char
putfield com/mba/proxylight/RequestProcessor/read_buf [C
aload 5
iconst_0
aload 0
getfield com/mba/proxylight/RequestProcessor/read_buf [C
iconst_0
aload 0
getfield com/mba/proxylight/RequestProcessor/read_offset I
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L5:
aload 0
getfield com/mba/proxylight/RequestProcessor/read_buf [C
astore 5
aload 0
getfield com/mba/proxylight/RequestProcessor/read_offset I
istore 4
aload 0
iload 4
iconst_1
iadd
putfield com/mba/proxylight/RequestProcessor/read_offset I
aload 5
iload 4
iload 2
i2c
castore
goto L0
L4:
aload 0
getfield com/mba/proxylight/RequestProcessor/read_buf [C
iconst_0
aload 0
getfield com/mba/proxylight/RequestProcessor/read_offset I
invokestatic java/lang/String/copyValueOf([CII)Ljava/lang/String;
astore 1
aload 0
iconst_0
putfield com/mba/proxylight/RequestProcessor/read_offset I
aload 1
areturn
.limit locals 6
.limit stack 5
.end method

.method private transfer(Lcom/mba/proxylight/Socket;Lcom/mba/proxylight/Socket;J)Z
.throws java/io/IOException
aload 0
getfield com/mba/proxylight/RequestProcessor/readBuffer Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/clear()Ljava/nio/Buffer;
pop
aload 0
aload 1
aload 0
getfield com/mba/proxylight/RequestProcessor/readBuffer Ljava/nio/ByteBuffer;
lload 3
invokespecial com/mba/proxylight/RequestProcessor/read(Lcom/mba/proxylight/Socket;Ljava/nio/ByteBuffer;J)I
istore 5
iload 5
iconst_m1
if_icmpne L0
iconst_0
ireturn
L0:
iload 5
ifle L1
aload 0
getfield com/mba/proxylight/RequestProcessor/readBuffer Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/flip()Ljava/nio/Buffer;
pop
aload 0
aload 2
aload 0
getfield com/mba/proxylight/RequestProcessor/readBuffer Ljava/nio/ByteBuffer;
lload 3
invokespecial com/mba/proxylight/RequestProcessor/write(Lcom/mba/proxylight/Socket;Ljava/nio/ByteBuffer;J)V
L1:
iconst_1
ireturn
.limit locals 6
.limit stack 5
.end method

.method private write(Lcom/mba/proxylight/Socket;Ljava/nio/ByteBuffer;J)V
.throws java/io/IOException
aload 1
getfield com/mba/proxylight/Socket/socket Ljava/nio/channels/SocketChannel;
aload 2
invokevirtual java/nio/channels/SocketChannel/write(Ljava/nio/ByteBuffer;)I
pop
aload 1
lload 3
putfield com/mba/proxylight/Socket/lastWrite J
return
.limit locals 5
.limit stack 3
.end method

.method public abstract debug(Ljava/lang/String;)V
.end method

.method public abstract error(Ljava/lang/String;Ljava/lang/Throwable;)V
.end method

.method public abstract getRemoteProxyHost()Ljava/lang/String;
.end method

.method public abstract getRemoteProxyPort()I
.end method

.method public abstract getRequestFilters()Ljava/util/List;
.signature "()Ljava/util/List<Lcom/mba/proxylight/RequestFilter;>;"
.end method

.method public isAlive()Z
aload 0
getfield com/mba/proxylight/RequestProcessor/alive Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public process(Ljava/nio/channels/SelectionKey;)V
.throws java/io/IOException
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
monitorenter
L0:
aload 1
invokevirtual java/nio/channels/SelectionKey/channel()Ljava/nio/channels/SelectableChannel;
checkcast java/nio/channels/ServerSocketChannel
astore 1
aload 0
new com/mba/proxylight/Socket
dup
invokespecial com/mba/proxylight/Socket/<init>()V
putfield com/mba/proxylight/RequestProcessor/inSocket Lcom/mba/proxylight/Socket;
aload 0
getfield com/mba/proxylight/RequestProcessor/inSocket Lcom/mba/proxylight/Socket;
aload 1
invokevirtual java/nio/channels/ServerSocketChannel/accept()Ljava/nio/channels/SocketChannel;
putfield com/mba/proxylight/Socket/socket Ljava/nio/channels/SocketChannel;
aload 0
getfield com/mba/proxylight/RequestProcessor/inSocket Lcom/mba/proxylight/Socket;
getfield com/mba/proxylight/Socket/socket Ljava/nio/channels/SocketChannel;
iconst_0
invokevirtual java/nio/channels/SocketChannel/configureBlocking(Z)Ljava/nio/channels/SelectableChannel;
pop
aload 0
invokestatic java/nio/channels/spi/SelectorProvider/provider()Ljava/nio/channels/spi/SelectorProvider;
invokevirtual java/nio/channels/spi/SelectorProvider/openSelector()Ljava/nio/channels/spi/AbstractSelector;
putfield com/mba/proxylight/RequestProcessor/selector Ljava/nio/channels/Selector;
aload 0
getfield com/mba/proxylight/RequestProcessor/inSocket Lcom/mba/proxylight/Socket;
getfield com/mba/proxylight/Socket/socket Ljava/nio/channels/SocketChannel;
aload 0
getfield com/mba/proxylight/RequestProcessor/selector Ljava/nio/channels/Selector;
iconst_1
aload 0
getfield com/mba/proxylight/RequestProcessor/inSocket Lcom/mba/proxylight/Socket;
invokevirtual java/nio/channels/SocketChannel/register(Ljava/nio/channels/Selector;ILjava/lang/Object;)Ljava/nio/channels/SelectionKey;
pop
aload 0
invokevirtual java/lang/Object/notify()V
aload 0
monitorexit
L1:
return
L2:
astore 1
L3:
aload 0
monitorexit
L4:
aload 1
athrow
.limit locals 2
.limit stack 4
.end method

.method public abstract recycle()V
.end method

.method public abstract resolve(Ljava/lang/String;)Ljava/net/InetAddress;
.end method

.method public shutdown()V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
invokespecial com/mba/proxylight/RequestProcessor/closeAll()V
aload 0
iconst_1
putfield com/mba/proxylight/RequestProcessor/shutdown Z
aload 0
monitorenter
L0:
aload 0
invokevirtual java/lang/Object/notify()V
aload 0
monitorexit
L1:
return
L2:
astore 1
L3:
aload 0
monitorexit
L4:
aload 1
athrow
.limit locals 2
.limit stack 2
.end method
