.bytecode 50.0
.class public synchronized abstract com/google/android/vending/licensing/ILicenseResultListener$Stub
.super android/os/Binder
.implements com/google/android/vending/licensing/ILicenseResultListener
.inner class public static abstract Stub inner com/google/android/vending/licensing/ILicenseResultListener$Stub outer com/google/android/vending/licensing/ILicenseResultListener
.inner class private static Proxy inner com/google/android/vending/licensing/ILicenseResultListener$Stub$Proxy outer com/google/android/vending/licensing/ILicenseResultListener$Stub

.field private static final 'DESCRIPTOR' Ljava/lang/String; = "com.android.vending.licensing.ILicenseResultListener"

.field static final 'TRANSACTION_verifyLicense' I = 1


.method public <init>()V
aload 0
invokespecial android/os/Binder/<init>()V
aload 0
aload 0
ldc "com.android.vending.licensing.ILicenseResultListener"
invokevirtual com/google/android/vending/licensing/ILicenseResultListener$Stub/attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
return
.limit locals 1
.limit stack 3
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/google/android/vending/licensing/ILicenseResultListener;
aload 0
ifnonnull L0
aconst_null
areturn
L0:
aload 0
ldc "com.android.vending.licensing.ILicenseResultListener"
invokeinterface android/os/IBinder/queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface; 1
astore 1
aload 1
ifnull L1
aload 1
instanceof com/google/android/vending/licensing/ILicenseResultListener
ifeq L1
aload 1
checkcast com/google/android/vending/licensing/ILicenseResultListener
areturn
L1:
new com/google/android/vending/licensing/ILicenseResultListener$Stub$Proxy
dup
aload 0
invokespecial com/google/android/vending/licensing/ILicenseResultListener$Stub$Proxy/<init>(Landroid/os/IBinder;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public asBinder()Landroid/os/IBinder;
aload 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
.throws android/os/RemoteException
iload 1
lookupswitch
1 : L0
1598968902 : L1
default : L2
L2:
aload 0
iload 1
aload 2
aload 3
iload 4
invokespecial android/os/Binder/onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
ireturn
L1:
aload 3
ldc "com.android.vending.licensing.ILicenseResultListener"
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L0:
aload 2
ldc "com.android.vending.licensing.ILicenseResultListener"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 2
invokevirtual android/os/Parcel/readInt()I
pop
aload 0
iconst_0
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
invokevirtual com/google/android/vending/licensing/ILicenseResultListener$Stub/verifyLicense(ILjava/lang/String;Ljava/lang/String;)V
iconst_1
ireturn
.limit locals 5
.limit stack 5
.end method
