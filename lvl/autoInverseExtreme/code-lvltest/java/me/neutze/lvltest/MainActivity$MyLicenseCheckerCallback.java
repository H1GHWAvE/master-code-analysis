package me.neutze.lvltest;
 class MainActivity$MyLicenseCheckerCallback implements com.google.android.vending.licensing.LicenseCheckerCallback {
    final synthetic me.neutze.lvltest.MainActivity this$0;

    private MainActivity$MyLicenseCheckerCallback(me.neutze.lvltest.MainActivity p1)
    {
        this.this$0 = p1;
        return;
    }

    synthetic MainActivity$MyLicenseCheckerCallback(me.neutze.lvltest.MainActivity p1, me.neutze.lvltest.MainActivity$1 p2)
    {
        this(p1);
        return;
    }

    public void allow(int p3)
    {
        if (!this.this$0.isFinishing()) {
            me.neutze.lvltest.MainActivity.access$500(this.this$0, "allowed");
            android.util.Log.e("JOHANNES", "allow");
        }
        return;
    }

    public void applicationError(int p3)
    {
        me.neutze.lvltest.MainActivity.access$500(this.this$0, "error");
        android.util.Log.e("JOHANNES", "error");
        return;
    }

    public void dontAllow(int p3)
    {
        if (!this.this$0.isFinishing()) {
            me.neutze.lvltest.MainActivity.access$500(this.this$0, "not allowed");
            android.util.Log.e("JOHANNES", "dontAllow");
        }
        return;
    }
}
