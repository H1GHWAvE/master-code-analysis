package com.google.android.vending.licensing;
public class LicenseChecker implements android.content.ServiceConnection {
    private static final boolean DEBUG_LICENSE_ERROR = False;
    private static final String KEY_FACTORY_ALGORITHM = "RSA";
    private static final java.security.SecureRandom RANDOM = None;
    private static final String TAG = "LicenseChecker";
    private static final int TIMEOUT_MS = 10000;
    private final java.util.Set mChecksInProgress;
    private final android.content.Context mContext;
    private android.os.Handler mHandler;
    private final String mPackageName;
    private final java.util.Queue mPendingChecks;
    private final com.google.android.vending.licensing.Policy mPolicy;
    private java.security.PublicKey mPublicKey;
    private com.google.android.vending.licensing.ILicensingService mService;
    private final String mVersionCode;

    static LicenseChecker()
    {
        com.google.android.vending.licensing.LicenseChecker.RANDOM = new java.security.SecureRandom();
        return;
    }

    public LicenseChecker(android.content.Context p4, com.google.android.vending.licensing.Policy p5, String p6)
    {
        this.mChecksInProgress = new java.util.HashSet();
        this.mPendingChecks = new java.util.LinkedList();
        this.mContext = p4;
        this.mPolicy = p5;
        this.mPublicKey = com.google.android.vending.licensing.LicenseChecker.generatePublicKey(p6);
        this.mPackageName = this.mContext.getPackageName();
        this.mVersionCode = com.google.android.vending.licensing.LicenseChecker.getVersionCode(p4, this.mPackageName);
        android.os.HandlerThread v0_1 = new android.os.HandlerThread("background thread");
        v0_1.start();
        this.mHandler = new android.os.Handler(v0_1.getLooper());
        return;
    }

    static synthetic void access$100(com.google.android.vending.licensing.LicenseChecker p0, com.google.android.vending.licensing.LicenseValidator p1)
    {
        p0.handleServiceConnectionError(p1);
        return;
    }

    static synthetic void access$200(com.google.android.vending.licensing.LicenseChecker p0, com.google.android.vending.licensing.LicenseValidator p1)
    {
        p0.finishCheck(p1);
        return;
    }

    static synthetic java.util.Set access$300(com.google.android.vending.licensing.LicenseChecker p1)
    {
        return p1.mChecksInProgress;
    }

    static synthetic java.security.PublicKey access$500(com.google.android.vending.licensing.LicenseChecker p1)
    {
        return p1.mPublicKey;
    }

    static synthetic android.os.Handler access$600(com.google.android.vending.licensing.LicenseChecker p1)
    {
        return p1.mHandler;
    }

    private void cleanupService()
    {
        if (this.mService != null) {
            try {
                this.mContext.unbindService(this);
            } catch (IllegalArgumentException v0) {
                android.util.Log.e("LicenseChecker", "Unable to unbind from licensing service (already unbound)");
            }
            this.mService = 0;
        }
        return;
    }

    private declared_synchronized void finishCheck(com.google.android.vending.licensing.LicenseValidator p2)
    {
        try {
            this.mChecksInProgress.remove(p2);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
        if (this.mChecksInProgress.isEmpty()) {
            this.cleanupService();
        }
        return;
    }

    private int generateNonce()
    {
        return com.google.android.vending.licensing.LicenseChecker.RANDOM.nextInt();
    }

    private static java.security.PublicKey generatePublicKey(String p5)
    {
        try {
            return java.security.KeyFactory.getInstance("RSA").generatePublic(new java.security.spec.X509EncodedKeySpec(com.google.android.vending.licensing.util.Base64.decode(p5)));
        } catch (java.security.spec.InvalidKeySpecException v1_0) {
            android.util.Log.e("LicenseChecker", "Invalid key specification.");
            throw new IllegalArgumentException(v1_0);
        } catch (java.security.spec.InvalidKeySpecException v1_2) {
            throw new RuntimeException(v1_2);
        } catch (java.security.spec.InvalidKeySpecException v1_1) {
            android.util.Log.e("LicenseChecker", "Could not decode from Base64.");
            throw new IllegalArgumentException(v1_1);
        }
    }

    private static String getVersionCode(android.content.Context p3, String p4)
    {
        try {
            String v1_3 = String.valueOf(p3.getPackageManager().getPackageInfo(p4, 0).versionCode);
        } catch (android.content.pm.PackageManager$NameNotFoundException v0) {
            android.util.Log.e("LicenseChecker", "Package not found. could not get version code.");
            v1_3 = "";
        }
        return v1_3;
    }

    private declared_synchronized void handleServiceConnectionError(com.google.android.vending.licensing.LicenseValidator p4)
    {
        try {
            this.mPolicy.processServerResponse(291, 0);
        } catch (com.google.android.vending.licensing.LicenseCheckerCallback v0_5) {
            throw v0_5;
        }
        if (!this.mPolicy.allowAccess()) {
            p4.getCallback().dontAllow(291);
        } else {
            p4.getCallback().allow(291);
        }
        return;
    }

    private void runChecks()
    {
        while(true) {
            com.google.android.vending.licensing.LicenseValidator v1_1 = ((com.google.android.vending.licensing.LicenseValidator) this.mPendingChecks.poll());
            if (v1_1 == null) {
                break;
            }
            try {
                android.util.Log.i("LicenseChecker", new StringBuilder().append("Calling checkLicense on service for ").append(v1_1.getPackageName()).toString());
                this.mService.checkLicense(((long) v1_1.getNonce()), v1_1.getPackageName(), new com.google.android.vending.licensing.LicenseChecker$ResultListener(this, v1_1));
                this.mChecksInProgress.add(v1_1);
            } catch (android.os.RemoteException v0) {
                android.util.Log.w("LicenseChecker", "RemoteException in checkLicense call.", v0);
                this.handleServiceConnectionError(v1_1);
            }
        }
        return;
    }

    public declared_synchronized void checkAccess(com.google.android.vending.licensing.LicenseCheckerCallback p11)
    {
        try {
            if (!this.mPolicy.allowAccess()) {
                com.google.android.vending.licensing.LicenseValidator v0_1 = new com.google.android.vending.licensing.LicenseValidator(this.mPolicy, new com.google.android.vending.licensing.NullDeviceLimiter(), p11, this.generateNonce(), this.mPackageName, this.mVersionCode);
                if (this.mService != null) {
                    this.mPendingChecks.offer(v0_1);
                    this.runChecks();
                } else {
                    android.util.Log.i("LicenseChecker", "Binding to licensing service.");
                    try {
                        android.content.Intent v9_1 = new android.content.Intent(new String(com.google.android.vending.licensing.util.Base64.decode("Y29tLmFuZHJvaWQudmVuZGluZy5saWNlbnNpbmcuSUxpY2Vuc2luZ1NlcnZpY2U=")));
                    } catch (com.google.android.vending.licensing.util.Base64DecoderException v8) {
                        p11.applicationError(6);
                    } catch (com.google.android.vending.licensing.util.Base64DecoderException v8) {
                        v8.printStackTrace();
                    }
                    if (android.os.Build$VERSION.SDK_INT >= 14) {
                        v9_1.setPackage("com.android.vending");
                    }
                    if (!this.mContext.bindService(v9_1, this, 1)) {
                        android.util.Log.e("LicenseChecker", "Could not bind to service.");
                        this.handleServiceConnectionError(v0_1);
                    } else {
                        this.mPendingChecks.offer(v0_1);
                    }
                }
            } else {
                android.util.Log.i("LicenseChecker", "Using cached license response");
                p11.allow(256);
            }
        } catch (String v1_16) {
            throw v1_16;
        }
        return;
    }

    public declared_synchronized void onDestroy()
    {
        try {
            this.cleanupService();
            this.mHandler.getLooper().quit();
            return;
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public declared_synchronized void onServiceConnected(android.content.ComponentName p2, android.os.IBinder p3)
    {
        try {
            this.mService = com.google.android.vending.licensing.ILicensingService$Stub.asInterface(p3);
            this.runChecks();
            return;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    public declared_synchronized void onServiceDisconnected(android.content.ComponentName p3)
    {
        try {
            android.util.Log.w("LicenseChecker", "Service unexpectedly disconnected.");
            this.mService = 0;
            return;
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }
}
