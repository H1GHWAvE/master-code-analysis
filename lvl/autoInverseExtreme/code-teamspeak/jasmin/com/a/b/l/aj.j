.bytecode 50.0
.class final synchronized enum com/a/b/l/aj
.super java/lang/Enum
.implements java/util/Comparator
.annotation invisible Lcom/a/b/a/d;
.end annotation

.field public static final enum 'a' Lcom/a/b/l/aj;

.field static final 'b' Z

.field static final 'c' Lsun/misc/Unsafe;

.field static final 'd' I

.field private static final synthetic 'e' [Lcom/a/b/l/aj;

.method static <clinit>()V
new com/a/b/l/aj
dup
ldc "INSTANCE"
invokespecial com/a/b/l/aj/<init>(Ljava/lang/String;)V
putstatic com/a/b/l/aj/a Lcom/a/b/l/aj;
iconst_1
anewarray com/a/b/l/aj
dup
iconst_0
getstatic com/a/b/l/aj/a Lcom/a/b/l/aj;
aastore
putstatic com/a/b/l/aj/e [Lcom/a/b/l/aj;
invokestatic java/nio/ByteOrder/nativeOrder()Ljava/nio/ByteOrder;
getstatic java/nio/ByteOrder/BIG_ENDIAN Ljava/nio/ByteOrder;
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
putstatic com/a/b/l/aj/b Z
invokestatic com/a/b/l/aj/a()Lsun/misc/Unsafe;
astore 0
aload 0
putstatic com/a/b/l/aj/c Lsun/misc/Unsafe;
aload 0
ldc [B
invokevirtual sun/misc/Unsafe/arrayBaseOffset(Ljava/lang/Class;)I
putstatic com/a/b/l/aj/d I
getstatic com/a/b/l/aj/c Lsun/misc/Unsafe;
ldc [B
invokevirtual sun/misc/Unsafe/arrayIndexScale(Ljava/lang/Class;)I
iconst_1
if_icmpeq L0
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L0:
return
.limit locals 1
.limit stack 4
.end method

.method private <init>(Ljava/lang/String;)V
aload 0
aload 1
iconst_0
invokespecial java/lang/Enum/<init>(Ljava/lang/String;I)V
return
.limit locals 2
.limit stack 3
.end method

.method private static a([B[B)I
aload 0
arraylength
aload 1
arraylength
invokestatic java/lang/Math/min(II)I
istore 5
iload 5
bipush 8
idiv
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
bipush 8
imul
if_icmpge L1
getstatic com/a/b/l/aj/c Lsun/misc/Unsafe;
aload 0
getstatic com/a/b/l/aj/d I
i2l
iload 2
i2l
ladd
invokevirtual sun/misc/Unsafe/getLong(Ljava/lang/Object;J)J
lstore 6
getstatic com/a/b/l/aj/c Lsun/misc/Unsafe;
aload 1
getstatic com/a/b/l/aj/d I
i2l
iload 2
i2l
ladd
invokevirtual sun/misc/Unsafe/getLong(Ljava/lang/Object;J)J
lstore 8
lload 6
lload 8
lcmp
ifeq L2
getstatic com/a/b/l/aj/b Z
ifeq L3
lload 6
lload 8
invokestatic com/a/b/l/ap/a(JJ)I
istore 2
L4:
iload 2
ireturn
L3:
lload 6
lload 8
lxor
invokestatic java/lang/Long/numberOfTrailingZeros(J)I
bipush -8
iand
istore 2
lload 6
iload 2
lushr
ldc2_w 255L
land
lload 8
iload 2
lushr
ldc2_w 255L
land
lsub
l2i
ireturn
L2:
iload 2
bipush 8
iadd
istore 2
goto L0
L1:
iload 3
bipush 8
imul
istore 3
L5:
iload 3
iload 5
if_icmpge L6
aload 0
iload 3
baload
aload 1
iload 3
baload
invokestatic com/a/b/l/ag/a(BB)I
istore 4
iload 4
istore 2
iload 4
ifne L4
iload 3
iconst_1
iadd
istore 3
goto L5
L6:
aload 0
arraylength
aload 1
arraylength
isub
ireturn
.limit locals 10
.limit stack 6
.end method

.method private static a()Lsun/misc/Unsafe;
.catch java/lang/SecurityException from L0 to L1 using L2
.catch java/security/PrivilegedActionException from L3 to L4 using L5
L0:
invokestatic sun/misc/Unsafe/getUnsafe()Lsun/misc/Unsafe;
astore 0
L1:
aload 0
areturn
L2:
astore 0
L3:
new com/a/b/l/ak
dup
invokespecial com/a/b/l/ak/<init>()V
invokestatic java/security/AccessController/doPrivileged(Ljava/security/PrivilegedExceptionAction;)Ljava/lang/Object;
checkcast sun/misc/Unsafe
astore 0
L4:
aload 0
areturn
L5:
astore 0
new java/lang/RuntimeException
dup
ldc "Could not initialize intrinsics"
aload 0
invokevirtual java/security/PrivilegedActionException/getCause()Ljava/lang/Throwable;
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
.limit locals 1
.limit stack 4
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/l/aj;
ldc com/a/b/l/aj
aload 0
invokestatic java/lang/Enum/valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
checkcast com/a/b/l/aj
areturn
.limit locals 1
.limit stack 2
.end method

.method public static values()[Lcom/a/b/l/aj;
getstatic com/a/b/l/aj/e [Lcom/a/b/l/aj;
invokevirtual [Lcom/a/b/l/aj;/clone()Ljava/lang/Object;
checkcast [Lcom/a/b/l/aj;
areturn
.limit locals 0
.limit stack 1
.end method

.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
aload 1
checkcast [B
astore 1
aload 2
checkcast [B
astore 2
aload 1
arraylength
aload 2
arraylength
invokestatic java/lang/Math/min(II)I
istore 6
iload 6
bipush 8
idiv
istore 4
iconst_0
istore 3
L0:
iload 3
iload 4
bipush 8
imul
if_icmpge L1
getstatic com/a/b/l/aj/c Lsun/misc/Unsafe;
aload 1
getstatic com/a/b/l/aj/d I
i2l
iload 3
i2l
ladd
invokevirtual sun/misc/Unsafe/getLong(Ljava/lang/Object;J)J
lstore 7
getstatic com/a/b/l/aj/c Lsun/misc/Unsafe;
aload 2
getstatic com/a/b/l/aj/d I
i2l
iload 3
i2l
ladd
invokevirtual sun/misc/Unsafe/getLong(Ljava/lang/Object;J)J
lstore 9
lload 7
lload 9
lcmp
ifeq L2
getstatic com/a/b/l/aj/b Z
ifeq L3
lload 7
lload 9
invokestatic com/a/b/l/ap/a(JJ)I
istore 3
L4:
iload 3
ireturn
L3:
lload 7
lload 9
lxor
invokestatic java/lang/Long/numberOfTrailingZeros(J)I
bipush -8
iand
istore 3
lload 7
iload 3
lushr
ldc2_w 255L
land
lload 9
iload 3
lushr
ldc2_w 255L
land
lsub
l2i
ireturn
L2:
iload 3
bipush 8
iadd
istore 3
goto L0
L1:
iload 4
bipush 8
imul
istore 4
L5:
iload 4
iload 6
if_icmpge L6
aload 1
iload 4
baload
aload 2
iload 4
baload
invokestatic com/a/b/l/ag/a(BB)I
istore 5
iload 5
istore 3
iload 5
ifne L4
iload 4
iconst_1
iadd
istore 4
goto L5
L6:
aload 1
arraylength
aload 2
arraylength
isub
ireturn
.limit locals 11
.limit stack 6
.end method
