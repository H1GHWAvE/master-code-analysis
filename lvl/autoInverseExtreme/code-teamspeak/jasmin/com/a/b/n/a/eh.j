.bytecode 50.0
.class synchronized com/a/b/n/a/eh
.super com/a/b/n/a/o

.field private final 'a' Ljava/util/concurrent/ExecutorService;

.method <init>(Ljava/util/concurrent/ExecutorService;)V
aload 0
invokespecial com/a/b/n/a/o/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/ExecutorService
putfield com/a/b/n/a/eh/a Ljava/util/concurrent/ExecutorService;
return
.limit locals 2
.limit stack 2
.end method

.method public awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
aload 0
getfield com/a/b/n/a/eh/a Ljava/util/concurrent/ExecutorService;
lload 1
aload 3
invokeinterface java/util/concurrent/ExecutorService/awaitTermination(JLjava/util/concurrent/TimeUnit;)Z 3
ireturn
.limit locals 4
.limit stack 4
.end method

.method public execute(Ljava/lang/Runnable;)V
aload 0
getfield com/a/b/n/a/eh/a Ljava/util/concurrent/ExecutorService;
aload 1
invokeinterface java/util/concurrent/ExecutorService/execute(Ljava/lang/Runnable;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public isShutdown()Z
aload 0
getfield com/a/b/n/a/eh/a Ljava/util/concurrent/ExecutorService;
invokeinterface java/util/concurrent/ExecutorService/isShutdown()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isTerminated()Z
aload 0
getfield com/a/b/n/a/eh/a Ljava/util/concurrent/ExecutorService;
invokeinterface java/util/concurrent/ExecutorService/isTerminated()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public shutdown()V
aload 0
getfield com/a/b/n/a/eh/a Ljava/util/concurrent/ExecutorService;
invokeinterface java/util/concurrent/ExecutorService/shutdown()V 0
return
.limit locals 1
.limit stack 1
.end method

.method public shutdownNow()Ljava/util/List;
aload 0
getfield com/a/b/n/a/eh/a Ljava/util/concurrent/ExecutorService;
invokeinterface java/util/concurrent/ExecutorService/shutdownNow()Ljava/util/List; 0
areturn
.limit locals 1
.limit stack 1
.end method
