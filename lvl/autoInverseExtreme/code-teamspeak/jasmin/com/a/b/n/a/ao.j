.bytecode 50.0
.class final synchronized com/a/b/n/a/ao
.super java/lang/Object
.annotation invisible Ljavax/annotation/concurrent/Immutable;
.end annotation

.field final 'a' Lcom/a/b/n/a/ew;

.field final 'b' Z

.field final 'c' Ljava/lang/Throwable;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.end field

.method <init>(Lcom/a/b/n/a/ew;)V
aload 0
aload 1
iconst_0
aconst_null
invokespecial com/a/b/n/a/ao/<init>(Lcom/a/b/n/a/ew;ZLjava/lang/Throwable;)V
return
.limit locals 2
.limit stack 4
.end method

.method <init>(Lcom/a/b/n/a/ew;ZLjava/lang/Throwable;)V
.annotation visibleparam 3 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokespecial java/lang/Object/<init>()V
iload 2
ifeq L0
aload 1
getstatic com/a/b/n/a/ew/b Lcom/a/b/n/a/ew;
if_acmpne L1
L0:
iconst_1
istore 6
L2:
iload 6
ldc "shudownWhenStartupFinishes can only be set if state is STARTING. Got %s instead."
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 3
ifnull L3
iconst_1
istore 4
L4:
aload 1
getstatic com/a/b/n/a/ew/f Lcom/a/b/n/a/ew;
if_acmpne L5
iconst_1
istore 5
L6:
iload 4
iload 5
ixor
ifne L7
iconst_1
istore 6
L8:
iload 6
ldc "A failure cause should be set if and only if the state is failed.  Got %s and %s instead."
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
dup
iconst_1
aload 3
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
aload 1
putfield com/a/b/n/a/ao/a Lcom/a/b/n/a/ew;
aload 0
iload 2
putfield com/a/b/n/a/ao/b Z
aload 0
aload 3
putfield com/a/b/n/a/ao/c Ljava/lang/Throwable;
return
L1:
iconst_0
istore 6
goto L2
L3:
iconst_0
istore 4
goto L4
L5:
iconst_0
istore 5
goto L6
L7:
iconst_0
istore 6
goto L8
.limit locals 7
.limit stack 6
.end method

.method private a()Lcom/a/b/n/a/ew;
aload 0
getfield com/a/b/n/a/ao/b Z
ifeq L0
aload 0
getfield com/a/b/n/a/ao/a Lcom/a/b/n/a/ew;
getstatic com/a/b/n/a/ew/b Lcom/a/b/n/a/ew;
if_acmpne L0
getstatic com/a/b/n/a/ew/d Lcom/a/b/n/a/ew;
areturn
L0:
aload 0
getfield com/a/b/n/a/ao/a Lcom/a/b/n/a/ew;
areturn
.limit locals 1
.limit stack 2
.end method

.method private b()Ljava/lang/Throwable;
aload 0
getfield com/a/b/n/a/ao/a Lcom/a/b/n/a/ew;
getstatic com/a/b/n/a/ew/f Lcom/a/b/n/a/ew;
if_acmpne L0
iconst_1
istore 1
L1:
iload 1
ldc "failureCause() is only valid if the service has failed, service is %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/n/a/ao/a Lcom/a/b/n/a/ew;
aastore
invokestatic com/a/b/b/cn/b(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
getfield com/a/b/n/a/ao/c Ljava/lang/Throwable;
areturn
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 6
.end method
