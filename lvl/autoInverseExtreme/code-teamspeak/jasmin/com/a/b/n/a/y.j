.bytecode 50.0
.class final synchronized com/a/b/n/a/y
.super com/a/b/n/a/cb
.implements java/util/concurrent/Callable

.field final synthetic 'a' Lcom/a/b/n/a/x;

.field private final 'b' Ljava/lang/Runnable;

.field private final 'c' Ljava/util/concurrent/ScheduledExecutorService;

.field private final 'd' Lcom/a/b/n/a/ad;

.field private final 'e' Ljava/util/concurrent/locks/ReentrantLock;

.field private 'f' Ljava/util/concurrent/Future;
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "lock"
.end annotation
.end field

.method <init>(Lcom/a/b/n/a/x;Lcom/a/b/n/a/ad;Ljava/util/concurrent/ScheduledExecutorService;Ljava/lang/Runnable;)V
aload 0
aload 1
putfield com/a/b/n/a/y/a Lcom/a/b/n/a/x;
aload 0
invokespecial com/a/b/n/a/cb/<init>()V
aload 0
new java/util/concurrent/locks/ReentrantLock
dup
invokespecial java/util/concurrent/locks/ReentrantLock/<init>()V
putfield com/a/b/n/a/y/e Ljava/util/concurrent/locks/ReentrantLock;
aload 0
aload 4
putfield com/a/b/n/a/y/b Ljava/lang/Runnable;
aload 0
aload 3
putfield com/a/b/n/a/y/c Ljava/util/concurrent/ScheduledExecutorService;
aload 0
aload 2
putfield com/a/b/n/a/y/d Lcom/a/b/n/a/ad;
return
.limit locals 5
.limit stack 3
.end method

.method private c()Ljava/lang/Void;
aload 0
getfield com/a/b/n/a/y/b Ljava/lang/Runnable;
invokeinterface java/lang/Runnable/run()V 0
aload 0
invokevirtual com/a/b/n/a/y/a()V
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a()V
.catch java/lang/Throwable from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/lang/Throwable from L1 to L4 using L2
.catch all from L1 to L4 using L3
.catch all from L5 to L6 using L3
aload 0
getfield com/a/b/n/a/y/e Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/lock()V
L0:
aload 0
getfield com/a/b/n/a/y/f Ljava/util/concurrent/Future;
ifnull L1
aload 0
getfield com/a/b/n/a/y/f Ljava/util/concurrent/Future;
invokeinterface java/util/concurrent/Future/isCancelled()Z 0
ifne L4
L1:
aload 0
getfield com/a/b/n/a/y/a Lcom/a/b/n/a/x;
invokevirtual com/a/b/n/a/x/a()Lcom/a/b/n/a/z;
astore 1
aload 0
aload 0
getfield com/a/b/n/a/y/c Ljava/util/concurrent/ScheduledExecutorService;
aload 0
aload 1
getfield com/a/b/n/a/z/a J
aload 1
getfield com/a/b/n/a/z/b Ljava/util/concurrent/TimeUnit;
invokeinterface java/util/concurrent/ScheduledExecutorService/schedule(Ljava/util/concurrent/Callable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture; 4
putfield com/a/b/n/a/y/f Ljava/util/concurrent/Future;
L4:
aload 0
getfield com/a/b/n/a/y/e Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/unlock()V
return
L2:
astore 1
L5:
aload 0
getfield com/a/b/n/a/y/d Lcom/a/b/n/a/ad;
aload 1
invokevirtual com/a/b/n/a/ad/a(Ljava/lang/Throwable;)V
L6:
aload 0
getfield com/a/b/n/a/y/e Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/unlock()V
return
L3:
astore 1
aload 0
getfield com/a/b/n/a/y/e Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/unlock()V
aload 1
athrow
.limit locals 2
.limit stack 6
.end method

.method protected final b()Ljava/util/concurrent/Future;
new java/lang/UnsupportedOperationException
dup
ldc "Only cancel is supported by this future"
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method public final synthetic call()Ljava/lang/Object;
aload 0
invokespecial com/a/b/n/a/y/c()Ljava/lang/Void;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final cancel(Z)Z
.catch all from L0 to L1 using L2
aload 0
getfield com/a/b/n/a/y/e Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/lock()V
L0:
aload 0
getfield com/a/b/n/a/y/f Ljava/util/concurrent/Future;
iload 1
invokeinterface java/util/concurrent/Future/cancel(Z)Z 1
istore 1
L1:
aload 0
getfield com/a/b/n/a/y/e Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/unlock()V
iload 1
ireturn
L2:
astore 2
aload 0
getfield com/a/b/n/a/y/e Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/unlock()V
aload 2
athrow
.limit locals 3
.limit stack 2
.end method

.method protected final synthetic k_()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/n/a/y/b()Ljava/util/concurrent/Future;
areturn
.limit locals 1
.limit stack 1
.end method
