.bytecode 50.0
.class public synchronized abstract com/a/b/n/a/ca
.super com/a/b/d/hg
.implements java/util/concurrent/ExecutorService

.method protected <init>()V
aload 0
invokespecial com/a/b/d/hg/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method protected abstract a()Ljava/util/concurrent/ExecutorService;
.end method

.method public awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
aload 0
invokevirtual com/a/b/n/a/ca/a()Ljava/util/concurrent/ExecutorService;
lload 1
aload 3
invokeinterface java/util/concurrent/ExecutorService/awaitTermination(JLjava/util/concurrent/TimeUnit;)Z 3
ireturn
.limit locals 4
.limit stack 4
.end method

.method public execute(Ljava/lang/Runnable;)V
aload 0
invokevirtual com/a/b/n/a/ca/a()Ljava/util/concurrent/ExecutorService;
aload 1
invokeinterface java/util/concurrent/ExecutorService/execute(Ljava/lang/Runnable;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public invokeAll(Ljava/util/Collection;)Ljava/util/List;
aload 0
invokevirtual com/a/b/n/a/ca/a()Ljava/util/concurrent/ExecutorService;
aload 1
invokeinterface java/util/concurrent/ExecutorService/invokeAll(Ljava/util/Collection;)Ljava/util/List; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public invokeAll(Ljava/util/Collection;JLjava/util/concurrent/TimeUnit;)Ljava/util/List;
aload 0
invokevirtual com/a/b/n/a/ca/a()Ljava/util/concurrent/ExecutorService;
aload 1
lload 2
aload 4
invokeinterface java/util/concurrent/ExecutorService/invokeAll(Ljava/util/Collection;JLjava/util/concurrent/TimeUnit;)Ljava/util/List; 4
areturn
.limit locals 5
.limit stack 5
.end method

.method public invokeAny(Ljava/util/Collection;)Ljava/lang/Object;
aload 0
invokevirtual com/a/b/n/a/ca/a()Ljava/util/concurrent/ExecutorService;
aload 1
invokeinterface java/util/concurrent/ExecutorService/invokeAny(Ljava/util/Collection;)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public invokeAny(Ljava/util/Collection;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
aload 0
invokevirtual com/a/b/n/a/ca/a()Ljava/util/concurrent/ExecutorService;
aload 1
lload 2
aload 4
invokeinterface java/util/concurrent/ExecutorService/invokeAny(Ljava/util/Collection;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object; 4
areturn
.limit locals 5
.limit stack 5
.end method

.method public isShutdown()Z
aload 0
invokevirtual com/a/b/n/a/ca/a()Ljava/util/concurrent/ExecutorService;
invokeinterface java/util/concurrent/ExecutorService/isShutdown()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isTerminated()Z
aload 0
invokevirtual com/a/b/n/a/ca/a()Ljava/util/concurrent/ExecutorService;
invokeinterface java/util/concurrent/ExecutorService/isTerminated()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method protected synthetic k_()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/n/a/ca/a()Ljava/util/concurrent/ExecutorService;
areturn
.limit locals 1
.limit stack 1
.end method

.method public shutdown()V
aload 0
invokevirtual com/a/b/n/a/ca/a()Ljava/util/concurrent/ExecutorService;
invokeinterface java/util/concurrent/ExecutorService/shutdown()V 0
return
.limit locals 1
.limit stack 1
.end method

.method public shutdownNow()Ljava/util/List;
aload 0
invokevirtual com/a/b/n/a/ca/a()Ljava/util/concurrent/ExecutorService;
invokeinterface java/util/concurrent/ExecutorService/shutdownNow()Ljava/util/List; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
aload 0
invokevirtual com/a/b/n/a/ca/a()Ljava/util/concurrent/ExecutorService;
aload 1
invokeinterface java/util/concurrent/ExecutorService/submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public submit(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/Future;
aload 0
invokevirtual com/a/b/n/a/ca/a()Ljava/util/concurrent/ExecutorService;
aload 1
aload 2
invokeinterface java/util/concurrent/ExecutorService/submit(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/Future; 2
areturn
.limit locals 3
.limit stack 3
.end method

.method public submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
aload 0
invokevirtual com/a/b/n/a/ca/a()Ljava/util/concurrent/ExecutorService;
aload 1
invokeinterface java/util/concurrent/ExecutorService/submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future; 1
areturn
.limit locals 2
.limit stack 2
.end method
