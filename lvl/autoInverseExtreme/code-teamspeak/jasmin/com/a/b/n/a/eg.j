.bytecode 50.0
.class final synchronized com/a/b/n/a/eg
.super com/a/b/n/a/o

.field private final 'a' Ljava/util/concurrent/locks/Lock;

.field private final 'b' Ljava/util/concurrent/locks/Condition;

.field private 'c' I

.field private 'd' Z

.method private <init>()V
aload 0
invokespecial com/a/b/n/a/o/<init>()V
aload 0
new java/util/concurrent/locks/ReentrantLock
dup
invokespecial java/util/concurrent/locks/ReentrantLock/<init>()V
putfield com/a/b/n/a/eg/a Ljava/util/concurrent/locks/Lock;
aload 0
aload 0
getfield com/a/b/n/a/eg/a Ljava/util/concurrent/locks/Lock;
invokeinterface java/util/concurrent/locks/Lock/newCondition()Ljava/util/concurrent/locks/Condition; 0
putfield com/a/b/n/a/eg/b Ljava/util/concurrent/locks/Condition;
aload 0
iconst_0
putfield com/a/b/n/a/eg/c I
aload 0
iconst_0
putfield com/a/b/n/a/eg/d Z
return
.limit locals 1
.limit stack 3
.end method

.method synthetic <init>(B)V
aload 0
invokespecial com/a/b/n/a/eg/<init>()V
return
.limit locals 2
.limit stack 1
.end method

.method private a()V
.catch all from L0 to L1 using L1
.catch all from L2 to L3 using L1
aload 0
getfield com/a/b/n/a/eg/a Ljava/util/concurrent/locks/Lock;
invokeinterface java/util/concurrent/locks/Lock/lock()V 0
L0:
aload 0
invokevirtual com/a/b/n/a/eg/isShutdown()Z
ifeq L2
new java/util/concurrent/RejectedExecutionException
dup
ldc "Executor already shutdown"
invokespecial java/util/concurrent/RejectedExecutionException/<init>(Ljava/lang/String;)V
athrow
L1:
astore 1
aload 0
getfield com/a/b/n/a/eg/a Ljava/util/concurrent/locks/Lock;
invokeinterface java/util/concurrent/locks/Lock/unlock()V 0
aload 1
athrow
L2:
aload 0
aload 0
getfield com/a/b/n/a/eg/c I
iconst_1
iadd
putfield com/a/b/n/a/eg/c I
L3:
aload 0
getfield com/a/b/n/a/eg/a Ljava/util/concurrent/locks/Lock;
invokeinterface java/util/concurrent/locks/Lock/unlock()V 0
return
.limit locals 2
.limit stack 3
.end method

.method private b()V
.catch all from L0 to L1 using L2
aload 0
getfield com/a/b/n/a/eg/a Ljava/util/concurrent/locks/Lock;
invokeinterface java/util/concurrent/locks/Lock/lock()V 0
L0:
aload 0
aload 0
getfield com/a/b/n/a/eg/c I
iconst_1
isub
putfield com/a/b/n/a/eg/c I
aload 0
invokevirtual com/a/b/n/a/eg/isTerminated()Z
ifeq L1
aload 0
getfield com/a/b/n/a/eg/b Ljava/util/concurrent/locks/Condition;
invokeinterface java/util/concurrent/locks/Condition/signalAll()V 0
L1:
aload 0
getfield com/a/b/n/a/eg/a Ljava/util/concurrent/locks/Lock;
invokeinterface java/util/concurrent/locks/Lock/unlock()V 0
return
L2:
astore 1
aload 0
getfield com/a/b/n/a/eg/a Ljava/util/concurrent/locks/Lock;
invokeinterface java/util/concurrent/locks/Lock/unlock()V 0
aload 1
athrow
.limit locals 2
.limit stack 3
.end method

.method public final awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 3
lload 1
invokevirtual java/util/concurrent/TimeUnit/toNanos(J)J
lstore 1
aload 0
getfield com/a/b/n/a/eg/a Ljava/util/concurrent/locks/Lock;
invokeinterface java/util/concurrent/locks/Lock/lock()V 0
L0:
aload 0
invokevirtual com/a/b/n/a/eg/isTerminated()Z
istore 4
L1:
iload 4
ifeq L5
aload 0
getfield com/a/b/n/a/eg/a Ljava/util/concurrent/locks/Lock;
invokeinterface java/util/concurrent/locks/Lock/unlock()V 0
iconst_1
ireturn
L5:
lload 1
lconst_0
lcmp
ifgt L3
aload 0
getfield com/a/b/n/a/eg/a Ljava/util/concurrent/locks/Lock;
invokeinterface java/util/concurrent/locks/Lock/unlock()V 0
iconst_0
ireturn
L3:
aload 0
getfield com/a/b/n/a/eg/b Ljava/util/concurrent/locks/Condition;
lload 1
invokeinterface java/util/concurrent/locks/Condition/awaitNanos(J)J 2
lstore 1
L4:
goto L0
L2:
astore 3
aload 0
getfield com/a/b/n/a/eg/a Ljava/util/concurrent/locks/Lock;
invokeinterface java/util/concurrent/locks/Lock/unlock()V 0
aload 3
athrow
.limit locals 5
.limit stack 4
.end method

.method public final execute(Ljava/lang/Runnable;)V
.catch all from L0 to L1 using L1
.catch all from L2 to L3 using L1
.catch all from L4 to L5 using L6
aload 0
getfield com/a/b/n/a/eg/a Ljava/util/concurrent/locks/Lock;
invokeinterface java/util/concurrent/locks/Lock/lock()V 0
L0:
aload 0
invokevirtual com/a/b/n/a/eg/isShutdown()Z
ifeq L2
new java/util/concurrent/RejectedExecutionException
dup
ldc "Executor already shutdown"
invokespecial java/util/concurrent/RejectedExecutionException/<init>(Ljava/lang/String;)V
athrow
L1:
astore 1
aload 0
getfield com/a/b/n/a/eg/a Ljava/util/concurrent/locks/Lock;
invokeinterface java/util/concurrent/locks/Lock/unlock()V 0
aload 1
athrow
L2:
aload 0
aload 0
getfield com/a/b/n/a/eg/c I
iconst_1
iadd
putfield com/a/b/n/a/eg/c I
L3:
aload 0
getfield com/a/b/n/a/eg/a Ljava/util/concurrent/locks/Lock;
invokeinterface java/util/concurrent/locks/Lock/unlock()V 0
L4:
aload 1
invokeinterface java/lang/Runnable/run()V 0
L5:
aload 0
invokespecial com/a/b/n/a/eg/b()V
return
L6:
astore 1
aload 0
invokespecial com/a/b/n/a/eg/b()V
aload 1
athrow
.limit locals 2
.limit stack 3
.end method

.method public final isShutdown()Z
.catch all from L0 to L1 using L2
aload 0
getfield com/a/b/n/a/eg/a Ljava/util/concurrent/locks/Lock;
invokeinterface java/util/concurrent/locks/Lock/lock()V 0
L0:
aload 0
getfield com/a/b/n/a/eg/d Z
istore 1
L1:
aload 0
getfield com/a/b/n/a/eg/a Ljava/util/concurrent/locks/Lock;
invokeinterface java/util/concurrent/locks/Lock/unlock()V 0
iload 1
ireturn
L2:
astore 2
aload 0
getfield com/a/b/n/a/eg/a Ljava/util/concurrent/locks/Lock;
invokeinterface java/util/concurrent/locks/Lock/unlock()V 0
aload 2
athrow
.limit locals 3
.limit stack 1
.end method

.method public final isTerminated()Z
.catch all from L0 to L1 using L2
aload 0
getfield com/a/b/n/a/eg/a Ljava/util/concurrent/locks/Lock;
invokeinterface java/util/concurrent/locks/Lock/lock()V 0
L0:
aload 0
getfield com/a/b/n/a/eg/d Z
ifeq L3
aload 0
getfield com/a/b/n/a/eg/c I
istore 1
L1:
iload 1
ifne L3
iconst_1
istore 2
L4:
aload 0
getfield com/a/b/n/a/eg/a Ljava/util/concurrent/locks/Lock;
invokeinterface java/util/concurrent/locks/Lock/unlock()V 0
iload 2
ireturn
L3:
iconst_0
istore 2
goto L4
L2:
astore 3
aload 0
getfield com/a/b/n/a/eg/a Ljava/util/concurrent/locks/Lock;
invokeinterface java/util/concurrent/locks/Lock/unlock()V 0
aload 3
athrow
.limit locals 4
.limit stack 1
.end method

.method public final shutdown()V
.catch all from L0 to L1 using L2
aload 0
getfield com/a/b/n/a/eg/a Ljava/util/concurrent/locks/Lock;
invokeinterface java/util/concurrent/locks/Lock/lock()V 0
L0:
aload 0
iconst_1
putfield com/a/b/n/a/eg/d Z
L1:
aload 0
getfield com/a/b/n/a/eg/a Ljava/util/concurrent/locks/Lock;
invokeinterface java/util/concurrent/locks/Lock/unlock()V 0
return
L2:
astore 1
aload 0
getfield com/a/b/n/a/eg/a Ljava/util/concurrent/locks/Lock;
invokeinterface java/util/concurrent/locks/Lock/unlock()V 0
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method public final shutdownNow()Ljava/util/List;
aload 0
invokevirtual com/a/b/n/a/eg/shutdown()V
invokestatic java/util/Collections/emptyList()Ljava/util/List;
areturn
.limit locals 1
.limit stack 1
.end method
