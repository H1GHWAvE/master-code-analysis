.bytecode 50.0
.class public final synchronized com/a/b/n/a/gs
.super java/lang/Object
.annotation invisible Lcom/a/b/a/a;
.end annotation

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/util/concurrent/BlockingQueue;)Ljava/lang/Object;
.catch java/lang/InterruptedException from L0 to L1 using L2
.catch all from L0 to L1 using L3
iconst_0
istore 1
L0:
aload 0
invokeinterface java/util/concurrent/BlockingQueue/take()Ljava/lang/Object; 0
astore 2
L1:
iload 1
ifeq L4
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
L4:
aload 2
areturn
L2:
astore 2
iconst_1
istore 1
goto L0
L3:
astore 0
iload 1
ifeq L5
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
L5:
aload 0
athrow
.limit locals 3
.limit stack 1
.end method

.method public static a(Ljava/util/concurrent/Future;)Ljava/lang/Object;
.catch java/lang/InterruptedException from L0 to L1 using L2
.catch all from L0 to L1 using L3
iconst_0
istore 1
L0:
aload 0
invokeinterface java/util/concurrent/Future/get()Ljava/lang/Object; 0
astore 2
L1:
iload 1
ifeq L4
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
L4:
aload 2
areturn
L2:
astore 2
iconst_1
istore 1
goto L0
L3:
astore 0
iload 1
ifeq L5
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
L5:
aload 0
athrow
.limit locals 3
.limit stack 1
.end method

.method public static a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch java/lang/InterruptedException from L5 to L6 using L7
.catch all from L5 to L6 using L2
.catch all from L8 to L9 using L2
iconst_0
istore 6
iconst_0
istore 5
iload 6
istore 4
L0:
aload 3
lload 1
invokevirtual java/util/concurrent/TimeUnit/toNanos(J)J
lstore 7
L1:
iload 6
istore 4
L3:
invokestatic java/lang/System/nanoTime()J
lstore 9
L4:
lload 7
lstore 1
L10:
iload 5
istore 4
L5:
aload 0
lload 1
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
invokeinterface java/util/concurrent/Future/get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object; 3
astore 3
L6:
iload 5
ifeq L11
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
L11:
aload 3
areturn
L7:
astore 3
iconst_1
istore 4
iconst_1
istore 5
L8:
invokestatic java/lang/System/nanoTime()J
lstore 1
L9:
lload 9
lload 7
ladd
lload 1
lsub
lstore 1
goto L10
L2:
astore 0
iload 4
ifeq L12
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
L12:
aload 0
athrow
.limit locals 11
.limit stack 4
.end method

.method private static a(JLjava/util/concurrent/TimeUnit;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch java/lang/InterruptedException from L5 to L6 using L7
.catch all from L5 to L6 using L2
.catch all from L8 to L9 using L2
iconst_0
istore 5
iconst_0
istore 4
iload 5
istore 3
L0:
aload 2
lload 0
invokevirtual java/util/concurrent/TimeUnit/toNanos(J)J
lstore 6
L1:
iload 5
istore 3
L3:
invokestatic java/lang/System/nanoTime()J
lstore 8
L4:
lload 6
lstore 0
L10:
iload 4
istore 3
L5:
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
lload 0
invokevirtual java/util/concurrent/TimeUnit/sleep(J)V
L6:
iload 4
ifeq L11
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
L11:
return
L7:
astore 2
iconst_1
istore 3
iconst_1
istore 4
L8:
invokestatic java/lang/System/nanoTime()J
lstore 0
L9:
lload 8
lload 6
ladd
lload 0
lsub
lstore 0
goto L10
L2:
astore 2
iload 3
ifeq L12
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
L12:
aload 2
athrow
.limit locals 10
.limit stack 4
.end method

.method private static a(Ljava/lang/Thread;)V
.catch java/lang/InterruptedException from L0 to L1 using L2
.catch all from L0 to L1 using L3
iconst_0
istore 1
L0:
aload 0
invokevirtual java/lang/Thread/join()V
L1:
iload 1
ifeq L4
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
L4:
return
L2:
astore 2
iconst_1
istore 1
goto L0
L3:
astore 0
iload 1
ifeq L5
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
L5:
aload 0
athrow
.limit locals 3
.limit stack 1
.end method

.method private static a(Ljava/lang/Thread;JLjava/util/concurrent/TimeUnit;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch java/lang/InterruptedException from L5 to L6 using L7
.catch all from L5 to L6 using L2
.catch all from L8 to L9 using L2
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
iconst_0
istore 6
iconst_0
istore 5
iload 6
istore 4
L0:
aload 3
lload 1
invokevirtual java/util/concurrent/TimeUnit/toNanos(J)J
lstore 7
L1:
iload 6
istore 4
L3:
invokestatic java/lang/System/nanoTime()J
lstore 9
L4:
lload 7
lstore 1
L10:
iload 5
istore 4
L5:
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
aload 0
lload 1
invokevirtual java/util/concurrent/TimeUnit/timedJoin(Ljava/lang/Thread;J)V
L6:
iload 5
ifeq L11
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
L11:
return
L7:
astore 3
iconst_1
istore 4
iconst_1
istore 5
L8:
invokestatic java/lang/System/nanoTime()J
lstore 1
L9:
lload 9
lload 7
ladd
lload 1
lsub
lstore 1
goto L10
L2:
astore 0
iload 4
ifeq L12
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
L12:
aload 0
athrow
.limit locals 11
.limit stack 4
.end method

.method private static a(Ljava/util/concurrent/BlockingQueue;Ljava/lang/Object;)V
.catch java/lang/InterruptedException from L0 to L1 using L2
.catch all from L0 to L1 using L3
iconst_0
istore 2
L0:
aload 0
aload 1
invokeinterface java/util/concurrent/BlockingQueue/put(Ljava/lang/Object;)V 1
L1:
iload 2
ifeq L4
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
L4:
return
L2:
astore 3
iconst_1
istore 2
goto L0
L3:
astore 0
iload 2
ifeq L5
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
L5:
aload 0
athrow
.limit locals 4
.limit stack 2
.end method

.method private static a(Ljava/util/concurrent/CountDownLatch;)V
.catch java/lang/InterruptedException from L0 to L1 using L2
.catch all from L0 to L1 using L3
iconst_0
istore 1
L0:
aload 0
invokevirtual java/util/concurrent/CountDownLatch/await()V
L1:
iload 1
ifeq L4
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
L4:
return
L2:
astore 2
iconst_1
istore 1
goto L0
L3:
astore 0
iload 1
ifeq L5
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
L5:
aload 0
athrow
.limit locals 3
.limit stack 1
.end method

.method private static a(Ljava/util/concurrent/CountDownLatch;JLjava/util/concurrent/TimeUnit;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch java/lang/InterruptedException from L5 to L6 using L7
.catch all from L5 to L6 using L2
.catch all from L8 to L9 using L2
iconst_0
istore 6
iconst_0
istore 5
iload 6
istore 4
L0:
aload 3
lload 1
invokevirtual java/util/concurrent/TimeUnit/toNanos(J)J
lstore 7
L1:
iload 6
istore 4
L3:
invokestatic java/lang/System/nanoTime()J
lstore 9
L4:
lload 7
lstore 1
L10:
iload 5
istore 4
L5:
aload 0
lload 1
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
invokevirtual java/util/concurrent/CountDownLatch/await(JLjava/util/concurrent/TimeUnit;)Z
istore 11
L6:
iload 5
ifeq L11
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
L11:
iload 11
ireturn
L7:
astore 3
iconst_1
istore 4
iconst_1
istore 5
L8:
invokestatic java/lang/System/nanoTime()J
lstore 1
L9:
lload 9
lload 7
ladd
lload 1
lsub
lstore 1
goto L10
L2:
astore 0
iload 4
ifeq L12
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
L12:
aload 0
athrow
.limit locals 12
.limit stack 4
.end method

.method private static a(Ljava/util/concurrent/Semaphore;JLjava/util/concurrent/TimeUnit;)Z
aload 0
lload 1
aload 3
invokestatic com/a/b/n/a/gs/b(Ljava/util/concurrent/Semaphore;JLjava/util/concurrent/TimeUnit;)Z
ireturn
.limit locals 4
.limit stack 4
.end method

.method private static b(Ljava/util/concurrent/Semaphore;JLjava/util/concurrent/TimeUnit;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch java/lang/InterruptedException from L5 to L6 using L7
.catch all from L5 to L6 using L2
.catch all from L8 to L9 using L10
iconst_0
istore 6
iconst_0
istore 5
iload 6
istore 4
L0:
aload 3
lload 1
invokevirtual java/util/concurrent/TimeUnit/toNanos(J)J
lstore 7
L1:
iload 6
istore 4
L3:
invokestatic java/lang/System/nanoTime()J
lstore 9
L4:
lload 7
lstore 1
L11:
iload 5
istore 4
L5:
aload 0
iconst_1
lload 1
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
invokevirtual java/util/concurrent/Semaphore/tryAcquire(IJLjava/util/concurrent/TimeUnit;)Z
istore 11
L6:
iload 5
ifeq L12
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
L12:
iload 11
ireturn
L7:
astore 3
L8:
invokestatic java/lang/System/nanoTime()J
lstore 1
L9:
lload 9
lload 7
ladd
lload 1
lsub
lstore 1
iconst_1
istore 5
goto L11
L2:
astore 0
L13:
iload 4
ifeq L14
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
L14:
aload 0
athrow
L10:
astore 0
iconst_1
istore 4
goto L13
.limit locals 12
.limit stack 5
.end method
