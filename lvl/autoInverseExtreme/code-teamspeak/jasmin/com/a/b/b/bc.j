.bytecode 50.0
.class public synchronized com/a/b/b/bc
.super java/lang/Object
.implements java/io/Closeable

.field private static final 'd' Ljava/util/logging/Logger;

.field private static final 'e' Ljava/lang/String; = "com.google.common.base.internal.Finalizer"

.field private static final 'f' Ljava/lang/reflect/Method;

.field final 'a' Ljava/lang/ref/ReferenceQueue;

.field final 'b' Ljava/lang/ref/PhantomReference;

.field final 'c' Z

.method static <clinit>()V
iconst_0
istore 0
ldc com/a/b/b/bc
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokestatic java/util/logging/Logger/getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;
putstatic com/a/b/b/bc/d Ljava/util/logging/Logger;
new com/a/b/b/bg
dup
invokespecial com/a/b/b/bg/<init>()V
astore 1
new com/a/b/b/bd
dup
invokespecial com/a/b/b/bd/<init>()V
astore 2
new com/a/b/b/be
dup
invokespecial com/a/b/b/be/<init>()V
astore 3
L0:
iload 0
iconst_3
if_icmpge L1
iconst_3
anewarray com/a/b/b/bf
dup
iconst_0
aload 1
aastore
dup
iconst_1
aload 2
aastore
dup
iconst_2
aload 3
aastore
iload 0
aaload
invokeinterface com/a/b/b/bf/a()Ljava/lang/Class; 0
astore 4
aload 4
ifnull L2
aload 4
invokestatic com/a/b/b/bc/a(Ljava/lang/Class;)Ljava/lang/reflect/Method;
putstatic com/a/b/b/bc/f Ljava/lang/reflect/Method;
return
L2:
iload 0
iconst_1
iadd
istore 0
goto L0
L1:
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
.limit locals 5
.limit stack 4
.end method

.method public <init>()V
.catch java/lang/IllegalAccessException from L0 to L1 using L2
.catch java/lang/Throwable from L0 to L1 using L3
iconst_1
istore 1
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/lang/ref/ReferenceQueue
dup
invokespecial java/lang/ref/ReferenceQueue/<init>()V
putfield com/a/b/b/bc/a Ljava/lang/ref/ReferenceQueue;
aload 0
new java/lang/ref/PhantomReference
dup
aload 0
aload 0
getfield com/a/b/b/bc/a Ljava/lang/ref/ReferenceQueue;
invokespecial java/lang/ref/PhantomReference/<init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V
putfield com/a/b/b/bc/b Ljava/lang/ref/PhantomReference;
L0:
getstatic com/a/b/b/bc/f Ljava/lang/reflect/Method;
aconst_null
iconst_3
anewarray java/lang/Object
dup
iconst_0
ldc com/a/b/b/bb
aastore
dup
iconst_1
aload 0
getfield com/a/b/b/bc/a Ljava/lang/ref/ReferenceQueue;
aastore
dup
iconst_2
aload 0
getfield com/a/b/b/bc/b Ljava/lang/ref/PhantomReference;
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L1:
aload 0
iload 1
putfield com/a/b/b/bc/c Z
return
L2:
astore 2
new java/lang/AssertionError
dup
aload 2
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
L3:
astore 2
getstatic com/a/b/b/bc/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
ldc "Failed to start reference finalizer thread. Reference cleanup will only occur when new references are created."
aload 2
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
iconst_0
istore 1
goto L1
.limit locals 3
.limit stack 6
.end method

.method private static transient a([Lcom/a/b/b/bf;)Ljava/lang/Class;
iconst_0
istore 1
L0:
iload 1
iconst_3
if_icmpge L1
aload 0
iload 1
aaload
invokeinterface com/a/b/b/bf/a()Ljava/lang/Class; 0
astore 2
aload 2
ifnull L2
aload 2
areturn
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
.limit locals 3
.limit stack 2
.end method

.method private static a(Ljava/lang/Class;)Ljava/lang/reflect/Method;
.catch java/lang/NoSuchMethodException from L0 to L1 using L2
L0:
aload 0
ldc "startFinalizer"
iconst_3
anewarray java/lang/Class
dup
iconst_0
ldc java/lang/Class
aastore
dup
iconst_1
ldc java/lang/ref/ReferenceQueue
aastore
dup
iconst_2
ldc java/lang/ref/PhantomReference
aastore
invokevirtual java/lang/Class/getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
astore 0
L1:
aload 0
areturn
L2:
astore 0
new java/lang/AssertionError
dup
aload 0
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 1
.limit stack 6
.end method

.method static synthetic b()Ljava/util/logging/Logger;
getstatic com/a/b/b/bc/d Ljava/util/logging/Logger;
areturn
.limit locals 0
.limit stack 1
.end method

.method final a()V
aload 0
getfield com/a/b/b/bc/c Z
ifeq L0
L1:
return
L0:
aload 0
getfield com/a/b/b/bc/a Ljava/lang/ref/ReferenceQueue;
invokevirtual java/lang/ref/ReferenceQueue/poll()Ljava/lang/ref/Reference;
astore 1
aload 1
ifnull L1
aload 1
invokevirtual java/lang/ref/Reference/clear()V
goto L0
.limit locals 2
.limit stack 1
.end method

.method public close()V
aload 0
getfield com/a/b/b/bc/b Ljava/lang/ref/PhantomReference;
invokevirtual java/lang/ref/PhantomReference/enqueue()Z
pop
aload 0
invokevirtual com/a/b/b/bc/a()V
return
.limit locals 1
.limit stack 1
.end method
