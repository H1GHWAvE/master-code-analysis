.bytecode 50.0
.class final synchronized com/a/b/b/eb
.super java/lang/Object
.implements com/a/b/b/dz
.implements java/io/Serializable
.annotation invisible Lcom/a/b/a/d;
.end annotation

.field private static final 'e' J = 0L


.field final 'a' Lcom/a/b/b/dz;

.field final 'b' J

.field volatile transient 'c' Ljava/lang/Object;

.field volatile transient 'd' J

.method <init>(Lcom/a/b/b/dz;JLjava/util/concurrent/TimeUnit;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/b/dz
putfield com/a/b/b/eb/a Lcom/a/b/b/dz;
aload 0
aload 4
lload 2
invokevirtual java/util/concurrent/TimeUnit/toNanos(J)J
putfield com/a/b/b/eb/b J
lload 2
lconst_0
lcmp
ifle L0
iconst_1
istore 5
L1:
iload 5
invokestatic com/a/b/b/cn/a(Z)V
return
L0:
iconst_0
istore 5
goto L1
.limit locals 6
.limit stack 4
.end method

.method public final a()Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
aload 0
getfield com/a/b/b/eb/d J
lstore 1
invokestatic java/lang/System/nanoTime()J
lstore 3
lload 1
lconst_0
lcmp
ifeq L9
lload 3
lload 1
lsub
lconst_0
lcmp
iflt L6
L9:
aload 0
monitorenter
L0:
lload 1
aload 0
getfield com/a/b/b/eb/d J
lcmp
ifne L5
aload 0
getfield com/a/b/b/eb/a Lcom/a/b/b/dz;
invokeinterface com/a/b/b/dz/a()Ljava/lang/Object; 0
astore 5
aload 0
aload 5
putfield com/a/b/b/eb/c Ljava/lang/Object;
lload 3
aload 0
getfield com/a/b/b/eb/b J
ladd
lstore 3
L1:
lload 3
lstore 1
lload 3
lconst_0
lcmp
ifne L3
lconst_1
lstore 1
L3:
aload 0
lload 1
putfield com/a/b/b/eb/d J
aload 0
monitorexit
L4:
aload 5
areturn
L5:
aload 0
monitorexit
L6:
aload 0
getfield com/a/b/b/eb/c Ljava/lang/Object;
areturn
L2:
astore 5
L7:
aload 0
monitorexit
L8:
aload 5
athrow
.limit locals 6
.limit stack 4
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/b/eb/a Lcom/a/b/b/dz;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 3
aload 0
getfield com/a/b/b/eb/b J
lstore 1
new java/lang/StringBuilder
dup
aload 3
invokevirtual java/lang/String/length()I
bipush 62
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Suppliers.memoizeWithExpiration("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 1
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", NANOS)"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 4
.limit stack 4
.end method
