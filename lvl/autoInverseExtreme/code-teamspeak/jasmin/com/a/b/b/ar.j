.bytecode 50.0
.class public final synchronized com/a/b/b/ar
.super java/lang/Object

.field private static final 'a' Ljava/util/Map;

.method static <clinit>()V
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 0
aload 0
getstatic java/lang/Boolean/TYPE Ljava/lang/Class;
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokestatic com/a/b/b/ar/a(Ljava/util/Map;Ljava/lang/Class;Ljava/lang/Object;)V
aload 0
getstatic java/lang/Character/TYPE Ljava/lang/Class;
iconst_0
invokestatic java/lang/Character/valueOf(C)Ljava/lang/Character;
invokestatic com/a/b/b/ar/a(Ljava/util/Map;Ljava/lang/Class;Ljava/lang/Object;)V
aload 0
getstatic java/lang/Byte/TYPE Ljava/lang/Class;
iconst_0
invokestatic java/lang/Byte/valueOf(B)Ljava/lang/Byte;
invokestatic com/a/b/b/ar/a(Ljava/util/Map;Ljava/lang/Class;Ljava/lang/Object;)V
aload 0
getstatic java/lang/Short/TYPE Ljava/lang/Class;
iconst_0
invokestatic java/lang/Short/valueOf(S)Ljava/lang/Short;
invokestatic com/a/b/b/ar/a(Ljava/util/Map;Ljava/lang/Class;Ljava/lang/Object;)V
aload 0
getstatic java/lang/Integer/TYPE Ljava/lang/Class;
iconst_0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokestatic com/a/b/b/ar/a(Ljava/util/Map;Ljava/lang/Class;Ljava/lang/Object;)V
aload 0
getstatic java/lang/Long/TYPE Ljava/lang/Class;
lconst_0
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokestatic com/a/b/b/ar/a(Ljava/util/Map;Ljava/lang/Class;Ljava/lang/Object;)V
aload 0
getstatic java/lang/Float/TYPE Ljava/lang/Class;
fconst_0
invokestatic java/lang/Float/valueOf(F)Ljava/lang/Float;
invokestatic com/a/b/b/ar/a(Ljava/util/Map;Ljava/lang/Class;Ljava/lang/Object;)V
aload 0
getstatic java/lang/Double/TYPE Ljava/lang/Class;
dconst_0
invokestatic java/lang/Double/valueOf(D)Ljava/lang/Double;
invokestatic com/a/b/b/ar/a(Ljava/util/Map;Ljava/lang/Class;Ljava/lang/Object;)V
aload 0
invokestatic java/util/Collections/unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;
putstatic com/a/b/b/ar/a Ljava/util/Map;
return
.limit locals 1
.limit stack 4
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/Class;)Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
getstatic com/a/b/b/ar/a Ljava/util/Map;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
.limit locals 1
.limit stack 2
.end method

.method private static a(Ljava/util/Map;Ljava/lang/Class;Ljava/lang/Object;)V
aload 0
aload 1
aload 2
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
return
.limit locals 3
.limit stack 3
.end method
