.bytecode 50.0
.class public final synchronized com/a/b/b/cn
.super java/lang/Object
.annotation invisible Lcom/a/b/a/b;
.end annotation

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(II)I
iload 0
iflt L0
iload 0
iload 1
if_icmplt L1
L0:
iload 0
ifge L2
ldc "%s (%s) must not be negative"
iconst_2
anewarray java/lang/Object
dup
iconst_0
ldc "index"
aastore
dup
iconst_1
iload 0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
astore 2
L3:
new java/lang/IndexOutOfBoundsException
dup
aload 2
invokespecial java/lang/IndexOutOfBoundsException/<init>(Ljava/lang/String;)V
athrow
L2:
iload 1
ifge L4
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
bipush 26
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "negative size: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L4:
ldc "%s (%s) must be less than size (%s)"
iconst_3
anewarray java/lang/Object
dup
iconst_0
ldc "index"
aastore
dup
iconst_1
iload 0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_2
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
astore 2
goto L3
L1:
iload 0
ireturn
.limit locals 3
.limit stack 5
.end method

.method private static a(IILjava/lang/String;)I
.annotation visibleparam 3 Ljavax/annotation/Nullable;
.end annotation
iload 0
iflt L0
iload 0
iload 1
if_icmplt L1
L0:
iload 0
ifge L2
ldc "%s (%s) must not be negative"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 2
aastore
dup
iconst_1
iload 0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
astore 2
L3:
new java/lang/IndexOutOfBoundsException
dup
aload 2
invokespecial java/lang/IndexOutOfBoundsException/<init>(Ljava/lang/String;)V
athrow
L2:
iload 1
ifge L4
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
bipush 26
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "negative size: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L4:
ldc "%s (%s) must be less than size (%s)"
iconst_3
anewarray java/lang/Object
dup
iconst_0
aload 2
aastore
dup
iconst_1
iload 0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_2
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
astore 2
goto L3
L1:
iload 0
ireturn
.limit locals 3
.limit stack 5
.end method

.method public static a(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
ifnonnull L0
new java/lang/NullPointerException
dup
invokespecial java/lang/NullPointerException/<init>()V
athrow
L0:
aload 0
areturn
.limit locals 1
.limit stack 2
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
ifnonnull L0
new java/lang/NullPointerException
dup
aload 1
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokespecial java/lang/NullPointerException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
areturn
.limit locals 2
.limit stack 3
.end method

.method public static transient a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 3 Ljavax/annotation/Nullable;
.end annotation
aload 0
ifnonnull L0
new java/lang/NullPointerException
dup
aload 1
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokespecial java/lang/NullPointerException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
areturn
.limit locals 3
.limit stack 4
.end method

.method static transient a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
iconst_0
istore 2
aload 0
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 0
new java/lang/StringBuilder
dup
aload 0
invokevirtual java/lang/String/length()I
aload 1
arraylength
bipush 16
imul
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
astore 5
iconst_0
istore 3
L0:
iload 2
aload 1
arraylength
if_icmpge L1
aload 0
ldc "%s"
iload 3
invokevirtual java/lang/String/indexOf(Ljava/lang/String;I)I
istore 4
iload 4
iconst_m1
if_icmpeq L1
aload 5
aload 0
iload 3
iload 4
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 5
aload 1
iload 2
aaload
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
pop
iload 4
iconst_2
iadd
istore 3
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 5
aload 0
iload 3
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
iload 2
aload 1
arraylength
if_icmpge L2
aload 5
ldc " ["
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 5
aload 1
iload 2
aaload
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
pop
iload 2
iconst_1
iadd
istore 2
L3:
iload 2
aload 1
arraylength
if_icmpge L4
aload 5
ldc ", "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 5
aload 1
iload 2
aaload
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
pop
iload 2
iconst_1
iadd
istore 2
goto L3
L4:
aload 5
bipush 93
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
L2:
aload 5
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 6
.limit stack 5
.end method

.method public static a(III)V
iload 0
iflt L0
iload 1
iload 0
if_icmplt L0
iload 1
iload 2
if_icmple L1
L0:
iload 0
iflt L2
iload 0
iload 2
if_icmple L3
L2:
iload 0
iload 2
ldc "start index"
invokestatic com/a/b/b/cn/d(IILjava/lang/String;)Ljava/lang/String;
astore 3
L4:
new java/lang/IndexOutOfBoundsException
dup
aload 3
invokespecial java/lang/IndexOutOfBoundsException/<init>(Ljava/lang/String;)V
athrow
L3:
iload 1
iflt L5
iload 1
iload 2
if_icmple L6
L5:
iload 1
iload 2
ldc "end index"
invokestatic com/a/b/b/cn/d(IILjava/lang/String;)Ljava/lang/String;
astore 3
goto L4
L6:
ldc "end index (%s) must not be less than start index (%s)"
iconst_2
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
iload 0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
astore 3
goto L4
L1:
return
.limit locals 4
.limit stack 5
.end method

.method public static a(Z)V
iload 0
ifne L0
new java/lang/IllegalArgumentException
dup
invokespecial java/lang/IllegalArgumentException/<init>()V
athrow
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public static a(ZLjava/lang/Object;)V
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
iload 0
ifne L0
new java/lang/IllegalArgumentException
dup
aload 1
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
return
.limit locals 2
.limit stack 3
.end method

.method public static transient a(ZLjava/lang/String;[Ljava/lang/Object;)V
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 3 Ljavax/annotation/Nullable;
.end annotation
iload 0
ifne L0
new java/lang/IllegalArgumentException
dup
aload 1
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
return
.limit locals 3
.limit stack 4
.end method

.method public static b(II)I
iload 0
iflt L0
iload 0
iload 1
if_icmple L1
L0:
new java/lang/IndexOutOfBoundsException
dup
iload 0
iload 1
ldc "index"
invokestatic com/a/b/b/cn/d(IILjava/lang/String;)Ljava/lang/String;
invokespecial java/lang/IndexOutOfBoundsException/<init>(Ljava/lang/String;)V
athrow
L1:
iload 0
ireturn
.limit locals 2
.limit stack 5
.end method

.method private static b(III)Ljava/lang/String;
iload 0
iflt L0
iload 0
iload 2
if_icmple L1
L0:
iload 0
iload 2
ldc "start index"
invokestatic com/a/b/b/cn/d(IILjava/lang/String;)Ljava/lang/String;
areturn
L1:
iload 1
iflt L2
iload 1
iload 2
if_icmple L3
L2:
iload 1
iload 2
ldc "end index"
invokestatic com/a/b/b/cn/d(IILjava/lang/String;)Ljava/lang/String;
areturn
L3:
ldc "end index (%s) must not be less than start index (%s)"
iconst_2
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
iload 0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
areturn
.limit locals 3
.limit stack 5
.end method

.method private static b(IILjava/lang/String;)Ljava/lang/String;
iload 0
ifge L0
ldc "%s (%s) must not be negative"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 2
aastore
dup
iconst_1
iload 0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
areturn
L0:
iload 1
ifge L1
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
bipush 26
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "negative size: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
ldc "%s (%s) must be less than size (%s)"
iconst_3
anewarray java/lang/Object
dup
iconst_0
aload 2
aastore
dup
iconst_1
iload 0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_2
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
areturn
.limit locals 3
.limit stack 5
.end method

.method public static b(Z)V
iload 0
ifne L0
new java/lang/IllegalStateException
dup
invokespecial java/lang/IllegalStateException/<init>()V
athrow
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public static b(ZLjava/lang/Object;)V
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
iload 0
ifne L0
new java/lang/IllegalStateException
dup
aload 1
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
return
.limit locals 2
.limit stack 3
.end method

.method public static transient b(ZLjava/lang/String;[Ljava/lang/Object;)V
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 3 Ljavax/annotation/Nullable;
.end annotation
iload 0
ifne L0
new java/lang/IllegalStateException
dup
aload 1
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
return
.limit locals 3
.limit stack 4
.end method

.method private static c(IILjava/lang/String;)I
.annotation visibleparam 3 Ljavax/annotation/Nullable;
.end annotation
iload 0
iflt L0
iload 0
iload 1
if_icmple L1
L0:
new java/lang/IndexOutOfBoundsException
dup
iload 0
iload 1
aload 2
invokestatic com/a/b/b/cn/d(IILjava/lang/String;)Ljava/lang/String;
invokespecial java/lang/IndexOutOfBoundsException/<init>(Ljava/lang/String;)V
athrow
L1:
iload 0
ireturn
.limit locals 3
.limit stack 5
.end method

.method private static d(IILjava/lang/String;)Ljava/lang/String;
iload 0
ifge L0
ldc "%s (%s) must not be negative"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 2
aastore
dup
iconst_1
iload 0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
areturn
L0:
iload 1
ifge L1
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
bipush 26
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "negative size: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
ldc "%s (%s) must not be greater than size (%s)"
iconst_3
anewarray java/lang/Object
dup
iconst_0
aload 2
aastore
dup
iconst_1
iload 0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_2
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
areturn
.limit locals 3
.limit stack 5
.end method
