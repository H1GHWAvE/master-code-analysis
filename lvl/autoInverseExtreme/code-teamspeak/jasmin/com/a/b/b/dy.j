.bytecode 50.0
.class public final synchronized com/a/b/b/dy
.super java/lang/Object
.annotation invisible Lcom/a/b/a/b;
.end annotation

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
invokeinterface java/lang/CharSequence/length()I 0
aload 1
invokeinterface java/lang/CharSequence/length()I 0
invokestatic java/lang/Math/min(II)I
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 0
iload 2
invokeinterface java/lang/CharSequence/charAt(I)C 1
aload 1
iload 2
invokeinterface java/lang/CharSequence/charAt(I)C 1
if_icmpne L1
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 0
iload 2
iconst_1
isub
invokestatic com/a/b/b/dy/a(Ljava/lang/CharSequence;I)Z
ifne L2
iload 2
istore 3
aload 1
iload 2
iconst_1
isub
invokestatic com/a/b/b/dy/a(Ljava/lang/CharSequence;I)Z
ifeq L3
L2:
iload 2
iconst_1
isub
istore 3
L3:
aload 0
iconst_0
iload 3
invokeinterface java/lang/CharSequence/subSequence(II)Ljava/lang/CharSequence; 2
invokeinterface java/lang/CharSequence/toString()Ljava/lang/String; 0
areturn
.limit locals 4
.limit stack 3
.end method

.method private static a(Ljava/lang/String;I)Ljava/lang/String;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
iload 1
iconst_1
if_icmpgt L0
iload 1
iflt L1
iconst_1
istore 4
L2:
iload 4
ldc "invalid count: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
iload 1
ifne L3
ldc ""
astore 0
L3:
aload 0
areturn
L1:
iconst_0
istore 4
goto L2
L0:
aload 0
invokevirtual java/lang/String/length()I
istore 2
iload 2
i2l
iload 1
i2l
lmul
lstore 5
lload 5
l2i
istore 3
iload 3
i2l
lload 5
lcmp
ifeq L4
new java/lang/ArrayIndexOutOfBoundsException
dup
new java/lang/StringBuilder
dup
bipush 51
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Required array size too large: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 5
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/ArrayIndexOutOfBoundsException/<init>(Ljava/lang/String;)V
athrow
L4:
iload 3
newarray char
astore 7
aload 0
iconst_0
iload 2
aload 7
iconst_0
invokevirtual java/lang/String/getChars(II[CI)V
iload 2
istore 1
L5:
iload 1
iload 3
iload 1
isub
if_icmpge L6
aload 7
iconst_0
aload 7
iload 1
iload 1
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
iload 1
iconst_1
ishl
istore 1
goto L5
L6:
aload 7
iconst_0
aload 7
iload 1
iload 3
iload 1
isub
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
new java/lang/String
dup
aload 7
invokespecial java/lang/String/<init>([C)V
areturn
.limit locals 8
.limit stack 6
.end method

.method private static a(Ljava/lang/String;IC)Ljava/lang/String;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
invokevirtual java/lang/String/length()I
iload 1
if_icmplt L0
aload 0
areturn
L0:
new java/lang/StringBuilder
dup
iload 1
invokespecial java/lang/StringBuilder/<init>(I)V
astore 4
aload 0
invokevirtual java/lang/String/length()I
istore 3
L1:
iload 3
iload 1
if_icmpge L2
aload 4
iload 2
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
iload 3
iconst_1
iadd
istore 3
goto L1
L2:
aload 4
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 4
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 5
.limit stack 3
.end method

.method private static a(Ljava/lang/CharSequence;I)Z
.annotation invisible Lcom/a/b/a/d;
.end annotation
iload 1
iflt L0
iload 1
aload 0
invokeinterface java/lang/CharSequence/length()I 0
iconst_2
isub
if_icmpgt L0
aload 0
iload 1
invokeinterface java/lang/CharSequence/charAt(I)C 1
invokestatic java/lang/Character/isHighSurrogate(C)Z
ifeq L0
aload 0
iload 1
iconst_1
iadd
invokeinterface java/lang/CharSequence/charAt(I)C 1
invokestatic java/lang/Character/isLowSurrogate(C)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 3
.end method

.method public static a(Ljava/lang/String;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
ifnull L0
aload 0
invokevirtual java/lang/String/length()I
ifne L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
invokeinterface java/lang/CharSequence/length()I 0
aload 1
invokeinterface java/lang/CharSequence/length()I 0
invokestatic java/lang/Math/min(II)I
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 0
aload 0
invokeinterface java/lang/CharSequence/length()I 0
iload 2
isub
iconst_1
isub
invokeinterface java/lang/CharSequence/charAt(I)C 1
aload 1
aload 1
invokeinterface java/lang/CharSequence/length()I 0
iload 2
isub
iconst_1
isub
invokeinterface java/lang/CharSequence/charAt(I)C 1
if_icmpne L1
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 0
aload 0
invokeinterface java/lang/CharSequence/length()I 0
iload 2
isub
iconst_1
isub
invokestatic com/a/b/b/dy/a(Ljava/lang/CharSequence;I)Z
ifne L2
iload 2
istore 3
aload 1
aload 1
invokeinterface java/lang/CharSequence/length()I 0
iload 2
isub
iconst_1
isub
invokestatic com/a/b/b/dy/a(Ljava/lang/CharSequence;I)Z
ifeq L3
L2:
iload 2
iconst_1
isub
istore 3
L3:
aload 0
aload 0
invokeinterface java/lang/CharSequence/length()I 0
iload 3
isub
aload 0
invokeinterface java/lang/CharSequence/length()I 0
invokeinterface java/lang/CharSequence/subSequence(II)Ljava/lang/CharSequence; 2
invokeinterface java/lang/CharSequence/toString()Ljava/lang/String; 0
areturn
.limit locals 4
.limit stack 4
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
astore 1
aload 0
ifnonnull L0
ldc ""
astore 1
L0:
aload 1
areturn
.limit locals 2
.limit stack 1
.end method

.method private static b(Ljava/lang/String;IC)Ljava/lang/String;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
invokevirtual java/lang/String/length()I
iload 1
if_icmplt L0
aload 0
areturn
L0:
new java/lang/StringBuilder
dup
iload 1
invokespecial java/lang/StringBuilder/<init>(I)V
astore 4
aload 4
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 0
invokevirtual java/lang/String/length()I
istore 3
L1:
iload 3
iload 1
if_icmpge L2
aload 4
iload 2
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
iload 3
iconst_1
iadd
istore 3
goto L1
L2:
aload 4
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 5
.limit stack 3
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
astore 1
aload 0
invokestatic com/a/b/b/dy/a(Ljava/lang/String;)Z
ifeq L0
aconst_null
astore 1
L0:
aload 1
areturn
.limit locals 2
.limit stack 1
.end method
