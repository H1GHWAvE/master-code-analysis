.bytecode 50.0
.class final synchronized com/a/b/b/bq
.super java/lang/Object
.implements com/a/b/b/bj
.implements java/io/Serializable

.field private static final 'b' J = 0L


.field final 'a' Ljava/util/Map;

.method <init>(Ljava/util/Map;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Map
putfield com/a/b/b/bq/a Ljava/util/Map;
return
.limit locals 2
.limit stack 2
.end method

.method public final e(Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/b/bq/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
astore 3
aload 3
ifnonnull L0
aload 0
getfield com/a/b/b/bq/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ifeq L1
L0:
iconst_1
istore 2
L2:
iload 2
ldc "Key '%s' not present in map"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 3
areturn
L1:
iconst_0
istore 2
goto L2
.limit locals 4
.limit stack 6
.end method

.method public final equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
instanceof com/a/b/b/bq
ifeq L0
aload 1
checkcast com/a/b/b/bq
astore 1
aload 0
getfield com/a/b/b/bq/a Ljava/util/Map;
aload 1
getfield com/a/b/b/bq/a Ljava/util/Map;
invokeinterface java/util/Map/equals(Ljava/lang/Object;)Z 1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final hashCode()I
aload 0
getfield com/a/b/b/bq/a Ljava/util/Map;
invokeinterface java/util/Map/hashCode()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/b/bq/a Ljava/util/Map;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 8
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "forMap("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 4
.end method
