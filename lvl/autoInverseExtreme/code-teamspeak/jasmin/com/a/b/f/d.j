.bytecode 50.0
.class final synchronized com/a/b/f/d
.super java/lang/Object

.field private final 'a' Ljava/lang/String;

.field private final 'b' Ljava/util/List;

.method <init>(Ljava/lang/reflect/Method;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokevirtual java/lang/reflect/Method/getName()Ljava/lang/String;
putfield com/a/b/f/d/a Ljava/lang/String;
aload 0
aload 1
invokevirtual java/lang/reflect/Method/getParameterTypes()[Ljava/lang/Class;
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
putfield com/a/b/f/d/b Ljava/util/List;
return
.limit locals 2
.limit stack 2
.end method

.method public final equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
iconst_0
istore 3
iload 3
istore 2
aload 1
instanceof com/a/b/f/d
ifeq L0
aload 1
checkcast com/a/b/f/d
astore 1
iload 3
istore 2
aload 0
getfield com/a/b/f/d/a Ljava/lang/String;
aload 1
getfield com/a/b/f/d/a Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
iload 3
istore 2
aload 0
getfield com/a/b/f/d/b Ljava/util/List;
aload 1
getfield com/a/b/f/d/b Ljava/util/List;
invokeinterface java/util/List/equals(Ljava/lang/Object;)Z 1
ifeq L0
iconst_1
istore 2
L0:
iload 2
ireturn
.limit locals 4
.limit stack 2
.end method

.method public final hashCode()I
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/f/d/a Ljava/lang/String;
aastore
dup
iconst_1
aload 0
getfield com/a/b/f/d/b Ljava/util/List;
aastore
invokestatic java/util/Arrays/hashCode([Ljava/lang/Object;)I
ireturn
.limit locals 1
.limit stack 4
.end method
