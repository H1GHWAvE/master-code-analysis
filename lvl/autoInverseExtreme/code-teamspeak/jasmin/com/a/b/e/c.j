.bytecode 50.0
.class public synchronized abstract com/a/b/e/c
.super com/a/b/e/p
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation invisible Lcom/a/b/a/b;
.end annotation

.field private final 'a' [[C

.field private final 'b' I

.field private final 'c' I

.field private final 'd' I

.field private final 'e' C

.field private final 'f' C

.method private <init>(Lcom/a/b/e/b;II)V
aload 0
invokespecial com/a/b/e/p/<init>()V
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
aload 1
getfield com/a/b/e/b/a [[C
putfield com/a/b/e/c/a [[C
aload 0
aload 0
getfield com/a/b/e/c/a [[C
arraylength
putfield com/a/b/e/c/b I
iload 2
istore 5
iload 3
istore 4
iload 3
iload 2
if_icmpge L0
iconst_m1
istore 4
ldc_w 2147483647
istore 5
L0:
aload 0
iload 5
putfield com/a/b/e/c/c I
aload 0
iload 4
putfield com/a/b/e/c/d I
iload 5
ldc_w 55296
if_icmplt L1
aload 0
ldc_w 65535
putfield com/a/b/e/c/e C
aload 0
iconst_0
putfield com/a/b/e/c/f C
return
L1:
aload 0
iload 5
i2c
putfield com/a/b/e/c/e C
aload 0
iload 4
ldc_w 55295
invokestatic java/lang/Math/min(II)I
i2c
putfield com/a/b/e/c/f C
return
.limit locals 6
.limit stack 3
.end method

.method private <init>(Ljava/util/Map;II)V
aload 0
aload 1
invokestatic com/a/b/e/b/a(Ljava/util/Map;)Lcom/a/b/e/b;
iload 2
iload 3
invokespecial com/a/b/e/c/<init>(Lcom/a/b/e/b;II)V
return
.limit locals 4
.limit stack 4
.end method

.method protected final a(Ljava/lang/CharSequence;II)I
L0:
iload 2
iload 3
if_icmpge L1
aload 1
iload 2
invokeinterface java/lang/CharSequence/charAt(I)C 1
istore 4
iload 4
aload 0
getfield com/a/b/e/c/b I
if_icmpge L2
aload 0
getfield com/a/b/e/c/a [[C
iload 4
aaload
ifnonnull L1
L2:
iload 4
aload 0
getfield com/a/b/e/c/f C
if_icmpgt L1
iload 4
aload 0
getfield com/a/b/e/c/e C
if_icmplt L1
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
iload 2
ireturn
.limit locals 5
.limit stack 2
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
iconst_0
istore 2
L0:
aload 1
astore 4
iload 2
aload 1
invokevirtual java/lang/String/length()I
if_icmpge L1
aload 1
iload 2
invokevirtual java/lang/String/charAt(I)C
istore 3
iload 3
aload 0
getfield com/a/b/e/c/b I
if_icmpge L2
aload 0
getfield com/a/b/e/c/a [[C
iload 3
aaload
ifnonnull L3
L2:
iload 3
aload 0
getfield com/a/b/e/c/f C
if_icmpgt L3
iload 3
aload 0
getfield com/a/b/e/c/e C
if_icmpge L4
L3:
aload 0
aload 1
iload 2
invokevirtual com/a/b/e/c/a(Ljava/lang/String;I)Ljava/lang/String;
astore 4
L1:
aload 4
areturn
L4:
iload 2
iconst_1
iadd
istore 2
goto L0
.limit locals 5
.limit stack 3
.end method

.method protected abstract a()[C
.end method

.method protected final a(I)[C
iload 1
aload 0
getfield com/a/b/e/c/b I
if_icmpge L0
aload 0
getfield com/a/b/e/c/a [[C
iload 1
aaload
astore 2
aload 2
ifnull L0
aload 2
areturn
L0:
iload 1
aload 0
getfield com/a/b/e/c/c I
if_icmplt L1
iload 1
aload 0
getfield com/a/b/e/c/d I
if_icmpgt L1
aconst_null
areturn
L1:
aload 0
invokevirtual com/a/b/e/c/a()[C
areturn
.limit locals 3
.limit stack 2
.end method
