.bytecode 50.0
.class synchronized abstract com/a/b/c/dl
.super java/lang/Number

.field static final 'a' Ljava/lang/ThreadLocal;

.field static final 'b' Ljava/util/Random;

.field static final 'c' I

.field private static final 'g' Lsun/misc/Unsafe;

.field private static final 'h' J

.field private static final 'i' J

.field volatile transient 'd' [Lcom/a/b/c/dn;

.field volatile transient 'e' J

.field volatile transient 'f' I

.method static <clinit>()V
.catch java/lang/Exception from L0 to L1 using L2
new java/lang/ThreadLocal
dup
invokespecial java/lang/ThreadLocal/<init>()V
putstatic com/a/b/c/dl/a Ljava/lang/ThreadLocal;
new java/util/Random
dup
invokespecial java/util/Random/<init>()V
putstatic com/a/b/c/dl/b Ljava/util/Random;
invokestatic java/lang/Runtime/getRuntime()Ljava/lang/Runtime;
invokevirtual java/lang/Runtime/availableProcessors()I
putstatic com/a/b/c/dl/c I
L0:
invokestatic com/a/b/c/dl/b()Lsun/misc/Unsafe;
putstatic com/a/b/c/dl/g Lsun/misc/Unsafe;
getstatic com/a/b/c/dl/g Lsun/misc/Unsafe;
ldc com/a/b/c/dl
ldc "base"
invokevirtual java/lang/Class/getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
invokevirtual sun/misc/Unsafe/objectFieldOffset(Ljava/lang/reflect/Field;)J
putstatic com/a/b/c/dl/h J
getstatic com/a/b/c/dl/g Lsun/misc/Unsafe;
ldc com/a/b/c/dl
ldc "busy"
invokevirtual java/lang/Class/getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
invokevirtual sun/misc/Unsafe/objectFieldOffset(Ljava/lang/reflect/Field;)J
putstatic com/a/b/c/dl/i J
L1:
return
L2:
astore 0
new java/lang/Error
dup
aload 0
invokespecial java/lang/Error/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method <init>()V
aload 0
invokespecial java/lang/Number/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private a()V
aload 0
getfield com/a/b/c/dl/d [Lcom/a/b/c/dn;
astore 3
aload 0
lconst_0
putfield com/a/b/c/dl/e J
aload 3
ifnull L0
aload 3
arraylength
istore 2
iconst_0
istore 1
L1:
iload 1
iload 2
if_icmpge L0
aload 3
iload 1
aaload
astore 4
aload 4
ifnull L2
aload 4
lconst_0
putfield com/a/b/c/dn/h J
L2:
iload 1
iconst_1
iadd
istore 1
goto L1
L0:
return
.limit locals 5
.limit stack 3
.end method

.method private a(J[IZ)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L7
.catch all from L8 to L9 using L7
.catch all from L10 to L11 using L12
aload 3
ifnonnull L13
getstatic com/a/b/c/dl/a Ljava/lang/ThreadLocal;
astore 13
iconst_1
newarray int
astore 3
aload 13
aload 3
invokevirtual java/lang/ThreadLocal/set(Ljava/lang/Object;)V
getstatic com/a/b/c/dl/b Ljava/util/Random;
invokevirtual java/util/Random/nextInt()I
istore 6
iload 6
istore 5
iload 6
ifne L14
iconst_1
istore 5
L14:
aload 3
iconst_0
iload 5
iastore
L15:
iconst_0
istore 6
iload 5
istore 7
L16:
aload 0
getfield com/a/b/c/dl/d [Lcom/a/b/c/dn;
astore 13
aload 13
ifnull L17
aload 13
arraylength
istore 8
iload 8
ifle L17
aload 13
iload 8
iconst_1
isub
iload 7
iand
aaload
astore 14
aload 14
ifnonnull L18
aload 0
getfield com/a/b/c/dl/f I
ifne L19
new com/a/b/c/dn
dup
lload 1
invokespecial com/a/b/c/dn/<init>(J)V
astore 13
aload 0
getfield com/a/b/c/dl/f I
ifne L19
aload 0
invokevirtual com/a/b/c/dl/c()Z
ifeq L19
iconst_0
istore 8
L0:
aload 0
getfield com/a/b/c/dl/d [Lcom/a/b/c/dn;
astore 14
L1:
iload 8
istore 5
aload 14
ifnull L20
L3:
aload 14
arraylength
istore 9
L4:
iload 8
istore 5
iload 9
ifle L20
iload 9
iconst_1
isub
iload 7
iand
istore 9
iload 8
istore 5
aload 14
iload 9
aaload
ifnonnull L20
aload 14
iload 9
aload 13
aastore
iconst_1
istore 5
L20:
aload 0
iconst_0
putfield com/a/b/c/dl/f I
iload 5
ifeq L16
L21:
return
L13:
aload 3
iconst_0
iaload
istore 5
goto L15
L2:
astore 3
aload 0
iconst_0
putfield com/a/b/c/dl/f I
aload 3
athrow
L19:
iconst_0
istore 5
iload 4
istore 12
L22:
iload 7
iload 7
bipush 13
ishl
ixor
istore 6
iload 6
iload 6
bipush 17
iushr
ixor
istore 6
iload 6
iload 6
iconst_5
ishl
ixor
istore 7
aload 3
iconst_0
iload 7
iastore
iload 5
istore 6
iload 12
istore 4
goto L16
L18:
iload 4
ifne L23
iconst_1
istore 12
iload 6
istore 5
goto L22
L23:
aload 14
getfield com/a/b/c/dn/h J
lstore 10
aload 14
lload 10
aload 0
lload 10
lload 1
invokevirtual com/a/b/c/dl/a(JJ)J
invokevirtual com/a/b/c/dn/a(JJ)Z
ifne L21
iload 8
getstatic com/a/b/c/dl/c I
if_icmpge L24
aload 0
getfield com/a/b/c/dl/d [Lcom/a/b/c/dn;
aload 13
if_acmpeq L25
L24:
iconst_0
istore 5
iload 4
istore 12
goto L22
L25:
iload 6
ifne L26
iconst_1
istore 5
iload 4
istore 12
goto L22
L26:
iload 6
istore 5
iload 4
istore 12
aload 0
getfield com/a/b/c/dl/f I
ifne L22
iload 6
istore 5
iload 4
istore 12
aload 0
invokevirtual com/a/b/c/dl/c()Z
ifeq L22
L5:
aload 0
getfield com/a/b/c/dl/d [Lcom/a/b/c/dn;
aload 13
if_acmpne L9
iload 8
iconst_1
ishl
anewarray com/a/b/c/dn
astore 14
L6:
iconst_0
istore 5
goto L27
L8:
aload 0
aload 14
putfield com/a/b/c/dl/d [Lcom/a/b/c/dn;
L9:
aload 0
iconst_0
putfield com/a/b/c/dl/f I
iconst_0
istore 6
goto L16
L7:
astore 3
aload 0
iconst_0
putfield com/a/b/c/dl/f I
aload 3
athrow
L17:
aload 0
getfield com/a/b/c/dl/f I
ifne L28
aload 0
getfield com/a/b/c/dl/d [Lcom/a/b/c/dn;
aload 13
if_acmpne L28
aload 0
invokevirtual com/a/b/c/dl/c()Z
ifeq L28
iconst_0
istore 5
L10:
aload 0
getfield com/a/b/c/dl/d [Lcom/a/b/c/dn;
aload 13
if_acmpne L29
iconst_2
anewarray com/a/b/c/dn
astore 13
aload 13
iload 7
iconst_1
iand
new com/a/b/c/dn
dup
lload 1
invokespecial com/a/b/c/dn/<init>(J)V
aastore
aload 0
aload 13
putfield com/a/b/c/dl/d [Lcom/a/b/c/dn;
L11:
iconst_1
istore 5
L29:
aload 0
iconst_0
putfield com/a/b/c/dl/f I
iload 5
ifne L21
goto L16
L12:
astore 3
aload 0
iconst_0
putfield com/a/b/c/dl/f I
aload 3
athrow
L28:
aload 0
getfield com/a/b/c/dl/e J
lstore 10
aload 0
lload 10
aload 0
lload 10
lload 1
invokevirtual com/a/b/c/dl/a(JJ)J
invokevirtual com/a/b/c/dl/b(JJ)Z
ifeq L16
return
L27:
iload 5
iload 8
if_icmpge L8
aload 14
iload 5
aload 13
iload 5
aaload
aastore
iload 5
iconst_1
iadd
istore 5
goto L27
.limit locals 15
.limit stack 8
.end method

.method private static b()Lsun/misc/Unsafe;
.catch java/lang/SecurityException from L0 to L1 using L2
.catch java/security/PrivilegedActionException from L3 to L4 using L5
L0:
invokestatic sun/misc/Unsafe/getUnsafe()Lsun/misc/Unsafe;
astore 0
L1:
aload 0
areturn
L2:
astore 0
L3:
new com/a/b/c/dm
dup
invokespecial com/a/b/c/dm/<init>()V
invokestatic java/security/AccessController/doPrivileged(Ljava/security/PrivilegedExceptionAction;)Ljava/lang/Object;
checkcast sun/misc/Unsafe
astore 0
L4:
aload 0
areturn
L5:
astore 0
new java/lang/RuntimeException
dup
ldc "Could not initialize intrinsics"
aload 0
invokevirtual java/security/PrivilegedActionException/getCause()Ljava/lang/Throwable;
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
.limit locals 1
.limit stack 4
.end method

.method static synthetic d()Lsun/misc/Unsafe;
invokestatic com/a/b/c/dl/b()Lsun/misc/Unsafe;
areturn
.limit locals 0
.limit stack 1
.end method

.method abstract a(JJ)J
.end method

.method final b(JJ)Z
getstatic com/a/b/c/dl/g Lsun/misc/Unsafe;
aload 0
getstatic com/a/b/c/dl/h J
lload 1
lload 3
invokevirtual sun/misc/Unsafe/compareAndSwapLong(Ljava/lang/Object;JJJ)Z
ireturn
.limit locals 5
.limit stack 8
.end method

.method final c()Z
getstatic com/a/b/c/dl/g Lsun/misc/Unsafe;
aload 0
getstatic com/a/b/c/dl/i J
iconst_0
iconst_1
invokevirtual sun/misc/Unsafe/compareAndSwapInt(Ljava/lang/Object;JII)Z
ireturn
.limit locals 1
.limit stack 6
.end method
