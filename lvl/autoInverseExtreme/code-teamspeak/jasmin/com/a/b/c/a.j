.bytecode 50.0
.class public synchronized abstract com/a/b/c/a
.super java/lang/Object
.implements com/a/b/c/e
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation invisible Lcom/a/b/a/b;
.end annotation

.method protected <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public final a(Ljava/lang/Iterable;)Lcom/a/b/d/jt;
invokestatic com/a/b/d/sz/d()Ljava/util/LinkedHashMap;
astore 2
aload 1
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
astore 3
aload 2
aload 3
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ifne L0
aload 0
aload 3
invokevirtual com/a/b/c/a/d(Ljava/lang/Object;)Ljava/lang/Object;
astore 4
aload 4
ifnull L0
aload 2
aload 3
aload 4
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
goto L0
L1:
aload 2
invokestatic com/a/b/d/jt/a(Ljava/util/Map;)Lcom/a/b/d/jt;
areturn
.limit locals 5
.limit stack 3
.end method

.method public final a(Ljava/lang/Object;Ljava/util/concurrent/Callable;)Ljava/lang/Object;
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 3
.limit stack 2
.end method

.method public final a()V
return
.limit locals 1
.limit stack 0
.end method

.method public final a(Ljava/lang/Object;)V
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 3
.limit stack 2
.end method

.method public final a(Ljava/util/Map;)V
aload 1
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 1
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 1
aload 1
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
pop
aload 1
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
pop
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final b()J
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 1
.limit stack 2
.end method

.method public final b(Ljava/lang/Iterable;)V
aload 1
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 1
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
pop
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final c()V
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 1
.limit stack 2
.end method

.method public final d()Lcom/a/b/c/ai;
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 1
.limit stack 2
.end method

.method public final e()Ljava/util/concurrent/ConcurrentMap;
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 1
.limit stack 2
.end method
