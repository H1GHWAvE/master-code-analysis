.bytecode 50.0
.class synchronized abstract com/a/b/c/bh
.super java/lang/Object
.implements java/util/Iterator

.field 'b' I

.field 'c' I

.field 'd' Lcom/a/b/c/bt;

.field 'e' Ljava/util/concurrent/atomic/AtomicReferenceArray;

.field 'f' Lcom/a/b/c/bs;

.field 'g' Lcom/a/b/c/ct;

.field 'h' Lcom/a/b/c/ct;

.field final synthetic 'i' Lcom/a/b/c/ao;

.method <init>(Lcom/a/b/c/ao;)V
aload 0
aload 1
putfield com/a/b/c/bh/i Lcom/a/b/c/ao;
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
getfield com/a/b/c/ao/i [Lcom/a/b/c/bt;
arraylength
iconst_1
isub
putfield com/a/b/c/bh/b I
aload 0
iconst_m1
putfield com/a/b/c/bh/c I
aload 0
invokespecial com/a/b/c/bh/b()V
return
.limit locals 2
.limit stack 3
.end method

.method private a(Lcom/a/b/c/bs;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
aconst_null
astore 6
L0:
aload 0
getfield com/a/b/c/bh/i Lcom/a/b/c/ao;
getfield com/a/b/c/ao/v Lcom/a/b/b/ej;
invokevirtual com/a/b/b/ej/a()J
lstore 2
aload 1
invokeinterface com/a/b/c/bs/d()Ljava/lang/Object; 0
astore 8
aload 0
getfield com/a/b/c/bh/i Lcom/a/b/c/ao;
astore 9
aload 1
invokeinterface com/a/b/c/bs/d()Ljava/lang/Object; 0
ifnonnull L5
L1:
aload 6
astore 5
L9:
aload 5
ifnull L10
L3:
aload 0
new com/a/b/c/ct
dup
aload 0
getfield com/a/b/c/bh/i Lcom/a/b/c/ao;
aload 8
aload 5
invokespecial com/a/b/c/ct/<init>(Lcom/a/b/c/ao;Ljava/lang/Object;Ljava/lang/Object;)V
putfield com/a/b/c/bh/g Lcom/a/b/c/ct;
L4:
aload 0
getfield com/a/b/c/bh/d Lcom/a/b/c/bt;
invokevirtual com/a/b/c/bt/a()V
iconst_1
ireturn
L5:
aload 1
invokeinterface com/a/b/c/bs/a()Lcom/a/b/c/cg; 0
invokeinterface com/a/b/c/cg/get()Ljava/lang/Object; 0
astore 7
L6:
aload 6
astore 5
aload 7
ifnull L9
L7:
aload 9
aload 1
lload 2
invokevirtual com/a/b/c/ao/a(Lcom/a/b/c/bs;J)Z
istore 4
L8:
aload 6
astore 5
iload 4
ifne L9
aload 7
astore 5
goto L9
L10:
aload 0
getfield com/a/b/c/bh/d Lcom/a/b/c/bt;
invokevirtual com/a/b/c/bt/a()V
iconst_0
ireturn
L2:
astore 1
aload 0
getfield com/a/b/c/bh/d Lcom/a/b/c/bt;
invokevirtual com/a/b/c/bt/a()V
aload 1
athrow
.limit locals 10
.limit stack 6
.end method

.method private b()V
aload 0
aconst_null
putfield com/a/b/c/bh/g Lcom/a/b/c/ct;
aload 0
invokespecial com/a/b/c/bh/c()Z
ifeq L0
L1:
return
L0:
aload 0
invokespecial com/a/b/c/bh/d()Z
ifne L1
L2:
aload 0
getfield com/a/b/c/bh/b I
iflt L1
aload 0
getfield com/a/b/c/bh/i Lcom/a/b/c/ao;
getfield com/a/b/c/ao/i [Lcom/a/b/c/bt;
astore 2
aload 0
getfield com/a/b/c/bh/b I
istore 1
aload 0
iload 1
iconst_1
isub
putfield com/a/b/c/bh/b I
aload 0
aload 2
iload 1
aaload
putfield com/a/b/c/bh/d Lcom/a/b/c/bt;
aload 0
getfield com/a/b/c/bh/d Lcom/a/b/c/bt;
getfield com/a/b/c/bt/b I
ifeq L2
aload 0
aload 0
getfield com/a/b/c/bh/d Lcom/a/b/c/bt;
getfield com/a/b/c/bt/f Ljava/util/concurrent/atomic/AtomicReferenceArray;
putfield com/a/b/c/bh/e Ljava/util/concurrent/atomic/AtomicReferenceArray;
aload 0
aload 0
getfield com/a/b/c/bh/e Ljava/util/concurrent/atomic/AtomicReferenceArray;
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/length()I
iconst_1
isub
putfield com/a/b/c/bh/c I
aload 0
invokespecial com/a/b/c/bh/d()Z
ifeq L2
return
.limit locals 3
.limit stack 3
.end method

.method private c()Z
aload 0
getfield com/a/b/c/bh/f Lcom/a/b/c/bs;
ifnull L0
aload 0
aload 0
getfield com/a/b/c/bh/f Lcom/a/b/c/bs;
invokeinterface com/a/b/c/bs/b()Lcom/a/b/c/bs; 0
putfield com/a/b/c/bh/f Lcom/a/b/c/bs;
L1:
aload 0
getfield com/a/b/c/bh/f Lcom/a/b/c/bs;
ifnull L0
aload 0
aload 0
getfield com/a/b/c/bh/f Lcom/a/b/c/bs;
invokespecial com/a/b/c/bh/a(Lcom/a/b/c/bs;)Z
ifeq L2
iconst_1
ireturn
L2:
aload 0
aload 0
getfield com/a/b/c/bh/f Lcom/a/b/c/bs;
invokeinterface com/a/b/c/bs/b()Lcom/a/b/c/bs; 0
putfield com/a/b/c/bh/f Lcom/a/b/c/bs;
goto L1
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private d()Z
L0:
aload 0
getfield com/a/b/c/bh/c I
iflt L1
aload 0
getfield com/a/b/c/bh/e Ljava/util/concurrent/atomic/AtomicReferenceArray;
astore 2
aload 0
getfield com/a/b/c/bh/c I
istore 1
aload 0
iload 1
iconst_1
isub
putfield com/a/b/c/bh/c I
aload 2
iload 1
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/get(I)Ljava/lang/Object;
checkcast com/a/b/c/bs
astore 2
aload 0
aload 2
putfield com/a/b/c/bh/f Lcom/a/b/c/bs;
aload 2
ifnull L0
aload 0
aload 0
getfield com/a/b/c/bh/f Lcom/a/b/c/bs;
invokespecial com/a/b/c/bh/a(Lcom/a/b/c/bs;)Z
ifne L2
aload 0
invokespecial com/a/b/c/bh/c()Z
ifeq L0
L2:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 3
.limit stack 3
.end method

.method final a()Lcom/a/b/c/ct;
aload 0
getfield com/a/b/c/bh/g Lcom/a/b/c/ct;
ifnonnull L0
new java/util/NoSuchElementException
dup
invokespecial java/util/NoSuchElementException/<init>()V
athrow
L0:
aload 0
aload 0
getfield com/a/b/c/bh/g Lcom/a/b/c/ct;
putfield com/a/b/c/bh/h Lcom/a/b/c/ct;
aload 0
invokespecial com/a/b/c/bh/b()V
aload 0
getfield com/a/b/c/bh/h Lcom/a/b/c/ct;
areturn
.limit locals 1
.limit stack 2
.end method

.method public hasNext()Z
aload 0
getfield com/a/b/c/bh/g Lcom/a/b/c/ct;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public abstract next()Ljava/lang/Object;
.end method

.method public remove()V
aload 0
getfield com/a/b/c/bh/h Lcom/a/b/c/ct;
ifnull L0
iconst_1
istore 1
L1:
iload 1
invokestatic com/a/b/b/cn/b(Z)V
aload 0
getfield com/a/b/c/bh/i Lcom/a/b/c/ao;
aload 0
getfield com/a/b/c/bh/h Lcom/a/b/c/ct;
invokevirtual com/a/b/c/ct/getKey()Ljava/lang/Object;
invokevirtual com/a/b/c/ao/remove(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
aconst_null
putfield com/a/b/c/bh/h Lcom/a/b/c/ct;
return
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 2
.end method
