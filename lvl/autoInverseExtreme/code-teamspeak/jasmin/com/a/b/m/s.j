.bytecode 50.0
.class public final synchronized com/a/b/m/s
.super java/lang/Object
.implements java/lang/reflect/AnnotatedElement
.annotation invisible Lcom/a/b/a/a;
.end annotation

.field private final 'a' Lcom/a/b/m/k;

.field private final 'b' I

.field private final 'c' Lcom/a/b/m/ae;

.field private final 'd' Lcom/a/b/d/jl;

.method <init>(Lcom/a/b/m/k;ILcom/a/b/m/ae;[Ljava/lang/annotation/Annotation;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/a/b/m/s/a Lcom/a/b/m/k;
aload 0
iload 2
putfield com/a/b/m/s/b I
aload 0
aload 3
putfield com/a/b/m/s/c Lcom/a/b/m/ae;
aload 0
aload 4
invokestatic com/a/b/d/jl/a([Ljava/lang/Object;)Lcom/a/b/d/jl;
putfield com/a/b/m/s/d Lcom/a/b/d/jl;
return
.limit locals 5
.limit stack 2
.end method

.method private a()Lcom/a/b/m/ae;
aload 0
getfield com/a/b/m/s/c Lcom/a/b/m/ae;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Ljava/lang/Class;)[Ljava/lang/annotation/Annotation;
aload 0
getfield com/a/b/m/s/d Lcom/a/b/d/jl;
invokestatic com/a/b/d/gd/a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;
aload 1
invokevirtual com/a/b/d/gd/a(Ljava/lang/Class;)Lcom/a/b/d/gd;
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
aload 1
invokestatic com/a/b/d/mq/a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;
checkcast [Ljava/lang/annotation/Annotation;
areturn
.limit locals 2
.limit stack 2
.end method

.method private b()Lcom/a/b/m/k;
aload 0
getfield com/a/b/m/s/a Lcom/a/b/m/k;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/a/b/m/s/d Lcom/a/b/d/jl;
invokestatic com/a/b/d/gd/a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;
aload 1
invokevirtual com/a/b/d/gd/a(Ljava/lang/Class;)Lcom/a/b/d/gd;
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 1
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokestatic com/a/b/b/ci/b(Ljava/lang/Object;)Lcom/a/b/b/ci;
astore 1
L1:
aload 1
invokevirtual com/a/b/b/ci/d()Ljava/lang/Object;
checkcast java/lang/annotation/Annotation
areturn
L0:
invokestatic com/a/b/b/ci/f()Lcom/a/b/b/ci;
astore 1
goto L1
.limit locals 2
.limit stack 2
.end method

.method private c(Ljava/lang/Class;)[Ljava/lang/annotation/Annotation;
aload 0
getfield com/a/b/m/s/d Lcom/a/b/d/jl;
invokestatic com/a/b/d/gd/a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;
aload 1
invokevirtual com/a/b/d/gd/a(Ljava/lang/Class;)Lcom/a/b/d/gd;
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
aload 1
invokestatic com/a/b/d/mq/a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;
checkcast [Ljava/lang/annotation/Annotation;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
iconst_0
istore 3
iload 3
istore 2
aload 1
instanceof com/a/b/m/s
ifeq L0
aload 1
checkcast com/a/b/m/s
astore 1
iload 3
istore 2
aload 0
getfield com/a/b/m/s/b I
aload 1
getfield com/a/b/m/s/b I
if_icmpne L0
iload 3
istore 2
aload 0
getfield com/a/b/m/s/a Lcom/a/b/m/k;
aload 1
getfield com/a/b/m/s/a Lcom/a/b/m/k;
invokevirtual com/a/b/m/k/equals(Ljava/lang/Object;)Z
ifeq L0
iconst_1
istore 2
L0:
iload 2
ireturn
.limit locals 4
.limit stack 2
.end method

.method public final getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/a/b/m/s/d Lcom/a/b/d/jl;
invokevirtual com/a/b/d/jl/iterator()Ljava/util/Iterator;
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/annotation/Annotation
astore 3
aload 1
aload 3
invokevirtual java/lang/Class/isInstance(Ljava/lang/Object;)Z
ifeq L0
aload 1
aload 3
invokevirtual java/lang/Class/cast(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/annotation/Annotation
areturn
L1:
aconst_null
areturn
.limit locals 4
.limit stack 2
.end method

.method public final getAnnotations()[Ljava/lang/annotation/Annotation;
aload 0
invokevirtual com/a/b/m/s/getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;
aload 0
getfield com/a/b/m/s/d Lcom/a/b/d/jl;
aload 0
getfield com/a/b/m/s/d Lcom/a/b/d/jl;
invokevirtual com/a/b/d/jl/size()I
anewarray java/lang/annotation/Annotation
invokevirtual com/a/b/d/jl/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
checkcast [Ljava/lang/annotation/Annotation;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final hashCode()I
aload 0
getfield com/a/b/m/s/b I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final isAnnotationPresent(Ljava/lang/Class;)Z
aload 0
aload 1
invokevirtual com/a/b/m/s/getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/m/s/c Lcom/a/b/m/ae;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 2
aload 0
getfield com/a/b/m/s/b I
istore 1
new java/lang/StringBuilder
dup
aload 2
invokevirtual java/lang/String/length()I
bipush 15
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " arg"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 3
.limit stack 4
.end method
