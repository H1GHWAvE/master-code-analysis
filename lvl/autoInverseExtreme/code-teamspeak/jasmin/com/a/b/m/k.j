.bytecode 50.0
.class public synchronized abstract com/a/b/m/k
.super com/a/b/m/g
.implements java/lang/reflect/GenericDeclaration
.annotation invisible Lcom/a/b/a/a;
.end annotation

.method <init>(Ljava/lang/reflect/AccessibleObject;)V
aload 0
aload 1
invokespecial com/a/b/m/g/<init>(Ljava/lang/reflect/AccessibleObject;)V
return
.limit locals 2
.limit stack 2
.end method

.method private a(Lcom/a/b/m/ae;)Lcom/a/b/m/k;
aload 1
aload 0
invokevirtual com/a/b/m/k/g()Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
invokevirtual com/a/b/m/ae/a(Lcom/a/b/m/ae;)Z
ifne L0
aload 0
invokevirtual com/a/b/m/k/g()Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 2
aload 1
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 2
invokevirtual java/lang/String/length()I
bipush 35
iadd
aload 1
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Invokable is known to return "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", not "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
areturn
.limit locals 3
.limit stack 6
.end method

.method private a(Ljava/lang/Class;)Lcom/a/b/m/k;
aload 1
invokestatic com/a/b/m/ae/a(Ljava/lang/Class;)Lcom/a/b/m/ae;
astore 2
aload 2
aload 0
invokevirtual com/a/b/m/k/g()Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
invokevirtual com/a/b/m/ae/a(Lcom/a/b/m/ae;)Z
ifne L0
aload 0
invokevirtual com/a/b/m/k/g()Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
aload 2
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 2
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 35
iadd
aload 2
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Invokable is known to return "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", not "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
areturn
.limit locals 3
.limit stack 6
.end method

.method private static a(Ljava/lang/reflect/Constructor;)Lcom/a/b/m/k;
new com/a/b/m/l
dup
aload 0
invokespecial com/a/b/m/l/<init>(Ljava/lang/reflect/Constructor;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Ljava/lang/reflect/Method;)Lcom/a/b/m/k;
new com/a/b/m/m
dup
aload 0
invokespecial com/a/b/m/m/<init>(Ljava/lang/reflect/Method;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private transient b(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast [Ljava/lang/Object;
invokevirtual com/a/b/m/k/a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 3
.limit stack 3
.end method

.method private h()Lcom/a/b/m/ae;
aload 0
invokevirtual com/a/b/m/k/g()Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
areturn
.limit locals 1
.limit stack 1
.end method

.method private i()Lcom/a/b/d/jl;
aload 0
invokevirtual com/a/b/m/k/d()[Ljava/lang/reflect/Type;
astore 2
aload 0
invokevirtual com/a/b/m/k/f()[[Ljava/lang/annotation/Annotation;
astore 3
invokestatic com/a/b/d/jl/h()Lcom/a/b/d/jn;
astore 4
iconst_0
istore 1
L0:
iload 1
aload 2
arraylength
if_icmpge L1
aload 4
new com/a/b/m/s
dup
aload 0
iload 1
aload 2
iload 1
aaload
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
aload 3
iload 1
aaload
invokespecial com/a/b/m/s/<init>(Lcom/a/b/m/k;ILcom/a/b/m/ae;[Ljava/lang/annotation/Annotation;)V
invokevirtual com/a/b/d/jn/c(Ljava/lang/Object;)Lcom/a/b/d/jn;
pop
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 4
invokevirtual com/a/b/d/jn/b()Lcom/a/b/d/jl;
areturn
.limit locals 5
.limit stack 8
.end method

.method private j()Lcom/a/b/d/jl;
invokestatic com/a/b/d/jl/h()Lcom/a/b/d/jn;
astore 3
aload 0
invokevirtual com/a/b/m/k/e()[Ljava/lang/reflect/Type;
astore 4
aload 4
arraylength
istore 2
iconst_0
istore 1
L0:
iload 1
iload 2
if_icmpge L1
aload 3
aload 4
iload 1
aaload
invokestatic com/a/b/m/ae/a(Ljava/lang/reflect/Type;)Lcom/a/b/m/ae;
invokevirtual com/a/b/d/jn/c(Ljava/lang/Object;)Lcom/a/b/d/jn;
pop
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 3
invokevirtual com/a/b/d/jn/b()Lcom/a/b/d/jl;
areturn
.limit locals 5
.limit stack 3
.end method

.method public a()Lcom/a/b/m/ae;
aload 0
invokevirtual com/a/b/m/k/getDeclaringClass()Ljava/lang/Class;
invokestatic com/a/b/m/ae/a(Ljava/lang/Class;)Lcom/a/b/m/ae;
areturn
.limit locals 1
.limit stack 1
.end method

.method abstract a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.end method

.method public abstract b()Z
.end method

.method public abstract c()Z
.end method

.method abstract d()[Ljava/lang/reflect/Type;
.end method

.method abstract e()[Ljava/lang/reflect/Type;
.end method

.method public volatile synthetic equals(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/m/g/equals(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method abstract f()[[Ljava/lang/annotation/Annotation;
.end method

.method abstract g()Ljava/lang/reflect/Type;
.end method

.method public final getDeclaringClass()Ljava/lang/Class;
aload 0
invokespecial com/a/b/m/g/getDeclaringClass()Ljava/lang/Class;
areturn
.limit locals 1
.limit stack 1
.end method

.method public volatile synthetic hashCode()I
aload 0
invokespecial com/a/b/m/g/hashCode()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public volatile synthetic toString()Ljava/lang/String;
aload 0
invokespecial com/a/b/m/g/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
