.bytecode 50.0
.class final synchronized com/a/b/m/x
.super com/a/b/m/ax

.field final synthetic 'a' Ljava/util/Map;

.field final synthetic 'b' Ljava/lang/reflect/Type;

.method <init>(Ljava/util/Map;Ljava/lang/reflect/Type;)V
aload 0
aload 1
putfield com/a/b/m/x/a Ljava/util/Map;
aload 0
aload 2
putfield com/a/b/m/x/b Ljava/lang/reflect/Type;
aload 0
invokespecial com/a/b/m/ax/<init>()V
return
.limit locals 3
.limit stack 2
.end method

.method final a(Ljava/lang/Class;)V
aload 1
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 21
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "No type mapping from "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 2
.limit stack 6
.end method

.method final a(Ljava/lang/reflect/GenericArrayType;)V
aload 0
getfield com/a/b/m/x/b Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ay/c(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
astore 3
aload 3
ifnull L0
iconst_1
istore 2
L1:
iload 2
ldc "%s is not an array type."
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/m/x/b Ljava/lang/reflect/Type;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
getfield com/a/b/m/x/a Ljava/util/Map;
aload 1
invokeinterface java/lang/reflect/GenericArrayType/getGenericComponentType()Ljava/lang/reflect/Type; 0
aload 3
invokestatic com/a/b/m/w/a(Ljava/util/Map;Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)V
return
L0:
iconst_0
istore 2
goto L1
.limit locals 4
.limit stack 6
.end method

.method final a(Ljava/lang/reflect/ParameterizedType;)V
iconst_0
istore 2
ldc java/lang/reflect/ParameterizedType
aload 0
getfield com/a/b/m/x/b Ljava/lang/reflect/Type;
invokestatic com/a/b/m/w/a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/reflect/ParameterizedType
astore 4
aload 1
invokeinterface java/lang/reflect/ParameterizedType/getRawType()Ljava/lang/reflect/Type; 0
aload 4
invokeinterface java/lang/reflect/ParameterizedType/getRawType()Ljava/lang/reflect/Type; 0
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ldc "Inconsistent raw type: %s vs. %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
dup
iconst_1
aload 0
getfield com/a/b/m/x/b Ljava/lang/reflect/Type;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 1
invokeinterface java/lang/reflect/ParameterizedType/getActualTypeArguments()[Ljava/lang/reflect/Type; 0
astore 5
aload 4
invokeinterface java/lang/reflect/ParameterizedType/getActualTypeArguments()[Ljava/lang/reflect/Type; 0
astore 6
aload 5
arraylength
aload 6
arraylength
if_icmpne L0
iconst_1
istore 3
L1:
iload 3
ldc "%s not compatible with %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
dup
iconst_1
aload 4
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
L2:
iload 2
aload 5
arraylength
if_icmpge L3
aload 0
getfield com/a/b/m/x/a Ljava/util/Map;
aload 5
iload 2
aaload
aload 6
iload 2
aaload
invokestatic com/a/b/m/w/a(Ljava/util/Map;Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)V
iload 2
iconst_1
iadd
istore 2
goto L2
L0:
iconst_0
istore 3
goto L1
L3:
return
.limit locals 7
.limit stack 6
.end method

.method final a(Ljava/lang/reflect/TypeVariable;)V
aload 0
getfield com/a/b/m/x/a Ljava/util/Map;
new com/a/b/m/ab
dup
aload 1
invokespecial com/a/b/m/ab/<init>(Ljava/lang/reflect/TypeVariable;)V
aload 0
getfield com/a/b/m/x/b Ljava/lang/reflect/Type;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
return
.limit locals 2
.limit stack 4
.end method

.method final a(Ljava/lang/reflect/WildcardType;)V
iconst_0
istore 4
ldc java/lang/reflect/WildcardType
aload 0
getfield com/a/b/m/x/b Ljava/lang/reflect/Type;
invokestatic com/a/b/m/w/a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/reflect/WildcardType
astore 9
aload 1
invokeinterface java/lang/reflect/WildcardType/getUpperBounds()[Ljava/lang/reflect/Type; 0
astore 6
aload 9
invokeinterface java/lang/reflect/WildcardType/getUpperBounds()[Ljava/lang/reflect/Type; 0
astore 7
aload 1
invokeinterface java/lang/reflect/WildcardType/getLowerBounds()[Ljava/lang/reflect/Type; 0
astore 8
aload 9
invokeinterface java/lang/reflect/WildcardType/getLowerBounds()[Ljava/lang/reflect/Type; 0
astore 9
aload 6
arraylength
aload 7
arraylength
if_icmpne L0
aload 8
arraylength
aload 9
arraylength
if_icmpne L0
iconst_1
istore 5
L1:
iload 5
ldc "Incompatible type: %s vs. %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
dup
iconst_1
aload 0
getfield com/a/b/m/x/b Ljava/lang/reflect/Type;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
iconst_0
istore 2
L2:
iload 4
istore 3
iload 2
aload 6
arraylength
if_icmpge L3
aload 0
getfield com/a/b/m/x/a Ljava/util/Map;
aload 6
iload 2
aaload
aload 7
iload 2
aaload
invokestatic com/a/b/m/w/a(Ljava/util/Map;Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)V
iload 2
iconst_1
iadd
istore 2
goto L2
L0:
iconst_0
istore 5
goto L1
L3:
iload 3
aload 8
arraylength
if_icmpge L4
aload 0
getfield com/a/b/m/x/a Ljava/util/Map;
aload 8
iload 3
aaload
aload 9
iload 3
aaload
invokestatic com/a/b/m/w/a(Ljava/util/Map;Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)V
iload 3
iconst_1
iadd
istore 3
goto L3
L4:
return
.limit locals 10
.limit stack 6
.end method
