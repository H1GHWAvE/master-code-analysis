.bytecode 50.0
.class synchronized com/a/b/i/ai
.super com/a/b/i/ah

.field private static final 'a' Lcom/a/b/b/di;

.field private final 'b' Ljava/lang/CharSequence;

.method static <clinit>()V
ldc "\r\n|\n|\r"
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
invokestatic com/a/b/b/di/a(Ljava/util/regex/Pattern;)Lcom/a/b/b/di;
putstatic com/a/b/i/ai/a Lcom/a/b/b/di;
return
.limit locals 0
.limit stack 1
.end method

.method protected <init>(Ljava/lang/CharSequence;)V
aload 0
invokespecial com/a/b/i/ah/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/CharSequence
putfield com/a/b/i/ai/b Ljava/lang/CharSequence;
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Lcom/a/b/i/ai;)Ljava/lang/CharSequence;
aload 0
getfield com/a/b/i/ai/b Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic f()Lcom/a/b/b/di;
getstatic com/a/b/i/ai/a Lcom/a/b/b/di;
areturn
.limit locals 0
.limit stack 1
.end method

.method private g()Ljava/lang/Iterable;
new com/a/b/i/aj
dup
aload 0
invokespecial com/a/b/i/aj/<init>(Lcom/a/b/i/ai;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public final a()Ljava/io/Reader;
new com/a/b/i/af
dup
aload 0
getfield com/a/b/i/ai/b Ljava/lang/CharSequence;
invokespecial com/a/b/i/af/<init>(Ljava/lang/CharSequence;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public final a(Lcom/a/b/i/by;)Ljava/lang/Object;
aload 0
invokespecial com/a/b/i/ai/g()Ljava/lang/Iterable;
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
invokeinterface com/a/b/i/by/a(Ljava/lang/String;)Z 1
pop
goto L0
L1:
aload 1
invokeinterface com/a/b/i/by/a()Ljava/lang/Object; 0
areturn
.limit locals 3
.limit stack 2
.end method

.method public final b()Ljava/lang/String;
aload 0
getfield com/a/b/i/ai/b Ljava/lang/CharSequence;
invokeinterface java/lang/CharSequence/toString()Ljava/lang/String; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public final c()Ljava/lang/String;
aload 0
invokespecial com/a/b/i/ai/g()Ljava/lang/Iterable;
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 1
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 1
.end method

.method public final d()Lcom/a/b/d/jl;
aload 0
invokespecial com/a/b/i/ai/g()Ljava/lang/Iterable;
invokestatic com/a/b/d/jl/a(Ljava/lang/Iterable;)Lcom/a/b/d/jl;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final e()Z
aload 0
getfield com/a/b/i/ai/b Ljava/lang/CharSequence;
invokeinterface java/lang/CharSequence/length()I 0
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public toString()Ljava/lang/String;
aload 0
getfield com/a/b/i/ai/b Ljava/lang/CharSequence;
ldc "..."
invokestatic com/a/b/b/e/a(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 17
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "CharSource.wrap("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 4
.end method
