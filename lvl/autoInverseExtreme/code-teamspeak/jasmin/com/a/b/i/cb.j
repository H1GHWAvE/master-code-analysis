.bytecode 50.0
.class public final synchronized com/a/b/i/cb
.super java/io/FilterInputStream
.implements java/io/DataInput
.annotation invisible Lcom/a/b/a/a;
.end annotation

.method private <init>(Ljava/io/InputStream;)V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/io/InputStream
invokespecial java/io/FilterInputStream/<init>(Ljava/io/InputStream;)V
return
.limit locals 2
.limit stack 2
.end method

.method private a()B
aload 0
getfield com/a/b/i/cb/in Ljava/io/InputStream;
invokevirtual java/io/InputStream/read()I
istore 1
iconst_m1
iload 1
if_icmpne L0
new java/io/EOFException
dup
invokespecial java/io/EOFException/<init>()V
athrow
L0:
iload 1
i2b
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final readBoolean()Z
aload 0
invokevirtual com/a/b/i/cb/readUnsignedByte()I
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final readByte()B
aload 0
invokevirtual com/a/b/i/cb/readUnsignedByte()I
i2b
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final readChar()C
aload 0
invokevirtual com/a/b/i/cb/readUnsignedShort()I
i2c
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final readDouble()D
aload 0
invokevirtual com/a/b/i/cb/readLong()J
invokestatic java/lang/Double/longBitsToDouble(J)D
dreturn
.limit locals 1
.limit stack 2
.end method

.method public final readFloat()F
aload 0
invokevirtual com/a/b/i/cb/readInt()I
invokestatic java/lang/Float/intBitsToFloat(I)F
freturn
.limit locals 1
.limit stack 1
.end method

.method public final readFully([B)V
aload 0
aload 1
invokestatic com/a/b/i/z/a(Ljava/io/InputStream;[B)V
return
.limit locals 2
.limit stack 2
.end method

.method public final readFully([BII)V
aload 0
aload 1
iload 2
iload 3
invokestatic com/a/b/i/z/a(Ljava/io/InputStream;[BII)V
return
.limit locals 4
.limit stack 4
.end method

.method public final readInt()I
aload 0
invokespecial com/a/b/i/cb/a()B
istore 1
aload 0
invokespecial com/a/b/i/cb/a()B
istore 2
aload 0
invokespecial com/a/b/i/cb/a()B
istore 3
aload 0
invokespecial com/a/b/i/cb/a()B
iload 3
iload 2
iload 1
invokestatic com/a/b/l/q/a(BBBB)I
ireturn
.limit locals 4
.limit stack 4
.end method

.method public final readLine()Ljava/lang/String;
new java/lang/UnsupportedOperationException
dup
ldc "readLine is not supported"
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method public final readLong()J
aload 0
invokespecial com/a/b/i/cb/a()B
istore 1
aload 0
invokespecial com/a/b/i/cb/a()B
istore 2
aload 0
invokespecial com/a/b/i/cb/a()B
istore 3
aload 0
invokespecial com/a/b/i/cb/a()B
istore 4
aload 0
invokespecial com/a/b/i/cb/a()B
istore 5
aload 0
invokespecial com/a/b/i/cb/a()B
istore 6
aload 0
invokespecial com/a/b/i/cb/a()B
istore 7
aload 0
invokespecial com/a/b/i/cb/a()B
iload 7
iload 6
iload 5
iload 4
iload 3
iload 2
iload 1
invokestatic com/a/b/l/u/a(BBBBBBBB)J
lreturn
.limit locals 8
.limit stack 8
.end method

.method public final readShort()S
aload 0
invokevirtual com/a/b/i/cb/readUnsignedShort()I
i2s
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final readUTF()Ljava/lang/String;
new java/io/DataInputStream
dup
aload 0
getfield com/a/b/i/cb/in Ljava/io/InputStream;
invokespecial java/io/DataInputStream/<init>(Ljava/io/InputStream;)V
invokevirtual java/io/DataInputStream/readUTF()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method

.method public final readUnsignedByte()I
aload 0
getfield com/a/b/i/cb/in Ljava/io/InputStream;
invokevirtual java/io/InputStream/read()I
istore 1
iload 1
ifge L0
new java/io/EOFException
dup
invokespecial java/io/EOFException/<init>()V
athrow
L0:
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final readUnsignedShort()I
aload 0
invokespecial com/a/b/i/cb/a()B
istore 1
iconst_0
iconst_0
aload 0
invokespecial com/a/b/i/cb/a()B
iload 1
invokestatic com/a/b/l/q/a(BBBB)I
ireturn
.limit locals 2
.limit stack 4
.end method

.method public final skipBytes(I)I
aload 0
getfield com/a/b/i/cb/in Ljava/io/InputStream;
iload 1
i2l
invokevirtual java/io/InputStream/skip(J)J
l2i
ireturn
.limit locals 2
.limit stack 3
.end method
