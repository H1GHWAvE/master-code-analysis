.bytecode 50.0
.class final synchronized com/a/b/i/y
.super com/a/b/i/s

.field final synthetic 'a' Lcom/a/b/i/s;

.field private final 'b' J

.field private final 'c' J

.method private <init>(Lcom/a/b/i/s;JJ)V
aload 0
aload 1
putfield com/a/b/i/y/a Lcom/a/b/i/s;
aload 0
invokespecial com/a/b/i/s/<init>()V
lload 2
lconst_0
lcmp
iflt L0
iconst_1
istore 6
L1:
iload 6
ldc "offset (%s) may not be negative"
iconst_1
anewarray java/lang/Object
dup
iconst_0
lload 2
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
lload 4
lconst_0
lcmp
iflt L2
iconst_1
istore 6
L3:
iload 6
ldc "length (%s) may not be negative"
iconst_1
anewarray java/lang/Object
dup
iconst_0
lload 4
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
lload 2
putfield com/a/b/i/y/b J
aload 0
lload 4
putfield com/a/b/i/y/c J
return
L0:
iconst_0
istore 6
goto L1
L2:
iconst_0
istore 6
goto L3
.limit locals 7
.limit stack 7
.end method

.method synthetic <init>(Lcom/a/b/i/s;JJB)V
aload 0
aload 1
lload 2
lload 4
invokespecial com/a/b/i/y/<init>(Lcom/a/b/i/s;JJ)V
return
.limit locals 7
.limit stack 6
.end method

.method private a(Ljava/io/InputStream;)Ljava/io/InputStream;
.catch java/lang/Throwable from L0 to L1 using L2
.catch all from L3 to L4 using L4
aload 0
getfield com/a/b/i/y/b J
lconst_0
lcmp
ifle L1
L0:
aload 1
aload 0
getfield com/a/b/i/y/b J
invokestatic com/a/b/i/z/b(Ljava/io/InputStream;J)V
L1:
aload 1
aload 0
getfield com/a/b/i/y/c J
invokestatic com/a/b/i/z/a(Ljava/io/InputStream;J)Ljava/io/InputStream;
areturn
L2:
astore 3
invokestatic com/a/b/i/ar/a()Lcom/a/b/i/ar;
astore 2
aload 2
aload 1
invokevirtual com/a/b/i/ar/a(Ljava/io/Closeable;)Ljava/io/Closeable;
pop
L3:
aload 2
aload 3
invokevirtual com/a/b/i/ar/a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
athrow
L4:
astore 1
aload 2
invokevirtual com/a/b/i/ar/close()V
aload 1
athrow
.limit locals 4
.limit stack 4
.end method

.method public final a(JJ)Lcom/a/b/i/s;
lload 1
lconst_0
lcmp
iflt L0
iconst_1
istore 7
L1:
iload 7
ldc "offset (%s) may not be negative"
iconst_1
anewarray java/lang/Object
dup
iconst_0
lload 1
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
lload 3
lconst_0
lcmp
iflt L2
iconst_1
istore 7
L3:
iload 7
ldc "length (%s) may not be negative"
iconst_1
anewarray java/lang/Object
dup
iconst_0
lload 3
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
getfield com/a/b/i/y/c J
lstore 5
aload 0
getfield com/a/b/i/y/a Lcom/a/b/i/s;
aload 0
getfield com/a/b/i/y/b J
lload 1
ladd
lload 3
lload 5
lload 1
lsub
invokestatic java/lang/Math/min(JJ)J
invokevirtual com/a/b/i/s/a(JJ)Lcom/a/b/i/s;
areturn
L0:
iconst_0
istore 7
goto L1
L2:
iconst_0
istore 7
goto L3
.limit locals 8
.limit stack 9
.end method

.method public final a()Ljava/io/InputStream;
aload 0
aload 0
getfield com/a/b/i/y/a Lcom/a/b/i/s;
invokevirtual com/a/b/i/s/a()Ljava/io/InputStream;
invokespecial com/a/b/i/y/a(Ljava/io/InputStream;)Ljava/io/InputStream;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final b()Ljava/io/InputStream;
aload 0
aload 0
getfield com/a/b/i/y/a Lcom/a/b/i/s;
invokevirtual com/a/b/i/s/b()Ljava/io/InputStream;
invokespecial com/a/b/i/y/a(Ljava/io/InputStream;)Ljava/io/InputStream;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final c()Z
aload 0
getfield com/a/b/i/y/c J
lconst_0
lcmp
ifeq L0
aload 0
invokespecial com/a/b/i/s/c()Z
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 4
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/i/y/a Lcom/a/b/i/s;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 5
aload 0
getfield com/a/b/i/y/b J
lstore 1
aload 0
getfield com/a/b/i/y/c J
lstore 3
new java/lang/StringBuilder
dup
aload 5
invokevirtual java/lang/String/length()I
bipush 50
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".slice("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 1
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 3
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 6
.limit stack 4
.end method
