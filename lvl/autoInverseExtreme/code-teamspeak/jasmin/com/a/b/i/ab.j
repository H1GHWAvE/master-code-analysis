.bytecode 50.0
.class final synchronized com/a/b/i/ab
.super java/lang/Object
.implements com/a/b/i/m

.field final 'a' Ljava/io/DataInput;

.method <init>(Ljava/io/ByteArrayInputStream;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/io/DataInputStream
dup
aload 1
invokespecial java/io/DataInputStream/<init>(Ljava/io/InputStream;)V
putfield com/a/b/i/ab/a Ljava/io/DataInput;
return
.limit locals 2
.limit stack 4
.end method

.method public final readBoolean()Z
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/i/ab/a Ljava/io/DataInput;
invokeinterface java/io/DataInput/readBoolean()Z 0
istore 1
L1:
iload 1
ireturn
L2:
astore 2
new java/lang/IllegalStateException
dup
aload 2
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method public final readByte()B
.catch java/io/EOFException from L0 to L1 using L2
.catch java/io/IOException from L0 to L1 using L3
L0:
aload 0
getfield com/a/b/i/ab/a Ljava/io/DataInput;
invokeinterface java/io/DataInput/readByte()B 0
istore 1
L1:
iload 1
ireturn
L2:
astore 2
new java/lang/IllegalStateException
dup
aload 2
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/Throwable;)V
athrow
L3:
astore 2
new java/lang/AssertionError
dup
aload 2
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method public final readChar()C
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/i/ab/a Ljava/io/DataInput;
invokeinterface java/io/DataInput/readChar()C 0
istore 1
L1:
iload 1
ireturn
L2:
astore 2
new java/lang/IllegalStateException
dup
aload 2
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method public final readDouble()D
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/i/ab/a Ljava/io/DataInput;
invokeinterface java/io/DataInput/readDouble()D 0
dstore 1
L1:
dload 1
dreturn
L2:
astore 3
new java/lang/IllegalStateException
dup
aload 3
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 4
.limit stack 3
.end method

.method public final readFloat()F
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/i/ab/a Ljava/io/DataInput;
invokeinterface java/io/DataInput/readFloat()F 0
fstore 1
L1:
fload 1
freturn
L2:
astore 2
new java/lang/IllegalStateException
dup
aload 2
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method public final readFully([B)V
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/i/ab/a Ljava/io/DataInput;
aload 1
invokeinterface java/io/DataInput/readFully([B)V 1
L1:
return
L2:
astore 1
new java/lang/IllegalStateException
dup
aload 1
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 2
.limit stack 3
.end method

.method public final readFully([BII)V
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/i/ab/a Ljava/io/DataInput;
aload 1
iload 2
iload 3
invokeinterface java/io/DataInput/readFully([BII)V 3
L1:
return
L2:
astore 1
new java/lang/IllegalStateException
dup
aload 1
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 4
.limit stack 4
.end method

.method public final readInt()I
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/i/ab/a Ljava/io/DataInput;
invokeinterface java/io/DataInput/readInt()I 0
istore 1
L1:
iload 1
ireturn
L2:
astore 2
new java/lang/IllegalStateException
dup
aload 2
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method public final readLine()Ljava/lang/String;
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/i/ab/a Ljava/io/DataInput;
invokeinterface java/io/DataInput/readLine()Ljava/lang/String; 0
astore 1
L1:
aload 1
areturn
L2:
astore 1
new java/lang/IllegalStateException
dup
aload 1
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 2
.limit stack 3
.end method

.method public final readLong()J
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/i/ab/a Ljava/io/DataInput;
invokeinterface java/io/DataInput/readLong()J 0
lstore 1
L1:
lload 1
lreturn
L2:
astore 3
new java/lang/IllegalStateException
dup
aload 3
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 4
.limit stack 3
.end method

.method public final readShort()S
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/i/ab/a Ljava/io/DataInput;
invokeinterface java/io/DataInput/readShort()S 0
istore 1
L1:
iload 1
ireturn
L2:
astore 2
new java/lang/IllegalStateException
dup
aload 2
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method public final readUTF()Ljava/lang/String;
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/i/ab/a Ljava/io/DataInput;
invokeinterface java/io/DataInput/readUTF()Ljava/lang/String; 0
astore 1
L1:
aload 1
areturn
L2:
astore 1
new java/lang/IllegalStateException
dup
aload 1
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 2
.limit stack 3
.end method

.method public final readUnsignedByte()I
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/i/ab/a Ljava/io/DataInput;
invokeinterface java/io/DataInput/readUnsignedByte()I 0
istore 1
L1:
iload 1
ireturn
L2:
astore 2
new java/lang/IllegalStateException
dup
aload 2
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method public final readUnsignedShort()I
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/i/ab/a Ljava/io/DataInput;
invokeinterface java/io/DataInput/readUnsignedShort()I 0
istore 1
L1:
iload 1
ireturn
L2:
astore 2
new java/lang/IllegalStateException
dup
aload 2
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method public final skipBytes(I)I
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/i/ab/a Ljava/io/DataInput;
iload 1
invokeinterface java/io/DataInput/skipBytes(I)I 1
istore 1
L1:
iload 1
ireturn
L2:
astore 2
new java/lang/IllegalStateException
dup
aload 2
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 3
.limit stack 3
.end method
