.bytecode 50.0
.class final synchronized com/a/b/i/bf
.super com/a/b/i/p

.field private final 'a' Ljava/io/File;

.field private final 'b' Lcom/a/b/d/lo;

.method private transient <init>(Ljava/io/File;[Lcom/a/b/i/bb;)V
aload 0
invokespecial com/a/b/i/p/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/io/File
putfield com/a/b/i/bf/a Ljava/io/File;
aload 0
aload 2
invokestatic com/a/b/d/lo/a([Ljava/lang/Object;)Lcom/a/b/d/lo;
putfield com/a/b/i/bf/b Lcom/a/b/d/lo;
return
.limit locals 3
.limit stack 2
.end method

.method synthetic <init>(Ljava/io/File;[Lcom/a/b/i/bb;B)V
aload 0
aload 1
aload 2
invokespecial com/a/b/i/bf/<init>(Ljava/io/File;[Lcom/a/b/i/bb;)V
return
.limit locals 4
.limit stack 3
.end method

.method private b()Ljava/io/FileOutputStream;
new java/io/FileOutputStream
dup
aload 0
getfield com/a/b/i/bf/a Ljava/io/File;
aload 0
getfield com/a/b/i/bf/b Lcom/a/b/d/lo;
getstatic com/a/b/i/bb/a Lcom/a/b/i/bb;
invokevirtual com/a/b/d/lo/contains(Ljava/lang/Object;)Z
invokespecial java/io/FileOutputStream/<init>(Ljava/io/File;Z)V
areturn
.limit locals 1
.limit stack 5
.end method

.method public final synthetic a()Ljava/io/OutputStream;
new java/io/FileOutputStream
dup
aload 0
getfield com/a/b/i/bf/a Ljava/io/File;
aload 0
getfield com/a/b/i/bf/b Lcom/a/b/d/lo;
getstatic com/a/b/i/bb/a Lcom/a/b/i/bb;
invokevirtual com/a/b/d/lo/contains(Ljava/lang/Object;)Z
invokespecial java/io/FileOutputStream/<init>(Ljava/io/File;Z)V
areturn
.limit locals 1
.limit stack 5
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/i/bf/a Ljava/io/File;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
aload 0
getfield com/a/b/i/bf/b Lcom/a/b/d/lo;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 2
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 20
iadd
aload 2
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Files.asByteSink("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 3
.limit stack 4
.end method
