.bytecode 50.0
.class public synchronized abstract com/a/b/i/b
.super java/lang/Object
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation invisible Lcom/a/b/a/b;
b Z = 1
.end annotation

.field private static final 'a' Lcom/a/b/i/b;

.field private static final 'b' Lcom/a/b/i/b;

.field private static final 'c' Lcom/a/b/i/b;

.field private static final 'd' Lcom/a/b/i/b;

.field private static final 'e' Lcom/a/b/i/b;

.method static <clinit>()V
new com/a/b/i/j
dup
ldc "base64()"
ldc "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
bipush 61
invokestatic java/lang/Character/valueOf(C)Ljava/lang/Character;
invokespecial com/a/b/i/j/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Character;)V
putstatic com/a/b/i/b/a Lcom/a/b/i/b;
new com/a/b/i/j
dup
ldc "base64Url()"
ldc "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_"
bipush 61
invokestatic java/lang/Character/valueOf(C)Ljava/lang/Character;
invokespecial com/a/b/i/j/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Character;)V
putstatic com/a/b/i/b/b Lcom/a/b/i/b;
new com/a/b/i/j
dup
ldc "base32()"
ldc "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567"
bipush 61
invokestatic java/lang/Character/valueOf(C)Ljava/lang/Character;
invokespecial com/a/b/i/j/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Character;)V
putstatic com/a/b/i/b/c Lcom/a/b/i/b;
new com/a/b/i/j
dup
ldc "base32Hex()"
ldc "0123456789ABCDEFGHIJKLMNOPQRSTUV"
bipush 61
invokestatic java/lang/Character/valueOf(C)Ljava/lang/Character;
invokespecial com/a/b/i/j/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Character;)V
putstatic com/a/b/i/b/d Lcom/a/b/i/b;
new com/a/b/i/j
dup
ldc "base16()"
ldc "0123456789ABCDEF"
aconst_null
invokespecial com/a/b/i/j/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Character;)V
putstatic com/a/b/i/b/e Lcom/a/b/i/b;
return
.limit locals 0
.limit stack 5
.end method

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Lcom/a/b/i/bu;Lcom/a/b/b/m;)Lcom/a/b/i/bu;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/i/e
dup
aload 0
aload 1
invokespecial com/a/b/i/e/<init>(Lcom/a/b/i/bu;Lcom/a/b/b/m;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static a(Lcom/a/b/i/bv;Ljava/lang/String;I)Lcom/a/b/i/bv;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
iload 2
ifle L0
iconst_1
istore 3
L1:
iload 3
invokestatic com/a/b/b/cn/a(Z)V
new com/a/b/i/f
dup
iload 2
aload 1
aload 0
invokespecial com/a/b/i/f/<init>(ILjava/lang/String;Lcom/a/b/i/bv;)V
areturn
L0:
iconst_0
istore 3
goto L1
.limit locals 4
.limit stack 5
.end method

.method private a(Lcom/a/b/i/ag;)Lcom/a/b/i/p;
.annotation invisible Lcom/a/b/a/c;
a s = "ByteSink,CharSink"
.end annotation
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/i/c
dup
aload 0
aload 1
invokespecial com/a/b/i/c/<init>(Lcom/a/b/i/b;Lcom/a/b/i/ag;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private a(Lcom/a/b/i/ah;)Lcom/a/b/i/s;
.annotation invisible Lcom/a/b/a/c;
a s = "ByteSource,CharSource"
.end annotation
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/i/d
dup
aload 0
aload 1
invokespecial com/a/b/i/d/<init>(Lcom/a/b/i/b;Lcom/a/b/i/ah;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private a(Ljava/io/Reader;)Ljava/io/InputStream;
.annotation invisible Lcom/a/b/a/c;
a s = "Reader,InputStream"
.end annotation
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
new com/a/b/i/bm
dup
aload 1
invokespecial com/a/b/i/bm/<init>(Ljava/io/Reader;)V
invokevirtual com/a/b/i/b/a(Lcom/a/b/i/bu;)Lcom/a/b/i/bs;
astore 1
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/i/bo
dup
aload 1
invokespecial com/a/b/i/bo/<init>(Lcom/a/b/i/bs;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private a(Ljava/io/Writer;)Ljava/io/OutputStream;
.annotation invisible Lcom/a/b/a/c;
a s = "Writer,OutputStream"
.end annotation
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
new com/a/b/i/bq
dup
aload 1
invokespecial com/a/b/i/bq/<init>(Ljava/io/Writer;)V
invokevirtual com/a/b/i/b/a(Lcom/a/b/i/bv;)Lcom/a/b/i/bt;
astore 1
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/i/bp
dup
aload 1
invokespecial com/a/b/i/bp/<init>(Lcom/a/b/i/bt;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private a([B)Ljava/lang/String;
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast [B
aload 1
arraylength
invokevirtual com/a/b/i/b/a([BI)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 3
.end method

.method private a(Ljava/lang/CharSequence;)[B
.catch com/a/b/i/h from L0 to L1 using L2
L0:
aload 0
aload 1
invokespecial com/a/b/i/b/b(Ljava/lang/CharSequence;)[B
astore 1
L1:
aload 1
areturn
L2:
astore 1
new java/lang/IllegalArgumentException
dup
aload 1
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 2
.limit stack 3
.end method

.method private b(Ljava/lang/CharSequence;)[B
.catch com/a/b/i/h from L0 to L1 using L2
.catch java/io/IOException from L0 to L1 using L3
.catch com/a/b/i/h from L4 to L5 using L2
.catch java/io/IOException from L4 to L5 using L3
aload 0
invokevirtual com/a/b/i/b/a()Lcom/a/b/b/m;
aload 1
invokevirtual com/a/b/b/m/k(Ljava/lang/CharSequence;)Ljava/lang/String;
astore 4
aload 4
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
new com/a/b/i/bn
dup
aload 4
invokespecial com/a/b/i/bn/<init>(Ljava/lang/CharSequence;)V
invokevirtual com/a/b/i/b/a(Lcom/a/b/i/bu;)Lcom/a/b/i/bs;
astore 1
aload 0
aload 4
invokeinterface java/lang/CharSequence/length()I 0
invokevirtual com/a/b/i/b/b(I)I
newarray byte
astore 4
L0:
aload 1
invokeinterface com/a/b/i/bs/a()I 0
istore 3
L1:
iconst_0
istore 2
L6:
iload 3
iconst_m1
if_icmpeq L7
aload 4
iload 2
iload 3
i2b
bastore
L4:
aload 1
invokeinterface com/a/b/i/bs/a()I 0
istore 3
L5:
iload 2
iconst_1
iadd
istore 2
goto L6
L2:
astore 1
aload 1
athrow
L3:
astore 1
new java/lang/AssertionError
dup
aload 1
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
L7:
iload 2
aload 4
arraylength
if_icmpne L8
aload 4
areturn
L8:
iload 2
newarray byte
astore 1
aload 4
iconst_0
aload 1
iconst_0
iload 2
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 1
areturn
.limit locals 5
.limit stack 5
.end method

.method private static b([BI)[B
iload 1
aload 0
arraylength
if_icmpne L0
aload 0
areturn
L0:
iload 1
newarray byte
astore 2
aload 0
iconst_0
aload 2
iconst_0
iload 1
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 2
areturn
.limit locals 3
.limit stack 5
.end method

.method public static e()Lcom/a/b/i/b;
getstatic com/a/b/i/b/e Lcom/a/b/i/b;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static f()Lcom/a/b/i/b;
getstatic com/a/b/i/b/a Lcom/a/b/i/b;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static g()Lcom/a/b/i/b;
getstatic com/a/b/i/b/b Lcom/a/b/i/b;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static h()Lcom/a/b/i/b;
getstatic com/a/b/i/b/c Lcom/a/b/i/b;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static i()Lcom/a/b/i/b;
getstatic com/a/b/i/b/d Lcom/a/b/i/b;
areturn
.limit locals 0
.limit stack 1
.end method

.method abstract a(I)I
.end method

.method abstract a()Lcom/a/b/b/m;
.end method

.method public abstract a(C)Lcom/a/b/i/b;
.annotation visible Ljavax/annotation/CheckReturnValue;
.end annotation
.end method

.method public abstract a(Ljava/lang/String;I)Lcom/a/b/i/b;
.annotation visible Ljavax/annotation/CheckReturnValue;
.end annotation
.end method

.method abstract a(Lcom/a/b/i/bu;)Lcom/a/b/i/bs;
.end method

.method abstract a(Lcom/a/b/i/bv;)Lcom/a/b/i/bt;
.end method

.method public final a([BI)Ljava/lang/String;
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L3 to L4 using L2
iconst_0
istore 3
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
iconst_0
iload 2
iconst_0
iadd
aload 1
arraylength
invokestatic com/a/b/b/cn/a(III)V
new com/a/b/i/br
dup
new java/lang/StringBuilder
dup
aload 0
iload 2
invokevirtual com/a/b/i/b/a(I)I
invokespecial java/lang/StringBuilder/<init>(I)V
invokespecial com/a/b/i/br/<init>(Ljava/lang/StringBuilder;)V
astore 4
aload 0
aload 4
invokevirtual com/a/b/i/b/a(Lcom/a/b/i/bv;)Lcom/a/b/i/bt;
astore 5
L5:
iload 3
iload 2
if_icmpge L3
L0:
aload 5
aload 1
iload 3
iconst_0
iadd
baload
invokeinterface com/a/b/i/bt/a(B)V 1
L1:
iload 3
iconst_1
iadd
istore 3
goto L5
L3:
aload 5
invokeinterface com/a/b/i/bt/b()V 0
L4:
aload 4
invokevirtual java/lang/Object/toString()Ljava/lang/String;
areturn
L2:
astore 1
new java/lang/AssertionError
dup
ldc "impossible"
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 6
.limit stack 6
.end method

.method abstract b(I)I
.end method

.method public abstract b()Lcom/a/b/i/b;
.annotation visible Ljavax/annotation/CheckReturnValue;
.end annotation
.end method

.method public abstract c()Lcom/a/b/i/b;
.annotation visible Ljavax/annotation/CheckReturnValue;
.end annotation
.end method

.method public abstract d()Lcom/a/b/i/b;
.annotation visible Ljavax/annotation/CheckReturnValue;
.end annotation
.end method
