.bytecode 50.0
.class public final synchronized com/a/b/k/a
.super java/lang/Object
.implements java/io/Serializable
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation invisible Lcom/a/b/a/b;
.end annotation
.annotation invisible Ljavax/annotation/concurrent/Immutable;
.end annotation

.field private static final 'b' I = -1


.field private static final 'e' J = 0L


.field final 'a' Ljava/lang/String;

.field private final 'c' I

.field private final 'd' Z

.method private <init>(Ljava/lang/String;IZ)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/a/b/k/a/a Ljava/lang/String;
aload 0
iload 2
putfield com/a/b/k/a/c I
aload 0
iload 3
putfield com/a/b/k/a/d Z
return
.limit locals 4
.limit stack 2
.end method

.method private a(I)I
aload 0
invokevirtual com/a/b/k/a/a()Z
ifeq L0
aload 0
getfield com/a/b/k/a/c I
istore 1
L0:
iload 1
ireturn
.limit locals 2
.limit stack 1
.end method

.method public static a(Ljava/lang/String;)Lcom/a/b/k/a;
.catch java/lang/NumberFormatException from L0 to L1 using L2
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aconst_null
astore 5
aload 0
ldc "["
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L3
aload 0
iconst_0
invokevirtual java/lang/String/charAt(I)C
bipush 91
if_icmpne L4
iconst_1
istore 3
L5:
iload 3
ldc "Bracketed host-port string must start with a bracket: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
bipush 58
invokevirtual java/lang/String/indexOf(I)I
istore 1
aload 0
bipush 93
invokevirtual java/lang/String/lastIndexOf(I)I
istore 2
iload 1
iflt L6
iload 2
iload 1
if_icmple L6
iconst_1
istore 3
L7:
iload 3
ldc "Invalid bracketed host/port: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
iconst_1
iload 2
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
astore 6
iload 2
iconst_1
iadd
aload 0
invokevirtual java/lang/String/length()I
if_icmpne L8
iconst_2
anewarray java/lang/String
astore 5
aload 5
iconst_0
aload 6
aastore
aload 5
iconst_1
ldc ""
aastore
L9:
aload 5
iconst_0
aaload
astore 6
aload 5
iconst_1
aaload
astore 5
iconst_0
istore 3
L10:
aload 5
invokestatic com/a/b/b/dy/a(Ljava/lang/String;)Z
ifne L11
aload 5
ldc "+"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifne L12
iconst_1
istore 4
L13:
iload 4
ldc "Unparseable port number: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
L0:
aload 5
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
istore 1
L1:
iload 1
invokestatic com/a/b/k/a/c(I)Z
ldc "Port number out of range: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
L14:
new com/a/b/k/a
dup
aload 6
iload 1
iload 3
invokespecial com/a/b/k/a/<init>(Ljava/lang/String;IZ)V
areturn
L4:
iconst_0
istore 3
goto L5
L6:
iconst_0
istore 3
goto L7
L8:
aload 0
iload 2
iconst_1
iadd
invokevirtual java/lang/String/charAt(I)C
bipush 58
if_icmpne L15
iconst_1
istore 3
L16:
iload 3
ldc "Only a colon may follow a close bracket: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
iload 2
iconst_2
iadd
istore 1
L17:
iload 1
aload 0
invokevirtual java/lang/String/length()I
if_icmpge L18
aload 0
iload 1
invokevirtual java/lang/String/charAt(I)C
invokestatic java/lang/Character/isDigit(C)Z
ldc "Port must be numeric: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
iload 1
iconst_1
iadd
istore 1
goto L17
L15:
iconst_0
istore 3
goto L16
L18:
iconst_2
anewarray java/lang/String
astore 5
aload 5
iconst_0
aload 6
aastore
aload 5
iconst_1
aload 0
iload 2
iconst_2
iadd
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
aastore
goto L9
L3:
aload 0
bipush 58
invokevirtual java/lang/String/indexOf(I)I
istore 1
iload 1
iflt L19
aload 0
bipush 58
iload 1
iconst_1
iadd
invokevirtual java/lang/String/indexOf(II)I
iconst_m1
if_icmpne L19
aload 0
iconst_0
iload 1
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
astore 6
aload 0
iload 1
iconst_1
iadd
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
astore 5
iconst_0
istore 3
goto L10
L19:
iload 1
iflt L20
iconst_1
istore 3
L21:
aload 0
astore 6
goto L10
L20:
iconst_0
istore 3
goto L21
L12:
iconst_0
istore 4
goto L13
L2:
astore 5
aload 0
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 0
aload 0
invokevirtual java/lang/String/length()I
ifeq L22
ldc "Unparseable port number: "
aload 0
invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
astore 0
L23:
new java/lang/IllegalArgumentException
dup
aload 0
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L22:
new java/lang/String
dup
ldc "Unparseable port number: "
invokespecial java/lang/String/<init>(Ljava/lang/String;)V
astore 0
goto L23
L11:
iconst_m1
istore 1
goto L14
.limit locals 7
.limit stack 6
.end method

.method private static a(Ljava/lang/String;I)Lcom/a/b/k/a;
iload 1
invokestatic com/a/b/k/a/c(I)Z
ldc "Port out of range: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
invokestatic com/a/b/k/a/a(Ljava/lang/String;)Lcom/a/b/k/a;
astore 3
aload 3
invokevirtual com/a/b/k/a/a()Z
ifne L0
iconst_1
istore 2
L1:
iload 2
ldc "Host has a port: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
new com/a/b/k/a
dup
aload 3
getfield com/a/b/k/a/a Ljava/lang/String;
iload 1
aload 3
getfield com/a/b/k/a/d Z
invokespecial com/a/b/k/a/<init>(Ljava/lang/String;IZ)V
areturn
L0:
iconst_0
istore 2
goto L1
.limit locals 4
.limit stack 6
.end method

.method private b(I)Lcom/a/b/k/a;
iload 1
invokestatic com/a/b/k/a/c(I)Z
invokestatic com/a/b/b/cn/a(Z)V
aload 0
invokevirtual com/a/b/k/a/a()Z
ifne L0
aload 0
getfield com/a/b/k/a/c I
iload 1
if_icmpne L1
L0:
aload 0
areturn
L1:
new com/a/b/k/a
dup
aload 0
getfield com/a/b/k/a/a Ljava/lang/String;
iload 1
aload 0
getfield com/a/b/k/a/d Z
invokespecial com/a/b/k/a/<init>(Ljava/lang/String;IZ)V
areturn
.limit locals 2
.limit stack 5
.end method

.method private static b(Ljava/lang/String;)Lcom/a/b/k/a;
aload 0
invokestatic com/a/b/k/a/a(Ljava/lang/String;)Lcom/a/b/k/a;
astore 2
aload 2
invokevirtual com/a/b/k/a/a()Z
ifne L0
iconst_1
istore 1
L1:
iload 1
ldc "Host has a port: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 2
areturn
L0:
iconst_0
istore 1
goto L1
.limit locals 3
.limit stack 6
.end method

.method private b()Ljava/lang/String;
aload 0
getfield com/a/b/k/a/a Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c()I
aload 0
invokevirtual com/a/b/k/a/a()Z
invokestatic com/a/b/b/cn/b(Z)V
aload 0
getfield com/a/b/k/a/c I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static c(I)Z
iload 0
iflt L0
iload 0
ldc_w 65535
if_icmpgt L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static c(Ljava/lang/String;)[Ljava/lang/String;
aload 0
iconst_0
invokevirtual java/lang/String/charAt(I)C
bipush 91
if_icmpne L0
iconst_1
istore 3
L1:
iload 3
ldc "Bracketed host-port string must start with a bracket: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
bipush 58
invokevirtual java/lang/String/indexOf(I)I
istore 1
aload 0
bipush 93
invokevirtual java/lang/String/lastIndexOf(I)I
istore 2
iload 1
iflt L2
iload 2
iload 1
if_icmple L2
iconst_1
istore 3
L3:
iload 3
ldc "Invalid bracketed host/port: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
iconst_1
iload 2
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
astore 4
iload 2
iconst_1
iadd
aload 0
invokevirtual java/lang/String/length()I
if_icmpne L4
iconst_2
anewarray java/lang/String
dup
iconst_0
aload 4
aastore
dup
iconst_1
ldc ""
aastore
areturn
L0:
iconst_0
istore 3
goto L1
L2:
iconst_0
istore 3
goto L3
L4:
aload 0
iload 2
iconst_1
iadd
invokevirtual java/lang/String/charAt(I)C
bipush 58
if_icmpne L5
iconst_1
istore 3
L6:
iload 3
ldc "Only a colon may follow a close bracket: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
iload 2
iconst_2
iadd
istore 1
L7:
iload 1
aload 0
invokevirtual java/lang/String/length()I
if_icmpge L8
aload 0
iload 1
invokevirtual java/lang/String/charAt(I)C
invokestatic java/lang/Character/isDigit(C)Z
ldc "Port must be numeric: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
iload 1
iconst_1
iadd
istore 1
goto L7
L5:
iconst_0
istore 3
goto L6
L8:
iconst_2
anewarray java/lang/String
dup
iconst_0
aload 4
aastore
dup
iconst_1
aload 0
iload 2
iconst_2
iadd
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
aastore
areturn
.limit locals 5
.limit stack 6
.end method

.method private d()Lcom/a/b/k/a;
aload 0
getfield com/a/b/k/a/d Z
ifne L0
iconst_1
istore 1
L1:
iload 1
ldc "Possible bracketless IPv6 literal: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/k/a/a Ljava/lang/String;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
areturn
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 6
.end method

.method public final a()Z
aload 0
getfield com/a/b/k/a/c I
iflt L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
if_acmpne L0
L1:
iconst_1
ireturn
L0:
aload 1
instanceof com/a/b/k/a
ifeq L2
aload 1
checkcast com/a/b/k/a
astore 1
aload 0
getfield com/a/b/k/a/a Ljava/lang/String;
aload 1
getfield com/a/b/k/a/a Ljava/lang/String;
invokestatic com/a/b/b/ce/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L3
aload 0
getfield com/a/b/k/a/c I
aload 1
getfield com/a/b/k/a/c I
if_icmpne L3
aload 0
getfield com/a/b/k/a/d Z
aload 1
getfield com/a/b/k/a/d Z
if_icmpeq L1
L3:
iconst_0
ireturn
L2:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final hashCode()I
iconst_3
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/k/a/a Ljava/lang/String;
aastore
dup
iconst_1
aload 0
getfield com/a/b/k/a/c I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_2
aload 0
getfield com/a/b/k/a/d Z
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
aastore
invokestatic java/util/Arrays/hashCode([Ljava/lang/Object;)I
ireturn
.limit locals 1
.limit stack 4
.end method

.method public final toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
aload 0
getfield com/a/b/k/a/a Ljava/lang/String;
invokevirtual java/lang/String/length()I
bipush 8
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
astore 1
aload 0
getfield com/a/b/k/a/a Ljava/lang/String;
bipush 58
invokevirtual java/lang/String/indexOf(I)I
iflt L0
aload 1
bipush 91
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
aload 0
getfield com/a/b/k/a/a Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
bipush 93
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
L1:
aload 0
invokevirtual com/a/b/k/a/a()Z
ifeq L2
aload 1
bipush 58
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
aload 0
getfield com/a/b/k/a/c I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
pop
L2:
aload 1
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L0:
aload 1
aload 0
getfield com/a/b/k/a/a Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L1
.limit locals 2
.limit stack 4
.end method
