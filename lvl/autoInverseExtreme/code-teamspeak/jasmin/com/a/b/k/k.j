.bytecode 50.0
.class public final synchronized com/a/b/k/k
.super com/a/b/e/p
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation invisible Lcom/a/b/a/b;
.end annotation

.field private static final 'a' [C

.field private static final 'b' [C

.field private final 'c' Z

.field private final 'd' [Z

.method static <clinit>()V
iconst_1
newarray char
dup
iconst_0
bipush 43
castore
putstatic com/a/b/k/k/a [C
ldc "0123456789ABCDEF"
invokevirtual java/lang/String/toCharArray()[C
putstatic com/a/b/k/k/b [C
return
.limit locals 0
.limit stack 4
.end method

.method public <init>(Ljava/lang/String;Z)V
aload 0
invokespecial com/a/b/e/p/<init>()V
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
ldc ".*[0-9A-Za-z].*"
invokevirtual java/lang/String/matches(Ljava/lang/String;)Z
ifeq L0
new java/lang/IllegalArgumentException
dup
ldc "Alphanumeric characters are always 'safe' and should not be explicitly specified"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
ldc "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
astore 1
iload 2
ifeq L1
aload 1
ldc " "
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L1
new java/lang/IllegalArgumentException
dup
ldc "plusForSpace cannot be specified when space is a 'safe' character"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
iload 2
putfield com/a/b/k/k/c Z
aload 0
aload 1
invokestatic com/a/b/k/k/b(Ljava/lang/String;)[Z
putfield com/a/b/k/k/d [Z
return
.limit locals 3
.limit stack 3
.end method

.method private static b(Ljava/lang/String;)[Z
iconst_0
istore 3
aload 0
invokevirtual java/lang/String/toCharArray()[C
astore 0
aload 0
arraylength
istore 4
iconst_m1
istore 2
iconst_0
istore 1
L0:
iload 1
iload 4
if_icmpge L1
aload 0
iload 1
caload
iload 2
invokestatic java/lang/Math/max(II)I
istore 2
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
iload 2
iconst_1
iadd
newarray boolean
astore 5
aload 0
arraylength
istore 2
iload 3
istore 1
L2:
iload 1
iload 2
if_icmpge L3
aload 5
aload 0
iload 1
caload
iconst_1
bastore
iload 1
iconst_1
iadd
istore 1
goto L2
L3:
aload 5
areturn
.limit locals 6
.limit stack 3
.end method

.method protected final a(Ljava/lang/CharSequence;II)I
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
L0:
iload 2
iload 3
if_icmpge L1
aload 1
iload 2
invokeinterface java/lang/CharSequence/charAt(I)C 1
istore 4
iload 4
aload 0
getfield com/a/b/k/k/d [Z
arraylength
if_icmpge L1
aload 0
getfield com/a/b/k/k/d [Z
iload 4
baload
ifeq L1
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
iload 2
ireturn
.limit locals 5
.limit stack 2
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokevirtual java/lang/String/length()I
istore 3
iconst_0
istore 2
L0:
aload 1
astore 5
iload 2
iload 3
if_icmpge L1
aload 1
iload 2
invokevirtual java/lang/String/charAt(I)C
istore 4
iload 4
aload 0
getfield com/a/b/k/k/d [Z
arraylength
if_icmpge L2
aload 0
getfield com/a/b/k/k/d [Z
iload 4
baload
ifne L3
L2:
aload 0
aload 1
iload 2
invokevirtual com/a/b/k/k/a(Ljava/lang/String;I)Ljava/lang/String;
astore 5
L1:
aload 5
areturn
L3:
iload 2
iconst_1
iadd
istore 2
goto L0
.limit locals 6
.limit stack 3
.end method

.method protected final a(I)[C
iload 1
aload 0
getfield com/a/b/k/k/d [Z
arraylength
if_icmpge L0
aload 0
getfield com/a/b/k/k/d [Z
iload 1
baload
ifeq L0
aconst_null
areturn
L0:
iload 1
bipush 32
if_icmpne L1
aload 0
getfield com/a/b/k/k/c Z
ifeq L1
getstatic com/a/b/k/k/a [C
areturn
L1:
iload 1
bipush 127
if_icmpgt L2
getstatic com/a/b/k/k/b [C
iload 1
bipush 15
iand
caload
istore 2
iconst_3
newarray char
dup
iconst_0
bipush 37
castore
dup
iconst_1
getstatic com/a/b/k/k/b [C
iload 1
iconst_4
iushr
caload
castore
dup
iconst_2
iload 2
castore
areturn
L2:
iload 1
sipush 2047
if_icmpgt L3
getstatic com/a/b/k/k/b [C
iload 1
bipush 15
iand
caload
istore 2
iload 1
iconst_4
iushr
istore 1
getstatic com/a/b/k/k/b [C
iload 1
iconst_3
iand
bipush 8
ior
caload
istore 3
iload 1
iconst_2
iushr
istore 1
getstatic com/a/b/k/k/b [C
iload 1
bipush 15
iand
caload
istore 4
bipush 6
newarray char
dup
iconst_0
bipush 37
castore
dup
iconst_1
getstatic com/a/b/k/k/b [C
iload 1
iconst_4
iushr
bipush 12
ior
caload
castore
dup
iconst_2
iload 4
castore
dup
iconst_3
bipush 37
castore
dup
iconst_4
iload 3
castore
dup
iconst_5
iload 2
castore
areturn
L3:
iload 1
ldc_w 65535
if_icmpgt L4
getstatic com/a/b/k/k/b [C
iload 1
bipush 15
iand
caload
istore 2
iload 1
iconst_4
iushr
istore 1
getstatic com/a/b/k/k/b [C
iload 1
iconst_3
iand
bipush 8
ior
caload
istore 3
iload 1
iconst_2
iushr
istore 1
getstatic com/a/b/k/k/b [C
iload 1
bipush 15
iand
caload
istore 4
iload 1
iconst_4
iushr
istore 1
getstatic com/a/b/k/k/b [C
iload 1
iconst_3
iand
bipush 8
ior
caload
istore 5
bipush 9
newarray char
dup
iconst_0
bipush 37
castore
dup
iconst_1
bipush 69
castore
dup
iconst_2
getstatic com/a/b/k/k/b [C
iload 1
iconst_2
iushr
caload
castore
dup
iconst_3
bipush 37
castore
dup
iconst_4
iload 5
castore
dup
iconst_5
iload 4
castore
dup
bipush 6
bipush 37
castore
dup
bipush 7
iload 3
castore
dup
bipush 8
iload 2
castore
areturn
L4:
iload 1
ldc_w 1114111
if_icmpgt L5
getstatic com/a/b/k/k/b [C
iload 1
bipush 15
iand
caload
istore 2
iload 1
iconst_4
iushr
istore 1
getstatic com/a/b/k/k/b [C
iload 1
iconst_3
iand
bipush 8
ior
caload
istore 3
iload 1
iconst_2
iushr
istore 1
getstatic com/a/b/k/k/b [C
iload 1
bipush 15
iand
caload
istore 4
iload 1
iconst_4
iushr
istore 1
getstatic com/a/b/k/k/b [C
iload 1
iconst_3
iand
bipush 8
ior
caload
istore 5
iload 1
iconst_2
iushr
istore 1
getstatic com/a/b/k/k/b [C
iload 1
bipush 15
iand
caload
istore 6
iload 1
iconst_4
iushr
istore 1
getstatic com/a/b/k/k/b [C
iload 1
iconst_3
iand
bipush 8
ior
caload
istore 7
bipush 12
newarray char
dup
iconst_0
bipush 37
castore
dup
iconst_1
bipush 70
castore
dup
iconst_2
getstatic com/a/b/k/k/b [C
iload 1
iconst_2
iushr
bipush 7
iand
caload
castore
dup
iconst_3
bipush 37
castore
dup
iconst_4
iload 7
castore
dup
iconst_5
iload 6
castore
dup
bipush 6
bipush 37
castore
dup
bipush 7
iload 5
castore
dup
bipush 8
iload 4
castore
dup
bipush 9
bipush 37
castore
dup
bipush 10
iload 3
castore
dup
bipush 11
iload 2
castore
areturn
L5:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
bipush 43
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Invalid unicode character value "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 8
.limit stack 6
.end method
