.bytecode 50.0
.class public final synchronized com/a/b/k/d
.super java/lang/Object
.annotation invisible Lcom/a/b/a/a;
.end annotation

.field private static final 'a' I = 4


.field private static final 'b' I = 8


.field private static final 'c' Ljava/net/Inet4Address;

.field private static final 'd' Ljava/net/Inet4Address;

.method static <clinit>()V
ldc "127.0.0.1"
invokestatic com/a/b/k/d/a(Ljava/lang/String;)Ljava/net/InetAddress;
checkcast java/net/Inet4Address
putstatic com/a/b/k/d/c Ljava/net/Inet4Address;
ldc "0.0.0.0"
invokestatic com/a/b/k/d/a(Ljava/lang/String;)Ljava/net/InetAddress;
checkcast java/net/Inet4Address
putstatic com/a/b/k/d/d Ljava/net/Inet4Address;
return
.limit locals 0
.limit stack 1
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Ljava/net/InetAddress;)Ljava/lang/String;
aload 0
instanceof java/net/Inet6Address
ifeq L0
aload 0
invokestatic com/a/b/k/d/b(Ljava/net/InetAddress;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 0
new java/lang/StringBuilder
dup
aload 0
invokevirtual java/lang/String/length()I
iconst_2
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "["
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L0:
aload 0
invokestatic com/a/b/k/d/b(Ljava/net/InetAddress;)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 4
.end method

.method static synthetic a()Ljava/net/Inet4Address;
getstatic com/a/b/k/d/d Ljava/net/Inet4Address;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static a(I)Ljava/net/Inet4Address;
iload 0
invokestatic com/a/b/l/q/b(I)[B
invokestatic com/a/b/k/d/a([B)Ljava/net/Inet4Address;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a([B)Ljava/net/Inet4Address;
aload 0
arraylength
iconst_4
if_icmpne L0
iconst_1
istore 1
L1:
iload 1
ldc "Byte array has invalid length for an IPv4 address: %s != 4."
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
arraylength
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
invokestatic com/a/b/k/d/b([B)Ljava/net/InetAddress;
checkcast java/net/Inet4Address
areturn
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 6
.end method

.method public static a(Ljava/lang/String;)Ljava/net/InetAddress;
aload 0
invokestatic com/a/b/k/d/c(Ljava/lang/String;)[B
astore 1
aload 1
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "'%s' is not an IP string literal."
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
invokestatic com/a/b/k/d/b([B)Ljava/net/InetAddress;
areturn
.limit locals 2
.limit stack 7
.end method

.method private static a([I)V
iconst_0
istore 1
iconst_m1
istore 4
iconst_m1
istore 3
iconst_m1
istore 2
L0:
iload 1
bipush 9
if_icmpge L1
iload 1
bipush 8
if_icmpge L2
aload 0
iload 1
iaload
ifne L2
iload 3
istore 6
iload 4
istore 5
iload 2
istore 7
iload 4
ifge L3
iload 1
istore 5
iload 2
istore 7
iload 3
istore 6
L3:
iload 1
iconst_1
iadd
istore 1
iload 6
istore 3
iload 5
istore 4
iload 7
istore 2
goto L0
L2:
iload 3
istore 6
iload 4
istore 5
iload 2
istore 7
iload 4
iflt L3
iload 1
iload 4
isub
istore 5
iload 5
iload 3
if_icmple L4
iload 4
istore 3
iload 5
istore 2
L5:
iconst_m1
istore 5
iload 2
istore 6
iload 3
istore 7
goto L3
L1:
iload 3
iconst_2
if_icmplt L6
aload 0
iload 2
iload 2
iload 3
iadd
iconst_m1
invokestatic java/util/Arrays/fill([IIII)V
L6:
return
L4:
iload 2
istore 4
iload 3
istore 2
iload 4
istore 3
goto L5
.limit locals 8
.limit stack 4
.end method

.method private static a(Ljava/net/Inet6Address;)Z
aload 0
invokevirtual java/net/Inet6Address/isIPv4CompatibleAddress()Z
ifne L0
L1:
iconst_0
ireturn
L0:
aload 0
invokevirtual java/net/Inet6Address/getAddress()[B
astore 0
aload 0
bipush 12
baload
ifne L2
aload 0
bipush 13
baload
ifne L2
aload 0
bipush 14
baload
ifne L2
aload 0
bipush 15
baload
ifeq L1
aload 0
bipush 15
baload
iconst_1
if_icmpeq L1
L2:
iconst_1
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static b(Ljava/net/InetAddress;)Ljava/lang/String;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
instanceof java/net/Inet4Address
ifeq L0
aload 0
invokevirtual java/net/InetAddress/getHostAddress()Ljava/lang/String;
areturn
L0:
aload 0
instanceof java/net/Inet6Address
invokestatic com/a/b/b/cn/a(Z)V
aload 0
invokevirtual java/net/InetAddress/getAddress()[B
astore 8
bipush 8
newarray int
astore 0
iconst_0
istore 1
L1:
iload 1
bipush 8
if_icmpge L2
aload 0
iload 1
iconst_0
iconst_0
aload 8
iload 1
iconst_2
imul
baload
aload 8
iload 1
iconst_2
imul
iconst_1
iadd
baload
invokestatic com/a/b/l/q/a(BBBB)I
iastore
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
iconst_0
istore 1
iconst_m1
istore 4
iconst_m1
istore 3
iconst_m1
istore 2
L3:
iload 1
bipush 9
if_icmpge L4
iload 1
bipush 8
if_icmpge L5
aload 0
iload 1
iaload
ifne L5
iload 3
istore 6
iload 4
istore 5
iload 2
istore 7
iload 4
ifge L6
iload 1
istore 5
iload 2
istore 7
iload 3
istore 6
L6:
iload 1
iconst_1
iadd
istore 1
iload 6
istore 3
iload 5
istore 4
iload 7
istore 2
goto L3
L5:
iload 3
istore 6
iload 4
istore 5
iload 2
istore 7
iload 4
iflt L6
iload 1
iload 4
isub
istore 5
iload 5
iload 3
if_icmple L7
iload 4
istore 3
iload 5
istore 2
L8:
iconst_m1
istore 5
iload 2
istore 6
iload 3
istore 7
goto L6
L4:
iload 3
iconst_2
if_icmplt L9
aload 0
iload 2
iload 2
iload 3
iadd
iconst_m1
invokestatic java/util/Arrays/fill([IIII)V
L9:
new java/lang/StringBuilder
dup
bipush 39
invokespecial java/lang/StringBuilder/<init>(I)V
astore 8
iconst_0
istore 2
iconst_0
istore 3
L10:
iload 2
bipush 8
if_icmpge L11
aload 0
iload 2
iaload
iflt L12
iconst_1
istore 1
L13:
iload 1
ifeq L14
iload 3
ifeq L15
aload 8
bipush 58
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
L15:
aload 8
aload 0
iload 2
iaload
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L16:
iload 2
iconst_1
iadd
istore 2
iload 1
istore 3
goto L10
L12:
iconst_0
istore 1
goto L13
L14:
iload 2
ifeq L17
iload 3
ifeq L16
L17:
aload 8
ldc "::"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L16
L11:
aload 8
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L7:
iload 2
istore 4
iload 3
istore 2
iload 4
istore 3
goto L8
.limit locals 9
.limit stack 8
.end method

.method private static b([I)Ljava/lang/String;
new java/lang/StringBuilder
dup
bipush 39
invokespecial java/lang/StringBuilder/<init>(I)V
astore 4
iconst_0
istore 2
iconst_0
istore 3
L0:
iload 2
bipush 8
if_icmpge L1
aload 0
iload 2
iaload
iflt L2
iconst_1
istore 1
L3:
iload 1
ifeq L4
iload 3
ifeq L5
aload 4
bipush 58
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
L5:
aload 4
aload 0
iload 2
iaload
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L6:
iload 2
iconst_1
iadd
istore 2
iload 1
istore 3
goto L0
L2:
iconst_0
istore 1
goto L3
L4:
iload 2
ifeq L7
iload 3
ifeq L6
L7:
aload 4
ldc "::"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L6
L1:
aload 4
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 5
.limit stack 3
.end method

.method private static b(Ljava/net/Inet6Address;)Ljava/net/Inet4Address;
aload 0
invokestatic com/a/b/k/d/a(Ljava/net/Inet6Address;)Z
ldc "Address '%s' is not IPv4-compatible."
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
invokestatic com/a/b/k/d/b(Ljava/net/InetAddress;)Ljava/lang/String;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
invokevirtual java/net/Inet6Address/getAddress()[B
bipush 12
bipush 16
invokestatic java/util/Arrays/copyOfRange([BII)[B
invokestatic com/a/b/k/d/a([B)Ljava/net/Inet4Address;
areturn
.limit locals 1
.limit stack 6
.end method

.method private static b([B)Ljava/net/InetAddress;
.catch java/net/UnknownHostException from L0 to L1 using L2
L0:
aload 0
invokestatic java/net/InetAddress/getByAddress([B)Ljava/net/InetAddress;
astore 0
L1:
aload 0
areturn
L2:
astore 0
new java/lang/AssertionError
dup
aload 0
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method private static b(Ljava/lang/String;)Z
aload 0
invokestatic com/a/b/k/d/c(Ljava/lang/String;)[B
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static c(Ljava/net/InetAddress;)Ljava/net/Inet4Address;
iconst_0
istore 2
aload 0
instanceof java/net/Inet4Address
ifeq L0
aload 0
checkcast java/net/Inet4Address
areturn
L0:
aload 0
invokevirtual java/net/InetAddress/getAddress()[B
astore 7
iconst_0
istore 1
L1:
iload 1
bipush 15
if_icmpge L2
aload 7
iload 1
baload
ifeq L3
iconst_0
istore 1
L4:
iload 1
ifeq L5
aload 7
bipush 15
baload
iconst_1
if_icmpne L5
getstatic com/a/b/k/d/c Ljava/net/Inet4Address;
areturn
L3:
iload 1
iconst_1
iadd
istore 1
goto L1
L5:
iload 1
ifeq L6
aload 7
bipush 15
baload
ifne L6
getstatic com/a/b/k/d/d Ljava/net/Inet4Address;
areturn
L6:
aload 0
checkcast java/net/Inet6Address
astore 0
aload 0
invokestatic com/a/b/k/d/a(Ljava/net/Inet6Address;)Z
ifne L7
aload 0
invokestatic com/a/b/k/d/c(Ljava/net/Inet6Address;)Z
ifne L7
aload 0
invokestatic com/a/b/k/d/e(Ljava/net/Inet6Address;)Z
ifeq L8
L7:
iconst_1
istore 1
L9:
iload 1
ifeq L10
aload 0
invokestatic com/a/b/k/d/a(Ljava/net/Inet6Address;)Z
ifeq L11
aload 0
invokestatic com/a/b/k/d/a(Ljava/net/Inet6Address;)Z
ldc "Address '%s' is not IPv4-compatible."
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
invokestatic com/a/b/k/d/b(Ljava/net/InetAddress;)Ljava/lang/String;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
invokevirtual java/net/Inet6Address/getAddress()[B
bipush 12
bipush 16
invokestatic java/util/Arrays/copyOfRange([BII)[B
invokestatic com/a/b/k/d/a([B)Ljava/net/Inet4Address;
astore 0
L12:
aload 0
invokevirtual java/net/Inet4Address/hashCode()I
i2l
lstore 5
L13:
invokestatic com/a/b/g/am/a()Lcom/a/b/g/ak;
lload 5
invokeinterface com/a/b/g/ak/a(J)Lcom/a/b/g/ag; 2
invokevirtual com/a/b/g/ag/b()I
ldc_w -536870912
ior
istore 2
iload 2
istore 1
iload 2
iconst_m1
if_icmpne L14
bipush -2
istore 1
L14:
iload 1
invokestatic com/a/b/l/q/b(I)[B
invokestatic com/a/b/k/d/a([B)Ljava/net/Inet4Address;
areturn
L8:
iconst_0
istore 1
goto L9
L11:
aload 0
invokestatic com/a/b/k/d/c(Ljava/net/Inet6Address;)Z
ifeq L15
aload 0
invokestatic com/a/b/k/d/c(Ljava/net/Inet6Address;)Z
ldc "Address '%s' is not a 6to4 address."
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
invokestatic com/a/b/k/d/b(Ljava/net/InetAddress;)Ljava/lang/String;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
invokevirtual java/net/Inet6Address/getAddress()[B
iconst_2
bipush 6
invokestatic java/util/Arrays/copyOfRange([BII)[B
invokestatic com/a/b/k/d/a([B)Ljava/net/Inet4Address;
astore 0
goto L12
L15:
aload 0
invokestatic com/a/b/k/d/e(Ljava/net/Inet6Address;)Z
ifeq L16
aload 0
invokestatic com/a/b/k/d/e(Ljava/net/Inet6Address;)Z
ldc "Address '%s' is not a Teredo address."
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
invokestatic com/a/b/k/d/b(Ljava/net/InetAddress;)Ljava/lang/String;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
invokevirtual java/net/Inet6Address/getAddress()[B
astore 7
aload 7
iconst_4
bipush 8
invokestatic java/util/Arrays/copyOfRange([BII)[B
invokestatic com/a/b/k/d/a([B)Ljava/net/Inet4Address;
astore 0
aload 7
bipush 8
invokestatic com/a/b/i/z/a([BI)Lcom/a/b/i/m;
invokeinterface com/a/b/i/m/readShort()S 0
istore 3
aload 7
bipush 10
invokestatic com/a/b/i/z/a([BI)Lcom/a/b/i/m;
invokeinterface com/a/b/i/m/readShort()S 0
istore 4
aload 7
bipush 12
bipush 16
invokestatic java/util/Arrays/copyOfRange([BII)[B
astore 7
iload 2
istore 1
L17:
iload 1
aload 7
arraylength
if_icmpge L18
aload 7
iload 1
aload 7
iload 1
baload
iconst_m1
ixor
i2b
bastore
iload 1
iconst_1
iadd
istore 1
goto L17
L18:
new com/a/b/k/e
dup
aload 0
aload 7
invokestatic com/a/b/k/d/a([B)Ljava/net/Inet4Address;
iload 4
iconst_m1
ixor
ldc_w 65535
iand
iload 3
ldc_w 65535
iand
invokespecial com/a/b/k/e/<init>(Ljava/net/Inet4Address;Ljava/net/Inet4Address;II)V
getfield com/a/b/k/e/a Ljava/net/Inet4Address;
astore 0
goto L12
L16:
new java/lang/IllegalArgumentException
dup
ldc "'%s' has no embedded IPv4 address."
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
invokestatic com/a/b/k/d/b(Ljava/net/InetAddress;)Ljava/lang/String;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L10:
aload 0
invokevirtual java/net/Inet6Address/getAddress()[B
iconst_0
bipush 8
invokestatic java/nio/ByteBuffer/wrap([BII)Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getLong()J
lstore 5
goto L13
L2:
iconst_1
istore 1
goto L4
.limit locals 8
.limit stack 7
.end method

.method private static c([B)Ljava/net/InetAddress;
aload 0
arraylength
newarray byte
astore 2
iconst_0
istore 1
L0:
iload 1
aload 0
arraylength
if_icmpge L1
aload 2
iload 1
aload 0
aload 0
arraylength
iload 1
isub
iconst_1
isub
baload
bastore
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 2
invokestatic java/net/InetAddress/getByAddress([B)Ljava/net/InetAddress;
areturn
.limit locals 3
.limit stack 5
.end method

.method private static c(Ljava/net/Inet6Address;)Z
aload 0
invokevirtual java/net/Inet6Address/getAddress()[B
astore 0
aload 0
iconst_0
baload
bipush 32
if_icmpne L0
aload 0
iconst_1
baload
iconst_2
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static c(Ljava/lang/String;)[B
iconst_0
istore 2
iconst_0
istore 4
iconst_0
istore 3
L0:
iload 2
aload 0
invokevirtual java/lang/String/length()I
if_icmpge L1
aload 0
iload 2
invokevirtual java/lang/String/charAt(I)C
istore 1
iload 1
bipush 46
if_icmpne L2
iconst_1
istore 4
L3:
iload 2
iconst_1
iadd
istore 2
goto L0
L2:
iload 1
bipush 58
if_icmpne L4
iload 4
ifeq L5
aconst_null
areturn
L5:
iconst_1
istore 3
goto L3
L4:
iload 1
bipush 16
invokestatic java/lang/Character/digit(CI)I
iconst_m1
if_icmpne L3
aconst_null
areturn
L1:
iload 3
ifeq L6
aload 0
astore 5
iload 4
ifeq L7
aload 0
bipush 58
invokevirtual java/lang/String/lastIndexOf(I)I
istore 2
aload 0
iconst_0
iload 2
iconst_1
iadd
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
astore 5
aload 0
iload 2
iconst_1
iadd
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
invokestatic com/a/b/k/d/d(Ljava/lang/String;)[B
astore 6
aload 6
ifnonnull L8
aconst_null
astore 0
L9:
aload 0
astore 5
aload 0
ifnonnull L7
aconst_null
areturn
L8:
aload 6
iconst_0
baload
sipush 255
iand
bipush 8
ishl
aload 6
iconst_1
baload
sipush 255
iand
ior
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
astore 0
aload 6
iconst_2
baload
istore 2
aload 6
iconst_3
baload
sipush 255
iand
iload 2
sipush 255
iand
bipush 8
ishl
ior
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
astore 6
aload 5
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 5
aload 0
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 0
aload 6
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 6
new java/lang/StringBuilder
dup
aload 5
invokevirtual java/lang/String/length()I
iconst_1
iadd
aload 0
invokevirtual java/lang/String/length()I
iadd
aload 6
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
goto L9
L7:
aload 5
invokestatic com/a/b/k/d/e(Ljava/lang/String;)[B
areturn
L6:
iload 4
ifeq L10
aload 0
invokestatic com/a/b/k/d/d(Ljava/lang/String;)[B
areturn
L10:
aconst_null
areturn
.limit locals 7
.limit stack 4
.end method

.method private static d(Ljava/net/InetAddress;)I
iconst_0
istore 2
aload 0
instanceof java/net/Inet4Address
ifeq L0
aload 0
checkcast java/net/Inet4Address
astore 0
L1:
aload 0
invokevirtual java/net/Inet4Address/getAddress()[B
invokestatic com/a/b/i/z/a([B)Lcom/a/b/i/m;
invokeinterface com/a/b/i/m/readInt()I 0
ireturn
L0:
aload 0
invokevirtual java/net/InetAddress/getAddress()[B
astore 7
iconst_0
istore 1
L2:
iload 1
bipush 15
if_icmpge L3
aload 7
iload 1
baload
ifeq L4
iconst_0
istore 1
L5:
iload 1
ifeq L6
aload 7
bipush 15
baload
iconst_1
if_icmpne L6
getstatic com/a/b/k/d/c Ljava/net/Inet4Address;
astore 0
goto L1
L4:
iload 1
iconst_1
iadd
istore 1
goto L2
L6:
iload 1
ifeq L7
aload 7
bipush 15
baload
ifne L7
getstatic com/a/b/k/d/d Ljava/net/Inet4Address;
astore 0
goto L1
L7:
aload 0
checkcast java/net/Inet6Address
astore 0
aload 0
invokestatic com/a/b/k/d/a(Ljava/net/Inet6Address;)Z
ifne L8
aload 0
invokestatic com/a/b/k/d/c(Ljava/net/Inet6Address;)Z
ifne L8
aload 0
invokestatic com/a/b/k/d/e(Ljava/net/Inet6Address;)Z
ifeq L9
L8:
iconst_1
istore 1
L10:
iload 1
ifeq L11
aload 0
invokestatic com/a/b/k/d/a(Ljava/net/Inet6Address;)Z
ifeq L12
aload 0
invokestatic com/a/b/k/d/a(Ljava/net/Inet6Address;)Z
ldc "Address '%s' is not IPv4-compatible."
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
invokestatic com/a/b/k/d/b(Ljava/net/InetAddress;)Ljava/lang/String;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
invokevirtual java/net/Inet6Address/getAddress()[B
bipush 12
bipush 16
invokestatic java/util/Arrays/copyOfRange([BII)[B
invokestatic com/a/b/k/d/a([B)Ljava/net/Inet4Address;
astore 0
L13:
aload 0
invokevirtual java/net/Inet4Address/hashCode()I
i2l
lstore 5
L14:
invokestatic com/a/b/g/am/a()Lcom/a/b/g/ak;
lload 5
invokeinterface com/a/b/g/ak/a(J)Lcom/a/b/g/ag; 2
invokevirtual com/a/b/g/ag/b()I
ldc_w -536870912
ior
istore 2
iload 2
istore 1
iload 2
iconst_m1
if_icmpne L15
bipush -2
istore 1
L15:
iload 1
invokestatic com/a/b/l/q/b(I)[B
invokestatic com/a/b/k/d/a([B)Ljava/net/Inet4Address;
astore 0
goto L1
L9:
iconst_0
istore 1
goto L10
L12:
aload 0
invokestatic com/a/b/k/d/c(Ljava/net/Inet6Address;)Z
ifeq L16
aload 0
invokestatic com/a/b/k/d/c(Ljava/net/Inet6Address;)Z
ldc "Address '%s' is not a 6to4 address."
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
invokestatic com/a/b/k/d/b(Ljava/net/InetAddress;)Ljava/lang/String;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
invokevirtual java/net/Inet6Address/getAddress()[B
iconst_2
bipush 6
invokestatic java/util/Arrays/copyOfRange([BII)[B
invokestatic com/a/b/k/d/a([B)Ljava/net/Inet4Address;
astore 0
goto L13
L16:
aload 0
invokestatic com/a/b/k/d/e(Ljava/net/Inet6Address;)Z
ifeq L17
aload 0
invokestatic com/a/b/k/d/e(Ljava/net/Inet6Address;)Z
ldc "Address '%s' is not a Teredo address."
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
invokestatic com/a/b/k/d/b(Ljava/net/InetAddress;)Ljava/lang/String;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
invokevirtual java/net/Inet6Address/getAddress()[B
astore 7
aload 7
iconst_4
bipush 8
invokestatic java/util/Arrays/copyOfRange([BII)[B
invokestatic com/a/b/k/d/a([B)Ljava/net/Inet4Address;
astore 0
aload 7
bipush 8
invokestatic com/a/b/i/z/a([BI)Lcom/a/b/i/m;
invokeinterface com/a/b/i/m/readShort()S 0
istore 3
aload 7
bipush 10
invokestatic com/a/b/i/z/a([BI)Lcom/a/b/i/m;
invokeinterface com/a/b/i/m/readShort()S 0
istore 4
aload 7
bipush 12
bipush 16
invokestatic java/util/Arrays/copyOfRange([BII)[B
astore 7
iload 2
istore 1
L18:
iload 1
aload 7
arraylength
if_icmpge L19
aload 7
iload 1
aload 7
iload 1
baload
iconst_m1
ixor
i2b
bastore
iload 1
iconst_1
iadd
istore 1
goto L18
L19:
new com/a/b/k/e
dup
aload 0
aload 7
invokestatic com/a/b/k/d/a([B)Ljava/net/Inet4Address;
iload 4
iconst_m1
ixor
ldc_w 65535
iand
iload 3
ldc_w 65535
iand
invokespecial com/a/b/k/e/<init>(Ljava/net/Inet4Address;Ljava/net/Inet4Address;II)V
getfield com/a/b/k/e/a Ljava/net/Inet4Address;
astore 0
goto L13
L17:
new java/lang/IllegalArgumentException
dup
ldc "'%s' has no embedded IPv4 address."
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
invokestatic com/a/b/k/d/b(Ljava/net/InetAddress;)Ljava/lang/String;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L11:
aload 0
invokevirtual java/net/Inet6Address/getAddress()[B
iconst_0
bipush 8
invokestatic java/nio/ByteBuffer/wrap([BII)Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/getLong()J
lstore 5
goto L14
L3:
iconst_1
istore 1
goto L5
.limit locals 8
.limit stack 7
.end method

.method private static d(Ljava/net/Inet6Address;)Ljava/net/Inet4Address;
aload 0
invokestatic com/a/b/k/d/c(Ljava/net/Inet6Address;)Z
ldc "Address '%s' is not a 6to4 address."
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
invokestatic com/a/b/k/d/b(Ljava/net/InetAddress;)Ljava/lang/String;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
invokevirtual java/net/Inet6Address/getAddress()[B
iconst_2
bipush 6
invokestatic java/util/Arrays/copyOfRange([BII)[B
invokestatic com/a/b/k/d/a([B)Ljava/net/Inet4Address;
areturn
.limit locals 1
.limit stack 6
.end method

.method private static d(Ljava/lang/String;)[B
.catch java/lang/NumberFormatException from L0 to L1 using L2
.catch java/lang/NumberFormatException from L3 to L4 using L2
.catch java/lang/NumberFormatException from L4 to L5 using L2
aload 0
ldc "\\."
iconst_5
invokevirtual java/lang/String/split(Ljava/lang/String;I)[Ljava/lang/String;
astore 0
aload 0
arraylength
iconst_4
if_icmpeq L6
aconst_null
areturn
L6:
iconst_4
newarray byte
astore 3
iconst_0
istore 1
L7:
iload 1
iconst_4
if_icmpge L8
aload 0
iload 1
aaload
astore 4
L0:
aload 4
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
istore 2
L1:
iload 2
sipush 255
if_icmpgt L4
L3:
aload 4
ldc "0"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L5
aload 4
invokevirtual java/lang/String/length()I
iconst_1
if_icmple L5
L4:
new java/lang/NumberFormatException
dup
invokespecial java/lang/NumberFormatException/<init>()V
athrow
L5:
aload 3
iload 1
iload 2
i2b
bastore
iload 1
iconst_1
iadd
istore 1
goto L7
L8:
aload 3
areturn
L2:
astore 0
aconst_null
areturn
.limit locals 5
.limit stack 3
.end method

.method private static e(Ljava/net/InetAddress;)Ljava/net/InetAddress;
aload 0
invokevirtual java/net/InetAddress/getAddress()[B
astore 3
aload 3
arraylength
iconst_1
isub
istore 1
L0:
iload 1
iflt L1
aload 3
iload 1
baload
ifne L1
aload 3
iload 1
iconst_m1
bastore
iload 1
iconst_1
isub
istore 1
goto L0
L1:
iload 1
iflt L2
iconst_1
istore 2
L3:
iload 2
ldc "Decrementing %s would wrap."
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 3
iload 1
aload 3
iload 1
baload
iconst_1
isub
i2b
bastore
aload 3
invokestatic com/a/b/k/d/b([B)Ljava/net/InetAddress;
areturn
L2:
iconst_0
istore 2
goto L3
.limit locals 4
.limit stack 6
.end method

.method private static e(Ljava/net/Inet6Address;)Z
aload 0
invokevirtual java/net/Inet6Address/getAddress()[B
astore 0
aload 0
iconst_0
baload
bipush 32
if_icmpne L0
aload 0
iconst_1
baload
iconst_1
if_icmpne L0
aload 0
iconst_2
baload
ifne L0
aload 0
iconst_3
baload
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static e(Ljava/lang/String;)[B
.catch java/lang/NumberFormatException from L0 to L1 using L2
.catch java/lang/NumberFormatException from L3 to L4 using L2
.catch java/lang/NumberFormatException from L5 to L6 using L2
aload 0
ldc ":"
bipush 10
invokevirtual java/lang/String/split(Ljava/lang/String;I)[Ljava/lang/String;
astore 0
aload 0
arraylength
iconst_3
if_icmplt L7
aload 0
arraylength
bipush 9
if_icmple L8
L7:
aconst_null
areturn
L8:
iconst_m1
istore 1
iconst_1
istore 2
L9:
iload 2
aload 0
arraylength
iconst_1
isub
if_icmpge L10
iload 1
istore 3
aload 0
iload 2
aaload
invokevirtual java/lang/String/length()I
ifne L11
iload 1
iflt L12
aconst_null
areturn
L12:
iload 2
istore 3
L11:
iload 2
iconst_1
iadd
istore 2
iload 3
istore 1
goto L9
L10:
iload 1
iflt L13
aload 0
arraylength
iload 1
isub
iconst_1
isub
istore 4
aload 0
iconst_0
aaload
invokevirtual java/lang/String/length()I
ifne L14
iload 1
iconst_1
isub
istore 3
iload 3
istore 2
iload 3
ifeq L15
aconst_null
areturn
L14:
iload 1
istore 2
L15:
iload 4
istore 3
aload 0
aload 0
arraylength
iconst_1
isub
aaload
invokevirtual java/lang/String/length()I
ifne L16
iload 4
iconst_1
isub
istore 4
iload 4
istore 3
iload 4
ifeq L16
aconst_null
areturn
L13:
aload 0
arraylength
istore 3
iconst_0
istore 2
L17:
bipush 8
iload 3
iload 2
iadd
isub
istore 4
iload 1
iflt L18
iload 4
ifle L19
L20:
bipush 16
invokestatic java/nio/ByteBuffer/allocate(I)Ljava/nio/ByteBuffer;
astore 5
iconst_0
istore 1
L21:
iload 1
iload 3
if_icmpge L22
L0:
aload 5
aload 0
iload 1
aaload
invokestatic com/a/b/k/d/h(Ljava/lang/String;)S
invokevirtual java/nio/ByteBuffer/putShort(S)Ljava/nio/ByteBuffer;
pop
L1:
iload 1
iconst_1
iadd
istore 1
goto L21
L23:
iload 2
istore 3
iload 1
iload 4
if_icmpge L24
L3:
aload 5
iconst_0
invokevirtual java/nio/ByteBuffer/putShort(S)Ljava/nio/ByteBuffer;
pop
L4:
iload 1
iconst_1
iadd
istore 1
goto L23
L24:
iload 3
ifle L25
L5:
aload 5
aload 0
aload 0
arraylength
iload 3
isub
aaload
invokestatic com/a/b/k/d/h(Ljava/lang/String;)S
invokevirtual java/nio/ByteBuffer/putShort(S)Ljava/nio/ByteBuffer;
pop
L6:
iload 3
iconst_1
isub
istore 3
goto L24
L2:
astore 0
aconst_null
areturn
L25:
aload 5
invokevirtual java/nio/ByteBuffer/array()[B
areturn
L16:
iload 2
istore 4
iload 3
istore 2
iload 4
istore 3
goto L17
L18:
iload 4
ifeq L20
L19:
aconst_null
areturn
L22:
iconst_0
istore 1
goto L23
.limit locals 6
.limit stack 4
.end method

.method private static f(Ljava/net/Inet6Address;)Lcom/a/b/k/e;
iconst_0
istore 1
aload 0
invokestatic com/a/b/k/d/e(Ljava/net/Inet6Address;)Z
ldc "Address '%s' is not a Teredo address."
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
invokestatic com/a/b/k/d/b(Ljava/net/InetAddress;)Ljava/lang/String;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
invokevirtual java/net/Inet6Address/getAddress()[B
astore 4
aload 4
iconst_4
bipush 8
invokestatic java/util/Arrays/copyOfRange([BII)[B
invokestatic com/a/b/k/d/a([B)Ljava/net/Inet4Address;
astore 0
aload 4
bipush 8
invokestatic com/a/b/i/z/a([BI)Lcom/a/b/i/m;
invokeinterface com/a/b/i/m/readShort()S 0
istore 2
aload 4
bipush 10
invokestatic com/a/b/i/z/a([BI)Lcom/a/b/i/m;
invokeinterface com/a/b/i/m/readShort()S 0
istore 3
aload 4
bipush 12
bipush 16
invokestatic java/util/Arrays/copyOfRange([BII)[B
astore 4
L0:
iload 1
aload 4
arraylength
if_icmpge L1
aload 4
iload 1
aload 4
iload 1
baload
iconst_m1
ixor
i2b
bastore
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
new com/a/b/k/e
dup
aload 0
aload 4
invokestatic com/a/b/k/d/a([B)Ljava/net/Inet4Address;
iload 3
iconst_m1
ixor
ldc_w 65535
iand
iload 2
ldc_w 65535
iand
invokespecial com/a/b/k/e/<init>(Ljava/net/Inet4Address;Ljava/net/Inet4Address;II)V
areturn
.limit locals 5
.limit stack 7
.end method

.method private static f(Ljava/lang/String;)Ljava/lang/String;
aload 0
bipush 58
invokevirtual java/lang/String/lastIndexOf(I)I
istore 1
aload 0
iconst_0
iload 1
iconst_1
iadd
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
astore 2
aload 0
iload 1
iconst_1
iadd
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
invokestatic com/a/b/k/d/d(Ljava/lang/String;)[B
astore 3
aload 3
ifnonnull L0
aconst_null
areturn
L0:
aload 3
iconst_0
baload
sipush 255
iand
bipush 8
ishl
aload 3
iconst_1
baload
sipush 255
iand
ior
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
astore 0
aload 3
iconst_2
baload
istore 1
aload 3
iconst_3
baload
sipush 255
iand
iload 1
sipush 255
iand
bipush 8
ishl
ior
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
astore 3
aload 2
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 2
aload 0
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 0
aload 3
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 3
new java/lang/StringBuilder
dup
aload 2
invokevirtual java/lang/String/length()I
iconst_1
iadd
aload 0
invokevirtual java/lang/String/length()I
iadd
aload 3
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 4
.limit stack 4
.end method

.method private static f(Ljava/net/InetAddress;)Ljava/net/InetAddress;
aload 0
invokevirtual java/net/InetAddress/getAddress()[B
astore 3
aload 3
arraylength
iconst_1
isub
istore 1
L0:
iload 1
iflt L1
aload 3
iload 1
baload
iconst_m1
if_icmpne L1
aload 3
iload 1
iconst_0
bastore
iload 1
iconst_1
isub
istore 1
goto L0
L1:
iload 1
iflt L2
iconst_1
istore 2
L3:
iload 2
ldc "Incrementing %s would wrap."
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 3
iload 1
aload 3
iload 1
baload
iconst_1
iadd
i2b
bastore
aload 3
invokestatic com/a/b/k/d/b([B)Ljava/net/InetAddress;
areturn
L2:
iconst_0
istore 2
goto L3
.limit locals 4
.limit stack 6
.end method

.method private static g(Ljava/lang/String;)B
aload 0
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
istore 1
iload 1
sipush 255
if_icmpgt L0
aload 0
ldc "0"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L1
aload 0
invokevirtual java/lang/String/length()I
iconst_1
if_icmple L1
L0:
new java/lang/NumberFormatException
dup
invokespecial java/lang/NumberFormatException/<init>()V
athrow
L1:
iload 1
i2b
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static g(Ljava/net/Inet6Address;)Z
aload 0
invokestatic com/a/b/k/d/e(Ljava/net/Inet6Address;)Z
ifeq L0
L1:
iconst_0
ireturn
L0:
aload 0
invokevirtual java/net/Inet6Address/getAddress()[B
astore 0
aload 0
bipush 8
baload
iconst_3
ior
iconst_3
if_icmpne L1
aload 0
bipush 9
baload
ifne L1
aload 0
bipush 10
baload
bipush 94
if_icmpne L1
aload 0
bipush 11
baload
bipush -2
if_icmpne L1
iconst_1
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static g(Ljava/net/InetAddress;)Z
aload 0
invokevirtual java/net/InetAddress/getAddress()[B
astore 0
iconst_0
istore 1
L0:
iload 1
aload 0
arraylength
if_icmpge L1
aload 0
iload 1
baload
iconst_m1
if_icmpeq L2
iconst_0
ireturn
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
iconst_1
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static h(Ljava/net/Inet6Address;)Ljava/net/Inet4Address;
aload 0
invokestatic com/a/b/k/d/e(Ljava/net/Inet6Address;)Z
ifne L0
aload 0
invokevirtual java/net/Inet6Address/getAddress()[B
astore 2
aload 2
bipush 8
baload
iconst_3
ior
iconst_3
if_icmpne L0
aload 2
bipush 9
baload
ifne L0
aload 2
bipush 10
baload
bipush 94
if_icmpne L0
aload 2
bipush 11
baload
bipush -2
if_icmpne L0
iconst_1
istore 1
L1:
iload 1
ldc "Address '%s' is not an ISATAP address."
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
invokestatic com/a/b/k/d/b(Ljava/net/InetAddress;)Ljava/lang/String;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
invokevirtual java/net/Inet6Address/getAddress()[B
bipush 12
bipush 16
invokestatic java/util/Arrays/copyOfRange([BII)[B
invokestatic com/a/b/k/d/a([B)Ljava/net/Inet4Address;
areturn
L0:
iconst_0
istore 1
goto L1
.limit locals 3
.limit stack 6
.end method

.method private static h(Ljava/lang/String;)S
aload 0
bipush 16
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;I)I
istore 1
iload 1
ldc_w 65535
if_icmple L0
new java/lang/NumberFormatException
dup
invokespecial java/lang/NumberFormatException/<init>()V
athrow
L0:
iload 1
i2s
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static i(Ljava/lang/String;)Ljava/net/InetAddress;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
ldc "["
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L0
aload 0
ldc "]"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L0
aload 0
iconst_1
aload 0
invokevirtual java/lang/String/length()I
iconst_1
isub
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
astore 2
bipush 16
istore 1
L1:
aload 2
invokestatic com/a/b/k/d/c(Ljava/lang/String;)[B
astore 2
aload 2
ifnull L2
aload 2
arraylength
iload 1
if_icmpeq L3
L2:
new java/lang/IllegalArgumentException
dup
ldc "Not a valid URI IP literal: '%s'"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
iconst_4
istore 1
aload 0
astore 2
goto L1
L3:
aload 2
invokestatic com/a/b/k/d/b([B)Ljava/net/InetAddress;
areturn
.limit locals 3
.limit stack 7
.end method

.method private static i(Ljava/net/Inet6Address;)Z
aload 0
invokestatic com/a/b/k/d/a(Ljava/net/Inet6Address;)Z
ifne L0
aload 0
invokestatic com/a/b/k/d/c(Ljava/net/Inet6Address;)Z
ifne L0
aload 0
invokestatic com/a/b/k/d/e(Ljava/net/Inet6Address;)Z
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static j(Ljava/net/Inet6Address;)Ljava/net/Inet4Address;
iconst_0
istore 1
aload 0
invokestatic com/a/b/k/d/a(Ljava/net/Inet6Address;)Z
ifeq L0
aload 0
invokestatic com/a/b/k/d/a(Ljava/net/Inet6Address;)Z
ldc "Address '%s' is not IPv4-compatible."
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
invokestatic com/a/b/k/d/b(Ljava/net/InetAddress;)Ljava/lang/String;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
invokevirtual java/net/Inet6Address/getAddress()[B
bipush 12
bipush 16
invokestatic java/util/Arrays/copyOfRange([BII)[B
invokestatic com/a/b/k/d/a([B)Ljava/net/Inet4Address;
areturn
L0:
aload 0
invokestatic com/a/b/k/d/c(Ljava/net/Inet6Address;)Z
ifeq L1
aload 0
invokestatic com/a/b/k/d/c(Ljava/net/Inet6Address;)Z
ldc "Address '%s' is not a 6to4 address."
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
invokestatic com/a/b/k/d/b(Ljava/net/InetAddress;)Ljava/lang/String;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
invokevirtual java/net/Inet6Address/getAddress()[B
iconst_2
bipush 6
invokestatic java/util/Arrays/copyOfRange([BII)[B
invokestatic com/a/b/k/d/a([B)Ljava/net/Inet4Address;
areturn
L1:
aload 0
invokestatic com/a/b/k/d/e(Ljava/net/Inet6Address;)Z
ifeq L2
aload 0
invokestatic com/a/b/k/d/e(Ljava/net/Inet6Address;)Z
ldc "Address '%s' is not a Teredo address."
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
invokestatic com/a/b/k/d/b(Ljava/net/InetAddress;)Ljava/lang/String;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
invokevirtual java/net/Inet6Address/getAddress()[B
astore 4
aload 4
iconst_4
bipush 8
invokestatic java/util/Arrays/copyOfRange([BII)[B
invokestatic com/a/b/k/d/a([B)Ljava/net/Inet4Address;
astore 0
aload 4
bipush 8
invokestatic com/a/b/i/z/a([BI)Lcom/a/b/i/m;
invokeinterface com/a/b/i/m/readShort()S 0
istore 2
aload 4
bipush 10
invokestatic com/a/b/i/z/a([BI)Lcom/a/b/i/m;
invokeinterface com/a/b/i/m/readShort()S 0
istore 3
aload 4
bipush 12
bipush 16
invokestatic java/util/Arrays/copyOfRange([BII)[B
astore 4
L3:
iload 1
aload 4
arraylength
if_icmpge L4
aload 4
iload 1
aload 4
iload 1
baload
iconst_m1
ixor
i2b
bastore
iload 1
iconst_1
iadd
istore 1
goto L3
L4:
new com/a/b/k/e
dup
aload 0
aload 4
invokestatic com/a/b/k/d/a([B)Ljava/net/Inet4Address;
iload 3
iconst_m1
ixor
ldc_w 65535
iand
iload 2
ldc_w 65535
iand
invokespecial com/a/b/k/e/<init>(Ljava/net/Inet4Address;Ljava/net/Inet4Address;II)V
getfield com/a/b/k/e/a Ljava/net/Inet4Address;
areturn
L2:
new java/lang/IllegalArgumentException
dup
ldc "'%s' has no embedded IPv4 address."
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
invokestatic com/a/b/k/d/b(Ljava/net/InetAddress;)Ljava/lang/String;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 5
.limit stack 7
.end method

.method private static j(Ljava/lang/String;)Z
.catch java/lang/IllegalArgumentException from L0 to L1 using L2
.catch java/lang/IllegalArgumentException from L3 to L4 using L2
.catch java/lang/IllegalArgumentException from L5 to L6 using L2
.catch java/lang/IllegalArgumentException from L6 to L7 using L2
.catch java/lang/IllegalArgumentException from L7 to L8 using L2
L0:
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
ldc "["
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L9
aload 0
ldc "]"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L9
aload 0
iconst_1
aload 0
invokevirtual java/lang/String/length()I
iconst_1
isub
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
astore 2
L1:
bipush 16
istore 1
L3:
aload 2
invokestatic com/a/b/k/d/c(Ljava/lang/String;)[B
astore 2
L4:
aload 2
ifnull L6
L5:
aload 2
arraylength
iload 1
if_icmpeq L7
L6:
new java/lang/IllegalArgumentException
dup
ldc "Not a valid URI IP literal: '%s'"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L7:
aload 2
invokestatic com/a/b/k/d/b([B)Ljava/net/InetAddress;
pop
L8:
iconst_1
ireturn
L2:
astore 0
iconst_0
ireturn
L9:
iconst_4
istore 1
aload 0
astore 2
goto L3
.limit locals 3
.limit stack 7
.end method

.method private static k(Ljava/lang/String;)Z
bipush 10
istore 3
aload 0
invokestatic com/a/b/k/d/c(Ljava/lang/String;)[B
astore 0
aload 0
ifnull L0
aload 0
arraylength
bipush 16
if_icmpne L0
iconst_0
istore 1
L1:
iload 3
istore 2
iload 1
bipush 10
if_icmpge L2
aload 0
iload 1
baload
ifeq L3
L0:
iconst_0
ireturn
L3:
iload 1
iconst_1
iadd
istore 1
goto L1
L4:
iload 2
iconst_1
iadd
istore 2
L2:
iload 2
bipush 12
if_icmpge L5
aload 0
iload 2
baload
iconst_m1
if_icmpeq L4
iconst_0
ireturn
L5:
iconst_1
ireturn
.limit locals 4
.limit stack 2
.end method
