.bytecode 50.0
.class public synchronized abstract com/a/b/g/i
.super com/a/b/g/d

.field private final 'a' Ljava/nio/ByteBuffer;

.field private final 'b' I

.field private final 'c' I

.method protected <init>(I)V
aload 0
iload 1
iload 1
invokespecial com/a/b/g/i/<init>(II)V
return
.limit locals 2
.limit stack 3
.end method

.method private <init>(II)V
aload 0
invokespecial com/a/b/g/d/<init>()V
iload 2
iload 1
irem
ifne L0
iconst_1
istore 3
L1:
iload 3
invokestatic com/a/b/b/cn/a(Z)V
aload 0
iload 2
bipush 7
iadd
invokestatic java/nio/ByteBuffer/allocate(I)Ljava/nio/ByteBuffer;
getstatic java/nio/ByteOrder/LITTLE_ENDIAN Ljava/nio/ByteOrder;
invokevirtual java/nio/ByteBuffer/order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
putfield com/a/b/g/i/a Ljava/nio/ByteBuffer;
aload 0
iload 2
putfield com/a/b/g/i/b I
aload 0
iload 1
putfield com/a/b/g/i/c I
return
L0:
iconst_0
istore 3
goto L1
.limit locals 4
.limit stack 3
.end method

.method private c(Ljava/nio/ByteBuffer;)Lcom/a/b/g/al;
aload 1
invokevirtual java/nio/ByteBuffer/remaining()I
aload 0
getfield com/a/b/g/i/a Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/remaining()I
if_icmpgt L0
aload 0
getfield com/a/b/g/i/a Ljava/nio/ByteBuffer;
aload 1
invokevirtual java/nio/ByteBuffer/put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
pop
aload 0
invokespecial com/a/b/g/i/c()V
aload 0
areturn
L0:
aload 0
getfield com/a/b/g/i/b I
istore 3
aload 0
getfield com/a/b/g/i/a Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/position()I
istore 4
iconst_0
istore 2
L1:
iload 2
iload 3
iload 4
isub
if_icmpge L2
aload 0
getfield com/a/b/g/i/a Ljava/nio/ByteBuffer;
aload 1
invokevirtual java/nio/ByteBuffer/get()B
invokevirtual java/nio/ByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
aload 0
invokespecial com/a/b/g/i/d()V
L3:
aload 1
invokevirtual java/nio/ByteBuffer/remaining()I
aload 0
getfield com/a/b/g/i/c I
if_icmplt L4
aload 0
aload 1
invokevirtual com/a/b/g/i/a(Ljava/nio/ByteBuffer;)V
goto L3
L4:
aload 0
getfield com/a/b/g/i/a Ljava/nio/ByteBuffer;
aload 1
invokevirtual java/nio/ByteBuffer/put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
pop
aload 0
areturn
.limit locals 5
.limit stack 3
.end method

.method private c()V
aload 0
getfield com/a/b/g/i/a Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/remaining()I
bipush 8
if_icmpge L0
aload 0
invokespecial com/a/b/g/i/d()V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method private d()V
aload 0
getfield com/a/b/g/i/a Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/flip()Ljava/nio/Buffer;
pop
L0:
aload 0
getfield com/a/b/g/i/a Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/remaining()I
aload 0
getfield com/a/b/g/i/c I
if_icmplt L1
aload 0
aload 0
getfield com/a/b/g/i/a Ljava/nio/ByteBuffer;
invokevirtual com/a/b/g/i/a(Ljava/nio/ByteBuffer;)V
goto L0
L1:
aload 0
getfield com/a/b/g/i/a Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/compact()Ljava/nio/ByteBuffer;
pop
return
.limit locals 1
.limit stack 2
.end method

.method public final a()Lcom/a/b/g/ag;
aload 0
invokespecial com/a/b/g/i/d()V
aload 0
getfield com/a/b/g/i/a Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/flip()Ljava/nio/Buffer;
pop
aload 0
getfield com/a/b/g/i/a Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/remaining()I
ifle L0
aload 0
aload 0
getfield com/a/b/g/i/a Ljava/nio/ByteBuffer;
invokevirtual com/a/b/g/i/b(Ljava/nio/ByteBuffer;)V
L0:
aload 0
invokevirtual com/a/b/g/i/b()Lcom/a/b/g/ag;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final a(C)Lcom/a/b/g/al;
aload 0
getfield com/a/b/g/i/a Ljava/nio/ByteBuffer;
iload 1
invokevirtual java/nio/ByteBuffer/putChar(C)Ljava/nio/ByteBuffer;
pop
aload 0
invokespecial com/a/b/g/i/c()V
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(I)Lcom/a/b/g/al;
aload 0
getfield com/a/b/g/i/a Ljava/nio/ByteBuffer;
iload 1
invokevirtual java/nio/ByteBuffer/putInt(I)Ljava/nio/ByteBuffer;
pop
aload 0
invokespecial com/a/b/g/i/c()V
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(J)Lcom/a/b/g/al;
aload 0
getfield com/a/b/g/i/a Ljava/nio/ByteBuffer;
lload 1
invokevirtual java/nio/ByteBuffer/putLong(J)Ljava/nio/ByteBuffer;
pop
aload 0
invokespecial com/a/b/g/i/c()V
aload 0
areturn
.limit locals 3
.limit stack 3
.end method

.method public final a(Ljava/lang/CharSequence;)Lcom/a/b/g/al;
iconst_0
istore 2
L0:
iload 2
aload 1
invokeinterface java/lang/CharSequence/length()I 0
if_icmpge L1
aload 0
aload 1
iload 2
invokeinterface java/lang/CharSequence/charAt(I)C 1
invokevirtual com/a/b/g/i/a(C)Lcom/a/b/g/al;
pop
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 0
areturn
.limit locals 3
.limit stack 3
.end method

.method public final a(Ljava/lang/Object;Lcom/a/b/g/w;)Lcom/a/b/g/al;
aload 2
aload 1
aload 0
invokeinterface com/a/b/g/w/a(Ljava/lang/Object;Lcom/a/b/g/bn;)V 2
aload 0
areturn
.limit locals 3
.limit stack 3
.end method

.method public final a(S)Lcom/a/b/g/al;
aload 0
getfield com/a/b/g/i/a Ljava/nio/ByteBuffer;
iload 1
invokevirtual java/nio/ByteBuffer/putShort(S)Ljava/nio/ByteBuffer;
pop
aload 0
invokespecial com/a/b/g/i/c()V
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method protected abstract a(Ljava/nio/ByteBuffer;)V
.end method

.method abstract b()Lcom/a/b/g/ag;
.end method

.method public final b(B)Lcom/a/b/g/al;
aload 0
getfield com/a/b/g/i/a Ljava/nio/ByteBuffer;
iload 1
invokevirtual java/nio/ByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
aload 0
invokespecial com/a/b/g/i/c()V
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final b([B)Lcom/a/b/g/al;
aload 0
aload 1
iconst_0
aload 1
arraylength
invokevirtual com/a/b/g/i/b([BII)Lcom/a/b/g/al;
areturn
.limit locals 2
.limit stack 4
.end method

.method public final b([BII)Lcom/a/b/g/al;
aload 1
iload 2
iload 3
invokestatic java/nio/ByteBuffer/wrap([BII)Ljava/nio/ByteBuffer;
getstatic java/nio/ByteOrder/LITTLE_ENDIAN Ljava/nio/ByteOrder;
invokevirtual java/nio/ByteBuffer/order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
astore 1
aload 1
invokevirtual java/nio/ByteBuffer/remaining()I
aload 0
getfield com/a/b/g/i/a Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/remaining()I
if_icmpgt L0
aload 0
getfield com/a/b/g/i/a Ljava/nio/ByteBuffer;
aload 1
invokevirtual java/nio/ByteBuffer/put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
pop
aload 0
invokespecial com/a/b/g/i/c()V
aload 0
areturn
L0:
aload 0
getfield com/a/b/g/i/b I
istore 3
aload 0
getfield com/a/b/g/i/a Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/position()I
istore 4
iconst_0
istore 2
L1:
iload 2
iload 3
iload 4
isub
if_icmpge L2
aload 0
getfield com/a/b/g/i/a Ljava/nio/ByteBuffer;
aload 1
invokevirtual java/nio/ByteBuffer/get()B
invokevirtual java/nio/ByteBuffer/put(B)Ljava/nio/ByteBuffer;
pop
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
aload 0
invokespecial com/a/b/g/i/d()V
L3:
aload 1
invokevirtual java/nio/ByteBuffer/remaining()I
aload 0
getfield com/a/b/g/i/c I
if_icmplt L4
aload 0
aload 1
invokevirtual com/a/b/g/i/a(Ljava/nio/ByteBuffer;)V
goto L3
L4:
aload 0
getfield com/a/b/g/i/a Ljava/nio/ByteBuffer;
aload 1
invokevirtual java/nio/ByteBuffer/put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
pop
aload 0
areturn
.limit locals 5
.limit stack 3
.end method

.method public final synthetic b(C)Lcom/a/b/g/bn;
aload 0
iload 1
invokevirtual com/a/b/g/i/a(C)Lcom/a/b/g/al;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final synthetic b(I)Lcom/a/b/g/bn;
aload 0
iload 1
invokevirtual com/a/b/g/i/a(I)Lcom/a/b/g/al;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final synthetic b(J)Lcom/a/b/g/bn;
aload 0
lload 1
invokevirtual com/a/b/g/i/a(J)Lcom/a/b/g/al;
areturn
.limit locals 3
.limit stack 3
.end method

.method public final synthetic b(Ljava/lang/CharSequence;)Lcom/a/b/g/bn;
aload 0
aload 1
invokevirtual com/a/b/g/i/a(Ljava/lang/CharSequence;)Lcom/a/b/g/al;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final synthetic b(S)Lcom/a/b/g/bn;
aload 0
iload 1
invokevirtual com/a/b/g/i/a(S)Lcom/a/b/g/al;
areturn
.limit locals 2
.limit stack 2
.end method

.method protected b(Ljava/nio/ByteBuffer;)V
aload 1
aload 1
invokevirtual java/nio/ByteBuffer/limit()I
invokevirtual java/nio/ByteBuffer/position(I)Ljava/nio/Buffer;
pop
aload 1
aload 0
getfield com/a/b/g/i/c I
bipush 7
iadd
invokevirtual java/nio/ByteBuffer/limit(I)Ljava/nio/Buffer;
pop
L0:
aload 1
invokevirtual java/nio/ByteBuffer/position()I
aload 0
getfield com/a/b/g/i/c I
if_icmpge L1
aload 1
lconst_0
invokevirtual java/nio/ByteBuffer/putLong(J)Ljava/nio/ByteBuffer;
pop
goto L0
L1:
aload 1
aload 0
getfield com/a/b/g/i/c I
invokevirtual java/nio/ByteBuffer/limit(I)Ljava/nio/Buffer;
pop
aload 1
invokevirtual java/nio/ByteBuffer/flip()Ljava/nio/Buffer;
pop
aload 0
aload 1
invokevirtual com/a/b/g/i/a(Ljava/nio/ByteBuffer;)V
return
.limit locals 2
.limit stack 3
.end method

.method public final synthetic c(B)Lcom/a/b/g/bn;
aload 0
iload 1
invokevirtual com/a/b/g/i/b(B)Lcom/a/b/g/al;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final synthetic c([B)Lcom/a/b/g/bn;
aload 0
aload 1
invokevirtual com/a/b/g/i/b([B)Lcom/a/b/g/al;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final synthetic c([BII)Lcom/a/b/g/bn;
aload 0
aload 1
iload 2
iload 3
invokevirtual com/a/b/g/i/b([BII)Lcom/a/b/g/al;
areturn
.limit locals 4
.limit stack 4
.end method
