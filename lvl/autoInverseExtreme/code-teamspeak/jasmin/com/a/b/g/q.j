.bytecode 50.0
.class final synchronized com/a/b/g/q
.super java/lang/Object

.field final 'a' [J

.field 'b' J

.method <init>(J)V
aload 0
lload 1
getstatic java/math/RoundingMode/CEILING Ljava/math/RoundingMode;
invokestatic com/a/b/j/i/d(JLjava/math/RoundingMode;)J
invokestatic com/a/b/l/q/a(J)I
newarray long
invokespecial com/a/b/g/q/<init>([J)V
return
.limit locals 3
.limit stack 4
.end method

.method <init>([J)V
iconst_0
istore 2
aload 0
invokespecial java/lang/Object/<init>()V
aload 1
arraylength
ifle L0
iconst_1
istore 4
L1:
iload 4
ldc "data length is zero!"
invokestatic com/a/b/b/cn/a(ZLjava/lang/Object;)V
aload 0
aload 1
putfield com/a/b/g/q/a [J
lconst_0
lstore 5
aload 1
arraylength
istore 3
L2:
iload 2
iload 3
if_icmpge L3
lload 5
aload 1
iload 2
laload
invokestatic java/lang/Long/bitCount(J)I
i2l
ladd
lstore 5
iload 2
iconst_1
iadd
istore 2
goto L2
L0:
iconst_0
istore 4
goto L1
L3:
aload 0
lload 5
putfield com/a/b/g/q/b J
return
.limit locals 7
.limit stack 4
.end method

.method private a(Lcom/a/b/g/q;)V
iconst_0
istore 2
aload 0
getfield com/a/b/g/q/a [J
arraylength
aload 1
getfield com/a/b/g/q/a [J
arraylength
if_icmpne L0
iconst_1
istore 3
L1:
iload 3
ldc "BitArrays must be of equal length (%s != %s)"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/g/q/a [J
arraylength
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
aload 1
getfield com/a/b/g/q/a [J
arraylength
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
lconst_0
putfield com/a/b/g/q/b J
L2:
iload 2
aload 0
getfield com/a/b/g/q/a [J
arraylength
if_icmpge L3
aload 0
getfield com/a/b/g/q/a [J
astore 4
aload 4
iload 2
aload 4
iload 2
laload
aload 1
getfield com/a/b/g/q/a [J
iload 2
laload
lor
lastore
aload 0
aload 0
getfield com/a/b/g/q/b J
aload 0
getfield com/a/b/g/q/a [J
iload 2
laload
invokestatic java/lang/Long/bitCount(J)I
i2l
ladd
putfield com/a/b/g/q/b J
iload 2
iconst_1
iadd
istore 2
goto L2
L0:
iconst_0
istore 3
goto L1
L3:
return
.limit locals 5
.limit stack 6
.end method

.method private c()J
aload 0
getfield com/a/b/g/q/b J
lreturn
.limit locals 1
.limit stack 2
.end method

.method final a()J
aload 0
getfield com/a/b/g/q/a [J
arraylength
i2l
ldc2_w 64L
lmul
lreturn
.limit locals 1
.limit stack 4
.end method

.method final a(J)Z
aload 0
lload 1
invokevirtual com/a/b/g/q/b(J)Z
ifne L0
aload 0
getfield com/a/b/g/q/a [J
astore 4
lload 1
bipush 6
lushr
l2i
istore 3
aload 4
iload 3
aload 4
iload 3
laload
lconst_1
lload 1
l2i
lshl
lor
lastore
aload 0
aload 0
getfield com/a/b/g/q/b J
lconst_1
ladd
putfield com/a/b/g/q/b J
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 5
.limit stack 8
.end method

.method final b()Lcom/a/b/g/q;
new com/a/b/g/q
dup
aload 0
getfield com/a/b/g/q/a [J
invokevirtual [J/clone()Ljava/lang/Object;
checkcast [J
invokespecial com/a/b/g/q/<init>([J)V
areturn
.limit locals 1
.limit stack 3
.end method

.method final b(J)Z
aload 0
getfield com/a/b/g/q/a [J
lload 1
bipush 6
lushr
l2i
laload
lconst_1
lload 1
l2i
lshl
land
lconst_0
lcmp
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 6
.end method

.method public final equals(Ljava/lang/Object;)Z
aload 1
instanceof com/a/b/g/q
ifeq L0
aload 1
checkcast com/a/b/g/q
astore 1
aload 0
getfield com/a/b/g/q/a [J
aload 1
getfield com/a/b/g/q/a [J
invokestatic java/util/Arrays/equals([J[J)Z
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final hashCode()I
aload 0
getfield com/a/b/g/q/a [J
invokestatic java/util/Arrays/hashCode([J)I
ireturn
.limit locals 1
.limit stack 1
.end method
