.bytecode 50.0
.class public synchronized abstract com/a/b/g/ag
.super java/lang/Object
.annotation invisible Lcom/a/b/a/a;
.end annotation

.field private static final 'a' [C

.method static <clinit>()V
ldc "0123456789abcdef"
invokevirtual java/lang/String/toCharArray()[C
putstatic com/a/b/g/ag/a [C
return
.limit locals 0
.limit stack 1
.end method

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(C)I
iload 0
bipush 48
if_icmplt L0
iload 0
bipush 57
if_icmpgt L0
iload 0
bipush 48
isub
ireturn
L0:
iload 0
bipush 97
if_icmplt L1
iload 0
bipush 102
if_icmpgt L1
iload 0
bipush 97
isub
bipush 10
iadd
ireturn
L1:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
bipush 32
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Illegal hexadecimal character: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 0
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 5
.end method

.method public static a(I)Lcom/a/b/g/ag;
new com/a/b/g/ai
dup
iload 0
invokespecial com/a/b/g/ai/<init>(I)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public static a(J)Lcom/a/b/g/ag;
new com/a/b/g/aj
dup
lload 0
invokespecial com/a/b/g/aj/<init>(J)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static a(Ljava/lang/String;)Lcom/a/b/g/ag;
iconst_0
istore 1
aload 0
invokevirtual java/lang/String/length()I
iconst_2
if_icmplt L0
iconst_1
istore 4
L1:
iload 4
ldc "input string (%s) must have at least 2 characters"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
invokevirtual java/lang/String/length()I
iconst_2
irem
ifne L2
iconst_1
istore 4
L3:
iload 4
ldc "input string (%s) must have an even number of characters"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
invokevirtual java/lang/String/length()I
iconst_2
idiv
newarray byte
astore 5
L4:
iload 1
aload 0
invokevirtual java/lang/String/length()I
if_icmpge L5
aload 0
iload 1
invokevirtual java/lang/String/charAt(I)C
invokestatic com/a/b/g/ag/a(C)I
istore 2
aload 0
iload 1
iconst_1
iadd
invokevirtual java/lang/String/charAt(I)C
invokestatic com/a/b/g/ag/a(C)I
istore 3
aload 5
iload 1
iconst_2
idiv
iload 2
iconst_4
ishl
iload 3
iadd
i2b
bastore
iload 1
iconst_2
iadd
istore 1
goto L4
L0:
iconst_0
istore 4
goto L1
L2:
iconst_0
istore 4
goto L3
L5:
aload 5
invokestatic com/a/b/g/ag/a([B)Lcom/a/b/g/ag;
areturn
.limit locals 6
.limit stack 6
.end method

.method static a([B)Lcom/a/b/g/ag;
new com/a/b/g/ah
dup
aload 0
invokespecial com/a/b/g/ah/<init>([B)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private b([BII)I
iconst_2
newarray int
dup
iconst_0
iload 3
iastore
dup
iconst_1
aload 0
invokevirtual com/a/b/g/ag/a()I
bipush 8
idiv
iastore
invokestatic com/a/b/l/q/a([I)I
istore 3
iload 2
iload 2
iload 3
iadd
aload 1
arraylength
invokestatic com/a/b/b/cn/a(III)V
aload 0
aload 1
iload 2
iload 3
invokevirtual com/a/b/g/ag/a([BII)V
iload 3
ireturn
.limit locals 4
.limit stack 5
.end method

.method private static b([B)Lcom/a/b/g/ag;
aload 0
arraylength
ifle L0
iconst_1
istore 1
L1:
iload 1
ldc "A HashCode must contain at least 1 byte."
invokestatic com/a/b/b/cn/a(ZLjava/lang/Object;)V
aload 0
invokevirtual [B/clone()Ljava/lang/Object;
checkcast [B
invokestatic com/a/b/g/ag/a([B)Lcom/a/b/g/ag;
areturn
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 2
.end method

.method public abstract a()I
.end method

.method abstract a([BII)V
.end method

.method abstract a(Lcom/a/b/g/ag;)Z
.end method

.method public abstract b()I
.end method

.method public abstract c()J
.end method

.method public abstract d()J
.end method

.method public abstract e()[B
.end method

.method public final equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
iconst_0
istore 3
iload 3
istore 2
aload 1
instanceof com/a/b/g/ag
ifeq L0
aload 1
checkcast com/a/b/g/ag
astore 1
iload 3
istore 2
aload 0
invokevirtual com/a/b/g/ag/a()I
aload 1
invokevirtual com/a/b/g/ag/a()I
if_icmpne L0
iload 3
istore 2
aload 0
aload 1
invokevirtual com/a/b/g/ag/a(Lcom/a/b/g/ag;)Z
ifeq L0
iconst_1
istore 2
L0:
iload 2
ireturn
.limit locals 4
.limit stack 2
.end method

.method f()[B
aload 0
invokevirtual com/a/b/g/ag/e()[B
areturn
.limit locals 1
.limit stack 1
.end method

.method public final hashCode()I
aload 0
invokevirtual com/a/b/g/ag/a()I
bipush 32
if_icmplt L0
aload 0
invokevirtual com/a/b/g/ag/b()I
istore 3
L1:
iload 3
ireturn
L0:
aload 0
invokevirtual com/a/b/g/ag/e()[B
astore 4
aload 4
iconst_0
baload
sipush 255
iand
istore 1
iconst_1
istore 2
L2:
iload 1
istore 3
iload 2
aload 4
arraylength
if_icmpge L1
iload 1
aload 4
iload 2
baload
sipush 255
iand
iload 2
bipush 8
imul
ishl
ior
istore 1
iload 2
iconst_1
iadd
istore 2
goto L2
.limit locals 5
.limit stack 4
.end method

.method public final toString()Ljava/lang/String;
aload 0
invokevirtual com/a/b/g/ag/e()[B
astore 4
new java/lang/StringBuilder
dup
aload 4
arraylength
iconst_2
imul
invokespecial java/lang/StringBuilder/<init>(I)V
astore 5
aload 4
arraylength
istore 2
iconst_0
istore 1
L0:
iload 1
iload 2
if_icmpge L1
aload 4
iload 1
baload
istore 3
aload 5
getstatic com/a/b/g/ag/a [C
iload 3
iconst_4
ishr
bipush 15
iand
caload
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
getstatic com/a/b/g/ag/a [C
iload 3
bipush 15
iand
caload
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 5
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 6
.limit stack 4
.end method
