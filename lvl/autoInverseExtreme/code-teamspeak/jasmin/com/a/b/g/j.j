.bytecode 50.0
.class public final synchronized com/a/b/g/j
.super java/lang/Object
.implements com/a/b/b/co
.implements java/io/Serializable
.annotation invisible Lcom/a/b/a/a;
.end annotation

.field private static final 'e' Lcom/a/b/g/m;

.field private final 'a' Lcom/a/b/g/q;

.field private final 'b' I

.field private final 'c' Lcom/a/b/g/w;

.field private final 'd' Lcom/a/b/g/m;

.method static <clinit>()V
getstatic com/a/b/g/n/b Lcom/a/b/g/n;
putstatic com/a/b/g/j/e Lcom/a/b/g/m;
return
.limit locals 0
.limit stack 1
.end method

.method private <init>(Lcom/a/b/g/q;ILcom/a/b/g/w;Lcom/a/b/g/m;)V
aload 0
invokespecial java/lang/Object/<init>()V
iload 2
ifle L0
iconst_1
istore 5
L1:
iload 5
ldc "numHashFunctions (%s) must be > 0"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
iload 2
sipush 255
if_icmpgt L2
iconst_1
istore 5
L3:
iload 5
ldc "numHashFunctions (%s) must be <= 255"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/g/q
putfield com/a/b/g/j/a Lcom/a/b/g/q;
aload 0
iload 2
putfield com/a/b/g/j/b I
aload 0
aload 3
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/g/w
putfield com/a/b/g/j/c Lcom/a/b/g/w;
aload 0
aload 4
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/g/m
putfield com/a/b/g/j/d Lcom/a/b/g/m;
return
L0:
iconst_0
istore 5
goto L1
L2:
iconst_0
istore 5
goto L3
.limit locals 6
.limit stack 6
.end method

.method synthetic <init>(Lcom/a/b/g/q;ILcom/a/b/g/w;Lcom/a/b/g/m;B)V
aload 0
aload 1
iload 2
aload 3
aload 4
invokespecial com/a/b/g/j/<init>(Lcom/a/b/g/q;ILcom/a/b/g/w;Lcom/a/b/g/m;)V
return
.limit locals 6
.limit stack 5
.end method

.method private static a(JJ)I
.annotation invisible Lcom/a/b/a/d;
.end annotation
iconst_1
lload 2
l2d
lload 0
l2d
ddiv
ldc2_w 2.0D
invokestatic java/lang/Math/log(D)D
dmul
invokestatic java/lang/Math/round(D)J
l2i
invokestatic java/lang/Math/max(II)I
ireturn
.limit locals 4
.limit stack 5
.end method

.method private static a(JD)J
.annotation invisible Lcom/a/b/a/d;
.end annotation
dload 2
dstore 4
dload 2
dconst_0
dcmpl
ifne L0
ldc2_w 4.9E-324D
dstore 4
L0:
lload 0
lneg
l2d
dload 4
invokestatic java/lang/Math/log(D)D
dmul
ldc2_w 2.0D
invokestatic java/lang/Math/log(D)D
ldc2_w 2.0D
invokestatic java/lang/Math/log(D)D
dmul
ddiv
d2l
lreturn
.limit locals 6
.limit stack 6
.end method

.method private a()Lcom/a/b/g/j;
new com/a/b/g/j
dup
aload 0
getfield com/a/b/g/j/a Lcom/a/b/g/q;
invokevirtual com/a/b/g/q/b()Lcom/a/b/g/q;
aload 0
getfield com/a/b/g/j/b I
aload 0
getfield com/a/b/g/j/c Lcom/a/b/g/w;
aload 0
getfield com/a/b/g/j/d Lcom/a/b/g/m;
invokespecial com/a/b/g/j/<init>(Lcom/a/b/g/q;ILcom/a/b/g/w;Lcom/a/b/g/m;)V
areturn
.limit locals 1
.limit stack 6
.end method

.method private static a(Lcom/a/b/g/w;I)Lcom/a/b/g/j;
aload 0
iload 1
getstatic com/a/b/g/j/e Lcom/a/b/g/m;
invokestatic com/a/b/g/j/a(Lcom/a/b/g/w;ILcom/a/b/g/m;)Lcom/a/b/g/j;
areturn
.limit locals 2
.limit stack 3
.end method

.method private static a(Lcom/a/b/g/w;ILcom/a/b/g/m;)Lcom/a/b/g/j;
.annotation invisible Lcom/a/b/a/d;
.end annotation
.catch java/lang/IllegalArgumentException from L0 to L1 using L2
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
iload 1
iflt L3
iconst_1
istore 4
L4:
iload 4
ldc "Expected insertions (%s) must be >= 0"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
iconst_1
ldc "False positive probability (%s) must be > 0.0"
iconst_1
anewarray java/lang/Object
dup
iconst_0
ldc2_w 0.03D
invokestatic java/lang/Double/valueOf(D)Ljava/lang/Double;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
iconst_1
ldc "False positive probability (%s) must be < 1.0"
iconst_1
anewarray java/lang/Object
dup
iconst_0
ldc2_w 0.03D
invokestatic java/lang/Double/valueOf(D)Ljava/lang/Double;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
iload 1
istore 3
iload 1
ifne L5
iconst_1
istore 3
L5:
iload 3
i2l
lneg
l2d
ldc2_w 0.03D
invokestatic java/lang/Math/log(D)D
dmul
ldc2_w 2.0D
invokestatic java/lang/Math/log(D)D
ldc2_w 2.0D
invokestatic java/lang/Math/log(D)D
dmul
ddiv
d2l
lstore 5
iload 3
i2l
lstore 7
iconst_1
lload 5
l2d
lload 7
l2d
ddiv
ldc2_w 2.0D
invokestatic java/lang/Math/log(D)D
dmul
invokestatic java/lang/Math/round(D)J
l2i
invokestatic java/lang/Math/max(II)I
istore 1
L0:
new com/a/b/g/j
dup
new com/a/b/g/q
dup
lload 5
invokespecial com/a/b/g/q/<init>(J)V
iload 1
aload 0
aload 2
invokespecial com/a/b/g/j/<init>(Lcom/a/b/g/q;ILcom/a/b/g/w;Lcom/a/b/g/m;)V
astore 0
L1:
aload 0
areturn
L3:
iconst_0
istore 4
goto L4
L2:
astore 0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
bipush 57
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Could not create BloomFilter of "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 5
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc " bits"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 0
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
.limit locals 9
.limit stack 7
.end method

.method private static a(Ljava/io/InputStream;Lcom/a/b/g/w;)Lcom/a/b/g/j;
.catch java/lang/RuntimeException from L0 to L1 using L2
.catch java/lang/RuntimeException from L1 to L3 using L4
.catch java/lang/RuntimeException from L5 to L6 using L7
.catch java/lang/RuntimeException from L8 to L9 using L7
.catch java/lang/RuntimeException from L10 to L11 using L7
.catch java/lang/RuntimeException from L12 to L13 using L7
.catch java/lang/RuntimeException from L14 to L15 using L7
iconst_m1
istore 4
aload 0
ldc "InputStream"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
ldc "Funnel"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L0:
new java/io/DataInputStream
dup
aload 0
invokespecial java/io/DataInputStream/<init>(Ljava/io/InputStream;)V
astore 0
aload 0
invokevirtual java/io/DataInputStream/readByte()B
istore 3
L1:
aload 0
invokevirtual java/io/DataInputStream/readByte()B
istore 2
L3:
iload 2
sipush 255
iand
istore 5
iload 4
istore 2
L5:
aload 0
invokevirtual java/io/DataInputStream/readInt()I
istore 6
L6:
iload 6
istore 2
L8:
invokestatic com/a/b/g/n/values()[Lcom/a/b/g/n;
iload 3
aaload
astore 7
L9:
iload 6
istore 2
L10:
iload 6
newarray long
astore 8
L11:
iconst_0
istore 4
L16:
iload 4
iload 6
if_icmpge L17
iload 6
istore 2
L12:
aload 8
iload 4
aload 0
invokevirtual java/io/DataInputStream/readLong()J
lastore
L13:
iload 4
iconst_1
iadd
istore 4
goto L16
L17:
iload 6
istore 2
L14:
new com/a/b/g/j
dup
new com/a/b/g/q
dup
aload 8
invokespecial com/a/b/g/q/<init>([J)V
iload 5
aload 1
aload 7
invokespecial com/a/b/g/j/<init>(Lcom/a/b/g/q;ILcom/a/b/g/w;Lcom/a/b/g/m;)V
astore 0
L15:
aload 0
areturn
L2:
astore 0
iconst_m1
istore 2
iconst_m1
istore 3
L18:
ldc "Unable to deserialize BloomFilter from InputStream. strategyOrdinal: "
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/io/IOException
dup
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 65
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 3
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " numHashFunctions: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " dataLength: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 4
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
astore 1
aload 1
aload 0
invokevirtual java/io/IOException/initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
pop
aload 1
athrow
L4:
astore 0
iconst_m1
istore 2
goto L18
L7:
astore 0
iload 2
istore 4
iload 5
istore 2
goto L18
.limit locals 9
.limit stack 6
.end method

.method static synthetic a(Lcom/a/b/g/j;)Lcom/a/b/g/q;
aload 0
getfield com/a/b/g/j/a Lcom/a/b/g/q;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Ljava/io/OutputStream;)V
new java/io/DataOutputStream
dup
aload 1
invokespecial java/io/DataOutputStream/<init>(Ljava/io/OutputStream;)V
astore 1
aload 0
getfield com/a/b/g/j/d Lcom/a/b/g/m;
invokeinterface com/a/b/g/m/ordinal()I 0
i2l
lstore 4
lload 4
l2i
i2b
istore 2
iload 2
i2l
lload 4
lcmp
ifeq L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
bipush 34
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Out of range: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 4
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
iload 2
invokevirtual java/io/DataOutputStream/writeByte(I)V
aload 0
getfield com/a/b/g/j/b I
i2l
lstore 4
lload 4
bipush 8
lshr
lconst_0
lcmp
ifeq L1
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
bipush 34
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Out of range: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 4
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 1
lload 4
l2i
i2b
invokevirtual java/io/DataOutputStream/writeByte(I)V
aload 1
aload 0
getfield com/a/b/g/j/a Lcom/a/b/g/q;
getfield com/a/b/g/q/a [J
arraylength
invokevirtual java/io/DataOutputStream/writeInt(I)V
aload 0
getfield com/a/b/g/j/a Lcom/a/b/g/q;
getfield com/a/b/g/q/a [J
astore 6
aload 6
arraylength
istore 3
iconst_0
istore 2
L2:
iload 2
iload 3
if_icmpge L3
aload 1
aload 6
iload 2
laload
invokevirtual java/io/DataOutputStream/writeLong(J)V
iload 2
iconst_1
iadd
istore 2
goto L2
L3:
return
.limit locals 7
.limit stack 5
.end method

.method private b()D
aload 0
getfield com/a/b/g/j/a Lcom/a/b/g/q;
getfield com/a/b/g/q/b J
l2d
aload 0
getfield com/a/b/g/j/a Lcom/a/b/g/q;
invokevirtual com/a/b/g/q/a()J
l2d
ddiv
aload 0
getfield com/a/b/g/j/b I
i2d
invokestatic java/lang/Math/pow(DD)D
dreturn
.limit locals 1
.limit stack 4
.end method

.method static synthetic b(Lcom/a/b/g/j;)I
aload 0
getfield com/a/b/g/j/b I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static b(Lcom/a/b/g/w;I)Lcom/a/b/g/j;
aload 0
iload 1
getstatic com/a/b/g/j/e Lcom/a/b/g/m;
invokestatic com/a/b/g/j/a(Lcom/a/b/g/w;ILcom/a/b/g/m;)Lcom/a/b/g/j;
areturn
.limit locals 2
.limit stack 3
.end method

.method private b(Ljava/lang/Object;)Z
aload 0
getfield com/a/b/g/j/d Lcom/a/b/g/m;
aload 1
aload 0
getfield com/a/b/g/j/c Lcom/a/b/g/w;
aload 0
getfield com/a/b/g/j/b I
aload 0
getfield com/a/b/g/j/a Lcom/a/b/g/q;
invokeinterface com/a/b/g/m/b(Ljava/lang/Object;Lcom/a/b/g/w;ILcom/a/b/g/q;)Z 4
ireturn
.limit locals 2
.limit stack 5
.end method

.method private c()J
.annotation invisible Lcom/a/b/a/d;
.end annotation
aload 0
getfield com/a/b/g/j/a Lcom/a/b/g/q;
invokevirtual com/a/b/g/q/a()J
lreturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic c(Lcom/a/b/g/j;)Lcom/a/b/g/w;
aload 0
getfield com/a/b/g/j/c Lcom/a/b/g/w;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c(Ljava/lang/Object;)Z
aload 0
getfield com/a/b/g/j/d Lcom/a/b/g/m;
aload 1
aload 0
getfield com/a/b/g/j/c Lcom/a/b/g/w;
aload 0
getfield com/a/b/g/j/b I
aload 0
getfield com/a/b/g/j/a Lcom/a/b/g/q;
invokeinterface com/a/b/g/m/a(Ljava/lang/Object;Lcom/a/b/g/w;ILcom/a/b/g/q;)Z 4
ireturn
.limit locals 2
.limit stack 5
.end method

.method static synthetic d(Lcom/a/b/g/j;)Lcom/a/b/g/m;
aload 0
getfield com/a/b/g/j/d Lcom/a/b/g/m;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d()Ljava/lang/Object;
new com/a/b/g/l
dup
aload 0
invokespecial com/a/b/g/l/<init>(Lcom/a/b/g/j;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private e(Lcom/a/b/g/j;)Z
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
aload 1
if_acmpeq L0
aload 0
getfield com/a/b/g/j/b I
aload 1
getfield com/a/b/g/j/b I
if_icmpne L0
aload 0
getfield com/a/b/g/j/a Lcom/a/b/g/q;
invokevirtual com/a/b/g/q/a()J
aload 1
getfield com/a/b/g/j/a Lcom/a/b/g/q;
invokevirtual com/a/b/g/q/a()J
lcmp
ifne L0
aload 0
getfield com/a/b/g/j/d Lcom/a/b/g/m;
aload 1
getfield com/a/b/g/j/d Lcom/a/b/g/m;
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L0
aload 0
getfield com/a/b/g/j/c Lcom/a/b/g/w;
aload 1
getfield com/a/b/g/j/c Lcom/a/b/g/w;
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 4
.end method

.method private f(Lcom/a/b/g/j;)V
iconst_0
istore 2
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
aload 1
if_acmpeq L0
iconst_1
istore 3
L1:
iload 3
ldc "Cannot combine a BloomFilter with itself."
invokestatic com/a/b/b/cn/a(ZLjava/lang/Object;)V
aload 0
getfield com/a/b/g/j/b I
aload 1
getfield com/a/b/g/j/b I
if_icmpne L2
iconst_1
istore 3
L3:
iload 3
ldc "BloomFilters must have the same number of hash functions (%s != %s)"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/g/j/b I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
aload 1
getfield com/a/b/g/j/b I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
getfield com/a/b/g/j/a Lcom/a/b/g/q;
invokevirtual com/a/b/g/q/a()J
aload 1
getfield com/a/b/g/j/a Lcom/a/b/g/q;
invokevirtual com/a/b/g/q/a()J
lcmp
ifne L4
iconst_1
istore 3
L5:
iload 3
ldc "BloomFilters must have the same size underlying bit arrays (%s != %s)"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/g/j/a Lcom/a/b/g/q;
invokevirtual com/a/b/g/q/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
dup
iconst_1
aload 1
getfield com/a/b/g/j/a Lcom/a/b/g/q;
invokevirtual com/a/b/g/q/a()J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
getfield com/a/b/g/j/d Lcom/a/b/g/m;
aload 1
getfield com/a/b/g/j/d Lcom/a/b/g/m;
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ldc "BloomFilters must have equal strategies (%s != %s)"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/g/j/d Lcom/a/b/g/m;
aastore
dup
iconst_1
aload 1
getfield com/a/b/g/j/d Lcom/a/b/g/m;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
getfield com/a/b/g/j/c Lcom/a/b/g/w;
aload 1
getfield com/a/b/g/j/c Lcom/a/b/g/w;
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ldc "BloomFilters must have equal funnels (%s != %s)"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/g/j/c Lcom/a/b/g/w;
aastore
dup
iconst_1
aload 1
getfield com/a/b/g/j/c Lcom/a/b/g/w;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
getfield com/a/b/g/j/a Lcom/a/b/g/q;
astore 4
aload 1
getfield com/a/b/g/j/a Lcom/a/b/g/q;
astore 1
aload 4
getfield com/a/b/g/q/a [J
arraylength
aload 1
getfield com/a/b/g/q/a [J
arraylength
if_icmpne L6
iconst_1
istore 3
L7:
iload 3
ldc "BitArrays must be of equal length (%s != %s)"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 4
getfield com/a/b/g/q/a [J
arraylength
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
aload 1
getfield com/a/b/g/q/a [J
arraylength
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 4
lconst_0
putfield com/a/b/g/q/b J
L8:
iload 2
aload 4
getfield com/a/b/g/q/a [J
arraylength
if_icmpge L9
aload 4
getfield com/a/b/g/q/a [J
astore 5
aload 5
iload 2
aload 5
iload 2
laload
aload 1
getfield com/a/b/g/q/a [J
iload 2
laload
lor
lastore
aload 4
aload 4
getfield com/a/b/g/q/b J
aload 4
getfield com/a/b/g/q/a [J
iload 2
laload
invokestatic java/lang/Long/bitCount(J)I
i2l
ladd
putfield com/a/b/g/q/b J
iload 2
iconst_1
iadd
istore 2
goto L8
L0:
iconst_0
istore 3
goto L1
L2:
iconst_0
istore 3
goto L3
L4:
iconst_0
istore 3
goto L5
L6:
iconst_0
istore 3
goto L7
L9:
return
.limit locals 6
.limit stack 7
.end method

.method public final a(Ljava/lang/Object;)Z
.annotation visible Ljava/lang/Deprecated;
.end annotation
aload 0
getfield com/a/b/g/j/d Lcom/a/b/g/m;
aload 1
aload 0
getfield com/a/b/g/j/c Lcom/a/b/g/w;
aload 0
getfield com/a/b/g/j/b I
aload 0
getfield com/a/b/g/j/a Lcom/a/b/g/q;
invokeinterface com/a/b/g/m/b(Ljava/lang/Object;Lcom/a/b/g/w;ILcom/a/b/g/q;)Z 4
ireturn
.limit locals 2
.limit stack 5
.end method

.method public final equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
aload 0
if_acmpne L0
L1:
iconst_1
ireturn
L0:
aload 1
instanceof com/a/b/g/j
ifeq L2
aload 1
checkcast com/a/b/g/j
astore 1
aload 0
getfield com/a/b/g/j/b I
aload 1
getfield com/a/b/g/j/b I
if_icmpne L3
aload 0
getfield com/a/b/g/j/c Lcom/a/b/g/w;
aload 1
getfield com/a/b/g/j/c Lcom/a/b/g/w;
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L3
aload 0
getfield com/a/b/g/j/a Lcom/a/b/g/q;
aload 1
getfield com/a/b/g/j/a Lcom/a/b/g/q;
invokevirtual com/a/b/g/q/equals(Ljava/lang/Object;)Z
ifeq L3
aload 0
getfield com/a/b/g/j/d Lcom/a/b/g/m;
aload 1
getfield com/a/b/g/j/d Lcom/a/b/g/m;
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifne L1
L3:
iconst_0
ireturn
L2:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final hashCode()I
iconst_4
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/g/j/b I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
aload 0
getfield com/a/b/g/j/c Lcom/a/b/g/w;
aastore
dup
iconst_2
aload 0
getfield com/a/b/g/j/d Lcom/a/b/g/m;
aastore
dup
iconst_3
aload 0
getfield com/a/b/g/j/a Lcom/a/b/g/q;
aastore
invokestatic java/util/Arrays/hashCode([Ljava/lang/Object;)I
ireturn
.limit locals 1
.limit stack 4
.end method
