.bytecode 50.0
.class final synchronized com/a/b/g/bk
.super com/a/b/g/i

.field private static final 'a' I = 16


.field private static final 'b' J = -8663945395140668459L


.field private static final 'c' J = 5545529020109919103L


.field private 'd' J

.field private 'e' J

.field private 'f' I

.method <init>(I)V
aload 0
bipush 16
invokespecial com/a/b/g/i/<init>(I)V
aload 0
iload 1
i2l
putfield com/a/b/g/bk/d J
aload 0
iload 1
i2l
putfield com/a/b/g/bk/e J
aload 0
iconst_0
putfield com/a/b/g/bk/f I
return
.limit locals 2
.limit stack 3
.end method

.method private a(JJ)V
aload 0
aload 0
getfield com/a/b/g/bk/d J
lload 1
invokestatic com/a/b/g/bk/d(J)J
lxor
putfield com/a/b/g/bk/d J
aload 0
aload 0
getfield com/a/b/g/bk/d J
bipush 27
invokestatic java/lang/Long/rotateLeft(JI)J
putfield com/a/b/g/bk/d J
aload 0
aload 0
getfield com/a/b/g/bk/d J
aload 0
getfield com/a/b/g/bk/e J
ladd
putfield com/a/b/g/bk/d J
aload 0
aload 0
getfield com/a/b/g/bk/d J
ldc2_w 5L
lmul
ldc2_w 1390208809L
ladd
putfield com/a/b/g/bk/d J
aload 0
aload 0
getfield com/a/b/g/bk/e J
lload 3
invokestatic com/a/b/g/bk/e(J)J
lxor
putfield com/a/b/g/bk/e J
aload 0
aload 0
getfield com/a/b/g/bk/e J
bipush 31
invokestatic java/lang/Long/rotateLeft(JI)J
putfield com/a/b/g/bk/e J
aload 0
aload 0
getfield com/a/b/g/bk/e J
aload 0
getfield com/a/b/g/bk/d J
ladd
putfield com/a/b/g/bk/e J
aload 0
aload 0
getfield com/a/b/g/bk/e J
ldc2_w 5L
lmul
ldc2_w 944331445L
ladd
putfield com/a/b/g/bk/e J
return
.limit locals 5
.limit stack 5
.end method

.method private static c(J)J
lload 0
bipush 33
lushr
lload 0
lxor
ldc2_w -49064778989728563L
lmul
lstore 0
lload 0
lload 0
bipush 33
lushr
lxor
ldc2_w -4265267296055464877L
lmul
lstore 0
lload 0
lload 0
bipush 33
lushr
lxor
lreturn
.limit locals 2
.limit stack 5
.end method

.method private static d(J)J
ldc2_w -8663945395140668459L
lload 0
lmul
bipush 31
invokestatic java/lang/Long/rotateLeft(JI)J
ldc2_w 5545529020109919103L
lmul
lreturn
.limit locals 2
.limit stack 4
.end method

.method private static e(J)J
ldc2_w 5545529020109919103L
lload 0
lmul
bipush 33
invokestatic java/lang/Long/rotateLeft(JI)J
ldc2_w -8663945395140668459L
lmul
lreturn
.limit locals 2
.limit stack 4
.end method

.method protected final a(Ljava/nio/ByteBuffer;)V
aload 1
invokevirtual java/nio/ByteBuffer/getLong()J
lstore 2
aload 1
invokevirtual java/nio/ByteBuffer/getLong()J
lstore 4
aload 0
getfield com/a/b/g/bk/d J
lstore 6
aload 0
lload 2
invokestatic com/a/b/g/bk/d(J)J
lload 6
lxor
putfield com/a/b/g/bk/d J
aload 0
aload 0
getfield com/a/b/g/bk/d J
bipush 27
invokestatic java/lang/Long/rotateLeft(JI)J
putfield com/a/b/g/bk/d J
aload 0
aload 0
getfield com/a/b/g/bk/d J
aload 0
getfield com/a/b/g/bk/e J
ladd
putfield com/a/b/g/bk/d J
aload 0
aload 0
getfield com/a/b/g/bk/d J
ldc2_w 5L
lmul
ldc2_w 1390208809L
ladd
putfield com/a/b/g/bk/d J
aload 0
aload 0
getfield com/a/b/g/bk/e J
lload 4
invokestatic com/a/b/g/bk/e(J)J
lxor
putfield com/a/b/g/bk/e J
aload 0
aload 0
getfield com/a/b/g/bk/e J
bipush 31
invokestatic java/lang/Long/rotateLeft(JI)J
putfield com/a/b/g/bk/e J
aload 0
aload 0
getfield com/a/b/g/bk/e J
aload 0
getfield com/a/b/g/bk/d J
ladd
putfield com/a/b/g/bk/e J
aload 0
aload 0
getfield com/a/b/g/bk/e J
ldc2_w 5L
lmul
ldc2_w 944331445L
ladd
putfield com/a/b/g/bk/e J
aload 0
aload 0
getfield com/a/b/g/bk/f I
bipush 16
iadd
putfield com/a/b/g/bk/f I
return
.limit locals 8
.limit stack 5
.end method

.method public final b()Lcom/a/b/g/ag;
aload 0
aload 0
getfield com/a/b/g/bk/d J
aload 0
getfield com/a/b/g/bk/f I
i2l
lxor
putfield com/a/b/g/bk/d J
aload 0
aload 0
getfield com/a/b/g/bk/e J
aload 0
getfield com/a/b/g/bk/f I
i2l
lxor
putfield com/a/b/g/bk/e J
aload 0
aload 0
getfield com/a/b/g/bk/d J
aload 0
getfield com/a/b/g/bk/e J
ladd
putfield com/a/b/g/bk/d J
aload 0
aload 0
getfield com/a/b/g/bk/e J
aload 0
getfield com/a/b/g/bk/d J
ladd
putfield com/a/b/g/bk/e J
aload 0
aload 0
getfield com/a/b/g/bk/d J
invokestatic com/a/b/g/bk/c(J)J
putfield com/a/b/g/bk/d J
aload 0
aload 0
getfield com/a/b/g/bk/e J
invokestatic com/a/b/g/bk/c(J)J
putfield com/a/b/g/bk/e J
aload 0
aload 0
getfield com/a/b/g/bk/d J
aload 0
getfield com/a/b/g/bk/e J
ladd
putfield com/a/b/g/bk/d J
aload 0
aload 0
getfield com/a/b/g/bk/e J
aload 0
getfield com/a/b/g/bk/d J
ladd
putfield com/a/b/g/bk/e J
bipush 16
newarray byte
invokestatic java/nio/ByteBuffer/wrap([B)Ljava/nio/ByteBuffer;
getstatic java/nio/ByteOrder/LITTLE_ENDIAN Ljava/nio/ByteOrder;
invokevirtual java/nio/ByteBuffer/order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
aload 0
getfield com/a/b/g/bk/d J
invokevirtual java/nio/ByteBuffer/putLong(J)Ljava/nio/ByteBuffer;
aload 0
getfield com/a/b/g/bk/e J
invokevirtual java/nio/ByteBuffer/putLong(J)Ljava/nio/ByteBuffer;
invokevirtual java/nio/ByteBuffer/array()[B
invokestatic com/a/b/g/ag/a([B)Lcom/a/b/g/ag;
areturn
.limit locals 1
.limit stack 5
.end method

.method protected final b(Ljava/nio/ByteBuffer;)V
aload 0
aload 0
getfield com/a/b/g/bk/f I
aload 1
invokevirtual java/nio/ByteBuffer/remaining()I
iadd
putfield com/a/b/g/bk/f I
aload 1
invokevirtual java/nio/ByteBuffer/remaining()I
tableswitch 1
L0
L1
L2
L3
L4
L5
L6
L7
L8
L9
L10
L11
L12
L13
L14
default : L15
L15:
new java/lang/AssertionError
dup
ldc "Should never get here."
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
L14:
aload 1
bipush 14
invokevirtual java/nio/ByteBuffer/get(I)B
sipush 255
iand
i2l
bipush 48
lshl
lconst_0
lxor
lstore 2
L16:
lload 2
aload 1
bipush 13
invokevirtual java/nio/ByteBuffer/get(I)B
sipush 255
iand
i2l
bipush 40
lshl
lxor
lstore 2
L17:
lload 2
aload 1
bipush 12
invokevirtual java/nio/ByteBuffer/get(I)B
sipush 255
iand
i2l
bipush 32
lshl
lxor
lstore 2
L18:
lload 2
aload 1
bipush 11
invokevirtual java/nio/ByteBuffer/get(I)B
sipush 255
iand
i2l
bipush 24
lshl
lxor
lstore 2
L19:
lload 2
aload 1
bipush 10
invokevirtual java/nio/ByteBuffer/get(I)B
sipush 255
iand
i2l
bipush 16
lshl
lxor
lstore 2
L20:
lload 2
aload 1
bipush 9
invokevirtual java/nio/ByteBuffer/get(I)B
sipush 255
iand
i2l
bipush 8
lshl
lxor
lstore 2
L21:
lload 2
aload 1
bipush 8
invokevirtual java/nio/ByteBuffer/get(I)B
sipush 255
iand
i2l
lxor
lstore 2
L22:
lconst_0
aload 1
invokevirtual java/nio/ByteBuffer/getLong()J
lxor
lstore 4
L23:
aload 0
getfield com/a/b/g/bk/d J
lstore 6
aload 0
lload 4
invokestatic com/a/b/g/bk/d(J)J
lload 6
lxor
putfield com/a/b/g/bk/d J
aload 0
getfield com/a/b/g/bk/e J
lstore 4
aload 0
lload 2
invokestatic com/a/b/g/bk/e(J)J
lload 4
lxor
putfield com/a/b/g/bk/e J
return
L6:
aload 1
bipush 6
invokevirtual java/nio/ByteBuffer/get(I)B
sipush 255
iand
i2l
bipush 48
lshl
lconst_0
lxor
lstore 2
L24:
lload 2
aload 1
iconst_5
invokevirtual java/nio/ByteBuffer/get(I)B
sipush 255
iand
i2l
bipush 40
lshl
lxor
lstore 2
L25:
lload 2
aload 1
iconst_4
invokevirtual java/nio/ByteBuffer/get(I)B
sipush 255
iand
i2l
bipush 32
lshl
lxor
lstore 2
L26:
lload 2
aload 1
iconst_3
invokevirtual java/nio/ByteBuffer/get(I)B
sipush 255
iand
i2l
bipush 24
lshl
lxor
lstore 2
L27:
lload 2
aload 1
iconst_2
invokevirtual java/nio/ByteBuffer/get(I)B
sipush 255
iand
i2l
bipush 16
lshl
lxor
lstore 2
L28:
lload 2
aload 1
iconst_1
invokevirtual java/nio/ByteBuffer/get(I)B
sipush 255
iand
i2l
bipush 8
lshl
lxor
lstore 2
L29:
lload 2
aload 1
iconst_0
invokevirtual java/nio/ByteBuffer/get(I)B
sipush 255
iand
i2l
lxor
lstore 4
lconst_0
lstore 2
goto L23
L13:
lconst_0
lstore 2
goto L16
L12:
lconst_0
lstore 2
goto L17
L11:
lconst_0
lstore 2
goto L18
L10:
lconst_0
lstore 2
goto L19
L9:
lconst_0
lstore 2
goto L20
L8:
lconst_0
lstore 2
goto L21
L7:
lconst_0
lstore 2
goto L22
L5:
lconst_0
lstore 2
goto L24
L4:
lconst_0
lstore 2
goto L25
L3:
lconst_0
lstore 2
goto L26
L2:
lconst_0
lstore 2
goto L27
L1:
lconst_0
lstore 2
goto L28
L0:
lconst_0
lstore 2
goto L29
.limit locals 8
.limit stack 5
.end method
