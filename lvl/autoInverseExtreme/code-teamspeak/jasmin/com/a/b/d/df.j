.bytecode 50.0
.class final synchronized com/a/b/d/df
.super java/lang/Object
.implements com/a/b/d/sr

.field final 'a' Lcom/a/b/b/bj;

.field volatile 'b' Lcom/a/b/d/sr;
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "ComputingValueReference.this"
.end annotation
.end field

.method public <init>(Lcom/a/b/b/bj;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
invokestatic com/a/b/d/qy/g()Lcom/a/b/d/sr;
putfield com/a/b/d/df/b Lcom/a/b/d/sr;
aload 0
aload 1
putfield com/a/b/d/df/a Lcom/a/b/b/bj;
return
.limit locals 2
.limit stack 2
.end method

.method private b(Lcom/a/b/d/sr;)V
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
aload 0
monitorenter
L0:
aload 0
getfield com/a/b/d/df/b Lcom/a/b/d/sr;
getstatic com/a/b/d/qy/x Lcom/a/b/d/sr;
if_acmpne L1
aload 0
aload 1
putfield com/a/b/d/df/b Lcom/a/b/d/sr;
aload 0
invokevirtual java/lang/Object/notifyAll()V
L1:
aload 0
monitorexit
L3:
return
L2:
astore 1
L4:
aload 0
monitorexit
L5:
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method public final a()Lcom/a/b/d/rz;
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;Lcom/a/b/d/rz;)Lcom/a/b/d/sr;
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
areturn
.limit locals 4
.limit stack 1
.end method

.method final a(Ljava/lang/Object;)Ljava/lang/Object;
.catch java/lang/Throwable from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/d/df/a Lcom/a/b/b/bj;
aload 1
invokeinterface com/a/b/b/bj/e(Ljava/lang/Object;)Ljava/lang/Object; 1
astore 1
L1:
aload 0
new com/a/b/d/dc
dup
aload 1
invokespecial com/a/b/d/dc/<init>(Ljava/lang/Object;)V
invokespecial com/a/b/d/df/b(Lcom/a/b/d/sr;)V
aload 1
areturn
L2:
astore 1
aload 0
new com/a/b/d/db
dup
aload 1
invokespecial com/a/b/d/db/<init>(Ljava/lang/Throwable;)V
invokespecial com/a/b/d/df/b(Lcom/a/b/d/sr;)V
new java/util/concurrent/ExecutionException
dup
aload 1
invokespecial java/util/concurrent/ExecutionException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 2
.limit stack 4
.end method

.method public final a(Lcom/a/b/d/sr;)V
aload 0
aload 1
invokespecial com/a/b/d/df/b(Lcom/a/b/d/sr;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final b()Z
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final c()Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L5
.catch java/lang/InterruptedException from L6 to L7 using L8
.catch all from L6 to L7 using L5
.catch all from L9 to L10 using L5
.catch all from L11 to L12 using L5
.catch all from L12 to L2 using L2
aload 0
getfield com/a/b/d/df/b Lcom/a/b/d/sr;
getstatic com/a/b/d/qy/x Lcom/a/b/d/sr;
if_acmpne L13
iconst_0
istore 1
iconst_0
istore 2
L0:
aload 0
monitorenter
L1:
iload 2
istore 1
L3:
aload 0
getfield com/a/b/d/df/b Lcom/a/b/d/sr;
astore 3
getstatic com/a/b/d/qy/x Lcom/a/b/d/sr;
astore 4
L4:
aload 3
aload 4
if_acmpne L9
L6:
aload 0
invokevirtual java/lang/Object/wait()V
L7:
goto L3
L8:
astore 3
iconst_1
istore 1
goto L3
L9:
aload 0
monitorexit
L10:
iload 1
ifeq L13
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
L13:
aload 0
getfield com/a/b/d/df/b Lcom/a/b/d/sr;
invokeinterface com/a/b/d/sr/c()Ljava/lang/Object; 0
areturn
L5:
astore 3
L11:
aload 0
monitorexit
L12:
aload 3
athrow
L2:
astore 3
iload 1
ifeq L14
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
L14:
aload 3
athrow
.limit locals 5
.limit stack 2
.end method

.method public final get()Ljava/lang/Object;
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method
