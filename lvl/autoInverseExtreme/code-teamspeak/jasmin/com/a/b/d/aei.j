.bytecode 50.0
.class synchronized com/a/b/d/aei
.super com/a/b/d/hr
.implements java/io/Serializable

.field private static final 'b' J = 0L


.field final 'a' Lcom/a/b/d/adv;

.method <init>(Lcom/a/b/d/adv;)V
aload 0
invokespecial com/a/b/d/hr/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/d/adv
putfield com/a/b/d/aei/a Lcom/a/b/d/adv;
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 3 Ljavax/annotation/Nullable;
.end annotation
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 4
.limit stack 2
.end method

.method public a()Ljava/util/Set;
aload 0
invokespecial com/a/b/d/hr/a()Ljava/util/Set;
invokestatic java/util/Collections/unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Lcom/a/b/d/adv;)V
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public final b()Ljava/util/Set;
aload 0
invokespecial com/a/b/d/hr/b()Ljava/util/Set;
invokestatic java/util/Collections/unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 3
.limit stack 2
.end method

.method public final d(Ljava/lang/Object;)Ljava/util/Map;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokespecial com/a/b/d/hr/d(Ljava/lang/Object;)Ljava/util/Map;
invokestatic java/util/Collections/unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final d()V
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 1
.limit stack 2
.end method

.method public final e(Ljava/lang/Object;)Ljava/util/Map;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokespecial com/a/b/d/hr/e(Ljava/lang/Object;)Ljava/util/Map;
invokestatic java/util/Collections/unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final e()Ljava/util/Set;
aload 0
invokespecial com/a/b/d/hr/e()Ljava/util/Set;
invokestatic java/util/Collections/unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected f()Lcom/a/b/d/adv;
aload 0
getfield com/a/b/d/aei/a Lcom/a/b/d/adv;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final h()Ljava/util/Collection;
aload 0
invokespecial com/a/b/d/hr/h()Ljava/util/Collection;
invokestatic java/util/Collections/unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected synthetic k_()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/aei/f()Lcom/a/b/d/adv;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final l()Ljava/util/Map;
invokestatic com/a/b/d/adx/a()Lcom/a/b/b/bj;
astore 1
aload 0
invokespecial com/a/b/d/hr/l()Ljava/util/Map;
aload 1
invokestatic com/a/b/d/sz/a(Ljava/util/Map;Lcom/a/b/b/bj;)Ljava/util/Map;
invokestatic java/util/Collections/unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;
areturn
.limit locals 2
.limit stack 2
.end method

.method public m()Ljava/util/Map;
invokestatic com/a/b/d/adx/a()Lcom/a/b/b/bj;
astore 1
aload 0
invokespecial com/a/b/d/hr/m()Ljava/util/Map;
aload 1
invokestatic com/a/b/d/sz/a(Ljava/util/Map;Lcom/a/b/b/bj;)Ljava/util/Map;
invokestatic java/util/Collections/unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;
areturn
.limit locals 2
.limit stack 2
.end method
