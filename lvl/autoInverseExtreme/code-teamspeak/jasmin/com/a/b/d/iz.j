.bytecode 50.0
.class public synchronized abstract com/a/b/d/iz
.super java/util/AbstractCollection
.implements java/io/Serializable
.annotation invisible Lcom/a/b/a/b;
b Z = 1
.end annotation

.field private transient 'a' Lcom/a/b/d/jl;

.method <init>()V
aload 0
invokespecial java/util/AbstractCollection/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method a([Ljava/lang/Object;I)I
aload 0
invokevirtual com/a/b/d/iz/iterator()Ljava/util/Iterator;
astore 3
L0:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
iload 2
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
aastore
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
iload 2
ireturn
.limit locals 4
.limit stack 3
.end method

.method public final add(Ljava/lang/Object;)Z
.annotation visible Ljava/lang/Deprecated;
.end annotation
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public final addAll(Ljava/util/Collection;)Z
.annotation visible Ljava/lang/Deprecated;
.end annotation
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public abstract c()Lcom/a/b/d/agi;
.end method

.method public final clear()V
.annotation visible Ljava/lang/Deprecated;
.end annotation
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 1
.limit stack 2
.end method

.method public contains(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
ifnull L0
aload 0
aload 1
invokespecial java/util/AbstractCollection/contains(Ljava/lang/Object;)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public f()Lcom/a/b/d/jl;
aload 0
getfield com/a/b/d/iz/a Lcom/a/b/d/jl;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
aload 0
invokevirtual com/a/b/d/iz/l()Lcom/a/b/d/jl;
astore 1
aload 0
aload 1
putfield com/a/b/d/iz/a Lcom/a/b/d/jl;
L0:
aload 1
areturn
.limit locals 3
.limit stack 2
.end method

.method g()Ljava/lang/Object;
new com/a/b/d/jp
dup
aload 0
invokevirtual com/a/b/d/iz/toArray()[Ljava/lang/Object;
invokespecial com/a/b/d/jp/<init>([Ljava/lang/Object;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method abstract h_()Z
.end method

.method public synthetic iterator()Ljava/util/Iterator;
aload 0
invokevirtual com/a/b/d/iz/c()Lcom/a/b/d/agi;
areturn
.limit locals 1
.limit stack 1
.end method

.method l()Lcom/a/b/d/jl;
aload 0
invokevirtual com/a/b/d/iz/size()I
tableswitch 0
L0
L1
default : L2
L2:
new com/a/b/d/yw
dup
aload 0
aload 0
invokevirtual com/a/b/d/iz/toArray()[Ljava/lang/Object;
invokespecial com/a/b/d/yw/<init>(Lcom/a/b/d/iz;[Ljava/lang/Object;)V
areturn
L0:
invokestatic com/a/b/d/jl/d()Lcom/a/b/d/jl;
areturn
L1:
aload 0
invokevirtual com/a/b/d/iz/c()Lcom/a/b/d/agi;
invokevirtual com/a/b/d/agi/next()Ljava/lang/Object;
invokestatic com/a/b/d/jl/a(Ljava/lang/Object;)Lcom/a/b/d/jl;
areturn
.limit locals 1
.limit stack 4
.end method

.method public final remove(Ljava/lang/Object;)Z
.annotation visible Ljava/lang/Deprecated;
.end annotation
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public final removeAll(Ljava/util/Collection;)Z
.annotation visible Ljava/lang/Deprecated;
.end annotation
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public final retainAll(Ljava/util/Collection;)Z
.annotation visible Ljava/lang/Deprecated;
.end annotation
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public final toArray()[Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/iz/size()I
istore 1
iload 1
ifne L0
getstatic com/a/b/d/yc/a [Ljava/lang/Object;
areturn
L0:
iload 1
anewarray java/lang/Object
astore 2
aload 0
aload 2
iconst_0
invokevirtual com/a/b/d/iz/a([Ljava/lang/Object;I)I
pop
aload 2
areturn
.limit locals 3
.limit stack 3
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
invokevirtual com/a/b/d/iz/size()I
istore 2
aload 1
arraylength
iload 2
if_icmpge L0
aload 1
iload 2
invokestatic com/a/b/d/yc/a([Ljava/lang/Object;I)[Ljava/lang/Object;
astore 3
L1:
aload 0
aload 3
iconst_0
invokevirtual com/a/b/d/iz/a([Ljava/lang/Object;I)I
pop
aload 3
areturn
L0:
aload 1
astore 3
aload 1
arraylength
iload 2
if_icmple L1
aload 1
iload 2
aconst_null
aastore
aload 1
astore 3
goto L1
.limit locals 4
.limit stack 3
.end method
