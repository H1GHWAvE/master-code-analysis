.bytecode 50.0
.class public final synchronized com/a/b/d/vc
.super java/util/AbstractQueue
.annotation invisible Lcom/a/b/a/a;
.end annotation

.field private static final 'g' I = 1431655765


.field private static final 'h' I = -1431655766


.field private static final 'i' I = 11


.field final 'a' I
.annotation invisible Lcom/a/b/a/d;
.end annotation
.end field

.field 'b' [Ljava/lang/Object;

.field private final 'c' Lcom/a/b/d/vf;

.field private final 'd' Lcom/a/b/d/vf;

.field private 'e' I

.field private 'f' I

.method private <init>(Lcom/a/b/d/ve;I)V
aload 0
invokespecial java/util/AbstractQueue/<init>()V
aload 1
getfield com/a/b/d/ve/a Ljava/util/Comparator;
invokestatic com/a/b/d/yd/a(Ljava/util/Comparator;)Lcom/a/b/d/yd;
astore 3
aload 0
new com/a/b/d/vf
dup
aload 0
aload 3
invokespecial com/a/b/d/vf/<init>(Lcom/a/b/d/vc;Lcom/a/b/d/yd;)V
putfield com/a/b/d/vc/c Lcom/a/b/d/vf;
aload 0
new com/a/b/d/vf
dup
aload 0
aload 3
invokevirtual com/a/b/d/yd/a()Lcom/a/b/d/yd;
invokespecial com/a/b/d/vf/<init>(Lcom/a/b/d/vc;Lcom/a/b/d/yd;)V
putfield com/a/b/d/vc/d Lcom/a/b/d/vf;
aload 0
getfield com/a/b/d/vc/c Lcom/a/b/d/vf;
aload 0
getfield com/a/b/d/vc/d Lcom/a/b/d/vf;
putfield com/a/b/d/vf/b Lcom/a/b/d/vf;
aload 0
getfield com/a/b/d/vc/d Lcom/a/b/d/vf;
aload 0
getfield com/a/b/d/vc/c Lcom/a/b/d/vf;
putfield com/a/b/d/vf/b Lcom/a/b/d/vf;
aload 0
aload 1
getfield com/a/b/d/ve/c I
putfield com/a/b/d/vc/a I
aload 0
iload 2
anewarray java/lang/Object
putfield com/a/b/d/vc/b [Ljava/lang/Object;
return
.limit locals 4
.limit stack 5
.end method

.method synthetic <init>(Lcom/a/b/d/ve;IB)V
aload 0
aload 1
iload 2
invokespecial com/a/b/d/vc/<init>(Lcom/a/b/d/ve;I)V
return
.limit locals 4
.limit stack 3
.end method

.method private static a(II)I
iload 0
iconst_1
isub
iload 1
invokestatic java/lang/Math/min(II)I
iconst_1
iadd
ireturn
.limit locals 2
.limit stack 2
.end method

.method static a(IILjava/lang/Iterable;)I
.annotation invisible Lcom/a/b/a/d;
.end annotation
iload 0
istore 3
iload 0
iconst_m1
if_icmpne L0
bipush 11
istore 3
L0:
iload 3
istore 0
aload 2
instanceof java/util/Collection
ifeq L1
iload 3
aload 2
checkcast java/util/Collection
invokeinterface java/util/Collection/size()I 0
invokestatic java/lang/Math/max(II)I
istore 0
L1:
iload 0
iload 1
invokestatic com/a/b/d/vc/a(II)I
ireturn
.limit locals 4
.limit stack 2
.end method

.method private static a()Lcom/a/b/d/vc;
new com/a/b/d/ve
dup
invokestatic com/a/b/d/yd/d()Lcom/a/b/d/yd;
iconst_0
invokespecial com/a/b/d/ve/<init>(Ljava/util/Comparator;B)V
invokestatic java/util/Collections/emptySet()Ljava/util/Set;
invokevirtual com/a/b/d/ve/a(Ljava/lang/Iterable;)Lcom/a/b/d/vc;
areturn
.limit locals 0
.limit stack 4
.end method

.method private static a(Ljava/lang/Iterable;)Lcom/a/b/d/vc;
new com/a/b/d/ve
dup
invokestatic com/a/b/d/yd/d()Lcom/a/b/d/yd;
iconst_0
invokespecial com/a/b/d/ve/<init>(Ljava/util/Comparator;B)V
aload 0
invokevirtual com/a/b/d/ve/a(Ljava/lang/Iterable;)Lcom/a/b/d/vc;
areturn
.limit locals 1
.limit stack 4
.end method

.method private static a(Ljava/util/Comparator;)Lcom/a/b/d/ve;
new com/a/b/d/ve
dup
aload 0
iconst_0
invokespecial com/a/b/d/ve/<init>(Ljava/util/Comparator;B)V
areturn
.limit locals 1
.limit stack 4
.end method

.method private a(ILjava/lang/Object;)Lcom/a/b/d/vg;
aload 0
iload 1
invokespecial com/a/b/d/vc/f(I)Lcom/a/b/d/vf;
astore 6
iload 1
istore 4
L0:
iload 4
iconst_2
imul
iconst_1
iadd
istore 3
iload 3
ifge L1
iconst_m1
istore 3
L2:
iload 3
ifle L3
aload 6
getfield com/a/b/d/vf/c Lcom/a/b/d/vc;
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iload 4
aload 6
getfield com/a/b/d/vf/c Lcom/a/b/d/vc;
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iload 3
aaload
aastore
iload 3
istore 4
goto L0
L1:
aload 6
iload 3
iconst_2
imul
iconst_1
iadd
iconst_4
invokevirtual com/a/b/d/vf/b(II)I
istore 3
goto L2
L3:
aload 6
iload 4
aload 2
invokevirtual com/a/b/d/vf/a(ILjava/lang/Object;)I
istore 3
iload 3
iload 4
if_icmpne L4
aload 6
iload 4
iconst_2
imul
iconst_1
iadd
iconst_2
invokevirtual com/a/b/d/vf/b(II)I
istore 3
iload 3
ifle L5
aload 6
getfield com/a/b/d/vf/a Lcom/a/b/d/yd;
aload 6
getfield com/a/b/d/vf/c Lcom/a/b/d/vc;
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iload 3
aaload
aload 2
invokevirtual com/a/b/d/yd/compare(Ljava/lang/Object;Ljava/lang/Object;)I
ifge L5
aload 6
getfield com/a/b/d/vf/c Lcom/a/b/d/vc;
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iload 4
aload 6
getfield com/a/b/d/vf/c Lcom/a/b/d/vc;
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iload 3
aaload
aastore
aload 6
getfield com/a/b/d/vf/c Lcom/a/b/d/vc;
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iload 3
aload 2
aastore
L6:
iload 3
iload 4
if_icmpeq L7
iload 3
iload 1
if_icmpge L8
aload 6
getfield com/a/b/d/vf/c Lcom/a/b/d/vc;
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iload 1
aaload
astore 5
L9:
aload 6
getfield com/a/b/d/vf/b Lcom/a/b/d/vf;
iload 3
aload 2
invokevirtual com/a/b/d/vf/a(ILjava/lang/Object;)I
iload 1
if_icmpge L7
new com/a/b/d/vg
dup
aload 2
aload 5
invokespecial com/a/b/d/vg/<init>(Ljava/lang/Object;Ljava/lang/Object;)V
areturn
L5:
aload 6
iload 4
aload 2
invokevirtual com/a/b/d/vf/b(ILjava/lang/Object;)I
istore 3
goto L6
L8:
aload 6
getfield com/a/b/d/vf/c Lcom/a/b/d/vc;
astore 5
iload 1
iconst_1
isub
iconst_2
idiv
istore 4
aload 5
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iload 4
aaload
astore 5
goto L9
L7:
aconst_null
areturn
L4:
iload 3
iload 1
if_icmpge L10
new com/a/b/d/vg
dup
aload 2
aload 0
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iload 1
aaload
invokespecial com/a/b/d/vg/<init>(Ljava/lang/Object;Ljava/lang/Object;)V
areturn
L10:
aconst_null
areturn
.limit locals 7
.limit stack 5
.end method

.method static synthetic a(Lcom/a/b/d/vc;)[Ljava/lang/Object;
aload 0
getfield com/a/b/d/vc/b [Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b()I
aload 0
getfield com/a/b/d/vc/e I
tableswitch 1
L0
L1
default : L2
L2:
aload 0
getfield com/a/b/d/vc/d Lcom/a/b/d/vf;
iconst_1
iconst_2
invokevirtual com/a/b/d/vf/a(II)I
ifgt L3
L1:
iconst_1
ireturn
L0:
iconst_0
ireturn
L3:
iconst_2
ireturn
.limit locals 1
.limit stack 3
.end method

.method static synthetic b(Lcom/a/b/d/vc;)I
aload 0
getfield com/a/b/d/vc/e I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static b(I)Lcom/a/b/d/ve;
iconst_0
istore 1
new com/a/b/d/ve
dup
invokestatic com/a/b/d/yd/d()Lcom/a/b/d/yd;
iconst_0
invokespecial com/a/b/d/ve/<init>(Ljava/util/Comparator;B)V
astore 2
iload 0
iflt L0
iconst_1
istore 1
L0:
iload 1
invokestatic com/a/b/b/cn/a(Z)V
aload 2
iload 0
putfield com/a/b/d/ve/b I
aload 2
areturn
.limit locals 3
.limit stack 4
.end method

.method static synthetic c(Lcom/a/b/d/vc;)I
aload 0
getfield com/a/b/d/vc/f I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static c(I)Lcom/a/b/d/ve;
iconst_0
istore 1
new com/a/b/d/ve
dup
invokestatic com/a/b/d/yd/d()Lcom/a/b/d/yd;
iconst_0
invokespecial com/a/b/d/ve/<init>(Ljava/util/Comparator;B)V
astore 2
iload 0
ifle L0
iconst_1
istore 1
L0:
iload 1
invokestatic com/a/b/b/cn/a(Z)V
aload 2
iload 0
putfield com/a/b/d/ve/c I
aload 2
areturn
.limit locals 3
.limit stack 4
.end method

.method private c()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/vc/poll()Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/vc/remove()Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d(I)Ljava/lang/Object;
aload 0
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iload 1
aaload
areturn
.limit locals 2
.limit stack 2
.end method

.method private e()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/vc/peek()Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e(I)Ljava/lang/Object;
aload 0
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iload 1
aaload
astore 2
aload 0
iload 1
invokevirtual com/a/b/d/vc/a(I)Lcom/a/b/d/vg;
pop
aload 2
areturn
.limit locals 3
.limit stack 2
.end method

.method private f(I)Lcom/a/b/d/vf;
iconst_1
istore 2
iload 1
iconst_1
iadd
istore 1
iload 1
ifle L0
iconst_1
istore 3
L1:
iload 3
ldc "negative index"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
ldc_w 1431655765
iload 1
iand
iload 1
ldc_w -1431655766
iand
if_icmple L2
iload 2
istore 1
L3:
iload 1
ifeq L4
aload 0
getfield com/a/b/d/vc/c Lcom/a/b/d/vf;
areturn
L0:
iconst_0
istore 3
goto L1
L2:
iconst_0
istore 1
goto L3
L4:
aload 0
getfield com/a/b/d/vc/d Lcom/a/b/d/vf;
areturn
.limit locals 4
.limit stack 3
.end method

.method private f()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/vc/isEmpty()Z
ifeq L0
aconst_null
areturn
L0:
aload 0
aload 0
invokespecial com/a/b/d/vc/b()I
invokespecial com/a/b/d/vc/e(I)Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 2
.end method

.method private g()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/vc/isEmpty()Z
ifeq L0
new java/util/NoSuchElementException
dup
invokespecial java/util/NoSuchElementException/<init>()V
athrow
L0:
aload 0
aload 0
invokespecial com/a/b/d/vc/b()I
invokespecial com/a/b/d/vc/e(I)Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 2
.end method

.method private static g(I)Z
.annotation invisible Lcom/a/b/a/d;
.end annotation
iload 0
iconst_1
iadd
istore 0
iload 0
ifle L0
iconst_1
istore 1
L1:
iload 1
ldc "negative index"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
ldc_w 1431655765
iload 0
iand
iload 0
ldc_w -1431655766
iand
if_icmple L2
iconst_1
ireturn
L0:
iconst_0
istore 1
goto L1
L2:
iconst_0
ireturn
.limit locals 2
.limit stack 3
.end method

.method private h()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/vc/isEmpty()Z
ifeq L0
aconst_null
areturn
L0:
aload 0
invokespecial com/a/b/d/vc/b()I
istore 1
aload 0
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iload 1
aaload
areturn
.limit locals 2
.limit stack 2
.end method

.method private i()Z
.annotation invisible Lcom/a/b/a/d;
.end annotation
iconst_1
istore 4
iconst_1
istore 2
L0:
iload 4
istore 3
iload 2
aload 0
getfield com/a/b/d/vc/e I
if_icmpge L1
aload 0
iload 2
invokespecial com/a/b/d/vc/f(I)Lcom/a/b/d/vf;
astore 5
iload 2
iconst_2
imul
iconst_1
iadd
aload 5
getfield com/a/b/d/vf/c Lcom/a/b/d/vc;
getfield com/a/b/d/vc/e I
if_icmpge L2
aload 5
iload 2
iload 2
iconst_2
imul
iconst_1
iadd
invokevirtual com/a/b/d/vf/a(II)I
ifle L2
iconst_0
istore 1
L3:
iload 1
ifne L4
iconst_0
istore 3
L1:
iload 3
ireturn
L2:
iload 2
iconst_2
imul
iconst_2
iadd
aload 5
getfield com/a/b/d/vf/c Lcom/a/b/d/vc;
getfield com/a/b/d/vc/e I
if_icmpge L5
aload 5
iload 2
iload 2
iconst_2
imul
iconst_2
iadd
invokevirtual com/a/b/d/vf/a(II)I
ifle L5
iconst_0
istore 1
goto L3
L5:
iload 2
ifle L6
aload 5
iload 2
iload 2
iconst_1
isub
iconst_2
idiv
invokevirtual com/a/b/d/vf/a(II)I
ifle L6
iconst_0
istore 1
goto L3
L6:
iload 2
iconst_2
if_icmple L7
aload 5
iload 2
iconst_1
isub
iconst_2
idiv
iconst_1
isub
iconst_2
idiv
iload 2
invokevirtual com/a/b/d/vf/a(II)I
ifle L7
iconst_0
istore 1
goto L3
L7:
iconst_1
istore 1
goto L3
L4:
iload 2
iconst_1
iadd
istore 2
goto L0
.limit locals 6
.limit stack 4
.end method

.method private j()Ljava/util/Comparator;
aload 0
getfield com/a/b/d/vc/c Lcom/a/b/d/vf;
getfield com/a/b/d/vf/a Lcom/a/b/d/yd;
areturn
.limit locals 1
.limit stack 1
.end method

.method private k()I
.annotation invisible Lcom/a/b/a/d;
.end annotation
aload 0
getfield com/a/b/d/vc/b [Ljava/lang/Object;
arraylength
ireturn
.limit locals 1
.limit stack 1
.end method

.method private l()V
aload 0
getfield com/a/b/d/vc/e I
aload 0
getfield com/a/b/d/vc/b [Ljava/lang/Object;
arraylength
if_icmple L0
aload 0
getfield com/a/b/d/vc/b [Ljava/lang/Object;
arraylength
istore 1
iload 1
bipush 64
if_icmpge L1
iload 1
iconst_1
iadd
iconst_2
imul
istore 1
L2:
iload 1
aload 0
getfield com/a/b/d/vc/a I
invokestatic com/a/b/d/vc/a(II)I
anewarray java/lang/Object
astore 2
aload 0
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iconst_0
aload 2
iconst_0
aload 0
getfield com/a/b/d/vc/b [Ljava/lang/Object;
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
aload 2
putfield com/a/b/d/vc/b [Ljava/lang/Object;
L0:
return
L1:
iload 1
iconst_2
idiv
iconst_3
invokestatic com/a/b/j/g/b(II)I
istore 1
goto L2
.limit locals 3
.limit stack 5
.end method

.method private m()I
aload 0
getfield com/a/b/d/vc/b [Ljava/lang/Object;
arraylength
istore 1
iload 1
bipush 64
if_icmpge L0
iload 1
iconst_1
iadd
iconst_2
imul
istore 1
L1:
iload 1
aload 0
getfield com/a/b/d/vc/a I
invokestatic com/a/b/d/vc/a(II)I
ireturn
L0:
iload 1
iconst_2
idiv
iconst_3
invokestatic com/a/b/j/g/b(II)I
istore 1
goto L1
.limit locals 2
.limit stack 2
.end method

.method final a(I)Lcom/a/b/d/vg;
.annotation invisible Lcom/a/b/a/d;
.end annotation
iload 1
aload 0
getfield com/a/b/d/vc/e I
invokestatic com/a/b/b/cn/b(II)I
pop
aload 0
aload 0
getfield com/a/b/d/vc/f I
iconst_1
iadd
putfield com/a/b/d/vc/f I
aload 0
aload 0
getfield com/a/b/d/vc/e I
iconst_1
isub
putfield com/a/b/d/vc/e I
aload 0
getfield com/a/b/d/vc/e I
iload 1
if_icmpne L0
aload 0
getfield com/a/b/d/vc/b [Ljava/lang/Object;
aload 0
getfield com/a/b/d/vc/e I
aconst_null
aastore
aconst_null
areturn
L0:
aload 0
getfield com/a/b/d/vc/e I
istore 2
aload 0
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iload 2
aaload
astore 6
aload 0
aload 0
getfield com/a/b/d/vc/e I
invokespecial com/a/b/d/vc/f(I)Lcom/a/b/d/vf;
astore 5
aload 5
getfield com/a/b/d/vf/c Lcom/a/b/d/vc;
getfield com/a/b/d/vc/e I
iconst_1
isub
iconst_2
idiv
istore 2
iload 2
ifeq L1
iload 2
iconst_1
isub
iconst_2
idiv
iconst_2
imul
iconst_2
iadd
istore 3
iload 3
iload 2
if_icmpeq L1
iload 3
iconst_2
imul
iconst_1
iadd
aload 5
getfield com/a/b/d/vf/c Lcom/a/b/d/vc;
getfield com/a/b/d/vc/e I
if_icmplt L1
aload 5
getfield com/a/b/d/vf/c Lcom/a/b/d/vc;
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iload 3
aaload
astore 7
aload 5
getfield com/a/b/d/vf/a Lcom/a/b/d/yd;
aload 7
aload 6
invokevirtual com/a/b/d/yd/compare(Ljava/lang/Object;Ljava/lang/Object;)I
ifge L1
aload 5
getfield com/a/b/d/vf/c Lcom/a/b/d/vc;
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iload 3
aload 6
aastore
aload 5
getfield com/a/b/d/vf/c Lcom/a/b/d/vc;
getfield com/a/b/d/vc/b [Ljava/lang/Object;
aload 5
getfield com/a/b/d/vf/c Lcom/a/b/d/vc;
getfield com/a/b/d/vc/e I
aload 7
aastore
L2:
aload 0
getfield com/a/b/d/vc/e I
istore 2
aload 0
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iload 2
aaload
astore 7
aload 0
getfield com/a/b/d/vc/b [Ljava/lang/Object;
aload 0
getfield com/a/b/d/vc/e I
aconst_null
aastore
aload 0
iload 1
invokespecial com/a/b/d/vc/f(I)Lcom/a/b/d/vf;
astore 8
iload 1
istore 4
L3:
iload 4
iconst_2
imul
iconst_1
iadd
istore 2
iload 2
ifge L4
iconst_m1
istore 2
L5:
iload 2
ifle L6
aload 8
getfield com/a/b/d/vf/c Lcom/a/b/d/vc;
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iload 4
aload 8
getfield com/a/b/d/vf/c Lcom/a/b/d/vc;
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iload 2
aaload
aastore
iload 2
istore 4
goto L3
L1:
aload 5
getfield com/a/b/d/vf/c Lcom/a/b/d/vc;
getfield com/a/b/d/vc/e I
istore 3
goto L2
L4:
aload 8
iload 2
iconst_2
imul
iconst_1
iadd
iconst_4
invokevirtual com/a/b/d/vf/b(II)I
istore 2
goto L5
L6:
aload 8
iload 4
aload 7
invokevirtual com/a/b/d/vf/a(ILjava/lang/Object;)I
istore 2
iload 2
iload 4
if_icmpne L7
aload 8
iload 4
iconst_2
imul
iconst_1
iadd
iconst_2
invokevirtual com/a/b/d/vf/b(II)I
istore 2
iload 2
ifle L8
aload 8
getfield com/a/b/d/vf/a Lcom/a/b/d/yd;
aload 8
getfield com/a/b/d/vf/c Lcom/a/b/d/vc;
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iload 2
aaload
aload 7
invokevirtual com/a/b/d/yd/compare(Ljava/lang/Object;Ljava/lang/Object;)I
ifge L8
aload 8
getfield com/a/b/d/vf/c Lcom/a/b/d/vc;
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iload 4
aload 8
getfield com/a/b/d/vf/c Lcom/a/b/d/vc;
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iload 2
aaload
aastore
aload 8
getfield com/a/b/d/vf/c Lcom/a/b/d/vc;
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iload 2
aload 7
aastore
L9:
iload 2
iload 4
if_icmpeq L10
iload 2
iload 1
if_icmpge L11
aload 8
getfield com/a/b/d/vf/c Lcom/a/b/d/vc;
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iload 1
aaload
astore 5
L12:
aload 8
getfield com/a/b/d/vf/b Lcom/a/b/d/vf;
iload 2
aload 7
invokevirtual com/a/b/d/vf/a(ILjava/lang/Object;)I
iload 1
if_icmpge L10
new com/a/b/d/vg
dup
aload 7
aload 5
invokespecial com/a/b/d/vg/<init>(Ljava/lang/Object;Ljava/lang/Object;)V
astore 5
L13:
iload 3
iload 1
if_icmpge L14
aload 5
ifnonnull L15
new com/a/b/d/vg
dup
aload 6
aload 7
invokespecial com/a/b/d/vg/<init>(Ljava/lang/Object;Ljava/lang/Object;)V
areturn
L8:
aload 8
iload 4
aload 7
invokevirtual com/a/b/d/vf/b(ILjava/lang/Object;)I
istore 2
goto L9
L11:
aload 8
getfield com/a/b/d/vf/c Lcom/a/b/d/vc;
astore 5
iload 1
iconst_1
isub
iconst_2
idiv
istore 4
aload 5
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iload 4
aaload
astore 5
goto L12
L10:
aconst_null
astore 5
goto L13
L7:
iload 2
iload 1
if_icmpge L16
new com/a/b/d/vg
dup
aload 7
aload 0
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iload 1
aaload
invokespecial com/a/b/d/vg/<init>(Ljava/lang/Object;Ljava/lang/Object;)V
astore 5
goto L13
L16:
aconst_null
astore 5
goto L13
L15:
new com/a/b/d/vg
dup
aload 6
aload 5
getfield com/a/b/d/vg/b Ljava/lang/Object;
invokespecial com/a/b/d/vg/<init>(Ljava/lang/Object;Ljava/lang/Object;)V
areturn
L14:
aload 5
areturn
.limit locals 9
.limit stack 5
.end method

.method public final add(Ljava/lang/Object;)Z
aload 0
aload 1
invokevirtual com/a/b/d/vc/offer(Ljava/lang/Object;)Z
pop
iconst_1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final addAll(Ljava/util/Collection;)Z
iconst_0
istore 2
aload 1
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokevirtual com/a/b/d/vc/offer(Ljava/lang/Object;)Z
pop
iconst_1
istore 2
goto L0
L1:
iload 2
ireturn
.limit locals 3
.limit stack 2
.end method

.method public final clear()V
iconst_0
istore 1
L0:
iload 1
aload 0
getfield com/a/b/d/vc/e I
if_icmpge L1
aload 0
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iload 1
aconst_null
aastore
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 0
iconst_0
putfield com/a/b/d/vc/e I
return
.limit locals 2
.limit stack 3
.end method

.method public final iterator()Ljava/util/Iterator;
new com/a/b/d/vh
dup
aload 0
iconst_0
invokespecial com/a/b/d/vh/<init>(Lcom/a/b/d/vc;B)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public final offer(Ljava/lang/Object;)Z
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
aload 0
getfield com/a/b/d/vc/f I
iconst_1
iadd
putfield com/a/b/d/vc/f I
aload 0
getfield com/a/b/d/vc/e I
istore 3
aload 0
iload 3
iconst_1
iadd
putfield com/a/b/d/vc/e I
aload 0
getfield com/a/b/d/vc/e I
aload 0
getfield com/a/b/d/vc/b [Ljava/lang/Object;
arraylength
if_icmple L0
aload 0
getfield com/a/b/d/vc/b [Ljava/lang/Object;
arraylength
istore 2
iload 2
bipush 64
if_icmpge L1
iload 2
iconst_1
iadd
iconst_2
imul
istore 2
L2:
iload 2
aload 0
getfield com/a/b/d/vc/a I
invokestatic com/a/b/d/vc/a(II)I
anewarray java/lang/Object
astore 4
aload 0
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iconst_0
aload 4
iconst_0
aload 0
getfield com/a/b/d/vc/b [Ljava/lang/Object;
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
aload 4
putfield com/a/b/d/vc/b [Ljava/lang/Object;
L0:
aload 0
iload 3
invokespecial com/a/b/d/vc/f(I)Lcom/a/b/d/vf;
astore 4
aload 4
iload 3
aload 1
invokevirtual com/a/b/d/vf/b(ILjava/lang/Object;)I
istore 2
iload 2
iload 3
if_icmpne L3
iload 3
istore 2
L4:
aload 4
iload 2
aload 1
invokevirtual com/a/b/d/vf/a(ILjava/lang/Object;)I
pop
aload 0
getfield com/a/b/d/vc/e I
aload 0
getfield com/a/b/d/vc/a I
if_icmple L5
aload 0
invokevirtual com/a/b/d/vc/isEmpty()Z
ifeq L6
aconst_null
astore 4
L7:
aload 4
aload 1
if_acmpeq L8
L5:
iconst_1
ireturn
L1:
iload 2
iconst_2
idiv
iconst_3
invokestatic com/a/b/j/g/b(II)I
istore 2
goto L2
L3:
aload 4
getfield com/a/b/d/vf/b Lcom/a/b/d/vf;
astore 4
goto L4
L6:
aload 0
aload 0
invokespecial com/a/b/d/vc/b()I
invokespecial com/a/b/d/vc/e(I)Ljava/lang/Object;
astore 4
goto L7
L8:
iconst_0
ireturn
.limit locals 5
.limit stack 5
.end method

.method public final peek()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/vc/isEmpty()Z
ifeq L0
aconst_null
areturn
L0:
aload 0
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iconst_0
aaload
areturn
.limit locals 1
.limit stack 2
.end method

.method public final poll()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/vc/isEmpty()Z
ifeq L0
aconst_null
areturn
L0:
aload 0
iconst_0
invokespecial com/a/b/d/vc/e(I)Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final size()I
aload 0
getfield com/a/b/d/vc/e I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final toArray()[Ljava/lang/Object;
aload 0
getfield com/a/b/d/vc/e I
anewarray java/lang/Object
astore 1
aload 0
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iconst_0
aload 1
iconst_0
aload 0
getfield com/a/b/d/vc/e I
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 1
areturn
.limit locals 2
.limit stack 5
.end method
