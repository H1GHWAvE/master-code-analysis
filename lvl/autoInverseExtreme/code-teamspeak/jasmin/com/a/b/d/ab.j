.bytecode 50.0
.class synchronized com/a/b/d/ab
.super java/util/AbstractCollection

.field final 'b' Ljava/lang/Object;

.field 'c' Ljava/util/Collection;

.field final 'd' Lcom/a/b/d/ab;

.field final 'e' Ljava/util/Collection;

.field final synthetic 'f' Lcom/a/b/d/n;

.method <init>(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/Collection;Lcom/a/b/d/ab;)V
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 4 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
putfield com/a/b/d/ab/f Lcom/a/b/d/n;
aload 0
invokespecial java/util/AbstractCollection/<init>()V
aload 0
aload 2
putfield com/a/b/d/ab/b Ljava/lang/Object;
aload 0
aload 3
putfield com/a/b/d/ab/c Ljava/util/Collection;
aload 0
aload 4
putfield com/a/b/d/ab/d Lcom/a/b/d/ab;
aload 4
ifnonnull L0
aconst_null
astore 1
L1:
aload 0
aload 1
putfield com/a/b/d/ab/e Ljava/util/Collection;
return
L0:
aload 4
getfield com/a/b/d/ab/c Ljava/util/Collection;
astore 1
goto L1
.limit locals 5
.limit stack 2
.end method

.method private d()Ljava/lang/Object;
aload 0
getfield com/a/b/d/ab/b Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e()Ljava/util/Collection;
aload 0
getfield com/a/b/d/ab/c Ljava/util/Collection;
areturn
.limit locals 1
.limit stack 1
.end method

.method private f()Lcom/a/b/d/ab;
aload 0
getfield com/a/b/d/ab/d Lcom/a/b/d/ab;
areturn
.limit locals 1
.limit stack 1
.end method

.method final a()V
aload 0
getfield com/a/b/d/ab/d Lcom/a/b/d/ab;
ifnull L0
aload 0
getfield com/a/b/d/ab/d Lcom/a/b/d/ab;
invokevirtual com/a/b/d/ab/a()V
aload 0
getfield com/a/b/d/ab/d Lcom/a/b/d/ab;
getfield com/a/b/d/ab/c Ljava/util/Collection;
aload 0
getfield com/a/b/d/ab/e Ljava/util/Collection;
if_acmpeq L1
new java/util/ConcurrentModificationException
dup
invokespecial java/util/ConcurrentModificationException/<init>()V
athrow
L0:
aload 0
getfield com/a/b/d/ab/c Ljava/util/Collection;
invokeinterface java/util/Collection/isEmpty()Z 0
ifeq L1
aload 0
getfield com/a/b/d/ab/f Lcom/a/b/d/n;
invokestatic com/a/b/d/n/a(Lcom/a/b/d/n;)Ljava/util/Map;
aload 0
getfield com/a/b/d/ab/b Ljava/lang/Object;
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/util/Collection
astore 1
aload 1
ifnull L1
aload 0
aload 1
putfield com/a/b/d/ab/c Ljava/util/Collection;
L1:
return
.limit locals 2
.limit stack 2
.end method

.method public add(Ljava/lang/Object;)Z
aload 0
invokevirtual com/a/b/d/ab/a()V
aload 0
getfield com/a/b/d/ab/c Ljava/util/Collection;
invokeinterface java/util/Collection/isEmpty()Z 0
istore 2
aload 0
getfield com/a/b/d/ab/c Ljava/util/Collection;
aload 1
invokeinterface java/util/Collection/add(Ljava/lang/Object;)Z 1
istore 3
iload 3
ifeq L0
aload 0
getfield com/a/b/d/ab/f Lcom/a/b/d/n;
invokestatic com/a/b/d/n/c(Lcom/a/b/d/n;)I
pop
iload 2
ifeq L0
aload 0
invokevirtual com/a/b/d/ab/c()V
L0:
iload 3
ireturn
.limit locals 4
.limit stack 2
.end method

.method public addAll(Ljava/util/Collection;)Z
aload 1
invokeinterface java/util/Collection/isEmpty()Z 0
ifeq L0
iconst_0
istore 4
L1:
iload 4
ireturn
L0:
aload 0
invokevirtual com/a/b/d/ab/size()I
istore 2
aload 0
getfield com/a/b/d/ab/c Ljava/util/Collection;
aload 1
invokeinterface java/util/Collection/addAll(Ljava/util/Collection;)Z 1
istore 5
iload 5
istore 4
iload 5
ifeq L1
aload 0
getfield com/a/b/d/ab/c Ljava/util/Collection;
invokeinterface java/util/Collection/size()I 0
istore 3
aload 0
getfield com/a/b/d/ab/f Lcom/a/b/d/n;
iload 3
iload 2
isub
invokestatic com/a/b/d/n/a(Lcom/a/b/d/n;I)I
pop
iload 5
istore 4
iload 2
ifne L1
aload 0
invokevirtual com/a/b/d/ab/c()V
iload 5
ireturn
.limit locals 6
.limit stack 3
.end method

.method final b()V
aload 0
astore 1
L0:
aload 1
getfield com/a/b/d/ab/d Lcom/a/b/d/ab;
ifnull L1
aload 1
getfield com/a/b/d/ab/d Lcom/a/b/d/ab;
astore 1
goto L0
L1:
aload 1
getfield com/a/b/d/ab/c Ljava/util/Collection;
invokeinterface java/util/Collection/isEmpty()Z 0
ifeq L2
aload 1
getfield com/a/b/d/ab/f Lcom/a/b/d/n;
invokestatic com/a/b/d/n/a(Lcom/a/b/d/n;)Ljava/util/Map;
aload 1
getfield com/a/b/d/ab/b Ljava/lang/Object;
invokeinterface java/util/Map/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
pop
L2:
return
.limit locals 2
.limit stack 2
.end method

.method final c()V
aload 0
astore 1
L0:
aload 1
getfield com/a/b/d/ab/d Lcom/a/b/d/ab;
ifnull L1
aload 1
getfield com/a/b/d/ab/d Lcom/a/b/d/ab;
astore 1
goto L0
L1:
aload 1
getfield com/a/b/d/ab/f Lcom/a/b/d/n;
invokestatic com/a/b/d/n/a(Lcom/a/b/d/n;)Ljava/util/Map;
aload 1
getfield com/a/b/d/ab/b Ljava/lang/Object;
aload 1
getfield com/a/b/d/ab/c Ljava/util/Collection;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
return
.limit locals 2
.limit stack 3
.end method

.method public clear()V
aload 0
invokevirtual com/a/b/d/ab/size()I
istore 1
iload 1
ifne L0
return
L0:
aload 0
getfield com/a/b/d/ab/c Ljava/util/Collection;
invokeinterface java/util/Collection/clear()V 0
aload 0
getfield com/a/b/d/ab/f Lcom/a/b/d/n;
iload 1
invokestatic com/a/b/d/n/b(Lcom/a/b/d/n;I)I
pop
aload 0
invokevirtual com/a/b/d/ab/b()V
return
.limit locals 2
.limit stack 2
.end method

.method public contains(Ljava/lang/Object;)Z
aload 0
invokevirtual com/a/b/d/ab/a()V
aload 0
getfield com/a/b/d/ab/c Ljava/util/Collection;
aload 1
invokeinterface java/util/Collection/contains(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public containsAll(Ljava/util/Collection;)Z
aload 0
invokevirtual com/a/b/d/ab/a()V
aload 0
getfield com/a/b/d/ab/c Ljava/util/Collection;
aload 1
invokeinterface java/util/Collection/containsAll(Ljava/util/Collection;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
aload 0
if_acmpne L0
iconst_1
ireturn
L0:
aload 0
invokevirtual com/a/b/d/ab/a()V
aload 0
getfield com/a/b/d/ab/c Ljava/util/Collection;
aload 1
invokeinterface java/util/Collection/equals(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public hashCode()I
aload 0
invokevirtual com/a/b/d/ab/a()V
aload 0
getfield com/a/b/d/ab/c Ljava/util/Collection;
invokeinterface java/util/Collection/hashCode()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public iterator()Ljava/util/Iterator;
aload 0
invokevirtual com/a/b/d/ab/a()V
new com/a/b/d/ac
dup
aload 0
invokespecial com/a/b/d/ac/<init>(Lcom/a/b/d/ab;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public remove(Ljava/lang/Object;)Z
aload 0
invokevirtual com/a/b/d/ab/a()V
aload 0
getfield com/a/b/d/ab/c Ljava/util/Collection;
aload 1
invokeinterface java/util/Collection/remove(Ljava/lang/Object;)Z 1
istore 2
iload 2
ifeq L0
aload 0
getfield com/a/b/d/ab/f Lcom/a/b/d/n;
invokestatic com/a/b/d/n/b(Lcom/a/b/d/n;)I
pop
aload 0
invokevirtual com/a/b/d/ab/b()V
L0:
iload 2
ireturn
.limit locals 3
.limit stack 2
.end method

.method public removeAll(Ljava/util/Collection;)Z
aload 1
invokeinterface java/util/Collection/isEmpty()Z 0
ifeq L0
iconst_0
istore 4
L1:
iload 4
ireturn
L0:
aload 0
invokevirtual com/a/b/d/ab/size()I
istore 2
aload 0
getfield com/a/b/d/ab/c Ljava/util/Collection;
aload 1
invokeinterface java/util/Collection/removeAll(Ljava/util/Collection;)Z 1
istore 5
iload 5
istore 4
iload 5
ifeq L1
aload 0
getfield com/a/b/d/ab/c Ljava/util/Collection;
invokeinterface java/util/Collection/size()I 0
istore 3
aload 0
getfield com/a/b/d/ab/f Lcom/a/b/d/n;
iload 3
iload 2
isub
invokestatic com/a/b/d/n/a(Lcom/a/b/d/n;I)I
pop
aload 0
invokevirtual com/a/b/d/ab/b()V
iload 5
ireturn
.limit locals 6
.limit stack 3
.end method

.method public retainAll(Ljava/util/Collection;)Z
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
invokevirtual com/a/b/d/ab/size()I
istore 2
aload 0
getfield com/a/b/d/ab/c Ljava/util/Collection;
aload 1
invokeinterface java/util/Collection/retainAll(Ljava/util/Collection;)Z 1
istore 4
iload 4
ifeq L0
aload 0
getfield com/a/b/d/ab/c Ljava/util/Collection;
invokeinterface java/util/Collection/size()I 0
istore 3
aload 0
getfield com/a/b/d/ab/f Lcom/a/b/d/n;
iload 3
iload 2
isub
invokestatic com/a/b/d/n/a(Lcom/a/b/d/n;I)I
pop
aload 0
invokevirtual com/a/b/d/ab/b()V
L0:
iload 4
ireturn
.limit locals 5
.limit stack 3
.end method

.method public size()I
aload 0
invokevirtual com/a/b/d/ab/a()V
aload 0
getfield com/a/b/d/ab/c Ljava/util/Collection;
invokeinterface java/util/Collection/size()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public toString()Ljava/lang/String;
aload 0
invokevirtual com/a/b/d/ab/a()V
aload 0
getfield com/a/b/d/ab/c Ljava/util/Collection;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
