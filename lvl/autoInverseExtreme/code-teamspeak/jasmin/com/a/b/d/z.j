.bytecode 50.0
.class synchronized com/a/b/d/z
.super com/a/b/d/q
.implements java/util/SortedMap

.field 'd' Ljava/util/SortedSet;

.field final synthetic 'e' Lcom/a/b/d/n;

.method <init>(Lcom/a/b/d/n;Ljava/util/SortedMap;)V
aload 0
aload 1
putfield com/a/b/d/z/e Lcom/a/b/d/n;
aload 0
aload 1
aload 2
invokespecial com/a/b/d/q/<init>(Lcom/a/b/d/n;Ljava/util/Map;)V
return
.limit locals 3
.limit stack 3
.end method

.method b()Ljava/util/SortedSet;
new com/a/b/d/aa
dup
aload 0
getfield com/a/b/d/z/e Lcom/a/b/d/n;
aload 0
invokevirtual com/a/b/d/z/d()Ljava/util/SortedMap;
invokespecial com/a/b/d/aa/<init>(Lcom/a/b/d/n;Ljava/util/SortedMap;)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public c()Ljava/util/SortedSet;
aload 0
getfield com/a/b/d/z/d Ljava/util/SortedSet;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
aload 0
invokevirtual com/a/b/d/z/b()Ljava/util/SortedSet;
astore 1
aload 0
aload 1
putfield com/a/b/d/z/d Ljava/util/SortedSet;
L0:
aload 1
areturn
.limit locals 3
.limit stack 2
.end method

.method public comparator()Ljava/util/Comparator;
aload 0
invokevirtual com/a/b/d/z/d()Ljava/util/SortedMap;
invokeinterface java/util/SortedMap/comparator()Ljava/util/Comparator; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method d()Ljava/util/SortedMap;
aload 0
getfield com/a/b/d/z/a Ljava/util/Map;
checkcast java/util/SortedMap
areturn
.limit locals 1
.limit stack 1
.end method

.method synthetic e()Ljava/util/Set;
aload 0
invokevirtual com/a/b/d/z/b()Ljava/util/SortedSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method public firstKey()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/z/d()Ljava/util/SortedMap;
invokeinterface java/util/SortedMap/firstKey()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
new com/a/b/d/z
dup
aload 0
getfield com/a/b/d/z/e Lcom/a/b/d/n;
aload 0
invokevirtual com/a/b/d/z/d()Ljava/util/SortedMap;
aload 1
invokeinterface java/util/SortedMap/headMap(Ljava/lang/Object;)Ljava/util/SortedMap; 1
invokespecial com/a/b/d/z/<init>(Lcom/a/b/d/n;Ljava/util/SortedMap;)V
areturn
.limit locals 2
.limit stack 5
.end method

.method public synthetic keySet()Ljava/util/Set;
aload 0
invokevirtual com/a/b/d/z/c()Ljava/util/SortedSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method public lastKey()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/z/d()Ljava/util/SortedMap;
invokeinterface java/util/SortedMap/lastKey()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
new com/a/b/d/z
dup
aload 0
getfield com/a/b/d/z/e Lcom/a/b/d/n;
aload 0
invokevirtual com/a/b/d/z/d()Ljava/util/SortedMap;
aload 1
aload 2
invokeinterface java/util/SortedMap/subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap; 2
invokespecial com/a/b/d/z/<init>(Lcom/a/b/d/n;Ljava/util/SortedMap;)V
areturn
.limit locals 3
.limit stack 6
.end method

.method public tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
new com/a/b/d/z
dup
aload 0
getfield com/a/b/d/z/e Lcom/a/b/d/n;
aload 0
invokevirtual com/a/b/d/z/d()Ljava/util/SortedMap;
aload 1
invokeinterface java/util/SortedMap/tailMap(Ljava/lang/Object;)Ljava/util/SortedMap; 1
invokespecial com/a/b/d/z/<init>(Lcom/a/b/d/n;Ljava/util/SortedMap;)V
areturn
.limit locals 2
.limit stack 5
.end method
