.bytecode 50.0
.class final synchronized com/a/b/d/aao
.super java/util/AbstractSet

.field final 'a' Lcom/a/b/d/jt;

.method <init>(Ljava/util/Set;)V
aload 0
invokespecial java/util/AbstractSet/<init>()V
invokestatic com/a/b/d/jt/l()Lcom/a/b/d/ju;
astore 4
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Set
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 1
iconst_0
istore 2
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 4
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual com/a/b/d/ju/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;
pop
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 0
aload 4
invokevirtual com/a/b/d/ju/a()Lcom/a/b/d/jt;
putfield com/a/b/d/aao/a Lcom/a/b/d/jt;
aload 0
getfield com/a/b/d/aao/a Lcom/a/b/d/jt;
invokevirtual com/a/b/d/jt/size()I
bipush 30
if_icmpgt L2
iconst_1
istore 3
L3:
iload 3
ldc "Too many elements to create power set: %s > 30"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/d/aao/a Lcom/a/b/d/jt;
invokevirtual com/a/b/d/jt/size()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
return
L2:
iconst_0
istore 3
goto L3
.limit locals 5
.limit stack 6
.end method

.method public final contains(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
instanceof java/util/Set
ifeq L0
aload 1
checkcast java/util/Set
astore 1
aload 0
getfield com/a/b/d/aao/a Lcom/a/b/d/jt;
invokevirtual com/a/b/d/jt/g()Lcom/a/b/d/lo;
aload 1
invokevirtual com/a/b/d/lo/containsAll(Ljava/util/Collection;)Z
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
instanceof com/a/b/d/aao
ifeq L0
aload 1
checkcast com/a/b/d/aao
astore 1
aload 0
getfield com/a/b/d/aao/a Lcom/a/b/d/jt;
aload 1
getfield com/a/b/d/aao/a Lcom/a/b/d/jt;
invokevirtual com/a/b/d/jt/equals(Ljava/lang/Object;)Z
ireturn
L0:
aload 0
aload 1
invokespecial java/util/AbstractSet/equals(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final hashCode()I
aload 0
getfield com/a/b/d/aao/a Lcom/a/b/d/jt;
invokevirtual com/a/b/d/jt/g()Lcom/a/b/d/lo;
invokevirtual com/a/b/d/lo/hashCode()I
aload 0
getfield com/a/b/d/aao/a Lcom/a/b/d/jt;
invokevirtual com/a/b/d/jt/size()I
iconst_1
isub
ishl
ireturn
.limit locals 1
.limit stack 3
.end method

.method public final isEmpty()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final iterator()Ljava/util/Iterator;
new com/a/b/d/aap
dup
aload 0
aload 0
invokevirtual com/a/b/d/aao/size()I
invokespecial com/a/b/d/aap/<init>(Lcom/a/b/d/aao;I)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public final size()I
iconst_1
aload 0
getfield com/a/b/d/aao/a Lcom/a/b/d/jt;
invokevirtual com/a/b/d/jt/size()I
ishl
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/d/aao/a Lcom/a/b/d/jt;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 10
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "powerSet("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 4
.end method
