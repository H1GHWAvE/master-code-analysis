.bytecode 50.0
.class final synchronized com/a/b/d/to
.super com/a/b/b/ak
.implements java/io/Serializable

.field private static final 'b' J = 0L


.field private final 'a' Lcom/a/b/d/bw;

.method <init>(Lcom/a/b/d/bw;)V
aload 0
invokespecial com/a/b/b/ak/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/d/bw
putfield com/a/b/d/to/a Lcom/a/b/d/bw;
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Lcom/a/b/d/bw;Ljava/lang/Object;)Ljava/lang/Object;
aload 0
aload 1
invokeinterface com/a/b/d/bw/get(Ljava/lang/Object;)Ljava/lang/Object; 1
astore 0
aload 0
ifnull L0
iconst_1
istore 2
L1:
iload 2
ldc "No non-null mapping present for input: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
areturn
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 6
.end method

.method protected final a(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
getfield com/a/b/d/to/a Lcom/a/b/d/bw;
invokeinterface com/a/b/d/bw/b()Lcom/a/b/d/bw; 0
aload 1
invokestatic com/a/b/d/to/a(Lcom/a/b/d/bw;Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method protected final b(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
getfield com/a/b/d/to/a Lcom/a/b/d/bw;
aload 1
invokestatic com/a/b/d/to/a(Lcom/a/b/d/bw;Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
instanceof com/a/b/d/to
ifeq L0
aload 1
checkcast com/a/b/d/to
astore 1
aload 0
getfield com/a/b/d/to/a Lcom/a/b/d/bw;
aload 1
getfield com/a/b/d/to/a Lcom/a/b/d/bw;
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final hashCode()I
aload 0
getfield com/a/b/d/to/a Lcom/a/b/d/bw;
invokevirtual java/lang/Object/hashCode()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/d/to/a Lcom/a/b/d/bw;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 18
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Maps.asConverter("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 4
.end method
