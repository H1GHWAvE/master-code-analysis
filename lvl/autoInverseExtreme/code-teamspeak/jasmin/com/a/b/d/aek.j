.bytecode 50.0
.class synchronized abstract com/a/b/d/aek
.super com/a/b/d/aej
.implements java/util/ListIterator
.annotation invisible Lcom/a/b/a/b;
.end annotation

.method <init>(Ljava/util/ListIterator;)V
aload 0
aload 1
invokespecial com/a/b/d/aej/<init>(Ljava/util/Iterator;)V
return
.limit locals 2
.limit stack 2
.end method

.method private a()Ljava/util/ListIterator;
aload 0
getfield com/a/b/d/aek/c Ljava/util/Iterator;
invokestatic com/a/b/d/nj/k(Ljava/util/Iterator;)Ljava/util/ListIterator;
areturn
.limit locals 1
.limit stack 1
.end method

.method public add(Ljava/lang/Object;)V
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public final hasPrevious()Z
aload 0
getfield com/a/b/d/aek/c Ljava/util/Iterator;
invokestatic com/a/b/d/nj/k(Ljava/util/Iterator;)Ljava/util/ListIterator;
invokeinterface java/util/ListIterator/hasPrevious()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final nextIndex()I
aload 0
getfield com/a/b/d/aek/c Ljava/util/Iterator;
invokestatic com/a/b/d/nj/k(Ljava/util/Iterator;)Ljava/util/ListIterator;
invokeinterface java/util/ListIterator/nextIndex()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final previous()Ljava/lang/Object;
aload 0
aload 0
getfield com/a/b/d/aek/c Ljava/util/Iterator;
invokestatic com/a/b/d/nj/k(Ljava/util/Iterator;)Ljava/util/ListIterator;
invokeinterface java/util/ListIterator/previous()Ljava/lang/Object; 0
invokevirtual com/a/b/d/aek/a(Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final previousIndex()I
aload 0
getfield com/a/b/d/aek/c Ljava/util/Iterator;
invokestatic com/a/b/d/nj/k(Ljava/util/Iterator;)Ljava/util/ListIterator;
invokeinterface java/util/ListIterator/previousIndex()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public set(Ljava/lang/Object;)V
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method
