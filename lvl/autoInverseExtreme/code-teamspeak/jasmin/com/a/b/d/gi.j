.bytecode 50.0
.class public synchronized abstract com/a/b/d/gi
.super com/a/b/d/gs
.implements java/util/concurrent/ConcurrentMap
.annotation invisible Lcom/a/b/a/b;
.end annotation

.method protected <init>()V
aload 0
invokespecial com/a/b/d/gs/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method protected synthetic a()Ljava/util/Map;
aload 0
invokevirtual com/a/b/d/gi/b()Ljava/util/concurrent/ConcurrentMap;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected abstract b()Ljava/util/concurrent/ConcurrentMap;
.end method

.method protected synthetic k_()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gi/b()Ljava/util/concurrent/ConcurrentMap;
areturn
.limit locals 1
.limit stack 1
.end method

.method public putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gi/b()Ljava/util/concurrent/ConcurrentMap;
aload 1
aload 2
invokeinterface java/util/concurrent/ConcurrentMap/putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
areturn
.limit locals 3
.limit stack 3
.end method

.method public remove(Ljava/lang/Object;Ljava/lang/Object;)Z
aload 0
invokevirtual com/a/b/d/gi/b()Ljava/util/concurrent/ConcurrentMap;
aload 1
aload 2
invokeinterface java/util/concurrent/ConcurrentMap/remove(Ljava/lang/Object;Ljava/lang/Object;)Z 2
ireturn
.limit locals 3
.limit stack 3
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gi/b()Ljava/util/concurrent/ConcurrentMap;
aload 1
aload 2
invokeinterface java/util/concurrent/ConcurrentMap/replace(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
areturn
.limit locals 3
.limit stack 3
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z
aload 0
invokevirtual com/a/b/d/gi/b()Ljava/util/concurrent/ConcurrentMap;
aload 1
aload 2
aload 3
invokeinterface java/util/concurrent/ConcurrentMap/replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z 3
ireturn
.limit locals 4
.limit stack 4
.end method
