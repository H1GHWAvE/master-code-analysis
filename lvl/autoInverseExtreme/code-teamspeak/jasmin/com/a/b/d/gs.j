.bytecode 50.0
.class public synchronized abstract com/a/b/d/gs
.super com/a/b/d/hg
.implements java/util/Map
.annotation invisible Lcom/a/b/a/b;
.end annotation

.method public <init>()V
aload 0
invokespecial com/a/b/d/hg/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private a(Ljava/lang/Object;)Ljava/lang/Object;
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokevirtual com/a/b/d/gs/entrySet()Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 3
aload 3
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
aload 1
invokestatic com/a/b/b/ce/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L0
aload 3
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
astore 1
aload 2
invokeinterface java/util/Iterator/remove()V 0
aload 1
areturn
L1:
aconst_null
areturn
.limit locals 4
.limit stack 2
.end method

.method private a(Ljava/util/Map;)V
aload 0
aload 1
invokestatic com/a/b/d/sz/a(Ljava/util/Map;Ljava/util/Map;)V
return
.limit locals 2
.limit stack 2
.end method

.method private b(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokestatic com/a/b/d/sz/e(Ljava/util/Map;Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private d()V
aload 0
invokevirtual com/a/b/d/gs/entrySet()Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
invokestatic com/a/b/d/nj/i(Ljava/util/Iterator;)V
return
.limit locals 1
.limit stack 1
.end method

.method private d(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokestatic com/a/b/d/sz/f(Ljava/util/Map;Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private e()Z
aload 0
invokevirtual com/a/b/d/gs/entrySet()Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private f()I
aload 0
invokevirtual com/a/b/d/gs/entrySet()Ljava/util/Set;
invokestatic com/a/b/d/aad/a(Ljava/util/Set;)I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private h()Ljava/lang/String;
aload 0
invokestatic com/a/b/d/sz/a(Ljava/util/Map;)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public abstract a()Ljava/util/Map;
.end method

.method protected c(Ljava/lang/Object;)Z
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokestatic com/a/b/d/sz/d(Ljava/util/Map;Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public clear()V
aload 0
invokevirtual com/a/b/d/gs/a()Ljava/util/Map;
invokeinterface java/util/Map/clear()V 0
return
.limit locals 1
.limit stack 1
.end method

.method public containsKey(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokevirtual com/a/b/d/gs/a()Ljava/util/Map;
aload 1
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public containsValue(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokevirtual com/a/b/d/gs/a()Ljava/util/Map;
aload 1
invokeinterface java/util/Map/containsValue(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public entrySet()Ljava/util/Set;
aload 0
invokevirtual com/a/b/d/gs/a()Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
aload 0
if_acmpeq L0
aload 0
invokevirtual com/a/b/d/gs/a()Ljava/util/Map;
aload 1
invokeinterface java/util/Map/equals(Ljava/lang/Object;)Z 1
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokevirtual com/a/b/d/gs/a()Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public hashCode()I
aload 0
invokevirtual com/a/b/d/gs/a()Ljava/util/Map;
invokeinterface java/util/Map/hashCode()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isEmpty()Z
aload 0
invokevirtual com/a/b/d/gs/a()Ljava/util/Map;
invokeinterface java/util/Map/isEmpty()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public synthetic k_()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gs/a()Ljava/util/Map;
areturn
.limit locals 1
.limit stack 1
.end method

.method public keySet()Ljava/util/Set;
aload 0
invokevirtual com/a/b/d/gs/a()Ljava/util/Map;
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gs/a()Ljava/util/Map;
aload 1
aload 2
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
areturn
.limit locals 3
.limit stack 3
.end method

.method public putAll(Ljava/util/Map;)V
aload 0
invokevirtual com/a/b/d/gs/a()Ljava/util/Map;
aload 1
invokeinterface java/util/Map/putAll(Ljava/util/Map;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/gs/a()Ljava/util/Map;
aload 1
invokeinterface java/util/Map/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public size()I
aload 0
invokevirtual com/a/b/d/gs/a()Ljava/util/Map;
invokeinterface java/util/Map/size()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public values()Ljava/util/Collection;
aload 0
invokevirtual com/a/b/d/gs/a()Ljava/util/Map;
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
areturn
.limit locals 1
.limit stack 1
.end method
