.bytecode 50.0
.class public final synchronized com/a/b/d/yk
.super java/lang/Object

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/util/concurrent/BlockingQueue;Ljava/util/Collection;IJLjava/util/concurrent/TimeUnit;)I
.annotation invisible Lcom/a/b/a/a;
.end annotation
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
invokestatic java/lang/System/nanoTime()J
lstore 9
aload 5
lload 3
invokevirtual java/util/concurrent/TimeUnit/toNanos(J)J
lstore 3
iconst_0
istore 6
L0:
iload 6
istore 8
iload 6
iload 2
if_icmpge L1
iload 6
aload 0
aload 1
iload 2
iload 6
isub
invokeinterface java/util/concurrent/BlockingQueue/drainTo(Ljava/util/Collection;I)I 2
iadd
istore 7
iload 7
istore 6
iload 7
iload 2
if_icmpge L0
aload 0
lload 3
lload 9
ladd
invokestatic java/lang/System/nanoTime()J
lsub
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
invokeinterface java/util/concurrent/BlockingQueue/poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object; 3
astore 5
iload 7
istore 8
aload 5
ifnull L1
aload 1
aload 5
invokeinterface java/util/Collection/add(Ljava/lang/Object;)Z 1
pop
iload 7
iconst_1
iadd
istore 6
goto L0
L1:
iload 8
ireturn
.limit locals 11
.limit stack 5
.end method

.method private static a()Ljava/util/ArrayDeque;
new java/util/ArrayDeque
dup
invokespecial java/util/ArrayDeque/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method private static a(Ljava/lang/Iterable;)Ljava/util/ArrayDeque;
aload 0
instanceof java/util/Collection
ifeq L0
new java/util/ArrayDeque
dup
aload 0
invokestatic com/a/b/d/cm/a(Ljava/lang/Iterable;)Ljava/util/Collection;
invokespecial java/util/ArrayDeque/<init>(Ljava/util/Collection;)V
areturn
L0:
new java/util/ArrayDeque
dup
invokespecial java/util/ArrayDeque/<init>()V
astore 1
aload 1
aload 0
invokestatic com/a/b/d/mq/a(Ljava/util/Collection;Ljava/lang/Iterable;)Z
pop
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method private static a(Ljava/util/Deque;)Ljava/util/Deque;
new com/a/b/d/ade
dup
aload 0
invokespecial com/a/b/d/ade/<init>(Ljava/util/Deque;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Ljava/util/Queue;)Ljava/util/Queue;
aload 0
instanceof com/a/b/d/ado
ifeq L0
aload 0
areturn
L0:
new com/a/b/d/ado
dup
aload 0
invokespecial com/a/b/d/ado/<init>(Ljava/util/Queue;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(I)Ljava/util/concurrent/ArrayBlockingQueue;
new java/util/concurrent/ArrayBlockingQueue
dup
iload 0
invokespecial java/util/concurrent/ArrayBlockingQueue/<init>(I)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static b(Ljava/util/concurrent/BlockingQueue;Ljava/util/Collection;IJLjava/util/concurrent/TimeUnit;)I
.annotation invisible Lcom/a/b/a/a;
.end annotation
.catch all from L0 to L1 using L2
.catch java/lang/InterruptedException from L3 to L4 using L5
.catch all from L3 to L4 using L2
.catch all from L6 to L7 using L2
iconst_0
istore 7
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
invokestatic java/lang/System/nanoTime()J
lstore 11
aload 5
lload 3
invokevirtual java/util/concurrent/TimeUnit/toNanos(J)J
lstore 3
iconst_0
istore 6
L8:
iload 6
istore 9
iload 7
istore 10
iload 6
iload 2
if_icmpge L9
iload 7
istore 9
L0:
aload 0
aload 1
iload 2
iload 6
isub
invokeinterface java/util/concurrent/BlockingQueue/drainTo(Ljava/util/Collection;I)I 2
istore 8
L1:
iload 6
iload 8
iadd
istore 8
iload 8
istore 6
iload 8
iload 2
if_icmpge L8
L10:
iload 7
istore 9
L3:
aload 0
lload 11
lload 3
ladd
invokestatic java/lang/System/nanoTime()J
lsub
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
invokeinterface java/util/concurrent/BlockingQueue/poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object; 3
astore 5
L4:
iload 8
istore 9
iload 7
istore 10
aload 5
ifnull L9
iload 7
istore 9
L6:
aload 1
aload 5
invokeinterface java/util/Collection/add(Ljava/lang/Object;)Z 1
pop
L7:
iload 8
iconst_1
iadd
istore 6
goto L8
L5:
astore 5
iconst_1
istore 7
goto L10
L9:
iload 10
ifeq L11
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
L11:
iload 9
ireturn
L2:
astore 0
iload 9
ifeq L12
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
L12:
aload 0
athrow
.limit locals 13
.limit stack 5
.end method

.method private static b()Ljava/util/concurrent/ConcurrentLinkedQueue;
new java/util/concurrent/ConcurrentLinkedQueue
dup
invokespecial java/util/concurrent/ConcurrentLinkedQueue/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method private static b(Ljava/lang/Iterable;)Ljava/util/concurrent/ConcurrentLinkedQueue;
aload 0
instanceof java/util/Collection
ifeq L0
new java/util/concurrent/ConcurrentLinkedQueue
dup
aload 0
invokestatic com/a/b/d/cm/a(Ljava/lang/Iterable;)Ljava/util/Collection;
invokespecial java/util/concurrent/ConcurrentLinkedQueue/<init>(Ljava/util/Collection;)V
areturn
L0:
new java/util/concurrent/ConcurrentLinkedQueue
dup
invokespecial java/util/concurrent/ConcurrentLinkedQueue/<init>()V
astore 1
aload 1
aload 0
invokestatic com/a/b/d/mq/a(Ljava/util/Collection;Ljava/lang/Iterable;)Z
pop
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method private static b(I)Ljava/util/concurrent/LinkedBlockingDeque;
new java/util/concurrent/LinkedBlockingDeque
dup
iload 0
invokespecial java/util/concurrent/LinkedBlockingDeque/<init>(I)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static c()Ljava/util/concurrent/LinkedBlockingDeque;
new java/util/concurrent/LinkedBlockingDeque
dup
invokespecial java/util/concurrent/LinkedBlockingDeque/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method private static c(Ljava/lang/Iterable;)Ljava/util/concurrent/LinkedBlockingDeque;
aload 0
instanceof java/util/Collection
ifeq L0
new java/util/concurrent/LinkedBlockingDeque
dup
aload 0
invokestatic com/a/b/d/cm/a(Ljava/lang/Iterable;)Ljava/util/Collection;
invokespecial java/util/concurrent/LinkedBlockingDeque/<init>(Ljava/util/Collection;)V
areturn
L0:
new java/util/concurrent/LinkedBlockingDeque
dup
invokespecial java/util/concurrent/LinkedBlockingDeque/<init>()V
astore 1
aload 1
aload 0
invokestatic com/a/b/d/mq/a(Ljava/util/Collection;Ljava/lang/Iterable;)Z
pop
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method private static c(I)Ljava/util/concurrent/LinkedBlockingQueue;
new java/util/concurrent/LinkedBlockingQueue
dup
iload 0
invokespecial java/util/concurrent/LinkedBlockingQueue/<init>(I)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static d()Ljava/util/concurrent/LinkedBlockingQueue;
new java/util/concurrent/LinkedBlockingQueue
dup
invokespecial java/util/concurrent/LinkedBlockingQueue/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method private static d(Ljava/lang/Iterable;)Ljava/util/concurrent/LinkedBlockingQueue;
aload 0
instanceof java/util/Collection
ifeq L0
new java/util/concurrent/LinkedBlockingQueue
dup
aload 0
invokestatic com/a/b/d/cm/a(Ljava/lang/Iterable;)Ljava/util/Collection;
invokespecial java/util/concurrent/LinkedBlockingQueue/<init>(Ljava/util/Collection;)V
areturn
L0:
new java/util/concurrent/LinkedBlockingQueue
dup
invokespecial java/util/concurrent/LinkedBlockingQueue/<init>()V
astore 1
aload 1
aload 0
invokestatic com/a/b/d/mq/a(Ljava/util/Collection;Ljava/lang/Iterable;)Z
pop
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method private static e()Ljava/util/concurrent/PriorityBlockingQueue;
new java/util/concurrent/PriorityBlockingQueue
dup
invokespecial java/util/concurrent/PriorityBlockingQueue/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method private static e(Ljava/lang/Iterable;)Ljava/util/concurrent/PriorityBlockingQueue;
aload 0
instanceof java/util/Collection
ifeq L0
new java/util/concurrent/PriorityBlockingQueue
dup
aload 0
invokestatic com/a/b/d/cm/a(Ljava/lang/Iterable;)Ljava/util/Collection;
invokespecial java/util/concurrent/PriorityBlockingQueue/<init>(Ljava/util/Collection;)V
areturn
L0:
new java/util/concurrent/PriorityBlockingQueue
dup
invokespecial java/util/concurrent/PriorityBlockingQueue/<init>()V
astore 1
aload 1
aload 0
invokestatic com/a/b/d/mq/a(Ljava/util/Collection;Ljava/lang/Iterable;)Z
pop
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method private static f()Ljava/util/PriorityQueue;
new java/util/PriorityQueue
dup
invokespecial java/util/PriorityQueue/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method private static f(Ljava/lang/Iterable;)Ljava/util/PriorityQueue;
aload 0
instanceof java/util/Collection
ifeq L0
new java/util/PriorityQueue
dup
aload 0
invokestatic com/a/b/d/cm/a(Ljava/lang/Iterable;)Ljava/util/Collection;
invokespecial java/util/PriorityQueue/<init>(Ljava/util/Collection;)V
areturn
L0:
new java/util/PriorityQueue
dup
invokespecial java/util/PriorityQueue/<init>()V
astore 1
aload 1
aload 0
invokestatic com/a/b/d/mq/a(Ljava/util/Collection;Ljava/lang/Iterable;)Z
pop
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method private static g()Ljava/util/concurrent/SynchronousQueue;
new java/util/concurrent/SynchronousQueue
dup
invokespecial java/util/concurrent/SynchronousQueue/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method
