.bytecode 50.0
.class final synchronized com/a/b/d/er
.super com/a/b/d/ep
.implements java/io/Serializable

.field private static final 'a' Lcom/a/b/d/er;

.field private static final 'b' J = 0L


.method static <clinit>()V
new com/a/b/d/er
dup
invokespecial com/a/b/d/er/<init>()V
putstatic com/a/b/d/er/a Lcom/a/b/d/er;
return
.limit locals 0
.limit stack 2
.end method

.method private <init>()V
aload 0
invokespecial com/a/b/d/ep/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/Integer;Ljava/lang/Integer;)J
aload 1
invokevirtual java/lang/Integer/intValue()I
i2l
aload 0
invokevirtual java/lang/Integer/intValue()I
i2l
lsub
lreturn
.limit locals 2
.limit stack 4
.end method

.method private static a(Ljava/lang/Integer;)Ljava/lang/Integer;
aload 0
invokevirtual java/lang/Integer/intValue()I
istore 1
iload 1
ldc_w 2147483647
if_icmpne L0
aconst_null
areturn
L0:
iload 1
iconst_1
iadd
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static b(Ljava/lang/Integer;)Ljava/lang/Integer;
aload 0
invokevirtual java/lang/Integer/intValue()I
istore 1
iload 1
ldc_w -2147483648
if_icmpne L0
aconst_null
areturn
L0:
iload 1
iconst_1
isub
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
areturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic c()Lcom/a/b/d/er;
getstatic com/a/b/d/er/a Lcom/a/b/d/er;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static d()Ljava/lang/Integer;
ldc_w -2147483648
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static e()Ljava/lang/Integer;
ldc_w 2147483647
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static f()Ljava/lang/Object;
getstatic com/a/b/d/er/a Lcom/a/b/d/er;
areturn
.limit locals 0
.limit stack 1
.end method

.method public final synthetic a(Ljava/lang/Comparable;Ljava/lang/Comparable;)J
aload 1
checkcast java/lang/Integer
astore 1
aload 2
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
i2l
aload 1
invokevirtual java/lang/Integer/intValue()I
i2l
lsub
lreturn
.limit locals 3
.limit stack 4
.end method

.method public final synthetic a()Ljava/lang/Comparable;
ldc_w -2147483648
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final synthetic a(Ljava/lang/Comparable;)Ljava/lang/Comparable;
aload 1
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
istore 2
iload 2
ldc_w 2147483647
if_icmpne L0
aconst_null
areturn
L0:
iload 2
iconst_1
iadd
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
areturn
.limit locals 3
.limit stack 2
.end method

.method public final synthetic b()Ljava/lang/Comparable;
ldc_w 2147483647
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final synthetic b(Ljava/lang/Comparable;)Ljava/lang/Comparable;
aload 1
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
istore 2
iload 2
ldc_w -2147483648
if_icmpne L0
aconst_null
areturn
L0:
iload 2
iconst_1
isub
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
areturn
.limit locals 3
.limit stack 2
.end method

.method public final toString()Ljava/lang/String;
ldc "DiscreteDomain.integers()"
areturn
.limit locals 1
.limit stack 1
.end method
