.bytecode 50.0
.class synchronized com/a/b/d/qb
.super com/a/b/d/gs

.field private final 'a' Ljava/util/Map;

.field final 'b' Lcom/a/b/d/pn;

.field private transient 'c' Ljava/util/Set;

.method <init>(Ljava/util/Map;Lcom/a/b/d/pn;)V
aload 0
invokespecial com/a/b/d/gs/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Map
putfield com/a/b/d/qb/a Ljava/util/Map;
aload 0
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/d/pn
putfield com/a/b/d/qb/b Lcom/a/b/d/pn;
return
.limit locals 3
.limit stack 2
.end method

.method protected a()Ljava/util/Map;
aload 0
getfield com/a/b/d/qb/a Ljava/util/Map;
areturn
.limit locals 1
.limit stack 1
.end method

.method public entrySet()Ljava/util/Set;
aload 0
getfield com/a/b/d/qb/c Ljava/util/Set;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
aload 0
getfield com/a/b/d/qb/a Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
aload 0
getfield com/a/b/d/qb/b Lcom/a/b/d/pn;
invokestatic com/a/b/d/po/a(Ljava/util/Set;Lcom/a/b/d/pn;)Ljava/util/Set;
astore 1
aload 0
aload 1
putfield com/a/b/d/qb/c Ljava/util/Set;
L0:
aload 1
areturn
.limit locals 3
.limit stack 2
.end method

.method protected synthetic k_()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/qb/a()Ljava/util/Map;
areturn
.limit locals 1
.limit stack 1
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
aload 0
getfield com/a/b/d/qb/b Lcom/a/b/d/pn;
aload 1
aload 2
invokeinterface com/a/b/d/pn/a(Ljava/lang/Object;Ljava/lang/Object;)V 2
aload 0
getfield com/a/b/d/qb/a Ljava/util/Map;
aload 1
aload 2
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
areturn
.limit locals 3
.limit stack 3
.end method

.method public putAll(Ljava/util/Map;)V
aload 0
getfield com/a/b/d/qb/a Ljava/util/Map;
astore 2
aload 0
getfield com/a/b/d/qb/b Lcom/a/b/d/pn;
astore 3
new java/util/LinkedHashMap
dup
aload 1
invokespecial java/util/LinkedHashMap/<init>(Ljava/util/Map;)V
astore 1
aload 1
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 4
L0:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 5
aload 3
aload 5
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
aload 5
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
invokeinterface com/a/b/d/pn/a(Ljava/lang/Object;Ljava/lang/Object;)V 2
goto L0
L1:
aload 2
aload 1
invokeinterface java/util/Map/putAll(Ljava/util/Map;)V 1
return
.limit locals 6
.limit stack 3
.end method
