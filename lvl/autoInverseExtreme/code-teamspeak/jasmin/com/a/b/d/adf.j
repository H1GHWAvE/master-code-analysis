.bytecode 50.0
.class final synchronized com/a/b/d/adf
.super com/a/b/d/adn
.implements java/util/Map$Entry
.annotation invisible Lcom/a/b/a/c;
a s = "works but is needed only for NavigableMap"
.end annotation

.field private static final 'a' J = 0L


.method <init>(Ljava/util/Map$Entry;Ljava/lang/Object;)V
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
aload 2
invokespecial com/a/b/d/adn/<init>(Ljava/lang/Object;Ljava/lang/Object;)V
return
.limit locals 3
.limit stack 3
.end method

.method private a()Ljava/util/Map$Entry;
aload 0
invokespecial com/a/b/d/adn/d()Ljava/lang/Object;
checkcast java/util/Map$Entry
areturn
.limit locals 1
.limit stack 1
.end method

.method final volatile synthetic d()Ljava/lang/Object;
aload 0
invokespecial com/a/b/d/adn/d()Ljava/lang/Object;
checkcast java/util/Map$Entry
areturn
.limit locals 1
.limit stack 1
.end method

.method public final equals(Ljava/lang/Object;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adf/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokespecial com/a/b/d/adn/d()Ljava/lang/Object;
checkcast java/util/Map$Entry
aload 1
invokeinterface java/util/Map$Entry/equals(Ljava/lang/Object;)Z 1
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method public final getKey()Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adf/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
invokespecial com/a/b/d/adn/d()Ljava/lang/Object;
checkcast java/util/Map$Entry
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
astore 2
aload 1
monitorexit
L1:
aload 2
areturn
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 1
.end method

.method public final getValue()Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adf/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
invokespecial com/a/b/d/adn/d()Ljava/lang/Object;
checkcast java/util/Map$Entry
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
astore 2
aload 1
monitorexit
L1:
aload 2
areturn
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 1
.end method

.method public final hashCode()I
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adf/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokespecial com/a/b/d/adn/d()Ljava/lang/Object;
checkcast java/util/Map$Entry
invokeinterface java/util/Map$Entry/hashCode()I 0
istore 1
aload 2
monitorexit
L1:
iload 1
ireturn
L2:
astore 3
L3:
aload 2
monitorexit
L4:
aload 3
athrow
.limit locals 4
.limit stack 1
.end method

.method public final setValue(Ljava/lang/Object;)Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adf/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokespecial com/a/b/d/adn/d()Ljava/lang/Object;
checkcast java/util/Map$Entry
aload 1
invokeinterface java/util/Map$Entry/setValue(Ljava/lang/Object;)Ljava/lang/Object; 1
astore 1
aload 2
monitorexit
L1:
aload 1
areturn
L2:
astore 1
L3:
aload 2
monitorexit
L4:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method
