.bytecode 50.0
.class synchronized com/a/b/d/fi
.super com/a/b/d/an
.implements com/a/b/d/ga
.annotation invisible Lcom/a/b/a/b;
.end annotation

.field final 'a' Lcom/a/b/d/vi;

.field final 'b' Lcom/a/b/b/co;

.method <init>(Lcom/a/b/d/vi;Lcom/a/b/b/co;)V
aload 0
invokespecial com/a/b/d/an/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/d/vi
putfield com/a/b/d/fi/a Lcom/a/b/d/vi;
aload 0
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/b/co
putfield com/a/b/d/fi/b Lcom/a/b/b/co;
return
.limit locals 3
.limit stack 2
.end method

.method static a(Ljava/util/Collection;Lcom/a/b/b/co;)Ljava/util/Collection;
aload 0
instanceof java/util/Set
ifeq L0
aload 0
checkcast java/util/Set
aload 1
invokestatic com/a/b/d/aad/a(Ljava/util/Set;Lcom/a/b/b/co;)Ljava/util/Set;
areturn
L0:
aload 0
aload 1
invokestatic com/a/b/d/cm/a(Ljava/util/Collection;Lcom/a/b/b/co;)Ljava/util/Collection;
areturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Lcom/a/b/d/fi;Ljava/lang/Object;Ljava/lang/Object;)Z
aload 0
getfield com/a/b/d/fi/b Lcom/a/b/b/co;
aload 1
aload 2
invokestatic com/a/b/d/sz/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;
invokeinterface com/a/b/b/co/a(Ljava/lang/Object;)Z 1
ireturn
.limit locals 3
.limit stack 3
.end method

.method private d()Ljava/util/Collection;
aload 0
getfield com/a/b/d/fi/a Lcom/a/b/d/vi;
instanceof com/a/b/d/aac
ifeq L0
invokestatic java/util/Collections/emptySet()Ljava/util/Set;
areturn
L0:
invokestatic java/util/Collections/emptyList()Ljava/util/List;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d(Ljava/lang/Object;Ljava/lang/Object;)Z
aload 0
getfield com/a/b/d/fi/b Lcom/a/b/b/co;
aload 1
aload 2
invokestatic com/a/b/d/sz/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;
invokeinterface com/a/b/b/co/a(Ljava/lang/Object;)Z 1
ireturn
.limit locals 3
.limit stack 3
.end method

.method public a()Lcom/a/b/d/vi;
aload 0
getfield com/a/b/d/fi/a Lcom/a/b/d/vi;
areturn
.limit locals 1
.limit stack 1
.end method

.method final a(Lcom/a/b/b/co;)Z
aload 0
getfield com/a/b/d/fi/a Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/b()Ljava/util/Map; 0
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 3
iconst_0
istore 2
L0:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 4
aload 4
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
astore 5
aload 4
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast java/util/Collection
new com/a/b/d/fr
dup
aload 0
aload 5
invokespecial com/a/b/d/fr/<init>(Lcom/a/b/d/fi;Ljava/lang/Object;)V
invokestatic com/a/b/d/fi/a(Ljava/util/Collection;Lcom/a/b/b/co;)Ljava/util/Collection;
astore 6
aload 6
invokeinterface java/util/Collection/isEmpty()Z 0
ifne L2
aload 1
aload 5
aload 6
invokestatic com/a/b/d/sz/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;
invokeinterface com/a/b/b/co/a(Ljava/lang/Object;)Z 1
ifeq L2
aload 6
invokeinterface java/util/Collection/size()I 0
aload 4
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast java/util/Collection
invokeinterface java/util/Collection/size()I 0
if_icmpne L3
aload 3
invokeinterface java/util/Iterator/remove()V 0
L4:
iconst_1
istore 2
L5:
goto L0
L3:
aload 6
invokeinterface java/util/Collection/clear()V 0
goto L4
L1:
iload 2
ireturn
L2:
goto L5
.limit locals 7
.limit stack 5
.end method

.method public final c()Lcom/a/b/b/co;
aload 0
getfield com/a/b/d/fi/b Lcom/a/b/b/co;
areturn
.limit locals 1
.limit stack 1
.end method

.method public c(Ljava/lang/Object;)Ljava/util/Collection;
aload 0
getfield com/a/b/d/fi/a Lcom/a/b/d/vi;
aload 1
invokeinterface com/a/b/d/vi/c(Ljava/lang/Object;)Ljava/util/Collection; 1
new com/a/b/d/fr
dup
aload 0
aload 1
invokespecial com/a/b/d/fr/<init>(Lcom/a/b/d/fi;Ljava/lang/Object;)V
invokestatic com/a/b/d/fi/a(Ljava/util/Collection;Lcom/a/b/b/co;)Ljava/util/Collection;
areturn
.limit locals 2
.limit stack 5
.end method

.method public d(Ljava/lang/Object;)Ljava/util/Collection;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokevirtual com/a/b/d/fi/b()Ljava/util/Map;
aload 1
invokeinterface java/util/Map/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
astore 2
aload 0
getfield com/a/b/d/fi/a Lcom/a/b/d/vi;
instanceof com/a/b/d/aac
ifeq L0
invokestatic java/util/Collections/emptySet()Ljava/util/Set;
astore 1
L1:
aload 2
aload 1
invokestatic com/a/b/b/ca/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Collection
areturn
L0:
invokestatic java/util/Collections/emptyList()Ljava/util/List;
astore 1
goto L1
.limit locals 3
.limit stack 2
.end method

.method public final f()I
aload 0
invokevirtual com/a/b/d/fi/k()Ljava/util/Collection;
invokeinterface java/util/Collection/size()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final f(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokevirtual com/a/b/d/fi/b()Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final g()V
aload 0
invokevirtual com/a/b/d/fi/k()Ljava/util/Collection;
invokeinterface java/util/Collection/clear()V 0
return
.limit locals 1
.limit stack 1
.end method

.method final l()Ljava/util/Iterator;
new java/lang/AssertionError
dup
ldc "should never be called"
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method final m()Ljava/util/Map;
new com/a/b/d/fj
dup
aload 0
invokespecial com/a/b/d/fj/<init>(Lcom/a/b/d/fi;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method o()Ljava/util/Collection;
aload 0
getfield com/a/b/d/fi/a Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/k()Ljava/util/Collection; 0
aload 0
getfield com/a/b/d/fi/b Lcom/a/b/b/co;
invokestatic com/a/b/d/fi/a(Ljava/util/Collection;Lcom/a/b/b/co;)Ljava/util/Collection;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final p()Ljava/util/Set;
aload 0
invokevirtual com/a/b/d/fi/b()Ljava/util/Map;
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method final r()Lcom/a/b/d/xc;
new com/a/b/d/fo
dup
aload 0
invokespecial com/a/b/d/fo/<init>(Lcom/a/b/d/fi;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method final s()Ljava/util/Collection;
new com/a/b/d/gb
dup
aload 0
invokespecial com/a/b/d/gb/<init>(Lcom/a/b/d/ga;)V
areturn
.limit locals 1
.limit stack 3
.end method
