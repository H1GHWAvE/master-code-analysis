.bytecode 50.0
.class public final synchronized com/a/b/d/nj
.super java/lang/Object
.annotation invisible Lcom/a/b/a/b;
b Z = 1
.end annotation

.field static final 'a' Lcom/a/b/d/agj;

.field private static final 'b' Ljava/util/Iterator;

.method static <clinit>()V
new com/a/b/d/nk
dup
invokespecial com/a/b/d/nk/<init>()V
putstatic com/a/b/d/nj/a Lcom/a/b/d/agj;
new com/a/b/d/nq
dup
invokespecial com/a/b/d/nq/<init>()V
putstatic com/a/b/d/nj/b Ljava/util/Iterator;
return
.limit locals 0
.limit stack 2
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a()Lcom/a/b/d/agi;
.annotation visible Ljava/lang/Deprecated;
.end annotation
getstatic com/a/b/d/nj/a Lcom/a/b/d/agj;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static a(Lcom/a/b/d/agi;)Lcom/a/b/d/agi;
.annotation visible Ljava/lang/Deprecated;
.end annotation
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/d/agi
areturn
.limit locals 1
.limit stack 1
.end method

.method public static a(Ljava/lang/Iterable;Ljava/util/Comparator;)Lcom/a/b/d/agi;
.annotation invisible Lcom/a/b/a/a;
.end annotation
aload 0
ldc "iterators"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
ldc "comparator"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/d/ny
dup
aload 0
aload 1
invokespecial com/a/b/d/ny/<init>(Ljava/lang/Iterable;Ljava/util/Comparator;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public static a(Ljava/lang/Object;)Lcom/a/b/d/agi;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
new com/a/b/d/nn
dup
aload 0
invokespecial com/a/b/d/nn/<init>(Ljava/lang/Object;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Ljava/util/Enumeration;)Lcom/a/b/d/agi;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/d/no
dup
aload 0
invokespecial com/a/b/d/no/<init>(Ljava/util/Enumeration;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public static a(Ljava/util/Iterator;)Lcom/a/b/d/agi;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
instanceof com/a/b/d/agi
ifeq L0
aload 0
checkcast com/a/b/d/agi
areturn
L0:
new com/a/b/d/nr
dup
aload 0
invokespecial com/a/b/d/nr/<init>(Ljava/util/Iterator;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public static a(Ljava/util/Iterator;I)Lcom/a/b/d/agi;
aload 0
iload 1
iconst_0
invokestatic com/a/b/d/nj/a(Ljava/util/Iterator;IZ)Lcom/a/b/d/agi;
areturn
.limit locals 2
.limit stack 3
.end method

.method private static a(Ljava/util/Iterator;IZ)Lcom/a/b/d/agi;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
iload 1
ifle L0
iconst_1
istore 3
L1:
iload 3
invokestatic com/a/b/b/cn/a(Z)V
new com/a/b/d/nu
dup
aload 0
iload 1
iload 2
invokespecial com/a/b/d/nu/<init>(Ljava/util/Iterator;IZ)V
areturn
L0:
iconst_0
istore 3
goto L1
.limit locals 4
.limit stack 5
.end method

.method public static a(Ljava/util/Iterator;Ljava/lang/Class;)Lcom/a/b/d/agi;
.annotation invisible Lcom/a/b/a/c;
a s = "Class.isInstance"
.end annotation
aload 0
aload 1
invokestatic com/a/b/b/cp/a(Ljava/lang/Class;)Lcom/a/b/b/co;
invokestatic com/a/b/d/nj/b(Ljava/util/Iterator;Lcom/a/b/b/co;)Lcom/a/b/d/agi;
areturn
.limit locals 2
.limit stack 2
.end method

.method public static transient a([Ljava/lang/Object;)Lcom/a/b/d/agi;
aload 0
iconst_0
aload 0
arraylength
iconst_0
invokestatic com/a/b/d/nj/a([Ljava/lang/Object;III)Lcom/a/b/d/agj;
areturn
.limit locals 1
.limit stack 4
.end method

.method static a([Ljava/lang/Object;III)Lcom/a/b/d/agj;
iload 2
iflt L0
iconst_1
istore 4
L1:
iload 4
invokestatic com/a/b/b/cn/a(Z)V
iload 1
iload 1
iload 2
iadd
aload 0
arraylength
invokestatic com/a/b/b/cn/a(III)V
iload 3
iload 2
invokestatic com/a/b/b/cn/b(II)I
pop
iload 2
ifne L2
getstatic com/a/b/d/nj/a Lcom/a/b/d/agj;
areturn
L0:
iconst_0
istore 4
goto L1
L2:
new com/a/b/d/nm
dup
iload 2
iload 3
aload 0
iload 1
invokespecial com/a/b/d/nm/<init>(II[Ljava/lang/Object;I)V
areturn
.limit locals 5
.limit stack 6
.end method

.method private static a(Lcom/a/b/d/yi;)Lcom/a/b/d/yi;
.annotation visible Ljava/lang/Deprecated;
.end annotation
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/d/yi
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/util/Iterator;ILjava/lang/Object;)Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 3 Ljavax/annotation/Nullable;
.end annotation
iload 1
invokestatic com/a/b/d/nj/a(I)V
aload 0
iload 1
invokestatic com/a/b/d/nj/d(Ljava/util/Iterator;I)I
pop
aload 0
aload 2
invokestatic com/a/b/d/nj/d(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 3
.limit stack 2
.end method

.method public static a(Ljava/util/Iterator;Lcom/a/b/b/co;Ljava/lang/Object;)Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 3 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokestatic com/a/b/d/nj/b(Ljava/util/Iterator;Lcom/a/b/b/co;)Lcom/a/b/d/agi;
aload 2
invokestatic com/a/b/d/nj/d(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 3
.limit stack 2
.end method

.method public static a(Ljava/lang/Iterable;)Ljava/util/Iterator;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/d/ns
dup
aload 0
invokespecial com/a/b/d/ns/<init>(Ljava/lang/Iterable;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public static a(Ljava/util/Iterator;Lcom/a/b/b/bj;)Ljava/util/Iterator;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/d/nw
dup
aload 0
aload 1
invokespecial com/a/b/d/nw/<init>(Ljava/util/Iterator;Lcom/a/b/b/bj;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static a(Ljava/util/Iterator;Ljava/util/Iterator;Ljava/util/Iterator;)Ljava/util/Iterator;
aload 0
aload 1
aload 2
invokestatic com/a/b/d/jl/a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jl;
invokevirtual com/a/b/d/jl/c()Lcom/a/b/d/agi;
invokestatic com/a/b/d/nj/e(Ljava/util/Iterator;)Ljava/util/Iterator;
areturn
.limit locals 3
.limit stack 3
.end method

.method private static a(Ljava/util/Iterator;Ljava/util/Iterator;Ljava/util/Iterator;Ljava/util/Iterator;)Ljava/util/Iterator;
aload 0
aload 1
aload 2
aload 3
invokestatic com/a/b/d/jl/a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jl;
invokevirtual com/a/b/d/jl/c()Lcom/a/b/d/agi;
invokestatic com/a/b/d/nj/e(Ljava/util/Iterator;)Ljava/util/Iterator;
areturn
.limit locals 4
.limit stack 4
.end method

.method private static transient a([Ljava/util/Iterator;)Ljava/util/Iterator;
aload 0
invokestatic com/a/b/d/jl/a([Ljava/lang/Object;)Lcom/a/b/d/jl;
invokevirtual com/a/b/d/jl/c()Lcom/a/b/d/agi;
invokestatic com/a/b/d/nj/e(Ljava/util/Iterator;)Ljava/util/Iterator;
areturn
.limit locals 1
.limit stack 1
.end method

.method static a(I)V
iload 0
ifge L0
new java/lang/IndexOutOfBoundsException
dup
new java/lang/StringBuilder
dup
bipush 43
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "position ("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 0
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ") must not be negative"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IndexOutOfBoundsException/<init>(Ljava/lang/String;)V
athrow
L0:
return
.limit locals 1
.limit stack 5
.end method

.method public static a(Ljava/util/Collection;Ljava/util/Iterator;)Z
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
iconst_0
istore 2
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
iload 2
aload 0
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokeinterface java/util/Collection/add(Ljava/lang/Object;)Z 1
ior
istore 2
goto L0
L1:
iload 2
ireturn
.limit locals 3
.limit stack 3
.end method

.method public static a(Ljava/util/Iterator;Lcom/a/b/b/co;)Z
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
iconst_0
istore 2
L0:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokeinterface com/a/b/b/co/a(Ljava/lang/Object;)Z 1
ifeq L0
aload 0
invokeinterface java/util/Iterator/remove()V 0
iconst_1
istore 2
goto L0
L1:
iload 2
ireturn
.limit locals 3
.limit stack 2
.end method

.method public static a(Ljava/util/Iterator;Ljava/lang/Object;)Z
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokestatic com/a/b/b/cp/a(Ljava/lang/Object;)Lcom/a/b/b/co;
invokestatic com/a/b/d/nj/c(Ljava/util/Iterator;Lcom/a/b/b/co;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public static a(Ljava/util/Iterator;Ljava/util/Collection;)Z
aload 0
aload 1
invokestatic com/a/b/b/cp/a(Ljava/util/Collection;)Lcom/a/b/b/co;
invokestatic com/a/b/d/nj/a(Ljava/util/Iterator;Lcom/a/b/b/co;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public static a(Ljava/util/Iterator;Ljava/util/Iterator;)Z
L0:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifne L2
L3:
iconst_0
ireturn
L2:
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokestatic com/a/b/b/ce/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifne L0
iconst_0
ireturn
L1:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifne L3
iconst_1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public static b(Ljava/util/Iterator;)I
iconst_0
istore 1
L0:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
pop
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public static b(Ljava/util/Iterator;I)Lcom/a/b/d/agi;
aload 0
iload 1
iconst_1
invokestatic com/a/b/d/nj/a(Ljava/util/Iterator;IZ)Lcom/a/b/d/agi;
areturn
.limit locals 2
.limit stack 3
.end method

.method public static b(Ljava/util/Iterator;Lcom/a/b/b/co;)Lcom/a/b/d/agi;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/d/nv
dup
aload 0
aload 1
invokespecial com/a/b/d/nv/<init>(Ljava/util/Iterator;Lcom/a/b/b/co;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public static b(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 0
invokestatic com/a/b/d/nj/d(Ljava/util/Iterator;)Ljava/lang/Object;
astore 1
L0:
aload 1
areturn
.limit locals 2
.limit stack 1
.end method

.method static b()Ljava/util/Iterator;
getstatic com/a/b/d/nj/b Ljava/util/Iterator;
areturn
.limit locals 0
.limit stack 1
.end method

.method public static b(Ljava/util/Iterator;Ljava/util/Iterator;)Ljava/util/Iterator;
aload 0
aload 1
invokestatic com/a/b/d/jl/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jl;
invokevirtual com/a/b/d/jl/c()Lcom/a/b/d/agi;
invokestatic com/a/b/d/nj/e(Ljava/util/Iterator;)Ljava/util/Iterator;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static transient b([Ljava/lang/Object;)Ljava/util/Iterator;
aload 0
invokestatic com/a/b/d/ov/a([Ljava/lang/Object;)Ljava/util/ArrayList;
invokestatic com/a/b/d/nj/a(Ljava/lang/Iterable;)Ljava/util/Iterator;
areturn
.limit locals 1
.limit stack 1
.end method

.method public static b(Ljava/util/Iterator;Ljava/util/Collection;)Z
aload 0
aload 1
invokestatic com/a/b/b/cp/a(Ljava/util/Collection;)Lcom/a/b/b/co;
invokestatic com/a/b/b/cp/a(Lcom/a/b/b/co;)Lcom/a/b/b/co;
invokestatic com/a/b/d/nj/a(Ljava/util/Iterator;Lcom/a/b/b/co;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static b(Ljava/util/Iterator;Ljava/lang/Class;)[Ljava/lang/Object;
.annotation invisible Lcom/a/b/a/c;
a s = "Array.newInstance(Class, int)"
.end annotation
aload 0
invokestatic com/a/b/d/ov/a(Ljava/util/Iterator;)Ljava/util/ArrayList;
aload 1
invokestatic com/a/b/d/mq/a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method public static c(Ljava/util/Iterator;Ljava/lang/Object;)I
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokestatic com/a/b/b/cp/a(Ljava/lang/Object;)Lcom/a/b/b/co;
invokestatic com/a/b/d/nj/b(Ljava/util/Iterator;Lcom/a/b/b/co;)Lcom/a/b/d/agi;
invokestatic com/a/b/d/nj/b(Ljava/util/Iterator;)I
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static c()Lcom/a/b/d/agj;
getstatic com/a/b/d/nj/a Lcom/a/b/d/agj;
areturn
.limit locals 0
.limit stack 1
.end method

.method public static c(Ljava/util/Iterator;I)Ljava/lang/Object;
iload 1
invokestatic com/a/b/d/nj/a(I)V
aload 0
iload 1
invokestatic com/a/b/d/nj/d(Ljava/util/Iterator;I)I
istore 2
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifne L0
new java/lang/IndexOutOfBoundsException
dup
new java/lang/StringBuilder
dup
bipush 91
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "position ("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ") must be less than the number of elements that remained ("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IndexOutOfBoundsException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
areturn
.limit locals 3
.limit stack 5
.end method

.method public static c(Ljava/util/Iterator;)Ljava/lang/String;
getstatic com/a/b/d/cm/a Lcom/a/b/b/bv;
new java/lang/StringBuilder
dup
ldc "["
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual com/a/b/b/bv/a(Ljava/lang/StringBuilder;Ljava/util/Iterator;)Ljava/lang/StringBuilder;
bipush 93
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 4
.end method

.method public static c(Ljava/util/Iterator;Lcom/a/b/b/co;)Z
aload 0
aload 1
invokestatic com/a/b/d/nj/g(Ljava/util/Iterator;Lcom/a/b/b/co;)I
iconst_m1
if_icmpeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public static d(Ljava/util/Iterator;I)I
iconst_0
istore 2
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
iload 1
iflt L0
iconst_1
istore 3
L1:
iload 3
ldc "numberToAdvance must be nonnegative"
invokestatic com/a/b/b/cn/a(ZLjava/lang/Object;)V
L2:
iload 2
iload 1
if_icmpge L3
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
pop
iload 2
iconst_1
iadd
istore 2
goto L2
L0:
iconst_0
istore 3
goto L1
L3:
iload 2
ireturn
.limit locals 4
.limit stack 2
.end method

.method public static d(Ljava/util/Iterator;)Ljava/lang/Object;
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
astore 3
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifne L0
aload 3
areturn
L0:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 2
aload 3
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 3
aload 2
new java/lang/StringBuilder
dup
aload 3
invokevirtual java/lang/String/length()I
bipush 31
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "expected one element but was: <"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
iconst_0
istore 1
L1:
iload 1
iconst_4
if_icmpge L2
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 3
aload 2
new java/lang/StringBuilder
dup
aload 3
invokevirtual java/lang/String/length()I
iconst_2
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc ", "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 2
ldc ", ..."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L3:
aload 2
bipush 62
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
new java/lang/IllegalArgumentException
dup
aload 2
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 4
.limit stack 5
.end method

.method public static d(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
astore 1
L0:
aload 1
areturn
.limit locals 2
.limit stack 1
.end method

.method public static d(Ljava/util/Iterator;Lcom/a/b/b/co;)Z
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
L0:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokeinterface com/a/b/b/co/a(Ljava/lang/Object;)Z 1
ifne L0
iconst_0
ireturn
L1:
iconst_1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public static e(Ljava/util/Iterator;Lcom/a/b/b/co;)Ljava/lang/Object;
aload 0
aload 1
invokestatic com/a/b/d/nj/b(Ljava/util/Iterator;Lcom/a/b/b/co;)Lcom/a/b/d/agi;
invokevirtual com/a/b/d/agi/next()Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method public static e(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 0
invokestatic com/a/b/d/nj/f(Ljava/util/Iterator;)Ljava/lang/Object;
astore 1
L0:
aload 1
areturn
.limit locals 2
.limit stack 1
.end method

.method public static e(Ljava/util/Iterator;)Ljava/util/Iterator;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/d/nt
dup
aload 0
invokespecial com/a/b/d/nt/<init>(Ljava/util/Iterator;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public static e(Ljava/util/Iterator;I)Ljava/util/Iterator;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
iload 1
iflt L0
iconst_1
istore 2
L1:
iload 2
ldc "limit is negative"
invokestatic com/a/b/b/cn/a(ZLjava/lang/Object;)V
new com/a/b/d/nx
dup
iload 1
aload 0
invokespecial com/a/b/d/nx/<init>(ILjava/util/Iterator;)V
areturn
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 4
.end method

.method public static f(Ljava/util/Iterator;Lcom/a/b/b/co;)Lcom/a/b/b/ci;
aload 0
aload 1
invokestatic com/a/b/d/nj/b(Ljava/util/Iterator;Lcom/a/b/b/co;)Lcom/a/b/d/agi;
astore 0
aload 0
invokevirtual com/a/b/d/agi/hasNext()Z
ifeq L0
aload 0
invokevirtual com/a/b/d/agi/next()Ljava/lang/Object;
invokestatic com/a/b/b/ci/b(Ljava/lang/Object;)Lcom/a/b/b/ci;
areturn
L0:
invokestatic com/a/b/b/ci/f()Lcom/a/b/b/ci;
areturn
.limit locals 2
.limit stack 2
.end method

.method public static f(Ljava/util/Iterator;)Ljava/lang/Object;
L0:
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
astore 1
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifne L0
aload 1
areturn
.limit locals 2
.limit stack 1
.end method

.method public static g(Ljava/util/Iterator;Lcom/a/b/b/co;)I
aload 1
ldc "predicate"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
iconst_0
istore 2
L0:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokeinterface com/a/b/b/co/a(Ljava/lang/Object;)Z 1
ifeq L2
iload 2
ireturn
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
iconst_m1
ireturn
.limit locals 3
.limit stack 2
.end method

.method public static g(Ljava/util/Iterator;)Ljava/util/Iterator;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/d/nl
dup
aload 0
invokespecial com/a/b/d/nl/<init>(Ljava/util/Iterator;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method static h(Ljava/util/Iterator;)Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
astore 1
aload 0
invokeinterface java/util/Iterator/remove()V 0
aload 1
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 1
.end method

.method static i(Ljava/util/Iterator;)V
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
L0:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
pop
aload 0
invokeinterface java/util/Iterator/remove()V 0
goto L0
L1:
return
.limit locals 1
.limit stack 1
.end method

.method public static j(Ljava/util/Iterator;)Lcom/a/b/d/yi;
aload 0
instanceof com/a/b/d/oa
ifeq L0
aload 0
checkcast com/a/b/d/oa
areturn
L0:
new com/a/b/d/oa
dup
aload 0
invokespecial com/a/b/d/oa/<init>(Ljava/util/Iterator;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method static k(Ljava/util/Iterator;)Ljava/util/ListIterator;
aload 0
checkcast java/util/ListIterator
areturn
.limit locals 1
.limit stack 1
.end method

.method private static l(Ljava/util/Iterator;)Ljava/util/Enumeration;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/d/np
dup
aload 0
invokespecial com/a/b/d/np/<init>(Ljava/util/Iterator;)V
areturn
.limit locals 1
.limit stack 3
.end method
