.bytecode 50.0
.class final synchronized com/a/b/d/rp
.super java/util/AbstractQueue

.field final 'a' Lcom/a/b/d/rz;

.method <init>()V
aload 0
invokespecial java/util/AbstractQueue/<init>()V
aload 0
new com/a/b/d/rq
dup
aload 0
invokespecial com/a/b/d/rq/<init>(Lcom/a/b/d/rp;)V
putfield com/a/b/d/rp/a Lcom/a/b/d/rz;
return
.limit locals 1
.limit stack 4
.end method

.method private a()Lcom/a/b/d/rz;
aload 0
getfield com/a/b/d/rp/a Lcom/a/b/d/rz;
invokeinterface com/a/b/d/rz/h()Lcom/a/b/d/rz; 0
astore 2
aload 2
astore 1
aload 2
aload 0
getfield com/a/b/d/rp/a Lcom/a/b/d/rz;
if_acmpne L0
aconst_null
astore 1
L0:
aload 1
areturn
.limit locals 3
.limit stack 2
.end method

.method private a(Lcom/a/b/d/rz;)Z
aload 1
invokeinterface com/a/b/d/rz/i()Lcom/a/b/d/rz; 0
aload 1
invokeinterface com/a/b/d/rz/h()Lcom/a/b/d/rz; 0
invokestatic com/a/b/d/qy/b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)V
aload 0
getfield com/a/b/d/rp/a Lcom/a/b/d/rz;
invokeinterface com/a/b/d/rz/i()Lcom/a/b/d/rz; 0
aload 1
invokestatic com/a/b/d/qy/b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)V
aload 1
aload 0
getfield com/a/b/d/rp/a Lcom/a/b/d/rz;
invokestatic com/a/b/d/qy/b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)V
iconst_1
ireturn
.limit locals 2
.limit stack 2
.end method

.method private b()Lcom/a/b/d/rz;
aload 0
getfield com/a/b/d/rp/a Lcom/a/b/d/rz;
invokeinterface com/a/b/d/rz/h()Lcom/a/b/d/rz; 0
astore 1
aload 1
aload 0
getfield com/a/b/d/rp/a Lcom/a/b/d/rz;
if_acmpne L0
aconst_null
areturn
L0:
aload 0
aload 1
invokevirtual com/a/b/d/rp/remove(Ljava/lang/Object;)Z
pop
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public final clear()V
aload 0
getfield com/a/b/d/rp/a Lcom/a/b/d/rz;
invokeinterface com/a/b/d/rz/h()Lcom/a/b/d/rz; 0
astore 1
L0:
aload 1
aload 0
getfield com/a/b/d/rp/a Lcom/a/b/d/rz;
if_acmpeq L1
aload 1
invokeinterface com/a/b/d/rz/h()Lcom/a/b/d/rz; 0
astore 2
aload 1
invokestatic com/a/b/d/qy/c(Lcom/a/b/d/rz;)V
aload 2
astore 1
goto L0
L1:
aload 0
getfield com/a/b/d/rp/a Lcom/a/b/d/rz;
aload 0
getfield com/a/b/d/rp/a Lcom/a/b/d/rz;
invokeinterface com/a/b/d/rz/c(Lcom/a/b/d/rz;)V 1
aload 0
getfield com/a/b/d/rp/a Lcom/a/b/d/rz;
aload 0
getfield com/a/b/d/rp/a Lcom/a/b/d/rz;
invokeinterface com/a/b/d/rz/d(Lcom/a/b/d/rz;)V 1
return
.limit locals 3
.limit stack 2
.end method

.method public final contains(Ljava/lang/Object;)Z
aload 1
checkcast com/a/b/d/rz
invokeinterface com/a/b/d/rz/h()Lcom/a/b/d/rz; 0
getstatic com/a/b/d/ry/a Lcom/a/b/d/ry;
if_acmpeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final isEmpty()Z
aload 0
getfield com/a/b/d/rp/a Lcom/a/b/d/rz;
invokeinterface com/a/b/d/rz/h()Lcom/a/b/d/rz; 0
aload 0
getfield com/a/b/d/rp/a Lcom/a/b/d/rz;
if_acmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final iterator()Ljava/util/Iterator;
new com/a/b/d/rr
dup
aload 0
aload 0
invokespecial com/a/b/d/rp/a()Lcom/a/b/d/rz;
invokespecial com/a/b/d/rr/<init>(Lcom/a/b/d/rp;Lcom/a/b/d/rz;)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public final synthetic offer(Ljava/lang/Object;)Z
aload 1
checkcast com/a/b/d/rz
astore 1
aload 1
invokeinterface com/a/b/d/rz/i()Lcom/a/b/d/rz; 0
aload 1
invokeinterface com/a/b/d/rz/h()Lcom/a/b/d/rz; 0
invokestatic com/a/b/d/qy/b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)V
aload 0
getfield com/a/b/d/rp/a Lcom/a/b/d/rz;
invokeinterface com/a/b/d/rz/i()Lcom/a/b/d/rz; 0
aload 1
invokestatic com/a/b/d/qy/b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)V
aload 1
aload 0
getfield com/a/b/d/rp/a Lcom/a/b/d/rz;
invokestatic com/a/b/d/qy/b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)V
iconst_1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final synthetic peek()Ljava/lang/Object;
aload 0
invokespecial com/a/b/d/rp/a()Lcom/a/b/d/rz;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final synthetic poll()Ljava/lang/Object;
aload 0
getfield com/a/b/d/rp/a Lcom/a/b/d/rz;
invokeinterface com/a/b/d/rz/h()Lcom/a/b/d/rz; 0
astore 1
aload 1
aload 0
getfield com/a/b/d/rp/a Lcom/a/b/d/rz;
if_acmpne L0
aconst_null
areturn
L0:
aload 0
aload 1
invokevirtual com/a/b/d/rp/remove(Ljava/lang/Object;)Z
pop
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public final remove(Ljava/lang/Object;)Z
aload 1
checkcast com/a/b/d/rz
astore 1
aload 1
invokeinterface com/a/b/d/rz/i()Lcom/a/b/d/rz; 0
astore 2
aload 1
invokeinterface com/a/b/d/rz/h()Lcom/a/b/d/rz; 0
astore 3
aload 2
aload 3
invokestatic com/a/b/d/qy/b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)V
aload 1
invokestatic com/a/b/d/qy/c(Lcom/a/b/d/rz;)V
aload 3
getstatic com/a/b/d/ry/a Lcom/a/b/d/ry;
if_acmpeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 4
.limit stack 2
.end method

.method public final size()I
iconst_0
istore 1
aload 0
getfield com/a/b/d/rp/a Lcom/a/b/d/rz;
invokeinterface com/a/b/d/rz/h()Lcom/a/b/d/rz; 0
astore 2
L0:
aload 2
aload 0
getfield com/a/b/d/rp/a Lcom/a/b/d/rz;
if_acmpeq L1
iload 1
iconst_1
iadd
istore 1
aload 2
invokeinterface com/a/b/d/rz/h()Lcom/a/b/d/rz; 0
astore 2
goto L0
L1:
iload 1
ireturn
.limit locals 3
.limit stack 2
.end method
