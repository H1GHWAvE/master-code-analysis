.bytecode 50.0
.class final synchronized com/a/b/d/pi
.super java/util/AbstractList
.implements java/io/Serializable
.implements java/util/RandomAccess

.field private static final 'c' J = 0L


.field final 'a' Ljava/util/List;

.field final 'b' Lcom/a/b/b/bj;

.method <init>(Ljava/util/List;Lcom/a/b/b/bj;)V
aload 0
invokespecial java/util/AbstractList/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/List
putfield com/a/b/d/pi/a Ljava/util/List;
aload 0
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/b/bj
putfield com/a/b/d/pi/b Lcom/a/b/b/bj;
return
.limit locals 3
.limit stack 2
.end method

.method public final clear()V
aload 0
getfield com/a/b/d/pi/a Ljava/util/List;
invokeinterface java/util/List/clear()V 0
return
.limit locals 1
.limit stack 1
.end method

.method public final get(I)Ljava/lang/Object;
aload 0
getfield com/a/b/d/pi/b Lcom/a/b/b/bj;
aload 0
getfield com/a/b/d/pi/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
invokeinterface com/a/b/b/bj/e(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 3
.end method

.method public final isEmpty()Z
aload 0
getfield com/a/b/d/pi/a Ljava/util/List;
invokeinterface java/util/List/isEmpty()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final iterator()Ljava/util/Iterator;
aload 0
invokevirtual com/a/b/d/pi/listIterator()Ljava/util/ListIterator;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final listIterator(I)Ljava/util/ListIterator;
new com/a/b/d/pj
dup
aload 0
aload 0
getfield com/a/b/d/pi/a Ljava/util/List;
iload 1
invokeinterface java/util/List/listIterator(I)Ljava/util/ListIterator; 1
invokespecial com/a/b/d/pj/<init>(Lcom/a/b/d/pi;Ljava/util/ListIterator;)V
areturn
.limit locals 2
.limit stack 5
.end method

.method public final remove(I)Ljava/lang/Object;
aload 0
getfield com/a/b/d/pi/b Lcom/a/b/b/bj;
aload 0
getfield com/a/b/d/pi/a Ljava/util/List;
iload 1
invokeinterface java/util/List/remove(I)Ljava/lang/Object; 1
invokeinterface com/a/b/b/bj/e(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 3
.end method

.method public final size()I
aload 0
getfield com/a/b/d/pi/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
ireturn
.limit locals 1
.limit stack 1
.end method
