.bytecode 50.0
.class final synchronized com/a/b/d/vh
.super java/lang/Object
.implements java/util/Iterator

.field final synthetic 'a' Lcom/a/b/d/vc;

.field private 'b' I

.field private 'c' I

.field private 'd' Ljava/util/Queue;

.field private 'e' Ljava/util/List;

.field private 'f' Ljava/lang/Object;

.field private 'g' Z

.method private <init>(Lcom/a/b/d/vc;)V
aload 0
aload 1
putfield com/a/b/d/vh/a Lcom/a/b/d/vc;
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_m1
putfield com/a/b/d/vh/b I
aload 0
aload 0
getfield com/a/b/d/vh/a Lcom/a/b/d/vc;
invokestatic com/a/b/d/vc/c(Lcom/a/b/d/vc;)I
putfield com/a/b/d/vh/c I
return
.limit locals 2
.limit stack 2
.end method

.method synthetic <init>(Lcom/a/b/d/vc;B)V
aload 0
aload 1
invokespecial com/a/b/d/vh/<init>(Lcom/a/b/d/vc;)V
return
.limit locals 3
.limit stack 2
.end method

.method private a(I)I
iload 1
istore 2
aload 0
getfield com/a/b/d/vh/e Ljava/util/List;
ifnull L0
L1:
iload 1
istore 2
iload 1
aload 0
getfield com/a/b/d/vh/a Lcom/a/b/d/vc;
invokevirtual com/a/b/d/vc/size()I
if_icmpge L0
aload 0
getfield com/a/b/d/vh/e Ljava/util/List;
astore 5
aload 0
getfield com/a/b/d/vh/a Lcom/a/b/d/vc;
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iload 1
aaload
astore 4
aload 5
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 5
L2:
aload 5
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 5
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
aload 4
if_acmpne L2
iconst_1
istore 3
L4:
iload 1
istore 2
iload 3
ifeq L0
iload 1
iconst_1
iadd
istore 1
goto L1
L3:
iconst_0
istore 3
goto L4
L0:
iload 2
ireturn
.limit locals 6
.limit stack 2
.end method

.method private a()V
aload 0
getfield com/a/b/d/vh/a Lcom/a/b/d/vc;
invokestatic com/a/b/d/vc/c(Lcom/a/b/d/vc;)I
aload 0
getfield com/a/b/d/vh/c I
if_icmpeq L0
new java/util/ConcurrentModificationException
dup
invokespecial java/util/ConcurrentModificationException/<init>()V
athrow
L0:
return
.limit locals 1
.limit stack 2
.end method

.method private static a(Ljava/lang/Iterable;Ljava/lang/Object;)Z
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 0
L0:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
aload 1
if_acmpne L0
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/Object;)Z
iconst_0
istore 4
iconst_0
istore 2
L0:
iload 4
istore 3
iload 2
aload 0
getfield com/a/b/d/vh/a Lcom/a/b/d/vc;
invokestatic com/a/b/d/vc/b(Lcom/a/b/d/vc;)I
if_icmpge L1
aload 0
getfield com/a/b/d/vh/a Lcom/a/b/d/vc;
invokestatic com/a/b/d/vc/a(Lcom/a/b/d/vc;)[Ljava/lang/Object;
iload 2
aaload
aload 1
if_acmpne L2
aload 0
getfield com/a/b/d/vh/a Lcom/a/b/d/vc;
iload 2
invokevirtual com/a/b/d/vc/a(I)Lcom/a/b/d/vg;
pop
iconst_1
istore 3
L1:
iload 3
ireturn
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
.limit locals 5
.limit stack 2
.end method

.method public final hasNext()Z
aload 0
invokespecial com/a/b/d/vh/a()V
aload 0
aload 0
getfield com/a/b/d/vh/b I
iconst_1
iadd
invokespecial com/a/b/d/vh/a(I)I
aload 0
getfield com/a/b/d/vh/a Lcom/a/b/d/vc;
invokevirtual com/a/b/d/vc/size()I
if_icmplt L0
aload 0
getfield com/a/b/d/vh/d Ljava/util/Queue;
ifnull L1
aload 0
getfield com/a/b/d/vh/d Ljava/util/Queue;
invokeinterface java/util/Queue/isEmpty()Z 0
ifne L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 3
.end method

.method public final next()Ljava/lang/Object;
aload 0
invokespecial com/a/b/d/vh/a()V
aload 0
aload 0
getfield com/a/b/d/vh/b I
iconst_1
iadd
invokespecial com/a/b/d/vh/a(I)I
istore 1
iload 1
aload 0
getfield com/a/b/d/vh/a Lcom/a/b/d/vc;
invokevirtual com/a/b/d/vc/size()I
if_icmpge L0
aload 0
iload 1
putfield com/a/b/d/vh/b I
aload 0
iconst_1
putfield com/a/b/d/vh/g Z
aload 0
getfield com/a/b/d/vh/a Lcom/a/b/d/vc;
astore 2
aload 0
getfield com/a/b/d/vh/b I
istore 1
aload 2
getfield com/a/b/d/vc/b [Ljava/lang/Object;
iload 1
aaload
areturn
L0:
aload 0
getfield com/a/b/d/vh/d Ljava/util/Queue;
ifnull L1
aload 0
aload 0
getfield com/a/b/d/vh/a Lcom/a/b/d/vc;
invokevirtual com/a/b/d/vc/size()I
putfield com/a/b/d/vh/b I
aload 0
aload 0
getfield com/a/b/d/vh/d Ljava/util/Queue;
invokeinterface java/util/Queue/poll()Ljava/lang/Object; 0
putfield com/a/b/d/vh/f Ljava/lang/Object;
aload 0
getfield com/a/b/d/vh/f Ljava/lang/Object;
ifnull L1
aload 0
iconst_1
putfield com/a/b/d/vh/g Z
aload 0
getfield com/a/b/d/vh/f Ljava/lang/Object;
areturn
L1:
new java/util/NoSuchElementException
dup
ldc "iterator moved past last element in queue."
invokespecial java/util/NoSuchElementException/<init>(Ljava/lang/String;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method public final remove()V
aload 0
getfield com/a/b/d/vh/g Z
ldc "no calls to next() since the last call to remove()"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
aload 0
invokespecial com/a/b/d/vh/a()V
aload 0
iconst_0
putfield com/a/b/d/vh/g Z
aload 0
aload 0
getfield com/a/b/d/vh/c I
iconst_1
iadd
putfield com/a/b/d/vh/c I
aload 0
getfield com/a/b/d/vh/b I
aload 0
getfield com/a/b/d/vh/a Lcom/a/b/d/vc;
invokevirtual com/a/b/d/vc/size()I
if_icmpge L0
aload 0
getfield com/a/b/d/vh/a Lcom/a/b/d/vc;
aload 0
getfield com/a/b/d/vh/b I
invokevirtual com/a/b/d/vc/a(I)Lcom/a/b/d/vg;
astore 1
aload 1
ifnull L1
aload 0
getfield com/a/b/d/vh/d Ljava/util/Queue;
ifnonnull L2
aload 0
new java/util/ArrayDeque
dup
invokespecial java/util/ArrayDeque/<init>()V
putfield com/a/b/d/vh/d Ljava/util/Queue;
aload 0
new java/util/ArrayList
dup
iconst_3
invokespecial java/util/ArrayList/<init>(I)V
putfield com/a/b/d/vh/e Ljava/util/List;
L2:
aload 0
getfield com/a/b/d/vh/d Ljava/util/Queue;
aload 1
getfield com/a/b/d/vg/a Ljava/lang/Object;
invokeinterface java/util/Queue/add(Ljava/lang/Object;)Z 1
pop
aload 0
getfield com/a/b/d/vh/e Ljava/util/List;
aload 1
getfield com/a/b/d/vg/b Ljava/lang/Object;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L1:
aload 0
aload 0
getfield com/a/b/d/vh/b I
iconst_1
isub
putfield com/a/b/d/vh/b I
return
L0:
aload 0
aload 0
getfield com/a/b/d/vh/f Ljava/lang/Object;
invokespecial com/a/b/d/vh/a(Ljava/lang/Object;)Z
invokestatic com/a/b/b/cn/b(Z)V
aload 0
aconst_null
putfield com/a/b/d/vh/f Ljava/lang/Object;
return
.limit locals 2
.limit stack 4
.end method
