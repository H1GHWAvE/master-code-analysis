.bytecode 50.0
.class final synchronized com/a/b/d/cp
.super java/util/AbstractCollection

.field final 'a' Lcom/a/b/d/jl;

.field final 'b' Ljava/util/Comparator;

.field final 'c' I

.method <init>(Ljava/lang/Iterable;Ljava/util/Comparator;)V
aload 0
invokespecial java/util/AbstractCollection/<init>()V
aload 0
aload 2
invokestatic com/a/b/d/yd/a(Ljava/util/Comparator;)Lcom/a/b/d/yd;
aload 1
invokevirtual com/a/b/d/yd/b(Ljava/lang/Iterable;)Lcom/a/b/d/jl;
putfield com/a/b/d/cp/a Lcom/a/b/d/jl;
aload 0
aload 2
putfield com/a/b/d/cp/b Ljava/util/Comparator;
aload 0
aload 0
getfield com/a/b/d/cp/a Lcom/a/b/d/jl;
aload 2
invokestatic com/a/b/d/cp/a(Ljava/util/List;Ljava/util/Comparator;)I
putfield com/a/b/d/cp/c I
return
.limit locals 3
.limit stack 3
.end method

.method private static a(Ljava/util/List;Ljava/util/Comparator;)I
iconst_1
istore 2
lconst_1
lstore 7
iconst_1
istore 3
L0:
iload 3
aload 0
invokeinterface java/util/List/size()I 0
if_icmpge L1
iload 2
istore 4
lload 7
lstore 5
aload 1
aload 0
iload 3
iconst_1
isub
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
aload 0
iload 3
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
ifge L2
lload 7
iload 3
iload 2
invokestatic com/a/b/j/i/a(II)J
lmul
lstore 7
iconst_0
istore 4
lload 7
lstore 5
lload 7
invokestatic com/a/b/d/cm/a(J)Z
ifne L2
ldc_w 2147483647
ireturn
L2:
iload 3
iconst_1
iadd
istore 3
iload 4
iconst_1
iadd
istore 2
lload 5
lstore 7
goto L0
L1:
iload 3
iload 2
invokestatic com/a/b/j/i/a(II)J
lload 7
lmul
lstore 5
lload 5
invokestatic com/a/b/d/cm/a(J)Z
ifne L3
ldc_w 2147483647
ireturn
L3:
lload 5
l2i
ireturn
.limit locals 9
.limit stack 4
.end method

.method public final contains(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
instanceof java/util/List
ifeq L0
aload 1
checkcast java/util/List
astore 1
aload 0
getfield com/a/b/d/cp/a Lcom/a/b/d/jl;
aload 1
invokestatic com/a/b/d/cm/a(Ljava/util/List;Ljava/util/List;)Z
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final isEmpty()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final iterator()Ljava/util/Iterator;
new com/a/b/d/cq
dup
aload 0
getfield com/a/b/d/cp/a Lcom/a/b/d/jl;
aload 0
getfield com/a/b/d/cp/b Ljava/util/Comparator;
invokespecial com/a/b/d/cq/<init>(Ljava/util/List;Ljava/util/Comparator;)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public final size()I
aload 0
getfield com/a/b/d/cp/c I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/d/cp/a Lcom/a/b/d/jl;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 30
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "orderedPermutationCollection("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 4
.end method
