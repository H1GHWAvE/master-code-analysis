.bytecode 50.0
.class final synchronized com/a/b/d/ji
.super com/a/b/d/lo
.annotation invisible Lcom/a/b/a/b;
a Z = 1
b Z = 1
.end annotation

.field private final transient 'a' Ljava/util/EnumSet;

.field private transient 'c' I

.method private <init>(Ljava/util/EnumSet;)V
aload 0
invokespecial com/a/b/d/lo/<init>()V
aload 0
aload 1
putfield com/a/b/d/ji/a Ljava/util/EnumSet;
return
.limit locals 2
.limit stack 2
.end method

.method synthetic <init>(Ljava/util/EnumSet;B)V
aload 0
aload 1
invokespecial com/a/b/d/ji/<init>(Ljava/util/EnumSet;)V
return
.limit locals 3
.limit stack 2
.end method

.method static a(Ljava/util/EnumSet;)Lcom/a/b/d/lo;
aload 0
invokevirtual java/util/EnumSet/size()I
tableswitch 0
L0
L1
default : L2
L2:
new com/a/b/d/ji
dup
aload 0
invokespecial com/a/b/d/ji/<init>(Ljava/util/EnumSet;)V
areturn
L0:
getstatic com/a/b/d/ey/a Lcom/a/b/d/ey;
areturn
L1:
aload 0
invokestatic com/a/b/d/mq/b(Ljava/lang/Iterable;)Ljava/lang/Object;
invokestatic com/a/b/d/lo/d(Ljava/lang/Object;)Lcom/a/b/d/lo;
areturn
.limit locals 1
.limit stack 3
.end method

.method public final c()Lcom/a/b/d/agi;
aload 0
getfield com/a/b/d/ji/a Ljava/util/EnumSet;
invokevirtual java/util/EnumSet/iterator()Ljava/util/Iterator;
invokestatic com/a/b/d/nj/a(Ljava/util/Iterator;)Lcom/a/b/d/agi;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final contains(Ljava/lang/Object;)Z
aload 0
getfield com/a/b/d/ji/a Ljava/util/EnumSet;
aload 1
invokevirtual java/util/EnumSet/contains(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final containsAll(Ljava/util/Collection;)Z
aload 0
getfield com/a/b/d/ji/a Ljava/util/EnumSet;
aload 1
invokevirtual java/util/EnumSet/containsAll(Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final equals(Ljava/lang/Object;)Z
aload 1
aload 0
if_acmpeq L0
aload 0
getfield com/a/b/d/ji/a Ljava/util/EnumSet;
aload 1
invokevirtual java/util/EnumSet/equals(Ljava/lang/Object;)Z
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method final g()Ljava/lang/Object;
new com/a/b/d/jk
dup
aload 0
getfield com/a/b/d/ji/a Ljava/util/EnumSet;
invokespecial com/a/b/d/jk/<init>(Ljava/util/EnumSet;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method final h_()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final hashCode()I
aload 0
getfield com/a/b/d/ji/c I
istore 2
iload 2
istore 1
iload 2
ifne L0
aload 0
getfield com/a/b/d/ji/a Ljava/util/EnumSet;
invokevirtual java/util/EnumSet/hashCode()I
istore 1
aload 0
iload 1
putfield com/a/b/d/ji/c I
L0:
iload 1
ireturn
.limit locals 3
.limit stack 2
.end method

.method public final isEmpty()Z
aload 0
getfield com/a/b/d/ji/a Ljava/util/EnumSet;
invokevirtual java/util/EnumSet/isEmpty()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
aload 0
invokevirtual com/a/b/d/ji/c()Lcom/a/b/d/agi;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final size()I
aload 0
getfield com/a/b/d/ji/a Ljava/util/EnumSet;
invokevirtual java/util/EnumSet/size()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/d/ji/a Ljava/util/EnumSet;
invokevirtual java/util/EnumSet/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
