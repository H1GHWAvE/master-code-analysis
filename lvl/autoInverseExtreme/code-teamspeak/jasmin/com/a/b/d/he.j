.bytecode 50.0
.class public synchronized abstract com/a/b/d/he
.super com/a/b/d/hp
.implements java/util/NavigableSet

.method protected <init>()V
aload 0
invokespecial com/a/b/d/hp/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private a(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
aload 1
iconst_0
invokevirtual com/a/b/d/he/headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
invokeinterface java/util/NavigableSet/descendingIterator()Ljava/util/Iterator; 0
aconst_null
invokestatic com/a/b/d/nj/d(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 3
.end method

.method private a(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;
.annotation invisible Lcom/a/b/a/a;
.end annotation
aload 0
aload 1
iload 2
invokevirtual com/a/b/d/he/tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
aload 3
iload 4
invokeinterface java/util/NavigableSet/headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet; 2
areturn
.limit locals 5
.limit stack 3
.end method

.method private d(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
aload 1
iconst_1
invokevirtual com/a/b/d/he/headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
invokeinterface java/util/NavigableSet/descendingIterator()Ljava/util/Iterator; 0
aconst_null
invokestatic com/a/b/d/nj/d(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 3
.end method

.method private e()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/he/iterator()Ljava/util/Iterator;
invokestatic com/a/b/d/nj/h(Ljava/util/Iterator;)Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
aload 1
iconst_1
invokevirtual com/a/b/d/he/tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
invokeinterface java/util/NavigableSet/iterator()Ljava/util/Iterator; 0
aconst_null
invokestatic com/a/b/d/nj/d(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 3
.end method

.method private f()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/he/descendingIterator()Ljava/util/Iterator;
invokestatic com/a/b/d/nj/h(Ljava/util/Iterator;)Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method private f(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
aload 1
iconst_0
invokevirtual com/a/b/d/he/tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
invokeinterface java/util/NavigableSet/iterator()Ljava/util/Iterator; 0
aconst_null
invokestatic com/a/b/d/nj/d(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 3
.end method

.method private g(Ljava/lang/Object;)Ljava/util/SortedSet;
aload 0
aload 1
iconst_0
invokevirtual com/a/b/d/he/headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
areturn
.limit locals 2
.limit stack 3
.end method

.method private h()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/he/iterator()Ljava/util/Iterator;
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method private h(Ljava/lang/Object;)Ljava/util/SortedSet;
aload 0
aload 1
iconst_1
invokevirtual com/a/b/d/he/tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
areturn
.limit locals 2
.limit stack 3
.end method

.method private i()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/he/descendingIterator()Ljava/util/Iterator;
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method protected synthetic a()Ljava/util/Set;
aload 0
invokevirtual com/a/b/d/he/d()Ljava/util/NavigableSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
aload 0
aload 1
iconst_1
aload 2
iconst_0
invokevirtual com/a/b/d/he/subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;
areturn
.limit locals 3
.limit stack 5
.end method

.method protected synthetic b()Ljava/util/Collection;
aload 0
invokevirtual com/a/b/d/he/d()Ljava/util/NavigableSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected synthetic c()Ljava/util/SortedSet;
aload 0
invokevirtual com/a/b/d/he/d()Ljava/util/NavigableSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method public ceiling(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/he/d()Ljava/util/NavigableSet;
aload 1
invokeinterface java/util/NavigableSet/ceiling(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method protected abstract d()Ljava/util/NavigableSet;
.end method

.method public descendingIterator()Ljava/util/Iterator;
aload 0
invokevirtual com/a/b/d/he/d()Ljava/util/NavigableSet;
invokeinterface java/util/NavigableSet/descendingIterator()Ljava/util/Iterator; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public descendingSet()Ljava/util/NavigableSet;
aload 0
invokevirtual com/a/b/d/he/d()Ljava/util/NavigableSet;
invokeinterface java/util/NavigableSet/descendingSet()Ljava/util/NavigableSet; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public floor(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/he/d()Ljava/util/NavigableSet;
aload 1
invokeinterface java/util/NavigableSet/floor(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
aload 0
invokevirtual com/a/b/d/he/d()Ljava/util/NavigableSet;
aload 1
iload 2
invokeinterface java/util/NavigableSet/headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet; 2
areturn
.limit locals 3
.limit stack 3
.end method

.method public higher(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/he/d()Ljava/util/NavigableSet;
aload 1
invokeinterface java/util/NavigableSet/higher(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method protected synthetic k_()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/he/d()Ljava/util/NavigableSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method public lower(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/he/d()Ljava/util/NavigableSet;
aload 1
invokeinterface java/util/NavigableSet/lower(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public pollFirst()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/he/d()Ljava/util/NavigableSet;
invokeinterface java/util/NavigableSet/pollFirst()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public pollLast()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/he/d()Ljava/util/NavigableSet;
invokeinterface java/util/NavigableSet/pollLast()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;
aload 0
invokevirtual com/a/b/d/he/d()Ljava/util/NavigableSet;
aload 1
iload 2
aload 3
iload 4
invokeinterface java/util/NavigableSet/subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet; 4
areturn
.limit locals 5
.limit stack 5
.end method

.method public tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
aload 0
invokevirtual com/a/b/d/he/d()Ljava/util/NavigableSet;
aload 1
iload 2
invokeinterface java/util/NavigableSet/tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet; 2
areturn
.limit locals 3
.limit stack 3
.end method
