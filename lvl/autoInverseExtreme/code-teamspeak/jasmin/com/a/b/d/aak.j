.bytecode 50.0
.class final synchronized com/a/b/d/aak
.super com/a/b/d/aam
.implements java/util/NavigableSet
.annotation invisible Lcom/a/b/a/c;
a s = "NavigableSet"
.end annotation

.method <init>(Ljava/util/NavigableSet;Lcom/a/b/b/co;)V
aload 0
aload 1
aload 2
invokespecial com/a/b/d/aam/<init>(Ljava/util/SortedSet;Lcom/a/b/b/co;)V
return
.limit locals 3
.limit stack 3
.end method

.method private a()Ljava/util/NavigableSet;
aload 0
getfield com/a/b/d/aak/a Ljava/util/Collection;
checkcast java/util/NavigableSet
areturn
.limit locals 1
.limit stack 1
.end method

.method public final ceiling(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
aload 1
iconst_1
invokevirtual com/a/b/d/aak/tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
invokestatic com/a/b/d/mq/f(Ljava/lang/Iterable;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 3
.end method

.method public final descendingIterator()Ljava/util/Iterator;
aload 0
getfield com/a/b/d/aak/a Ljava/util/Collection;
checkcast java/util/NavigableSet
invokeinterface java/util/NavigableSet/descendingIterator()Ljava/util/Iterator; 0
aload 0
getfield com/a/b/d/aak/b Lcom/a/b/b/co;
invokestatic com/a/b/d/nj/b(Ljava/util/Iterator;Lcom/a/b/b/co;)Lcom/a/b/d/agi;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final descendingSet()Ljava/util/NavigableSet;
aload 0
getfield com/a/b/d/aak/a Ljava/util/Collection;
checkcast java/util/NavigableSet
invokeinterface java/util/NavigableSet/descendingSet()Ljava/util/NavigableSet; 0
aload 0
getfield com/a/b/d/aak/b Lcom/a/b/b/co;
invokestatic com/a/b/d/aad/a(Ljava/util/NavigableSet;Lcom/a/b/b/co;)Ljava/util/NavigableSet;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final floor(Ljava/lang/Object;)Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
iconst_1
invokevirtual com/a/b/d/aak/headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
invokeinterface java/util/NavigableSet/descendingIterator()Ljava/util/Iterator; 0
aconst_null
invokestatic com/a/b/d/nj/d(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 3
.end method

.method public final headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
aload 0
getfield com/a/b/d/aak/a Ljava/util/Collection;
checkcast java/util/NavigableSet
aload 1
iload 2
invokeinterface java/util/NavigableSet/headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet; 2
aload 0
getfield com/a/b/d/aak/b Lcom/a/b/b/co;
invokestatic com/a/b/d/aad/a(Ljava/util/NavigableSet;Lcom/a/b/b/co;)Ljava/util/NavigableSet;
areturn
.limit locals 3
.limit stack 3
.end method

.method public final higher(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
aload 1
iconst_0
invokevirtual com/a/b/d/aak/tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
invokestatic com/a/b/d/mq/f(Ljava/lang/Iterable;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 3
.end method

.method public final last()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/aak/descendingIterator()Ljava/util/Iterator;
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public final lower(Ljava/lang/Object;)Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
iconst_0
invokevirtual com/a/b/d/aak/headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
invokeinterface java/util/NavigableSet/descendingIterator()Ljava/util/Iterator; 0
aconst_null
invokestatic com/a/b/d/nj/d(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 3
.end method

.method public final pollFirst()Ljava/lang/Object;
aload 0
getfield com/a/b/d/aak/a Ljava/util/Collection;
checkcast java/util/NavigableSet
aload 0
getfield com/a/b/d/aak/b Lcom/a/b/b/co;
invokestatic com/a/b/d/mq/b(Ljava/lang/Iterable;Lcom/a/b/b/co;)Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final pollLast()Ljava/lang/Object;
aload 0
getfield com/a/b/d/aak/a Ljava/util/Collection;
checkcast java/util/NavigableSet
invokeinterface java/util/NavigableSet/descendingSet()Ljava/util/NavigableSet; 0
aload 0
getfield com/a/b/d/aak/b Lcom/a/b/b/co;
invokestatic com/a/b/d/mq/b(Ljava/lang/Iterable;Lcom/a/b/b/co;)Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;
aload 0
getfield com/a/b/d/aak/a Ljava/util/Collection;
checkcast java/util/NavigableSet
aload 1
iload 2
aload 3
iload 4
invokeinterface java/util/NavigableSet/subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet; 4
aload 0
getfield com/a/b/d/aak/b Lcom/a/b/b/co;
invokestatic com/a/b/d/aad/a(Ljava/util/NavigableSet;Lcom/a/b/b/co;)Ljava/util/NavigableSet;
areturn
.limit locals 5
.limit stack 5
.end method

.method public final tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
aload 0
getfield com/a/b/d/aak/a Ljava/util/Collection;
checkcast java/util/NavigableSet
aload 1
iload 2
invokeinterface java/util/NavigableSet/tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet; 2
aload 0
getfield com/a/b/d/aak/b Lcom/a/b/b/co;
invokestatic com/a/b/d/aad/a(Ljava/util/NavigableSet;Lcom/a/b/b/co;)Ljava/util/NavigableSet;
areturn
.limit locals 3
.limit stack 3
.end method
