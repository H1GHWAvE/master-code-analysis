.bytecode 50.0
.class public synchronized abstract com/a/b/d/hp
.super com/a/b/d/hi
.implements java/util/SortedSet
.annotation invisible Lcom/a/b/a/b;
.end annotation

.method protected <init>()V
aload 0
invokespecial com/a/b/d/hi/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private b(Ljava/lang/Object;Ljava/lang/Object;)I
aload 0
invokevirtual com/a/b/d/hp/comparator()Ljava/util/Comparator;
astore 3
aload 3
ifnonnull L0
aload 1
checkcast java/lang/Comparable
aload 2
invokeinterface java/lang/Comparable/compareTo(Ljava/lang/Object;)I 1
ireturn
L0:
aload 3
aload 1
aload 2
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
ireturn
.limit locals 4
.limit stack 3
.end method

.method protected synthetic a()Ljava/util/Set;
aload 0
invokevirtual com/a/b/d/hp/c()Ljava/util/SortedSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
.annotation invisible Lcom/a/b/a/a;
.end annotation
aload 0
aload 1
invokevirtual com/a/b/d/hp/tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
aload 2
invokeinterface java/util/SortedSet/headSet(Ljava/lang/Object;)Ljava/util/SortedSet; 1
areturn
.limit locals 3
.limit stack 2
.end method

.method protected synthetic b()Ljava/util/Collection;
aload 0
invokevirtual com/a/b/d/hp/c()Ljava/util/SortedSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final b(Ljava/lang/Object;)Z
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.catch java/lang/ClassCastException from L0 to L1 using L2
.catch java/util/NoSuchElementException from L0 to L1 using L3
.catch java/lang/NullPointerException from L0 to L1 using L4
iconst_0
istore 3
L0:
aload 0
aload 0
aload 1
invokeinterface java/util/SortedSet/tailSet(Ljava/lang/Object;)Ljava/util/SortedSet; 1
invokeinterface java/util/SortedSet/first()Ljava/lang/Object; 0
aload 1
invokespecial com/a/b/d/hp/b(Ljava/lang/Object;Ljava/lang/Object;)I
istore 2
L1:
iload 2
ifne L5
iconst_1
istore 3
L5:
iload 3
ireturn
L4:
astore 1
iconst_0
ireturn
L3:
astore 1
iconst_0
ireturn
L2:
astore 1
iconst_0
ireturn
.limit locals 4
.limit stack 3
.end method

.method protected abstract c()Ljava/util/SortedSet;
.end method

.method protected final c(Ljava/lang/Object;)Z
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.catch java/lang/ClassCastException from L0 to L1 using L2
.catch java/lang/NullPointerException from L0 to L1 using L3
.catch java/lang/ClassCastException from L4 to L5 using L2
.catch java/lang/NullPointerException from L4 to L5 using L3
.catch java/lang/ClassCastException from L6 to L7 using L2
.catch java/lang/NullPointerException from L6 to L7 using L3
iconst_0
istore 3
L0:
aload 0
aload 1
invokeinterface java/util/SortedSet/tailSet(Ljava/lang/Object;)Ljava/util/SortedSet; 1
invokeinterface java/util/SortedSet/iterator()Ljava/util/Iterator; 0
astore 4
L1:
iload 3
istore 2
L4:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L8
L5:
iload 3
istore 2
L6:
aload 0
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
aload 1
invokespecial com/a/b/d/hp/b(Ljava/lang/Object;Ljava/lang/Object;)I
ifne L8
aload 4
invokeinterface java/util/Iterator/remove()V 0
L7:
iconst_1
istore 2
L8:
iload 2
ireturn
L3:
astore 1
iconst_0
ireturn
L2:
astore 1
iconst_0
ireturn
.limit locals 5
.limit stack 3
.end method

.method public comparator()Ljava/util/Comparator;
aload 0
invokevirtual com/a/b/d/hp/c()Ljava/util/SortedSet;
invokeinterface java/util/SortedSet/comparator()Ljava/util/Comparator; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public first()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/hp/c()Ljava/util/SortedSet;
invokeinterface java/util/SortedSet/first()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
aload 0
invokevirtual com/a/b/d/hp/c()Ljava/util/SortedSet;
aload 1
invokeinterface java/util/SortedSet/headSet(Ljava/lang/Object;)Ljava/util/SortedSet; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method protected synthetic k_()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/hp/c()Ljava/util/SortedSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method public last()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/hp/c()Ljava/util/SortedSet;
invokeinterface java/util/SortedSet/last()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
aload 0
invokevirtual com/a/b/d/hp/c()Ljava/util/SortedSet;
aload 1
aload 2
invokeinterface java/util/SortedSet/subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet; 2
areturn
.limit locals 3
.limit stack 3
.end method

.method public tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
aload 0
invokevirtual com/a/b/d/hp/c()Ljava/util/SortedSet;
aload 1
invokeinterface java/util/SortedSet/tailSet(Ljava/lang/Object;)Ljava/util/SortedSet; 1
areturn
.limit locals 2
.limit stack 2
.end method
