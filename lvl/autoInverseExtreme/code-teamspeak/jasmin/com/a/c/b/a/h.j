.bytecode 50.0
.class public final synchronized com/a/c/b/a/h
.super com/a/c/d/a

.field private static final 'c' Ljava/io/Reader;

.field private static final 'd' Ljava/lang/Object;

.field public final 'a' Ljava/util/List;

.method static <clinit>()V
new com/a/c/b/a/i
dup
invokespecial com/a/c/b/a/i/<init>()V
putstatic com/a/c/b/a/h/c Ljava/io/Reader;
new java/lang/Object
dup
invokespecial java/lang/Object/<init>()V
putstatic com/a/c/b/a/h/d Ljava/lang/Object;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>(Lcom/a/c/w;)V
aload 0
getstatic com/a/c/b/a/h/c Ljava/io/Reader;
invokespecial com/a/c/d/a/<init>(Ljava/io/Reader;)V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield com/a/c/b/a/h/a Ljava/util/List;
aload 0
getfield com/a/c/b/a/h/a Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
return
.limit locals 2
.limit stack 3
.end method

.method private q()Ljava/lang/Object;
aload 0
getfield com/a/c/b/a/h/a Ljava/util/List;
aload 0
getfield com/a/c/b/a/h/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
iconst_1
isub
invokeinterface java/util/List/remove(I)Ljava/lang/Object; 1
areturn
.limit locals 1
.limit stack 3
.end method

.method private r()V
aload 0
getstatic com/a/c/d/d/e Lcom/a/c/d/d;
invokevirtual com/a/c/b/a/h/a(Lcom/a/c/d/d;)V
aload 0
invokevirtual com/a/c/b/a/h/g()Ljava/lang/Object;
checkcast java/util/Iterator
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 1
aload 0
getfield com/a/c/b/a/h/a Ljava/util/List;
aload 1
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 0
getfield com/a/c/b/a/h/a Ljava/util/List;
new com/a/c/ac
dup
aload 1
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
checkcast java/lang/String
invokespecial com/a/c/ac/<init>(Ljava/lang/String;)V
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
return
.limit locals 2
.limit stack 4
.end method

.method public final a()V
aload 0
getstatic com/a/c/d/d/a Lcom/a/c/d/d;
invokevirtual com/a/c/b/a/h/a(Lcom/a/c/d/d;)V
aload 0
invokevirtual com/a/c/b/a/h/g()Ljava/lang/Object;
checkcast com/a/c/t
astore 1
aload 0
getfield com/a/c/b/a/h/a Ljava/util/List;
aload 1
invokevirtual com/a/c/t/iterator()Ljava/util/Iterator;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Lcom/a/c/d/d;)V
aload 0
invokevirtual com/a/c/b/a/h/f()Lcom/a/c/d/d;
aload 1
if_acmpeq L0
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Expected "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " but was "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual com/a/c/b/a/h/f()Lcom/a/c/d/d;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
return
.limit locals 2
.limit stack 5
.end method

.method public final b()V
aload 0
getstatic com/a/c/d/d/b Lcom/a/c/d/d;
invokevirtual com/a/c/b/a/h/a(Lcom/a/c/d/d;)V
aload 0
invokespecial com/a/c/b/a/h/q()Ljava/lang/Object;
pop
aload 0
invokespecial com/a/c/b/a/h/q()Ljava/lang/Object;
pop
return
.limit locals 1
.limit stack 2
.end method

.method public final c()V
aload 0
getstatic com/a/c/d/d/c Lcom/a/c/d/d;
invokevirtual com/a/c/b/a/h/a(Lcom/a/c/d/d;)V
aload 0
invokevirtual com/a/c/b/a/h/g()Ljava/lang/Object;
checkcast com/a/c/z
astore 1
aload 0
getfield com/a/c/b/a/h/a Ljava/util/List;
aload 1
getfield com/a/c/z/a Lcom/a/c/b/ag;
invokevirtual com/a/c/b/ag/entrySet()Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
return
.limit locals 2
.limit stack 2
.end method

.method public final close()V
aload 0
getfield com/a/c/b/a/h/a Ljava/util/List;
invokeinterface java/util/List/clear()V 0
aload 0
getfield com/a/c/b/a/h/a Ljava/util/List;
getstatic com/a/c/b/a/h/d Ljava/lang/Object;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
return
.limit locals 1
.limit stack 2
.end method

.method public final d()V
aload 0
getstatic com/a/c/d/d/d Lcom/a/c/d/d;
invokevirtual com/a/c/b/a/h/a(Lcom/a/c/d/d;)V
aload 0
invokespecial com/a/c/b/a/h/q()Ljava/lang/Object;
pop
aload 0
invokespecial com/a/c/b/a/h/q()Ljava/lang/Object;
pop
return
.limit locals 1
.limit stack 2
.end method

.method public final e()Z
aload 0
invokevirtual com/a/c/b/a/h/f()Lcom/a/c/d/d;
astore 1
aload 1
getstatic com/a/c/d/d/d Lcom/a/c/d/d;
if_acmpeq L0
aload 1
getstatic com/a/c/d/d/b Lcom/a/c/d/d;
if_acmpeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final f()Lcom/a/c/d/d;
L0:
aload 0
getfield com/a/c/b/a/h/a Ljava/util/List;
invokeinterface java/util/List/isEmpty()Z 0
ifeq L1
getstatic com/a/c/d/d/j Lcom/a/c/d/d;
areturn
L1:
aload 0
invokevirtual com/a/c/b/a/h/g()Ljava/lang/Object;
astore 2
aload 2
instanceof java/util/Iterator
ifeq L2
aload 0
getfield com/a/c/b/a/h/a Ljava/util/List;
aload 0
getfield com/a/c/b/a/h/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
iconst_2
isub
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
instanceof com/a/c/z
istore 1
aload 2
checkcast java/util/Iterator
astore 2
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
iload 1
ifeq L4
getstatic com/a/c/d/d/e Lcom/a/c/d/d;
areturn
L4:
aload 0
getfield com/a/c/b/a/h/a Ljava/util/List;
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
goto L0
L3:
iload 1
ifeq L5
getstatic com/a/c/d/d/d Lcom/a/c/d/d;
areturn
L5:
getstatic com/a/c/d/d/b Lcom/a/c/d/d;
areturn
L2:
aload 2
instanceof com/a/c/z
ifeq L6
getstatic com/a/c/d/d/c Lcom/a/c/d/d;
areturn
L6:
aload 2
instanceof com/a/c/t
ifeq L7
getstatic com/a/c/d/d/a Lcom/a/c/d/d;
areturn
L7:
aload 2
instanceof com/a/c/ac
ifeq L8
aload 2
checkcast com/a/c/ac
astore 2
aload 2
getfield com/a/c/ac/a Ljava/lang/Object;
instanceof java/lang/String
ifeq L9
getstatic com/a/c/d/d/f Lcom/a/c/d/d;
areturn
L9:
aload 2
getfield com/a/c/ac/a Ljava/lang/Object;
instanceof java/lang/Boolean
ifeq L10
getstatic com/a/c/d/d/h Lcom/a/c/d/d;
areturn
L10:
aload 2
getfield com/a/c/ac/a Ljava/lang/Object;
instanceof java/lang/Number
ifeq L11
getstatic com/a/c/d/d/g Lcom/a/c/d/d;
areturn
L11:
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L8:
aload 2
instanceof com/a/c/y
ifeq L12
getstatic com/a/c/d/d/i Lcom/a/c/d/d;
areturn
L12:
aload 2
getstatic com/a/c/b/a/h/d Ljava/lang/Object;
if_acmpne L13
new java/lang/IllegalStateException
dup
ldc "JsonReader is closed"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L13:
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
.limit locals 3
.limit stack 3
.end method

.method public final g()Ljava/lang/Object;
aload 0
getfield com/a/c/b/a/h/a Ljava/util/List;
aload 0
getfield com/a/c/b/a/h/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
iconst_1
isub
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
areturn
.limit locals 1
.limit stack 3
.end method

.method public final h()Ljava/lang/String;
aload 0
getstatic com/a/c/d/d/e Lcom/a/c/d/d;
invokevirtual com/a/c/b/a/h/a(Lcom/a/c/d/d;)V
aload 0
invokevirtual com/a/c/b/a/h/g()Ljava/lang/Object;
checkcast java/util/Iterator
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 1
aload 0
getfield com/a/c/b/a/h/a Ljava/util/List;
aload 1
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 1
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
checkcast java/lang/String
areturn
.limit locals 2
.limit stack 2
.end method

.method public final i()Ljava/lang/String;
aload 0
invokevirtual com/a/c/b/a/h/f()Lcom/a/c/d/d;
astore 1
aload 1
getstatic com/a/c/d/d/f Lcom/a/c/d/d;
if_acmpeq L0
aload 1
getstatic com/a/c/d/d/g Lcom/a/c/d/d;
if_acmpeq L0
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Expected "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
getstatic com/a/c/d/d/f Lcom/a/c/d/d;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " but was "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
invokespecial com/a/c/b/a/h/q()Ljava/lang/Object;
checkcast com/a/c/ac
invokevirtual com/a/c/ac/b()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 5
.end method

.method public final j()Z
aload 0
getstatic com/a/c/d/d/h Lcom/a/c/d/d;
invokevirtual com/a/c/b/a/h/a(Lcom/a/c/d/d;)V
aload 0
invokespecial com/a/c/b/a/h/q()Ljava/lang/Object;
checkcast com/a/c/ac
invokevirtual com/a/c/ac/l()Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final k()V
aload 0
getstatic com/a/c/d/d/i Lcom/a/c/d/d;
invokevirtual com/a/c/b/a/h/a(Lcom/a/c/d/d;)V
aload 0
invokespecial com/a/c/b/a/h/q()Ljava/lang/Object;
pop
return
.limit locals 1
.limit stack 2
.end method

.method public final l()D
aload 0
invokevirtual com/a/c/b/a/h/f()Lcom/a/c/d/d;
astore 3
aload 3
getstatic com/a/c/d/d/g Lcom/a/c/d/d;
if_acmpeq L0
aload 3
getstatic com/a/c/d/d/f Lcom/a/c/d/d;
if_acmpeq L0
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Expected "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
getstatic com/a/c/d/d/g Lcom/a/c/d/d;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " but was "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
invokevirtual com/a/c/b/a/h/g()Ljava/lang/Object;
checkcast com/a/c/ac
invokevirtual com/a/c/ac/c()D
dstore 1
aload 0
getfield com/a/c/d/a/b Z
ifne L1
dload 1
invokestatic java/lang/Double/isNaN(D)Z
ifne L2
dload 1
invokestatic java/lang/Double/isInfinite(D)Z
ifeq L1
L2:
new java/lang/NumberFormatException
dup
new java/lang/StringBuilder
dup
ldc "JSON forbids NaN and infinities: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
dload 1
invokevirtual java/lang/StringBuilder/append(D)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/NumberFormatException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
invokespecial com/a/c/b/a/h/q()Ljava/lang/Object;
pop
dload 1
dreturn
.limit locals 4
.limit stack 5
.end method

.method public final m()J
aload 0
invokevirtual com/a/c/b/a/h/f()Lcom/a/c/d/d;
astore 3
aload 3
getstatic com/a/c/d/d/g Lcom/a/c/d/d;
if_acmpeq L0
aload 3
getstatic com/a/c/d/d/f Lcom/a/c/d/d;
if_acmpeq L0
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Expected "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
getstatic com/a/c/d/d/g Lcom/a/c/d/d;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " but was "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
invokevirtual com/a/c/b/a/h/g()Ljava/lang/Object;
checkcast com/a/c/ac
invokevirtual com/a/c/ac/g()J
lstore 1
aload 0
invokespecial com/a/c/b/a/h/q()Ljava/lang/Object;
pop
lload 1
lreturn
.limit locals 4
.limit stack 5
.end method

.method public final n()I
aload 0
invokevirtual com/a/c/b/a/h/f()Lcom/a/c/d/d;
astore 2
aload 2
getstatic com/a/c/d/d/g Lcom/a/c/d/d;
if_acmpeq L0
aload 2
getstatic com/a/c/d/d/f Lcom/a/c/d/d;
if_acmpeq L0
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Expected "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
getstatic com/a/c/d/d/g Lcom/a/c/d/d;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " but was "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
invokevirtual com/a/c/b/a/h/g()Ljava/lang/Object;
checkcast com/a/c/ac
invokevirtual com/a/c/ac/h()I
istore 1
aload 0
invokespecial com/a/c/b/a/h/q()Ljava/lang/Object;
pop
iload 1
ireturn
.limit locals 3
.limit stack 5
.end method

.method public final o()V
aload 0
invokevirtual com/a/c/b/a/h/f()Lcom/a/c/d/d;
getstatic com/a/c/d/d/e Lcom/a/c/d/d;
if_acmpne L0
aload 0
invokevirtual com/a/c/b/a/h/h()Ljava/lang/String;
pop
return
L0:
aload 0
invokespecial com/a/c/b/a/h/q()Ljava/lang/Object;
pop
return
.limit locals 1
.limit stack 2
.end method

.method public final toString()Ljava/lang/String;
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
