.bytecode 50.0
.class public final synchronized com/a/c/b/a/q
.super java/lang/Object
.implements com/a/c/ap

.field private final 'a' Lcom/a/c/b/f;

.field private final 'b' Lcom/a/c/j;

.field private final 'c' Lcom/a/c/b/s;

.method public <init>(Lcom/a/c/b/f;Lcom/a/c/j;Lcom/a/c/b/s;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/a/c/b/a/q/a Lcom/a/c/b/f;
aload 0
aload 2
putfield com/a/c/b/a/q/b Lcom/a/c/j;
aload 0
aload 3
putfield com/a/c/b/a/q/c Lcom/a/c/b/s;
return
.limit locals 4
.limit stack 2
.end method

.method static synthetic a(Lcom/a/c/b/a/q;Lcom/a/c/k;Ljava/lang/reflect/Field;Lcom/a/c/c/a;)Lcom/a/c/an;
aload 2
ldc com/a/c/a/b
invokevirtual java/lang/reflect/Field/getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
checkcast com/a/c/a/b
astore 2
aload 2
ifnull L0
aload 0
getfield com/a/c/b/a/q/a Lcom/a/c/b/f;
aload 1
aload 3
aload 2
invokestatic com/a/c/b/a/g/a(Lcom/a/c/b/f;Lcom/a/c/k;Lcom/a/c/c/a;Lcom/a/c/a/b;)Lcom/a/c/an;
astore 0
aload 0
ifnull L0
aload 0
areturn
L0:
aload 1
aload 3
invokevirtual com/a/c/k/a(Lcom/a/c/c/a;)Lcom/a/c/an;
areturn
.limit locals 4
.limit stack 4
.end method

.method private a(Lcom/a/c/k;Ljava/lang/reflect/Field;Lcom/a/c/c/a;)Lcom/a/c/an;
aload 2
ldc com/a/c/a/b
invokevirtual java/lang/reflect/Field/getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
checkcast com/a/c/a/b
astore 2
aload 2
ifnull L0
aload 0
getfield com/a/c/b/a/q/a Lcom/a/c/b/f;
aload 1
aload 3
aload 2
invokestatic com/a/c/b/a/g/a(Lcom/a/c/b/f;Lcom/a/c/k;Lcom/a/c/c/a;Lcom/a/c/a/b;)Lcom/a/c/an;
astore 2
aload 2
ifnull L0
aload 2
areturn
L0:
aload 1
aload 3
invokevirtual com/a/c/k/a(Lcom/a/c/c/a;)Lcom/a/c/an;
areturn
.limit locals 4
.limit stack 4
.end method

.method private a(Lcom/a/c/k;Ljava/lang/reflect/Field;Ljava/lang/String;Lcom/a/c/c/a;ZZ)Lcom/a/c/b/a/t;
new com/a/c/b/a/r
dup
aload 0
aload 3
iload 5
iload 6
aload 1
aload 2
aload 4
aload 4
getfield com/a/c/c/a/a Ljava/lang/Class;
invokestatic com/a/c/b/ap/a(Ljava/lang/reflect/Type;)Z
invokespecial com/a/c/b/a/r/<init>(Lcom/a/c/b/a/q;Ljava/lang/String;ZZLcom/a/c/k;Ljava/lang/reflect/Field;Lcom/a/c/c/a;Z)V
areturn
.limit locals 7
.limit stack 10
.end method

.method private a(Ljava/lang/reflect/Field;)Ljava/lang/String;
aload 1
ldc com/a/c/a/c
invokevirtual java/lang/reflect/Field/getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
checkcast com/a/c/a/c
astore 2
aload 2
ifnonnull L0
aload 0
getfield com/a/c/b/a/q/b Lcom/a/c/j;
aload 1
invokeinterface com/a/c/j/a(Ljava/lang/reflect/Field;)Ljava/lang/String; 1
areturn
L0:
aload 2
invokeinterface com/a/c/a/c/a()Ljava/lang/String; 0
areturn
.limit locals 3
.limit stack 2
.end method

.method private a(Lcom/a/c/k;Lcom/a/c/c/a;Ljava/lang/Class;)Ljava/util/Map;
new java/util/LinkedHashMap
dup
invokespecial java/util/LinkedHashMap/<init>()V
astore 9
aload 3
invokevirtual java/lang/Class/isInterface()Z
ifeq L0
aload 9
areturn
L0:
aload 2
getfield com/a/c/c/a/b Ljava/lang/reflect/Type;
astore 10
L1:
aload 3
ldc java/lang/Object
if_acmpeq L2
aload 3
invokevirtual java/lang/Class/getDeclaredFields()[Ljava/lang/reflect/Field;
astore 11
aload 11
arraylength
istore 5
iconst_0
istore 4
L3:
iload 4
iload 5
if_icmpge L4
aload 11
iload 4
aaload
astore 12
aload 0
aload 12
iconst_1
invokespecial com/a/c/b/a/q/a(Ljava/lang/reflect/Field;Z)Z
istore 6
aload 0
aload 12
iconst_0
invokespecial com/a/c/b/a/q/a(Ljava/lang/reflect/Field;Z)Z
istore 7
iload 6
ifne L5
iload 7
ifeq L6
L5:
aload 12
iconst_1
invokevirtual java/lang/reflect/Field/setAccessible(Z)V
aload 2
getfield com/a/c/c/a/b Ljava/lang/reflect/Type;
aload 3
aload 12
invokevirtual java/lang/reflect/Field/getGenericType()Ljava/lang/reflect/Type;
invokestatic com/a/c/b/b/a(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
astore 13
aload 12
ldc com/a/c/a/c
invokevirtual java/lang/reflect/Field/getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
checkcast com/a/c/a/c
astore 8
aload 8
ifnonnull L7
aload 0
getfield com/a/c/b/a/q/b Lcom/a/c/j;
aload 12
invokeinterface com/a/c/j/a(Ljava/lang/reflect/Field;)Ljava/lang/String; 1
astore 8
L8:
aload 13
invokestatic com/a/c/c/a/a(Ljava/lang/reflect/Type;)Lcom/a/c/c/a;
astore 13
new com/a/c/b/a/r
dup
aload 0
aload 8
iload 6
iload 7
aload 1
aload 12
aload 13
aload 13
getfield com/a/c/c/a/a Ljava/lang/Class;
invokestatic com/a/c/b/ap/a(Ljava/lang/reflect/Type;)Z
invokespecial com/a/c/b/a/r/<init>(Lcom/a/c/b/a/q;Ljava/lang/String;ZZLcom/a/c/k;Ljava/lang/reflect/Field;Lcom/a/c/c/a;Z)V
astore 8
aload 9
aload 8
getfield com/a/c/b/a/t/g Ljava/lang/String;
aload 8
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
checkcast com/a/c/b/a/t
astore 8
aload 8
ifnull L6
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 10
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " declares multiple JSON fields named "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 8
getfield com/a/c/b/a/t/g Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L7:
aload 8
invokeinterface com/a/c/a/c/a()Ljava/lang/String; 0
astore 8
goto L8
L6:
iload 4
iconst_1
iadd
istore 4
goto L3
L4:
aload 2
getfield com/a/c/c/a/b Ljava/lang/reflect/Type;
aload 3
aload 3
invokevirtual java/lang/Class/getGenericSuperclass()Ljava/lang/reflect/Type;
invokestatic com/a/c/b/b/a(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
invokestatic com/a/c/c/a/a(Ljava/lang/reflect/Type;)Lcom/a/c/c/a;
astore 2
aload 2
getfield com/a/c/c/a/a Ljava/lang/Class;
astore 3
goto L1
L2:
aload 9
areturn
.limit locals 14
.limit stack 10
.end method

.method private a(Ljava/lang/reflect/Field;Z)Z
aload 0
getfield com/a/c/b/a/q/c Lcom/a/c/b/s;
aload 1
invokevirtual java/lang/reflect/Field/getType()Ljava/lang/Class;
iload 2
invokevirtual com/a/c/b/s/a(Ljava/lang/Class;Z)Z
ifne L0
aload 0
getfield com/a/c/b/a/q/c Lcom/a/c/b/s;
astore 4
aload 4
getfield com/a/c/b/s/c I
aload 1
invokevirtual java/lang/reflect/Field/getModifiers()I
iand
ifeq L1
iconst_1
istore 3
L2:
iload 3
ifne L0
iconst_1
ireturn
L1:
aload 4
getfield com/a/c/b/s/b D
ldc2_w -1.0D
dcmpl
ifeq L3
aload 4
aload 1
ldc com/a/c/a/d
invokevirtual java/lang/reflect/Field/getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
checkcast com/a/c/a/d
aload 1
ldc com/a/c/a/e
invokevirtual java/lang/reflect/Field/getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
checkcast com/a/c/a/e
invokevirtual com/a/c/b/s/a(Lcom/a/c/a/d;Lcom/a/c/a/e;)Z
ifne L3
iconst_1
istore 3
goto L2
L3:
aload 1
invokevirtual java/lang/reflect/Field/isSynthetic()Z
ifeq L4
iconst_1
istore 3
goto L2
L4:
aload 4
getfield com/a/c/b/s/e Z
ifeq L5
aload 1
ldc com/a/c/a/a
invokevirtual java/lang/reflect/Field/getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
checkcast com/a/c/a/a
astore 5
aload 5
ifnull L6
iload 2
ifeq L7
aload 5
invokeinterface com/a/c/a/a/a()Z 0
ifne L5
L6:
iconst_1
istore 3
goto L2
L7:
aload 5
invokeinterface com/a/c/a/a/b()Z 0
ifeq L6
L5:
aload 4
getfield com/a/c/b/s/d Z
ifne L8
aload 1
invokevirtual java/lang/reflect/Field/getType()Ljava/lang/Class;
invokestatic com/a/c/b/s/b(Ljava/lang/Class;)Z
ifeq L8
iconst_1
istore 3
goto L2
L8:
aload 1
invokevirtual java/lang/reflect/Field/getType()Ljava/lang/Class;
invokestatic com/a/c/b/s/a(Ljava/lang/Class;)Z
ifeq L9
iconst_1
istore 3
goto L2
L9:
iload 2
ifeq L10
aload 4
getfield com/a/c/b/s/f Ljava/util/List;
astore 4
L11:
aload 4
invokeinterface java/util/List/isEmpty()Z 0
ifne L12
new com/a/c/c
dup
aload 1
invokespecial com/a/c/c/<init>(Ljava/lang/reflect/Field;)V
pop
aload 4
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 1
L13:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L12
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/c/b
invokeinterface com/a/c/b/a()Z 0
ifeq L13
iconst_1
istore 3
goto L2
L10:
aload 4
getfield com/a/c/b/s/g Ljava/util/List;
astore 4
goto L11
L12:
iconst_0
istore 3
goto L2
L0:
iconst_0
ireturn
.limit locals 6
.limit stack 4
.end method

.method public final a(Lcom/a/c/k;Lcom/a/c/c/a;)Lcom/a/c/an;
aload 2
getfield com/a/c/c/a/a Ljava/lang/Class;
astore 3
ldc java/lang/Object
aload 3
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ifne L0
aconst_null
areturn
L0:
new com/a/c/b/a/s
dup
aload 0
getfield com/a/c/b/a/q/a Lcom/a/c/b/f;
aload 2
invokevirtual com/a/c/b/f/a(Lcom/a/c/c/a;)Lcom/a/c/b/ao;
aload 0
aload 1
aload 2
aload 3
invokespecial com/a/c/b/a/q/a(Lcom/a/c/k;Lcom/a/c/c/a;Ljava/lang/Class;)Ljava/util/Map;
iconst_0
invokespecial com/a/c/b/a/s/<init>(Lcom/a/c/b/ao;Ljava/util/Map;B)V
areturn
.limit locals 4
.limit stack 7
.end method
