.bytecode 50.0
.class public final synchronized com/a/c/b/a/j
.super com/a/c/d/e

.field private static final 'f' Ljava/io/Writer;

.field private static final 'g' Lcom/a/c/ac;

.field private final 'h' Ljava/util/List;

.field private 'i' Ljava/lang/String;

.field private 'j' Lcom/a/c/w;

.method static <clinit>()V
new com/a/c/b/a/k
dup
invokespecial com/a/c/b/a/k/<init>()V
putstatic com/a/c/b/a/j/f Ljava/io/Writer;
new com/a/c/ac
dup
ldc "closed"
invokespecial com/a/c/ac/<init>(Ljava/lang/String;)V
putstatic com/a/c/b/a/j/g Lcom/a/c/ac;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>()V
aload 0
getstatic com/a/c/b/a/j/f Ljava/io/Writer;
invokespecial com/a/c/d/e/<init>(Ljava/io/Writer;)V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield com/a/c/b/a/j/h Ljava/util/List;
aload 0
getstatic com/a/c/y/a Lcom/a/c/y;
putfield com/a/c/b/a/j/j Lcom/a/c/w;
return
.limit locals 1
.limit stack 3
.end method

.method private a(Lcom/a/c/w;)V
aload 0
getfield com/a/c/b/a/j/i Ljava/lang/String;
ifnull L0
aload 1
instanceof com/a/c/y
ifeq L1
aload 0
getfield com/a/c/d/e/e Z
ifeq L2
L1:
aload 0
invokespecial com/a/c/b/a/j/g()Lcom/a/c/w;
checkcast com/a/c/z
aload 0
getfield com/a/c/b/a/j/i Ljava/lang/String;
aload 1
invokevirtual com/a/c/z/a(Ljava/lang/String;Lcom/a/c/w;)V
L2:
aload 0
aconst_null
putfield com/a/c/b/a/j/i Ljava/lang/String;
return
L0:
aload 0
getfield com/a/c/b/a/j/h Ljava/util/List;
invokeinterface java/util/List/isEmpty()Z 0
ifeq L3
aload 0
aload 1
putfield com/a/c/b/a/j/j Lcom/a/c/w;
return
L3:
aload 0
invokespecial com/a/c/b/a/j/g()Lcom/a/c/w;
astore 2
aload 2
instanceof com/a/c/t
ifeq L4
aload 2
checkcast com/a/c/t
aload 1
invokevirtual com/a/c/t/a(Lcom/a/c/w;)V
return
L4:
new java/lang/IllegalStateException
dup
invokespecial java/lang/IllegalStateException/<init>()V
athrow
.limit locals 3
.limit stack 3
.end method

.method private g()Lcom/a/c/w;
aload 0
getfield com/a/c/b/a/j/h Ljava/util/List;
aload 0
getfield com/a/c/b/a/j/h Ljava/util/List;
invokeinterface java/util/List/size()I 0
iconst_1
isub
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/a/c/w
areturn
.limit locals 1
.limit stack 3
.end method

.method public final a(D)Lcom/a/c/d/e;
aload 0
getfield com/a/c/d/e/c Z
ifne L0
dload 1
invokestatic java/lang/Double/isNaN(D)Z
ifne L1
dload 1
invokestatic java/lang/Double/isInfinite(D)Z
ifeq L0
L1:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "JSON forbids NaN and infinities: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
dload 1
invokevirtual java/lang/StringBuilder/append(D)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
new com/a/c/ac
dup
dload 1
invokestatic java/lang/Double/valueOf(D)Ljava/lang/Double;
invokespecial com/a/c/ac/<init>(Ljava/lang/Number;)V
invokespecial com/a/c/b/a/j/a(Lcom/a/c/w;)V
aload 0
areturn
.limit locals 3
.limit stack 5
.end method

.method public final a(J)Lcom/a/c/d/e;
aload 0
new com/a/c/ac
dup
lload 1
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokespecial com/a/c/ac/<init>(Ljava/lang/Number;)V
invokespecial com/a/c/b/a/j/a(Lcom/a/c/w;)V
aload 0
areturn
.limit locals 3
.limit stack 5
.end method

.method public final a(Ljava/lang/Number;)Lcom/a/c/d/e;
aload 1
ifnonnull L0
aload 0
invokevirtual com/a/c/b/a/j/f()Lcom/a/c/d/e;
areturn
L0:
aload 0
getfield com/a/c/d/e/c Z
ifne L1
aload 1
invokevirtual java/lang/Number/doubleValue()D
dstore 2
dload 2
invokestatic java/lang/Double/isNaN(D)Z
ifne L2
dload 2
invokestatic java/lang/Double/isInfinite(D)Z
ifeq L1
L2:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "JSON forbids NaN and infinities: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
new com/a/c/ac
dup
aload 1
invokespecial com/a/c/ac/<init>(Ljava/lang/Number;)V
invokespecial com/a/c/b/a/j/a(Lcom/a/c/w;)V
aload 0
areturn
.limit locals 4
.limit stack 5
.end method

.method public final a(Ljava/lang/String;)Lcom/a/c/d/e;
aload 0
getfield com/a/c/b/a/j/h Ljava/util/List;
invokeinterface java/util/List/isEmpty()Z 0
ifne L0
aload 0
getfield com/a/c/b/a/j/i Ljava/lang/String;
ifnull L1
L0:
new java/lang/IllegalStateException
dup
invokespecial java/lang/IllegalStateException/<init>()V
athrow
L1:
aload 0
invokespecial com/a/c/b/a/j/g()Lcom/a/c/w;
instanceof com/a/c/z
ifeq L2
aload 0
aload 1
putfield com/a/c/b/a/j/i Ljava/lang/String;
aload 0
areturn
L2:
new java/lang/IllegalStateException
dup
invokespecial java/lang/IllegalStateException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public final a(Z)Lcom/a/c/d/e;
aload 0
new com/a/c/ac
dup
iload 1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokespecial com/a/c/ac/<init>(Ljava/lang/Boolean;)V
invokespecial com/a/c/b/a/j/a(Lcom/a/c/w;)V
aload 0
areturn
.limit locals 2
.limit stack 4
.end method

.method public final a()Lcom/a/c/w;
aload 0
getfield com/a/c/b/a/j/h Ljava/util/List;
invokeinterface java/util/List/isEmpty()Z 0
ifne L0
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Expected one JSON element but was "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/a/c/b/a/j/h Ljava/util/List;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield com/a/c/b/a/j/j Lcom/a/c/w;
areturn
.limit locals 1
.limit stack 5
.end method

.method public final b()Lcom/a/c/d/e;
new com/a/c/t
dup
invokespecial com/a/c/t/<init>()V
astore 1
aload 0
aload 1
invokespecial com/a/c/b/a/j/a(Lcom/a/c/w;)V
aload 0
getfield com/a/c/b/a/j/h Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final b(Ljava/lang/String;)Lcom/a/c/d/e;
aload 1
ifnonnull L0
aload 0
invokevirtual com/a/c/b/a/j/f()Lcom/a/c/d/e;
areturn
L0:
aload 0
new com/a/c/ac
dup
aload 1
invokespecial com/a/c/ac/<init>(Ljava/lang/String;)V
invokespecial com/a/c/b/a/j/a(Lcom/a/c/w;)V
aload 0
areturn
.limit locals 2
.limit stack 4
.end method

.method public final c()Lcom/a/c/d/e;
aload 0
getfield com/a/c/b/a/j/h Ljava/util/List;
invokeinterface java/util/List/isEmpty()Z 0
ifne L0
aload 0
getfield com/a/c/b/a/j/i Ljava/lang/String;
ifnull L1
L0:
new java/lang/IllegalStateException
dup
invokespecial java/lang/IllegalStateException/<init>()V
athrow
L1:
aload 0
invokespecial com/a/c/b/a/j/g()Lcom/a/c/w;
instanceof com/a/c/t
ifeq L2
aload 0
getfield com/a/c/b/a/j/h Ljava/util/List;
aload 0
getfield com/a/c/b/a/j/h Ljava/util/List;
invokeinterface java/util/List/size()I 0
iconst_1
isub
invokeinterface java/util/List/remove(I)Ljava/lang/Object; 1
pop
aload 0
areturn
L2:
new java/lang/IllegalStateException
dup
invokespecial java/lang/IllegalStateException/<init>()V
athrow
.limit locals 1
.limit stack 3
.end method

.method public final close()V
aload 0
getfield com/a/c/b/a/j/h Ljava/util/List;
invokeinterface java/util/List/isEmpty()Z 0
ifne L0
new java/io/IOException
dup
ldc "Incomplete document"
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield com/a/c/b/a/j/h Ljava/util/List;
getstatic com/a/c/b/a/j/g Lcom/a/c/ac;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
return
.limit locals 1
.limit stack 3
.end method

.method public final d()Lcom/a/c/d/e;
new com/a/c/z
dup
invokespecial com/a/c/z/<init>()V
astore 1
aload 0
aload 1
invokespecial com/a/c/b/a/j/a(Lcom/a/c/w;)V
aload 0
getfield com/a/c/b/a/j/h Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final e()Lcom/a/c/d/e;
aload 0
getfield com/a/c/b/a/j/h Ljava/util/List;
invokeinterface java/util/List/isEmpty()Z 0
ifne L0
aload 0
getfield com/a/c/b/a/j/i Ljava/lang/String;
ifnull L1
L0:
new java/lang/IllegalStateException
dup
invokespecial java/lang/IllegalStateException/<init>()V
athrow
L1:
aload 0
invokespecial com/a/c/b/a/j/g()Lcom/a/c/w;
instanceof com/a/c/z
ifeq L2
aload 0
getfield com/a/c/b/a/j/h Ljava/util/List;
aload 0
getfield com/a/c/b/a/j/h Ljava/util/List;
invokeinterface java/util/List/size()I 0
iconst_1
isub
invokeinterface java/util/List/remove(I)Ljava/lang/Object; 1
pop
aload 0
areturn
L2:
new java/lang/IllegalStateException
dup
invokespecial java/lang/IllegalStateException/<init>()V
athrow
.limit locals 1
.limit stack 3
.end method

.method public final f()Lcom/a/c/d/e;
aload 0
getstatic com/a/c/y/a Lcom/a/c/y;
invokespecial com/a/c/b/a/j/a(Lcom/a/c/w;)V
aload 0
areturn
.limit locals 1
.limit stack 2
.end method

.method public final flush()V
return
.limit locals 1
.limit stack 0
.end method
