.bytecode 50.0
.class final synchronized com/a/c/b/c
.super java/lang/Object
.implements java/io/Serializable
.implements java/lang/reflect/GenericArrayType

.field private static final 'b' J = 0L


.field private final 'a' Ljava/lang/reflect/Type;

.method public <init>(Ljava/lang/reflect/Type;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokestatic com/a/c/b/b/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
putfield com/a/c/b/c/a Ljava/lang/reflect/Type;
return
.limit locals 2
.limit stack 2
.end method

.method public final equals(Ljava/lang/Object;)Z
aload 1
instanceof java/lang/reflect/GenericArrayType
ifeq L0
aload 0
aload 1
checkcast java/lang/reflect/GenericArrayType
invokestatic com/a/c/b/b/a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final getGenericComponentType()Ljava/lang/reflect/Type;
aload 0
getfield com/a/c/b/c/a Ljava/lang/reflect/Type;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final hashCode()I
aload 0
getfield com/a/c/b/c/a Ljava/lang/reflect/Type;
invokevirtual java/lang/Object/hashCode()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/a/c/b/c/a Ljava/lang/reflect/Type;
invokestatic com/a/c/b/b/c(Ljava/lang/reflect/Type;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "[]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method
