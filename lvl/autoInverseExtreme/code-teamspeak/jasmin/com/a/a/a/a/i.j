.bytecode 50.0
.class public synchronized abstract com/a/a/a/a/i
.super android/os/Binder
.implements com/a/a/a/a/h

.field static final 'a' I = 1


.field private static final 'b' Ljava/lang/String; = "com.android.vending.licensing.ILicensingService"

.method public <init>()V
aload 0
invokespecial android/os/Binder/<init>()V
aload 0
aload 0
ldc "com.android.vending.licensing.ILicensingService"
invokevirtual com/a/a/a/a/i/attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
return
.limit locals 1
.limit stack 3
.end method

.method public static a(Landroid/os/IBinder;)Lcom/a/a/a/a/h;
aload 0
ifnonnull L0
aconst_null
areturn
L0:
aload 0
ldc "com.android.vending.licensing.ILicensingService"
invokeinterface android/os/IBinder/queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface; 1
astore 1
aload 1
ifnull L1
aload 1
instanceof com/a/a/a/a/h
ifeq L1
aload 1
checkcast com/a/a/a/a/h
areturn
L1:
new com/a/a/a/a/j
dup
aload 0
invokespecial com/a/a/a/a/j/<init>(Landroid/os/IBinder;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public asBinder()Landroid/os/IBinder;
aload 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
iload 1
lookupswitch
1 : L0
1598968902 : L1
default : L2
L2:
aload 0
iload 1
aload 2
aload 3
iload 4
invokespecial android/os/Binder/onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
ireturn
L1:
aload 3
ldc "com.android.vending.licensing.ILicensingService"
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L0:
aload 2
ldc "com.android.vending.licensing.ILicensingService"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 2
invokevirtual android/os/Parcel/readLong()J
lstore 5
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
astore 3
aload 2
invokevirtual android/os/Parcel/readStrongBinder()Landroid/os/IBinder;
astore 2
aload 2
ifnonnull L3
aconst_null
astore 2
L4:
aload 0
lload 5
aload 3
aload 2
invokevirtual com/a/a/a/a/i/a(JLjava/lang/String;Lcom/a/a/a/a/e;)V
iconst_1
ireturn
L3:
aload 2
ldc "com.android.vending.licensing.ILicenseResultListener"
invokeinterface android/os/IBinder/queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface; 1
astore 7
aload 7
ifnull L5
aload 7
instanceof com/a/a/a/a/e
ifeq L5
aload 7
checkcast com/a/a/a/a/e
astore 2
goto L4
L5:
new com/a/a/a/a/g
dup
aload 2
invokespecial com/a/a/a/a/g/<init>(Landroid/os/IBinder;)V
astore 2
goto L4
.limit locals 8
.limit stack 5
.end method
