.bytecode 50.0
.class public final synchronized com/a/a/a/a/t
.super java/lang/Object

.field private static final 'a' Ljava/lang/String; = "PreferenceObfuscator"

.field private final 'b' Landroid/content/SharedPreferences;

.field private final 'c' Lcom/a/a/a/a/r;

.field private 'd' Landroid/content/SharedPreferences$Editor;

.method public <init>(Landroid/content/SharedPreferences;Lcom/a/a/a/a/r;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/a/a/a/a/t/b Landroid/content/SharedPreferences;
aload 0
aload 2
putfield com/a/a/a/a/t/c Lcom/a/a/a/a/r;
aload 0
aconst_null
putfield com/a/a/a/a/t/d Landroid/content/SharedPreferences$Editor;
return
.limit locals 3
.limit stack 2
.end method

.method public final a()V
aload 0
getfield com/a/a/a/a/t/d Landroid/content/SharedPreferences$Editor;
ifnull L0
aload 0
getfield com/a/a/a/a/t/d Landroid/content/SharedPreferences$Editor;
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
aload 0
aconst_null
putfield com/a/a/a/a/t/d Landroid/content/SharedPreferences$Editor;
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
aload 0
getfield com/a/a/a/a/t/d Landroid/content/SharedPreferences$Editor;
ifnonnull L0
aload 0
aload 0
getfield com/a/a/a/a/t/b Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
putfield com/a/a/a/a/t/d Landroid/content/SharedPreferences$Editor;
L0:
aload 0
getfield com/a/a/a/a/t/c Lcom/a/a/a/a/r;
aload 2
aload 1
invokeinterface com/a/a/a/a/r/a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
astore 2
aload 0
getfield com/a/a/a/a/t/d Landroid/content/SharedPreferences$Editor;
aload 1
aload 2
invokeinterface android/content/SharedPreferences$Editor/putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 2
pop
return
.limit locals 3
.limit stack 3
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.catch com/a/a/a/a/y from L0 to L1 using L2
aload 0
getfield com/a/a/a/a/t/b Landroid/content/SharedPreferences;
aload 1
aconst_null
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
astore 4
aload 2
astore 3
aload 4
ifnull L1
L0:
aload 0
getfield com/a/a/a/a/t/c Lcom/a/a/a/a/r;
aload 4
aload 1
invokeinterface com/a/a/a/a/r/b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
astore 3
L1:
aload 3
areturn
L2:
astore 3
ldc "PreferenceObfuscator"
new java/lang/StringBuilder
dup
ldc "Validation error while reading preference: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 2
areturn
.limit locals 5
.limit stack 4
.end method
