.bytecode 50.0
.class public final synchronized com/a/a/a/a/k
.super java/lang/Object
.implements android/content/ServiceConnection

.field private static final 'b' Ljava/lang/String; = "LicenseChecker"

.field private static final 'c' Ljava/lang/String; = "RSA"

.field private static final 'd' I = 10000


.field private static final 'e' Ljava/security/SecureRandom;

.field private static final 'f' Z = 0


.field public 'a' I

.field private 'g' Lcom/a/a/a/a/h;

.field private 'h' Ljava/security/PublicKey;

.field private final 'i' Landroid/content/Context;

.field private final 'j' Lcom/a/a/a/a/s;

.field private 'k' Landroid/os/Handler;

.field private final 'l' Ljava/lang/String;

.field private final 'm' Ljava/lang/String;

.field private final 'n' Ljava/util/Set;

.field private final 'o' Ljava/util/Queue;

.method static <clinit>()V
new java/security/SecureRandom
dup
invokespecial java/security/SecureRandom/<init>()V
putstatic com/a/a/a/a/k/e Ljava/security/SecureRandom;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>(Landroid/content/Context;Lcom/a/a/a/a/s;Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/HashSet
dup
invokespecial java/util/HashSet/<init>()V
putfield com/a/a/a/a/k/n Ljava/util/Set;
aload 0
new java/util/LinkedList
dup
invokespecial java/util/LinkedList/<init>()V
putfield com/a/a/a/a/k/o Ljava/util/Queue;
aload 0
aload 1
putfield com/a/a/a/a/k/i Landroid/content/Context;
aload 0
aload 2
putfield com/a/a/a/a/k/j Lcom/a/a/a/a/s;
aload 0
aload 3
invokestatic com/a/a/a/a/k/a(Ljava/lang/String;)Ljava/security/PublicKey;
putfield com/a/a/a/a/k/h Ljava/security/PublicKey;
aload 0
aload 0
getfield com/a/a/a/a/k/i Landroid/content/Context;
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
putfield com/a/a/a/a/k/l Ljava/lang/String;
aload 0
aload 1
aload 0
getfield com/a/a/a/a/k/l Ljava/lang/String;
invokestatic com/a/a/a/a/k/a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
putfield com/a/a/a/a/k/m Ljava/lang/String;
new android/os/HandlerThread
dup
ldc "background thread"
invokespecial android/os/HandlerThread/<init>(Ljava/lang/String;)V
astore 1
aload 1
invokevirtual android/os/HandlerThread/start()V
aload 0
new android/os/Handler
dup
aload 1
invokevirtual android/os/HandlerThread/getLooper()Landroid/os/Looper;
invokespecial android/os/Handler/<init>(Landroid/os/Looper;)V
putfield com/a/a/a/a/k/k Landroid/os/Handler;
return
.limit locals 4
.limit stack 4
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
L0:
aload 0
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
aload 1
iconst_0
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
getfield android/content/pm/PackageInfo/versionCode I
istore 2
L1:
iload 2
invokestatic java/lang/String/valueOf(I)Ljava/lang/String;
areturn
L2:
astore 0
ldc "LicenseChecker"
ldc "Package not found. could not get version code."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
ldc ""
areturn
.limit locals 3
.limit stack 3
.end method

.method private static a(Ljava/lang/String;)Ljava/security/PublicKey;
.catch java/security/NoSuchAlgorithmException from L0 to L1 using L2
.catch com/a/a/a/a/a/b from L0 to L1 using L3
.catch java/security/spec/InvalidKeySpecException from L0 to L1 using L4
L0:
aload 0
invokestatic com/a/a/a/a/a/a/a(Ljava/lang/String;)[B
astore 0
ldc "RSA"
invokestatic java/security/KeyFactory/getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;
new java/security/spec/X509EncodedKeySpec
dup
aload 0
invokespecial java/security/spec/X509EncodedKeySpec/<init>([B)V
invokevirtual java/security/KeyFactory/generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;
astore 0
L1:
aload 0
areturn
L2:
astore 0
new java/lang/RuntimeException
dup
aload 0
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
L3:
astore 0
ldc "LicenseChecker"
ldc "Could not decode from Base64."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
new java/lang/IllegalArgumentException
dup
aload 0
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/Throwable;)V
athrow
L4:
astore 0
ldc "LicenseChecker"
ldc "Invalid key specification."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
new java/lang/IllegalArgumentException
dup
aload 0
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 1
.limit stack 4
.end method

.method static synthetic a(Lcom/a/a/a/a/k;)Ljava/util/Set;
aload 0
getfield com/a/a/a/a/k/n Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a()V
.catch android/os/RemoteException from L0 to L1 using L2
L3:
aload 0
getfield com/a/a/a/a/k/o Ljava/util/Queue;
invokeinterface java/util/Queue/poll()Ljava/lang/Object; 0
checkcast com/a/a/a/a/p
astore 1
aload 1
ifnull L4
L0:
ldc "LicenseChecker"
new java/lang/StringBuilder
dup
ldc "Calling checkLicense on service for "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
getfield com/a/a/a/a/p/c Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/a/a/a/a/k/g Lcom/a/a/a/a/h;
aload 1
getfield com/a/a/a/a/p/b I
i2l
aload 1
getfield com/a/a/a/a/p/c Ljava/lang/String;
new com/a/a/a/a/l
dup
aload 0
aload 1
invokespecial com/a/a/a/a/l/<init>(Lcom/a/a/a/a/k;Lcom/a/a/a/a/p;)V
invokeinterface com/a/a/a/a/h/a(JLjava/lang/String;Lcom/a/a/a/a/e;)V 4
aload 0
getfield com/a/a/a/a/k/n Ljava/util/Set;
aload 1
invokeinterface java/util/Set/add(Ljava/lang/Object;)Z 1
pop
L1:
goto L3
L2:
astore 2
ldc "LicenseChecker"
ldc "RemoteException in checkLicense call."
aload 2
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
aload 0
aload 1
invokespecial com/a/a/a/a/k/b(Lcom/a/a/a/a/p;)V
goto L3
L4:
return
.limit locals 3
.limit stack 8
.end method

.method static synthetic a(Lcom/a/a/a/a/k;Lcom/a/a/a/a/p;)V
aload 0
aload 1
invokespecial com/a/a/a/a/k/b(Lcom/a/a/a/a/p;)V
return
.limit locals 2
.limit stack 2
.end method

.method private a(Lcom/a/a/a/a/p;)V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
getfield com/a/a/a/a/k/n Ljava/util/Set;
aload 1
invokeinterface java/util/Set/remove(Ljava/lang/Object;)Z 1
pop
aload 0
getfield com/a/a/a/a/k/n Ljava/util/Set;
invokeinterface java/util/Set/isEmpty()Z 0
ifeq L1
aload 0
invokespecial com/a/a/a/a/k/c()V
L1:
aload 0
monitorexit
return
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method static synthetic b(Lcom/a/a/a/a/k;)Ljava/security/PublicKey;
aload 0
getfield com/a/a/a/a/k/h Ljava/security/PublicKey;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b()V
aload 0
iconst_0
putfield com/a/a/a/a/k/a I
aload 0
aload 0
getfield com/a/a/a/a/k/a I
bipush 16
iadd
putfield com/a/a/a/a/k/a I
return
.limit locals 1
.limit stack 3
.end method

.method static synthetic b(Lcom/a/a/a/a/k;Lcom/a/a/a/a/p;)V
aload 0
aload 1
invokespecial com/a/a/a/a/k/a(Lcom/a/a/a/a/p;)V
return
.limit locals 2
.limit stack 2
.end method

.method private b(Lcom/a/a/a/a/p;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
monitorenter
L0:
aload 0
getfield com/a/a/a/a/k/j Lcom/a/a/a/a/s;
sipush 291
aconst_null
invokeinterface com/a/a/a/a/s/a(ILcom/a/a/a/a/u;)V 2
aload 0
getfield com/a/a/a/a/k/j Lcom/a/a/a/a/s;
invokeinterface com/a/a/a/a/s/a()Z 0
ifeq L3
aload 1
getfield com/a/a/a/a/p/a Lcom/a/a/a/a/o;
invokeinterface com/a/a/a/a/o/a()V 0
L1:
aload 0
monitorexit
return
L3:
aload 1
getfield com/a/a/a/a/p/a Lcom/a/a/a/a/o;
sipush 291
invokeinterface com/a/a/a/a/o/a(I)V 1
L4:
goto L1
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 3
.end method

.method static synthetic c(Lcom/a/a/a/a/k;)Landroid/os/Handler;
aload 0
getfield com/a/a/a/a/k/k Landroid/os/Handler;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c()V
.catch java/lang/IllegalArgumentException from L0 to L1 using L2
aload 0
getfield com/a/a/a/a/k/g Lcom/a/a/a/a/h;
ifnull L3
L0:
aload 0
getfield com/a/a/a/a/k/i Landroid/content/Context;
aload 0
invokevirtual android/content/Context/unbindService(Landroid/content/ServiceConnection;)V
L1:
aload 0
aconst_null
putfield com/a/a/a/a/k/g Lcom/a/a/a/a/h;
L3:
return
L2:
astore 1
ldc "LicenseChecker"
ldc "Unable to unbind from licensing service (already unbound)"
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
goto L1
.limit locals 2
.limit stack 2
.end method

.method private d()V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
invokespecial com/a/a/a/a/k/c()V
aload 0
getfield com/a/a/a/a/k/k Landroid/os/Handler;
invokevirtual android/os/Handler/getLooper()Landroid/os/Looper;
invokevirtual android/os/Looper/quit()V
L1:
aload 0
monitorexit
return
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 1
.end method

.method private static e()I
getstatic com/a/a/a/a/k/e Ljava/security/SecureRandom;
invokevirtual java/security/SecureRandom/nextInt()I
ireturn
.limit locals 0
.limit stack 1
.end method

.method public final a(Lcom/a/a/a/a/o;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch java/lang/SecurityException from L4 to L5 using L6
.catch com/a/a/a/a/a/b from L4 to L5 using L7
.catch all from L4 to L5 using L2
.catch all from L8 to L9 using L2
.catch java/lang/SecurityException from L10 to L11 using L6
.catch com/a/a/a/a/a/b from L10 to L11 using L7
.catch all from L10 to L11 using L2
.catch all from L12 to L13 using L2
.catch all from L14 to L15 using L2
aload 0
monitorenter
L0:
aload 0
getfield com/a/a/a/a/k/j Lcom/a/a/a/a/s;
invokeinterface com/a/a/a/a/s/a()Z 0
ifeq L3
ldc "LicenseChecker"
ldc "Using cached license response"
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 1
invokeinterface com/a/a/a/a/o/a()V 0
aload 1
sipush 256
aload 0
getfield com/a/a/a/a/k/j Lcom/a/a/a/a/s;
invokeinterface com/a/a/a/a/s/b()Lcom/a/a/a/a/w; 0
getfield com/a/a/a/a/w/a Lcom/a/a/a/a/u;
aload 0
getfield com/a/a/a/a/k/j Lcom/a/a/a/a/s;
invokeinterface com/a/a/a/a/s/b()Lcom/a/a/a/a/w; 0
getfield com/a/a/a/a/w/b Ljava/lang/String;
aload 0
getfield com/a/a/a/a/k/j Lcom/a/a/a/a/s;
invokeinterface com/a/a/a/a/s/b()Lcom/a/a/a/a/w; 0
getfield com/a/a/a/a/w/c Ljava/lang/String;
invokeinterface com/a/a/a/a/o/a(ILcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V 4
L1:
aload 0
monitorexit
return
L3:
new com/a/a/a/a/p
dup
aload 0
getfield com/a/a/a/a/k/j Lcom/a/a/a/a/s;
new com/a/a/a/a/q
dup
invokespecial com/a/a/a/a/q/<init>()V
aload 1
getstatic com/a/a/a/a/k/e Ljava/security/SecureRandom;
invokevirtual java/security/SecureRandom/nextInt()I
aload 0
getfield com/a/a/a/a/k/l Ljava/lang/String;
aload 0
getfield com/a/a/a/a/k/m Ljava/lang/String;
invokespecial com/a/a/a/a/p/<init>(Lcom/a/a/a/a/s;Lcom/a/a/a/a/d;Lcom/a/a/a/a/o;ILjava/lang/String;Ljava/lang/String;)V
astore 2
aload 0
getfield com/a/a/a/a/k/g Lcom/a/a/a/a/h;
ifnonnull L14
ldc "LicenseChecker"
ldc "Binding to licensing service."
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;)I
pop
L4:
new android/content/Intent
dup
new java/lang/String
dup
ldc "Y29tLmFuZHJvaWQudmVuZGluZy5saWNlbnNpbmcuSUxpY2Vuc2luZ1NlcnZpY2U="
invokestatic com/a/a/a/a/a/a/a(Ljava/lang/String;)[B
invokespecial java/lang/String/<init>([B)V
invokespecial android/content/Intent/<init>(Ljava/lang/String;)V
astore 3
aload 3
ldc "com.android.vending"
invokevirtual android/content/Intent/setPackage(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 0
getfield com/a/a/a/a/k/i Landroid/content/Context;
aload 3
aload 0
iconst_1
invokevirtual android/content/Context/bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
ifeq L10
aload 0
getfield com/a/a/a/a/k/o Ljava/util/Queue;
aload 2
invokeinterface java/util/Queue/offer(Ljava/lang/Object;)Z 1
pop
L5:
goto L1
L6:
astore 2
L8:
aload 1
bipush 6
invokeinterface com/a/a/a/a/o/b(I)V 1
L9:
goto L1
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
L10:
ldc "LicenseChecker"
ldc "Could not bind to service."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
aload 2
invokespecial com/a/a/a/a/k/b(Lcom/a/a/a/a/p;)V
L11:
goto L1
L7:
astore 1
L12:
aload 1
invokevirtual com/a/a/a/a/a/b/printStackTrace()V
L13:
goto L1
L14:
aload 0
getfield com/a/a/a/a/k/o Ljava/util/Queue;
aload 2
invokeinterface java/util/Queue/offer(Ljava/lang/Object;)Z 1
pop
aload 0
invokespecial com/a/a/a/a/k/a()V
L15:
goto L1
.limit locals 4
.limit stack 8
.end method

.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
aload 2
invokestatic com/a/a/a/a/i/a(Landroid/os/IBinder;)Lcom/a/a/a/a/h;
putfield com/a/a/a/a/k/g Lcom/a/a/a/a/h;
aload 0
invokespecial com/a/a/a/a/k/a()V
L1:
aload 0
monitorexit
return
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
ldc "LicenseChecker"
ldc "Service unexpectedly disconnected."
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
aconst_null
putfield com/a/a/a/a/k/g Lcom/a/a/a/a/h;
L1:
aload 0
monitorexit
return
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 2
.end method
