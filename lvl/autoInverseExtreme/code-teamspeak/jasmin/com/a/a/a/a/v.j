.bytecode 50.0
.class public final synchronized com/a/a/a/a/v
.super java/lang/Object
.implements com/a/a/a/a/s

.field private static final 'a' Ljava/lang/String; = "ServerManagedPolicy"

.field private static final 'b' Ljava/lang/String; = "com.android.vending.licensing.ServerManagedPolicy"

.field private static final 'f' Ljava/lang/String; = "lastResponse"

.field private static final 'g' Ljava/lang/String; = "validityTimestamp"

.field private static final 'h' Ljava/lang/String; = "retryUntil"

.field private static final 'i' Ljava/lang/String; = "maxRetries"

.field private static final 'j' Ljava/lang/String; = "retryCount"

.field private static final 'k' Ljava/lang/String; = "rawData"

.field private static final 'l' Ljava/lang/String; = "0"

.field private static final 'm' Ljava/lang/String; = "0"

.field private static final 'n' Ljava/lang/String; = "0"

.field private static final 'o' Ljava/lang/String; = "0"

.field private static final 'p' J = 60000L


.field private 'q' J

.field private 'r' J

.field private 's' J

.field private 't' J

.field private 'u' J

.field private 'v' I

.field private 'w' Lcom/a/a/a/a/t;

.field private 'x' Ljava/lang/String;

.method public <init>(Landroid/content/Context;Lcom/a/a/a/a/r;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
lconst_0
putfield com/a/a/a/a/v/u J
aload 0
new com/a/a/a/a/t
dup
aload 1
ldc "com.android.vending.licensing.ServerManagedPolicy"
iconst_0
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
aload 2
invokespecial com/a/a/a/a/t/<init>(Landroid/content/SharedPreferences;Lcom/a/a/a/a/r;)V
putfield com/a/a/a/a/v/w Lcom/a/a/a/a/t;
aload 0
aload 0
getfield com/a/a/a/a/v/w Lcom/a/a/a/a/t;
ldc "rawData"
ldc ""
invokevirtual com/a/a/a/a/t/b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
putfield com/a/a/a/a/v/x Ljava/lang/String;
aload 0
aload 0
getfield com/a/a/a/a/v/w Lcom/a/a/a/a/t;
ldc "lastResponse"
sipush 291
invokestatic java/lang/Integer/toString(I)Ljava/lang/String;
invokevirtual com/a/a/a/a/t/b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
putfield com/a/a/a/a/v/v I
aload 0
aload 0
getfield com/a/a/a/a/v/w Lcom/a/a/a/a/t;
ldc "validityTimestamp"
ldc "0"
invokevirtual com/a/a/a/a/t/b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
putfield com/a/a/a/a/v/q J
aload 0
aload 0
getfield com/a/a/a/a/v/w Lcom/a/a/a/a/t;
ldc "retryUntil"
ldc "0"
invokevirtual com/a/a/a/a/t/b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
putfield com/a/a/a/a/v/r J
aload 0
aload 0
getfield com/a/a/a/a/v/w Lcom/a/a/a/a/t;
ldc "maxRetries"
ldc "0"
invokevirtual com/a/a/a/a/t/b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
putfield com/a/a/a/a/v/s J
aload 0
aload 0
getfield com/a/a/a/a/v/w Lcom/a/a/a/a/t;
ldc "retryCount"
ldc "0"
invokevirtual com/a/a/a/a/t/b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
putfield com/a/a/a/a/v/t J
return
.limit locals 3
.limit stack 6
.end method

.method private a(I)V
aload 0
invokestatic java/lang/System/currentTimeMillis()J
putfield com/a/a/a/a/v/u J
aload 0
iload 1
putfield com/a/a/a/a/v/v I
aload 0
getfield com/a/a/a/a/v/w Lcom/a/a/a/a/t;
ldc "lastResponse"
iload 1
invokestatic java/lang/Integer/toString(I)Ljava/lang/String;
invokevirtual com/a/a/a/a/t/a(Ljava/lang/String;Ljava/lang/String;)V
return
.limit locals 2
.limit stack 3
.end method

.method private a(J)V
aload 0
lload 1
putfield com/a/a/a/a/v/t J
aload 0
getfield com/a/a/a/a/v/w Lcom/a/a/a/a/t;
ldc "retryCount"
lload 1
invokestatic java/lang/Long/toString(J)Ljava/lang/String;
invokevirtual com/a/a/a/a/t/a(Ljava/lang/String;Ljava/lang/String;)V
return
.limit locals 3
.limit stack 4
.end method

.method private a(Ljava/lang/String;)V
aload 0
getfield com/a/a/a/a/v/w Lcom/a/a/a/a/t;
ldc "rawData"
aload 1
invokevirtual com/a/a/a/a/t/a(Ljava/lang/String;Ljava/lang/String;)V
return
.limit locals 2
.limit stack 3
.end method

.method private b(Ljava/lang/String;)V
.catch java/lang/NumberFormatException from L0 to L1 using L2
L0:
aload 1
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
lstore 2
L1:
lload 2
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
astore 5
aload 1
astore 4
aload 5
astore 1
L3:
aload 0
aload 1
invokevirtual java/lang/Long/longValue()J
putfield com/a/a/a/a/v/q J
aload 0
getfield com/a/a/a/a/v/w Lcom/a/a/a/a/t;
ldc "validityTimestamp"
aload 4
invokevirtual com/a/a/a/a/t/a(Ljava/lang/String;Ljava/lang/String;)V
return
L2:
astore 1
ldc "ServerManagedPolicy"
ldc "License validity timestamp (VT) missing, caching for a minute"
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
invokestatic java/lang/System/currentTimeMillis()J
ldc2_w 60000L
ladd
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
astore 1
aload 1
invokevirtual java/lang/Long/longValue()J
invokestatic java/lang/Long/toString(J)Ljava/lang/String;
astore 4
goto L3
.limit locals 6
.limit stack 4
.end method

.method private c()J
aload 0
getfield com/a/a/a/a/v/t J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private c(Ljava/lang/String;)V
.catch java/lang/NumberFormatException from L0 to L1 using L2
L0:
aload 1
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
lstore 2
L1:
lload 2
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
astore 5
aload 1
astore 4
aload 5
astore 1
L3:
aload 0
aload 1
invokevirtual java/lang/Long/longValue()J
putfield com/a/a/a/a/v/r J
aload 0
getfield com/a/a/a/a/v/w Lcom/a/a/a/a/t;
ldc "retryUntil"
aload 4
invokevirtual com/a/a/a/a/t/a(Ljava/lang/String;Ljava/lang/String;)V
return
L2:
astore 1
ldc "ServerManagedPolicy"
ldc "License retry timestamp (GT) missing, grace period disabled"
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
ldc "0"
astore 4
lconst_0
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
astore 1
goto L3
.limit locals 6
.limit stack 3
.end method

.method private d()J
aload 0
getfield com/a/a/a/a/v/q J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private d(Ljava/lang/String;)V
.catch java/lang/NumberFormatException from L0 to L1 using L2
L0:
aload 1
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
lstore 2
L1:
lload 2
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
astore 5
aload 1
astore 4
aload 5
astore 1
L3:
aload 0
aload 1
invokevirtual java/lang/Long/longValue()J
putfield com/a/a/a/a/v/s J
aload 0
getfield com/a/a/a/a/v/w Lcom/a/a/a/a/t;
ldc "maxRetries"
aload 4
invokevirtual com/a/a/a/a/t/a(Ljava/lang/String;Ljava/lang/String;)V
return
L2:
astore 1
ldc "ServerManagedPolicy"
ldc "Licence retry count (GR) missing, grace period disabled"
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
ldc "0"
astore 4
lconst_0
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
astore 1
goto L3
.limit locals 6
.limit stack 3
.end method

.method private e()J
aload 0
getfield com/a/a/a/a/v/r J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private static e(Ljava/lang/String;)Ljava/util/Map;
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
iconst_0
istore 1
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 3
L0:
aload 0
ldc "&"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 0
aload 0
arraylength
istore 2
L1:
iload 1
iload 2
if_icmpge L5
L3:
aload 0
iload 1
aaload
ldc "="
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 4
aload 3
aload 4
iconst_0
aaload
aload 4
iconst_1
aaload
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L4:
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
astore 0
ldc "ServerManagedPolicy"
ldc "Invalid syntax error while decoding extras data from server."
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
L5:
aload 3
areturn
.limit locals 5
.limit stack 4
.end method

.method private f()J
aload 0
getfield com/a/a/a/a/v/s J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public final a(ILcom/a/a/a/a/u;)V
iload 1
sipush 291
if_icmpeq L0
aload 0
lconst_0
invokespecial com/a/a/a/a/v/a(J)V
L1:
iload 1
sipush 256
if_icmpne L2
aload 2
getfield com/a/a/a/a/u/g Ljava/lang/String;
invokestatic com/a/a/a/a/v/e(Ljava/lang/String;)Ljava/util/Map;
astore 2
aload 0
iload 1
putfield com/a/a/a/a/v/v I
aload 0
aload 2
ldc "VT"
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
invokespecial com/a/a/a/a/v/b(Ljava/lang/String;)V
aload 0
aload 2
ldc "GT"
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
invokespecial com/a/a/a/a/v/c(Ljava/lang/String;)V
aload 0
aload 2
ldc "GR"
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
invokespecial com/a/a/a/a/v/d(Ljava/lang/String;)V
L3:
aload 0
invokestatic java/lang/System/currentTimeMillis()J
putfield com/a/a/a/a/v/u J
aload 0
iload 1
putfield com/a/a/a/a/v/v I
aload 0
getfield com/a/a/a/a/v/w Lcom/a/a/a/a/t;
ldc "lastResponse"
iload 1
invokestatic java/lang/Integer/toString(I)Ljava/lang/String;
invokevirtual com/a/a/a/a/t/a(Ljava/lang/String;Ljava/lang/String;)V
aload 0
getfield com/a/a/a/a/v/w Lcom/a/a/a/a/t;
invokevirtual com/a/a/a/a/t/a()V
return
L0:
aload 0
aload 0
getfield com/a/a/a/a/v/t J
lconst_1
ladd
invokespecial com/a/a/a/a/v/a(J)V
goto L1
L2:
iload 1
sipush 561
if_icmpne L3
aload 0
ldc "0"
invokespecial com/a/a/a/a/v/b(Ljava/lang/String;)V
aload 0
ldc "0"
invokespecial com/a/a/a/a/v/c(Ljava/lang/String;)V
aload 0
ldc "0"
invokespecial com/a/a/a/a/v/d(Ljava/lang/String;)V
aload 0
ldc ""
invokespecial com/a/a/a/a/v/a(Ljava/lang/String;)V
goto L3
.limit locals 3
.limit stack 5
.end method

.method public final a(Lcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
new com/a/a/a/a/w
dup
aload 1
aload 2
aload 3
invokespecial com/a/a/a/a/w/<init>(Lcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
astore 1
new com/a/c/k
dup
invokespecial com/a/c/k/<init>()V
astore 2
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
astore 3
new java/io/StringWriter
dup
invokespecial java/io/StringWriter/<init>()V
astore 4
aload 2
aload 1
aload 3
aload 4
invokevirtual com/a/c/k/a(Ljava/lang/Object;Ljava/lang/reflect/Type;Ljava/lang/Appendable;)V
aload 0
aload 4
invokevirtual java/io/StringWriter/toString()Ljava/lang/String;
invokespecial com/a/a/a/a/v/a(Ljava/lang/String;)V
aload 0
getfield com/a/a/a/a/v/w Lcom/a/a/a/a/t;
invokevirtual com/a/a/a/a/t/a()V
return
.limit locals 5
.limit stack 5
.end method

.method public final a()Z
invokestatic java/lang/System/currentTimeMillis()J
lstore 1
aload 0
getfield com/a/a/a/a/v/v I
sipush 256
if_icmpne L0
lload 1
aload 0
getfield com/a/a/a/a/v/q J
lcmp
ifgt L1
L2:
iconst_1
ireturn
L0:
aload 0
getfield com/a/a/a/a/v/v I
sipush 291
if_icmpne L1
lload 1
aload 0
getfield com/a/a/a/a/v/u J
ldc2_w 60000L
ladd
lcmp
ifge L1
lload 1
aload 0
getfield com/a/a/a/a/v/r J
lcmp
ifle L2
aload 0
getfield com/a/a/a/a/v/t J
aload 0
getfield com/a/a/a/a/v/s J
lcmp
ifle L2
iconst_0
ireturn
L1:
iconst_0
ireturn
.limit locals 3
.limit stack 6
.end method

.method public final b()Lcom/a/a/a/a/w;
new com/a/c/k
dup
invokespecial com/a/c/k/<init>()V
astore 1
aload 0
getfield com/a/a/a/a/v/x Ljava/lang/String;
astore 2
aload 2
ifnonnull L0
aconst_null
astore 1
L1:
ldc com/a/a/a/a/w
invokestatic com/a/c/b/ap/a(Ljava/lang/Class;)Ljava/lang/Class;
aload 1
invokevirtual java/lang/Class/cast(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/a/a/a/w
areturn
L0:
new com/a/c/d/a
dup
new java/io/StringReader
dup
aload 2
invokespecial java/io/StringReader/<init>(Ljava/lang/String;)V
invokespecial com/a/c/d/a/<init>(Ljava/io/Reader;)V
astore 2
aload 1
aload 2
ldc com/a/a/a/a/w
invokevirtual com/a/c/k/a(Lcom/a/c/d/a;Ljava/lang/reflect/Type;)Ljava/lang/Object;
astore 1
aload 1
aload 2
invokestatic com/a/c/k/a(Ljava/lang/Object;Lcom/a/c/d/a;)V
goto L1
.limit locals 3
.limit stack 5
.end method
