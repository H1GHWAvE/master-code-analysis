.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/c
.super android/support/v4/app/Fragment
.implements com/teamspeak/ts3client/data/d/c
.implements com/teamspeak/ts3client/data/w

.field public static 'a' Lcom/teamspeak/ts3client/c;

.field public static 'b' Lcom/teamspeak/ts3client/data/a;

.field private 'at' Landroid/widget/TableLayout;

.field private 'au' Landroid/webkit/WebView;

.field private 'av' Ljava/lang/String;

.field private 'aw' Landroid/widget/RelativeLayout;

.field private 'ax' Lcom/teamspeak/ts3client/customs/FloatingButton;

.field private 'c' Lcom/teamspeak/ts3client/Ts3Application;

.field private 'd' Landroid/support/v4/app/bi;

.field private 'e' Landroid/widget/TextView;

.field private 'f' Landroid/widget/TextView;

.field private 'g' Landroid/widget/TextView;

.field private 'h' Landroid/widget/TextView;

.field private 'i' Landroid/widget/TextView;

.field private 'j' Landroid/widget/TextView;

.field private 'k' Landroid/widget/TextView;

.field private 'l' Landroid/widget/TableRow;

.field private 'm' Landroid/widget/TableRow;

.method public <init>()V
aload 0
invokespecial android/support/v4/app/Fragment/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/c;)Landroid/support/v4/app/bi;
aload 0
getfield com/teamspeak/ts3client/c/d Landroid/support/v4/app/bi;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Lcom/teamspeak/ts3client/data/a;)Lcom/teamspeak/ts3client/c;
getstatic com/teamspeak/ts3client/c/b Lcom/teamspeak/ts3client/data/a;
aload 0
if_acmpne L0
getstatic com/teamspeak/ts3client/c/a Lcom/teamspeak/ts3client/c;
areturn
L0:
new com/teamspeak/ts3client/c
dup
invokespecial com/teamspeak/ts3client/c/<init>()V
putstatic com/teamspeak/ts3client/c/a Lcom/teamspeak/ts3client/c;
aload 0
putstatic com/teamspeak/ts3client/c/b Lcom/teamspeak/ts3client/data/a;
getstatic com/teamspeak/ts3client/c/a Lcom/teamspeak/ts3client/c;
areturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic a()Lcom/teamspeak/ts3client/data/a;
getstatic com/teamspeak/ts3client/c/b Lcom/teamspeak/ts3client/data/a;
areturn
.limit locals 0
.limit stack 1
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/c;Ljava/lang/String;)Ljava/lang/String;
aload 0
aload 1
putfield com/teamspeak/ts3client/c/av Ljava/lang/String;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Z)V
aload 0
invokevirtual com/teamspeak/ts3client/c/l()Z
ifeq L0
aload 0
invokevirtual com/teamspeak/ts3client/c/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/g
dup
aload 0
iload 1
invokespecial com/teamspeak/ts3client/g/<init>(Lcom/teamspeak/ts3client/c;Z)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
L0:
return
.limit locals 2
.limit stack 5
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/c;)Landroid/webkit/WebView;
aload 0
getfield com/teamspeak/ts3client/c/au Landroid/webkit/WebView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic b()Lcom/teamspeak/ts3client/c;
getstatic com/teamspeak/ts3client/c/a Lcom/teamspeak/ts3client/c;
areturn
.limit locals 0
.limit stack 1
.end method

.method private b(Ljava/lang/String;)V
aload 0
invokevirtual com/teamspeak/ts3client/c/l()Z
ifeq L0
aload 0
invokevirtual com/teamspeak/ts3client/c/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/f
dup
aload 0
aload 1
invokespecial com/teamspeak/ts3client/f/<init>(Lcom/teamspeak/ts3client/c;Ljava/lang/String;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
L0:
return
.limit locals 2
.limit stack 5
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/c;)Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/c/c Lcom/teamspeak/ts3client/Ts3Application;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/c;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/c/e Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/c;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/c/i Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/c;)Landroid/widget/TableRow;
aload 0
getfield com/teamspeak/ts3client/c/m Landroid/widget/TableRow;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic g(Lcom/teamspeak/ts3client/c;)Landroid/widget/TableLayout;
aload 0
getfield com/teamspeak/ts3client/c/at Landroid/widget/TableLayout;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic h(Lcom/teamspeak/ts3client/c;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/c/f Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic i(Lcom/teamspeak/ts3client/c;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/c/g Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic j(Lcom/teamspeak/ts3client/c;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/c/h Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic k(Lcom/teamspeak/ts3client/c;)Landroid/widget/TableRow;
aload 0
getfield com/teamspeak/ts3client/c/l Landroid/widget/TableRow;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic l(Lcom/teamspeak/ts3client/c;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/c/j Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic m(Lcom/teamspeak/ts3client/c;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/c/k Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic n(Lcom/teamspeak/ts3client/c;)Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/c/av Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
.annotation invisible Landroid/annotation/SuppressLint;
value [s = "NewApi" 
.end annotation
aload 1
ldc_w 2130903076
aload 2
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
astore 1
aload 0
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
putfield com/teamspeak/ts3client/c/d Landroid/support/v4/app/bi;
aload 0
aload 1
ldc_w 2131493071
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/RelativeLayout
putfield com/teamspeak/ts3client/c/aw Landroid/widget/RelativeLayout;
aload 0
aload 1
ldc_w 2131493073
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TableLayout
putfield com/teamspeak/ts3client/c/at Landroid/widget/TableLayout;
aload 0
aload 1
ldc_w 2131493076
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/c/e Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493082
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/c/f Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493085
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/c/g Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493086
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TableRow
putfield com/teamspeak/ts3client/c/l Landroid/widget/TableRow;
aload 0
aload 1
ldc_w 2131493088
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/c/h Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493077
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TableRow
putfield com/teamspeak/ts3client/c/m Landroid/widget/TableRow;
aload 0
aload 1
ldc_w 2131493079
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/c/i Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493091
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/c/j Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493094
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/c/k Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493097
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast com/teamspeak/ts3client/customs/FloatingButton
putfield com/teamspeak/ts3client/c/ax Lcom/teamspeak/ts3client/customs/FloatingButton;
aload 0
getfield com/teamspeak/ts3client/c/ax Lcom/teamspeak/ts3client/customs/FloatingButton;
ldc_w 2130837621
ldc_w 22.0F
ldc_w 22.0F
invokestatic com/teamspeak/ts3client/data/d/t/a(IFF)Landroid/graphics/Bitmap;
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setBitmap(Landroid/graphics/Bitmap;)V
aload 0
aload 1
ldc_w 2131493096
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/webkit/WebView
putfield com/teamspeak/ts3client/c/au Landroid/webkit/WebView;
ldc "channelinfo.name"
aload 1
ldc_w 2131493075
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/View;I)V
ldc "channelinfo.topic"
aload 1
ldc_w 2131493078
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/View;I)V
ldc "channelinfo.codec"
aload 1
ldc_w 2131493081
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/View;I)V
ldc "channelinfo.codecquality"
aload 1
ldc_w 2131493084
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/View;I)V
ldc "channelinfo.type"
aload 1
ldc_w 2131493087
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/View;I)V
ldc "channelinfo.currentclients"
aload 1
ldc_w 2131493090
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/View;I)V
ldc "channelinfo.subscription"
aload 1
ldc_w 2131493093
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/View;I)V
ldc "channelinfo.description"
aload 1
ldc_w 2131493095
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/View;I)V
aload 0
getfield com/teamspeak/ts3client/c/au Landroid/webkit/WebView;
iconst_0
invokevirtual android/webkit/WebView/setBackgroundColor(I)V
aload 0
getfield com/teamspeak/ts3client/c/au Landroid/webkit/WebView;
invokevirtual android/webkit/WebView/getSettings()Landroid/webkit/WebSettings;
iconst_0
invokevirtual android/webkit/WebSettings/setJavaScriptEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/c/au Landroid/webkit/WebView;
invokevirtual android/webkit/WebView/getSettings()Landroid/webkit/WebSettings;
getstatic android/webkit/WebSettings$PluginState/OFF Landroid/webkit/WebSettings$PluginState;
invokevirtual android/webkit/WebSettings/setPluginState(Landroid/webkit/WebSettings$PluginState;)V
aload 0
getfield com/teamspeak/ts3client/c/au Landroid/webkit/WebView;
invokevirtual android/webkit/WebView/getSettings()Landroid/webkit/WebSettings;
iconst_1
invokevirtual android/webkit/WebSettings/setAllowFileAccess(Z)V
aload 0
getfield com/teamspeak/ts3client/c/au Landroid/webkit/WebView;
invokevirtual android/webkit/WebView/getSettings()Landroid/webkit/WebSettings;
iconst_1
invokevirtual android/webkit/WebSettings/setBuiltInZoomControls(Z)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L0
aload 0
getfield com/teamspeak/ts3client/c/au Landroid/webkit/WebView;
invokevirtual android/webkit/WebView/getSettings()Landroid/webkit/WebSettings;
iconst_0
invokevirtual android/webkit/WebSettings/setDisplayZoomControls(Z)V
L0:
aload 0
getfield com/teamspeak/ts3client/c/au Landroid/webkit/WebView;
new com/teamspeak/ts3client/d
dup
aload 0
invokespecial com/teamspeak/ts3client/d/<init>(Lcom/teamspeak/ts3client/c;)V
invokevirtual android/webkit/WebView/setWebViewClient(Landroid/webkit/WebViewClient;)V
aload 0
getfield com/teamspeak/ts3client/c/ax Lcom/teamspeak/ts3client/customs/FloatingButton;
new com/teamspeak/ts3client/e
dup
aload 0
invokespecial com/teamspeak/ts3client/e/<init>(Lcom/teamspeak/ts3client/c;)V
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
invokevirtual com/teamspeak/ts3client/c/n()V
aload 0
getfield com/teamspeak/ts3client/c/c Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/o Landroid/support/v7/app/a;
ldc "dialog.channel.info.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/support/v7/app/a/b(Ljava/lang/CharSequence;)V
aload 1
invokevirtual android/view/View/requestFocus()Z
pop
aload 1
areturn
.limit locals 3
.limit stack 4
.end method

.method public final a(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/support/v4/app/Fragment/a(Landroid/os/Bundle;)V
aload 0
aload 0
invokevirtual com/teamspeak/ts3client/c/i()Landroid/support/v4/app/bb;
invokevirtual android/support/v4/app/bb/getApplicationContext()Landroid/content/Context;
checkcast com/teamspeak/ts3client/Ts3Application
putfield com/teamspeak/ts3client/c/c Lcom/teamspeak/ts3client/Ts3Application;
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
aload 1
invokeinterface android/view/Menu/clear()V 0
aload 0
aload 1
aload 2
invokespecial android/support/v4/app/Fragment/a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
return
.limit locals 3
.limit stack 3
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
aload 1
instanceof com/teamspeak/ts3client/jni/events/UpdateChannel
ifeq L0
aload 1
checkcast com/teamspeak/ts3client/jni/events/UpdateChannel
getfield com/teamspeak/ts3client/jni/events/UpdateChannel/a J
getstatic com/teamspeak/ts3client/c/b Lcom/teamspeak/ts3client/data/a;
getfield com/teamspeak/ts3client/data/a/b J
lcmp
ifne L1
aload 0
iconst_1
invokespecial com/teamspeak/ts3client/c/a(Z)V
aload 0
getfield com/teamspeak/ts3client/c/c Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/b(Lcom/teamspeak/ts3client/data/w;)V
L1:
return
L0:
aload 1
instanceof com/teamspeak/ts3client/jni/events/rare/ServerPermissionError
ifeq L1
aload 1
checkcast com/teamspeak/ts3client/jni/events/rare/ServerPermissionError
getfield com/teamspeak/ts3client/jni/events/rare/ServerPermissionError/a I
aload 0
getfield com/teamspeak/ts3client/c/c Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/u Lcom/teamspeak/ts3client/data/d/v;
getstatic com/teamspeak/ts3client/jni/g/bK Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/d/v/a(Lcom/teamspeak/ts3client/jni/g;)I
if_icmpne L1
ldc "channelinfo.desc.permerror"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
astore 1
aload 0
invokevirtual com/teamspeak/ts3client/c/l()Z
ifeq L2
aload 0
invokevirtual com/teamspeak/ts3client/c/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/f
dup
aload 0
aload 1
invokespecial com/teamspeak/ts3client/f/<init>(Lcom/teamspeak/ts3client/c;Ljava/lang/String;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
L2:
aload 0
getfield com/teamspeak/ts3client/c/c Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/b(Lcom/teamspeak/ts3client/data/w;)V
return
.limit locals 2
.limit stack 5
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
aload 0
aload 0
getfield com/teamspeak/ts3client/c/av Ljava/lang/String;
aload 1
new java/lang/StringBuilder
dup
ldc "<img src=\"file://"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\">"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
putfield com/teamspeak/ts3client/c/av Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/c/au Landroid/webkit/WebView;
ldc ""
aload 0
getfield com/teamspeak/ts3client/c/av Ljava/lang/String;
ldc "text/html"
ldc "utf-8"
ldc "about:blank"
invokevirtual android/webkit/WebView/loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/c/aw Landroid/widget/RelativeLayout;
invokevirtual android/widget/RelativeLayout/requestFocus()Z
pop
return
.limit locals 3
.limit stack 6
.end method

.method public final c(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/support/v4/app/Fragment/c(Landroid/os/Bundle;)V
aload 0
getfield com/teamspeak/ts3client/c/c Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/data/w;)V
aload 0
getfield com/teamspeak/ts3client/c/c Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/c/c Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/c/b Lcom/teamspeak/ts3client/data/a;
getfield com/teamspeak/ts3client/data/a/b J
ldc "ChannelInfoFragment"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestChannelDescription(JJLjava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/c/c Lcom/teamspeak/ts3client/Ts3Application;
aload 0
putfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
aload 0
iconst_0
invokespecial com/teamspeak/ts3client/c/a(Z)V
return
.limit locals 2
.limit stack 6
.end method
