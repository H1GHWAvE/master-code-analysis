.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/tsdns/f
.super java/lang/Object
.implements com/teamspeak/ts3client/tsdns/k

.field public static 'a' I

.field public static 'b' I

.field public static 'c' I

.field public static 'd' I

.field public static 'e' I

.field public static 'f' I

.field public static 'g' I

.field public static 'h' I

.field private 'i' Z

.field private 'j' Ljava/lang/String;

.field private 'k' Lcom/teamspeak/ts3client/tsdns/i;

.field private 'l' I

.field private 'm' Ljava/util/ArrayList;

.field private 'n' Ljava/util/BitSet;

.field private 'o' Ljava/util/Vector;

.field private 'p' Lcom/teamspeak/ts3client/tsdns/h;

.field private 'q' Z

.field private 'r' J

.field private 's' Ljava/util/logging/Logger;

.method static <clinit>()V
iconst_0
putstatic com/teamspeak/ts3client/tsdns/f/a I
iconst_1
putstatic com/teamspeak/ts3client/tsdns/f/b I
iconst_2
putstatic com/teamspeak/ts3client/tsdns/f/c I
iconst_3
putstatic com/teamspeak/ts3client/tsdns/f/d I
iconst_0
putstatic com/teamspeak/ts3client/tsdns/f/e I
iconst_1
putstatic com/teamspeak/ts3client/tsdns/f/f I
iconst_2
putstatic com/teamspeak/ts3client/tsdns/f/g I
iconst_3
putstatic com/teamspeak/ts3client/tsdns/f/h I
return
.limit locals 0
.limit stack 1
.end method

.method public <init>(Ljava/lang/String;ILcom/teamspeak/ts3client/tsdns/i;ZLjava/util/logging/Logger;)V
.annotation invisible Landroid/annotation/SuppressLint;
value [s = "DefaultLocale" 
.end annotation
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield com/teamspeak/ts3client/tsdns/f/i Z
aload 0
ldc ""
putfield com/teamspeak/ts3client/tsdns/f/j Ljava/lang/String;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield com/teamspeak/ts3client/tsdns/f/m Ljava/util/ArrayList;
aload 0
new java/util/BitSet
dup
invokespecial java/util/BitSet/<init>()V
putfield com/teamspeak/ts3client/tsdns/f/n Ljava/util/BitSet;
aload 0
new java/util/Vector
dup
invokespecial java/util/Vector/<init>()V
putfield com/teamspeak/ts3client/tsdns/f/o Ljava/util/Vector;
aload 0
ldc2_w 3500L
putfield com/teamspeak/ts3client/tsdns/f/r J
aload 0
aload 1
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
putfield com/teamspeak/ts3client/tsdns/f/j Ljava/lang/String;
aload 0
iload 2
putfield com/teamspeak/ts3client/tsdns/f/l I
aload 0
aload 3
putfield com/teamspeak/ts3client/tsdns/f/k Lcom/teamspeak/ts3client/tsdns/i;
aload 0
aload 5
putfield com/teamspeak/ts3client/tsdns/f/s Ljava/util/logging/Logger;
aload 0
iload 4
putfield com/teamspeak/ts3client/tsdns/f/q Z
iload 4
ifeq L0
aload 0
ldc2_w 7000L
putfield com/teamspeak/ts3client/tsdns/f/r J
L0:
return
.limit locals 6
.limit stack 3
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/tsdns/f;)J
aload 0
getfield com/teamspeak/ts3client/tsdns/f/r J
lreturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/tsdns/f;Lcom/teamspeak/ts3client/tsdns/h;)V
aload 0
aload 1
invokespecial com/teamspeak/ts3client/tsdns/f/b(Lcom/teamspeak/ts3client/tsdns/h;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/util/ArrayList;)V
aload 0
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 0
L0:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 1
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 1
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
goto L0
L1:
return
.limit locals 2
.limit stack 2
.end method

.method private b(Lcom/teamspeak/ts3client/tsdns/h;)V
aload 0
getfield com/teamspeak/ts3client/tsdns/f/s Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
new java/lang/StringBuilder
dup
ldc "PUBLISH:"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
getfield com/teamspeak/ts3client/tsdns/h/d I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " - "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
getfield com/teamspeak/ts3client/tsdns/h/b I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " - "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
getfield com/teamspeak/ts3client/tsdns/h/a Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " - "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
getfield com/teamspeak/ts3client/tsdns/h/c I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/tsdns/f/k Lcom/teamspeak/ts3client/tsdns/i;
aload 1
invokeinterface com/teamspeak/ts3client/tsdns/i/a(Lcom/teamspeak/ts3client/tsdns/h;)V 1
return
.limit locals 2
.limit stack 5
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/tsdns/f;)Z
aload 0
getfield com/teamspeak/ts3client/tsdns/f/i Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/tsdns/f;)Z
aload 0
iconst_1
putfield com/teamspeak/ts3client/tsdns/f/i Z
iconst_1
ireturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/tsdns/f;)Lcom/teamspeak/ts3client/tsdns/h;
aload 0
getfield com/teamspeak/ts3client/tsdns/f/p Lcom/teamspeak/ts3client/tsdns/h;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/tsdns/f;)Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/tsdns/f/j Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/tsdns/f;)I
aload 0
getfield com/teamspeak/ts3client/tsdns/f/l I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a()V
aload 0
getfield com/teamspeak/ts3client/tsdns/f/j Ljava/lang/String;
ldc ":"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
getfield com/teamspeak/ts3client/tsdns/f/j Ljava/lang/String;
invokestatic org/xbill/DNS/Address/isDottedQuad(Ljava/lang/String;)Z
ifeq L1
L0:
aload 0
new com/teamspeak/ts3client/tsdns/h
dup
aload 0
getfield com/teamspeak/ts3client/tsdns/f/j Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/tsdns/f/l I
iconst_m1
getstatic com/teamspeak/ts3client/tsdns/f/a I
iconst_m1
iconst_1
invokespecial com/teamspeak/ts3client/tsdns/h/<init>(Ljava/lang/String;IIIIZ)V
invokespecial com/teamspeak/ts3client/tsdns/f/b(Lcom/teamspeak/ts3client/tsdns/h;)V
return
L1:
aload 0
getfield com/teamspeak/ts3client/tsdns/f/m Ljava/util/ArrayList;
aload 0
getfield com/teamspeak/ts3client/tsdns/f/j Ljava/lang/String;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
new java/util/ArrayList
dup
aload 0
getfield com/teamspeak/ts3client/tsdns/f/j Ljava/lang/String;
ldc "\\."
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
invokespecial java/util/ArrayList/<init>(Ljava/util/Collection;)V
astore 3
aload 3
invokevirtual java/util/ArrayList/size()I
getstatic com/teamspeak/ts3client/tsdns/f/d I
isub
iconst_1
isub
istore 1
iload 1
ifge L2
iconst_0
istore 1
L3:
iload 1
aload 3
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
if_icmpge L4
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 4
iload 1
istore 2
L5:
iload 2
aload 3
invokevirtual java/util/ArrayList/size()I
if_icmpge L6
aload 4
aload 3
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/String
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
iload 2
aload 3
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
if_icmpge L7
aload 4
ldc "."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L7:
iload 2
iconst_1
iadd
istore 2
goto L5
L6:
aload 0
getfield com/teamspeak/ts3client/tsdns/f/m Ljava/util/ArrayList;
aload 4
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
iload 1
iconst_1
iadd
istore 1
goto L3
L4:
new java/lang/Thread
dup
new com/teamspeak/ts3client/tsdns/g
dup
aload 0
invokespecial com/teamspeak/ts3client/tsdns/g/<init>(Lcom/teamspeak/ts3client/tsdns/f;)V
invokespecial java/lang/Thread/<init>(Ljava/lang/Runnable;)V
astore 3
aload 0
getfield com/teamspeak/ts3client/tsdns/f/n Ljava/util/BitSet;
iconst_0
invokevirtual java/util/BitSet/set(I)V
aload 0
getfield com/teamspeak/ts3client/tsdns/f/n Ljava/util/BitSet;
getstatic com/teamspeak/ts3client/tsdns/f/d I
iconst_2
imul
iconst_3
iadd
invokevirtual java/util/BitSet/set(I)V
aload 0
getfield com/teamspeak/ts3client/tsdns/f/m Ljava/util/ArrayList;
invokestatic java/util/Collections/reverse(Ljava/util/List;)V
aload 0
getfield com/teamspeak/ts3client/tsdns/f/m Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 4
iconst_0
istore 1
L8:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L9
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
pop
aload 0
getfield com/teamspeak/ts3client/tsdns/f/n Ljava/util/BitSet;
iload 1
iconst_1
iadd
invokevirtual java/util/BitSet/set(I)V
aload 0
getfield com/teamspeak/ts3client/tsdns/f/n Ljava/util/BitSet;
getstatic com/teamspeak/ts3client/tsdns/f/d I
iconst_2
iadd
iload 1
iadd
invokevirtual java/util/BitSet/set(I)V
iload 1
iconst_1
iadd
istore 1
goto L8
L9:
new com/teamspeak/ts3client/tsdns/j
dup
aload 0
getfield com/teamspeak/ts3client/tsdns/f/j Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/tsdns/f/j Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/tsdns/f/l I
getstatic com/teamspeak/ts3client/tsdns/f/h I
iconst_0
aload 0
invokespecial com/teamspeak/ts3client/tsdns/j/<init>(Ljava/lang/String;Ljava/lang/String;IIILcom/teamspeak/ts3client/tsdns/k;)V
invokevirtual com/teamspeak/ts3client/tsdns/j/start()V
new com/teamspeak/ts3client/tsdns/j
dup
aload 0
getfield com/teamspeak/ts3client/tsdns/f/j Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/tsdns/f/j Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/tsdns/f/l I
getstatic com/teamspeak/ts3client/tsdns/f/e I
getstatic com/teamspeak/ts3client/tsdns/f/d I
iconst_2
imul
iconst_3
iadd
aload 0
invokespecial com/teamspeak/ts3client/tsdns/j/<init>(Ljava/lang/String;Ljava/lang/String;IIILcom/teamspeak/ts3client/tsdns/k;)V
invokevirtual com/teamspeak/ts3client/tsdns/j/start()V
aload 0
getfield com/teamspeak/ts3client/tsdns/f/m Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 4
iconst_0
istore 1
L10:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L11
new com/teamspeak/ts3client/tsdns/j
dup
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
aload 0
getfield com/teamspeak/ts3client/tsdns/f/j Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/tsdns/f/l I
getstatic com/teamspeak/ts3client/tsdns/f/f I
getstatic com/teamspeak/ts3client/tsdns/f/d I
iconst_2
iadd
iload 1
iadd
aload 0
invokespecial com/teamspeak/ts3client/tsdns/j/<init>(Ljava/lang/String;Ljava/lang/String;IIILcom/teamspeak/ts3client/tsdns/k;)V
invokevirtual com/teamspeak/ts3client/tsdns/j/start()V
iload 1
iconst_1
iadd
istore 1
goto L10
L11:
aload 0
getfield com/teamspeak/ts3client/tsdns/f/m Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 4
iconst_0
istore 1
L12:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L13
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 5
new com/teamspeak/ts3client/tsdns/j
dup
aload 0
getfield com/teamspeak/ts3client/tsdns/f/j Ljava/lang/String;
aload 5
aload 0
getfield com/teamspeak/ts3client/tsdns/f/l I
getstatic com/teamspeak/ts3client/tsdns/f/g I
iload 1
iconst_1
iadd
aload 0
invokespecial com/teamspeak/ts3client/tsdns/j/<init>(Ljava/lang/String;Ljava/lang/String;IIILcom/teamspeak/ts3client/tsdns/k;)V
invokevirtual com/teamspeak/ts3client/tsdns/j/start()V
iload 1
iconst_1
iadd
istore 1
goto L12
L13:
aload 3
invokevirtual java/lang/Thread/start()V
return
L2:
goto L3
.limit locals 6
.limit stack 9
.end method

.method public final a(Lcom/teamspeak/ts3client/tsdns/h;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L5 to L6 using L2
.catch all from L6 to L7 using L2
.catch all from L8 to L9 using L2
.catch all from L10 to L11 using L2
aload 0
monitorenter
L0:
aload 0
getfield com/teamspeak/ts3client/tsdns/f/i Z
istore 2
L1:
iload 2
ifeq L3
L12:
aload 0
monitorexit
return
L3:
aload 0
getfield com/teamspeak/ts3client/tsdns/f/n Ljava/util/BitSet;
aload 1
getfield com/teamspeak/ts3client/tsdns/h/b I
invokevirtual java/util/BitSet/clear(I)V
aload 1
getfield com/teamspeak/ts3client/tsdns/h/d I
getstatic com/teamspeak/ts3client/tsdns/f/a I
if_icmpne L4
aload 0
getfield com/teamspeak/ts3client/tsdns/f/p Lcom/teamspeak/ts3client/tsdns/h;
ifnonnull L8
aload 0
aload 1
putfield com/teamspeak/ts3client/tsdns/f/p Lcom/teamspeak/ts3client/tsdns/h;
L4:
aload 0
getfield com/teamspeak/ts3client/tsdns/f/n Ljava/util/BitSet;
aload 1
getfield com/teamspeak/ts3client/tsdns/h/b I
invokevirtual java/util/BitSet/nextSetBit(I)I
iconst_m1
if_icmpne L5
aload 1
getfield com/teamspeak/ts3client/tsdns/h/d I
getstatic com/teamspeak/ts3client/tsdns/f/a I
if_icmpeq L6
L5:
aload 0
getfield com/teamspeak/ts3client/tsdns/f/n Ljava/util/BitSet;
invokevirtual java/util/BitSet/isEmpty()Z
ifeq L12
L6:
aload 0
iconst_1
putfield com/teamspeak/ts3client/tsdns/f/i Z
aload 0
getfield com/teamspeak/ts3client/tsdns/f/p Lcom/teamspeak/ts3client/tsdns/h;
ifnonnull L10
aload 0
new com/teamspeak/ts3client/tsdns/h
dup
aload 0
getfield com/teamspeak/ts3client/tsdns/f/j Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/tsdns/f/l I
iconst_m1
getstatic com/teamspeak/ts3client/tsdns/f/c I
iconst_m1
iconst_1
invokespecial com/teamspeak/ts3client/tsdns/h/<init>(Ljava/lang/String;IIIIZ)V
invokespecial com/teamspeak/ts3client/tsdns/f/b(Lcom/teamspeak/ts3client/tsdns/h;)V
L7:
goto L12
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
L8:
aload 0
getfield com/teamspeak/ts3client/tsdns/f/p Lcom/teamspeak/ts3client/tsdns/h;
getfield com/teamspeak/ts3client/tsdns/h/b I
aload 1
getfield com/teamspeak/ts3client/tsdns/h/b I
if_icmpge L4
aload 0
aload 1
putfield com/teamspeak/ts3client/tsdns/f/p Lcom/teamspeak/ts3client/tsdns/h;
L9:
goto L4
L10:
aload 0
new com/teamspeak/ts3client/tsdns/h
dup
aload 0
getfield com/teamspeak/ts3client/tsdns/f/p Lcom/teamspeak/ts3client/tsdns/h;
invokespecial com/teamspeak/ts3client/tsdns/h/<init>(Lcom/teamspeak/ts3client/tsdns/h;)V
invokespecial com/teamspeak/ts3client/tsdns/f/b(Lcom/teamspeak/ts3client/tsdns/h;)V
L11:
goto L12
.limit locals 3
.limit stack 9
.end method

.method public final a(Ljava/lang/Thread;)V
aload 0
getfield com/teamspeak/ts3client/tsdns/f/o Ljava/util/Vector;
aload 1
invokevirtual java/util/Vector/add(Ljava/lang/Object;)Z
pop
return
.limit locals 2
.limit stack 2
.end method

.method public final b()Z
aload 0
getfield com/teamspeak/ts3client/tsdns/f/q Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final c()Ljava/util/logging/Logger;
aload 0
getfield com/teamspeak/ts3client/tsdns/f/s Ljava/util/logging/Logger;
areturn
.limit locals 1
.limit stack 1
.end method
