.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/data/aa
.super java/lang/Object
.annotation invisible Landroid/annotation/SuppressLint;
value [s = "UseSparseArrays" 
.end annotation

.field private static final 'a' Ljava/util/HashMap;

.method static <clinit>()V
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 0
aload 0
putstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
aload 0
iconst_0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_ok"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
iconst_1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_undefined"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
iconst_2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_not_implemented"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
iconst_3
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_ok_no_update"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
iconst_4
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_dont_notify"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
iconst_5
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_lib_time_limit_reached"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 256
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_command_not_found"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 257
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_unable_to_bind_network_port"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 258
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_no_network_port_available"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 512
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_client_invalid_id"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 513
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_client_nickname_inuse"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 515
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_client_protocol_limit_reached"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 516
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_client_invalid_type"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 517
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_client_already_subscribed"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 518
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_client_not_logged_in"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 519
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_client_could_not_validate_identity"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 522
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_client_version_outdated"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 524
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_client_is_flooding"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 768
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_channel_invalid_id"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 769
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_channel_protocol_limit_reached"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 770
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_channel_already_in"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 771
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_channel_name_inuse"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 772
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_channel_not_empty"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 773
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_channel_can_not_delete_default"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 774
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_channel_default_require_permanent"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 775
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_channel_invalid_flags"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 776
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_channel_parent_not_permanent"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 777
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_channel_maxclients_reached"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 778
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_channel_maxfamily_reached"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 779
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_channel_invalid_order"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 780
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_channel_no_filetransfer_supported"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 781
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_channel_invalid_password"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 1024
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_server_invalid_id"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 1025
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_server_running"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 1026
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_server_is_shutting_down"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 1027
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_server_maxclients_reached"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 1028
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_server_invalid_password"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 1031
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_server_is_virtual"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 1033
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_server_is_not_running"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 1034
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_server_is_booting"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 1035
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_server_status_invalid"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 1536
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_parameter_quote"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 1537
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_parameter_invalid_count"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 1538
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_parameter_invalid"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 1539
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_parameter_not_found"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 1540
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_parameter_convert"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 1541
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_parameter_invalid_size"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 1542
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_parameter_missing"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 1792
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_vs_critical"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 1793
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_connection_lost"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 1794
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_not_connected"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 1795
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_no_cached_connection_info"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 1796
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_currently_not_possible"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 1797
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_failed_connection_initialisation"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 1798
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_could_not_resolve_hostname"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 1799
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_invalid_server_connection_handler_id"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 1800
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_could_not_initialise_input_manager"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 1801
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_clientlibrary_not_initialised"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 1802
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_serverlibrary_not_initialised"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 1803
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_whisper_too_many_targets"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 1804
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_whisper_no_targets"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2304
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_sound_preprocessor_disabled"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2305
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_sound_internal_preprocessor"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2306
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_sound_internal_encoder"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2307
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_sound_internal_playback"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2308
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_sound_no_capture_device_available"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2309
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_sound_no_playback_device_available"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2310
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_sound_could_not_open_capture_device"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2311
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_sound_could_not_open_playback_device"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2312
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_sound_handler_has_device"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2313
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_sound_invalid_capture_device"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2314
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_sound_invalid_playback_device"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2315
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_sound_invalid_wave"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2316
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_sound_unsupported_wave"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2317
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_sound_open_wave"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2318
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_sound_internal_capture"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2319
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_sound_device_in_use"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2320
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_sound_device_already_registerred"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2321
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_sound_unknown_device"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2322
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_sound_unsupported_frequency"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2323
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_sound_invalid_channel_count"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2324
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_sound_read_wave"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2325
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_sound_need_more_data"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2326
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_sound_device_busy"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2327
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_sound_no_data"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2328
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_sound_channel_mask_mismatch"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2816
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_accounting_virtualserver_limit_reached"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2817
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_accounting_slot_limit_reached"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2818
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_accounting_license_file_not_found"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2819
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_accounting_license_date_not_ok"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2820
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_accounting_unable_to_connect_to_server"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2821
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_accounting_unknown_error"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2822
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_accounting_server_error"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2823
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_accounting_instance_limit_reached"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2824
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_accounting_instance_check_error"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2825
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_accounting_license_file_invalid"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2826
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_accounting_running_elsewhere"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2827
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_accounting_instance_duplicated"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2828
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_accounting_already_started"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2829
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_accounting_not_started"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 2830
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_accounting_to_many_starts"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 4352
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_provisioning_invalid_password"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 4353
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_provisioning_invalid_request"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 4354
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_provisioning_no_slots_available"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 4355
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_provisioning_pool_missing"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 4356
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_provisioning_pool_unknown"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 4357
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_provisioning_unknown_ip_location"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 4358
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_provisioning_internal_tries_exceeded"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 4359
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_provisioning_too_many_slots_requested"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 4360
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_provisioning_too_many_reserved"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 4361
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_provisioning_could_not_connect"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 4368
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_provisioning_auth_server_not_connected"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 4369
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_provisioning_auth_data_too_large"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 4370
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_provisioning_already_initialized"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 4371
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_provisioning_not_initialized"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 4372
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_provisioning_connecting"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 4373
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_provisioning_already_connected"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 4374
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_provisioning_not_connected"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
sipush 4375
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "ERROR_provisioning_io_error"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
return
.limit locals 1
.limit stack 3
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(I)Ljava/lang/String;
getstatic com/teamspeak/ts3client/data/aa/a Ljava/util/HashMap;
iload 0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
areturn
.limit locals 1
.limit stack 2
.end method
