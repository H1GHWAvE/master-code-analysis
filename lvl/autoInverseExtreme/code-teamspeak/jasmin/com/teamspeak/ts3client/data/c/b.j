.bytecode 50.0
.class final synchronized com/teamspeak/ts3client/data/c/b
.super android/os/AsyncTask
.implements com/teamspeak/ts3client/data/w

.field final synthetic 'a' Lcom/teamspeak/ts3client/data/c/a;

.field private 'b' I

.field private 'c' Ljava/lang/String;

.field private 'd' Ljava/lang/String;

.field private 'e' Ljava/lang/String;

.field private 'f' Lcom/teamspeak/ts3client/data/c;

.field private 'g' Ljava/lang/String;

.field private 'h' Ljava/io/File;

.method <init>(Lcom/teamspeak/ts3client/data/c/a;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/data/c/b/a Lcom/teamspeak/ts3client/data/c/a;
aload 0
invokespecial android/os/AsyncTask/<init>()V
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
invokestatic android/os/Environment/getExternalStorageDirectory()Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/TS3/cache/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
putfield com/teamspeak/ts3client/data/c/b/c Ljava/lang/String;
aload 0
ldc ""
putfield com/teamspeak/ts3client/data/c/b/e Ljava/lang/String;
return
.limit locals 2
.limit stack 3
.end method

.method private a()Landroid/graphics/Bitmap;
.catch java/security/NoSuchAlgorithmException from L0 to L1 using L2
.catch java/io/FileNotFoundException from L0 to L1 using L3
.catch java/io/IOException from L0 to L1 using L4
.catch java/security/NoSuchAlgorithmException from L1 to L5 using L2
.catch java/io/FileNotFoundException from L1 to L5 using L3
.catch java/io/IOException from L1 to L5 using L4
.catch java/security/NoSuchAlgorithmException from L6 to L7 using L2
.catch java/io/FileNotFoundException from L6 to L7 using L3
.catch java/io/IOException from L6 to L7 using L4
.catch java/security/NoSuchAlgorithmException from L8 to L9 using L2
.catch java/io/FileNotFoundException from L8 to L9 using L3
.catch java/io/IOException from L8 to L9 using L4
.catch java/security/NoSuchAlgorithmException from L10 to L11 using L12
.catch java/io/FileNotFoundException from L10 to L11 using L13
.catch java/io/IOException from L10 to L11 using L14
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/teamspeak/ts3client/data/c/b/c Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/q Ljava/lang/String;
ldc "/"
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/avatar/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
putfield com/teamspeak/ts3client/data/c/b/g Ljava/lang/String;
new java/io/File
dup
aload 0
getfield com/teamspeak/ts3client/data/c/b/g Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 2
aload 2
invokevirtual java/io/File/exists()Z
ifne L15
aload 2
invokevirtual java/io/File/mkdirs()Z
pop
L15:
aload 0
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/teamspeak/ts3client/data/c/b/g Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "avatar_"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/data/c/b/e Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
putfield com/teamspeak/ts3client/data/c/b/h Ljava/io/File;
aload 0
getfield com/teamspeak/ts3client/data/c/b/h Ljava/io/File;
invokevirtual java/io/File/exists()Z
ifeq L16
L0:
ldc "MD5"
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
astore 2
new java/io/FileInputStream
dup
aload 0
getfield com/teamspeak/ts3client/data/c/b/h Ljava/io/File;
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
astore 3
sipush 4000
newarray byte
astore 4
L1:
aload 3
aload 4
invokevirtual java/io/InputStream/read([B)I
istore 1
L5:
iload 1
ifle L8
L6:
aload 2
aload 4
iconst_0
iload 1
invokevirtual java/security/MessageDigest/update([BII)V
L7:
goto L1
L2:
astore 3
ldc ""
astore 2
L17:
aload 3
invokevirtual java/security/NoSuchAlgorithmException/printStackTrace()V
aload 2
astore 3
L18:
aload 3
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L19
aload 0
getfield com/teamspeak/ts3client/data/c/b/d Ljava/lang/String;
aload 3
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L20
new android/graphics/BitmapFactory$Options
dup
invokespecial android/graphics/BitmapFactory$Options/<init>()V
astore 2
aload 2
sipush 160
putfield android/graphics/BitmapFactory$Options/inTargetDensity I
aload 0
getfield com/teamspeak/ts3client/data/c/b/h Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aload 2
invokestatic android/graphics/BitmapFactory/decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
areturn
L8:
aload 2
invokevirtual java/security/MessageDigest/digest()[B
astore 4
L9:
ldc ""
astore 2
iconst_0
istore 1
L21:
aload 2
astore 3
L10:
iload 1
aload 4
arraylength
if_icmpge L18
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
iload 1
baload
sipush 255
iand
sipush 256
iadd
bipush 16
invokestatic java/lang/Integer/toString(II)Ljava/lang/String;
iconst_1
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 3
L11:
iload 1
iconst_1
iadd
istore 1
aload 3
astore 2
goto L21
L3:
astore 3
ldc ""
astore 2
L22:
aload 3
invokevirtual java/io/FileNotFoundException/printStackTrace()V
aload 2
astore 3
goto L18
L4:
astore 3
ldc ""
astore 2
L23:
aload 3
invokevirtual java/io/IOException/printStackTrace()V
aload 2
astore 3
goto L18
L20:
aload 0
getfield com/teamspeak/ts3client/data/c/b/h Ljava/io/File;
invokevirtual java/io/File/delete()Z
pop
aload 0
invokespecial com/teamspeak/ts3client/data/c/b/b()V
aconst_null
areturn
L16:
aload 0
invokespecial com/teamspeak/ts3client/data/c/b/b()V
aconst_null
areturn
L19:
new android/graphics/BitmapFactory$Options
dup
invokespecial android/graphics/BitmapFactory$Options/<init>()V
astore 2
aload 2
sipush 160
putfield android/graphics/BitmapFactory$Options/inTargetDensity I
aload 0
getfield com/teamspeak/ts3client/data/c/b/h Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aload 2
invokestatic android/graphics/BitmapFactory/decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
areturn
L14:
astore 3
goto L23
L13:
astore 3
goto L22
L12:
astore 3
goto L17
.limit locals 5
.limit stack 5
.end method

.method private transient a([Lcom/teamspeak/ts3client/data/c;)Ljava/lang/Void;
aload 0
aload 1
iconst_0
aaload
putfield com/teamspeak/ts3client/data/c/b/f Lcom/teamspeak/ts3client/data/c;
aload 0
aload 0
getfield com/teamspeak/ts3client/data/c/b/f Lcom/teamspeak/ts3client/data/c;
invokevirtual com/teamspeak/ts3client/data/c/b()Ljava/lang/String;
putfield com/teamspeak/ts3client/data/c/b/d Ljava/lang/String;
aload 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/data/c/b/f Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/b Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_identityStringToFilename(Ljava/lang/String;)Ljava/lang/String;
putfield com/teamspeak/ts3client/data/c/b/e Ljava/lang/String;
aload 0
invokespecial com/teamspeak/ts3client/data/c/b/a()Landroid/graphics/Bitmap;
astore 1
aload 1
ifnull L0
aload 0
getfield com/teamspeak/ts3client/data/c/b/f Lcom/teamspeak/ts3client/data/c;
aload 1
invokevirtual com/teamspeak/ts3client/data/c/a(Landroid/graphics/Bitmap;)V
L0:
aconst_null
areturn
.limit locals 2
.limit stack 3
.end method

.method private b()V
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
new java/lang/StringBuilder
dup
ldc "Loading Avatar avatar_"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/data/c/b/e Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " from Server"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/data/w;)V
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
lconst_0
ldc ""
new java/lang/StringBuilder
dup
ldc "/avatar_"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/data/c/b/e Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "Request info: /avatar_"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/data/c/b/e Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestFileInfo(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
pop
return
.limit locals 1
.limit stack 10
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
aload 1
instanceof com/teamspeak/ts3client/jni/events/rare/FileTransferStatus
ifeq L0
aload 1
checkcast com/teamspeak/ts3client/jni/events/rare/FileTransferStatus
getfield com/teamspeak/ts3client/jni/events/rare/FileTransferStatus/b J
ldc2_w 2065L
lcmp
ifne L0
aload 1
checkcast com/teamspeak/ts3client/jni/events/rare/FileTransferStatus
getfield com/teamspeak/ts3client/jni/events/rare/FileTransferStatus/a I
aload 0
getfield com/teamspeak/ts3client/data/c/b/b I
if_icmpne L0
new android/graphics/BitmapFactory$Options
dup
invokespecial android/graphics/BitmapFactory$Options/<init>()V
astore 3
aload 3
sipush 160
putfield android/graphics/BitmapFactory$Options/inTargetDensity I
aload 0
getfield com/teamspeak/ts3client/data/c/b/h Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aload 3
invokestatic android/graphics/BitmapFactory/decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
astore 3
aload 0
getfield com/teamspeak/ts3client/data/c/b/f Lcom/teamspeak/ts3client/data/c;
aload 3
invokevirtual com/teamspeak/ts3client/data/c/a(Landroid/graphics/Bitmap;)V
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/b(Lcom/teamspeak/ts3client/data/w;)V
L0:
aload 1
instanceof com/teamspeak/ts3client/jni/events/rare/FileInfo
ifeq L1
aload 1
checkcast com/teamspeak/ts3client/jni/events/rare/FileInfo
getfield com/teamspeak/ts3client/jni/events/rare/FileInfo/a Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "/avatar_"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/data/c/b/e Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L1
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "avatarlimit"
ldc "500"
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
astore 3
aload 3
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L2
ldc "500"
astore 3
L3:
aload 1
checkcast com/teamspeak/ts3client/jni/events/rare/FileInfo
getfield com/teamspeak/ts3client/jni/events/rare/FileInfo/b J
aload 3
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
sipush 1000
imul
i2l
lcmp
iflt L4
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/h Landroid/net/ConnectivityManager;
iconst_1
invokevirtual android/net/ConnectivityManager/getNetworkInfo(I)Landroid/net/NetworkInfo;
invokevirtual android/net/NetworkInfo/isConnected()Z
ifeq L5
L4:
iconst_1
istore 2
L6:
iload 2
ifeq L7
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/data/w;)V
aload 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
lconst_0
ldc ""
new java/lang/StringBuilder
dup
ldc "/avatar_"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/data/c/b/e Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
iconst_1
iconst_0
aload 0
getfield com/teamspeak/ts3client/data/c/b/g Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "avatar_"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/data/c/b/f Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/c I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestFile(JJLjava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)I
putfield com/teamspeak/ts3client/data/c/b/b I
L1:
aload 1
instanceof com/teamspeak/ts3client/jni/events/ServerError
ifeq L8
aload 1
checkcast com/teamspeak/ts3client/jni/events/ServerError
astore 1
aload 1
getfield com/teamspeak/ts3client/jni/events/ServerError/b I
sipush 768
if_icmpne L8
aload 1
getfield com/teamspeak/ts3client/jni/events/ServerError/c Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "Request info: /avatar_"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/data/c/b/e Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L8
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/data/w;)V
aload 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
lconst_0
ldc ""
new java/lang/StringBuilder
dup
ldc "/avatar_"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/data/c/b/e Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
iconst_1
iconst_0
aload 0
getfield com/teamspeak/ts3client/data/c/b/g Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "avatar_"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/data/c/b/f Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/c I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestFile(JJLjava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)I
putfield com/teamspeak/ts3client/data/c/b/b I
L8:
return
L7:
new android/graphics/BitmapFactory$Options
dup
invokespecial android/graphics/BitmapFactory$Options/<init>()V
sipush 160
putfield android/graphics/BitmapFactory$Options/inTargetDensity I
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getResources()Landroid/content/res/Resources;
ldc_w 2130837650
invokestatic android/graphics/BitmapFactory/decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
astore 3
aload 0
getfield com/teamspeak/ts3client/data/c/b/f Lcom/teamspeak/ts3client/data/c;
aload 3
invokevirtual com/teamspeak/ts3client/data/c/a(Landroid/graphics/Bitmap;)V
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/b(Lcom/teamspeak/ts3client/data/w;)V
goto L1
L5:
iconst_0
istore 2
goto L6
L2:
goto L3
.limit locals 4
.limit stack 14
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
aload 0
aload 1
checkcast [Lcom/teamspeak/ts3client/data/c;
iconst_0
aaload
putfield com/teamspeak/ts3client/data/c/b/f Lcom/teamspeak/ts3client/data/c;
aload 0
aload 0
getfield com/teamspeak/ts3client/data/c/b/f Lcom/teamspeak/ts3client/data/c;
invokevirtual com/teamspeak/ts3client/data/c/b()Ljava/lang/String;
putfield com/teamspeak/ts3client/data/c/b/d Ljava/lang/String;
aload 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/data/c/b/f Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/b Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_identityStringToFilename(Ljava/lang/String;)Ljava/lang/String;
putfield com/teamspeak/ts3client/data/c/b/e Ljava/lang/String;
aload 0
invokespecial com/teamspeak/ts3client/data/c/b/a()Landroid/graphics/Bitmap;
astore 1
aload 1
ifnull L0
aload 0
getfield com/teamspeak/ts3client/data/c/b/f Lcom/teamspeak/ts3client/data/c;
aload 1
invokevirtual com/teamspeak/ts3client/data/c/a(Landroid/graphics/Bitmap;)V
L0:
aconst_null
areturn
.limit locals 2
.limit stack 3
.end method
