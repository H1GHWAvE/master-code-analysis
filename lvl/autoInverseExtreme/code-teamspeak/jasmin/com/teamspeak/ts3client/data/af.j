.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/data/af
.super java/lang/Object

.field private static 'f' Lcom/teamspeak/ts3client/data/af;

.field public 'a' I

.field public 'b' Ljava/lang/String;

.field 'c' Z

.field public 'd' Lcom/teamspeak/ts3client/data/z;

.field public 'e' Z

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield com/teamspeak/ts3client/data/af/a I
return
.limit locals 1
.limit stack 2
.end method

.method private static synthetic a(Lcom/teamspeak/ts3client/data/af;I)I
aload 0
iload 1
putfield com/teamspeak/ts3client/data/af/a I
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public static a()Lcom/teamspeak/ts3client/data/af;
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
ldc com/teamspeak/ts3client/data/af
monitorenter
L0:
getstatic com/teamspeak/ts3client/data/af/f Lcom/teamspeak/ts3client/data/af;
ifnonnull L1
new com/teamspeak/ts3client/data/af
dup
invokespecial com/teamspeak/ts3client/data/af/<init>()V
putstatic com/teamspeak/ts3client/data/af/f Lcom/teamspeak/ts3client/data/af;
L1:
getstatic com/teamspeak/ts3client/data/af/f Lcom/teamspeak/ts3client/data/af;
astore 0
L3:
ldc com/teamspeak/ts3client/data/af
monitorexit
aload 0
areturn
L2:
astore 0
ldc com/teamspeak/ts3client/data/af
monitorexit
aload 0
athrow
.limit locals 1
.limit stack 2
.end method

.method private a(Lcom/teamspeak/ts3client/data/z;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/data/af/d Lcom/teamspeak/ts3client/data/z;
return
.limit locals 2
.limit stack 2
.end method

.method private static synthetic a(Lcom/teamspeak/ts3client/data/af;)Z
aload 0
getfield com/teamspeak/ts3client/data/af/c Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic a(Lcom/teamspeak/ts3client/data/af;Z)Z
aload 0
iload 1
putfield com/teamspeak/ts3client/data/af/c Z
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static synthetic b(Lcom/teamspeak/ts3client/data/af;)Lcom/teamspeak/ts3client/data/z;
aload 0
getfield com/teamspeak/ts3client/data/af/d Lcom/teamspeak/ts3client/data/z;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b()V
aload 0
getfield com/teamspeak/ts3client/data/af/e Z
ifeq L0
return
L0:
invokestatic java/util/Calendar/getInstance()Ljava/util/Calendar;
astore 1
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "LicenseAgreement"
iconst_0
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
ifeq L1
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "la_lastcheck"
lconst_0
invokeinterface android/content/SharedPreferences/getLong(Ljava/lang/String;J)J 3
aload 1
invokevirtual java/util/Calendar/getTimeInMillis()J
ldc2_w 86400000L
lsub
lcmp
ifge L2
L1:
aload 0
invokestatic com/teamspeak/ts3client/jni/Ts3Jni/b()Lcom/teamspeak/ts3client/jni/Ts3Jni;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_android_getUpdaterURL()Ljava/lang/String;
putfield com/teamspeak/ts3client/data/af/b Ljava/lang/String;
aload 0
iconst_1
putfield com/teamspeak/ts3client/data/af/e Z
new com/teamspeak/ts3client/data/ah
dup
aload 0
iconst_0
invokespecial com/teamspeak/ts3client/data/ah/<init>(Lcom/teamspeak/ts3client/data/af;B)V
iconst_1
anewarray java/lang/String
dup
iconst_0
aload 0
getfield com/teamspeak/ts3client/data/af/b Ljava/lang/String;
aastore
invokevirtual com/teamspeak/ts3client/data/ah/execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
pop
return
L2:
ldc "UpdateServerData"
ldc "skipped downloading new update info"
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield com/teamspeak/ts3client/data/af/d Lcom/teamspeak/ts3client/data/z;
invokeinterface com/teamspeak/ts3client/data/z/o()V 0
return
.limit locals 2
.limit stack 6
.end method

.method private c()I
aload 0
getfield com/teamspeak/ts3client/data/af/a I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic c(Lcom/teamspeak/ts3client/data/af;)Z
aload 0
iconst_0
putfield com/teamspeak/ts3client/data/af/e Z
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private d()Z
aload 0
getfield com/teamspeak/ts3client/data/af/c Z
ireturn
.limit locals 1
.limit stack 1
.end method
