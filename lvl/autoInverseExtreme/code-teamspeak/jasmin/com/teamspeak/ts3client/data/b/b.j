.bytecode 50.0
.class final synchronized com/teamspeak/ts3client/data/b/b
.super android/database/sqlite/SQLiteOpenHelper

.field final synthetic 'a' Lcom/teamspeak/ts3client/data/b/a;

.method public <init>(Lcom/teamspeak/ts3client/data/b/a;Landroid/content/Context;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/data/b/b/a Lcom/teamspeak/ts3client/data/b/a;
aload 0
aload 2
ldc "Teamspeak-Bookmark"
aconst_null
iconst_5
invokespecial android/database/sqlite/SQLiteOpenHelper/<init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V
return
.limit locals 3
.limit stack 5
.end method

.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
.catch java/lang/Exception from L0 to L1 using L2
aload 1
ldc "create table server (server_id integer primary key autoincrement, label text not null, address text not null, serverpassword blob null, nickname text not null, defaultchannel text not null, defaultchannelpassword text not null, ident int not null, subscribeall text not null,subscriptionlist int not null);"
invokevirtual android/database/sqlite/SQLiteDatabase/execSQL(Ljava/lang/String;)V
new android/content/ContentValues
dup
invokespecial android/content/ContentValues/<init>()V
astore 2
aload 2
ldc "label"
ldc "TeamSpeak Public"
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 2
ldc "address"
ldc "voice.teamspeak.com"
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 2
ldc "serverpassword"
ldc ""
invokestatic com/teamspeak/ts3client/data/b/a/a(Ljava/lang/String;)[B
invokevirtual android/content/ContentValues/put(Ljava/lang/String;[B)V
aload 2
ldc "nickname"
ldc "Android_Client"
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 2
ldc "defaultchannel"
ldc "Default Channel (Mobile)"
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 2
ldc "defaultchannelpassword"
ldc ""
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
aload 2
ldc "ident"
iconst_1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Integer;)V
aload 2
ldc "subscribeall"
iconst_0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/Integer;)V
aload 2
ldc "subscriptionlist"
ldc ""
invokevirtual android/content/ContentValues/put(Ljava/lang/String;Ljava/lang/String;)V
L0:
aload 1
ldc "server"
aconst_null
aload 2
invokevirtual android/database/sqlite/SQLiteDatabase/insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
pop2
L1:
return
L2:
astore 1
return
.limit locals 3
.limit stack 4
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
iload 2
iload 3
if_icmpge L0
L1:
iload 2
iconst_1
isub
iload 3
if_icmpge L0
iload 2
tableswitch 5
L2
default : L3
L3:
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
aload 1
ldc "DROP TABLE IF EXISTS server"
invokevirtual android/database/sqlite/SQLiteDatabase/execSQL(Ljava/lang/String;)V
aload 0
aload 1
invokevirtual com/teamspeak/ts3client/data/b/b/onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
goto L3
L0:
return
.limit locals 4
.limit stack 2
.end method
