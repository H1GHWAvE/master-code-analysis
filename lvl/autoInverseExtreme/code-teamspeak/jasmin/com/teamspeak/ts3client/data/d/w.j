.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/data/d/w
.super java/lang/Object

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(J)Ljava/lang/String;
lload 0
ldc2_w 60L
lrem
lstore 2
lload 0
ldc2_w 60L
ldiv
lstore 4
lload 4
ldc2_w 60L
lrem
lstore 0
lload 4
ldc2_w 60L
ldiv
lstore 6
lload 6
ldc2_w 24L
lrem
lstore 4
lload 6
ldc2_w 24L
ldiv
lstore 8
lload 8
ldc2_w 365L
lrem
lstore 6
lload 8
ldc2_w 365L
ldiv
lstore 8
ldc ""
astore 11
lload 8
lconst_0
lcmp
ifle L0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 8
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc "a "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 11
L0:
aload 11
astore 10
lload 6
lconst_0
lcmp
ifle L1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 11
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 6
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc "d "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 10
L1:
aload 10
astore 11
lload 4
lconst_0
lcmp
ifle L2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 10
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
astore 11
lload 4
ldc2_w 10L
lcmp
ifge L3
new java/lang/StringBuilder
dup
ldc "0"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
lload 4
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 10
L4:
aload 11
aload 10
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 11
L2:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 11
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
astore 11
lload 0
ldc2_w 10L
lcmp
ifge L5
new java/lang/StringBuilder
dup
ldc "0"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
lload 0
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 10
L6:
aload 11
aload 10
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
astore 11
lload 2
ldc2_w 10L
lcmp
ifge L7
new java/lang/StringBuilder
dup
ldc "0"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
lload 2
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 10
L8:
aload 11
aload 10
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L3:
lload 4
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
astore 10
goto L4
L5:
lload 0
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
astore 10
goto L6
L7:
lload 2
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
astore 10
goto L8
.limit locals 12
.limit stack 4
.end method

.method public static a(JZ)Ljava/lang/String;
new java/text/DecimalFormat
dup
ldc "#.##"
invokespecial java/text/DecimalFormat/<init>(Ljava/lang/String;)V
astore 8
iconst_0
istore 5
iload 2
ifeq L0
ldc2_w 1024L
lstore 6
L1:
lload 0
lload 6
lload 6
lmul
lcmp
ifle L2
iload 5
iconst_1
iadd
istore 5
lload 0
lload 6
ldiv
lstore 0
goto L1
L0:
ldc2_w 1000L
lstore 6
goto L1
L2:
lload 0
lload 6
lcmp
iflt L3
iload 5
iconst_1
iadd
istore 5
lload 0
l2d
lload 6
l2d
ddiv
dstore 3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 8
dload 3
invokevirtual java/text/DecimalFormat/format(D)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 8
iload 5
iconst_1
if_icmpne L4
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 8
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
astore 9
iload 2
ifeq L5
ldc " K"
astore 8
L6:
aload 9
aload 8
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 8
L7:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 8
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
astore 9
iload 2
ifeq L8
ldc "iB"
astore 8
L9:
aload 9
aload 8
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L3:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
lload 0
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc " Bytes"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L5:
ldc " k"
astore 8
goto L6
L4:
iload 5
iconst_2
if_icmpne L10
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 8
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " M"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 8
goto L7
L10:
iload 5
iconst_3
if_icmpne L11
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 8
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " G"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 8
goto L7
L11:
iload 5
iconst_4
if_icmpne L12
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 8
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " T"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 8
goto L7
L12:
iload 5
iconst_5
if_icmpne L13
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 8
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " P"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 8
goto L7
L13:
iload 5
bipush 6
if_icmpne L14
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 8
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " E"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 8
goto L7
L14:
ldc "overflow"
areturn
L8:
ldc ""
astore 8
goto L9
.limit locals 10
.limit stack 6
.end method

.method private static b(J)Ljava/lang/String;
ldc ""
astore 11
lload 0
ldc2_w 60L
lrem
lstore 2
lload 0
ldc2_w 60L
ldiv
lstore 4
lload 4
ldc2_w 60L
lrem
lstore 0
lload 4
ldc2_w 60L
ldiv
lstore 6
lload 6
ldc2_w 24L
lrem
lstore 4
lload 6
ldc2_w 24L
ldiv
lstore 8
lload 8
ldc2_w 365L
lrem
lstore 6
lload 8
ldc2_w 365L
ldiv
lstore 8
lload 8
lconst_0
lcmp
ifle L0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc ""
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 8
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc "a "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 11
L0:
aload 11
astore 10
lload 6
lconst_0
lcmp
ifle L1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 11
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 6
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc "d "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 10
L1:
aload 10
astore 11
lload 4
lconst_0
lcmp
ifle L2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 10
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 4
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc "h "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 11
L2:
aload 11
astore 10
lload 0
lconst_0
lcmp
ifle L3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 11
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 0
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc "m "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 10
L3:
aload 10
astore 11
lload 2
lconst_0
lcmp
ifle L4
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 10
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 2
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc "s"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 11
L4:
aload 11
areturn
.limit locals 12
.limit stack 4
.end method
