.bytecode 50.0
.class public synchronized com/teamspeak/ts3client/customs/CustomCodecSettings
.super android/widget/LinearLayout

.field private 'a' Lcom/teamspeak/ts3client/data/d/ab;

.field private 'b' I

.field private 'c' Landroid/widget/SeekBar;

.field private 'd' Landroid/widget/TextView;

.field private 'e' Lcom/teamspeak/ts3client/Ts3Application;

.field private 'f' Landroid/widget/TextView;

.field private 'g' Landroid/widget/TextView;

.field private 'h' Landroid/widget/Spinner;

.field private 'i' Landroid/widget/Spinner;

.field private 'j' Z

.field private 'k' Ljava/util/BitSet;

.field private 'l' I

.field private 'm' Lcom/teamspeak/ts3client/customs/a;

.field private 'n' Lcom/teamspeak/ts3client/customs/a;

.field private 'o' Z

.field private 'p' Z

.field private 'q' Z

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
aload 0
new com/teamspeak/ts3client/data/d/ab
dup
iconst_0
iconst_0
invokespecial com/teamspeak/ts3client/data/d/ab/<init>(II)V
putfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
aload 0
iconst_0
putfield com/teamspeak/ts3client/customs/CustomCodecSettings/b I
aload 0
iconst_0
putfield com/teamspeak/ts3client/customs/CustomCodecSettings/j Z
aload 0
new java/util/BitSet
dup
invokespecial java/util/BitSet/<init>()V
putfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
aload 0
iconst_0
putfield com/teamspeak/ts3client/customs/CustomCodecSettings/l I
aload 0
aload 1
invokevirtual android/content/Context/getApplicationContext()Landroid/content/Context;
checkcast com/teamspeak/ts3client/Ts3Application
putfield com/teamspeak/ts3client/customs/CustomCodecSettings/e Lcom/teamspeak/ts3client/Ts3Application;
aload 1
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
ldc_w 2130903072
aconst_null
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
astore 1
aload 0
aload 1
invokespecial com/teamspeak/ts3client/customs/CustomCodecSettings/a(Landroid/view/View;)V
aload 0
aload 1
invokevirtual com/teamspeak/ts3client/customs/CustomCodecSettings/addView(Landroid/view/View;)V
aload 1
invokevirtual android/view/View/isInEditMode()Z
ifne L0
aload 0
invokespecial com/teamspeak/ts3client/customs/CustomCodecSettings/b()V
L0:
return
.limit locals 2
.limit stack 5
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
new com/teamspeak/ts3client/data/d/ab
dup
iconst_0
iconst_0
invokespecial com/teamspeak/ts3client/data/d/ab/<init>(II)V
putfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
aload 0
iconst_0
putfield com/teamspeak/ts3client/customs/CustomCodecSettings/b I
aload 0
iconst_0
putfield com/teamspeak/ts3client/customs/CustomCodecSettings/j Z
aload 0
new java/util/BitSet
dup
invokespecial java/util/BitSet/<init>()V
putfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
aload 0
iconst_0
putfield com/teamspeak/ts3client/customs/CustomCodecSettings/l I
aload 1
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
ldc_w 2130903072
aconst_null
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
astore 2
aload 2
invokevirtual android/view/View/isInEditMode()Z
ifne L0
aload 0
aload 1
invokevirtual android/content/Context/getApplicationContext()Landroid/content/Context;
checkcast com/teamspeak/ts3client/Ts3Application
putfield com/teamspeak/ts3client/customs/CustomCodecSettings/e Lcom/teamspeak/ts3client/Ts3Application;
aload 0
aload 2
invokespecial com/teamspeak/ts3client/customs/CustomCodecSettings/a(Landroid/view/View;)V
L0:
aload 0
aload 2
invokevirtual com/teamspeak/ts3client/customs/CustomCodecSettings/addView(Landroid/view/View;)V
aload 2
invokevirtual android/view/View/isInEditMode()Z
ifne L1
aload 0
invokespecial com/teamspeak/ts3client/customs/CustomCodecSettings/b()V
L1:
return
.limit locals 3
.limit stack 5
.end method

.method private a()V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
ifeq L0
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/l I
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
getfield com/teamspeak/ts3client/data/d/ab/b I
if_icmpge L1
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/l I
putfield com/teamspeak/ts3client/data/d/ab/b I
L1:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/c Landroid/widget/SeekBar;
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
getfield com/teamspeak/ts3client/data/d/ab/b I
invokevirtual android/widget/SeekBar/setProgress(I)V
L0:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/l I
ifne L2
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/c Landroid/widget/SeekBar;
iconst_0
invokevirtual android/widget/SeekBar/setEnabled(Z)V
L2:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
getstatic com/teamspeak/ts3client/data/d/y/b I
invokevirtual java/util/BitSet/get(I)Z
ifeq L3
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/q Z
ifne L4
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
ifne L4
L3:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/n Lcom/teamspeak/ts3client/customs/a;
iconst_4
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
ifeq L4
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
getfield com/teamspeak/ts3client/data/d/ab/a I
iconst_4
if_icmpne L5
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
iconst_2
putfield com/teamspeak/ts3client/data/d/ab/a I
L5:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/h Landroid/widget/Spinner;
iconst_2
invokevirtual android/widget/Spinner/setSelection(I)V
L4:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
getstatic com/teamspeak/ts3client/data/d/y/c I
invokevirtual java/util/BitSet/get(I)Z
ifeq L6
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/q Z
ifne L7
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
ifne L7
L6:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/n Lcom/teamspeak/ts3client/customs/a;
iconst_2
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
ifeq L7
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
getfield com/teamspeak/ts3client/data/d/ab/a I
iconst_2
if_icmpne L8
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
iconst_1
putfield com/teamspeak/ts3client/data/d/ab/a I
L8:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/h Landroid/widget/Spinner;
iconst_1
invokevirtual android/widget/Spinner/setSelection(I)V
L7:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
getstatic com/teamspeak/ts3client/data/d/y/d I
invokevirtual java/util/BitSet/get(I)Z
ifeq L9
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/q Z
ifne L10
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
ifne L10
L9:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/n Lcom/teamspeak/ts3client/customs/a;
iconst_1
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
ifeq L10
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
getfield com/teamspeak/ts3client/data/d/ab/a I
iconst_1
if_icmpne L11
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
iconst_0
putfield com/teamspeak/ts3client/data/d/ab/a I
L11:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/h Landroid/widget/Spinner;
iconst_0
invokevirtual android/widget/Spinner/setSelection(I)V
L10:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
getstatic com/teamspeak/ts3client/data/d/y/e I
invokevirtual java/util/BitSet/get(I)Z
ifeq L12
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/q Z
ifne L13
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
ifne L13
L12:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/n Lcom/teamspeak/ts3client/customs/a;
iconst_0
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
ifeq L13
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
getfield com/teamspeak/ts3client/data/d/ab/a I
ifne L14
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
iconst_5
putfield com/teamspeak/ts3client/data/d/ab/a I
L14:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/h Landroid/widget/Spinner;
iconst_5
invokevirtual android/widget/Spinner/setSelection(I)V
L13:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
getstatic com/teamspeak/ts3client/data/d/y/a I
invokevirtual java/util/BitSet/get(I)Z
ifeq L15
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/q Z
ifne L16
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
ifne L16
L15:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/n Lcom/teamspeak/ts3client/customs/a;
iconst_5
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
ifeq L16
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
getfield com/teamspeak/ts3client/data/d/ab/a I
iconst_5
if_icmpne L17
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
iconst_4
putfield com/teamspeak/ts3client/data/d/ab/a I
L17:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/h Landroid/widget/Spinner;
iconst_4
invokevirtual android/widget/Spinner/setSelection(I)V
L16:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
getstatic com/teamspeak/ts3client/data/d/y/f I
invokevirtual java/util/BitSet/get(I)Z
ifeq L18
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/q Z
ifne L19
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
ifne L19
L18:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/n Lcom/teamspeak/ts3client/customs/a;
iconst_3
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
ifeq L19
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
getfield com/teamspeak/ts3client/data/d/ab/a I
iconst_3
if_icmpne L20
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
iconst_2
putfield com/teamspeak/ts3client/data/d/ab/a I
L20:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/h Landroid/widget/Spinner;
iconst_2
invokevirtual android/widget/Spinner/setSelection(I)V
L19:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
getstatic com/teamspeak/ts3client/data/d/y/a I
invokevirtual java/util/BitSet/get(I)Z
ifne L21
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
getstatic com/teamspeak/ts3client/data/d/y/f I
invokevirtual java/util/BitSet/get(I)Z
ifne L21
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/m Lcom/teamspeak/ts3client/customs/a;
iconst_2
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
L21:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
getstatic com/teamspeak/ts3client/data/d/y/e I
invokevirtual java/util/BitSet/get(I)Z
ifne L22
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
getstatic com/teamspeak/ts3client/data/d/y/d I
invokevirtual java/util/BitSet/get(I)Z
ifne L22
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
getstatic com/teamspeak/ts3client/data/d/y/c I
invokevirtual java/util/BitSet/get(I)Z
ifne L22
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
getstatic com/teamspeak/ts3client/data/d/y/b I
invokevirtual java/util/BitSet/get(I)Z
ifne L22
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/m Lcom/teamspeak/ts3client/customs/a;
iconst_0
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/m Lcom/teamspeak/ts3client/customs/a;
iconst_1
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
L22:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
getstatic com/teamspeak/ts3client/data/d/y/a I
invokevirtual java/util/BitSet/get(I)Z
ifne L23
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
getstatic com/teamspeak/ts3client/data/d/y/f I
invokevirtual java/util/BitSet/get(I)Z
ifne L23
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/m Lcom/teamspeak/ts3client/customs/a;
iconst_2
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/m Lcom/teamspeak/ts3client/customs/a;
iconst_3
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
L23:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
iconst_0
invokevirtual java/util/BitSet/nextSetBit(I)I
iconst_m1
if_icmpeq L24
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/q Z
ifne L25
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
ifne L25
L24:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/m Lcom/teamspeak/ts3client/customs/a;
iconst_0
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/m Lcom/teamspeak/ts3client/customs/a;
iconst_1
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/m Lcom/teamspeak/ts3client/customs/a;
iconst_2
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/m Lcom/teamspeak/ts3client/customs/a;
iconst_3
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
L25:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
ifne L26
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/p Z
ifne L26
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/c Landroid/widget/SeekBar;
iconst_0
invokevirtual android/widget/SeekBar/setEnabled(Z)V
L26:
return
.limit locals 1
.limit stack 2
.end method

.method private a(Landroid/view/View;)V
ldc "channeldialog.editpreset.text"
aload 1
ldc_w 2131493026
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/View;I)V
ldc "channeldialog.editcodec.text"
aload 1
ldc_w 2131493029
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/View;I)V
ldc "channeldialog.editquality.text"
aload 1
ldc_w 2131493032
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/View;I)V
aload 0
aload 1
ldc_w 2131493033
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/SeekBar
putfield com/teamspeak/ts3client/customs/CustomCodecSettings/c Landroid/widget/SeekBar;
aload 0
aload 1
ldc_w 2131493035
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/customs/CustomCodecSettings/d Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493036
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/customs/CustomCodecSettings/f Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493034
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/customs/CustomCodecSettings/g Landroid/widget/TextView;
aload 0
aload 1
ldc_w 2131493030
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/Spinner
putfield com/teamspeak/ts3client/customs/CustomCodecSettings/h Landroid/widget/Spinner;
aload 0
aload 1
ldc_w 2131493027
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/Spinner
putfield com/teamspeak/ts3client/customs/CustomCodecSettings/i Landroid/widget/Spinner;
aload 0
ldc "channeldialog.editpreset"
aload 0
invokevirtual com/teamspeak/ts3client/customs/CustomCodecSettings/getContext()Landroid/content/Context;
iconst_4
invokestatic com/teamspeak/ts3client/data/e/a/b(Ljava/lang/String;Landroid/content/Context;I)Lcom/teamspeak/ts3client/customs/a;
putfield com/teamspeak/ts3client/customs/CustomCodecSettings/m Lcom/teamspeak/ts3client/customs/a;
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/i Landroid/widget/Spinner;
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/m Lcom/teamspeak/ts3client/customs/a;
invokevirtual android/widget/Spinner/setAdapter(Landroid/widget/SpinnerAdapter;)V
aload 0
ldc "channeldialog.editcodec"
aload 0
invokevirtual com/teamspeak/ts3client/customs/CustomCodecSettings/getContext()Landroid/content/Context;
bipush 6
invokestatic com/teamspeak/ts3client/data/e/a/b(Ljava/lang/String;Landroid/content/Context;I)Lcom/teamspeak/ts3client/customs/a;
putfield com/teamspeak/ts3client/customs/CustomCodecSettings/n Lcom/teamspeak/ts3client/customs/a;
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/h Landroid/widget/Spinner;
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/n Lcom/teamspeak/ts3client/customs/a;
invokevirtual android/widget/Spinner/setAdapter(Landroid/widget/SpinnerAdapter;)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/c Landroid/widget/SeekBar;
bipush 10
invokevirtual android/widget/SeekBar/setMax(I)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/c Landroid/widget/SeekBar;
iconst_0
invokevirtual android/widget/SeekBar/setProgress(I)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/c Landroid/widget/SeekBar;
iconst_1
invokevirtual android/widget/SeekBar/setFocusable(Z)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/c Landroid/widget/SeekBar;
iconst_1
invokevirtual android/widget/SeekBar/setFocusableInTouchMode(Z)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/i Landroid/widget/Spinner;
new com/teamspeak/ts3client/customs/b
dup
aload 0
invokespecial com/teamspeak/ts3client/customs/b/<init>(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)V
invokevirtual android/widget/Spinner/setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/h Landroid/widget/Spinner;
new com/teamspeak/ts3client/customs/c
dup
aload 0
invokespecial com/teamspeak/ts3client/customs/c/<init>(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)V
invokevirtual android/widget/Spinner/setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/c Landroid/widget/SeekBar;
new com/teamspeak/ts3client/customs/d
dup
aload 0
invokespecial com/teamspeak/ts3client/customs/d/<init>(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)V
invokevirtual android/widget/SeekBar/setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V
aload 0
invokespecial com/teamspeak/ts3client/customs/CustomCodecSettings/getPerms()V
return
.limit locals 2
.limit stack 4
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Z
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/j Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;Z)Z
aload 0
iload 1
putfield com/teamspeak/ts3client/customs/CustomCodecSettings/j Z
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Ljava/util/BitSet;
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b()V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
getfield com/teamspeak/ts3client/data/d/ab/a I
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
getfield com/teamspeak/ts3client/data/d/ab/b I
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/l I
invokestatic com/teamspeak/ts3client/data/d/y/a(IILjava/util/BitSet;I)Lcom/teamspeak/ts3client/data/d/aa;
astore 1
aload 0
iconst_1
putfield com/teamspeak/ts3client/customs/CustomCodecSettings/j Z
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/c Landroid/widget/SeekBar;
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
getfield com/teamspeak/ts3client/data/d/ab/b I
invokevirtual android/widget/SeekBar/setProgress(I)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/h Landroid/widget/Spinner;
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
getfield com/teamspeak/ts3client/data/d/ab/a I
invokevirtual android/widget/Spinner/setSelection(I)V
getstatic com/teamspeak/ts3client/customs/e/a [I
aload 1
invokevirtual com/teamspeak/ts3client/data/d/aa/ordinal()I
iaload
tableswitch 1
L0
L1
L2
L3
default : L4
L4:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/i Landroid/widget/Spinner;
iconst_3
invokevirtual android/widget/Spinner/setSelection(I)V
L5:
aload 0
iconst_0
putfield com/teamspeak/ts3client/customs/CustomCodecSettings/j Z
return
L0:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/i Landroid/widget/Spinner;
iconst_0
invokevirtual android/widget/Spinner/setSelection(I)V
goto L5
L1:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/i Landroid/widget/Spinner;
iconst_1
invokevirtual android/widget/Spinner/setSelection(I)V
goto L5
L2:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/i Landroid/widget/Spinner;
iconst_2
invokevirtual android/widget/Spinner/setSelection(I)V
goto L5
L3:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/i Landroid/widget/Spinner;
iconst_3
invokevirtual android/widget/Spinner/setSelection(I)V
goto L5
.limit locals 2
.limit stack 4
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)I
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/l I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private c()V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/h Landroid/widget/Spinner;
invokevirtual android/widget/Spinner/getSelectedItemPosition()I
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/c Landroid/widget/SeekBar;
invokevirtual android/widget/SeekBar/getProgress()I
invokestatic com/teamspeak/ts3client/data/d/y/a(II)J
lstore 1
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/d Landroid/widget/TextView;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
new java/text/DecimalFormat
dup
ldc "#.##"
invokespecial java/text/DecimalFormat/<init>(Ljava/lang/String;)V
lload 1
l2f
ldc_w 1024.0F
fdiv
ldc_w 8.0F
fdiv
f2d
invokevirtual java/text/DecimalFormat/format(D)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " KiB/s"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
lload 1
invokespecial com/teamspeak/ts3client/customs/CustomCodecSettings/setWarn(J)V
return
.limit locals 3
.limit stack 5
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Landroid/widget/SeekBar;
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/c Landroid/widget/SeekBar;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Landroid/widget/Spinner;
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/h Landroid/widget/Spinner;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Lcom/teamspeak/ts3client/data/d/ab;
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic g(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/h Landroid/widget/Spinner;
invokevirtual android/widget/Spinner/getSelectedItemPosition()I
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/c Landroid/widget/SeekBar;
invokevirtual android/widget/SeekBar/getProgress()I
invokestatic com/teamspeak/ts3client/data/d/y/a(II)J
lstore 1
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/d Landroid/widget/TextView;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
new java/text/DecimalFormat
dup
ldc "#.##"
invokespecial java/text/DecimalFormat/<init>(Ljava/lang/String;)V
lload 1
l2f
ldc_w 1024.0F
fdiv
ldc_w 8.0F
fdiv
f2d
invokevirtual java/text/DecimalFormat/format(D)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " KiB/s"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
lload 1
invokespecial com/teamspeak/ts3client/customs/CustomCodecSettings/setWarn(J)V
return
.limit locals 3
.limit stack 5
.end method

.method private getPerms()V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/u Lcom/teamspeak/ts3client/data/d/v;
getstatic com/teamspeak/ts3client/jni/g/aQ Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/d/v/a(Lcom/teamspeak/ts3client/jni/g;)I
invokevirtual com/teamspeak/ts3client/data/f/a/a(I)Z
ifeq L0
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
iconst_0
invokevirtual java/util/BitSet/set(I)V
L0:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/u Lcom/teamspeak/ts3client/data/d/v;
getstatic com/teamspeak/ts3client/jni/g/aP Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/d/v/a(Lcom/teamspeak/ts3client/jni/g;)I
invokevirtual com/teamspeak/ts3client/data/f/a/a(I)Z
ifeq L1
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
iconst_1
invokevirtual java/util/BitSet/set(I)V
L1:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/u Lcom/teamspeak/ts3client/data/d/v;
getstatic com/teamspeak/ts3client/jni/g/aN Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/d/v/a(Lcom/teamspeak/ts3client/jni/g;)I
invokevirtual com/teamspeak/ts3client/data/f/a/a(I)Z
ifeq L2
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
iconst_2
invokevirtual java/util/BitSet/set(I)V
L2:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/u Lcom/teamspeak/ts3client/data/d/v;
getstatic com/teamspeak/ts3client/jni/g/aM Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/d/v/a(Lcom/teamspeak/ts3client/jni/g;)I
invokevirtual com/teamspeak/ts3client/data/f/a/a(I)Z
ifeq L3
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
iconst_3
invokevirtual java/util/BitSet/set(I)V
L3:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/u Lcom/teamspeak/ts3client/data/d/v;
getstatic com/teamspeak/ts3client/jni/g/aL Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/d/v/a(Lcom/teamspeak/ts3client/jni/g;)I
invokevirtual com/teamspeak/ts3client/data/f/a/a(I)Z
ifeq L4
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
iconst_4
invokevirtual java/util/BitSet/set(I)V
L4:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/u Lcom/teamspeak/ts3client/data/d/v;
getstatic com/teamspeak/ts3client/jni/g/aO Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/d/v/a(Lcom/teamspeak/ts3client/jni/g;)I
invokevirtual com/teamspeak/ts3client/data/f/a/a(I)Z
ifeq L5
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
iconst_5
invokevirtual java/util/BitSet/set(I)V
L5:
aload 0
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/bj Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
putfield com/teamspeak/ts3client/customs/CustomCodecSettings/q Z
aload 0
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/bk Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
putfield com/teamspeak/ts3client/customs/CustomCodecSettings/p Z
aload 0
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/u Lcom/teamspeak/ts3client/data/d/v;
getstatic com/teamspeak/ts3client/jni/g/aR Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/d/v/a(Lcom/teamspeak/ts3client/jni/g;)I
invokevirtual com/teamspeak/ts3client/data/f/a/b(I)I
putfield com/teamspeak/ts3client/customs/CustomCodecSettings/l I
return
.limit locals 1
.limit stack 4
.end method

.method static synthetic h(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Landroid/widget/Spinner;
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/i Landroid/widget/Spinner;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic i(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Z
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic j(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)I
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/b I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic k(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/g Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method private setWarn(J)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/f Landroid/widget/TextView;
ldc ""
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
lload 1
ldc2_w 29491L
lcmp
iflt L0
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/f Landroid/widget/TextView;
ldc "codec.warning"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L0:
return
.limit locals 3
.limit stack 4
.end method

.method public final a(Lcom/teamspeak/ts3client/data/d/ab;Z)V
aload 0
aload 1
putfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
iload 2
ifeq L0
aload 0
getstatic com/teamspeak/ts3client/data/d/aa/c Lcom/teamspeak/ts3client/data/d/aa;
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/l I
invokestatic com/teamspeak/ts3client/data/d/y/a(Lcom/teamspeak/ts3client/data/d/aa;Ljava/util/BitSet;I)Lcom/teamspeak/ts3client/data/d/ab;
putfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
L0:
aload 0
aload 1
getfield com/teamspeak/ts3client/data/d/ab/b I
putfield com/teamspeak/ts3client/customs/CustomCodecSettings/b I
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/b I
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/l I
if_icmpge L1
aload 0
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/l I
putfield com/teamspeak/ts3client/customs/CustomCodecSettings/b I
L1:
aload 0
iload 2
putfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
ifeq L2
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/l I
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
getfield com/teamspeak/ts3client/data/d/ab/b I
if_icmpge L3
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/l I
putfield com/teamspeak/ts3client/data/d/ab/b I
L3:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/c Landroid/widget/SeekBar;
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
getfield com/teamspeak/ts3client/data/d/ab/b I
invokevirtual android/widget/SeekBar/setProgress(I)V
L2:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/l I
ifne L4
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/c Landroid/widget/SeekBar;
iconst_0
invokevirtual android/widget/SeekBar/setEnabled(Z)V
L4:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
getstatic com/teamspeak/ts3client/data/d/y/b I
invokevirtual java/util/BitSet/get(I)Z
ifeq L5
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/q Z
ifne L6
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
ifne L6
L5:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/n Lcom/teamspeak/ts3client/customs/a;
iconst_4
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
ifeq L6
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
getfield com/teamspeak/ts3client/data/d/ab/a I
iconst_4
if_icmpne L7
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
iconst_2
putfield com/teamspeak/ts3client/data/d/ab/a I
L7:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/h Landroid/widget/Spinner;
iconst_2
invokevirtual android/widget/Spinner/setSelection(I)V
L6:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
getstatic com/teamspeak/ts3client/data/d/y/c I
invokevirtual java/util/BitSet/get(I)Z
ifeq L8
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/q Z
ifne L9
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
ifne L9
L8:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/n Lcom/teamspeak/ts3client/customs/a;
iconst_2
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
ifeq L9
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
getfield com/teamspeak/ts3client/data/d/ab/a I
iconst_2
if_icmpne L10
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
iconst_1
putfield com/teamspeak/ts3client/data/d/ab/a I
L10:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/h Landroid/widget/Spinner;
iconst_1
invokevirtual android/widget/Spinner/setSelection(I)V
L9:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
getstatic com/teamspeak/ts3client/data/d/y/d I
invokevirtual java/util/BitSet/get(I)Z
ifeq L11
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/q Z
ifne L12
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
ifne L12
L11:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/n Lcom/teamspeak/ts3client/customs/a;
iconst_1
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
ifeq L12
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
getfield com/teamspeak/ts3client/data/d/ab/a I
iconst_1
if_icmpne L13
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
iconst_0
putfield com/teamspeak/ts3client/data/d/ab/a I
L13:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/h Landroid/widget/Spinner;
iconst_0
invokevirtual android/widget/Spinner/setSelection(I)V
L12:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
getstatic com/teamspeak/ts3client/data/d/y/e I
invokevirtual java/util/BitSet/get(I)Z
ifeq L14
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/q Z
ifne L15
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
ifne L15
L14:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/n Lcom/teamspeak/ts3client/customs/a;
iconst_0
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
ifeq L15
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
getfield com/teamspeak/ts3client/data/d/ab/a I
ifne L16
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
iconst_5
putfield com/teamspeak/ts3client/data/d/ab/a I
L16:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/h Landroid/widget/Spinner;
iconst_5
invokevirtual android/widget/Spinner/setSelection(I)V
L15:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
getstatic com/teamspeak/ts3client/data/d/y/a I
invokevirtual java/util/BitSet/get(I)Z
ifeq L17
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/q Z
ifne L18
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
ifne L18
L17:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/n Lcom/teamspeak/ts3client/customs/a;
iconst_5
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
ifeq L18
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
getfield com/teamspeak/ts3client/data/d/ab/a I
iconst_5
if_icmpne L19
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
iconst_4
putfield com/teamspeak/ts3client/data/d/ab/a I
L19:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/h Landroid/widget/Spinner;
iconst_4
invokevirtual android/widget/Spinner/setSelection(I)V
L18:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
getstatic com/teamspeak/ts3client/data/d/y/f I
invokevirtual java/util/BitSet/get(I)Z
ifeq L20
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/q Z
ifne L21
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
ifne L21
L20:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/n Lcom/teamspeak/ts3client/customs/a;
iconst_3
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
ifeq L21
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
getfield com/teamspeak/ts3client/data/d/ab/a I
iconst_3
if_icmpne L22
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
iconst_2
putfield com/teamspeak/ts3client/data/d/ab/a I
L22:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/h Landroid/widget/Spinner;
iconst_2
invokevirtual android/widget/Spinner/setSelection(I)V
L21:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
getstatic com/teamspeak/ts3client/data/d/y/a I
invokevirtual java/util/BitSet/get(I)Z
ifne L23
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
getstatic com/teamspeak/ts3client/data/d/y/f I
invokevirtual java/util/BitSet/get(I)Z
ifne L23
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/m Lcom/teamspeak/ts3client/customs/a;
iconst_2
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
L23:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
getstatic com/teamspeak/ts3client/data/d/y/e I
invokevirtual java/util/BitSet/get(I)Z
ifne L24
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
getstatic com/teamspeak/ts3client/data/d/y/d I
invokevirtual java/util/BitSet/get(I)Z
ifne L24
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
getstatic com/teamspeak/ts3client/data/d/y/c I
invokevirtual java/util/BitSet/get(I)Z
ifne L24
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
getstatic com/teamspeak/ts3client/data/d/y/b I
invokevirtual java/util/BitSet/get(I)Z
ifne L24
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/m Lcom/teamspeak/ts3client/customs/a;
iconst_0
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/m Lcom/teamspeak/ts3client/customs/a;
iconst_1
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
L24:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
getstatic com/teamspeak/ts3client/data/d/y/a I
invokevirtual java/util/BitSet/get(I)Z
ifne L25
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
getstatic com/teamspeak/ts3client/data/d/y/f I
invokevirtual java/util/BitSet/get(I)Z
ifne L25
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/m Lcom/teamspeak/ts3client/customs/a;
iconst_2
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/m Lcom/teamspeak/ts3client/customs/a;
iconst_3
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
L25:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/k Ljava/util/BitSet;
iconst_0
invokevirtual java/util/BitSet/nextSetBit(I)I
iconst_m1
if_icmpeq L26
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/q Z
ifne L27
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
ifne L27
L26:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/m Lcom/teamspeak/ts3client/customs/a;
iconst_0
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/m Lcom/teamspeak/ts3client/customs/a;
iconst_1
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/m Lcom/teamspeak/ts3client/customs/a;
iconst_2
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/m Lcom/teamspeak/ts3client/customs/a;
iconst_3
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
L27:
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/o Z
ifne L28
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/p Z
ifne L28
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/c Landroid/widget/SeekBar;
iconst_0
invokevirtual android/widget/SeekBar/setEnabled(Z)V
L28:
aload 0
invokespecial com/teamspeak/ts3client/customs/CustomCodecSettings/b()V
return
.limit locals 3
.limit stack 4
.end method

.method public getSettings()Lcom/teamspeak/ts3client/data/d/ab;
aload 0
getfield com/teamspeak/ts3client/customs/CustomCodecSettings/a Lcom/teamspeak/ts3client/data/d/ab;
areturn
.limit locals 1
.limit stack 1
.end method
