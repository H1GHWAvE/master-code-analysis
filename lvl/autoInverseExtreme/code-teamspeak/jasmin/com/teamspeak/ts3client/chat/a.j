.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/chat/a
.super java/lang/Object
.implements com/teamspeak/ts3client/data/d/s

.field 'a' Ljava/lang/String;

.field public 'b' Z

.field 'c' Ljava/util/ArrayList;

.field 'd' I

.field 'e' Ljava/lang/String;

.field 'f' Lcom/teamspeak/ts3client/chat/h;

.field 'g' Lcom/teamspeak/ts3client/data/c;

.field 'h' Ljava/util/Date;

.field private 'i' Z

.method public <init>(Ljava/lang/String;Ljava/lang/String;Lcom/teamspeak/ts3client/data/c;)V
aload 0
aload 1
aload 2
aload 3
iconst_0
invokespecial com/teamspeak/ts3client/chat/a/<init>(Ljava/lang/String;Ljava/lang/String;Lcom/teamspeak/ts3client/data/c;Z)V
return
.limit locals 4
.limit stack 5
.end method

.method public <init>(Ljava/lang/String;Ljava/lang/String;Lcom/teamspeak/ts3client/data/c;Z)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield com/teamspeak/ts3client/chat/a/b Z
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield com/teamspeak/ts3client/chat/a/c Ljava/util/ArrayList;
aload 0
iconst_0
putfield com/teamspeak/ts3client/chat/a/d I
aload 0
new java/util/Date
dup
invokespecial java/util/Date/<init>()V
putfield com/teamspeak/ts3client/chat/a/h Ljava/util/Date;
aload 0
iconst_0
putfield com/teamspeak/ts3client/chat/a/i Z
aload 0
aload 2
putfield com/teamspeak/ts3client/chat/a/e Ljava/lang/String;
aload 0
aload 1
putfield com/teamspeak/ts3client/chat/a/a Ljava/lang/String;
aload 0
aload 3
putfield com/teamspeak/ts3client/chat/a/g Lcom/teamspeak/ts3client/data/c;
aload 0
iload 4
putfield com/teamspeak/ts3client/chat/a/i Z
iload 4
ifne L0
aload 3
ifnull L0
aload 0
getfield com/teamspeak/ts3client/chat/a/g Lcom/teamspeak/ts3client/data/c;
getfield com/teamspeak/ts3client/data/c/y Landroid/graphics/Bitmap;
ifnonnull L1
aload 0
invokevirtual com/teamspeak/ts3client/chat/a/a()V
L1:
aload 0
invokevirtual com/teamspeak/ts3client/chat/a/c()V
L0:
return
.limit locals 5
.limit stack 3
.end method

.method private static synthetic a(Lcom/teamspeak/ts3client/chat/a;)Ljava/util/ArrayList;
aload 0
getfield com/teamspeak/ts3client/chat/a/c Ljava/util/ArrayList;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic a(Lcom/teamspeak/ts3client/chat/a;Ljava/util/Date;)Ljava/util/Date;
aload 0
aload 1
putfield com/teamspeak/ts3client/chat/a/h Ljava/util/Date;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Lcom/teamspeak/ts3client/chat/h;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/chat/a/f Lcom/teamspeak/ts3client/chat/h;
return
.limit locals 2
.limit stack 2
.end method

.method private static synthetic b(Lcom/teamspeak/ts3client/chat/a;)V
aload 0
invokevirtual com/teamspeak/ts3client/chat/a/c()V
return
.limit locals 1
.limit stack 1
.end method

.method private d()Ljava/util/Date;
aload 0
getfield com/teamspeak/ts3client/chat/a/h Ljava/util/Date;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e()I
aload 0
getfield com/teamspeak/ts3client/chat/a/c Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private f()Ljava/util/ArrayList;
aload 0
getfield com/teamspeak/ts3client/chat/a/c Ljava/util/ArrayList;
areturn
.limit locals 1
.limit stack 1
.end method

.method private g()I
aload 0
getfield com/teamspeak/ts3client/chat/a/d I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private h()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/chat/a/a Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private i()Lcom/teamspeak/ts3client/data/c;
aload 0
getfield com/teamspeak/ts3client/chat/a/g Lcom/teamspeak/ts3client/data/c;
areturn
.limit locals 1
.limit stack 1
.end method

.method private j()Z
aload 0
getfield com/teamspeak/ts3client/chat/a/b Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private k()V
aload 0
iconst_1
putfield com/teamspeak/ts3client/chat/a/b Z
return
.limit locals 1
.limit stack 2
.end method

.method private l()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/chat/a/e Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a()V
aload 0
getfield com/teamspeak/ts3client/chat/a/g Lcom/teamspeak/ts3client/data/c;
ifnull L0
aload 0
getfield com/teamspeak/ts3client/chat/a/g Lcom/teamspeak/ts3client/data/c;
invokevirtual com/teamspeak/ts3client/data/c/b()Ljava/lang/String;
ifnull L0
aload 0
getfield com/teamspeak/ts3client/chat/a/g Lcom/teamspeak/ts3client/data/c;
invokevirtual com/teamspeak/ts3client/data/c/b()Ljava/lang/String;
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L0
aload 0
getfield com/teamspeak/ts3client/chat/a/g Lcom/teamspeak/ts3client/data/c;
aload 0
invokevirtual com/teamspeak/ts3client/data/c/a(Lcom/teamspeak/ts3client/data/d/s;)V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public final a(Lcom/teamspeak/ts3client/chat/y;)V
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
ifnull L0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
invokevirtual android/support/v4/app/Fragment/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/chat/b
dup
aload 0
aload 1
invokespecial com/teamspeak/ts3client/chat/b/<init>(Lcom/teamspeak/ts3client/chat/a;Lcom/teamspeak/ts3client/chat/y;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
L0:
return
.limit locals 2
.limit stack 5
.end method

.method public final a(Ljava/lang/String;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/chat/a/a Ljava/lang/String;
aload 0
invokevirtual com/teamspeak/ts3client/chat/a/c()V
return
.limit locals 2
.limit stack 2
.end method

.method public final b()V
aload 0
aload 0
getfield com/teamspeak/ts3client/chat/a/c Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
putfield com/teamspeak/ts3client/chat/a/d I
return
.limit locals 1
.limit stack 2
.end method

.method final c()V
aload 0
getfield com/teamspeak/ts3client/chat/a/f Lcom/teamspeak/ts3client/chat/h;
ifnull L0
aload 0
getfield com/teamspeak/ts3client/chat/a/f Lcom/teamspeak/ts3client/chat/h;
invokevirtual com/teamspeak/ts3client/chat/h/a()V
L0:
invokestatic com/teamspeak/ts3client/chat/d/a()Lcom/teamspeak/ts3client/chat/d;
invokevirtual com/teamspeak/ts3client/chat/d/d()V
return
.limit locals 1
.limit stack 1
.end method

.method public final y()V
aload 0
getfield com/teamspeak/ts3client/chat/a/f Lcom/teamspeak/ts3client/chat/h;
ifnull L0
aload 0
getfield com/teamspeak/ts3client/chat/a/f Lcom/teamspeak/ts3client/chat/h;
invokevirtual com/teamspeak/ts3client/chat/h/a()V
L0:
return
.limit locals 1
.limit stack 1
.end method
