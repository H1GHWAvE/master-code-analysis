.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/chat/y
.super java/lang/Object

.field 'a' Ljava/lang/String;

.field 'b' Ljava/lang/String;

.field 'c' Landroid/text/Spanned;

.field 'd' Z

.field 'e' Ljava/util/Date;

.field 'f' Z

.field private 'g' Ljava/lang/String;

.method public <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V
aload 0
aload 1
aload 2
aload 3
aload 4
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokespecial com/teamspeak/ts3client/chat/y/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
return
.limit locals 5
.limit stack 6
.end method

.method public <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield com/teamspeak/ts3client/chat/y/d Z
aload 0
iconst_0
putfield com/teamspeak/ts3client/chat/y/f Z
aload 3
astore 6
aload 5
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L0
new java/lang/StringBuilder
dup
ldc "*** "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 6
L0:
aload 1
ifnull L1
aload 0
aload 1
invokestatic com/teamspeak/ts3client/data/d/x/a(Ljava/lang/String;)Ljava/lang/String;
putfield com/teamspeak/ts3client/chat/y/a Ljava/lang/String;
L1:
aload 0
aload 2
putfield com/teamspeak/ts3client/chat/y/g Ljava/lang/String;
aload 0
aload 6
invokestatic com/teamspeak/ts3client/data/d/x/a(Ljava/lang/String;)Ljava/lang/String;
putfield com/teamspeak/ts3client/chat/y/b Ljava/lang/String;
aload 0
aload 0
getfield com/teamspeak/ts3client/chat/y/b Ljava/lang/String;
invokestatic com/teamspeak/ts3client/data/d/a/a(Ljava/lang/String;)Landroid/text/Spanned;
putfield com/teamspeak/ts3client/chat/y/c Landroid/text/Spanned;
aload 0
new java/util/Date
dup
invokespecial java/util/Date/<init>()V
putfield com/teamspeak/ts3client/chat/y/e Ljava/util/Date;
aload 0
aload 4
invokevirtual java/lang/Boolean/booleanValue()Z
putfield com/teamspeak/ts3client/chat/y/d Z
aload 0
aload 5
invokevirtual java/lang/Boolean/booleanValue()Z
putfield com/teamspeak/ts3client/chat/y/f Z
return
.limit locals 7
.limit stack 3
.end method

.method private b()Landroid/text/Spanned;
aload 0
getfield com/teamspeak/ts3client/chat/y/c Landroid/text/Spanned;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/chat/y/a Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/chat/y/g Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/chat/y/b Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private f()Ljava/util/Date;
aload 0
getfield com/teamspeak/ts3client/chat/y/e Ljava/util/Date;
areturn
.limit locals 1
.limit stack 1
.end method

.method private g()Z
aload 0
getfield com/teamspeak/ts3client/chat/y/f Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a()Z
aload 0
getfield com/teamspeak/ts3client/chat/y/d Z
ireturn
.limit locals 1
.limit stack 1
.end method
