.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/e/e
.super android/widget/BaseAdapter

.field 'a' Ljava/util/ArrayList;

.field private 'b' Landroid/content/Context;

.field private 'c' Landroid/view/LayoutInflater;

.method public <init>(Landroid/content/Context;)V
aload 0
invokespecial android/widget/BaseAdapter/<init>()V
aload 0
aload 1
putfield com/teamspeak/ts3client/e/e/b Landroid/content/Context;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield com/teamspeak/ts3client/e/e/a Ljava/util/ArrayList;
aload 0
aload 1
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
putfield com/teamspeak/ts3client/e/e/c Landroid/view/LayoutInflater;
return
.limit locals 2
.limit stack 3
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/e/e;)Landroid/content/Context;
aload 0
getfield com/teamspeak/ts3client/e/e/b Landroid/content/Context;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Lcom/teamspeak/ts3client/e/a;)V
aload 0
getfield com/teamspeak/ts3client/e/e/a Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/contains(Ljava/lang/Object;)Z
ifne L0
aload 0
getfield com/teamspeak/ts3client/e/e/a Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L0:
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/e/e;)Ljava/util/ArrayList;
aload 0
getfield com/teamspeak/ts3client/e/e/a Ljava/util/ArrayList;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b(Lcom/teamspeak/ts3client/e/a;)V
aload 0
getfield com/teamspeak/ts3client/e/e/a Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/contains(Ljava/lang/Object;)Z
ifne L0
aload 0
getfield com/teamspeak/ts3client/e/e/a Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/remove(Ljava/lang/Object;)Z
pop
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final getCount()I
aload 0
getfield com/teamspeak/ts3client/e/e/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getItem(I)Ljava/lang/Object;
aload 0
getfield com/teamspeak/ts3client/e/e/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final getItemId(I)J
lconst_0
lreturn
.limit locals 2
.limit stack 2
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
aload 0
getfield com/teamspeak/ts3client/e/e/c Landroid/view/LayoutInflater;
ldc_w 2130903106
aconst_null
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
astore 2
aload 2
ldc_w 2131493256
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
astore 3
aload 2
ldc_w 2131493257
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/ImageView
astore 4
aload 2
ldc_w 2131493258
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/ImageView
astore 5
aload 0
getfield com/teamspeak/ts3client/e/e/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/teamspeak/ts3client/e/a
getfield com/teamspeak/ts3client/e/a/e Z
ifeq L0
aload 5
ldc_w -8947849
getstatic android/graphics/PorterDuff$Mode/MULTIPLY Landroid/graphics/PorterDuff$Mode;
invokevirtual android/widget/ImageView/setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V
L1:
aload 0
getfield com/teamspeak/ts3client/e/e/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/teamspeak/ts3client/e/a
getfield com/teamspeak/ts3client/e/a/e Z
ifeq L2
aload 4
ldc_w -8947849
getstatic android/graphics/PorterDuff$Mode/MULTIPLY Landroid/graphics/PorterDuff$Mode;
invokevirtual android/widget/ImageView/setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V
L3:
aload 3
aload 0
getfield com/teamspeak/ts3client/e/e/a Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/teamspeak/ts3client/e/a
getfield com/teamspeak/ts3client/e/a/b Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 2
areturn
L0:
aload 5
new com/teamspeak/ts3client/e/f
dup
aload 0
iload 1
invokespecial com/teamspeak/ts3client/e/f/<init>(Lcom/teamspeak/ts3client/e/e;I)V
invokevirtual android/widget/ImageView/setOnClickListener(Landroid/view/View$OnClickListener;)V
goto L1
L2:
aload 4
new com/teamspeak/ts3client/e/i
dup
aload 0
iload 1
invokespecial com/teamspeak/ts3client/e/i/<init>(Lcom/teamspeak/ts3client/e/e;I)V
invokevirtual android/widget/ImageView/setOnClickListener(Landroid/view/View$OnClickListener;)V
goto L3
.limit locals 6
.limit stack 5
.end method
