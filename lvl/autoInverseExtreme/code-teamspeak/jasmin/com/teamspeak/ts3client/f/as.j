.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/f/as
.super android/support/v4/app/Fragment

.field private 'a' Lcom/teamspeak/ts3client/Ts3Application;

.method public <init>()V
aload 0
invokespecial android/support/v4/app/Fragment/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
aload 0
invokevirtual com/teamspeak/ts3client/f/as/n()V
aload 0
aload 0
invokevirtual com/teamspeak/ts3client/f/as/i()Landroid/support/v4/app/bb;
invokevirtual android/support/v4/app/bb/getApplicationContext()Landroid/content/Context;
checkcast com/teamspeak/ts3client/Ts3Application
putfield com/teamspeak/ts3client/f/as/a Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/f/as/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/o Landroid/support/v7/app/a;
ldc "menu.settings"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/support/v7/app/a/b(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/f/as/a Lcom/teamspeak/ts3client/Ts3Application;
aload 0
putfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
new android/widget/ScrollView
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
invokespecial android/widget/ScrollView/<init>(Landroid/content/Context;)V
astore 2
new android/widget/LinearLayout
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
astore 3
aload 3
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
iconst_m1
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
invokevirtual android/widget/LinearLayout/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 3
iconst_1
invokevirtual android/widget/LinearLayout/setOrientation(I)V
new java/util/TreeMap
dup
invokespecial java/util/TreeMap/<init>()V
astore 4
aload 4
ldc "vibrate_chat"
ldc "settings.vibrate.chat"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/util/TreeMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 4
ldc "vibrate_chatnew"
ldc "settings.vibrate.chatnew"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/util/TreeMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 4
ldc "vibrate_poke"
ldc "settings.vibrate.poke"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/util/TreeMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 3
new com/teamspeak/ts3client/f/a
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
ldc "settings.spacer.audio"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokespecial com/teamspeak/ts3client/f/a/<init>(Landroid/content/Context;Ljava/lang/String;)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
new com/teamspeak/ts3client/f/c
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
ldc "audio_ptt"
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "settings.ptt"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.ptt.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokespecial com/teamspeak/ts3client/f/c/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
new com/teamspeak/ts3client/f/c
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
ldc "android_overlay_setting"
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "settings.android_overlay"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.android_overlay.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.android_overlay.info"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokespecial com/teamspeak/ts3client/f/c/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
new com/teamspeak/ts3client/f/p
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
ldc "ptt_key_setting"
ldc "settings.pttkey"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.pttkey.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.pttkey.intercept"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.pttkey.info"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
invokespecial com/teamspeak/ts3client/f/p/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/support/v4/app/bi;)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
new com/teamspeak/ts3client/f/ak
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
ldc "voiceactivation_level"
ldc "settings.val"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.val.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
invokespecial com/teamspeak/ts3client/f/ak/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/support/v4/app/bi;)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
new com/teamspeak/ts3client/f/k
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
ldc "volume_modifier"
ldc "settings.volume_modifier"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.volume_modifier.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
invokespecial com/teamspeak/ts3client/f/k/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/support/v4/app/bi;)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
new com/teamspeak/ts3client/f/c
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
ldc "audio_handfree"
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "settings.handfree"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.handfree.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.handfree.warn"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_0
invokespecial com/teamspeak/ts3client/f/c/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;B)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
new com/teamspeak/ts3client/f/c
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
ldc "audio_bt"
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "settings.bt"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.bt.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokespecial com/teamspeak/ts3client/f/c/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
new com/teamspeak/ts3client/f/ag
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
ldc "whisper"
ldc "settings.whisper"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.whisper.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
bipush -2
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
invokespecial com/teamspeak/ts3client/f/ag/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/support/v4/app/bi;)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
new com/teamspeak/ts3client/f/ag
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
ldc "sound_pack"
ldc "settings.soundpack"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.soundpack.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc_w 2131361796
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
invokespecial com/teamspeak/ts3client/f/ag/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/support/v4/app/bi;)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
new com/teamspeak/ts3client/f/b
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
ldc "settings.audiosettings"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.audiosettings.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
invokespecial com/teamspeak/ts3client/f/b/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/support/v4/app/bi;)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
new com/teamspeak/ts3client/f/a
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
ldc "settings.spacer.call"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokespecial com/teamspeak/ts3client/f/a/<init>(Landroid/content/Context;Ljava/lang/String;)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
new com/teamspeak/ts3client/f/c
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
ldc "call_setaway"
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "settings.callstatus"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.callstatus.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokespecial com/teamspeak/ts3client/f/c/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
new com/teamspeak/ts3client/f/f
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
ldc "call_awaymessage"
ldc "settings.callmessage.edittext"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.callmessage"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.callmessage.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
ldc_w 524289
iconst_0
invokespecial com/teamspeak/ts3client/f/f/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/support/v4/app/bi;IB)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
new com/teamspeak/ts3client/f/a
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
ldc "settings.spacer.gui"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokespecial com/teamspeak/ts3client/f/a/<init>(Landroid/content/Context;Ljava/lang/String;)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
new com/teamspeak/ts3client/f/ag
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
ldc "lang_tag"
ldc "settings.language"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.language.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
iconst_m1
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
invokespecial com/teamspeak/ts3client/f/ag/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/support/v4/app/bi;)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
new com/teamspeak/ts3client/f/v
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
ldc "settings.vibrate"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.vibrate.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.vibrate.text2"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
aload 4
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
invokespecial com/teamspeak/ts3client/f/v/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/TreeMap;Landroid/support/v4/app/bi;)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
new com/teamspeak/ts3client/f/f
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
ldc "avatarlimit"
ldc "500"
ldc "settings.avatarlimit"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.avatarlimit.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
bipush 7
invokespecial com/teamspeak/ts3client/f/f/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/support/v4/app/bi;I)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
new com/teamspeak/ts3client/f/f
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
ldc "reconnect.limit"
ldc "15"
ldc "settings.reconnect.limit"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.reconnect.limit.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
iconst_4
invokespecial com/teamspeak/ts3client/f/f/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/support/v4/app/bi;I)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
new com/teamspeak/ts3client/f/ab
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
ldc "channel_height"
ldc "settings.channelheight"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.channelheight.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
invokespecial com/teamspeak/ts3client/f/ab/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/support/v4/app/bi;)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
new com/teamspeak/ts3client/f/ab
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
ldc "client_height"
ldc "settings.clientheight"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.clientheight.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
invokespecial com/teamspeak/ts3client/f/ab/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/support/v4/app/bi;)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
new com/teamspeak/ts3client/f/c
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
ldc "longclick_client"
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "settings.clientclick"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.clientclick.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokespecial com/teamspeak/ts3client/f/c/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
new com/teamspeak/ts3client/f/c
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
ldc "trenn_linie"
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "settings.seperator"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.seperator.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokespecial com/teamspeak/ts3client/f/c/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
new com/teamspeak/ts3client/f/c
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
ldc "show_squeryclient"
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "settings.queryclient"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.queryclient.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokespecial com/teamspeak/ts3client/f/c/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
new com/teamspeak/ts3client/f/c
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
ldc "show_flags"
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "settings.flags"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.flags.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokespecial com/teamspeak/ts3client/f/c/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
new com/teamspeak/ts3client/f/c
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
ldc "use_proximity"
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "settings.proximity"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.proximity.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokespecial com/teamspeak/ts3client/f/c/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
new com/teamspeak/ts3client/f/c
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
ldc "enable_fullscreen"
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "settings.fullscreen"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.fullscreen.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokespecial com/teamspeak/ts3client/f/c/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
new com/teamspeak/ts3client/f/ag
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
ldc "screen_rotation"
ldc "settings.screenrotation"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.screenrotation.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
bipush -3
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
invokespecial com/teamspeak/ts3client/f/ag/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/support/v4/app/bi;)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
new com/teamspeak/ts3client/f/c
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
ldc "talk_notification"
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "settings.talknotification"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.talknotification.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokespecial com/teamspeak/ts3client/f/c/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
new com/teamspeak/ts3client/f/a
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
ldc "settings.spacer.system"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokespecial com/teamspeak/ts3client/f/a/<init>(Landroid/content/Context;Ljava/lang/String;)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 3
new com/teamspeak/ts3client/f/c
dup
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
ldc "create_debuglog"
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
ldc "settings.debuglog"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
ldc "settings.debuglog.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokespecial com/teamspeak/ts3client/f/c/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 2
aload 3
invokevirtual android/widget/ScrollView/addView(Landroid/view/View;)V
aload 2
areturn
.limit locals 5
.limit stack 11
.end method

.method public final a(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/support/v4/app/Fragment/a(Landroid/os/Bundle;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
aload 1
invokeinterface android/view/Menu/clear()V 0
aload 0
aload 1
aload 2
invokespecial android/support/v4/app/Fragment/a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
return
.limit locals 3
.limit stack 3
.end method

.method public final c(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/support/v4/app/Fragment/c(Landroid/os/Bundle;)V
return
.limit locals 2
.limit stack 2
.end method
