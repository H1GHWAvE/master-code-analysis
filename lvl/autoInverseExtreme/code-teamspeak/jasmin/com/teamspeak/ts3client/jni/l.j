.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/jni/l
.super java/lang/Object

.field private static 'a' Lcom/teamspeak/ts3client/jni/l;

.field private static 'b' Ljava/lang/String;

.field private static 'c' Ljava/util/Vector;

.method static <clinit>()V
aconst_null
putstatic com/teamspeak/ts3client/jni/l/a Lcom/teamspeak/ts3client/jni/l;
ldc "Ts3Callback"
putstatic com/teamspeak/ts3client/jni/l/b Ljava/lang/String;
new java/util/Vector
dup
invokespecial java/util/Vector/<init>()V
putstatic com/teamspeak/ts3client/jni/l/c Ljava/util/Vector;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a()Lcom/teamspeak/ts3client/jni/l;
getstatic com/teamspeak/ts3client/jni/l/a Lcom/teamspeak/ts3client/jni/l;
ifnonnull L0
new com/teamspeak/ts3client/jni/l
dup
invokespecial com/teamspeak/ts3client/jni/l/<init>()V
putstatic com/teamspeak/ts3client/jni/l/a Lcom/teamspeak/ts3client/jni/l;
L0:
getstatic com/teamspeak/ts3client/jni/l/a Lcom/teamspeak/ts3client/jni/l;
areturn
.limit locals 0
.limit stack 2
.end method

.method private a(Lcom/teamspeak/ts3client/data/w;Lcom/teamspeak/ts3client/jni/k;)V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 1
aload 2
invokeinterface com/teamspeak/ts3client/data/w/a(Lcom/teamspeak/ts3client/jni/k;)V 1
L1:
aload 0
monitorexit
return
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method public static a(Lcom/teamspeak/ts3client/jni/k;)V
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L4
.catch all from L3 to L5 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L4
.catch all from L8 to L9 using L2
ldc com/teamspeak/ts3client/jni/l
monitorenter
L0:
getstatic com/teamspeak/ts3client/jni/l/c Ljava/util/Vector;
astore 1
aload 1
monitorenter
L1:
getstatic com/teamspeak/ts3client/jni/l/c Ljava/util/Vector;
invokevirtual java/util/Vector/clone()Ljava/lang/Object;
checkcast java/util/Vector
astore 2
aload 1
monitorexit
L3:
aload 2
invokevirtual java/util/Vector/iterator()Ljava/util/Iterator;
astore 1
L5:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L9
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/teamspeak/ts3client/data/w
astore 2
getstatic com/teamspeak/ts3client/jni/l/a Lcom/teamspeak/ts3client/jni/l;
aload 2
aload 0
invokespecial com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/data/w;Lcom/teamspeak/ts3client/jni/k;)V
L6:
goto L5
L2:
astore 0
ldc com/teamspeak/ts3client/jni/l
monitorexit
aload 0
athrow
L4:
astore 0
L7:
aload 1
monitorexit
L8:
aload 0
athrow
L9:
ldc com/teamspeak/ts3client/jni/l
monitorexit
return
.limit locals 3
.limit stack 3
.end method

.method public final a(Lcom/teamspeak/ts3client/data/w;)V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
getstatic com/teamspeak/ts3client/jni/l/c Ljava/util/Vector;
aload 1
invokevirtual java/util/Vector/contains(Ljava/lang/Object;)Z
ifne L1
getstatic com/teamspeak/ts3client/jni/l/c Ljava/util/Vector;
aload 1
invokevirtual java/util/Vector/add(Ljava/lang/Object;)Z
pop
L1:
aload 0
monitorexit
return
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method public final b(Lcom/teamspeak/ts3client/data/w;)V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
getstatic com/teamspeak/ts3client/jni/l/c Ljava/util/Vector;
aload 1
invokevirtual java/util/Vector/remove(Ljava/lang/Object;)Z
pop
L1:
aload 0
monitorexit
return
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 2
.end method
