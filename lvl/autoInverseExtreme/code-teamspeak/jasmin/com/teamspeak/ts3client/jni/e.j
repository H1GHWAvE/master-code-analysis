.bytecode 50.0
.class public final synchronized enum com/teamspeak/ts3client/jni/e
.super java/lang/Enum

.field public static final enum 'a' Lcom/teamspeak/ts3client/jni/e;

.field public static final enum 'b' Lcom/teamspeak/ts3client/jni/e;

.field public static final enum 'c' Lcom/teamspeak/ts3client/jni/e;

.field public static final enum 'd' Lcom/teamspeak/ts3client/jni/e;

.field public static final enum 'e' Lcom/teamspeak/ts3client/jni/e;

.field private static final synthetic 'g' [Lcom/teamspeak/ts3client/jni/e;

.field private 'f' I

.method static <clinit>()V
new com/teamspeak/ts3client/jni/e
dup
ldc "STATUS_DISCONNECTED"
iconst_0
iconst_0
invokespecial com/teamspeak/ts3client/jni/e/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/e/a Lcom/teamspeak/ts3client/jni/e;
new com/teamspeak/ts3client/jni/e
dup
ldc "STATUS_CONNECTING"
iconst_1
iconst_1
invokespecial com/teamspeak/ts3client/jni/e/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/e/b Lcom/teamspeak/ts3client/jni/e;
new com/teamspeak/ts3client/jni/e
dup
ldc "STATUS_CONNECTED"
iconst_2
iconst_2
invokespecial com/teamspeak/ts3client/jni/e/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/e/c Lcom/teamspeak/ts3client/jni/e;
new com/teamspeak/ts3client/jni/e
dup
ldc "STATUS_CONNECTION_ESTABLISHING"
iconst_3
iconst_3
invokespecial com/teamspeak/ts3client/jni/e/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/e/d Lcom/teamspeak/ts3client/jni/e;
new com/teamspeak/ts3client/jni/e
dup
ldc "STATUS_CONNECTION_ESTABLISHED"
iconst_4
iconst_4
invokespecial com/teamspeak/ts3client/jni/e/<init>(Ljava/lang/String;II)V
putstatic com/teamspeak/ts3client/jni/e/e Lcom/teamspeak/ts3client/jni/e;
iconst_5
anewarray com/teamspeak/ts3client/jni/e
dup
iconst_0
getstatic com/teamspeak/ts3client/jni/e/a Lcom/teamspeak/ts3client/jni/e;
aastore
dup
iconst_1
getstatic com/teamspeak/ts3client/jni/e/b Lcom/teamspeak/ts3client/jni/e;
aastore
dup
iconst_2
getstatic com/teamspeak/ts3client/jni/e/c Lcom/teamspeak/ts3client/jni/e;
aastore
dup
iconst_3
getstatic com/teamspeak/ts3client/jni/e/d Lcom/teamspeak/ts3client/jni/e;
aastore
dup
iconst_4
getstatic com/teamspeak/ts3client/jni/e/e Lcom/teamspeak/ts3client/jni/e;
aastore
putstatic com/teamspeak/ts3client/jni/e/g [Lcom/teamspeak/ts3client/jni/e;
return
.limit locals 0
.limit stack 5
.end method

.method private <init>(Ljava/lang/String;II)V
aload 0
aload 1
iload 2
invokespecial java/lang/Enum/<init>(Ljava/lang/String;I)V
aload 0
iload 3
putfield com/teamspeak/ts3client/jni/e/f I
return
.limit locals 4
.limit stack 3
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/teamspeak/ts3client/jni/e;
ldc com/teamspeak/ts3client/jni/e
aload 0
invokestatic java/lang/Enum/valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
checkcast com/teamspeak/ts3client/jni/e
areturn
.limit locals 1
.limit stack 2
.end method

.method public static values()[Lcom/teamspeak/ts3client/jni/e;
getstatic com/teamspeak/ts3client/jni/e/g [Lcom/teamspeak/ts3client/jni/e;
invokevirtual [Lcom/teamspeak/ts3client/jni/e;/clone()Ljava/lang/Object;
checkcast [Lcom/teamspeak/ts3client/jni/e;
areturn
.limit locals 0
.limit stack 1
.end method

.method public final a()I
aload 0
getfield com/teamspeak/ts3client/jni/e/f I
ireturn
.limit locals 1
.limit stack 1
.end method
