.bytecode 50.0
.class public synchronized com/teamspeak/ts3client/jni/events/ClientIDs
.super java/lang/Object
.implements com/teamspeak/ts3client/jni/k

.field public 'a' Ljava/lang/String;

.field public 'b' I

.field private 'c' J

.field private 'd' Ljava/lang/String;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private <init>(JLjava/lang/String;ILjava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
lload 1
putfield com/teamspeak/ts3client/jni/events/ClientIDs/c J
aload 0
aload 3
putfield com/teamspeak/ts3client/jni/events/ClientIDs/a Ljava/lang/String;
aload 0
iload 4
putfield com/teamspeak/ts3client/jni/events/ClientIDs/b I
aload 0
aload 5
putfield com/teamspeak/ts3client/jni/events/ClientIDs/d Ljava/lang/String;
aload 0
invokestatic com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/jni/k;)V
return
.limit locals 6
.limit stack 3
.end method

.method private a()J
aload 0
getfield com/teamspeak/ts3client/jni/events/ClientIDs/c J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private b()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/ClientIDs/a Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c()I
aload 0
getfield com/teamspeak/ts3client/jni/events/ClientIDs/b I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/ClientIDs/d Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "ClientIDs [serverConnectionHandlerID="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/jni/events/ClientIDs/c J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", uniqueClientIdentifier="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/ClientIDs/a Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", clientID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/ClientIDs/b I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", clientName="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/ClientIDs/d Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method
