.bytecode 50.0
.class public synchronized com/teamspeak/ts3client/jni/events/UpdateClient
.super java/lang/Object
.implements com/teamspeak/ts3client/jni/k

.field public 'a' I

.field private 'b' J

.field private 'c' I

.field private 'd' Ljava/lang/String;

.field private 'e' Ljava/lang/String;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private <init>(JIILjava/lang/String;Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
lload 1
putfield com/teamspeak/ts3client/jni/events/UpdateClient/b J
aload 0
iload 3
putfield com/teamspeak/ts3client/jni/events/UpdateClient/a I
aload 0
iload 4
putfield com/teamspeak/ts3client/jni/events/UpdateClient/c I
aload 0
aload 5
putfield com/teamspeak/ts3client/jni/events/UpdateClient/d Ljava/lang/String;
aload 0
aload 6
putfield com/teamspeak/ts3client/jni/events/UpdateClient/e Ljava/lang/String;
aload 0
invokestatic com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/jni/k;)V
return
.limit locals 7
.limit stack 3
.end method

.method private b()I
aload 0
getfield com/teamspeak/ts3client/jni/events/UpdateClient/c I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private c()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/UpdateClient/d Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/UpdateClient/e Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e()J
aload 0
getfield com/teamspeak/ts3client/jni/events/UpdateClient/b J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public final a()I
aload 0
getfield com/teamspeak/ts3client/jni/events/UpdateClient/a I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "UpdateClient [serverConnectionHandlerID="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/jni/events/UpdateClient/b J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", clientID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/UpdateClient/a I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", invokerID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/UpdateClient/c I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", invokerName="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/UpdateClient/d Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", invokerUniqueIdentifier="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/UpdateClient/e Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method
