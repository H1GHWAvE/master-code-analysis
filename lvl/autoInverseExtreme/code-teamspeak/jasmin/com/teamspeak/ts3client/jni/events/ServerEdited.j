.bytecode 50.0
.class public synchronized com/teamspeak/ts3client/jni/events/ServerEdited
.super java/lang/Object
.implements com/teamspeak/ts3client/jni/k

.field private 'a' J

.field private 'b' I

.field private 'c' Ljava/lang/String;

.field private 'd' Ljava/lang/String;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private <init>(JILjava/lang/String;Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
lload 1
putfield com/teamspeak/ts3client/jni/events/ServerEdited/a J
aload 0
iload 3
putfield com/teamspeak/ts3client/jni/events/ServerEdited/b I
aload 0
aload 4
putfield com/teamspeak/ts3client/jni/events/ServerEdited/c Ljava/lang/String;
aload 0
aload 5
putfield com/teamspeak/ts3client/jni/events/ServerEdited/d Ljava/lang/String;
aload 0
invokestatic com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/jni/k;)V
return
.limit locals 6
.limit stack 3
.end method

.method private a()I
aload 0
getfield com/teamspeak/ts3client/jni/events/ServerEdited/b I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private b()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/ServerEdited/c Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/ServerEdited/d Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d()J
aload 0
getfield com/teamspeak/ts3client/jni/events/ServerEdited/a J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "ServerEdited [serverConnectionHandlerID="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/jni/events/ServerEdited/a J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", editerID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/ServerEdited/b I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", editerName="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/ServerEdited/c Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", editerUniqueIdentifier="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/ServerEdited/d Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method
