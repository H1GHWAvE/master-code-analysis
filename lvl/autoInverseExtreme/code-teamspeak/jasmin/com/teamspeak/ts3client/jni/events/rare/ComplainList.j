.bytecode 50.0
.class public synchronized com/teamspeak/ts3client/jni/events/rare/ComplainList
.super java/lang/Object
.implements com/teamspeak/ts3client/jni/k

.field private 'a' J

.field private 'b' J

.field private 'c' Ljava/lang/String;

.field private 'd' J

.field private 'e' Ljava/lang/String;

.field private 'f' Ljava/lang/String;

.field private 'g' J

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private <init>(JJLjava/lang/String;JLjava/lang/String;Ljava/lang/String;J)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
lload 1
putfield com/teamspeak/ts3client/jni/events/rare/ComplainList/a J
aload 0
lload 3
putfield com/teamspeak/ts3client/jni/events/rare/ComplainList/b J
aload 0
aload 5
putfield com/teamspeak/ts3client/jni/events/rare/ComplainList/c Ljava/lang/String;
aload 0
lload 6
putfield com/teamspeak/ts3client/jni/events/rare/ComplainList/d J
aload 0
aload 8
putfield com/teamspeak/ts3client/jni/events/rare/ComplainList/e Ljava/lang/String;
aload 0
aload 9
putfield com/teamspeak/ts3client/jni/events/rare/ComplainList/f Ljava/lang/String;
aload 0
lload 10
putfield com/teamspeak/ts3client/jni/events/rare/ComplainList/g J
aload 0
invokestatic com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/jni/k;)V
return
.limit locals 12
.limit stack 3
.end method

.method private a()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ComplainList/f Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b()J
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ComplainList/d J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private c()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ComplainList/e Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d()J
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ComplainList/a J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private e()J
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ComplainList/b J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private f()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ComplainList/c Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private g()J
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ComplainList/g J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "ComplainList [serverConnectionHandlerID="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ComplainList/a J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", targetClientDatabaseID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ComplainList/b J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", targetClientNickName="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ComplainList/c Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", fromClientDatabaseID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ComplainList/d J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", fromClientNickName="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ComplainList/e Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", complainReason="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ComplainList/f Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", timestamp="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/ComplainList/g J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method
