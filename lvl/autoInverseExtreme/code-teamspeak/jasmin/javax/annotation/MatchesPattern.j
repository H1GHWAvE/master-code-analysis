.bytecode 50.0
.class public abstract interface annotation javax/annotation/MatchesPattern
.super java/lang/Object
.implements java/lang/annotation/Annotation
.annotation visible Ljava/lang/annotation/Documented;
.end annotation
.annotation visible Ljava/lang/annotation/Retention;
value e Ljava/lang/annotation/RetentionPolicy; = "RUNTIME"
.end annotation
.annotation visible Ljavax/annotation/meta/TypeQualifier;
applicableTo c = Ljava/lang/String;
.end annotation

.method public abstract flags()I
.annotation default
I = 0
.end annotation
.end method

.method public abstract value()Ljava/lang/String;
.annotation visible Ljavax/annotation/RegEx;
.end annotation
.end method
