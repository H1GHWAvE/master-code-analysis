.bytecode 50.0
.class public synchronized org/xbill/DNS/DNSSEC
.super java/lang/Object

.field private static final 'ASN1_INT' I = 2


.field private static final 'ASN1_SEQ' I = 48


.field private static final 'DSA_LEN' I = 20


.field private static final 'ECDSA_P256' Lorg/xbill/DNS/DNSSEC$ECKeyInfo;

.field private static final 'ECDSA_P384' Lorg/xbill/DNS/DNSSEC$ECKeyInfo;

.field private static final 'GOST' Lorg/xbill/DNS/DNSSEC$ECKeyInfo;

.method static <clinit>()V
new org/xbill/DNS/DNSSEC$ECKeyInfo
dup
bipush 32
ldc "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD97"
ldc "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD94"
ldc "A6"
ldc "1"
ldc "8D91E471E0989CDA27DF505A453F2B7635294F2DDF23E3B122ACC99C9E9F1E14"
ldc "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6C611070995AD10045841B09B761B893"
invokespecial org/xbill/DNS/DNSSEC$ECKeyInfo/<init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
putstatic org/xbill/DNS/DNSSEC/GOST Lorg/xbill/DNS/DNSSEC$ECKeyInfo;
new org/xbill/DNS/DNSSEC$ECKeyInfo
dup
bipush 32
ldc "FFFFFFFF00000001000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF"
ldc "FFFFFFFF00000001000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFC"
ldc "5AC635D8AA3A93E7B3EBBD55769886BC651D06B0CC53B0F63BCE3C3E27D2604B"
ldc "6B17D1F2E12C4247F8BCE6E563A440F277037D812DEB33A0F4A13945D898C296"
ldc "4FE342E2FE1A7F9B8EE7EB4A7C0F9E162BCE33576B315ECECBB6406837BF51F5"
ldc "FFFFFFFF00000000FFFFFFFFFFFFFFFFBCE6FAADA7179E84F3B9CAC2FC632551"
invokespecial org/xbill/DNS/DNSSEC$ECKeyInfo/<init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
putstatic org/xbill/DNS/DNSSEC/ECDSA_P256 Lorg/xbill/DNS/DNSSEC$ECKeyInfo;
new org/xbill/DNS/DNSSEC$ECKeyInfo
dup
bipush 48
ldc "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFF0000000000000000FFFFFFFF"
ldc "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFF0000000000000000FFFFFFFC"
ldc "B3312FA7E23EE7E4988E056BE3F82D19181D9C6EFE8141120314088F5013875AC656398D8A2ED19D2A85C8EDD3EC2AEF"
ldc "AA87CA22BE8B05378EB1C71EF320AD746E1D3B628BA79B9859F741E082542A385502F25DBF55296C3A545E3872760AB7"
ldc "3617DE4A96262C6F5D9E98BF9292DC29F8F41DBD289A147CE9DA3113B5F0B8C00A60B1CE1D7E819D7A431D7C90EA0E5F"
ldc "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC7634D81F4372DDF581A0DB248B0A77AECEC196ACCC52973"
invokespecial org/xbill/DNS/DNSSEC$ECKeyInfo/<init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
putstatic org/xbill/DNS/DNSSEC/ECDSA_P384 Lorg/xbill/DNS/DNSSEC$ECKeyInfo;
return
.limit locals 0
.limit stack 9
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static BigIntegerLength(Ljava/math/BigInteger;)I
aload 0
invokevirtual java/math/BigInteger/bitLength()I
bipush 7
iadd
bipush 8
idiv
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static DSASignaturefromDNS([B)[B
bipush 21
istore 2
aload 0
arraylength
bipush 41
if_icmpeq L0
new org/xbill/DNS/DNSSEC$SignatureVerificationException
dup
invokespecial org/xbill/DNS/DNSSEC$SignatureVerificationException/<init>()V
athrow
L0:
new org/xbill/DNS/DNSInput
dup
aload 0
invokespecial org/xbill/DNS/DNSInput/<init>([B)V
astore 4
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 0
aload 4
invokevirtual org/xbill/DNS/DNSInput/readU8()I
pop
aload 4
bipush 20
invokevirtual org/xbill/DNS/DNSInput/readByteArray(I)[B
astore 3
aload 3
iconst_0
baload
ifge L1
bipush 21
istore 1
L2:
aload 4
bipush 20
invokevirtual org/xbill/DNS/DNSInput/readByteArray(I)[B
astore 4
aload 4
iconst_0
baload
ifge L3
L4:
aload 0
bipush 48
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 0
iload 1
iload 2
iadd
iconst_4
iadd
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 0
iconst_2
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 0
iload 1
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
iload 1
bipush 20
if_icmple L5
aload 0
iconst_0
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
L5:
aload 0
aload 3
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
aload 0
iconst_2
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 0
iload 2
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
iload 2
bipush 20
if_icmple L6
aload 0
iconst_0
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
L6:
aload 0
aload 4
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
aload 0
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
areturn
L3:
bipush 20
istore 2
goto L4
L1:
bipush 20
istore 1
goto L2
.limit locals 5
.limit stack 3
.end method

.method private static DSASignaturetoDNS([BI)[B
new org/xbill/DNS/DNSInput
dup
aload 0
invokespecial org/xbill/DNS/DNSInput/<init>([B)V
astore 0
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 2
aload 2
iload 1
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 0
invokevirtual org/xbill/DNS/DNSInput/readU8()I
bipush 48
if_icmpeq L0
new java/io/IOException
dup
invokespecial java/io/IOException/<init>()V
athrow
L0:
aload 0
invokevirtual org/xbill/DNS/DNSInput/readU8()I
pop
aload 0
invokevirtual org/xbill/DNS/DNSInput/readU8()I
iconst_2
if_icmpeq L1
new java/io/IOException
dup
invokespecial java/io/IOException/<init>()V
athrow
L1:
aload 0
invokevirtual org/xbill/DNS/DNSInput/readU8()I
istore 1
iload 1
bipush 21
if_icmpne L2
aload 0
invokevirtual org/xbill/DNS/DNSInput/readU8()I
ifeq L3
new java/io/IOException
dup
invokespecial java/io/IOException/<init>()V
athrow
L2:
iload 1
bipush 20
if_icmpeq L3
new java/io/IOException
dup
invokespecial java/io/IOException/<init>()V
athrow
L3:
aload 2
aload 0
bipush 20
invokevirtual org/xbill/DNS/DNSInput/readByteArray(I)[B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
aload 0
invokevirtual org/xbill/DNS/DNSInput/readU8()I
iconst_2
if_icmpeq L4
new java/io/IOException
dup
invokespecial java/io/IOException/<init>()V
athrow
L4:
aload 0
invokevirtual org/xbill/DNS/DNSInput/readU8()I
istore 1
iload 1
bipush 21
if_icmpne L5
aload 0
invokevirtual org/xbill/DNS/DNSInput/readU8()I
ifeq L6
new java/io/IOException
dup
invokespecial java/io/IOException/<init>()V
athrow
L5:
iload 1
bipush 20
if_icmpeq L6
new java/io/IOException
dup
invokespecial java/io/IOException/<init>()V
athrow
L6:
aload 2
aload 0
bipush 20
invokevirtual org/xbill/DNS/DNSInput/readByteArray(I)[B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
aload 2
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
areturn
.limit locals 3
.limit stack 3
.end method

.method private static ECDSASignaturefromDNS([BLorg/xbill/DNS/DNSSEC$ECKeyInfo;)[B
aload 0
arraylength
aload 1
getfield org/xbill/DNS/DNSSEC$ECKeyInfo/length I
iconst_2
imul
if_icmpeq L0
new org/xbill/DNS/DNSSEC$SignatureVerificationException
dup
invokespecial org/xbill/DNS/DNSSEC$SignatureVerificationException/<init>()V
athrow
L0:
new org/xbill/DNS/DNSInput
dup
aload 0
invokespecial org/xbill/DNS/DNSInput/<init>([B)V
astore 6
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 0
aload 6
aload 1
getfield org/xbill/DNS/DNSSEC$ECKeyInfo/length I
invokevirtual org/xbill/DNS/DNSInput/readByteArray(I)[B
astore 5
aload 1
getfield org/xbill/DNS/DNSSEC$ECKeyInfo/length I
istore 3
iload 3
istore 2
aload 5
iconst_0
baload
ifge L1
iload 3
iconst_1
iadd
istore 2
L1:
aload 6
aload 1
getfield org/xbill/DNS/DNSSEC$ECKeyInfo/length I
invokevirtual org/xbill/DNS/DNSInput/readByteArray(I)[B
astore 6
aload 1
getfield org/xbill/DNS/DNSSEC$ECKeyInfo/length I
istore 4
iload 4
istore 3
aload 6
iconst_0
baload
ifge L2
iload 4
iconst_1
iadd
istore 3
L2:
aload 0
bipush 48
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 0
iload 2
iload 3
iadd
iconst_4
iadd
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 0
iconst_2
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 0
iload 2
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
iload 2
aload 1
getfield org/xbill/DNS/DNSSEC$ECKeyInfo/length I
if_icmple L3
aload 0
iconst_0
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
L3:
aload 0
aload 5
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
aload 0
iconst_2
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 0
iload 3
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
iload 3
aload 1
getfield org/xbill/DNS/DNSSEC$ECKeyInfo/length I
if_icmple L4
aload 0
iconst_0
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
L4:
aload 0
aload 6
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
aload 0
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
areturn
.limit locals 7
.limit stack 3
.end method

.method private static ECDSASignaturetoDNS([BLorg/xbill/DNS/DNSSEC$ECKeyInfo;)[B
new org/xbill/DNS/DNSInput
dup
aload 0
invokespecial org/xbill/DNS/DNSInput/<init>([B)V
astore 0
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 3
aload 0
invokevirtual org/xbill/DNS/DNSInput/readU8()I
bipush 48
if_icmpeq L0
new java/io/IOException
dup
invokespecial java/io/IOException/<init>()V
athrow
L0:
aload 0
invokevirtual org/xbill/DNS/DNSInput/readU8()I
pop
aload 0
invokevirtual org/xbill/DNS/DNSInput/readU8()I
iconst_2
if_icmpeq L1
new java/io/IOException
dup
invokespecial java/io/IOException/<init>()V
athrow
L1:
aload 0
invokevirtual org/xbill/DNS/DNSInput/readU8()I
istore 2
iload 2
aload 1
getfield org/xbill/DNS/DNSSEC$ECKeyInfo/length I
iconst_1
iadd
if_icmpne L2
aload 0
invokevirtual org/xbill/DNS/DNSInput/readU8()I
ifeq L3
new java/io/IOException
dup
invokespecial java/io/IOException/<init>()V
athrow
L2:
iload 2
aload 1
getfield org/xbill/DNS/DNSSEC$ECKeyInfo/length I
if_icmpeq L3
new java/io/IOException
dup
invokespecial java/io/IOException/<init>()V
athrow
L3:
aload 3
aload 0
aload 1
getfield org/xbill/DNS/DNSSEC$ECKeyInfo/length I
invokevirtual org/xbill/DNS/DNSInput/readByteArray(I)[B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
aload 0
invokevirtual org/xbill/DNS/DNSInput/readU8()I
iconst_2
if_icmpeq L4
new java/io/IOException
dup
invokespecial java/io/IOException/<init>()V
athrow
L4:
aload 0
invokevirtual org/xbill/DNS/DNSInput/readU8()I
istore 2
iload 2
aload 1
getfield org/xbill/DNS/DNSSEC$ECKeyInfo/length I
iconst_1
iadd
if_icmpne L5
aload 0
invokevirtual org/xbill/DNS/DNSInput/readU8()I
ifeq L6
new java/io/IOException
dup
invokespecial java/io/IOException/<init>()V
athrow
L5:
iload 2
aload 1
getfield org/xbill/DNS/DNSSEC$ECKeyInfo/length I
if_icmpeq L6
new java/io/IOException
dup
invokespecial java/io/IOException/<init>()V
athrow
L6:
aload 3
aload 0
aload 1
getfield org/xbill/DNS/DNSSEC$ECKeyInfo/length I
invokevirtual org/xbill/DNS/DNSInput/readByteArray(I)[B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
aload 3
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
areturn
.limit locals 4
.limit stack 3
.end method

.method private static ECGOSTSignaturefromDNS([BLorg/xbill/DNS/DNSSEC$ECKeyInfo;)[B
aload 0
arraylength
aload 1
getfield org/xbill/DNS/DNSSEC$ECKeyInfo/length I
iconst_2
imul
if_icmpeq L0
new org/xbill/DNS/DNSSEC$SignatureVerificationException
dup
invokespecial org/xbill/DNS/DNSSEC$SignatureVerificationException/<init>()V
athrow
L0:
aload 0
areturn
.limit locals 2
.limit stack 3
.end method

.method public static algString(I)Ljava/lang/String;
iload 0
tableswitch 1
L0
L1
L2
L1
L3
L2
L3
L4
L1
L5
L1
L6
L7
L8
default : L1
L1:
new org/xbill/DNS/DNSSEC$UnsupportedAlgorithmException
dup
iload 0
invokespecial org/xbill/DNS/DNSSEC$UnsupportedAlgorithmException/<init>(I)V
athrow
L0:
ldc "MD5withRSA"
areturn
L2:
ldc "SHA1withDSA"
areturn
L3:
ldc "SHA1withRSA"
areturn
L4:
ldc "SHA256withRSA"
areturn
L5:
ldc "SHA512withRSA"
areturn
L6:
ldc "GOST3411withECGOST3410"
areturn
L7:
ldc "SHA256withECDSA"
areturn
L8:
ldc "SHA384withECDSA"
areturn
.limit locals 1
.limit stack 3
.end method

.method static checkAlgorithm(Ljava/security/PrivateKey;I)V
iload 1
tableswitch 1
L0
L1
L2
L1
L0
L2
L0
L0
L1
L0
L1
L3
L3
L3
default : L1
L1:
new org/xbill/DNS/DNSSEC$UnsupportedAlgorithmException
dup
iload 1
invokespecial org/xbill/DNS/DNSSEC$UnsupportedAlgorithmException/<init>(I)V
athrow
L0:
aload 0
instanceof java/security/interfaces/RSAPrivateKey
ifne L4
new org/xbill/DNS/DNSSEC$IncompatibleKeyException
dup
invokespecial org/xbill/DNS/DNSSEC$IncompatibleKeyException/<init>()V
athrow
L2:
aload 0
instanceof java/security/interfaces/DSAPrivateKey
ifne L4
new org/xbill/DNS/DNSSEC$IncompatibleKeyException
dup
invokespecial org/xbill/DNS/DNSSEC$IncompatibleKeyException/<init>()V
athrow
L3:
aload 0
instanceof java/security/interfaces/ECPrivateKey
ifne L4
new org/xbill/DNS/DNSSEC$IncompatibleKeyException
dup
invokespecial org/xbill/DNS/DNSSEC$IncompatibleKeyException/<init>()V
athrow
L4:
return
.limit locals 2
.limit stack 3
.end method

.method public static digestMessage(Lorg/xbill/DNS/SIGRecord;Lorg/xbill/DNS/Message;[B)[B
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 3
aload 3
aload 0
invokestatic org/xbill/DNS/DNSSEC/digestSIG(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/SIGBase;)V
aload 2
ifnull L0
aload 3
aload 2
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
L0:
aload 1
aload 3
invokevirtual org/xbill/DNS/Message/toWire(Lorg/xbill/DNS/DNSOutput;)V
aload 3
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
areturn
.limit locals 4
.limit stack 2
.end method

.method public static digestRRset(Lorg/xbill/DNS/RRSIGRecord;Lorg/xbill/DNS/RRset;)[B
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 6
aload 6
aload 0
invokestatic org/xbill/DNS/DNSSEC/digestSIG(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/SIGBase;)V
aload 1
invokevirtual org/xbill/DNS/RRset/size()I
istore 2
iload 2
anewarray org/xbill/DNS/Record
astore 7
aload 1
invokevirtual org/xbill/DNS/RRset/rrs()Ljava/util/Iterator;
astore 9
aload 1
invokevirtual org/xbill/DNS/RRset/getName()Lorg/xbill/DNS/Name;
astore 8
aload 0
invokevirtual org/xbill/DNS/RRSIGRecord/getLabels()I
iconst_1
iadd
istore 3
aload 8
invokevirtual org/xbill/DNS/Name/labels()I
iload 3
if_icmple L0
aload 8
aload 8
invokevirtual org/xbill/DNS/Name/labels()I
iload 3
isub
invokevirtual org/xbill/DNS/Name/wild(I)Lorg/xbill/DNS/Name;
astore 5
L1:
aload 9
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
iload 2
iconst_1
isub
istore 2
aload 7
iload 2
aload 9
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast org/xbill/DNS/Record
aastore
goto L1
L2:
aload 7
invokestatic java/util/Arrays/sort([Ljava/lang/Object;)V
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 9
aload 5
ifnull L3
aload 5
aload 9
invokevirtual org/xbill/DNS/Name/toWireCanonical(Lorg/xbill/DNS/DNSOutput;)V
L4:
aload 9
aload 1
invokevirtual org/xbill/DNS/RRset/getType()I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 9
aload 1
invokevirtual org/xbill/DNS/RRset/getDClass()I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 9
aload 0
invokevirtual org/xbill/DNS/RRSIGRecord/getOrigTTL()J
invokevirtual org/xbill/DNS/DNSOutput/writeU32(J)V
iconst_0
istore 2
L5:
iload 2
aload 7
arraylength
if_icmpge L6
aload 6
aload 9
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
aload 6
invokevirtual org/xbill/DNS/DNSOutput/current()I
istore 3
aload 6
iconst_0
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 6
aload 7
iload 2
aaload
invokevirtual org/xbill/DNS/Record/rdataToWireCanonical()[B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
aload 6
invokevirtual org/xbill/DNS/DNSOutput/current()I
istore 4
aload 6
invokevirtual org/xbill/DNS/DNSOutput/save()V
aload 6
iload 3
invokevirtual org/xbill/DNS/DNSOutput/jump(I)V
aload 6
iload 4
iload 3
isub
iconst_2
isub
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 6
invokevirtual org/xbill/DNS/DNSOutput/restore()V
iload 2
iconst_1
iadd
istore 2
goto L5
L3:
aload 8
aload 9
invokevirtual org/xbill/DNS/Name/toWireCanonical(Lorg/xbill/DNS/DNSOutput;)V
goto L4
L6:
aload 6
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
areturn
L0:
aconst_null
astore 5
goto L1
.limit locals 10
.limit stack 3
.end method

.method private static digestSIG(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/SIGBase;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/SIGBase/getTypeCovered()I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 0
aload 1
invokevirtual org/xbill/DNS/SIGBase/getAlgorithm()I
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 0
aload 1
invokevirtual org/xbill/DNS/SIGBase/getLabels()I
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 0
aload 1
invokevirtual org/xbill/DNS/SIGBase/getOrigTTL()J
invokevirtual org/xbill/DNS/DNSOutput/writeU32(J)V
aload 0
aload 1
invokevirtual org/xbill/DNS/SIGBase/getExpire()Ljava/util/Date;
invokevirtual java/util/Date/getTime()J
ldc2_w 1000L
ldiv
invokevirtual org/xbill/DNS/DNSOutput/writeU32(J)V
aload 0
aload 1
invokevirtual org/xbill/DNS/SIGBase/getTimeSigned()Ljava/util/Date;
invokevirtual java/util/Date/getTime()J
ldc2_w 1000L
ldiv
invokevirtual org/xbill/DNS/DNSOutput/writeU32(J)V
aload 0
aload 1
invokevirtual org/xbill/DNS/SIGBase/getFootprint()I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 1
invokevirtual org/xbill/DNS/SIGBase/getSigner()Lorg/xbill/DNS/Name;
aload 0
invokevirtual org/xbill/DNS/Name/toWireCanonical(Lorg/xbill/DNS/DNSOutput;)V
return
.limit locals 2
.limit stack 5
.end method

.method private static fromDSAPublicKey(Ljava/security/interfaces/DSAPublicKey;)[B
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 2
aload 0
invokeinterface java/security/interfaces/DSAPublicKey/getParams()Ljava/security/interfaces/DSAParams; 0
invokeinterface java/security/interfaces/DSAParams/getQ()Ljava/math/BigInteger; 0
astore 3
aload 0
invokeinterface java/security/interfaces/DSAPublicKey/getParams()Ljava/security/interfaces/DSAParams; 0
invokeinterface java/security/interfaces/DSAParams/getP()Ljava/math/BigInteger; 0
astore 4
aload 0
invokeinterface java/security/interfaces/DSAPublicKey/getParams()Ljava/security/interfaces/DSAParams; 0
invokeinterface java/security/interfaces/DSAParams/getG()Ljava/math/BigInteger; 0
astore 5
aload 0
invokeinterface java/security/interfaces/DSAPublicKey/getY()Ljava/math/BigInteger; 0
astore 0
aload 4
invokevirtual java/math/BigInteger/toByteArray()[B
arraylength
bipush 64
isub
bipush 8
idiv
istore 1
aload 2
iload 1
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 2
aload 3
invokestatic org/xbill/DNS/DNSSEC/writeBigInteger(Lorg/xbill/DNS/DNSOutput;Ljava/math/BigInteger;)V
aload 2
aload 4
invokestatic org/xbill/DNS/DNSSEC/writeBigInteger(Lorg/xbill/DNS/DNSOutput;Ljava/math/BigInteger;)V
aload 2
aload 5
iload 1
bipush 8
imul
bipush 64
iadd
invokestatic org/xbill/DNS/DNSSEC/writePaddedBigInteger(Lorg/xbill/DNS/DNSOutput;Ljava/math/BigInteger;I)V
aload 2
aload 0
iload 1
bipush 8
imul
bipush 64
iadd
invokestatic org/xbill/DNS/DNSSEC/writePaddedBigInteger(Lorg/xbill/DNS/DNSOutput;Ljava/math/BigInteger;I)V
aload 2
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
areturn
.limit locals 6
.limit stack 4
.end method

.method private static fromECDSAPublicKey(Ljava/security/interfaces/ECPublicKey;Lorg/xbill/DNS/DNSSEC$ECKeyInfo;)[B
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 2
aload 0
invokeinterface java/security/interfaces/ECPublicKey/getW()Ljava/security/spec/ECPoint; 0
invokevirtual java/security/spec/ECPoint/getAffineX()Ljava/math/BigInteger;
astore 3
aload 0
invokeinterface java/security/interfaces/ECPublicKey/getW()Ljava/security/spec/ECPoint; 0
invokevirtual java/security/spec/ECPoint/getAffineY()Ljava/math/BigInteger;
astore 0
aload 2
aload 3
aload 1
getfield org/xbill/DNS/DNSSEC$ECKeyInfo/length I
invokestatic org/xbill/DNS/DNSSEC/writePaddedBigInteger(Lorg/xbill/DNS/DNSOutput;Ljava/math/BigInteger;I)V
aload 2
aload 0
aload 1
getfield org/xbill/DNS/DNSSEC$ECKeyInfo/length I
invokestatic org/xbill/DNS/DNSSEC/writePaddedBigInteger(Lorg/xbill/DNS/DNSOutput;Ljava/math/BigInteger;I)V
aload 2
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
areturn
.limit locals 4
.limit stack 3
.end method

.method private static fromECGOSTPublicKey(Ljava/security/interfaces/ECPublicKey;Lorg/xbill/DNS/DNSSEC$ECKeyInfo;)[B
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 2
aload 0
invokeinterface java/security/interfaces/ECPublicKey/getW()Ljava/security/spec/ECPoint; 0
invokevirtual java/security/spec/ECPoint/getAffineX()Ljava/math/BigInteger;
astore 3
aload 0
invokeinterface java/security/interfaces/ECPublicKey/getW()Ljava/security/spec/ECPoint; 0
invokevirtual java/security/spec/ECPoint/getAffineY()Ljava/math/BigInteger;
astore 0
aload 2
aload 3
aload 1
getfield org/xbill/DNS/DNSSEC$ECKeyInfo/length I
invokestatic org/xbill/DNS/DNSSEC/writePaddedBigIntegerLittleEndian(Lorg/xbill/DNS/DNSOutput;Ljava/math/BigInteger;I)V
aload 2
aload 0
aload 1
getfield org/xbill/DNS/DNSSEC$ECKeyInfo/length I
invokestatic org/xbill/DNS/DNSSEC/writePaddedBigIntegerLittleEndian(Lorg/xbill/DNS/DNSOutput;Ljava/math/BigInteger;I)V
aload 2
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
areturn
.limit locals 4
.limit stack 3
.end method

.method static fromPublicKey(Ljava/security/PublicKey;I)[B
iload 1
tableswitch 1
L0
L1
L2
L1
L0
L2
L0
L0
L1
L0
L1
L3
L4
L5
default : L1
L1:
new org/xbill/DNS/DNSSEC$UnsupportedAlgorithmException
dup
iload 1
invokespecial org/xbill/DNS/DNSSEC$UnsupportedAlgorithmException/<init>(I)V
athrow
L0:
aload 0
instanceof java/security/interfaces/RSAPublicKey
ifne L6
new org/xbill/DNS/DNSSEC$IncompatibleKeyException
dup
invokespecial org/xbill/DNS/DNSSEC$IncompatibleKeyException/<init>()V
athrow
L6:
aload 0
checkcast java/security/interfaces/RSAPublicKey
invokestatic org/xbill/DNS/DNSSEC/fromRSAPublicKey(Ljava/security/interfaces/RSAPublicKey;)[B
areturn
L2:
aload 0
instanceof java/security/interfaces/DSAPublicKey
ifne L7
new org/xbill/DNS/DNSSEC$IncompatibleKeyException
dup
invokespecial org/xbill/DNS/DNSSEC$IncompatibleKeyException/<init>()V
athrow
L7:
aload 0
checkcast java/security/interfaces/DSAPublicKey
invokestatic org/xbill/DNS/DNSSEC/fromDSAPublicKey(Ljava/security/interfaces/DSAPublicKey;)[B
areturn
L3:
aload 0
instanceof java/security/interfaces/ECPublicKey
ifne L8
new org/xbill/DNS/DNSSEC$IncompatibleKeyException
dup
invokespecial org/xbill/DNS/DNSSEC$IncompatibleKeyException/<init>()V
athrow
L8:
aload 0
checkcast java/security/interfaces/ECPublicKey
getstatic org/xbill/DNS/DNSSEC/GOST Lorg/xbill/DNS/DNSSEC$ECKeyInfo;
invokestatic org/xbill/DNS/DNSSEC/fromECGOSTPublicKey(Ljava/security/interfaces/ECPublicKey;Lorg/xbill/DNS/DNSSEC$ECKeyInfo;)[B
areturn
L4:
aload 0
instanceof java/security/interfaces/ECPublicKey
ifne L9
new org/xbill/DNS/DNSSEC$IncompatibleKeyException
dup
invokespecial org/xbill/DNS/DNSSEC$IncompatibleKeyException/<init>()V
athrow
L9:
aload 0
checkcast java/security/interfaces/ECPublicKey
getstatic org/xbill/DNS/DNSSEC/ECDSA_P256 Lorg/xbill/DNS/DNSSEC$ECKeyInfo;
invokestatic org/xbill/DNS/DNSSEC/fromECDSAPublicKey(Ljava/security/interfaces/ECPublicKey;Lorg/xbill/DNS/DNSSEC$ECKeyInfo;)[B
areturn
L5:
aload 0
instanceof java/security/interfaces/ECPublicKey
ifne L10
new org/xbill/DNS/DNSSEC$IncompatibleKeyException
dup
invokespecial org/xbill/DNS/DNSSEC$IncompatibleKeyException/<init>()V
athrow
L10:
aload 0
checkcast java/security/interfaces/ECPublicKey
getstatic org/xbill/DNS/DNSSEC/ECDSA_P384 Lorg/xbill/DNS/DNSSEC$ECKeyInfo;
invokestatic org/xbill/DNS/DNSSEC/fromECDSAPublicKey(Ljava/security/interfaces/ECPublicKey;Lorg/xbill/DNS/DNSSEC$ECKeyInfo;)[B
areturn
.limit locals 2
.limit stack 3
.end method

.method private static fromRSAPublicKey(Ljava/security/interfaces/RSAPublicKey;)[B
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 2
aload 0
invokeinterface java/security/interfaces/RSAPublicKey/getPublicExponent()Ljava/math/BigInteger; 0
astore 3
aload 0
invokeinterface java/security/interfaces/RSAPublicKey/getModulus()Ljava/math/BigInteger; 0
astore 0
aload 3
invokestatic org/xbill/DNS/DNSSEC/BigIntegerLength(Ljava/math/BigInteger;)I
istore 1
iload 1
sipush 256
if_icmpge L0
aload 2
iload 1
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
L1:
aload 2
aload 3
invokestatic org/xbill/DNS/DNSSEC/writeBigInteger(Lorg/xbill/DNS/DNSOutput;Ljava/math/BigInteger;)V
aload 2
aload 0
invokestatic org/xbill/DNS/DNSSEC/writeBigInteger(Lorg/xbill/DNS/DNSOutput;Ljava/math/BigInteger;)V
aload 2
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
areturn
L0:
aload 2
iconst_0
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 2
iload 1
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
goto L1
.limit locals 4
.limit stack 2
.end method

.method static generateDSDigest(Lorg/xbill/DNS/DNSKEYRecord;I)[B
.catch java/security/NoSuchAlgorithmException from L0 to L1 using L1
.catch java/security/NoSuchAlgorithmException from L2 to L3 using L1
.catch java/security/NoSuchAlgorithmException from L4 to L5 using L1
.catch java/security/NoSuchAlgorithmException from L6 to L7 using L1
.catch java/security/NoSuchAlgorithmException from L8 to L9 using L1
iload 1
tableswitch 1
L2
L4
L6
L8
default : L0
L0:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuffer
dup
ldc "unknown DS digest type "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
iload 1
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
astore 0
new java/lang/IllegalStateException
dup
ldc "no message digest support"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L2:
ldc "sha-1"
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
astore 2
L3:
aload 2
aload 0
invokevirtual org/xbill/DNS/DNSKEYRecord/getName()Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/toWireCanonical()[B
invokevirtual java/security/MessageDigest/update([B)V
aload 2
aload 0
invokevirtual org/xbill/DNS/DNSKEYRecord/rdataToWireCanonical()[B
invokevirtual java/security/MessageDigest/update([B)V
aload 2
invokevirtual java/security/MessageDigest/digest()[B
areturn
L4:
ldc "sha-256"
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
astore 2
L5:
goto L3
L6:
ldc "GOST3411"
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
astore 2
L7:
goto L3
L8:
ldc "sha-384"
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
astore 2
L9:
goto L3
.limit locals 3
.limit stack 5
.end method

.method private static matches(Lorg/xbill/DNS/SIGBase;Lorg/xbill/DNS/KEYBase;)Z
aload 1
invokevirtual org/xbill/DNS/KEYBase/getAlgorithm()I
aload 0
invokevirtual org/xbill/DNS/SIGBase/getAlgorithm()I
if_icmpne L0
aload 1
invokevirtual org/xbill/DNS/KEYBase/getFootprint()I
aload 0
invokevirtual org/xbill/DNS/SIGBase/getFootprint()I
if_icmpne L0
aload 1
invokevirtual org/xbill/DNS/KEYBase/getName()Lorg/xbill/DNS/Name;
aload 0
invokevirtual org/xbill/DNS/SIGBase/getSigner()Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/equals(Ljava/lang/Object;)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static readBigInteger(Lorg/xbill/DNS/DNSInput;)Ljava/math/BigInteger;
new java/math/BigInteger
dup
iconst_1
aload 0
invokevirtual org/xbill/DNS/DNSInput/readByteArray()[B
invokespecial java/math/BigInteger/<init>(I[B)V
areturn
.limit locals 1
.limit stack 4
.end method

.method private static readBigInteger(Lorg/xbill/DNS/DNSInput;I)Ljava/math/BigInteger;
new java/math/BigInteger
dup
iconst_1
aload 0
iload 1
invokevirtual org/xbill/DNS/DNSInput/readByteArray(I)[B
invokespecial java/math/BigInteger/<init>(I[B)V
areturn
.limit locals 2
.limit stack 5
.end method

.method private static readBigIntegerLittleEndian(Lorg/xbill/DNS/DNSInput;I)Ljava/math/BigInteger;
aload 0
iload 1
invokevirtual org/xbill/DNS/DNSInput/readByteArray(I)[B
astore 0
aload 0
invokestatic org/xbill/DNS/DNSSEC/reverseByteArray([B)V
new java/math/BigInteger
dup
iconst_1
aload 0
invokespecial java/math/BigInteger/<init>(I[B)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static reverseByteArray([B)V
iconst_0
istore 2
L0:
iload 2
aload 0
arraylength
iconst_2
idiv
if_icmpge L1
aload 0
arraylength
iload 2
isub
iconst_1
isub
istore 3
aload 0
iload 2
baload
istore 1
aload 0
iload 2
aload 0
iload 3
baload
bastore
aload 0
iload 3
iload 1
bastore
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 4
.limit stack 4
.end method

.method public static sign(Lorg/xbill/DNS/RRset;Lorg/xbill/DNS/DNSKEYRecord;Ljava/security/PrivateKey;Ljava/util/Date;Ljava/util/Date;)Lorg/xbill/DNS/RRSIGRecord;
aload 0
aload 1
aload 2
aload 3
aload 4
aconst_null
invokestatic org/xbill/DNS/DNSSEC/sign(Lorg/xbill/DNS/RRset;Lorg/xbill/DNS/DNSKEYRecord;Ljava/security/PrivateKey;Ljava/util/Date;Ljava/util/Date;Ljava/lang/String;)Lorg/xbill/DNS/RRSIGRecord;
areturn
.limit locals 5
.limit stack 6
.end method

.method public static sign(Lorg/xbill/DNS/RRset;Lorg/xbill/DNS/DNSKEYRecord;Ljava/security/PrivateKey;Ljava/util/Date;Ljava/util/Date;Ljava/lang/String;)Lorg/xbill/DNS/RRSIGRecord;
aload 1
invokevirtual org/xbill/DNS/DNSKEYRecord/getAlgorithm()I
istore 6
aload 2
iload 6
invokestatic org/xbill/DNS/DNSSEC/checkAlgorithm(Ljava/security/PrivateKey;I)V
new org/xbill/DNS/RRSIGRecord
dup
aload 0
invokevirtual org/xbill/DNS/RRset/getName()Lorg/xbill/DNS/Name;
aload 0
invokevirtual org/xbill/DNS/RRset/getDClass()I
aload 0
invokevirtual org/xbill/DNS/RRset/getTTL()J
aload 0
invokevirtual org/xbill/DNS/RRset/getType()I
iload 6
aload 0
invokevirtual org/xbill/DNS/RRset/getTTL()J
aload 4
aload 3
aload 1
invokevirtual org/xbill/DNS/DNSKEYRecord/getFootprint()I
aload 1
invokevirtual org/xbill/DNS/DNSKEYRecord/getName()Lorg/xbill/DNS/Name;
aconst_null
invokespecial org/xbill/DNS/RRSIGRecord/<init>(Lorg/xbill/DNS/Name;IJIIJLjava/util/Date;Ljava/util/Date;ILorg/xbill/DNS/Name;[B)V
astore 3
aload 3
aload 2
aload 1
invokevirtual org/xbill/DNS/DNSKEYRecord/getPublicKey()Ljava/security/PublicKey;
iload 6
aload 3
aload 0
invokestatic org/xbill/DNS/DNSSEC/digestRRset(Lorg/xbill/DNS/RRSIGRecord;Lorg/xbill/DNS/RRset;)[B
aload 5
invokestatic org/xbill/DNS/DNSSEC/sign(Ljava/security/PrivateKey;Ljava/security/PublicKey;I[BLjava/lang/String;)[B
invokevirtual org/xbill/DNS/RRSIGRecord/setSignature([B)V
aload 3
areturn
.limit locals 7
.limit stack 15
.end method

.method private static sign(Ljava/security/PrivateKey;Ljava/security/PublicKey;I[BLjava/lang/String;)[B
.catch java/security/GeneralSecurityException from L0 to L1 using L2
.catch java/security/GeneralSecurityException from L1 to L3 using L2
.catch java/io/IOException from L4 to L5 using L6
.catch java/security/GeneralSecurityException from L7 to L8 using L2
.catch java/io/IOException from L9 to L10 using L10
.catch java/io/IOException from L11 to L12 using L10
.catch java/io/IOException from L12 to L13 using L10
aload 4
ifnull L7
L0:
iload 2
invokestatic org/xbill/DNS/DNSSEC/algString(I)Ljava/lang/String;
aload 4
invokestatic java/security/Signature/getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/Signature;
astore 4
L1:
aload 4
aload 0
invokevirtual java/security/Signature/initSign(Ljava/security/PrivateKey;)V
aload 4
aload 3
invokevirtual java/security/Signature/update([B)V
aload 4
invokevirtual java/security/Signature/sign()[B
astore 0
L3:
aload 1
instanceof java/security/interfaces/DSAPublicKey
ifeq L14
L4:
aload 0
aload 1
checkcast java/security/interfaces/DSAPublicKey
invokeinterface java/security/interfaces/DSAPublicKey/getParams()Ljava/security/interfaces/DSAParams; 0
invokeinterface java/security/interfaces/DSAParams/getP()Ljava/math/BigInteger; 0
invokestatic org/xbill/DNS/DNSSEC/BigIntegerLength(Ljava/math/BigInteger;)I
bipush 64
isub
bipush 8
idiv
invokestatic org/xbill/DNS/DNSSEC/DSASignaturetoDNS([BI)[B
astore 3
L5:
aload 3
areturn
L7:
iload 2
invokestatic org/xbill/DNS/DNSSEC/algString(I)Ljava/lang/String;
invokestatic java/security/Signature/getInstance(Ljava/lang/String;)Ljava/security/Signature;
astore 4
L8:
goto L1
L2:
astore 0
new org/xbill/DNS/DNSSEC$DNSSECException
dup
aload 0
invokevirtual java/security/GeneralSecurityException/toString()Ljava/lang/String;
invokespecial org/xbill/DNS/DNSSEC$DNSSECException/<init>(Ljava/lang/String;)V
athrow
L6:
astore 0
new java/lang/IllegalStateException
dup
invokespecial java/lang/IllegalStateException/<init>()V
athrow
L14:
aload 0
astore 3
aload 1
instanceof java/security/interfaces/ECPublicKey
ifeq L5
aload 0
astore 3
iload 2
tableswitch 12
L5
L11
L12
default : L9
L9:
new org/xbill/DNS/DNSSEC$UnsupportedAlgorithmException
dup
iload 2
invokespecial org/xbill/DNS/DNSSEC$UnsupportedAlgorithmException/<init>(I)V
athrow
L10:
astore 0
new java/lang/IllegalStateException
dup
invokespecial java/lang/IllegalStateException/<init>()V
athrow
L11:
aload 0
getstatic org/xbill/DNS/DNSSEC/ECDSA_P256 Lorg/xbill/DNS/DNSSEC$ECKeyInfo;
invokestatic org/xbill/DNS/DNSSEC/ECDSASignaturetoDNS([BLorg/xbill/DNS/DNSSEC$ECKeyInfo;)[B
areturn
L12:
aload 0
getstatic org/xbill/DNS/DNSSEC/ECDSA_P384 Lorg/xbill/DNS/DNSSEC$ECKeyInfo;
invokestatic org/xbill/DNS/DNSSEC/ECDSASignaturetoDNS([BLorg/xbill/DNS/DNSSEC$ECKeyInfo;)[B
astore 0
L13:
aload 0
areturn
.limit locals 5
.limit stack 3
.end method

.method static signMessage(Lorg/xbill/DNS/Message;Lorg/xbill/DNS/SIGRecord;Lorg/xbill/DNS/KEYRecord;Ljava/security/PrivateKey;Ljava/util/Date;Ljava/util/Date;)Lorg/xbill/DNS/SIGRecord;
aload 2
invokevirtual org/xbill/DNS/KEYRecord/getAlgorithm()I
istore 6
aload 3
iload 6
invokestatic org/xbill/DNS/DNSSEC/checkAlgorithm(Ljava/security/PrivateKey;I)V
new org/xbill/DNS/SIGRecord
dup
getstatic org/xbill/DNS/Name/root Lorg/xbill/DNS/Name;
sipush 255
lconst_0
iconst_0
iload 6
lconst_0
aload 5
aload 4
aload 2
invokevirtual org/xbill/DNS/KEYRecord/getFootprint()I
aload 2
invokevirtual org/xbill/DNS/KEYRecord/getName()Lorg/xbill/DNS/Name;
aconst_null
invokespecial org/xbill/DNS/SIGRecord/<init>(Lorg/xbill/DNS/Name;IJIIJLjava/util/Date;Ljava/util/Date;ILorg/xbill/DNS/Name;[B)V
astore 4
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 5
aload 5
aload 4
invokestatic org/xbill/DNS/DNSSEC/digestSIG(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/SIGBase;)V
aload 1
ifnull L0
aload 5
aload 1
invokevirtual org/xbill/DNS/SIGRecord/getSignature()[B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
L0:
aload 5
aload 0
invokevirtual org/xbill/DNS/Message/toWire()[B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
aload 4
aload 3
aload 2
invokevirtual org/xbill/DNS/KEYRecord/getPublicKey()Ljava/security/PublicKey;
iload 6
aload 5
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
aconst_null
invokestatic org/xbill/DNS/DNSSEC/sign(Ljava/security/PrivateKey;Ljava/security/PublicKey;I[BLjava/lang/String;)[B
invokevirtual org/xbill/DNS/SIGRecord/setSignature([B)V
aload 4
areturn
.limit locals 7
.limit stack 15
.end method

.method private static toDSAPublicKey(Lorg/xbill/DNS/KEYBase;)Ljava/security/PublicKey;
new org/xbill/DNS/DNSInput
dup
aload 0
invokevirtual org/xbill/DNS/KEYBase/getKey()[B
invokespecial org/xbill/DNS/DNSInput/<init>([B)V
astore 2
aload 2
invokevirtual org/xbill/DNS/DNSInput/readU8()I
istore 1
iload 1
bipush 8
if_icmple L0
new org/xbill/DNS/DNSSEC$MalformedKeyException
dup
aload 0
invokespecial org/xbill/DNS/DNSSEC$MalformedKeyException/<init>(Lorg/xbill/DNS/KEYBase;)V
athrow
L0:
aload 2
bipush 20
invokestatic org/xbill/DNS/DNSSEC/readBigInteger(Lorg/xbill/DNS/DNSInput;I)Ljava/math/BigInteger;
astore 0
aload 2
iload 1
bipush 8
imul
bipush 64
iadd
invokestatic org/xbill/DNS/DNSSEC/readBigInteger(Lorg/xbill/DNS/DNSInput;I)Ljava/math/BigInteger;
astore 3
aload 2
iload 1
bipush 8
imul
bipush 64
iadd
invokestatic org/xbill/DNS/DNSSEC/readBigInteger(Lorg/xbill/DNS/DNSInput;I)Ljava/math/BigInteger;
astore 4
aload 2
iload 1
bipush 8
imul
bipush 64
iadd
invokestatic org/xbill/DNS/DNSSEC/readBigInteger(Lorg/xbill/DNS/DNSInput;I)Ljava/math/BigInteger;
astore 2
ldc "DSA"
invokestatic java/security/KeyFactory/getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;
new java/security/spec/DSAPublicKeySpec
dup
aload 2
aload 3
aload 0
aload 4
invokespecial java/security/spec/DSAPublicKeySpec/<init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V
invokevirtual java/security/KeyFactory/generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;
areturn
.limit locals 5
.limit stack 7
.end method

.method private static toECDSAPublicKey(Lorg/xbill/DNS/KEYBase;Lorg/xbill/DNS/DNSSEC$ECKeyInfo;)Ljava/security/PublicKey;
new org/xbill/DNS/DNSInput
dup
aload 0
invokevirtual org/xbill/DNS/KEYBase/getKey()[B
invokespecial org/xbill/DNS/DNSInput/<init>([B)V
astore 0
new java/security/spec/ECPoint
dup
aload 0
aload 1
getfield org/xbill/DNS/DNSSEC$ECKeyInfo/length I
invokestatic org/xbill/DNS/DNSSEC/readBigInteger(Lorg/xbill/DNS/DNSInput;I)Ljava/math/BigInteger;
aload 0
aload 1
getfield org/xbill/DNS/DNSSEC$ECKeyInfo/length I
invokestatic org/xbill/DNS/DNSSEC/readBigInteger(Lorg/xbill/DNS/DNSInput;I)Ljava/math/BigInteger;
invokespecial java/security/spec/ECPoint/<init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V
astore 0
ldc "EC"
invokestatic java/security/KeyFactory/getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;
new java/security/spec/ECPublicKeySpec
dup
aload 0
aload 1
getfield org/xbill/DNS/DNSSEC$ECKeyInfo/spec Ljava/security/spec/ECParameterSpec;
invokespecial java/security/spec/ECPublicKeySpec/<init>(Ljava/security/spec/ECPoint;Ljava/security/spec/ECParameterSpec;)V
invokevirtual java/security/KeyFactory/generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;
areturn
.limit locals 2
.limit stack 5
.end method

.method private static toECGOSTPublicKey(Lorg/xbill/DNS/KEYBase;Lorg/xbill/DNS/DNSSEC$ECKeyInfo;)Ljava/security/PublicKey;
new org/xbill/DNS/DNSInput
dup
aload 0
invokevirtual org/xbill/DNS/KEYBase/getKey()[B
invokespecial org/xbill/DNS/DNSInput/<init>([B)V
astore 0
new java/security/spec/ECPoint
dup
aload 0
aload 1
getfield org/xbill/DNS/DNSSEC$ECKeyInfo/length I
invokestatic org/xbill/DNS/DNSSEC/readBigIntegerLittleEndian(Lorg/xbill/DNS/DNSInput;I)Ljava/math/BigInteger;
aload 0
aload 1
getfield org/xbill/DNS/DNSSEC$ECKeyInfo/length I
invokestatic org/xbill/DNS/DNSSEC/readBigIntegerLittleEndian(Lorg/xbill/DNS/DNSInput;I)Ljava/math/BigInteger;
invokespecial java/security/spec/ECPoint/<init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V
astore 0
ldc "ECGOST3410"
invokestatic java/security/KeyFactory/getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;
new java/security/spec/ECPublicKeySpec
dup
aload 0
aload 1
getfield org/xbill/DNS/DNSSEC$ECKeyInfo/spec Ljava/security/spec/ECParameterSpec;
invokespecial java/security/spec/ECPublicKeySpec/<init>(Ljava/security/spec/ECPoint;Ljava/security/spec/ECParameterSpec;)V
invokevirtual java/security/KeyFactory/generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;
areturn
.limit locals 2
.limit stack 5
.end method

.method static toPublicKey(Lorg/xbill/DNS/KEYBase;)Ljava/security/PublicKey;
.catch java/io/IOException from L0 to L1 using L1
.catch java/security/GeneralSecurityException from L0 to L1 using L2
.catch java/io/IOException from L3 to L4 using L1
.catch java/security/GeneralSecurityException from L3 to L4 using L2
.catch java/io/IOException from L4 to L5 using L1
.catch java/security/GeneralSecurityException from L4 to L5 using L2
.catch java/io/IOException from L5 to L6 using L1
.catch java/security/GeneralSecurityException from L5 to L6 using L2
.catch java/io/IOException from L6 to L7 using L1
.catch java/security/GeneralSecurityException from L6 to L7 using L2
.catch java/io/IOException from L7 to L8 using L1
.catch java/security/GeneralSecurityException from L7 to L8 using L2
aload 0
invokevirtual org/xbill/DNS/KEYBase/getAlgorithm()I
istore 1
iload 1
tableswitch 1
L3
L0
L4
L0
L3
L4
L3
L3
L0
L3
L0
L5
L6
L7
default : L0
L0:
new org/xbill/DNS/DNSSEC$UnsupportedAlgorithmException
dup
iload 1
invokespecial org/xbill/DNS/DNSSEC$UnsupportedAlgorithmException/<init>(I)V
athrow
L1:
astore 2
new org/xbill/DNS/DNSSEC$MalformedKeyException
dup
aload 0
invokespecial org/xbill/DNS/DNSSEC$MalformedKeyException/<init>(Lorg/xbill/DNS/KEYBase;)V
athrow
L3:
aload 0
invokestatic org/xbill/DNS/DNSSEC/toRSAPublicKey(Lorg/xbill/DNS/KEYBase;)Ljava/security/PublicKey;
areturn
L4:
aload 0
invokestatic org/xbill/DNS/DNSSEC/toDSAPublicKey(Lorg/xbill/DNS/KEYBase;)Ljava/security/PublicKey;
areturn
L5:
aload 0
getstatic org/xbill/DNS/DNSSEC/GOST Lorg/xbill/DNS/DNSSEC$ECKeyInfo;
invokestatic org/xbill/DNS/DNSSEC/toECGOSTPublicKey(Lorg/xbill/DNS/KEYBase;Lorg/xbill/DNS/DNSSEC$ECKeyInfo;)Ljava/security/PublicKey;
areturn
L6:
aload 0
getstatic org/xbill/DNS/DNSSEC/ECDSA_P256 Lorg/xbill/DNS/DNSSEC$ECKeyInfo;
invokestatic org/xbill/DNS/DNSSEC/toECDSAPublicKey(Lorg/xbill/DNS/KEYBase;Lorg/xbill/DNS/DNSSEC$ECKeyInfo;)Ljava/security/PublicKey;
areturn
L7:
aload 0
getstatic org/xbill/DNS/DNSSEC/ECDSA_P384 Lorg/xbill/DNS/DNSSEC$ECKeyInfo;
invokestatic org/xbill/DNS/DNSSEC/toECDSAPublicKey(Lorg/xbill/DNS/KEYBase;Lorg/xbill/DNS/DNSSEC$ECKeyInfo;)Ljava/security/PublicKey;
astore 2
L8:
aload 2
areturn
L2:
astore 0
new org/xbill/DNS/DNSSEC$DNSSECException
dup
aload 0
invokevirtual java/security/GeneralSecurityException/toString()Ljava/lang/String;
invokespecial org/xbill/DNS/DNSSEC$DNSSECException/<init>(Ljava/lang/String;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method private static toRSAPublicKey(Lorg/xbill/DNS/KEYBase;)Ljava/security/PublicKey;
new org/xbill/DNS/DNSInput
dup
aload 0
invokevirtual org/xbill/DNS/KEYBase/getKey()[B
invokespecial org/xbill/DNS/DNSInput/<init>([B)V
astore 3
aload 3
invokevirtual org/xbill/DNS/DNSInput/readU8()I
istore 2
iload 2
istore 1
iload 2
ifne L0
aload 3
invokevirtual org/xbill/DNS/DNSInput/readU16()I
istore 1
L0:
aload 3
iload 1
invokestatic org/xbill/DNS/DNSSEC/readBigInteger(Lorg/xbill/DNS/DNSInput;I)Ljava/math/BigInteger;
astore 0
aload 3
invokestatic org/xbill/DNS/DNSSEC/readBigInteger(Lorg/xbill/DNS/DNSInput;)Ljava/math/BigInteger;
astore 3
ldc "RSA"
invokestatic java/security/KeyFactory/getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;
new java/security/spec/RSAPublicKeySpec
dup
aload 3
aload 0
invokespecial java/security/spec/RSAPublicKeySpec/<init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V
invokevirtual java/security/KeyFactory/generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;
areturn
.limit locals 4
.limit stack 5
.end method

.method private static trimByteArray([B)[B
aload 0
astore 1
aload 0
iconst_0
baload
ifne L0
aload 0
arraylength
iconst_1
isub
newarray byte
astore 1
aload 0
iconst_1
aload 1
iconst_0
aload 0
arraylength
iconst_1
isub
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L0:
aload 1
areturn
.limit locals 2
.limit stack 6
.end method

.method private static verify(Ljava/security/PublicKey;I[B[B)V
.catch java/io/IOException from L0 to L1 using L2
.catch java/security/GeneralSecurityException from L1 to L3 using L3
.catch java/io/IOException from L4 to L5 using L5
.catch java/io/IOException from L6 to L7 using L5
.catch java/io/IOException from L8 to L9 using L5
.catch java/io/IOException from L10 to L11 using L5
aload 0
instanceof java/security/interfaces/DSAPublicKey
ifeq L12
L0:
aload 3
invokestatic org/xbill/DNS/DNSSEC/DSASignaturefromDNS([B)[B
astore 4
L1:
iload 1
invokestatic org/xbill/DNS/DNSSEC/algString(I)Ljava/lang/String;
invokestatic java/security/Signature/getInstance(Ljava/lang/String;)Ljava/security/Signature;
astore 3
aload 3
aload 0
invokevirtual java/security/Signature/initVerify(Ljava/security/PublicKey;)V
aload 3
aload 2
invokevirtual java/security/Signature/update([B)V
aload 3
aload 4
invokevirtual java/security/Signature/verify([B)Z
ifne L13
new org/xbill/DNS/DNSSEC$SignatureVerificationException
dup
invokespecial org/xbill/DNS/DNSSEC$SignatureVerificationException/<init>()V
athrow
L3:
astore 0
new org/xbill/DNS/DNSSEC$DNSSECException
dup
aload 0
invokevirtual java/security/GeneralSecurityException/toString()Ljava/lang/String;
invokespecial org/xbill/DNS/DNSSEC$DNSSECException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 0
new java/lang/IllegalStateException
dup
invokespecial java/lang/IllegalStateException/<init>()V
athrow
L12:
aload 3
astore 4
aload 0
instanceof java/security/interfaces/ECPublicKey
ifeq L1
iload 1
tableswitch 12
L6
L8
L10
default : L4
L4:
new org/xbill/DNS/DNSSEC$UnsupportedAlgorithmException
dup
iload 1
invokespecial org/xbill/DNS/DNSSEC$UnsupportedAlgorithmException/<init>(I)V
athrow
L5:
astore 0
new java/lang/IllegalStateException
dup
invokespecial java/lang/IllegalStateException/<init>()V
athrow
L6:
aload 3
getstatic org/xbill/DNS/DNSSEC/GOST Lorg/xbill/DNS/DNSSEC$ECKeyInfo;
invokestatic org/xbill/DNS/DNSSEC/ECGOSTSignaturefromDNS([BLorg/xbill/DNS/DNSSEC$ECKeyInfo;)[B
astore 4
L7:
goto L1
L8:
aload 3
getstatic org/xbill/DNS/DNSSEC/ECDSA_P256 Lorg/xbill/DNS/DNSSEC$ECKeyInfo;
invokestatic org/xbill/DNS/DNSSEC/ECDSASignaturefromDNS([BLorg/xbill/DNS/DNSSEC$ECKeyInfo;)[B
astore 4
L9:
goto L1
L10:
aload 3
getstatic org/xbill/DNS/DNSSEC/ECDSA_P384 Lorg/xbill/DNS/DNSSEC$ECKeyInfo;
invokestatic org/xbill/DNS/DNSSEC/ECDSASignaturefromDNS([BLorg/xbill/DNS/DNSSEC$ECKeyInfo;)[B
astore 4
L11:
goto L1
L13:
return
.limit locals 5
.limit stack 3
.end method

.method public static verify(Lorg/xbill/DNS/RRset;Lorg/xbill/DNS/RRSIGRecord;Lorg/xbill/DNS/DNSKEYRecord;)V
aload 1
aload 2
invokestatic org/xbill/DNS/DNSSEC/matches(Lorg/xbill/DNS/SIGBase;Lorg/xbill/DNS/KEYBase;)Z
ifne L0
new org/xbill/DNS/DNSSEC$KeyMismatchException
dup
aload 2
aload 1
invokespecial org/xbill/DNS/DNSSEC$KeyMismatchException/<init>(Lorg/xbill/DNS/KEYBase;Lorg/xbill/DNS/SIGBase;)V
athrow
L0:
new java/util/Date
dup
invokespecial java/util/Date/<init>()V
astore 3
aload 3
aload 1
invokevirtual org/xbill/DNS/RRSIGRecord/getExpire()Ljava/util/Date;
invokevirtual java/util/Date/compareTo(Ljava/util/Date;)I
ifle L1
new org/xbill/DNS/DNSSEC$SignatureExpiredException
dup
aload 1
invokevirtual org/xbill/DNS/RRSIGRecord/getExpire()Ljava/util/Date;
aload 3
invokespecial org/xbill/DNS/DNSSEC$SignatureExpiredException/<init>(Ljava/util/Date;Ljava/util/Date;)V
athrow
L1:
aload 3
aload 1
invokevirtual org/xbill/DNS/RRSIGRecord/getTimeSigned()Ljava/util/Date;
invokevirtual java/util/Date/compareTo(Ljava/util/Date;)I
ifge L2
new org/xbill/DNS/DNSSEC$SignatureNotYetValidException
dup
aload 1
invokevirtual org/xbill/DNS/RRSIGRecord/getTimeSigned()Ljava/util/Date;
aload 3
invokespecial org/xbill/DNS/DNSSEC$SignatureNotYetValidException/<init>(Ljava/util/Date;Ljava/util/Date;)V
athrow
L2:
aload 2
invokevirtual org/xbill/DNS/DNSKEYRecord/getPublicKey()Ljava/security/PublicKey;
aload 1
invokevirtual org/xbill/DNS/RRSIGRecord/getAlgorithm()I
aload 1
aload 0
invokestatic org/xbill/DNS/DNSSEC/digestRRset(Lorg/xbill/DNS/RRSIGRecord;Lorg/xbill/DNS/RRset;)[B
aload 1
invokevirtual org/xbill/DNS/RRSIGRecord/getSignature()[B
invokestatic org/xbill/DNS/DNSSEC/verify(Ljava/security/PublicKey;I[B[B)V
return
.limit locals 4
.limit stack 4
.end method

.method static verifyMessage(Lorg/xbill/DNS/Message;[BLorg/xbill/DNS/SIGRecord;Lorg/xbill/DNS/SIGRecord;Lorg/xbill/DNS/KEYRecord;)V
aload 0
getfield org/xbill/DNS/Message/sig0start I
ifne L0
new org/xbill/DNS/DNSSEC$NoSignatureException
dup
invokespecial org/xbill/DNS/DNSSEC$NoSignatureException/<init>()V
athrow
L0:
aload 2
aload 4
invokestatic org/xbill/DNS/DNSSEC/matches(Lorg/xbill/DNS/SIGBase;Lorg/xbill/DNS/KEYBase;)Z
ifne L1
new org/xbill/DNS/DNSSEC$KeyMismatchException
dup
aload 4
aload 2
invokespecial org/xbill/DNS/DNSSEC$KeyMismatchException/<init>(Lorg/xbill/DNS/KEYBase;Lorg/xbill/DNS/SIGBase;)V
athrow
L1:
new java/util/Date
dup
invokespecial java/util/Date/<init>()V
astore 5
aload 5
aload 2
invokevirtual org/xbill/DNS/SIGRecord/getExpire()Ljava/util/Date;
invokevirtual java/util/Date/compareTo(Ljava/util/Date;)I
ifle L2
new org/xbill/DNS/DNSSEC$SignatureExpiredException
dup
aload 2
invokevirtual org/xbill/DNS/SIGRecord/getExpire()Ljava/util/Date;
aload 5
invokespecial org/xbill/DNS/DNSSEC$SignatureExpiredException/<init>(Ljava/util/Date;Ljava/util/Date;)V
athrow
L2:
aload 5
aload 2
invokevirtual org/xbill/DNS/SIGRecord/getTimeSigned()Ljava/util/Date;
invokevirtual java/util/Date/compareTo(Ljava/util/Date;)I
ifge L3
new org/xbill/DNS/DNSSEC$SignatureNotYetValidException
dup
aload 2
invokevirtual org/xbill/DNS/SIGRecord/getTimeSigned()Ljava/util/Date;
aload 5
invokespecial org/xbill/DNS/DNSSEC$SignatureNotYetValidException/<init>(Ljava/util/Date;Ljava/util/Date;)V
athrow
L3:
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 5
aload 5
aload 2
invokestatic org/xbill/DNS/DNSSEC/digestSIG(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/SIGBase;)V
aload 3
ifnull L4
aload 5
aload 3
invokevirtual org/xbill/DNS/SIGRecord/getSignature()[B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
L4:
aload 0
invokevirtual org/xbill/DNS/Message/getHeader()Lorg/xbill/DNS/Header;
invokevirtual org/xbill/DNS/Header/clone()Ljava/lang/Object;
checkcast org/xbill/DNS/Header
astore 3
aload 3
iconst_3
invokevirtual org/xbill/DNS/Header/decCount(I)V
aload 5
aload 3
invokevirtual org/xbill/DNS/Header/toWire()[B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
aload 5
aload 1
bipush 12
aload 0
getfield org/xbill/DNS/Message/sig0start I
bipush 12
isub
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([BII)V
aload 4
invokevirtual org/xbill/DNS/KEYRecord/getPublicKey()Ljava/security/PublicKey;
aload 2
invokevirtual org/xbill/DNS/SIGRecord/getAlgorithm()I
aload 5
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
aload 2
invokevirtual org/xbill/DNS/SIGRecord/getSignature()[B
invokestatic org/xbill/DNS/DNSSEC/verify(Ljava/security/PublicKey;I[B[B)V
return
.limit locals 6
.limit stack 5
.end method

.method private static writeBigInteger(Lorg/xbill/DNS/DNSOutput;Ljava/math/BigInteger;)V
aload 0
aload 1
invokevirtual java/math/BigInteger/toByteArray()[B
invokestatic org/xbill/DNS/DNSSEC/trimByteArray([B)[B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
return
.limit locals 2
.limit stack 2
.end method

.method private static writePaddedBigInteger(Lorg/xbill/DNS/DNSOutput;Ljava/math/BigInteger;I)V
aload 1
invokevirtual java/math/BigInteger/toByteArray()[B
invokestatic org/xbill/DNS/DNSSEC/trimByteArray([B)[B
astore 1
aload 1
arraylength
iload 2
if_icmple L0
new java/lang/IllegalArgumentException
dup
invokespecial java/lang/IllegalArgumentException/<init>()V
athrow
L0:
aload 1
arraylength
iload 2
if_icmpge L1
aload 0
iload 2
aload 1
arraylength
isub
newarray byte
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
L1:
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
return
.limit locals 3
.limit stack 3
.end method

.method private static writePaddedBigIntegerLittleEndian(Lorg/xbill/DNS/DNSOutput;Ljava/math/BigInteger;I)V
aload 1
invokevirtual java/math/BigInteger/toByteArray()[B
invokestatic org/xbill/DNS/DNSSEC/trimByteArray([B)[B
astore 1
aload 1
arraylength
iload 2
if_icmple L0
new java/lang/IllegalArgumentException
dup
invokespecial java/lang/IllegalArgumentException/<init>()V
athrow
L0:
aload 1
invokestatic org/xbill/DNS/DNSSEC/reverseByteArray([B)V
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
aload 1
arraylength
iload 2
if_icmpge L1
aload 0
iload 2
aload 1
arraylength
isub
newarray byte
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
L1:
return
.limit locals 3
.limit stack 3
.end method
