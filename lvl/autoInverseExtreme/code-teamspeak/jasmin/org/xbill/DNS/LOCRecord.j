.bytecode 50.0
.class public synchronized org/xbill/DNS/LOCRecord
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = 9058224788126750409L


.field private static 'w2' Ljava/text/NumberFormat;

.field private static 'w3' Ljava/text/NumberFormat;

.field private 'altitude' J

.field private 'hPrecision' J

.field private 'latitude' J

.field private 'longitude' J

.field private 'size' J

.field private 'vPrecision' J

.method static <clinit>()V
new java/text/DecimalFormat
dup
invokespecial java/text/DecimalFormat/<init>()V
astore 0
aload 0
putstatic org/xbill/DNS/LOCRecord/w2 Ljava/text/NumberFormat;
aload 0
iconst_2
invokevirtual java/text/NumberFormat/setMinimumIntegerDigits(I)V
new java/text/DecimalFormat
dup
invokespecial java/text/DecimalFormat/<init>()V
astore 0
aload 0
putstatic org/xbill/DNS/LOCRecord/w3 Ljava/text/NumberFormat;
aload 0
iconst_3
invokevirtual java/text/NumberFormat/setMinimumIntegerDigits(I)V
return
.limit locals 1
.limit stack 2
.end method

.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJDDDDDD)V
aload 0
aload 1
bipush 29
iload 2
lload 3
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
aload 0
ldc2_w 3600.0D
dload 5
dmul
ldc2_w 1000.0D
dmul
ldc2_w 2.147483648E9D
dadd
d2l
putfield org/xbill/DNS/LOCRecord/latitude J
aload 0
ldc2_w 3600.0D
dload 7
dmul
ldc2_w 1000.0D
dmul
ldc2_w 2.147483648E9D
dadd
d2l
putfield org/xbill/DNS/LOCRecord/longitude J
aload 0
ldc2_w 100000.0D
dload 9
dadd
ldc2_w 100.0D
dmul
d2l
putfield org/xbill/DNS/LOCRecord/altitude J
aload 0
ldc2_w 100.0D
dload 11
dmul
d2l
putfield org/xbill/DNS/LOCRecord/size J
aload 0
ldc2_w 100.0D
dload 13
dmul
d2l
putfield org/xbill/DNS/LOCRecord/hPrecision J
aload 0
ldc2_w 100.0D
dload 15
dmul
d2l
putfield org/xbill/DNS/LOCRecord/vPrecision J
return
.limit locals 17
.limit stack 6
.end method

.method private parseDouble(Lorg/xbill/DNS/Tokenizer;Ljava/lang/String;ZJJJ)J
.catch java/lang/NumberFormatException from L0 to L1 using L2
.catch java/lang/NumberFormatException from L3 to L2 using L2
aload 1
invokevirtual org/xbill/DNS/Tokenizer/get()Lorg/xbill/DNS/Tokenizer$Token;
astore 12
aload 12
invokevirtual org/xbill/DNS/Tokenizer$Token/isEOL()Z
ifeq L4
iload 3
ifeq L5
aload 1
new java/lang/StringBuffer
dup
ldc "Invalid LOC "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L5:
aload 1
invokevirtual org/xbill/DNS/Tokenizer/unget()V
L6:
lload 8
lreturn
L4:
aload 12
getfield org/xbill/DNS/Tokenizer$Token/value Ljava/lang/String;
astore 13
aload 13
astore 12
aload 13
invokevirtual java/lang/String/length()I
iconst_1
if_icmple L0
aload 13
astore 12
aload 13
aload 13
invokevirtual java/lang/String/length()I
iconst_1
isub
invokevirtual java/lang/String/charAt(I)C
bipush 109
if_icmpne L0
aload 13
iconst_0
aload 13
invokevirtual java/lang/String/length()I
iconst_1
isub
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
astore 12
L0:
aload 0
aload 12
invokespecial org/xbill/DNS/LOCRecord/parseFixedPoint(Ljava/lang/String;)D
ldc2_w 100.0D
dmul
d2l
lstore 10
L1:
lload 10
lload 4
lcmp
iflt L3
lload 10
lstore 8
lload 10
lload 6
lcmp
ifle L6
L3:
aload 1
new java/lang/StringBuffer
dup
ldc "Invalid LOC "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L2:
astore 12
aload 1
new java/lang/StringBuffer
dup
ldc "Invalid LOC "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
.limit locals 14
.limit stack 4
.end method

.method private parseFixedPoint(Ljava/lang/String;)D
aload 1
ldc "^-?\\d+$"
invokevirtual java/lang/String/matches(Ljava/lang/String;)Z
ifeq L0
aload 1
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
i2d
dreturn
L0:
aload 1
ldc "^-?\\d+\\.\\d*$"
invokevirtual java/lang/String/matches(Ljava/lang/String;)Z
ifeq L1
aload 1
ldc "\\."
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 1
aload 1
iconst_0
aaload
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
i2d
dstore 6
aload 1
iconst_1
aaload
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
i2d
dstore 4
dload 4
dstore 2
dload 6
dconst_0
dcmpg
ifge L2
dload 4
ldc2_w -1.0D
dmul
dstore 2
L2:
dload 2
ldc2_w 10.0D
aload 1
iconst_1
aaload
invokevirtual java/lang/String/length()I
i2d
invokestatic java/lang/Math/pow(DD)D
ddiv
dload 6
dadd
dreturn
L1:
new java/lang/NumberFormatException
dup
invokespecial java/lang/NumberFormatException/<init>()V
athrow
.limit locals 8
.limit stack 6
.end method

.method private static parseLOCformat(I)J
iload 0
iconst_4
ishr
i2l
lstore 2
iload 0
bipush 15
iand
istore 1
lload 2
ldc2_w 9L
lcmp
ifgt L0
iload 1
istore 0
iload 1
bipush 9
if_icmple L1
L0:
new org/xbill/DNS/WireParseException
dup
ldc "Invalid LOC Encoding"
invokespecial org/xbill/DNS/WireParseException/<init>(Ljava/lang/String;)V
athrow
L1:
iload 0
ifle L2
lload 2
ldc2_w 10L
lmul
lstore 2
iload 0
iconst_1
isub
istore 0
goto L1
L2:
lload 2
lreturn
.limit locals 4
.limit stack 4
.end method

.method private parsePosition(Lorg/xbill/DNS/Tokenizer;Ljava/lang/String;)J
.catch java/lang/NumberFormatException from L0 to L1 using L2
.catch java/lang/NumberFormatException from L3 to L2 using L2
.catch java/lang/NumberFormatException from L4 to L5 using L2
.catch java/lang/NumberFormatException from L6 to L7 using L2
.catch java/lang/NumberFormatException from L8 to L9 using L2
.catch java/lang/NumberFormatException from L10 to L11 using L2
aload 2
ldc "latitude"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
istore 10
iconst_0
istore 7
dconst_0
dstore 5
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt16()I
istore 9
iload 9
sipush 180
if_icmpgt L12
iload 9
bipush 90
if_icmple L13
iload 10
ifeq L13
L12:
aload 1
new java/lang/StringBuffer
dup
ldc "Invalid LOC "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc " degrees"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L13:
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
astore 14
aload 14
astore 13
dload 5
dstore 3
L0:
aload 14
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
istore 8
L1:
iload 8
iflt L14
iload 8
bipush 59
if_icmple L15
L14:
aload 14
astore 13
iload 8
istore 7
dload 5
dstore 3
L3:
aload 1
new java/lang/StringBuffer
dup
ldc "Invalid LOC "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc " minutes"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L2:
astore 14
L16:
aload 13
invokevirtual java/lang/String/length()I
iconst_1
if_icmpeq L17
aload 1
new java/lang/StringBuffer
dup
ldc "Invalid LOC "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L15:
aload 14
astore 13
iload 8
istore 7
dload 5
dstore 3
L4:
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
astore 14
L5:
aload 14
astore 13
iload 8
istore 7
dload 5
dstore 3
L6:
aload 0
aload 14
invokespecial org/xbill/DNS/LOCRecord/parseFixedPoint(Ljava/lang/String;)D
dstore 5
L7:
dload 5
dconst_0
dcmpg
iflt L18
dload 5
ldc2_w 60.0D
dcmpl
iflt L9
L18:
aload 14
astore 13
iload 8
istore 7
dload 5
dstore 3
L8:
aload 1
new java/lang/StringBuffer
dup
ldc "Invalid LOC "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc " seconds"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L9:
aload 14
astore 13
iload 8
istore 7
dload 5
dstore 3
L10:
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
astore 14
L11:
aload 14
astore 13
iload 8
istore 7
dload 5
dstore 3
goto L16
L17:
dload 3
ldc2_w 60L
iload 7
i2l
ldc2_w 60L
iload 9
i2l
lmul
ladd
lmul
l2d
dadd
ldc2_w 1000.0D
dmul
d2l
lstore 11
aload 13
iconst_0
invokevirtual java/lang/String/charAt(I)C
invokestatic java/lang/Character/toUpperCase(C)C
istore 7
iload 10
ifeq L19
iload 7
bipush 83
if_icmpeq L20
L19:
iload 10
ifne L21
iload 7
bipush 87
if_icmpne L21
L20:
lload 11
lneg
lstore 11
L22:
lload 11
ldc2_w 2147483648L
ladd
lreturn
L21:
iload 10
ifeq L23
iload 7
bipush 78
if_icmpne L24
L23:
iload 10
ifne L25
iload 7
bipush 69
if_icmpeq L25
L24:
aload 1
new java/lang/StringBuffer
dup
ldc "Invalid LOC "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L25:
goto L22
.limit locals 15
.limit stack 10
.end method

.method private positionToString(JCC)Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 5
lload 1
ldc2_w 2147483648L
lsub
lstore 1
lload 1
lconst_0
lcmp
ifge L0
lload 1
lneg
lstore 1
L1:
aload 5
lload 1
ldc2_w 3600000L
ldiv
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
pop
lload 1
ldc2_w 3600000L
lrem
lstore 1
aload 5
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 5
lload 1
ldc2_w 60000L
ldiv
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
pop
aload 5
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 0
aload 5
getstatic org/xbill/DNS/LOCRecord/w3 Ljava/text/NumberFormat;
lload 1
ldc2_w 60000L
lrem
ldc2_w 1000L
invokespecial org/xbill/DNS/LOCRecord/renderFixedPoint(Ljava/lang/StringBuffer;Ljava/text/NumberFormat;JJ)V
aload 5
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 5
iload 4
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
aload 5
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
L0:
iload 3
istore 4
goto L1
.limit locals 6
.limit stack 7
.end method

.method private renderFixedPoint(Ljava/lang/StringBuffer;Ljava/text/NumberFormat;JJ)V
aload 1
lload 3
lload 5
ldiv
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
pop
lload 3
lload 5
lrem
lstore 3
lload 3
lconst_0
lcmp
ifeq L0
aload 1
ldc "."
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 2
lload 3
invokevirtual java/text/NumberFormat/format(J)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L0:
return
.limit locals 7
.limit stack 5
.end method

.method private toLOCformat(J)I
iconst_0
istore 3
L0:
lload 1
ldc2_w 9L
lcmp
ifle L1
iload 3
iconst_1
iadd
i2b
istore 3
lload 1
ldc2_w 10L
ldiv
lstore 1
goto L0
L1:
iload 3
i2l
lload 1
iconst_4
lshl
ladd
l2i
ireturn
.limit locals 4
.limit stack 5
.end method

.method public getAltitude()D
aload 0
getfield org/xbill/DNS/LOCRecord/altitude J
ldc2_w 10000000L
lsub
l2d
ldc2_w 100.0D
ddiv
dreturn
.limit locals 1
.limit stack 4
.end method

.method public getHPrecision()D
aload 0
getfield org/xbill/DNS/LOCRecord/hPrecision J
l2d
ldc2_w 100.0D
ddiv
dreturn
.limit locals 1
.limit stack 4
.end method

.method public getLatitude()D
aload 0
getfield org/xbill/DNS/LOCRecord/latitude J
ldc2_w 2147483648L
lsub
l2d
ldc2_w 3600000.0D
ddiv
dreturn
.limit locals 1
.limit stack 4
.end method

.method public getLongitude()D
aload 0
getfield org/xbill/DNS/LOCRecord/longitude J
ldc2_w 2147483648L
lsub
l2d
ldc2_w 3600000.0D
ddiv
dreturn
.limit locals 1
.limit stack 4
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/LOCRecord
dup
invokespecial org/xbill/DNS/LOCRecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method public getSize()D
aload 0
getfield org/xbill/DNS/LOCRecord/size J
l2d
ldc2_w 100.0D
ddiv
dreturn
.limit locals 1
.limit stack 4
.end method

.method public getVPrecision()D
aload 0
getfield org/xbill/DNS/LOCRecord/vPrecision J
l2d
ldc2_w 100.0D
ddiv
dreturn
.limit locals 1
.limit stack 4
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
aload 0
aload 0
aload 1
ldc "latitude"
invokespecial org/xbill/DNS/LOCRecord/parsePosition(Lorg/xbill/DNS/Tokenizer;Ljava/lang/String;)J
putfield org/xbill/DNS/LOCRecord/latitude J
aload 0
aload 0
aload 1
ldc "longitude"
invokespecial org/xbill/DNS/LOCRecord/parsePosition(Lorg/xbill/DNS/Tokenizer;Ljava/lang/String;)J
putfield org/xbill/DNS/LOCRecord/longitude J
aload 0
aload 0
aload 1
ldc "altitude"
iconst_1
ldc2_w -10000000L
ldc2_w 4284967295L
lconst_0
invokespecial org/xbill/DNS/LOCRecord/parseDouble(Lorg/xbill/DNS/Tokenizer;Ljava/lang/String;ZJJJ)J
ldc2_w 10000000L
ladd
putfield org/xbill/DNS/LOCRecord/altitude J
aload 0
aload 0
aload 1
ldc "size"
iconst_0
lconst_0
ldc2_w 9000000000L
ldc2_w 100L
invokespecial org/xbill/DNS/LOCRecord/parseDouble(Lorg/xbill/DNS/Tokenizer;Ljava/lang/String;ZJJJ)J
putfield org/xbill/DNS/LOCRecord/size J
aload 0
aload 0
aload 1
ldc "horizontal precision"
iconst_0
lconst_0
ldc2_w 9000000000L
ldc2_w 1000000L
invokespecial org/xbill/DNS/LOCRecord/parseDouble(Lorg/xbill/DNS/Tokenizer;Ljava/lang/String;ZJJJ)J
putfield org/xbill/DNS/LOCRecord/hPrecision J
aload 0
aload 0
aload 1
ldc "vertical precision"
iconst_0
lconst_0
ldc2_w 9000000000L
ldc2_w 1000L
invokespecial org/xbill/DNS/LOCRecord/parseDouble(Lorg/xbill/DNS/Tokenizer;Ljava/lang/String;ZJJJ)J
putfield org/xbill/DNS/LOCRecord/vPrecision J
return
.limit locals 3
.limit stack 11
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
ifeq L0
new org/xbill/DNS/WireParseException
dup
ldc "Invalid LOC version"
invokespecial org/xbill/DNS/WireParseException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
invokestatic org/xbill/DNS/LOCRecord/parseLOCformat(I)J
putfield org/xbill/DNS/LOCRecord/size J
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
invokestatic org/xbill/DNS/LOCRecord/parseLOCformat(I)J
putfield org/xbill/DNS/LOCRecord/hPrecision J
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
invokestatic org/xbill/DNS/LOCRecord/parseLOCformat(I)J
putfield org/xbill/DNS/LOCRecord/vPrecision J
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU32()J
putfield org/xbill/DNS/LOCRecord/latitude J
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU32()J
putfield org/xbill/DNS/LOCRecord/longitude J
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU32()J
putfield org/xbill/DNS/LOCRecord/altitude J
return
.limit locals 2
.limit stack 3
.end method

.method rrToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 1
aload 0
aload 0
getfield org/xbill/DNS/LOCRecord/latitude J
bipush 78
bipush 83
invokespecial org/xbill/DNS/LOCRecord/positionToString(JCC)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
aload 0
getfield org/xbill/DNS/LOCRecord/longitude J
bipush 69
bipush 87
invokespecial org/xbill/DNS/LOCRecord/positionToString(JCC)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 0
aload 1
getstatic org/xbill/DNS/LOCRecord/w2 Ljava/text/NumberFormat;
aload 0
getfield org/xbill/DNS/LOCRecord/altitude J
ldc2_w 10000000L
lsub
ldc2_w 100L
invokespecial org/xbill/DNS/LOCRecord/renderFixedPoint(Ljava/lang/StringBuffer;Ljava/text/NumberFormat;JJ)V
aload 1
ldc "m "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 0
aload 1
getstatic org/xbill/DNS/LOCRecord/w2 Ljava/text/NumberFormat;
aload 0
getfield org/xbill/DNS/LOCRecord/size J
ldc2_w 100L
invokespecial org/xbill/DNS/LOCRecord/renderFixedPoint(Ljava/lang/StringBuffer;Ljava/text/NumberFormat;JJ)V
aload 1
ldc "m "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 0
aload 1
getstatic org/xbill/DNS/LOCRecord/w2 Ljava/text/NumberFormat;
aload 0
getfield org/xbill/DNS/LOCRecord/hPrecision J
ldc2_w 100L
invokespecial org/xbill/DNS/LOCRecord/renderFixedPoint(Ljava/lang/StringBuffer;Ljava/text/NumberFormat;JJ)V
aload 1
ldc "m "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 0
aload 1
getstatic org/xbill/DNS/LOCRecord/w2 Ljava/text/NumberFormat;
aload 0
getfield org/xbill/DNS/LOCRecord/vPrecision J
ldc2_w 100L
invokespecial org/xbill/DNS/LOCRecord/renderFixedPoint(Ljava/lang/StringBuffer;Ljava/text/NumberFormat;JJ)V
aload 1
ldc "m"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 7
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
iconst_0
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 1
aload 0
aload 0
getfield org/xbill/DNS/LOCRecord/size J
invokespecial org/xbill/DNS/LOCRecord/toLOCformat(J)I
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 1
aload 0
aload 0
getfield org/xbill/DNS/LOCRecord/hPrecision J
invokespecial org/xbill/DNS/LOCRecord/toLOCformat(J)I
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 1
aload 0
aload 0
getfield org/xbill/DNS/LOCRecord/vPrecision J
invokespecial org/xbill/DNS/LOCRecord/toLOCformat(J)I
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 1
aload 0
getfield org/xbill/DNS/LOCRecord/latitude J
invokevirtual org/xbill/DNS/DNSOutput/writeU32(J)V
aload 1
aload 0
getfield org/xbill/DNS/LOCRecord/longitude J
invokevirtual org/xbill/DNS/DNSOutput/writeU32(J)V
aload 1
aload 0
getfield org/xbill/DNS/LOCRecord/altitude J
invokevirtual org/xbill/DNS/DNSOutput/writeU32(J)V
return
.limit locals 4
.limit stack 4
.end method
