.bytecode 50.0
.class public synchronized org/xbill/DNS/ARecord
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = -2172609200849142323L


.field private 'addr' I

.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJLjava/net/InetAddress;)V
aload 0
aload 1
iconst_1
iload 2
lload 3
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
aload 5
invokestatic org/xbill/DNS/Address/familyOf(Ljava/net/InetAddress;)I
iconst_1
if_icmpeq L0
new java/lang/IllegalArgumentException
dup
ldc "invalid IPv4 address"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 5
invokevirtual java/net/InetAddress/getAddress()[B
invokestatic org/xbill/DNS/ARecord/fromArray([B)I
putfield org/xbill/DNS/ARecord/addr I
return
.limit locals 6
.limit stack 6
.end method

.method private static final fromArray([B)I
aload 0
iconst_0
baload
sipush 255
iand
bipush 24
ishl
aload 0
iconst_1
baload
sipush 255
iand
bipush 16
ishl
ior
aload 0
iconst_2
baload
sipush 255
iand
bipush 8
ishl
ior
aload 0
iconst_3
baload
sipush 255
iand
ior
ireturn
.limit locals 1
.limit stack 3
.end method

.method private static final toArray(I)[B
iconst_4
newarray byte
dup
iconst_0
iload 0
bipush 24
iushr
sipush 255
iand
i2b
bastore
dup
iconst_1
iload 0
bipush 16
iushr
sipush 255
iand
i2b
bastore
dup
iconst_2
iload 0
bipush 8
iushr
sipush 255
iand
i2b
bastore
dup
iconst_3
iload 0
sipush 255
iand
i2b
bastore
areturn
.limit locals 1
.limit stack 5
.end method

.method public getAddress()Ljava/net/InetAddress;
.catch java/net/UnknownHostException from L0 to L1 using L2
.catch java/net/UnknownHostException from L1 to L3 using L2
L0:
aload 0
getfield org/xbill/DNS/ARecord/name Lorg/xbill/DNS/Name;
ifnonnull L1
aload 0
getfield org/xbill/DNS/ARecord/addr I
invokestatic org/xbill/DNS/ARecord/toArray(I)[B
invokestatic java/net/InetAddress/getByAddress([B)Ljava/net/InetAddress;
areturn
L1:
aload 0
getfield org/xbill/DNS/ARecord/name Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/toString()Ljava/lang/String;
aload 0
getfield org/xbill/DNS/ARecord/addr I
invokestatic org/xbill/DNS/ARecord/toArray(I)[B
invokestatic java/net/InetAddress/getByAddress(Ljava/lang/String;[B)Ljava/net/InetAddress;
astore 1
L3:
aload 1
areturn
L2:
astore 1
aconst_null
areturn
.limit locals 2
.limit stack 2
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/ARecord
dup
invokespecial org/xbill/DNS/ARecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
aload 0
aload 1
iconst_1
invokevirtual org/xbill/DNS/Tokenizer/getAddressBytes(I)[B
invokestatic org/xbill/DNS/ARecord/fromArray([B)I
putfield org/xbill/DNS/ARecord/addr I
return
.limit locals 3
.limit stack 3
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
aload 1
iconst_4
invokevirtual org/xbill/DNS/DNSInput/readByteArray(I)[B
invokestatic org/xbill/DNS/ARecord/fromArray([B)I
putfield org/xbill/DNS/ARecord/addr I
return
.limit locals 2
.limit stack 3
.end method

.method rrToString()Ljava/lang/String;
aload 0
getfield org/xbill/DNS/ARecord/addr I
invokestatic org/xbill/DNS/ARecord/toArray(I)[B
invokestatic org/xbill/DNS/Address/toDottedQuad([B)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
aload 0
getfield org/xbill/DNS/ARecord/addr I
i2l
ldc2_w 4294967295L
land
invokevirtual org/xbill/DNS/DNSOutput/writeU32(J)V
return
.limit locals 4
.limit stack 5
.end method
