.bytecode 50.0
.class public synchronized org/xbill/DNS/A6Record
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = -8815026887337346789L


.field private 'prefix' Lorg/xbill/DNS/Name;

.field private 'prefixBits' I

.field private 'suffix' Ljava/net/InetAddress;

.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJILjava/net/InetAddress;Lorg/xbill/DNS/Name;)V
aload 0
aload 1
bipush 38
iload 2
lload 3
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
aload 0
ldc "prefixBits"
iload 5
invokestatic org/xbill/DNS/A6Record/checkU8(Ljava/lang/String;I)I
putfield org/xbill/DNS/A6Record/prefixBits I
aload 6
ifnull L0
aload 6
invokestatic org/xbill/DNS/Address/familyOf(Ljava/net/InetAddress;)I
iconst_2
if_icmpeq L0
new java/lang/IllegalArgumentException
dup
ldc "invalid IPv6 address"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 6
putfield org/xbill/DNS/A6Record/suffix Ljava/net/InetAddress;
aload 7
ifnull L1
aload 0
ldc "prefix"
aload 7
invokestatic org/xbill/DNS/A6Record/checkName(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/A6Record/prefix Lorg/xbill/DNS/Name;
L1:
return
.limit locals 8
.limit stack 6
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/A6Record
dup
invokespecial org/xbill/DNS/A6Record/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method public getPrefix()Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/A6Record/prefix Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getPrefixBits()I
aload 0
getfield org/xbill/DNS/A6Record/prefixBits I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getSuffix()Ljava/net/InetAddress;
aload 0
getfield org/xbill/DNS/A6Record/suffix Ljava/net/InetAddress;
areturn
.limit locals 1
.limit stack 1
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
.catch java/net/UnknownHostException from L0 to L1 using L2
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt8()I
putfield org/xbill/DNS/A6Record/prefixBits I
aload 0
getfield org/xbill/DNS/A6Record/prefixBits I
sipush 128
if_icmple L3
aload 1
ldc "prefix bits must be [0..128]"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L3:
aload 0
getfield org/xbill/DNS/A6Record/prefixBits I
sipush 128
if_icmpge L1
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
astore 3
L0:
aload 0
aload 3
iconst_2
invokestatic org/xbill/DNS/Address/getByAddress(Ljava/lang/String;I)Ljava/net/InetAddress;
putfield org/xbill/DNS/A6Record/suffix Ljava/net/InetAddress;
L1:
aload 0
getfield org/xbill/DNS/A6Record/prefixBits I
ifle L4
aload 0
aload 1
aload 2
invokevirtual org/xbill/DNS/Tokenizer/getName(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/A6Record/prefix Lorg/xbill/DNS/Name;
L4:
return
L2:
astore 2
aload 1
new java/lang/StringBuffer
dup
ldc "invalid IPv6 address: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 3
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
.limit locals 4
.limit stack 4
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
putfield org/xbill/DNS/A6Record/prefixBits I
sipush 128
aload 0
getfield org/xbill/DNS/A6Record/prefixBits I
isub
bipush 7
iadd
bipush 8
idiv
istore 2
aload 0
getfield org/xbill/DNS/A6Record/prefixBits I
sipush 128
if_icmpge L0
bipush 16
newarray byte
astore 3
aload 1
aload 3
bipush 16
iload 2
isub
iload 2
invokevirtual org/xbill/DNS/DNSInput/readByteArray([BII)V
aload 0
aload 3
invokestatic java/net/InetAddress/getByAddress([B)Ljava/net/InetAddress;
putfield org/xbill/DNS/A6Record/suffix Ljava/net/InetAddress;
L0:
aload 0
getfield org/xbill/DNS/A6Record/prefixBits I
ifle L1
aload 0
new org/xbill/DNS/Name
dup
aload 1
invokespecial org/xbill/DNS/Name/<init>(Lorg/xbill/DNS/DNSInput;)V
putfield org/xbill/DNS/A6Record/prefix Lorg/xbill/DNS/Name;
L1:
return
.limit locals 4
.limit stack 4
.end method

.method rrToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 1
aload 0
getfield org/xbill/DNS/A6Record/prefixBits I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 0
getfield org/xbill/DNS/A6Record/suffix Ljava/net/InetAddress;
ifnull L0
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/A6Record/suffix Ljava/net/InetAddress;
invokevirtual java/net/InetAddress/getHostAddress()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L0:
aload 0
getfield org/xbill/DNS/A6Record/prefix Lorg/xbill/DNS/Name;
ifnull L1
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/A6Record/prefix Lorg/xbill/DNS/Name;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
pop
L1:
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
aload 0
getfield org/xbill/DNS/A6Record/prefixBits I
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 0
getfield org/xbill/DNS/A6Record/suffix Ljava/net/InetAddress;
ifnull L0
sipush 128
aload 0
getfield org/xbill/DNS/A6Record/prefixBits I
isub
bipush 7
iadd
bipush 8
idiv
istore 4
aload 1
aload 0
getfield org/xbill/DNS/A6Record/suffix Ljava/net/InetAddress;
invokevirtual java/net/InetAddress/getAddress()[B
bipush 16
iload 4
isub
iload 4
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([BII)V
L0:
aload 0
getfield org/xbill/DNS/A6Record/prefix Lorg/xbill/DNS/Name;
ifnull L1
aload 0
getfield org/xbill/DNS/A6Record/prefix Lorg/xbill/DNS/Name;
aload 1
aconst_null
iload 3
invokevirtual org/xbill/DNS/Name/toWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
L1:
return
.limit locals 5
.limit stack 4
.end method
