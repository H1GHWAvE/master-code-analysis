.bytecode 50.0
.class public final synchronized org/xbill/DNS/Rcode
.super java/lang/Object

.field public static final 'BADKEY' I = 17


.field public static final 'BADMODE' I = 19


.field public static final 'BADSIG' I = 16


.field public static final 'BADTIME' I = 18


.field public static final 'BADVERS' I = 16


.field public static final 'FORMERR' I = 1


.field public static final 'NOERROR' I = 0


.field public static final 'NOTAUTH' I = 9


.field public static final 'NOTIMP' I = 4


.field public static final 'NOTIMPL' I = 4


.field public static final 'NOTZONE' I = 10


.field public static final 'NXDOMAIN' I = 3


.field public static final 'NXRRSET' I = 8


.field public static final 'REFUSED' I = 5


.field public static final 'SERVFAIL' I = 2


.field public static final 'YXDOMAIN' I = 6


.field public static final 'YXRRSET' I = 7


.field private static 'rcodes' Lorg/xbill/DNS/Mnemonic;

.field private static 'tsigrcodes' Lorg/xbill/DNS/Mnemonic;

.method static <clinit>()V
new org/xbill/DNS/Mnemonic
dup
ldc "DNS Rcode"
iconst_2
invokespecial org/xbill/DNS/Mnemonic/<init>(Ljava/lang/String;I)V
putstatic org/xbill/DNS/Rcode/rcodes Lorg/xbill/DNS/Mnemonic;
new org/xbill/DNS/Mnemonic
dup
ldc "TSIG rcode"
iconst_2
invokespecial org/xbill/DNS/Mnemonic/<init>(Ljava/lang/String;I)V
putstatic org/xbill/DNS/Rcode/tsigrcodes Lorg/xbill/DNS/Mnemonic;
getstatic org/xbill/DNS/Rcode/rcodes Lorg/xbill/DNS/Mnemonic;
sipush 4095
invokevirtual org/xbill/DNS/Mnemonic/setMaximum(I)V
getstatic org/xbill/DNS/Rcode/rcodes Lorg/xbill/DNS/Mnemonic;
ldc "RESERVED"
invokevirtual org/xbill/DNS/Mnemonic/setPrefix(Ljava/lang/String;)V
getstatic org/xbill/DNS/Rcode/rcodes Lorg/xbill/DNS/Mnemonic;
iconst_1
invokevirtual org/xbill/DNS/Mnemonic/setNumericAllowed(Z)V
getstatic org/xbill/DNS/Rcode/rcodes Lorg/xbill/DNS/Mnemonic;
iconst_0
ldc "NOERROR"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Rcode/rcodes Lorg/xbill/DNS/Mnemonic;
iconst_1
ldc "FORMERR"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Rcode/rcodes Lorg/xbill/DNS/Mnemonic;
iconst_2
ldc "SERVFAIL"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Rcode/rcodes Lorg/xbill/DNS/Mnemonic;
iconst_3
ldc "NXDOMAIN"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Rcode/rcodes Lorg/xbill/DNS/Mnemonic;
iconst_4
ldc "NOTIMP"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Rcode/rcodes Lorg/xbill/DNS/Mnemonic;
iconst_4
ldc "NOTIMPL"
invokevirtual org/xbill/DNS/Mnemonic/addAlias(ILjava/lang/String;)V
getstatic org/xbill/DNS/Rcode/rcodes Lorg/xbill/DNS/Mnemonic;
iconst_5
ldc "REFUSED"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Rcode/rcodes Lorg/xbill/DNS/Mnemonic;
bipush 6
ldc "YXDOMAIN"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Rcode/rcodes Lorg/xbill/DNS/Mnemonic;
bipush 7
ldc "YXRRSET"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Rcode/rcodes Lorg/xbill/DNS/Mnemonic;
bipush 8
ldc "NXRRSET"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Rcode/rcodes Lorg/xbill/DNS/Mnemonic;
bipush 9
ldc "NOTAUTH"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Rcode/rcodes Lorg/xbill/DNS/Mnemonic;
bipush 10
ldc "NOTZONE"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Rcode/rcodes Lorg/xbill/DNS/Mnemonic;
bipush 16
ldc "BADVERS"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Rcode/tsigrcodes Lorg/xbill/DNS/Mnemonic;
ldc_w 65535
invokevirtual org/xbill/DNS/Mnemonic/setMaximum(I)V
getstatic org/xbill/DNS/Rcode/tsigrcodes Lorg/xbill/DNS/Mnemonic;
ldc "RESERVED"
invokevirtual org/xbill/DNS/Mnemonic/setPrefix(Ljava/lang/String;)V
getstatic org/xbill/DNS/Rcode/tsigrcodes Lorg/xbill/DNS/Mnemonic;
iconst_1
invokevirtual org/xbill/DNS/Mnemonic/setNumericAllowed(Z)V
getstatic org/xbill/DNS/Rcode/tsigrcodes Lorg/xbill/DNS/Mnemonic;
getstatic org/xbill/DNS/Rcode/rcodes Lorg/xbill/DNS/Mnemonic;
invokevirtual org/xbill/DNS/Mnemonic/addAll(Lorg/xbill/DNS/Mnemonic;)V
getstatic org/xbill/DNS/Rcode/tsigrcodes Lorg/xbill/DNS/Mnemonic;
bipush 16
ldc "BADSIG"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Rcode/tsigrcodes Lorg/xbill/DNS/Mnemonic;
bipush 17
ldc "BADKEY"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Rcode/tsigrcodes Lorg/xbill/DNS/Mnemonic;
bipush 18
ldc "BADTIME"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Rcode/tsigrcodes Lorg/xbill/DNS/Mnemonic;
bipush 19
ldc "BADMODE"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
return
.limit locals 0
.limit stack 4
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static TSIGstring(I)Ljava/lang/String;
getstatic org/xbill/DNS/Rcode/tsigrcodes Lorg/xbill/DNS/Mnemonic;
iload 0
invokevirtual org/xbill/DNS/Mnemonic/getText(I)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public static string(I)Ljava/lang/String;
getstatic org/xbill/DNS/Rcode/rcodes Lorg/xbill/DNS/Mnemonic;
iload 0
invokevirtual org/xbill/DNS/Mnemonic/getText(I)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public static value(Ljava/lang/String;)I
getstatic org/xbill/DNS/Rcode/rcodes Lorg/xbill/DNS/Mnemonic;
aload 0
invokevirtual org/xbill/DNS/Mnemonic/getValue(Ljava/lang/String;)I
ireturn
.limit locals 1
.limit stack 2
.end method
