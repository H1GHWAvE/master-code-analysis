.bytecode 50.0
.class public synchronized org/xbill/DNS/RelativeNameException
.super java/lang/IllegalArgumentException

.method public <init>(Ljava/lang/String;)V
aload 0
aload 1
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(Lorg/xbill/DNS/Name;)V
aload 0
new java/lang/StringBuffer
dup
ldc "'"
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
ldc "' is not an absolute name"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
return
.limit locals 2
.limit stack 4
.end method
