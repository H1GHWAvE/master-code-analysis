.bytecode 50.0
.class synchronized abstract org/xbill/DNS/KEYBase
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = 3469321722693285454L


.field protected 'alg' I

.field protected 'flags' I

.field protected 'footprint' I

.field protected 'key' [B

.field protected 'proto' I

.field protected 'publicKey' Ljava/security/PublicKey;

.method protected <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
aload 0
iconst_m1
putfield org/xbill/DNS/KEYBase/footprint I
aload 0
aconst_null
putfield org/xbill/DNS/KEYBase/publicKey Ljava/security/PublicKey;
return
.limit locals 1
.limit stack 2
.end method

.method public <init>(Lorg/xbill/DNS/Name;IIJIII[B)V
aload 0
aload 1
iload 2
iload 3
lload 4
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
aload 0
iconst_m1
putfield org/xbill/DNS/KEYBase/footprint I
aload 0
aconst_null
putfield org/xbill/DNS/KEYBase/publicKey Ljava/security/PublicKey;
aload 0
ldc "flags"
iload 6
invokestatic org/xbill/DNS/KEYBase/checkU16(Ljava/lang/String;I)I
putfield org/xbill/DNS/KEYBase/flags I
aload 0
ldc "proto"
iload 7
invokestatic org/xbill/DNS/KEYBase/checkU8(Ljava/lang/String;I)I
putfield org/xbill/DNS/KEYBase/proto I
aload 0
ldc "alg"
iload 8
invokestatic org/xbill/DNS/KEYBase/checkU8(Ljava/lang/String;I)I
putfield org/xbill/DNS/KEYBase/alg I
aload 0
aload 9
putfield org/xbill/DNS/KEYBase/key [B
return
.limit locals 10
.limit stack 6
.end method

.method public getAlgorithm()I
aload 0
getfield org/xbill/DNS/KEYBase/alg I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getFlags()I
aload 0
getfield org/xbill/DNS/KEYBase/flags I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getFootprint()I
iconst_0
istore 2
aload 0
getfield org/xbill/DNS/KEYBase/footprint I
iflt L0
aload 0
getfield org/xbill/DNS/KEYBase/footprint I
ireturn
L0:
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 4
aload 0
aload 4
aconst_null
iconst_0
invokevirtual org/xbill/DNS/KEYBase/rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 4
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
astore 4
aload 0
getfield org/xbill/DNS/KEYBase/alg I
iconst_1
if_icmpne L1
aload 4
aload 4
arraylength
iconst_3
isub
baload
sipush 255
iand
bipush 8
ishl
aload 4
aload 4
arraylength
iconst_2
isub
baload
sipush 255
iand
iadd
istore 1
L2:
aload 0
iload 1
ldc_w 65535
iand
putfield org/xbill/DNS/KEYBase/footprint I
aload 0
getfield org/xbill/DNS/KEYBase/footprint I
ireturn
L1:
iconst_0
istore 1
L3:
iload 2
aload 4
arraylength
iconst_1
isub
if_icmpge L4
iload 1
aload 4
iload 2
baload
sipush 255
iand
bipush 8
ishl
aload 4
iload 2
iconst_1
iadd
baload
sipush 255
iand
iadd
iadd
istore 1
iload 2
iconst_2
iadd
istore 2
goto L3
L4:
iload 1
istore 3
iload 2
aload 4
arraylength
if_icmpge L5
iload 1
aload 4
iload 2
baload
sipush 255
iand
bipush 8
ishl
iadd
istore 3
L5:
iload 3
bipush 16
ishr
ldc_w 65535
iand
iload 3
iadd
istore 1
goto L2
.limit locals 5
.limit stack 5
.end method

.method public getKey()[B
aload 0
getfield org/xbill/DNS/KEYBase/key [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public getProtocol()I
aload 0
getfield org/xbill/DNS/KEYBase/proto I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getPublicKey()Ljava/security/PublicKey;
aload 0
getfield org/xbill/DNS/KEYBase/publicKey Ljava/security/PublicKey;
ifnull L0
aload 0
getfield org/xbill/DNS/KEYBase/publicKey Ljava/security/PublicKey;
areturn
L0:
aload 0
aload 0
invokestatic org/xbill/DNS/DNSSEC/toPublicKey(Lorg/xbill/DNS/KEYBase;)Ljava/security/PublicKey;
putfield org/xbill/DNS/KEYBase/publicKey Ljava/security/PublicKey;
aload 0
getfield org/xbill/DNS/KEYBase/publicKey Ljava/security/PublicKey;
areturn
.limit locals 1
.limit stack 2
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
putfield org/xbill/DNS/KEYBase/flags I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
putfield org/xbill/DNS/KEYBase/proto I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
putfield org/xbill/DNS/KEYBase/alg I
aload 1
invokevirtual org/xbill/DNS/DNSInput/remaining()I
ifle L0
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readByteArray()[B
putfield org/xbill/DNS/KEYBase/key [B
L0:
return
.limit locals 2
.limit stack 2
.end method

.method rrToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 1
aload 0
getfield org/xbill/DNS/KEYBase/flags I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/KEYBase/proto I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/KEYBase/alg I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 0
getfield org/xbill/DNS/KEYBase/key [B
ifnull L0
ldc "multiline"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L1
aload 1
ldc " (\n"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/KEYBase/key [B
bipush 64
ldc "\u0009"
iconst_1
invokestatic org/xbill/DNS/utils/base64/formatString([BILjava/lang/String;Z)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
ldc " ; key_tag = "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
invokevirtual org/xbill/DNS/KEYBase/getFootprint()I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
L0:
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
L1:
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/KEYBase/key [B
invokestatic org/xbill/DNS/utils/base64/toString([B)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
goto L0
.limit locals 2
.limit stack 5
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
aload 0
getfield org/xbill/DNS/KEYBase/flags I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 1
aload 0
getfield org/xbill/DNS/KEYBase/proto I
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 1
aload 0
getfield org/xbill/DNS/KEYBase/alg I
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 0
getfield org/xbill/DNS/KEYBase/key [B
ifnull L0
aload 1
aload 0
getfield org/xbill/DNS/KEYBase/key [B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
L0:
return
.limit locals 4
.limit stack 2
.end method
