.bytecode 50.0
.class public final synchronized org/xbill/DNS/Lookup
.super java/lang/Object

.field public static final 'HOST_NOT_FOUND' I = 3


.field public static final 'SUCCESSFUL' I = 0


.field public static final 'TRY_AGAIN' I = 2


.field public static final 'TYPE_NOT_FOUND' I = 4


.field public static final 'UNRECOVERABLE' I = 1


.field static 'class$org$xbill$DNS$Lookup' Ljava/lang/Class;

.field private static 'defaultCaches' Ljava/util/Map;

.field private static 'defaultNdots' I

.field private static 'defaultResolver' Lorg/xbill/DNS/Resolver;

.field private static 'defaultSearchPath' [Lorg/xbill/DNS/Name;

.field private static final 'noAliases' [Lorg/xbill/DNS/Name;

.field private 'aliases' Ljava/util/List;

.field private 'answers' [Lorg/xbill/DNS/Record;

.field private 'badresponse' Z

.field private 'badresponse_error' Ljava/lang/String;

.field private 'cache' Lorg/xbill/DNS/Cache;

.field private 'credibility' I

.field private 'dclass' I

.field private 'done' Z

.field private 'doneCurrent' Z

.field private 'error' Ljava/lang/String;

.field private 'foundAlias' Z

.field private 'iterations' I

.field private 'name' Lorg/xbill/DNS/Name;

.field private 'nametoolong' Z

.field private 'networkerror' Z

.field private 'nxdomain' Z

.field private 'referral' Z

.field private 'resolver' Lorg/xbill/DNS/Resolver;

.field private 'result' I

.field private 'searchPath' [Lorg/xbill/DNS/Name;

.field private 'temporary_cache' Z

.field private 'timedout' Z

.field private 'type' I

.field private 'verbose' Z

.method static <clinit>()V
iconst_0
anewarray org/xbill/DNS/Name
putstatic org/xbill/DNS/Lookup/noAliases [Lorg/xbill/DNS/Name;
invokestatic org/xbill/DNS/Lookup/refreshDefault()V
return
.limit locals 0
.limit stack 1
.end method

.method public <init>(Ljava/lang/String;)V
aload 0
aload 1
invokestatic org/xbill/DNS/Name/fromString(Ljava/lang/String;)Lorg/xbill/DNS/Name;
iconst_1
iconst_1
invokespecial org/xbill/DNS/Lookup/<init>(Lorg/xbill/DNS/Name;II)V
return
.limit locals 2
.limit stack 4
.end method

.method public <init>(Ljava/lang/String;I)V
aload 0
aload 1
invokestatic org/xbill/DNS/Name/fromString(Ljava/lang/String;)Lorg/xbill/DNS/Name;
iload 2
iconst_1
invokespecial org/xbill/DNS/Lookup/<init>(Lorg/xbill/DNS/Name;II)V
return
.limit locals 3
.limit stack 4
.end method

.method public <init>(Ljava/lang/String;II)V
aload 0
aload 1
invokestatic org/xbill/DNS/Name/fromString(Ljava/lang/String;)Lorg/xbill/DNS/Name;
iload 2
iload 3
invokespecial org/xbill/DNS/Lookup/<init>(Lorg/xbill/DNS/Name;II)V
return
.limit locals 4
.limit stack 4
.end method

.method public <init>(Lorg/xbill/DNS/Name;)V
aload 0
aload 1
iconst_1
iconst_1
invokespecial org/xbill/DNS/Lookup/<init>(Lorg/xbill/DNS/Name;II)V
return
.limit locals 2
.limit stack 4
.end method

.method public <init>(Lorg/xbill/DNS/Name;I)V
aload 0
aload 1
iload 2
iconst_1
invokespecial org/xbill/DNS/Lookup/<init>(Lorg/xbill/DNS/Name;II)V
return
.limit locals 3
.limit stack 4
.end method

.method public <init>(Lorg/xbill/DNS/Name;II)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
invokespecial java/lang/Object/<init>()V
iload 2
invokestatic org/xbill/DNS/Type/check(I)V
iload 3
invokestatic org/xbill/DNS/DClass/check(I)V
iload 2
invokestatic org/xbill/DNS/Type/isRR(I)Z
ifne L5
iload 2
sipush 255
if_icmpeq L5
new java/lang/IllegalArgumentException
dup
ldc "Cannot query for meta-types other than ANY"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L5:
aload 0
aload 1
putfield org/xbill/DNS/Lookup/name Lorg/xbill/DNS/Name;
aload 0
iload 2
putfield org/xbill/DNS/Lookup/type I
aload 0
iload 3
putfield org/xbill/DNS/Lookup/dclass I
getstatic org/xbill/DNS/Lookup/class$org$xbill$DNS$Lookup Ljava/lang/Class;
ifnonnull L6
ldc "org.xbill.DNS.Lookup"
invokestatic org/xbill/DNS/Lookup/class$(Ljava/lang/String;)Ljava/lang/Class;
astore 1
aload 1
putstatic org/xbill/DNS/Lookup/class$org$xbill$DNS$Lookup Ljava/lang/Class;
L7:
aload 1
monitorenter
L0:
aload 0
invokestatic org/xbill/DNS/Lookup/getDefaultResolver()Lorg/xbill/DNS/Resolver;
putfield org/xbill/DNS/Lookup/resolver Lorg/xbill/DNS/Resolver;
aload 0
invokestatic org/xbill/DNS/Lookup/getDefaultSearchPath()[Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/Lookup/searchPath [Lorg/xbill/DNS/Name;
aload 0
iload 3
invokestatic org/xbill/DNS/Lookup/getDefaultCache(I)Lorg/xbill/DNS/Cache;
putfield org/xbill/DNS/Lookup/cache Lorg/xbill/DNS/Cache;
aload 1
monitorexit
L1:
aload 0
iconst_3
putfield org/xbill/DNS/Lookup/credibility I
aload 0
ldc "verbose"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
putfield org/xbill/DNS/Lookup/verbose Z
aload 0
iconst_m1
putfield org/xbill/DNS/Lookup/result I
return
L6:
getstatic org/xbill/DNS/Lookup/class$org$xbill$DNS$Lookup Ljava/lang/Class;
astore 1
goto L7
L2:
astore 4
L3:
aload 1
monitorexit
L4:
aload 4
athrow
.limit locals 5
.limit stack 3
.end method

.method private checkDone()V
aload 0
getfield org/xbill/DNS/Lookup/done Z
ifeq L0
aload 0
getfield org/xbill/DNS/Lookup/result I
iconst_m1
if_icmpeq L0
return
L0:
new java/lang/StringBuffer
dup
new java/lang/StringBuffer
dup
ldc "Lookup of "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
getfield org/xbill/DNS/Lookup/name Lorg/xbill/DNS/Name;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
astore 1
aload 0
getfield org/xbill/DNS/Lookup/dclass I
iconst_1
if_icmpeq L1
aload 1
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 0
getfield org/xbill/DNS/Lookup/dclass I
invokestatic org/xbill/DNS/DClass/string(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L1:
aload 1
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 0
getfield org/xbill/DNS/Lookup/type I
invokestatic org/xbill/DNS/Type/string(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc " isn't done"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
new java/lang/IllegalStateException
dup
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
.limit locals 2
.limit stack 5
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
.catch java/lang/ClassNotFoundException from L0 to L1 using L2
L0:
aload 0
invokestatic java/lang/Class/forName(Ljava/lang/String;)Ljava/lang/Class;
astore 0
L1:
aload 0
areturn
L2:
astore 0
new java/lang/NoClassDefFoundError
dup
invokespecial java/lang/NoClassDefFoundError/<init>()V
aload 0
invokevirtual java/lang/NoClassDefFoundError/initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
athrow
.limit locals 1
.limit stack 2
.end method

.method private follow(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Name;)V
aload 0
iconst_1
putfield org/xbill/DNS/Lookup/foundAlias Z
aload 0
iconst_0
putfield org/xbill/DNS/Lookup/badresponse Z
aload 0
iconst_0
putfield org/xbill/DNS/Lookup/networkerror Z
aload 0
iconst_0
putfield org/xbill/DNS/Lookup/timedout Z
aload 0
iconst_0
putfield org/xbill/DNS/Lookup/nxdomain Z
aload 0
iconst_0
putfield org/xbill/DNS/Lookup/referral Z
aload 0
aload 0
getfield org/xbill/DNS/Lookup/iterations I
iconst_1
iadd
putfield org/xbill/DNS/Lookup/iterations I
aload 0
getfield org/xbill/DNS/Lookup/iterations I
bipush 6
if_icmpge L0
aload 1
aload 2
invokevirtual org/xbill/DNS/Name/equals(Ljava/lang/Object;)Z
ifeq L1
L0:
aload 0
iconst_1
putfield org/xbill/DNS/Lookup/result I
aload 0
ldc "CNAME loop"
putfield org/xbill/DNS/Lookup/error Ljava/lang/String;
aload 0
iconst_1
putfield org/xbill/DNS/Lookup/done Z
return
L1:
aload 0
getfield org/xbill/DNS/Lookup/aliases Ljava/util/List;
ifnonnull L2
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield org/xbill/DNS/Lookup/aliases Ljava/util/List;
L2:
aload 0
getfield org/xbill/DNS/Lookup/aliases Ljava/util/List;
aload 2
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 0
aload 1
invokespecial org/xbill/DNS/Lookup/lookup(Lorg/xbill/DNS/Name;)V
return
.limit locals 3
.limit stack 3
.end method

.method public static getDefaultCache(I)Lorg/xbill/DNS/Cache;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
ldc org/xbill/DNS/Lookup
monitorenter
L0:
iload 0
invokestatic org/xbill/DNS/DClass/check(I)V
getstatic org/xbill/DNS/Lookup/defaultCaches Ljava/util/Map;
iload 0
invokestatic org/xbill/DNS/Mnemonic/toInteger(I)Ljava/lang/Integer;
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast org/xbill/DNS/Cache
astore 2
L1:
aload 2
astore 1
aload 2
ifnonnull L4
L3:
new org/xbill/DNS/Cache
dup
iload 0
invokespecial org/xbill/DNS/Cache/<init>(I)V
astore 1
getstatic org/xbill/DNS/Lookup/defaultCaches Ljava/util/Map;
iload 0
invokestatic org/xbill/DNS/Mnemonic/toInteger(I)Ljava/lang/Integer;
aload 1
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L4:
ldc org/xbill/DNS/Lookup
monitorexit
aload 1
areturn
L2:
astore 1
ldc org/xbill/DNS/Lookup
monitorexit
aload 1
athrow
.limit locals 3
.limit stack 3
.end method

.method public static getDefaultResolver()Lorg/xbill/DNS/Resolver;
.catch all from L0 to L1 using L2
ldc org/xbill/DNS/Lookup
monitorenter
L0:
getstatic org/xbill/DNS/Lookup/defaultResolver Lorg/xbill/DNS/Resolver;
astore 0
L1:
ldc org/xbill/DNS/Lookup
monitorexit
aload 0
areturn
L2:
astore 0
ldc org/xbill/DNS/Lookup
monitorexit
aload 0
athrow
.limit locals 1
.limit stack 1
.end method

.method public static getDefaultSearchPath()[Lorg/xbill/DNS/Name;
.catch all from L0 to L1 using L2
ldc org/xbill/DNS/Lookup
monitorenter
L0:
getstatic org/xbill/DNS/Lookup/defaultSearchPath [Lorg/xbill/DNS/Name;
astore 0
L1:
ldc org/xbill/DNS/Lookup
monitorexit
aload 0
areturn
L2:
astore 0
ldc org/xbill/DNS/Lookup
monitorexit
aload 0
athrow
.limit locals 1
.limit stack 1
.end method

.method private lookup(Lorg/xbill/DNS/Name;)V
.catch java/io/IOException from L0 to L1 using L2
aload 0
getfield org/xbill/DNS/Lookup/cache Lorg/xbill/DNS/Cache;
aload 1
aload 0
getfield org/xbill/DNS/Lookup/type I
aload 0
getfield org/xbill/DNS/Lookup/credibility I
invokevirtual org/xbill/DNS/Cache/lookupRecords(Lorg/xbill/DNS/Name;II)Lorg/xbill/DNS/SetResponse;
astore 3
aload 0
getfield org/xbill/DNS/Lookup/verbose Z
ifeq L3
getstatic java/lang/System/err Ljava/io/PrintStream;
new java/lang/StringBuffer
dup
ldc "lookup "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
aload 0
getfield org/xbill/DNS/Lookup/type I
invokestatic org/xbill/DNS/Type/string(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic java/lang/System/err Ljava/io/PrintStream;
aload 3
invokevirtual java/io/PrintStream/println(Ljava/lang/Object;)V
L3:
aload 0
aload 1
aload 3
invokespecial org/xbill/DNS/Lookup/processResponse(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/SetResponse;)V
aload 0
getfield org/xbill/DNS/Lookup/done Z
ifne L4
aload 0
getfield org/xbill/DNS/Lookup/doneCurrent Z
ifeq L5
L4:
return
L5:
aload 1
aload 0
getfield org/xbill/DNS/Lookup/type I
aload 0
getfield org/xbill/DNS/Lookup/dclass I
invokestatic org/xbill/DNS/Record/newRecord(Lorg/xbill/DNS/Name;II)Lorg/xbill/DNS/Record;
invokestatic org/xbill/DNS/Message/newQuery(Lorg/xbill/DNS/Record;)Lorg/xbill/DNS/Message;
astore 3
L0:
aload 0
getfield org/xbill/DNS/Lookup/resolver Lorg/xbill/DNS/Resolver;
aload 3
invokeinterface org/xbill/DNS/Resolver/send(Lorg/xbill/DNS/Message;)Lorg/xbill/DNS/Message; 1
astore 4
L1:
aload 4
invokevirtual org/xbill/DNS/Message/getHeader()Lorg/xbill/DNS/Header;
invokevirtual org/xbill/DNS/Header/getRcode()I
istore 2
iload 2
ifeq L6
iload 2
iconst_3
if_icmpeq L6
aload 0
iconst_1
putfield org/xbill/DNS/Lookup/badresponse Z
aload 0
iload 2
invokestatic org/xbill/DNS/Rcode/string(I)Ljava/lang/String;
putfield org/xbill/DNS/Lookup/badresponse_error Ljava/lang/String;
return
L2:
astore 1
aload 1
instanceof java/io/InterruptedIOException
ifeq L7
aload 0
iconst_1
putfield org/xbill/DNS/Lookup/timedout Z
return
L7:
aload 0
iconst_1
putfield org/xbill/DNS/Lookup/networkerror Z
return
L6:
aload 3
invokevirtual org/xbill/DNS/Message/getQuestion()Lorg/xbill/DNS/Record;
aload 4
invokevirtual org/xbill/DNS/Message/getQuestion()Lorg/xbill/DNS/Record;
invokevirtual org/xbill/DNS/Record/equals(Ljava/lang/Object;)Z
ifne L8
aload 0
iconst_1
putfield org/xbill/DNS/Lookup/badresponse Z
aload 0
ldc "response does not match query"
putfield org/xbill/DNS/Lookup/badresponse_error Ljava/lang/String;
return
L8:
aload 0
getfield org/xbill/DNS/Lookup/cache Lorg/xbill/DNS/Cache;
aload 4
invokevirtual org/xbill/DNS/Cache/addMessage(Lorg/xbill/DNS/Message;)Lorg/xbill/DNS/SetResponse;
astore 4
aload 4
astore 3
aload 4
ifnonnull L9
aload 0
getfield org/xbill/DNS/Lookup/cache Lorg/xbill/DNS/Cache;
aload 1
aload 0
getfield org/xbill/DNS/Lookup/type I
aload 0
getfield org/xbill/DNS/Lookup/credibility I
invokevirtual org/xbill/DNS/Cache/lookupRecords(Lorg/xbill/DNS/Name;II)Lorg/xbill/DNS/SetResponse;
astore 3
L9:
aload 0
getfield org/xbill/DNS/Lookup/verbose Z
ifeq L10
getstatic java/lang/System/err Ljava/io/PrintStream;
new java/lang/StringBuffer
dup
ldc "queried "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
aload 0
getfield org/xbill/DNS/Lookup/type I
invokestatic org/xbill/DNS/Type/string(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic java/lang/System/err Ljava/io/PrintStream;
aload 3
invokevirtual java/io/PrintStream/println(Ljava/lang/Object;)V
L10:
aload 0
aload 1
aload 3
invokespecial org/xbill/DNS/Lookup/processResponse(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/SetResponse;)V
return
.limit locals 5
.limit stack 4
.end method

.method private processResponse(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/SetResponse;)V
.catch org/xbill/DNS/NameTooLongException from L0 to L1 using L2
aload 2
invokevirtual org/xbill/DNS/SetResponse/isSuccessful()Z
ifeq L3
aload 2
invokevirtual org/xbill/DNS/SetResponse/answers()[Lorg/xbill/DNS/RRset;
astore 1
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 2
iconst_0
istore 3
L4:
iload 3
aload 1
arraylength
if_icmpge L5
aload 1
iload 3
aaload
invokevirtual org/xbill/DNS/RRset/rrs()Ljava/util/Iterator;
astore 4
L6:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L7
aload 2
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
goto L6
L7:
iload 3
iconst_1
iadd
istore 3
goto L4
L5:
aload 0
iconst_0
putfield org/xbill/DNS/Lookup/result I
aload 0
aload 2
aload 2
invokeinterface java/util/List/size()I 0
anewarray org/xbill/DNS/Record
invokeinterface java/util/List/toArray([Ljava/lang/Object;)[Ljava/lang/Object; 1
checkcast [Lorg/xbill/DNS/Record;
checkcast [Lorg/xbill/DNS/Record;
putfield org/xbill/DNS/Lookup/answers [Lorg/xbill/DNS/Record;
aload 0
iconst_1
putfield org/xbill/DNS/Lookup/done Z
L8:
return
L3:
aload 2
invokevirtual org/xbill/DNS/SetResponse/isNXDOMAIN()Z
ifeq L9
aload 0
iconst_1
putfield org/xbill/DNS/Lookup/nxdomain Z
aload 0
iconst_1
putfield org/xbill/DNS/Lookup/doneCurrent Z
aload 0
getfield org/xbill/DNS/Lookup/iterations I
ifle L8
aload 0
iconst_3
putfield org/xbill/DNS/Lookup/result I
aload 0
iconst_1
putfield org/xbill/DNS/Lookup/done Z
return
L9:
aload 2
invokevirtual org/xbill/DNS/SetResponse/isNXRRSET()Z
ifeq L10
aload 0
iconst_4
putfield org/xbill/DNS/Lookup/result I
aload 0
aconst_null
putfield org/xbill/DNS/Lookup/answers [Lorg/xbill/DNS/Record;
aload 0
iconst_1
putfield org/xbill/DNS/Lookup/done Z
return
L10:
aload 2
invokevirtual org/xbill/DNS/SetResponse/isCNAME()Z
ifeq L11
aload 0
aload 2
invokevirtual org/xbill/DNS/SetResponse/getCNAME()Lorg/xbill/DNS/CNAMERecord;
invokevirtual org/xbill/DNS/CNAMERecord/getTarget()Lorg/xbill/DNS/Name;
aload 1
invokespecial org/xbill/DNS/Lookup/follow(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Name;)V
return
L11:
aload 2
invokevirtual org/xbill/DNS/SetResponse/isDNAME()Z
ifeq L12
aload 2
invokevirtual org/xbill/DNS/SetResponse/getDNAME()Lorg/xbill/DNS/DNAMERecord;
astore 2
L0:
aload 0
aload 1
aload 2
invokevirtual org/xbill/DNS/Name/fromDNAME(Lorg/xbill/DNS/DNAMERecord;)Lorg/xbill/DNS/Name;
aload 1
invokespecial org/xbill/DNS/Lookup/follow(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Name;)V
L1:
return
L2:
astore 1
aload 0
iconst_1
putfield org/xbill/DNS/Lookup/result I
aload 0
ldc "Invalid DNAME target"
putfield org/xbill/DNS/Lookup/error Ljava/lang/String;
aload 0
iconst_1
putfield org/xbill/DNS/Lookup/done Z
return
L12:
aload 2
invokevirtual org/xbill/DNS/SetResponse/isDelegation()Z
ifeq L8
aload 0
iconst_1
putfield org/xbill/DNS/Lookup/referral Z
return
.limit locals 5
.limit stack 3
.end method

.method public static refreshDefault()V
.catch java/net/UnknownHostException from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch all from L1 to L4 using L3
.catch all from L5 to L3 using L3
ldc org/xbill/DNS/Lookup
monitorenter
L0:
new org/xbill/DNS/ExtendedResolver
dup
invokespecial org/xbill/DNS/ExtendedResolver/<init>()V
putstatic org/xbill/DNS/Lookup/defaultResolver Lorg/xbill/DNS/Resolver;
L1:
invokestatic org/xbill/DNS/ResolverConfig/getCurrentConfig()Lorg/xbill/DNS/ResolverConfig;
invokevirtual org/xbill/DNS/ResolverConfig/searchPath()[Lorg/xbill/DNS/Name;
putstatic org/xbill/DNS/Lookup/defaultSearchPath [Lorg/xbill/DNS/Name;
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putstatic org/xbill/DNS/Lookup/defaultCaches Ljava/util/Map;
invokestatic org/xbill/DNS/ResolverConfig/getCurrentConfig()Lorg/xbill/DNS/ResolverConfig;
invokevirtual org/xbill/DNS/ResolverConfig/ndots()I
putstatic org/xbill/DNS/Lookup/defaultNdots I
L4:
ldc org/xbill/DNS/Lookup
monitorexit
return
L2:
astore 0
L5:
new java/lang/RuntimeException
dup
ldc "Failed to initialize resolver"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L3:
astore 0
ldc org/xbill/DNS/Lookup
monitorexit
aload 0
athrow
.limit locals 1
.limit stack 3
.end method

.method private final reset()V
aload 0
iconst_0
putfield org/xbill/DNS/Lookup/iterations I
aload 0
iconst_0
putfield org/xbill/DNS/Lookup/foundAlias Z
aload 0
iconst_0
putfield org/xbill/DNS/Lookup/done Z
aload 0
iconst_0
putfield org/xbill/DNS/Lookup/doneCurrent Z
aload 0
aconst_null
putfield org/xbill/DNS/Lookup/aliases Ljava/util/List;
aload 0
aconst_null
putfield org/xbill/DNS/Lookup/answers [Lorg/xbill/DNS/Record;
aload 0
iconst_m1
putfield org/xbill/DNS/Lookup/result I
aload 0
aconst_null
putfield org/xbill/DNS/Lookup/error Ljava/lang/String;
aload 0
iconst_0
putfield org/xbill/DNS/Lookup/nxdomain Z
aload 0
iconst_0
putfield org/xbill/DNS/Lookup/badresponse Z
aload 0
aconst_null
putfield org/xbill/DNS/Lookup/badresponse_error Ljava/lang/String;
aload 0
iconst_0
putfield org/xbill/DNS/Lookup/networkerror Z
aload 0
iconst_0
putfield org/xbill/DNS/Lookup/timedout Z
aload 0
iconst_0
putfield org/xbill/DNS/Lookup/nametoolong Z
aload 0
iconst_0
putfield org/xbill/DNS/Lookup/referral Z
aload 0
getfield org/xbill/DNS/Lookup/temporary_cache Z
ifeq L0
aload 0
getfield org/xbill/DNS/Lookup/cache Lorg/xbill/DNS/Cache;
invokevirtual org/xbill/DNS/Cache/clearCache()V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method private resolve(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Name;)V
.catch org/xbill/DNS/NameTooLongException from L0 to L1 using L2
aload 0
iconst_0
putfield org/xbill/DNS/Lookup/doneCurrent Z
aload 1
astore 3
aload 2
ifnull L1
L0:
aload 1
aload 2
invokestatic org/xbill/DNS/Name/concatenate(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
astore 3
L1:
aload 0
aload 3
invokespecial org/xbill/DNS/Lookup/lookup(Lorg/xbill/DNS/Name;)V
return
L2:
astore 1
aload 0
iconst_1
putfield org/xbill/DNS/Lookup/nametoolong Z
return
.limit locals 4
.limit stack 2
.end method

.method public static setDefaultCache(Lorg/xbill/DNS/Cache;I)V
.catch all from L0 to L1 using L2
ldc org/xbill/DNS/Lookup
monitorenter
L0:
iload 1
invokestatic org/xbill/DNS/DClass/check(I)V
getstatic org/xbill/DNS/Lookup/defaultCaches Ljava/util/Map;
iload 1
invokestatic org/xbill/DNS/Mnemonic/toInteger(I)Ljava/lang/Integer;
aload 0
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L1:
ldc org/xbill/DNS/Lookup
monitorexit
return
L2:
astore 0
ldc org/xbill/DNS/Lookup
monitorexit
aload 0
athrow
.limit locals 2
.limit stack 3
.end method

.method public static setDefaultResolver(Lorg/xbill/DNS/Resolver;)V
.catch all from L0 to L1 using L2
ldc org/xbill/DNS/Lookup
monitorenter
L0:
aload 0
putstatic org/xbill/DNS/Lookup/defaultResolver Lorg/xbill/DNS/Resolver;
L1:
ldc org/xbill/DNS/Lookup
monitorexit
return
L2:
astore 0
ldc org/xbill/DNS/Lookup
monitorexit
aload 0
athrow
.limit locals 1
.limit stack 1
.end method

.method public static setDefaultSearchPath([Ljava/lang/String;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
ldc org/xbill/DNS/Lookup
monitorenter
aload 0
ifnonnull L3
L0:
aconst_null
putstatic org/xbill/DNS/Lookup/defaultSearchPath [Lorg/xbill/DNS/Name;
L1:
ldc org/xbill/DNS/Lookup
monitorexit
return
L3:
aload 0
arraylength
anewarray org/xbill/DNS/Name
astore 2
L4:
iconst_0
istore 1
L5:
iload 1
aload 0
arraylength
if_icmpge L7
aload 2
iload 1
aload 0
iload 1
aaload
getstatic org/xbill/DNS/Name/root Lorg/xbill/DNS/Name;
invokestatic org/xbill/DNS/Name/fromString(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
aastore
L6:
iload 1
iconst_1
iadd
istore 1
goto L5
L7:
aload 2
putstatic org/xbill/DNS/Lookup/defaultSearchPath [Lorg/xbill/DNS/Name;
L8:
goto L1
L2:
astore 0
ldc org/xbill/DNS/Lookup
monitorexit
aload 0
athrow
.limit locals 3
.limit stack 4
.end method

.method public static setDefaultSearchPath([Lorg/xbill/DNS/Name;)V
.catch all from L0 to L1 using L2
ldc org/xbill/DNS/Lookup
monitorenter
L0:
aload 0
putstatic org/xbill/DNS/Lookup/defaultSearchPath [Lorg/xbill/DNS/Name;
L1:
ldc org/xbill/DNS/Lookup
monitorexit
return
L2:
astore 0
ldc org/xbill/DNS/Lookup
monitorexit
aload 0
athrow
.limit locals 1
.limit stack 1
.end method

.method public static setPacketLogger(Lorg/xbill/DNS/PacketLogger;)V
.catch all from L0 to L1 using L2
ldc org/xbill/DNS/Lookup
monitorenter
L0:
aload 0
invokestatic org/xbill/DNS/Client/setPacketLogger(Lorg/xbill/DNS/PacketLogger;)V
L1:
ldc org/xbill/DNS/Lookup
monitorexit
return
L2:
astore 0
ldc org/xbill/DNS/Lookup
monitorexit
aload 0
athrow
.limit locals 1
.limit stack 1
.end method

.method public final getAliases()[Lorg/xbill/DNS/Name;
aload 0
invokespecial org/xbill/DNS/Lookup/checkDone()V
aload 0
getfield org/xbill/DNS/Lookup/aliases Ljava/util/List;
ifnonnull L0
getstatic org/xbill/DNS/Lookup/noAliases [Lorg/xbill/DNS/Name;
areturn
L0:
aload 0
getfield org/xbill/DNS/Lookup/aliases Ljava/util/List;
aload 0
getfield org/xbill/DNS/Lookup/aliases Ljava/util/List;
invokeinterface java/util/List/size()I 0
anewarray org/xbill/DNS/Name
invokeinterface java/util/List/toArray([Ljava/lang/Object;)[Ljava/lang/Object; 1
checkcast [Lorg/xbill/DNS/Name;
checkcast [Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final getAnswers()[Lorg/xbill/DNS/Record;
aload 0
invokespecial org/xbill/DNS/Lookup/checkDone()V
aload 0
getfield org/xbill/DNS/Lookup/answers [Lorg/xbill/DNS/Record;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getErrorString()Ljava/lang/String;
aload 0
invokespecial org/xbill/DNS/Lookup/checkDone()V
aload 0
getfield org/xbill/DNS/Lookup/error Ljava/lang/String;
ifnull L0
aload 0
getfield org/xbill/DNS/Lookup/error Ljava/lang/String;
areturn
L0:
aload 0
getfield org/xbill/DNS/Lookup/result I
tableswitch 0
L1
L2
L3
L4
L5
default : L6
L6:
new java/lang/IllegalStateException
dup
ldc "unknown result"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L1:
ldc "successful"
areturn
L2:
ldc "unrecoverable error"
areturn
L3:
ldc "try again"
areturn
L4:
ldc "host not found"
areturn
L5:
ldc "type not found"
areturn
.limit locals 1
.limit stack 3
.end method

.method public final getResult()I
aload 0
invokespecial org/xbill/DNS/Lookup/checkDone()V
aload 0
getfield org/xbill/DNS/Lookup/result I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final run()[Lorg/xbill/DNS/Record;
aload 0
getfield org/xbill/DNS/Lookup/done Z
ifeq L0
aload 0
invokespecial org/xbill/DNS/Lookup/reset()V
L0:
aload 0
getfield org/xbill/DNS/Lookup/name Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/isAbsolute()Z
ifeq L1
aload 0
aload 0
getfield org/xbill/DNS/Lookup/name Lorg/xbill/DNS/Name;
aconst_null
invokespecial org/xbill/DNS/Lookup/resolve(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Name;)V
L2:
aload 0
getfield org/xbill/DNS/Lookup/done Z
ifne L3
aload 0
getfield org/xbill/DNS/Lookup/badresponse Z
ifeq L4
aload 0
iconst_2
putfield org/xbill/DNS/Lookup/result I
aload 0
aload 0
getfield org/xbill/DNS/Lookup/badresponse_error Ljava/lang/String;
putfield org/xbill/DNS/Lookup/error Ljava/lang/String;
aload 0
iconst_1
putfield org/xbill/DNS/Lookup/done Z
L3:
aload 0
getfield org/xbill/DNS/Lookup/answers [Lorg/xbill/DNS/Record;
areturn
L1:
aload 0
getfield org/xbill/DNS/Lookup/searchPath [Lorg/xbill/DNS/Name;
ifnonnull L5
aload 0
aload 0
getfield org/xbill/DNS/Lookup/name Lorg/xbill/DNS/Name;
getstatic org/xbill/DNS/Name/root Lorg/xbill/DNS/Name;
invokespecial org/xbill/DNS/Lookup/resolve(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Name;)V
goto L2
L5:
aload 0
getfield org/xbill/DNS/Lookup/name Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/labels()I
getstatic org/xbill/DNS/Lookup/defaultNdots I
if_icmple L6
aload 0
aload 0
getfield org/xbill/DNS/Lookup/name Lorg/xbill/DNS/Name;
getstatic org/xbill/DNS/Name/root Lorg/xbill/DNS/Name;
invokespecial org/xbill/DNS/Lookup/resolve(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Name;)V
L6:
aload 0
getfield org/xbill/DNS/Lookup/done Z
ifeq L7
aload 0
getfield org/xbill/DNS/Lookup/answers [Lorg/xbill/DNS/Record;
areturn
L7:
iconst_0
istore 1
L8:
iload 1
aload 0
getfield org/xbill/DNS/Lookup/searchPath [Lorg/xbill/DNS/Name;
arraylength
if_icmpge L2
aload 0
aload 0
getfield org/xbill/DNS/Lookup/name Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/Lookup/searchPath [Lorg/xbill/DNS/Name;
iload 1
aaload
invokespecial org/xbill/DNS/Lookup/resolve(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Name;)V
aload 0
getfield org/xbill/DNS/Lookup/done Z
ifeq L9
aload 0
getfield org/xbill/DNS/Lookup/answers [Lorg/xbill/DNS/Record;
areturn
L9:
aload 0
getfield org/xbill/DNS/Lookup/foundAlias Z
ifne L2
iload 1
iconst_1
iadd
istore 1
goto L8
L4:
aload 0
getfield org/xbill/DNS/Lookup/timedout Z
ifeq L10
aload 0
iconst_2
putfield org/xbill/DNS/Lookup/result I
aload 0
ldc "timed out"
putfield org/xbill/DNS/Lookup/error Ljava/lang/String;
aload 0
iconst_1
putfield org/xbill/DNS/Lookup/done Z
goto L3
L10:
aload 0
getfield org/xbill/DNS/Lookup/networkerror Z
ifeq L11
aload 0
iconst_2
putfield org/xbill/DNS/Lookup/result I
aload 0
ldc "network error"
putfield org/xbill/DNS/Lookup/error Ljava/lang/String;
aload 0
iconst_1
putfield org/xbill/DNS/Lookup/done Z
goto L3
L11:
aload 0
getfield org/xbill/DNS/Lookup/nxdomain Z
ifeq L12
aload 0
iconst_3
putfield org/xbill/DNS/Lookup/result I
aload 0
iconst_1
putfield org/xbill/DNS/Lookup/done Z
goto L3
L12:
aload 0
getfield org/xbill/DNS/Lookup/referral Z
ifeq L13
aload 0
iconst_1
putfield org/xbill/DNS/Lookup/result I
aload 0
ldc "referral"
putfield org/xbill/DNS/Lookup/error Ljava/lang/String;
aload 0
iconst_1
putfield org/xbill/DNS/Lookup/done Z
goto L3
L13:
aload 0
getfield org/xbill/DNS/Lookup/nametoolong Z
ifeq L3
aload 0
iconst_1
putfield org/xbill/DNS/Lookup/result I
aload 0
ldc "name too long"
putfield org/xbill/DNS/Lookup/error Ljava/lang/String;
aload 0
iconst_1
putfield org/xbill/DNS/Lookup/done Z
goto L3
.limit locals 2
.limit stack 4
.end method

.method public final setCache(Lorg/xbill/DNS/Cache;)V
aload 1
ifnonnull L0
aload 0
new org/xbill/DNS/Cache
dup
aload 0
getfield org/xbill/DNS/Lookup/dclass I
invokespecial org/xbill/DNS/Cache/<init>(I)V
putfield org/xbill/DNS/Lookup/cache Lorg/xbill/DNS/Cache;
aload 0
iconst_1
putfield org/xbill/DNS/Lookup/temporary_cache Z
return
L0:
aload 0
aload 1
putfield org/xbill/DNS/Lookup/cache Lorg/xbill/DNS/Cache;
aload 0
iconst_0
putfield org/xbill/DNS/Lookup/temporary_cache Z
return
.limit locals 2
.limit stack 4
.end method

.method public final setCredibility(I)V
aload 0
iload 1
putfield org/xbill/DNS/Lookup/credibility I
return
.limit locals 2
.limit stack 2
.end method

.method public final setNdots(I)V
iload 1
ifge L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuffer
dup
ldc "Illegal ndots value: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
iload 1
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
iload 1
putstatic org/xbill/DNS/Lookup/defaultNdots I
return
.limit locals 2
.limit stack 5
.end method

.method public final setResolver(Lorg/xbill/DNS/Resolver;)V
aload 0
aload 1
putfield org/xbill/DNS/Lookup/resolver Lorg/xbill/DNS/Resolver;
return
.limit locals 2
.limit stack 2
.end method

.method public final setSearchPath([Ljava/lang/String;)V
aload 1
ifnonnull L0
aload 0
aconst_null
putfield org/xbill/DNS/Lookup/searchPath [Lorg/xbill/DNS/Name;
return
L0:
aload 1
arraylength
anewarray org/xbill/DNS/Name
astore 3
iconst_0
istore 2
L1:
iload 2
aload 1
arraylength
if_icmpge L2
aload 3
iload 2
aload 1
iload 2
aaload
getstatic org/xbill/DNS/Name/root Lorg/xbill/DNS/Name;
invokestatic org/xbill/DNS/Name/fromString(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
aastore
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
aload 0
aload 3
putfield org/xbill/DNS/Lookup/searchPath [Lorg/xbill/DNS/Name;
return
.limit locals 4
.limit stack 4
.end method

.method public final setSearchPath([Lorg/xbill/DNS/Name;)V
aload 0
aload 1
putfield org/xbill/DNS/Lookup/searchPath [Lorg/xbill/DNS/Name;
return
.limit locals 2
.limit stack 2
.end method
