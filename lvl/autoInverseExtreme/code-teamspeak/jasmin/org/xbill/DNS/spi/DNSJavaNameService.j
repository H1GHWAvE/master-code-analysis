.bytecode 50.0
.class public synchronized org/xbill/DNS/spi/DNSJavaNameService
.super java/lang/Object
.implements java/lang/reflect/InvocationHandler

.field static 'array$$B' Ljava/lang/Class;

.field static 'array$Ljava$net$InetAddress' Ljava/lang/Class;

.field private static final 'domainProperty' Ljava/lang/String; = "sun.net.spi.nameservice.domain"

.field private static final 'nsProperty' Ljava/lang/String; = "sun.net.spi.nameservice.nameservers"

.field private static final 'v6Property' Ljava/lang/String; = "java.net.preferIPv6Addresses"

.field private 'preferV6' Z

.method protected <init>()V
.catch java/net/UnknownHostException from L0 to L1 using L2
.catch org/xbill/DNS/TextParseException from L3 to L4 using L5
iconst_0
istore 1
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield org/xbill/DNS/spi/DNSJavaNameService/preferV6 Z
ldc "sun.net.spi.nameservice.nameservers"
invokestatic java/lang/System/getProperty(Ljava/lang/String;)Ljava/lang/String;
astore 4
ldc "sun.net.spi.nameservice.domain"
invokestatic java/lang/System/getProperty(Ljava/lang/String;)Ljava/lang/String;
astore 3
ldc "java.net.preferIPv6Addresses"
invokestatic java/lang/System/getProperty(Ljava/lang/String;)Ljava/lang/String;
astore 2
aload 4
ifnull L1
new java/util/StringTokenizer
dup
aload 4
ldc ","
invokespecial java/util/StringTokenizer/<init>(Ljava/lang/String;Ljava/lang/String;)V
astore 4
aload 4
invokevirtual java/util/StringTokenizer/countTokens()I
anewarray java/lang/String
astore 5
L6:
aload 4
invokevirtual java/util/StringTokenizer/hasMoreTokens()Z
ifeq L0
aload 5
iload 1
aload 4
invokevirtual java/util/StringTokenizer/nextToken()Ljava/lang/String;
aastore
iload 1
iconst_1
iadd
istore 1
goto L6
L0:
new org/xbill/DNS/ExtendedResolver
dup
aload 5
invokespecial org/xbill/DNS/ExtendedResolver/<init>([Ljava/lang/String;)V
invokestatic org/xbill/DNS/Lookup/setDefaultResolver(Lorg/xbill/DNS/Resolver;)V
L1:
aload 3
ifnull L4
L3:
iconst_1
anewarray java/lang/String
dup
iconst_0
aload 3
aastore
invokestatic org/xbill/DNS/Lookup/setDefaultSearchPath([Ljava/lang/String;)V
L4:
aload 2
ifnull L7
aload 2
ldc "true"
invokevirtual java/lang/String/equalsIgnoreCase(Ljava/lang/String;)Z
ifeq L7
aload 0
iconst_1
putfield org/xbill/DNS/spi/DNSJavaNameService/preferV6 Z
L7:
return
L2:
astore 4
getstatic java/lang/System/err Ljava/io/PrintStream;
ldc "DNSJavaNameService: invalid sun.net.spi.nameservice.nameservers"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
goto L1
L5:
astore 3
getstatic java/lang/System/err Ljava/io/PrintStream;
ldc "DNSJavaNameService: invalid sun.net.spi.nameservice.domain"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
goto L4
.limit locals 6
.limit stack 4
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
.catch java/lang/ClassNotFoundException from L0 to L1 using L2
L0:
aload 0
invokestatic java/lang/Class/forName(Ljava/lang/String;)Ljava/lang/Class;
astore 0
L1:
aload 0
areturn
L2:
astore 0
new java/lang/NoClassDefFoundError
dup
invokespecial java/lang/NoClassDefFoundError/<init>()V
aload 0
invokevirtual java/lang/NoClassDefFoundError/initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
athrow
.limit locals 1
.limit stack 2
.end method

.method public getHostByAddr([B)Ljava/lang/String;
new org/xbill/DNS/Lookup
dup
aload 1
invokestatic java/net/InetAddress/getByAddress([B)Ljava/net/InetAddress;
invokestatic org/xbill/DNS/ReverseMap/fromAddress(Ljava/net/InetAddress;)Lorg/xbill/DNS/Name;
bipush 12
invokespecial org/xbill/DNS/Lookup/<init>(Lorg/xbill/DNS/Name;I)V
invokevirtual org/xbill/DNS/Lookup/run()[Lorg/xbill/DNS/Record;
astore 1
aload 1
ifnonnull L0
new java/net/UnknownHostException
dup
invokespecial java/net/UnknownHostException/<init>()V
athrow
L0:
aload 1
iconst_0
aaload
checkcast org/xbill/DNS/PTRRecord
invokevirtual org/xbill/DNS/PTRRecord/getTarget()Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 4
.end method

.method public invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
.catch java/lang/Throwable from L0 to L1 using L2
.catch java/lang/Throwable from L1 to L3 using L2
.catch java/lang/Throwable from L3 to L4 using L2
.catch java/lang/Throwable from L4 to L5 using L2
.catch java/lang/Throwable from L6 to L7 using L2
.catch java/lang/Throwable from L8 to L9 using L2
.catch java/lang/Throwable from L10 to L11 using L2
iconst_0
istore 4
L0:
aload 2
invokevirtual java/lang/reflect/Method/getName()Ljava/lang/String;
ldc "getHostByAddr"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L1
aload 0
aload 3
iconst_0
aaload
checkcast [B
checkcast [B
invokevirtual org/xbill/DNS/spi/DNSJavaNameService/getHostByAddr([B)Ljava/lang/String;
areturn
L1:
aload 2
invokevirtual java/lang/reflect/Method/getName()Ljava/lang/String;
ldc "lookupAllHostAddr"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L12
aload 0
aload 3
iconst_0
aaload
checkcast java/lang/String
invokevirtual org/xbill/DNS/spi/DNSJavaNameService/lookupAllHostAddr(Ljava/lang/String;)[Ljava/net/InetAddress;
astore 3
aload 2
invokevirtual java/lang/reflect/Method/getReturnType()Ljava/lang/Class;
astore 2
getstatic org/xbill/DNS/spi/DNSJavaNameService/array$Ljava$net$InetAddress Ljava/lang/Class;
ifnonnull L8
ldc "[Ljava.net.InetAddress;"
invokestatic org/xbill/DNS/spi/DNSJavaNameService/class$(Ljava/lang/String;)Ljava/lang/Class;
astore 1
aload 1
putstatic org/xbill/DNS/spi/DNSJavaNameService/array$Ljava$net$InetAddress Ljava/lang/Class;
L3:
aload 2
aload 1
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifne L13
getstatic org/xbill/DNS/spi/DNSJavaNameService/array$$B Ljava/lang/Class;
ifnonnull L10
ldc "[[B"
invokestatic org/xbill/DNS/spi/DNSJavaNameService/class$(Ljava/lang/String;)Ljava/lang/Class;
astore 1
aload 1
putstatic org/xbill/DNS/spi/DNSJavaNameService/array$$B Ljava/lang/Class;
L4:
aload 2
aload 1
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L12
aload 3
arraylength
istore 5
iload 5
anewarray [B
astore 1
L5:
iload 4
iload 5
if_icmpge L14
L6:
aload 1
iload 4
aload 3
iload 4
aaload
invokevirtual java/net/InetAddress/getAddress()[B
aastore
L7:
iload 4
iconst_1
iadd
istore 4
goto L5
L8:
getstatic org/xbill/DNS/spi/DNSJavaNameService/array$Ljava$net$InetAddress Ljava/lang/Class;
astore 1
L9:
goto L3
L10:
getstatic org/xbill/DNS/spi/DNSJavaNameService/array$$B Ljava/lang/Class;
astore 1
L11:
goto L4
L14:
aload 1
areturn
L2:
astore 1
getstatic java/lang/System/err Ljava/io/PrintStream;
ldc "DNSJavaNameService: Unexpected error."
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/Throwable/printStackTrace()V
aload 1
athrow
L12:
new java/lang/IllegalArgumentException
dup
ldc "Unknown function name or arguments."
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L13:
aload 3
areturn
.limit locals 6
.limit stack 4
.end method

.method public lookupAllHostAddr(Ljava/lang/String;)[Ljava/net/InetAddress;
.catch org/xbill/DNS/TextParseException from L0 to L1 using L2
L0:
new org/xbill/DNS/Name
dup
aload 1
invokespecial org/xbill/DNS/Name/<init>(Ljava/lang/String;)V
astore 5
L1:
aconst_null
astore 3
aload 0
getfield org/xbill/DNS/spi/DNSJavaNameService/preferV6 Z
ifeq L3
new org/xbill/DNS/Lookup
dup
aload 5
bipush 28
invokespecial org/xbill/DNS/Lookup/<init>(Lorg/xbill/DNS/Name;I)V
invokevirtual org/xbill/DNS/Lookup/run()[Lorg/xbill/DNS/Record;
astore 3
L3:
aload 3
astore 4
aload 3
ifnonnull L4
new org/xbill/DNS/Lookup
dup
aload 5
iconst_1
invokespecial org/xbill/DNS/Lookup/<init>(Lorg/xbill/DNS/Name;I)V
invokevirtual org/xbill/DNS/Lookup/run()[Lorg/xbill/DNS/Record;
astore 4
L4:
aload 4
ifnonnull L5
aload 0
getfield org/xbill/DNS/spi/DNSJavaNameService/preferV6 Z
ifne L5
new org/xbill/DNS/Lookup
dup
aload 5
bipush 28
invokespecial org/xbill/DNS/Lookup/<init>(Lorg/xbill/DNS/Name;I)V
invokevirtual org/xbill/DNS/Lookup/run()[Lorg/xbill/DNS/Record;
astore 3
L6:
aload 3
ifnonnull L7
new java/net/UnknownHostException
dup
aload 1
invokespecial java/net/UnknownHostException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 3
new java/net/UnknownHostException
dup
aload 1
invokespecial java/net/UnknownHostException/<init>(Ljava/lang/String;)V
athrow
L7:
aload 3
arraylength
anewarray java/net/InetAddress
astore 1
iconst_0
istore 2
L8:
iload 2
aload 3
arraylength
if_icmpge L9
aload 3
iload 2
aaload
instanceof org/xbill/DNS/ARecord
ifeq L10
aload 1
iload 2
aload 3
iload 2
aaload
checkcast org/xbill/DNS/ARecord
invokevirtual org/xbill/DNS/ARecord/getAddress()Ljava/net/InetAddress;
aastore
L11:
iload 2
iconst_1
iadd
istore 2
goto L8
L10:
aload 1
iload 2
aload 3
iload 2
aaload
checkcast org/xbill/DNS/AAAARecord
invokevirtual org/xbill/DNS/AAAARecord/getAddress()Ljava/net/InetAddress;
aastore
goto L11
L9:
aload 1
areturn
L5:
aload 4
astore 3
goto L6
.limit locals 6
.limit stack 4
.end method
