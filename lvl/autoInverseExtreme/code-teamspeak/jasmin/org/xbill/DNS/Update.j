.bytecode 50.0
.class public synchronized org/xbill/DNS/Update
.super org/xbill/DNS/Message

.field private 'dclass' I

.field private 'origin' Lorg/xbill/DNS/Name;

.method public <init>(Lorg/xbill/DNS/Name;)V
aload 0
aload 1
iconst_1
invokespecial org/xbill/DNS/Update/<init>(Lorg/xbill/DNS/Name;I)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Lorg/xbill/DNS/Name;I)V
aload 0
invokespecial org/xbill/DNS/Message/<init>()V
aload 1
invokevirtual org/xbill/DNS/Name/isAbsolute()Z
ifne L0
new org/xbill/DNS/RelativeNameException
dup
aload 1
invokespecial org/xbill/DNS/RelativeNameException/<init>(Lorg/xbill/DNS/Name;)V
athrow
L0:
iload 2
invokestatic org/xbill/DNS/DClass/check(I)V
aload 0
invokevirtual org/xbill/DNS/Update/getHeader()Lorg/xbill/DNS/Header;
iconst_5
invokevirtual org/xbill/DNS/Header/setOpcode(I)V
aload 0
aload 1
bipush 6
iconst_1
invokestatic org/xbill/DNS/Record/newRecord(Lorg/xbill/DNS/Name;II)Lorg/xbill/DNS/Record;
iconst_0
invokevirtual org/xbill/DNS/Update/addRecord(Lorg/xbill/DNS/Record;I)V
aload 0
aload 1
putfield org/xbill/DNS/Update/origin Lorg/xbill/DNS/Name;
aload 0
iload 2
putfield org/xbill/DNS/Update/dclass I
return
.limit locals 3
.limit stack 4
.end method

.method private newPrereq(Lorg/xbill/DNS/Record;)V
aload 0
aload 1
iconst_1
invokevirtual org/xbill/DNS/Update/addRecord(Lorg/xbill/DNS/Record;I)V
return
.limit locals 2
.limit stack 3
.end method

.method private newUpdate(Lorg/xbill/DNS/Record;)V
aload 0
aload 1
iconst_2
invokevirtual org/xbill/DNS/Update/addRecord(Lorg/xbill/DNS/Record;I)V
return
.limit locals 2
.limit stack 3
.end method

.method public absent(Lorg/xbill/DNS/Name;)V
aload 0
aload 1
sipush 255
sipush 254
lconst_0
invokestatic org/xbill/DNS/Record/newRecord(Lorg/xbill/DNS/Name;IIJ)Lorg/xbill/DNS/Record;
invokespecial org/xbill/DNS/Update/newPrereq(Lorg/xbill/DNS/Record;)V
return
.limit locals 2
.limit stack 6
.end method

.method public absent(Lorg/xbill/DNS/Name;I)V
aload 0
aload 1
iload 2
sipush 254
lconst_0
invokestatic org/xbill/DNS/Record/newRecord(Lorg/xbill/DNS/Name;IIJ)Lorg/xbill/DNS/Record;
invokespecial org/xbill/DNS/Update/newPrereq(Lorg/xbill/DNS/Record;)V
return
.limit locals 3
.limit stack 6
.end method

.method public add(Lorg/xbill/DNS/Name;IJLjava/lang/String;)V
aload 0
aload 1
iload 2
aload 0
getfield org/xbill/DNS/Update/dclass I
lload 3
aload 5
aload 0
getfield org/xbill/DNS/Update/origin Lorg/xbill/DNS/Name;
invokestatic org/xbill/DNS/Record/fromString(Lorg/xbill/DNS/Name;IIJLjava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Record;
invokespecial org/xbill/DNS/Update/newUpdate(Lorg/xbill/DNS/Record;)V
return
.limit locals 6
.limit stack 8
.end method

.method public add(Lorg/xbill/DNS/Name;IJLorg/xbill/DNS/Tokenizer;)V
aload 0
aload 1
iload 2
aload 0
getfield org/xbill/DNS/Update/dclass I
lload 3
aload 5
aload 0
getfield org/xbill/DNS/Update/origin Lorg/xbill/DNS/Name;
invokestatic org/xbill/DNS/Record/fromString(Lorg/xbill/DNS/Name;IIJLorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Record;
invokespecial org/xbill/DNS/Update/newUpdate(Lorg/xbill/DNS/Record;)V
return
.limit locals 6
.limit stack 8
.end method

.method public add(Lorg/xbill/DNS/RRset;)V
aload 1
invokevirtual org/xbill/DNS/RRset/rrs()Ljava/util/Iterator;
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast org/xbill/DNS/Record
invokevirtual org/xbill/DNS/Update/add(Lorg/xbill/DNS/Record;)V
goto L0
L1:
return
.limit locals 2
.limit stack 2
.end method

.method public add(Lorg/xbill/DNS/Record;)V
aload 0
aload 1
invokespecial org/xbill/DNS/Update/newUpdate(Lorg/xbill/DNS/Record;)V
return
.limit locals 2
.limit stack 2
.end method

.method public add([Lorg/xbill/DNS/Record;)V
iconst_0
istore 2
L0:
iload 2
aload 1
arraylength
if_icmpge L1
aload 0
aload 1
iload 2
aaload
invokevirtual org/xbill/DNS/Update/add(Lorg/xbill/DNS/Record;)V
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 3
.limit stack 3
.end method

.method public delete(Lorg/xbill/DNS/Name;)V
aload 0
aload 1
sipush 255
sipush 255
lconst_0
invokestatic org/xbill/DNS/Record/newRecord(Lorg/xbill/DNS/Name;IIJ)Lorg/xbill/DNS/Record;
invokespecial org/xbill/DNS/Update/newUpdate(Lorg/xbill/DNS/Record;)V
return
.limit locals 2
.limit stack 6
.end method

.method public delete(Lorg/xbill/DNS/Name;I)V
aload 0
aload 1
iload 2
sipush 255
lconst_0
invokestatic org/xbill/DNS/Record/newRecord(Lorg/xbill/DNS/Name;IIJ)Lorg/xbill/DNS/Record;
invokespecial org/xbill/DNS/Update/newUpdate(Lorg/xbill/DNS/Record;)V
return
.limit locals 3
.limit stack 6
.end method

.method public delete(Lorg/xbill/DNS/Name;ILjava/lang/String;)V
aload 0
aload 1
iload 2
sipush 254
lconst_0
aload 3
aload 0
getfield org/xbill/DNS/Update/origin Lorg/xbill/DNS/Name;
invokestatic org/xbill/DNS/Record/fromString(Lorg/xbill/DNS/Name;IIJLjava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Record;
invokespecial org/xbill/DNS/Update/newUpdate(Lorg/xbill/DNS/Record;)V
return
.limit locals 4
.limit stack 8
.end method

.method public delete(Lorg/xbill/DNS/Name;ILorg/xbill/DNS/Tokenizer;)V
aload 0
aload 1
iload 2
sipush 254
lconst_0
aload 3
aload 0
getfield org/xbill/DNS/Update/origin Lorg/xbill/DNS/Name;
invokestatic org/xbill/DNS/Record/fromString(Lorg/xbill/DNS/Name;IIJLorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Record;
invokespecial org/xbill/DNS/Update/newUpdate(Lorg/xbill/DNS/Record;)V
return
.limit locals 4
.limit stack 8
.end method

.method public delete(Lorg/xbill/DNS/RRset;)V
aload 1
invokevirtual org/xbill/DNS/RRset/rrs()Ljava/util/Iterator;
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast org/xbill/DNS/Record
invokevirtual org/xbill/DNS/Update/delete(Lorg/xbill/DNS/Record;)V
goto L0
L1:
return
.limit locals 2
.limit stack 2
.end method

.method public delete(Lorg/xbill/DNS/Record;)V
aload 0
aload 1
sipush 254
lconst_0
invokevirtual org/xbill/DNS/Record/withDClass(IJ)Lorg/xbill/DNS/Record;
invokespecial org/xbill/DNS/Update/newUpdate(Lorg/xbill/DNS/Record;)V
return
.limit locals 2
.limit stack 5
.end method

.method public delete([Lorg/xbill/DNS/Record;)V
iconst_0
istore 2
L0:
iload 2
aload 1
arraylength
if_icmpge L1
aload 0
aload 1
iload 2
aaload
invokevirtual org/xbill/DNS/Update/delete(Lorg/xbill/DNS/Record;)V
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 3
.limit stack 3
.end method

.method public present(Lorg/xbill/DNS/Name;)V
aload 0
aload 1
sipush 255
sipush 255
lconst_0
invokestatic org/xbill/DNS/Record/newRecord(Lorg/xbill/DNS/Name;IIJ)Lorg/xbill/DNS/Record;
invokespecial org/xbill/DNS/Update/newPrereq(Lorg/xbill/DNS/Record;)V
return
.limit locals 2
.limit stack 6
.end method

.method public present(Lorg/xbill/DNS/Name;I)V
aload 0
aload 1
iload 2
sipush 255
lconst_0
invokestatic org/xbill/DNS/Record/newRecord(Lorg/xbill/DNS/Name;IIJ)Lorg/xbill/DNS/Record;
invokespecial org/xbill/DNS/Update/newPrereq(Lorg/xbill/DNS/Record;)V
return
.limit locals 3
.limit stack 6
.end method

.method public present(Lorg/xbill/DNS/Name;ILjava/lang/String;)V
aload 0
aload 1
iload 2
aload 0
getfield org/xbill/DNS/Update/dclass I
lconst_0
aload 3
aload 0
getfield org/xbill/DNS/Update/origin Lorg/xbill/DNS/Name;
invokestatic org/xbill/DNS/Record/fromString(Lorg/xbill/DNS/Name;IIJLjava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Record;
invokespecial org/xbill/DNS/Update/newPrereq(Lorg/xbill/DNS/Record;)V
return
.limit locals 4
.limit stack 8
.end method

.method public present(Lorg/xbill/DNS/Name;ILorg/xbill/DNS/Tokenizer;)V
aload 0
aload 1
iload 2
aload 0
getfield org/xbill/DNS/Update/dclass I
lconst_0
aload 3
aload 0
getfield org/xbill/DNS/Update/origin Lorg/xbill/DNS/Name;
invokestatic org/xbill/DNS/Record/fromString(Lorg/xbill/DNS/Name;IIJLorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Record;
invokespecial org/xbill/DNS/Update/newPrereq(Lorg/xbill/DNS/Record;)V
return
.limit locals 4
.limit stack 8
.end method

.method public present(Lorg/xbill/DNS/Record;)V
aload 0
aload 1
invokespecial org/xbill/DNS/Update/newPrereq(Lorg/xbill/DNS/Record;)V
return
.limit locals 2
.limit stack 2
.end method

.method public replace(Lorg/xbill/DNS/Name;IJLjava/lang/String;)V
aload 0
aload 1
iload 2
invokevirtual org/xbill/DNS/Update/delete(Lorg/xbill/DNS/Name;I)V
aload 0
aload 1
iload 2
lload 3
aload 5
invokevirtual org/xbill/DNS/Update/add(Lorg/xbill/DNS/Name;IJLjava/lang/String;)V
return
.limit locals 6
.limit stack 6
.end method

.method public replace(Lorg/xbill/DNS/Name;IJLorg/xbill/DNS/Tokenizer;)V
aload 0
aload 1
iload 2
invokevirtual org/xbill/DNS/Update/delete(Lorg/xbill/DNS/Name;I)V
aload 0
aload 1
iload 2
lload 3
aload 5
invokevirtual org/xbill/DNS/Update/add(Lorg/xbill/DNS/Name;IJLorg/xbill/DNS/Tokenizer;)V
return
.limit locals 6
.limit stack 6
.end method

.method public replace(Lorg/xbill/DNS/RRset;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/RRset/getName()Lorg/xbill/DNS/Name;
aload 1
invokevirtual org/xbill/DNS/RRset/getType()I
invokevirtual org/xbill/DNS/Update/delete(Lorg/xbill/DNS/Name;I)V
aload 1
invokevirtual org/xbill/DNS/RRset/rrs()Ljava/util/Iterator;
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast org/xbill/DNS/Record
invokevirtual org/xbill/DNS/Update/add(Lorg/xbill/DNS/Record;)V
goto L0
L1:
return
.limit locals 2
.limit stack 3
.end method

.method public replace(Lorg/xbill/DNS/Record;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/Record/getName()Lorg/xbill/DNS/Name;
aload 1
invokevirtual org/xbill/DNS/Record/getType()I
invokevirtual org/xbill/DNS/Update/delete(Lorg/xbill/DNS/Name;I)V
aload 0
aload 1
invokevirtual org/xbill/DNS/Update/add(Lorg/xbill/DNS/Record;)V
return
.limit locals 2
.limit stack 3
.end method

.method public replace([Lorg/xbill/DNS/Record;)V
iconst_0
istore 2
L0:
iload 2
aload 1
arraylength
if_icmpge L1
aload 0
aload 1
iload 2
aaload
invokevirtual org/xbill/DNS/Update/replace(Lorg/xbill/DNS/Record;)V
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 3
.limit stack 3
.end method
