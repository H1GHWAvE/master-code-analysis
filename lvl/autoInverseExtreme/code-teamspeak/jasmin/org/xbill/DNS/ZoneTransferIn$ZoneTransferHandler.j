.bytecode 50.0
.class public abstract interface org/xbill/DNS/ZoneTransferIn$ZoneTransferHandler
.super java/lang/Object

.method public abstract handleRecord(Lorg/xbill/DNS/Record;)V
.end method

.method public abstract startAXFR()V
.end method

.method public abstract startIXFR()V
.end method

.method public abstract startIXFRAdds(Lorg/xbill/DNS/Record;)V
.end method

.method public abstract startIXFRDeletes(Lorg/xbill/DNS/Record;)V
.end method
