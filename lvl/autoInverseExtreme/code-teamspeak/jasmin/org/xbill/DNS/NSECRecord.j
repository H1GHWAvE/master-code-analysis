.bytecode 50.0
.class public synchronized org/xbill/DNS/NSECRecord
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = -5165065768816265385L


.field private 'next' Lorg/xbill/DNS/Name;

.field private 'types' Lorg/xbill/DNS/TypeBitmap;

.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJLorg/xbill/DNS/Name;[I)V
aload 0
aload 1
bipush 47
iload 2
lload 3
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
aload 0
ldc "next"
aload 5
invokestatic org/xbill/DNS/NSECRecord/checkName(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/NSECRecord/next Lorg/xbill/DNS/Name;
iconst_0
istore 2
L0:
iload 2
aload 6
arraylength
if_icmpge L1
aload 6
iload 2
iaload
invokestatic org/xbill/DNS/Type/check(I)V
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 0
new org/xbill/DNS/TypeBitmap
dup
aload 6
invokespecial org/xbill/DNS/TypeBitmap/<init>([I)V
putfield org/xbill/DNS/NSECRecord/types Lorg/xbill/DNS/TypeBitmap;
return
.limit locals 7
.limit stack 6
.end method

.method public getNext()Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/NSECRecord/next Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/NSECRecord
dup
invokespecial org/xbill/DNS/NSECRecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method public getTypes()[I
aload 0
getfield org/xbill/DNS/NSECRecord/types Lorg/xbill/DNS/TypeBitmap;
invokevirtual org/xbill/DNS/TypeBitmap/toArray()[I
areturn
.limit locals 1
.limit stack 1
.end method

.method public hasType(I)Z
aload 0
getfield org/xbill/DNS/NSECRecord/types Lorg/xbill/DNS/TypeBitmap;
iload 1
invokevirtual org/xbill/DNS/TypeBitmap/contains(I)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
aload 0
aload 1
aload 2
invokevirtual org/xbill/DNS/Tokenizer/getName(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/NSECRecord/next Lorg/xbill/DNS/Name;
aload 0
new org/xbill/DNS/TypeBitmap
dup
aload 1
invokespecial org/xbill/DNS/TypeBitmap/<init>(Lorg/xbill/DNS/Tokenizer;)V
putfield org/xbill/DNS/NSECRecord/types Lorg/xbill/DNS/TypeBitmap;
return
.limit locals 3
.limit stack 4
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
new org/xbill/DNS/Name
dup
aload 1
invokespecial org/xbill/DNS/Name/<init>(Lorg/xbill/DNS/DNSInput;)V
putfield org/xbill/DNS/NSECRecord/next Lorg/xbill/DNS/Name;
aload 0
new org/xbill/DNS/TypeBitmap
dup
aload 1
invokespecial org/xbill/DNS/TypeBitmap/<init>(Lorg/xbill/DNS/DNSInput;)V
putfield org/xbill/DNS/NSECRecord/types Lorg/xbill/DNS/TypeBitmap;
return
.limit locals 2
.limit stack 4
.end method

.method rrToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 1
aload 0
getfield org/xbill/DNS/NSECRecord/next Lorg/xbill/DNS/Name;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
pop
aload 0
getfield org/xbill/DNS/NSECRecord/types Lorg/xbill/DNS/TypeBitmap;
invokevirtual org/xbill/DNS/TypeBitmap/empty()Z
ifne L0
aload 1
bipush 32
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/NSECRecord/types Lorg/xbill/DNS/TypeBitmap;
invokevirtual org/xbill/DNS/TypeBitmap/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L0:
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 0
getfield org/xbill/DNS/NSECRecord/next Lorg/xbill/DNS/Name;
aload 1
aconst_null
iconst_0
invokevirtual org/xbill/DNS/Name/toWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 0
getfield org/xbill/DNS/NSECRecord/types Lorg/xbill/DNS/TypeBitmap;
aload 1
invokevirtual org/xbill/DNS/TypeBitmap/toWire(Lorg/xbill/DNS/DNSOutput;)V
return
.limit locals 4
.limit stack 4
.end method
