.bytecode 50.0
.class public synchronized org/xbill/DNS/DHCIDRecord
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = -8214820200808997707L


.field private 'data' [B

.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJ[B)V
aload 0
aload 1
bipush 49
iload 2
lload 3
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
aload 0
aload 5
putfield org/xbill/DNS/DHCIDRecord/data [B
return
.limit locals 6
.limit stack 6
.end method

.method public getData()[B
aload 0
getfield org/xbill/DNS/DHCIDRecord/data [B
areturn
.limit locals 1
.limit stack 1
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/DHCIDRecord
dup
invokespecial org/xbill/DNS/DHCIDRecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getBase64()[B
putfield org/xbill/DNS/DHCIDRecord/data [B
return
.limit locals 3
.limit stack 2
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readByteArray()[B
putfield org/xbill/DNS/DHCIDRecord/data [B
return
.limit locals 2
.limit stack 2
.end method

.method rrToString()Ljava/lang/String;
aload 0
getfield org/xbill/DNS/DHCIDRecord/data [B
invokestatic org/xbill/DNS/utils/base64/toString([B)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
aload 0
getfield org/xbill/DNS/DHCIDRecord/data [B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
return
.limit locals 4
.limit stack 2
.end method
