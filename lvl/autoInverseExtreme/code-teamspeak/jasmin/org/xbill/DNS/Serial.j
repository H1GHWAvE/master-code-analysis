.bytecode 50.0
.class public final synchronized org/xbill/DNS/Serial
.super java/lang/Object

.field private static final 'MAX32' J = 4294967295L


.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static compare(JJ)I
lload 0
lconst_0
lcmp
iflt L0
lload 0
ldc2_w 4294967295L
lcmp
ifle L1
L0:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
lload 0
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
ldc " out of range"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
lload 2
lconst_0
lcmp
iflt L2
lload 2
ldc2_w 4294967295L
lcmp
ifle L3
L2:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
lload 2
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
ldc " out of range"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L3:
lload 0
lload 2
lsub
lstore 2
lload 2
ldc2_w 4294967295L
lcmp
iflt L4
lload 2
ldc2_w 4294967296L
lsub
lstore 0
L5:
lload 0
l2i
ireturn
L4:
lload 2
lstore 0
lload 2
ldc2_w -4294967295L
lcmp
ifge L5
lload 2
ldc2_w 4294967296L
ladd
lstore 0
goto L5
.limit locals 4
.limit stack 5
.end method

.method public static increment(J)J
lload 0
lconst_0
lcmp
iflt L0
lload 0
ldc2_w 4294967295L
lcmp
ifle L1
L0:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
lload 0
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
ldc " out of range"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
lload 0
ldc2_w 4294967295L
lcmp
ifne L2
lconst_0
lreturn
L2:
lconst_1
lload 0
ladd
lreturn
.limit locals 2
.limit stack 5
.end method
