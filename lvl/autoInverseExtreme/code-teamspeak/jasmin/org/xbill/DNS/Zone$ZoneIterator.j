.bytecode 50.0
.class synchronized org/xbill/DNS/Zone$ZoneIterator
.super java/lang/Object
.implements java/util/Iterator

.field private 'count' I

.field private 'current' [Lorg/xbill/DNS/RRset;

.field private final 'this$0' Lorg/xbill/DNS/Zone;

.field private 'wantLastSOA' Z

.field private 'zentries' Ljava/util/Iterator;

.method <init>(Lorg/xbill/DNS/Zone;Z)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
aload 1
putfield org/xbill/DNS/Zone$ZoneIterator/this$0 Lorg/xbill/DNS/Zone;
aload 0
invokespecial java/lang/Object/<init>()V
aload 1
monitorenter
L0:
aload 0
aload 1
invokestatic org/xbill/DNS/Zone/access$000(Lorg/xbill/DNS/Zone;)Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
putfield org/xbill/DNS/Zone$ZoneIterator/zentries Ljava/util/Iterator;
aload 1
monitorexit
L1:
aload 0
iload 2
putfield org/xbill/DNS/Zone$ZoneIterator/wantLastSOA Z
aload 1
aload 1
invokestatic org/xbill/DNS/Zone/access$100(Lorg/xbill/DNS/Zone;)Ljava/lang/Object;
invokestatic org/xbill/DNS/Zone/access$200(Lorg/xbill/DNS/Zone;Ljava/lang/Object;)[Lorg/xbill/DNS/RRset;
astore 1
aload 0
aload 1
arraylength
anewarray org/xbill/DNS/RRset
putfield org/xbill/DNS/Zone$ZoneIterator/current [Lorg/xbill/DNS/RRset;
iconst_2
istore 4
iconst_0
istore 3
L5:
iload 3
aload 1
arraylength
if_icmpge L6
aload 1
iload 3
aaload
invokevirtual org/xbill/DNS/RRset/getType()I
istore 5
iload 5
bipush 6
if_icmpne L7
aload 0
getfield org/xbill/DNS/Zone$ZoneIterator/current [Lorg/xbill/DNS/RRset;
iconst_0
aload 1
iload 3
aaload
aastore
L8:
iload 3
iconst_1
iadd
istore 3
goto L5
L2:
astore 6
L3:
aload 1
monitorexit
L4:
aload 6
athrow
L7:
iload 5
iconst_2
if_icmpne L9
aload 0
getfield org/xbill/DNS/Zone$ZoneIterator/current [Lorg/xbill/DNS/RRset;
iconst_1
aload 1
iload 3
aaload
aastore
goto L8
L9:
aload 0
getfield org/xbill/DNS/Zone$ZoneIterator/current [Lorg/xbill/DNS/RRset;
iload 4
aload 1
iload 3
aaload
aastore
iload 4
iconst_1
iadd
istore 4
goto L8
L6:
return
.limit locals 7
.limit stack 4
.end method

.method public hasNext()Z
aload 0
getfield org/xbill/DNS/Zone$ZoneIterator/current [Lorg/xbill/DNS/RRset;
ifnonnull L0
aload 0
getfield org/xbill/DNS/Zone$ZoneIterator/wantLastSOA Z
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public next()Ljava/lang/Object;
aload 0
invokevirtual org/xbill/DNS/Zone$ZoneIterator/hasNext()Z
ifne L0
new java/util/NoSuchElementException
dup
invokespecial java/util/NoSuchElementException/<init>()V
athrow
L0:
aload 0
getfield org/xbill/DNS/Zone$ZoneIterator/current [Lorg/xbill/DNS/RRset;
ifnonnull L1
aload 0
iconst_0
putfield org/xbill/DNS/Zone$ZoneIterator/wantLastSOA Z
aload 0
getfield org/xbill/DNS/Zone$ZoneIterator/this$0 Lorg/xbill/DNS/Zone;
aload 0
getfield org/xbill/DNS/Zone$ZoneIterator/this$0 Lorg/xbill/DNS/Zone;
invokestatic org/xbill/DNS/Zone/access$100(Lorg/xbill/DNS/Zone;)Ljava/lang/Object;
bipush 6
invokestatic org/xbill/DNS/Zone/access$300(Lorg/xbill/DNS/Zone;Ljava/lang/Object;I)Lorg/xbill/DNS/RRset;
areturn
L1:
aload 0
getfield org/xbill/DNS/Zone$ZoneIterator/current [Lorg/xbill/DNS/RRset;
astore 2
aload 0
getfield org/xbill/DNS/Zone$ZoneIterator/count I
istore 1
aload 0
iload 1
iconst_1
iadd
putfield org/xbill/DNS/Zone$ZoneIterator/count I
aload 2
iload 1
aaload
astore 2
aload 0
getfield org/xbill/DNS/Zone$ZoneIterator/count I
aload 0
getfield org/xbill/DNS/Zone$ZoneIterator/current [Lorg/xbill/DNS/RRset;
arraylength
if_icmpne L2
aload 0
aconst_null
putfield org/xbill/DNS/Zone$ZoneIterator/current [Lorg/xbill/DNS/RRset;
L3:
aload 0
getfield org/xbill/DNS/Zone$ZoneIterator/zentries Ljava/util/Iterator;
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 0
getfield org/xbill/DNS/Zone$ZoneIterator/zentries Ljava/util/Iterator;
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 3
aload 3
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
aload 0
getfield org/xbill/DNS/Zone$ZoneIterator/this$0 Lorg/xbill/DNS/Zone;
invokestatic org/xbill/DNS/Zone/access$400(Lorg/xbill/DNS/Zone;)Lorg/xbill/DNS/Name;
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifne L3
aload 0
getfield org/xbill/DNS/Zone$ZoneIterator/this$0 Lorg/xbill/DNS/Zone;
aload 3
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
invokestatic org/xbill/DNS/Zone/access$200(Lorg/xbill/DNS/Zone;Ljava/lang/Object;)[Lorg/xbill/DNS/RRset;
astore 3
aload 3
arraylength
ifeq L3
aload 0
aload 3
putfield org/xbill/DNS/Zone$ZoneIterator/current [Lorg/xbill/DNS/RRset;
aload 0
iconst_0
putfield org/xbill/DNS/Zone$ZoneIterator/count I
L2:
aload 2
areturn
.limit locals 4
.limit stack 3
.end method

.method public remove()V
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 1
.limit stack 2
.end method
