.bytecode 50.0
.class public synchronized org/xbill/DNS/APLRecord$Element
.super java/lang/Object

.field public final 'address' Ljava/lang/Object;

.field public final 'family' I

.field public final 'negative' Z

.field public final 'prefixLength' I

.method private <init>(IZLjava/lang/Object;I)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iload 1
putfield org/xbill/DNS/APLRecord$Element/family I
aload 0
iload 2
putfield org/xbill/DNS/APLRecord$Element/negative Z
aload 0
aload 3
putfield org/xbill/DNS/APLRecord$Element/address Ljava/lang/Object;
aload 0
iload 4
putfield org/xbill/DNS/APLRecord$Element/prefixLength I
iload 1
iload 4
invokestatic org/xbill/DNS/APLRecord/access$000(II)Z
ifne L0
new java/lang/IllegalArgumentException
dup
ldc "invalid prefix length"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
return
.limit locals 5
.limit stack 3
.end method

.method <init>(IZLjava/lang/Object;ILorg/xbill/DNS/APLRecord$1;)V
aload 0
iload 1
iload 2
aload 3
iload 4
invokespecial org/xbill/DNS/APLRecord$Element/<init>(IZLjava/lang/Object;I)V
return
.limit locals 6
.limit stack 5
.end method

.method public <init>(ZLjava/net/InetAddress;I)V
aload 0
aload 2
invokestatic org/xbill/DNS/Address/familyOf(Ljava/net/InetAddress;)I
iload 1
aload 2
iload 3
invokespecial org/xbill/DNS/APLRecord$Element/<init>(IZLjava/lang/Object;I)V
return
.limit locals 4
.limit stack 5
.end method

.method public equals(Ljava/lang/Object;)Z
aload 1
ifnull L0
aload 1
instanceof org/xbill/DNS/APLRecord$Element
ifne L1
L0:
iconst_0
ireturn
L1:
aload 1
checkcast org/xbill/DNS/APLRecord$Element
astore 1
aload 0
getfield org/xbill/DNS/APLRecord$Element/family I
aload 1
getfield org/xbill/DNS/APLRecord$Element/family I
if_icmpne L0
aload 0
getfield org/xbill/DNS/APLRecord$Element/negative Z
aload 1
getfield org/xbill/DNS/APLRecord$Element/negative Z
if_icmpne L0
aload 0
getfield org/xbill/DNS/APLRecord$Element/prefixLength I
aload 1
getfield org/xbill/DNS/APLRecord$Element/prefixLength I
if_icmpne L0
aload 0
getfield org/xbill/DNS/APLRecord$Element/address Ljava/lang/Object;
aload 1
getfield org/xbill/DNS/APLRecord$Element/address Ljava/lang/Object;
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L0
iconst_1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public hashCode()I
aload 0
getfield org/xbill/DNS/APLRecord$Element/address Ljava/lang/Object;
invokevirtual java/lang/Object/hashCode()I
istore 2
aload 0
getfield org/xbill/DNS/APLRecord$Element/prefixLength I
istore 3
aload 0
getfield org/xbill/DNS/APLRecord$Element/negative Z
ifeq L0
iconst_1
istore 1
L1:
iload 1
iload 3
iload 2
iadd
iadd
ireturn
L0:
iconst_0
istore 1
goto L1
.limit locals 4
.limit stack 3
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 0
getfield org/xbill/DNS/APLRecord$Element/negative Z
ifeq L0
aload 1
ldc "!"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L0:
aload 1
aload 0
getfield org/xbill/DNS/APLRecord$Element/family I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
ldc ":"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 0
getfield org/xbill/DNS/APLRecord$Element/family I
iconst_1
if_icmpeq L1
aload 0
getfield org/xbill/DNS/APLRecord$Element/family I
iconst_2
if_icmpne L2
L1:
aload 1
aload 0
getfield org/xbill/DNS/APLRecord$Element/address Ljava/lang/Object;
checkcast java/net/InetAddress
invokevirtual java/net/InetAddress/getHostAddress()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L3:
aload 1
ldc "/"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/APLRecord$Element/prefixLength I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
L2:
aload 1
aload 0
getfield org/xbill/DNS/APLRecord$Element/address Ljava/lang/Object;
checkcast [B
checkcast [B
invokestatic org/xbill/DNS/utils/base16/toString([B)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
goto L3
.limit locals 2
.limit stack 2
.end method
