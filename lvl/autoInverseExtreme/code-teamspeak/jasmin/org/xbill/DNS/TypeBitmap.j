.bytecode 50.0
.class final synchronized org/xbill/DNS/TypeBitmap
.super java/lang/Object
.implements java/io/Serializable

.field private static final 'serialVersionUID' J = -125354057735389003L


.field private 'types' Ljava/util/TreeSet;

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/TreeSet
dup
invokespecial java/util/TreeSet/<init>()V
putfield org/xbill/DNS/TypeBitmap/types Ljava/util/TreeSet;
return
.limit locals 1
.limit stack 3
.end method

.method public <init>(Lorg/xbill/DNS/DNSInput;)V
aload 0
invokespecial org/xbill/DNS/TypeBitmap/<init>()V
L0:
aload 1
invokevirtual org/xbill/DNS/DNSInput/remaining()I
ifle L1
aload 1
invokevirtual org/xbill/DNS/DNSInput/remaining()I
iconst_2
if_icmpge L2
new org/xbill/DNS/WireParseException
dup
ldc "invalid bitmap descriptor"
invokespecial org/xbill/DNS/WireParseException/<init>(Ljava/lang/String;)V
athrow
L2:
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
istore 4
iload 4
iconst_m1
if_icmpge L3
new org/xbill/DNS/WireParseException
dup
ldc "invalid ordering"
invokespecial org/xbill/DNS/WireParseException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
istore 5
iload 5
aload 1
invokevirtual org/xbill/DNS/DNSInput/remaining()I
if_icmple L4
new org/xbill/DNS/WireParseException
dup
ldc "invalid bitmap"
invokespecial org/xbill/DNS/WireParseException/<init>(Ljava/lang/String;)V
athrow
L4:
iconst_0
istore 2
L5:
iload 2
iload 5
if_icmpge L0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
istore 6
iload 6
ifeq L6
iconst_0
istore 3
L7:
iload 3
bipush 8
if_icmpge L6
iconst_1
bipush 7
iload 3
isub
ishl
iload 6
iand
ifeq L8
aload 0
getfield org/xbill/DNS/TypeBitmap/types Ljava/util/TreeSet;
iload 4
sipush 256
imul
iload 2
bipush 8
imul
iadd
iload 3
iadd
invokestatic org/xbill/DNS/Mnemonic/toInteger(I)Ljava/lang/Integer;
invokevirtual java/util/TreeSet/add(Ljava/lang/Object;)Z
pop
L8:
iload 3
iconst_1
iadd
istore 3
goto L7
L6:
iload 2
iconst_1
iadd
istore 2
goto L5
L1:
return
.limit locals 7
.limit stack 4
.end method

.method public <init>(Lorg/xbill/DNS/Tokenizer;)V
aload 0
invokespecial org/xbill/DNS/TypeBitmap/<init>()V
L0:
aload 1
invokevirtual org/xbill/DNS/Tokenizer/get()Lorg/xbill/DNS/Tokenizer$Token;
astore 3
aload 3
invokevirtual org/xbill/DNS/Tokenizer$Token/isString()Z
ifeq L1
aload 3
getfield org/xbill/DNS/Tokenizer$Token/value Ljava/lang/String;
invokestatic org/xbill/DNS/Type/value(Ljava/lang/String;)I
istore 2
iload 2
ifge L2
aload 1
new java/lang/StringBuffer
dup
ldc "Invalid type: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 3
getfield org/xbill/DNS/Tokenizer$Token/value Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L2:
aload 0
getfield org/xbill/DNS/TypeBitmap/types Ljava/util/TreeSet;
iload 2
invokestatic org/xbill/DNS/Mnemonic/toInteger(I)Ljava/lang/Integer;
invokevirtual java/util/TreeSet/add(Ljava/lang/Object;)Z
pop
goto L0
L1:
aload 1
invokevirtual org/xbill/DNS/Tokenizer/unget()V
return
.limit locals 4
.limit stack 4
.end method

.method public <init>([I)V
aload 0
invokespecial org/xbill/DNS/TypeBitmap/<init>()V
iconst_0
istore 2
L0:
iload 2
aload 1
arraylength
if_icmpge L1
aload 1
iload 2
iaload
invokestatic org/xbill/DNS/Type/check(I)V
aload 0
getfield org/xbill/DNS/TypeBitmap/types Ljava/util/TreeSet;
new java/lang/Integer
dup
aload 1
iload 2
iaload
invokespecial java/lang/Integer/<init>(I)V
invokevirtual java/util/TreeSet/add(Ljava/lang/Object;)Z
pop
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 3
.limit stack 5
.end method

.method private static mapToWire(Lorg/xbill/DNS/DNSOutput;Ljava/util/TreeSet;I)V
aload 1
invokevirtual java/util/TreeSet/last()Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
sipush 255
iand
bipush 8
idiv
iconst_1
iadd
istore 3
iload 3
newarray int
astore 5
aload 0
iload 2
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 0
iload 3
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 1
invokevirtual java/util/TreeSet/iterator()Ljava/util/Iterator;
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
istore 2
iload 2
sipush 255
iand
bipush 8
idiv
istore 4
aload 5
iload 4
iconst_1
bipush 7
iload 2
bipush 8
irem
isub
ishl
aload 5
iload 4
iaload
ior
iastore
goto L0
L1:
iconst_0
istore 2
L2:
iload 2
iload 3
if_icmpge L3
aload 0
aload 5
iload 2
iaload
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
iload 2
iconst_1
iadd
istore 2
goto L2
L3:
return
.limit locals 6
.limit stack 6
.end method

.method public final contains(I)Z
aload 0
getfield org/xbill/DNS/TypeBitmap/types Ljava/util/TreeSet;
iload 1
invokestatic org/xbill/DNS/Mnemonic/toInteger(I)Ljava/lang/Integer;
invokevirtual java/util/TreeSet/contains(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final empty()Z
aload 0
getfield org/xbill/DNS/TypeBitmap/types Ljava/util/TreeSet;
invokevirtual java/util/TreeSet/isEmpty()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final toArray()[I
aload 0
getfield org/xbill/DNS/TypeBitmap/types Ljava/util/TreeSet;
invokevirtual java/util/TreeSet/size()I
newarray int
astore 2
aload 0
getfield org/xbill/DNS/TypeBitmap/types Ljava/util/TreeSet;
invokevirtual java/util/TreeSet/iterator()Ljava/util/Iterator;
astore 3
iconst_0
istore 1
L0:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
iload 1
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
iastore
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 2
areturn
.limit locals 4
.limit stack 3
.end method

.method public final toString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 0
getfield org/xbill/DNS/TypeBitmap/types Ljava/util/TreeSet;
invokevirtual java/util/TreeSet/iterator()Ljava/util/Iterator;
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
invokestatic org/xbill/DNS/Type/string(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 1
bipush 32
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
goto L0
L1:
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 3
.limit stack 2
.end method

.method public final toWire(Lorg/xbill/DNS/DNSOutput;)V
aload 0
getfield org/xbill/DNS/TypeBitmap/types Ljava/util/TreeSet;
invokevirtual java/util/TreeSet/size()I
ifne L0
return
L0:
new java/util/TreeSet
dup
invokespecial java/util/TreeSet/<init>()V
astore 6
aload 0
getfield org/xbill/DNS/TypeBitmap/types Ljava/util/TreeSet;
invokevirtual java/util/TreeSet/iterator()Ljava/util/Iterator;
astore 7
iconst_m1
istore 2
L1:
aload 7
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 7
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
istore 5
iload 5
bipush 8
ishr
istore 4
iload 4
iload 2
if_icmpeq L3
iload 4
istore 3
aload 6
invokevirtual java/util/TreeSet/size()I
ifle L4
aload 1
aload 6
iload 2
invokestatic org/xbill/DNS/TypeBitmap/mapToWire(Lorg/xbill/DNS/DNSOutput;Ljava/util/TreeSet;I)V
aload 6
invokevirtual java/util/TreeSet/clear()V
iload 4
istore 3
L4:
aload 6
new java/lang/Integer
dup
iload 5
invokespecial java/lang/Integer/<init>(I)V
invokevirtual java/util/TreeSet/add(Ljava/lang/Object;)Z
pop
iload 3
istore 2
goto L1
L2:
aload 1
aload 6
iload 2
invokestatic org/xbill/DNS/TypeBitmap/mapToWire(Lorg/xbill/DNS/DNSOutput;Ljava/util/TreeSet;I)V
return
L3:
iload 2
istore 3
goto L4
.limit locals 8
.limit stack 4
.end method
