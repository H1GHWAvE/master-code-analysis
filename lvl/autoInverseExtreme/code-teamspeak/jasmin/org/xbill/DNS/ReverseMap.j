.bytecode 50.0
.class public final synchronized org/xbill/DNS/ReverseMap
.super java/lang/Object

.field private static 'inaddr4' Lorg/xbill/DNS/Name;

.field private static 'inaddr6' Lorg/xbill/DNS/Name;

.method static <clinit>()V
ldc "in-addr.arpa."
invokestatic org/xbill/DNS/Name/fromConstantString(Ljava/lang/String;)Lorg/xbill/DNS/Name;
putstatic org/xbill/DNS/ReverseMap/inaddr4 Lorg/xbill/DNS/Name;
ldc "ip6.arpa."
invokestatic org/xbill/DNS/Name/fromConstantString(Ljava/lang/String;)Lorg/xbill/DNS/Name;
putstatic org/xbill/DNS/ReverseMap/inaddr6 Lorg/xbill/DNS/Name;
return
.limit locals 0
.limit stack 1
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static fromAddress(Ljava/lang/String;)Lorg/xbill/DNS/Name;
aload 0
iconst_1
invokestatic org/xbill/DNS/Address/toByteArray(Ljava/lang/String;I)[B
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
aload 0
iconst_2
invokestatic org/xbill/DNS/Address/toByteArray(Ljava/lang/String;I)[B
astore 1
L0:
aload 1
ifnonnull L1
new java/net/UnknownHostException
dup
ldc "Invalid IP address"
invokespecial java/net/UnknownHostException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 1
invokestatic org/xbill/DNS/ReverseMap/fromAddress([B)Lorg/xbill/DNS/Name;
areturn
.limit locals 3
.limit stack 3
.end method

.method public static fromAddress(Ljava/lang/String;I)Lorg/xbill/DNS/Name;
aload 0
iload 1
invokestatic org/xbill/DNS/Address/toByteArray(Ljava/lang/String;I)[B
astore 0
aload 0
ifnonnull L0
new java/net/UnknownHostException
dup
ldc "Invalid IP address"
invokespecial java/net/UnknownHostException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
invokestatic org/xbill/DNS/ReverseMap/fromAddress([B)Lorg/xbill/DNS/Name;
areturn
.limit locals 2
.limit stack 3
.end method

.method public static fromAddress(Ljava/net/InetAddress;)Lorg/xbill/DNS/Name;
aload 0
invokevirtual java/net/InetAddress/getAddress()[B
invokestatic org/xbill/DNS/ReverseMap/fromAddress([B)Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method public static fromAddress([B)Lorg/xbill/DNS/Name;
.catch org/xbill/DNS/TextParseException from L0 to L1 using L2
.catch org/xbill/DNS/TextParseException from L1 to L3 using L2
aload 0
arraylength
iconst_4
if_icmpeq L4
aload 0
arraylength
bipush 16
if_icmpeq L4
new java/lang/IllegalArgumentException
dup
ldc "array must contain 4 or 16 elements"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L4:
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 3
aload 0
arraylength
iconst_4
if_icmpne L5
aload 0
arraylength
iconst_1
isub
istore 1
L6:
iload 1
iflt L0
aload 3
aload 0
iload 1
baload
sipush 255
iand
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
iload 1
ifle L7
aload 3
ldc "."
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L7:
iload 1
iconst_1
isub
istore 1
goto L6
L5:
iconst_2
newarray int
astore 4
aload 0
arraylength
iconst_1
isub
istore 1
L8:
iload 1
iflt L0
aload 4
iconst_0
aload 0
iload 1
baload
sipush 255
iand
iconst_4
ishr
iastore
aload 4
iconst_1
aload 0
iload 1
baload
sipush 255
iand
bipush 15
iand
iastore
iconst_1
istore 2
L9:
iload 2
iflt L10
aload 3
aload 4
iload 2
iaload
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
iload 1
ifgt L11
iload 2
ifle L12
L11:
aload 3
ldc "."
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L12:
iload 2
iconst_1
isub
istore 2
goto L9
L10:
iload 1
iconst_1
isub
istore 1
goto L8
L0:
aload 0
arraylength
iconst_4
if_icmpne L1
aload 3
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
getstatic org/xbill/DNS/ReverseMap/inaddr4 Lorg/xbill/DNS/Name;
invokestatic org/xbill/DNS/Name/fromString(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
areturn
L1:
aload 3
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
getstatic org/xbill/DNS/ReverseMap/inaddr6 Lorg/xbill/DNS/Name;
invokestatic org/xbill/DNS/Name/fromString(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
astore 0
L3:
aload 0
areturn
L2:
astore 0
new java/lang/IllegalStateException
dup
ldc "name cannot be invalid"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
.limit locals 5
.limit stack 4
.end method

.method public static fromAddress([I)Lorg/xbill/DNS/Name;
aload 0
arraylength
newarray byte
astore 2
iconst_0
istore 1
L0:
iload 1
aload 0
arraylength
if_icmpge L1
aload 0
iload 1
iaload
iflt L2
aload 0
iload 1
iaload
sipush 255
if_icmple L3
L2:
new java/lang/IllegalArgumentException
dup
ldc "array must contain values between 0 and 255"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 2
iload 1
aload 0
iload 1
iaload
i2b
bastore
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 2
invokestatic org/xbill/DNS/ReverseMap/fromAddress([B)Lorg/xbill/DNS/Name;
areturn
.limit locals 3
.limit stack 4
.end method
