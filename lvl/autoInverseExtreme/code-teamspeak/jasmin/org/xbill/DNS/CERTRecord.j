.bytecode 50.0
.class public synchronized org/xbill/DNS/CERTRecord
.super org/xbill/DNS/Record

.field public static final 'OID' I = 254


.field public static final 'PGP' I = 3


.field public static final 'PKIX' I = 1


.field public static final 'SPKI' I = 2


.field public static final 'URI' I = 253


.field private static final 'serialVersionUID' J = 4763014646517016835L


.field private 'alg' I

.field private 'cert' [B

.field private 'certType' I

.field private 'keyTag' I

.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJIII[B)V
aload 0
aload 1
bipush 37
iload 2
lload 3
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
aload 0
ldc "certType"
iload 5
invokestatic org/xbill/DNS/CERTRecord/checkU16(Ljava/lang/String;I)I
putfield org/xbill/DNS/CERTRecord/certType I
aload 0
ldc "keyTag"
iload 6
invokestatic org/xbill/DNS/CERTRecord/checkU16(Ljava/lang/String;I)I
putfield org/xbill/DNS/CERTRecord/keyTag I
aload 0
ldc "alg"
iload 7
invokestatic org/xbill/DNS/CERTRecord/checkU8(Ljava/lang/String;I)I
putfield org/xbill/DNS/CERTRecord/alg I
aload 0
aload 8
putfield org/xbill/DNS/CERTRecord/cert [B
return
.limit locals 9
.limit stack 6
.end method

.method public getAlgorithm()I
aload 0
getfield org/xbill/DNS/CERTRecord/alg I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getCert()[B
aload 0
getfield org/xbill/DNS/CERTRecord/cert [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public getCertType()I
aload 0
getfield org/xbill/DNS/CERTRecord/certType I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getKeyTag()I
aload 0
getfield org/xbill/DNS/CERTRecord/keyTag I
ireturn
.limit locals 1
.limit stack 1
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/CERTRecord
dup
invokespecial org/xbill/DNS/CERTRecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
astore 2
aload 0
aload 2
invokestatic org/xbill/DNS/CERTRecord$CertificateType/value(Ljava/lang/String;)I
putfield org/xbill/DNS/CERTRecord/certType I
aload 0
getfield org/xbill/DNS/CERTRecord/certType I
ifge L0
aload 1
new java/lang/StringBuffer
dup
ldc "Invalid certificate type: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L0:
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt16()I
putfield org/xbill/DNS/CERTRecord/keyTag I
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
astore 2
aload 0
aload 2
invokestatic org/xbill/DNS/DNSSEC$Algorithm/value(Ljava/lang/String;)I
putfield org/xbill/DNS/CERTRecord/alg I
aload 0
getfield org/xbill/DNS/CERTRecord/alg I
ifge L1
aload 1
new java/lang/StringBuffer
dup
ldc "Invalid algorithm: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L1:
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getBase64()[B
putfield org/xbill/DNS/CERTRecord/cert [B
return
.limit locals 3
.limit stack 4
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
putfield org/xbill/DNS/CERTRecord/certType I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
putfield org/xbill/DNS/CERTRecord/keyTag I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
putfield org/xbill/DNS/CERTRecord/alg I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readByteArray()[B
putfield org/xbill/DNS/CERTRecord/cert [B
return
.limit locals 2
.limit stack 2
.end method

.method rrToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 1
aload 0
getfield org/xbill/DNS/CERTRecord/certType I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/CERTRecord/keyTag I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/CERTRecord/alg I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 0
getfield org/xbill/DNS/CERTRecord/cert [B
ifnull L0
ldc "multiline"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L1
aload 1
ldc " (\n"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/CERTRecord/cert [B
bipush 64
ldc "\u0009"
iconst_1
invokestatic org/xbill/DNS/utils/base64/formatString([BILjava/lang/String;Z)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L0:
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
L1:
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/CERTRecord/cert [B
invokestatic org/xbill/DNS/utils/base64/toString([B)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
goto L0
.limit locals 2
.limit stack 5
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
aload 0
getfield org/xbill/DNS/CERTRecord/certType I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 1
aload 0
getfield org/xbill/DNS/CERTRecord/keyTag I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 1
aload 0
getfield org/xbill/DNS/CERTRecord/alg I
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 1
aload 0
getfield org/xbill/DNS/CERTRecord/cert [B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
return
.limit locals 4
.limit stack 2
.end method
