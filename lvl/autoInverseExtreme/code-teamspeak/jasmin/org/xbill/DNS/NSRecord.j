.bytecode 50.0
.class public synchronized org/xbill/DNS/NSRecord
.super org/xbill/DNS/SingleCompressedNameBase

.field private static final 'serialVersionUID' J = 487170758138268838L


.method <init>()V
aload 0
invokespecial org/xbill/DNS/SingleCompressedNameBase/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJLorg/xbill/DNS/Name;)V
aload 0
aload 1
iconst_2
iload 2
lload 3
aload 5
ldc "target"
invokespecial org/xbill/DNS/SingleCompressedNameBase/<init>(Lorg/xbill/DNS/Name;IIJLorg/xbill/DNS/Name;Ljava/lang/String;)V
return
.limit locals 6
.limit stack 8
.end method

.method public getAdditionalName()Lorg/xbill/DNS/Name;
aload 0
invokevirtual org/xbill/DNS/NSRecord/getSingleName()Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/NSRecord
dup
invokespecial org/xbill/DNS/NSRecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method public getTarget()Lorg/xbill/DNS/Name;
aload 0
invokevirtual org/xbill/DNS/NSRecord/getSingleName()Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method
