.bytecode 50.0
.class synchronized org/xbill/DNS/Mnemonic
.super java/lang/Object

.field static final 'CASE_LOWER' I = 3


.field static final 'CASE_SENSITIVE' I = 1


.field static final 'CASE_UPPER' I = 2


.field private static 'cachedInts' [Ljava/lang/Integer;

.field private 'description' Ljava/lang/String;

.field private 'max' I

.field private 'numericok' Z

.field private 'prefix' Ljava/lang/String;

.field private 'strings' Ljava/util/HashMap;

.field private 'values' Ljava/util/HashMap;

.field private 'wordcase' I

.method static <clinit>()V
bipush 64
anewarray java/lang/Integer
putstatic org/xbill/DNS/Mnemonic/cachedInts [Ljava/lang/Integer;
iconst_0
istore 0
L0:
iload 0
getstatic org/xbill/DNS/Mnemonic/cachedInts [Ljava/lang/Integer;
arraylength
if_icmpge L1
getstatic org/xbill/DNS/Mnemonic/cachedInts [Ljava/lang/Integer;
iload 0
new java/lang/Integer
dup
iload 0
invokespecial java/lang/Integer/<init>(I)V
aastore
iload 0
iconst_1
iadd
istore 0
goto L0
L1:
return
.limit locals 1
.limit stack 5
.end method

.method public <init>(Ljava/lang/String;I)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield org/xbill/DNS/Mnemonic/description Ljava/lang/String;
aload 0
iload 2
putfield org/xbill/DNS/Mnemonic/wordcase I
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield org/xbill/DNS/Mnemonic/strings Ljava/util/HashMap;
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield org/xbill/DNS/Mnemonic/values Ljava/util/HashMap;
aload 0
ldc_w 2147483647
putfield org/xbill/DNS/Mnemonic/max I
return
.limit locals 3
.limit stack 3
.end method

.method private parseNumeric(Ljava/lang/String;)I
.catch java/lang/NumberFormatException from L0 to L1 using L2
.catch java/lang/NumberFormatException from L3 to L4 using L2
L0:
aload 1
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
istore 2
L1:
iload 2
iflt L5
L3:
aload 0
getfield org/xbill/DNS/Mnemonic/max I
istore 3
L4:
iload 2
iload 3
if_icmpgt L5
iload 2
ireturn
L2:
astore 1
L5:
iconst_m1
ireturn
.limit locals 4
.limit stack 2
.end method

.method private sanitize(Ljava/lang/String;)Ljava/lang/String;
aload 0
getfield org/xbill/DNS/Mnemonic/wordcase I
iconst_2
if_icmpne L0
aload 1
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
astore 2
L1:
aload 2
areturn
L0:
aload 1
astore 2
aload 0
getfield org/xbill/DNS/Mnemonic/wordcase I
iconst_3
if_icmpne L1
aload 1
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
areturn
.limit locals 3
.limit stack 2
.end method

.method public static toInteger(I)Ljava/lang/Integer;
iload 0
iflt L0
iload 0
getstatic org/xbill/DNS/Mnemonic/cachedInts [Ljava/lang/Integer;
arraylength
if_icmpge L0
getstatic org/xbill/DNS/Mnemonic/cachedInts [Ljava/lang/Integer;
iload 0
aaload
areturn
L0:
new java/lang/Integer
dup
iload 0
invokespecial java/lang/Integer/<init>(I)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public add(ILjava/lang/String;)V
aload 0
iload 1
invokevirtual org/xbill/DNS/Mnemonic/check(I)V
iload 1
invokestatic org/xbill/DNS/Mnemonic/toInteger(I)Ljava/lang/Integer;
astore 3
aload 0
aload 2
invokespecial org/xbill/DNS/Mnemonic/sanitize(Ljava/lang/String;)Ljava/lang/String;
astore 2
aload 0
getfield org/xbill/DNS/Mnemonic/strings Ljava/util/HashMap;
aload 2
aload 3
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield org/xbill/DNS/Mnemonic/values Ljava/util/HashMap;
aload 3
aload 2
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
return
.limit locals 4
.limit stack 3
.end method

.method public addAlias(ILjava/lang/String;)V
aload 0
iload 1
invokevirtual org/xbill/DNS/Mnemonic/check(I)V
iload 1
invokestatic org/xbill/DNS/Mnemonic/toInteger(I)Ljava/lang/Integer;
astore 3
aload 0
aload 2
invokespecial org/xbill/DNS/Mnemonic/sanitize(Ljava/lang/String;)Ljava/lang/String;
astore 2
aload 0
getfield org/xbill/DNS/Mnemonic/strings Ljava/util/HashMap;
aload 2
aload 3
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
return
.limit locals 4
.limit stack 3
.end method

.method public addAll(Lorg/xbill/DNS/Mnemonic;)V
aload 0
getfield org/xbill/DNS/Mnemonic/wordcase I
aload 1
getfield org/xbill/DNS/Mnemonic/wordcase I
if_icmpeq L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 1
getfield org/xbill/DNS/Mnemonic/description Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc ": wordcases do not match"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield org/xbill/DNS/Mnemonic/strings Ljava/util/HashMap;
aload 1
getfield org/xbill/DNS/Mnemonic/strings Ljava/util/HashMap;
invokevirtual java/util/HashMap/putAll(Ljava/util/Map;)V
aload 0
getfield org/xbill/DNS/Mnemonic/values Ljava/util/HashMap;
aload 1
getfield org/xbill/DNS/Mnemonic/values Ljava/util/HashMap;
invokevirtual java/util/HashMap/putAll(Ljava/util/Map;)V
return
.limit locals 2
.limit stack 4
.end method

.method public check(I)V
iload 1
iflt L0
iload 1
aload 0
getfield org/xbill/DNS/Mnemonic/max I
if_icmple L1
L0:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 0
getfield org/xbill/DNS/Mnemonic/description Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
iload 1
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
ldc "is out of range"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
return
.limit locals 2
.limit stack 4
.end method

.method public getText(I)Ljava/lang/String;
aload 0
iload 1
invokevirtual org/xbill/DNS/Mnemonic/check(I)V
aload 0
getfield org/xbill/DNS/Mnemonic/values Ljava/util/HashMap;
iload 1
invokestatic org/xbill/DNS/Mnemonic/toInteger(I)Ljava/lang/Integer;
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
astore 2
aload 2
ifnull L0
L1:
aload 2
areturn
L0:
iload 1
invokestatic java/lang/Integer/toString(I)Ljava/lang/String;
astore 3
aload 3
astore 2
aload 0
getfield org/xbill/DNS/Mnemonic/prefix Ljava/lang/String;
ifnull L1
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 0
getfield org/xbill/DNS/Mnemonic/prefix Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
aload 3
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 4
.limit stack 2
.end method

.method public getValue(Ljava/lang/String;)I
aload 0
aload 1
invokespecial org/xbill/DNS/Mnemonic/sanitize(Ljava/lang/String;)Ljava/lang/String;
astore 1
aload 0
getfield org/xbill/DNS/Mnemonic/strings Ljava/util/HashMap;
aload 1
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Integer
astore 4
aload 4
ifnull L0
aload 4
invokevirtual java/lang/Integer/intValue()I
istore 2
L1:
iload 2
ireturn
L0:
aload 0
getfield org/xbill/DNS/Mnemonic/prefix Ljava/lang/String;
ifnull L2
aload 1
aload 0
getfield org/xbill/DNS/Mnemonic/prefix Ljava/lang/String;
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L2
aload 0
aload 1
aload 0
getfield org/xbill/DNS/Mnemonic/prefix Ljava/lang/String;
invokevirtual java/lang/String/length()I
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
invokespecial org/xbill/DNS/Mnemonic/parseNumeric(Ljava/lang/String;)I
istore 3
iload 3
istore 2
iload 3
ifge L1
L2:
aload 0
getfield org/xbill/DNS/Mnemonic/numericok Z
ifeq L3
aload 0
aload 1
invokespecial org/xbill/DNS/Mnemonic/parseNumeric(Ljava/lang/String;)I
ireturn
L3:
iconst_m1
ireturn
.limit locals 5
.limit stack 3
.end method

.method public setMaximum(I)V
aload 0
iload 1
putfield org/xbill/DNS/Mnemonic/max I
return
.limit locals 2
.limit stack 2
.end method

.method public setNumericAllowed(Z)V
aload 0
iload 1
putfield org/xbill/DNS/Mnemonic/numericok Z
return
.limit locals 2
.limit stack 2
.end method

.method public setPrefix(Ljava/lang/String;)V
aload 0
aload 0
aload 1
invokespecial org/xbill/DNS/Mnemonic/sanitize(Ljava/lang/String;)Ljava/lang/String;
putfield org/xbill/DNS/Mnemonic/prefix Ljava/lang/String;
return
.limit locals 2
.limit stack 3
.end method
