.bytecode 50.0
.class public synchronized org/xbill/DNS/utils/base32
.super java/lang/Object

.field private 'alphabet' Ljava/lang/String;

.field private 'lowercase' Z

.field private 'padding' Z

.method public <init>(Ljava/lang/String;ZZ)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield org/xbill/DNS/utils/base32/alphabet Ljava/lang/String;
aload 0
iload 2
putfield org/xbill/DNS/utils/base32/padding Z
aload 0
iload 3
putfield org/xbill/DNS/utils/base32/lowercase Z
return
.limit locals 4
.limit stack 2
.end method

.method private static blockLenToPadding(I)I
iload 0
tableswitch 1
L0
L1
L2
L3
L4
default : L5
L5:
iconst_m1
ireturn
L0:
bipush 6
ireturn
L1:
iconst_4
ireturn
L2:
iconst_3
ireturn
L3:
iconst_1
ireturn
L4:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static paddingToBlockLen(I)I
iload 0
tableswitch 0
L0
L1
L2
L3
L4
L2
L5
default : L2
L2:
iconst_m1
ireturn
L5:
iconst_1
ireturn
L4:
iconst_2
ireturn
L3:
iconst_3
ireturn
L1:
iconst_4
ireturn
L0:
iconst_5
ireturn
.limit locals 1
.limit stack 1
.end method

.method public fromString(Ljava/lang/String;)[B
.catch java/io/IOException from L0 to L1 using L2
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
astore 18
aload 1
invokevirtual java/lang/String/getBytes()[B
astore 1
iconst_0
istore 3
L3:
iload 3
aload 1
arraylength
if_icmpge L4
aload 1
iload 3
baload
i2c
istore 2
iload 2
invokestatic java/lang/Character/isWhitespace(C)Z
ifne L5
aload 18
iload 2
invokestatic java/lang/Character/toUpperCase(C)C
i2b
invokevirtual java/io/ByteArrayOutputStream/write(I)V
L5:
iload 3
iconst_1
iadd
istore 3
goto L3
L4:
aload 0
getfield org/xbill/DNS/utils/base32/padding Z
ifeq L6
aload 18
invokevirtual java/io/ByteArrayOutputStream/size()I
bipush 8
irem
ifeq L7
aconst_null
areturn
L6:
aload 18
invokevirtual java/io/ByteArrayOutputStream/size()I
bipush 8
irem
ifeq L7
aload 18
bipush 61
invokevirtual java/io/ByteArrayOutputStream/write(I)V
goto L6
L7:
aload 18
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
astore 1
aload 18
invokevirtual java/io/ByteArrayOutputStream/reset()V
new java/io/DataOutputStream
dup
aload 18
invokespecial java/io/DataOutputStream/<init>(Ljava/io/OutputStream;)V
astore 19
iconst_0
istore 3
L8:
iload 3
aload 1
arraylength
bipush 8
idiv
if_icmpge L9
bipush 8
newarray short
astore 20
bipush 8
istore 5
iconst_0
istore 4
L10:
iload 4
bipush 8
if_icmpge L11
aload 1
iload 3
bipush 8
imul
iload 4
iadd
baload
i2c
bipush 61
if_icmpeq L11
aload 20
iload 4
aload 0
getfield org/xbill/DNS/utils/base32/alphabet Ljava/lang/String;
aload 1
iload 3
bipush 8
imul
iload 4
iadd
baload
invokevirtual java/lang/String/indexOf(I)I
i2s
sastore
aload 20
iload 4
saload
ifge L12
aconst_null
areturn
L12:
iload 5
iconst_1
isub
istore 5
iload 4
iconst_1
iadd
istore 4
goto L10
L11:
iload 5
invokestatic org/xbill/DNS/utils/base32/paddingToBlockLen(I)I
istore 5
iload 5
ifge L13
aconst_null
areturn
L13:
aload 20
iconst_0
saload
istore 6
aload 20
iconst_1
saload
istore 7
aload 20
iconst_1
saload
istore 8
aload 20
iconst_2
saload
istore 9
aload 20
iconst_3
saload
istore 10
aload 20
iconst_3
saload
istore 11
aload 20
iconst_4
saload
istore 12
aload 20
iconst_4
saload
istore 13
aload 20
iconst_5
saload
istore 14
aload 20
bipush 6
saload
istore 15
aload 20
bipush 6
saload
istore 16
aload 20
bipush 7
saload
istore 17
iconst_0
istore 4
L14:
iload 4
iload 5
if_icmpge L15
L0:
aload 19
iconst_5
newarray int
dup
iconst_0
iload 6
iconst_3
ishl
iload 7
iconst_2
ishr
ior
iastore
dup
iconst_1
iload 8
iconst_3
iand
bipush 6
ishl
iload 9
iconst_1
ishl
ior
iload 10
iconst_4
ishr
ior
iastore
dup
iconst_2
iload 11
bipush 15
iand
iconst_4
ishl
iload 12
iconst_1
ishr
bipush 15
iand
ior
iastore
dup
iconst_3
iload 13
bipush 7
ishl
iload 14
iconst_2
ishl
ior
iload 15
iconst_3
ishr
ior
iastore
dup
iconst_4
iload 17
iload 16
bipush 7
iand
iconst_5
ishl
ior
iastore
iload 4
iaload
sipush 255
iand
i2b
invokevirtual java/io/DataOutputStream/writeByte(I)V
L1:
iload 4
iconst_1
iadd
istore 4
goto L14
L2:
astore 20
L15:
iload 3
iconst_1
iadd
istore 3
goto L8
L9:
aload 18
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
areturn
.limit locals 21
.limit stack 7
.end method

.method public toString([B)Ljava/lang/String;
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
astore 15
iconst_0
istore 3
L0:
iload 3
aload 1
arraylength
iconst_4
iadd
iconst_5
idiv
if_icmpge L1
iconst_5
newarray short
astore 16
iconst_0
istore 4
iconst_5
istore 5
L2:
iload 4
iconst_5
if_icmpge L3
iload 3
iconst_5
imul
iload 4
iadd
aload 1
arraylength
if_icmpge L4
aload 16
iload 4
aload 1
iload 3
iconst_5
imul
iload 4
iadd
baload
sipush 255
iand
i2s
sastore
L5:
iload 4
iconst_1
iadd
istore 4
goto L2
L4:
aload 16
iload 4
iconst_0
sastore
iload 5
iconst_1
isub
istore 5
goto L5
L3:
iload 5
invokestatic org/xbill/DNS/utils/base32/blockLenToPadding(I)I
istore 6
aload 16
iconst_0
saload
iconst_3
ishr
bipush 31
iand
i2b
istore 7
aload 16
iconst_0
saload
bipush 7
iand
iconst_2
ishl
aload 16
iconst_1
saload
bipush 6
ishr
iconst_3
iand
ior
i2b
istore 8
aload 16
iconst_1
saload
iconst_1
ishr
bipush 31
iand
i2b
istore 9
aload 16
iconst_1
saload
iconst_1
iand
iconst_4
ishl
aload 16
iconst_2
saload
iconst_4
ishr
bipush 15
iand
ior
i2b
istore 10
aload 16
iconst_2
saload
bipush 15
iand
iconst_1
ishl
aload 16
iconst_3
saload
bipush 7
ishr
iconst_1
iand
ior
i2b
istore 11
aload 16
iconst_3
saload
iconst_2
ishr
bipush 31
iand
i2b
istore 12
aload 16
iconst_3
saload
iconst_3
iand
iconst_3
ishl
aload 16
iconst_4
saload
iconst_5
ishr
bipush 7
iand
ior
i2b
istore 13
aload 16
iconst_4
saload
bipush 31
iand
i2b
istore 14
iconst_0
istore 4
L6:
iload 4
bipush 8
iload 6
isub
if_icmpge L7
aload 0
getfield org/xbill/DNS/utils/base32/alphabet Ljava/lang/String;
bipush 8
newarray int
dup
iconst_0
iload 7
iastore
dup
iconst_1
iload 8
iastore
dup
iconst_2
iload 9
iastore
dup
iconst_3
iload 10
iastore
dup
iconst_4
iload 11
iastore
dup
iconst_5
iload 12
iastore
dup
bipush 6
iload 13
iastore
dup
bipush 7
iload 14
iastore
iload 4
iaload
invokevirtual java/lang/String/charAt(I)C
istore 2
iload 2
istore 5
aload 0
getfield org/xbill/DNS/utils/base32/lowercase Z
ifeq L8
iload 2
invokestatic java/lang/Character/toLowerCase(C)C
istore 5
L8:
aload 15
iload 5
invokevirtual java/io/ByteArrayOutputStream/write(I)V
iload 4
iconst_1
iadd
istore 4
goto L6
L7:
aload 0
getfield org/xbill/DNS/utils/base32/padding Z
ifeq L9
bipush 8
iload 6
isub
istore 4
L10:
iload 4
bipush 8
if_icmpge L9
aload 15
bipush 61
invokevirtual java/io/ByteArrayOutputStream/write(I)V
iload 4
iconst_1
iadd
istore 4
goto L10
L9:
iload 3
iconst_1
iadd
istore 3
goto L0
L1:
new java/lang/String
dup
aload 15
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
invokespecial java/lang/String/<init>([B)V
areturn
.limit locals 17
.limit stack 5
.end method
