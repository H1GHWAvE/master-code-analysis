.bytecode 50.0
.class synchronized abstract android/support/v4/m/r
.super java/lang/Object
.implements android/support/v4/m/l

.field private final 'a' Landroid/support/v4/m/q;

.method public <init>(Landroid/support/v4/m/q;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v4/m/r/a Landroid/support/v4/m/q;
return
.limit locals 2
.limit stack 2
.end method

.method private b(Ljava/lang/CharSequence;II)Z
aload 0
getfield android/support/v4/m/r/a Landroid/support/v4/m/q;
aload 1
iload 2
iload 3
invokeinterface android/support/v4/m/q/a(Ljava/lang/CharSequence;II)I 3
tableswitch 0
L0
L1
default : L2
L2:
aload 0
invokevirtual android/support/v4/m/r/a()Z
ireturn
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 4
.limit stack 4
.end method

.method protected abstract a()Z
.end method

.method public final a(Ljava/lang/CharSequence;II)Z
aload 1
ifnull L0
iload 2
iflt L0
iload 3
iflt L0
aload 1
invokeinterface java/lang/CharSequence/length()I 0
iload 3
isub
iload 2
if_icmpge L1
L0:
new java/lang/IllegalArgumentException
dup
invokespecial java/lang/IllegalArgumentException/<init>()V
athrow
L1:
aload 0
getfield android/support/v4/m/r/a Landroid/support/v4/m/q;
ifnonnull L2
aload 0
invokevirtual android/support/v4/m/r/a()Z
ireturn
L2:
aload 0
getfield android/support/v4/m/r/a Landroid/support/v4/m/q;
aload 1
iload 2
iload 3
invokeinterface android/support/v4/m/q/a(Ljava/lang/CharSequence;II)I 3
tableswitch 0
L3
L4
default : L5
L5:
aload 0
invokevirtual android/support/v4/m/r/a()Z
ireturn
L3:
iconst_1
ireturn
L4:
iconst_0
ireturn
.limit locals 4
.limit stack 4
.end method

.method public final a([CII)Z
aload 0
aload 1
invokestatic java/nio/CharBuffer/wrap([C)Ljava/nio/CharBuffer;
iload 2
iload 3
invokevirtual android/support/v4/m/r/a(Ljava/lang/CharSequence;II)Z
ireturn
.limit locals 4
.limit stack 4
.end method
