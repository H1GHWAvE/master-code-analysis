.bytecode 50.0
.class public final synchronized android/support/v4/e/a/x
.super java/lang/Object

.field private static final 'a' Ljava/lang/String; = "RoundedBitmapDrawableFactory"

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)Landroid/support/v4/e/a/v;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
new android/support/v4/e/a/w
dup
aload 0
aload 1
invokespecial android/support/v4/e/a/w/<init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
areturn
L0:
new android/support/v4/e/a/y
dup
aload 0
aload 1
invokespecial android/support/v4/e/a/y/<init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static a(Landroid/content/res/Resources;Ljava/io/InputStream;)Landroid/support/v4/e/a/v;
aload 0
aload 1
invokestatic android/graphics/BitmapFactory/decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
invokestatic android/support/v4/e/a/x/a(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)Landroid/support/v4/e/a/v;
astore 0
aload 0
getfield android/support/v4/e/a/v/a Landroid/graphics/Bitmap;
ifnonnull L0
ldc "RoundedBitmapDrawableFactory"
new java/lang/StringBuilder
dup
ldc "RoundedBitmapDrawable cannot decode "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
L0:
aload 0
areturn
.limit locals 2
.limit stack 4
.end method

.method private static a(Landroid/content/res/Resources;Ljava/lang/String;)Landroid/support/v4/e/a/v;
aload 0
aload 1
invokestatic android/graphics/BitmapFactory/decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;
invokestatic android/support/v4/e/a/x/a(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)Landroid/support/v4/e/a/v;
astore 0
aload 0
getfield android/support/v4/e/a/v/a Landroid/graphics/Bitmap;
ifnonnull L0
ldc "RoundedBitmapDrawableFactory"
new java/lang/StringBuilder
dup
ldc "RoundedBitmapDrawable cannot decode "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
L0:
aload 0
areturn
.limit locals 2
.limit stack 4
.end method
