.bytecode 50.0
.class public final synchronized android/support/v4/n/x
.super java/lang/Object

.field public static final 'a' I = 19


.field private static final 'b' I = 60


.field private static final 'c' I = 3600


.field private static final 'd' I = 86400


.field private static final 'e' Ljava/lang/Object;

.field private static 'f' [C

.method static <clinit>()V
new java/lang/Object
dup
invokespecial java/lang/Object/<init>()V
putstatic android/support/v4/n/x/e Ljava/lang/Object;
bipush 24
newarray char
putstatic android/support/v4/n/x/f [C
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(IIZI)I
iload 0
bipush 99
if_icmpgt L0
iload 2
ifeq L1
iload 3
iconst_3
if_icmplt L1
L0:
iload 1
iconst_3
iadd
ireturn
L1:
iload 0
bipush 9
if_icmpgt L2
iload 2
ifeq L3
iload 3
iconst_2
if_icmplt L3
L2:
iload 1
iconst_2
iadd
ireturn
L3:
iload 2
ifne L4
iload 0
ifle L5
L4:
iload 1
iconst_1
iadd
ireturn
L5:
iconst_0
ireturn
.limit locals 4
.limit stack 2
.end method

.method private static a(J)I
iconst_0
istore 9
getstatic android/support/v4/n/x/f [C
arraylength
ifge L0
iconst_0
newarray char
putstatic android/support/v4/n/x/f [C
L0:
getstatic android/support/v4/n/x/f [C
astore 10
lload 0
lconst_0
lcmp
ifne L1
aload 10
iconst_0
bipush 48
castore
iconst_1
ireturn
L1:
lload 0
lconst_0
lcmp
ifle L2
bipush 43
istore 2
L3:
lload 0
ldc2_w 1000L
lrem
l2i
istore 7
lload 0
ldc2_w 1000L
ldiv
l2d
invokestatic java/lang/Math/floor(D)D
d2i
istore 3
iload 3
ldc_w 86400
if_icmple L4
iload 3
ldc_w 86400
idiv
istore 4
iload 3
ldc_w 86400
iload 4
imul
isub
istore 3
L5:
iload 3
sipush 3600
if_icmple L6
iload 3
sipush 3600
idiv
istore 5
iload 3
iload 5
sipush 3600
imul
isub
istore 3
L7:
iload 3
bipush 60
if_icmple L8
iload 3
bipush 60
idiv
istore 6
iload 3
iload 6
bipush 60
imul
isub
istore 3
L9:
aload 10
iconst_0
iload 2
castore
aload 10
iload 4
bipush 100
iconst_1
iconst_0
invokestatic android/support/v4/n/x/a([CICIZ)I
istore 4
iload 4
iconst_1
if_icmpeq L10
iconst_1
istore 8
L11:
aload 10
iload 5
bipush 104
iload 4
iload 8
invokestatic android/support/v4/n/x/a([CICIZ)I
istore 4
iload 4
iconst_1
if_icmpeq L12
iconst_1
istore 8
L13:
aload 10
iload 6
bipush 109
iload 4
iload 8
invokestatic android/support/v4/n/x/a([CICIZ)I
istore 4
iload 9
istore 8
iload 4
iconst_1
if_icmpeq L14
iconst_1
istore 8
L14:
aload 10
iload 7
bipush 109
aload 10
iload 3
bipush 115
iload 4
iload 8
invokestatic android/support/v4/n/x/a([CICIZ)I
iconst_1
invokestatic android/support/v4/n/x/a([CICIZ)I
istore 3
aload 10
iload 3
bipush 115
castore
iload 3
iconst_1
iadd
ireturn
L2:
bipush 45
istore 2
lload 0
lneg
lstore 0
goto L3
L10:
iconst_0
istore 8
goto L11
L12:
iconst_0
istore 8
goto L13
L8:
iconst_0
istore 6
goto L9
L6:
iconst_0
istore 5
goto L7
L4:
iconst_0
istore 4
goto L5
.limit locals 11
.limit stack 8
.end method

.method private static a([CICIZ)I
iload 4
ifne L0
iload 3
istore 5
iload 1
ifle L1
L0:
iload 1
bipush 99
if_icmple L2
iload 1
bipush 100
idiv
istore 6
aload 0
iload 3
iload 6
bipush 48
iadd
i2c
castore
iload 3
iconst_1
iadd
istore 5
iload 1
iload 6
bipush 100
imul
isub
istore 1
L3:
iload 1
bipush 9
if_icmpgt L4
iload 5
istore 7
iload 1
istore 6
iload 3
iload 5
if_icmpeq L5
L4:
iload 1
bipush 10
idiv
istore 3
aload 0
iload 5
iload 3
bipush 48
iadd
i2c
castore
iload 5
iconst_1
iadd
istore 7
iload 1
iload 3
bipush 10
imul
isub
istore 6
L5:
aload 0
iload 7
iload 6
bipush 48
iadd
i2c
castore
iload 7
iconst_1
iadd
istore 1
aload 0
iload 1
iload 2
castore
iload 1
iconst_1
iadd
istore 5
L1:
iload 5
ireturn
L2:
iload 3
istore 5
goto L3
.limit locals 8
.limit stack 4
.end method

.method public static a(JJLjava/io/PrintWriter;)V
lload 0
lconst_0
lcmp
ifne L0
aload 4
ldc "--"
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
return
L0:
lload 0
lload 2
lsub
aload 4
invokestatic android/support/v4/n/x/b(JLjava/io/PrintWriter;)V
return
.limit locals 5
.limit stack 4
.end method

.method public static a(JLjava/io/PrintWriter;)V
lload 0
aload 2
invokestatic android/support/v4/n/x/b(JLjava/io/PrintWriter;)V
return
.limit locals 3
.limit stack 3
.end method

.method private static a(JLjava/lang/StringBuilder;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
getstatic android/support/v4/n/x/e Ljava/lang/Object;
astore 4
aload 4
monitorenter
L0:
lload 0
invokestatic android/support/v4/n/x/a(J)I
istore 3
aload 2
getstatic android/support/v4/n/x/f [C
iconst_0
iload 3
invokevirtual java/lang/StringBuilder/append([CII)Ljava/lang/StringBuilder;
pop
aload 4
monitorexit
L1:
return
L2:
astore 2
L3:
aload 4
monitorexit
L4:
aload 2
athrow
.limit locals 5
.limit stack 4
.end method

.method private static b(JLjava/io/PrintWriter;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
getstatic android/support/v4/n/x/e Ljava/lang/Object;
astore 4
aload 4
monitorenter
L0:
lload 0
invokestatic android/support/v4/n/x/a(J)I
istore 3
aload 2
new java/lang/String
dup
getstatic android/support/v4/n/x/f [C
iconst_0
iload 3
invokespecial java/lang/String/<init>([CII)V
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 4
monitorexit
L1:
return
L2:
astore 2
L3:
aload 4
monitorexit
L4:
aload 2
athrow
.limit locals 5
.limit stack 6
.end method
