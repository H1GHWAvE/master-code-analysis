.bytecode 50.0
.class public final synchronized android/support/v4/n/e
.super java/lang/Object

.field private 'a' [I

.field private 'b' I

.field private 'c' I

.field private 'd' I

.method public <init>()V
aload 0
iconst_0
invokespecial android/support/v4/n/e/<init>(B)V
return
.limit locals 1
.limit stack 2
.end method

.method private <init>(B)V
bipush 8
istore 1
aload 0
invokespecial java/lang/Object/<init>()V
bipush 8
invokestatic java/lang/Integer/bitCount(I)I
iconst_1
if_icmpeq L0
iconst_1
bipush 8
invokestatic java/lang/Integer/highestOneBit(I)I
iconst_1
iadd
ishl
istore 1
L0:
aload 0
iload 1
iconst_1
isub
putfield android/support/v4/n/e/d I
aload 0
iload 1
newarray int
putfield android/support/v4/n/e/a [I
return
.limit locals 2
.limit stack 3
.end method

.method private a()V
aload 0
getfield android/support/v4/n/e/a [I
arraylength
istore 1
iload 1
aload 0
getfield android/support/v4/n/e/b I
isub
istore 2
iload 1
iconst_1
ishl
istore 3
iload 3
ifge L0
new java/lang/RuntimeException
dup
ldc "Max array capacity exceeded"
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/String;)V
athrow
L0:
iload 3
newarray int
astore 4
aload 0
getfield android/support/v4/n/e/a [I
aload 0
getfield android/support/v4/n/e/b I
aload 4
iconst_0
iload 2
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
getfield android/support/v4/n/e/a [I
iconst_0
aload 4
iload 2
aload 0
getfield android/support/v4/n/e/b I
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
aload 4
putfield android/support/v4/n/e/a [I
aload 0
iconst_0
putfield android/support/v4/n/e/b I
aload 0
iload 1
putfield android/support/v4/n/e/c I
aload 0
iload 3
iconst_1
isub
putfield android/support/v4/n/e/d I
return
.limit locals 5
.limit stack 5
.end method

.method private a(I)V
aload 0
aload 0
getfield android/support/v4/n/e/b I
iconst_1
isub
aload 0
getfield android/support/v4/n/e/d I
iand
putfield android/support/v4/n/e/b I
aload 0
getfield android/support/v4/n/e/a [I
aload 0
getfield android/support/v4/n/e/b I
iload 1
iastore
aload 0
getfield android/support/v4/n/e/b I
aload 0
getfield android/support/v4/n/e/c I
if_icmpne L0
aload 0
invokespecial android/support/v4/n/e/a()V
L0:
return
.limit locals 2
.limit stack 3
.end method

.method private b()I
aload 0
getfield android/support/v4/n/e/b I
aload 0
getfield android/support/v4/n/e/c I
if_icmpne L0
new java/lang/ArrayIndexOutOfBoundsException
dup
invokespecial java/lang/ArrayIndexOutOfBoundsException/<init>()V
athrow
L0:
aload 0
getfield android/support/v4/n/e/a [I
aload 0
getfield android/support/v4/n/e/b I
iaload
istore 1
aload 0
aload 0
getfield android/support/v4/n/e/b I
iconst_1
iadd
aload 0
getfield android/support/v4/n/e/d I
iand
putfield android/support/v4/n/e/b I
iload 1
ireturn
.limit locals 2
.limit stack 3
.end method

.method private b(I)V
aload 0
getfield android/support/v4/n/e/a [I
aload 0
getfield android/support/v4/n/e/c I
iload 1
iastore
aload 0
aload 0
getfield android/support/v4/n/e/c I
iconst_1
iadd
aload 0
getfield android/support/v4/n/e/d I
iand
putfield android/support/v4/n/e/c I
aload 0
getfield android/support/v4/n/e/c I
aload 0
getfield android/support/v4/n/e/b I
if_icmpne L0
aload 0
invokespecial android/support/v4/n/e/a()V
L0:
return
.limit locals 2
.limit stack 3
.end method

.method private c()I
aload 0
getfield android/support/v4/n/e/b I
aload 0
getfield android/support/v4/n/e/c I
if_icmpne L0
new java/lang/ArrayIndexOutOfBoundsException
dup
invokespecial java/lang/ArrayIndexOutOfBoundsException/<init>()V
athrow
L0:
aload 0
getfield android/support/v4/n/e/c I
iconst_1
isub
aload 0
getfield android/support/v4/n/e/d I
iand
istore 1
aload 0
getfield android/support/v4/n/e/a [I
iload 1
iaload
istore 2
aload 0
iload 1
putfield android/support/v4/n/e/c I
iload 2
ireturn
.limit locals 3
.limit stack 2
.end method

.method private c(I)V
iload 1
ifgt L0
return
L0:
iload 1
aload 0
invokespecial android/support/v4/n/e/g()I
if_icmple L1
new java/lang/ArrayIndexOutOfBoundsException
dup
invokespecial java/lang/ArrayIndexOutOfBoundsException/<init>()V
athrow
L1:
aload 0
aload 0
getfield android/support/v4/n/e/b I
iload 1
iadd
aload 0
getfield android/support/v4/n/e/d I
iand
putfield android/support/v4/n/e/b I
return
.limit locals 2
.limit stack 3
.end method

.method private d()V
aload 0
aload 0
getfield android/support/v4/n/e/b I
putfield android/support/v4/n/e/c I
return
.limit locals 1
.limit stack 2
.end method

.method private d(I)V
iload 1
ifgt L0
return
L0:
iload 1
aload 0
invokespecial android/support/v4/n/e/g()I
if_icmple L1
new java/lang/ArrayIndexOutOfBoundsException
dup
invokespecial java/lang/ArrayIndexOutOfBoundsException/<init>()V
athrow
L1:
aload 0
aload 0
getfield android/support/v4/n/e/c I
iload 1
isub
aload 0
getfield android/support/v4/n/e/d I
iand
putfield android/support/v4/n/e/c I
return
.limit locals 2
.limit stack 3
.end method

.method private e()I
aload 0
getfield android/support/v4/n/e/b I
aload 0
getfield android/support/v4/n/e/c I
if_icmpne L0
new java/lang/ArrayIndexOutOfBoundsException
dup
invokespecial java/lang/ArrayIndexOutOfBoundsException/<init>()V
athrow
L0:
aload 0
getfield android/support/v4/n/e/a [I
aload 0
getfield android/support/v4/n/e/b I
iaload
ireturn
.limit locals 1
.limit stack 2
.end method

.method private e(I)I
iload 1
iflt L0
iload 1
aload 0
invokespecial android/support/v4/n/e/g()I
if_icmplt L1
L0:
new java/lang/ArrayIndexOutOfBoundsException
dup
invokespecial java/lang/ArrayIndexOutOfBoundsException/<init>()V
athrow
L1:
aload 0
getfield android/support/v4/n/e/a [I
aload 0
getfield android/support/v4/n/e/b I
iload 1
iadd
aload 0
getfield android/support/v4/n/e/d I
iand
iaload
ireturn
.limit locals 2
.limit stack 3
.end method

.method private f()I
aload 0
getfield android/support/v4/n/e/b I
aload 0
getfield android/support/v4/n/e/c I
if_icmpne L0
new java/lang/ArrayIndexOutOfBoundsException
dup
invokespecial java/lang/ArrayIndexOutOfBoundsException/<init>()V
athrow
L0:
aload 0
getfield android/support/v4/n/e/a [I
aload 0
getfield android/support/v4/n/e/c I
iconst_1
isub
aload 0
getfield android/support/v4/n/e/d I
iand
iaload
ireturn
.limit locals 1
.limit stack 3
.end method

.method private g()I
aload 0
getfield android/support/v4/n/e/c I
aload 0
getfield android/support/v4/n/e/b I
isub
aload 0
getfield android/support/v4/n/e/d I
iand
ireturn
.limit locals 1
.limit stack 2
.end method

.method private h()Z
aload 0
getfield android/support/v4/n/e/b I
aload 0
getfield android/support/v4/n/e/c I
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method
