.bytecode 50.0
.class public synchronized android/support/v4/n/a
.super android/support/v4/n/v
.implements java/util/Map

.field 'a' Landroid/support/v4/n/k;

.method public <init>()V
aload 0
invokespecial android/support/v4/n/v/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private <init>(I)V
aload 0
iload 1
invokespecial android/support/v4/n/v/<init>(I)V
return
.limit locals 2
.limit stack 2
.end method

.method private <init>(Landroid/support/v4/n/v;)V
aload 0
aload 1
invokespecial android/support/v4/n/v/<init>(Landroid/support/v4/n/v;)V
return
.limit locals 2
.limit stack 2
.end method

.method private a()Landroid/support/v4/n/k;
aload 0
getfield android/support/v4/n/a/a Landroid/support/v4/n/k;
ifnonnull L0
aload 0
new android/support/v4/n/b
dup
aload 0
invokespecial android/support/v4/n/b/<init>(Landroid/support/v4/n/a;)V
putfield android/support/v4/n/a/a Landroid/support/v4/n/k;
L0:
aload 0
getfield android/support/v4/n/a/a Landroid/support/v4/n/k;
areturn
.limit locals 1
.limit stack 4
.end method

.method private a(Ljava/util/Collection;)Z
aload 0
aload 1
invokestatic android/support/v4/n/k/a(Ljava/util/Map;Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private b(Ljava/util/Collection;)Z
aload 0
aload 1
invokestatic android/support/v4/n/k/b(Ljava/util/Map;Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private c(Ljava/util/Collection;)Z
aload 0
aload 1
invokestatic android/support/v4/n/k/c(Ljava/util/Map;Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public entrySet()Ljava/util/Set;
aload 0
invokespecial android/support/v4/n/a/a()Landroid/support/v4/n/k;
astore 1
aload 1
getfield android/support/v4/n/k/b Landroid/support/v4/n/m;
ifnonnull L0
aload 1
new android/support/v4/n/m
dup
aload 1
invokespecial android/support/v4/n/m/<init>(Landroid/support/v4/n/k;)V
putfield android/support/v4/n/k/b Landroid/support/v4/n/m;
L0:
aload 1
getfield android/support/v4/n/k/b Landroid/support/v4/n/m;
areturn
.limit locals 2
.limit stack 4
.end method

.method public keySet()Ljava/util/Set;
aload 0
invokespecial android/support/v4/n/a/a()Landroid/support/v4/n/k;
astore 1
aload 1
getfield android/support/v4/n/k/c Landroid/support/v4/n/n;
ifnonnull L0
aload 1
new android/support/v4/n/n
dup
aload 1
invokespecial android/support/v4/n/n/<init>(Landroid/support/v4/n/k;)V
putfield android/support/v4/n/k/c Landroid/support/v4/n/n;
L0:
aload 1
getfield android/support/v4/n/k/c Landroid/support/v4/n/n;
areturn
.limit locals 2
.limit stack 4
.end method

.method public putAll(Ljava/util/Map;)V
aload 0
aload 0
getfield android/support/v4/n/a/h I
aload 1
invokeinterface java/util/Map/size()I 0
iadd
invokevirtual android/support/v4/n/a/a(I)V
aload 1
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 2
aload 0
aload 2
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
aload 2
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
invokevirtual android/support/v4/n/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
goto L0
L1:
return
.limit locals 3
.limit stack 3
.end method

.method public values()Ljava/util/Collection;
aload 0
invokespecial android/support/v4/n/a/a()Landroid/support/v4/n/k;
astore 1
aload 1
getfield android/support/v4/n/k/d Landroid/support/v4/n/p;
ifnonnull L0
aload 1
new android/support/v4/n/p
dup
aload 1
invokespecial android/support/v4/n/p/<init>(Landroid/support/v4/n/k;)V
putfield android/support/v4/n/k/d Landroid/support/v4/n/p;
L0:
aload 1
getfield android/support/v4/n/k/d Landroid/support/v4/n/p;
areturn
.limit locals 2
.limit stack 4
.end method
