.bytecode 50.0
.class final synchronized android/support/v4/n/n
.super java/lang/Object
.implements java/util/Set

.field final synthetic 'a' Landroid/support/v4/n/k;

.method <init>(Landroid/support/v4/n/k;)V
aload 0
aload 1
putfield android/support/v4/n/n/a Landroid/support/v4/n/k;
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 2
.limit stack 2
.end method

.method public final add(Ljava/lang/Object;)Z
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public final addAll(Ljava/util/Collection;)Z
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public final clear()V
aload 0
getfield android/support/v4/n/n/a Landroid/support/v4/n/k;
invokevirtual android/support/v4/n/k/c()V
return
.limit locals 1
.limit stack 1
.end method

.method public final contains(Ljava/lang/Object;)Z
aload 0
getfield android/support/v4/n/n/a Landroid/support/v4/n/k;
aload 1
invokevirtual android/support/v4/n/k/a(Ljava/lang/Object;)I
iflt L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final containsAll(Ljava/util/Collection;)Z
aload 0
getfield android/support/v4/n/n/a Landroid/support/v4/n/k;
invokevirtual android/support/v4/n/k/b()Ljava/util/Map;
aload 1
invokestatic android/support/v4/n/k/a(Ljava/util/Map;Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final equals(Ljava/lang/Object;)Z
aload 0
aload 1
invokestatic android/support/v4/n/k/a(Ljava/util/Set;Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final hashCode()I
aload 0
getfield android/support/v4/n/n/a Landroid/support/v4/n/k;
invokevirtual android/support/v4/n/k/a()I
iconst_1
isub
istore 1
iconst_0
istore 2
L0:
iload 1
iflt L1
aload 0
getfield android/support/v4/n/n/a Landroid/support/v4/n/k;
iload 1
iconst_0
invokevirtual android/support/v4/n/k/a(II)Ljava/lang/Object;
astore 4
aload 4
ifnonnull L2
iconst_0
istore 3
L3:
iload 2
iload 3
iadd
istore 2
iload 1
iconst_1
isub
istore 1
goto L0
L2:
aload 4
invokevirtual java/lang/Object/hashCode()I
istore 3
goto L3
L1:
iload 2
ireturn
.limit locals 5
.limit stack 3
.end method

.method public final isEmpty()Z
aload 0
getfield android/support/v4/n/n/a Landroid/support/v4/n/k;
invokevirtual android/support/v4/n/k/a()I
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final iterator()Ljava/util/Iterator;
new android/support/v4/n/l
dup
aload 0
getfield android/support/v4/n/n/a Landroid/support/v4/n/k;
iconst_0
invokespecial android/support/v4/n/l/<init>(Landroid/support/v4/n/k;I)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public final remove(Ljava/lang/Object;)Z
aload 0
getfield android/support/v4/n/n/a Landroid/support/v4/n/k;
aload 1
invokevirtual android/support/v4/n/k/a(Ljava/lang/Object;)I
istore 2
iload 2
iflt L0
aload 0
getfield android/support/v4/n/n/a Landroid/support/v4/n/k;
iload 2
invokevirtual android/support/v4/n/k/a(I)V
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method public final removeAll(Ljava/util/Collection;)Z
aload 0
getfield android/support/v4/n/n/a Landroid/support/v4/n/k;
invokevirtual android/support/v4/n/k/b()Ljava/util/Map;
aload 1
invokestatic android/support/v4/n/k/b(Ljava/util/Map;Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final retainAll(Ljava/util/Collection;)Z
aload 0
getfield android/support/v4/n/n/a Landroid/support/v4/n/k;
invokevirtual android/support/v4/n/k/b()Ljava/util/Map;
aload 1
invokestatic android/support/v4/n/k/c(Ljava/util/Map;Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final size()I
aload 0
getfield android/support/v4/n/n/a Landroid/support/v4/n/k;
invokevirtual android/support/v4/n/k/a()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final toArray()[Ljava/lang/Object;
aload 0
getfield android/support/v4/n/n/a Landroid/support/v4/n/k;
iconst_0
invokevirtual android/support/v4/n/k/b(I)[Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
aload 0
getfield android/support/v4/n/n/a Landroid/support/v4/n/k;
aload 1
iconst_0
invokevirtual android/support/v4/n/k/a([Ljava/lang/Object;I)[Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 3
.end method
