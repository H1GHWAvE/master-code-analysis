.bytecode 50.0
.class public synchronized android/support/v4/widget/SlidingPaneLayout
.super android/view/ViewGroup

.field static final 'a' Landroid/support/v4/widget/di;

.field private static final 'b' Ljava/lang/String; = "SlidingPaneLayout"

.field private static final 'c' I = 32


.field private static final 'd' I = -858993460


.field private static final 'f' I = 400


.field private 'e' I

.field private 'g' I

.field private 'h' Landroid/graphics/drawable/Drawable;

.field private 'i' Landroid/graphics/drawable/Drawable;

.field private final 'j' I

.field private 'k' Z

.field private 'l' Landroid/view/View;

.field private 'm' F

.field private 'n' F

.field private 'o' I

.field private 'p' Z

.field private 'q' I

.field private 'r' F

.field private 's' F

.field private 't' Landroid/support/v4/widget/df;

.field private final 'u' Landroid/support/v4/widget/eg;

.field private 'v' Z

.field private 'w' Z

.field private final 'x' Landroid/graphics/Rect;

.field private final 'y' Ljava/util/ArrayList;

.method static <clinit>()V
getstatic android/os/Build$VERSION/SDK_INT I
istore 0
iload 0
bipush 17
if_icmplt L0
new android/support/v4/widget/dl
dup
invokespecial android/support/v4/widget/dl/<init>()V
putstatic android/support/v4/widget/SlidingPaneLayout/a Landroid/support/v4/widget/di;
return
L0:
iload 0
bipush 16
if_icmplt L1
new android/support/v4/widget/dk
dup
invokespecial android/support/v4/widget/dk/<init>()V
putstatic android/support/v4/widget/SlidingPaneLayout/a Landroid/support/v4/widget/di;
return
L1:
new android/support/v4/widget/dj
dup
invokespecial android/support/v4/widget/dj/<init>()V
putstatic android/support/v4/widget/SlidingPaneLayout/a Landroid/support/v4/widget/di;
return
.limit locals 1
.limit stack 2
.end method

.method private <init>(Landroid/content/Context;)V
aload 0
aload 1
iconst_0
invokespecial android/support/v4/widget/SlidingPaneLayout/<init>(Landroid/content/Context;B)V
return
.limit locals 2
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;B)V
aload 0
aload 1
iconst_0
invokespecial android/support/v4/widget/SlidingPaneLayout/<init>(Landroid/content/Context;C)V
return
.limit locals 3
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;C)V
aload 0
aload 1
aconst_null
iconst_0
invokespecial android/view/ViewGroup/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
ldc_w -858993460
putfield android/support/v4/widget/SlidingPaneLayout/e I
aload 0
iconst_1
putfield android/support/v4/widget/SlidingPaneLayout/w Z
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/v4/widget/SlidingPaneLayout/x Landroid/graphics/Rect;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/widget/SlidingPaneLayout/y Ljava/util/ArrayList;
aload 1
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/density F
fstore 3
aload 0
ldc_w 32.0F
fload 3
fmul
ldc_w 0.5F
fadd
f2i
putfield android/support/v4/widget/SlidingPaneLayout/j I
aload 1
invokestatic android/view/ViewConfiguration/get(Landroid/content/Context;)Landroid/view/ViewConfiguration;
pop
aload 0
iconst_0
invokevirtual android/support/v4/widget/SlidingPaneLayout/setWillNotDraw(Z)V
aload 0
new android/support/v4/widget/db
dup
aload 0
invokespecial android/support/v4/widget/db/<init>(Landroid/support/v4/widget/SlidingPaneLayout;)V
invokestatic android/support/v4/view/cx/a(Landroid/view/View;Landroid/support/v4/view/a;)V
aload 0
iconst_1
invokestatic android/support/v4/view/cx/c(Landroid/view/View;I)V
aload 0
aload 0
ldc_w 0.5F
new android/support/v4/widget/dd
dup
aload 0
iconst_0
invokespecial android/support/v4/widget/dd/<init>(Landroid/support/v4/widget/SlidingPaneLayout;B)V
invokestatic android/support/v4/widget/eg/a(Landroid/view/ViewGroup;FLandroid/support/v4/widget/ej;)Landroid/support/v4/widget/eg;
putfield android/support/v4/widget/SlidingPaneLayout/u Landroid/support/v4/widget/eg;
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/u Landroid/support/v4/widget/eg;
fload 3
ldc_w 400.0F
fmul
putfield android/support/v4/widget/eg/s F
return
.limit locals 4
.limit stack 7
.end method

.method private a(I)V
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/l Landroid/view/View;
ifnonnull L0
aload 0
fconst_0
putfield android/support/v4/widget/SlidingPaneLayout/m F
L1:
return
L0:
aload 0
invokespecial android/support/v4/widget/SlidingPaneLayout/n()Z
istore 4
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/l Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/de
astore 5
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/l Landroid/view/View;
invokevirtual android/view/View/getWidth()I
istore 3
iload 1
istore 2
iload 4
ifeq L2
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getWidth()I
iload 1
isub
iload 3
isub
istore 2
L2:
iload 4
ifeq L3
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingRight()I
istore 1
L4:
iload 4
ifeq L5
aload 5
getfield android/support/v4/widget/de/rightMargin I
istore 3
L6:
aload 0
iload 2
iload 3
iload 1
iadd
isub
i2f
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/o I
i2f
fdiv
putfield android/support/v4/widget/SlidingPaneLayout/m F
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/q I
ifeq L7
aload 0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/m F
invokespecial android/support/v4/widget/SlidingPaneLayout/b(F)V
L7:
aload 5
getfield android/support/v4/widget/de/c Z
ifeq L1
aload 0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/l Landroid/view/View;
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/m F
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/e I
invokespecial android/support/v4/widget/SlidingPaneLayout/a(Landroid/view/View;FI)V
return
L3:
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingLeft()I
istore 1
goto L4
L5:
aload 5
getfield android/support/v4/widget/de/leftMargin I
istore 3
goto L6
.limit locals 6
.limit stack 4
.end method

.method static synthetic a(Landroid/support/v4/widget/SlidingPaneLayout;I)V
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/l Landroid/view/View;
ifnonnull L0
aload 0
fconst_0
putfield android/support/v4/widget/SlidingPaneLayout/m F
L1:
return
L0:
aload 0
invokespecial android/support/v4/widget/SlidingPaneLayout/n()Z
istore 4
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/l Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/de
astore 5
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/l Landroid/view/View;
invokevirtual android/view/View/getWidth()I
istore 3
iload 1
istore 2
iload 4
ifeq L2
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getWidth()I
iload 1
isub
iload 3
isub
istore 2
L2:
iload 4
ifeq L3
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingRight()I
istore 1
L4:
iload 4
ifeq L5
aload 5
getfield android/support/v4/widget/de/rightMargin I
istore 3
L6:
aload 0
iload 2
iload 3
iload 1
iadd
isub
i2f
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/o I
i2f
fdiv
putfield android/support/v4/widget/SlidingPaneLayout/m F
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/q I
ifeq L7
aload 0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/m F
invokespecial android/support/v4/widget/SlidingPaneLayout/b(F)V
L7:
aload 5
getfield android/support/v4/widget/de/c Z
ifeq L1
aload 0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/l Landroid/view/View;
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/m F
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/e I
invokespecial android/support/v4/widget/SlidingPaneLayout/a(Landroid/view/View;FI)V
return
L3:
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingLeft()I
istore 1
goto L4
L5:
aload 5
getfield android/support/v4/widget/de/leftMargin I
istore 3
goto L6
.limit locals 6
.limit stack 4
.end method

.method static synthetic a(Landroid/support/v4/widget/SlidingPaneLayout;Landroid/view/View;)V
aload 0
aload 1
invokespecial android/support/v4/widget/SlidingPaneLayout/d(Landroid/view/View;)V
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/view/View;FI)V
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/de
astore 5
fload 2
fconst_0
fcmpl
ifle L0
iload 3
ifeq L0
ldc_w -16777216
iload 3
iand
bipush 24
iushr
i2f
fload 2
fmul
f2i
istore 4
aload 5
getfield android/support/v4/widget/de/d Landroid/graphics/Paint;
ifnonnull L1
aload 5
new android/graphics/Paint
dup
invokespecial android/graphics/Paint/<init>()V
putfield android/support/v4/widget/de/d Landroid/graphics/Paint;
L1:
aload 5
getfield android/support/v4/widget/de/d Landroid/graphics/Paint;
new android/graphics/PorterDuffColorFilter
dup
iload 4
bipush 24
ishl
ldc_w 16777215
iload 3
iand
ior
getstatic android/graphics/PorterDuff$Mode/SRC_OVER Landroid/graphics/PorterDuff$Mode;
invokespecial android/graphics/PorterDuffColorFilter/<init>(ILandroid/graphics/PorterDuff$Mode;)V
invokevirtual android/graphics/Paint/setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;
pop
aload 1
invokestatic android/support/v4/view/cx/e(Landroid/view/View;)I
iconst_2
if_icmpeq L2
aload 1
iconst_2
aload 5
getfield android/support/v4/widget/de/d Landroid/graphics/Paint;
invokestatic android/support/v4/view/cx/a(Landroid/view/View;ILandroid/graphics/Paint;)V
L2:
aload 0
aload 1
invokespecial android/support/v4/widget/SlidingPaneLayout/d(Landroid/view/View;)V
L3:
return
L0:
aload 1
invokestatic android/support/v4/view/cx/e(Landroid/view/View;)I
ifeq L3
aload 5
getfield android/support/v4/widget/de/d Landroid/graphics/Paint;
ifnull L4
aload 5
getfield android/support/v4/widget/de/d Landroid/graphics/Paint;
aconst_null
invokevirtual android/graphics/Paint/setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;
pop
L4:
new android/support/v4/widget/dc
dup
aload 0
aload 1
invokespecial android/support/v4/widget/dc/<init>(Landroid/support/v4/widget/SlidingPaneLayout;Landroid/view/View;)V
astore 1
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/y Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
aload 1
invokestatic android/support/v4/view/cx/a(Landroid/view/View;Ljava/lang/Runnable;)V
return
.limit locals 6
.limit stack 6
.end method

.method private a(F)Z
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/k Z
ifne L0
iconst_0
ireturn
L0:
aload 0
invokespecial android/support/v4/widget/SlidingPaneLayout/n()Z
istore 5
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/l Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/de
astore 6
iload 5
ifeq L1
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingRight()I
istore 2
aload 6
getfield android/support/v4/widget/de/rightMargin I
istore 3
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/l Landroid/view/View;
invokevirtual android/view/View/getWidth()I
istore 4
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getWidth()I
i2f
iload 3
iload 2
iadd
i2f
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/o I
i2f
fload 1
fmul
fadd
iload 4
i2f
fadd
fsub
f2i
istore 2
L2:
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/u Landroid/support/v4/widget/eg;
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/l Landroid/view/View;
iload 2
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/l Landroid/view/View;
invokevirtual android/view/View/getTop()I
invokevirtual android/support/v4/widget/eg/a(Landroid/view/View;II)Z
ifeq L3
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/a()V
aload 0
invokestatic android/support/v4/view/cx/b(Landroid/view/View;)V
iconst_1
ireturn
L1:
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingLeft()I
istore 2
aload 6
getfield android/support/v4/widget/de/leftMargin I
iload 2
iadd
i2f
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/o I
i2f
fload 1
fmul
fadd
f2i
istore 2
goto L2
L3:
iconst_0
ireturn
.limit locals 7
.limit stack 4
.end method

.method static synthetic a(Landroid/support/v4/widget/SlidingPaneLayout;)Z
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/p Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(Landroid/support/v4/widget/SlidingPaneLayout;Z)Z
aload 0
iload 1
putfield android/support/v4/widget/SlidingPaneLayout/v Z
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/view/View;III)Z
aload 1
instanceof android/view/ViewGroup
ifeq L0
aload 1
checkcast android/view/ViewGroup
astore 8
aload 1
invokevirtual android/view/View/getScrollX()I
istore 6
aload 1
invokevirtual android/view/View/getScrollY()I
istore 7
aload 8
invokevirtual android/view/ViewGroup/getChildCount()I
iconst_1
isub
istore 5
L1:
iload 5
iflt L0
aload 8
iload 5
invokevirtual android/view/ViewGroup/getChildAt(I)Landroid/view/View;
astore 9
iload 3
iload 6
iadd
aload 9
invokevirtual android/view/View/getLeft()I
if_icmplt L2
iload 3
iload 6
iadd
aload 9
invokevirtual android/view/View/getRight()I
if_icmpge L2
iload 4
iload 7
iadd
aload 9
invokevirtual android/view/View/getTop()I
if_icmplt L2
iload 4
iload 7
iadd
aload 9
invokevirtual android/view/View/getBottom()I
if_icmpge L2
aload 0
aload 9
iload 2
iload 3
iload 6
iadd
aload 9
invokevirtual android/view/View/getLeft()I
isub
iload 4
iload 7
iadd
aload 9
invokevirtual android/view/View/getTop()I
isub
invokespecial android/support/v4/widget/SlidingPaneLayout/a(Landroid/view/View;III)Z
ifeq L2
iconst_1
ireturn
L2:
iload 5
iconst_1
isub
istore 5
goto L1
L0:
aload 0
invokespecial android/support/v4/widget/SlidingPaneLayout/n()Z
ifeq L3
L4:
aload 1
iload 2
invokestatic android/support/v4/view/cx/a(Landroid/view/View;I)Z
ifeq L5
iconst_1
ireturn
L3:
iload 2
ineg
istore 2
goto L4
L5:
iconst_0
ireturn
.limit locals 10
.limit stack 6
.end method

.method static synthetic b(Landroid/support/v4/widget/SlidingPaneLayout;)Landroid/support/v4/widget/eg;
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/u Landroid/support/v4/widget/eg;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static b()V
return
.limit locals 0
.limit stack 0
.end method

.method private b(F)V
aload 0
invokespecial android/support/v4/widget/SlidingPaneLayout/n()Z
istore 8
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/l Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/de
astore 9
aload 9
getfield android/support/v4/widget/de/c Z
ifeq L0
iload 8
ifeq L1
aload 9
getfield android/support/v4/widget/de/rightMargin I
istore 3
L2:
iload 3
ifgt L0
iconst_1
istore 3
L3:
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildCount()I
istore 7
iconst_0
istore 4
L4:
iload 4
iload 7
if_icmpge L5
aload 0
iload 4
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildAt(I)Landroid/view/View;
astore 9
aload 9
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/l Landroid/view/View;
if_acmpeq L6
fconst_1
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/n F
fsub
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/q I
i2f
fmul
f2i
istore 5
aload 0
fload 1
putfield android/support/v4/widget/SlidingPaneLayout/n F
iload 5
fconst_1
fload 1
fsub
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/q I
i2f
fmul
f2i
isub
istore 6
iload 6
istore 5
iload 8
ifeq L7
iload 6
ineg
istore 5
L7:
aload 9
iload 5
invokevirtual android/view/View/offsetLeftAndRight(I)V
iload 3
ifeq L6
iload 8
ifeq L8
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/n F
fconst_1
fsub
fstore 2
L9:
aload 0
aload 9
fload 2
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/g I
invokespecial android/support/v4/widget/SlidingPaneLayout/a(Landroid/view/View;FI)V
L6:
iload 4
iconst_1
iadd
istore 4
goto L4
L1:
aload 9
getfield android/support/v4/widget/de/leftMargin I
istore 3
goto L2
L0:
iconst_0
istore 3
goto L3
L8:
fconst_1
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/n F
fsub
fstore 2
goto L9
L5:
return
.limit locals 10
.limit stack 4
.end method

.method static synthetic c(Landroid/support/v4/widget/SlidingPaneLayout;)F
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/m F
freturn
.limit locals 1
.limit stack 1
.end method

.method private c()V
aload 0
bipush 32
invokevirtual android/support/v4/widget/SlidingPaneLayout/sendAccessibilityEvent(I)V
return
.limit locals 1
.limit stack 2
.end method

.method private static c(Landroid/view/View;)Z
aload 0
invokestatic android/support/v4/view/cx/h(Landroid/view/View;)Z
ifeq L0
L1:
iconst_1
ireturn
L0:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 18
if_icmplt L2
iconst_0
ireturn
L2:
aload 0
invokevirtual android/view/View/getBackground()Landroid/graphics/drawable/Drawable;
astore 0
aload 0
ifnull L3
aload 0
invokevirtual android/graphics/drawable/Drawable/getOpacity()I
iconst_m1
if_icmpeq L1
iconst_0
ireturn
L3:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic d(Landroid/support/v4/widget/SlidingPaneLayout;)Landroid/view/View;
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/l Landroid/view/View;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d()V
aload 0
bipush 32
invokevirtual android/support/v4/widget/SlidingPaneLayout/sendAccessibilityEvent(I)V
return
.limit locals 1
.limit stack 2
.end method

.method private d(Landroid/view/View;)V
getstatic android/support/v4/widget/SlidingPaneLayout/a Landroid/support/v4/widget/di;
aload 0
aload 1
invokeinterface android/support/v4/widget/di/a(Landroid/support/v4/widget/SlidingPaneLayout;Landroid/view/View;)V 2
return
.limit locals 2
.limit stack 3
.end method

.method private e()Z
iconst_0
istore 1
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/w Z
ifne L0
aload 0
fconst_0
invokespecial android/support/v4/widget/SlidingPaneLayout/a(F)Z
ifeq L1
L0:
aload 0
iconst_0
putfield android/support/v4/widget/SlidingPaneLayout/v Z
iconst_1
istore 1
L1:
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic e(Landroid/support/v4/widget/SlidingPaneLayout;)Z
aload 0
invokespecial android/support/v4/widget/SlidingPaneLayout/n()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic f(Landroid/support/v4/widget/SlidingPaneLayout;)I
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/o I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private f()Z
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/w Z
ifne L0
aload 0
fconst_1
invokespecial android/support/v4/widget/SlidingPaneLayout/a(F)Z
ifeq L1
L0:
aload 0
iconst_1
putfield android/support/v4/widget/SlidingPaneLayout/v Z
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic g(Landroid/support/v4/widget/SlidingPaneLayout;)Ljava/util/ArrayList;
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/y Ljava/util/ArrayList;
areturn
.limit locals 1
.limit stack 1
.end method

.method private g()V
.annotation visible Ljava/lang/Deprecated;
.end annotation
aload 0
invokespecial android/support/v4/widget/SlidingPaneLayout/h()Z
pop
return
.limit locals 1
.limit stack 1
.end method

.method private h()Z
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/w Z
ifne L0
aload 0
fconst_1
invokespecial android/support/v4/widget/SlidingPaneLayout/a(F)Z
ifeq L1
L0:
aload 0
iconst_1
putfield android/support/v4/widget/SlidingPaneLayout/v Z
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private i()V
.annotation visible Ljava/lang/Deprecated;
.end annotation
aload 0
invokespecial android/support/v4/widget/SlidingPaneLayout/e()Z
pop
return
.limit locals 1
.limit stack 1
.end method

.method private j()Z
aload 0
invokespecial android/support/v4/widget/SlidingPaneLayout/e()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private k()Z
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/k Z
ifeq L0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/m F
fconst_1
fcmpl
ifne L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private l()Z
.annotation visible Ljava/lang/Deprecated;
.end annotation
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/k Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private m()Z
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/k Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private n()Z
aload 0
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
iconst_1
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method final a()V
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildCount()I
istore 2
iconst_0
istore 1
L0:
iload 1
iload 2
if_icmpge L1
aload 0
iload 1
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildAt(I)Landroid/view/View;
astore 3
aload 3
invokevirtual android/view/View/getVisibility()I
iconst_4
if_icmpne L2
aload 3
iconst_0
invokevirtual android/view/View/setVisibility(I)V
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
return
.limit locals 4
.limit stack 2
.end method

.method final a(Landroid/view/View;)V
aload 0
invokespecial android/support/v4/widget/SlidingPaneLayout/n()Z
istore 17
iload 17
ifeq L0
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getWidth()I
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingRight()I
isub
istore 2
L1:
iload 17
ifeq L2
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingLeft()I
istore 3
L3:
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingTop()I
istore 10
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getHeight()I
istore 11
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingBottom()I
istore 12
aload 1
ifnull L4
aload 1
invokestatic android/support/v4/view/cx/h(Landroid/view/View;)Z
ifeq L5
iconst_1
istore 4
L6:
iload 4
ifeq L4
aload 1
invokevirtual android/view/View/getLeft()I
istore 7
aload 1
invokevirtual android/view/View/getRight()I
istore 6
aload 1
invokevirtual android/view/View/getTop()I
istore 5
aload 1
invokevirtual android/view/View/getBottom()I
istore 4
L7:
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildCount()I
istore 13
iconst_0
istore 8
L8:
iload 8
iload 13
if_icmpge L9
aload 0
iload 8
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildAt(I)Landroid/view/View;
astore 18
aload 18
aload 1
if_acmpeq L9
iload 17
ifeq L10
iload 3
istore 9
L11:
iload 9
aload 18
invokevirtual android/view/View/getLeft()I
invokestatic java/lang/Math/max(II)I
istore 14
iload 10
aload 18
invokevirtual android/view/View/getTop()I
invokestatic java/lang/Math/max(II)I
istore 15
iload 17
ifeq L12
iload 2
istore 9
L13:
iload 9
aload 18
invokevirtual android/view/View/getRight()I
invokestatic java/lang/Math/min(II)I
istore 9
iload 11
iload 12
isub
aload 18
invokevirtual android/view/View/getBottom()I
invokestatic java/lang/Math/min(II)I
istore 16
iload 14
iload 7
if_icmplt L14
iload 15
iload 5
if_icmplt L14
iload 9
iload 6
if_icmpgt L14
iload 16
iload 4
if_icmpgt L14
iconst_4
istore 9
L15:
aload 18
iload 9
invokevirtual android/view/View/setVisibility(I)V
iload 8
iconst_1
iadd
istore 8
goto L8
L0:
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingLeft()I
istore 2
goto L1
L2:
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getWidth()I
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingRight()I
isub
istore 3
goto L3
L5:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 18
if_icmpge L16
aload 1
invokevirtual android/view/View/getBackground()Landroid/graphics/drawable/Drawable;
astore 18
aload 18
ifnull L16
aload 18
invokevirtual android/graphics/drawable/Drawable/getOpacity()I
iconst_m1
if_icmpne L17
iconst_1
istore 4
goto L6
L17:
iconst_0
istore 4
goto L6
L16:
iconst_0
istore 4
goto L6
L4:
iconst_0
istore 4
iconst_0
istore 5
iconst_0
istore 6
iconst_0
istore 7
goto L7
L10:
iload 2
istore 9
goto L11
L12:
iload 3
istore 9
goto L13
L14:
iconst_0
istore 9
goto L15
L9:
return
.limit locals 19
.limit stack 2
.end method

.method final b(Landroid/view/View;)Z
aload 1
ifnonnull L0
iconst_0
ireturn
L0:
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/de
astore 1
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/k Z
ifeq L1
aload 1
getfield android/support/v4/widget/de/c Z
ifeq L1
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/m F
fconst_0
fcmpl
ifle L1
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
aload 1
instanceof android/support/v4/widget/de
ifeq L0
aload 0
aload 1
invokespecial android/view/ViewGroup/checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public computeScroll()V
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/u Landroid/support/v4/widget/eg;
invokevirtual android/support/v4/widget/eg/c()Z
ifeq L0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/k Z
ifne L1
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/u Landroid/support/v4/widget/eg;
invokevirtual android/support/v4/widget/eg/b()V
L0:
return
L1:
aload 0
invokestatic android/support/v4/view/cx/b(Landroid/view/View;)V
return
.limit locals 1
.limit stack 1
.end method

.method public draw(Landroid/graphics/Canvas;)V
aload 0
aload 1
invokespecial android/view/ViewGroup/draw(Landroid/graphics/Canvas;)V
aload 0
invokespecial android/support/v4/widget/SlidingPaneLayout/n()Z
ifeq L0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/i Landroid/graphics/drawable/Drawable;
astore 7
L1:
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildCount()I
iconst_1
if_icmple L2
aload 0
iconst_1
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildAt(I)Landroid/view/View;
astore 8
L3:
aload 8
ifnull L4
aload 7
ifnonnull L5
L4:
return
L0:
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/h Landroid/graphics/drawable/Drawable;
astore 7
goto L1
L2:
aconst_null
astore 8
goto L3
L5:
aload 8
invokevirtual android/view/View/getTop()I
istore 4
aload 8
invokevirtual android/view/View/getBottom()I
istore 5
aload 7
invokevirtual android/graphics/drawable/Drawable/getIntrinsicWidth()I
istore 6
aload 0
invokespecial android/support/v4/widget/SlidingPaneLayout/n()Z
ifeq L6
aload 8
invokevirtual android/view/View/getRight()I
istore 3
iload 3
iload 6
iadd
istore 2
L7:
aload 7
iload 3
iload 4
iload 2
iload 5
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
aload 7
aload 1
invokevirtual android/graphics/drawable/Drawable/draw(Landroid/graphics/Canvas;)V
return
L6:
aload 8
invokevirtual android/view/View/getLeft()I
istore 2
iload 2
iload 6
isub
istore 3
goto L7
.limit locals 9
.limit stack 5
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
aload 2
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/de
astore 7
aload 1
iconst_2
invokevirtual android/graphics/Canvas/save(I)I
istore 5
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/k Z
ifeq L0
aload 7
getfield android/support/v4/widget/de/b Z
ifne L0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/l Landroid/view/View;
ifnull L0
aload 1
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/x Landroid/graphics/Rect;
invokevirtual android/graphics/Canvas/getClipBounds(Landroid/graphics/Rect;)Z
pop
aload 0
invokespecial android/support/v4/widget/SlidingPaneLayout/n()Z
ifeq L1
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/x Landroid/graphics/Rect;
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/x Landroid/graphics/Rect;
getfield android/graphics/Rect/left I
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/l Landroid/view/View;
invokevirtual android/view/View/getRight()I
invokestatic java/lang/Math/max(II)I
putfield android/graphics/Rect/left I
L2:
aload 1
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/x Landroid/graphics/Rect;
invokevirtual android/graphics/Canvas/clipRect(Landroid/graphics/Rect;)Z
pop
L0:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmpge L3
aload 7
getfield android/support/v4/widget/de/c Z
ifeq L4
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/m F
fconst_0
fcmpl
ifle L4
aload 2
invokevirtual android/view/View/isDrawingCacheEnabled()Z
ifne L5
aload 2
iconst_1
invokevirtual android/view/View/setDrawingCacheEnabled(Z)V
L5:
aload 2
invokevirtual android/view/View/getDrawingCache()Landroid/graphics/Bitmap;
astore 8
aload 8
ifnull L6
aload 1
aload 8
aload 2
invokevirtual android/view/View/getLeft()I
i2f
aload 2
invokevirtual android/view/View/getTop()I
i2f
aload 7
getfield android/support/v4/widget/de/d Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
iconst_0
istore 6
L7:
aload 1
iload 5
invokevirtual android/graphics/Canvas/restoreToCount(I)V
iload 6
ireturn
L1:
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/x Landroid/graphics/Rect;
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/x Landroid/graphics/Rect;
getfield android/graphics/Rect/right I
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/l Landroid/view/View;
invokevirtual android/view/View/getLeft()I
invokestatic java/lang/Math/min(II)I
putfield android/graphics/Rect/right I
goto L2
L6:
ldc "SlidingPaneLayout"
new java/lang/StringBuilder
dup
ldc "drawChild: child view "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " returned null drawing cache"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
L3:
aload 0
aload 1
aload 2
lload 3
invokespecial android/view/ViewGroup/drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
istore 6
goto L7
L4:
aload 2
invokevirtual android/view/View/isDrawingCacheEnabled()Z
ifeq L3
aload 2
iconst_0
invokevirtual android/view/View/setDrawingCacheEnabled(Z)V
goto L3
.limit locals 9
.limit stack 5
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
new android/support/v4/widget/de
dup
invokespecial android/support/v4/widget/de/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
new android/support/v4/widget/de
dup
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getContext()Landroid/content/Context;
aload 1
invokespecial android/support/v4/widget/de/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
aload 1
instanceof android/view/ViewGroup$MarginLayoutParams
ifeq L0
new android/support/v4/widget/de
dup
aload 1
checkcast android/view/ViewGroup$MarginLayoutParams
invokespecial android/support/v4/widget/de/<init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
areturn
L0:
new android/support/v4/widget/de
dup
aload 1
invokespecial android/support/v4/widget/de/<init>(Landroid/view/ViewGroup$LayoutParams;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public getCoveredFadeColor()I
.annotation invisible Landroid/support/a/j;
.end annotation
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/g I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getParallaxDistance()I
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/q I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getSliderFadeColor()I
.annotation invisible Landroid/support/a/j;
.end annotation
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/e I
ireturn
.limit locals 1
.limit stack 1
.end method

.method protected onAttachedToWindow()V
aload 0
invokespecial android/view/ViewGroup/onAttachedToWindow()V
aload 0
iconst_1
putfield android/support/v4/widget/SlidingPaneLayout/w Z
return
.limit locals 1
.limit stack 2
.end method

.method protected onDetachedFromWindow()V
aload 0
invokespecial android/view/ViewGroup/onDetachedFromWindow()V
aload 0
iconst_1
putfield android/support/v4/widget/SlidingPaneLayout/w Z
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/y Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 2
iconst_0
istore 1
L0:
iload 1
iload 2
if_icmpge L1
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/y Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/widget/dc
invokevirtual android/support/v4/widget/dc/run()V
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/y Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
return
.limit locals 3
.limit stack 2
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
iconst_0
istore 6
aload 1
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;)I
istore 4
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/k Z
ifne L0
iload 4
ifne L0
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildCount()I
iconst_1
if_icmple L0
aload 0
iconst_1
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildAt(I)Landroid/view/View;
astore 7
aload 7
ifnull L0
aload 7
aload 1
invokevirtual android/view/MotionEvent/getX()F
f2i
aload 1
invokevirtual android/view/MotionEvent/getY()F
f2i
invokestatic android/support/v4/widget/eg/b(Landroid/view/View;II)Z
ifne L1
iconst_1
istore 5
L2:
aload 0
iload 5
putfield android/support/v4/widget/SlidingPaneLayout/v Z
L0:
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/k Z
ifeq L3
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/p Z
ifeq L4
iload 4
ifeq L4
L3:
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/u Landroid/support/v4/widget/eg;
invokevirtual android/support/v4/widget/eg/a()V
aload 0
aload 1
invokespecial android/view/ViewGroup/onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
istore 5
L5:
iload 5
ireturn
L1:
iconst_0
istore 5
goto L2
L4:
iload 4
iconst_3
if_icmpeq L6
iload 4
iconst_1
if_icmpne L7
L6:
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/u Landroid/support/v4/widget/eg;
invokevirtual android/support/v4/widget/eg/a()V
iconst_0
ireturn
L7:
iload 4
tableswitch 0
L8
L9
L10
default : L9
L9:
iconst_0
istore 4
L11:
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/u Landroid/support/v4/widget/eg;
aload 1
invokevirtual android/support/v4/widget/eg/a(Landroid/view/MotionEvent;)Z
ifne L12
iload 6
istore 5
iload 4
ifeq L5
L12:
iconst_1
ireturn
L8:
aload 0
iconst_0
putfield android/support/v4/widget/SlidingPaneLayout/p Z
aload 1
invokevirtual android/view/MotionEvent/getX()F
fstore 2
aload 1
invokevirtual android/view/MotionEvent/getY()F
fstore 3
aload 0
fload 2
putfield android/support/v4/widget/SlidingPaneLayout/r F
aload 0
fload 3
putfield android/support/v4/widget/SlidingPaneLayout/s F
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/l Landroid/view/View;
fload 2
f2i
fload 3
f2i
invokestatic android/support/v4/widget/eg/b(Landroid/view/View;II)Z
ifeq L9
aload 0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/l Landroid/view/View;
invokevirtual android/support/v4/widget/SlidingPaneLayout/b(Landroid/view/View;)Z
ifeq L9
iconst_1
istore 4
goto L11
L10:
aload 1
invokevirtual android/view/MotionEvent/getX()F
fstore 3
aload 1
invokevirtual android/view/MotionEvent/getY()F
fstore 2
fload 3
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/r F
fsub
invokestatic java/lang/Math/abs(F)F
fstore 3
fload 2
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/s F
fsub
invokestatic java/lang/Math/abs(F)F
fstore 2
fload 3
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/u Landroid/support/v4/widget/eg;
getfield android/support/v4/widget/eg/n I
i2f
fcmpl
ifle L9
fload 2
fload 3
fcmpl
ifle L9
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/u Landroid/support/v4/widget/eg;
invokevirtual android/support/v4/widget/eg/a()V
aload 0
iconst_1
putfield android/support/v4/widget/SlidingPaneLayout/p Z
iconst_0
ireturn
.limit locals 8
.limit stack 3
.end method

.method protected onLayout(ZIIII)V
aload 0
invokespecial android/support/v4/widget/SlidingPaneLayout/n()Z
istore 14
iload 14
ifeq L0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/u Landroid/support/v4/widget/eg;
iconst_2
putfield android/support/v4/widget/eg/u I
L1:
iload 4
iload 2
isub
istore 9
iload 14
ifeq L2
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingRight()I
istore 2
L3:
iload 14
ifeq L4
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingLeft()I
istore 4
L5:
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingTop()I
istore 11
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildCount()I
istore 10
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/w Z
ifeq L6
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/k Z
ifeq L7
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/v Z
ifeq L7
fconst_1
fstore 6
L8:
aload 0
fload 6
putfield android/support/v4/widget/SlidingPaneLayout/m F
L6:
iconst_0
istore 5
iload 2
istore 3
L9:
iload 5
iload 10
if_icmpge L10
aload 0
iload 5
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildAt(I)Landroid/view/View;
astore 15
aload 15
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L11
aload 15
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/de
astore 16
aload 15
invokevirtual android/view/View/getMeasuredWidth()I
istore 12
iconst_0
istore 8
aload 16
getfield android/support/v4/widget/de/b Z
ifeq L12
aload 16
getfield android/support/v4/widget/de/leftMargin I
istore 7
aload 16
getfield android/support/v4/widget/de/rightMargin I
istore 13
iload 2
iload 9
iload 4
isub
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/j I
isub
invokestatic java/lang/Math/min(II)I
iload 3
isub
iload 7
iload 13
iadd
isub
istore 13
aload 0
iload 13
putfield android/support/v4/widget/SlidingPaneLayout/o I
iload 14
ifeq L13
aload 16
getfield android/support/v4/widget/de/rightMargin I
istore 7
L14:
iload 3
iload 7
iadd
iload 13
iadd
iload 12
iconst_2
idiv
iadd
iload 9
iload 4
isub
if_icmple L15
iconst_1
istore 1
L16:
aload 16
iload 1
putfield android/support/v4/widget/de/c Z
iload 13
i2f
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/m F
fmul
f2i
istore 13
iload 3
iload 7
iload 13
iadd
iadd
istore 3
aload 0
iload 13
i2f
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/o I
i2f
fdiv
putfield android/support/v4/widget/SlidingPaneLayout/m F
iload 8
istore 7
L17:
iload 14
ifeq L18
iload 9
iload 3
isub
iload 7
iadd
istore 8
iload 8
iload 12
isub
istore 7
L19:
aload 15
iload 7
iload 11
iload 8
aload 15
invokevirtual android/view/View/getMeasuredHeight()I
iload 11
iadd
invokevirtual android/view/View/layout(IIII)V
aload 15
invokevirtual android/view/View/getWidth()I
iload 2
iadd
istore 7
iload 3
istore 2
iload 7
istore 3
L20:
iload 5
iconst_1
iadd
istore 7
iload 2
istore 5
iload 3
istore 2
iload 5
istore 3
iload 7
istore 5
goto L9
L0:
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/u Landroid/support/v4/widget/eg;
iconst_1
putfield android/support/v4/widget/eg/u I
goto L1
L2:
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingLeft()I
istore 2
goto L3
L4:
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingRight()I
istore 4
goto L5
L7:
fconst_0
fstore 6
goto L8
L13:
aload 16
getfield android/support/v4/widget/de/leftMargin I
istore 7
goto L14
L15:
iconst_0
istore 1
goto L16
L12:
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/k Z
ifeq L21
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/q I
ifeq L21
fconst_1
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/m F
fsub
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/q I
i2f
fmul
f2i
istore 3
L22:
iload 3
istore 7
iload 2
istore 3
goto L17
L18:
iload 3
iload 7
isub
istore 7
iload 7
iload 12
iadd
istore 8
goto L19
L10:
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/w Z
ifeq L23
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/k Z
ifeq L24
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/q I
ifeq L25
aload 0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/m F
invokespecial android/support/v4/widget/SlidingPaneLayout/b(F)V
L25:
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/l Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/de
getfield android/support/v4/widget/de/c Z
ifeq L26
aload 0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/l Landroid/view/View;
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/m F
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/e I
invokespecial android/support/v4/widget/SlidingPaneLayout/a(Landroid/view/View;FI)V
L26:
aload 0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/l Landroid/view/View;
invokevirtual android/support/v4/widget/SlidingPaneLayout/a(Landroid/view/View;)V
L23:
aload 0
iconst_0
putfield android/support/v4/widget/SlidingPaneLayout/w Z
return
L24:
iconst_0
istore 2
L27:
iload 2
iload 10
if_icmpge L26
aload 0
aload 0
iload 2
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildAt(I)Landroid/view/View;
fconst_0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/e I
invokespecial android/support/v4/widget/SlidingPaneLayout/a(Landroid/view/View;FI)V
iload 2
iconst_1
iadd
istore 2
goto L27
L21:
iconst_0
istore 3
goto L22
L11:
iload 3
istore 7
iload 2
istore 3
iload 7
istore 2
goto L20
.limit locals 17
.limit stack 6
.end method

.method protected onMeasure(II)V
iload 1
invokestatic android/view/View$MeasureSpec/getMode(I)I
istore 7
iload 1
invokestatic android/view/View$MeasureSpec/getSize(I)I
istore 5
iload 2
invokestatic android/view/View$MeasureSpec/getMode(I)I
istore 6
iload 2
invokestatic android/view/View$MeasureSpec/getSize(I)I
istore 1
iload 7
ldc_w 1073741824
if_icmpeq L0
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/isInEditMode()Z
ifeq L1
iload 7
ldc_w -2147483648
if_icmpeq L2
iload 7
ifne L2
iload 6
istore 5
sipush 300
istore 6
L3:
iload 5
lookupswitch
-2147483648 : L4
1073741824 : L5
default : L6
L6:
iconst_0
istore 1
iconst_m1
istore 7
L7:
iconst_0
istore 15
iload 6
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingLeft()I
isub
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingRight()I
isub
istore 11
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildCount()I
istore 12
iload 12
iconst_2
if_icmple L8
ldc "SlidingPaneLayout"
ldc "onMeasure: More than two child views are not supported."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
L8:
aload 0
aconst_null
putfield android/support/v4/widget/SlidingPaneLayout/l Landroid/view/View;
iconst_0
istore 10
iload 11
istore 2
fconst_0
fstore 3
L9:
iload 10
iload 12
if_icmpge L10
aload 0
iload 10
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildAt(I)Landroid/view/View;
astore 17
aload 17
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/de
astore 18
aload 17
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpne L11
aload 18
iconst_0
putfield android/support/v4/widget/de/c Z
iload 2
istore 8
iload 1
istore 2
iload 8
istore 1
L12:
iload 10
iconst_1
iadd
istore 10
iload 2
istore 8
iload 1
istore 2
iload 8
istore 1
goto L9
L1:
new java/lang/IllegalStateException
dup
ldc "Width must have an exact value or MATCH_PARENT"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
iload 6
ifne L2
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/isInEditMode()Z
ifeq L13
iload 6
ifne L2
ldc_w -2147483648
istore 2
iload 5
istore 6
sipush 300
istore 1
iload 2
istore 5
goto L3
L13:
new java/lang/IllegalStateException
dup
ldc "Height must not be UNSPECIFIED"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L5:
iload 1
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingTop()I
isub
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingBottom()I
isub
istore 1
iload 1
istore 7
goto L7
L4:
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingTop()I
istore 7
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingBottom()I
istore 8
iconst_0
istore 2
iload 1
iload 7
isub
iload 8
isub
istore 7
iload 2
istore 1
goto L7
L11:
fload 3
fstore 4
aload 18
getfield android/support/v4/widget/de/a F
fconst_0
fcmpl
ifle L14
fload 3
aload 18
getfield android/support/v4/widget/de/a F
fadd
fstore 4
aload 18
getfield android/support/v4/widget/de/width I
ifeq L15
L14:
aload 18
getfield android/support/v4/widget/de/leftMargin I
aload 18
getfield android/support/v4/widget/de/rightMargin I
iadd
istore 8
aload 18
getfield android/support/v4/widget/de/width I
bipush -2
if_icmpne L16
iload 11
iload 8
isub
ldc_w -2147483648
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 8
L17:
aload 18
getfield android/support/v4/widget/de/height I
bipush -2
if_icmpne L18
iload 7
ldc_w -2147483648
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 9
L19:
aload 17
iload 8
iload 9
invokevirtual android/view/View/measure(II)V
aload 17
invokevirtual android/view/View/getMeasuredWidth()I
istore 9
aload 17
invokevirtual android/view/View/getMeasuredHeight()I
istore 13
iload 1
istore 8
iload 5
ldc_w -2147483648
if_icmpne L20
iload 1
istore 8
iload 13
iload 1
if_icmple L20
iload 13
iload 7
invokestatic java/lang/Math/min(II)I
istore 8
L20:
iload 2
iload 9
isub
istore 1
iload 1
ifge L21
iconst_1
istore 16
L22:
aload 18
iload 16
putfield android/support/v4/widget/de/b Z
aload 18
getfield android/support/v4/widget/de/b Z
ifeq L23
aload 0
aload 17
putfield android/support/v4/widget/SlidingPaneLayout/l Landroid/view/View;
L23:
fload 4
fstore 3
iload 16
iload 15
ior
istore 15
iload 8
istore 2
goto L12
L16:
aload 18
getfield android/support/v4/widget/de/width I
iconst_m1
if_icmpne L24
iload 11
iload 8
isub
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 8
goto L17
L24:
aload 18
getfield android/support/v4/widget/de/width I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 8
goto L17
L18:
aload 18
getfield android/support/v4/widget/de/height I
iconst_m1
if_icmpne L25
iload 7
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 9
goto L19
L25:
aload 18
getfield android/support/v4/widget/de/height I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 9
goto L19
L21:
iconst_0
istore 16
goto L22
L10:
iload 15
ifne L26
fload 3
fconst_0
fcmpl
ifle L27
L26:
iload 11
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/j I
isub
istore 10
iconst_0
istore 8
L28:
iload 8
iload 12
if_icmpge L27
aload 0
iload 8
invokevirtual android/support/v4/widget/SlidingPaneLayout/getChildAt(I)Landroid/view/View;
astore 17
aload 17
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L29
aload 17
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/de
astore 18
aload 17
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L29
aload 18
getfield android/support/v4/widget/de/width I
ifne L30
aload 18
getfield android/support/v4/widget/de/a F
fconst_0
fcmpl
ifle L30
iconst_1
istore 5
L31:
iload 5
ifeq L32
iconst_0
istore 9
L33:
iload 15
ifeq L34
aload 17
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/l Landroid/view/View;
if_acmpeq L34
aload 18
getfield android/support/v4/widget/de/width I
ifge L29
iload 9
iload 10
if_icmpgt L35
aload 18
getfield android/support/v4/widget/de/a F
fconst_0
fcmpl
ifle L29
L35:
iload 5
ifeq L36
aload 18
getfield android/support/v4/widget/de/height I
bipush -2
if_icmpne L37
iload 7
ldc_w -2147483648
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 5
L38:
aload 17
iload 10
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
iload 5
invokevirtual android/view/View/measure(II)V
L29:
iload 8
iconst_1
iadd
istore 8
goto L28
L30:
iconst_0
istore 5
goto L31
L32:
aload 17
invokevirtual android/view/View/getMeasuredWidth()I
istore 9
goto L33
L37:
aload 18
getfield android/support/v4/widget/de/height I
iconst_m1
if_icmpne L39
iload 7
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 5
goto L38
L39:
aload 18
getfield android/support/v4/widget/de/height I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 5
goto L38
L36:
aload 17
invokevirtual android/view/View/getMeasuredHeight()I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 5
goto L38
L34:
aload 18
getfield android/support/v4/widget/de/a F
fconst_0
fcmpl
ifle L29
aload 18
getfield android/support/v4/widget/de/width I
ifne L40
aload 18
getfield android/support/v4/widget/de/height I
bipush -2
if_icmpne L41
iload 7
ldc_w -2147483648
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 5
L42:
iload 15
ifeq L43
aload 18
getfield android/support/v4/widget/de/leftMargin I
istore 13
iload 11
aload 18
getfield android/support/v4/widget/de/rightMargin I
iload 13
iadd
isub
istore 13
iload 13
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 14
iload 9
iload 13
if_icmpeq L29
aload 17
iload 14
iload 5
invokevirtual android/view/View/measure(II)V
goto L29
L41:
aload 18
getfield android/support/v4/widget/de/height I
iconst_m1
if_icmpne L44
iload 7
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 5
goto L42
L44:
aload 18
getfield android/support/v4/widget/de/height I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 5
goto L42
L40:
aload 17
invokevirtual android/view/View/getMeasuredHeight()I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 5
goto L42
L43:
iconst_0
iload 2
invokestatic java/lang/Math/max(II)I
istore 13
aload 17
aload 18
getfield android/support/v4/widget/de/a F
iload 13
i2f
fmul
fload 3
fdiv
f2i
iload 9
iadd
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
iload 5
invokevirtual android/view/View/measure(II)V
goto L29
L27:
aload 0
iload 6
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingTop()I
iload 1
iadd
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingBottom()I
iadd
invokevirtual android/support/v4/widget/SlidingPaneLayout/setMeasuredDimension(II)V
aload 0
iload 15
putfield android/support/v4/widget/SlidingPaneLayout/k Z
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/u Landroid/support/v4/widget/eg;
getfield android/support/v4/widget/eg/m I
ifeq L45
iload 15
ifne L45
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/u Landroid/support/v4/widget/eg;
invokevirtual android/support/v4/widget/eg/b()V
L45:
return
L15:
iload 1
istore 8
fload 4
fstore 3
iload 2
istore 1
iload 8
istore 2
goto L12
L2:
iload 6
istore 2
iload 5
istore 6
iload 2
istore 5
goto L3
.limit locals 19
.limit stack 4
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
aload 1
checkcast android/support/v4/widget/SlidingPaneLayout$SavedState
astore 1
aload 0
aload 1
invokevirtual android/support/v4/widget/SlidingPaneLayout$SavedState/getSuperState()Landroid/os/Parcelable;
invokespecial android/view/ViewGroup/onRestoreInstanceState(Landroid/os/Parcelable;)V
aload 1
getfield android/support/v4/widget/SlidingPaneLayout$SavedState/a Z
ifeq L0
aload 0
invokespecial android/support/v4/widget/SlidingPaneLayout/h()Z
pop
L1:
aload 0
aload 1
getfield android/support/v4/widget/SlidingPaneLayout$SavedState/a Z
putfield android/support/v4/widget/SlidingPaneLayout/v Z
return
L0:
aload 0
invokespecial android/support/v4/widget/SlidingPaneLayout/e()Z
pop
goto L1
.limit locals 2
.limit stack 2
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
new android/support/v4/widget/SlidingPaneLayout$SavedState
dup
aload 0
invokespecial android/view/ViewGroup/onSaveInstanceState()Landroid/os/Parcelable;
invokespecial android/support/v4/widget/SlidingPaneLayout$SavedState/<init>(Landroid/os/Parcelable;)V
astore 2
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/k Z
ifeq L0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/k Z
ifeq L1
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/m F
fconst_1
fcmpl
ifne L2
L1:
iconst_1
istore 1
L3:
aload 2
iload 1
putfield android/support/v4/widget/SlidingPaneLayout$SavedState/a Z
aload 2
areturn
L2:
iconst_0
istore 1
goto L3
L0:
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/v Z
istore 1
goto L3
.limit locals 3
.limit stack 3
.end method

.method protected onSizeChanged(IIII)V
aload 0
iload 1
iload 2
iload 3
iload 4
invokespecial android/view/ViewGroup/onSizeChanged(IIII)V
iload 1
iload 3
if_icmpeq L0
aload 0
iconst_1
putfield android/support/v4/widget/SlidingPaneLayout/w Z
L0:
return
.limit locals 5
.limit stack 5
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/k Z
ifne L0
aload 0
aload 1
invokespecial android/view/ViewGroup/onTouchEvent(Landroid/view/MotionEvent;)Z
ireturn
L0:
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/u Landroid/support/v4/widget/eg;
aload 1
invokevirtual android/support/v4/widget/eg/b(Landroid/view/MotionEvent;)V
aload 1
invokevirtual android/view/MotionEvent/getAction()I
sipush 255
iand
tableswitch 0
L1
L2
default : L3
L3:
iconst_1
ireturn
L1:
aload 1
invokevirtual android/view/MotionEvent/getX()F
fstore 2
aload 1
invokevirtual android/view/MotionEvent/getY()F
fstore 3
aload 0
fload 2
putfield android/support/v4/widget/SlidingPaneLayout/r F
aload 0
fload 3
putfield android/support/v4/widget/SlidingPaneLayout/s F
goto L3
L2:
aload 0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/l Landroid/view/View;
invokevirtual android/support/v4/widget/SlidingPaneLayout/b(Landroid/view/View;)Z
ifeq L3
aload 1
invokevirtual android/view/MotionEvent/getX()F
fstore 2
aload 1
invokevirtual android/view/MotionEvent/getY()F
fstore 3
fload 2
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/r F
fsub
fstore 4
fload 3
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/s F
fsub
fstore 5
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/u Landroid/support/v4/widget/eg;
getfield android/support/v4/widget/eg/n I
istore 6
fload 4
fload 4
fmul
fload 5
fload 5
fmul
fadd
iload 6
iload 6
imul
i2f
fcmpg
ifge L3
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/l Landroid/view/View;
fload 2
f2i
fload 3
f2i
invokestatic android/support/v4/widget/eg/b(Landroid/view/View;II)Z
ifeq L3
aload 0
invokespecial android/support/v4/widget/SlidingPaneLayout/e()Z
pop
goto L3
.limit locals 7
.limit stack 3
.end method

.method public requestChildFocus(Landroid/view/View;Landroid/view/View;)V
aload 0
aload 1
aload 2
invokespecial android/view/ViewGroup/requestChildFocus(Landroid/view/View;Landroid/view/View;)V
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/isInTouchMode()Z
ifne L0
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/k Z
ifne L0
aload 1
aload 0
getfield android/support/v4/widget/SlidingPaneLayout/l Landroid/view/View;
if_acmpne L1
iconst_1
istore 3
L2:
aload 0
iload 3
putfield android/support/v4/widget/SlidingPaneLayout/v Z
L0:
return
L1:
iconst_0
istore 3
goto L2
.limit locals 4
.limit stack 3
.end method

.method public setCoveredFadeColor(I)V
.annotation invisibleparam 1 Landroid/support/a/j;
.end annotation
aload 0
iload 1
putfield android/support/v4/widget/SlidingPaneLayout/g I
return
.limit locals 2
.limit stack 2
.end method

.method public setPanelSlideListener(Landroid/support/v4/widget/df;)V
aload 0
aload 1
putfield android/support/v4/widget/SlidingPaneLayout/t Landroid/support/v4/widget/df;
return
.limit locals 2
.limit stack 2
.end method

.method public setParallaxDistance(I)V
aload 0
iload 1
putfield android/support/v4/widget/SlidingPaneLayout/q I
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/requestLayout()V
return
.limit locals 2
.limit stack 2
.end method

.method public setShadowDrawable(Landroid/graphics/drawable/Drawable;)V
.annotation visible Ljava/lang/Deprecated;
.end annotation
aload 0
aload 1
invokevirtual android/support/v4/widget/SlidingPaneLayout/setShadowDrawableLeft(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 2
.limit stack 2
.end method

.method public setShadowDrawableLeft(Landroid/graphics/drawable/Drawable;)V
aload 0
aload 1
putfield android/support/v4/widget/SlidingPaneLayout/h Landroid/graphics/drawable/Drawable;
return
.limit locals 2
.limit stack 2
.end method

.method public setShadowDrawableRight(Landroid/graphics/drawable/Drawable;)V
aload 0
aload 1
putfield android/support/v4/widget/SlidingPaneLayout/i Landroid/graphics/drawable/Drawable;
return
.limit locals 2
.limit stack 2
.end method

.method public setShadowResource(I)V
.annotation visible Ljava/lang/Deprecated;
.end annotation
.annotation invisibleparam 1 Landroid/support/a/m;
.end annotation
aload 0
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getResources()Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v4/widget/SlidingPaneLayout/setShadowDrawable(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 2
.limit stack 3
.end method

.method public setShadowResourceLeft(I)V
aload 0
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getResources()Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v4/widget/SlidingPaneLayout/setShadowDrawableLeft(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 2
.limit stack 3
.end method

.method public setShadowResourceRight(I)V
aload 0
aload 0
invokevirtual android/support/v4/widget/SlidingPaneLayout/getResources()Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v4/widget/SlidingPaneLayout/setShadowDrawableRight(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 2
.limit stack 3
.end method

.method public setSliderFadeColor(I)V
.annotation invisibleparam 1 Landroid/support/a/j;
.end annotation
aload 0
iload 1
putfield android/support/v4/widget/SlidingPaneLayout/e I
return
.limit locals 2
.limit stack 2
.end method
