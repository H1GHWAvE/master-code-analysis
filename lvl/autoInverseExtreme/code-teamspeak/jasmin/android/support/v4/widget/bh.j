.bytecode 50.0
.class final synchronized android/support/v4/widget/bh
.super android/support/v4/view/a

.method <init>()V
aload 0
invokespecial android/support/v4/view/a/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/view/View;Landroid/support/v4/view/a/q;)V
aload 0
aload 1
aload 2
invokespecial android/support/v4/view/a/a(Landroid/view/View;Landroid/support/v4/view/a/q;)V
aload 1
checkcast android/support/v4/widget/NestedScrollView
astore 1
aload 2
ldc android/widget/ScrollView
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual android/support/v4/view/a/q/b(Ljava/lang/CharSequence;)V
aload 1
invokevirtual android/support/v4/widget/NestedScrollView/isEnabled()Z
ifeq L0
aload 1
invokestatic android/support/v4/widget/NestedScrollView/a(Landroid/support/v4/widget/NestedScrollView;)I
istore 3
iload 3
ifle L0
aload 2
iconst_1
invokevirtual android/support/v4/view/a/q/i(Z)V
aload 1
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
ifle L1
aload 2
sipush 8192
invokevirtual android/support/v4/view/a/q/a(I)V
L1:
aload 1
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
iload 3
if_icmpge L0
aload 2
sipush 4096
invokevirtual android/support/v4/view/a/q/a(I)V
L0:
return
.limit locals 4
.limit stack 3
.end method

.method public final a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
aload 0
aload 1
aload 2
invokespecial android/support/v4/view/a/a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
aload 1
checkcast android/support/v4/widget/NestedScrollView
astore 1
aload 2
ldc android/widget/ScrollView
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual android/view/accessibility/AccessibilityEvent/setClassName(Ljava/lang/CharSequence;)V
aload 2
invokestatic android/support/v4/view/a/a/a(Landroid/view/accessibility/AccessibilityEvent;)Landroid/support/v4/view/a/bd;
astore 2
aload 1
invokestatic android/support/v4/widget/NestedScrollView/a(Landroid/support/v4/widget/NestedScrollView;)I
ifle L0
iconst_1
istore 4
L1:
aload 2
iload 4
invokevirtual android/support/v4/view/a/bd/a(Z)V
aload 1
invokevirtual android/support/v4/widget/NestedScrollView/getScrollX()I
istore 3
getstatic android/support/v4/view/a/bd/a Landroid/support/v4/view/a/bg;
aload 2
getfield android/support/v4/view/a/bd/b Ljava/lang/Object;
iload 3
invokeinterface android/support/v4/view/a/bg/f(Ljava/lang/Object;I)V 2
aload 1
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
istore 3
getstatic android/support/v4/view/a/bd/a Landroid/support/v4/view/a/bg;
aload 2
getfield android/support/v4/view/a/bd/b Ljava/lang/Object;
iload 3
invokeinterface android/support/v4/view/a/bg/g(Ljava/lang/Object;I)V 2
aload 1
invokevirtual android/support/v4/widget/NestedScrollView/getScrollX()I
istore 3
getstatic android/support/v4/view/a/bd/a Landroid/support/v4/view/a/bg;
aload 2
getfield android/support/v4/view/a/bd/b Ljava/lang/Object;
iload 3
invokeinterface android/support/v4/view/a/bg/i(Ljava/lang/Object;I)V 2
aload 1
invokestatic android/support/v4/widget/NestedScrollView/a(Landroid/support/v4/widget/NestedScrollView;)I
istore 3
getstatic android/support/v4/view/a/bd/a Landroid/support/v4/view/a/bg;
aload 2
getfield android/support/v4/view/a/bd/b Ljava/lang/Object;
iload 3
invokeinterface android/support/v4/view/a/bg/j(Ljava/lang/Object;I)V 2
return
L0:
iconst_0
istore 4
goto L1
.limit locals 5
.limit stack 3
.end method

.method public final a(Landroid/view/View;ILandroid/os/Bundle;)Z
aload 0
aload 1
iload 2
aload 3
invokespecial android/support/v4/view/a/a(Landroid/view/View;ILandroid/os/Bundle;)Z
ifeq L0
iconst_1
ireturn
L0:
aload 1
checkcast android/support/v4/widget/NestedScrollView
astore 1
aload 1
invokevirtual android/support/v4/widget/NestedScrollView/isEnabled()Z
ifne L1
iconst_0
ireturn
L1:
iload 2
lookupswitch
4096 : L2
8192 : L3
default : L4
L4:
iconst_0
ireturn
L2:
aload 1
invokevirtual android/support/v4/widget/NestedScrollView/getHeight()I
aload 1
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingBottom()I
isub
aload 1
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingTop()I
isub
aload 1
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
iadd
aload 1
invokestatic android/support/v4/widget/NestedScrollView/a(Landroid/support/v4/widget/NestedScrollView;)I
invokestatic java/lang/Math/min(II)I
istore 2
iload 2
aload 1
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
if_icmpeq L5
aload 1
iload 2
invokevirtual android/support/v4/widget/NestedScrollView/a(I)V
iconst_1
ireturn
L5:
iconst_0
ireturn
L3:
aload 1
invokevirtual android/support/v4/widget/NestedScrollView/getHeight()I
istore 2
aload 1
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingBottom()I
istore 4
aload 1
invokevirtual android/support/v4/widget/NestedScrollView/getPaddingTop()I
istore 5
aload 1
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
iload 2
iload 4
isub
iload 5
isub
isub
iconst_0
invokestatic java/lang/Math/max(II)I
istore 2
iload 2
aload 1
invokevirtual android/support/v4/widget/NestedScrollView/getScrollY()I
if_icmpeq L6
aload 1
iload 2
invokevirtual android/support/v4/widget/NestedScrollView/a(I)V
iconst_1
ireturn
L6:
iconst_0
ireturn
.limit locals 6
.limit stack 4
.end method
