.bytecode 50.0
.class final synchronized android/support/v4/widget/dd
.super android/support/v4/widget/ej

.field final synthetic 'a' Landroid/support/v4/widget/SlidingPaneLayout;

.method private <init>(Landroid/support/v4/widget/SlidingPaneLayout;)V
aload 0
aload 1
putfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
aload 0
invokespecial android/support/v4/widget/ej/<init>()V
return
.limit locals 2
.limit stack 2
.end method

.method synthetic <init>(Landroid/support/v4/widget/SlidingPaneLayout;B)V
aload 0
aload 1
invokespecial android/support/v4/widget/dd/<init>(Landroid/support/v4/widget/SlidingPaneLayout;)V
return
.limit locals 3
.limit stack 2
.end method

.method public final a(Landroid/view/View;I)I
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/d(Landroid/support/v4/widget/SlidingPaneLayout;)Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/de
astore 1
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/e(Landroid/support/v4/widget/SlidingPaneLayout;)Z
ifeq L0
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
invokevirtual android/support/v4/widget/SlidingPaneLayout/getWidth()I
istore 3
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingRight()I
istore 4
iload 3
aload 1
getfield android/support/v4/widget/de/rightMargin I
iload 4
iadd
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/d(Landroid/support/v4/widget/SlidingPaneLayout;)Landroid/view/View;
invokevirtual android/view/View/getWidth()I
iadd
isub
istore 3
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/f(Landroid/support/v4/widget/SlidingPaneLayout;)I
istore 4
iload 2
iload 3
invokestatic java/lang/Math/min(II)I
iload 3
iload 4
isub
invokestatic java/lang/Math/max(II)I
ireturn
L0:
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingLeft()I
istore 3
aload 1
getfield android/support/v4/widget/de/leftMargin I
iload 3
iadd
istore 3
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/f(Landroid/support/v4/widget/SlidingPaneLayout;)I
istore 4
iload 2
iload 3
invokestatic java/lang/Math/max(II)I
iload 4
iload 3
iadd
invokestatic java/lang/Math/min(II)I
ireturn
.limit locals 5
.limit stack 3
.end method

.method public final a(I)V
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/b(Landroid/support/v4/widget/SlidingPaneLayout;)Landroid/support/v4/widget/eg;
getfield android/support/v4/widget/eg/m I
ifne L0
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/c(Landroid/support/v4/widget/SlidingPaneLayout;)F
fconst_0
fcmpl
ifne L1
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/d(Landroid/support/v4/widget/SlidingPaneLayout;)Landroid/view/View;
invokevirtual android/support/v4/widget/SlidingPaneLayout/a(Landroid/view/View;)V
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
astore 2
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/d(Landroid/support/v4/widget/SlidingPaneLayout;)Landroid/view/View;
pop
aload 2
bipush 32
invokevirtual android/support/v4/widget/SlidingPaneLayout/sendAccessibilityEvent(I)V
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
iconst_0
invokestatic android/support/v4/widget/SlidingPaneLayout/a(Landroid/support/v4/widget/SlidingPaneLayout;Z)Z
pop
L0:
return
L1:
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
astore 2
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/d(Landroid/support/v4/widget/SlidingPaneLayout;)Landroid/view/View;
pop
aload 2
bipush 32
invokevirtual android/support/v4/widget/SlidingPaneLayout/sendAccessibilityEvent(I)V
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
iconst_1
invokestatic android/support/v4/widget/SlidingPaneLayout/a(Landroid/support/v4/widget/SlidingPaneLayout;Z)Z
pop
return
.limit locals 3
.limit stack 2
.end method

.method public final a(II)V
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/b(Landroid/support/v4/widget/SlidingPaneLayout;)Landroid/support/v4/widget/eg;
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/d(Landroid/support/v4/widget/SlidingPaneLayout;)Landroid/view/View;
iload 2
invokevirtual android/support/v4/widget/eg/a(Landroid/view/View;I)V
return
.limit locals 3
.limit stack 3
.end method

.method public final a(Landroid/view/View;F)V
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/de
astore 5
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/e(Landroid/support/v4/widget/SlidingPaneLayout;)Z
ifeq L0
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingRight()I
istore 3
aload 5
getfield android/support/v4/widget/de/rightMargin I
iload 3
iadd
istore 4
fload 2
fconst_0
fcmpg
iflt L1
iload 4
istore 3
fload 2
fconst_0
fcmpl
ifne L2
iload 4
istore 3
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/c(Landroid/support/v4/widget/SlidingPaneLayout;)F
ldc_w 0.5F
fcmpl
ifle L2
L1:
iload 4
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/f(Landroid/support/v4/widget/SlidingPaneLayout;)I
iadd
istore 3
L2:
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/d(Landroid/support/v4/widget/SlidingPaneLayout;)Landroid/view/View;
invokevirtual android/view/View/getWidth()I
istore 4
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
invokevirtual android/support/v4/widget/SlidingPaneLayout/getWidth()I
iload 3
isub
iload 4
isub
istore 3
L3:
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/b(Landroid/support/v4/widget/SlidingPaneLayout;)Landroid/support/v4/widget/eg;
iload 3
aload 1
invokevirtual android/view/View/getTop()I
invokevirtual android/support/v4/widget/eg/a(II)Z
pop
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
invokevirtual android/support/v4/widget/SlidingPaneLayout/invalidate()V
return
L0:
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
invokevirtual android/support/v4/widget/SlidingPaneLayout/getPaddingLeft()I
istore 3
aload 5
getfield android/support/v4/widget/de/leftMargin I
iload 3
iadd
istore 4
fload 2
fconst_0
fcmpl
ifgt L4
iload 4
istore 3
fload 2
fconst_0
fcmpl
ifne L3
iload 4
istore 3
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/c(Landroid/support/v4/widget/SlidingPaneLayout;)F
ldc_w 0.5F
fcmpl
ifle L3
L4:
iload 4
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/f(Landroid/support/v4/widget/SlidingPaneLayout;)I
iadd
istore 3
goto L3
.limit locals 6
.limit stack 3
.end method

.method public final a(Landroid/view/View;)Z
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/a(Landroid/support/v4/widget/SlidingPaneLayout;)Z
ifeq L0
iconst_0
ireturn
L0:
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v4/widget/de
getfield android/support/v4/widget/de/b Z
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final b(Landroid/view/View;)I
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
invokestatic android/support/v4/widget/SlidingPaneLayout/f(Landroid/support/v4/widget/SlidingPaneLayout;)I
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final b(Landroid/view/View;I)V
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
iload 2
invokestatic android/support/v4/widget/SlidingPaneLayout/a(Landroid/support/v4/widget/SlidingPaneLayout;I)V
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
invokevirtual android/support/v4/widget/SlidingPaneLayout/invalidate()V
return
.limit locals 3
.limit stack 2
.end method

.method public final c(Landroid/view/View;)I
aload 1
invokevirtual android/view/View/getTop()I
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final d(Landroid/view/View;)V
aload 0
getfield android/support/v4/widget/dd/a Landroid/support/v4/widget/SlidingPaneLayout;
invokevirtual android/support/v4/widget/SlidingPaneLayout/a()V
return
.limit locals 2
.limit stack 1
.end method
