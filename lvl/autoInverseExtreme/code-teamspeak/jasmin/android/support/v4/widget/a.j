.bytecode 50.0
.class public synchronized abstract android/support/v4/widget/a
.super java/lang/Object
.implements android/view/View$OnTouchListener

.field private static final 'A' I = 315


.field private static final 'B' I = 1575


.field private static final 'C' F = 3.4028235E38F


.field private static final 'D' F = 0.2F


.field private static final 'E' F = 1.0F


.field private static final 'F' I

.field private static final 'G' I = 500


.field private static final 'H' I = 500


.field public static final 'a' F = 0.0F


.field public static final 'b' F = 3.4028235E38F


.field public static final 'c' F = 0.0F


.field public static final 'd' I = 0


.field public static final 'e' I = 1


.field public static final 'f' I = 2


.field private static final 'g' I = 0


.field private static final 'h' I = 1


.field private static final 'z' I = 1


.field private final 'i' Landroid/support/v4/widget/c;

.field private final 'j' Landroid/view/animation/Interpolator;

.field private final 'k' Landroid/view/View;

.field private 'l' Ljava/lang/Runnable;

.field private 'm' [F

.field private 'n' [F

.field private 'o' I

.field private 'p' I

.field private 'q' [F

.field private 'r' [F

.field private 's' [F

.field private 't' Z

.field private 'u' Z

.field private 'v' Z

.field private 'w' Z

.field private 'x' Z

.field private 'y' Z

.method static <clinit>()V
invokestatic android/view/ViewConfiguration/getTapTimeout()I
putstatic android/support/v4/widget/a/F I
return
.limit locals 0
.limit stack 1
.end method

.method public <init>(Landroid/view/View;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new android/support/v4/widget/c
dup
invokespecial android/support/v4/widget/c/<init>()V
putfield android/support/v4/widget/a/i Landroid/support/v4/widget/c;
aload 0
new android/view/animation/AccelerateInterpolator
dup
invokespecial android/view/animation/AccelerateInterpolator/<init>()V
putfield android/support/v4/widget/a/j Landroid/view/animation/Interpolator;
aload 0
iconst_2
newarray float
dup
iconst_0
fconst_0
fastore
dup
iconst_1
fconst_0
fastore
putfield android/support/v4/widget/a/m [F
aload 0
iconst_2
newarray float
dup
iconst_0
ldc_w 3.4028235E38F
fastore
dup
iconst_1
ldc_w 3.4028235E38F
fastore
putfield android/support/v4/widget/a/n [F
aload 0
iconst_2
newarray float
dup
iconst_0
fconst_0
fastore
dup
iconst_1
fconst_0
fastore
putfield android/support/v4/widget/a/q [F
aload 0
iconst_2
newarray float
dup
iconst_0
fconst_0
fastore
dup
iconst_1
fconst_0
fastore
putfield android/support/v4/widget/a/r [F
aload 0
iconst_2
newarray float
dup
iconst_0
ldc_w 3.4028235E38F
fastore
dup
iconst_1
ldc_w 3.4028235E38F
fastore
putfield android/support/v4/widget/a/s [F
aload 0
aload 1
putfield android/support/v4/widget/a/k Landroid/view/View;
invokestatic android/content/res/Resources/getSystem()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
astore 1
ldc_w 1575.0F
aload 1
getfield android/util/DisplayMetrics/density F
fmul
ldc_w 0.5F
fadd
f2i
istore 3
aload 1
getfield android/util/DisplayMetrics/density F
ldc_w 315.0F
fmul
ldc_w 0.5F
fadd
f2i
istore 4
iload 3
i2f
fstore 2
aload 0
getfield android/support/v4/widget/a/s [F
iconst_0
fload 2
ldc_w 1000.0F
fdiv
fastore
aload 0
getfield android/support/v4/widget/a/s [F
iconst_1
fload 2
ldc_w 1000.0F
fdiv
fastore
iload 4
i2f
fstore 2
aload 0
getfield android/support/v4/widget/a/r [F
iconst_0
fload 2
ldc_w 1000.0F
fdiv
fastore
aload 0
getfield android/support/v4/widget/a/r [F
iconst_1
fload 2
ldc_w 1000.0F
fdiv
fastore
aload 0
iconst_1
putfield android/support/v4/widget/a/o I
aload 0
getfield android/support/v4/widget/a/n [F
iconst_0
ldc_w 3.4028235E38F
fastore
aload 0
getfield android/support/v4/widget/a/n [F
iconst_1
ldc_w 3.4028235E38F
fastore
aload 0
getfield android/support/v4/widget/a/m [F
iconst_0
ldc_w 0.2F
fastore
aload 0
getfield android/support/v4/widget/a/m [F
iconst_1
ldc_w 0.2F
fastore
aload 0
getfield android/support/v4/widget/a/q [F
iconst_0
ldc_w 0.001F
fastore
aload 0
getfield android/support/v4/widget/a/q [F
iconst_1
ldc_w 0.001F
fastore
aload 0
getstatic android/support/v4/widget/a/F I
putfield android/support/v4/widget/a/p I
aload 0
getfield android/support/v4/widget/a/i Landroid/support/v4/widget/c;
sipush 500
putfield android/support/v4/widget/c/a I
aload 0
getfield android/support/v4/widget/a/i Landroid/support/v4/widget/c;
sipush 500
putfield android/support/v4/widget/c/b I
return
.limit locals 5
.limit stack 5
.end method

.method static synthetic a(F)F
fload 0
fconst_0
fconst_1
invokestatic android/support/v4/widget/a/a(FFF)F
freturn
.limit locals 1
.limit stack 3
.end method

.method private static a(FFF)F
fload 0
fload 2
fcmpl
ifle L0
fload 2
freturn
L0:
fload 0
fload 1
fcmpg
ifge L1
fload 1
freturn
L1:
fload 0
freturn
.limit locals 3
.limit stack 2
.end method

.method private a(FFFF)F
fconst_0
fstore 5
fload 1
fload 2
fmul
fconst_0
fload 3
invokestatic android/support/v4/widget/a/a(FFF)F
fstore 1
aload 0
fload 4
fload 1
invokespecial android/support/v4/widget/a/c(FF)F
fstore 3
aload 0
fload 2
fload 4
fsub
fload 1
invokespecial android/support/v4/widget/a/c(FF)F
fload 3
fsub
fstore 2
fload 2
fconst_0
fcmpg
ifge L0
aload 0
getfield android/support/v4/widget/a/j Landroid/view/animation/Interpolator;
fload 2
fneg
invokeinterface android/view/animation/Interpolator/getInterpolation(F)F 1
fneg
fstore 1
L1:
fload 1
ldc_w -1.0F
fconst_1
invokestatic android/support/v4/widget/a/a(FFF)F
fstore 1
L2:
fload 1
freturn
L0:
fload 5
fstore 1
fload 2
fconst_0
fcmpl
ifle L2
aload 0
getfield android/support/v4/widget/a/j Landroid/view/animation/Interpolator;
fload 2
invokeinterface android/view/animation/Interpolator/getInterpolation(F)F 1
fstore 1
goto L1
.limit locals 6
.limit stack 3
.end method

.method private a(IFFF)F
aload 0
getfield android/support/v4/widget/a/m [F
iload 1
faload
fload 3
fmul
fconst_0
aload 0
getfield android/support/v4/widget/a/n [F
iload 1
faload
invokestatic android/support/v4/widget/a/a(FFF)F
fstore 5
aload 0
fload 2
fload 5
invokespecial android/support/v4/widget/a/c(FF)F
fstore 6
aload 0
fload 3
fload 2
fsub
fload 5
invokespecial android/support/v4/widget/a/c(FF)F
fload 6
fsub
fstore 2
fload 2
fconst_0
fcmpg
ifge L0
aload 0
getfield android/support/v4/widget/a/j Landroid/view/animation/Interpolator;
fload 2
fneg
invokeinterface android/view/animation/Interpolator/getInterpolation(F)F 1
fneg
fstore 2
L1:
fload 2
ldc_w -1.0F
fconst_1
invokestatic android/support/v4/widget/a/a(FFF)F
fstore 2
L2:
fload 2
fconst_0
fcmpl
ifne L3
fconst_0
freturn
L0:
fload 2
fconst_0
fcmpl
ifle L4
aload 0
getfield android/support/v4/widget/a/j Landroid/view/animation/Interpolator;
fload 2
invokeinterface android/view/animation/Interpolator/getInterpolation(F)F 1
fstore 2
goto L1
L4:
fconst_0
fstore 2
goto L2
L3:
aload 0
getfield android/support/v4/widget/a/q [F
iload 1
faload
fstore 6
aload 0
getfield android/support/v4/widget/a/r [F
iload 1
faload
fstore 3
aload 0
getfield android/support/v4/widget/a/s [F
iload 1
faload
fstore 5
fload 6
fload 4
fmul
fstore 4
fload 2
fconst_0
fcmpl
ifle L5
fload 2
fload 4
fmul
fload 3
fload 5
invokestatic android/support/v4/widget/a/a(FFF)F
freturn
L5:
fload 2
fneg
fload 4
fmul
fload 3
fload 5
invokestatic android/support/v4/widget/a/a(FFF)F
fneg
freturn
.limit locals 7
.limit stack 4
.end method

.method static synthetic a(II)I
iload 0
iload 1
if_icmple L0
iload 1
ireturn
L0:
iload 0
ifge L1
iconst_0
ireturn
L1:
iload 0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static a(III)I
iload 0
iload 2
if_icmple L0
iload 2
ireturn
L0:
iload 0
iload 1
if_icmpge L1
iload 1
ireturn
L1:
iload 0
ireturn
.limit locals 3
.limit stack 2
.end method

.method private a(FF)Landroid/support/v4/widget/a;
aload 0
getfield android/support/v4/widget/a/s [F
iconst_0
fload 1
ldc_w 1000.0F
fdiv
fastore
aload 0
getfield android/support/v4/widget/a/s [F
iconst_1
fload 2
ldc_w 1000.0F
fdiv
fastore
aload 0
areturn
.limit locals 3
.limit stack 4
.end method

.method static synthetic a(Landroid/support/v4/widget/a;)Z
aload 0
getfield android/support/v4/widget/a/w Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private b(FF)Landroid/support/v4/widget/a;
aload 0
getfield android/support/v4/widget/a/r [F
iconst_0
fload 1
ldc_w 1000.0F
fdiv
fastore
aload 0
getfield android/support/v4/widget/a/r [F
iconst_1
fload 2
ldc_w 1000.0F
fdiv
fastore
aload 0
areturn
.limit locals 3
.limit stack 4
.end method

.method private b(Z)Landroid/support/v4/widget/a;
aload 0
iload 1
putfield android/support/v4/widget/a/y Z
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private b()Z
aload 0
getfield android/support/v4/widget/a/x Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic b(Landroid/support/v4/widget/a;)Z
aload 0
getfield android/support/v4/widget/a/u Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private c(FF)F
fload 2
fconst_0
fcmpl
ifne L0
L1:
fconst_0
freturn
L0:
aload 0
getfield android/support/v4/widget/a/o I
tableswitch 0
L2
L2
L3
default : L4
L4:
fconst_0
freturn
L2:
fload 1
fload 2
fcmpg
ifge L1
fload 1
fconst_0
fcmpl
iflt L5
fconst_1
fload 1
fload 2
fdiv
fsub
freturn
L5:
aload 0
getfield android/support/v4/widget/a/w Z
ifeq L1
aload 0
getfield android/support/v4/widget/a/o I
iconst_1
if_icmpne L1
fconst_1
freturn
L3:
fload 1
fconst_0
fcmpg
ifge L1
fload 1
fload 2
fneg
fdiv
freturn
.limit locals 3
.limit stack 3
.end method

.method private c(I)Landroid/support/v4/widget/a;
aload 0
iload 1
putfield android/support/v4/widget/a/p I
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private c()Z
aload 0
getfield android/support/v4/widget/a/y Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic c(Landroid/support/v4/widget/a;)Z
aload 0
iconst_0
putfield android/support/v4/widget/a/u Z
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private d()Landroid/support/v4/widget/a;
aload 0
getfield android/support/v4/widget/a/q [F
iconst_0
ldc_w 0.001F
fastore
aload 0
getfield android/support/v4/widget/a/q [F
iconst_1
ldc_w 0.001F
fastore
aload 0
areturn
.limit locals 1
.limit stack 3
.end method

.method static synthetic d(Landroid/support/v4/widget/a;)Landroid/support/v4/widget/c;
aload 0
getfield android/support/v4/widget/a/i Landroid/support/v4/widget/c;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e()Landroid/support/v4/widget/a;
aload 0
iconst_1
putfield android/support/v4/widget/a/o I
aload 0
areturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic e(Landroid/support/v4/widget/a;)Z
aload 0
invokespecial android/support/v4/widget/a/j()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private f()Landroid/support/v4/widget/a;
aload 0
getfield android/support/v4/widget/a/m [F
iconst_0
ldc_w 0.2F
fastore
aload 0
getfield android/support/v4/widget/a/m [F
iconst_1
ldc_w 0.2F
fastore
aload 0
areturn
.limit locals 1
.limit stack 3
.end method

.method static synthetic f(Landroid/support/v4/widget/a;)Z
aload 0
iconst_0
putfield android/support/v4/widget/a/w Z
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private g()Landroid/support/v4/widget/a;
aload 0
getfield android/support/v4/widget/a/n [F
iconst_0
ldc_w 3.4028235E38F
fastore
aload 0
getfield android/support/v4/widget/a/n [F
iconst_1
ldc_w 3.4028235E38F
fastore
aload 0
areturn
.limit locals 1
.limit stack 3
.end method

.method static synthetic g(Landroid/support/v4/widget/a;)Z
aload 0
getfield android/support/v4/widget/a/v Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private h()Landroid/support/v4/widget/a;
aload 0
getfield android/support/v4/widget/a/i Landroid/support/v4/widget/c;
sipush 500
putfield android/support/v4/widget/c/a I
aload 0
areturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic h(Landroid/support/v4/widget/a;)Z
aload 0
iconst_0
putfield android/support/v4/widget/a/v Z
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private i()Landroid/support/v4/widget/a;
aload 0
getfield android/support/v4/widget/a/i Landroid/support/v4/widget/c;
sipush 500
putfield android/support/v4/widget/c/b I
aload 0
areturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic i(Landroid/support/v4/widget/a;)V
invokestatic android/os/SystemClock/uptimeMillis()J
lstore 1
lload 1
lload 1
iconst_3
fconst_0
fconst_0
iconst_0
invokestatic android/view/MotionEvent/obtain(JJIFFI)Landroid/view/MotionEvent;
astore 3
aload 0
getfield android/support/v4/widget/a/k Landroid/view/View;
aload 3
invokevirtual android/view/View/onTouchEvent(Landroid/view/MotionEvent;)Z
pop
aload 3
invokevirtual android/view/MotionEvent/recycle()V
return
.limit locals 4
.limit stack 8
.end method

.method static synthetic j(Landroid/support/v4/widget/a;)Landroid/view/View;
aload 0
getfield android/support/v4/widget/a/k Landroid/view/View;
areturn
.limit locals 1
.limit stack 1
.end method

.method private j()Z
aload 0
getfield android/support/v4/widget/a/i Landroid/support/v4/widget/c;
astore 3
aload 3
getfield android/support/v4/widget/c/d F
aload 3
getfield android/support/v4/widget/c/d F
invokestatic java/lang/Math/abs(F)F
fdiv
f2i
istore 1
aload 3
getfield android/support/v4/widget/c/c F
aload 3
getfield android/support/v4/widget/c/c F
invokestatic java/lang/Math/abs(F)F
fdiv
f2i
istore 2
iload 1
ifeq L0
aload 0
iload 1
invokevirtual android/support/v4/widget/a/b(I)Z
ifne L1
L0:
iload 2
ifeq L2
L2:
iconst_0
ireturn
L1:
iconst_1
ireturn
.limit locals 4
.limit stack 2
.end method

.method private k()V
aload 0
getfield android/support/v4/widget/a/l Ljava/lang/Runnable;
ifnonnull L0
aload 0
new android/support/v4/widget/d
dup
aload 0
iconst_0
invokespecial android/support/v4/widget/d/<init>(Landroid/support/v4/widget/a;B)V
putfield android/support/v4/widget/a/l Ljava/lang/Runnable;
L0:
aload 0
iconst_1
putfield android/support/v4/widget/a/w Z
aload 0
iconst_1
putfield android/support/v4/widget/a/u Z
aload 0
getfield android/support/v4/widget/a/t Z
ifne L1
aload 0
getfield android/support/v4/widget/a/p I
ifle L1
aload 0
getfield android/support/v4/widget/a/k Landroid/view/View;
aload 0
getfield android/support/v4/widget/a/l Ljava/lang/Runnable;
aload 0
getfield android/support/v4/widget/a/p I
i2l
invokestatic android/support/v4/view/cx/a(Landroid/view/View;Ljava/lang/Runnable;J)V
L2:
aload 0
iconst_1
putfield android/support/v4/widget/a/t Z
return
L1:
aload 0
getfield android/support/v4/widget/a/l Ljava/lang/Runnable;
invokeinterface java/lang/Runnable/run()V 0
goto L2
.limit locals 1
.limit stack 5
.end method

.method private l()V
aload 0
getfield android/support/v4/widget/a/u Z
ifeq L0
aload 0
iconst_0
putfield android/support/v4/widget/a/w Z
return
L0:
aload 0
getfield android/support/v4/widget/a/i Landroid/support/v4/widget/c;
astore 5
invokestatic android/view/animation/AnimationUtils/currentAnimationTimeMillis()J
lstore 3
lload 3
aload 5
getfield android/support/v4/widget/c/e J
lsub
l2i
istore 1
aload 5
getfield android/support/v4/widget/c/b I
istore 2
iload 1
iload 2
if_icmple L1
iload 2
istore 1
L2:
aload 5
iload 1
putfield android/support/v4/widget/c/k I
aload 5
aload 5
lload 3
invokevirtual android/support/v4/widget/c/a(J)F
putfield android/support/v4/widget/c/j F
aload 5
lload 3
putfield android/support/v4/widget/c/i J
return
L1:
iload 1
ifge L3
iconst_0
istore 1
goto L2
L3:
goto L2
.limit locals 6
.limit stack 4
.end method

.method private m()V
invokestatic android/os/SystemClock/uptimeMillis()J
lstore 1
lload 1
lload 1
iconst_3
fconst_0
fconst_0
iconst_0
invokestatic android/view/MotionEvent/obtain(JJIFFI)Landroid/view/MotionEvent;
astore 3
aload 0
getfield android/support/v4/widget/a/k Landroid/view/View;
aload 3
invokevirtual android/view/View/onTouchEvent(Landroid/view/MotionEvent;)Z
pop
aload 3
invokevirtual android/view/MotionEvent/recycle()V
return
.limit locals 4
.limit stack 8
.end method

.method public final a(Z)Landroid/support/v4/widget/a;
aload 0
getfield android/support/v4/widget/a/x Z
ifeq L0
iload 1
ifne L0
aload 0
invokespecial android/support/v4/widget/a/l()V
L0:
aload 0
iload 1
putfield android/support/v4/widget/a/x Z
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public abstract a(I)V
.end method

.method public abstract a()Z
.end method

.method public abstract b(I)Z
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
aload 0
getfield android/support/v4/widget/a/x Z
ifne L0
L1:
iconst_0
ireturn
L0:
aload 2
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;)I
tableswitch 0
L2
L3
L4
L3
default : L5
L5:
aload 0
getfield android/support/v4/widget/a/y Z
ifeq L1
aload 0
getfield android/support/v4/widget/a/w Z
ifeq L1
iconst_1
ireturn
L2:
aload 0
iconst_1
putfield android/support/v4/widget/a/v Z
aload 0
iconst_0
putfield android/support/v4/widget/a/t Z
L4:
aload 0
iconst_0
aload 2
invokevirtual android/view/MotionEvent/getX()F
aload 1
invokevirtual android/view/View/getWidth()I
i2f
aload 0
getfield android/support/v4/widget/a/k Landroid/view/View;
invokevirtual android/view/View/getWidth()I
i2f
invokespecial android/support/v4/widget/a/a(IFFF)F
fstore 3
aload 0
iconst_1
aload 2
invokevirtual android/view/MotionEvent/getY()F
aload 1
invokevirtual android/view/View/getHeight()I
i2f
aload 0
getfield android/support/v4/widget/a/k Landroid/view/View;
invokevirtual android/view/View/getHeight()I
i2f
invokespecial android/support/v4/widget/a/a(IFFF)F
fstore 4
aload 0
getfield android/support/v4/widget/a/i Landroid/support/v4/widget/c;
astore 1
aload 1
fload 3
putfield android/support/v4/widget/c/c F
aload 1
fload 4
putfield android/support/v4/widget/c/d F
aload 0
getfield android/support/v4/widget/a/w Z
ifne L5
aload 0
invokespecial android/support/v4/widget/a/j()Z
ifeq L5
aload 0
getfield android/support/v4/widget/a/l Ljava/lang/Runnable;
ifnonnull L6
aload 0
new android/support/v4/widget/d
dup
aload 0
iconst_0
invokespecial android/support/v4/widget/d/<init>(Landroid/support/v4/widget/a;B)V
putfield android/support/v4/widget/a/l Ljava/lang/Runnable;
L6:
aload 0
iconst_1
putfield android/support/v4/widget/a/w Z
aload 0
iconst_1
putfield android/support/v4/widget/a/u Z
aload 0
getfield android/support/v4/widget/a/t Z
ifne L7
aload 0
getfield android/support/v4/widget/a/p I
ifle L7
aload 0
getfield android/support/v4/widget/a/k Landroid/view/View;
aload 0
getfield android/support/v4/widget/a/l Ljava/lang/Runnable;
aload 0
getfield android/support/v4/widget/a/p I
i2l
invokestatic android/support/v4/view/cx/a(Landroid/view/View;Ljava/lang/Runnable;J)V
L8:
aload 0
iconst_1
putfield android/support/v4/widget/a/t Z
goto L5
L7:
aload 0
getfield android/support/v4/widget/a/l Ljava/lang/Runnable;
invokeinterface java/lang/Runnable/run()V 0
goto L8
L3:
aload 0
invokespecial android/support/v4/widget/a/l()V
goto L5
.limit locals 5
.limit stack 5
.end method
