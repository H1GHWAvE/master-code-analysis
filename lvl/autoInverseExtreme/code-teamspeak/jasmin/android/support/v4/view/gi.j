.bytecode 50.0
.class final synchronized android/support/v4/view/gi
.super android/support/v4/view/gh

.field final 'a' Landroid/view/WindowInsets;

.method <init>(Landroid/view/WindowInsets;)V
aload 0
invokespecial android/support/v4/view/gh/<init>()V
aload 0
aload 1
putfield android/support/v4/view/gi/a Landroid/view/WindowInsets;
return
.limit locals 2
.limit stack 2
.end method

.method private p()Landroid/view/WindowInsets;
aload 0
getfield android/support/v4/view/gi/a Landroid/view/WindowInsets;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a()I
aload 0
getfield android/support/v4/view/gi/a Landroid/view/WindowInsets;
invokevirtual android/view/WindowInsets/getSystemWindowInsetLeft()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a(IIII)Landroid/support/v4/view/gh;
new android/support/v4/view/gi
dup
aload 0
getfield android/support/v4/view/gi/a Landroid/view/WindowInsets;
iload 1
iload 2
iload 3
iload 4
invokevirtual android/view/WindowInsets/replaceSystemWindowInsets(IIII)Landroid/view/WindowInsets;
invokespecial android/support/v4/view/gi/<init>(Landroid/view/WindowInsets;)V
areturn
.limit locals 5
.limit stack 7
.end method

.method public final a(Landroid/graphics/Rect;)Landroid/support/v4/view/gh;
new android/support/v4/view/gi
dup
aload 0
getfield android/support/v4/view/gi/a Landroid/view/WindowInsets;
aload 1
invokevirtual android/view/WindowInsets/replaceSystemWindowInsets(Landroid/graphics/Rect;)Landroid/view/WindowInsets;
invokespecial android/support/v4/view/gi/<init>(Landroid/view/WindowInsets;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public final b()I
aload 0
getfield android/support/v4/view/gi/a Landroid/view/WindowInsets;
invokevirtual android/view/WindowInsets/getSystemWindowInsetTop()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final c()I
aload 0
getfield android/support/v4/view/gi/a Landroid/view/WindowInsets;
invokevirtual android/view/WindowInsets/getSystemWindowInsetRight()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final d()I
aload 0
getfield android/support/v4/view/gi/a Landroid/view/WindowInsets;
invokevirtual android/view/WindowInsets/getSystemWindowInsetBottom()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final e()Z
aload 0
getfield android/support/v4/view/gi/a Landroid/view/WindowInsets;
invokevirtual android/view/WindowInsets/hasSystemWindowInsets()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final f()Z
aload 0
getfield android/support/v4/view/gi/a Landroid/view/WindowInsets;
invokevirtual android/view/WindowInsets/hasInsets()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final g()Z
aload 0
getfield android/support/v4/view/gi/a Landroid/view/WindowInsets;
invokevirtual android/view/WindowInsets/isConsumed()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final h()Z
aload 0
getfield android/support/v4/view/gi/a Landroid/view/WindowInsets;
invokevirtual android/view/WindowInsets/isRound()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final i()Landroid/support/v4/view/gh;
new android/support/v4/view/gi
dup
aload 0
getfield android/support/v4/view/gi/a Landroid/view/WindowInsets;
invokevirtual android/view/WindowInsets/consumeSystemWindowInsets()Landroid/view/WindowInsets;
invokespecial android/support/v4/view/gi/<init>(Landroid/view/WindowInsets;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public final j()I
aload 0
getfield android/support/v4/view/gi/a Landroid/view/WindowInsets;
invokevirtual android/view/WindowInsets/getStableInsetTop()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final k()I
aload 0
getfield android/support/v4/view/gi/a Landroid/view/WindowInsets;
invokevirtual android/view/WindowInsets/getStableInsetLeft()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final l()I
aload 0
getfield android/support/v4/view/gi/a Landroid/view/WindowInsets;
invokevirtual android/view/WindowInsets/getStableInsetRight()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final m()I
aload 0
getfield android/support/v4/view/gi/a Landroid/view/WindowInsets;
invokevirtual android/view/WindowInsets/getStableInsetBottom()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final n()Z
aload 0
getfield android/support/v4/view/gi/a Landroid/view/WindowInsets;
invokevirtual android/view/WindowInsets/hasStableInsets()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final o()Landroid/support/v4/view/gh;
new android/support/v4/view/gi
dup
aload 0
getfield android/support/v4/view/gi/a Landroid/view/WindowInsets;
invokevirtual android/view/WindowInsets/consumeStableInsets()Landroid/view/WindowInsets;
invokespecial android/support/v4/view/gi/<init>(Landroid/view/WindowInsets;)V
areturn
.limit locals 1
.limit stack 3
.end method
