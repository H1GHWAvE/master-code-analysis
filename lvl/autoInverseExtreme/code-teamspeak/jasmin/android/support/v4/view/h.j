.bytecode 50.0
.class final synchronized android/support/v4/view/h
.super java/lang/Object

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a()Ljava/lang/Object;
new android/view/View$AccessibilityDelegate
dup
invokespecial android/view/View$AccessibilityDelegate/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method private static a(Landroid/support/v4/view/j;)Ljava/lang/Object;
new android/support/v4/view/i
dup
aload 0
invokespecial android/support/v4/view/i/<init>(Landroid/support/v4/view/j;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Ljava/lang/Object;Landroid/view/View;I)V
aload 0
checkcast android/view/View$AccessibilityDelegate
aload 1
iload 2
invokevirtual android/view/View$AccessibilityDelegate/sendAccessibilityEvent(Landroid/view/View;I)V
return
.limit locals 3
.limit stack 3
.end method

.method private static a(Ljava/lang/Object;Landroid/view/View;Ljava/lang/Object;)V
aload 0
checkcast android/view/View$AccessibilityDelegate
aload 1
aload 2
checkcast android/view/accessibility/AccessibilityNodeInfo
invokevirtual android/view/View$AccessibilityDelegate/onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V
return
.limit locals 3
.limit stack 3
.end method

.method private static a(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
aload 0
checkcast android/view/View$AccessibilityDelegate
aload 1
aload 2
invokevirtual android/view/View$AccessibilityDelegate/dispatchPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method private static a(Ljava/lang/Object;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
aload 0
checkcast android/view/View$AccessibilityDelegate
aload 1
aload 2
aload 3
invokevirtual android/view/View$AccessibilityDelegate/onRequestSendAccessibilityEvent(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
ireturn
.limit locals 4
.limit stack 4
.end method

.method private static b(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
aload 0
checkcast android/view/View$AccessibilityDelegate
aload 1
aload 2
invokevirtual android/view/View$AccessibilityDelegate/onInitializeAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
return
.limit locals 3
.limit stack 3
.end method

.method private static c(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
aload 0
checkcast android/view/View$AccessibilityDelegate
aload 1
aload 2
invokevirtual android/view/View$AccessibilityDelegate/onPopulateAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
return
.limit locals 3
.limit stack 3
.end method

.method private static d(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
aload 0
checkcast android/view/View$AccessibilityDelegate
aload 1
aload 2
invokevirtual android/view/View$AccessibilityDelegate/sendAccessibilityEventUnchecked(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
return
.limit locals 3
.limit stack 3
.end method
