.bytecode 50.0
.class final synchronized android/support/v4/view/ff
.super android/support/v4/view/fe

.method <init>()V
aload 0
invokespecial android/support/v4/view/fe/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/view/ViewParent;Landroid/view/View;)V
.catch java/lang/AbstractMethodError from L0 to L1 using L2
L0:
aload 1
aload 2
invokeinterface android/view/ViewParent/onStopNestedScroll(Landroid/view/View;)V 1
L1:
return
L2:
astore 2
ldc "ViewParentCompat"
new java/lang/StringBuilder
dup
ldc "ViewParent "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " does not implement interface method onStopNestedScroll"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 2
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
.limit locals 3
.limit stack 4
.end method

.method public final a(Landroid/view/ViewParent;Landroid/view/View;IIII)V
.catch java/lang/AbstractMethodError from L0 to L1 using L2
L0:
aload 1
aload 2
iload 3
iload 4
iload 5
iload 6
invokeinterface android/view/ViewParent/onNestedScroll(Landroid/view/View;IIII)V 5
L1:
return
L2:
astore 2
ldc "ViewParentCompat"
new java/lang/StringBuilder
dup
ldc "ViewParent "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " does not implement interface method onNestedScroll"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 2
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
.limit locals 7
.limit stack 6
.end method

.method public final a(Landroid/view/ViewParent;Landroid/view/View;II[I)V
.catch java/lang/AbstractMethodError from L0 to L1 using L2
L0:
aload 1
aload 2
iload 3
iload 4
aload 5
invokeinterface android/view/ViewParent/onNestedPreScroll(Landroid/view/View;II[I)V 4
L1:
return
L2:
astore 2
ldc "ViewParentCompat"
new java/lang/StringBuilder
dup
ldc "ViewParent "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " does not implement interface method onNestedPreScroll"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 2
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
.limit locals 6
.limit stack 5
.end method

.method public final a(Landroid/view/ViewParent;Landroid/view/View;FF)Z
aload 1
aload 2
fload 3
fload 4
invokestatic android/support/v4/view/fj/a(Landroid/view/ViewParent;Landroid/view/View;FF)Z
ireturn
.limit locals 5
.limit stack 4
.end method

.method public final a(Landroid/view/ViewParent;Landroid/view/View;FFZ)Z
aload 1
aload 2
fload 3
fload 4
iload 5
invokestatic android/support/v4/view/fj/a(Landroid/view/ViewParent;Landroid/view/View;FFZ)Z
ireturn
.limit locals 6
.limit stack 5
.end method

.method public final a(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;I)Z
aload 1
aload 2
aload 3
iload 4
invokestatic android/support/v4/view/fj/a(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;I)Z
ireturn
.limit locals 5
.limit stack 4
.end method

.method public final b(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/View;I)V
.catch java/lang/AbstractMethodError from L0 to L1 using L2
L0:
aload 1
aload 2
aload 3
iload 4
invokeinterface android/view/ViewParent/onNestedScrollAccepted(Landroid/view/View;Landroid/view/View;I)V 3
L1:
return
L2:
astore 2
ldc "ViewParentCompat"
new java/lang/StringBuilder
dup
ldc "ViewParent "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " does not implement interface method onNestedScrollAccepted"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 2
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
.limit locals 5
.limit stack 4
.end method
