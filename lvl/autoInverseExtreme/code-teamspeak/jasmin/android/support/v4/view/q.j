.bytecode 50.0
.class public final synchronized android/support/v4/view/q
.super java/lang/Object

.field private final 'a' Landroid/support/v4/view/r;

.method private <init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V
aload 0
aload 1
aload 2
iconst_0
invokespecial android/support/v4/view/q/<init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;B)V
return
.limit locals 3
.limit stack 4
.end method

.method private <init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;B)V
aload 0
invokespecial java/lang/Object/<init>()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 17
if_icmple L0
aload 0
new android/support/v4/view/u
dup
aload 1
aload 2
invokespecial android/support/v4/view/u/<init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V
putfield android/support/v4/view/q/a Landroid/support/v4/view/r;
return
L0:
aload 0
new android/support/v4/view/s
dup
aload 1
aload 2
invokespecial android/support/v4/view/s/<init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V
putfield android/support/v4/view/q/a Landroid/support/v4/view/r;
return
.limit locals 4
.limit stack 5
.end method

.method private a(Landroid/view/GestureDetector$OnDoubleTapListener;)V
aload 0
getfield android/support/v4/view/q/a Landroid/support/v4/view/r;
aload 1
invokeinterface android/support/v4/view/r/a(Landroid/view/GestureDetector$OnDoubleTapListener;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method private a(Z)V
aload 0
getfield android/support/v4/view/q/a Landroid/support/v4/view/r;
iload 1
invokeinterface android/support/v4/view/r/a(Z)V 1
return
.limit locals 2
.limit stack 2
.end method

.method private a()Z
aload 0
getfield android/support/v4/view/q/a Landroid/support/v4/view/r;
invokeinterface android/support/v4/view/r/a()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private a(Landroid/view/MotionEvent;)Z
aload 0
getfield android/support/v4/view/q/a Landroid/support/v4/view/r;
aload 1
invokeinterface android/support/v4/view/r/a(Landroid/view/MotionEvent;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method
