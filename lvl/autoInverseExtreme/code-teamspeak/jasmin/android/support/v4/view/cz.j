.bytecode 50.0
.class synchronized android/support/v4/view/cz
.super android/support/v4/view/cy

.method <init>()V
aload 0
invokespecial android/support/v4/view/cy/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/view/ViewGroup;)V
.catch java/lang/NoSuchMethodException from L0 to L1 using L2
.catch java/lang/IllegalAccessException from L3 to L4 using L5
.catch java/lang/IllegalArgumentException from L3 to L4 using L6
.catch java/lang/reflect/InvocationTargetException from L3 to L4 using L7
getstatic android/support/v4/view/dk/b Ljava/lang/reflect/Method;
ifnonnull L3
L0:
ldc android/view/ViewGroup
ldc "setChildrenDrawingOrderEnabled"
iconst_1
anewarray java/lang/Class
dup
iconst_0
getstatic java/lang/Boolean/TYPE Ljava/lang/Class;
aastore
invokevirtual java/lang/Class/getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
putstatic android/support/v4/view/dk/b Ljava/lang/reflect/Method;
L1:
getstatic android/support/v4/view/dk/b Ljava/lang/reflect/Method;
iconst_1
invokevirtual java/lang/reflect/Method/setAccessible(Z)V
L3:
getstatic android/support/v4/view/dk/b Ljava/lang/reflect/Method;
aload 1
iconst_1
anewarray java/lang/Object
dup
iconst_0
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L4:
return
L2:
astore 2
ldc "ViewCompat"
ldc "Unable to find childrenDrawingOrderEnabled"
aload 2
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
goto L1
L5:
astore 1
ldc "ViewCompat"
ldc "Unable to invoke childrenDrawingOrderEnabled"
aload 1
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
L6:
astore 1
ldc "ViewCompat"
ldc "Unable to invoke childrenDrawingOrderEnabled"
aload 1
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
L7:
astore 1
ldc "ViewCompat"
ldc "Unable to invoke childrenDrawingOrderEnabled"
aload 1
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
.limit locals 3
.limit stack 6
.end method

.method public final m(Landroid/view/View;)Z
aload 1
invokevirtual android/view/View/isOpaque()Z
ireturn
.limit locals 2
.limit stack 1
.end method
