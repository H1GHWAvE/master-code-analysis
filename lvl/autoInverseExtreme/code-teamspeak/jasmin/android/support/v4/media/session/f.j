.bytecode 50.0
.class final synchronized android/support/v4/media/session/f
.super java/lang/Object
.implements android/support/v4/media/session/d

.field private 'a' Landroid/os/IBinder;

.method <init>(Landroid/os/IBinder;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v4/media/session/f/a Landroid/os/IBinder;
return
.limit locals 2
.limit stack 2
.end method

.method private static t()Ljava/lang/String;
ldc "android.support.v4.media.session.IMediaSession"
areturn
.limit locals 0
.limit stack 1
.end method

.method public final a(IILjava/lang/String;)V
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 4
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 5
L0:
aload 4
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 4
iload 1
invokevirtual android/os/Parcel/writeInt(I)V
aload 4
iload 2
invokevirtual android/os/Parcel/writeInt(I)V
aload 4
aload 3
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
bipush 11
aload 4
aload 5
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 5
invokevirtual android/os/Parcel/readException()V
L1:
aload 5
invokevirtual android/os/Parcel/recycle()V
aload 4
invokevirtual android/os/Parcel/recycle()V
return
L2:
astore 3
aload 5
invokevirtual android/os/Parcel/recycle()V
aload 4
invokevirtual android/os/Parcel/recycle()V
aload 3
athrow
.limit locals 6
.limit stack 5
.end method

.method public final a(J)V
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 3
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 4
L0:
aload 3
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 3
lload 1
invokevirtual android/os/Parcel/writeLong(J)V
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
bipush 17
aload 3
aload 4
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 4
invokevirtual android/os/Parcel/readException()V
L1:
aload 4
invokevirtual android/os/Parcel/recycle()V
aload 3
invokevirtual android/os/Parcel/recycle()V
return
L2:
astore 5
aload 4
invokevirtual android/os/Parcel/recycle()V
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 5
athrow
.limit locals 6
.limit stack 5
.end method

.method public final a(Landroid/net/Uri;Landroid/os/Bundle;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L6 to L7 using L2
.catch all from L8 to L9 using L2
.catch all from L10 to L11 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 3
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 4
L0:
aload 3
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
L1:
aload 1
ifnull L8
L3:
aload 3
iconst_1
invokevirtual android/os/Parcel/writeInt(I)V
aload 1
aload 3
iconst_0
invokevirtual android/net/Uri/writeToParcel(Landroid/os/Parcel;I)V
L4:
aload 2
ifnull L10
L5:
aload 3
iconst_1
invokevirtual android/os/Parcel/writeInt(I)V
aload 2
aload 3
iconst_0
invokevirtual android/os/Bundle/writeToParcel(Landroid/os/Parcel;I)V
L6:
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
bipush 16
aload 3
aload 4
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 4
invokevirtual android/os/Parcel/readException()V
L7:
aload 4
invokevirtual android/os/Parcel/recycle()V
aload 3
invokevirtual android/os/Parcel/recycle()V
return
L8:
aload 3
iconst_0
invokevirtual android/os/Parcel/writeInt(I)V
L9:
goto L4
L2:
astore 1
aload 4
invokevirtual android/os/Parcel/recycle()V
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
L10:
aload 3
iconst_0
invokevirtual android/os/Parcel/writeInt(I)V
L11:
goto L6
.limit locals 5
.limit stack 5
.end method

.method public final a(Landroid/support/v4/media/RatingCompat;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 3
L0:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
L1:
aload 1
ifnull L6
L3:
aload 2
iconst_1
invokevirtual android/os/Parcel/writeInt(I)V
aload 1
aload 2
iconst_0
invokevirtual android/support/v4/media/RatingCompat/writeToParcel(Landroid/os/Parcel;I)V
L4:
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
bipush 25
aload 2
aload 3
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 3
invokevirtual android/os/Parcel/readException()V
L5:
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
return
L6:
aload 2
iconst_0
invokevirtual android/os/Parcel/writeInt(I)V
L7:
goto L4
L2:
astore 1
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
.limit locals 4
.limit stack 5
.end method

.method public final a(Landroid/support/v4/media/session/a;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 3
L0:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
L1:
aload 1
ifnull L6
L3:
aload 1
invokeinterface android/support/v4/media/session/a/asBinder()Landroid/os/IBinder; 0
astore 1
L4:
aload 2
aload 1
invokevirtual android/os/Parcel/writeStrongBinder(Landroid/os/IBinder;)V
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
iconst_3
aload 2
aload 3
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 3
invokevirtual android/os/Parcel/readException()V
L5:
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
return
L6:
aconst_null
astore 1
goto L4
L2:
astore 1
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
.limit locals 4
.limit stack 5
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 3
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 4
L0:
aload 3
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 3
aload 1
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
L1:
aload 2
ifnull L6
L3:
aload 3
iconst_1
invokevirtual android/os/Parcel/writeInt(I)V
aload 2
aload 3
iconst_0
invokevirtual android/os/Bundle/writeToParcel(Landroid/os/Parcel;I)V
L4:
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
bipush 14
aload 3
aload 4
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 4
invokevirtual android/os/Parcel/readException()V
L5:
aload 4
invokevirtual android/os/Parcel/recycle()V
aload 3
invokevirtual android/os/Parcel/recycle()V
return
L6:
aload 3
iconst_0
invokevirtual android/os/Parcel/writeInt(I)V
L7:
goto L4
L2:
astore 1
aload 4
invokevirtual android/os/Parcel/recycle()V
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
.limit locals 5
.limit stack 5
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;Landroid/support/v4/media/session/MediaSessionCompat$ResultReceiverWrapper;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L6 to L7 using L2
.catch all from L8 to L9 using L2
.catch all from L10 to L11 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 4
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 5
L0:
aload 4
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 4
aload 1
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
L1:
aload 2
ifnull L8
L3:
aload 4
iconst_1
invokevirtual android/os/Parcel/writeInt(I)V
aload 2
aload 4
iconst_0
invokevirtual android/os/Bundle/writeToParcel(Landroid/os/Parcel;I)V
L4:
aload 3
ifnull L10
L5:
aload 4
iconst_1
invokevirtual android/os/Parcel/writeInt(I)V
aload 3
aload 4
iconst_0
invokevirtual android/support/v4/media/session/MediaSessionCompat$ResultReceiverWrapper/writeToParcel(Landroid/os/Parcel;I)V
L6:
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
iconst_1
aload 4
aload 5
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 5
invokevirtual android/os/Parcel/readException()V
L7:
aload 5
invokevirtual android/os/Parcel/recycle()V
aload 4
invokevirtual android/os/Parcel/recycle()V
return
L8:
aload 4
iconst_0
invokevirtual android/os/Parcel/writeInt(I)V
L9:
goto L4
L2:
astore 1
aload 5
invokevirtual android/os/Parcel/recycle()V
aload 4
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
L10:
aload 4
iconst_0
invokevirtual android/os/Parcel/writeInt(I)V
L11:
goto L6
.limit locals 6
.limit stack 5
.end method

.method public final a()Z
.catch all from L0 to L1 using L2
iconst_0
istore 2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 3
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 4
L0:
aload 3
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
iconst_5
aload 3
aload 4
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 4
invokevirtual android/os/Parcel/readException()V
aload 4
invokevirtual android/os/Parcel/readInt()I
istore 1
L1:
iload 1
ifeq L3
iconst_1
istore 2
L3:
aload 4
invokevirtual android/os/Parcel/recycle()V
aload 3
invokevirtual android/os/Parcel/recycle()V
iload 2
ireturn
L2:
astore 5
aload 4
invokevirtual android/os/Parcel/recycle()V
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 5
athrow
.limit locals 6
.limit stack 5
.end method

.method public final a(Landroid/view/KeyEvent;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
iconst_1
istore 3
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 4
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 5
L0:
aload 4
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
L1:
aload 1
ifnull L6
L3:
aload 4
iconst_1
invokevirtual android/os/Parcel/writeInt(I)V
aload 1
aload 4
iconst_0
invokevirtual android/view/KeyEvent/writeToParcel(Landroid/os/Parcel;I)V
L4:
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
iconst_2
aload 4
aload 5
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 5
invokevirtual android/os/Parcel/readException()V
aload 5
invokevirtual android/os/Parcel/readInt()I
istore 2
L5:
iload 2
ifeq L8
L9:
aload 5
invokevirtual android/os/Parcel/recycle()V
aload 4
invokevirtual android/os/Parcel/recycle()V
iload 3
ireturn
L6:
aload 4
iconst_0
invokevirtual android/os/Parcel/writeInt(I)V
L7:
goto L4
L2:
astore 1
aload 5
invokevirtual android/os/Parcel/recycle()V
aload 4
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
L8:
iconst_0
istore 3
goto L9
.limit locals 6
.limit stack 5
.end method

.method public final asBinder()Landroid/os/IBinder;
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final b()Ljava/lang/String;
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 1
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
L0:
aload 1
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
bipush 6
aload 1
aload 2
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 2
invokevirtual android/os/Parcel/readException()V
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
astore 3
L1:
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 3
areturn
L2:
astore 3
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 3
athrow
.limit locals 4
.limit stack 5
.end method

.method public final b(IILjava/lang/String;)V
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 4
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 5
L0:
aload 4
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 4
iload 1
invokevirtual android/os/Parcel/writeInt(I)V
aload 4
iload 2
invokevirtual android/os/Parcel/writeInt(I)V
aload 4
aload 3
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
bipush 12
aload 4
aload 5
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 5
invokevirtual android/os/Parcel/readException()V
L1:
aload 5
invokevirtual android/os/Parcel/recycle()V
aload 4
invokevirtual android/os/Parcel/recycle()V
return
L2:
astore 3
aload 5
invokevirtual android/os/Parcel/recycle()V
aload 4
invokevirtual android/os/Parcel/recycle()V
aload 3
athrow
.limit locals 6
.limit stack 5
.end method

.method public final b(J)V
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 3
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 4
L0:
aload 3
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 3
lload 1
invokevirtual android/os/Parcel/writeLong(J)V
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
bipush 24
aload 3
aload 4
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 4
invokevirtual android/os/Parcel/readException()V
L1:
aload 4
invokevirtual android/os/Parcel/recycle()V
aload 3
invokevirtual android/os/Parcel/recycle()V
return
L2:
astore 5
aload 4
invokevirtual android/os/Parcel/recycle()V
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 5
athrow
.limit locals 6
.limit stack 5
.end method

.method public final b(Landroid/support/v4/media/session/a;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 3
L0:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
L1:
aload 1
ifnull L6
L3:
aload 1
invokeinterface android/support/v4/media/session/a/asBinder()Landroid/os/IBinder; 0
astore 1
L4:
aload 2
aload 1
invokevirtual android/os/Parcel/writeStrongBinder(Landroid/os/IBinder;)V
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
iconst_4
aload 2
aload 3
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 3
invokevirtual android/os/Parcel/readException()V
L5:
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
return
L6:
aconst_null
astore 1
goto L4
L2:
astore 1
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
.limit locals 4
.limit stack 5
.end method

.method public final b(Ljava/lang/String;Landroid/os/Bundle;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 3
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 4
L0:
aload 3
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 3
aload 1
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
L1:
aload 2
ifnull L6
L3:
aload 3
iconst_1
invokevirtual android/os/Parcel/writeInt(I)V
aload 2
aload 3
iconst_0
invokevirtual android/os/Bundle/writeToParcel(Landroid/os/Parcel;I)V
L4:
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
bipush 15
aload 3
aload 4
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 4
invokevirtual android/os/Parcel/readException()V
L5:
aload 4
invokevirtual android/os/Parcel/recycle()V
aload 3
invokevirtual android/os/Parcel/recycle()V
return
L6:
aload 3
iconst_0
invokevirtual android/os/Parcel/writeInt(I)V
L7:
goto L4
L2:
astore 1
aload 4
invokevirtual android/os/Parcel/recycle()V
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
.limit locals 5
.limit stack 5
.end method

.method public final c()Ljava/lang/String;
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 1
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
L0:
aload 1
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
bipush 7
aload 1
aload 2
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 2
invokevirtual android/os/Parcel/readException()V
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
astore 3
L1:
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 3
areturn
L2:
astore 3
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 3
athrow
.limit locals 4
.limit stack 5
.end method

.method public final c(Ljava/lang/String;Landroid/os/Bundle;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 3
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 4
L0:
aload 3
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 3
aload 1
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
L1:
aload 2
ifnull L6
L3:
aload 3
iconst_1
invokevirtual android/os/Parcel/writeInt(I)V
aload 2
aload 3
iconst_0
invokevirtual android/os/Bundle/writeToParcel(Landroid/os/Parcel;I)V
L4:
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
bipush 26
aload 3
aload 4
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 4
invokevirtual android/os/Parcel/readException()V
L5:
aload 4
invokevirtual android/os/Parcel/recycle()V
aload 3
invokevirtual android/os/Parcel/recycle()V
return
L6:
aload 3
iconst_0
invokevirtual android/os/Parcel/writeInt(I)V
L7:
goto L4
L2:
astore 1
aload 4
invokevirtual android/os/Parcel/recycle()V
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
.limit locals 5
.limit stack 5
.end method

.method public final d()Landroid/app/PendingIntent;
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 3
L0:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
bipush 8
aload 2
aload 3
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 3
invokevirtual android/os/Parcel/readException()V
aload 3
invokevirtual android/os/Parcel/readInt()I
ifeq L3
getstatic android/app/PendingIntent/CREATOR Landroid/os/Parcelable$Creator;
aload 3
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast android/app/PendingIntent
astore 1
L1:
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
areturn
L3:
aconst_null
astore 1
goto L1
L2:
astore 1
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
.limit locals 4
.limit stack 5
.end method

.method public final e()J
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 3
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 4
L0:
aload 3
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
bipush 9
aload 3
aload 4
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 4
invokevirtual android/os/Parcel/readException()V
aload 4
invokevirtual android/os/Parcel/readLong()J
lstore 1
L1:
aload 4
invokevirtual android/os/Parcel/recycle()V
aload 3
invokevirtual android/os/Parcel/recycle()V
lload 1
lreturn
L2:
astore 5
aload 4
invokevirtual android/os/Parcel/recycle()V
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 5
athrow
.limit locals 6
.limit stack 5
.end method

.method public final f()Landroid/support/v4/media/session/ParcelableVolumeInfo;
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 3
L0:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
bipush 10
aload 2
aload 3
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 3
invokevirtual android/os/Parcel/readException()V
aload 3
invokevirtual android/os/Parcel/readInt()I
ifeq L3
getstatic android/support/v4/media/session/ParcelableVolumeInfo/CREATOR Landroid/os/Parcelable$Creator;
aload 3
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast android/support/v4/media/session/ParcelableVolumeInfo
astore 1
L1:
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
areturn
L3:
aconst_null
astore 1
goto L1
L2:
astore 1
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
.limit locals 4
.limit stack 5
.end method

.method public final g()V
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 1
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
L0:
aload 1
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
bipush 13
aload 1
aload 2
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 2
invokevirtual android/os/Parcel/readException()V
L1:
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
return
L2:
astore 3
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 3
athrow
.limit locals 4
.limit stack 5
.end method

.method public final h()V
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 1
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
L0:
aload 1
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
bipush 18
aload 1
aload 2
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 2
invokevirtual android/os/Parcel/readException()V
L1:
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
return
L2:
astore 3
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 3
athrow
.limit locals 4
.limit stack 5
.end method

.method public final i()V
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 1
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
L0:
aload 1
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
bipush 19
aload 1
aload 2
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 2
invokevirtual android/os/Parcel/readException()V
L1:
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
return
L2:
astore 3
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 3
athrow
.limit locals 4
.limit stack 5
.end method

.method public final j()V
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 1
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
L0:
aload 1
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
bipush 20
aload 1
aload 2
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 2
invokevirtual android/os/Parcel/readException()V
L1:
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
return
L2:
astore 3
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 3
athrow
.limit locals 4
.limit stack 5
.end method

.method public final k()V
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 1
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
L0:
aload 1
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
bipush 21
aload 1
aload 2
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 2
invokevirtual android/os/Parcel/readException()V
L1:
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
return
L2:
astore 3
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 3
athrow
.limit locals 4
.limit stack 5
.end method

.method public final l()V
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 1
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
L0:
aload 1
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
bipush 22
aload 1
aload 2
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 2
invokevirtual android/os/Parcel/readException()V
L1:
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
return
L2:
astore 3
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 3
athrow
.limit locals 4
.limit stack 5
.end method

.method public final m()V
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 1
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
L0:
aload 1
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
bipush 23
aload 1
aload 2
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 2
invokevirtual android/os/Parcel/readException()V
L1:
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
return
L2:
astore 3
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 3
athrow
.limit locals 4
.limit stack 5
.end method

.method public final n()Landroid/support/v4/media/MediaMetadataCompat;
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 3
L0:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
bipush 27
aload 2
aload 3
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 3
invokevirtual android/os/Parcel/readException()V
aload 3
invokevirtual android/os/Parcel/readInt()I
ifeq L3
getstatic android/support/v4/media/MediaMetadataCompat/CREATOR Landroid/os/Parcelable$Creator;
aload 3
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast android/support/v4/media/MediaMetadataCompat
astore 1
L1:
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
areturn
L3:
aconst_null
astore 1
goto L1
L2:
astore 1
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
.limit locals 4
.limit stack 5
.end method

.method public final o()Landroid/support/v4/media/session/PlaybackStateCompat;
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 3
L0:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
bipush 28
aload 2
aload 3
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 3
invokevirtual android/os/Parcel/readException()V
aload 3
invokevirtual android/os/Parcel/readInt()I
ifeq L3
getstatic android/support/v4/media/session/PlaybackStateCompat/CREATOR Landroid/os/Parcelable$Creator;
aload 3
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast android/support/v4/media/session/PlaybackStateCompat
astore 1
L1:
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
areturn
L3:
aconst_null
astore 1
goto L1
L2:
astore 1
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
.limit locals 4
.limit stack 5
.end method

.method public final p()Ljava/util/List;
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 1
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
L0:
aload 1
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
bipush 29
aload 1
aload 2
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 2
invokevirtual android/os/Parcel/readException()V
aload 2
getstatic android/support/v4/media/session/MediaSessionCompat$QueueItem/CREATOR Landroid/os/Parcelable$Creator;
invokevirtual android/os/Parcel/createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;
astore 3
L1:
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 3
areturn
L2:
astore 3
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 3
athrow
.limit locals 4
.limit stack 5
.end method

.method public final q()Ljava/lang/CharSequence;
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 3
L0:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
bipush 30
aload 2
aload 3
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 3
invokevirtual android/os/Parcel/readException()V
aload 3
invokevirtual android/os/Parcel/readInt()I
ifeq L3
getstatic android/text/TextUtils/CHAR_SEQUENCE_CREATOR Landroid/os/Parcelable$Creator;
aload 3
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast java/lang/CharSequence
astore 1
L1:
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
areturn
L3:
aconst_null
astore 1
goto L1
L2:
astore 1
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
.limit locals 4
.limit stack 5
.end method

.method public final r()Landroid/os/Bundle;
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 3
L0:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
bipush 31
aload 2
aload 3
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 3
invokevirtual android/os/Parcel/readException()V
aload 3
invokevirtual android/os/Parcel/readInt()I
ifeq L3
getstatic android/os/Bundle/CREATOR Landroid/os/Parcelable$Creator;
aload 3
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast android/os/Bundle
astore 1
L1:
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
areturn
L3:
aconst_null
astore 1
goto L1
L2:
astore 1
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
.limit locals 4
.limit stack 5
.end method

.method public final s()I
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 3
L0:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 0
getfield android/support/v4/media/session/f/a Landroid/os/IBinder;
bipush 32
aload 2
aload 3
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 3
invokevirtual android/os/Parcel/readException()V
aload 3
invokevirtual android/os/Parcel/readInt()I
istore 1
L1:
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
iload 1
ireturn
L2:
astore 4
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 4
athrow
.limit locals 5
.limit stack 5
.end method
