.bytecode 50.0
.class public final synchronized android/support/v4/media/i
.super java/lang/Object

.field final 'a' Landroid/os/Bundle;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
putfield android/support/v4/media/i/a Landroid/os/Bundle;
return
.limit locals 1
.limit stack 3
.end method

.method private <init>(Landroid/support/v4/media/MediaMetadataCompat;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new android/os/Bundle
dup
aload 1
invokestatic android/support/v4/media/MediaMetadataCompat/a(Landroid/support/v4/media/MediaMetadataCompat;)Landroid/os/Bundle;
invokespecial android/os/Bundle/<init>(Landroid/os/Bundle;)V
putfield android/support/v4/media/i/a Landroid/os/Bundle;
return
.limit locals 2
.limit stack 4
.end method

.method private a()Landroid/support/v4/media/MediaMetadataCompat;
new android/support/v4/media/MediaMetadataCompat
dup
aload 0
getfield android/support/v4/media/i/a Landroid/os/Bundle;
iconst_0
invokespecial android/support/v4/media/MediaMetadataCompat/<init>(Landroid/os/Bundle;B)V
areturn
.limit locals 1
.limit stack 4
.end method

.method private a(Ljava/lang/String;J)Landroid/support/v4/media/i;
invokestatic android/support/v4/media/MediaMetadataCompat/a()Landroid/support/v4/n/a;
aload 1
invokevirtual android/support/v4/n/a/containsKey(Ljava/lang/Object;)Z
ifeq L0
invokestatic android/support/v4/media/MediaMetadataCompat/a()Landroid/support/v4/n/a;
aload 1
invokevirtual android/support/v4/n/a/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
ifeq L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "The "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " key cannot be used to put a long"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/media/i/a Landroid/os/Bundle;
aload 1
lload 2
invokevirtual android/os/Bundle/putLong(Ljava/lang/String;J)V
aload 0
areturn
.limit locals 4
.limit stack 5
.end method

.method private a(Ljava/lang/String;Landroid/graphics/Bitmap;)Landroid/support/v4/media/i;
invokestatic android/support/v4/media/MediaMetadataCompat/a()Landroid/support/v4/n/a;
aload 1
invokevirtual android/support/v4/n/a/containsKey(Ljava/lang/Object;)Z
ifeq L0
invokestatic android/support/v4/media/MediaMetadataCompat/a()Landroid/support/v4/n/a;
aload 1
invokevirtual android/support/v4/n/a/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
iconst_2
if_icmpeq L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "The "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " key cannot be used to put a Bitmap"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/media/i/a Landroid/os/Bundle;
aload 1
aload 2
invokevirtual android/os/Bundle/putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
aload 0
areturn
.limit locals 3
.limit stack 5
.end method

.method private a(Ljava/lang/String;Landroid/support/v4/media/RatingCompat;)Landroid/support/v4/media/i;
invokestatic android/support/v4/media/MediaMetadataCompat/a()Landroid/support/v4/n/a;
aload 1
invokevirtual android/support/v4/n/a/containsKey(Ljava/lang/Object;)Z
ifeq L0
invokestatic android/support/v4/media/MediaMetadataCompat/a()Landroid/support/v4/n/a;
aload 1
invokevirtual android/support/v4/n/a/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
iconst_3
if_icmpeq L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "The "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " key cannot be used to put a Rating"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/media/i/a Landroid/os/Bundle;
aload 1
aload 2
invokevirtual android/os/Bundle/putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
aload 0
areturn
.limit locals 3
.limit stack 5
.end method

.method private a(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/support/v4/media/i;
invokestatic android/support/v4/media/MediaMetadataCompat/a()Landroid/support/v4/n/a;
aload 1
invokevirtual android/support/v4/n/a/containsKey(Ljava/lang/Object;)Z
ifeq L0
invokestatic android/support/v4/media/MediaMetadataCompat/a()Landroid/support/v4/n/a;
aload 1
invokevirtual android/support/v4/n/a/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
iconst_1
if_icmpeq L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "The "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " key cannot be used to put a CharSequence"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/media/i/a Landroid/os/Bundle;
aload 1
aload 2
invokevirtual android/os/Bundle/putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V
aload 0
areturn
.limit locals 3
.limit stack 5
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Landroid/support/v4/media/i;
invokestatic android/support/v4/media/MediaMetadataCompat/a()Landroid/support/v4/n/a;
aload 1
invokevirtual android/support/v4/n/a/containsKey(Ljava/lang/Object;)Z
ifeq L0
invokestatic android/support/v4/media/MediaMetadataCompat/a()Landroid/support/v4/n/a;
aload 1
invokevirtual android/support/v4/n/a/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
iconst_1
if_icmpeq L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "The "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " key cannot be used to put a String"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/media/i/a Landroid/os/Bundle;
aload 1
aload 2
invokevirtual android/os/Bundle/putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V
aload 0
areturn
.limit locals 3
.limit stack 5
.end method
