.bytecode 50.0
.class public synchronized android/support/v4/media/d
.super java/lang/Object

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a()Ljava/lang/Object;
new android/media/MediaDescription$Builder
dup
invokespecial android/media/MediaDescription$Builder/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
checkcast android/media/MediaDescription$Builder
invokevirtual android/media/MediaDescription$Builder/build()Landroid/media/MediaDescription;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/Object;Landroid/graphics/Bitmap;)V
aload 0
checkcast android/media/MediaDescription$Builder
aload 1
invokevirtual android/media/MediaDescription$Builder/setIconBitmap(Landroid/graphics/Bitmap;)Landroid/media/MediaDescription$Builder;
pop
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;Landroid/net/Uri;)V
aload 0
checkcast android/media/MediaDescription$Builder
aload 1
invokevirtual android/media/MediaDescription$Builder/setIconUri(Landroid/net/Uri;)Landroid/media/MediaDescription$Builder;
pop
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;Landroid/os/Bundle;)V
aload 0
checkcast android/media/MediaDescription$Builder
aload 1
invokevirtual android/media/MediaDescription$Builder/setExtras(Landroid/os/Bundle;)Landroid/media/MediaDescription$Builder;
pop
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/CharSequence;)V
aload 0
checkcast android/media/MediaDescription$Builder
aload 1
invokevirtual android/media/MediaDescription$Builder/setTitle(Ljava/lang/CharSequence;)Landroid/media/MediaDescription$Builder;
pop
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/String;)V
aload 0
checkcast android/media/MediaDescription$Builder
aload 1
invokevirtual android/media/MediaDescription$Builder/setMediaId(Ljava/lang/String;)Landroid/media/MediaDescription$Builder;
pop
return
.limit locals 2
.limit stack 2
.end method

.method private static b(Ljava/lang/Object;Ljava/lang/CharSequence;)V
aload 0
checkcast android/media/MediaDescription$Builder
aload 1
invokevirtual android/media/MediaDescription$Builder/setSubtitle(Ljava/lang/CharSequence;)Landroid/media/MediaDescription$Builder;
pop
return
.limit locals 2
.limit stack 2
.end method

.method private static c(Ljava/lang/Object;Ljava/lang/CharSequence;)V
aload 0
checkcast android/media/MediaDescription$Builder
aload 1
invokevirtual android/media/MediaDescription$Builder/setDescription(Ljava/lang/CharSequence;)Landroid/media/MediaDescription$Builder;
pop
return
.limit locals 2
.limit stack 2
.end method
