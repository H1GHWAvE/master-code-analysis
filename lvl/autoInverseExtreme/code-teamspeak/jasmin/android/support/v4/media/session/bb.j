.bytecode 50.0
.class synchronized android/support/v4/media/session/bb
.super android/media/session/MediaSession$Callback

.field protected final 'a' Landroid/support/v4/media/session/ba;

.method public <init>(Landroid/support/v4/media/session/ba;)V
aload 0
invokespecial android/media/session/MediaSession$Callback/<init>()V
aload 0
aload 1
putfield android/support/v4/media/session/bb/a Landroid/support/v4/media/session/ba;
return
.limit locals 2
.limit stack 2
.end method

.method public onCommand(Ljava/lang/String;Landroid/os/Bundle;Landroid/os/ResultReceiver;)V
return
.limit locals 4
.limit stack 0
.end method

.method public onCustomAction(Ljava/lang/String;Landroid/os/Bundle;)V
return
.limit locals 3
.limit stack 0
.end method

.method public onFastForward()V
return
.limit locals 1
.limit stack 0
.end method

.method public onMediaButtonEvent(Landroid/content/Intent;)Z
aload 0
aload 1
invokespecial android/media/session/MediaSession$Callback/onMediaButtonEvent(Landroid/content/Intent;)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public onPause()V
return
.limit locals 1
.limit stack 0
.end method

.method public onPlay()V
return
.limit locals 1
.limit stack 0
.end method

.method public onPlayFromMediaId(Ljava/lang/String;Landroid/os/Bundle;)V
return
.limit locals 3
.limit stack 0
.end method

.method public onPlayFromSearch(Ljava/lang/String;Landroid/os/Bundle;)V
return
.limit locals 3
.limit stack 0
.end method

.method public onRewind()V
return
.limit locals 1
.limit stack 0
.end method

.method public onSeekTo(J)V
return
.limit locals 3
.limit stack 0
.end method

.method public onSetRating(Landroid/media/Rating;)V
aload 0
getfield android/support/v4/media/session/bb/a Landroid/support/v4/media/session/ba;
aload 1
invokeinterface android/support/v4/media/session/ba/a(Ljava/lang/Object;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public onSkipToNext()V
return
.limit locals 1
.limit stack 0
.end method

.method public onSkipToPrevious()V
return
.limit locals 1
.limit stack 0
.end method

.method public onSkipToQueueItem(J)V
return
.limit locals 3
.limit stack 0
.end method

.method public onStop()V
return
.limit locals 1
.limit stack 0
.end method
