.bytecode 50.0
.class public final synchronized android/support/v4/media/session/PlaybackStateCompat$CustomAction
.super java/lang/Object
.implements android/os/Parcelable

.field public static final 'CREATOR' Landroid/os/Parcelable$Creator;

.field final 'a' Ljava/lang/String;

.field final 'b' Ljava/lang/CharSequence;

.field final 'c' I

.field final 'd' Landroid/os/Bundle;

.field 'e' Ljava/lang/Object;

.method static <clinit>()V
new android/support/v4/media/session/bm
dup
invokespecial android/support/v4/media/session/bm/<init>()V
putstatic android/support/v4/media/session/PlaybackStateCompat$CustomAction/CREATOR Landroid/os/Parcelable$Creator;
return
.limit locals 0
.limit stack 2
.end method

.method private <init>(Landroid/os/Parcel;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
putfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/a Ljava/lang/String;
aload 0
getstatic android/text/TextUtils/CHAR_SEQUENCE_CREATOR Landroid/os/Parcelable$Creator;
aload 1
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast java/lang/CharSequence
putfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/b Ljava/lang/CharSequence;
aload 0
aload 1
invokevirtual android/os/Parcel/readInt()I
putfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/c I
aload 0
aload 1
invokevirtual android/os/Parcel/readBundle()Landroid/os/Bundle;
putfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/d Landroid/os/Bundle;
return
.limit locals 2
.limit stack 3
.end method

.method synthetic <init>(Landroid/os/Parcel;B)V
aload 0
aload 1
invokespecial android/support/v4/media/session/PlaybackStateCompat$CustomAction/<init>(Landroid/os/Parcel;)V
return
.limit locals 3
.limit stack 2
.end method

.method private <init>(Ljava/lang/String;Ljava/lang/CharSequence;ILandroid/os/Bundle;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/a Ljava/lang/String;
aload 0
aload 2
putfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/b Ljava/lang/CharSequence;
aload 0
iload 3
putfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/c I
aload 0
aload 4
putfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/d Landroid/os/Bundle;
return
.limit locals 5
.limit stack 2
.end method

.method synthetic <init>(Ljava/lang/String;Ljava/lang/CharSequence;ILandroid/os/Bundle;B)V
aload 0
aload 1
aload 2
iload 3
aload 4
invokespecial android/support/v4/media/session/PlaybackStateCompat$CustomAction/<init>(Ljava/lang/String;Ljava/lang/CharSequence;ILandroid/os/Bundle;)V
return
.limit locals 6
.limit stack 5
.end method

.method public static a(Ljava/lang/Object;)Landroid/support/v4/media/session/PlaybackStateCompat$CustomAction;
aload 0
ifnull L0
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmpge L1
L0:
aconst_null
areturn
L1:
new android/support/v4/media/session/PlaybackStateCompat$CustomAction
dup
aload 0
checkcast android/media/session/PlaybackState$CustomAction
invokevirtual android/media/session/PlaybackState$CustomAction/getAction()Ljava/lang/String;
aload 0
checkcast android/media/session/PlaybackState$CustomAction
invokevirtual android/media/session/PlaybackState$CustomAction/getName()Ljava/lang/CharSequence;
aload 0
checkcast android/media/session/PlaybackState$CustomAction
invokevirtual android/media/session/PlaybackState$CustomAction/getIcon()I
aload 0
checkcast android/media/session/PlaybackState$CustomAction
invokevirtual android/media/session/PlaybackState$CustomAction/getExtras()Landroid/os/Bundle;
invokespecial android/support/v4/media/session/PlaybackStateCompat$CustomAction/<init>(Ljava/lang/String;Ljava/lang/CharSequence;ILandroid/os/Bundle;)V
astore 1
aload 1
aload 0
putfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/e Ljava/lang/Object;
aload 1
areturn
.limit locals 2
.limit stack 6
.end method

.method private a()Ljava/lang/Object;
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/e Ljava/lang/Object;
ifnonnull L0
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmpge L1
L0:
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/e Ljava/lang/Object;
areturn
L1:
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/a Ljava/lang/String;
astore 3
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/b Ljava/lang/CharSequence;
astore 4
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/c I
istore 1
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/d Landroid/os/Bundle;
astore 2
new android/media/session/PlaybackState$CustomAction$Builder
dup
aload 3
aload 4
iload 1
invokespecial android/media/session/PlaybackState$CustomAction$Builder/<init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V
astore 3
aload 3
aload 2
invokevirtual android/media/session/PlaybackState$CustomAction$Builder/setExtras(Landroid/os/Bundle;)Landroid/media/session/PlaybackState$CustomAction$Builder;
pop
aload 0
aload 3
invokevirtual android/media/session/PlaybackState$CustomAction$Builder/build()Landroid/media/session/PlaybackState$CustomAction;
putfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/e Ljava/lang/Object;
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/e Ljava/lang/Object;
areturn
.limit locals 5
.limit stack 5
.end method

.method private b()Ljava/lang/String;
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/a Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c()Ljava/lang/CharSequence;
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/b Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d()I
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/c I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private e()Landroid/os/Bundle;
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/d Landroid/os/Bundle;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final describeContents()I
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "Action:mName='"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/b Ljava/lang/CharSequence;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", mIcon="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/c I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", mExtras="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/d Landroid/os/Bundle;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
aload 1
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/a Ljava/lang/String;
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/b Ljava/lang/CharSequence;
aload 1
iload 2
invokestatic android/text/TextUtils/writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V
aload 1
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/c I
invokevirtual android/os/Parcel/writeInt(I)V
aload 1
aload 0
getfield android/support/v4/media/session/PlaybackStateCompat$CustomAction/d Landroid/os/Bundle;
invokevirtual android/os/Parcel/writeBundle(Landroid/os/Bundle;)V
return
.limit locals 3
.limit stack 3
.end method
