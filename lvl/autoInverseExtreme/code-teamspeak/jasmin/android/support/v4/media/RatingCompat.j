.bytecode 50.0
.class public final synchronized android/support/v4/media/RatingCompat
.super java/lang/Object
.implements android/os/Parcelable

.field public static final 'CREATOR' Landroid/os/Parcelable$Creator;

.field public static final 'a' I = 0


.field public static final 'b' I = 1


.field public static final 'c' I = 2


.field public static final 'd' I = 3


.field public static final 'e' I = 4


.field public static final 'f' I = 5


.field public static final 'g' I = 6


.field private static final 'h' Ljava/lang/String; = "Rating"

.field private static final 'i' F = -1.0F


.field private final 'j' I

.field private final 'k' F

.field private 'l' Ljava/lang/Object;

.method static <clinit>()V
new android/support/v4/media/o
dup
invokespecial android/support/v4/media/o/<init>()V
putstatic android/support/v4/media/RatingCompat/CREATOR Landroid/os/Parcelable$Creator;
return
.limit locals 0
.limit stack 2
.end method

.method private <init>(IF)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iload 1
putfield android/support/v4/media/RatingCompat/j I
aload 0
fload 2
putfield android/support/v4/media/RatingCompat/k F
return
.limit locals 3
.limit stack 2
.end method

.method synthetic <init>(IFB)V
aload 0
iload 1
fload 2
invokespecial android/support/v4/media/RatingCompat/<init>(IF)V
return
.limit locals 4
.limit stack 3
.end method

.method private static a(F)Landroid/support/v4/media/RatingCompat;
fload 0
fconst_0
fcmpg
iflt L0
fload 0
ldc_w 100.0F
fcmpl
ifle L1
L0:
ldc "Rating"
ldc "Invalid percentage-based rating value"
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aconst_null
areturn
L1:
new android/support/v4/media/RatingCompat
dup
bipush 6
fload 0
invokespecial android/support/v4/media/RatingCompat/<init>(IF)V
areturn
.limit locals 1
.limit stack 4
.end method

.method private static a(I)Landroid/support/v4/media/RatingCompat;
iload 0
tableswitch 1
L0
L0
L0
L0
L0
L0
default : L1
L1:
aconst_null
areturn
L0:
new android/support/v4/media/RatingCompat
dup
iload 0
ldc_w -1.0F
invokespecial android/support/v4/media/RatingCompat/<init>(IF)V
areturn
.limit locals 1
.limit stack 4
.end method

.method private static a(IF)Landroid/support/v4/media/RatingCompat;
iload 0
tableswitch 3
L0
L1
L2
default : L3
L3:
ldc "Rating"
new java/lang/StringBuilder
dup
ldc "Invalid rating style ("
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 0
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ") for a star rating"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aconst_null
areturn
L0:
ldc_w 3.0F
fstore 2
L4:
fload 1
fconst_0
fcmpg
iflt L5
fload 1
fload 2
fcmpl
ifle L6
L5:
ldc "Rating"
ldc "Trying to set out of range star-based rating"
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aconst_null
areturn
L1:
ldc_w 4.0F
fstore 2
goto L4
L2:
ldc_w 5.0F
fstore 2
goto L4
L6:
new android/support/v4/media/RatingCompat
dup
iload 0
fload 1
invokespecial android/support/v4/media/RatingCompat/<init>(IF)V
areturn
.limit locals 3
.limit stack 4
.end method

.method public static a(Ljava/lang/Object;)Landroid/support/v4/media/RatingCompat;
fconst_1
fstore 1
aconst_null
astore 4
aload 0
ifnull L0
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmpge L1
L0:
aconst_null
areturn
L1:
aload 0
checkcast android/media/Rating
invokevirtual android/media/Rating/getRatingStyle()I
istore 3
aload 0
checkcast android/media/Rating
invokevirtual android/media/Rating/isRated()Z
ifeq L2
iload 3
tableswitch 1
L3
L4
L5
L5
L5
L6
default : L7
L7:
aconst_null
areturn
L3:
aload 0
checkcast android/media/Rating
invokevirtual android/media/Rating/hasHeart()Z
ifeq L8
fconst_1
fstore 1
L9:
new android/support/v4/media/RatingCompat
dup
iconst_1
fload 1
invokespecial android/support/v4/media/RatingCompat/<init>(IF)V
astore 4
L10:
aload 4
aload 0
putfield android/support/v4/media/RatingCompat/l Ljava/lang/Object;
aload 4
areturn
L8:
fconst_0
fstore 1
goto L9
L4:
aload 0
checkcast android/media/Rating
invokevirtual android/media/Rating/isThumbUp()Z
ifeq L11
L12:
new android/support/v4/media/RatingCompat
dup
iconst_2
fload 1
invokespecial android/support/v4/media/RatingCompat/<init>(IF)V
astore 4
goto L10
L11:
fconst_0
fstore 1
goto L12
L5:
aload 0
checkcast android/media/Rating
invokevirtual android/media/Rating/getStarRating()F
fstore 2
iload 3
tableswitch 3
L13
L14
L15
default : L16
L16:
ldc "Rating"
new java/lang/StringBuilder
dup
ldc "Invalid rating style ("
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 3
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ") for a star rating"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aconst_null
astore 4
L17:
goto L10
L13:
ldc_w 3.0F
fstore 1
L18:
fload 2
fconst_0
fcmpg
iflt L19
fload 2
fload 1
fcmpl
ifle L20
L19:
ldc "Rating"
ldc "Trying to set out of range star-based rating"
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aconst_null
astore 4
goto L17
L14:
ldc_w 4.0F
fstore 1
goto L18
L15:
ldc_w 5.0F
fstore 1
goto L18
L20:
new android/support/v4/media/RatingCompat
dup
iload 3
fload 2
invokespecial android/support/v4/media/RatingCompat/<init>(IF)V
astore 4
goto L17
L6:
aload 0
checkcast android/media/Rating
invokevirtual android/media/Rating/getPercentRating()F
fstore 1
fload 1
fconst_0
fcmpg
iflt L21
fload 1
ldc_w 100.0F
fcmpl
ifle L22
L21:
ldc "Rating"
ldc "Invalid percentage-based rating value"
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
goto L10
L22:
new android/support/v4/media/RatingCompat
dup
bipush 6
fload 1
invokespecial android/support/v4/media/RatingCompat/<init>(IF)V
astore 4
goto L10
L2:
iload 3
tableswitch 1
L23
L23
L23
L23
L23
L23
default : L24
L24:
goto L10
L23:
new android/support/v4/media/RatingCompat
dup
iload 3
ldc_w -1.0F
invokespecial android/support/v4/media/RatingCompat/<init>(IF)V
astore 4
goto L10
.limit locals 5
.limit stack 4
.end method

.method private static a(Z)Landroid/support/v4/media/RatingCompat;
iload 0
ifeq L0
fconst_1
fstore 1
L1:
new android/support/v4/media/RatingCompat
dup
iconst_1
fload 1
invokespecial android/support/v4/media/RatingCompat/<init>(IF)V
areturn
L0:
fconst_0
fstore 1
goto L1
.limit locals 2
.limit stack 4
.end method

.method private static b(Z)Landroid/support/v4/media/RatingCompat;
iload 0
ifeq L0
fconst_1
fstore 1
L1:
new android/support/v4/media/RatingCompat
dup
iconst_2
fload 1
invokespecial android/support/v4/media/RatingCompat/<init>(IF)V
areturn
L0:
fconst_0
fstore 1
goto L1
.limit locals 2
.limit stack 4
.end method

.method private b()Z
aload 0
getfield android/support/v4/media/RatingCompat/k F
fconst_0
fcmpl
iflt L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private c()I
aload 0
getfield android/support/v4/media/RatingCompat/j I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d()Z
aload 0
getfield android/support/v4/media/RatingCompat/j I
iconst_1
if_icmpeq L0
L1:
iconst_0
ireturn
L0:
aload 0
getfield android/support/v4/media/RatingCompat/k F
fconst_1
fcmpl
ifne L1
iconst_1
ireturn
.limit locals 1
.limit stack 2
.end method

.method private e()Z
aload 0
getfield android/support/v4/media/RatingCompat/j I
iconst_2
if_icmpeq L0
L1:
iconst_0
ireturn
L0:
aload 0
getfield android/support/v4/media/RatingCompat/k F
fconst_1
fcmpl
ifne L1
iconst_1
ireturn
.limit locals 1
.limit stack 2
.end method

.method private f()F
aload 0
getfield android/support/v4/media/RatingCompat/j I
tableswitch 3
L0
L0
L0
default : L1
L1:
ldc_w -1.0F
freturn
L0:
aload 0
invokespecial android/support/v4/media/RatingCompat/b()Z
ifeq L1
aload 0
getfield android/support/v4/media/RatingCompat/k F
freturn
.limit locals 1
.limit stack 1
.end method

.method private g()F
aload 0
getfield android/support/v4/media/RatingCompat/j I
bipush 6
if_icmpne L0
aload 0
invokespecial android/support/v4/media/RatingCompat/b()Z
ifne L1
L0:
ldc_w -1.0F
freturn
L1:
aload 0
getfield android/support/v4/media/RatingCompat/k F
freturn
.limit locals 1
.limit stack 2
.end method

.method public final a()Ljava/lang/Object;
ldc_w -1.0F
fstore 2
iconst_1
istore 5
iconst_1
istore 4
aload 0
getfield android/support/v4/media/RatingCompat/l Ljava/lang/Object;
ifnonnull L0
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmpge L1
L0:
aload 0
getfield android/support/v4/media/RatingCompat/l Ljava/lang/Object;
areturn
L1:
aload 0
invokespecial android/support/v4/media/RatingCompat/b()Z
ifeq L2
aload 0
getfield android/support/v4/media/RatingCompat/j I
tableswitch 1
L3
L4
L5
L5
L5
L6
default : L7
L7:
aconst_null
areturn
L3:
aload 0
getfield android/support/v4/media/RatingCompat/j I
iconst_1
if_icmpne L8
aload 0
getfield android/support/v4/media/RatingCompat/k F
fconst_1
fcmpl
ifne L8
L9:
aload 0
iload 4
invokestatic android/media/Rating/newHeartRating(Z)Landroid/media/Rating;
putfield android/support/v4/media/RatingCompat/l Ljava/lang/Object;
L10:
aload 0
getfield android/support/v4/media/RatingCompat/l Ljava/lang/Object;
areturn
L8:
iconst_0
istore 4
goto L9
L4:
aload 0
getfield android/support/v4/media/RatingCompat/j I
iconst_2
if_icmpne L11
aload 0
getfield android/support/v4/media/RatingCompat/k F
fconst_1
fcmpl
ifne L11
iload 5
istore 4
L12:
aload 0
iload 4
invokestatic android/media/Rating/newThumbRating(Z)Landroid/media/Rating;
putfield android/support/v4/media/RatingCompat/l Ljava/lang/Object;
goto L10
L11:
iconst_0
istore 4
goto L12
L5:
aload 0
getfield android/support/v4/media/RatingCompat/j I
istore 3
aload 0
getfield android/support/v4/media/RatingCompat/j I
tableswitch 3
L13
L13
L13
default : L14
L14:
ldc_w -1.0F
fstore 1
L15:
aload 0
iload 3
fload 1
invokestatic android/media/Rating/newStarRating(IF)Landroid/media/Rating;
putfield android/support/v4/media/RatingCompat/l Ljava/lang/Object;
goto L10
L13:
aload 0
invokespecial android/support/v4/media/RatingCompat/b()Z
ifeq L14
aload 0
getfield android/support/v4/media/RatingCompat/k F
fstore 1
goto L15
L6:
fload 2
fstore 1
aload 0
getfield android/support/v4/media/RatingCompat/j I
bipush 6
if_icmpne L16
aload 0
invokespecial android/support/v4/media/RatingCompat/b()Z
ifne L17
fload 2
fstore 1
L16:
aload 0
fload 1
invokestatic android/media/Rating/newPercentageRating(F)Landroid/media/Rating;
putfield android/support/v4/media/RatingCompat/l Ljava/lang/Object;
goto L7
L17:
aload 0
getfield android/support/v4/media/RatingCompat/k F
fstore 1
goto L16
L2:
aload 0
aload 0
getfield android/support/v4/media/RatingCompat/j I
invokestatic android/media/Rating/newUnratedRating(I)Landroid/media/Rating;
putfield android/support/v4/media/RatingCompat/l Ljava/lang/Object;
goto L10
.limit locals 6
.limit stack 3
.end method

.method public final describeContents()I
aload 0
getfield android/support/v4/media/RatingCompat/j I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "Rating:style="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield android/support/v4/media/RatingCompat/j I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " rating="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
astore 2
aload 0
getfield android/support/v4/media/RatingCompat/k F
fconst_0
fcmpg
ifge L0
ldc "unrated"
astore 1
L1:
aload 2
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L0:
aload 0
getfield android/support/v4/media/RatingCompat/k F
invokestatic java/lang/String/valueOf(F)Ljava/lang/String;
astore 1
goto L1
.limit locals 3
.limit stack 3
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
aload 1
aload 0
getfield android/support/v4/media/RatingCompat/j I
invokevirtual android/os/Parcel/writeInt(I)V
aload 1
aload 0
getfield android/support/v4/media/RatingCompat/k F
invokevirtual android/os/Parcel/writeFloat(F)V
return
.limit locals 3
.limit stack 2
.end method
