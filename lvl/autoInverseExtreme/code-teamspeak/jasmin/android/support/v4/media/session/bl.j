.bytecode 50.0
.class public final synchronized android/support/v4/media/session/bl
.super java/lang/Object

.field final 'a' Ljava/util/List;

.field 'b' I

.field 'c' J

.field 'd' J

.field 'e' F

.field 'f' J

.field 'g' Ljava/lang/CharSequence;

.field 'h' J

.field 'i' J

.field 'j' Landroid/os/Bundle;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/media/session/bl/a Ljava/util/List;
aload 0
ldc2_w -1L
putfield android/support/v4/media/session/bl/i J
return
.limit locals 1
.limit stack 3
.end method

.method public <init>(Landroid/support/v4/media/session/PlaybackStateCompat;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/media/session/bl/a Ljava/util/List;
aload 0
ldc2_w -1L
putfield android/support/v4/media/session/bl/i J
aload 0
aload 1
invokestatic android/support/v4/media/session/PlaybackStateCompat/a(Landroid/support/v4/media/session/PlaybackStateCompat;)I
putfield android/support/v4/media/session/bl/b I
aload 0
aload 1
invokestatic android/support/v4/media/session/PlaybackStateCompat/b(Landroid/support/v4/media/session/PlaybackStateCompat;)J
putfield android/support/v4/media/session/bl/c J
aload 0
aload 1
invokestatic android/support/v4/media/session/PlaybackStateCompat/c(Landroid/support/v4/media/session/PlaybackStateCompat;)F
putfield android/support/v4/media/session/bl/e F
aload 0
aload 1
invokestatic android/support/v4/media/session/PlaybackStateCompat/d(Landroid/support/v4/media/session/PlaybackStateCompat;)J
putfield android/support/v4/media/session/bl/h J
aload 0
aload 1
invokestatic android/support/v4/media/session/PlaybackStateCompat/e(Landroid/support/v4/media/session/PlaybackStateCompat;)J
putfield android/support/v4/media/session/bl/d J
aload 0
aload 1
invokestatic android/support/v4/media/session/PlaybackStateCompat/f(Landroid/support/v4/media/session/PlaybackStateCompat;)J
putfield android/support/v4/media/session/bl/f J
aload 0
aload 1
invokestatic android/support/v4/media/session/PlaybackStateCompat/g(Landroid/support/v4/media/session/PlaybackStateCompat;)Ljava/lang/CharSequence;
putfield android/support/v4/media/session/bl/g Ljava/lang/CharSequence;
aload 1
invokestatic android/support/v4/media/session/PlaybackStateCompat/h(Landroid/support/v4/media/session/PlaybackStateCompat;)Ljava/util/List;
ifnull L0
aload 0
getfield android/support/v4/media/session/bl/a Ljava/util/List;
aload 1
invokestatic android/support/v4/media/session/PlaybackStateCompat/h(Landroid/support/v4/media/session/PlaybackStateCompat;)Ljava/util/List;
invokeinterface java/util/List/addAll(Ljava/util/Collection;)Z 1
pop
L0:
aload 0
aload 1
invokestatic android/support/v4/media/session/PlaybackStateCompat/i(Landroid/support/v4/media/session/PlaybackStateCompat;)J
putfield android/support/v4/media/session/bl/i J
aload 0
aload 1
invokestatic android/support/v4/media/session/PlaybackStateCompat/j(Landroid/support/v4/media/session/PlaybackStateCompat;)Landroid/os/Bundle;
putfield android/support/v4/media/session/bl/j Landroid/os/Bundle;
return
.limit locals 2
.limit stack 3
.end method

.method private a()Landroid/support/v4/media/session/PlaybackStateCompat;
new android/support/v4/media/session/PlaybackStateCompat
dup
aload 0
getfield android/support/v4/media/session/bl/b I
aload 0
getfield android/support/v4/media/session/bl/c J
aload 0
getfield android/support/v4/media/session/bl/d J
aload 0
getfield android/support/v4/media/session/bl/e F
aload 0
getfield android/support/v4/media/session/bl/f J
aload 0
getfield android/support/v4/media/session/bl/g Ljava/lang/CharSequence;
aload 0
getfield android/support/v4/media/session/bl/h J
aload 0
getfield android/support/v4/media/session/bl/a Ljava/util/List;
aload 0
getfield android/support/v4/media/session/bl/i J
aload 0
getfield android/support/v4/media/session/bl/j Landroid/os/Bundle;
iconst_0
invokespecial android/support/v4/media/session/PlaybackStateCompat/<init>(IJJFJLjava/lang/CharSequence;JLjava/util/List;JLandroid/os/Bundle;B)V
areturn
.limit locals 1
.limit stack 18
.end method

.method private a(IJF)Landroid/support/v4/media/session/bl;
aload 0
iload 1
lload 2
fload 4
invokestatic android/os/SystemClock/elapsedRealtime()J
invokevirtual android/support/v4/media/session/bl/a(IJFJ)Landroid/support/v4/media/session/bl;
areturn
.limit locals 5
.limit stack 7
.end method

.method private a(J)Landroid/support/v4/media/session/bl;
aload 0
lload 1
putfield android/support/v4/media/session/bl/d J
aload 0
areturn
.limit locals 3
.limit stack 3
.end method

.method private a(Landroid/os/Bundle;)Landroid/support/v4/media/session/bl;
aload 0
aload 1
putfield android/support/v4/media/session/bl/j Landroid/os/Bundle;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/support/v4/media/session/PlaybackStateCompat$CustomAction;)Landroid/support/v4/media/session/bl;
aload 0
getfield android/support/v4/media/session/bl/a Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/CharSequence;)Landroid/support/v4/media/session/bl;
aload 0
aload 1
putfield android/support/v4/media/session/bl/g Ljava/lang/CharSequence;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;I)Landroid/support/v4/media/session/bl;
new android/support/v4/media/session/PlaybackStateCompat$CustomAction
dup
aload 1
aload 2
iload 3
aconst_null
iconst_0
invokespecial android/support/v4/media/session/PlaybackStateCompat$CustomAction/<init>(Ljava/lang/String;Ljava/lang/CharSequence;ILandroid/os/Bundle;B)V
astore 1
aload 0
getfield android/support/v4/media/session/bl/a Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 0
areturn
.limit locals 4
.limit stack 7
.end method

.method private b(J)Landroid/support/v4/media/session/bl;
aload 0
lload 1
putfield android/support/v4/media/session/bl/f J
aload 0
areturn
.limit locals 3
.limit stack 3
.end method

.method private c(J)Landroid/support/v4/media/session/bl;
aload 0
lload 1
putfield android/support/v4/media/session/bl/i J
aload 0
areturn
.limit locals 3
.limit stack 3
.end method

.method public final a(IJFJ)Landroid/support/v4/media/session/bl;
aload 0
iload 1
putfield android/support/v4/media/session/bl/b I
aload 0
lload 2
putfield android/support/v4/media/session/bl/c J
aload 0
lload 5
putfield android/support/v4/media/session/bl/h J
aload 0
fload 4
putfield android/support/v4/media/session/bl/e F
aload 0
areturn
.limit locals 7
.limit stack 3
.end method
