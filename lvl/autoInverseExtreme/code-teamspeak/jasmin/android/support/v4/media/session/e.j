.bytecode 50.0
.class public synchronized abstract android/support/v4/media/session/e
.super android/os/Binder
.implements android/support/v4/media/session/d

.field static final 'A' I = 27


.field static final 'B' I = 28


.field static final 'C' I = 29


.field static final 'D' I = 30


.field static final 'E' I = 31


.field static final 'F' I = 32


.field private static final 'G' Ljava/lang/String; = "android.support.v4.media.session.IMediaSession"

.field static final 'a' I = 1


.field static final 'b' I = 2


.field static final 'c' I = 3


.field static final 'd' I = 4


.field static final 'e' I = 5


.field static final 'f' I = 6


.field static final 'g' I = 7


.field static final 'h' I = 8


.field static final 'i' I = 9


.field static final 'j' I = 10


.field static final 'k' I = 11


.field static final 'l' I = 12


.field static final 'm' I = 13


.field static final 'n' I = 14


.field static final 'o' I = 15


.field static final 'p' I = 16


.field static final 'q' I = 17


.field static final 'r' I = 18


.field static final 's' I = 19


.field static final 't' I = 20


.field static final 'u' I = 21


.field static final 'v' I = 22


.field static final 'w' I = 23


.field static final 'x' I = 24


.field static final 'y' I = 25


.field static final 'z' I = 26


.method public <init>()V
aload 0
invokespecial android/os/Binder/<init>()V
aload 0
aload 0
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/support/v4/media/session/e/attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
return
.limit locals 1
.limit stack 3
.end method

.method public static a(Landroid/os/IBinder;)Landroid/support/v4/media/session/d;
aload 0
ifnonnull L0
aconst_null
areturn
L0:
aload 0
ldc "android.support.v4.media.session.IMediaSession"
invokeinterface android/os/IBinder/queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface; 1
astore 1
aload 1
ifnull L1
aload 1
instanceof android/support/v4/media/session/d
ifeq L1
aload 1
checkcast android/support/v4/media/session/d
areturn
L1:
new android/support/v4/media/session/f
dup
aload 0
invokespecial android/support/v4/media/session/f/<init>(Landroid/os/IBinder;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public asBinder()Landroid/os/IBinder;
aload 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
iconst_0
istore 5
iload 1
lookupswitch
1 : L0
2 : L1
3 : L2
4 : L3
5 : L4
6 : L5
7 : L6
8 : L7
9 : L8
10 : L9
11 : L10
12 : L11
13 : L12
14 : L13
15 : L14
16 : L15
17 : L16
18 : L17
19 : L18
20 : L19
21 : L20
22 : L21
23 : L22
24 : L23
25 : L24
26 : L25
27 : L26
28 : L27
29 : L28
30 : L29
31 : L30
32 : L31
1598968902 : L32
default : L33
L33:
aload 0
iload 1
aload 2
aload 3
iload 4
invokespecial android/os/Binder/onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
ireturn
L32:
aload 3
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L0:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
astore 10
aload 2
invokevirtual android/os/Parcel/readInt()I
ifeq L34
getstatic android/os/Bundle/CREATOR Landroid/os/Parcelable$Creator;
aload 2
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast android/os/Bundle
astore 9
L35:
aload 2
invokevirtual android/os/Parcel/readInt()I
ifeq L36
getstatic android/support/v4/media/session/MediaSessionCompat$ResultReceiverWrapper/CREATOR Landroid/os/Parcelable$Creator;
aload 2
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast android/support/v4/media/session/MediaSessionCompat$ResultReceiverWrapper
astore 2
L37:
aload 0
aload 10
aload 9
aload 2
invokevirtual android/support/v4/media/session/e/a(Ljava/lang/String;Landroid/os/Bundle;Landroid/support/v4/media/session/MediaSessionCompat$ResultReceiverWrapper;)V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
L34:
aconst_null
astore 9
goto L35
L36:
aconst_null
astore 2
goto L37
L1:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 2
invokevirtual android/os/Parcel/readInt()I
ifeq L38
getstatic android/view/KeyEvent/CREATOR Landroid/os/Parcelable$Creator;
aload 2
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast android/view/KeyEvent
astore 2
L39:
aload 0
aload 2
invokevirtual android/support/v4/media/session/e/a(Landroid/view/KeyEvent;)Z
istore 6
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iload 6
ifeq L40
iconst_1
istore 1
L41:
aload 3
iload 1
invokevirtual android/os/Parcel/writeInt(I)V
iconst_1
ireturn
L38:
aconst_null
astore 2
goto L39
L40:
iconst_0
istore 1
goto L41
L2:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
aload 2
invokevirtual android/os/Parcel/readStrongBinder()Landroid/os/IBinder;
invokestatic android/support/v4/media/session/b/a(Landroid/os/IBinder;)Landroid/support/v4/media/session/a;
invokevirtual android/support/v4/media/session/e/a(Landroid/support/v4/media/session/a;)V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
L3:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
aload 2
invokevirtual android/os/Parcel/readStrongBinder()Landroid/os/IBinder;
invokestatic android/support/v4/media/session/b/a(Landroid/os/IBinder;)Landroid/support/v4/media/session/a;
invokevirtual android/support/v4/media/session/e/b(Landroid/support/v4/media/session/a;)V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
L4:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual android/support/v4/media/session/e/a()Z
istore 6
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iload 5
istore 1
iload 6
ifeq L42
iconst_1
istore 1
L42:
aload 3
iload 1
invokevirtual android/os/Parcel/writeInt(I)V
iconst_1
ireturn
L5:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual android/support/v4/media/session/e/b()Ljava/lang/String;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
aload 2
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L6:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual android/support/v4/media/session/e/c()Ljava/lang/String;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
aload 2
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L7:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual android/support/v4/media/session/e/d()Landroid/app/PendingIntent;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 2
ifnull L43
aload 3
iconst_1
invokevirtual android/os/Parcel/writeInt(I)V
aload 2
aload 3
iconst_1
invokevirtual android/app/PendingIntent/writeToParcel(Landroid/os/Parcel;I)V
iconst_1
ireturn
L43:
aload 3
iconst_0
invokevirtual android/os/Parcel/writeInt(I)V
iconst_1
ireturn
L8:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual android/support/v4/media/session/e/e()J
lstore 7
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
lload 7
invokevirtual android/os/Parcel/writeLong(J)V
iconst_1
ireturn
L9:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual android/support/v4/media/session/e/f()Landroid/support/v4/media/session/ParcelableVolumeInfo;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 2
ifnull L44
aload 3
iconst_1
invokevirtual android/os/Parcel/writeInt(I)V
aload 2
aload 3
iconst_1
invokevirtual android/support/v4/media/session/ParcelableVolumeInfo/writeToParcel(Landroid/os/Parcel;I)V
iconst_1
ireturn
L44:
aload 3
iconst_0
invokevirtual android/os/Parcel/writeInt(I)V
iconst_1
ireturn
L10:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
aload 2
invokevirtual android/os/Parcel/readInt()I
aload 2
invokevirtual android/os/Parcel/readInt()I
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
invokevirtual android/support/v4/media/session/e/a(IILjava/lang/String;)V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
L11:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
aload 2
invokevirtual android/os/Parcel/readInt()I
aload 2
invokevirtual android/os/Parcel/readInt()I
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
invokevirtual android/support/v4/media/session/e/b(IILjava/lang/String;)V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
L12:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual android/support/v4/media/session/e/g()V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
L13:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
astore 9
aload 2
invokevirtual android/os/Parcel/readInt()I
ifeq L45
getstatic android/os/Bundle/CREATOR Landroid/os/Parcelable$Creator;
aload 2
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast android/os/Bundle
astore 2
L46:
aload 0
aload 9
aload 2
invokevirtual android/support/v4/media/session/e/a(Ljava/lang/String;Landroid/os/Bundle;)V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
L45:
aconst_null
astore 2
goto L46
L14:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
astore 9
aload 2
invokevirtual android/os/Parcel/readInt()I
ifeq L47
getstatic android/os/Bundle/CREATOR Landroid/os/Parcelable$Creator;
aload 2
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast android/os/Bundle
astore 2
L48:
aload 0
aload 9
aload 2
invokevirtual android/support/v4/media/session/e/b(Ljava/lang/String;Landroid/os/Bundle;)V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
L47:
aconst_null
astore 2
goto L48
L15:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 2
invokevirtual android/os/Parcel/readInt()I
ifeq L49
getstatic android/net/Uri/CREATOR Landroid/os/Parcelable$Creator;
aload 2
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast android/net/Uri
astore 9
L50:
aload 2
invokevirtual android/os/Parcel/readInt()I
ifeq L51
getstatic android/os/Bundle/CREATOR Landroid/os/Parcelable$Creator;
aload 2
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast android/os/Bundle
astore 2
L52:
aload 0
aload 9
aload 2
invokevirtual android/support/v4/media/session/e/a(Landroid/net/Uri;Landroid/os/Bundle;)V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
L49:
aconst_null
astore 9
goto L50
L51:
aconst_null
astore 2
goto L52
L16:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
aload 2
invokevirtual android/os/Parcel/readLong()J
invokevirtual android/support/v4/media/session/e/a(J)V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
L17:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual android/support/v4/media/session/e/h()V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
L18:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual android/support/v4/media/session/e/i()V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
L19:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual android/support/v4/media/session/e/j()V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
L20:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual android/support/v4/media/session/e/k()V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
L21:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual android/support/v4/media/session/e/l()V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
L22:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual android/support/v4/media/session/e/m()V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
L23:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
aload 2
invokevirtual android/os/Parcel/readLong()J
invokevirtual android/support/v4/media/session/e/b(J)V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
L24:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 2
invokevirtual android/os/Parcel/readInt()I
ifeq L53
getstatic android/support/v4/media/RatingCompat/CREATOR Landroid/os/Parcelable$Creator;
aload 2
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast android/support/v4/media/RatingCompat
astore 2
L54:
aload 0
aload 2
invokevirtual android/support/v4/media/session/e/a(Landroid/support/v4/media/RatingCompat;)V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
L53:
aconst_null
astore 2
goto L54
L25:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
astore 9
aload 2
invokevirtual android/os/Parcel/readInt()I
ifeq L55
getstatic android/os/Bundle/CREATOR Landroid/os/Parcelable$Creator;
aload 2
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
checkcast android/os/Bundle
astore 2
L56:
aload 0
aload 9
aload 2
invokevirtual android/support/v4/media/session/e/c(Ljava/lang/String;Landroid/os/Bundle;)V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
L55:
aconst_null
astore 2
goto L56
L26:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual android/support/v4/media/session/e/n()Landroid/support/v4/media/MediaMetadataCompat;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 2
ifnull L57
aload 3
iconst_1
invokevirtual android/os/Parcel/writeInt(I)V
aload 2
aload 3
iconst_1
invokevirtual android/support/v4/media/MediaMetadataCompat/writeToParcel(Landroid/os/Parcel;I)V
iconst_1
ireturn
L57:
aload 3
iconst_0
invokevirtual android/os/Parcel/writeInt(I)V
iconst_1
ireturn
L27:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual android/support/v4/media/session/e/o()Landroid/support/v4/media/session/PlaybackStateCompat;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 2
ifnull L58
aload 3
iconst_1
invokevirtual android/os/Parcel/writeInt(I)V
aload 2
aload 3
iconst_1
invokevirtual android/support/v4/media/session/PlaybackStateCompat/writeToParcel(Landroid/os/Parcel;I)V
iconst_1
ireturn
L58:
aload 3
iconst_0
invokevirtual android/os/Parcel/writeInt(I)V
iconst_1
ireturn
L28:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual android/support/v4/media/session/e/p()Ljava/util/List;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
aload 2
invokevirtual android/os/Parcel/writeTypedList(Ljava/util/List;)V
iconst_1
ireturn
L29:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual android/support/v4/media/session/e/q()Ljava/lang/CharSequence;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 2
ifnull L59
aload 3
iconst_1
invokevirtual android/os/Parcel/writeInt(I)V
aload 2
aload 3
iconst_1
invokestatic android/text/TextUtils/writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V
iconst_1
ireturn
L59:
aload 3
iconst_0
invokevirtual android/os/Parcel/writeInt(I)V
iconst_1
ireturn
L30:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual android/support/v4/media/session/e/r()Landroid/os/Bundle;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 2
ifnull L60
aload 3
iconst_1
invokevirtual android/os/Parcel/writeInt(I)V
aload 2
aload 3
iconst_1
invokevirtual android/os/Bundle/writeToParcel(Landroid/os/Parcel;I)V
iconst_1
ireturn
L60:
aload 3
iconst_0
invokevirtual android/os/Parcel/writeInt(I)V
iconst_1
ireturn
L31:
aload 2
ldc "android.support.v4.media.session.IMediaSession"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual android/support/v4/media/session/e/s()I
istore 1
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
iload 1
invokevirtual android/os/Parcel/writeInt(I)V
iconst_1
ireturn
.limit locals 11
.limit stack 5
.end method
