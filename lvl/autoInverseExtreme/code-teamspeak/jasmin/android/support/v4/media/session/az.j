.bytecode 50.0
.class final synchronized android/support/v4/media/session/az
.super java/lang/Object

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Object;
new android/media/session/MediaSession
dup
aload 0
aload 1
invokespecial android/media/session/MediaSession/<init>(Landroid/content/Context;Ljava/lang/String;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static a(Landroid/support/v4/media/session/ba;)Ljava/lang/Object;
new android/support/v4/media/session/bb
dup
aload 0
invokespecial android/support/v4/media/session/bb/<init>(Landroid/support/v4/media/session/ba;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
instanceof android/media/session/MediaSession
ifeq L0
aload 0
areturn
L0:
new java/lang/IllegalArgumentException
dup
ldc "mediaSession is not a valid MediaSession object"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method private static a(Ljava/lang/Object;I)V
aload 0
checkcast android/media/session/MediaSession
iload 1
invokevirtual android/media/session/MediaSession/setFlags(I)V
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;Landroid/app/PendingIntent;)V
aload 0
checkcast android/media/session/MediaSession
aload 1
invokevirtual android/media/session/MediaSession/setSessionActivity(Landroid/app/PendingIntent;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;Landroid/os/Bundle;)V
aload 0
checkcast android/media/session/MediaSession
aload 1
invokevirtual android/media/session/MediaSession/setExtras(Landroid/os/Bundle;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/CharSequence;)V
aload 0
checkcast android/media/session/MediaSession
aload 1
invokevirtual android/media/session/MediaSession/setQueueTitle(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;)V
aload 0
checkcast android/media/session/MediaSession
aload 1
checkcast android/media/VolumeProvider
invokevirtual android/media/session/MediaSession/setPlaybackToRemote(Landroid/media/VolumeProvider;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Landroid/os/Handler;)V
aload 0
checkcast android/media/session/MediaSession
aload 1
checkcast android/media/session/MediaSession$Callback
aload 2
invokevirtual android/media/session/MediaSession/setCallback(Landroid/media/session/MediaSession$Callback;Landroid/os/Handler;)V
return
.limit locals 3
.limit stack 3
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/String;Landroid/os/Bundle;)V
aload 0
checkcast android/media/session/MediaSession
aload 1
aload 2
invokevirtual android/media/session/MediaSession/sendSessionEvent(Ljava/lang/String;Landroid/os/Bundle;)V
return
.limit locals 3
.limit stack 3
.end method

.method public static a(Ljava/lang/Object;Ljava/util/List;)V
aload 1
ifnonnull L0
aload 0
checkcast android/media/session/MediaSession
aconst_null
invokevirtual android/media/session/MediaSession/setQueue(Ljava/util/List;)V
return
L0:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 2
aload 1
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 1
L1:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 2
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/media/session/MediaSession$QueueItem
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
goto L1
L2:
aload 0
checkcast android/media/session/MediaSession
aload 2
invokevirtual android/media/session/MediaSession/setQueue(Ljava/util/List;)V
return
.limit locals 3
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;Z)V
aload 0
checkcast android/media/session/MediaSession
iload 1
invokevirtual android/media/session/MediaSession/setActive(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method private static b(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
instanceof android/media/session/MediaSession$Token
ifeq L0
aload 0
areturn
L0:
new java/lang/IllegalArgumentException
dup
ldc "token is not a valid MediaSession.Token object"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method private static b(Ljava/lang/Object;I)V
new android/media/AudioAttributes$Builder
dup
invokespecial android/media/AudioAttributes$Builder/<init>()V
astore 2
aload 2
iload 1
invokevirtual android/media/AudioAttributes$Builder/setLegacyStreamType(I)Landroid/media/AudioAttributes$Builder;
pop
aload 0
checkcast android/media/session/MediaSession
aload 2
invokevirtual android/media/AudioAttributes$Builder/build()Landroid/media/AudioAttributes;
invokevirtual android/media/session/MediaSession/setPlaybackToLocal(Landroid/media/AudioAttributes;)V
return
.limit locals 3
.limit stack 2
.end method

.method private static b(Ljava/lang/Object;Landroid/app/PendingIntent;)V
aload 0
checkcast android/media/session/MediaSession
aload 1
invokevirtual android/media/session/MediaSession/setMediaButtonReceiver(Landroid/app/PendingIntent;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static b(Ljava/lang/Object;Ljava/lang/Object;)V
aload 0
checkcast android/media/session/MediaSession
aload 1
checkcast android/media/session/PlaybackState
invokevirtual android/media/session/MediaSession/setPlaybackState(Landroid/media/session/PlaybackState;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static c(Ljava/lang/Object;Ljava/lang/Object;)V
aload 0
checkcast android/media/session/MediaSession
aload 1
checkcast android/media/MediaMetadata
invokevirtual android/media/session/MediaSession/setMetadata(Landroid/media/MediaMetadata;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static c(Ljava/lang/Object;)Z
aload 0
checkcast android/media/session/MediaSession
invokevirtual android/media/session/MediaSession/isActive()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static d(Ljava/lang/Object;)V
aload 0
checkcast android/media/session/MediaSession
invokevirtual android/media/session/MediaSession/release()V
return
.limit locals 1
.limit stack 1
.end method

.method private static e(Ljava/lang/Object;)Landroid/os/Parcelable;
aload 0
checkcast android/media/session/MediaSession
invokevirtual android/media/session/MediaSession/getSessionToken()Landroid/media/session/MediaSession$Token;
areturn
.limit locals 1
.limit stack 1
.end method
