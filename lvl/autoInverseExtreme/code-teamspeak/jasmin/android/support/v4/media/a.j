.bytecode 50.0
.class final synchronized android/support/v4/media/a
.super java/lang/Object
.implements android/os/Parcelable$Creator

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/os/Parcel;)Landroid/support/v4/media/MediaDescriptionCompat;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmpge L0
new android/support/v4/media/MediaDescriptionCompat
dup
aload 0
iconst_0
invokespecial android/support/v4/media/MediaDescriptionCompat/<init>(Landroid/os/Parcel;B)V
areturn
L0:
getstatic android/media/MediaDescription/CREATOR Landroid/os/Parcelable$Creator;
aload 0
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
invokestatic android/support/v4/media/MediaDescriptionCompat/a(Ljava/lang/Object;)Landroid/support/v4/media/MediaDescriptionCompat;
areturn
.limit locals 1
.limit stack 4
.end method

.method private static a(I)[Landroid/support/v4/media/MediaDescriptionCompat;
iload 0
anewarray android/support/v4/media/MediaDescriptionCompat
areturn
.limit locals 1
.limit stack 1
.end method

.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmpge L0
new android/support/v4/media/MediaDescriptionCompat
dup
aload 1
iconst_0
invokespecial android/support/v4/media/MediaDescriptionCompat/<init>(Landroid/os/Parcel;B)V
areturn
L0:
getstatic android/media/MediaDescription/CREATOR Landroid/os/Parcelable$Creator;
aload 1
invokeinterface android/os/Parcelable$Creator/createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object; 1
invokestatic android/support/v4/media/MediaDescriptionCompat/a(Ljava/lang/Object;)Landroid/support/v4/media/MediaDescriptionCompat;
areturn
.limit locals 2
.limit stack 4
.end method

.method public final volatile synthetic newArray(I)[Ljava/lang/Object;
iload 1
anewarray android/support/v4/media/MediaDescriptionCompat
areturn
.limit locals 2
.limit stack 1
.end method
