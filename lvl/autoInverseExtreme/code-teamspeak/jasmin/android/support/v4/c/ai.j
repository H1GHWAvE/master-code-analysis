.bytecode 50.0
.class synchronized abstract android/support/v4/c/ai
.super java/lang/Object

.field private static final 'a' Ljava/lang/String; = "AsyncTask"

.field private static final 'b' I = 5


.field private static final 'c' I = 128


.field public static final 'd' Ljava/util/concurrent/Executor;

.field private static final 'f' I = 1


.field private static final 'g' Ljava/util/concurrent/ThreadFactory;

.field private static final 'h' Ljava/util/concurrent/BlockingQueue;

.field private static final 'i' I = 1


.field private static final 'j' I = 2


.field private static 'k' Landroid/support/v4/c/ao;

.field private static volatile 'l' Ljava/util/concurrent/Executor;

.field final 'e' Ljava/util/concurrent/FutureTask;

.field private final 'm' Landroid/support/v4/c/aq;

.field private volatile 'n' I

.field private final 'o' Ljava/util/concurrent/atomic/AtomicBoolean;

.method static <clinit>()V
new android/support/v4/c/aj
dup
invokespecial android/support/v4/c/aj/<init>()V
putstatic android/support/v4/c/ai/g Ljava/util/concurrent/ThreadFactory;
new java/util/concurrent/LinkedBlockingQueue
dup
bipush 10
invokespecial java/util/concurrent/LinkedBlockingQueue/<init>(I)V
putstatic android/support/v4/c/ai/h Ljava/util/concurrent/BlockingQueue;
new java/util/concurrent/ThreadPoolExecutor
dup
iconst_5
sipush 128
lconst_1
getstatic java/util/concurrent/TimeUnit/SECONDS Ljava/util/concurrent/TimeUnit;
getstatic android/support/v4/c/ai/h Ljava/util/concurrent/BlockingQueue;
getstatic android/support/v4/c/ai/g Ljava/util/concurrent/ThreadFactory;
invokespecial java/util/concurrent/ThreadPoolExecutor/<init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V
astore 0
aload 0
putstatic android/support/v4/c/ai/d Ljava/util/concurrent/Executor;
aload 0
putstatic android/support/v4/c/ai/l Ljava/util/concurrent/Executor;
return
.limit locals 1
.limit stack 9
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
getstatic android/support/v4/c/ap/a I
putfield android/support/v4/c/ai/n I
aload 0
new java/util/concurrent/atomic/AtomicBoolean
dup
invokespecial java/util/concurrent/atomic/AtomicBoolean/<init>()V
putfield android/support/v4/c/ai/o Ljava/util/concurrent/atomic/AtomicBoolean;
aload 0
new android/support/v4/c/ak
dup
aload 0
invokespecial android/support/v4/c/ak/<init>(Landroid/support/v4/c/ai;)V
putfield android/support/v4/c/ai/m Landroid/support/v4/c/aq;
aload 0
new android/support/v4/c/al
dup
aload 0
aload 0
getfield android/support/v4/c/ai/m Landroid/support/v4/c/aq;
invokespecial android/support/v4/c/al/<init>(Landroid/support/v4/c/ai;Ljava/util/concurrent/Callable;)V
putfield android/support/v4/c/ai/e Ljava/util/concurrent/FutureTask;
return
.limit locals 1
.limit stack 5
.end method

.method private transient a([Ljava/lang/Object;)Landroid/support/v4/c/ai;
aload 0
getstatic android/support/v4/c/ai/l Ljava/util/concurrent/Executor;
aload 1
invokevirtual android/support/v4/c/ai/a(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/support/v4/c/ai;
areturn
.limit locals 2
.limit stack 3
.end method

.method private a(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
aload 0
getfield android/support/v4/c/ai/e Ljava/util/concurrent/FutureTask;
lload 1
aload 3
invokevirtual java/util/concurrent/FutureTask/get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
areturn
.limit locals 4
.limit stack 4
.end method

.method static synthetic a(Landroid/support/v4/c/ai;Ljava/lang/Object;)Ljava/lang/Object;
aload 0
aload 1
invokespecial android/support/v4/c/ai/d(Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Landroid/support/v4/c/ai;)Ljava/util/concurrent/atomic/AtomicBoolean;
aload 0
getfield android/support/v4/c/ai/o Ljava/util/concurrent/atomic/AtomicBoolean;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/Runnable;)V
getstatic android/support/v4/c/ai/l Ljava/util/concurrent/Executor;
aload 0
invokeinterface java/util/concurrent/Executor/execute(Ljava/lang/Runnable;)V 1
return
.limit locals 1
.limit stack 2
.end method

.method private static a(Ljava/util/concurrent/Executor;)V
aload 0
putstatic android/support/v4/c/ai/l Ljava/util/concurrent/Executor;
return
.limit locals 1
.limit stack 1
.end method

.method protected static transient b()V
return
.limit locals 0
.limit stack 0
.end method

.method static synthetic b(Landroid/support/v4/c/ai;Ljava/lang/Object;)V
aload 0
getfield android/support/v4/c/ai/o Ljava/util/concurrent/atomic/AtomicBoolean;
invokevirtual java/util/concurrent/atomic/AtomicBoolean/get()Z
ifne L0
aload 0
aload 1
invokespecial android/support/v4/c/ai/d(Ljava/lang/Object;)Ljava/lang/Object;
pop
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private transient b([Ljava/lang/Object;)V
aload 0
getfield android/support/v4/c/ai/e Ljava/util/concurrent/FutureTask;
invokevirtual java/util/concurrent/FutureTask/isCancelled()Z
ifne L0
invokestatic android/support/v4/c/ai/c()Landroid/os/Handler;
iconst_2
new android/support/v4/c/an
dup
aload 0
aload 1
invokespecial android/support/v4/c/an/<init>(Landroid/support/v4/c/ai;[Ljava/lang/Object;)V
invokevirtual android/os/Handler/obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
invokevirtual android/os/Message/sendToTarget()V
L0:
return
.limit locals 2
.limit stack 6
.end method

.method private static c()Landroid/os/Handler;
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
ldc android/support/v4/c/ai
monitorenter
L0:
getstatic android/support/v4/c/ai/k Landroid/support/v4/c/ao;
ifnonnull L1
new android/support/v4/c/ao
dup
invokespecial android/support/v4/c/ao/<init>()V
putstatic android/support/v4/c/ai/k Landroid/support/v4/c/ao;
L1:
getstatic android/support/v4/c/ai/k Landroid/support/v4/c/ao;
astore 0
ldc android/support/v4/c/ai
monitorexit
L3:
aload 0
areturn
L2:
astore 0
L4:
ldc android/support/v4/c/ai
monitorexit
L5:
aload 0
athrow
.limit locals 1
.limit stack 2
.end method

.method static synthetic c(Landroid/support/v4/c/ai;Ljava/lang/Object;)V
aload 0
getfield android/support/v4/c/ai/e Ljava/util/concurrent/FutureTask;
invokevirtual java/util/concurrent/FutureTask/isCancelled()Z
ifeq L0
aload 0
aload 1
invokevirtual android/support/v4/c/ai/b(Ljava/lang/Object;)V
L1:
aload 0
getstatic android/support/v4/c/ap/c I
putfield android/support/v4/c/ai/n I
return
L0:
aload 0
aload 1
invokevirtual android/support/v4/c/ai/a(Ljava/lang/Object;)V
goto L1
.limit locals 2
.limit stack 2
.end method

.method private c(Ljava/lang/Object;)V
aload 0
getfield android/support/v4/c/ai/o Ljava/util/concurrent/atomic/AtomicBoolean;
invokevirtual java/util/concurrent/atomic/AtomicBoolean/get()Z
ifne L0
aload 0
aload 1
invokespecial android/support/v4/c/ai/d(Ljava/lang/Object;)Ljava/lang/Object;
pop
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private d()I
aload 0
getfield android/support/v4/c/ai/n I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d(Ljava/lang/Object;)Ljava/lang/Object;
invokestatic android/support/v4/c/ai/c()Landroid/os/Handler;
iconst_1
new android/support/v4/c/an
dup
aload 0
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
invokespecial android/support/v4/c/an/<init>(Landroid/support/v4/c/ai;[Ljava/lang/Object;)V
invokevirtual android/os/Handler/obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
invokevirtual android/os/Message/sendToTarget()V
aload 1
areturn
.limit locals 2
.limit stack 9
.end method

.method private static e()V
return
.limit locals 0
.limit stack 0
.end method

.method private e(Ljava/lang/Object;)V
aload 0
getfield android/support/v4/c/ai/e Ljava/util/concurrent/FutureTask;
invokevirtual java/util/concurrent/FutureTask/isCancelled()Z
ifeq L0
aload 0
aload 1
invokevirtual android/support/v4/c/ai/b(Ljava/lang/Object;)V
L1:
aload 0
getstatic android/support/v4/c/ap/c I
putfield android/support/v4/c/ai/n I
return
L0:
aload 0
aload 1
invokevirtual android/support/v4/c/ai/a(Ljava/lang/Object;)V
goto L1
.limit locals 2
.limit stack 2
.end method

.method private static f()V
return
.limit locals 0
.limit stack 0
.end method

.method private g()Z
aload 0
getfield android/support/v4/c/ai/e Ljava/util/concurrent/FutureTask;
invokevirtual java/util/concurrent/FutureTask/isCancelled()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private h()Z
aload 0
getfield android/support/v4/c/ai/e Ljava/util/concurrent/FutureTask;
iconst_0
invokevirtual java/util/concurrent/FutureTask/cancel(Z)Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method private i()Ljava/lang/Object;
aload 0
getfield android/support/v4/c/ai/e Ljava/util/concurrent/FutureTask;
invokevirtual java/util/concurrent/FutureTask/get()Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final transient a(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/support/v4/c/ai;
aload 0
getfield android/support/v4/c/ai/n I
getstatic android/support/v4/c/ap/a I
if_icmpeq L0
getstatic android/support/v4/c/am/a [I
aload 0
getfield android/support/v4/c/ai/n I
iconst_1
isub
iaload
tableswitch 1
L1
L2
default : L0
L0:
aload 0
getstatic android/support/v4/c/ap/b I
putfield android/support/v4/c/ai/n I
aload 0
getfield android/support/v4/c/ai/m Landroid/support/v4/c/aq;
aload 2
putfield android/support/v4/c/aq/b [Ljava/lang/Object;
aload 1
aload 0
getfield android/support/v4/c/ai/e Ljava/util/concurrent/FutureTask;
invokeinterface java/util/concurrent/Executor/execute(Ljava/lang/Runnable;)V 1
aload 0
areturn
L1:
new java/lang/IllegalStateException
dup
ldc "Cannot execute task: the task is already running."
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L2:
new java/lang/IllegalStateException
dup
ldc "Cannot execute task: the task has already been executed (a task can be executed only once)"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method protected transient abstract a()Ljava/lang/Object;
.end method

.method protected a(Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 0
.end method

.method protected b(Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 0
.end method
