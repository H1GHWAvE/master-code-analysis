.bytecode 50.0
.class public synchronized android/support/v4/c/aa
.super java/lang/Object

.field public 'p' I

.field public 'q' Landroid/support/v4/c/ad;

.field public 'r' Landroid/support/v4/c/ac;

.field 's' Landroid/content/Context;

.field 't' Z

.field public 'u' Z

.field 'v' Z

.field 'w' Z

.field 'x' Z

.method public <init>(Landroid/content/Context;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield android/support/v4/c/aa/t Z
aload 0
iconst_0
putfield android/support/v4/c/aa/u Z
aload 0
iconst_1
putfield android/support/v4/c/aa/v Z
aload 0
iconst_0
putfield android/support/v4/c/aa/w Z
aload 0
iconst_0
putfield android/support/v4/c/aa/x Z
aload 0
aload 1
invokevirtual android/content/Context/getApplicationContext()Landroid/content/Context;
putfield android/support/v4/c/aa/s Landroid/content/Context;
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;)Ljava/lang/String;
new java/lang/StringBuilder
dup
bipush 64
invokespecial java/lang/StringBuilder/<init>(I)V
astore 1
aload 0
aload 1
invokestatic android/support/v4/n/g/a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V
aload 1
ldc "}"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 3
.end method

.method private a(ILandroid/support/v4/c/ad;)V
aload 0
getfield android/support/v4/c/aa/q Landroid/support/v4/c/ad;
ifnull L0
new java/lang/IllegalStateException
dup
ldc "There is already a listener registered"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 2
putfield android/support/v4/c/aa/q Landroid/support/v4/c/ad;
aload 0
iload 1
putfield android/support/v4/c/aa/p I
return
.limit locals 3
.limit stack 3
.end method

.method private b(Landroid/support/v4/c/ac;)V
aload 0
getfield android/support/v4/c/aa/r Landroid/support/v4/c/ac;
ifnull L0
new java/lang/IllegalStateException
dup
ldc "There is already a listener registered"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
putfield android/support/v4/c/aa/r Landroid/support/v4/c/ac;
return
.limit locals 2
.limit stack 3
.end method

.method private c()V
aload 0
getfield android/support/v4/c/aa/r Landroid/support/v4/c/ac;
ifnull L0
aload 0
getfield android/support/v4/c/aa/r Landroid/support/v4/c/ac;
invokeinterface android/support/v4/c/ac/d()V 0
L0:
return
.limit locals 1
.limit stack 1
.end method

.method private d()Landroid/content/Context;
aload 0
getfield android/support/v4/c/aa/s Landroid/content/Context;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e()I
aload 0
getfield android/support/v4/c/aa/p I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private o()Z
aload 0
getfield android/support/v4/c/aa/t Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private p()Z
aload 0
getfield android/support/v4/c/aa/u Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private q()Z
aload 0
getfield android/support/v4/c/aa/v Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private r()V
aload 0
iconst_1
putfield android/support/v4/c/aa/u Z
return
.limit locals 1
.limit stack 2
.end method

.method private static s()V
return
.limit locals 0
.limit stack 0
.end method

.method private t()Z
aload 0
getfield android/support/v4/c/aa/w Z
istore 1
aload 0
iconst_0
putfield android/support/v4/c/aa/w Z
aload 0
aload 0
getfield android/support/v4/c/aa/x Z
iload 1
ior
putfield android/support/v4/c/aa/x Z
iload 1
ireturn
.limit locals 2
.limit stack 3
.end method

.method private u()V
aload 0
iconst_0
putfield android/support/v4/c/aa/x Z
return
.limit locals 1
.limit stack 2
.end method

.method private v()V
aload 0
getfield android/support/v4/c/aa/x Z
ifeq L0
aload 0
iconst_1
putfield android/support/v4/c/aa/w Z
L0:
return
.limit locals 1
.limit stack 2
.end method

.method protected a()V
return
.limit locals 1
.limit stack 0
.end method

.method public final a(Landroid/support/v4/c/ac;)V
aload 0
getfield android/support/v4/c/aa/r Landroid/support/v4/c/ac;
ifnonnull L0
new java/lang/IllegalStateException
dup
ldc "No listener register"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/c/aa/r Landroid/support/v4/c/ac;
aload 1
if_acmpeq L1
new java/lang/IllegalArgumentException
dup
ldc "Attempting to unregister the wrong listener"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
aconst_null
putfield android/support/v4/c/aa/r Landroid/support/v4/c/ac;
return
.limit locals 2
.limit stack 3
.end method

.method public final a(Landroid/support/v4/c/ad;)V
aload 0
getfield android/support/v4/c/aa/q Landroid/support/v4/c/ad;
ifnonnull L0
new java/lang/IllegalStateException
dup
ldc "No listener register"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/c/aa/q Landroid/support/v4/c/ad;
aload 1
if_acmpeq L1
new java/lang/IllegalArgumentException
dup
ldc "Attempting to unregister the wrong listener"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
aconst_null
putfield android/support/v4/c/aa/q Landroid/support/v4/c/ad;
return
.limit locals 2
.limit stack 3
.end method

.method public a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mId="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/c/aa/p I
invokevirtual java/io/PrintWriter/print(I)V
aload 3
ldc " mListener="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/c/aa/q Landroid/support/v4/c/ad;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
aload 0
getfield android/support/v4/c/aa/t Z
ifne L0
aload 0
getfield android/support/v4/c/aa/w Z
ifne L0
aload 0
getfield android/support/v4/c/aa/x Z
ifeq L1
L0:
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mStarted="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/c/aa/t Z
invokevirtual java/io/PrintWriter/print(Z)V
aload 3
ldc " mContentChanged="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/c/aa/w Z
invokevirtual java/io/PrintWriter/print(Z)V
aload 3
ldc " mProcessingChange="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/c/aa/x Z
invokevirtual java/io/PrintWriter/println(Z)V
L1:
aload 0
getfield android/support/v4/c/aa/u Z
ifne L2
aload 0
getfield android/support/v4/c/aa/v Z
ifeq L3
L2:
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mAbandoned="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/c/aa/u Z
invokevirtual java/io/PrintWriter/print(Z)V
aload 3
ldc " mReset="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/c/aa/v Z
invokevirtual java/io/PrintWriter/println(Z)V
L3:
return
.limit locals 5
.limit stack 2
.end method

.method public b(Ljava/lang/Object;)V
aload 0
getfield android/support/v4/c/aa/q Landroid/support/v4/c/ad;
ifnull L0
aload 0
getfield android/support/v4/c/aa/q Landroid/support/v4/c/ad;
aload 0
aload 1
invokeinterface android/support/v4/c/ad/a(Landroid/support/v4/c/aa;Ljava/lang/Object;)V 2
L0:
return
.limit locals 2
.limit stack 3
.end method

.method protected b()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method protected f()V
return
.limit locals 1
.limit stack 0
.end method

.method protected g()V
return
.limit locals 1
.limit stack 0
.end method

.method protected h()V
return
.limit locals 1
.limit stack 0
.end method

.method public final i()V
aload 0
iconst_1
putfield android/support/v4/c/aa/t Z
aload 0
iconst_0
putfield android/support/v4/c/aa/v Z
aload 0
iconst_0
putfield android/support/v4/c/aa/u Z
aload 0
invokevirtual android/support/v4/c/aa/f()V
return
.limit locals 1
.limit stack 2
.end method

.method public final j()Z
aload 0
invokevirtual android/support/v4/c/aa/b()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final k()V
aload 0
invokevirtual android/support/v4/c/aa/a()V
return
.limit locals 1
.limit stack 1
.end method

.method public final l()V
aload 0
iconst_0
putfield android/support/v4/c/aa/t Z
aload 0
invokevirtual android/support/v4/c/aa/g()V
return
.limit locals 1
.limit stack 2
.end method

.method public final m()V
aload 0
invokevirtual android/support/v4/c/aa/h()V
aload 0
iconst_1
putfield android/support/v4/c/aa/v Z
aload 0
iconst_0
putfield android/support/v4/c/aa/t Z
aload 0
iconst_0
putfield android/support/v4/c/aa/u Z
aload 0
iconst_0
putfield android/support/v4/c/aa/w Z
aload 0
iconst_0
putfield android/support/v4/c/aa/x Z
return
.limit locals 1
.limit stack 2
.end method

.method public final n()V
aload 0
getfield android/support/v4/c/aa/t Z
ifeq L0
aload 0
invokevirtual android/support/v4/c/aa/a()V
return
L0:
aload 0
iconst_1
putfield android/support/v4/c/aa/w Z
return
.limit locals 1
.limit stack 2
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
bipush 64
invokespecial java/lang/StringBuilder/<init>(I)V
astore 1
aload 0
aload 1
invokestatic android/support/v4/n/g/a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V
aload 1
ldc " id="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
aload 0
getfield android/support/v4/c/aa/p I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
pop
aload 1
ldc "}"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 3
.end method
