.bytecode 50.0
.class public final synchronized android/support/v4/c/ae
.super java/lang/Object

.field static final 'a' I = 1


.field private static final 'b' Ljava/lang/String; = "LocalBroadcastManager"

.field private static final 'c' Z = 0


.field private static final 'i' Ljava/lang/Object;

.field private static 'j' Landroid/support/v4/c/ae;

.field private final 'd' Landroid/content/Context;

.field private final 'e' Ljava/util/HashMap;

.field private final 'f' Ljava/util/HashMap;

.field private final 'g' Ljava/util/ArrayList;

.field private final 'h' Landroid/os/Handler;

.method static <clinit>()V
new java/lang/Object
dup
invokespecial java/lang/Object/<init>()V
putstatic android/support/v4/c/ae/i Ljava/lang/Object;
return
.limit locals 0
.limit stack 2
.end method

.method private <init>(Landroid/content/Context;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield android/support/v4/c/ae/e Ljava/util/HashMap;
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield android/support/v4/c/ae/f Ljava/util/HashMap;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/c/ae/g Ljava/util/ArrayList;
aload 0
aload 1
putfield android/support/v4/c/ae/d Landroid/content/Context;
aload 0
new android/support/v4/c/af
dup
aload 0
aload 1
invokevirtual android/content/Context/getMainLooper()Landroid/os/Looper;
invokespecial android/support/v4/c/af/<init>(Landroid/support/v4/c/ae;Landroid/os/Looper;)V
putfield android/support/v4/c/ae/h Landroid/os/Handler;
return
.limit locals 2
.limit stack 5
.end method

.method private static a(Landroid/content/Context;)Landroid/support/v4/c/ae;
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
getstatic android/support/v4/c/ae/i Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
getstatic android/support/v4/c/ae/j Landroid/support/v4/c/ae;
ifnonnull L1
new android/support/v4/c/ae
dup
aload 0
invokevirtual android/content/Context/getApplicationContext()Landroid/content/Context;
invokespecial android/support/v4/c/ae/<init>(Landroid/content/Context;)V
putstatic android/support/v4/c/ae/j Landroid/support/v4/c/ae;
L1:
getstatic android/support/v4/c/ae/j Landroid/support/v4/c/ae;
astore 0
aload 1
monitorexit
L3:
aload 0
areturn
L2:
astore 0
L4:
aload 1
monitorexit
L5:
aload 0
athrow
.limit locals 2
.limit stack 3
.end method

.method private a()V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
L9:
aload 0
getfield android/support/v4/c/ae/e Ljava/util/HashMap;
astore 3
aload 3
monitorenter
L0:
aload 0
getfield android/support/v4/c/ae/g Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 1
L1:
iload 1
ifgt L5
L3:
aload 3
monitorexit
L4:
return
L5:
iload 1
anewarray android/support/v4/c/ag
astore 4
aload 0
getfield android/support/v4/c/ae/g Ljava/util/ArrayList;
aload 4
invokevirtual java/util/ArrayList/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
pop
aload 0
getfield android/support/v4/c/ae/g Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
aload 3
monitorexit
L6:
iconst_0
istore 1
L10:
iload 1
aload 4
arraylength
if_icmpge L9
aload 4
iload 1
aaload
astore 3
iconst_0
istore 2
L11:
iload 2
aload 3
getfield android/support/v4/c/ag/b Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L12
aload 3
getfield android/support/v4/c/ag/b Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/c/ah
getfield android/support/v4/c/ah/b Landroid/content/BroadcastReceiver;
aload 0
getfield android/support/v4/c/ae/d Landroid/content/Context;
aload 3
getfield android/support/v4/c/ag/a Landroid/content/Intent;
invokevirtual android/content/BroadcastReceiver/onReceive(Landroid/content/Context;Landroid/content/Intent;)V
iload 2
iconst_1
iadd
istore 2
goto L11
L2:
astore 4
L7:
aload 3
monitorexit
L8:
aload 4
athrow
L12:
iload 1
iconst_1
iadd
istore 1
goto L10
.limit locals 5
.limit stack 3
.end method

.method private a(Landroid/content/BroadcastReceiver;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
.catch all from L9 to L10 using L2
.catch all from L11 to L12 using L2
.catch all from L13 to L14 using L2
.catch all from L15 to L16 using L2
aload 0
getfield android/support/v4/c/ae/e Ljava/util/HashMap;
astore 5
aload 5
monitorenter
L0:
aload 0
getfield android/support/v4/c/ae/e Ljava/util/HashMap;
aload 1
invokevirtual java/util/HashMap/remove(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/ArrayList
astore 6
L1:
aload 6
ifnonnull L17
L3:
aload 5
monitorexit
L4:
return
L5:
iload 3
aload 6
invokevirtual java/util/ArrayList/size()I
if_icmpge L13
aload 6
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/content/IntentFilter
astore 7
L6:
iconst_0
istore 4
L7:
iload 4
aload 7
invokevirtual android/content/IntentFilter/countActions()I
if_icmpge L18
aload 7
iload 4
invokevirtual android/content/IntentFilter/getAction(I)Ljava/lang/String;
astore 8
aload 0
getfield android/support/v4/c/ae/f Ljava/util/HashMap;
aload 8
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/ArrayList
astore 9
L8:
aload 9
ifnull L19
iconst_0
istore 2
L9:
iload 2
aload 9
invokevirtual java/util/ArrayList/size()I
if_icmpge L11
aload 9
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/c/ah
getfield android/support/v4/c/ah/b Landroid/content/BroadcastReceiver;
aload 1
if_acmpne L20
aload 9
iload 2
invokevirtual java/util/ArrayList/remove(I)Ljava/lang/Object;
pop
L10:
iload 2
iconst_1
isub
istore 2
goto L21
L11:
aload 9
invokevirtual java/util/ArrayList/size()I
ifgt L19
aload 0
getfield android/support/v4/c/ae/f Ljava/util/HashMap;
aload 8
invokevirtual java/util/HashMap/remove(Ljava/lang/Object;)Ljava/lang/Object;
pop
L12:
goto L19
L13:
aload 5
monitorexit
L14:
return
L2:
astore 1
L15:
aload 5
monitorexit
L16:
aload 1
athrow
L20:
goto L21
L17:
iconst_0
istore 3
goto L5
L21:
iload 2
iconst_1
iadd
istore 2
goto L9
L19:
iload 4
iconst_1
iadd
istore 4
goto L7
L18:
iload 3
iconst_1
iadd
istore 3
goto L5
.limit locals 10
.limit stack 2
.end method

.method private a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
.catch all from L8 to L9 using L2
.catch all from L9 to L10 using L2
.catch all from L11 to L12 using L2
.catch all from L13 to L14 using L2
aload 0
getfield android/support/v4/c/ae/e Ljava/util/HashMap;
astore 6
aload 6
monitorenter
L0:
new android/support/v4/c/ah
dup
aload 2
aload 1
invokespecial android/support/v4/c/ah/<init>(Landroid/content/IntentFilter;Landroid/content/BroadcastReceiver;)V
astore 7
aload 0
getfield android/support/v4/c/ae/e Ljava/util/HashMap;
aload 1
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/ArrayList
astore 5
L1:
aload 5
astore 4
aload 5
ifnonnull L4
L3:
new java/util/ArrayList
dup
iconst_1
invokespecial java/util/ArrayList/<init>(I)V
astore 4
aload 0
getfield android/support/v4/c/ae/e Ljava/util/HashMap;
aload 1
aload 4
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L4:
aload 4
aload 2
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L5:
iconst_0
istore 3
L6:
iload 3
aload 2
invokevirtual android/content/IntentFilter/countActions()I
if_icmpge L11
aload 2
iload 3
invokevirtual android/content/IntentFilter/getAction(I)Ljava/lang/String;
astore 5
aload 0
getfield android/support/v4/c/ae/f Ljava/util/HashMap;
aload 5
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/ArrayList
astore 4
L7:
aload 4
astore 1
aload 4
ifnonnull L9
L8:
new java/util/ArrayList
dup
iconst_1
invokespecial java/util/ArrayList/<init>(I)V
astore 1
aload 0
getfield android/support/v4/c/ae/f Ljava/util/HashMap;
aload 5
aload 1
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L9:
aload 1
aload 7
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L10:
iload 3
iconst_1
iadd
istore 3
goto L6
L11:
aload 6
monitorexit
L12:
return
L2:
astore 1
L13:
aload 6
monitorexit
L14:
aload 1
athrow
.limit locals 8
.limit stack 4
.end method

.method static synthetic a(Landroid/support/v4/c/ae;)V
aload 0
invokespecial android/support/v4/c/ae/a()V
return
.limit locals 1
.limit stack 1
.end method

.method private a(Landroid/content/Intent;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
.catch all from L8 to L9 using L2
.catch all from L10 to L11 using L2
.catch all from L11 to L12 using L2
.catch all from L13 to L14 using L2
.catch all from L15 to L16 using L2
.catch all from L17 to L18 using L2
.catch all from L19 to L20 using L2
.catch all from L20 to L21 using L2
.catch all from L22 to L23 using L2
.catch all from L24 to L25 using L2
.catch all from L26 to L27 using L2
.catch all from L28 to L29 using L2
.catch all from L29 to L30 using L2
.catch all from L31 to L32 using L2
aload 0
getfield android/support/v4/c/ae/e Ljava/util/HashMap;
astore 7
aload 7
monitorenter
L0:
aload 1
invokevirtual android/content/Intent/getAction()Ljava/lang/String;
astore 8
aload 1
aload 0
getfield android/support/v4/c/ae/d Landroid/content/Context;
invokevirtual android/content/Context/getContentResolver()Landroid/content/ContentResolver;
invokevirtual android/content/Intent/resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;
astore 9
aload 1
invokevirtual android/content/Intent/getData()Landroid/net/Uri;
astore 10
aload 1
invokevirtual android/content/Intent/getScheme()Ljava/lang/String;
astore 11
aload 1
invokevirtual android/content/Intent/getCategories()Ljava/util/Set;
astore 12
aload 1
invokevirtual android/content/Intent/getFlags()I
bipush 8
iand
ifeq L33
L1:
iconst_1
istore 2
L34:
iload 2
ifeq L4
L3:
ldc "LocalBroadcastManager"
new java/lang/StringBuilder
dup
ldc "Resolving type "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 9
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " scheme "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 11
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " of intent "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L4:
aload 0
getfield android/support/v4/c/ae/f Ljava/util/HashMap;
aload 1
invokevirtual android/content/Intent/getAction()Ljava/lang/String;
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/ArrayList
astore 13
L5:
aload 13
ifnull L31
iload 2
ifeq L35
L6:
ldc "LocalBroadcastManager"
new java/lang/StringBuilder
dup
ldc "Action list: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 13
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L7:
goto L35
L8:
iload 3
aload 13
invokevirtual java/util/ArrayList/size()I
if_icmpge L36
aload 13
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/c/ah
astore 6
L9:
iload 2
ifeq L11
L10:
ldc "LocalBroadcastManager"
new java/lang/StringBuilder
dup
ldc "Matching against filter "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 6
getfield android/support/v4/c/ah/a Landroid/content/IntentFilter;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L11:
aload 6
getfield android/support/v4/c/ah/c Z
ifeq L15
L12:
iload 2
ifeq L37
L13:
ldc "LocalBroadcastManager"
ldc "  Filter's target already added"
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L14:
goto L38
L15:
aload 6
getfield android/support/v4/c/ah/a Landroid/content/IntentFilter;
aload 8
aload 9
aload 11
aload 10
aload 12
ldc "LocalBroadcastManager"
invokevirtual android/content/IntentFilter/match(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/util/Set;Ljava/lang/String;)I
istore 4
L16:
iload 4
iflt L39
iload 2
ifeq L18
L17:
ldc "LocalBroadcastManager"
new java/lang/StringBuilder
dup
ldc "  Filter matched!  match=0x"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 4
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L18:
aload 5
ifnonnull L40
L19:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 5
L20:
aload 5
aload 6
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 6
iconst_1
putfield android/support/v4/c/ah/c Z
L21:
goto L38
L2:
astore 1
L22:
aload 7
monitorexit
L23:
aload 1
athrow
L39:
iload 2
ifeq L37
iload 4
tableswitch -4
L41
L42
L43
L44
default : L45
L45:
ldc "unknown reason"
astore 6
L24:
ldc "LocalBroadcastManager"
new java/lang/StringBuilder
dup
ldc "  Filter did not match: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L25:
goto L37
L26:
iload 2
aload 5
invokevirtual java/util/ArrayList/size()I
if_icmpge L28
aload 5
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/c/ah
iconst_0
putfield android/support/v4/c/ah/c Z
L27:
iload 2
iconst_1
iadd
istore 2
goto L26
L28:
aload 0
getfield android/support/v4/c/ae/g Ljava/util/ArrayList;
new android/support/v4/c/ag
dup
aload 1
aload 5
invokespecial android/support/v4/c/ag/<init>(Landroid/content/Intent;Ljava/util/ArrayList;)V
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
getfield android/support/v4/c/ae/h Landroid/os/Handler;
iconst_1
invokevirtual android/os/Handler/hasMessages(I)Z
ifne L29
aload 0
getfield android/support/v4/c/ae/h Landroid/os/Handler;
iconst_1
invokevirtual android/os/Handler/sendEmptyMessage(I)Z
pop
L29:
aload 7
monitorexit
L30:
iconst_1
ireturn
L31:
aload 7
monitorexit
L32:
iconst_0
ireturn
L40:
goto L20
L35:
aconst_null
astore 5
iconst_0
istore 3
goto L8
L38:
iload 3
iconst_1
iadd
istore 3
goto L8
L33:
iconst_0
istore 2
goto L34
L37:
goto L38
L42:
ldc "action"
astore 6
goto L24
L41:
ldc "category"
astore 6
goto L24
L43:
ldc "data"
astore 6
goto L24
L44:
ldc "type"
astore 6
goto L24
L36:
aload 5
ifnull L31
iconst_0
istore 2
goto L26
.limit locals 14
.limit stack 7
.end method

.method private b(Landroid/content/Intent;)V
aload 0
aload 1
invokespecial android/support/v4/c/ae/a(Landroid/content/Intent;)Z
ifeq L0
aload 0
invokespecial android/support/v4/c/ae/a()V
L0:
return
.limit locals 2
.limit stack 2
.end method
