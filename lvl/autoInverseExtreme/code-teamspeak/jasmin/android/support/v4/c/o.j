.bytecode 50.0
.class public final synchronized android/support/v4/c/o
.super android/support/v4/c/a

.field final 'h' Landroid/support/v4/c/ab;

.field 'i' Landroid/net/Uri;

.field 'j' [Ljava/lang/String;

.field 'k' Ljava/lang/String;

.field 'l' [Ljava/lang/String;

.field 'm' Ljava/lang/String;

.field 'n' Landroid/database/Cursor;

.field 'o' Landroid/support/v4/i/c;

.method private <init>(Landroid/content/Context;)V
aload 0
aload 1
invokespecial android/support/v4/c/a/<init>(Landroid/content/Context;)V
aload 0
new android/support/v4/c/ab
dup
aload 0
invokespecial android/support/v4/c/ab/<init>(Landroid/support/v4/c/aa;)V
putfield android/support/v4/c/o/h Landroid/support/v4/c/ab;
return
.limit locals 2
.limit stack 4
.end method

.method private <init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
aload 0
aload 1
invokespecial android/support/v4/c/a/<init>(Landroid/content/Context;)V
aload 0
new android/support/v4/c/ab
dup
aload 0
invokespecial android/support/v4/c/ab/<init>(Landroid/support/v4/c/aa;)V
putfield android/support/v4/c/o/h Landroid/support/v4/c/ab;
aload 0
aload 2
putfield android/support/v4/c/o/i Landroid/net/Uri;
aload 0
aload 3
putfield android/support/v4/c/o/j [Ljava/lang/String;
aload 0
aload 4
putfield android/support/v4/c/o/k Ljava/lang/String;
aload 0
aload 5
putfield android/support/v4/c/o/l [Ljava/lang/String;
aload 0
aload 6
putfield android/support/v4/c/o/m Ljava/lang/String;
return
.limit locals 7
.limit stack 4
.end method

.method private a(Landroid/database/Cursor;)V
aload 0
getfield android/support/v4/c/aa/v Z
ifeq L0
aload 1
ifnull L1
aload 1
invokeinterface android/database/Cursor/close()V 0
L1:
return
L0:
aload 0
getfield android/support/v4/c/o/n Landroid/database/Cursor;
astore 2
aload 0
aload 1
putfield android/support/v4/c/o/n Landroid/database/Cursor;
aload 0
getfield android/support/v4/c/aa/t Z
ifeq L2
aload 0
aload 1
invokespecial android/support/v4/c/a/b(Ljava/lang/Object;)V
L2:
aload 2
ifnull L1
aload 2
aload 1
if_acmpeq L1
aload 2
invokeinterface android/database/Cursor/isClosed()Z 0
ifne L1
aload 2
invokeinterface android/database/Cursor/close()V 0
return
.limit locals 3
.limit stack 2
.end method

.method private a(Landroid/net/Uri;)V
aload 0
aload 1
putfield android/support/v4/c/o/i Landroid/net/Uri;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/String;)V
aload 0
aload 1
putfield android/support/v4/c/o/k Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method private a([Ljava/lang/String;)V
aload 0
aload 1
putfield android/support/v4/c/o/j [Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method private static b(Landroid/database/Cursor;)V
aload 0
ifnull L0
aload 0
invokeinterface android/database/Cursor/isClosed()Z 0
ifne L0
aload 0
invokeinterface android/database/Cursor/close()V 0
L0:
return
.limit locals 1
.limit stack 1
.end method

.method private b(Ljava/lang/String;)V
aload 0
aload 1
putfield android/support/v4/c/o/m Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method private b([Ljava/lang/String;)V
aload 0
aload 1
putfield android/support/v4/c/o/l [Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method private o()Landroid/database/Cursor;
.catch all from L0 to L1 using L2
.catch all from L3 to L2 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
.catch all from L7 to L8 using L9
.catch java/lang/RuntimeException from L10 to L11 using L12
.catch all from L10 to L11 using L9
.catch all from L13 to L14 using L15
.catch all from L16 to L9 using L9
.catch all from L17 to L18 using L19
.catch all from L20 to L21 using L15
.catch all from L22 to L23 using L19
aload 0
monitorenter
L0:
aload 0
getfield android/support/v4/c/a/d Landroid/support/v4/c/b;
ifnull L24
L1:
iconst_1
istore 1
L25:
iload 1
ifeq L6
L3:
new android/support/v4/i/h
dup
invokespecial android/support/v4/i/h/<init>()V
athrow
L2:
astore 2
L4:
aload 0
monitorexit
L5:
aload 2
athrow
L24:
iconst_0
istore 1
goto L25
L6:
aload 0
new android/support/v4/i/c
dup
invokespecial android/support/v4/i/c/<init>()V
putfield android/support/v4/c/o/o Landroid/support/v4/i/c;
aload 0
monitorexit
L7:
aload 0
getfield android/support/v4/c/aa/s Landroid/content/Context;
invokevirtual android/content/Context/getContentResolver()Landroid/content/ContentResolver;
aload 0
getfield android/support/v4/c/o/i Landroid/net/Uri;
aload 0
getfield android/support/v4/c/o/j [Ljava/lang/String;
aload 0
getfield android/support/v4/c/o/k Ljava/lang/String;
aload 0
getfield android/support/v4/c/o/l [Ljava/lang/String;
aload 0
getfield android/support/v4/c/o/m Ljava/lang/String;
aload 0
getfield android/support/v4/c/o/o Landroid/support/v4/i/c;
invokestatic android/support/v4/c/c/a(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/support/v4/i/c;)Landroid/database/Cursor;
astore 2
L8:
aload 2
ifnull L11
L10:
aload 2
invokeinterface android/database/Cursor/getCount()I 0
pop
aload 2
aload 0
getfield android/support/v4/c/o/h Landroid/support/v4/c/ab;
invokeinterface android/database/Cursor/registerContentObserver(Landroid/database/ContentObserver;)V 1
L11:
aload 0
monitorenter
L13:
aload 0
aconst_null
putfield android/support/v4/c/o/o Landroid/support/v4/i/c;
aload 0
monitorexit
L14:
aload 2
areturn
L12:
astore 3
L16:
aload 2
invokeinterface android/database/Cursor/close()V 0
aload 3
athrow
L9:
astore 2
aload 0
monitorenter
L17:
aload 0
aconst_null
putfield android/support/v4/c/o/o Landroid/support/v4/i/c;
aload 0
monitorexit
L18:
aload 2
athrow
L15:
astore 2
L20:
aload 0
monitorexit
L21:
aload 2
athrow
L19:
astore 2
L22:
aload 0
monitorexit
L23:
aload 2
athrow
.limit locals 4
.limit stack 7
.end method

.method private p()Landroid/net/Uri;
aload 0
getfield android/support/v4/c/o/i Landroid/net/Uri;
areturn
.limit locals 1
.limit stack 1
.end method

.method private q()[Ljava/lang/String;
aload 0
getfield android/support/v4/c/o/j [Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private r()Ljava/lang/String;
aload 0
getfield android/support/v4/c/o/k Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private s()[Ljava/lang/String;
aload 0
getfield android/support/v4/c/o/l [Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private t()Ljava/lang/String;
aload 0
getfield android/support/v4/c/o/m Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final synthetic a(Ljava/lang/Object;)V
aload 1
checkcast android/database/Cursor
astore 1
aload 1
ifnull L0
aload 1
invokeinterface android/database/Cursor/isClosed()Z 0
ifne L0
aload 1
invokeinterface android/database/Cursor/close()V 0
L0:
return
.limit locals 2
.limit stack 1
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
aload 0
aload 1
aload 2
aload 3
aload 4
invokespecial android/support/v4/c/a/a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mUri="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/c/o/i Landroid/net/Uri;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mProjection="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/c/o/j [Ljava/lang/String;
invokestatic java/util/Arrays/toString([Ljava/lang/Object;)Ljava/lang/String;
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mSelection="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/c/o/k Ljava/lang/String;
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mSelectionArgs="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/c/o/l [Ljava/lang/String;
invokestatic java/util/Arrays/toString([Ljava/lang/Object;)Ljava/lang/String;
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mSortOrder="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/c/o/m Ljava/lang/String;
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mCursor="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/c/o/n Landroid/database/Cursor;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mContentChanged="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/c/o/w Z
invokevirtual java/io/PrintWriter/println(Z)V
return
.limit locals 5
.limit stack 5
.end method

.method public final synthetic b(Ljava/lang/Object;)V
aload 0
aload 1
checkcast android/database/Cursor
invokespecial android/support/v4/c/o/a(Landroid/database/Cursor;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final synthetic d()Ljava/lang/Object;
aload 0
invokespecial android/support/v4/c/o/o()Landroid/database/Cursor;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final e()V
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L4
.catch all from L3 to L5 using L2
.catch all from L6 to L7 using L4
.catch all from L8 to L9 using L10
.catch all from L9 to L11 using L2
.catch all from L11 to L12 using L13
.catch all from L14 to L15 using L13
.catch all from L15 to L2 using L2
.catch all from L16 to L17 using L2
.catch all from L18 to L19 using L4
.catch all from L19 to L10 using L2
.catch all from L20 to L21 using L2
.catch all from L21 to L22 using L23
.catch all from L22 to L23 using L2
.catch all from L24 to L25 using L23
.catch all from L25 to L26 using L2
aload 0
invokespecial android/support/v4/c/a/e()V
aload 0
monitorenter
L0:
aload 0
getfield android/support/v4/c/o/o Landroid/support/v4/i/c;
ifnull L3
aload 0
getfield android/support/v4/c/o/o Landroid/support/v4/i/c;
astore 1
aload 1
monitorenter
L1:
aload 1
getfield android/support/v4/i/c/a Z
ifeq L6
aload 1
monitorexit
L3:
aload 0
monitorexit
L5:
return
L6:
aload 1
iconst_1
putfield android/support/v4/i/c/a Z
aload 1
iconst_1
putfield android/support/v4/i/c/c Z
aload 1
getfield android/support/v4/i/c/b Ljava/lang/Object;
astore 2
aload 1
monitorexit
L7:
aload 2
ifnull L9
L8:
aload 2
checkcast android/os/CancellationSignal
invokevirtual android/os/CancellationSignal/cancel()V
L9:
aload 1
monitorenter
L11:
aload 1
iconst_0
putfield android/support/v4/i/c/c Z
aload 1
invokevirtual java/lang/Object/notifyAll()V
aload 1
monitorexit
L12:
goto L3
L13:
astore 2
L14:
aload 1
monitorexit
L15:
aload 2
athrow
L2:
astore 1
L16:
aload 0
monitorexit
L17:
aload 1
athrow
L4:
astore 2
L18:
aload 1
monitorexit
L19:
aload 2
athrow
L10:
astore 2
L20:
aload 1
monitorenter
L21:
aload 1
iconst_0
putfield android/support/v4/i/c/c Z
aload 1
invokevirtual java/lang/Object/notifyAll()V
aload 1
monitorexit
L22:
aload 2
athrow
L23:
astore 2
L24:
aload 1
monitorexit
L25:
aload 2
athrow
L26:
.limit locals 3
.limit stack 2
.end method

.method protected final f()V
aload 0
getfield android/support/v4/c/o/n Landroid/database/Cursor;
ifnull L0
aload 0
aload 0
getfield android/support/v4/c/o/n Landroid/database/Cursor;
invokespecial android/support/v4/c/o/a(Landroid/database/Cursor;)V
L0:
aload 0
getfield android/support/v4/c/aa/w Z
istore 1
aload 0
iconst_0
putfield android/support/v4/c/aa/w Z
aload 0
aload 0
getfield android/support/v4/c/aa/x Z
iload 1
ior
putfield android/support/v4/c/aa/x Z
iload 1
ifne L1
aload 0
getfield android/support/v4/c/o/n Landroid/database/Cursor;
ifnonnull L2
L1:
aload 0
invokevirtual android/support/v4/c/o/k()V
L2:
return
.limit locals 2
.limit stack 3
.end method

.method protected final g()V
aload 0
invokevirtual android/support/v4/c/o/j()Z
pop
return
.limit locals 1
.limit stack 1
.end method

.method protected final h()V
aload 0
invokespecial android/support/v4/c/a/h()V
aload 0
invokevirtual android/support/v4/c/o/j()Z
pop
aload 0
getfield android/support/v4/c/o/n Landroid/database/Cursor;
ifnull L0
aload 0
getfield android/support/v4/c/o/n Landroid/database/Cursor;
invokeinterface android/database/Cursor/isClosed()Z 0
ifne L0
aload 0
getfield android/support/v4/c/o/n Landroid/database/Cursor;
invokeinterface android/database/Cursor/close()V 0
L0:
aload 0
aconst_null
putfield android/support/v4/c/o/n Landroid/database/Cursor;
return
.limit locals 1
.limit stack 2
.end method
