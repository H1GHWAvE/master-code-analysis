.bytecode 50.0
.class public synchronized abstract android/support/v4/f/a/a
.super java/lang/Object

.field public static final 'a' Ljava/lang/String; = "android.hardware.display.category.PRESENTATION"

.field private static final 'b' Ljava/util/WeakHashMap;

.method static <clinit>()V
new java/util/WeakHashMap
dup
invokespecial java/util/WeakHashMap/<init>()V
putstatic android/support/v4/f/a/a/b Ljava/util/WeakHashMap;
return
.limit locals 0
.limit stack 2
.end method

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/content/Context;)Landroid/support/v4/f/a/a;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
.catch all from L9 to L10 using L2
getstatic android/support/v4/f/a/a/b Ljava/util/WeakHashMap;
astore 3
aload 3
monitorenter
L0:
getstatic android/support/v4/f/a/a/b Ljava/util/WeakHashMap;
aload 0
invokevirtual java/util/WeakHashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/support/v4/f/a/a
astore 2
L1:
aload 2
astore 1
aload 2
ifnonnull L5
L3:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 17
if_icmplt L7
new android/support/v4/f/a/b
dup
aload 0
invokespecial android/support/v4/f/a/b/<init>(Landroid/content/Context;)V
astore 1
L4:
getstatic android/support/v4/f/a/a/b Ljava/util/WeakHashMap;
aload 0
aload 1
invokevirtual java/util/WeakHashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L5:
aload 3
monitorexit
L6:
aload 1
areturn
L7:
new android/support/v4/f/a/c
dup
aload 0
invokespecial android/support/v4/f/a/c/<init>(Landroid/content/Context;)V
astore 1
L8:
goto L4
L2:
astore 0
L9:
aload 3
monitorexit
L10:
aload 0
athrow
.limit locals 4
.limit stack 3
.end method

.method public abstract a(I)Landroid/view/Display;
.end method

.method public abstract a()[Landroid/view/Display;
.end method

.method public abstract a(Ljava/lang/String;)[Landroid/view/Display;
.end method
