.bytecode 50.0
.class final synchronized android/support/v4/j/i
.super java/lang/Object

.field public static final 'd' I = 1


.field public static final 'e' I = 2


.field public static final 'f' I = 1


.field public static final 'g' I = 2


.field public static final 'h' I = 1


.field public static final 'i' I = 2


.field private static final 'm' Ljava/lang/String; = "PrintHelperKitkat"

.field private static final 'n' I = 3500


.field final 'a' Landroid/content/Context;

.field 'b' Landroid/graphics/BitmapFactory$Options;

.field final 'c' Ljava/lang/Object;

.field 'j' I

.field 'k' I

.field 'l' I

.method <init>(Landroid/content/Context;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aconst_null
putfield android/support/v4/j/i/b Landroid/graphics/BitmapFactory$Options;
aload 0
new java/lang/Object
dup
invokespecial java/lang/Object/<init>()V
putfield android/support/v4/j/i/c Ljava/lang/Object;
aload 0
iconst_2
putfield android/support/v4/j/i/j I
aload 0
iconst_2
putfield android/support/v4/j/i/k I
aload 0
iconst_1
putfield android/support/v4/j/i/l I
aload 0
aload 1
putfield android/support/v4/j/i/a Landroid/content/Context;
return
.limit locals 2
.limit stack 3
.end method

.method private a()I
aload 0
getfield android/support/v4/j/i/j I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
iload 1
iconst_1
if_icmpeq L0
aload 0
areturn
L0:
aload 0
invokevirtual android/graphics/Bitmap/getWidth()I
aload 0
invokevirtual android/graphics/Bitmap/getHeight()I
getstatic android/graphics/Bitmap$Config/ARGB_8888 Landroid/graphics/Bitmap$Config;
invokestatic android/graphics/Bitmap/createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
astore 2
new android/graphics/Canvas
dup
aload 2
invokespecial android/graphics/Canvas/<init>(Landroid/graphics/Bitmap;)V
astore 3
new android/graphics/Paint
dup
invokespecial android/graphics/Paint/<init>()V
astore 4
new android/graphics/ColorMatrix
dup
invokespecial android/graphics/ColorMatrix/<init>()V
astore 5
aload 5
fconst_0
invokevirtual android/graphics/ColorMatrix/setSaturation(F)V
aload 4
new android/graphics/ColorMatrixColorFilter
dup
aload 5
invokespecial android/graphics/ColorMatrixColorFilter/<init>(Landroid/graphics/ColorMatrix;)V
invokevirtual android/graphics/Paint/setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;
pop
aload 3
aload 0
fconst_0
fconst_0
aload 4
invokevirtual android/graphics/Canvas/drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
aload 3
aconst_null
invokevirtual android/graphics/Canvas/setBitmap(Landroid/graphics/Bitmap;)V
aload 2
areturn
.limit locals 6
.limit stack 5
.end method

.method private a(Landroid/net/Uri;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch java/io/IOException from L5 to L6 using L7
.catch java/io/IOException from L8 to L9 using L10
aconst_null
astore 3
aload 1
ifnull L11
aload 0
getfield android/support/v4/j/i/a Landroid/content/Context;
ifnonnull L0
L11:
new java/lang/IllegalArgumentException
dup
ldc "bad argument to loadBitmap"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v4/j/i/a Landroid/content/Context;
invokevirtual android/content/Context/getContentResolver()Landroid/content/ContentResolver;
aload 1
invokevirtual android/content/ContentResolver/openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
astore 1
L1:
aload 1
astore 3
L3:
aload 1
aconst_null
aload 2
invokestatic android/graphics/BitmapFactory/decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
astore 2
L4:
aload 1
ifnull L6
L5:
aload 1
invokevirtual java/io/InputStream/close()V
L6:
aload 2
areturn
L7:
astore 1
ldc "PrintHelperKitkat"
ldc "close fail "
aload 1
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
aload 2
areturn
L2:
astore 1
aload 3
ifnull L9
L8:
aload 3
invokevirtual java/io/InputStream/close()V
L9:
aload 1
athrow
L10:
astore 2
ldc "PrintHelperKitkat"
ldc "close fail "
aload 2
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
goto L9
.limit locals 4
.limit stack 3
.end method

.method private static synthetic a(Landroid/support/v4/j/i;Landroid/net/Uri;)Landroid/graphics/Bitmap;
aload 0
aload 1
invokevirtual android/support/v4/j/i/a(Landroid/net/Uri;)Landroid/graphics/Bitmap;
areturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(IILandroid/graphics/RectF;I)Landroid/graphics/Matrix;
new android/graphics/Matrix
dup
invokespecial android/graphics/Matrix/<init>()V
astore 5
aload 2
invokevirtual android/graphics/RectF/width()F
iload 0
i2f
fdiv
fstore 4
iload 3
iconst_2
if_icmpne L0
fload 4
aload 2
invokevirtual android/graphics/RectF/height()F
iload 1
i2f
fdiv
invokestatic java/lang/Math/max(FF)F
fstore 4
L1:
aload 5
fload 4
fload 4
invokevirtual android/graphics/Matrix/postScale(FF)Z
pop
aload 5
aload 2
invokevirtual android/graphics/RectF/width()F
iload 0
i2f
fload 4
fmul
fsub
fconst_2
fdiv
aload 2
invokevirtual android/graphics/RectF/height()F
fload 4
iload 1
i2f
fmul
fsub
fconst_2
fdiv
invokevirtual android/graphics/Matrix/postTranslate(FF)Z
pop
aload 5
areturn
L0:
fload 4
aload 2
invokevirtual android/graphics/RectF/height()F
iload 1
i2f
fdiv
invokestatic java/lang/Math/min(FF)F
fstore 4
goto L1
.limit locals 6
.limit stack 5
.end method

.method private static synthetic a(Landroid/support/v4/j/i;)Ljava/lang/Object;
aload 0
getfield android/support/v4/j/i/c Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(I)V
aload 0
iload 1
putfield android/support/v4/j/i/j I
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/support/v4/j/n;)V
aload 2
ifnonnull L0
return
L0:
aload 0
getfield android/support/v4/j/i/j I
istore 4
aload 0
getfield android/support/v4/j/i/a Landroid/content/Context;
ldc "print"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/print/PrintManager
astore 6
getstatic android/print/PrintAttributes$MediaSize/UNKNOWN_PORTRAIT Landroid/print/PrintAttributes$MediaSize;
astore 5
aload 2
invokevirtual android/graphics/Bitmap/getWidth()I
aload 2
invokevirtual android/graphics/Bitmap/getHeight()I
if_icmple L1
getstatic android/print/PrintAttributes$MediaSize/UNKNOWN_LANDSCAPE Landroid/print/PrintAttributes$MediaSize;
astore 5
L1:
new android/print/PrintAttributes$Builder
dup
invokespecial android/print/PrintAttributes$Builder/<init>()V
aload 5
invokevirtual android/print/PrintAttributes$Builder/setMediaSize(Landroid/print/PrintAttributes$MediaSize;)Landroid/print/PrintAttributes$Builder;
aload 0
getfield android/support/v4/j/i/k I
invokevirtual android/print/PrintAttributes$Builder/setColorMode(I)Landroid/print/PrintAttributes$Builder;
invokevirtual android/print/PrintAttributes$Builder/build()Landroid/print/PrintAttributes;
astore 5
aload 6
aload 1
new android/support/v4/j/j
dup
aload 0
aload 1
aload 2
iload 4
aload 3
invokespecial android/support/v4/j/j/<init>(Landroid/support/v4/j/i;Ljava/lang/String;Landroid/graphics/Bitmap;ILandroid/support/v4/j/n;)V
aload 5
invokevirtual android/print/PrintManager/print(Ljava/lang/String;Landroid/print/PrintDocumentAdapter;Landroid/print/PrintAttributes;)Landroid/print/PrintJob;
pop
return
.limit locals 7
.limit stack 9
.end method

.method private a(Ljava/lang/String;Landroid/net/Uri;Landroid/support/v4/j/n;)V
new android/support/v4/j/k
dup
aload 0
aload 1
aload 2
aload 3
aload 0
getfield android/support/v4/j/i/j I
invokespecial android/support/v4/j/k/<init>(Landroid/support/v4/j/i;Ljava/lang/String;Landroid/net/Uri;Landroid/support/v4/j/n;I)V
astore 2
aload 0
getfield android/support/v4/j/i/a Landroid/content/Context;
ldc "print"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/print/PrintManager
astore 3
new android/print/PrintAttributes$Builder
dup
invokespecial android/print/PrintAttributes$Builder/<init>()V
astore 4
aload 4
aload 0
getfield android/support/v4/j/i/k I
invokevirtual android/print/PrintAttributes$Builder/setColorMode(I)Landroid/print/PrintAttributes$Builder;
pop
aload 0
getfield android/support/v4/j/i/l I
iconst_1
if_icmpne L0
aload 4
getstatic android/print/PrintAttributes$MediaSize/UNKNOWN_LANDSCAPE Landroid/print/PrintAttributes$MediaSize;
invokevirtual android/print/PrintAttributes$Builder/setMediaSize(Landroid/print/PrintAttributes$MediaSize;)Landroid/print/PrintAttributes$Builder;
pop
L1:
aload 3
aload 1
aload 2
aload 4
invokevirtual android/print/PrintAttributes$Builder/build()Landroid/print/PrintAttributes;
invokevirtual android/print/PrintManager/print(Ljava/lang/String;Landroid/print/PrintDocumentAdapter;Landroid/print/PrintAttributes;)Landroid/print/PrintJob;
pop
return
L0:
aload 0
getfield android/support/v4/j/i/l I
iconst_2
if_icmpne L1
aload 4
getstatic android/print/PrintAttributes$MediaSize/UNKNOWN_PORTRAIT Landroid/print/PrintAttributes$MediaSize;
invokevirtual android/print/PrintAttributes$Builder/setMediaSize(Landroid/print/PrintAttributes$MediaSize;)Landroid/print/PrintAttributes$Builder;
pop
goto L1
.limit locals 5
.limit stack 7
.end method

.method private b()I
aload 0
getfield android/support/v4/j/i/l I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static b(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
iload 1
iconst_1
if_icmpeq L0
aload 0
areturn
L0:
aload 0
invokevirtual android/graphics/Bitmap/getWidth()I
aload 0
invokevirtual android/graphics/Bitmap/getHeight()I
getstatic android/graphics/Bitmap$Config/ARGB_8888 Landroid/graphics/Bitmap$Config;
invokestatic android/graphics/Bitmap/createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
astore 2
new android/graphics/Canvas
dup
aload 2
invokespecial android/graphics/Canvas/<init>(Landroid/graphics/Bitmap;)V
astore 3
new android/graphics/Paint
dup
invokespecial android/graphics/Paint/<init>()V
astore 4
new android/graphics/ColorMatrix
dup
invokespecial android/graphics/ColorMatrix/<init>()V
astore 5
aload 5
fconst_0
invokevirtual android/graphics/ColorMatrix/setSaturation(F)V
aload 4
new android/graphics/ColorMatrixColorFilter
dup
aload 5
invokespecial android/graphics/ColorMatrixColorFilter/<init>(Landroid/graphics/ColorMatrix;)V
invokevirtual android/graphics/Paint/setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;
pop
aload 3
aload 0
fconst_0
fconst_0
aload 4
invokevirtual android/graphics/Canvas/drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
aload 3
aconst_null
invokevirtual android/graphics/Canvas/setBitmap(Landroid/graphics/Bitmap;)V
aload 2
areturn
.limit locals 6
.limit stack 5
.end method

.method private static b(IILandroid/graphics/RectF;I)Landroid/graphics/Matrix;
new android/graphics/Matrix
dup
invokespecial android/graphics/Matrix/<init>()V
astore 5
aload 2
invokevirtual android/graphics/RectF/width()F
iload 0
i2f
fdiv
fstore 4
iload 3
iconst_2
if_icmpne L0
fload 4
aload 2
invokevirtual android/graphics/RectF/height()F
iload 1
i2f
fdiv
invokestatic java/lang/Math/max(FF)F
fstore 4
L1:
aload 5
fload 4
fload 4
invokevirtual android/graphics/Matrix/postScale(FF)Z
pop
aload 5
aload 2
invokevirtual android/graphics/RectF/width()F
iload 0
i2f
fload 4
fmul
fsub
fconst_2
fdiv
aload 2
invokevirtual android/graphics/RectF/height()F
fload 4
iload 1
i2f
fmul
fsub
fconst_2
fdiv
invokevirtual android/graphics/Matrix/postTranslate(FF)Z
pop
aload 5
areturn
L0:
fload 4
aload 2
invokevirtual android/graphics/RectF/height()F
iload 1
i2f
fdiv
invokestatic java/lang/Math/min(FF)F
fstore 4
goto L1
.limit locals 6
.limit stack 5
.end method

.method private b(I)V
aload 0
iload 1
putfield android/support/v4/j/i/k I
return
.limit locals 2
.limit stack 2
.end method

.method private c()I
aload 0
getfield android/support/v4/j/i/k I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private c(I)V
aload 0
iload 1
putfield android/support/v4/j/i/l I
return
.limit locals 2
.limit stack 2
.end method

.method final a(Landroid/net/Uri;)Landroid/graphics/Bitmap;
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L4
.catch all from L5 to L6 using L7
.catch all from L8 to L9 using L7
.catch all from L10 to L11 using L2
.catch all from L12 to L13 using L14
.catch all from L15 to L16 using L14
iconst_1
istore 2
aload 1
ifnull L17
aload 0
getfield android/support/v4/j/i/a Landroid/content/Context;
ifnonnull L18
L17:
new java/lang/IllegalArgumentException
dup
ldc "bad argument to getScaledBitmap"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L18:
new android/graphics/BitmapFactory$Options
dup
invokespecial android/graphics/BitmapFactory$Options/<init>()V
astore 6
aload 6
iconst_1
putfield android/graphics/BitmapFactory$Options/inJustDecodeBounds Z
aload 0
aload 1
aload 6
invokespecial android/support/v4/j/i/a(Landroid/net/Uri;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
pop
aload 6
getfield android/graphics/BitmapFactory$Options/outWidth I
istore 4
aload 6
getfield android/graphics/BitmapFactory$Options/outHeight I
istore 5
iload 4
ifle L19
iload 5
ifgt L20
L19:
aconst_null
areturn
L20:
iload 4
iload 5
invokestatic java/lang/Math/max(II)I
istore 3
L21:
iload 3
sipush 3500
if_icmple L22
iload 3
iconst_1
iushr
istore 3
iload 2
iconst_1
ishl
istore 2
goto L21
L22:
iload 2
ifle L19
iload 4
iload 5
invokestatic java/lang/Math/min(II)I
iload 2
idiv
ifle L19
aload 0
getfield android/support/v4/j/i/c Ljava/lang/Object;
astore 6
aload 6
monitorenter
L0:
aload 0
new android/graphics/BitmapFactory$Options
dup
invokespecial android/graphics/BitmapFactory$Options/<init>()V
putfield android/support/v4/j/i/b Landroid/graphics/BitmapFactory$Options;
aload 0
getfield android/support/v4/j/i/b Landroid/graphics/BitmapFactory$Options;
iconst_1
putfield android/graphics/BitmapFactory$Options/inMutable Z
aload 0
getfield android/support/v4/j/i/b Landroid/graphics/BitmapFactory$Options;
iload 2
putfield android/graphics/BitmapFactory$Options/inSampleSize I
aload 0
getfield android/support/v4/j/i/b Landroid/graphics/BitmapFactory$Options;
astore 7
aload 6
monitorexit
L1:
aload 0
aload 1
aload 7
invokespecial android/support/v4/j/i/a(Landroid/net/Uri;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
astore 6
L3:
aload 0
getfield android/support/v4/j/i/c Ljava/lang/Object;
astore 1
aload 1
monitorenter
L5:
aload 0
aconst_null
putfield android/support/v4/j/i/b Landroid/graphics/BitmapFactory$Options;
aload 1
monitorexit
L6:
aload 6
areturn
L7:
astore 6
L8:
aload 1
monitorexit
L9:
aload 6
athrow
L2:
astore 1
L10:
aload 6
monitorexit
L11:
aload 1
athrow
L4:
astore 6
aload 0
getfield android/support/v4/j/i/c Ljava/lang/Object;
astore 1
aload 1
monitorenter
L12:
aload 0
aconst_null
putfield android/support/v4/j/i/b Landroid/graphics/BitmapFactory$Options;
aload 1
monitorexit
L13:
aload 6
athrow
L14:
astore 6
L15:
aload 1
monitorexit
L16:
aload 6
athrow
.limit locals 8
.limit stack 3
.end method
