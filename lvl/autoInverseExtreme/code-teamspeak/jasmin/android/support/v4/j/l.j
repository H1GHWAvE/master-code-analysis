.bytecode 50.0
.class final synchronized android/support/v4/j/l
.super android/os/AsyncTask

.field final synthetic 'a' Landroid/os/CancellationSignal;

.field final synthetic 'b' Landroid/print/PrintAttributes;

.field final synthetic 'c' Landroid/print/PrintAttributes;

.field final synthetic 'd' Landroid/print/PrintDocumentAdapter$LayoutResultCallback;

.field final synthetic 'e' Landroid/support/v4/j/k;

.method <init>(Landroid/support/v4/j/k;Landroid/os/CancellationSignal;Landroid/print/PrintAttributes;Landroid/print/PrintAttributes;Landroid/print/PrintDocumentAdapter$LayoutResultCallback;)V
aload 0
aload 1
putfield android/support/v4/j/l/e Landroid/support/v4/j/k;
aload 0
aload 2
putfield android/support/v4/j/l/a Landroid/os/CancellationSignal;
aload 0
aload 3
putfield android/support/v4/j/l/b Landroid/print/PrintAttributes;
aload 0
aload 4
putfield android/support/v4/j/l/c Landroid/print/PrintAttributes;
aload 0
aload 5
putfield android/support/v4/j/l/d Landroid/print/PrintDocumentAdapter$LayoutResultCallback;
aload 0
invokespecial android/os/AsyncTask/<init>()V
return
.limit locals 6
.limit stack 2
.end method

.method private transient a()Landroid/graphics/Bitmap;
.catch java/io/FileNotFoundException from L0 to L1 using L2
L0:
aload 0
getfield android/support/v4/j/l/e Landroid/support/v4/j/k;
getfield android/support/v4/j/k/g Landroid/support/v4/j/i;
aload 0
getfield android/support/v4/j/l/e Landroid/support/v4/j/k;
getfield android/support/v4/j/k/d Landroid/net/Uri;
invokevirtual android/support/v4/j/i/a(Landroid/net/Uri;)Landroid/graphics/Bitmap;
astore 1
L1:
aload 1
areturn
L2:
astore 1
aconst_null
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/graphics/Bitmap;)V
iconst_1
istore 2
aload 0
aload 1
invokespecial android/os/AsyncTask/onPostExecute(Ljava/lang/Object;)V
aload 0
getfield android/support/v4/j/l/e Landroid/support/v4/j/k;
aload 1
putfield android/support/v4/j/k/b Landroid/graphics/Bitmap;
aload 1
ifnull L0
new android/print/PrintDocumentInfo$Builder
dup
aload 0
getfield android/support/v4/j/l/e Landroid/support/v4/j/k;
getfield android/support/v4/j/k/c Ljava/lang/String;
invokespecial android/print/PrintDocumentInfo$Builder/<init>(Ljava/lang/String;)V
iconst_1
invokevirtual android/print/PrintDocumentInfo$Builder/setContentType(I)Landroid/print/PrintDocumentInfo$Builder;
iconst_1
invokevirtual android/print/PrintDocumentInfo$Builder/setPageCount(I)Landroid/print/PrintDocumentInfo$Builder;
invokevirtual android/print/PrintDocumentInfo$Builder/build()Landroid/print/PrintDocumentInfo;
astore 1
aload 0
getfield android/support/v4/j/l/b Landroid/print/PrintAttributes;
aload 0
getfield android/support/v4/j/l/c Landroid/print/PrintAttributes;
invokevirtual android/print/PrintAttributes/equals(Ljava/lang/Object;)Z
ifne L1
L2:
aload 0
getfield android/support/v4/j/l/d Landroid/print/PrintDocumentAdapter$LayoutResultCallback;
aload 1
iload 2
invokevirtual android/print/PrintDocumentAdapter$LayoutResultCallback/onLayoutFinished(Landroid/print/PrintDocumentInfo;Z)V
L3:
aload 0
getfield android/support/v4/j/l/e Landroid/support/v4/j/k;
aconst_null
putfield android/support/v4/j/k/a Landroid/os/AsyncTask;
return
L1:
iconst_0
istore 2
goto L2
L0:
aload 0
getfield android/support/v4/j/l/d Landroid/print/PrintDocumentAdapter$LayoutResultCallback;
aconst_null
invokevirtual android/print/PrintDocumentAdapter$LayoutResultCallback/onLayoutFailed(Ljava/lang/CharSequence;)V
goto L3
.limit locals 3
.limit stack 3
.end method

.method private b()V
aload 0
getfield android/support/v4/j/l/d Landroid/print/PrintDocumentAdapter$LayoutResultCallback;
invokevirtual android/print/PrintDocumentAdapter$LayoutResultCallback/onLayoutCancelled()V
aload 0
getfield android/support/v4/j/l/e Landroid/support/v4/j/k;
aconst_null
putfield android/support/v4/j/k/a Landroid/os/AsyncTask;
return
.limit locals 1
.limit stack 2
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
aload 0
invokespecial android/support/v4/j/l/a()Landroid/graphics/Bitmap;
areturn
.limit locals 2
.limit stack 1
.end method

.method protected final synthetic onCancelled(Ljava/lang/Object;)V
aload 0
getfield android/support/v4/j/l/d Landroid/print/PrintDocumentAdapter$LayoutResultCallback;
invokevirtual android/print/PrintDocumentAdapter$LayoutResultCallback/onLayoutCancelled()V
aload 0
getfield android/support/v4/j/l/e Landroid/support/v4/j/k;
aconst_null
putfield android/support/v4/j/k/a Landroid/os/AsyncTask;
return
.limit locals 2
.limit stack 2
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
iconst_1
istore 2
aload 1
checkcast android/graphics/Bitmap
astore 1
aload 0
aload 1
invokespecial android/os/AsyncTask/onPostExecute(Ljava/lang/Object;)V
aload 0
getfield android/support/v4/j/l/e Landroid/support/v4/j/k;
aload 1
putfield android/support/v4/j/k/b Landroid/graphics/Bitmap;
aload 1
ifnull L0
new android/print/PrintDocumentInfo$Builder
dup
aload 0
getfield android/support/v4/j/l/e Landroid/support/v4/j/k;
getfield android/support/v4/j/k/c Ljava/lang/String;
invokespecial android/print/PrintDocumentInfo$Builder/<init>(Ljava/lang/String;)V
iconst_1
invokevirtual android/print/PrintDocumentInfo$Builder/setContentType(I)Landroid/print/PrintDocumentInfo$Builder;
iconst_1
invokevirtual android/print/PrintDocumentInfo$Builder/setPageCount(I)Landroid/print/PrintDocumentInfo$Builder;
invokevirtual android/print/PrintDocumentInfo$Builder/build()Landroid/print/PrintDocumentInfo;
astore 1
aload 0
getfield android/support/v4/j/l/b Landroid/print/PrintAttributes;
aload 0
getfield android/support/v4/j/l/c Landroid/print/PrintAttributes;
invokevirtual android/print/PrintAttributes/equals(Ljava/lang/Object;)Z
ifne L1
L2:
aload 0
getfield android/support/v4/j/l/d Landroid/print/PrintDocumentAdapter$LayoutResultCallback;
aload 1
iload 2
invokevirtual android/print/PrintDocumentAdapter$LayoutResultCallback/onLayoutFinished(Landroid/print/PrintDocumentInfo;Z)V
L3:
aload 0
getfield android/support/v4/j/l/e Landroid/support/v4/j/k;
aconst_null
putfield android/support/v4/j/k/a Landroid/os/AsyncTask;
return
L1:
iconst_0
istore 2
goto L2
L0:
aload 0
getfield android/support/v4/j/l/d Landroid/print/PrintDocumentAdapter$LayoutResultCallback;
aconst_null
invokevirtual android/print/PrintDocumentAdapter$LayoutResultCallback/onLayoutFailed(Ljava/lang/CharSequence;)V
goto L3
.limit locals 3
.limit stack 3
.end method

.method protected final onPreExecute()V
aload 0
getfield android/support/v4/j/l/a Landroid/os/CancellationSignal;
new android/support/v4/j/m
dup
aload 0
invokespecial android/support/v4/j/m/<init>(Landroid/support/v4/j/l;)V
invokevirtual android/os/CancellationSignal/setOnCancelListener(Landroid/os/CancellationSignal$OnCancelListener;)V
return
.limit locals 1
.limit stack 4
.end method
