.bytecode 50.0
.class final synchronized android/support/v4/j/j
.super android/print/PrintDocumentAdapter

.field final synthetic 'a' Ljava/lang/String;

.field final synthetic 'b' Landroid/graphics/Bitmap;

.field final synthetic 'c' I

.field final synthetic 'd' Landroid/support/v4/j/n;

.field final synthetic 'e' Landroid/support/v4/j/i;

.field private 'f' Landroid/print/PrintAttributes;

.method <init>(Landroid/support/v4/j/i;Ljava/lang/String;Landroid/graphics/Bitmap;ILandroid/support/v4/j/n;)V
aload 0
aload 1
putfield android/support/v4/j/j/e Landroid/support/v4/j/i;
aload 0
aload 2
putfield android/support/v4/j/j/a Ljava/lang/String;
aload 0
aload 3
putfield android/support/v4/j/j/b Landroid/graphics/Bitmap;
aload 0
iload 4
putfield android/support/v4/j/j/c I
aload 0
aload 5
putfield android/support/v4/j/j/d Landroid/support/v4/j/n;
aload 0
invokespecial android/print/PrintDocumentAdapter/<init>()V
return
.limit locals 6
.limit stack 2
.end method

.method public final onFinish()V
return
.limit locals 1
.limit stack 0
.end method

.method public final onLayout(Landroid/print/PrintAttributes;Landroid/print/PrintAttributes;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$LayoutResultCallback;Landroid/os/Bundle;)V
iconst_1
istore 6
aload 0
aload 2
putfield android/support/v4/j/j/f Landroid/print/PrintAttributes;
new android/print/PrintDocumentInfo$Builder
dup
aload 0
getfield android/support/v4/j/j/a Ljava/lang/String;
invokespecial android/print/PrintDocumentInfo$Builder/<init>(Ljava/lang/String;)V
iconst_1
invokevirtual android/print/PrintDocumentInfo$Builder/setContentType(I)Landroid/print/PrintDocumentInfo$Builder;
iconst_1
invokevirtual android/print/PrintDocumentInfo$Builder/setPageCount(I)Landroid/print/PrintDocumentInfo$Builder;
invokevirtual android/print/PrintDocumentInfo$Builder/build()Landroid/print/PrintDocumentInfo;
astore 3
aload 2
aload 1
invokevirtual android/print/PrintAttributes/equals(Ljava/lang/Object;)Z
ifne L0
L1:
aload 4
aload 3
iload 6
invokevirtual android/print/PrintDocumentAdapter$LayoutResultCallback/onLayoutFinished(Landroid/print/PrintDocumentInfo;Z)V
return
L0:
iconst_0
istore 6
goto L1
.limit locals 7
.limit stack 3
.end method

.method public final onWrite([Landroid/print/PageRange;Landroid/os/ParcelFileDescriptor;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$WriteResultCallback;)V
.catch all from L0 to L1 using L2
.catch java/io/IOException from L1 to L3 using L4
.catch all from L1 to L3 using L2
.catch java/io/IOException from L5 to L6 using L7
.catch all from L8 to L9 using L2
.catch java/io/IOException from L10 to L11 using L12
new android/print/pdf/PrintedPdfDocument
dup
aload 0
getfield android/support/v4/j/j/e Landroid/support/v4/j/i;
getfield android/support/v4/j/i/a Landroid/content/Context;
aload 0
getfield android/support/v4/j/j/f Landroid/print/PrintAttributes;
invokespecial android/print/pdf/PrintedPdfDocument/<init>(Landroid/content/Context;Landroid/print/PrintAttributes;)V
astore 3
aload 0
getfield android/support/v4/j/j/b Landroid/graphics/Bitmap;
aload 0
getfield android/support/v4/j/j/f Landroid/print/PrintAttributes;
invokevirtual android/print/PrintAttributes/getColorMode()I
invokestatic android/support/v4/j/i/a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
astore 1
L0:
aload 3
iconst_1
invokevirtual android/print/pdf/PrintedPdfDocument/startPage(I)Landroid/graphics/pdf/PdfDocument$Page;
astore 5
new android/graphics/RectF
dup
aload 5
invokevirtual android/graphics/pdf/PdfDocument$Page/getInfo()Landroid/graphics/pdf/PdfDocument$PageInfo;
invokevirtual android/graphics/pdf/PdfDocument$PageInfo/getContentRect()Landroid/graphics/Rect;
invokespecial android/graphics/RectF/<init>(Landroid/graphics/Rect;)V
astore 6
aload 1
invokevirtual android/graphics/Bitmap/getWidth()I
aload 1
invokevirtual android/graphics/Bitmap/getHeight()I
aload 6
aload 0
getfield android/support/v4/j/j/c I
invokestatic android/support/v4/j/i/a(IILandroid/graphics/RectF;I)Landroid/graphics/Matrix;
astore 6
aload 5
invokevirtual android/graphics/pdf/PdfDocument$Page/getCanvas()Landroid/graphics/Canvas;
aload 1
aload 6
aconst_null
invokevirtual android/graphics/Canvas/drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V
aload 3
aload 5
invokevirtual android/print/pdf/PrintedPdfDocument/finishPage(Landroid/graphics/pdf/PdfDocument$Page;)V
L1:
aload 3
new java/io/FileOutputStream
dup
aload 2
invokevirtual android/os/ParcelFileDescriptor/getFileDescriptor()Ljava/io/FileDescriptor;
invokespecial java/io/FileOutputStream/<init>(Ljava/io/FileDescriptor;)V
invokevirtual android/print/pdf/PrintedPdfDocument/writeTo(Ljava/io/OutputStream;)V
aload 4
iconst_1
anewarray android/print/PageRange
dup
iconst_0
getstatic android/print/PageRange/ALL_PAGES Landroid/print/PageRange;
aastore
invokevirtual android/print/PrintDocumentAdapter$WriteResultCallback/onWriteFinished([Landroid/print/PageRange;)V
L3:
aload 3
invokevirtual android/print/pdf/PrintedPdfDocument/close()V
aload 2
ifnull L6
L5:
aload 2
invokevirtual android/os/ParcelFileDescriptor/close()V
L6:
aload 1
aload 0
getfield android/support/v4/j/j/b Landroid/graphics/Bitmap;
if_acmpeq L13
aload 1
invokevirtual android/graphics/Bitmap/recycle()V
L13:
return
L4:
astore 5
L8:
ldc "PrintHelperKitkat"
ldc "Error writing printed content"
aload 5
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
aload 4
aconst_null
invokevirtual android/print/PrintDocumentAdapter$WriteResultCallback/onWriteFailed(Ljava/lang/CharSequence;)V
L9:
goto L3
L2:
astore 4
aload 3
invokevirtual android/print/pdf/PrintedPdfDocument/close()V
aload 2
ifnull L11
L10:
aload 2
invokevirtual android/os/ParcelFileDescriptor/close()V
L11:
aload 1
aload 0
getfield android/support/v4/j/j/b Landroid/graphics/Bitmap;
if_acmpeq L14
aload 1
invokevirtual android/graphics/Bitmap/recycle()V
L14:
aload 4
athrow
L7:
astore 2
goto L6
L12:
astore 2
goto L11
.limit locals 7
.limit stack 5
.end method
