.bytecode 50.0
.class public final synchronized android/support/v4/j/a
.super java/lang/Object

.field public static final 'a' I = 1


.field public static final 'b' I = 2


.field public static final 'c' I = 1


.field public static final 'd' I = 2


.field public static final 'e' I = 1


.field public static final 'f' I = 2


.field 'g' Landroid/support/v4/j/h;

.method private <init>(Landroid/content/Context;)V
aload 0
invokespecial java/lang/Object/<init>()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 19
if_icmplt L0
iconst_1
istore 2
L1:
iload 2
ifeq L2
aload 0
new android/support/v4/j/d
dup
aload 1
invokespecial android/support/v4/j/d/<init>(Landroid/content/Context;)V
putfield android/support/v4/j/a/g Landroid/support/v4/j/h;
return
L0:
iconst_0
istore 2
goto L1
L2:
aload 0
new android/support/v4/j/g
dup
iconst_0
invokespecial android/support/v4/j/g/<init>(B)V
putfield android/support/v4/j/a/g Landroid/support/v4/j/h;
return
.limit locals 3
.limit stack 4
.end method

.method private a(I)V
aload 0
getfield android/support/v4/j/a/g Landroid/support/v4/j/h;
iload 1
invokeinterface android/support/v4/j/h/a(I)V 1
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/String;Landroid/graphics/Bitmap;)V
aload 0
getfield android/support/v4/j/a/g Landroid/support/v4/j/h;
aload 1
aload 2
aconst_null
invokeinterface android/support/v4/j/h/a(Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/support/v4/j/c;)V 3
return
.limit locals 3
.limit stack 4
.end method

.method private a(Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/support/v4/j/c;)V
aload 0
getfield android/support/v4/j/a/g Landroid/support/v4/j/h;
aload 1
aload 2
aload 3
invokeinterface android/support/v4/j/h/a(Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/support/v4/j/c;)V 3
return
.limit locals 4
.limit stack 4
.end method

.method private a(Ljava/lang/String;Landroid/net/Uri;)V
aload 0
getfield android/support/v4/j/a/g Landroid/support/v4/j/h;
aload 1
aload 2
aconst_null
invokeinterface android/support/v4/j/h/a(Ljava/lang/String;Landroid/net/Uri;Landroid/support/v4/j/c;)V 3
return
.limit locals 3
.limit stack 4
.end method

.method private a(Ljava/lang/String;Landroid/net/Uri;Landroid/support/v4/j/c;)V
aload 0
getfield android/support/v4/j/a/g Landroid/support/v4/j/h;
aload 1
aload 2
aload 3
invokeinterface android/support/v4/j/h/a(Ljava/lang/String;Landroid/net/Uri;Landroid/support/v4/j/c;)V 3
return
.limit locals 4
.limit stack 4
.end method

.method private static a()Z
getstatic android/os/Build$VERSION/SDK_INT I
bipush 19
if_icmplt L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 0
.limit stack 2
.end method

.method private b()I
aload 0
getfield android/support/v4/j/a/g Landroid/support/v4/j/h;
invokeinterface android/support/v4/j/h/a()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private b(I)V
aload 0
getfield android/support/v4/j/a/g Landroid/support/v4/j/h;
iload 1
invokeinterface android/support/v4/j/h/b(I)V 1
return
.limit locals 2
.limit stack 2
.end method

.method private c()I
aload 0
getfield android/support/v4/j/a/g Landroid/support/v4/j/h;
invokeinterface android/support/v4/j/h/b()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private c(I)V
aload 0
getfield android/support/v4/j/a/g Landroid/support/v4/j/h;
iload 1
invokeinterface android/support/v4/j/h/c(I)V 1
return
.limit locals 2
.limit stack 2
.end method

.method private d()I
aload 0
getfield android/support/v4/j/a/g Landroid/support/v4/j/h;
invokeinterface android/support/v4/j/h/c()I 0
ireturn
.limit locals 1
.limit stack 1
.end method
