.bytecode 50.0
.class final synchronized android/support/v4/j/d
.super java/lang/Object
.implements android/support/v4/j/h

.field private final 'a' Landroid/support/v4/j/i;

.method <init>(Landroid/content/Context;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new android/support/v4/j/i
dup
aload 1
invokespecial android/support/v4/j/i/<init>(Landroid/content/Context;)V
putfield android/support/v4/j/d/a Landroid/support/v4/j/i;
return
.limit locals 2
.limit stack 4
.end method

.method public final a()I
aload 0
getfield android/support/v4/j/d/a Landroid/support/v4/j/i;
getfield android/support/v4/j/i/j I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a(I)V
aload 0
getfield android/support/v4/j/d/a Landroid/support/v4/j/i;
iload 1
putfield android/support/v4/j/i/j I
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/support/v4/j/c;)V
aconst_null
astore 5
aload 3
ifnull L0
new android/support/v4/j/e
dup
aload 0
aload 3
invokespecial android/support/v4/j/e/<init>(Landroid/support/v4/j/d;Landroid/support/v4/j/c;)V
astore 5
L0:
aload 0
getfield android/support/v4/j/d/a Landroid/support/v4/j/i;
astore 6
aload 2
ifnull L1
aload 6
getfield android/support/v4/j/i/j I
istore 4
aload 6
getfield android/support/v4/j/i/a Landroid/content/Context;
ldc "print"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/print/PrintManager
astore 7
getstatic android/print/PrintAttributes$MediaSize/UNKNOWN_PORTRAIT Landroid/print/PrintAttributes$MediaSize;
astore 3
aload 2
invokevirtual android/graphics/Bitmap/getWidth()I
aload 2
invokevirtual android/graphics/Bitmap/getHeight()I
if_icmple L2
getstatic android/print/PrintAttributes$MediaSize/UNKNOWN_LANDSCAPE Landroid/print/PrintAttributes$MediaSize;
astore 3
L2:
new android/print/PrintAttributes$Builder
dup
invokespecial android/print/PrintAttributes$Builder/<init>()V
aload 3
invokevirtual android/print/PrintAttributes$Builder/setMediaSize(Landroid/print/PrintAttributes$MediaSize;)Landroid/print/PrintAttributes$Builder;
aload 6
getfield android/support/v4/j/i/k I
invokevirtual android/print/PrintAttributes$Builder/setColorMode(I)Landroid/print/PrintAttributes$Builder;
invokevirtual android/print/PrintAttributes$Builder/build()Landroid/print/PrintAttributes;
astore 3
aload 7
aload 1
new android/support/v4/j/j
dup
aload 6
aload 1
aload 2
iload 4
aload 5
invokespecial android/support/v4/j/j/<init>(Landroid/support/v4/j/i;Ljava/lang/String;Landroid/graphics/Bitmap;ILandroid/support/v4/j/n;)V
aload 3
invokevirtual android/print/PrintManager/print(Ljava/lang/String;Landroid/print/PrintDocumentAdapter;Landroid/print/PrintAttributes;)Landroid/print/PrintJob;
pop
L1:
return
.limit locals 8
.limit stack 9
.end method

.method public final a(Ljava/lang/String;Landroid/net/Uri;Landroid/support/v4/j/c;)V
aconst_null
astore 4
aload 3
ifnull L0
new android/support/v4/j/f
dup
aload 0
aload 3
invokespecial android/support/v4/j/f/<init>(Landroid/support/v4/j/d;Landroid/support/v4/j/c;)V
astore 4
L0:
aload 0
getfield android/support/v4/j/d/a Landroid/support/v4/j/i;
astore 3
new android/support/v4/j/k
dup
aload 3
aload 1
aload 2
aload 4
aload 3
getfield android/support/v4/j/i/j I
invokespecial android/support/v4/j/k/<init>(Landroid/support/v4/j/i;Ljava/lang/String;Landroid/net/Uri;Landroid/support/v4/j/n;I)V
astore 2
aload 3
getfield android/support/v4/j/i/a Landroid/content/Context;
ldc "print"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/print/PrintManager
astore 4
new android/print/PrintAttributes$Builder
dup
invokespecial android/print/PrintAttributes$Builder/<init>()V
astore 5
aload 5
aload 3
getfield android/support/v4/j/i/k I
invokevirtual android/print/PrintAttributes$Builder/setColorMode(I)Landroid/print/PrintAttributes$Builder;
pop
aload 3
getfield android/support/v4/j/i/l I
iconst_1
if_icmpne L1
aload 5
getstatic android/print/PrintAttributes$MediaSize/UNKNOWN_LANDSCAPE Landroid/print/PrintAttributes$MediaSize;
invokevirtual android/print/PrintAttributes$Builder/setMediaSize(Landroid/print/PrintAttributes$MediaSize;)Landroid/print/PrintAttributes$Builder;
pop
L2:
aload 4
aload 1
aload 2
aload 5
invokevirtual android/print/PrintAttributes$Builder/build()Landroid/print/PrintAttributes;
invokevirtual android/print/PrintManager/print(Ljava/lang/String;Landroid/print/PrintDocumentAdapter;Landroid/print/PrintAttributes;)Landroid/print/PrintJob;
pop
return
L1:
aload 3
getfield android/support/v4/j/i/l I
iconst_2
if_icmpne L2
aload 5
getstatic android/print/PrintAttributes$MediaSize/UNKNOWN_PORTRAIT Landroid/print/PrintAttributes$MediaSize;
invokevirtual android/print/PrintAttributes$Builder/setMediaSize(Landroid/print/PrintAttributes$MediaSize;)Landroid/print/PrintAttributes$Builder;
pop
goto L2
.limit locals 6
.limit stack 7
.end method

.method public final b()I
aload 0
getfield android/support/v4/j/d/a Landroid/support/v4/j/i;
getfield android/support/v4/j/i/k I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final b(I)V
aload 0
getfield android/support/v4/j/d/a Landroid/support/v4/j/i;
iload 1
putfield android/support/v4/j/i/k I
return
.limit locals 2
.limit stack 2
.end method

.method public final c()I
aload 0
getfield android/support/v4/j/d/a Landroid/support/v4/j/i;
getfield android/support/v4/j/i/l I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final c(I)V
aload 0
getfield android/support/v4/j/d/a Landroid/support/v4/j/i;
iload 1
putfield android/support/v4/j/i/l I
return
.limit locals 2
.limit stack 2
.end method
