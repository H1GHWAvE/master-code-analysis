.bytecode 50.0
.class final synchronized android/support/v4/l/a/b
.super java/lang/Object

.field public static final 'a' Ljava/lang/String; = "embeddedTts"

.field public static final 'b' Ljava/lang/String; = "networkTts"

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/speech/tts/TextToSpeech;Ljava/util/Locale;)Ljava/util/Set;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 15
if_icmplt L0
aload 0
aload 1
invokevirtual android/speech/tts/TextToSpeech/getFeatures(Ljava/util/Locale;)Ljava/util/Set;
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Landroid/speech/tts/TextToSpeech;Landroid/support/v4/l/a/e;)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 15
if_icmplt L0
aload 0
new android/support/v4/l/a/c
dup
aload 1
invokespecial android/support/v4/l/a/c/<init>(Landroid/support/v4/l/a/e;)V
invokevirtual android/speech/tts/TextToSpeech/setOnUtteranceProgressListener(Landroid/speech/tts/UtteranceProgressListener;)I
pop
return
L0:
aload 0
new android/support/v4/l/a/d
dup
aload 1
invokespecial android/support/v4/l/a/d/<init>(Landroid/support/v4/l/a/e;)V
invokevirtual android/speech/tts/TextToSpeech/setOnUtteranceCompletedListener(Landroid/speech/tts/TextToSpeech$OnUtteranceCompletedListener;)I
pop
return
.limit locals 2
.limit stack 4
.end method
