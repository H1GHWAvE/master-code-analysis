.bytecode 50.0
.class final synchronized android/support/v4/b/f
.super java/lang/Object
.implements android/support/v4/b/l

.field 'a' Ljava/util/List;

.field 'b' Ljava/util/List;

.field 'c' Landroid/view/View;

.field 'd' J

.field 'e' J

.field 'f' F

.field 'g' Ljava/lang/Runnable;

.field private 'h' Z

.field private 'i' Z

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/b/f/a Ljava/util/List;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/b/f/b Ljava/util/List;
aload 0
ldc2_w 200L
putfield android/support/v4/b/f/e J
aload 0
fconst_0
putfield android/support/v4/b/f/f F
aload 0
iconst_0
putfield android/support/v4/b/f/h Z
aload 0
iconst_0
putfield android/support/v4/b/f/i Z
aload 0
new android/support/v4/b/g
dup
aload 0
invokespecial android/support/v4/b/g/<init>(Landroid/support/v4/b/f;)V
putfield android/support/v4/b/f/g Ljava/lang/Runnable;
return
.limit locals 1
.limit stack 4
.end method

.method private static synthetic a(Landroid/support/v4/b/f;F)F
aload 0
fload 1
putfield android/support/v4/b/f/f F
fload 1
freturn
.limit locals 2
.limit stack 2
.end method

.method private static synthetic a(Landroid/support/v4/b/f;)J
aload 0
getfield android/support/v4/b/f/c Landroid/view/View;
invokevirtual android/view/View/getDrawingTime()J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private static synthetic b(Landroid/support/v4/b/f;)J
aload 0
getfield android/support/v4/b/f/d J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private static synthetic c(Landroid/support/v4/b/f;)J
aload 0
getfield android/support/v4/b/f/e J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private static synthetic d(Landroid/support/v4/b/f;)V
aload 0
getfield android/support/v4/b/f/b Ljava/util/List;
invokeinterface java/util/List/size()I 0
iconst_1
isub
istore 1
L0:
iload 1
iflt L1
aload 0
getfield android/support/v4/b/f/b Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
pop
iload 1
iconst_1
isub
istore 1
goto L0
L1:
return
.limit locals 2
.limit stack 2
.end method

.method private static synthetic e(Landroid/support/v4/b/f;)F
aload 0
getfield android/support/v4/b/f/f F
freturn
.limit locals 1
.limit stack 1
.end method

.method private e()V
aload 0
getfield android/support/v4/b/f/b Ljava/util/List;
invokeinterface java/util/List/size()I 0
iconst_1
isub
istore 1
L0:
iload 1
iflt L1
aload 0
getfield android/support/v4/b/f/b Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
pop
iload 1
iconst_1
isub
istore 1
goto L0
L1:
return
.limit locals 2
.limit stack 2
.end method

.method private f()J
aload 0
getfield android/support/v4/b/f/c Landroid/view/View;
invokevirtual android/view/View/getDrawingTime()J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private static synthetic f(Landroid/support/v4/b/f;)V
aload 0
invokevirtual android/support/v4/b/f/b()V
return
.limit locals 1
.limit stack 1
.end method

.method private static synthetic g(Landroid/support/v4/b/f;)Ljava/lang/Runnable;
aload 0
getfield android/support/v4/b/f/g Ljava/lang/Runnable;
areturn
.limit locals 1
.limit stack 1
.end method

.method private g()V
aload 0
getfield android/support/v4/b/f/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
iconst_1
isub
istore 1
L0:
iload 1
iflt L1
aload 0
getfield android/support/v4/b/f/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
pop
iload 1
iconst_1
isub
istore 1
goto L0
L1:
return
.limit locals 2
.limit stack 2
.end method

.method private h()V
aload 0
getfield android/support/v4/b/f/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
iconst_1
isub
istore 1
L0:
iload 1
iflt L1
aload 0
getfield android/support/v4/b/f/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
pop
iload 1
iconst_1
isub
istore 1
goto L0
L1:
return
.limit locals 2
.limit stack 2
.end method

.method public final a()V
aload 0
getfield android/support/v4/b/f/h Z
ifeq L0
return
L0:
aload 0
iconst_1
putfield android/support/v4/b/f/h Z
aload 0
getfield android/support/v4/b/f/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
iconst_1
isub
istore 1
L1:
iload 1
iflt L2
aload 0
getfield android/support/v4/b/f/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
pop
iload 1
iconst_1
isub
istore 1
goto L1
L2:
aload 0
fconst_0
putfield android/support/v4/b/f/f F
aload 0
aload 0
getfield android/support/v4/b/f/c Landroid/view/View;
invokevirtual android/view/View/getDrawingTime()J
putfield android/support/v4/b/f/d J
aload 0
getfield android/support/v4/b/f/c Landroid/view/View;
aload 0
getfield android/support/v4/b/f/g Ljava/lang/Runnable;
ldc2_w 16L
invokevirtual android/view/View/postDelayed(Ljava/lang/Runnable;J)Z
pop
return
.limit locals 2
.limit stack 4
.end method

.method public final a(J)V
aload 0
getfield android/support/v4/b/f/h Z
ifne L0
aload 0
lload 1
putfield android/support/v4/b/f/e J
L0:
return
.limit locals 3
.limit stack 3
.end method

.method public final a(Landroid/support/v4/b/b;)V
aload 0
getfield android/support/v4/b/f/a Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/support/v4/b/d;)V
aload 0
getfield android/support/v4/b/f/b Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/view/View;)V
aload 0
aload 1
putfield android/support/v4/b/f/c Landroid/view/View;
return
.limit locals 2
.limit stack 2
.end method

.method final b()V
aload 0
getfield android/support/v4/b/f/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
iconst_1
isub
istore 1
L0:
iload 1
iflt L1
aload 0
getfield android/support/v4/b/f/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
pop
iload 1
iconst_1
isub
istore 1
goto L0
L1:
return
.limit locals 2
.limit stack 2
.end method

.method public final c()V
aload 0
getfield android/support/v4/b/f/i Z
ifeq L0
return
L0:
aload 0
iconst_1
putfield android/support/v4/b/f/i Z
aload 0
getfield android/support/v4/b/f/h Z
ifeq L1
aload 0
getfield android/support/v4/b/f/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
iconst_1
isub
istore 1
L2:
iload 1
iflt L1
aload 0
getfield android/support/v4/b/f/a Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
pop
iload 1
iconst_1
isub
istore 1
goto L2
L1:
aload 0
invokevirtual android/support/v4/b/f/b()V
return
.limit locals 2
.limit stack 2
.end method

.method public final d()F
aload 0
getfield android/support/v4/b/f/f F
freturn
.limit locals 1
.limit stack 1
.end method
