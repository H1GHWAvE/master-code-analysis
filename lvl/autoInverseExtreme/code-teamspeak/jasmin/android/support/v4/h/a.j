.bytecode 50.0
.class public final synchronized android/support/v4/h/a
.super java/lang/Object

.field private static final 'a' Landroid/support/v4/h/c;

.method static <clinit>()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 16
if_icmplt L0
new android/support/v4/h/f
dup
invokespecial android/support/v4/h/f/<init>()V
putstatic android/support/v4/h/a/a Landroid/support/v4/h/c;
return
L0:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 13
if_icmplt L1
new android/support/v4/h/e
dup
invokespecial android/support/v4/h/e/<init>()V
putstatic android/support/v4/h/a/a Landroid/support/v4/h/c;
return
L1:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 8
if_icmplt L2
new android/support/v4/h/d
dup
invokespecial android/support/v4/h/d/<init>()V
putstatic android/support/v4/h/a/a Landroid/support/v4/h/c;
return
L2:
new android/support/v4/h/b
dup
invokespecial android/support/v4/h/b/<init>()V
putstatic android/support/v4/h/a/a Landroid/support/v4/h/c;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/net/ConnectivityManager;Landroid/content/Intent;)Landroid/net/NetworkInfo;
aload 1
ldc "networkInfo"
invokevirtual android/content/Intent/getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;
checkcast android/net/NetworkInfo
astore 1
aload 1
ifnull L0
aload 0
aload 1
invokevirtual android/net/NetworkInfo/getType()I
invokevirtual android/net/ConnectivityManager/getNetworkInfo(I)Landroid/net/NetworkInfo;
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Landroid/net/ConnectivityManager;)Z
getstatic android/support/v4/h/a/a Landroid/support/v4/h/c;
aload 0
invokeinterface android/support/v4/h/c/a(Landroid/net/ConnectivityManager;)Z 1
ireturn
.limit locals 1
.limit stack 2
.end method
