.bytecode 50.0
.class final synchronized android/support/v4/k/c
.super java/lang/Object

.field private static final 'a' Ljava/lang/String; = "DocumentFile"

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
aload 0
aload 1
ldc "vnd.android.document/directory"
aload 2
invokestatic android/support/v4/k/c/a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
areturn
.limit locals 3
.limit stack 4
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
aload 0
invokevirtual android/content/Context/getContentResolver()Landroid/content/ContentResolver;
aload 1
aload 2
aload 3
invokestatic android/provider/DocumentsContract/createDocument(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
areturn
.limit locals 4
.limit stack 4
.end method

.method private static a(Landroid/net/Uri;)Landroid/net/Uri;
aload 0
aload 0
invokestatic android/provider/DocumentsContract/getTreeDocumentId(Landroid/net/Uri;)Ljava/lang/String;
invokestatic android/provider/DocumentsContract/buildDocumentUriUsingTree(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
areturn
.limit locals 1
.limit stack 2
.end method

.method private static a(Ljava/lang/AutoCloseable;)V
.catch java/lang/RuntimeException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
aload 0
ifnull L1
L0:
aload 0
invokeinterface java/lang/AutoCloseable/close()V 0
L1:
return
L2:
astore 0
aload 0
athrow
L3:
astore 0
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)[Landroid/net/Uri;
.catch java/lang/Exception from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/lang/Exception from L4 to L5 using L6
.catch all from L4 to L5 using L7
.catch java/lang/Exception from L8 to L9 using L6
.catch all from L8 to L9 using L7
.catch all from L10 to L11 using L7
aload 0
invokevirtual android/content/Context/getContentResolver()Landroid/content/ContentResolver;
astore 0
aload 1
aload 1
invokestatic android/provider/DocumentsContract/getDocumentId(Landroid/net/Uri;)Ljava/lang/String;
invokestatic android/provider/DocumentsContract/buildChildDocumentsUriUsingTree(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
astore 2
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 3
L0:
aload 0
aload 2
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "document_id"
aastore
aconst_null
aconst_null
aconst_null
invokevirtual android/content/ContentResolver/query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
astore 2
L1:
aload 2
astore 0
L4:
aload 2
invokeinterface android/database/Cursor/moveToNext()Z 0
ifeq L12
L5:
aload 2
astore 0
L8:
aload 3
aload 1
aload 2
iconst_0
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
invokestatic android/provider/DocumentsContract/buildDocumentUriUsingTree(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L9:
goto L1
L6:
astore 1
L13:
aload 2
astore 0
L10:
ldc "DocumentFile"
new java/lang/StringBuilder
dup
ldc "Failed query: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
L11:
aload 2
invokestatic android/support/v4/k/c/a(Ljava/lang/AutoCloseable;)V
L14:
aload 3
aload 3
invokevirtual java/util/ArrayList/size()I
anewarray android/net/Uri
invokevirtual java/util/ArrayList/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
checkcast [Landroid/net/Uri;
areturn
L12:
aload 2
invokestatic android/support/v4/k/c/a(Ljava/lang/AutoCloseable;)V
goto L14
L3:
astore 1
aconst_null
astore 0
L15:
aload 0
invokestatic android/support/v4/k/c/a(Ljava/lang/AutoCloseable;)V
aload 1
athrow
L7:
astore 1
goto L15
L2:
astore 1
aconst_null
astore 2
goto L13
.limit locals 4
.limit stack 6
.end method

.method private static b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
aload 0
invokevirtual android/content/Context/getContentResolver()Landroid/content/ContentResolver;
aload 1
aload 2
invokestatic android/provider/DocumentsContract/renameDocument(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
areturn
.limit locals 3
.limit stack 3
.end method
