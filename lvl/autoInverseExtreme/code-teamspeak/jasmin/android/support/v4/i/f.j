.bytecode 50.0
.class public final synchronized android/support/v4/i/f
.super java/lang/Object

.field public static final 'a' Ljava/lang/String; = "unknown"

.field private static final 'b' Ljava/lang/String; = "EnvironmentCompat"

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/io/File;)Ljava/lang/String;
.catch java/io/IOException from L0 to L1 using L2
getstatic android/os/Build$VERSION/SDK_INT I
bipush 19
if_icmplt L0
aload 0
invokestatic android/os/Environment/getStorageState(Ljava/io/File;)Ljava/lang/String;
areturn
L0:
aload 0
invokevirtual java/io/File/getCanonicalPath()Ljava/lang/String;
invokestatic android/os/Environment/getExternalStorageDirectory()Ljava/io/File;
invokevirtual java/io/File/getCanonicalPath()Ljava/lang/String;
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L3
invokestatic android/os/Environment/getExternalStorageState()Ljava/lang/String;
astore 0
L1:
aload 0
areturn
L2:
astore 0
ldc "EnvironmentCompat"
new java/lang/StringBuilder
dup
ldc "Failed to resolve canonical path: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
L3:
ldc "unknown"
areturn
.limit locals 1
.limit stack 4
.end method
