.bytecode 50.0
.class public synchronized abstract android/support/v4/app/bw
.super android/support/v4/view/by

.field private static final 'c' Ljava/lang/String; = "FragmentPagerAdapter"

.field private static final 'd' Z = 0


.field private final 'e' Landroid/support/v4/app/bi;

.field private 'f' Landroid/support/v4/app/cd;

.field private 'g' Landroid/support/v4/app/Fragment;

.method private <init>(Landroid/support/v4/app/bi;)V
aload 0
invokespecial android/support/v4/view/by/<init>()V
aload 0
aconst_null
putfield android/support/v4/app/bw/f Landroid/support/v4/app/cd;
aload 0
aconst_null
putfield android/support/v4/app/bw/g Landroid/support/v4/app/Fragment;
aload 0
aload 1
putfield android/support/v4/app/bw/e Landroid/support/v4/app/bi;
return
.limit locals 2
.limit stack 2
.end method

.method private static a(I)J
iload 0
i2l
lreturn
.limit locals 1
.limit stack 2
.end method

.method private static a(IJ)Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "android:switcher:"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 0
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 1
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 3
.limit stack 3
.end method

.method public abstract a()Landroid/support/v4/app/Fragment;
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
aload 0
getfield android/support/v4/app/bw/f Landroid/support/v4/app/cd;
ifnonnull L0
aload 0
aload 0
getfield android/support/v4/app/bw/e Landroid/support/v4/app/bi;
invokevirtual android/support/v4/app/bi/a()Landroid/support/v4/app/cd;
putfield android/support/v4/app/bw/f Landroid/support/v4/app/cd;
L0:
iload 2
i2l
lstore 3
aload 1
invokevirtual android/view/ViewGroup/getId()I
lload 3
invokestatic android/support/v4/app/bw/a(IJ)Ljava/lang/String;
astore 5
aload 0
getfield android/support/v4/app/bw/e Landroid/support/v4/app/bi;
aload 5
invokevirtual android/support/v4/app/bi/a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
astore 5
aload 5
ifnull L1
aload 0
getfield android/support/v4/app/bw/f Landroid/support/v4/app/cd;
aload 5
invokevirtual android/support/v4/app/cd/f(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
pop
aload 5
astore 1
L2:
aload 1
aload 0
getfield android/support/v4/app/bw/g Landroid/support/v4/app/Fragment;
if_acmpeq L3
aload 1
iconst_0
invokevirtual android/support/v4/app/Fragment/b(Z)V
aload 1
iconst_0
invokevirtual android/support/v4/app/Fragment/c(Z)V
L3:
aload 1
areturn
L1:
aload 0
invokevirtual android/support/v4/app/bw/a()Landroid/support/v4/app/Fragment;
astore 5
aload 0
getfield android/support/v4/app/bw/f Landroid/support/v4/app/cd;
aload 1
invokevirtual android/view/ViewGroup/getId()I
aload 5
aload 1
invokevirtual android/view/ViewGroup/getId()I
lload 3
invokestatic android/support/v4/app/bw/a(IJ)Ljava/lang/String;
invokevirtual android/support/v4/app/cd/a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;
pop
aload 5
astore 1
goto L2
.limit locals 6
.limit stack 6
.end method

.method public final a(ILjava/lang/Object;)V
aload 0
getfield android/support/v4/app/bw/f Landroid/support/v4/app/cd;
ifnonnull L0
aload 0
aload 0
getfield android/support/v4/app/bw/e Landroid/support/v4/app/bi;
invokevirtual android/support/v4/app/bi/a()Landroid/support/v4/app/cd;
putfield android/support/v4/app/bw/f Landroid/support/v4/app/cd;
L0:
aload 0
getfield android/support/v4/app/bw/f Landroid/support/v4/app/cd;
aload 2
checkcast android/support/v4/app/Fragment
invokevirtual android/support/v4/app/cd/e(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
pop
return
.limit locals 3
.limit stack 2
.end method

.method public final a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
return
.limit locals 3
.limit stack 0
.end method

.method public final a(Ljava/lang/Object;)V
aload 1
checkcast android/support/v4/app/Fragment
astore 1
aload 1
aload 0
getfield android/support/v4/app/bw/g Landroid/support/v4/app/Fragment;
if_acmpeq L0
aload 0
getfield android/support/v4/app/bw/g Landroid/support/v4/app/Fragment;
ifnull L1
aload 0
getfield android/support/v4/app/bw/g Landroid/support/v4/app/Fragment;
iconst_0
invokevirtual android/support/v4/app/Fragment/b(Z)V
aload 0
getfield android/support/v4/app/bw/g Landroid/support/v4/app/Fragment;
iconst_0
invokevirtual android/support/v4/app/Fragment/c(Z)V
L1:
aload 1
ifnull L2
aload 1
iconst_1
invokevirtual android/support/v4/app/Fragment/b(Z)V
aload 1
iconst_1
invokevirtual android/support/v4/app/Fragment/c(Z)V
L2:
aload 0
aload 1
putfield android/support/v4/app/bw/g Landroid/support/v4/app/Fragment;
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
aload 2
checkcast android/support/v4/app/Fragment
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
aload 1
if_acmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method public final b()V
return
.limit locals 1
.limit stack 0
.end method

.method public final c()V
aload 0
getfield android/support/v4/app/bw/f Landroid/support/v4/app/cd;
ifnull L0
aload 0
getfield android/support/v4/app/bw/f Landroid/support/v4/app/cd;
invokevirtual android/support/v4/app/cd/j()I
pop
aload 0
aconst_null
putfield android/support/v4/app/bw/f Landroid/support/v4/app/cd;
aload 0
getfield android/support/v4/app/bw/e Landroid/support/v4/app/bi;
invokevirtual android/support/v4/app/bi/b()Z
pop
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public final d()Landroid/os/Parcelable;
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method
