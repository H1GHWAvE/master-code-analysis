.bytecode 50.0
.class final synchronized android/support/v4/app/i
.super android/graphics/drawable/InsetDrawable
.implements android/graphics/drawable/Drawable$Callback

.field 'a' F

.field 'b' F

.field final synthetic 'c' Landroid/support/v4/app/a;

.field private final 'd' Z

.field private final 'e' Landroid/graphics/Rect;

.method private <init>(Landroid/support/v4/app/a;Landroid/graphics/drawable/Drawable;)V
iconst_0
istore 3
aload 0
aload 1
putfield android/support/v4/app/i/c Landroid/support/v4/app/a;
aload 0
aload 2
iconst_0
invokespecial android/graphics/drawable/InsetDrawable/<init>(Landroid/graphics/drawable/Drawable;I)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 18
if_icmple L0
iconst_1
istore 3
L0:
aload 0
iload 3
putfield android/support/v4/app/i/d Z
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/v4/app/i/e Landroid/graphics/Rect;
return
.limit locals 4
.limit stack 3
.end method

.method synthetic <init>(Landroid/support/v4/app/a;Landroid/graphics/drawable/Drawable;B)V
aload 0
aload 1
aload 2
invokespecial android/support/v4/app/i/<init>(Landroid/support/v4/app/a;Landroid/graphics/drawable/Drawable;)V
return
.limit locals 4
.limit stack 3
.end method

.method private a()F
aload 0
getfield android/support/v4/app/i/a F
freturn
.limit locals 1
.limit stack 1
.end method

.method private b(F)V
aload 0
fload 1
putfield android/support/v4/app/i/b F
aload 0
invokevirtual android/support/v4/app/i/invalidateSelf()V
return
.limit locals 2
.limit stack 2
.end method

.method public final a(F)V
aload 0
fload 1
putfield android/support/v4/app/i/a F
aload 0
invokevirtual android/support/v4/app/i/invalidateSelf()V
return
.limit locals 2
.limit stack 2
.end method

.method public final draw(Landroid/graphics/Canvas;)V
iconst_1
istore 6
aload 0
aload 0
getfield android/support/v4/app/i/e Landroid/graphics/Rect;
invokevirtual android/support/v4/app/i/copyBounds(Landroid/graphics/Rect;)V
aload 1
invokevirtual android/graphics/Canvas/save()I
pop
aload 0
getfield android/support/v4/app/i/c Landroid/support/v4/app/a;
invokestatic android/support/v4/app/a/a(Landroid/support/v4/app/a;)Landroid/app/Activity;
invokevirtual android/app/Activity/getWindow()Landroid/view/Window;
invokevirtual android/view/Window/getDecorView()Landroid/view/View;
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
iconst_1
if_icmpne L0
iconst_1
istore 5
L1:
iload 5
ifeq L2
iconst_m1
istore 6
L2:
aload 0
getfield android/support/v4/app/i/e Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/width()I
istore 7
aload 0
getfield android/support/v4/app/i/b F
fneg
fstore 2
iload 7
i2f
fstore 3
aload 0
getfield android/support/v4/app/i/a F
fstore 4
aload 1
iload 6
i2f
fload 2
fload 3
fmul
fload 4
fmul
fmul
fconst_0
invokevirtual android/graphics/Canvas/translate(FF)V
iload 5
ifeq L3
aload 0
getfield android/support/v4/app/i/d Z
ifne L3
aload 1
iload 7
i2f
fconst_0
invokevirtual android/graphics/Canvas/translate(FF)V
aload 1
ldc_w -1.0F
fconst_1
invokevirtual android/graphics/Canvas/scale(FF)V
L3:
aload 0
aload 1
invokespecial android/graphics/drawable/InsetDrawable/draw(Landroid/graphics/Canvas;)V
aload 1
invokevirtual android/graphics/Canvas/restore()V
return
L0:
iconst_0
istore 5
goto L1
.limit locals 8
.limit stack 4
.end method
