.bytecode 50.0
.class final synchronized android/support/v4/app/cn
.super java/lang/Object
.implements android/support/v4/app/cl

.field private 'a' Landroid/os/IBinder;

.method <init>(Landroid/os/IBinder;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v4/app/cn/a Landroid/os/IBinder;
return
.limit locals 2
.limit stack 2
.end method

.method private static a()Ljava/lang/String;
ldc "android.support.v4.app.INotificationSideChannel"
areturn
.limit locals 0
.limit stack 1
.end method

.method public final a(Ljava/lang/String;)V
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
L0:
aload 2
ldc "android.support.v4.app.INotificationSideChannel"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 2
aload 1
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
aload 0
getfield android/support/v4/app/cn/a Landroid/os/IBinder;
iconst_3
aload 2
aconst_null
iconst_1
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
L1:
aload 2
invokevirtual android/os/Parcel/recycle()V
return
L2:
astore 1
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
.limit locals 3
.limit stack 5
.end method

.method public final a(Ljava/lang/String;ILjava/lang/String;)V
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 4
L0:
aload 4
ldc "android.support.v4.app.INotificationSideChannel"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 4
aload 1
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
aload 4
iload 2
invokevirtual android/os/Parcel/writeInt(I)V
aload 4
aload 3
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
aload 0
getfield android/support/v4/app/cn/a Landroid/os/IBinder;
iconst_2
aload 4
aconst_null
iconst_1
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
L1:
aload 4
invokevirtual android/os/Parcel/recycle()V
return
L2:
astore 1
aload 4
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
.limit locals 5
.limit stack 5
.end method

.method public final a(Ljava/lang/String;ILjava/lang/String;Landroid/app/Notification;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 5
L0:
aload 5
ldc "android.support.v4.app.INotificationSideChannel"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 5
aload 1
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
aload 5
iload 2
invokevirtual android/os/Parcel/writeInt(I)V
aload 5
aload 3
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
L1:
aload 4
ifnull L6
L3:
aload 5
iconst_1
invokevirtual android/os/Parcel/writeInt(I)V
aload 4
aload 5
iconst_0
invokevirtual android/app/Notification/writeToParcel(Landroid/os/Parcel;I)V
L4:
aload 0
getfield android/support/v4/app/cn/a Landroid/os/IBinder;
iconst_1
aload 5
aconst_null
iconst_1
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
L5:
aload 5
invokevirtual android/os/Parcel/recycle()V
return
L6:
aload 5
iconst_0
invokevirtual android/os/Parcel/writeInt(I)V
L7:
goto L4
L2:
astore 1
aload 5
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
.limit locals 6
.limit stack 5
.end method

.method public final asBinder()Landroid/os/IBinder;
aload 0
getfield android/support/v4/app/cn/a Landroid/os/IBinder;
areturn
.limit locals 1
.limit stack 1
.end method
