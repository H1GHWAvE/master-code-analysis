.bytecode 50.0
.class public synchronized android/support/v4/app/bb
.super android/support/v4/app/as
.implements android/support/v4/app/o
.implements android/support/v4/app/v

.field static final 'a' Ljava/lang/String; = "android:support:fragments"

.field static final 'b' I = 1


.field static final 'c' I = 2


.field private static final 'm' Ljava/lang/String; = "FragmentActivity"

.field private static final 'n' I = 11


.field final 'd' Landroid/os/Handler;

.field final 'e' Landroid/support/v4/app/bg;

.field 'f' Z

.field 'g' Z

.field 'h' Z

.field 'i' Z

.field 'j' Z

.field 'k' Z

.field 'l' Z

.method public <init>()V
aload 0
invokespecial android/support/v4/app/as/<init>()V
aload 0
new android/support/v4/app/bc
dup
aload 0
invokespecial android/support/v4/app/bc/<init>(Landroid/support/v4/app/bb;)V
putfield android/support/v4/app/bb/d Landroid/os/Handler;
aload 0
new android/support/v4/app/bg
dup
new android/support/v4/app/bd
dup
aload 0
invokespecial android/support/v4/app/bd/<init>(Landroid/support/v4/app/bb;)V
invokespecial android/support/v4/app/bg/<init>(Landroid/support/v4/app/bh;)V
putfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
return
.limit locals 1
.limit stack 6
.end method

.method private static a(Landroid/view/View;)Ljava/lang/String;
.catch android/content/res/Resources$NotFoundException from L0 to L1 using L2
.catch android/content/res/Resources$NotFoundException from L1 to L3 using L2
bipush 70
istore 3
bipush 46
istore 2
new java/lang/StringBuilder
dup
sipush 128
invokespecial java/lang/StringBuilder/<init>(I)V
astore 5
aload 5
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 5
bipush 123
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
aload 5
aload 0
invokestatic java/lang/System/identityHashCode(Ljava/lang/Object;)I
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 5
bipush 32
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
aload 0
invokevirtual android/view/View/getVisibility()I
lookupswitch
0 : L4
4 : L5
8 : L6
default : L7
L7:
aload 5
bipush 46
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
L8:
aload 0
invokevirtual android/view/View/isFocusable()Z
ifeq L9
bipush 70
istore 1
L10:
aload 5
iload 1
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
aload 0
invokevirtual android/view/View/isEnabled()Z
ifeq L11
bipush 69
istore 1
L12:
aload 5
iload 1
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
aload 0
invokevirtual android/view/View/willNotDraw()Z
ifeq L13
bipush 46
istore 1
L14:
aload 5
iload 1
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
aload 0
invokevirtual android/view/View/isHorizontalScrollBarEnabled()Z
ifeq L15
bipush 72
istore 1
L16:
aload 5
iload 1
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
aload 0
invokevirtual android/view/View/isVerticalScrollBarEnabled()Z
ifeq L17
bipush 86
istore 1
L18:
aload 5
iload 1
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
aload 0
invokevirtual android/view/View/isClickable()Z
ifeq L19
bipush 67
istore 1
L20:
aload 5
iload 1
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
aload 0
invokevirtual android/view/View/isLongClickable()Z
ifeq L21
bipush 76
istore 1
L22:
aload 5
iload 1
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
aload 5
bipush 32
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
aload 0
invokevirtual android/view/View/isFocused()Z
ifeq L23
iload 3
istore 1
L24:
aload 5
iload 1
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
aload 0
invokevirtual android/view/View/isSelected()Z
ifeq L25
bipush 83
istore 1
L26:
aload 5
iload 1
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
iload 2
istore 1
aload 0
invokevirtual android/view/View/isPressed()Z
ifeq L27
bipush 80
istore 1
L27:
aload 5
iload 1
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
aload 5
bipush 32
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
aload 5
aload 0
invokevirtual android/view/View/getLeft()I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
pop
aload 5
bipush 44
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
aload 5
aload 0
invokevirtual android/view/View/getTop()I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
pop
aload 5
bipush 45
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
aload 5
aload 0
invokevirtual android/view/View/getRight()I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
pop
aload 5
bipush 44
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
aload 5
aload 0
invokevirtual android/view/View/getBottom()I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
pop
aload 0
invokevirtual android/view/View/getId()I
istore 4
iload 4
iconst_m1
if_icmpeq L3
aload 5
ldc " #"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 5
iload 4
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 0
invokevirtual android/view/View/getResources()Landroid/content/res/Resources;
astore 6
iload 4
ifeq L3
aload 6
ifnull L3
ldc_w -16777216
iload 4
iand
lookupswitch
16777216 : L28
2130706432 : L29
default : L0
L0:
aload 6
iload 4
invokevirtual android/content/res/Resources/getResourcePackageName(I)Ljava/lang/String;
astore 0
L1:
aload 6
iload 4
invokevirtual android/content/res/Resources/getResourceTypeName(I)Ljava/lang/String;
astore 7
aload 6
iload 4
invokevirtual android/content/res/Resources/getResourceEntryName(I)Ljava/lang/String;
astore 6
aload 5
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 5
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 5
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 5
aload 7
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 5
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 5
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L3:
aload 5
ldc "}"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 5
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L4:
aload 5
bipush 86
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
goto L8
L5:
aload 5
bipush 73
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
goto L8
L6:
aload 5
bipush 71
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
goto L8
L9:
bipush 46
istore 1
goto L10
L11:
bipush 46
istore 1
goto L12
L13:
bipush 68
istore 1
goto L14
L15:
bipush 46
istore 1
goto L16
L17:
bipush 46
istore 1
goto L18
L19:
bipush 46
istore 1
goto L20
L21:
bipush 46
istore 1
goto L22
L23:
bipush 46
istore 1
goto L24
L25:
bipush 46
istore 1
goto L26
L29:
ldc "app"
astore 0
goto L1
L28:
ldc "android"
astore 0
goto L1
L2:
astore 0
goto L3
.limit locals 8
.limit stack 3
.end method

.method private a(Landroid/support/v4/app/Fragment;[Ljava/lang/String;I)V
iload 3
iconst_m1
if_icmpne L0
aload 0
aload 2
iload 3
invokestatic android/support/v4/app/m/a(Landroid/app/Activity;[Ljava/lang/String;I)V
return
L0:
iload 3
sipush -256
iand
ifeq L1
new java/lang/IllegalArgumentException
dup
ldc "Can only use lower 8 bits for requestCode"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
iconst_1
putfield android/support/v4/app/bb/l Z
aload 0
aload 2
aload 1
getfield android/support/v4/app/Fragment/z I
iconst_1
iadd
bipush 8
ishl
iload 3
sipush 255
iand
iadd
invokestatic android/support/v4/app/m/a(Landroid/app/Activity;[Ljava/lang/String;I)V
return
.limit locals 4
.limit stack 5
.end method

.method static synthetic a(Landroid/support/v4/app/bb;Landroid/support/v4/app/Fragment;[Ljava/lang/String;I)V
iload 3
iconst_m1
if_icmpne L0
aload 0
aload 2
iload 3
invokestatic android/support/v4/app/m/a(Landroid/app/Activity;[Ljava/lang/String;I)V
return
L0:
iload 3
sipush -256
iand
ifeq L1
new java/lang/IllegalArgumentException
dup
ldc "Can only use lower 8 bits for requestCode"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
iconst_1
putfield android/support/v4/app/bb/l Z
aload 0
aload 2
aload 1
getfield android/support/v4/app/Fragment/z I
iconst_1
iadd
bipush 8
ishl
iload 3
sipush 255
iand
iadd
invokestatic android/support/v4/app/m/a(Landroid/app/Activity;[Ljava/lang/String;I)V
return
.limit locals 4
.limit stack 5
.end method

.method private a(Landroid/support/v4/app/gj;)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
aload 0
aload 1
invokestatic android/support/v4/app/m/a(Landroid/support/v4/app/gj;)Landroid/support/v4/app/r;
invokestatic android/support/v4/app/q/a(Landroid/support/v4/app/r;)Landroid/app/SharedElementCallback;
invokevirtual android/app/Activity/setEnterSharedElementCallback(Landroid/app/SharedElementCallback;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/String;Ljava/io/PrintWriter;Landroid/view/View;)V
aload 2
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ifnonnull L0
aload 2
ldc "null"
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
L1:
return
L0:
aload 2
aload 3
invokestatic android/support/v4/app/bb/a(Landroid/view/View;)Ljava/lang/String;
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
aload 3
instanceof android/view/ViewGroup
ifeq L1
aload 3
checkcast android/view/ViewGroup
astore 3
aload 3
invokevirtual android/view/ViewGroup/getChildCount()I
istore 5
iload 5
ifle L1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "  "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
iconst_0
istore 4
L2:
iload 4
iload 5
if_icmpge L1
aload 0
aload 1
aload 2
aload 3
iload 4
invokevirtual android/view/ViewGroup/getChildAt(I)Landroid/view/View;
invokespecial android/support/v4/app/bb/a(Ljava/lang/String;Ljava/io/PrintWriter;Landroid/view/View;)V
iload 4
iconst_1
iadd
istore 4
goto L2
.limit locals 6
.limit stack 5
.end method

.method private a(Landroid/view/View;Landroid/view/Menu;)Z
aload 0
iconst_0
aload 1
aload 2
invokespecial android/support/v4/app/as/onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
ireturn
.limit locals 3
.limit stack 4
.end method

.method private b(Landroid/support/v4/app/gj;)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
aload 0
aload 1
invokestatic android/support/v4/app/m/a(Landroid/support/v4/app/gj;)Landroid/support/v4/app/r;
invokestatic android/support/v4/app/q/a(Landroid/support/v4/app/r;)Landroid/app/SharedElementCallback;
invokevirtual android/app/Activity/setExitSharedElementCallback(Landroid/app/SharedElementCallback;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private d()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
aload 0
invokevirtual android/app/Activity/finishAfterTransition()V
return
L0:
aload 0
invokevirtual android/app/Activity/finish()V
return
.limit locals 1
.limit stack 2
.end method

.method private e()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
aload 0
invokevirtual android/app/Activity/postponeEnterTransition()V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method private f()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
aload 0
invokevirtual android/app/Activity/startPostponedEnterTransition()V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method private static g()Ljava/lang/Object;
aconst_null
areturn
.limit locals 0
.limit stack 1
.end method

.method private h()Ljava/lang/Object;
aload 0
invokevirtual android/support/v4/app/bb/getLastNonConfigurationInstance()Ljava/lang/Object;
checkcast android/support/v4/app/be
astore 1
aload 1
ifnull L0
aload 1
getfield android/support/v4/app/be/a Ljava/lang/Object;
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 1
.end method

.method private i()V
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
astore 2
aload 0
getfield android/support/v4/app/bb/j Z
istore 1
aload 2
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
astore 2
aload 2
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
ifnull L0
aload 2
getfield android/support/v4/app/bh/j Z
ifeq L0
aload 2
iconst_0
putfield android/support/v4/app/bh/j Z
iload 1
ifeq L1
aload 2
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
invokevirtual android/support/v4/app/ct/d()V
L0:
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
iconst_2
invokevirtual android/support/v4/app/bl/c(I)V
return
L1:
aload 2
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
invokevirtual android/support/v4/app/ct/c()V
goto L0
.limit locals 3
.limit stack 2
.end method

.method private static j()V
return
.limit locals 0
.limit stack 0
.end method

.method private k()Landroid/support/v4/app/cr;
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
astore 1
aload 1
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
ifnull L0
aload 1
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
areturn
L0:
aload 1
iconst_1
putfield android/support/v4/app/bh/i Z
aload 1
aload 1
ldc "(root)"
aload 1
getfield android/support/v4/app/bh/j Z
iconst_1
invokevirtual android/support/v4/app/bh/a(Ljava/lang/String;ZZ)Landroid/support/v4/app/ct;
putfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
aload 1
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
areturn
.limit locals 2
.limit stack 5
.end method

.method final a(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
aload 1
aload 2
aload 3
aload 4
invokevirtual android/support/v4/app/bl/a(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
areturn
.limit locals 5
.limit stack 5
.end method

.method public final a(I)V
aload 0
getfield android/support/v4/app/bb/l Z
ifeq L0
aload 0
iconst_0
putfield android/support/v4/app/bb/l Z
L1:
return
L0:
iload 1
sipush -256
iand
ifeq L1
new java/lang/IllegalArgumentException
dup
ldc "Can only use lower 8 bits for requestCode"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 2
.limit stack 3
.end method

.method public final a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V
iload 3
iconst_m1
if_icmpne L0
aload 0
aload 2
iconst_m1
invokespecial android/support/v4/app/as/startActivityForResult(Landroid/content/Intent;I)V
return
L0:
ldc_w -65536
iload 3
iand
ifeq L1
new java/lang/IllegalArgumentException
dup
ldc "Can only use lower 16 bits for requestCode"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
aload 2
aload 1
getfield android/support/v4/app/Fragment/z I
iconst_1
iadd
bipush 16
ishl
ldc_w 65535
iload 3
iand
iadd
invokespecial android/support/v4/app/as/startActivityForResult(Landroid/content/Intent;I)V
return
.limit locals 4
.limit stack 5
.end method

.method final a(Z)V
aload 0
getfield android/support/v4/app/bb/i Z
ifne L0
aload 0
iconst_1
putfield android/support/v4/app/bb/i Z
aload 0
iload 1
putfield android/support/v4/app/bb/j Z
aload 0
getfield android/support/v4/app/bb/d Landroid/os/Handler;
iconst_1
invokevirtual android/os/Handler/removeMessages(I)V
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
astore 2
aload 0
getfield android/support/v4/app/bb/j Z
istore 1
aload 2
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
astore 2
aload 2
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
ifnull L1
aload 2
getfield android/support/v4/app/bh/j Z
ifeq L1
aload 2
iconst_0
putfield android/support/v4/app/bh/j Z
iload 1
ifeq L2
aload 2
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
invokevirtual android/support/v4/app/ct/d()V
L1:
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
iconst_2
invokevirtual android/support/v4/app/bl/c(I)V
L0:
return
L2:
aload 2
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
invokevirtual android/support/v4/app/ct/c()V
goto L1
.limit locals 3
.limit stack 2
.end method

.method public b()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L0
aload 0
invokevirtual android/app/Activity/invalidateOptionsMenu()V
return
L0:
aload 0
iconst_1
putfield android/support/v4/app/bb/k Z
return
.limit locals 1
.limit stack 2
.end method

.method public final c()Landroid/support/v4/app/bi;
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
areturn
.limit locals 1
.limit stack 1
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
getstatic android/os/Build$VERSION/SDK_INT I
istore 5
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "Local FragmentActivity "
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
invokestatic java/lang/System/identityHashCode(Ljava/lang/Object;)I
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc " State:"
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "  "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 6
aload 3
aload 6
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mCreated="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/bb/f Z
invokevirtual java/io/PrintWriter/print(Z)V
aload 3
ldc "mResumed="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/bb/g Z
invokevirtual java/io/PrintWriter/print(Z)V
aload 3
ldc " mStopped="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/bb/h Z
invokevirtual java/io/PrintWriter/print(Z)V
aload 3
ldc " mReallyStopped="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 0
getfield android/support/v4/app/bb/i Z
invokevirtual java/io/PrintWriter/println(Z)V
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
astore 7
aload 3
aload 6
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "mLoadersStarted="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 7
getfield android/support/v4/app/bh/j Z
invokevirtual java/io/PrintWriter/println(Z)V
aload 7
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
ifnull L0
aload 3
aload 6
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "Loader Manager "
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
aload 7
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
invokestatic java/lang/System/identityHashCode(Ljava/lang/Object;)I
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc ":"
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
aload 7
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "  "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 2
aload 3
aload 4
invokevirtual android/support/v4/app/ct/a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
L0:
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
aload 1
aload 2
aload 3
aload 4
invokevirtual android/support/v4/app/bi/a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
aload 3
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 3
ldc "View Hierarchy:"
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "  "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 3
aload 0
invokevirtual android/support/v4/app/bb/getWindow()Landroid/view/Window;
invokevirtual android/view/Window/getDecorView()Landroid/view/View;
invokespecial android/support/v4/app/bb/a(Ljava/lang/String;Ljava/io/PrintWriter;Landroid/view/View;)V
return
.limit locals 8
.limit stack 5
.end method

.method protected final o_()V
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/q()V
return
.limit locals 1
.limit stack 1
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
invokevirtual android/support/v4/app/bg/b()V
iload 1
bipush 16
ishr
istore 4
iload 4
ifeq L0
iload 4
iconst_1
isub
istore 2
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
invokevirtual android/support/v4/app/bg/a()I
istore 4
iload 4
ifeq L1
iload 2
iflt L1
iload 2
iload 4
if_icmplt L2
L1:
ldc "FragmentActivity"
new java/lang/StringBuilder
dup
ldc "Activity result fragment index out of range: 0x"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 1
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
return
L2:
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
new java/util/ArrayList
dup
iload 4
invokespecial java/util/ArrayList/<init>(I)V
invokevirtual android/support/v4/app/bg/a(Ljava/util/List;)Ljava/util/List;
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/support/v4/app/Fragment
ifnonnull L3
ldc "FragmentActivity"
new java/lang/StringBuilder
dup
ldc "Activity result no fragment exists for index: 0x"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 1
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
return
L3:
invokestatic android/support/v4/app/Fragment/o()V
return
L0:
aload 0
iload 1
iload 2
aload 3
invokespecial android/support/v4/app/as/onActivityResult(IILandroid/content/Intent;)V
return
.limit locals 5
.limit stack 4
.end method

.method public onBackPressed()V
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bi/d()Z
ifne L0
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L1
aload 0
invokevirtual android/app/Activity/finishAfterTransition()V
L0:
return
L1:
aload 0
invokevirtual android/app/Activity/finish()V
return
.limit locals 1
.limit stack 2
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
aload 0
aload 1
invokespecial android/support/v4/app/as/onConfigurationChanged(Landroid/content/res/Configuration;)V
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
aload 1
invokevirtual android/support/v4/app/bl/a(Landroid/content/res/Configuration;)V
return
.limit locals 2
.limit stack 2
.end method

.method public onCreate(Landroid/os/Bundle;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
astore 2
aload 2
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
aload 2
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
aload 2
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
aconst_null
invokevirtual android/support/v4/app/bl/a(Landroid/support/v4/app/bh;Landroid/support/v4/app/bf;Landroid/support/v4/app/Fragment;)V
aload 0
aload 1
invokespecial android/support/v4/app/as/onCreate(Landroid/os/Bundle;)V
aload 0
invokevirtual android/support/v4/app/bb/getLastNonConfigurationInstance()Ljava/lang/Object;
checkcast android/support/v4/app/be
astore 2
aload 2
ifnull L0
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
astore 3
aload 2
getfield android/support/v4/app/be/c Landroid/support/v4/n/v;
astore 4
aload 3
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
aload 4
putfield android/support/v4/app/bh/g Landroid/support/v4/n/v;
L0:
aload 1
ifnull L1
aload 1
ldc "android:support:fragments"
invokevirtual android/os/Bundle/getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
astore 3
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
astore 4
aload 2
ifnull L2
aload 2
getfield android/support/v4/app/be/b Ljava/util/List;
astore 1
L3:
aload 4
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
aload 3
aload 1
invokevirtual android/support/v4/app/bl/a(Landroid/os/Parcelable;Ljava/util/List;)V
L1:
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/n()V
return
L2:
aconst_null
astore 1
goto L3
.limit locals 5
.limit stack 4
.end method

.method public onCreatePanelMenu(ILandroid/view/Menu;)Z
iload 1
ifne L0
aload 0
iload 1
aload 2
invokespecial android/support/v4/app/as/onCreatePanelMenu(ILandroid/view/Menu;)Z
istore 3
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
astore 5
aload 0
invokevirtual android/support/v4/app/bb/getMenuInflater()Landroid/view/MenuInflater;
astore 6
aload 5
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
aload 2
aload 6
invokevirtual android/support/v4/app/bl/a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
istore 4
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L1
iload 3
iload 4
ior
ireturn
L1:
iconst_1
ireturn
L0:
aload 0
iload 1
aload 2
invokespecial android/support/v4/app/as/onCreatePanelMenu(ILandroid/view/Menu;)Z
ireturn
.limit locals 7
.limit stack 3
.end method

.method public volatile synthetic onCreateView(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
aload 0
aload 1
aload 2
aload 3
aload 4
invokespecial android/support/v4/app/as/onCreateView(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
areturn
.limit locals 5
.limit stack 5
.end method

.method public volatile synthetic onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
aload 0
aload 1
aload 2
aload 3
invokespecial android/support/v4/app/as/onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
areturn
.limit locals 4
.limit stack 4
.end method

.method public onDestroy()V
aload 0
invokespecial android/support/v4/app/as/onDestroy()V
aload 0
iconst_0
invokevirtual android/support/v4/app/bb/a(Z)V
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/s()V
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
astore 1
aload 1
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
ifnull L0
aload 1
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
invokevirtual android/support/v4/app/ct/g()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
getstatic android/os/Build$VERSION/SDK_INT I
iconst_5
if_icmpge L0
iload 1
iconst_4
if_icmpne L0
aload 2
invokevirtual android/view/KeyEvent/getRepeatCount()I
ifne L0
aload 0
invokevirtual android/support/v4/app/bb/onBackPressed()V
iconst_1
ireturn
L0:
aload 0
iload 1
aload 2
invokespecial android/support/v4/app/as/onKeyDown(ILandroid/view/KeyEvent;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method public onLowMemory()V
aload 0
invokespecial android/support/v4/app/as/onLowMemory()V
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/t()V
return
.limit locals 1
.limit stack 1
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
aload 0
iload 1
aload 2
invokespecial android/support/v4/app/as/onMenuItemSelected(ILandroid/view/MenuItem;)Z
ifeq L0
iconst_1
ireturn
L0:
iload 1
lookupswitch
0 : L1
6 : L2
default : L3
L3:
iconst_0
ireturn
L1:
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
aload 2
invokevirtual android/support/v4/app/bl/a(Landroid/view/MenuItem;)Z
ireturn
L2:
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
aload 2
invokevirtual android/support/v4/app/bl/b(Landroid/view/MenuItem;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method public onNewIntent(Landroid/content/Intent;)V
aload 0
aload 1
invokespecial android/support/v4/app/as/onNewIntent(Landroid/content/Intent;)V
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
invokevirtual android/support/v4/app/bg/b()V
return
.limit locals 2
.limit stack 2
.end method

.method public onPanelClosed(ILandroid/view/Menu;)V
iload 1
tableswitch 0
L0
default : L1
L1:
aload 0
iload 1
aload 2
invokespecial android/support/v4/app/as/onPanelClosed(ILandroid/view/Menu;)V
return
L0:
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
aload 2
invokevirtual android/support/v4/app/bl/b(Landroid/view/Menu;)V
goto L1
.limit locals 3
.limit stack 3
.end method

.method public onPause()V
aload 0
invokespecial android/support/v4/app/as/onPause()V
aload 0
iconst_0
putfield android/support/v4/app/bb/g Z
aload 0
getfield android/support/v4/app/bb/d Landroid/os/Handler;
iconst_2
invokevirtual android/os/Handler/hasMessages(I)Z
ifeq L0
aload 0
getfield android/support/v4/app/bb/d Landroid/os/Handler;
iconst_2
invokevirtual android/os/Handler/removeMessages(I)V
aload 0
invokevirtual android/support/v4/app/bb/o_()V
L0:
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
iconst_4
invokevirtual android/support/v4/app/bl/c(I)V
return
.limit locals 1
.limit stack 2
.end method

.method public onPostResume()V
aload 0
invokespecial android/support/v4/app/as/onPostResume()V
aload 0
getfield android/support/v4/app/bb/d Landroid/os/Handler;
iconst_2
invokevirtual android/os/Handler/removeMessages(I)V
aload 0
invokevirtual android/support/v4/app/bb/o_()V
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
invokevirtual android/support/v4/app/bg/c()Z
pop
return
.limit locals 1
.limit stack 2
.end method

.method public onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
iload 1
ifne L0
aload 3
ifnull L0
aload 0
getfield android/support/v4/app/bb/k Z
ifeq L1
aload 0
iconst_0
putfield android/support/v4/app/bb/k Z
aload 3
invokeinterface android/view/Menu/clear()V 0
aload 0
iload 1
aload 3
invokevirtual android/support/v4/app/bb/onCreatePanelMenu(ILandroid/view/Menu;)Z
pop
L1:
aload 0
iconst_0
aload 2
aload 3
invokespecial android/support/v4/app/as/onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
aload 3
invokevirtual android/support/v4/app/bl/a(Landroid/view/Menu;)Z
ior
ireturn
L0:
aload 0
iload 1
aload 2
aload 3
invokespecial android/support/v4/app/as/onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
ireturn
.limit locals 4
.limit stack 4
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
.annotation invisibleparam 2 Landroid/support/a/y;
.end annotation
.annotation invisibleparam 3 Landroid/support/a/y;
.end annotation
iload 1
bipush 8
ishr
sipush 255
iand
istore 4
iload 4
ifeq L0
iload 4
iconst_1
isub
istore 4
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
invokevirtual android/support/v4/app/bg/a()I
istore 5
iload 5
ifeq L1
iload 4
iflt L1
iload 4
iload 5
if_icmplt L2
L1:
ldc "FragmentActivity"
new java/lang/StringBuilder
dup
ldc "Activity result fragment index out of range: 0x"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 1
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
L0:
return
L2:
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
new java/util/ArrayList
dup
iload 5
invokespecial java/util/ArrayList/<init>(I)V
invokevirtual android/support/v4/app/bg/a(Ljava/util/List;)Ljava/util/List;
iload 4
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/support/v4/app/Fragment
ifnonnull L3
ldc "FragmentActivity"
new java/lang/StringBuilder
dup
ldc "Activity result no fragment exists for index: 0x"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 1
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
return
L3:
invokestatic android/support/v4/app/Fragment/p()V
return
.limit locals 6
.limit stack 4
.end method

.method public onResume()V
aload 0
invokespecial android/support/v4/app/as/onResume()V
aload 0
getfield android/support/v4/app/bb/d Landroid/os/Handler;
iconst_2
invokevirtual android/os/Handler/sendEmptyMessage(I)Z
pop
aload 0
iconst_1
putfield android/support/v4/app/bb/g Z
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
invokevirtual android/support/v4/app/bg/c()Z
pop
return
.limit locals 1
.limit stack 2
.end method

.method public final onRetainNonConfigurationInstance()Ljava/lang/Object;
aload 0
getfield android/support/v4/app/bb/h Z
ifeq L0
aload 0
iconst_1
invokevirtual android/support/v4/app/bb/a(Z)V
L0:
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
astore 6
aload 6
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
ifnull L1
iconst_0
istore 1
aconst_null
astore 3
L2:
aload 3
astore 4
iload 1
aload 6
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L3
aload 6
getfield android/support/v4/app/bl/l Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 7
aload 3
astore 5
aload 7
ifnull L4
aload 3
astore 5
aload 7
getfield android/support/v4/app/Fragment/V Z
ifeq L4
aload 3
astore 4
aload 3
ifnonnull L5
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 4
L5:
aload 4
aload 7
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 7
iconst_1
putfield android/support/v4/app/Fragment/W Z
aload 7
getfield android/support/v4/app/Fragment/C Landroid/support/v4/app/Fragment;
ifnull L6
aload 7
getfield android/support/v4/app/Fragment/C Landroid/support/v4/app/Fragment;
getfield android/support/v4/app/Fragment/z I
istore 2
L7:
aload 7
iload 2
putfield android/support/v4/app/Fragment/D I
aload 4
astore 5
getstatic android/support/v4/app/bl/b Z
ifeq L4
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "retainNonConfig: keeping retained "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 7
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 4
astore 5
L4:
iload 1
iconst_1
iadd
istore 1
aload 5
astore 3
goto L2
L6:
iconst_m1
istore 2
goto L7
L1:
aconst_null
astore 4
L3:
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
invokevirtual android/support/v4/app/bh/i()Landroid/support/v4/n/v;
astore 3
aload 4
ifnonnull L8
aload 3
ifnonnull L8
aconst_null
areturn
L8:
new android/support/v4/app/be
dup
invokespecial android/support/v4/app/be/<init>()V
astore 5
aload 5
aconst_null
putfield android/support/v4/app/be/a Ljava/lang/Object;
aload 5
aload 4
putfield android/support/v4/app/be/b Ljava/util/List;
aload 5
aload 3
putfield android/support/v4/app/be/c Landroid/support/v4/n/v;
aload 5
areturn
.limit locals 8
.limit stack 4
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/support/v4/app/as/onSaveInstanceState(Landroid/os/Bundle;)V
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/m()Landroid/os/Parcelable;
astore 2
aload 2
ifnull L0
aload 1
ldc "android:support:fragments"
aload 2
invokevirtual android/os/Bundle/putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
L0:
return
.limit locals 3
.limit stack 3
.end method

.method protected onStart()V
aload 0
invokespecial android/support/v4/app/as/onStart()V
aload 0
iconst_0
putfield android/support/v4/app/bb/h Z
aload 0
iconst_0
putfield android/support/v4/app/bb/i Z
aload 0
getfield android/support/v4/app/bb/d Landroid/os/Handler;
iconst_1
invokevirtual android/os/Handler/removeMessages(I)V
aload 0
getfield android/support/v4/app/bb/f Z
ifne L0
aload 0
iconst_1
putfield android/support/v4/app/bb/f Z
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/o()V
L0:
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
invokevirtual android/support/v4/app/bg/b()V
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
invokevirtual android/support/v4/app/bg/c()Z
pop
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
astore 4
aload 4
getfield android/support/v4/app/bh/j Z
ifne L1
aload 4
iconst_1
putfield android/support/v4/app/bh/j Z
aload 4
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
ifnull L2
aload 4
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
invokevirtual android/support/v4/app/ct/b()V
L3:
aload 4
iconst_1
putfield android/support/v4/app/bh/i Z
L1:
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/p()V
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
astore 5
aload 5
getfield android/support/v4/app/bh/g Landroid/support/v4/n/v;
ifnull L4
aload 5
getfield android/support/v4/app/bh/g Landroid/support/v4/n/v;
invokevirtual android/support/v4/n/v/size()I
istore 3
iload 3
anewarray android/support/v4/app/ct
astore 4
iload 3
iconst_1
isub
istore 1
L5:
iload 1
iflt L6
aload 4
iload 1
aload 5
getfield android/support/v4/app/bh/g Landroid/support/v4/n/v;
iload 1
invokevirtual android/support/v4/n/v/c(I)Ljava/lang/Object;
checkcast android/support/v4/app/ct
aastore
iload 1
iconst_1
isub
istore 1
goto L5
L2:
aload 4
getfield android/support/v4/app/bh/i Z
ifne L3
aload 4
aload 4
ldc "(root)"
aload 4
getfield android/support/v4/app/bh/j Z
iconst_0
invokevirtual android/support/v4/app/bh/a(Ljava/lang/String;ZZ)Landroid/support/v4/app/ct;
putfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
aload 4
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
ifnull L3
aload 4
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
getfield android/support/v4/app/ct/f Z
ifne L3
aload 4
getfield android/support/v4/app/bh/h Landroid/support/v4/app/ct;
invokevirtual android/support/v4/app/ct/b()V
goto L3
L6:
iconst_0
istore 1
L7:
iload 1
iload 3
if_icmpge L4
aload 4
iload 1
aaload
astore 5
aload 5
getfield android/support/v4/app/ct/g Z
ifeq L8
getstatic android/support/v4/app/ct/b Z
ifeq L9
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "Finished Retaining in "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L9:
aload 5
iconst_0
putfield android/support/v4/app/ct/g Z
aload 5
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
invokevirtual android/support/v4/n/w/a()I
iconst_1
isub
istore 2
L10:
iload 2
iflt L8
aload 5
getfield android/support/v4/app/ct/c Landroid/support/v4/n/w;
iload 2
invokevirtual android/support/v4/n/w/d(I)Ljava/lang/Object;
checkcast android/support/v4/app/cu
astore 6
aload 6
getfield android/support/v4/app/cu/i Z
ifeq L11
getstatic android/support/v4/app/ct/b Z
ifeq L12
ldc "LoaderManager"
new java/lang/StringBuilder
dup
ldc "  Finished Retaining: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L12:
aload 6
iconst_0
putfield android/support/v4/app/cu/i Z
aload 6
getfield android/support/v4/app/cu/h Z
aload 6
getfield android/support/v4/app/cu/j Z
if_icmpeq L11
aload 6
getfield android/support/v4/app/cu/h Z
ifne L11
aload 6
invokevirtual android/support/v4/app/cu/b()V
L11:
aload 6
getfield android/support/v4/app/cu/h Z
ifeq L13
aload 6
getfield android/support/v4/app/cu/e Z
ifeq L13
aload 6
getfield android/support/v4/app/cu/k Z
ifne L13
aload 6
aload 6
getfield android/support/v4/app/cu/d Landroid/support/v4/c/aa;
aload 6
getfield android/support/v4/app/cu/g Ljava/lang/Object;
invokevirtual android/support/v4/app/cu/b(Landroid/support/v4/c/aa;Ljava/lang/Object;)V
L13:
iload 2
iconst_1
isub
istore 2
goto L10
L8:
aload 5
invokevirtual android/support/v4/app/ct/f()V
iload 1
iconst_1
iadd
istore 1
goto L7
L4:
return
.limit locals 7
.limit stack 5
.end method

.method public onStateNotSaved()V
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
invokevirtual android/support/v4/app/bg/b()V
return
.limit locals 1
.limit stack 1
.end method

.method public onStop()V
aload 0
invokespecial android/support/v4/app/as/onStop()V
aload 0
iconst_1
putfield android/support/v4/app/bb/h Z
aload 0
getfield android/support/v4/app/bb/d Landroid/os/Handler;
iconst_1
invokevirtual android/os/Handler/sendEmptyMessage(I)Z
pop
aload 0
getfield android/support/v4/app/bb/e Landroid/support/v4/app/bg;
getfield android/support/v4/app/bg/a Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/f Landroid/support/v4/app/bl;
invokevirtual android/support/v4/app/bl/r()V
return
.limit locals 1
.limit stack 2
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
iload 2
iconst_m1
if_icmpeq L0
ldc_w -65536
iload 2
iand
ifeq L0
new java/lang/IllegalArgumentException
dup
ldc "Can only use lower 16 bits for requestCode"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
iload 2
invokespecial android/support/v4/app/as/startActivityForResult(Landroid/content/Intent;I)V
return
.limit locals 3
.limit stack 3
.end method
