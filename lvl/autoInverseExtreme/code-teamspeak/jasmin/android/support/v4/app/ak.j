.bytecode 50.0
.class final synchronized android/support/v4/app/ak
.super android/support/v4/app/cd
.implements android/support/v4/app/bj
.implements java/lang/Runnable

.field static final 'a' Ljava/lang/String; = "FragmentManager"

.field static final 'b' Z

.field static final 'd' I = 0


.field static final 'e' I = 1


.field static final 'f' I = 2


.field static final 'g' I = 3


.field static final 'h' I = 4


.field static final 'i' I = 5


.field static final 'j' I = 6


.field static final 'k' I = 7


.field 'A' Ljava/lang/CharSequence;

.field 'B' I

.field 'C' Ljava/lang/CharSequence;

.field 'D' Ljava/util/ArrayList;

.field 'E' Ljava/util/ArrayList;

.field final 'c' Landroid/support/v4/app/bl;

.field 'l' Landroid/support/v4/app/ao;

.field 'm' Landroid/support/v4/app/ao;

.field 'n' I

.field 'o' I

.field 'p' I

.field 'q' I

.field 'r' I

.field 's' I

.field 't' I

.field 'u' Z

.field 'v' Z

.field 'w' Ljava/lang/String;

.field 'x' Z

.field 'y' I

.field 'z' I

.method static <clinit>()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
iconst_1
istore 0
L1:
iload 0
putstatic android/support/v4/app/ak/b Z
return
L0:
iconst_0
istore 0
goto L1
.limit locals 1
.limit stack 2
.end method

.method public <init>(Landroid/support/v4/app/bl;)V
aload 0
invokespecial android/support/v4/app/cd/<init>()V
aload 0
iconst_1
putfield android/support/v4/app/ak/v Z
aload 0
iconst_m1
putfield android/support/v4/app/ak/y I
aload 0
aload 1
putfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Z)I
aload 0
getfield android/support/v4/app/ak/x Z
ifeq L0
new java/lang/IllegalStateException
dup
ldc "commit already called"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
getstatic android/support/v4/app/bl/b Z
ifeq L1
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "Commit: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
ldc "  "
new java/io/PrintWriter
dup
new android/support/v4/n/h
dup
ldc "FragmentManager"
invokespecial android/support/v4/n/h/<init>(Ljava/lang/String;)V
invokespecial java/io/PrintWriter/<init>(Ljava/io/Writer;)V
invokevirtual android/support/v4/app/ak/a(Ljava/lang/String;Ljava/io/PrintWriter;)V
L1:
aload 0
iconst_1
putfield android/support/v4/app/ak/x Z
aload 0
getfield android/support/v4/app/ak/u Z
ifeq L2
aload 0
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
aload 0
invokevirtual android/support/v4/app/bl/a(Landroid/support/v4/app/ak;)I
putfield android/support/v4/app/ak/y I
L3:
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
aload 0
iload 1
invokevirtual android/support/v4/app/bl/a(Ljava/lang/Runnable;Z)V
aload 0
getfield android/support/v4/app/ak/y I
ireturn
L2:
aload 0
iconst_m1
putfield android/support/v4/app/ak/y I
goto L3
.limit locals 2
.limit stack 7
.end method

.method private a(Landroid/util/SparseArray;Landroid/util/SparseArray;Z)Landroid/support/v4/app/ap;
iconst_0
istore 8
new android/support/v4/app/ap
dup
aload 0
invokespecial android/support/v4/app/ap/<init>(Landroid/support/v4/app/ak;)V
astore 9
aload 9
new android/view/View
dup
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
getfield android/support/v4/app/bl/u Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/c Landroid/content/Context;
invokespecial android/view/View/<init>(Landroid/content/Context;)V
putfield android/support/v4/app/ap/d Landroid/view/View;
iconst_0
istore 5
iconst_0
istore 4
L0:
iload 8
istore 6
iload 4
istore 7
iload 5
aload 1
invokevirtual android/util/SparseArray/size()I
if_icmpge L1
aload 0
aload 1
iload 5
invokevirtual android/util/SparseArray/keyAt(I)I
aload 9
iload 3
aload 1
aload 2
invokespecial android/support/v4/app/ak/a(ILandroid/support/v4/app/ap;ZLandroid/util/SparseArray;Landroid/util/SparseArray;)Z
ifeq L2
iconst_1
istore 4
L3:
iload 5
iconst_1
iadd
istore 5
goto L0
L1:
iload 6
aload 2
invokevirtual android/util/SparseArray/size()I
if_icmpge L4
aload 2
iload 6
invokevirtual android/util/SparseArray/keyAt(I)I
istore 5
iload 7
istore 4
aload 1
iload 5
invokevirtual android/util/SparseArray/get(I)Ljava/lang/Object;
ifnonnull L5
iload 7
istore 4
aload 0
iload 5
aload 9
iload 3
aload 1
aload 2
invokespecial android/support/v4/app/ak/a(ILandroid/support/v4/app/ap;ZLandroid/util/SparseArray;Landroid/util/SparseArray;)Z
ifeq L5
iconst_1
istore 4
L5:
iload 6
iconst_1
iadd
istore 6
iload 4
istore 7
goto L1
L4:
aload 9
astore 1
iload 7
ifne L6
aconst_null
astore 1
L6:
aload 1
areturn
L2:
goto L3
.limit locals 10
.limit stack 6
.end method

.method static synthetic a(Landroid/support/v4/app/ak;Landroid/support/v4/app/ap;ZLandroid/support/v4/app/Fragment;)Landroid/support/v4/n/a;
new android/support/v4/n/a
dup
invokespecial android/support/v4/n/a/<init>()V
astore 5
aload 3
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
astore 6
aload 5
astore 4
aload 6
ifnull L0
aload 5
astore 4
aload 0
getfield android/support/v4/app/ak/D Ljava/util/ArrayList;
ifnull L0
aload 5
aload 6
invokestatic android/support/v4/app/ce/a(Ljava/util/Map;Landroid/view/View;)V
iload 2
ifeq L1
aload 0
getfield android/support/v4/app/ak/D Ljava/util/ArrayList;
aload 0
getfield android/support/v4/app/ak/E Ljava/util/ArrayList;
aload 5
invokestatic android/support/v4/app/ak/a(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/support/v4/n/a;)Landroid/support/v4/n/a;
astore 4
L0:
iload 2
ifeq L2
aload 3
getfield android/support/v4/app/Fragment/as Landroid/support/v4/app/gj;
ifnull L3
aload 3
getfield android/support/v4/app/Fragment/as Landroid/support/v4/app/gj;
astore 3
L3:
aload 0
aload 1
aload 4
iconst_1
invokespecial android/support/v4/app/ak/a(Landroid/support/v4/app/ap;Landroid/support/v4/n/a;Z)V
aload 4
areturn
L1:
aload 5
aload 0
getfield android/support/v4/app/ak/E Ljava/util/ArrayList;
invokestatic android/support/v4/n/k/c(Ljava/util/Map;Ljava/util/Collection;)Z
pop
aload 5
astore 4
goto L0
L2:
aload 3
getfield android/support/v4/app/Fragment/ar Landroid/support/v4/app/gj;
ifnull L4
aload 3
getfield android/support/v4/app/Fragment/ar Landroid/support/v4/app/gj;
astore 0
L4:
aload 1
aload 4
iconst_1
invokestatic android/support/v4/app/ak/b(Landroid/support/v4/app/ap;Landroid/support/v4/n/a;Z)V
aload 4
areturn
.limit locals 7
.limit stack 4
.end method

.method private a(Landroid/support/v4/app/ap;Landroid/support/v4/app/Fragment;Z)Landroid/support/v4/n/a;
new android/support/v4/n/a
dup
invokespecial android/support/v4/n/a/<init>()V
astore 5
aload 5
astore 4
aload 0
getfield android/support/v4/app/ak/D Ljava/util/ArrayList;
ifnull L0
aload 5
aload 2
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
invokestatic android/support/v4/app/ce/a(Ljava/util/Map;Landroid/view/View;)V
iload 3
ifeq L1
aload 5
aload 0
getfield android/support/v4/app/ak/E Ljava/util/ArrayList;
invokestatic android/support/v4/n/k/c(Ljava/util/Map;Ljava/util/Collection;)Z
pop
aload 5
astore 4
L0:
iload 3
ifeq L2
aload 2
getfield android/support/v4/app/Fragment/ar Landroid/support/v4/app/gj;
ifnull L3
aload 2
getfield android/support/v4/app/Fragment/ar Landroid/support/v4/app/gj;
astore 2
L3:
aload 0
aload 1
aload 4
iconst_0
invokespecial android/support/v4/app/ak/a(Landroid/support/v4/app/ap;Landroid/support/v4/n/a;Z)V
aload 4
areturn
L1:
aload 0
getfield android/support/v4/app/ak/D Ljava/util/ArrayList;
aload 0
getfield android/support/v4/app/ak/E Ljava/util/ArrayList;
aload 5
invokestatic android/support/v4/app/ak/a(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/support/v4/n/a;)Landroid/support/v4/n/a;
astore 4
goto L0
L2:
aload 2
getfield android/support/v4/app/Fragment/as Landroid/support/v4/app/gj;
ifnull L4
aload 2
getfield android/support/v4/app/Fragment/as Landroid/support/v4/app/gj;
astore 2
L4:
aload 1
aload 4
iconst_0
invokestatic android/support/v4/app/ak/b(Landroid/support/v4/app/ap;Landroid/support/v4/n/a;Z)V
aload 4
areturn
.limit locals 6
.limit stack 4
.end method

.method private a(Landroid/support/v4/app/ap;ZLandroid/support/v4/app/Fragment;)Landroid/support/v4/n/a;
new android/support/v4/n/a
dup
invokespecial android/support/v4/n/a/<init>()V
astore 5
aload 3
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
astore 6
aload 5
astore 4
aload 6
ifnull L0
aload 5
astore 4
aload 0
getfield android/support/v4/app/ak/D Ljava/util/ArrayList;
ifnull L0
aload 5
aload 6
invokestatic android/support/v4/app/ce/a(Ljava/util/Map;Landroid/view/View;)V
iload 2
ifeq L1
aload 0
getfield android/support/v4/app/ak/D Ljava/util/ArrayList;
aload 0
getfield android/support/v4/app/ak/E Ljava/util/ArrayList;
aload 5
invokestatic android/support/v4/app/ak/a(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/support/v4/n/a;)Landroid/support/v4/n/a;
astore 4
L0:
iload 2
ifeq L2
aload 3
getfield android/support/v4/app/Fragment/as Landroid/support/v4/app/gj;
ifnull L3
aload 3
getfield android/support/v4/app/Fragment/as Landroid/support/v4/app/gj;
astore 3
L3:
aload 0
aload 1
aload 4
iconst_1
invokespecial android/support/v4/app/ak/a(Landroid/support/v4/app/ap;Landroid/support/v4/n/a;Z)V
aload 4
areturn
L1:
aload 5
aload 0
getfield android/support/v4/app/ak/E Ljava/util/ArrayList;
invokestatic android/support/v4/n/k/c(Ljava/util/Map;Ljava/util/Collection;)Z
pop
aload 5
astore 4
goto L0
L2:
aload 3
getfield android/support/v4/app/Fragment/ar Landroid/support/v4/app/gj;
ifnull L4
aload 3
getfield android/support/v4/app/Fragment/ar Landroid/support/v4/app/gj;
astore 3
L4:
aload 1
aload 4
iconst_1
invokestatic android/support/v4/app/ak/b(Landroid/support/v4/app/ap;Landroid/support/v4/n/a;Z)V
aload 4
areturn
.limit locals 7
.limit stack 4
.end method

.method private static a(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/support/v4/n/a;)Landroid/support/v4/n/a;
aload 2
invokevirtual android/support/v4/n/a/isEmpty()Z
ifeq L0
aload 2
areturn
L0:
new android/support/v4/n/a
dup
invokespecial android/support/v4/n/a/<init>()V
astore 5
aload 0
invokevirtual java/util/ArrayList/size()I
istore 4
iconst_0
istore 3
L1:
iload 3
iload 4
if_icmpge L2
aload 2
aload 0
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
invokevirtual android/support/v4/n/a/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/view/View
astore 6
aload 6
ifnull L3
aload 5
aload 1
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
aload 6
invokevirtual android/support/v4/n/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L3:
iload 3
iconst_1
iadd
istore 3
goto L1
L2:
aload 5
areturn
.limit locals 7
.limit stack 3
.end method

.method private static a(Landroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;Z)Ljava/lang/Object;
aload 0
ifnull L0
aload 1
ifnonnull L1
L0:
aconst_null
areturn
L1:
iload 2
ifeq L2
aload 1
astore 0
aload 1
getfield android/support/v4/app/Fragment/ao Ljava/lang/Object;
getstatic android/support/v4/app/Fragment/n Ljava/lang/Object;
if_acmpeq L2
aload 1
getfield android/support/v4/app/Fragment/ao Ljava/lang/Object;
astore 0
L3:
aload 0
ifnonnull L4
aconst_null
areturn
L2:
aload 0
getfield android/support/v4/app/Fragment/an Ljava/lang/Object;
astore 0
goto L3
L4:
aload 0
checkcast android/transition/Transition
astore 0
aload 0
ifnonnull L5
aconst_null
areturn
L5:
new android/transition/TransitionSet
dup
invokespecial android/transition/TransitionSet/<init>()V
astore 1
aload 1
aload 0
invokevirtual android/transition/TransitionSet/addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;
pop
aload 1
areturn
.limit locals 3
.limit stack 2
.end method

.method private static a(Landroid/support/v4/app/Fragment;Z)Ljava/lang/Object;
aload 0
ifnonnull L0
aconst_null
areturn
L0:
iload 1
ifeq L1
aload 0
getfield android/support/v4/app/Fragment/am Ljava/lang/Object;
getstatic android/support/v4/app/Fragment/n Ljava/lang/Object;
if_acmpne L2
aload 0
getfield android/support/v4/app/Fragment/al Ljava/lang/Object;
astore 0
L3:
aload 0
invokestatic android/support/v4/app/ce/a(Ljava/lang/Object;)Ljava/lang/Object;
areturn
L2:
aload 0
getfield android/support/v4/app/Fragment/am Ljava/lang/Object;
astore 0
goto L3
L1:
aload 0
getfield android/support/v4/app/Fragment/aj Ljava/lang/Object;
astore 0
goto L3
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;Landroid/support/v4/app/Fragment;Ljava/util/ArrayList;Landroid/support/v4/n/a;Landroid/view/View;)Ljava/lang/Object;
aload 0
astore 5
aload 0
ifnull L0
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
astore 1
aload 0
astore 5
aload 0
ifnull L0
aload 2
aload 1
invokestatic android/support/v4/app/ce/a(Ljava/util/ArrayList;Landroid/view/View;)V
aload 3
ifnull L1
aload 2
aload 3
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokevirtual java/util/ArrayList/removeAll(Ljava/util/Collection;)Z
pop
L1:
aload 2
invokevirtual java/util/ArrayList/isEmpty()Z
ifeq L2
aconst_null
astore 5
L0:
aload 5
areturn
L2:
aload 2
aload 4
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
checkcast android/transition/Transition
aload 2
invokestatic android/support/v4/app/ce/b(Ljava/lang/Object;Ljava/util/ArrayList;)V
aload 0
areturn
.limit locals 6
.limit stack 2
.end method

.method private a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;I)V
aload 2
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
putfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
aload 3
ifnull L0
aload 2
getfield android/support/v4/app/Fragment/S Ljava/lang/String;
ifnull L1
aload 3
aload 2
getfield android/support/v4/app/Fragment/S Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L1
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Can't change tag of fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ": was "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
getfield android/support/v4/app/Fragment/S Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " now "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 2
aload 3
putfield android/support/v4/app/Fragment/S Ljava/lang/String;
L0:
iload 1
ifeq L2
aload 2
getfield android/support/v4/app/Fragment/Q I
ifeq L3
aload 2
getfield android/support/v4/app/Fragment/Q I
iload 1
if_icmpeq L3
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
ldc "Can't change container ID of fragment "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ": was "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
getfield android/support/v4/app/Fragment/Q I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " now "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 2
iload 1
putfield android/support/v4/app/Fragment/Q I
aload 2
iload 1
putfield android/support/v4/app/Fragment/R I
L2:
new android/support/v4/app/ao
dup
invokespecial android/support/v4/app/ao/<init>()V
astore 3
aload 3
iload 4
putfield android/support/v4/app/ao/c I
aload 3
aload 2
putfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
aload 0
aload 3
invokevirtual android/support/v4/app/ak/a(Landroid/support/v4/app/ao;)V
return
.limit locals 5
.limit stack 5
.end method

.method static synthetic a(Landroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;ZLandroid/support/v4/n/a;)V
iload 2
ifeq L0
aload 1
getfield android/support/v4/app/Fragment/ar Landroid/support/v4/app/gj;
astore 0
L1:
aload 0
ifnull L2
new java/util/ArrayList
dup
aload 3
invokevirtual android/support/v4/n/a/keySet()Ljava/util/Set;
invokespecial java/util/ArrayList/<init>(Ljava/util/Collection;)V
pop
new java/util/ArrayList
dup
aload 3
invokevirtual android/support/v4/n/a/values()Ljava/util/Collection;
invokespecial java/util/ArrayList/<init>(Ljava/util/Collection;)V
pop
L2:
return
L0:
aload 0
getfield android/support/v4/app/Fragment/ar Landroid/support/v4/app/gj;
astore 0
goto L1
.limit locals 4
.limit stack 3
.end method

.method static synthetic a(Landroid/support/v4/app/ak;Landroid/support/v4/app/ap;ILjava/lang/Object;)V
aload 0
aload 1
iload 2
aload 3
invokespecial android/support/v4/app/ak/a(Landroid/support/v4/app/ap;ILjava/lang/Object;)V
return
.limit locals 4
.limit stack 4
.end method

.method static synthetic a(Landroid/support/v4/app/ak;Landroid/support/v4/n/a;Landroid/support/v4/app/ap;)V
aload 0
getfield android/support/v4/app/ak/E Ljava/util/ArrayList;
ifnull L0
aload 1
invokevirtual android/support/v4/n/a/isEmpty()Z
ifne L0
aload 1
aload 0
getfield android/support/v4/app/ak/E Ljava/util/ArrayList;
iconst_0
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
invokevirtual android/support/v4/n/a/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/view/View
astore 0
aload 0
ifnull L0
aload 2
getfield android/support/v4/app/ap/c Landroid/support/v4/app/cj;
aload 0
putfield android/support/v4/app/cj/a Landroid/view/View;
L0:
return
.limit locals 3
.limit stack 3
.end method

.method private a(Landroid/support/v4/app/ap;ILjava/lang/Object;)V
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
ifnull L0
iconst_0
istore 4
L1:
iload 4
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L0
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
iload 4
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 5
aload 5
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
ifnull L2
aload 5
getfield android/support/v4/app/Fragment/ab Landroid/view/ViewGroup;
ifnull L2
aload 5
getfield android/support/v4/app/Fragment/R I
iload 2
if_icmpne L2
aload 5
getfield android/support/v4/app/Fragment/T Z
ifeq L3
aload 1
getfield android/support/v4/app/ap/b Ljava/util/ArrayList;
aload 5
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
invokevirtual java/util/ArrayList/contains(Ljava/lang/Object;)Z
ifne L2
aload 3
aload 5
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
iconst_1
invokestatic android/support/v4/app/ce/a(Ljava/lang/Object;Landroid/view/View;Z)V
aload 1
getfield android/support/v4/app/ap/b Ljava/util/ArrayList;
aload 5
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L2:
iload 4
iconst_1
iadd
istore 4
goto L1
L3:
aload 3
aload 5
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
iconst_0
invokestatic android/support/v4/app/ce/a(Ljava/lang/Object;Landroid/view/View;Z)V
aload 1
getfield android/support/v4/app/ap/b Ljava/util/ArrayList;
aload 5
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
invokevirtual java/util/ArrayList/remove(Ljava/lang/Object;)Z
pop
goto L2
L0:
return
.limit locals 6
.limit stack 3
.end method

.method private a(Landroid/support/v4/app/ap;Landroid/support/v4/n/a;Z)V
aload 0
getfield android/support/v4/app/ak/E Ljava/util/ArrayList;
ifnonnull L0
iconst_0
istore 4
L1:
iconst_0
istore 5
L2:
iload 5
iload 4
if_icmpge L3
aload 0
getfield android/support/v4/app/ak/D Ljava/util/ArrayList;
iload 5
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/String
astore 6
aload 2
aload 0
getfield android/support/v4/app/ak/E Ljava/util/ArrayList;
iload 5
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/String
invokevirtual android/support/v4/n/a/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/view/View
astore 7
aload 7
ifnull L4
aload 7
invokevirtual android/view/View/getTransitionName()Ljava/lang/String;
astore 7
iload 3
ifeq L5
aload 1
getfield android/support/v4/app/ap/a Landroid/support/v4/n/a;
aload 6
aload 7
invokestatic android/support/v4/app/ak/a(Landroid/support/v4/n/a;Ljava/lang/String;Ljava/lang/String;)V
L4:
iload 5
iconst_1
iadd
istore 5
goto L2
L0:
aload 0
getfield android/support/v4/app/ak/E Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 4
goto L1
L5:
aload 1
getfield android/support/v4/app/ap/a Landroid/support/v4/n/a;
aload 7
aload 6
invokestatic android/support/v4/app/ak/a(Landroid/support/v4/n/a;Ljava/lang/String;Ljava/lang/String;)V
goto L4
L3:
return
.limit locals 8
.limit stack 3
.end method

.method private a(Landroid/support/v4/app/ap;Landroid/view/View;Ljava/lang/Object;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;ZLjava/util/ArrayList;)V
aload 2
invokevirtual android/view/View/getViewTreeObserver()Landroid/view/ViewTreeObserver;
new android/support/v4/app/am
dup
aload 0
aload 2
aload 3
aload 7
aload 1
iload 6
aload 4
aload 5
invokespecial android/support/v4/app/am/<init>(Landroid/support/v4/app/ak;Landroid/view/View;Ljava/lang/Object;Ljava/util/ArrayList;Landroid/support/v4/app/ap;ZLandroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;)V
invokevirtual android/view/ViewTreeObserver/addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V
return
.limit locals 8
.limit stack 11
.end method

.method private static a(Landroid/support/v4/app/ap;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
aload 1
ifnull L0
iconst_0
istore 3
L1:
iload 3
aload 1
invokevirtual java/util/ArrayList/size()I
if_icmpge L0
aload 1
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/String
astore 4
aload 2
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast java/lang/String
astore 5
aload 0
getfield android/support/v4/app/ap/a Landroid/support/v4/n/a;
aload 4
aload 5
invokestatic android/support/v4/app/ak/a(Landroid/support/v4/n/a;Ljava/lang/String;Ljava/lang/String;)V
iload 3
iconst_1
iadd
istore 3
goto L1
L0:
return
.limit locals 6
.limit stack 3
.end method

.method private a(Landroid/support/v4/n/a;Landroid/support/v4/app/ap;)V
aload 0
getfield android/support/v4/app/ak/E Ljava/util/ArrayList;
ifnull L0
aload 1
invokevirtual android/support/v4/n/a/isEmpty()Z
ifne L0
aload 1
aload 0
getfield android/support/v4/app/ak/E Ljava/util/ArrayList;
iconst_0
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
invokevirtual android/support/v4/n/a/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/view/View
astore 1
aload 1
ifnull L0
aload 2
getfield android/support/v4/app/ap/c Landroid/support/v4/app/cj;
aload 1
putfield android/support/v4/app/cj/a Landroid/view/View;
L0:
return
.limit locals 3
.limit stack 3
.end method

.method private static a(Landroid/support/v4/n/a;Ljava/lang/String;Ljava/lang/String;)V
aload 1
ifnull L0
aload 2
ifnull L0
iconst_0
istore 3
L1:
iload 3
aload 0
invokevirtual android/support/v4/n/a/size()I
if_icmpge L2
aload 1
aload 0
iload 3
invokevirtual android/support/v4/n/a/c(I)Ljava/lang/Object;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L3
aload 0
iload 3
aload 2
invokevirtual android/support/v4/n/a/a(ILjava/lang/Object;)Ljava/lang/Object;
pop
L0:
return
L3:
iload 3
iconst_1
iadd
istore 3
goto L1
L2:
aload 0
aload 1
aload 2
invokevirtual android/support/v4/n/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
return
.limit locals 4
.limit stack 3
.end method

.method private static a(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
aload 1
ifnull L0
aload 1
getfield android/support/v4/app/Fragment/R I
istore 2
iload 2
ifeq L0
aload 1
getfield android/support/v4/app/Fragment/T Z
ifne L0
aload 1
invokevirtual android/support/v4/app/Fragment/k()Z
ifeq L0
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
ifnull L0
aload 0
iload 2
invokevirtual android/util/SparseArray/get(I)Ljava/lang/Object;
ifnonnull L0
aload 0
iload 2
aload 1
invokevirtual android/util/SparseArray/put(ILjava/lang/Object;)V
L0:
return
.limit locals 3
.limit stack 3
.end method

.method private a(Landroid/view/View;Landroid/support/v4/app/ap;ILjava/lang/Object;)V
aload 1
invokevirtual android/view/View/getViewTreeObserver()Landroid/view/ViewTreeObserver;
new android/support/v4/app/an
dup
aload 0
aload 1
aload 2
iload 3
aload 4
invokespecial android/support/v4/app/an/<init>(Landroid/support/v4/app/ak;Landroid/view/View;Landroid/support/v4/app/ap;ILjava/lang/Object;)V
invokevirtual android/view/ViewTreeObserver/addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V
return
.limit locals 5
.limit stack 8
.end method

.method private a(ILandroid/support/v4/app/ap;ZLandroid/util/SparseArray;Landroid/util/SparseArray;)Z
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
getfield android/support/v4/app/bl/w Landroid/support/v4/app/bf;
iload 1
invokevirtual android/support/v4/app/bf/a(I)Landroid/view/View;
checkcast android/view/ViewGroup
astore 10
aload 10
ifnonnull L0
iconst_0
ireturn
L0:
aload 5
iload 1
invokevirtual android/util/SparseArray/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 9
aload 4
iload 1
invokevirtual android/util/SparseArray/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 12
aload 9
ifnonnull L1
aconst_null
astore 7
L2:
aload 9
ifnull L3
aload 12
ifnonnull L4
L3:
aconst_null
astore 4
L5:
aload 12
ifnonnull L6
aconst_null
astore 8
L7:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 11
aload 4
ifnull L8
aload 0
aload 2
aload 12
iload 3
invokespecial android/support/v4/app/ak/a(Landroid/support/v4/app/ap;Landroid/support/v4/app/Fragment;Z)Landroid/support/v4/n/a;
astore 6
aload 6
invokevirtual android/support/v4/n/a/isEmpty()Z
ifeq L9
aconst_null
astore 6
aconst_null
astore 5
L10:
aload 7
ifnonnull L11
aload 5
ifnonnull L11
aload 8
ifnonnull L11
iconst_0
ireturn
L1:
iload 3
ifeq L12
aload 9
getfield android/support/v4/app/Fragment/am Ljava/lang/Object;
getstatic android/support/v4/app/Fragment/n Ljava/lang/Object;
if_acmpne L13
aload 9
getfield android/support/v4/app/Fragment/al Ljava/lang/Object;
astore 4
L14:
aload 4
invokestatic android/support/v4/app/ce/a(Ljava/lang/Object;)Ljava/lang/Object;
astore 7
goto L2
L13:
aload 9
getfield android/support/v4/app/Fragment/am Ljava/lang/Object;
astore 4
goto L14
L12:
aload 9
getfield android/support/v4/app/Fragment/aj Ljava/lang/Object;
astore 4
goto L14
L4:
iload 3
ifeq L15
aload 12
getfield android/support/v4/app/Fragment/ao Ljava/lang/Object;
getstatic android/support/v4/app/Fragment/n Ljava/lang/Object;
if_acmpne L16
aload 12
getfield android/support/v4/app/Fragment/an Ljava/lang/Object;
astore 4
L17:
aload 4
ifnonnull L18
aconst_null
astore 4
goto L5
L16:
aload 12
getfield android/support/v4/app/Fragment/ao Ljava/lang/Object;
astore 4
goto L17
L15:
aload 9
getfield android/support/v4/app/Fragment/an Ljava/lang/Object;
astore 4
goto L17
L18:
aload 4
checkcast android/transition/Transition
astore 5
aload 5
ifnonnull L19
aconst_null
astore 4
goto L5
L19:
new android/transition/TransitionSet
dup
invokespecial android/transition/TransitionSet/<init>()V
astore 4
aload 4
aload 5
invokevirtual android/transition/TransitionSet/addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;
pop
goto L5
L6:
iload 3
ifeq L20
aload 12
getfield android/support/v4/app/Fragment/ak Ljava/lang/Object;
getstatic android/support/v4/app/Fragment/n Ljava/lang/Object;
if_acmpne L21
aload 12
getfield android/support/v4/app/Fragment/aj Ljava/lang/Object;
astore 5
L22:
aload 5
invokestatic android/support/v4/app/ce/a(Ljava/lang/Object;)Ljava/lang/Object;
astore 8
goto L7
L21:
aload 12
getfield android/support/v4/app/Fragment/ak Ljava/lang/Object;
astore 5
goto L22
L20:
aload 12
getfield android/support/v4/app/Fragment/al Ljava/lang/Object;
astore 5
goto L22
L9:
iload 3
ifeq L23
aload 12
getfield android/support/v4/app/Fragment/ar Landroid/support/v4/app/gj;
astore 5
L24:
aload 5
ifnull L25
new java/util/ArrayList
dup
aload 6
invokevirtual android/support/v4/n/a/keySet()Ljava/util/Set;
invokespecial java/util/ArrayList/<init>(Ljava/util/Collection;)V
pop
new java/util/ArrayList
dup
aload 6
invokevirtual android/support/v4/n/a/values()Ljava/util/Collection;
invokespecial java/util/ArrayList/<init>(Ljava/util/Collection;)V
pop
L25:
aload 10
invokevirtual android/view/View/getViewTreeObserver()Landroid/view/ViewTreeObserver;
new android/support/v4/app/am
dup
aload 0
aload 10
aload 4
aload 11
aload 2
iload 3
aload 9
aload 12
invokespecial android/support/v4/app/am/<init>(Landroid/support/v4/app/ak;Landroid/view/View;Ljava/lang/Object;Ljava/util/ArrayList;Landroid/support/v4/app/ap;ZLandroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;)V
invokevirtual android/view/ViewTreeObserver/addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V
aload 4
astore 5
goto L10
L23:
aload 9
getfield android/support/v4/app/Fragment/ar Landroid/support/v4/app/gj;
astore 5
goto L24
L11:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 13
aload 8
aload 12
aload 13
aload 6
aload 2
getfield android/support/v4/app/ap/d Landroid/view/View;
invokestatic android/support/v4/app/ak/a(Ljava/lang/Object;Landroid/support/v4/app/Fragment;Ljava/util/ArrayList;Landroid/support/v4/n/a;Landroid/view/View;)Ljava/lang/Object;
astore 15
aload 0
getfield android/support/v4/app/ak/E Ljava/util/ArrayList;
ifnull L26
aload 6
ifnull L26
aload 6
aload 0
getfield android/support/v4/app/ak/E Ljava/util/ArrayList;
iconst_0
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
invokevirtual android/support/v4/n/a/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/view/View
astore 4
aload 4
ifnull L26
aload 15
ifnull L27
aload 15
aload 4
invokestatic android/support/v4/app/ce/a(Ljava/lang/Object;Landroid/view/View;)V
L27:
aload 5
ifnull L26
aload 5
aload 4
invokestatic android/support/v4/app/ce/a(Ljava/lang/Object;Landroid/view/View;)V
L26:
new android/support/v4/app/al
dup
aload 0
aload 9
invokespecial android/support/v4/app/al/<init>(Landroid/support/v4/app/ak;Landroid/support/v4/app/Fragment;)V
astore 16
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 12
new android/support/v4/n/a
dup
invokespecial android/support/v4/n/a/<init>()V
astore 14
aload 9
ifnull L28
iload 3
ifeq L29
aload 9
getfield android/support/v4/app/Fragment/ap Ljava/lang/Boolean;
ifnonnull L30
iconst_1
istore 3
L31:
aload 7
checkcast android/transition/Transition
astore 8
aload 15
checkcast android/transition/Transition
astore 4
aload 5
checkcast android/transition/Transition
astore 17
aload 8
ifnull L32
aload 4
ifnull L32
L33:
iload 3
ifeq L34
new android/transition/TransitionSet
dup
invokespecial android/transition/TransitionSet/<init>()V
astore 9
aload 8
ifnull L35
aload 9
aload 8
invokevirtual android/transition/TransitionSet/addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;
pop
L35:
aload 4
ifnull L36
aload 9
aload 4
invokevirtual android/transition/TransitionSet/addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;
pop
L36:
aload 17
ifnull L37
aload 9
aload 17
invokevirtual android/transition/TransitionSet/addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;
pop
L37:
aload 9
astore 4
L38:
aload 4
ifnull L39
aload 2
getfield android/support/v4/app/ap/d Landroid/view/View;
astore 8
aload 2
getfield android/support/v4/app/ap/c Landroid/support/v4/app/cj;
astore 9
aload 2
getfield android/support/v4/app/ap/a Landroid/support/v4/n/a;
astore 17
aload 7
ifnonnull L40
aload 5
ifnull L41
L40:
aload 7
checkcast android/transition/Transition
astore 18
aload 18
ifnull L42
aload 18
aload 8
invokevirtual android/transition/Transition/addTarget(Landroid/view/View;)Landroid/transition/Transition;
pop
L42:
aload 5
ifnull L43
aload 5
aload 8
aload 6
aload 11
invokestatic android/support/v4/app/ce/a(Ljava/lang/Object;Landroid/view/View;Ljava/util/Map;Ljava/util/ArrayList;)V
L43:
aload 10
invokevirtual android/view/View/getViewTreeObserver()Landroid/view/ViewTreeObserver;
new android/support/v4/app/cg
dup
aload 10
aload 18
aload 8
aload 16
aload 17
aload 14
aload 12
invokespecial android/support/v4/app/cg/<init>(Landroid/view/View;Landroid/transition/Transition;Landroid/view/View;Landroid/support/v4/app/ck;Ljava/util/Map;Ljava/util/Map;Ljava/util/ArrayList;)V
invokevirtual android/view/ViewTreeObserver/addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V
aload 18
ifnull L41
aload 18
new android/support/v4/app/ch
dup
aload 9
invokespecial android/support/v4/app/ch/<init>(Landroid/support/v4/app/cj;)V
invokevirtual android/transition/Transition/setEpicenterCallback(Landroid/transition/Transition$EpicenterCallback;)V
L41:
aload 10
invokevirtual android/view/View/getViewTreeObserver()Landroid/view/ViewTreeObserver;
new android/support/v4/app/an
dup
aload 0
aload 10
aload 2
iload 1
aload 4
invokespecial android/support/v4/app/an/<init>(Landroid/support/v4/app/ak;Landroid/view/View;Landroid/support/v4/app/ap;ILjava/lang/Object;)V
invokevirtual android/view/ViewTreeObserver/addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V
aload 4
aload 2
getfield android/support/v4/app/ap/d Landroid/view/View;
iconst_1
invokestatic android/support/v4/app/ce/a(Ljava/lang/Object;Landroid/view/View;Z)V
aload 0
aload 2
iload 1
aload 4
invokespecial android/support/v4/app/ak/a(Landroid/support/v4/app/ap;ILjava/lang/Object;)V
aload 10
aload 4
checkcast android/transition/Transition
invokestatic android/transition/TransitionManager/beginDelayedTransition(Landroid/view/ViewGroup;Landroid/transition/Transition;)V
aload 2
getfield android/support/v4/app/ap/d Landroid/view/View;
astore 6
aload 2
getfield android/support/v4/app/ap/b Ljava/util/ArrayList;
astore 2
aload 7
checkcast android/transition/Transition
astore 7
aload 15
checkcast android/transition/Transition
astore 8
aload 5
checkcast android/transition/Transition
astore 5
aload 4
checkcast android/transition/Transition
astore 9
aload 9
ifnull L39
aload 10
invokevirtual android/view/View/getViewTreeObserver()Landroid/view/ViewTreeObserver;
new android/support/v4/app/ci
dup
aload 10
aload 7
aload 12
aload 8
aload 13
aload 5
aload 11
aload 14
aload 2
aload 9
aload 6
invokespecial android/support/v4/app/ci/<init>(Landroid/view/View;Landroid/transition/Transition;Ljava/util/ArrayList;Landroid/transition/Transition;Ljava/util/ArrayList;Landroid/transition/Transition;Ljava/util/ArrayList;Ljava/util/Map;Ljava/util/ArrayList;Landroid/transition/Transition;Landroid/view/View;)V
invokevirtual android/view/ViewTreeObserver/addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V
L39:
aload 4
ifnull L44
iconst_1
ireturn
L30:
aload 9
getfield android/support/v4/app/Fragment/ap Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
istore 3
goto L31
L29:
aload 9
getfield android/support/v4/app/Fragment/aq Ljava/lang/Boolean;
ifnonnull L45
iconst_1
istore 3
goto L31
L45:
aload 9
getfield android/support/v4/app/Fragment/aq Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
istore 3
goto L31
L34:
aload 4
ifnull L46
aload 8
ifnull L46
new android/transition/TransitionSet
dup
invokespecial android/transition/TransitionSet/<init>()V
aload 4
invokevirtual android/transition/TransitionSet/addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;
aload 8
invokevirtual android/transition/TransitionSet/addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;
iconst_1
invokevirtual android/transition/TransitionSet/setOrdering(I)Landroid/transition/TransitionSet;
astore 4
L47:
aload 17
ifnull L48
new android/transition/TransitionSet
dup
invokespecial android/transition/TransitionSet/<init>()V
astore 8
aload 4
ifnull L49
aload 8
aload 4
invokevirtual android/transition/TransitionSet/addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;
pop
L49:
aload 8
aload 17
invokevirtual android/transition/TransitionSet/addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;
pop
aload 8
astore 4
goto L38
L46:
aload 4
ifnull L50
goto L47
L50:
aload 8
astore 4
aload 8
ifnonnull L47
aconst_null
astore 4
goto L47
L48:
goto L38
L44:
iconst_0
ireturn
L32:
iconst_1
istore 3
goto L33
L28:
iconst_1
istore 3
goto L31
L8:
aconst_null
astore 6
aload 4
astore 5
goto L10
.limit locals 19
.limit stack 14
.end method

.method private static b(Landroid/support/v4/app/Fragment;Z)Ljava/lang/Object;
aload 0
ifnonnull L0
aconst_null
areturn
L0:
iload 1
ifeq L1
aload 0
getfield android/support/v4/app/Fragment/ak Ljava/lang/Object;
getstatic android/support/v4/app/Fragment/n Ljava/lang/Object;
if_acmpne L2
aload 0
getfield android/support/v4/app/Fragment/aj Ljava/lang/Object;
astore 0
L3:
aload 0
invokestatic android/support/v4/app/ce/a(Ljava/lang/Object;)Ljava/lang/Object;
areturn
L2:
aload 0
getfield android/support/v4/app/Fragment/ak Ljava/lang/Object;
astore 0
goto L3
L1:
aload 0
getfield android/support/v4/app/Fragment/al Ljava/lang/Object;
astore 0
goto L3
.limit locals 2
.limit stack 2
.end method

.method private static b(Landroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;ZLandroid/support/v4/n/a;)V
iload 2
ifeq L0
aload 1
getfield android/support/v4/app/Fragment/ar Landroid/support/v4/app/gj;
astore 0
L1:
aload 0
ifnull L2
new java/util/ArrayList
dup
aload 3
invokevirtual android/support/v4/n/a/keySet()Ljava/util/Set;
invokespecial java/util/ArrayList/<init>(Ljava/util/Collection;)V
pop
new java/util/ArrayList
dup
aload 3
invokevirtual android/support/v4/n/a/values()Ljava/util/Collection;
invokespecial java/util/ArrayList/<init>(Ljava/util/Collection;)V
pop
L2:
return
L0:
aload 0
getfield android/support/v4/app/Fragment/ar Landroid/support/v4/app/gj;
astore 0
goto L1
.limit locals 4
.limit stack 3
.end method

.method private static b(Landroid/support/v4/app/ap;Landroid/support/v4/n/a;Z)V
aload 1
invokevirtual android/support/v4/n/a/size()I
istore 4
iconst_0
istore 3
L0:
iload 3
iload 4
if_icmpge L1
aload 1
iload 3
invokevirtual android/support/v4/n/a/b(I)Ljava/lang/Object;
checkcast java/lang/String
astore 5
aload 1
iload 3
invokevirtual android/support/v4/n/a/c(I)Ljava/lang/Object;
checkcast android/view/View
invokevirtual android/view/View/getTransitionName()Ljava/lang/String;
astore 6
iload 2
ifeq L2
aload 0
getfield android/support/v4/app/ap/a Landroid/support/v4/n/a;
aload 5
aload 6
invokestatic android/support/v4/app/ak/a(Landroid/support/v4/n/a;Ljava/lang/String;Ljava/lang/String;)V
L3:
iload 3
iconst_1
iadd
istore 3
goto L0
L2:
aload 0
getfield android/support/v4/app/ap/a Landroid/support/v4/n/a;
aload 6
aload 5
invokestatic android/support/v4/app/ak/a(Landroid/support/v4/n/a;Ljava/lang/String;Ljava/lang/String;)V
goto L3
L1:
return
.limit locals 7
.limit stack 3
.end method

.method private static b(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
aload 1
ifnull L0
aload 1
getfield android/support/v4/app/Fragment/R I
istore 2
iload 2
ifeq L0
aload 0
iload 2
aload 1
invokevirtual android/util/SparseArray/put(ILjava/lang/Object;)V
L0:
return
.limit locals 3
.limit stack 3
.end method

.method private b(Landroid/util/SparseArray;Landroid/util/SparseArray;)V
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
getfield android/support/v4/app/bl/w Landroid/support/v4/app/bf;
invokevirtual android/support/v4/app/bf/a()Z
ifne L0
L1:
return
L0:
aload 0
getfield android/support/v4/app/ak/l Landroid/support/v4/app/ao;
astore 5
L2:
aload 5
ifnull L1
aload 5
getfield android/support/v4/app/ao/c I
tableswitch 1
L3
L4
L5
L6
L7
L8
L9
default : L10
L10:
aload 5
getfield android/support/v4/app/ao/a Landroid/support/v4/app/ao;
astore 5
goto L2
L3:
aload 2
aload 5
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
invokestatic android/support/v4/app/ak/b(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
goto L10
L4:
aload 5
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
astore 4
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
ifnull L11
iconst_0
istore 3
L12:
aload 4
astore 6
iload 3
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L13
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 7
aload 4
ifnull L14
aload 4
astore 6
aload 7
getfield android/support/v4/app/Fragment/R I
aload 4
getfield android/support/v4/app/Fragment/R I
if_icmpne L15
L14:
aload 7
aload 4
if_acmpne L16
aconst_null
astore 6
L15:
iload 3
iconst_1
iadd
istore 3
aload 6
astore 4
goto L12
L16:
aload 1
aload 7
invokestatic android/support/v4/app/ak/a(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
aload 4
astore 6
goto L15
L11:
aload 4
astore 6
L13:
aload 2
aload 6
invokestatic android/support/v4/app/ak/b(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
goto L10
L5:
aload 1
aload 5
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
invokestatic android/support/v4/app/ak/a(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
goto L10
L6:
aload 1
aload 5
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
invokestatic android/support/v4/app/ak/a(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
goto L10
L7:
aload 2
aload 5
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
invokestatic android/support/v4/app/ak/b(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
goto L10
L8:
aload 1
aload 5
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
invokestatic android/support/v4/app/ak/a(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
goto L10
L9:
aload 2
aload 5
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
invokestatic android/support/v4/app/ak/b(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
goto L10
.limit locals 8
.limit stack 2
.end method

.method private c(Landroid/support/v4/app/Fragment;Z)Landroid/support/v4/n/a;
new android/support/v4/n/a
dup
invokespecial android/support/v4/n/a/<init>()V
astore 3
aload 1
getfield android/support/v4/app/Fragment/ac Landroid/view/View;
astore 4
aload 3
astore 1
aload 4
ifnull L0
aload 3
astore 1
aload 0
getfield android/support/v4/app/ak/D Ljava/util/ArrayList;
ifnull L0
aload 3
aload 4
invokestatic android/support/v4/app/ce/a(Ljava/util/Map;Landroid/view/View;)V
iload 2
ifeq L1
aload 0
getfield android/support/v4/app/ak/D Ljava/util/ArrayList;
aload 0
getfield android/support/v4/app/ak/E Ljava/util/ArrayList;
aload 3
invokestatic android/support/v4/app/ak/a(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/support/v4/n/a;)Landroid/support/v4/n/a;
astore 1
L0:
aload 1
areturn
L1:
aload 3
aload 0
getfield android/support/v4/app/ak/E Ljava/util/ArrayList;
invokestatic android/support/v4/n/k/c(Ljava/util/Map;Ljava/util/Collection;)Z
pop
aload 3
areturn
.limit locals 5
.limit stack 3
.end method

.method private m()I
aload 0
getfield android/support/v4/app/ak/s I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private n()I
aload 0
getfield android/support/v4/app/ak/t I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a()I
aload 0
getfield android/support/v4/app/ak/y I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a(ZLandroid/support/v4/app/ap;Landroid/util/SparseArray;Landroid/util/SparseArray;)Landroid/support/v4/app/ap;
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
getstatic android/support/v4/app/bl/b Z
ifeq L7
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "popFromBackStack: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
ldc "  "
new java/io/PrintWriter
dup
new android/support/v4/n/h
dup
ldc "FragmentManager"
invokespecial android/support/v4/n/h/<init>(Ljava/lang/String;)V
invokespecial java/io/PrintWriter/<init>(Ljava/io/Writer;)V
invokevirtual android/support/v4/app/ak/a(Ljava/lang/String;Ljava/io/PrintWriter;)V
L7:
aload 2
astore 9
getstatic android/support/v4/app/ak/b Z
ifeq L8
aload 2
ifnonnull L9
aload 3
invokevirtual android/util/SparseArray/size()I
ifne L10
aload 2
astore 9
aload 4
invokevirtual android/util/SparseArray/size()I
ifeq L8
L10:
aload 0
aload 3
aload 4
iconst_1
invokespecial android/support/v4/app/ak/a(Landroid/util/SparseArray;Landroid/util/SparseArray;Z)Landroid/support/v4/app/ap;
astore 9
L8:
aload 0
iconst_m1
invokevirtual android/support/v4/app/ak/e(I)V
aload 9
ifnull L11
iconst_0
istore 5
L12:
aload 9
ifnull L13
iconst_0
istore 6
L14:
aload 0
getfield android/support/v4/app/ak/m Landroid/support/v4/app/ao;
astore 2
L15:
aload 2
ifnull L16
aload 9
ifnull L17
iconst_0
istore 7
L18:
aload 9
ifnull L19
iconst_0
istore 8
L20:
aload 2
getfield android/support/v4/app/ao/c I
tableswitch 1
L21
L22
L23
L24
L25
L26
L27
default : L28
L28:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "Unknown cmd: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
getfield android/support/v4/app/ao/c I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L9:
aload 2
astore 9
iload 1
ifne L8
aload 2
aload 0
getfield android/support/v4/app/ak/E Ljava/util/ArrayList;
aload 0
getfield android/support/v4/app/ak/D Ljava/util/ArrayList;
invokestatic android/support/v4/app/ak/a(Landroid/support/v4/app/ap;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
aload 2
astore 9
goto L8
L11:
aload 0
getfield android/support/v4/app/ak/t I
istore 5
goto L12
L13:
aload 0
getfield android/support/v4/app/ak/s I
istore 6
goto L14
L17:
aload 2
getfield android/support/v4/app/ao/g I
istore 7
goto L18
L19:
aload 2
getfield android/support/v4/app/ao/h I
istore 8
goto L20
L21:
aload 2
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
astore 3
aload 3
iload 8
putfield android/support/v4/app/Fragment/aa I
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
aload 3
iload 6
invokestatic android/support/v4/app/bl/d(I)I
iload 5
invokevirtual android/support/v4/app/bl/a(Landroid/support/v4/app/Fragment;II)V
L29:
aload 2
getfield android/support/v4/app/ao/b Landroid/support/v4/app/ao;
astore 2
goto L15
L22:
aload 2
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
astore 3
aload 3
ifnull L30
aload 3
iload 8
putfield android/support/v4/app/Fragment/aa I
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
aload 3
iload 6
invokestatic android/support/v4/app/bl/d(I)I
iload 5
invokevirtual android/support/v4/app/bl/a(Landroid/support/v4/app/Fragment;II)V
L30:
aload 2
getfield android/support/v4/app/ao/i Ljava/util/ArrayList;
ifnull L29
iconst_0
istore 8
L31:
iload 8
aload 2
getfield android/support/v4/app/ao/i Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L29
aload 2
getfield android/support/v4/app/ao/i Ljava/util/ArrayList;
iload 8
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 3
aload 3
iload 7
putfield android/support/v4/app/Fragment/aa I
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
aload 3
iconst_0
invokevirtual android/support/v4/app/bl/a(Landroid/support/v4/app/Fragment;Z)V
iload 8
iconst_1
iadd
istore 8
goto L31
L23:
aload 2
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
astore 3
aload 3
iload 7
putfield android/support/v4/app/Fragment/aa I
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
aload 3
iconst_0
invokevirtual android/support/v4/app/bl/a(Landroid/support/v4/app/Fragment;Z)V
goto L29
L24:
aload 2
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
astore 3
aload 3
iload 7
putfield android/support/v4/app/Fragment/aa I
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
aload 3
iload 6
invokestatic android/support/v4/app/bl/d(I)I
iload 5
invokevirtual android/support/v4/app/bl/c(Landroid/support/v4/app/Fragment;II)V
goto L29
L25:
aload 2
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
astore 3
aload 3
iload 8
putfield android/support/v4/app/Fragment/aa I
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
aload 3
iload 6
invokestatic android/support/v4/app/bl/d(I)I
iload 5
invokevirtual android/support/v4/app/bl/b(Landroid/support/v4/app/Fragment;II)V
goto L29
L26:
aload 2
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
astore 3
aload 3
iload 7
putfield android/support/v4/app/Fragment/aa I
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
aload 3
iload 6
invokestatic android/support/v4/app/bl/d(I)I
iload 5
invokevirtual android/support/v4/app/bl/e(Landroid/support/v4/app/Fragment;II)V
goto L29
L27:
aload 2
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
astore 3
aload 3
iload 7
putfield android/support/v4/app/Fragment/aa I
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
aload 3
iload 6
invokestatic android/support/v4/app/bl/d(I)I
iload 5
invokevirtual android/support/v4/app/bl/d(Landroid/support/v4/app/Fragment;II)V
goto L29
L16:
iload 1
ifeq L32
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
getfield android/support/v4/app/bl/t I
iload 6
invokestatic android/support/v4/app/bl/d(I)I
iload 5
iconst_1
invokevirtual android/support/v4/app/bl/a(IIIZ)V
aconst_null
astore 9
L32:
aload 0
getfield android/support/v4/app/ak/y I
iflt L33
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
astore 2
aload 0
getfield android/support/v4/app/ak/y I
istore 5
aload 2
monitorenter
L0:
aload 2
getfield android/support/v4/app/bl/q Ljava/util/ArrayList;
iload 5
aconst_null
invokevirtual java/util/ArrayList/set(ILjava/lang/Object;)Ljava/lang/Object;
pop
aload 2
getfield android/support/v4/app/bl/r Ljava/util/ArrayList;
ifnonnull L1
aload 2
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/bl/r Ljava/util/ArrayList;
L1:
getstatic android/support/v4/app/bl/b Z
ifeq L3
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "Freeing back stack index "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 5
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L3:
aload 2
getfield android/support/v4/app/bl/r Ljava/util/ArrayList;
iload 5
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 2
monitorexit
L4:
aload 0
iconst_m1
putfield android/support/v4/app/ak/y I
L33:
aload 9
areturn
L2:
astore 3
L5:
aload 2
monitorexit
L6:
aload 3
athrow
.limit locals 10
.limit stack 7
.end method

.method public final a(I)Landroid/support/v4/app/cd;
aload 0
iload 1
putfield android/support/v4/app/ak/s I
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(II)Landroid/support/v4/app/cd;
aload 0
iload 1
putfield android/support/v4/app/ak/o I
aload 0
iload 2
putfield android/support/v4/app/ak/p I
aload 0
iconst_0
putfield android/support/v4/app/ak/q I
aload 0
iconst_0
putfield android/support/v4/app/ak/r I
aload 0
areturn
.limit locals 3
.limit stack 2
.end method

.method public final a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
aload 0
iload 1
aload 2
aconst_null
iconst_1
invokespecial android/support/v4/app/ak/a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;I)V
aload 0
areturn
.limit locals 3
.limit stack 5
.end method

.method public final a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;
aload 0
iload 1
aload 2
aload 3
iconst_1
invokespecial android/support/v4/app/ak/a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;I)V
aload 0
areturn
.limit locals 4
.limit stack 5
.end method

.method public final a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
aload 0
aload 1
aconst_null
invokevirtual android/support/v4/app/ak/b(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;
areturn
.limit locals 2
.limit stack 3
.end method

.method public final a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;
aload 0
iconst_0
aload 1
aload 2
iconst_1
invokespecial android/support/v4/app/ak/a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;I)V
aload 0
areturn
.limit locals 3
.limit stack 5
.end method

.method public final a(Landroid/view/View;Ljava/lang/String;)Landroid/support/v4/app/cd;
getstatic android/support/v4/app/ak/b Z
ifeq L0
aload 1
invokevirtual android/view/View/getTransitionName()Ljava/lang/String;
astore 1
aload 1
ifnonnull L1
new java/lang/IllegalArgumentException
dup
ldc "Unique transitionNames are required for all sharedElements"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
getfield android/support/v4/app/ak/D Ljava/util/ArrayList;
ifnonnull L2
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/ak/D Ljava/util/ArrayList;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/ak/E Ljava/util/ArrayList;
L2:
aload 0
getfield android/support/v4/app/ak/D Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
getfield android/support/v4/app/ak/E Ljava/util/ArrayList;
aload 2
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L0:
aload 0
areturn
.limit locals 3
.limit stack 3
.end method

.method public final a(Ljava/lang/CharSequence;)Landroid/support/v4/app/cd;
aload 0
iconst_0
putfield android/support/v4/app/ak/z I
aload 0
aload 1
putfield android/support/v4/app/ak/A Ljava/lang/CharSequence;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method final a(Landroid/support/v4/app/ao;)V
aload 0
getfield android/support/v4/app/ak/l Landroid/support/v4/app/ao;
ifnonnull L0
aload 0
aload 1
putfield android/support/v4/app/ak/m Landroid/support/v4/app/ao;
aload 0
aload 1
putfield android/support/v4/app/ak/l Landroid/support/v4/app/ao;
L1:
aload 1
aload 0
getfield android/support/v4/app/ak/o I
putfield android/support/v4/app/ao/e I
aload 1
aload 0
getfield android/support/v4/app/ak/p I
putfield android/support/v4/app/ao/f I
aload 1
aload 0
getfield android/support/v4/app/ak/q I
putfield android/support/v4/app/ao/g I
aload 1
aload 0
getfield android/support/v4/app/ak/r I
putfield android/support/v4/app/ao/h I
aload 0
aload 0
getfield android/support/v4/app/ak/n I
iconst_1
iadd
putfield android/support/v4/app/ak/n I
return
L0:
aload 1
aload 0
getfield android/support/v4/app/ak/m Landroid/support/v4/app/ao;
putfield android/support/v4/app/ao/b Landroid/support/v4/app/ao;
aload 0
getfield android/support/v4/app/ak/m Landroid/support/v4/app/ao;
aload 1
putfield android/support/v4/app/ao/a Landroid/support/v4/app/ao;
aload 0
aload 1
putfield android/support/v4/app/ak/m Landroid/support/v4/app/ao;
goto L1
.limit locals 2
.limit stack 3
.end method

.method public final a(Landroid/util/SparseArray;Landroid/util/SparseArray;)V
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
getfield android/support/v4/app/bl/w Landroid/support/v4/app/bf;
invokevirtual android/support/v4/app/bf/a()Z
ifne L0
L1:
return
L0:
aload 0
getfield android/support/v4/app/ak/l Landroid/support/v4/app/ao;
astore 4
L2:
aload 4
ifnull L1
aload 4
getfield android/support/v4/app/ao/c I
tableswitch 1
L3
L4
L5
L6
L7
L8
L9
default : L10
L10:
aload 4
getfield android/support/v4/app/ao/a Landroid/support/v4/app/ao;
astore 4
goto L2
L3:
aload 1
aload 4
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
invokestatic android/support/v4/app/ak/a(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
goto L10
L4:
aload 4
getfield android/support/v4/app/ao/i Ljava/util/ArrayList;
ifnull L11
aload 4
getfield android/support/v4/app/ao/i Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
istore 3
L12:
iload 3
iflt L11
aload 2
aload 4
getfield android/support/v4/app/ao/i Ljava/util/ArrayList;
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
invokestatic android/support/v4/app/ak/b(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
iload 3
iconst_1
isub
istore 3
goto L12
L11:
aload 1
aload 4
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
invokestatic android/support/v4/app/ak/a(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
goto L10
L5:
aload 2
aload 4
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
invokestatic android/support/v4/app/ak/b(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
goto L10
L6:
aload 2
aload 4
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
invokestatic android/support/v4/app/ak/b(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
goto L10
L7:
aload 1
aload 4
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
invokestatic android/support/v4/app/ak/a(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
goto L10
L8:
aload 2
aload 4
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
invokestatic android/support/v4/app/ak/b(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
goto L10
L9:
aload 1
aload 4
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
invokestatic android/support/v4/app/ak/a(Landroid/util/SparseArray;Landroid/support/v4/app/Fragment;)V
goto L10
.limit locals 5
.limit stack 3
.end method

.method public final a(Ljava/lang/String;Ljava/io/PrintWriter;)V
aload 0
aload 1
aload 2
iconst_1
invokevirtual android/support/v4/app/ak/a(Ljava/lang/String;Ljava/io/PrintWriter;Z)V
return
.limit locals 3
.limit stack 4
.end method

.method public final a(Ljava/lang/String;Ljava/io/PrintWriter;Z)V
iload 3
ifeq L0
aload 2
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
ldc "mName="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
aload 0
getfield android/support/v4/app/ak/w Ljava/lang/String;
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
ldc " mIndex="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
aload 0
getfield android/support/v4/app/ak/y I
invokevirtual java/io/PrintWriter/print(I)V
aload 2
ldc " mCommitted="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
aload 0
getfield android/support/v4/app/ak/x Z
invokevirtual java/io/PrintWriter/println(Z)V
aload 0
getfield android/support/v4/app/ak/s I
ifeq L1
aload 2
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
ldc "mTransition=#"
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
aload 0
getfield android/support/v4/app/ak/s I
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
ldc " mTransitionStyle=#"
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
aload 0
getfield android/support/v4/app/ak/t I
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
L1:
aload 0
getfield android/support/v4/app/ak/o I
ifne L2
aload 0
getfield android/support/v4/app/ak/p I
ifeq L3
L2:
aload 2
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
ldc "mEnterAnim=#"
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
aload 0
getfield android/support/v4/app/ak/o I
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
ldc " mExitAnim=#"
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
aload 0
getfield android/support/v4/app/ak/p I
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
L3:
aload 0
getfield android/support/v4/app/ak/q I
ifne L4
aload 0
getfield android/support/v4/app/ak/r I
ifeq L5
L4:
aload 2
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
ldc "mPopEnterAnim=#"
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
aload 0
getfield android/support/v4/app/ak/q I
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
ldc " mPopExitAnim=#"
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
aload 0
getfield android/support/v4/app/ak/r I
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
L5:
aload 0
getfield android/support/v4/app/ak/z I
ifne L6
aload 0
getfield android/support/v4/app/ak/A Ljava/lang/CharSequence;
ifnull L7
L6:
aload 2
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
ldc "mBreadCrumbTitleRes=#"
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
aload 0
getfield android/support/v4/app/ak/z I
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
ldc " mBreadCrumbTitleText="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
aload 0
getfield android/support/v4/app/ak/A Ljava/lang/CharSequence;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
L7:
aload 0
getfield android/support/v4/app/ak/B I
ifne L8
aload 0
getfield android/support/v4/app/ak/C Ljava/lang/CharSequence;
ifnull L0
L8:
aload 2
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
ldc "mBreadCrumbShortTitleRes=#"
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
aload 0
getfield android/support/v4/app/ak/B I
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
ldc " mBreadCrumbShortTitleText="
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
aload 0
getfield android/support/v4/app/ak/C Ljava/lang/CharSequence;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
L0:
aload 0
getfield android/support/v4/app/ak/l Landroid/support/v4/app/ao;
ifnull L9
aload 2
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
ldc "Operations:"
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "    "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 8
aload 0
getfield android/support/v4/app/ak/l Landroid/support/v4/app/ao;
astore 7
iconst_0
istore 4
L10:
aload 7
ifnull L9
aload 7
getfield android/support/v4/app/ao/c I
tableswitch 0
L11
L12
L13
L14
L15
L16
L17
L18
default : L19
L19:
new java/lang/StringBuilder
dup
ldc "cmd="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 7
getfield android/support/v4/app/ao/c I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 6
L20:
aload 2
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
ldc "  Op #"
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
iload 4
invokevirtual java/io/PrintWriter/print(I)V
aload 2
ldc ": "
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
aload 6
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
ldc " "
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
aload 7
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
iload 3
ifeq L21
aload 7
getfield android/support/v4/app/ao/e I
ifne L22
aload 7
getfield android/support/v4/app/ao/f I
ifeq L23
L22:
aload 2
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
ldc "enterAnim=#"
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
aload 7
getfield android/support/v4/app/ao/e I
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
ldc " exitAnim=#"
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
aload 7
getfield android/support/v4/app/ao/f I
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
L23:
aload 7
getfield android/support/v4/app/ao/g I
ifne L24
aload 7
getfield android/support/v4/app/ao/h I
ifeq L21
L24:
aload 2
aload 1
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
ldc "popEnterAnim=#"
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
aload 7
getfield android/support/v4/app/ao/g I
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
ldc " popExitAnim=#"
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
aload 7
getfield android/support/v4/app/ao/h I
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
L21:
aload 7
getfield android/support/v4/app/ao/i Ljava/util/ArrayList;
ifnull L25
aload 7
getfield android/support/v4/app/ao/i Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ifle L25
iconst_0
istore 5
L26:
iload 5
aload 7
getfield android/support/v4/app/ao/i Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L25
aload 2
aload 8
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 7
getfield android/support/v4/app/ao/i Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
if_icmpne L27
aload 2
ldc "Removed: "
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
L28:
aload 2
aload 7
getfield android/support/v4/app/ao/i Ljava/util/ArrayList;
iload 5
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
invokevirtual java/io/PrintWriter/println(Ljava/lang/Object;)V
iload 5
iconst_1
iadd
istore 5
goto L26
L11:
ldc "NULL"
astore 6
goto L20
L12:
ldc "ADD"
astore 6
goto L20
L13:
ldc "REPLACE"
astore 6
goto L20
L14:
ldc "REMOVE"
astore 6
goto L20
L15:
ldc "HIDE"
astore 6
goto L20
L16:
ldc "SHOW"
astore 6
goto L20
L17:
ldc "DETACH"
astore 6
goto L20
L18:
ldc "ATTACH"
astore 6
goto L20
L27:
iload 5
ifne L29
aload 2
ldc "Removed:"
invokevirtual java/io/PrintWriter/println(Ljava/lang/String;)V
L29:
aload 2
aload 8
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
ldc "  #"
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
aload 2
iload 5
invokevirtual java/io/PrintWriter/print(I)V
aload 2
ldc ": "
invokevirtual java/io/PrintWriter/print(Ljava/lang/String;)V
goto L28
L25:
aload 7
getfield android/support/v4/app/ao/a Landroid/support/v4/app/ao;
astore 7
iload 4
iconst_1
iadd
istore 4
goto L10
L9:
return
.limit locals 9
.limit stack 3
.end method

.method public final b()I
aload 0
getfield android/support/v4/app/ak/z I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final b(I)Landroid/support/v4/app/cd;
aload 0
iload 1
putfield android/support/v4/app/ak/t I
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final b(II)Landroid/support/v4/app/cd;
aload 0
iload 1
putfield android/support/v4/app/ak/o I
aload 0
iload 2
putfield android/support/v4/app/ak/p I
aload 0
iconst_0
putfield android/support/v4/app/ak/q I
aload 0
iconst_0
putfield android/support/v4/app/ak/r I
aload 0
areturn
.limit locals 3
.limit stack 2
.end method

.method public final b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
new android/support/v4/app/ao
dup
invokespecial android/support/v4/app/ao/<init>()V
astore 2
aload 2
iconst_3
putfield android/support/v4/app/ao/c I
aload 2
aload 1
putfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
aload 0
aload 2
invokevirtual android/support/v4/app/ak/a(Landroid/support/v4/app/ao;)V
aload 0
areturn
.limit locals 3
.limit stack 2
.end method

.method public final b(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;
aload 0
ldc_w 2131493319
aload 1
aload 2
iconst_2
invokespecial android/support/v4/app/ak/a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;I)V
aload 0
areturn
.limit locals 3
.limit stack 5
.end method

.method public final b(Ljava/lang/CharSequence;)Landroid/support/v4/app/cd;
aload 0
iconst_0
putfield android/support/v4/app/ak/B I
aload 0
aload 1
putfield android/support/v4/app/ak/C Ljava/lang/CharSequence;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final c()I
aload 0
getfield android/support/v4/app/ak/B I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final c(I)Landroid/support/v4/app/cd;
aload 0
iload 1
putfield android/support/v4/app/ak/z I
aload 0
aconst_null
putfield android/support/v4/app/ak/A Ljava/lang/CharSequence;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final c(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
new android/support/v4/app/ao
dup
invokespecial android/support/v4/app/ao/<init>()V
astore 2
aload 2
iconst_4
putfield android/support/v4/app/ao/c I
aload 2
aload 1
putfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
aload 0
aload 2
invokevirtual android/support/v4/app/ak/a(Landroid/support/v4/app/ao;)V
aload 0
areturn
.limit locals 3
.limit stack 2
.end method

.method public final d(I)Landroid/support/v4/app/cd;
aload 0
iload 1
putfield android/support/v4/app/ak/B I
aload 0
aconst_null
putfield android/support/v4/app/ak/C Ljava/lang/CharSequence;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final d(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
new android/support/v4/app/ao
dup
invokespecial android/support/v4/app/ao/<init>()V
astore 2
aload 2
iconst_5
putfield android/support/v4/app/ao/c I
aload 2
aload 1
putfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
aload 0
aload 2
invokevirtual android/support/v4/app/ak/a(Landroid/support/v4/app/ao;)V
aload 0
areturn
.limit locals 3
.limit stack 2
.end method

.method public final d()Ljava/lang/CharSequence;
aload 0
getfield android/support/v4/app/ak/z I
ifeq L0
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
getfield android/support/v4/app/bl/u Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/c Landroid/content/Context;
aload 0
getfield android/support/v4/app/ak/z I
invokevirtual android/content/Context/getText(I)Ljava/lang/CharSequence;
areturn
L0:
aload 0
getfield android/support/v4/app/ak/A Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final e(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
new android/support/v4/app/ao
dup
invokespecial android/support/v4/app/ao/<init>()V
astore 2
aload 2
bipush 6
putfield android/support/v4/app/ao/c I
aload 2
aload 1
putfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
aload 0
aload 2
invokevirtual android/support/v4/app/ak/a(Landroid/support/v4/app/ao;)V
aload 0
areturn
.limit locals 3
.limit stack 2
.end method

.method public final e()Ljava/lang/CharSequence;
aload 0
getfield android/support/v4/app/ak/B I
ifeq L0
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
getfield android/support/v4/app/bl/u Landroid/support/v4/app/bh;
getfield android/support/v4/app/bh/c Landroid/content/Context;
aload 0
getfield android/support/v4/app/ak/B I
invokevirtual android/content/Context/getText(I)Ljava/lang/CharSequence;
areturn
L0:
aload 0
getfield android/support/v4/app/ak/C Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 2
.end method

.method final e(I)V
aload 0
getfield android/support/v4/app/ak/u Z
ifne L0
L1:
return
L0:
getstatic android/support/v4/app/bl/b Z
ifeq L2
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "Bump nesting in "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " by "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L2:
aload 0
getfield android/support/v4/app/ak/l Landroid/support/v4/app/ao;
astore 3
L3:
aload 3
ifnull L1
aload 3
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
ifnull L4
aload 3
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
astore 4
aload 4
aload 4
getfield android/support/v4/app/Fragment/L I
iload 1
iadd
putfield android/support/v4/app/Fragment/L I
getstatic android/support/v4/app/bl/b Z
ifeq L4
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "Bump nesting of "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 3
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " to "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
getfield android/support/v4/app/Fragment/L I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L4:
aload 3
getfield android/support/v4/app/ao/i Ljava/util/ArrayList;
ifnull L5
aload 3
getfield android/support/v4/app/ao/i Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
istore 2
L6:
iload 2
iflt L5
aload 3
getfield android/support/v4/app/ao/i Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 4
aload 4
aload 4
getfield android/support/v4/app/Fragment/L I
iload 1
iadd
putfield android/support/v4/app/Fragment/L I
getstatic android/support/v4/app/bl/b Z
ifeq L7
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "Bump nesting of "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " to "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
getfield android/support/v4/app/Fragment/L I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L7:
iload 2
iconst_1
isub
istore 2
goto L6
L5:
aload 3
getfield android/support/v4/app/ao/a Landroid/support/v4/app/ao;
astore 3
goto L3
.limit locals 5
.limit stack 4
.end method

.method public final f()Landroid/support/v4/app/cd;
aload 0
getfield android/support/v4/app/ak/v Z
ifne L0
new java/lang/IllegalStateException
dup
ldc "This FragmentTransaction is not allowed to be added to the back stack."
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
iconst_1
putfield android/support/v4/app/ak/u Z
aload 0
aconst_null
putfield android/support/v4/app/ak/w Ljava/lang/String;
aload 0
areturn
.limit locals 1
.limit stack 3
.end method

.method public final f(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;
new android/support/v4/app/ao
dup
invokespecial android/support/v4/app/ao/<init>()V
astore 2
aload 2
bipush 7
putfield android/support/v4/app/ao/c I
aload 2
aload 1
putfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
aload 0
aload 2
invokevirtual android/support/v4/app/ak/a(Landroid/support/v4/app/ao;)V
aload 0
areturn
.limit locals 3
.limit stack 2
.end method

.method public final g()Z
aload 0
getfield android/support/v4/app/ak/v Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final h()Landroid/support/v4/app/cd;
aload 0
getfield android/support/v4/app/ak/u Z
ifeq L0
new java/lang/IllegalStateException
dup
ldc "This transaction is already being added to the back stack"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
iconst_0
putfield android/support/v4/app/ak/v Z
aload 0
areturn
.limit locals 1
.limit stack 3
.end method

.method public final i()I
aload 0
iconst_0
invokespecial android/support/v4/app/ak/a(Z)I
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final j()I
aload 0
iconst_1
invokespecial android/support/v4/app/ak/a(Z)I
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final k()Ljava/lang/String;
aload 0
getfield android/support/v4/app/ak/w Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final l()Z
aload 0
getfield android/support/v4/app/ak/n I
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final run()V
getstatic android/support/v4/app/bl/b Z
ifeq L0
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "Run: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L0:
aload 0
getfield android/support/v4/app/ak/u Z
ifeq L1
aload 0
getfield android/support/v4/app/ak/y I
ifge L1
new java/lang/IllegalStateException
dup
ldc "addToBackStack() called after commit()"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
iconst_1
invokevirtual android/support/v4/app/ak/e(I)V
getstatic android/support/v4/app/ak/b Z
ifeq L2
new android/util/SparseArray
dup
invokespecial android/util/SparseArray/<init>()V
astore 7
new android/util/SparseArray
dup
invokespecial android/util/SparseArray/<init>()V
astore 8
aload 0
aload 7
aload 8
invokespecial android/support/v4/app/ak/b(Landroid/util/SparseArray;Landroid/util/SparseArray;)V
aload 0
aload 7
aload 8
iconst_0
invokespecial android/support/v4/app/ak/a(Landroid/util/SparseArray;Landroid/util/SparseArray;Z)Landroid/support/v4/app/ap;
astore 8
L3:
aload 8
ifnull L4
iconst_0
istore 1
L5:
aload 8
ifnull L6
iconst_0
istore 2
L7:
aload 0
getfield android/support/v4/app/ak/l Landroid/support/v4/app/ao;
astore 9
L8:
aload 9
ifnull L9
aload 8
ifnull L10
iconst_0
istore 3
L11:
aload 8
ifnull L12
iconst_0
istore 4
L13:
aload 9
getfield android/support/v4/app/ao/c I
tableswitch 1
L14
L15
L16
L17
L18
L19
L20
default : L21
L21:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "Unknown cmd: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 9
getfield android/support/v4/app/ao/c I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L4:
aload 0
getfield android/support/v4/app/ak/t I
istore 1
goto L5
L6:
aload 0
getfield android/support/v4/app/ak/s I
istore 2
goto L7
L10:
aload 9
getfield android/support/v4/app/ao/e I
istore 3
goto L11
L12:
aload 9
getfield android/support/v4/app/ao/f I
istore 4
goto L13
L14:
aload 9
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
astore 7
aload 7
iload 3
putfield android/support/v4/app/Fragment/aa I
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
aload 7
iconst_0
invokevirtual android/support/v4/app/bl/a(Landroid/support/v4/app/Fragment;Z)V
L22:
aload 9
getfield android/support/v4/app/ao/a Landroid/support/v4/app/ao;
astore 9
goto L8
L15:
aload 9
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
astore 7
aload 7
getfield android/support/v4/app/Fragment/R I
istore 6
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
ifnull L23
iconst_0
istore 5
L24:
aload 7
astore 10
iload 5
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmpge L25
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
getfield android/support/v4/app/bl/m Ljava/util/ArrayList;
iload 5
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v4/app/Fragment
astore 11
getstatic android/support/v4/app/bl/b Z
ifeq L26
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "OP_REPLACE: adding="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 7
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " old="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 11
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L26:
aload 7
astore 10
aload 11
getfield android/support/v4/app/Fragment/R I
iload 6
if_icmpne L27
aload 11
aload 7
if_acmpne L28
aconst_null
astore 10
aload 9
aconst_null
putfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
L27:
iload 5
iconst_1
iadd
istore 5
aload 10
astore 7
goto L24
L28:
aload 9
getfield android/support/v4/app/ao/i Ljava/util/ArrayList;
ifnonnull L29
aload 9
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/ao/i Ljava/util/ArrayList;
L29:
aload 9
getfield android/support/v4/app/ao/i Ljava/util/ArrayList;
aload 11
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 11
iload 4
putfield android/support/v4/app/Fragment/aa I
aload 0
getfield android/support/v4/app/ak/u Z
ifeq L30
aload 11
aload 11
getfield android/support/v4/app/Fragment/L I
iconst_1
iadd
putfield android/support/v4/app/Fragment/L I
getstatic android/support/v4/app/bl/b Z
ifeq L30
ldc "FragmentManager"
new java/lang/StringBuilder
dup
ldc "Bump nesting of "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 11
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " to "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 11
getfield android/support/v4/app/Fragment/L I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/v(Ljava/lang/String;Ljava/lang/String;)I
pop
L30:
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
aload 11
iload 2
iload 1
invokevirtual android/support/v4/app/bl/a(Landroid/support/v4/app/Fragment;II)V
aload 7
astore 10
goto L27
L23:
aload 7
astore 10
L25:
aload 10
ifnull L22
aload 10
iload 3
putfield android/support/v4/app/Fragment/aa I
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
aload 10
iconst_0
invokevirtual android/support/v4/app/bl/a(Landroid/support/v4/app/Fragment;Z)V
goto L22
L16:
aload 9
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
astore 7
aload 7
iload 4
putfield android/support/v4/app/Fragment/aa I
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
aload 7
iload 2
iload 1
invokevirtual android/support/v4/app/bl/a(Landroid/support/v4/app/Fragment;II)V
goto L22
L17:
aload 9
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
astore 7
aload 7
iload 4
putfield android/support/v4/app/Fragment/aa I
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
aload 7
iload 2
iload 1
invokevirtual android/support/v4/app/bl/b(Landroid/support/v4/app/Fragment;II)V
goto L22
L18:
aload 9
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
astore 7
aload 7
iload 3
putfield android/support/v4/app/Fragment/aa I
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
aload 7
iload 2
iload 1
invokevirtual android/support/v4/app/bl/c(Landroid/support/v4/app/Fragment;II)V
goto L22
L19:
aload 9
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
astore 7
aload 7
iload 4
putfield android/support/v4/app/Fragment/aa I
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
aload 7
iload 2
iload 1
invokevirtual android/support/v4/app/bl/d(Landroid/support/v4/app/Fragment;II)V
goto L22
L20:
aload 9
getfield android/support/v4/app/ao/d Landroid/support/v4/app/Fragment;
astore 7
aload 7
iload 3
putfield android/support/v4/app/Fragment/aa I
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
aload 7
iload 2
iload 1
invokevirtual android/support/v4/app/bl/e(Landroid/support/v4/app/Fragment;II)V
goto L22
L9:
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
getfield android/support/v4/app/bl/t I
iload 2
iload 1
iconst_1
invokevirtual android/support/v4/app/bl/a(IIIZ)V
aload 0
getfield android/support/v4/app/ak/u Z
ifeq L31
aload 0
getfield android/support/v4/app/ak/c Landroid/support/v4/app/bl;
astore 7
aload 7
getfield android/support/v4/app/bl/o Ljava/util/ArrayList;
ifnonnull L32
aload 7
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/bl/o Ljava/util/ArrayList;
L32:
aload 7
getfield android/support/v4/app/bl/o Ljava/util/ArrayList;
aload 0
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 7
invokevirtual android/support/v4/app/bl/l()V
L31:
return
L2:
aconst_null
astore 8
goto L3
.limit locals 12
.limit stack 5
.end method

.method public final toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
sipush 128
invokespecial java/lang/StringBuilder/<init>(I)V
astore 1
aload 1
ldc "BackStackEntry{"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
aload 0
invokestatic java/lang/System/identityHashCode(Ljava/lang/Object;)I
invokestatic java/lang/Integer/toHexString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 0
getfield android/support/v4/app/ak/y I
iflt L0
aload 1
ldc " #"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
aload 0
getfield android/support/v4/app/ak/y I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
pop
L0:
aload 0
getfield android/support/v4/app/ak/w Ljava/lang/String;
ifnull L1
aload 1
ldc " "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
aload 0
getfield android/support/v4/app/ak/w Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L1:
aload 1
ldc "}"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 3
.end method
