.bytecode 50.0
.class final synchronized android/support/v4/app/eh
.super java/lang/Object

.field public static final 'a' Ljava/lang/String; = "call"

.field public static final 'b' Ljava/lang/String; = "msg"

.field public static final 'c' Ljava/lang/String; = "email"

.field public static final 'd' Ljava/lang/String; = "event"

.field public static final 'e' Ljava/lang/String; = "promo"

.field public static final 'f' Ljava/lang/String; = "alarm"

.field public static final 'g' Ljava/lang/String; = "progress"

.field public static final 'h' Ljava/lang/String; = "social"

.field public static final 'i' Ljava/lang/String; = "err"

.field public static final 'j' Ljava/lang/String; = "transport"

.field public static final 'k' Ljava/lang/String; = "sys"

.field public static final 'l' Ljava/lang/String; = "service"

.field public static final 'm' Ljava/lang/String; = "recommendation"

.field public static final 'n' Ljava/lang/String; = "status"

.field private static final 'o' Ljava/lang/String; = "author"

.field private static final 'p' Ljava/lang/String; = "text"

.field private static final 'q' Ljava/lang/String; = "messages"

.field private static final 'r' Ljava/lang/String; = "remote_input"

.field private static final 's' Ljava/lang/String; = "on_reply"

.field private static final 't' Ljava/lang/String; = "on_read"

.field private static final 'u' Ljava/lang/String; = "participants"

.field private static final 'v' Ljava/lang/String; = "timestamp"

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static a(Landroid/support/v4/app/fw;)Landroid/app/RemoteInput;
new android/app/RemoteInput$Builder
dup
aload 0
invokevirtual android/support/v4/app/fw/a()Ljava/lang/String;
invokespecial android/app/RemoteInput$Builder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual android/support/v4/app/fw/b()Ljava/lang/CharSequence;
invokevirtual android/app/RemoteInput$Builder/setLabel(Ljava/lang/CharSequence;)Landroid/app/RemoteInput$Builder;
aload 0
invokevirtual android/support/v4/app/fw/c()[Ljava/lang/CharSequence;
invokevirtual android/app/RemoteInput$Builder/setChoices([Ljava/lang/CharSequence;)Landroid/app/RemoteInput$Builder;
aload 0
invokevirtual android/support/v4/app/fw/d()Z
invokevirtual android/app/RemoteInput$Builder/setAllowFreeFormInput(Z)Landroid/app/RemoteInput$Builder;
aload 0
invokevirtual android/support/v4/app/fw/e()Landroid/os/Bundle;
invokevirtual android/app/RemoteInput$Builder/addExtras(Landroid/os/Bundle;)Landroid/app/RemoteInput$Builder;
invokevirtual android/app/RemoteInput$Builder/build()Landroid/app/RemoteInput;
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Landroid/support/v4/app/em;)Landroid/os/Bundle;
aconst_null
astore 3
iconst_0
istore 1
aload 0
ifnonnull L0
aconst_null
areturn
L0:
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 4
aload 3
astore 2
aload 0
invokevirtual android/support/v4/app/em/d()[Ljava/lang/String;
ifnull L1
aload 3
astore 2
aload 0
invokevirtual android/support/v4/app/em/d()[Ljava/lang/String;
arraylength
iconst_1
if_icmple L1
aload 0
invokevirtual android/support/v4/app/em/d()[Ljava/lang/String;
iconst_0
aaload
astore 2
L1:
aload 0
invokevirtual android/support/v4/app/em/a()[Ljava/lang/String;
arraylength
anewarray android/os/Parcelable
astore 3
L2:
iload 1
aload 3
arraylength
if_icmpge L3
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 5
aload 5
ldc "text"
aload 0
invokevirtual android/support/v4/app/em/a()[Ljava/lang/String;
iload 1
aaload
invokevirtual android/os/Bundle/putString(Ljava/lang/String;Ljava/lang/String;)V
aload 5
ldc "author"
aload 2
invokevirtual android/os/Bundle/putString(Ljava/lang/String;Ljava/lang/String;)V
aload 3
iload 1
aload 5
aastore
iload 1
iconst_1
iadd
istore 1
goto L2
L3:
aload 4
ldc "messages"
aload 3
invokevirtual android/os/Bundle/putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V
aload 0
invokevirtual android/support/v4/app/em/g()Landroid/support/v4/app/fw;
astore 2
aload 2
ifnull L4
aload 4
ldc "remote_input"
new android/app/RemoteInput$Builder
dup
aload 2
invokevirtual android/support/v4/app/fw/a()Ljava/lang/String;
invokespecial android/app/RemoteInput$Builder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual android/support/v4/app/fw/b()Ljava/lang/CharSequence;
invokevirtual android/app/RemoteInput$Builder/setLabel(Ljava/lang/CharSequence;)Landroid/app/RemoteInput$Builder;
aload 2
invokevirtual android/support/v4/app/fw/c()[Ljava/lang/CharSequence;
invokevirtual android/app/RemoteInput$Builder/setChoices([Ljava/lang/CharSequence;)Landroid/app/RemoteInput$Builder;
aload 2
invokevirtual android/support/v4/app/fw/d()Z
invokevirtual android/app/RemoteInput$Builder/setAllowFreeFormInput(Z)Landroid/app/RemoteInput$Builder;
aload 2
invokevirtual android/support/v4/app/fw/e()Landroid/os/Bundle;
invokevirtual android/app/RemoteInput$Builder/addExtras(Landroid/os/Bundle;)Landroid/app/RemoteInput$Builder;
invokevirtual android/app/RemoteInput$Builder/build()Landroid/app/RemoteInput;
invokevirtual android/os/Bundle/putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
L4:
aload 4
ldc "on_reply"
aload 0
invokevirtual android/support/v4/app/em/b()Landroid/app/PendingIntent;
invokevirtual android/os/Bundle/putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
aload 4
ldc "on_read"
aload 0
invokevirtual android/support/v4/app/em/c()Landroid/app/PendingIntent;
invokevirtual android/os/Bundle/putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
aload 4
ldc "participants"
aload 0
invokevirtual android/support/v4/app/em/d()[Ljava/lang/String;
invokevirtual android/os/Bundle/putStringArray(Ljava/lang/String;[Ljava/lang/String;)V
aload 4
ldc "timestamp"
aload 0
invokevirtual android/support/v4/app/em/f()J
invokevirtual android/os/Bundle/putLong(Ljava/lang/String;J)V
aload 4
areturn
.limit locals 6
.limit stack 5
.end method

.method private static a(Landroid/os/Bundle;Landroid/support/v4/app/en;Landroid/support/v4/app/fx;)Landroid/support/v4/app/em;
iconst_0
istore 5
aload 0
ifnonnull L0
L1:
aconst_null
areturn
L0:
aload 0
ldc "messages"
invokevirtual android/os/Bundle/getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;
astore 7
aload 7
ifnull L2
aload 7
arraylength
anewarray java/lang/String
astore 6
iconst_0
istore 3
L3:
iload 3
aload 6
arraylength
if_icmpge L4
aload 7
iload 3
aaload
instanceof android/os/Bundle
ifne L5
iload 5
istore 4
L6:
iload 4
ifeq L1
L7:
aload 0
ldc "on_read"
invokevirtual android/os/Bundle/getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
checkcast android/app/PendingIntent
astore 7
aload 0
ldc "on_reply"
invokevirtual android/os/Bundle/getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
checkcast android/app/PendingIntent
astore 8
aload 0
ldc "remote_input"
invokevirtual android/os/Bundle/getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
checkcast android/app/RemoteInput
astore 10
aload 0
ldc "participants"
invokevirtual android/os/Bundle/getStringArray(Ljava/lang/String;)[Ljava/lang/String;
astore 9
aload 9
ifnull L1
aload 9
arraylength
iconst_1
if_icmpne L1
aload 10
ifnull L8
aload 2
aload 10
invokevirtual android/app/RemoteInput/getResultKey()Ljava/lang/String;
aload 10
invokevirtual android/app/RemoteInput/getLabel()Ljava/lang/CharSequence;
aload 10
invokevirtual android/app/RemoteInput/getChoices()[Ljava/lang/CharSequence;
aload 10
invokevirtual android/app/RemoteInput/getAllowFreeFormInput()Z
aload 10
invokevirtual android/app/RemoteInput/getExtras()Landroid/os/Bundle;
invokeinterface android/support/v4/app/fx/a(Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/CharSequence;ZLandroid/os/Bundle;)Landroid/support/v4/app/fw; 5
astore 2
L9:
aload 1
aload 6
aload 2
aload 8
aload 7
aload 9
aload 0
ldc "timestamp"
invokevirtual android/os/Bundle/getLong(Ljava/lang/String;)J
invokeinterface android/support/v4/app/en/a([Ljava/lang/String;Landroid/support/v4/app/fw;Landroid/app/PendingIntent;Landroid/app/PendingIntent;[Ljava/lang/String;J)Landroid/support/v4/app/em; 7
areturn
L5:
aload 6
iload 3
aload 7
iload 3
aaload
checkcast android/os/Bundle
ldc "text"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
aastore
iload 5
istore 4
aload 6
iload 3
aaload
ifnull L6
iload 3
iconst_1
iadd
istore 3
goto L3
L8:
aconst_null
astore 2
goto L9
L4:
iconst_1
istore 4
goto L6
L2:
aconst_null
astore 6
goto L7
.limit locals 11
.limit stack 8
.end method

.method private static a(Landroid/app/RemoteInput;Landroid/support/v4/app/fx;)Landroid/support/v4/app/fw;
aload 1
aload 0
invokevirtual android/app/RemoteInput/getResultKey()Ljava/lang/String;
aload 0
invokevirtual android/app/RemoteInput/getLabel()Ljava/lang/CharSequence;
aload 0
invokevirtual android/app/RemoteInput/getChoices()[Ljava/lang/CharSequence;
aload 0
invokevirtual android/app/RemoteInput/getAllowFreeFormInput()Z
aload 0
invokevirtual android/app/RemoteInput/getExtras()Landroid/os/Bundle;
invokeinterface android/support/v4/app/fx/a(Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/CharSequence;ZLandroid/os/Bundle;)Landroid/support/v4/app/fw; 5
areturn
.limit locals 2
.limit stack 6
.end method

.method private static a(Landroid/app/Notification;)Ljava/lang/String;
aload 0
getfield android/app/Notification/category Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
