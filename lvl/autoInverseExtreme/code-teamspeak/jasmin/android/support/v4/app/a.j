.bytecode 50.0
.class public final synchronized android/support/v4/app/a
.super java/lang/Object
.implements android/support/v4/widget/ac
.annotation visible Ljava/lang/Deprecated;
.end annotation

.field private static final 'a' Landroid/support/v4/app/c;

.field private static final 'b' F = 0.33333334F


.field private static final 'c' I = 16908332


.field private final 'd' Landroid/app/Activity;

.field private final 'e' Landroid/support/v4/app/g;

.field private final 'f' Landroid/support/v4/widget/DrawerLayout;

.field private 'g' Z

.field private 'h' Z

.field private 'i' Landroid/graphics/drawable/Drawable;

.field private 'j' Landroid/graphics/drawable/Drawable;

.field private 'k' Landroid/support/v4/app/i;

.field private final 'l' I

.field private final 'm' I

.field private final 'n' I

.field private 'o' Ljava/lang/Object;

.method static <clinit>()V
getstatic android/os/Build$VERSION/SDK_INT I
istore 0
iload 0
bipush 18
if_icmplt L0
new android/support/v4/app/f
dup
iconst_0
invokespecial android/support/v4/app/f/<init>(B)V
putstatic android/support/v4/app/a/a Landroid/support/v4/app/c;
return
L0:
iload 0
bipush 11
if_icmplt L1
new android/support/v4/app/e
dup
iconst_0
invokespecial android/support/v4/app/e/<init>(B)V
putstatic android/support/v4/app/a/a Landroid/support/v4/app/c;
return
L1:
new android/support/v4/app/d
dup
iconst_0
invokespecial android/support/v4/app/d/<init>(B)V
putstatic android/support/v4/app/a/a Landroid/support/v4/app/c;
return
.limit locals 1
.limit stack 3
.end method

.method private <init>(Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;III)V
.annotation invisibleparam 3 Landroid/support/a/m;
.end annotation
.annotation invisibleparam 4 Landroid/support/a/ah;
.end annotation
.annotation invisibleparam 5 Landroid/support/a/ah;
.end annotation
iconst_1
istore 7
aload 1
invokevirtual android/content/Context/getApplicationInfo()Landroid/content/pm/ApplicationInfo;
getfield android/content/pm/ApplicationInfo/targetSdkVersion I
bipush 21
if_icmplt L0
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
iconst_1
istore 6
L1:
iload 6
ifne L2
L3:
aload 0
aload 1
aload 2
iload 7
iload 3
iload 4
iload 5
invokespecial android/support/v4/app/a/<init>(Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;ZIII)V
return
L0:
iconst_0
istore 6
goto L1
L2:
iconst_0
istore 7
goto L3
.limit locals 8
.limit stack 7
.end method

.method private <init>(Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;ZIII)V
.annotation invisibleparam 4 Landroid/support/a/m;
.end annotation
.annotation invisibleparam 5 Landroid/support/a/ah;
.end annotation
.annotation invisibleparam 6 Landroid/support/a/ah;
.end annotation
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_1
putfield android/support/v4/app/a/g Z
aload 0
aload 1
putfield android/support/v4/app/a/d Landroid/app/Activity;
aload 1
instanceof android/support/v4/app/h
ifeq L0
aload 0
aload 1
checkcast android/support/v4/app/h
invokeinterface android/support/v4/app/h/a()Landroid/support/v4/app/g; 0
putfield android/support/v4/app/a/e Landroid/support/v4/app/g;
L1:
aload 0
aload 2
putfield android/support/v4/app/a/f Landroid/support/v4/widget/DrawerLayout;
aload 0
iload 4
putfield android/support/v4/app/a/l I
aload 0
iload 5
putfield android/support/v4/app/a/m I
aload 0
iload 6
putfield android/support/v4/app/a/n I
aload 0
aload 0
invokespecial android/support/v4/app/a/g()Landroid/graphics/drawable/Drawable;
putfield android/support/v4/app/a/i Landroid/graphics/drawable/Drawable;
aload 0
aload 1
iload 4
invokestatic android/support/v4/c/h/a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
putfield android/support/v4/app/a/j Landroid/graphics/drawable/Drawable;
aload 0
new android/support/v4/app/i
dup
aload 0
aload 0
getfield android/support/v4/app/a/j Landroid/graphics/drawable/Drawable;
iconst_0
invokespecial android/support/v4/app/i/<init>(Landroid/support/v4/app/a;Landroid/graphics/drawable/Drawable;B)V
putfield android/support/v4/app/a/k Landroid/support/v4/app/i;
aload 0
getfield android/support/v4/app/a/k Landroid/support/v4/app/i;
astore 1
iload 3
ifeq L2
ldc_w 0.33333334F
fstore 7
L3:
aload 1
fload 7
putfield android/support/v4/app/i/b F
aload 1
invokevirtual android/support/v4/app/i/invalidateSelf()V
return
L0:
aload 0
aconst_null
putfield android/support/v4/app/a/e Landroid/support/v4/app/g;
goto L1
L2:
fconst_0
fstore 7
goto L3
.limit locals 8
.limit stack 6
.end method

.method static synthetic a(Landroid/support/v4/app/a;)Landroid/app/Activity;
aload 0
getfield android/support/v4/app/a/d Landroid/app/Activity;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(I)V
aconst_null
astore 2
iload 1
ifeq L0
aload 0
getfield android/support/v4/app/a/d Landroid/app/Activity;
iload 1
invokestatic android/support/v4/c/h/a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
astore 2
L0:
aload 2
ifnonnull L1
aload 0
aload 0
invokespecial android/support/v4/app/a/g()Landroid/graphics/drawable/Drawable;
putfield android/support/v4/app/a/i Landroid/graphics/drawable/Drawable;
aload 0
iconst_0
putfield android/support/v4/app/a/h Z
L2:
aload 0
getfield android/support/v4/app/a/g Z
ifne L3
aload 0
aload 0
getfield android/support/v4/app/a/i Landroid/graphics/drawable/Drawable;
iconst_0
invokespecial android/support/v4/app/a/a(Landroid/graphics/drawable/Drawable;I)V
L3:
return
L1:
aload 0
aload 2
putfield android/support/v4/app/a/i Landroid/graphics/drawable/Drawable;
aload 0
iconst_1
putfield android/support/v4/app/a/h Z
goto L2
.limit locals 3
.limit stack 3
.end method

.method private a(Landroid/graphics/drawable/Drawable;)V
aload 1
ifnonnull L0
aload 0
aload 0
invokespecial android/support/v4/app/a/g()Landroid/graphics/drawable/Drawable;
putfield android/support/v4/app/a/i Landroid/graphics/drawable/Drawable;
aload 0
iconst_0
putfield android/support/v4/app/a/h Z
L1:
aload 0
getfield android/support/v4/app/a/g Z
ifne L2
aload 0
aload 0
getfield android/support/v4/app/a/i Landroid/graphics/drawable/Drawable;
iconst_0
invokespecial android/support/v4/app/a/a(Landroid/graphics/drawable/Drawable;I)V
L2:
return
L0:
aload 0
aload 1
putfield android/support/v4/app/a/i Landroid/graphics/drawable/Drawable;
aload 0
iconst_1
putfield android/support/v4/app/a/h Z
goto L1
.limit locals 2
.limit stack 3
.end method

.method private a(Landroid/graphics/drawable/Drawable;I)V
aload 0
getfield android/support/v4/app/a/e Landroid/support/v4/app/g;
ifnull L0
return
L0:
aload 0
getstatic android/support/v4/app/a/a Landroid/support/v4/app/c;
aload 0
getfield android/support/v4/app/a/o Ljava/lang/Object;
aload 0
getfield android/support/v4/app/a/d Landroid/app/Activity;
aload 1
iload 2
invokeinterface android/support/v4/app/c/a(Ljava/lang/Object;Landroid/app/Activity;Landroid/graphics/drawable/Drawable;I)Ljava/lang/Object; 4
putfield android/support/v4/app/a/o Ljava/lang/Object;
return
.limit locals 3
.limit stack 6
.end method

.method private a(Z)V
iload 1
aload 0
getfield android/support/v4/app/a/g Z
if_icmpeq L0
iload 1
ifeq L1
aload 0
getfield android/support/v4/app/a/k Landroid/support/v4/app/i;
astore 3
aload 0
getfield android/support/v4/app/a/f Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/c()Z
ifeq L2
aload 0
getfield android/support/v4/app/a/n I
istore 2
L3:
aload 0
aload 3
iload 2
invokespecial android/support/v4/app/a/a(Landroid/graphics/drawable/Drawable;I)V
L4:
aload 0
iload 1
putfield android/support/v4/app/a/g Z
L0:
return
L2:
aload 0
getfield android/support/v4/app/a/m I
istore 2
goto L3
L1:
aload 0
aload 0
getfield android/support/v4/app/a/i Landroid/graphics/drawable/Drawable;
iconst_0
invokespecial android/support/v4/app/a/a(Landroid/graphics/drawable/Drawable;I)V
goto L4
.limit locals 4
.limit stack 3
.end method

.method private static a(Landroid/content/Context;)Z
aload 0
invokevirtual android/content/Context/getApplicationInfo()Landroid/content/pm/ApplicationInfo;
getfield android/content/pm/ApplicationInfo/targetSdkVersion I
bipush 21
if_icmplt L0
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private a(Landroid/view/MenuItem;)Z
aload 1
ifnull L0
aload 1
invokeinterface android/view/MenuItem/getItemId()I 0
ldc_w 16908332
if_icmpne L0
aload 0
getfield android/support/v4/app/a/g Z
ifeq L0
aload 0
getfield android/support/v4/app/a/f Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/d()Z
ifeq L1
aload 0
getfield android/support/v4/app/a/f Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/b()V
L2:
iconst_1
ireturn
L1:
aload 0
getfield android/support/v4/app/a/f Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/a()V
goto L2
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private b(I)V
aload 0
getfield android/support/v4/app/a/e Landroid/support/v4/app/g;
ifnull L0
return
L0:
aload 0
getstatic android/support/v4/app/a/a Landroid/support/v4/app/c;
aload 0
getfield android/support/v4/app/a/o Ljava/lang/Object;
aload 0
getfield android/support/v4/app/a/d Landroid/app/Activity;
iload 1
invokeinterface android/support/v4/app/c/a(Ljava/lang/Object;Landroid/app/Activity;I)Ljava/lang/Object; 3
putfield android/support/v4/app/a/o Ljava/lang/Object;
return
.limit locals 2
.limit stack 5
.end method

.method private d()V
aload 0
getfield android/support/v4/app/a/f Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/c()Z
ifeq L0
aload 0
getfield android/support/v4/app/a/k Landroid/support/v4/app/i;
fconst_1
invokevirtual android/support/v4/app/i/a(F)V
L1:
aload 0
getfield android/support/v4/app/a/g Z
ifeq L2
aload 0
getfield android/support/v4/app/a/k Landroid/support/v4/app/i;
astore 2
aload 0
getfield android/support/v4/app/a/f Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/c()Z
ifeq L3
aload 0
getfield android/support/v4/app/a/n I
istore 1
L4:
aload 0
aload 2
iload 1
invokespecial android/support/v4/app/a/a(Landroid/graphics/drawable/Drawable;I)V
L2:
return
L0:
aload 0
getfield android/support/v4/app/a/k Landroid/support/v4/app/i;
fconst_0
invokevirtual android/support/v4/app/i/a(F)V
goto L1
L3:
aload 0
getfield android/support/v4/app/a/m I
istore 1
goto L4
.limit locals 3
.limit stack 3
.end method

.method private e()Z
aload 0
getfield android/support/v4/app/a/g Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private f()V
aload 0
getfield android/support/v4/app/a/h Z
ifne L0
aload 0
aload 0
invokespecial android/support/v4/app/a/g()Landroid/graphics/drawable/Drawable;
putfield android/support/v4/app/a/i Landroid/graphics/drawable/Drawable;
L0:
aload 0
aload 0
getfield android/support/v4/app/a/d Landroid/app/Activity;
aload 0
getfield android/support/v4/app/a/l I
invokestatic android/support/v4/c/h/a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
putfield android/support/v4/app/a/j Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v4/app/a/f Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/c()Z
ifeq L1
aload 0
getfield android/support/v4/app/a/k Landroid/support/v4/app/i;
fconst_1
invokevirtual android/support/v4/app/i/a(F)V
L2:
aload 0
getfield android/support/v4/app/a/g Z
ifeq L3
aload 0
getfield android/support/v4/app/a/k Landroid/support/v4/app/i;
astore 2
aload 0
getfield android/support/v4/app/a/f Landroid/support/v4/widget/DrawerLayout;
invokevirtual android/support/v4/widget/DrawerLayout/c()Z
ifeq L4
aload 0
getfield android/support/v4/app/a/n I
istore 1
L5:
aload 0
aload 2
iload 1
invokespecial android/support/v4/app/a/a(Landroid/graphics/drawable/Drawable;I)V
L3:
return
L1:
aload 0
getfield android/support/v4/app/a/k Landroid/support/v4/app/i;
fconst_0
invokevirtual android/support/v4/app/i/a(F)V
goto L2
L4:
aload 0
getfield android/support/v4/app/a/m I
istore 1
goto L5
.limit locals 3
.limit stack 3
.end method

.method private g()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v4/app/a/e Landroid/support/v4/app/g;
ifnull L0
aload 0
getfield android/support/v4/app/a/e Landroid/support/v4/app/g;
invokeinterface android/support/v4/app/g/a()Landroid/graphics/drawable/Drawable; 0
areturn
L0:
getstatic android/support/v4/app/a/a Landroid/support/v4/app/c;
aload 0
getfield android/support/v4/app/a/d Landroid/app/Activity;
invokeinterface android/support/v4/app/c/a(Landroid/app/Activity;)Landroid/graphics/drawable/Drawable; 1
areturn
.limit locals 1
.limit stack 2
.end method

.method public final a()V
aload 0
getfield android/support/v4/app/a/k Landroid/support/v4/app/i;
fconst_1
invokevirtual android/support/v4/app/i/a(F)V
aload 0
getfield android/support/v4/app/a/g Z
ifeq L0
aload 0
aload 0
getfield android/support/v4/app/a/n I
invokespecial android/support/v4/app/a/b(I)V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public final a(F)V
aload 0
getfield android/support/v4/app/a/k Landroid/support/v4/app/i;
getfield android/support/v4/app/i/a F
fstore 2
fload 1
ldc_w 0.5F
fcmpl
ifle L0
fload 2
fconst_0
fload 1
ldc_w 0.5F
fsub
invokestatic java/lang/Math/max(FF)F
fconst_2
fmul
invokestatic java/lang/Math/max(FF)F
fstore 1
L1:
aload 0
getfield android/support/v4/app/a/k Landroid/support/v4/app/i;
fload 1
invokevirtual android/support/v4/app/i/a(F)V
return
L0:
fload 2
fload 1
fconst_2
fmul
invokestatic java/lang/Math/min(FF)F
fstore 1
goto L1
.limit locals 3
.limit stack 4
.end method

.method public final b()V
aload 0
getfield android/support/v4/app/a/k Landroid/support/v4/app/i;
fconst_0
invokevirtual android/support/v4/app/i/a(F)V
aload 0
getfield android/support/v4/app/a/g Z
ifeq L0
aload 0
aload 0
getfield android/support/v4/app/a/m I
invokespecial android/support/v4/app/a/b(I)V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public final c()V
return
.limit locals 1
.limit stack 0
.end method
