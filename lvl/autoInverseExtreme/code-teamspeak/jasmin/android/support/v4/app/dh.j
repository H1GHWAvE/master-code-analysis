.bytecode 50.0
.class public final synchronized android/support/v4/app/dh
.super java/lang/Object

.field final 'a' Landroid/os/Bundle;

.field private final 'b' I

.field private final 'c' Ljava/lang/CharSequence;

.field private final 'd' Landroid/app/PendingIntent;

.field private 'e' Ljava/util/ArrayList;

.method private <init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V
aload 0
iload 1
aload 2
aload 3
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
invokespecial android/support/v4/app/dh/<init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;)V
return
.limit locals 4
.limit stack 6
.end method

.method private <init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iload 1
putfield android/support/v4/app/dh/b I
aload 0
aload 2
invokestatic android/support/v4/app/dm/d(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
putfield android/support/v4/app/dh/c Ljava/lang/CharSequence;
aload 0
aload 3
putfield android/support/v4/app/dh/d Landroid/app/PendingIntent;
aload 0
aload 4
putfield android/support/v4/app/dh/a Landroid/os/Bundle;
return
.limit locals 5
.limit stack 2
.end method

.method private <init>(Landroid/support/v4/app/df;)V
aload 0
aload 1
getfield android/support/v4/app/df/b I
aload 1
getfield android/support/v4/app/df/c Ljava/lang/CharSequence;
aload 1
getfield android/support/v4/app/df/d Landroid/app/PendingIntent;
new android/os/Bundle
dup
aload 1
invokestatic android/support/v4/app/df/a(Landroid/support/v4/app/df;)Landroid/os/Bundle;
invokespecial android/os/Bundle/<init>(Landroid/os/Bundle;)V
invokespecial android/support/v4/app/dh/<init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;)V
return
.limit locals 2
.limit stack 7
.end method

.method private a()Landroid/os/Bundle;
aload 0
getfield android/support/v4/app/dh/a Landroid/os/Bundle;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Landroid/os/Bundle;)Landroid/support/v4/app/dh;
aload 1
ifnull L0
aload 0
getfield android/support/v4/app/dh/a Landroid/os/Bundle;
aload 1
invokevirtual android/os/Bundle/putAll(Landroid/os/Bundle;)V
L0:
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/support/v4/app/di;)Landroid/support/v4/app/dh;
aload 1
aload 0
invokeinterface android/support/v4/app/di/a(Landroid/support/v4/app/dh;)Landroid/support/v4/app/dh; 1
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/support/v4/app/fn;)Landroid/support/v4/app/dh;
aload 0
getfield android/support/v4/app/dh/e Ljava/util/ArrayList;
ifnonnull L0
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v4/app/dh/e Ljava/util/ArrayList;
L0:
aload 0
getfield android/support/v4/app/dh/e Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
areturn
.limit locals 2
.limit stack 3
.end method

.method private b()Landroid/support/v4/app/df;
aload 0
getfield android/support/v4/app/dh/e Ljava/util/ArrayList;
ifnull L0
aload 0
getfield android/support/v4/app/dh/e Ljava/util/ArrayList;
aload 0
getfield android/support/v4/app/dh/e Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
anewarray android/support/v4/app/fn
invokevirtual java/util/ArrayList/toArray([Ljava/lang/Object;)[Ljava/lang/Object;
checkcast [Landroid/support/v4/app/fn;
astore 1
L1:
new android/support/v4/app/df
dup
aload 0
getfield android/support/v4/app/dh/b I
aload 0
getfield android/support/v4/app/dh/c Ljava/lang/CharSequence;
aload 0
getfield android/support/v4/app/dh/d Landroid/app/PendingIntent;
aload 0
getfield android/support/v4/app/dh/a Landroid/os/Bundle;
aload 1
iconst_0
invokespecial android/support/v4/app/df/<init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[Landroid/support/v4/app/fn;B)V
areturn
L0:
aconst_null
astore 1
goto L1
.limit locals 2
.limit stack 8
.end method
