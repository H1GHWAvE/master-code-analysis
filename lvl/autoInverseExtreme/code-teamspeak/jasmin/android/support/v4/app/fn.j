.bytecode 50.0
.class public final synchronized android/support/v4/app/fn
.super android/support/v4/app/fw

.field public static final 'a' Ljava/lang/String; = "android.remoteinput.results"

.field public static final 'b' Ljava/lang/String; = "android.remoteinput.resultsData"

.field public static final 'c' Landroid/support/v4/app/fx;

.field private static final 'd' Ljava/lang/String; = "RemoteInput"

.field private static final 'j' Landroid/support/v4/app/fq;

.field private final 'e' Ljava/lang/String;

.field private final 'f' Ljava/lang/CharSequence;

.field private final 'g' [Ljava/lang/CharSequence;

.field private final 'h' Z

.field private final 'i' Landroid/os/Bundle;

.method static <clinit>()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 20
if_icmplt L0
new android/support/v4/app/fr
dup
invokespecial android/support/v4/app/fr/<init>()V
putstatic android/support/v4/app/fn/j Landroid/support/v4/app/fq;
L1:
new android/support/v4/app/fo
dup
invokespecial android/support/v4/app/fo/<init>()V
putstatic android/support/v4/app/fn/c Landroid/support/v4/app/fx;
return
L0:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 16
if_icmplt L2
new android/support/v4/app/ft
dup
invokespecial android/support/v4/app/ft/<init>()V
putstatic android/support/v4/app/fn/j Landroid/support/v4/app/fq;
goto L1
L2:
new android/support/v4/app/fs
dup
invokespecial android/support/v4/app/fs/<init>()V
putstatic android/support/v4/app/fn/j Landroid/support/v4/app/fq;
goto L1
.limit locals 0
.limit stack 2
.end method

.method <init>(Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/CharSequence;ZLandroid/os/Bundle;)V
aload 0
invokespecial android/support/v4/app/fw/<init>()V
aload 0
aload 1
putfield android/support/v4/app/fn/e Ljava/lang/String;
aload 0
aload 2
putfield android/support/v4/app/fn/f Ljava/lang/CharSequence;
aload 0
aload 3
putfield android/support/v4/app/fn/g [Ljava/lang/CharSequence;
aload 0
iload 4
putfield android/support/v4/app/fn/h Z
aload 0
aload 5
putfield android/support/v4/app/fn/i Landroid/os/Bundle;
return
.limit locals 6
.limit stack 2
.end method

.method private static a(Landroid/content/Intent;)Landroid/os/Bundle;
getstatic android/support/v4/app/fn/j Landroid/support/v4/app/fq;
aload 0
invokeinterface android/support/v4/app/fq/a(Landroid/content/Intent;)Landroid/os/Bundle; 1
areturn
.limit locals 1
.limit stack 2
.end method

.method private static a([Landroid/support/v4/app/fn;Landroid/content/Intent;Landroid/os/Bundle;)V
getstatic android/support/v4/app/fn/j Landroid/support/v4/app/fq;
aload 0
aload 1
aload 2
invokeinterface android/support/v4/app/fq/a([Landroid/support/v4/app/fn;Landroid/content/Intent;Landroid/os/Bundle;)V 3
return
.limit locals 3
.limit stack 4
.end method

.method public final a()Ljava/lang/String;
aload 0
getfield android/support/v4/app/fn/e Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final b()Ljava/lang/CharSequence;
aload 0
getfield android/support/v4/app/fn/f Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final c()[Ljava/lang/CharSequence;
aload 0
getfield android/support/v4/app/fn/g [Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final d()Z
aload 0
getfield android/support/v4/app/fn/h Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final e()Landroid/os/Bundle;
aload 0
getfield android/support/v4/app/fn/i Landroid/os/Bundle;
areturn
.limit locals 1
.limit stack 1
.end method
