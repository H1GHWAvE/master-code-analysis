.bytecode 50.0
.class final synchronized android/support/v4/app/fi
.super java/lang/Object
.implements android/content/ServiceConnection
.implements android/os/Handler$Callback

.field private static final 'b' I = 0


.field private static final 'c' I = 1


.field private static final 'd' I = 2


.field private static final 'e' I = 3


.field private static final 'f' Ljava/lang/String; = "binder"

.field final 'a' Landroid/os/Handler;

.field private final 'g' Landroid/content/Context;

.field private final 'h' Landroid/os/HandlerThread;

.field private final 'i' Ljava/util/Map;

.field private 'j' Ljava/util/Set;

.method public <init>(Landroid/content/Context;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield android/support/v4/app/fi/i Ljava/util/Map;
aload 0
new java/util/HashSet
dup
invokespecial java/util/HashSet/<init>()V
putfield android/support/v4/app/fi/j Ljava/util/Set;
aload 0
aload 1
putfield android/support/v4/app/fi/g Landroid/content/Context;
aload 0
new android/os/HandlerThread
dup
ldc "NotificationManagerCompat"
invokespecial android/os/HandlerThread/<init>(Ljava/lang/String;)V
putfield android/support/v4/app/fi/h Landroid/os/HandlerThread;
aload 0
getfield android/support/v4/app/fi/h Landroid/os/HandlerThread;
invokevirtual android/os/HandlerThread/start()V
aload 0
new android/os/Handler
dup
aload 0
getfield android/support/v4/app/fi/h Landroid/os/HandlerThread;
invokevirtual android/os/HandlerThread/getLooper()Landroid/os/Looper;
aload 0
invokespecial android/os/Handler/<init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V
putfield android/support/v4/app/fi/a Landroid/os/Handler;
return
.limit locals 2
.limit stack 5
.end method

.method private a()V
aload 0
getfield android/support/v4/app/fi/g Landroid/content/Context;
invokestatic android/support/v4/app/fa/a(Landroid/content/Context;)Ljava/util/Set;
astore 2
aload 2
aload 0
getfield android/support/v4/app/fi/j Ljava/util/Set;
invokeinterface java/util/Set/equals(Ljava/lang/Object;)Z 1
ifeq L0
L1:
return
L0:
aload 0
aload 2
putfield android/support/v4/app/fi/j Ljava/util/Set;
aload 0
getfield android/support/v4/app/fi/g Landroid/content/Context;
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
ldc "android.support.BIND_NOTIFICATION_SIDE_CHANNEL"
invokevirtual android/content/Intent/setAction(Ljava/lang/String;)Landroid/content/Intent;
iconst_4
invokevirtual android/content/pm/PackageManager/queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;
astore 3
new java/util/HashSet
dup
invokespecial java/util/HashSet/<init>()V
astore 1
aload 3
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 3
L2:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/content/pm/ResolveInfo
astore 4
aload 2
aload 4
getfield android/content/pm/ResolveInfo/serviceInfo Landroid/content/pm/ServiceInfo;
getfield android/content/pm/ServiceInfo/packageName Ljava/lang/String;
invokeinterface java/util/Set/contains(Ljava/lang/Object;)Z 1
ifeq L2
new android/content/ComponentName
dup
aload 4
getfield android/content/pm/ResolveInfo/serviceInfo Landroid/content/pm/ServiceInfo;
getfield android/content/pm/ServiceInfo/packageName Ljava/lang/String;
aload 4
getfield android/content/pm/ResolveInfo/serviceInfo Landroid/content/pm/ServiceInfo;
getfield android/content/pm/ServiceInfo/name Ljava/lang/String;
invokespecial android/content/ComponentName/<init>(Ljava/lang/String;Ljava/lang/String;)V
astore 5
aload 4
getfield android/content/pm/ResolveInfo/serviceInfo Landroid/content/pm/ServiceInfo;
getfield android/content/pm/ServiceInfo/permission Ljava/lang/String;
ifnull L4
ldc "NotifManCompat"
new java/lang/StringBuilder
dup
ldc "Permission present on component "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", not adding listener record."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
goto L2
L4:
aload 1
aload 5
invokeinterface java/util/Set/add(Ljava/lang/Object;)Z 1
pop
goto L2
L3:
aload 1
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 2
L5:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L6
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/content/ComponentName
astore 3
aload 0
getfield android/support/v4/app/fi/i Ljava/util/Map;
aload 3
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ifne L5
ldc "NotifManCompat"
iconst_3
invokestatic android/util/Log/isLoggable(Ljava/lang/String;I)Z
ifeq L7
ldc "NotifManCompat"
new java/lang/StringBuilder
dup
ldc "Adding listener record for "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
L7:
aload 0
getfield android/support/v4/app/fi/i Ljava/util/Map;
aload 3
new android/support/v4/app/fj
dup
aload 3
invokespecial android/support/v4/app/fj/<init>(Landroid/content/ComponentName;)V
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
goto L5
L6:
aload 0
getfield android/support/v4/app/fi/i Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 2
L8:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 3
aload 1
aload 3
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
invokeinterface java/util/Set/contains(Ljava/lang/Object;)Z 1
ifne L8
ldc "NotifManCompat"
iconst_3
invokestatic android/util/Log/isLoggable(Ljava/lang/String;I)Z
ifeq L9
ldc "NotifManCompat"
new java/lang/StringBuilder
dup
ldc "Removing listener record for "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 3
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
L9:
aload 0
aload 3
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast android/support/v4/app/fj
invokespecial android/support/v4/app/fi/b(Landroid/support/v4/app/fj;)V
aload 2
invokeinterface java/util/Iterator/remove()V 0
goto L8
.limit locals 6
.limit stack 5
.end method

.method private a(Landroid/content/ComponentName;)V
aload 0
getfield android/support/v4/app/fi/i Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast android/support/v4/app/fj
astore 1
aload 1
ifnull L0
aload 0
aload 1
invokespecial android/support/v4/app/fi/b(Landroid/support/v4/app/fj;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/content/ComponentName;Landroid/os/IBinder;)V
aload 0
getfield android/support/v4/app/fi/i Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast android/support/v4/app/fj
astore 1
aload 1
ifnull L0
aload 1
aload 2
invokestatic android/support/v4/app/cm/a(Landroid/os/IBinder;)Landroid/support/v4/app/cl;
putfield android/support/v4/app/fj/c Landroid/support/v4/app/cl;
aload 1
iconst_0
putfield android/support/v4/app/fj/e I
aload 0
aload 1
invokespecial android/support/v4/app/fi/d(Landroid/support/v4/app/fj;)V
L0:
return
.limit locals 3
.limit stack 2
.end method

.method private a(Landroid/support/v4/app/fk;)V
aload 0
getfield android/support/v4/app/fi/a Landroid/os/Handler;
iconst_0
aload 1
invokevirtual android/os/Handler/obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
invokevirtual android/os/Message/sendToTarget()V
return
.limit locals 2
.limit stack 3
.end method

.method private a(Landroid/support/v4/app/fj;)Z
aload 1
getfield android/support/v4/app/fj/b Z
ifeq L0
iconst_1
ireturn
L0:
new android/content/Intent
dup
ldc "android.support.BIND_NOTIFICATION_SIDE_CHANNEL"
invokespecial android/content/Intent/<init>(Ljava/lang/String;)V
aload 1
getfield android/support/v4/app/fj/a Landroid/content/ComponentName;
invokevirtual android/content/Intent/setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
astore 2
aload 1
aload 0
getfield android/support/v4/app/fi/g Landroid/content/Context;
aload 2
aload 0
invokestatic android/support/v4/app/fa/a()I
invokevirtual android/content/Context/bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
putfield android/support/v4/app/fj/b Z
aload 1
getfield android/support/v4/app/fj/b Z
ifeq L1
aload 1
iconst_0
putfield android/support/v4/app/fj/e I
L2:
aload 1
getfield android/support/v4/app/fj/b Z
ireturn
L1:
ldc "NotifManCompat"
new java/lang/StringBuilder
dup
ldc "Unable to bind to listener "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
getfield android/support/v4/app/fj/a Landroid/content/ComponentName;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield android/support/v4/app/fi/g Landroid/content/Context;
aload 0
invokevirtual android/content/Context/unbindService(Landroid/content/ServiceConnection;)V
goto L2
.limit locals 3
.limit stack 5
.end method

.method private b(Landroid/content/ComponentName;)V
aload 0
getfield android/support/v4/app/fi/i Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast android/support/v4/app/fj
astore 1
aload 1
ifnull L0
aload 0
aload 1
invokespecial android/support/v4/app/fi/d(Landroid/support/v4/app/fj;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private b(Landroid/support/v4/app/fj;)V
aload 1
getfield android/support/v4/app/fj/b Z
ifeq L0
aload 0
getfield android/support/v4/app/fi/g Landroid/content/Context;
aload 0
invokevirtual android/content/Context/unbindService(Landroid/content/ServiceConnection;)V
aload 1
iconst_0
putfield android/support/v4/app/fj/b Z
L0:
aload 1
aconst_null
putfield android/support/v4/app/fj/c Landroid/support/v4/app/cl;
return
.limit locals 2
.limit stack 2
.end method

.method private b(Landroid/support/v4/app/fk;)V
aload 0
getfield android/support/v4/app/fi/g Landroid/content/Context;
invokestatic android/support/v4/app/fa/a(Landroid/content/Context;)Ljava/util/Set;
astore 3
aload 3
aload 0
getfield android/support/v4/app/fi/j Ljava/util/Set;
invokeinterface java/util/Set/equals(Ljava/lang/Object;)Z 1
ifne L0
aload 0
aload 3
putfield android/support/v4/app/fi/j Ljava/util/Set;
aload 0
getfield android/support/v4/app/fi/g Landroid/content/Context;
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
ldc "android.support.BIND_NOTIFICATION_SIDE_CHANNEL"
invokevirtual android/content/Intent/setAction(Ljava/lang/String;)Landroid/content/Intent;
iconst_4
invokevirtual android/content/pm/PackageManager/queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;
astore 4
new java/util/HashSet
dup
invokespecial java/util/HashSet/<init>()V
astore 2
aload 4
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 4
L1:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/content/pm/ResolveInfo
astore 5
aload 3
aload 5
getfield android/content/pm/ResolveInfo/serviceInfo Landroid/content/pm/ServiceInfo;
getfield android/content/pm/ServiceInfo/packageName Ljava/lang/String;
invokeinterface java/util/Set/contains(Ljava/lang/Object;)Z 1
ifeq L1
new android/content/ComponentName
dup
aload 5
getfield android/content/pm/ResolveInfo/serviceInfo Landroid/content/pm/ServiceInfo;
getfield android/content/pm/ServiceInfo/packageName Ljava/lang/String;
aload 5
getfield android/content/pm/ResolveInfo/serviceInfo Landroid/content/pm/ServiceInfo;
getfield android/content/pm/ServiceInfo/name Ljava/lang/String;
invokespecial android/content/ComponentName/<init>(Ljava/lang/String;Ljava/lang/String;)V
astore 6
aload 5
getfield android/content/pm/ResolveInfo/serviceInfo Landroid/content/pm/ServiceInfo;
getfield android/content/pm/ServiceInfo/permission Ljava/lang/String;
ifnull L3
ldc "NotifManCompat"
new java/lang/StringBuilder
dup
ldc "Permission present on component "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", not adding listener record."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
goto L1
L3:
aload 2
aload 6
invokeinterface java/util/Set/add(Ljava/lang/Object;)Z 1
pop
goto L1
L2:
aload 2
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 3
L4:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L5
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/content/ComponentName
astore 4
aload 0
getfield android/support/v4/app/fi/i Ljava/util/Map;
aload 4
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ifne L4
ldc "NotifManCompat"
iconst_3
invokestatic android/util/Log/isLoggable(Ljava/lang/String;I)Z
ifeq L6
ldc "NotifManCompat"
new java/lang/StringBuilder
dup
ldc "Adding listener record for "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
L6:
aload 0
getfield android/support/v4/app/fi/i Ljava/util/Map;
aload 4
new android/support/v4/app/fj
dup
aload 4
invokespecial android/support/v4/app/fj/<init>(Landroid/content/ComponentName;)V
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
goto L4
L5:
aload 0
getfield android/support/v4/app/fi/i Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 3
L7:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 4
aload 2
aload 4
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
invokeinterface java/util/Set/contains(Ljava/lang/Object;)Z 1
ifne L7
ldc "NotifManCompat"
iconst_3
invokestatic android/util/Log/isLoggable(Ljava/lang/String;I)Z
ifeq L8
ldc "NotifManCompat"
new java/lang/StringBuilder
dup
ldc "Removing listener record for "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 4
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
L8:
aload 0
aload 4
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast android/support/v4/app/fj
invokespecial android/support/v4/app/fi/b(Landroid/support/v4/app/fj;)V
aload 3
invokeinterface java/util/Iterator/remove()V 0
goto L7
L0:
aload 0
getfield android/support/v4/app/fi/i Ljava/util/Map;
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 2
L9:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L10
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/support/v4/app/fj
astore 3
aload 3
getfield android/support/v4/app/fj/d Ljava/util/LinkedList;
aload 1
invokevirtual java/util/LinkedList/add(Ljava/lang/Object;)Z
pop
aload 0
aload 3
invokespecial android/support/v4/app/fi/d(Landroid/support/v4/app/fj;)V
goto L9
L10:
return
.limit locals 7
.limit stack 5
.end method

.method private c(Landroid/support/v4/app/fj;)V
aload 0
getfield android/support/v4/app/fi/a Landroid/os/Handler;
iconst_3
aload 1
getfield android/support/v4/app/fj/a Landroid/content/ComponentName;
invokevirtual android/os/Handler/hasMessages(ILjava/lang/Object;)Z
ifeq L0
return
L0:
aload 1
aload 1
getfield android/support/v4/app/fj/e I
iconst_1
iadd
putfield android/support/v4/app/fj/e I
aload 1
getfield android/support/v4/app/fj/e I
bipush 6
if_icmple L1
ldc "NotifManCompat"
new java/lang/StringBuilder
dup
ldc "Giving up on delivering "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
getfield android/support/v4/app/fj/d Ljava/util/LinkedList;
invokevirtual java/util/LinkedList/size()I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " tasks to "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
getfield android/support/v4/app/fj/a Landroid/content/ComponentName;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " after "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
getfield android/support/v4/app/fj/e I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " retries"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 1
getfield android/support/v4/app/fj/d Ljava/util/LinkedList;
invokevirtual java/util/LinkedList/clear()V
return
L1:
iconst_1
aload 1
getfield android/support/v4/app/fj/e I
iconst_1
isub
ishl
sipush 1000
imul
istore 2
ldc "NotifManCompat"
iconst_3
invokestatic android/util/Log/isLoggable(Ljava/lang/String;I)Z
ifeq L2
ldc "NotifManCompat"
new java/lang/StringBuilder
dup
ldc "Scheduling retry for "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " ms"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
L2:
aload 0
getfield android/support/v4/app/fi/a Landroid/os/Handler;
iconst_3
aload 1
getfield android/support/v4/app/fj/a Landroid/content/ComponentName;
invokevirtual android/os/Handler/obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
astore 1
aload 0
getfield android/support/v4/app/fi/a Landroid/os/Handler;
aload 1
iload 2
i2l
invokevirtual android/os/Handler/sendMessageDelayed(Landroid/os/Message;J)Z
pop
return
.limit locals 3
.limit stack 4
.end method

.method private d(Landroid/support/v4/app/fj;)V
.catch android/os/DeadObjectException from L0 to L1 using L2
.catch android/os/RemoteException from L0 to L1 using L3
.catch android/os/DeadObjectException from L1 to L4 using L2
.catch android/os/RemoteException from L1 to L4 using L3
ldc "NotifManCompat"
iconst_3
invokestatic android/util/Log/isLoggable(Ljava/lang/String;I)Z
ifeq L5
ldc "NotifManCompat"
new java/lang/StringBuilder
dup
ldc "Processing component "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
getfield android/support/v4/app/fj/a Landroid/content/ComponentName;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
getfield android/support/v4/app/fj/d Ljava/util/LinkedList;
invokevirtual java/util/LinkedList/size()I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " queued tasks"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
L5:
aload 1
getfield android/support/v4/app/fj/d Ljava/util/LinkedList;
invokevirtual java/util/LinkedList/isEmpty()Z
ifeq L6
L7:
return
L6:
aload 1
getfield android/support/v4/app/fj/b Z
ifeq L8
iconst_1
istore 2
L9:
iload 2
ifeq L10
aload 1
getfield android/support/v4/app/fj/c Landroid/support/v4/app/cl;
ifnonnull L11
L10:
aload 0
aload 1
invokespecial android/support/v4/app/fi/c(Landroid/support/v4/app/fj;)V
return
L8:
new android/content/Intent
dup
ldc "android.support.BIND_NOTIFICATION_SIDE_CHANNEL"
invokespecial android/content/Intent/<init>(Ljava/lang/String;)V
aload 1
getfield android/support/v4/app/fj/a Landroid/content/ComponentName;
invokevirtual android/content/Intent/setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
astore 3
aload 1
aload 0
getfield android/support/v4/app/fi/g Landroid/content/Context;
aload 3
aload 0
invokestatic android/support/v4/app/fa/a()I
invokevirtual android/content/Context/bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
putfield android/support/v4/app/fj/b Z
aload 1
getfield android/support/v4/app/fj/b Z
ifeq L12
aload 1
iconst_0
putfield android/support/v4/app/fj/e I
L13:
aload 1
getfield android/support/v4/app/fj/b Z
istore 2
goto L9
L12:
ldc "NotifManCompat"
new java/lang/StringBuilder
dup
ldc "Unable to bind to listener "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
getfield android/support/v4/app/fj/a Landroid/content/ComponentName;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
getfield android/support/v4/app/fi/g Landroid/content/Context;
aload 0
invokevirtual android/content/Context/unbindService(Landroid/content/ServiceConnection;)V
goto L13
L11:
aload 1
getfield android/support/v4/app/fj/d Ljava/util/LinkedList;
invokevirtual java/util/LinkedList/peek()Ljava/lang/Object;
checkcast android/support/v4/app/fk
astore 3
aload 3
ifnull L14
L0:
ldc "NotifManCompat"
iconst_3
invokestatic android/util/Log/isLoggable(Ljava/lang/String;I)Z
ifeq L1
ldc "NotifManCompat"
new java/lang/StringBuilder
dup
ldc "Sending task "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
L1:
aload 3
aload 1
getfield android/support/v4/app/fj/c Landroid/support/v4/app/cl;
invokeinterface android/support/v4/app/fk/a(Landroid/support/v4/app/cl;)V 1
aload 1
getfield android/support/v4/app/fj/d Ljava/util/LinkedList;
invokevirtual java/util/LinkedList/remove()Ljava/lang/Object;
pop
L4:
goto L11
L2:
astore 3
ldc "NotifManCompat"
iconst_3
invokestatic android/util/Log/isLoggable(Ljava/lang/String;I)Z
ifeq L14
ldc "NotifManCompat"
new java/lang/StringBuilder
dup
ldc "Remote service has died: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
getfield android/support/v4/app/fj/a Landroid/content/ComponentName;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
L14:
aload 1
getfield android/support/v4/app/fj/d Ljava/util/LinkedList;
invokevirtual java/util/LinkedList/isEmpty()Z
ifne L7
aload 0
aload 1
invokespecial android/support/v4/app/fi/c(Landroid/support/v4/app/fj;)V
return
L3:
astore 3
ldc "NotifManCompat"
new java/lang/StringBuilder
dup
ldc "RemoteException communicating with "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
getfield android/support/v4/app/fj/a Landroid/content/ComponentName;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 3
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
goto L14
.limit locals 4
.limit stack 5
.end method

.method public final handleMessage(Landroid/os/Message;)Z
aload 1
getfield android/os/Message/what I
tableswitch 0
L0
L1
L2
L3
default : L4
L4:
iconst_0
ireturn
L0:
aload 1
getfield android/os/Message/obj Ljava/lang/Object;
checkcast android/support/v4/app/fk
astore 1
aload 0
getfield android/support/v4/app/fi/g Landroid/content/Context;
invokestatic android/support/v4/app/fa/a(Landroid/content/Context;)Ljava/util/Set;
astore 3
aload 3
aload 0
getfield android/support/v4/app/fi/j Ljava/util/Set;
invokeinterface java/util/Set/equals(Ljava/lang/Object;)Z 1
ifne L5
aload 0
aload 3
putfield android/support/v4/app/fi/j Ljava/util/Set;
aload 0
getfield android/support/v4/app/fi/g Landroid/content/Context;
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
ldc "android.support.BIND_NOTIFICATION_SIDE_CHANNEL"
invokevirtual android/content/Intent/setAction(Ljava/lang/String;)Landroid/content/Intent;
iconst_4
invokevirtual android/content/pm/PackageManager/queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;
astore 4
new java/util/HashSet
dup
invokespecial java/util/HashSet/<init>()V
astore 2
aload 4
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 4
L6:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L7
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/content/pm/ResolveInfo
astore 5
aload 3
aload 5
getfield android/content/pm/ResolveInfo/serviceInfo Landroid/content/pm/ServiceInfo;
getfield android/content/pm/ServiceInfo/packageName Ljava/lang/String;
invokeinterface java/util/Set/contains(Ljava/lang/Object;)Z 1
ifeq L6
new android/content/ComponentName
dup
aload 5
getfield android/content/pm/ResolveInfo/serviceInfo Landroid/content/pm/ServiceInfo;
getfield android/content/pm/ServiceInfo/packageName Ljava/lang/String;
aload 5
getfield android/content/pm/ResolveInfo/serviceInfo Landroid/content/pm/ServiceInfo;
getfield android/content/pm/ServiceInfo/name Ljava/lang/String;
invokespecial android/content/ComponentName/<init>(Ljava/lang/String;Ljava/lang/String;)V
astore 6
aload 5
getfield android/content/pm/ResolveInfo/serviceInfo Landroid/content/pm/ServiceInfo;
getfield android/content/pm/ServiceInfo/permission Ljava/lang/String;
ifnull L8
ldc "NotifManCompat"
new java/lang/StringBuilder
dup
ldc "Permission present on component "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", not adding listener record."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
goto L6
L8:
aload 2
aload 6
invokeinterface java/util/Set/add(Ljava/lang/Object;)Z 1
pop
goto L6
L7:
aload 2
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 3
L9:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L10
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/content/ComponentName
astore 4
aload 0
getfield android/support/v4/app/fi/i Ljava/util/Map;
aload 4
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ifne L9
ldc "NotifManCompat"
iconst_3
invokestatic android/util/Log/isLoggable(Ljava/lang/String;I)Z
ifeq L11
ldc "NotifManCompat"
new java/lang/StringBuilder
dup
ldc "Adding listener record for "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
L11:
aload 0
getfield android/support/v4/app/fi/i Ljava/util/Map;
aload 4
new android/support/v4/app/fj
dup
aload 4
invokespecial android/support/v4/app/fj/<init>(Landroid/content/ComponentName;)V
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
goto L9
L10:
aload 0
getfield android/support/v4/app/fi/i Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 3
L12:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L5
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 4
aload 2
aload 4
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
invokeinterface java/util/Set/contains(Ljava/lang/Object;)Z 1
ifne L12
ldc "NotifManCompat"
iconst_3
invokestatic android/util/Log/isLoggable(Ljava/lang/String;I)Z
ifeq L13
ldc "NotifManCompat"
new java/lang/StringBuilder
dup
ldc "Removing listener record for "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 4
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
L13:
aload 0
aload 4
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast android/support/v4/app/fj
invokespecial android/support/v4/app/fi/b(Landroid/support/v4/app/fj;)V
aload 3
invokeinterface java/util/Iterator/remove()V 0
goto L12
L5:
aload 0
getfield android/support/v4/app/fi/i Ljava/util/Map;
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 2
L14:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L15
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/support/v4/app/fj
astore 3
aload 3
getfield android/support/v4/app/fj/d Ljava/util/LinkedList;
aload 1
invokevirtual java/util/LinkedList/add(Ljava/lang/Object;)Z
pop
aload 0
aload 3
invokespecial android/support/v4/app/fi/d(Landroid/support/v4/app/fj;)V
goto L14
L15:
iconst_1
ireturn
L1:
aload 1
getfield android/os/Message/obj Ljava/lang/Object;
checkcast android/support/v4/app/fh
astore 2
aload 2
getfield android/support/v4/app/fh/a Landroid/content/ComponentName;
astore 1
aload 2
getfield android/support/v4/app/fh/b Landroid/os/IBinder;
astore 2
aload 0
getfield android/support/v4/app/fi/i Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast android/support/v4/app/fj
astore 1
aload 1
ifnull L16
aload 1
aload 2
invokestatic android/support/v4/app/cm/a(Landroid/os/IBinder;)Landroid/support/v4/app/cl;
putfield android/support/v4/app/fj/c Landroid/support/v4/app/cl;
aload 1
iconst_0
putfield android/support/v4/app/fj/e I
aload 0
aload 1
invokespecial android/support/v4/app/fi/d(Landroid/support/v4/app/fj;)V
L16:
iconst_1
ireturn
L2:
aload 1
getfield android/os/Message/obj Ljava/lang/Object;
checkcast android/content/ComponentName
astore 1
aload 0
getfield android/support/v4/app/fi/i Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast android/support/v4/app/fj
astore 1
aload 1
ifnull L17
aload 0
aload 1
invokespecial android/support/v4/app/fi/b(Landroid/support/v4/app/fj;)V
L17:
iconst_1
ireturn
L3:
aload 1
getfield android/os/Message/obj Ljava/lang/Object;
checkcast android/content/ComponentName
astore 1
aload 0
getfield android/support/v4/app/fi/i Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast android/support/v4/app/fj
astore 1
aload 1
ifnull L18
aload 0
aload 1
invokespecial android/support/v4/app/fi/d(Landroid/support/v4/app/fj;)V
L18:
iconst_1
ireturn
.limit locals 7
.limit stack 5
.end method

.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
ldc "NotifManCompat"
iconst_3
invokestatic android/util/Log/isLoggable(Ljava/lang/String;I)Z
ifeq L0
ldc "NotifManCompat"
new java/lang/StringBuilder
dup
ldc "Connected to service "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
L0:
aload 0
getfield android/support/v4/app/fi/a Landroid/os/Handler;
iconst_1
new android/support/v4/app/fh
dup
aload 1
aload 2
invokespecial android/support/v4/app/fh/<init>(Landroid/content/ComponentName;Landroid/os/IBinder;)V
invokevirtual android/os/Handler/obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
invokevirtual android/os/Message/sendToTarget()V
return
.limit locals 3
.limit stack 6
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
ldc "NotifManCompat"
iconst_3
invokestatic android/util/Log/isLoggable(Ljava/lang/String;I)Z
ifeq L0
ldc "NotifManCompat"
new java/lang/StringBuilder
dup
ldc "Disconnected from service "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
L0:
aload 0
getfield android/support/v4/app/fi/a Landroid/os/Handler;
iconst_2
aload 1
invokevirtual android/os/Handler/obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
invokevirtual android/os/Message/sendToTarget()V
return
.limit locals 2
.limit stack 4
.end method
