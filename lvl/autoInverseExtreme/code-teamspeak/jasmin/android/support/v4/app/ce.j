.bytecode 50.0
.class final synchronized android/support/v4/app/ce
.super java/lang/Object

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static a(Landroid/view/View;)Landroid/graphics/Rect;
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
astore 1
iconst_2
newarray int
astore 2
aload 0
aload 2
invokevirtual android/view/View/getLocationOnScreen([I)V
aload 1
aload 2
iconst_0
iaload
aload 2
iconst_1
iaload
aload 2
iconst_0
iaload
aload 0
invokevirtual android/view/View/getWidth()I
iadd
aload 2
iconst_1
iaload
aload 0
invokevirtual android/view/View/getHeight()I
iadd
invokevirtual android/graphics/Rect/set(IIII)V
aload 1
areturn
.limit locals 3
.limit stack 6
.end method

.method public static a(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
astore 1
aload 0
ifnull L0
aload 0
checkcast android/transition/Transition
invokevirtual android/transition/Transition/clone()Landroid/transition/Transition;
astore 1
L0:
aload 1
areturn
.limit locals 2
.limit stack 1
.end method

.method private static a(Ljava/lang/Object;Landroid/view/View;Ljava/util/ArrayList;Ljava/util/Map;Landroid/view/View;)Ljava/lang/Object;
aload 0
astore 5
aload 0
ifnull L0
aload 2
aload 1
invokestatic android/support/v4/app/ce/a(Ljava/util/ArrayList;Landroid/view/View;)V
aload 3
ifnull L1
aload 2
aload 3
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokevirtual java/util/ArrayList/removeAll(Ljava/util/Collection;)Z
pop
L1:
aload 2
invokevirtual java/util/ArrayList/isEmpty()Z
ifeq L2
aconst_null
astore 5
L0:
aload 5
areturn
L2:
aload 2
aload 4
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
checkcast android/transition/Transition
aload 2
invokestatic android/support/v4/app/ce/b(Ljava/lang/Object;Ljava/util/ArrayList;)V
aload 0
areturn
.limit locals 6
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;
aload 0
checkcast android/transition/Transition
astore 4
aload 1
checkcast android/transition/Transition
astore 0
aload 2
checkcast android/transition/Transition
astore 2
aload 4
ifnull L0
aload 0
ifnull L0
L1:
iload 3
ifeq L2
new android/transition/TransitionSet
dup
invokespecial android/transition/TransitionSet/<init>()V
astore 1
aload 4
ifnull L3
aload 1
aload 4
invokevirtual android/transition/TransitionSet/addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;
pop
L3:
aload 0
ifnull L4
aload 1
aload 0
invokevirtual android/transition/TransitionSet/addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;
pop
L4:
aload 2
ifnull L5
aload 1
aload 2
invokevirtual android/transition/TransitionSet/addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;
pop
L5:
aload 1
areturn
L2:
aconst_null
astore 1
aload 0
ifnull L6
aload 4
ifnull L6
new android/transition/TransitionSet
dup
invokespecial android/transition/TransitionSet/<init>()V
aload 0
invokevirtual android/transition/TransitionSet/addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;
aload 4
invokevirtual android/transition/TransitionSet/addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;
iconst_1
invokevirtual android/transition/TransitionSet/setOrdering(I)Landroid/transition/TransitionSet;
astore 0
L7:
aload 2
ifnull L8
new android/transition/TransitionSet
dup
invokespecial android/transition/TransitionSet/<init>()V
astore 1
aload 0
ifnull L9
aload 1
aload 0
invokevirtual android/transition/TransitionSet/addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;
pop
L9:
aload 1
aload 2
invokevirtual android/transition/TransitionSet/addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;
pop
aload 1
areturn
L6:
aload 0
ifnull L10
goto L7
L10:
aload 1
astore 0
aload 4
ifnull L7
aload 4
astore 0
goto L7
L8:
aload 0
areturn
L0:
iconst_1
istore 3
goto L1
.limit locals 5
.limit stack 2
.end method

.method private static a(Landroid/transition/Transition;Landroid/support/v4/app/cj;)V
aload 0
ifnull L0
aload 0
new android/support/v4/app/ch
dup
aload 1
invokespecial android/support/v4/app/ch/<init>(Landroid/support/v4/app/cj;)V
invokevirtual android/transition/Transition/setEpicenterCallback(Landroid/transition/Transition$EpicenterCallback;)V
L0:
return
.limit locals 2
.limit stack 4
.end method

.method private static a(Landroid/view/View;Landroid/view/View;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/util/Map;)V
aload 2
checkcast android/transition/Transition
astore 2
aload 4
checkcast android/transition/Transition
astore 4
aload 6
checkcast android/transition/Transition
astore 6
aload 8
checkcast android/transition/Transition
astore 8
aload 8
ifnull L0
aload 0
invokevirtual android/view/View/getViewTreeObserver()Landroid/view/ViewTreeObserver;
new android/support/v4/app/ci
dup
aload 0
aload 2
aload 3
aload 4
aload 5
aload 6
aload 7
aload 10
aload 9
aload 8
aload 1
invokespecial android/support/v4/app/ci/<init>(Landroid/view/View;Landroid/transition/Transition;Ljava/util/ArrayList;Landroid/transition/Transition;Ljava/util/ArrayList;Landroid/transition/Transition;Ljava/util/ArrayList;Ljava/util/Map;Ljava/util/ArrayList;Landroid/transition/Transition;Landroid/view/View;)V
invokevirtual android/view/ViewTreeObserver/addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V
L0:
return
.limit locals 11
.limit stack 14
.end method

.method private static a(Landroid/view/ViewGroup;Ljava/lang/Object;)V
aload 0
aload 1
checkcast android/transition/Transition
invokestatic android/transition/TransitionManager/beginDelayedTransition(Landroid/view/ViewGroup;Landroid/transition/Transition;)V
return
.limit locals 2
.limit stack 2
.end method

.method public static a(Ljava/lang/Object;Landroid/view/View;)V
aload 0
checkcast android/transition/Transition
new android/support/v4/app/cf
dup
aload 1
invokestatic android/support/v4/app/ce/a(Landroid/view/View;)Landroid/graphics/Rect;
invokespecial android/support/v4/app/cf/<init>(Landroid/graphics/Rect;)V
invokevirtual android/transition/Transition/setEpicenterCallback(Landroid/transition/Transition$EpicenterCallback;)V
return
.limit locals 2
.limit stack 4
.end method

.method public static a(Ljava/lang/Object;Landroid/view/View;Ljava/util/Map;Ljava/util/ArrayList;)V
aload 0
checkcast android/transition/TransitionSet
astore 0
aload 3
invokevirtual java/util/ArrayList/clear()V
aload 3
aload 2
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokevirtual java/util/ArrayList/addAll(Ljava/util/Collection;)Z
pop
aload 0
invokevirtual android/transition/TransitionSet/getTargets()Ljava/util/List;
astore 2
aload 2
invokeinterface java/util/List/clear()V 0
aload 3
invokevirtual java/util/ArrayList/size()I
istore 8
iconst_0
istore 4
L0:
iload 4
iload 8
if_icmpge L1
aload 3
iload 4
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/view/View
astore 10
aload 2
invokeinterface java/util/List/size()I 0
istore 7
aload 2
aload 10
iload 7
invokestatic android/support/v4/app/ce/a(Ljava/util/List;Landroid/view/View;I)Z
ifne L2
aload 2
aload 10
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
iload 7
istore 5
L3:
iload 5
aload 2
invokeinterface java/util/List/size()I 0
if_icmpge L2
aload 2
iload 5
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/view/View
astore 10
aload 10
instanceof android/view/ViewGroup
ifeq L4
aload 10
checkcast android/view/ViewGroup
astore 10
aload 10
invokevirtual android/view/ViewGroup/getChildCount()I
istore 9
iconst_0
istore 6
L5:
iload 6
iload 9
if_icmpge L4
aload 10
iload 6
invokevirtual android/view/ViewGroup/getChildAt(I)Landroid/view/View;
astore 11
aload 2
aload 11
iload 7
invokestatic android/support/v4/app/ce/a(Ljava/util/List;Landroid/view/View;I)Z
ifne L6
aload 2
aload 11
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L6:
iload 6
iconst_1
iadd
istore 6
goto L5
L4:
iload 5
iconst_1
iadd
istore 5
goto L3
L2:
iload 4
iconst_1
iadd
istore 4
goto L0
L1:
aload 3
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
aload 3
invokestatic android/support/v4/app/ce/b(Ljava/lang/Object;Ljava/util/ArrayList;)V
return
.limit locals 12
.limit stack 3
.end method

.method public static a(Ljava/lang/Object;Landroid/view/View;Z)V
aload 0
checkcast android/transition/Transition
aload 1
iload 2
invokevirtual android/transition/Transition/excludeTarget(Landroid/view/View;Z)Landroid/transition/Transition;
pop
return
.limit locals 3
.limit stack 3
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;Landroid/support/v4/app/ck;Landroid/view/View;Landroid/support/v4/app/cj;Ljava/util/Map;Ljava/util/ArrayList;Ljava/util/Map;Ljava/util/Map;Ljava/util/ArrayList;)V
aload 0
ifnonnull L0
aload 1
ifnull L1
L0:
aload 0
checkcast android/transition/Transition
astore 0
aload 0
ifnull L2
aload 0
aload 4
invokevirtual android/transition/Transition/addTarget(Landroid/view/View;)Landroid/transition/Transition;
pop
L2:
aload 1
ifnull L3
aload 1
aload 4
aload 8
aload 10
invokestatic android/support/v4/app/ce/a(Ljava/lang/Object;Landroid/view/View;Ljava/util/Map;Ljava/util/ArrayList;)V
L3:
aload 2
invokevirtual android/view/View/getViewTreeObserver()Landroid/view/ViewTreeObserver;
new android/support/v4/app/cg
dup
aload 2
aload 0
aload 4
aload 3
aload 6
aload 9
aload 7
invokespecial android/support/v4/app/cg/<init>(Landroid/view/View;Landroid/transition/Transition;Landroid/view/View;Landroid/support/v4/app/ck;Ljava/util/Map;Ljava/util/Map;Ljava/util/ArrayList;)V
invokevirtual android/view/ViewTreeObserver/addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V
aload 0
ifnull L1
aload 0
new android/support/v4/app/ch
dup
aload 5
invokespecial android/support/v4/app/ch/<init>(Landroid/support/v4/app/cj;)V
invokevirtual android/transition/Transition/setEpicenterCallback(Landroid/transition/Transition$EpicenterCallback;)V
L1:
return
.limit locals 11
.limit stack 10
.end method

.method public static a(Ljava/lang/Object;Ljava/util/ArrayList;)V
aload 0
checkcast android/transition/Transition
astore 0
aload 0
instanceof android/transition/TransitionSet
ifeq L0
aload 0
checkcast android/transition/TransitionSet
astore 0
aload 0
invokevirtual android/transition/TransitionSet/getTransitionCount()I
istore 3
iconst_0
istore 2
L1:
iload 2
iload 3
if_icmpge L2
aload 0
iload 2
invokevirtual android/transition/TransitionSet/getTransitionAt(I)Landroid/transition/Transition;
aload 1
invokestatic android/support/v4/app/ce/a(Ljava/lang/Object;Ljava/util/ArrayList;)V
iload 2
iconst_1
iadd
istore 2
goto L1
L0:
aload 0
invokestatic android/support/v4/app/ce/a(Landroid/transition/Transition;)Z
ifne L2
aload 0
invokevirtual android/transition/Transition/getTargets()Ljava/util/List;
astore 4
aload 4
ifnull L2
aload 4
invokeinterface java/util/List/size()I 0
aload 1
invokevirtual java/util/ArrayList/size()I
if_icmpne L2
aload 4
aload 1
invokeinterface java/util/List/containsAll(Ljava/util/Collection;)Z 1
ifeq L2
aload 1
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
istore 2
L3:
iload 2
iflt L2
aload 0
aload 1
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/view/View
invokevirtual android/transition/Transition/removeTarget(Landroid/view/View;)Landroid/transition/Transition;
pop
iload 2
iconst_1
isub
istore 2
goto L3
L2:
return
.limit locals 5
.limit stack 3
.end method

.method static a(Ljava/util/ArrayList;Landroid/view/View;)V
aload 1
invokevirtual android/view/View/getVisibility()I
ifne L0
aload 1
instanceof android/view/ViewGroup
ifeq L1
aload 1
checkcast android/view/ViewGroup
astore 1
aload 1
invokevirtual android/view/ViewGroup/isTransitionGroup()Z
ifeq L2
aload 0
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L0:
return
L2:
aload 1
invokevirtual android/view/ViewGroup/getChildCount()I
istore 3
iconst_0
istore 2
L3:
iload 2
iload 3
if_icmpge L0
aload 0
aload 1
iload 2
invokevirtual android/view/ViewGroup/getChildAt(I)Landroid/view/View;
invokestatic android/support/v4/app/ce/a(Ljava/util/ArrayList;Landroid/view/View;)V
iload 2
iconst_1
iadd
istore 2
goto L3
L1:
aload 0
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
return
.limit locals 4
.limit stack 3
.end method

.method private static a(Ljava/util/List;Landroid/view/View;)V
aload 0
invokeinterface java/util/List/size()I 0
istore 4
aload 0
aload 1
iload 4
invokestatic android/support/v4/app/ce/a(Ljava/util/List;Landroid/view/View;I)Z
ifeq L0
L1:
return
L0:
aload 0
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
iload 4
istore 2
L2:
iload 2
aload 0
invokeinterface java/util/List/size()I 0
if_icmpge L1
aload 0
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/view/View
astore 1
aload 1
instanceof android/view/ViewGroup
ifeq L3
aload 1
checkcast android/view/ViewGroup
astore 1
aload 1
invokevirtual android/view/ViewGroup/getChildCount()I
istore 5
iconst_0
istore 3
L4:
iload 3
iload 5
if_icmpge L3
aload 1
iload 3
invokevirtual android/view/ViewGroup/getChildAt(I)Landroid/view/View;
astore 6
aload 0
aload 6
iload 4
invokestatic android/support/v4/app/ce/a(Ljava/util/List;Landroid/view/View;I)Z
ifne L5
aload 0
aload 6
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L5:
iload 3
iconst_1
iadd
istore 3
goto L4
L3:
iload 2
iconst_1
iadd
istore 2
goto L2
.limit locals 7
.limit stack 3
.end method

.method public static a(Ljava/util/Map;Landroid/view/View;)V
aload 1
invokevirtual android/view/View/getVisibility()I
ifne L0
aload 1
invokevirtual android/view/View/getTransitionName()Ljava/lang/String;
astore 4
aload 4
ifnull L1
aload 0
aload 4
aload 1
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L1:
aload 1
instanceof android/view/ViewGroup
ifeq L0
aload 1
checkcast android/view/ViewGroup
astore 1
aload 1
invokevirtual android/view/ViewGroup/getChildCount()I
istore 3
iconst_0
istore 2
L2:
iload 2
iload 3
if_icmpge L0
aload 0
aload 1
iload 2
invokevirtual android/view/ViewGroup/getChildAt(I)Landroid/view/View;
invokestatic android/support/v4/app/ce/a(Ljava/util/Map;Landroid/view/View;)V
iload 2
iconst_1
iadd
istore 2
goto L2
L0:
return
.limit locals 5
.limit stack 3
.end method

.method private static a(Landroid/transition/Transition;)Z
aload 0
invokevirtual android/transition/Transition/getTargetIds()Ljava/util/List;
invokestatic android/support/v4/app/ce/a(Ljava/util/List;)Z
ifeq L0
aload 0
invokevirtual android/transition/Transition/getTargetNames()Ljava/util/List;
invokestatic android/support/v4/app/ce/a(Ljava/util/List;)Z
ifeq L0
aload 0
invokevirtual android/transition/Transition/getTargetTypes()Ljava/util/List;
invokestatic android/support/v4/app/ce/a(Ljava/util/List;)Z
ifne L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/util/List;)Z
aload 0
ifnull L0
aload 0
invokeinterface java/util/List/isEmpty()Z 0
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/util/List;Landroid/view/View;I)Z
iconst_0
istore 5
iconst_0
istore 3
L0:
iload 5
istore 4
iload 3
iload 2
if_icmpge L1
aload 0
iload 3
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
aload 1
if_acmpne L2
iconst_1
istore 4
L1:
iload 4
ireturn
L2:
iload 3
iconst_1
iadd
istore 3
goto L0
.limit locals 6
.limit stack 2
.end method

.method private static b(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
ifnonnull L0
L1:
aconst_null
areturn
L0:
aload 0
checkcast android/transition/Transition
astore 0
aload 0
ifnull L1
new android/transition/TransitionSet
dup
invokespecial android/transition/TransitionSet/<init>()V
astore 1
aload 1
aload 0
invokevirtual android/transition/TransitionSet/addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;
pop
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method private static b(Landroid/view/View;)Ljava/lang/String;
aload 0
invokevirtual android/view/View/getTransitionName()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public static b(Ljava/lang/Object;Ljava/util/ArrayList;)V
iconst_0
istore 2
aload 0
checkcast android/transition/Transition
astore 0
aload 0
instanceof android/transition/TransitionSet
ifeq L0
aload 0
checkcast android/transition/TransitionSet
astore 0
aload 0
invokevirtual android/transition/TransitionSet/getTransitionCount()I
istore 3
L1:
iload 2
iload 3
if_icmpge L2
aload 0
iload 2
invokevirtual android/transition/TransitionSet/getTransitionAt(I)Landroid/transition/Transition;
aload 1
invokestatic android/support/v4/app/ce/b(Ljava/lang/Object;Ljava/util/ArrayList;)V
iload 2
iconst_1
iadd
istore 2
goto L1
L0:
aload 0
invokestatic android/support/v4/app/ce/a(Landroid/transition/Transition;)Z
ifne L2
aload 0
invokevirtual android/transition/Transition/getTargets()Ljava/util/List;
invokestatic android/support/v4/app/ce/a(Ljava/util/List;)Z
ifeq L2
aload 1
invokevirtual java/util/ArrayList/size()I
istore 3
iconst_0
istore 2
L3:
iload 2
iload 3
if_icmpge L2
aload 0
aload 1
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/view/View
invokevirtual android/transition/Transition/addTarget(Landroid/view/View;)Landroid/transition/Transition;
pop
iload 2
iconst_1
iadd
istore 2
goto L3
L2:
return
.limit locals 4
.limit stack 3
.end method

.method private static synthetic b(Ljava/util/ArrayList;Landroid/view/View;)V
aload 0
aload 1
invokestatic android/support/v4/app/ce/a(Ljava/util/ArrayList;Landroid/view/View;)V
return
.limit locals 2
.limit stack 2
.end method

.method private static synthetic c(Landroid/view/View;)Landroid/graphics/Rect;
aload 0
invokestatic android/support/v4/app/ce/a(Landroid/view/View;)Landroid/graphics/Rect;
areturn
.limit locals 1
.limit stack 1
.end method
