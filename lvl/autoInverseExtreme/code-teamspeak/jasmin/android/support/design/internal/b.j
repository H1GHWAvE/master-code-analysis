.bytecode 50.0
.class public final synchronized android/support/design/internal/b
.super java/lang/Object
.implements android/support/v7/internal/view/menu/x
.implements android/widget/AdapterView$OnItemClickListener

.field private static final 'j' Ljava/lang/String; = "android:menu:list"

.field private static final 'k' Ljava/lang/String; = "android:menu:adapter"

.field public 'a' Landroid/support/design/internal/NavigationMenuView;

.field public 'b' Landroid/widget/LinearLayout;

.field public 'c' I

.field public 'd' Landroid/support/design/internal/c;

.field public 'e' Landroid/view/LayoutInflater;

.field public 'f' Landroid/content/res/ColorStateList;

.field public 'g' Landroid/content/res/ColorStateList;

.field public 'h' Landroid/graphics/drawable/Drawable;

.field public 'i' I

.field private 'l' Landroid/support/v7/internal/view/menu/y;

.field private 'm' Landroid/support/v7/internal/view/menu/i;

.field private 'n' I

.field private 'o' Z

.field private 'p' I

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(Landroid/support/design/internal/b;)Landroid/view/LayoutInflater;
aload 0
getfield android/support/design/internal/b/e Landroid/view/LayoutInflater;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Landroid/graphics/drawable/Drawable;)V
aload 0
aload 1
putfield android/support/design/internal/b/h Landroid/graphics/drawable/Drawable;
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic b(Landroid/support/design/internal/b;)Landroid/content/res/ColorStateList;
aload 0
getfield android/support/design/internal/b/g Landroid/content/res/ColorStateList;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b(I)Landroid/view/View;
.annotation invisibleparam 1 Landroid/support/a/v;
.end annotation
aload 0
getfield android/support/design/internal/b/e Landroid/view/LayoutInflater;
iload 1
aload 0
getfield android/support/design/internal/b/b Landroid/widget/LinearLayout;
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
astore 2
aload 0
aload 2
invokevirtual android/support/design/internal/b/a(Landroid/view/View;)V
aload 2
areturn
.limit locals 3
.limit stack 4
.end method

.method private b(Landroid/view/View;)V
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
aload 0
getfield android/support/design/internal/b/b Landroid/widget/LinearLayout;
aload 1
invokevirtual android/widget/LinearLayout/removeView(Landroid/view/View;)V
aload 0
getfield android/support/design/internal/b/b Landroid/widget/LinearLayout;
invokevirtual android/widget/LinearLayout/getChildCount()I
ifne L0
aload 0
getfield android/support/design/internal/b/a Landroid/support/design/internal/NavigationMenuView;
iconst_0
aload 0
getfield android/support/design/internal/b/i I
iconst_0
aload 0
getfield android/support/design/internal/b/a Landroid/support/design/internal/NavigationMenuView;
invokevirtual android/support/design/internal/NavigationMenuView/getPaddingBottom()I
invokevirtual android/support/design/internal/NavigationMenuView/setPadding(IIII)V
L0:
return
.limit locals 2
.limit stack 5
.end method

.method private c(Landroid/support/v7/internal/view/menu/m;)V
aload 0
getfield android/support/design/internal/b/d Landroid/support/design/internal/c;
aload 1
invokevirtual android/support/design/internal/c/a(Landroid/support/v7/internal/view/menu/m;)V
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic c(Landroid/support/design/internal/b;)Z
aload 0
getfield android/support/design/internal/b/o Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic d(Landroid/support/design/internal/b;)I
aload 0
getfield android/support/design/internal/b/n I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d()V
aload 0
iconst_1
putfield android/support/design/internal/b/c I
return
.limit locals 1
.limit stack 2
.end method

.method private e()Landroid/content/res/ColorStateList;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/design/internal/b/g Landroid/content/res/ColorStateList;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic e(Landroid/support/design/internal/b;)Landroid/content/res/ColorStateList;
aload 0
getfield android/support/design/internal/b/f Landroid/content/res/ColorStateList;
areturn
.limit locals 1
.limit stack 1
.end method

.method private f()Landroid/content/res/ColorStateList;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/design/internal/b/f Landroid/content/res/ColorStateList;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic f(Landroid/support/design/internal/b;)Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/design/internal/b/h Landroid/graphics/drawable/Drawable;
areturn
.limit locals 1
.limit stack 1
.end method

.method private g()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/design/internal/b/h Landroid/graphics/drawable/Drawable;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic g(Landroid/support/design/internal/b;)Landroid/support/v7/internal/view/menu/i;
aload 0
getfield android/support/design/internal/b/m Landroid/support/v7/internal/view/menu/i;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic h(Landroid/support/design/internal/b;)I
aload 0
getfield android/support/design/internal/b/p I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/view/ViewGroup;)Landroid/support/v7/internal/view/menu/z;
aload 0
getfield android/support/design/internal/b/a Landroid/support/design/internal/NavigationMenuView;
ifnonnull L0
aload 0
aload 0
getfield android/support/design/internal/b/e Landroid/view/LayoutInflater;
getstatic android/support/design/k/design_navigation_menu I
aload 1
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
checkcast android/support/design/internal/NavigationMenuView
putfield android/support/design/internal/b/a Landroid/support/design/internal/NavigationMenuView;
aload 0
getfield android/support/design/internal/b/d Landroid/support/design/internal/c;
ifnonnull L1
aload 0
new android/support/design/internal/c
dup
aload 0
invokespecial android/support/design/internal/c/<init>(Landroid/support/design/internal/b;)V
putfield android/support/design/internal/b/d Landroid/support/design/internal/c;
L1:
aload 0
aload 0
getfield android/support/design/internal/b/e Landroid/view/LayoutInflater;
getstatic android/support/design/k/design_navigation_item_header I
aload 0
getfield android/support/design/internal/b/a Landroid/support/design/internal/NavigationMenuView;
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
checkcast android/widget/LinearLayout
putfield android/support/design/internal/b/b Landroid/widget/LinearLayout;
aload 0
getfield android/support/design/internal/b/a Landroid/support/design/internal/NavigationMenuView;
aload 0
getfield android/support/design/internal/b/b Landroid/widget/LinearLayout;
aconst_null
iconst_0
invokevirtual android/support/design/internal/NavigationMenuView/addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V
aload 0
getfield android/support/design/internal/b/a Landroid/support/design/internal/NavigationMenuView;
aload 0
getfield android/support/design/internal/b/d Landroid/support/design/internal/c;
invokevirtual android/support/design/internal/NavigationMenuView/setAdapter(Landroid/widget/ListAdapter;)V
aload 0
getfield android/support/design/internal/b/a Landroid/support/design/internal/NavigationMenuView;
aload 0
invokevirtual android/support/design/internal/NavigationMenuView/setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
L0:
aload 0
getfield android/support/design/internal/b/a Landroid/support/design/internal/NavigationMenuView;
areturn
.limit locals 2
.limit stack 5
.end method

.method public final a(I)V
.annotation invisibleparam 1 Landroid/support/a/ai;
.end annotation
aload 0
iload 1
putfield android/support/design/internal/b/n I
aload 0
iconst_1
putfield android/support/design/internal/b/o Z
aload 0
iconst_0
invokevirtual android/support/design/internal/b/a(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;)V
aload 0
aload 1
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
putfield android/support/design/internal/b/e Landroid/view/LayoutInflater;
aload 0
aload 2
putfield android/support/design/internal/b/m Landroid/support/v7/internal/view/menu/i;
aload 1
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
astore 1
aload 0
aload 1
getstatic android/support/design/g/design_navigation_padding_top_default I
invokevirtual android/content/res/Resources/getDimensionPixelOffset(I)I
putfield android/support/design/internal/b/i I
aload 0
aload 1
getstatic android/support/design/g/design_navigation_separator_vertical_padding I
invokevirtual android/content/res/Resources/getDimensionPixelOffset(I)I
putfield android/support/design/internal/b/p I
return
.limit locals 3
.limit stack 3
.end method

.method public final a(Landroid/content/res/ColorStateList;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
aload 1
putfield android/support/design/internal/b/g Landroid/content/res/ColorStateList;
aload 0
iconst_0
invokevirtual android/support/design/internal/b/a(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/os/Parcelable;)V
aload 1
checkcast android/os/Bundle
astore 1
aload 1
ldc "android:menu:list"
invokevirtual android/os/Bundle/getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;
astore 3
aload 3
ifnull L0
aload 0
getfield android/support/design/internal/b/a Landroid/support/design/internal/NavigationMenuView;
aload 3
invokevirtual android/support/design/internal/NavigationMenuView/restoreHierarchyState(Landroid/util/SparseArray;)V
L0:
aload 1
ldc "android:menu:adapter"
invokevirtual android/os/Bundle/getBundle(Ljava/lang/String;)Landroid/os/Bundle;
astore 3
aload 3
ifnull L1
aload 0
getfield android/support/design/internal/b/d Landroid/support/design/internal/c;
astore 1
aload 3
ldc "android:menu:checked"
iconst_0
invokevirtual android/os/Bundle/getInt(Ljava/lang/String;I)I
istore 2
iload 2
ifeq L1
aload 1
iconst_1
putfield android/support/design/internal/c/c Z
aload 1
getfield android/support/design/internal/c/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 3
L2:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/support/design/internal/d
getfield android/support/design/internal/d/a Landroid/support/v7/internal/view/menu/m;
astore 4
aload 4
ifnull L2
aload 4
invokevirtual android/support/v7/internal/view/menu/m/getItemId()I
iload 2
if_icmpne L2
aload 1
aload 4
invokevirtual android/support/design/internal/c/a(Landroid/support/v7/internal/view/menu/m;)V
L3:
aload 1
iconst_0
putfield android/support/design/internal/c/c Z
aload 1
invokevirtual android/support/design/internal/c/a()V
L1:
return
.limit locals 5
.limit stack 3
.end method

.method public final a(Landroid/support/v7/internal/view/menu/i;Z)V
aload 0
getfield android/support/design/internal/b/l Landroid/support/v7/internal/view/menu/y;
ifnull L0
aload 0
getfield android/support/design/internal/b/l Landroid/support/v7/internal/view/menu/y;
aload 1
iload 2
invokeinterface android/support/v7/internal/view/menu/y/a(Landroid/support/v7/internal/view/menu/i;Z)V 2
L0:
return
.limit locals 3
.limit stack 3
.end method

.method public final a(Landroid/support/v7/internal/view/menu/y;)V
aload 0
aload 1
putfield android/support/design/internal/b/l Landroid/support/v7/internal/view/menu/y;
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/view/View;)V
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
aload 0
getfield android/support/design/internal/b/b Landroid/widget/LinearLayout;
aload 1
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 0
getfield android/support/design/internal/b/a Landroid/support/design/internal/NavigationMenuView;
iconst_0
iconst_0
iconst_0
aload 0
getfield android/support/design/internal/b/a Landroid/support/design/internal/NavigationMenuView;
invokevirtual android/support/design/internal/NavigationMenuView/getPaddingBottom()I
invokevirtual android/support/design/internal/NavigationMenuView/setPadding(IIII)V
return
.limit locals 2
.limit stack 5
.end method

.method public final a(Z)V
aload 0
getfield android/support/design/internal/b/d Landroid/support/design/internal/c;
ifnull L0
aload 0
getfield android/support/design/internal/b/d Landroid/support/design/internal/c;
invokevirtual android/support/design/internal/c/notifyDataSetChanged()V
L0:
return
.limit locals 2
.limit stack 1
.end method

.method public final a()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/support/v7/internal/view/menu/ad;)Z
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final a(Landroid/support/v7/internal/view/menu/m;)Z
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final b()I
aload 0
getfield android/support/design/internal/b/c I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final b(Landroid/content/res/ColorStateList;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
aload 1
putfield android/support/design/internal/b/f Landroid/content/res/ColorStateList;
aload 0
iconst_0
invokevirtual android/support/design/internal/b/a(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method public final b(Z)V
aload 0
getfield android/support/design/internal/b/d Landroid/support/design/internal/c;
ifnull L0
aload 0
getfield android/support/design/internal/b/d Landroid/support/design/internal/c;
iload 1
putfield android/support/design/internal/c/c Z
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final b(Landroid/support/v7/internal/view/menu/m;)Z
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final c()Landroid/os/Parcelable;
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 1
aload 0
getfield android/support/design/internal/b/a Landroid/support/design/internal/NavigationMenuView;
ifnull L0
new android/util/SparseArray
dup
invokespecial android/util/SparseArray/<init>()V
astore 2
aload 0
getfield android/support/design/internal/b/a Landroid/support/design/internal/NavigationMenuView;
aload 2
invokevirtual android/support/design/internal/NavigationMenuView/saveHierarchyState(Landroid/util/SparseArray;)V
aload 1
ldc "android:menu:list"
aload 2
invokevirtual android/os/Bundle/putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V
L0:
aload 0
getfield android/support/design/internal/b/d Landroid/support/design/internal/c;
ifnull L1
aload 0
getfield android/support/design/internal/b/d Landroid/support/design/internal/c;
astore 2
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 3
aload 2
getfield android/support/design/internal/c/b Landroid/support/v7/internal/view/menu/m;
ifnull L2
aload 3
ldc "android:menu:checked"
aload 2
getfield android/support/design/internal/c/b Landroid/support/v7/internal/view/menu/m;
invokevirtual android/support/v7/internal/view/menu/m/getItemId()I
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
L2:
aload 1
ldc "android:menu:adapter"
aload 3
invokevirtual android/os/Bundle/putBundle(Ljava/lang/String;Landroid/os/Bundle;)V
L1:
aload 1
areturn
.limit locals 4
.limit stack 3
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
iload 3
aload 0
getfield android/support/design/internal/b/a Landroid/support/design/internal/NavigationMenuView;
invokevirtual android/support/design/internal/NavigationMenuView/getHeaderViewsCount()I
isub
istore 3
iload 3
iflt L0
aload 0
iconst_1
invokevirtual android/support/design/internal/b/b(Z)V
aload 0
getfield android/support/design/internal/b/d Landroid/support/design/internal/c;
iload 3
invokevirtual android/support/design/internal/c/a(I)Landroid/support/design/internal/d;
getfield android/support/design/internal/d/a Landroid/support/v7/internal/view/menu/m;
astore 1
aload 1
ifnull L1
aload 1
invokevirtual android/support/v7/internal/view/menu/m/isCheckable()Z
ifeq L1
aload 0
getfield android/support/design/internal/b/d Landroid/support/design/internal/c;
aload 1
invokevirtual android/support/design/internal/c/a(Landroid/support/v7/internal/view/menu/m;)V
L1:
aload 0
getfield android/support/design/internal/b/m Landroid/support/v7/internal/view/menu/i;
aload 1
aload 0
iconst_0
invokevirtual android/support/v7/internal/view/menu/i/a(Landroid/view/MenuItem;Landroid/support/v7/internal/view/menu/x;I)Z
pop
aload 0
iconst_0
invokevirtual android/support/design/internal/b/b(Z)V
aload 0
iconst_0
invokevirtual android/support/design/internal/b/a(Z)V
L0:
return
.limit locals 6
.limit stack 4
.end method
