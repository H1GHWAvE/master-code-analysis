.bytecode 50.0
.class public final synchronized android/support/design/widget/p
.super android/widget/FrameLayout$LayoutParams

.field public static final 'a' I = 0


.field public static final 'b' I = 1


.field public static final 'c' I = 2


.field private static final 'f' F = 0.5F


.field 'd' I

.field 'e' F

.method private <init>(II)V
aload 0
iload 1
iload 2
invokespecial android/widget/FrameLayout$LayoutParams/<init>(II)V
aload 0
iconst_0
putfield android/support/design/widget/p/d I
aload 0
ldc_w 0.5F
putfield android/support/design/widget/p/e F
return
.limit locals 3
.limit stack 3
.end method

.method private <init>(III)V
aload 0
iload 1
iload 2
iload 3
invokespecial android/widget/FrameLayout$LayoutParams/<init>(III)V
aload 0
iconst_0
putfield android/support/design/widget/p/d I
aload 0
ldc_w 0.5F
putfield android/support/design/widget/p/e F
return
.limit locals 4
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
invokespecial android/widget/FrameLayout$LayoutParams/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
iconst_0
putfield android/support/design/widget/p/d I
aload 0
ldc_w 0.5F
putfield android/support/design/widget/p/e F
aload 1
aload 2
getstatic android/support/design/n/CollapsingAppBarLayout_LayoutParams [I
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
astore 1
aload 0
aload 1
getstatic android/support/design/n/CollapsingAppBarLayout_LayoutParams_layout_collapseMode I
iconst_0
invokevirtual android/content/res/TypedArray/getInt(II)I
putfield android/support/design/widget/p/d I
aload 0
aload 1
getstatic android/support/design/n/CollapsingAppBarLayout_LayoutParams_layout_collapseParallaxMultiplier I
ldc_w 0.5F
invokevirtual android/content/res/TypedArray/getFloat(IF)F
putfield android/support/design/widget/p/e F
aload 1
invokevirtual android/content/res/TypedArray/recycle()V
return
.limit locals 3
.limit stack 4
.end method

.method public <init>(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
aload 1
invokespecial android/widget/FrameLayout$LayoutParams/<init>(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
iconst_0
putfield android/support/design/widget/p/d I
aload 0
ldc_w 0.5F
putfield android/support/design/widget/p/e F
return
.limit locals 2
.limit stack 2
.end method

.method private <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
aload 0
aload 1
invokespecial android/widget/FrameLayout$LayoutParams/<init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
aload 0
iconst_0
putfield android/support/design/widget/p/d I
aload 0
ldc_w 0.5F
putfield android/support/design/widget/p/e F
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(Landroid/widget/FrameLayout$LayoutParams;)V
aload 0
aload 1
invokespecial android/widget/FrameLayout$LayoutParams/<init>(Landroid/widget/FrameLayout$LayoutParams;)V
aload 0
iconst_0
putfield android/support/design/widget/p/d I
aload 0
ldc_w 0.5F
putfield android/support/design/widget/p/e F
return
.limit locals 2
.limit stack 2
.end method

.method private a()I
aload 0
getfield android/support/design/widget/p/d I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private a(F)V
aload 0
fload 1
putfield android/support/design/widget/p/e F
return
.limit locals 2
.limit stack 2
.end method

.method private a(I)V
aload 0
iload 1
putfield android/support/design/widget/p/d I
return
.limit locals 2
.limit stack 2
.end method

.method private b()F
aload 0
getfield android/support/design/widget/p/e F
freturn
.limit locals 1
.limit stack 1
.end method
