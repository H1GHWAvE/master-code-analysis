.bytecode 50.0
.class synchronized android/support/design/widget/j
.super android/graphics/drawable/Drawable

.field private static final 'k' F = 1.3333F


.field final 'a' Landroid/graphics/Paint;

.field final 'b' Landroid/graphics/Rect;

.field final 'c' Landroid/graphics/RectF;

.field 'd' F

.field 'e' I

.field 'f' I

.field 'g' I

.field 'h' I

.field 'i' I

.field 'j' Z

.method public <init>()V
aload 0
invokespecial android/graphics/drawable/Drawable/<init>()V
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/design/widget/j/b Landroid/graphics/Rect;
aload 0
new android/graphics/RectF
dup
invokespecial android/graphics/RectF/<init>()V
putfield android/support/design/widget/j/c Landroid/graphics/RectF;
aload 0
iconst_1
putfield android/support/design/widget/j/j Z
aload 0
new android/graphics/Paint
dup
iconst_1
invokespecial android/graphics/Paint/<init>(I)V
putfield android/support/design/widget/j/a Landroid/graphics/Paint;
aload 0
getfield android/support/design/widget/j/a Landroid/graphics/Paint;
getstatic android/graphics/Paint$Style/STROKE Landroid/graphics/Paint$Style;
invokevirtual android/graphics/Paint/setStyle(Landroid/graphics/Paint$Style;)V
return
.limit locals 1
.limit stack 4
.end method

.method private a()Landroid/graphics/Shader;
aload 0
getfield android/support/design/widget/j/b Landroid/graphics/Rect;
astore 10
aload 0
aload 10
invokevirtual android/support/design/widget/j/copyBounds(Landroid/graphics/Rect;)V
aload 0
getfield android/support/design/widget/j/d F
aload 10
invokevirtual android/graphics/Rect/height()I
i2f
fdiv
fstore 1
aload 0
getfield android/support/design/widget/j/e I
aload 0
getfield android/support/design/widget/j/i I
invokestatic android/support/v4/e/j/a(II)I
istore 4
aload 0
getfield android/support/design/widget/j/f I
aload 0
getfield android/support/design/widget/j/i I
invokestatic android/support/v4/e/j/a(II)I
istore 5
aload 0
getfield android/support/design/widget/j/f I
iconst_0
invokestatic android/support/v4/e/j/b(II)I
aload 0
getfield android/support/design/widget/j/i I
invokestatic android/support/v4/e/j/a(II)I
istore 6
aload 0
getfield android/support/design/widget/j/h I
iconst_0
invokestatic android/support/v4/e/j/b(II)I
aload 0
getfield android/support/design/widget/j/i I
invokestatic android/support/v4/e/j/a(II)I
istore 7
aload 0
getfield android/support/design/widget/j/h I
aload 0
getfield android/support/design/widget/j/i I
invokestatic android/support/v4/e/j/a(II)I
istore 8
aload 0
getfield android/support/design/widget/j/g I
aload 0
getfield android/support/design/widget/j/i I
invokestatic android/support/v4/e/j/a(II)I
istore 9
aload 10
getfield android/graphics/Rect/top I
i2f
fstore 2
aload 10
getfield android/graphics/Rect/bottom I
i2f
fstore 3
getstatic android/graphics/Shader$TileMode/CLAMP Landroid/graphics/Shader$TileMode;
astore 10
new android/graphics/LinearGradient
dup
fconst_0
fload 2
fconst_0
fload 3
bipush 6
newarray int
dup
iconst_0
iload 4
iastore
dup
iconst_1
iload 5
iastore
dup
iconst_2
iload 6
iastore
dup
iconst_3
iload 7
iastore
dup
iconst_4
iload 8
iastore
dup
iconst_5
iload 9
iastore
bipush 6
newarray float
dup
iconst_0
fconst_0
fastore
dup
iconst_1
fload 1
fastore
dup
iconst_2
ldc_w 0.5F
fastore
dup
iconst_3
ldc_w 0.5F
fastore
dup
iconst_4
fconst_1
fload 1
fsub
fastore
dup
iconst_5
fconst_1
fastore
aload 10
invokespecial android/graphics/LinearGradient/<init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V
areturn
.limit locals 11
.limit stack 12
.end method

.method private a(F)V
aload 0
getfield android/support/design/widget/j/d F
fload 1
fcmpl
ifeq L0
aload 0
fload 1
putfield android/support/design/widget/j/d F
aload 0
getfield android/support/design/widget/j/a Landroid/graphics/Paint;
ldc_w 1.3333F
fload 1
fmul
invokevirtual android/graphics/Paint/setStrokeWidth(F)V
aload 0
iconst_1
putfield android/support/design/widget/j/j Z
aload 0
invokevirtual android/support/design/widget/j/invalidateSelf()V
L0:
return
.limit locals 2
.limit stack 3
.end method

.method private a(I)V
aload 0
iload 1
putfield android/support/design/widget/j/i I
aload 0
iconst_1
putfield android/support/design/widget/j/j Z
aload 0
invokevirtual android/support/design/widget/j/invalidateSelf()V
return
.limit locals 2
.limit stack 2
.end method

.method private a(IIII)V
aload 0
iload 1
putfield android/support/design/widget/j/e I
aload 0
iload 2
putfield android/support/design/widget/j/f I
aload 0
iload 3
putfield android/support/design/widget/j/g I
aload 0
iload 4
putfield android/support/design/widget/j/h I
return
.limit locals 5
.limit stack 2
.end method

.method public draw(Landroid/graphics/Canvas;)V
aload 0
getfield android/support/design/widget/j/j Z
ifeq L0
aload 0
getfield android/support/design/widget/j/a Landroid/graphics/Paint;
astore 11
aload 0
getfield android/support/design/widget/j/b Landroid/graphics/Rect;
astore 12
aload 0
aload 12
invokevirtual android/support/design/widget/j/copyBounds(Landroid/graphics/Rect;)V
aload 0
getfield android/support/design/widget/j/d F
aload 12
invokevirtual android/graphics/Rect/height()I
i2f
fdiv
fstore 2
aload 0
getfield android/support/design/widget/j/e I
aload 0
getfield android/support/design/widget/j/i I
invokestatic android/support/v4/e/j/a(II)I
istore 5
aload 0
getfield android/support/design/widget/j/f I
aload 0
getfield android/support/design/widget/j/i I
invokestatic android/support/v4/e/j/a(II)I
istore 6
aload 0
getfield android/support/design/widget/j/f I
iconst_0
invokestatic android/support/v4/e/j/b(II)I
aload 0
getfield android/support/design/widget/j/i I
invokestatic android/support/v4/e/j/a(II)I
istore 7
aload 0
getfield android/support/design/widget/j/h I
iconst_0
invokestatic android/support/v4/e/j/b(II)I
aload 0
getfield android/support/design/widget/j/i I
invokestatic android/support/v4/e/j/a(II)I
istore 8
aload 0
getfield android/support/design/widget/j/h I
aload 0
getfield android/support/design/widget/j/i I
invokestatic android/support/v4/e/j/a(II)I
istore 9
aload 0
getfield android/support/design/widget/j/g I
aload 0
getfield android/support/design/widget/j/i I
invokestatic android/support/v4/e/j/a(II)I
istore 10
aload 12
getfield android/graphics/Rect/top I
i2f
fstore 3
aload 12
getfield android/graphics/Rect/bottom I
i2f
fstore 4
getstatic android/graphics/Shader$TileMode/CLAMP Landroid/graphics/Shader$TileMode;
astore 12
aload 11
new android/graphics/LinearGradient
dup
fconst_0
fload 3
fconst_0
fload 4
bipush 6
newarray int
dup
iconst_0
iload 5
iastore
dup
iconst_1
iload 6
iastore
dup
iconst_2
iload 7
iastore
dup
iconst_3
iload 8
iastore
dup
iconst_4
iload 9
iastore
dup
iconst_5
iload 10
iastore
bipush 6
newarray float
dup
iconst_0
fconst_0
fastore
dup
iconst_1
fload 2
fastore
dup
iconst_2
ldc_w 0.5F
fastore
dup
iconst_3
ldc_w 0.5F
fastore
dup
iconst_4
fconst_1
fload 2
fsub
fastore
dup
iconst_5
fconst_1
fastore
aload 12
invokespecial android/graphics/LinearGradient/<init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V
invokevirtual android/graphics/Paint/setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;
pop
aload 0
iconst_0
putfield android/support/design/widget/j/j Z
L0:
aload 0
getfield android/support/design/widget/j/a Landroid/graphics/Paint;
invokevirtual android/graphics/Paint/getStrokeWidth()F
fconst_2
fdiv
fstore 2
aload 0
getfield android/support/design/widget/j/c Landroid/graphics/RectF;
astore 11
aload 0
aload 0
getfield android/support/design/widget/j/b Landroid/graphics/Rect;
invokevirtual android/support/design/widget/j/copyBounds(Landroid/graphics/Rect;)V
aload 11
aload 0
getfield android/support/design/widget/j/b Landroid/graphics/Rect;
invokevirtual android/graphics/RectF/set(Landroid/graphics/Rect;)V
aload 11
aload 11
getfield android/graphics/RectF/left F
fload 2
fadd
putfield android/graphics/RectF/left F
aload 11
aload 11
getfield android/graphics/RectF/top F
fload 2
fadd
putfield android/graphics/RectF/top F
aload 11
aload 11
getfield android/graphics/RectF/right F
fload 2
fsub
putfield android/graphics/RectF/right F
aload 11
aload 11
getfield android/graphics/RectF/bottom F
fload 2
fsub
putfield android/graphics/RectF/bottom F
aload 1
aload 11
aload 0
getfield android/support/design/widget/j/a Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V
return
.limit locals 13
.limit stack 13
.end method

.method public getOpacity()I
aload 0
getfield android/support/design/widget/j/d F
fconst_0
fcmpl
ifle L0
bipush -3
ireturn
L0:
bipush -2
ireturn
.limit locals 1
.limit stack 2
.end method

.method public getPadding(Landroid/graphics/Rect;)Z
aload 0
getfield android/support/design/widget/j/d F
invokestatic java/lang/Math/round(F)I
istore 2
aload 1
iload 2
iload 2
iload 2
iload 2
invokevirtual android/graphics/Rect/set(IIII)V
iconst_1
ireturn
.limit locals 3
.limit stack 5
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
aload 0
iconst_1
putfield android/support/design/widget/j/j Z
return
.limit locals 2
.limit stack 2
.end method

.method public setAlpha(I)V
aload 0
getfield android/support/design/widget/j/a Landroid/graphics/Paint;
iload 1
invokevirtual android/graphics/Paint/setAlpha(I)V
aload 0
invokevirtual android/support/design/widget/j/invalidateSelf()V
return
.limit locals 2
.limit stack 2
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
aload 0
getfield android/support/design/widget/j/a Landroid/graphics/Paint;
aload 1
invokevirtual android/graphics/Paint/setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;
pop
aload 0
invokevirtual android/support/design/widget/j/invalidateSelf()V
return
.limit locals 2
.limit stack 2
.end method
