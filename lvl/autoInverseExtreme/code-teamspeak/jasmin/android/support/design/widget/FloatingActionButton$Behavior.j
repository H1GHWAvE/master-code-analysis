.bytecode 50.0
.class public synchronized android/support/design/widget/FloatingActionButton$Behavior
.super android/support/design/widget/t

.field private static final 'a' Z

.field private 'b' Landroid/graphics/Rect;

.method static <clinit>()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L0
iconst_1
istore 0
L1:
iload 0
putstatic android/support/design/widget/FloatingActionButton$Behavior/a Z
return
L0:
iconst_0
istore 0
goto L1
.limit locals 1
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial android/support/design/widget/t/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/FloatingActionButton;)V
aload 1
invokevirtual android/support/design/widget/FloatingActionButton/getVisibility()I
ifeq L0
return
L0:
fconst_0
fstore 2
aload 0
aload 1
invokevirtual android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;)Ljava/util/List;
astore 7
aload 7
invokeinterface java/util/List/size()I 0
istore 5
iconst_0
istore 4
L1:
iload 4
iload 5
if_icmpge L2
aload 7
iload 4
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/view/View
astore 8
aload 8
instanceof android/support/design/widget/Snackbar$SnackbarLayout
ifeq L3
aload 1
invokevirtual android/view/View/getVisibility()I
ifne L4
aload 8
invokevirtual android/view/View/getVisibility()I
ifne L4
aload 0
getfield android/support/design/widget/CoordinatorLayout/i Landroid/graphics/Rect;
astore 9
aload 1
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
aload 0
if_acmpeq L5
iconst_1
istore 6
L6:
aload 0
aload 1
iload 6
aload 9
invokevirtual android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;ZLandroid/graphics/Rect;)V
aload 0
getfield android/support/design/widget/CoordinatorLayout/j Landroid/graphics/Rect;
astore 10
aload 8
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
aload 0
if_acmpeq L7
iconst_1
istore 6
L8:
aload 0
aload 8
iload 6
aload 10
invokevirtual android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;ZLandroid/graphics/Rect;)V
aload 9
getfield android/graphics/Rect/left I
aload 10
getfield android/graphics/Rect/right I
if_icmpgt L9
aload 9
getfield android/graphics/Rect/top I
aload 10
getfield android/graphics/Rect/bottom I
if_icmpgt L9
aload 9
getfield android/graphics/Rect/right I
aload 10
getfield android/graphics/Rect/left I
if_icmplt L9
aload 9
getfield android/graphics/Rect/bottom I
aload 10
getfield android/graphics/Rect/top I
if_icmplt L9
iconst_1
istore 3
L10:
iload 3
ifeq L3
fload 2
aload 8
invokestatic android/support/v4/view/cx/n(Landroid/view/View;)F
aload 8
invokevirtual android/view/View/getHeight()I
i2f
fsub
invokestatic java/lang/Math/min(FF)F
fstore 2
L11:
iload 4
iconst_1
iadd
istore 4
goto L1
L5:
iconst_0
istore 6
goto L6
L7:
iconst_0
istore 6
goto L8
L9:
iconst_0
istore 3
goto L10
L4:
iconst_0
istore 3
goto L10
L2:
aload 1
fload 2
invokestatic android/support/v4/view/cx/b(Landroid/view/View;F)V
return
L3:
goto L11
.limit locals 11
.limit stack 4
.end method

.method private static a(Landroid/support/design/widget/FloatingActionButton;Landroid/view/View;)V
aload 1
instanceof android/support/design/widget/Snackbar$SnackbarLayout
ifeq L0
aload 0
invokestatic android/support/v4/view/cx/n(Landroid/view/View;)F
fconst_0
fcmpl
ifeq L0
aload 0
invokestatic android/support/v4/view/cx/p(Landroid/view/View;)Landroid/support/v4/view/fk;
fconst_0
invokevirtual android/support/v4/view/fk/b(F)Landroid/support/v4/view/fk;
astore 0
aload 0
getfield android/support/v4/view/fk/a Ljava/lang/ref/WeakReference;
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/view/View
astore 1
aload 1
ifnull L1
getstatic android/support/v4/view/fk/c Landroid/support/v4/view/fu;
aload 0
aload 1
invokeinterface android/support/v4/view/fu/a(Landroid/support/v4/view/fk;Landroid/view/View;)V 2
L1:
aload 0
getfield android/support/v4/view/fk/a Ljava/lang/ref/WeakReference;
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/view/View
astore 1
aload 1
ifnull L2
getstatic android/support/v4/view/fk/c Landroid/support/v4/view/fu;
aload 0
aload 1
invokeinterface android/support/v4/view/fu/b(Landroid/support/v4/view/fk;Landroid/view/View;)V 2
L2:
aload 0
fconst_1
invokevirtual android/support/v4/view/fk/a(F)Landroid/support/v4/view/fk;
getstatic android/support/design/widget/a/b Landroid/view/animation/Interpolator;
invokevirtual android/support/v4/view/fk/a(Landroid/view/animation/Interpolator;)Landroid/support/v4/view/fk;
aconst_null
invokevirtual android/support/v4/view/fk/a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;
pop
L0:
return
.limit locals 2
.limit stack 3
.end method

.method private a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;Landroid/support/design/widget/FloatingActionButton;)Z
aload 3
invokevirtual android/support/design/widget/FloatingActionButton/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
getfield android/support/design/widget/w/f I
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getId()I
if_icmpeq L0
iconst_0
ireturn
L0:
aload 0
getfield android/support/design/widget/FloatingActionButton$Behavior/b Landroid/graphics/Rect;
ifnonnull L1
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/design/widget/FloatingActionButton$Behavior/b Landroid/graphics/Rect;
L1:
aload 0
getfield android/support/design/widget/FloatingActionButton$Behavior/b Landroid/graphics/Rect;
astore 4
aload 1
aload 2
aload 4
invokestatic android/support/design/widget/cz/a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;)V
aload 4
getfield android/graphics/Rect/bottom I
aload 2
invokevirtual android/support/design/widget/AppBarLayout/getMinimumHeightForVisibleOverlappingContent()I
if_icmpgt L2
aload 3
getfield android/support/design/widget/FloatingActionButton/a Landroid/support/design/widget/al;
invokevirtual android/support/design/widget/al/b()V
L3:
iconst_1
ireturn
L2:
aload 3
getfield android/support/design/widget/FloatingActionButton/a Landroid/support/design/widget/al;
invokevirtual android/support/design/widget/al/c()V
goto L3
.limit locals 5
.limit stack 3
.end method

.method private a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/FloatingActionButton;I)Z
iconst_0
istore 5
aload 1
aload 2
invokevirtual android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;)Ljava/util/List;
astore 7
aload 7
invokeinterface java/util/List/size()I 0
istore 6
iconst_0
istore 4
L0:
iload 4
iload 6
if_icmpge L1
aload 7
iload 4
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/view/View
astore 8
aload 8
instanceof android/support/design/widget/AppBarLayout
ifeq L2
aload 0
aload 1
aload 8
checkcast android/support/design/widget/AppBarLayout
aload 2
invokespecial android/support/design/widget/FloatingActionButton$Behavior/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;Landroid/support/design/widget/FloatingActionButton;)Z
ifne L1
L2:
iload 4
iconst_1
iadd
istore 4
goto L0
L1:
aload 1
aload 2
iload 3
invokevirtual android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;I)V
aload 2
invokestatic android/support/design/widget/FloatingActionButton/a(Landroid/support/design/widget/FloatingActionButton;)Landroid/graphics/Rect;
astore 7
aload 7
ifnull L3
aload 7
invokevirtual android/graphics/Rect/centerX()I
ifle L3
aload 7
invokevirtual android/graphics/Rect/centerY()I
ifle L3
aload 2
invokevirtual android/support/design/widget/FloatingActionButton/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
astore 8
aload 2
invokevirtual android/support/design/widget/FloatingActionButton/getRight()I
aload 1
invokevirtual android/support/design/widget/CoordinatorLayout/getWidth()I
aload 8
getfield android/support/design/widget/w/rightMargin I
isub
if_icmplt L4
aload 7
getfield android/graphics/Rect/right I
istore 3
L5:
aload 2
invokevirtual android/support/design/widget/FloatingActionButton/getBottom()I
aload 1
invokevirtual android/support/design/widget/CoordinatorLayout/getBottom()I
aload 8
getfield android/support/design/widget/w/bottomMargin I
isub
if_icmplt L6
aload 7
getfield android/graphics/Rect/bottom I
istore 4
L7:
aload 2
iload 4
invokevirtual android/support/design/widget/FloatingActionButton/offsetTopAndBottom(I)V
aload 2
iload 3
invokevirtual android/support/design/widget/FloatingActionButton/offsetLeftAndRight(I)V
L3:
iconst_1
ireturn
L4:
aload 2
invokevirtual android/support/design/widget/FloatingActionButton/getLeft()I
aload 8
getfield android/support/design/widget/w/leftMargin I
if_icmpgt L8
aload 7
getfield android/graphics/Rect/left I
ineg
istore 3
goto L5
L6:
iload 5
istore 4
aload 2
invokevirtual android/support/design/widget/FloatingActionButton/getTop()I
aload 8
getfield android/support/design/widget/w/topMargin I
if_icmpgt L7
aload 7
getfield android/graphics/Rect/top I
ineg
istore 4
goto L7
L8:
iconst_0
istore 3
goto L5
.limit locals 9
.limit stack 4
.end method

.method private a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/FloatingActionButton;Landroid/view/View;)Z
aload 3
instanceof android/support/design/widget/Snackbar$SnackbarLayout
ifeq L0
aload 2
invokevirtual android/support/design/widget/FloatingActionButton/getVisibility()I
ifne L1
fconst_0
fstore 4
aload 1
aload 2
invokevirtual android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;)Ljava/util/List;
astore 3
aload 3
invokeinterface java/util/List/size()I 0
istore 7
iconst_0
istore 6
L2:
iload 6
iload 7
if_icmpge L3
aload 3
iload 6
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/view/View
astore 9
aload 9
instanceof android/support/design/widget/Snackbar$SnackbarLayout
ifeq L4
aload 2
invokevirtual android/view/View/getVisibility()I
ifne L5
aload 9
invokevirtual android/view/View/getVisibility()I
ifne L5
aload 1
getfield android/support/design/widget/CoordinatorLayout/i Landroid/graphics/Rect;
astore 10
aload 2
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
aload 1
if_acmpeq L6
iconst_1
istore 8
L7:
aload 1
aload 2
iload 8
aload 10
invokevirtual android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;ZLandroid/graphics/Rect;)V
aload 1
getfield android/support/design/widget/CoordinatorLayout/j Landroid/graphics/Rect;
astore 11
aload 9
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
aload 1
if_acmpeq L8
iconst_1
istore 8
L9:
aload 1
aload 9
iload 8
aload 11
invokevirtual android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;ZLandroid/graphics/Rect;)V
aload 10
getfield android/graphics/Rect/left I
aload 11
getfield android/graphics/Rect/right I
if_icmpgt L10
aload 10
getfield android/graphics/Rect/top I
aload 11
getfield android/graphics/Rect/bottom I
if_icmpgt L10
aload 10
getfield android/graphics/Rect/right I
aload 11
getfield android/graphics/Rect/left I
if_icmplt L10
aload 10
getfield android/graphics/Rect/bottom I
aload 11
getfield android/graphics/Rect/top I
if_icmplt L10
iconst_1
istore 5
L11:
iload 5
ifeq L4
fload 4
aload 9
invokestatic android/support/v4/view/cx/n(Landroid/view/View;)F
aload 9
invokevirtual android/view/View/getHeight()I
i2f
fsub
invokestatic java/lang/Math/min(FF)F
fstore 4
L12:
iload 6
iconst_1
iadd
istore 6
goto L2
L6:
iconst_0
istore 8
goto L7
L8:
iconst_0
istore 8
goto L9
L10:
iconst_0
istore 5
goto L11
L5:
iconst_0
istore 5
goto L11
L3:
aload 2
fload 4
invokestatic android/support/v4/view/cx/b(Landroid/view/View;F)V
L1:
iconst_0
ireturn
L0:
aload 3
instanceof android/support/design/widget/AppBarLayout
ifeq L1
aload 0
aload 1
aload 3
checkcast android/support/design/widget/AppBarLayout
aload 2
invokespecial android/support/design/widget/FloatingActionButton$Behavior/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;Landroid/support/design/widget/FloatingActionButton;)Z
pop
iconst_0
ireturn
L4:
goto L12
.limit locals 12
.limit stack 4
.end method

.method private static b(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/FloatingActionButton;)F
fconst_0
fstore 2
aload 0
aload 1
invokevirtual android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;)Ljava/util/List;
astore 7
aload 7
invokeinterface java/util/List/size()I 0
istore 5
iconst_0
istore 4
L0:
iload 4
iload 5
if_icmpge L1
aload 7
iload 4
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/view/View
astore 8
aload 8
instanceof android/support/design/widget/Snackbar$SnackbarLayout
ifeq L2
aload 1
invokevirtual android/view/View/getVisibility()I
ifne L3
aload 8
invokevirtual android/view/View/getVisibility()I
ifne L3
aload 0
getfield android/support/design/widget/CoordinatorLayout/i Landroid/graphics/Rect;
astore 9
aload 1
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
aload 0
if_acmpeq L4
iconst_1
istore 6
L5:
aload 0
aload 1
iload 6
aload 9
invokevirtual android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;ZLandroid/graphics/Rect;)V
aload 0
getfield android/support/design/widget/CoordinatorLayout/j Landroid/graphics/Rect;
astore 10
aload 8
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
aload 0
if_acmpeq L6
iconst_1
istore 6
L7:
aload 0
aload 8
iload 6
aload 10
invokevirtual android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;ZLandroid/graphics/Rect;)V
aload 9
getfield android/graphics/Rect/left I
aload 10
getfield android/graphics/Rect/right I
if_icmpgt L8
aload 9
getfield android/graphics/Rect/top I
aload 10
getfield android/graphics/Rect/bottom I
if_icmpgt L8
aload 9
getfield android/graphics/Rect/right I
aload 10
getfield android/graphics/Rect/left I
if_icmplt L8
aload 9
getfield android/graphics/Rect/bottom I
aload 10
getfield android/graphics/Rect/top I
if_icmplt L8
iconst_1
istore 3
L9:
iload 3
ifeq L2
fload 2
aload 8
invokestatic android/support/v4/view/cx/n(Landroid/view/View;)F
aload 8
invokevirtual android/view/View/getHeight()I
i2f
fsub
invokestatic java/lang/Math/min(FF)F
fstore 2
L10:
iload 4
iconst_1
iadd
istore 4
goto L0
L4:
iconst_0
istore 6
goto L5
L6:
iconst_0
istore 6
goto L7
L8:
iconst_0
istore 3
goto L9
L3:
iconst_0
istore 3
goto L9
L1:
fload 2
freturn
L2:
goto L10
.limit locals 11
.limit stack 4
.end method

.method private static c(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/FloatingActionButton;)V
iconst_0
istore 3
aload 1
invokestatic android/support/design/widget/FloatingActionButton/a(Landroid/support/design/widget/FloatingActionButton;)Landroid/graphics/Rect;
astore 4
aload 4
ifnull L0
aload 4
invokevirtual android/graphics/Rect/centerX()I
ifle L0
aload 4
invokevirtual android/graphics/Rect/centerY()I
ifle L0
aload 1
invokevirtual android/support/design/widget/FloatingActionButton/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
astore 5
aload 1
invokevirtual android/support/design/widget/FloatingActionButton/getRight()I
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getWidth()I
aload 5
getfield android/support/design/widget/w/rightMargin I
isub
if_icmplt L1
aload 4
getfield android/graphics/Rect/right I
istore 2
L2:
aload 1
invokevirtual android/support/design/widget/FloatingActionButton/getBottom()I
aload 0
invokevirtual android/support/design/widget/CoordinatorLayout/getBottom()I
aload 5
getfield android/support/design/widget/w/bottomMargin I
isub
if_icmplt L3
aload 4
getfield android/graphics/Rect/bottom I
istore 3
L4:
aload 1
iload 3
invokevirtual android/support/design/widget/FloatingActionButton/offsetTopAndBottom(I)V
aload 1
iload 2
invokevirtual android/support/design/widget/FloatingActionButton/offsetLeftAndRight(I)V
L0:
return
L1:
aload 1
invokevirtual android/support/design/widget/FloatingActionButton/getLeft()I
aload 5
getfield android/support/design/widget/w/leftMargin I
if_icmpgt L5
aload 4
getfield android/graphics/Rect/left I
ineg
istore 2
goto L2
L3:
aload 1
invokevirtual android/support/design/widget/FloatingActionButton/getTop()I
aload 5
getfield android/support/design/widget/w/topMargin I
if_icmpgt L4
aload 4
getfield android/graphics/Rect/top I
ineg
istore 3
goto L4
L5:
iconst_0
istore 2
goto L2
.limit locals 6
.limit stack 3
.end method

.method private static c(Landroid/view/View;)Z
getstatic android/support/design/widget/FloatingActionButton$Behavior/a Z
ifeq L0
aload 0
instanceof android/support/design/widget/Snackbar$SnackbarLayout
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final synthetic a(Landroid/view/View;Landroid/view/View;)V
aload 1
checkcast android/support/design/widget/FloatingActionButton
astore 1
aload 2
instanceof android/support/design/widget/Snackbar$SnackbarLayout
ifeq L0
aload 1
invokestatic android/support/v4/view/cx/n(Landroid/view/View;)F
fconst_0
fcmpl
ifeq L0
aload 1
invokestatic android/support/v4/view/cx/p(Landroid/view/View;)Landroid/support/v4/view/fk;
fconst_0
invokevirtual android/support/v4/view/fk/b(F)Landroid/support/v4/view/fk;
astore 1
aload 1
getfield android/support/v4/view/fk/a Ljava/lang/ref/WeakReference;
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/view/View
astore 2
aload 2
ifnull L1
getstatic android/support/v4/view/fk/c Landroid/support/v4/view/fu;
aload 1
aload 2
invokeinterface android/support/v4/view/fu/a(Landroid/support/v4/view/fk;Landroid/view/View;)V 2
L1:
aload 1
getfield android/support/v4/view/fk/a Ljava/lang/ref/WeakReference;
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/view/View
astore 2
aload 2
ifnull L2
getstatic android/support/v4/view/fk/c Landroid/support/v4/view/fu;
aload 1
aload 2
invokeinterface android/support/v4/view/fu/b(Landroid/support/v4/view/fk;Landroid/view/View;)V 2
L2:
aload 1
fconst_1
invokevirtual android/support/v4/view/fk/a(F)Landroid/support/v4/view/fk;
getstatic android/support/design/widget/a/b Landroid/view/animation/Interpolator;
invokevirtual android/support/v4/view/fk/a(Landroid/view/animation/Interpolator;)Landroid/support/v4/view/fk;
aconst_null
invokevirtual android/support/v4/view/fk/a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;
pop
L0:
return
.limit locals 3
.limit stack 3
.end method

.method public final synthetic a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)Z
iconst_0
istore 5
aload 2
checkcast android/support/design/widget/FloatingActionButton
astore 2
aload 1
aload 2
invokevirtual android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;)Ljava/util/List;
astore 7
aload 7
invokeinterface java/util/List/size()I 0
istore 6
iconst_0
istore 4
L0:
iload 4
iload 6
if_icmpge L1
aload 7
iload 4
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/view/View
astore 8
aload 8
instanceof android/support/design/widget/AppBarLayout
ifeq L2
aload 0
aload 1
aload 8
checkcast android/support/design/widget/AppBarLayout
aload 2
invokespecial android/support/design/widget/FloatingActionButton$Behavior/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;Landroid/support/design/widget/FloatingActionButton;)Z
ifne L1
L2:
iload 4
iconst_1
iadd
istore 4
goto L0
L1:
aload 1
aload 2
iload 3
invokevirtual android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;I)V
aload 2
invokestatic android/support/design/widget/FloatingActionButton/a(Landroid/support/design/widget/FloatingActionButton;)Landroid/graphics/Rect;
astore 7
aload 7
ifnull L3
aload 7
invokevirtual android/graphics/Rect/centerX()I
ifle L3
aload 7
invokevirtual android/graphics/Rect/centerY()I
ifle L3
aload 2
invokevirtual android/support/design/widget/FloatingActionButton/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
astore 8
aload 2
invokevirtual android/support/design/widget/FloatingActionButton/getRight()I
aload 1
invokevirtual android/support/design/widget/CoordinatorLayout/getWidth()I
aload 8
getfield android/support/design/widget/w/rightMargin I
isub
if_icmplt L4
aload 7
getfield android/graphics/Rect/right I
istore 3
L5:
aload 2
invokevirtual android/support/design/widget/FloatingActionButton/getBottom()I
aload 1
invokevirtual android/support/design/widget/CoordinatorLayout/getBottom()I
aload 8
getfield android/support/design/widget/w/bottomMargin I
isub
if_icmplt L6
aload 7
getfield android/graphics/Rect/bottom I
istore 4
L7:
aload 2
iload 4
invokevirtual android/support/design/widget/FloatingActionButton/offsetTopAndBottom(I)V
aload 2
iload 3
invokevirtual android/support/design/widget/FloatingActionButton/offsetLeftAndRight(I)V
L3:
iconst_1
ireturn
L4:
aload 2
invokevirtual android/support/design/widget/FloatingActionButton/getLeft()I
aload 8
getfield android/support/design/widget/w/leftMargin I
if_icmpgt L8
aload 7
getfield android/graphics/Rect/left I
ineg
istore 3
goto L5
L6:
iload 5
istore 4
aload 2
invokevirtual android/support/design/widget/FloatingActionButton/getTop()I
aload 8
getfield android/support/design/widget/w/topMargin I
if_icmpgt L7
aload 7
getfield android/graphics/Rect/top I
ineg
istore 4
goto L7
L8:
iconst_0
istore 3
goto L5
.limit locals 9
.limit stack 4
.end method

.method public final synthetic a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)Z
aload 2
checkcast android/support/design/widget/FloatingActionButton
astore 2
aload 3
instanceof android/support/design/widget/Snackbar$SnackbarLayout
ifeq L0
aload 2
invokevirtual android/support/design/widget/FloatingActionButton/getVisibility()I
ifne L1
fconst_0
fstore 4
aload 1
aload 2
invokevirtual android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;)Ljava/util/List;
astore 3
aload 3
invokeinterface java/util/List/size()I 0
istore 7
iconst_0
istore 6
L2:
iload 6
iload 7
if_icmpge L3
aload 3
iload 6
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/view/View
astore 9
aload 9
instanceof android/support/design/widget/Snackbar$SnackbarLayout
ifeq L4
aload 2
invokevirtual android/view/View/getVisibility()I
ifne L5
aload 9
invokevirtual android/view/View/getVisibility()I
ifne L5
aload 1
getfield android/support/design/widget/CoordinatorLayout/i Landroid/graphics/Rect;
astore 10
aload 2
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
aload 1
if_acmpeq L6
iconst_1
istore 8
L7:
aload 1
aload 2
iload 8
aload 10
invokevirtual android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;ZLandroid/graphics/Rect;)V
aload 1
getfield android/support/design/widget/CoordinatorLayout/j Landroid/graphics/Rect;
astore 11
aload 9
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
aload 1
if_acmpeq L8
iconst_1
istore 8
L9:
aload 1
aload 9
iload 8
aload 11
invokevirtual android/support/design/widget/CoordinatorLayout/a(Landroid/view/View;ZLandroid/graphics/Rect;)V
aload 10
getfield android/graphics/Rect/left I
aload 11
getfield android/graphics/Rect/right I
if_icmpgt L10
aload 10
getfield android/graphics/Rect/top I
aload 11
getfield android/graphics/Rect/bottom I
if_icmpgt L10
aload 10
getfield android/graphics/Rect/right I
aload 11
getfield android/graphics/Rect/left I
if_icmplt L10
aload 10
getfield android/graphics/Rect/bottom I
aload 11
getfield android/graphics/Rect/top I
if_icmplt L10
iconst_1
istore 5
L11:
iload 5
ifeq L4
fload 4
aload 9
invokestatic android/support/v4/view/cx/n(Landroid/view/View;)F
aload 9
invokevirtual android/view/View/getHeight()I
i2f
fsub
invokestatic java/lang/Math/min(FF)F
fstore 4
L12:
iload 6
iconst_1
iadd
istore 6
goto L2
L6:
iconst_0
istore 8
goto L7
L8:
iconst_0
istore 8
goto L9
L10:
iconst_0
istore 5
goto L11
L5:
iconst_0
istore 5
goto L11
L3:
aload 2
fload 4
invokestatic android/support/v4/view/cx/b(Landroid/view/View;F)V
L1:
iconst_0
ireturn
L0:
aload 3
instanceof android/support/design/widget/AppBarLayout
ifeq L1
aload 0
aload 1
aload 3
checkcast android/support/design/widget/AppBarLayout
aload 2
invokespecial android/support/design/widget/FloatingActionButton$Behavior/a(Landroid/support/design/widget/CoordinatorLayout;Landroid/support/design/widget/AppBarLayout;Landroid/support/design/widget/FloatingActionButton;)Z
pop
iconst_0
ireturn
L4:
goto L12
.limit locals 12
.limit stack 4
.end method

.method public final volatile synthetic b(Landroid/view/View;)Z
getstatic android/support/design/widget/FloatingActionButton$Behavior/a Z
ifeq L0
aload 1
instanceof android/support/design/widget/Snackbar$SnackbarLayout
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method
