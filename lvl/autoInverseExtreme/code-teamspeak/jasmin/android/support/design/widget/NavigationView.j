.bytecode 50.0
.class public final synchronized android/support/design/widget/NavigationView
.super android/support/design/internal/f

.field private static final 'a' [I

.field private static final 'b' [I

.field private static final 'c' I = 1


.field private final 'd' Landroid/support/design/internal/a;

.field private final 'e' Landroid/support/design/internal/b;

.field private 'f' Landroid/support/design/widget/ap;

.field private 'g' I

.field private 'h' Landroid/view/MenuInflater;

.method static <clinit>()V
iconst_1
newarray int
dup
iconst_0
ldc_w 16842912
iastore
putstatic android/support/design/widget/NavigationView/a [I
iconst_1
newarray int
dup
iconst_0
ldc_w -16842910
iastore
putstatic android/support/design/widget/NavigationView/b [I
return
.limit locals 0
.limit stack 4
.end method

.method private <init>(Landroid/content/Context;)V
aload 0
aload 1
iconst_0
invokespecial android/support/design/widget/NavigationView/<init>(Landroid/content/Context;B)V
return
.limit locals 2
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;B)V
aload 0
aload 1
iconst_0
invokespecial android/support/design/widget/NavigationView/<init>(Landroid/content/Context;C)V
return
.limit locals 3
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;C)V
aconst_null
astore 4
aload 0
aload 1
iconst_0
invokespecial android/support/design/internal/f/<init>(Landroid/content/Context;C)V
aload 0
new android/support/design/internal/b
dup
invokespecial android/support/design/internal/b/<init>()V
putfield android/support/design/widget/NavigationView/e Landroid/support/design/internal/b;
aload 0
new android/support/design/internal/a
dup
aload 1
invokespecial android/support/design/internal/a/<init>(Landroid/content/Context;)V
putfield android/support/design/widget/NavigationView/d Landroid/support/design/internal/a;
aload 1
aconst_null
getstatic android/support/design/n/NavigationView [I
iconst_0
getstatic android/support/design/m/Widget_Design_NavigationView I
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
astore 7
aload 0
aload 7
getstatic android/support/design/n/NavigationView_android_background I
invokevirtual android/content/res/TypedArray/getDrawable(I)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/design/widget/NavigationView/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 7
getstatic android/support/design/n/NavigationView_elevation I
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L0
aload 0
aload 7
getstatic android/support/design/n/NavigationView_elevation I
iconst_0
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
i2f
invokestatic android/support/v4/view/cx/f(Landroid/view/View;F)V
L0:
aload 0
aload 7
getstatic android/support/design/n/NavigationView_android_fitsSystemWindows I
iconst_0
invokevirtual android/content/res/TypedArray/getBoolean(IZ)Z
invokestatic android/support/v4/view/cx/a(Landroid/view/View;Z)V
aload 0
aload 7
getstatic android/support/design/n/NavigationView_android_maxWidth I
iconst_0
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
putfield android/support/design/widget/NavigationView/g I
aload 7
getstatic android/support/design/n/NavigationView_itemIconTint I
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L1
aload 7
getstatic android/support/design/n/NavigationView_itemIconTint I
invokevirtual android/content/res/TypedArray/getColorStateList(I)Landroid/content/res/ColorStateList;
astore 5
L2:
aload 7
getstatic android/support/design/n/NavigationView_itemTextAppearance I
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L3
aload 7
getstatic android/support/design/n/NavigationView_itemTextAppearance I
iconst_0
invokevirtual android/content/res/TypedArray/getResourceId(II)I
istore 2
iconst_1
istore 3
L4:
aload 7
getstatic android/support/design/n/NavigationView_itemTextColor I
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L5
aload 7
getstatic android/support/design/n/NavigationView_itemTextColor I
invokevirtual android/content/res/TypedArray/getColorStateList(I)Landroid/content/res/ColorStateList;
astore 4
L5:
aload 4
astore 6
iload 3
ifne L6
aload 4
astore 6
aload 4
ifnonnull L6
aload 0
ldc_w 16842806
invokespecial android/support/design/widget/NavigationView/c(I)Landroid/content/res/ColorStateList;
astore 6
L6:
aload 7
getstatic android/support/design/n/NavigationView_itemBackground I
invokevirtual android/content/res/TypedArray/getDrawable(I)Landroid/graphics/drawable/Drawable;
astore 4
aload 0
getfield android/support/design/widget/NavigationView/d Landroid/support/design/internal/a;
new android/support/design/widget/ao
dup
aload 0
invokespecial android/support/design/widget/ao/<init>(Landroid/support/design/widget/NavigationView;)V
invokevirtual android/support/design/internal/a/a(Landroid/support/v7/internal/view/menu/j;)V
aload 0
getfield android/support/design/widget/NavigationView/e Landroid/support/design/internal/b;
iconst_1
putfield android/support/design/internal/b/c I
aload 0
getfield android/support/design/widget/NavigationView/e Landroid/support/design/internal/b;
aload 1
aload 0
getfield android/support/design/widget/NavigationView/d Landroid/support/design/internal/a;
invokevirtual android/support/design/internal/b/a(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;)V
aload 0
getfield android/support/design/widget/NavigationView/e Landroid/support/design/internal/b;
aload 5
invokevirtual android/support/design/internal/b/a(Landroid/content/res/ColorStateList;)V
iload 3
ifeq L7
aload 0
getfield android/support/design/widget/NavigationView/e Landroid/support/design/internal/b;
iload 2
invokevirtual android/support/design/internal/b/a(I)V
L7:
aload 0
getfield android/support/design/widget/NavigationView/e Landroid/support/design/internal/b;
aload 6
invokevirtual android/support/design/internal/b/b(Landroid/content/res/ColorStateList;)V
aload 0
getfield android/support/design/widget/NavigationView/e Landroid/support/design/internal/b;
aload 4
putfield android/support/design/internal/b/h Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/design/widget/NavigationView/d Landroid/support/design/internal/a;
aload 0
getfield android/support/design/widget/NavigationView/e Landroid/support/design/internal/b;
invokevirtual android/support/design/internal/a/a(Landroid/support/v7/internal/view/menu/x;)V
aload 0
aload 0
getfield android/support/design/widget/NavigationView/e Landroid/support/design/internal/b;
aload 0
invokevirtual android/support/design/internal/b/a(Landroid/view/ViewGroup;)Landroid/support/v7/internal/view/menu/z;
checkcast android/view/View
invokevirtual android/support/design/widget/NavigationView/addView(Landroid/view/View;)V
aload 7
getstatic android/support/design/n/NavigationView_menu I
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L8
aload 7
getstatic android/support/design/n/NavigationView_menu I
iconst_0
invokevirtual android/content/res/TypedArray/getResourceId(II)I
istore 2
aload 0
getfield android/support/design/widget/NavigationView/e Landroid/support/design/internal/b;
iconst_1
invokevirtual android/support/design/internal/b/b(Z)V
aload 0
invokespecial android/support/design/widget/NavigationView/getMenuInflater()Landroid/view/MenuInflater;
iload 2
aload 0
getfield android/support/design/widget/NavigationView/d Landroid/support/design/internal/a;
invokevirtual android/view/MenuInflater/inflate(ILandroid/view/Menu;)V
aload 0
getfield android/support/design/widget/NavigationView/e Landroid/support/design/internal/b;
iconst_0
invokevirtual android/support/design/internal/b/b(Z)V
aload 0
getfield android/support/design/widget/NavigationView/e Landroid/support/design/internal/b;
iconst_0
invokevirtual android/support/design/internal/b/a(Z)V
L8:
aload 7
getstatic android/support/design/n/NavigationView_headerLayout I
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L9
aload 7
getstatic android/support/design/n/NavigationView_headerLayout I
iconst_0
invokevirtual android/content/res/TypedArray/getResourceId(II)I
istore 2
aload 0
getfield android/support/design/widget/NavigationView/e Landroid/support/design/internal/b;
astore 1
aload 1
aload 1
getfield android/support/design/internal/b/e Landroid/view/LayoutInflater;
iload 2
aload 1
getfield android/support/design/internal/b/b Landroid/widget/LinearLayout;
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
invokevirtual android/support/design/internal/b/a(Landroid/view/View;)V
L9:
aload 7
invokevirtual android/content/res/TypedArray/recycle()V
return
L1:
aload 0
ldc_w 16842808
invokespecial android/support/design/widget/NavigationView/c(I)Landroid/content/res/ColorStateList;
astore 5
goto L2
L3:
iconst_0
istore 2
iconst_0
istore 3
goto L4
.limit locals 8
.limit stack 5
.end method

.method static synthetic a(Landroid/support/design/widget/NavigationView;)Landroid/support/design/widget/ap;
aload 0
getfield android/support/design/widget/NavigationView/f Landroid/support/design/widget/ap;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(I)V
aload 0
getfield android/support/design/widget/NavigationView/e Landroid/support/design/internal/b;
iconst_1
invokevirtual android/support/design/internal/b/b(Z)V
aload 0
invokespecial android/support/design/widget/NavigationView/getMenuInflater()Landroid/view/MenuInflater;
iload 1
aload 0
getfield android/support/design/widget/NavigationView/d Landroid/support/design/internal/a;
invokevirtual android/view/MenuInflater/inflate(ILandroid/view/Menu;)V
aload 0
getfield android/support/design/widget/NavigationView/e Landroid/support/design/internal/b;
iconst_0
invokevirtual android/support/design/internal/b/b(Z)V
aload 0
getfield android/support/design/widget/NavigationView/e Landroid/support/design/internal/b;
iconst_0
invokevirtual android/support/design/internal/b/a(Z)V
return
.limit locals 2
.limit stack 3
.end method

.method private a(Landroid/view/View;)V
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
aload 0
getfield android/support/design/widget/NavigationView/e Landroid/support/design/internal/b;
aload 1
invokevirtual android/support/design/internal/b/a(Landroid/view/View;)V
return
.limit locals 2
.limit stack 2
.end method

.method private b(I)Landroid/view/View;
.annotation invisibleparam 1 Landroid/support/a/v;
.end annotation
aload 0
getfield android/support/design/widget/NavigationView/e Landroid/support/design/internal/b;
astore 2
aload 2
getfield android/support/design/internal/b/e Landroid/view/LayoutInflater;
iload 1
aload 2
getfield android/support/design/internal/b/b Landroid/widget/LinearLayout;
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
astore 3
aload 2
aload 3
invokevirtual android/support/design/internal/b/a(Landroid/view/View;)V
aload 3
areturn
.limit locals 4
.limit stack 4
.end method

.method private b(Landroid/view/View;)V
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
aload 0
getfield android/support/design/widget/NavigationView/e Landroid/support/design/internal/b;
astore 2
aload 2
getfield android/support/design/internal/b/b Landroid/widget/LinearLayout;
aload 1
invokevirtual android/widget/LinearLayout/removeView(Landroid/view/View;)V
aload 2
getfield android/support/design/internal/b/b Landroid/widget/LinearLayout;
invokevirtual android/widget/LinearLayout/getChildCount()I
ifne L0
aload 2
getfield android/support/design/internal/b/a Landroid/support/design/internal/NavigationMenuView;
iconst_0
aload 2
getfield android/support/design/internal/b/i I
iconst_0
aload 2
getfield android/support/design/internal/b/a Landroid/support/design/internal/NavigationMenuView;
invokevirtual android/support/design/internal/NavigationMenuView/getPaddingBottom()I
invokevirtual android/support/design/internal/NavigationMenuView/setPadding(IIII)V
L0:
return
.limit locals 3
.limit stack 5
.end method

.method private c(I)Landroid/content/res/ColorStateList;
new android/util/TypedValue
dup
invokespecial android/util/TypedValue/<init>()V
astore 5
aload 0
invokevirtual android/support/design/widget/NavigationView/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getTheme()Landroid/content/res/Resources$Theme;
iload 1
aload 5
iconst_1
invokevirtual android/content/res/Resources$Theme/resolveAttribute(ILandroid/util/TypedValue;Z)Z
ifne L0
L1:
aconst_null
areturn
L0:
aload 0
invokevirtual android/support/design/widget/NavigationView/getResources()Landroid/content/res/Resources;
aload 5
getfield android/util/TypedValue/resourceId I
invokevirtual android/content/res/Resources/getColorStateList(I)Landroid/content/res/ColorStateList;
astore 4
aload 0
invokevirtual android/support/design/widget/NavigationView/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getTheme()Landroid/content/res/Resources$Theme;
getstatic android/support/design/d/colorPrimary I
aload 5
iconst_1
invokevirtual android/content/res/Resources$Theme/resolveAttribute(ILandroid/util/TypedValue;Z)Z
ifeq L1
aload 5
getfield android/util/TypedValue/data I
istore 1
aload 4
invokevirtual android/content/res/ColorStateList/getDefaultColor()I
istore 2
getstatic android/support/design/widget/NavigationView/b [I
astore 5
getstatic android/support/design/widget/NavigationView/a [I
astore 6
getstatic android/support/design/widget/NavigationView/EMPTY_STATE_SET [I
astore 7
aload 4
getstatic android/support/design/widget/NavigationView/b [I
iload 2
invokevirtual android/content/res/ColorStateList/getColorForState([II)I
istore 3
new android/content/res/ColorStateList
dup
iconst_3
anewarray [I
dup
iconst_0
aload 5
aastore
dup
iconst_1
aload 6
aastore
dup
iconst_2
aload 7
aastore
iconst_3
newarray int
dup
iconst_0
iload 3
iastore
dup
iconst_1
iload 1
iastore
dup
iconst_2
iload 2
iastore
invokespecial android/content/res/ColorStateList/<init>([[I[I)V
areturn
.limit locals 8
.limit stack 7
.end method

.method private getMenuInflater()Landroid/view/MenuInflater;
aload 0
getfield android/support/design/widget/NavigationView/h Landroid/view/MenuInflater;
ifnonnull L0
aload 0
new android/support/v7/internal/view/f
dup
aload 0
invokevirtual android/support/design/widget/NavigationView/getContext()Landroid/content/Context;
invokespecial android/support/v7/internal/view/f/<init>(Landroid/content/Context;)V
putfield android/support/design/widget/NavigationView/h Landroid/view/MenuInflater;
L0:
aload 0
getfield android/support/design/widget/NavigationView/h Landroid/view/MenuInflater;
areturn
.limit locals 1
.limit stack 4
.end method

.method public final getItemBackground()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/design/widget/NavigationView/e Landroid/support/design/internal/b;
getfield android/support/design/internal/b/h Landroid/graphics/drawable/Drawable;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getItemIconTintList()Landroid/content/res/ColorStateList;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/design/widget/NavigationView/e Landroid/support/design/internal/b;
getfield android/support/design/internal/b/g Landroid/content/res/ColorStateList;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getItemTextColor()Landroid/content/res/ColorStateList;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/design/widget/NavigationView/e Landroid/support/design/internal/b;
getfield android/support/design/internal/b/f Landroid/content/res/ColorStateList;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getMenu()Landroid/view/Menu;
aload 0
getfield android/support/design/widget/NavigationView/d Landroid/support/design/internal/a;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final onMeasure(II)V
iload 1
istore 3
iload 1
invokestatic android/view/View$MeasureSpec/getMode(I)I
lookupswitch
-2147483648 : L0
0 : L1
1073741824 : L2
default : L3
L3:
iload 1
istore 3
L2:
aload 0
iload 3
iload 2
invokespecial android/support/design/internal/f/onMeasure(II)V
return
L0:
iload 1
invokestatic android/view/View$MeasureSpec/getSize(I)I
aload 0
getfield android/support/design/widget/NavigationView/g I
invokestatic java/lang/Math/min(II)I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 3
goto L2
L1:
aload 0
getfield android/support/design/widget/NavigationView/g I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 3
goto L2
.limit locals 4
.limit stack 3
.end method

.method protected final onRestoreInstanceState(Landroid/os/Parcelable;)V
aload 1
checkcast android/support/design/widget/NavigationView$SavedState
astore 1
aload 0
aload 1
invokevirtual android/support/design/widget/NavigationView$SavedState/getSuperState()Landroid/os/Parcelable;
invokespecial android/support/design/internal/f/onRestoreInstanceState(Landroid/os/Parcelable;)V
aload 0
getfield android/support/design/widget/NavigationView/d Landroid/support/design/internal/a;
aload 1
getfield android/support/design/widget/NavigationView$SavedState/a Landroid/os/Bundle;
invokevirtual android/support/design/internal/a/b(Landroid/os/Bundle;)V
return
.limit locals 2
.limit stack 2
.end method

.method protected final onSaveInstanceState()Landroid/os/Parcelable;
new android/support/design/widget/NavigationView$SavedState
dup
aload 0
invokespecial android/support/design/internal/f/onSaveInstanceState()Landroid/os/Parcelable;
invokespecial android/support/design/widget/NavigationView$SavedState/<init>(Landroid/os/Parcelable;)V
astore 1
aload 1
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
putfield android/support/design/widget/NavigationView$SavedState/a Landroid/os/Bundle;
aload 0
getfield android/support/design/widget/NavigationView/d Landroid/support/design/internal/a;
aload 1
getfield android/support/design/widget/NavigationView$SavedState/a Landroid/os/Bundle;
invokevirtual android/support/design/internal/a/a(Landroid/os/Bundle;)V
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method public final setCheckedItem(I)V
.annotation invisibleparam 1 Landroid/support/a/p;
.end annotation
aload 0
getfield android/support/design/widget/NavigationView/d Landroid/support/design/internal/a;
iload 1
invokevirtual android/support/design/internal/a/findItem(I)Landroid/view/MenuItem;
astore 3
aload 3
ifnull L0
aload 0
getfield android/support/design/widget/NavigationView/e Landroid/support/design/internal/b;
astore 2
aload 3
checkcast android/support/v7/internal/view/menu/m
astore 3
aload 2
getfield android/support/design/internal/b/d Landroid/support/design/internal/c;
aload 3
invokevirtual android/support/design/internal/c/a(Landroid/support/v7/internal/view/menu/m;)V
L0:
return
.limit locals 4
.limit stack 2
.end method

.method public final setItemBackground(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield android/support/design/widget/NavigationView/e Landroid/support/design/internal/b;
aload 1
putfield android/support/design/internal/b/h Landroid/graphics/drawable/Drawable;
return
.limit locals 2
.limit stack 2
.end method

.method public final setItemBackgroundResource(I)V
.annotation invisibleparam 1 Landroid/support/a/m;
.end annotation
aload 0
aload 0
invokevirtual android/support/design/widget/NavigationView/getContext()Landroid/content/Context;
iload 1
invokestatic android/support/v4/c/h/a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/design/widget/NavigationView/setItemBackground(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 2
.limit stack 3
.end method

.method public final setItemIconTintList(Landroid/content/res/ColorStateList;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/design/widget/NavigationView/e Landroid/support/design/internal/b;
aload 1
invokevirtual android/support/design/internal/b/a(Landroid/content/res/ColorStateList;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final setItemTextAppearance(I)V
.annotation invisibleparam 1 Landroid/support/a/ai;
.end annotation
aload 0
getfield android/support/design/widget/NavigationView/e Landroid/support/design/internal/b;
iload 1
invokevirtual android/support/design/internal/b/a(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public final setItemTextColor(Landroid/content/res/ColorStateList;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/design/widget/NavigationView/e Landroid/support/design/internal/b;
aload 1
invokevirtual android/support/design/internal/b/b(Landroid/content/res/ColorStateList;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final setNavigationItemSelectedListener(Landroid/support/design/widget/ap;)V
aload 0
aload 1
putfield android/support/design/widget/NavigationView/f Landroid/support/design/widget/ap;
return
.limit locals 2
.limit stack 2
.end method
