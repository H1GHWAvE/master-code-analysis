.bytecode 50.0
.class final synchronized android/support/design/widget/cu
.super android/support/design/widget/cr

.field private static final 'a' I = 10


.field private static final 'b' I = 200


.field private static final 'c' Landroid/os/Handler;

.field private 'd' J

.field private 'e' Z

.field private final 'f' [I

.field private final 'g' [F

.field private 'h' I

.field private 'i' Landroid/view/animation/Interpolator;

.field private 'j' Landroid/support/design/widget/cs;

.field private 'k' Landroid/support/design/widget/ct;

.field private 'l' F

.field private final 'm' Ljava/lang/Runnable;

.method static <clinit>()V
new android/os/Handler
dup
invokestatic android/os/Looper/getMainLooper()Landroid/os/Looper;
invokespecial android/os/Handler/<init>(Landroid/os/Looper;)V
putstatic android/support/design/widget/cu/c Landroid/os/Handler;
return
.limit locals 0
.limit stack 3
.end method

.method <init>()V
aload 0
invokespecial android/support/design/widget/cr/<init>()V
aload 0
iconst_2
newarray int
putfield android/support/design/widget/cu/f [I
aload 0
iconst_2
newarray float
putfield android/support/design/widget/cu/g [F
aload 0
sipush 200
putfield android/support/design/widget/cu/h I
aload 0
new android/support/design/widget/cv
dup
aload 0
invokespecial android/support/design/widget/cv/<init>(Landroid/support/design/widget/cu;)V
putfield android/support/design/widget/cu/m Ljava/lang/Runnable;
return
.limit locals 1
.limit stack 4
.end method

.method static synthetic a(Landroid/support/design/widget/cu;)V
aload 0
getfield android/support/design/widget/cu/e Z
ifeq L0
invokestatic android/os/SystemClock/uptimeMillis()J
aload 0
getfield android/support/design/widget/cu/d J
lsub
l2f
aload 0
getfield android/support/design/widget/cu/h I
i2f
fdiv
fstore 2
fload 2
fstore 1
aload 0
getfield android/support/design/widget/cu/i Landroid/view/animation/Interpolator;
ifnull L1
aload 0
getfield android/support/design/widget/cu/i Landroid/view/animation/Interpolator;
fload 2
invokeinterface android/view/animation/Interpolator/getInterpolation(F)F 1
fstore 1
L1:
aload 0
fload 1
putfield android/support/design/widget/cu/l F
aload 0
getfield android/support/design/widget/cu/k Landroid/support/design/widget/ct;
ifnull L2
aload 0
getfield android/support/design/widget/cu/k Landroid/support/design/widget/ct;
invokeinterface android/support/design/widget/ct/a()V 0
L2:
invokestatic android/os/SystemClock/uptimeMillis()J
aload 0
getfield android/support/design/widget/cu/d J
aload 0
getfield android/support/design/widget/cu/h I
i2l
ladd
lcmp
iflt L0
aload 0
iconst_0
putfield android/support/design/widget/cu/e Z
aload 0
getfield android/support/design/widget/cu/j Landroid/support/design/widget/cs;
ifnull L0
aload 0
getfield android/support/design/widget/cu/j Landroid/support/design/widget/cs;
invokeinterface android/support/design/widget/cs/b()V 0
L0:
aload 0
getfield android/support/design/widget/cu/e Z
ifeq L3
getstatic android/support/design/widget/cu/c Landroid/os/Handler;
aload 0
getfield android/support/design/widget/cu/m Ljava/lang/Runnable;
ldc2_w 10L
invokevirtual android/os/Handler/postDelayed(Ljava/lang/Runnable;J)Z
pop
L3:
return
.limit locals 3
.limit stack 6
.end method

.method private h()V
aload 0
getfield android/support/design/widget/cu/e Z
ifeq L0
invokestatic android/os/SystemClock/uptimeMillis()J
aload 0
getfield android/support/design/widget/cu/d J
lsub
l2f
aload 0
getfield android/support/design/widget/cu/h I
i2f
fdiv
fstore 2
fload 2
fstore 1
aload 0
getfield android/support/design/widget/cu/i Landroid/view/animation/Interpolator;
ifnull L1
aload 0
getfield android/support/design/widget/cu/i Landroid/view/animation/Interpolator;
fload 2
invokeinterface android/view/animation/Interpolator/getInterpolation(F)F 1
fstore 1
L1:
aload 0
fload 1
putfield android/support/design/widget/cu/l F
aload 0
getfield android/support/design/widget/cu/k Landroid/support/design/widget/ct;
ifnull L2
aload 0
getfield android/support/design/widget/cu/k Landroid/support/design/widget/ct;
invokeinterface android/support/design/widget/ct/a()V 0
L2:
invokestatic android/os/SystemClock/uptimeMillis()J
aload 0
getfield android/support/design/widget/cu/d J
aload 0
getfield android/support/design/widget/cu/h I
i2l
ladd
lcmp
iflt L0
aload 0
iconst_0
putfield android/support/design/widget/cu/e Z
aload 0
getfield android/support/design/widget/cu/j Landroid/support/design/widget/cs;
ifnull L0
aload 0
getfield android/support/design/widget/cu/j Landroid/support/design/widget/cs;
invokeinterface android/support/design/widget/cs/b()V 0
L0:
aload 0
getfield android/support/design/widget/cu/e Z
ifeq L3
getstatic android/support/design/widget/cu/c Landroid/os/Handler;
aload 0
getfield android/support/design/widget/cu/m Ljava/lang/Runnable;
ldc2_w 10L
invokevirtual android/os/Handler/postDelayed(Ljava/lang/Runnable;J)Z
pop
L3:
return
.limit locals 3
.limit stack 6
.end method

.method public final a()V
aload 0
getfield android/support/design/widget/cu/e Z
ifeq L0
return
L0:
aload 0
getfield android/support/design/widget/cu/i Landroid/view/animation/Interpolator;
ifnonnull L1
aload 0
new android/view/animation/AccelerateDecelerateInterpolator
dup
invokespecial android/view/animation/AccelerateDecelerateInterpolator/<init>()V
putfield android/support/design/widget/cu/i Landroid/view/animation/Interpolator;
L1:
aload 0
invokestatic android/os/SystemClock/uptimeMillis()J
putfield android/support/design/widget/cu/d J
aload 0
iconst_1
putfield android/support/design/widget/cu/e Z
getstatic android/support/design/widget/cu/c Landroid/os/Handler;
aload 0
getfield android/support/design/widget/cu/m Ljava/lang/Runnable;
ldc2_w 10L
invokevirtual android/os/Handler/postDelayed(Ljava/lang/Runnable;J)Z
pop
return
.limit locals 1
.limit stack 4
.end method

.method public final a(FF)V
aload 0
getfield android/support/design/widget/cu/g [F
iconst_0
fload 1
fastore
aload 0
getfield android/support/design/widget/cu/g [F
iconst_1
fload 2
fastore
return
.limit locals 3
.limit stack 3
.end method

.method public final a(I)V
aload 0
iload 1
putfield android/support/design/widget/cu/h I
return
.limit locals 2
.limit stack 2
.end method

.method public final a(II)V
aload 0
getfield android/support/design/widget/cu/f [I
iconst_0
iload 1
iastore
aload 0
getfield android/support/design/widget/cu/f [I
iconst_1
iload 2
iastore
return
.limit locals 3
.limit stack 3
.end method

.method public final a(Landroid/support/design/widget/cs;)V
aload 0
aload 1
putfield android/support/design/widget/cu/j Landroid/support/design/widget/cs;
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/support/design/widget/ct;)V
aload 0
aload 1
putfield android/support/design/widget/cu/k Landroid/support/design/widget/ct;
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/view/animation/Interpolator;)V
aload 0
aload 1
putfield android/support/design/widget/cu/i Landroid/view/animation/Interpolator;
return
.limit locals 2
.limit stack 2
.end method

.method public final b()Z
aload 0
getfield android/support/design/widget/cu/e Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final c()I
aload 0
getfield android/support/design/widget/cu/f [I
iconst_0
iaload
aload 0
getfield android/support/design/widget/cu/f [I
iconst_1
iaload
aload 0
getfield android/support/design/widget/cu/l F
invokestatic android/support/design/widget/a/a(IIF)I
ireturn
.limit locals 1
.limit stack 3
.end method

.method public final d()F
aload 0
getfield android/support/design/widget/cu/g [F
iconst_0
faload
aload 0
getfield android/support/design/widget/cu/g [F
iconst_1
faload
aload 0
getfield android/support/design/widget/cu/l F
invokestatic android/support/design/widget/a/a(FFF)F
freturn
.limit locals 1
.limit stack 3
.end method

.method public final e()V
aload 0
iconst_0
putfield android/support/design/widget/cu/e Z
getstatic android/support/design/widget/cu/c Landroid/os/Handler;
aload 0
getfield android/support/design/widget/cu/m Ljava/lang/Runnable;
invokevirtual android/os/Handler/removeCallbacks(Ljava/lang/Runnable;)V
aload 0
getfield android/support/design/widget/cu/j Landroid/support/design/widget/cs;
ifnull L0
aload 0
getfield android/support/design/widget/cu/j Landroid/support/design/widget/cs;
invokeinterface android/support/design/widget/cs/c()V 0
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public final f()F
aload 0
getfield android/support/design/widget/cu/l F
freturn
.limit locals 1
.limit stack 1
.end method

.method public final g()V
aload 0
getfield android/support/design/widget/cu/e Z
ifeq L0
aload 0
iconst_0
putfield android/support/design/widget/cu/e Z
getstatic android/support/design/widget/cu/c Landroid/os/Handler;
aload 0
getfield android/support/design/widget/cu/m Ljava/lang/Runnable;
invokevirtual android/os/Handler/removeCallbacks(Ljava/lang/Runnable;)V
aload 0
fconst_1
putfield android/support/design/widget/cu/l F
aload 0
getfield android/support/design/widget/cu/k Landroid/support/design/widget/ct;
ifnull L1
aload 0
getfield android/support/design/widget/cu/k Landroid/support/design/widget/ct;
invokeinterface android/support/design/widget/ct/a()V 0
L1:
aload 0
getfield android/support/design/widget/cu/j Landroid/support/design/widget/cs;
ifnull L0
aload 0
getfield android/support/design/widget/cu/j Landroid/support/design/widget/cs;
invokeinterface android/support/design/widget/cs/b()V 0
L0:
return
.limit locals 1
.limit stack 2
.end method
