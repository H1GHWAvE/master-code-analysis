.bytecode 50.0
.class public synchronized abstract android/support/design/widget/t
.super java/lang/Object

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 3
.limit stack 1
.end method

.method private static a()I
ldc_w -16777216
ireturn
.limit locals 0
.limit stack 1
.end method

.method private static a(Landroid/support/v4/view/gh;)Landroid/support/v4/view/gh;
aload 0
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/view/View;Ljava/lang/Object;)V
aload 0
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
aload 1
putfield android/support/design/widget/w/m Ljava/lang/Object;
return
.limit locals 2
.limit stack 2
.end method

.method private static b()F
fconst_0
freturn
.limit locals 0
.limit stack 1
.end method

.method private static c(Landroid/view/View;)Ljava/lang/Object;
aload 0
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/design/widget/w
getfield android/support/design/widget/w/m Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static c()Z
iconst_0
ireturn
.limit locals 0
.limit stack 1
.end method

.method private static d()Z
iconst_0
ireturn
.limit locals 0
.limit stack 1
.end method

.method private static e()V
return
.limit locals 0
.limit stack 0
.end method

.method private static f()Z
iconst_0
ireturn
.limit locals 0
.limit stack 1
.end method

.method public a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)Landroid/os/Parcelable;
getstatic android/view/View$BaseSavedState/EMPTY_STATE Landroid/view/AbsSavedState;
areturn
.limit locals 3
.limit stack 1
.end method

.method public a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I[I)V
return
.limit locals 5
.limit stack 0
.end method

.method public a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/os/Parcelable;)V
return
.limit locals 4
.limit stack 0
.end method

.method public a(Landroid/view/View;)V
return
.limit locals 2
.limit stack 0
.end method

.method public a(Landroid/view/View;Landroid/view/View;)V
return
.limit locals 3
.limit stack 0
.end method

.method public a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;FZ)Z
iconst_0
ireturn
.limit locals 5
.limit stack 1
.end method

.method public a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)Z
iconst_0
ireturn
.limit locals 4
.limit stack 1
.end method

.method public a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;III)Z
iconst_0
ireturn
.limit locals 6
.limit stack 1
.end method

.method public a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z
iconst_0
ireturn
.limit locals 4
.limit stack 1
.end method

.method public a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)Z
iconst_0
ireturn
.limit locals 4
.limit stack 1
.end method

.method public a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;I)Z
iconst_0
ireturn
.limit locals 5
.limit stack 1
.end method

.method public b(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)V
return
.limit locals 4
.limit stack 0
.end method

.method public b(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z
iconst_0
ireturn
.limit locals 4
.limit stack 1
.end method

.method public b(Landroid/view/View;)Z
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method
