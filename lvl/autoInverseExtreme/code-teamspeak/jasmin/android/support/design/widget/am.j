.bytecode 50.0
.class final synchronized android/support/design/widget/am
.super android/support/design/widget/ai
.annotation invisible Landroid/annotation/TargetApi;
value I = 21
.end annotation

.field private 'h' Landroid/graphics/drawable/Drawable;

.field private 'i' Landroid/graphics/drawable/RippleDrawable;

.field private 'j' Landroid/graphics/drawable/Drawable;

.field private 'k' Landroid/view/animation/Interpolator;

.method <init>(Landroid/view/View;Landroid/support/design/widget/as;)V
aload 0
aload 1
aload 2
invokespecial android/support/design/widget/ai/<init>(Landroid/view/View;Landroid/support/design/widget/as;)V
aload 1
invokevirtual android/view/View/isInEditMode()Z
ifne L0
aload 0
aload 0
getfield android/support/design/widget/am/f Landroid/view/View;
invokevirtual android/view/View/getContext()Landroid/content/Context;
ldc_w 17563661
invokestatic android/view/animation/AnimationUtils/loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;
putfield android/support/design/widget/am/k Landroid/view/animation/Interpolator;
L0:
return
.limit locals 3
.limit stack 3
.end method

.method private a(Landroid/animation/Animator;)Landroid/animation/Animator;
aload 1
aload 0
getfield android/support/design/widget/am/k Landroid/view/animation/Interpolator;
invokevirtual android/animation/Animator/setInterpolator(Landroid/animation/TimeInterpolator;)V
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method final a()V
return
.limit locals 1
.limit stack 0
.end method

.method public final a(F)V
aload 0
getfield android/support/design/widget/am/f Landroid/view/View;
fload 1
invokestatic android/support/v4/view/cx/f(Landroid/view/View;F)V
return
.limit locals 2
.limit stack 2
.end method

.method final a(I)V
aload 0
getfield android/support/design/widget/am/i Landroid/graphics/drawable/RippleDrawable;
iload 1
invokestatic android/content/res/ColorStateList/valueOf(I)Landroid/content/res/ColorStateList;
invokevirtual android/graphics/drawable/RippleDrawable/setColor(Landroid/content/res/ColorStateList;)V
return
.limit locals 2
.limit stack 2
.end method

.method final a(Landroid/content/res/ColorStateList;)V
aload 0
getfield android/support/design/widget/am/h Landroid/graphics/drawable/Drawable;
aload 1
invokestatic android/support/v4/e/a/a/a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V
aload 0
getfield android/support/design/widget/am/j Landroid/graphics/drawable/Drawable;
ifnull L0
aload 0
getfield android/support/design/widget/am/j Landroid/graphics/drawable/Drawable;
aload 1
invokestatic android/support/v4/e/a/a/a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method final a(Landroid/graphics/PorterDuff$Mode;)V
aload 0
getfield android/support/design/widget/am/h Landroid/graphics/drawable/Drawable;
aload 1
invokestatic android/support/v4/e/a/a/a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V
return
.limit locals 2
.limit stack 2
.end method

.method final a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;II)V
aload 0
aload 1
invokevirtual android/graphics/drawable/Drawable/mutate()Landroid/graphics/drawable/Drawable;
invokestatic android/support/v4/e/a/a/c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
putfield android/support/design/widget/am/h Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/design/widget/am/h Landroid/graphics/drawable/Drawable;
aload 2
invokestatic android/support/v4/e/a/a/a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V
aload 3
ifnull L0
aload 0
getfield android/support/design/widget/am/h Landroid/graphics/drawable/Drawable;
aload 3
invokestatic android/support/v4/e/a/a/a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V
L0:
iload 5
ifle L1
aload 0
aload 0
iload 5
aload 2
invokevirtual android/support/design/widget/am/a(ILandroid/content/res/ColorStateList;)Landroid/graphics/drawable/Drawable;
putfield android/support/design/widget/am/j Landroid/graphics/drawable/Drawable;
new android/graphics/drawable/LayerDrawable
dup
iconst_2
anewarray android/graphics/drawable/Drawable
dup
iconst_0
aload 0
getfield android/support/design/widget/am/j Landroid/graphics/drawable/Drawable;
aastore
dup
iconst_1
aload 0
getfield android/support/design/widget/am/h Landroid/graphics/drawable/Drawable;
aastore
invokespecial android/graphics/drawable/LayerDrawable/<init>([Landroid/graphics/drawable/Drawable;)V
astore 1
L2:
aload 0
new android/graphics/drawable/RippleDrawable
dup
iload 4
invokestatic android/content/res/ColorStateList/valueOf(I)Landroid/content/res/ColorStateList;
aload 1
aconst_null
invokespecial android/graphics/drawable/RippleDrawable/<init>(Landroid/content/res/ColorStateList;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
putfield android/support/design/widget/am/i Landroid/graphics/drawable/RippleDrawable;
aload 0
getfield android/support/design/widget/am/g Landroid/support/design/widget/as;
aload 0
getfield android/support/design/widget/am/i Landroid/graphics/drawable/RippleDrawable;
invokeinterface android/support/design/widget/as/a(Landroid/graphics/drawable/Drawable;)V 1
aload 0
getfield android/support/design/widget/am/g Landroid/support/design/widget/as;
iconst_0
iconst_0
iconst_0
iconst_0
invokeinterface android/support/design/widget/as/a(IIII)V 4
return
L1:
aload 0
aconst_null
putfield android/support/design/widget/am/j Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/design/widget/am/h Landroid/graphics/drawable/Drawable;
astore 1
goto L2
.limit locals 6
.limit stack 6
.end method

.method final a([I)V
return
.limit locals 2
.limit stack 0
.end method

.method final b(F)V
new android/animation/StateListAnimator
dup
invokespecial android/animation/StateListAnimator/<init>()V
astore 2
aload 2
getstatic android/support/design/widget/am/c [I
aload 0
aload 0
getfield android/support/design/widget/am/f Landroid/view/View;
ldc "translationZ"
iconst_1
newarray float
dup
iconst_0
fload 1
fastore
invokestatic android/animation/ObjectAnimator/ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;
invokespecial android/support/design/widget/am/a(Landroid/animation/Animator;)Landroid/animation/Animator;
invokevirtual android/animation/StateListAnimator/addState([ILandroid/animation/Animator;)V
aload 2
getstatic android/support/design/widget/am/d [I
aload 0
aload 0
getfield android/support/design/widget/am/f Landroid/view/View;
ldc "translationZ"
iconst_1
newarray float
dup
iconst_0
fload 1
fastore
invokestatic android/animation/ObjectAnimator/ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;
invokespecial android/support/design/widget/am/a(Landroid/animation/Animator;)Landroid/animation/Animator;
invokevirtual android/animation/StateListAnimator/addState([ILandroid/animation/Animator;)V
aload 2
getstatic android/support/design/widget/am/e [I
aload 0
aload 0
getfield android/support/design/widget/am/f Landroid/view/View;
ldc "translationZ"
iconst_1
newarray float
dup
iconst_0
fconst_0
fastore
invokestatic android/animation/ObjectAnimator/ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;
invokespecial android/support/design/widget/am/a(Landroid/animation/Animator;)Landroid/animation/Animator;
invokevirtual android/animation/StateListAnimator/addState([ILandroid/animation/Animator;)V
aload 0
getfield android/support/design/widget/am/f Landroid/view/View;
aload 2
invokevirtual android/view/View/setStateListAnimator(Landroid/animation/StateListAnimator;)V
return
.limit locals 3
.limit stack 9
.end method

.method final d()Landroid/support/design/widget/j;
new android/support/design/widget/k
dup
invokespecial android/support/design/widget/k/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method
