.bytecode 50.0
.class public final synchronized android/support/design/widget/FloatingActionButton
.super android/widget/ImageView
.annotation visible Landroid/support/design/widget/u;
a c = Landroid/support/design/widget/FloatingActionButton$Behavior;
.end annotation

.field private static final 'b' I = 1


.field private static final 'c' I = 0


.field final 'a' Landroid/support/design/widget/al;

.field private 'd' Landroid/content/res/ColorStateList;

.field private 'e' Landroid/graphics/PorterDuff$Mode;

.field private 'f' I

.field private 'g' I

.field private 'h' I

.field private 'i' I

.field private final 'j' Landroid/graphics/Rect;

.method private <init>(Landroid/content/Context;)V
aload 0
aload 1
iconst_0
invokespecial android/support/design/widget/FloatingActionButton/<init>(Landroid/content/Context;B)V
return
.limit locals 2
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;B)V
aload 0
aload 1
iconst_0
invokespecial android/support/design/widget/FloatingActionButton/<init>(Landroid/content/Context;C)V
return
.limit locals 3
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;C)V
aconst_null
astore 5
aload 0
aload 1
aconst_null
iconst_0
invokespecial android/widget/ImageView/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/design/widget/FloatingActionButton/j Landroid/graphics/Rect;
aload 1
aconst_null
getstatic android/support/design/n/FloatingActionButton [I
iconst_0
getstatic android/support/design/m/Widget_Design_FloatingActionButton I
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
astore 7
aload 7
getstatic android/support/design/n/FloatingActionButton_android_background I
invokevirtual android/content/res/TypedArray/getDrawable(I)Landroid/graphics/drawable/Drawable;
astore 6
aload 0
aload 7
getstatic android/support/design/n/FloatingActionButton_backgroundTint I
invokevirtual android/content/res/TypedArray/getColorStateList(I)Landroid/content/res/ColorStateList;
putfield android/support/design/widget/FloatingActionButton/d Landroid/content/res/ColorStateList;
aload 7
getstatic android/support/design/n/FloatingActionButton_backgroundTintMode I
iconst_m1
invokevirtual android/content/res/TypedArray/getInt(II)I
lookupswitch
3 : L0
5 : L1
9 : L2
14 : L3
15 : L4
default : L5
L5:
aload 5
astore 1
L6:
aload 0
aload 1
putfield android/support/design/widget/FloatingActionButton/e Landroid/graphics/PorterDuff$Mode;
aload 0
aload 7
getstatic android/support/design/n/FloatingActionButton_rippleColor I
iconst_0
invokevirtual android/content/res/TypedArray/getColor(II)I
putfield android/support/design/widget/FloatingActionButton/g I
aload 0
aload 7
getstatic android/support/design/n/FloatingActionButton_fabSize I
iconst_0
invokevirtual android/content/res/TypedArray/getInt(II)I
putfield android/support/design/widget/FloatingActionButton/h I
aload 0
aload 7
getstatic android/support/design/n/FloatingActionButton_borderWidth I
iconst_0
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
putfield android/support/design/widget/FloatingActionButton/f I
aload 7
getstatic android/support/design/n/FloatingActionButton_elevation I
fconst_0
invokevirtual android/content/res/TypedArray/getDimension(IF)F
fstore 3
aload 7
getstatic android/support/design/n/FloatingActionButton_pressedTranslationZ I
fconst_0
invokevirtual android/content/res/TypedArray/getDimension(IF)F
fstore 4
aload 7
invokevirtual android/content/res/TypedArray/recycle()V
new android/support/design/widget/ac
dup
aload 0
invokespecial android/support/design/widget/ac/<init>(Landroid/support/design/widget/FloatingActionButton;)V
astore 1
getstatic android/os/Build$VERSION/SDK_INT I
istore 2
iload 2
bipush 21
if_icmplt L7
aload 0
new android/support/design/widget/am
dup
aload 0
aload 1
invokespecial android/support/design/widget/am/<init>(Landroid/view/View;Landroid/support/design/widget/as;)V
putfield android/support/design/widget/FloatingActionButton/a Landroid/support/design/widget/al;
L8:
aload 0
invokevirtual android/support/design/widget/FloatingActionButton/getResources()Landroid/content/res/Resources;
getstatic android/support/design/g/design_fab_content_size I
invokevirtual android/content/res/Resources/getDimension(I)F
f2i
istore 2
aload 0
aload 0
invokevirtual android/support/design/widget/FloatingActionButton/getSizeDimension()I
iload 2
isub
iconst_2
idiv
putfield android/support/design/widget/FloatingActionButton/i I
aload 0
getfield android/support/design/widget/FloatingActionButton/a Landroid/support/design/widget/al;
aload 6
aload 0
getfield android/support/design/widget/FloatingActionButton/d Landroid/content/res/ColorStateList;
aload 0
getfield android/support/design/widget/FloatingActionButton/e Landroid/graphics/PorterDuff$Mode;
aload 0
getfield android/support/design/widget/FloatingActionButton/g I
aload 0
getfield android/support/design/widget/FloatingActionButton/f I
invokevirtual android/support/design/widget/al/a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;II)V
aload 0
getfield android/support/design/widget/FloatingActionButton/a Landroid/support/design/widget/al;
fload 3
invokevirtual android/support/design/widget/al/a(F)V
aload 0
getfield android/support/design/widget/FloatingActionButton/a Landroid/support/design/widget/al;
fload 4
invokevirtual android/support/design/widget/al/b(F)V
aload 0
iconst_1
invokevirtual android/support/design/widget/FloatingActionButton/setClickable(Z)V
return
L0:
getstatic android/graphics/PorterDuff$Mode/SRC_OVER Landroid/graphics/PorterDuff$Mode;
astore 1
goto L6
L1:
getstatic android/graphics/PorterDuff$Mode/SRC_IN Landroid/graphics/PorterDuff$Mode;
astore 1
goto L6
L2:
getstatic android/graphics/PorterDuff$Mode/SRC_ATOP Landroid/graphics/PorterDuff$Mode;
astore 1
goto L6
L3:
getstatic android/graphics/PorterDuff$Mode/MULTIPLY Landroid/graphics/PorterDuff$Mode;
astore 1
goto L6
L4:
getstatic android/graphics/PorterDuff$Mode/SCREEN Landroid/graphics/PorterDuff$Mode;
astore 1
goto L6
L7:
iload 2
bipush 12
if_icmplt L9
aload 0
new android/support/design/widget/ai
dup
aload 0
aload 1
invokespecial android/support/design/widget/ai/<init>(Landroid/view/View;Landroid/support/design/widget/as;)V
putfield android/support/design/widget/FloatingActionButton/a Landroid/support/design/widget/al;
goto L8
L9:
aload 0
new android/support/design/widget/ad
dup
aload 0
aload 1
invokespecial android/support/design/widget/ad/<init>(Landroid/view/View;Landroid/support/design/widget/as;)V
putfield android/support/design/widget/FloatingActionButton/a Landroid/support/design/widget/al;
goto L8
.limit locals 8
.limit stack 6
.end method

.method private static a(II)I
iload 1
invokestatic android/view/View$MeasureSpec/getMode(I)I
istore 2
iload 1
invokestatic android/view/View$MeasureSpec/getSize(I)I
istore 1
iload 2
lookupswitch
-2147483648 : L0
0 : L1
1073741824 : L2
default : L1
L1:
iload 0
ireturn
L0:
iload 0
iload 1
invokestatic java/lang/Math/min(II)I
ireturn
L2:
iload 1
ireturn
.limit locals 3
.limit stack 2
.end method

.method private static a(I)Landroid/graphics/PorterDuff$Mode;
iload 0
lookupswitch
3 : L0
5 : L1
9 : L2
14 : L3
15 : L4
default : L5
L5:
aconst_null
areturn
L0:
getstatic android/graphics/PorterDuff$Mode/SRC_OVER Landroid/graphics/PorterDuff$Mode;
areturn
L1:
getstatic android/graphics/PorterDuff$Mode/SRC_IN Landroid/graphics/PorterDuff$Mode;
areturn
L2:
getstatic android/graphics/PorterDuff$Mode/SRC_ATOP Landroid/graphics/PorterDuff$Mode;
areturn
L3:
getstatic android/graphics/PorterDuff$Mode/MULTIPLY Landroid/graphics/PorterDuff$Mode;
areturn
L4:
getstatic android/graphics/PorterDuff$Mode/SCREEN Landroid/graphics/PorterDuff$Mode;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(Landroid/support/design/widget/FloatingActionButton;)Landroid/graphics/Rect;
aload 0
getfield android/support/design/widget/FloatingActionButton/j Landroid/graphics/Rect;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a()V
aload 0
getfield android/support/design/widget/FloatingActionButton/a Landroid/support/design/widget/al;
invokevirtual android/support/design/widget/al/c()V
return
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(Landroid/support/design/widget/FloatingActionButton;Landroid/graphics/drawable/Drawable;)V
aload 0
aload 1
invokespecial android/view/View/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic b(Landroid/support/design/widget/FloatingActionButton;)I
aload 0
getfield android/support/design/widget/FloatingActionButton/i I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private b()V
aload 0
getfield android/support/design/widget/FloatingActionButton/a Landroid/support/design/widget/al;
invokevirtual android/support/design/widget/al/b()V
return
.limit locals 1
.limit stack 1
.end method

.method protected final drawableStateChanged()V
aload 0
invokespecial android/widget/ImageView/drawableStateChanged()V
aload 0
getfield android/support/design/widget/FloatingActionButton/a Landroid/support/design/widget/al;
aload 0
invokevirtual android/support/design/widget/FloatingActionButton/getDrawableState()[I
invokevirtual android/support/design/widget/al/a([I)V
return
.limit locals 1
.limit stack 2
.end method

.method public final getBackgroundTintList()Landroid/content/res/ColorStateList;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/design/widget/FloatingActionButton/d Landroid/content/res/ColorStateList;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getBackgroundTintMode()Landroid/graphics/PorterDuff$Mode;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/design/widget/FloatingActionButton/e Landroid/graphics/PorterDuff$Mode;
areturn
.limit locals 1
.limit stack 1
.end method

.method final getSizeDimension()I
aload 0
getfield android/support/design/widget/FloatingActionButton/h I
tableswitch 1
L0
default : L1
L1:
aload 0
invokevirtual android/support/design/widget/FloatingActionButton/getResources()Landroid/content/res/Resources;
getstatic android/support/design/g/design_fab_size_normal I
invokevirtual android/content/res/Resources/getDimensionPixelSize(I)I
ireturn
L0:
aload 0
invokevirtual android/support/design/widget/FloatingActionButton/getResources()Landroid/content/res/Resources;
getstatic android/support/design/g/design_fab_size_mini I
invokevirtual android/content/res/Resources/getDimensionPixelSize(I)I
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final jumpDrawablesToCurrentState()V
.annotation invisible Landroid/annotation/TargetApi;
value I = 11
.end annotation
aload 0
invokespecial android/widget/ImageView/jumpDrawablesToCurrentState()V
aload 0
getfield android/support/design/widget/FloatingActionButton/a Landroid/support/design/widget/al;
invokevirtual android/support/design/widget/al/a()V
return
.limit locals 1
.limit stack 1
.end method

.method protected final onMeasure(II)V
aload 0
invokevirtual android/support/design/widget/FloatingActionButton/getSizeDimension()I
istore 3
iload 3
iload 1
invokestatic android/support/design/widget/FloatingActionButton/a(II)I
iload 3
iload 2
invokestatic android/support/design/widget/FloatingActionButton/a(II)I
invokestatic java/lang/Math/min(II)I
istore 1
aload 0
aload 0
getfield android/support/design/widget/FloatingActionButton/j Landroid/graphics/Rect;
getfield android/graphics/Rect/left I
iload 1
iadd
aload 0
getfield android/support/design/widget/FloatingActionButton/j Landroid/graphics/Rect;
getfield android/graphics/Rect/right I
iadd
iload 1
aload 0
getfield android/support/design/widget/FloatingActionButton/j Landroid/graphics/Rect;
getfield android/graphics/Rect/top I
iadd
aload 0
getfield android/support/design/widget/FloatingActionButton/j Landroid/graphics/Rect;
getfield android/graphics/Rect/bottom I
iadd
invokevirtual android/support/design/widget/FloatingActionButton/setMeasuredDimension(II)V
return
.limit locals 4
.limit stack 4
.end method

.method public final setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
aload 0
getfield android/support/design/widget/FloatingActionButton/a Landroid/support/design/widget/al;
ifnull L0
aload 0
getfield android/support/design/widget/FloatingActionButton/a Landroid/support/design/widget/al;
aload 1
aload 0
getfield android/support/design/widget/FloatingActionButton/d Landroid/content/res/ColorStateList;
aload 0
getfield android/support/design/widget/FloatingActionButton/e Landroid/graphics/PorterDuff$Mode;
aload 0
getfield android/support/design/widget/FloatingActionButton/g I
aload 0
getfield android/support/design/widget/FloatingActionButton/f I
invokevirtual android/support/design/widget/al/a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;II)V
L0:
return
.limit locals 2
.limit stack 6
.end method

.method public final setBackgroundTintList(Landroid/content/res/ColorStateList;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/design/widget/FloatingActionButton/d Landroid/content/res/ColorStateList;
aload 1
if_acmpeq L0
aload 0
aload 1
putfield android/support/design/widget/FloatingActionButton/d Landroid/content/res/ColorStateList;
aload 0
getfield android/support/design/widget/FloatingActionButton/a Landroid/support/design/widget/al;
aload 1
invokevirtual android/support/design/widget/al/a(Landroid/content/res/ColorStateList;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final setBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/design/widget/FloatingActionButton/e Landroid/graphics/PorterDuff$Mode;
aload 1
if_acmpeq L0
aload 0
aload 1
putfield android/support/design/widget/FloatingActionButton/e Landroid/graphics/PorterDuff$Mode;
aload 0
getfield android/support/design/widget/FloatingActionButton/a Landroid/support/design/widget/al;
aload 1
invokevirtual android/support/design/widget/al/a(Landroid/graphics/PorterDuff$Mode;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final setRippleColor(I)V
.annotation invisibleparam 1 Landroid/support/a/j;
.end annotation
aload 0
getfield android/support/design/widget/FloatingActionButton/g I
iload 1
if_icmpeq L0
aload 0
iload 1
putfield android/support/design/widget/FloatingActionButton/g I
aload 0
getfield android/support/design/widget/FloatingActionButton/a Landroid/support/design/widget/al;
iload 1
invokevirtual android/support/design/widget/al/a(I)V
L0:
return
.limit locals 2
.limit stack 2
.end method
