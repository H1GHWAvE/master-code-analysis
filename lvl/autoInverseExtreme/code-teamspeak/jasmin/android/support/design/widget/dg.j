.bytecode 50.0
.class final synchronized android/support/design/widget/dg
.super java/lang/Object

.field 'a' I

.field 'b' I

.field private final 'c' Landroid/view/View;

.field private 'd' I

.field private 'e' I

.method public <init>(Landroid/view/View;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/design/widget/dg/c Landroid/view/View;
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Landroid/view/View;)V
aload 0
invokestatic android/support/v4/view/cx/m(Landroid/view/View;)F
fstore 1
aload 0
fconst_1
fload 1
fadd
invokestatic android/support/v4/view/cx/a(Landroid/view/View;F)V
aload 0
fload 1
invokestatic android/support/v4/view/cx/a(Landroid/view/View;F)V
return
.limit locals 2
.limit stack 3
.end method

.method private b()V
aload 0
getfield android/support/design/widget/dg/c Landroid/view/View;
aload 0
getfield android/support/design/widget/dg/a I
aload 0
getfield android/support/design/widget/dg/c Landroid/view/View;
invokevirtual android/view/View/getTop()I
aload 0
getfield android/support/design/widget/dg/d I
isub
isub
invokestatic android/support/v4/view/cx/d(Landroid/view/View;I)V
aload 0
getfield android/support/design/widget/dg/c Landroid/view/View;
aload 0
getfield android/support/design/widget/dg/b I
aload 0
getfield android/support/design/widget/dg/c Landroid/view/View;
invokevirtual android/view/View/getLeft()I
aload 0
getfield android/support/design/widget/dg/e I
isub
isub
invokestatic android/support/v4/view/cx/e(Landroid/view/View;I)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 23
if_icmpge L0
aload 0
getfield android/support/design/widget/dg/c Landroid/view/View;
invokestatic android/support/design/widget/dg/a(Landroid/view/View;)V
aload 0
getfield android/support/design/widget/dg/c Landroid/view/View;
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
astore 1
aload 1
instanceof android/view/View
ifeq L0
aload 1
checkcast android/view/View
invokestatic android/support/design/widget/dg/a(Landroid/view/View;)V
L0:
return
.limit locals 2
.limit stack 4
.end method

.method private c()I
aload 0
getfield android/support/design/widget/dg/a I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d()I
aload 0
getfield android/support/design/widget/dg/b I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a()V
aload 0
aload 0
getfield android/support/design/widget/dg/c Landroid/view/View;
invokevirtual android/view/View/getTop()I
putfield android/support/design/widget/dg/d I
aload 0
aload 0
getfield android/support/design/widget/dg/c Landroid/view/View;
invokevirtual android/view/View/getLeft()I
putfield android/support/design/widget/dg/e I
aload 0
invokespecial android/support/design/widget/dg/b()V
return
.limit locals 1
.limit stack 2
.end method

.method public final a(I)Z
aload 0
getfield android/support/design/widget/dg/a I
iload 1
if_icmpeq L0
aload 0
iload 1
putfield android/support/design/widget/dg/a I
aload 0
invokespecial android/support/design/widget/dg/b()V
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final b(I)Z
aload 0
getfield android/support/design/widget/dg/b I
iload 1
if_icmpeq L0
aload 0
iload 1
putfield android/support/design/widget/dg/b I
aload 0
invokespecial android/support/design/widget/dg/b()V
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method
