.bytecode 50.0
.class public synchronized android/support/design/widget/Snackbar$SnackbarLayout
.super android/widget/LinearLayout

.field 'a' Landroid/widget/TextView;

.field 'b' Landroid/widget/Button;

.field private 'c' I

.field private 'd' I

.field private 'e' Landroid/support/design/widget/bg;

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
aconst_null
invokespecial android/support/design/widget/Snackbar$SnackbarLayout/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 1
aload 2
getstatic android/support/design/n/SnackbarLayout [I
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
astore 2
aload 0
aload 2
getstatic android/support/design/n/SnackbarLayout_android_maxWidth I
iconst_m1
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
putfield android/support/design/widget/Snackbar$SnackbarLayout/c I
aload 0
aload 2
getstatic android/support/design/n/SnackbarLayout_maxActionInlineWidth I
iconst_m1
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
putfield android/support/design/widget/Snackbar$SnackbarLayout/d I
aload 2
getstatic android/support/design/n/SnackbarLayout_elevation I
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L0
aload 0
aload 2
getstatic android/support/design/n/SnackbarLayout_elevation I
iconst_0
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
i2f
invokestatic android/support/v4/view/cx/f(Landroid/view/View;F)V
L0:
aload 2
invokevirtual android/content/res/TypedArray/recycle()V
aload 0
iconst_1
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/setClickable(Z)V
aload 1
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
getstatic android/support/design/k/design_layout_snackbar_include I
aload 0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
pop
return
.limit locals 3
.limit stack 4
.end method

.method private a()V
aload 0
getfield android/support/design/widget/Snackbar$SnackbarLayout/a Landroid/widget/TextView;
fconst_0
invokestatic android/support/v4/view/cx/c(Landroid/view/View;F)V
aload 0
getfield android/support/design/widget/Snackbar$SnackbarLayout/a Landroid/widget/TextView;
invokestatic android/support/v4/view/cx/p(Landroid/view/View;)Landroid/support/v4/view/fk;
fconst_1
invokevirtual android/support/v4/view/fk/a(F)Landroid/support/v4/view/fk;
ldc2_w 180L
invokevirtual android/support/v4/view/fk/a(J)Landroid/support/v4/view/fk;
ldc2_w 70L
invokevirtual android/support/v4/view/fk/b(J)Landroid/support/v4/view/fk;
invokevirtual android/support/v4/view/fk/b()V
aload 0
getfield android/support/design/widget/Snackbar$SnackbarLayout/b Landroid/widget/Button;
invokevirtual android/widget/Button/getVisibility()I
ifne L0
aload 0
getfield android/support/design/widget/Snackbar$SnackbarLayout/b Landroid/widget/Button;
fconst_0
invokestatic android/support/v4/view/cx/c(Landroid/view/View;F)V
aload 0
getfield android/support/design/widget/Snackbar$SnackbarLayout/b Landroid/widget/Button;
invokestatic android/support/v4/view/cx/p(Landroid/view/View;)Landroid/support/v4/view/fk;
fconst_1
invokevirtual android/support/v4/view/fk/a(F)Landroid/support/v4/view/fk;
ldc2_w 180L
invokevirtual android/support/v4/view/fk/a(J)Landroid/support/v4/view/fk;
ldc2_w 70L
invokevirtual android/support/v4/view/fk/b(J)Landroid/support/v4/view/fk;
invokevirtual android/support/v4/view/fk/b()V
L0:
return
.limit locals 1
.limit stack 3
.end method

.method private static a(Landroid/view/View;II)V
aload 0
invokestatic android/support/v4/view/cx/y(Landroid/view/View;)Z
ifeq L0
aload 0
aload 0
invokestatic android/support/v4/view/cx/k(Landroid/view/View;)I
iload 1
aload 0
invokestatic android/support/v4/view/cx/l(Landroid/view/View;)I
iload 2
invokestatic android/support/v4/view/cx/b(Landroid/view/View;IIII)V
return
L0:
aload 0
aload 0
invokevirtual android/view/View/getPaddingLeft()I
iload 1
aload 0
invokevirtual android/view/View/getPaddingRight()I
iload 2
invokevirtual android/view/View/setPadding(IIII)V
return
.limit locals 3
.limit stack 5
.end method

.method private a(III)Z
iconst_0
istore 4
iload 1
aload 0
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/getOrientation()I
if_icmpeq L0
aload 0
iload 1
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/setOrientation(I)V
iconst_1
istore 4
L0:
aload 0
getfield android/support/design/widget/Snackbar$SnackbarLayout/a Landroid/widget/TextView;
invokevirtual android/widget/TextView/getPaddingTop()I
iload 2
if_icmpne L1
aload 0
getfield android/support/design/widget/Snackbar$SnackbarLayout/a Landroid/widget/TextView;
invokevirtual android/widget/TextView/getPaddingBottom()I
iload 3
if_icmpeq L2
L1:
aload 0
getfield android/support/design/widget/Snackbar$SnackbarLayout/a Landroid/widget/TextView;
astore 5
aload 5
invokestatic android/support/v4/view/cx/y(Landroid/view/View;)Z
ifeq L3
aload 5
aload 5
invokestatic android/support/v4/view/cx/k(Landroid/view/View;)I
iload 2
aload 5
invokestatic android/support/v4/view/cx/l(Landroid/view/View;)I
iload 3
invokestatic android/support/v4/view/cx/b(Landroid/view/View;IIII)V
L4:
iconst_1
istore 4
L2:
iload 4
ireturn
L3:
aload 5
aload 5
invokevirtual android/view/View/getPaddingLeft()I
iload 2
aload 5
invokevirtual android/view/View/getPaddingRight()I
iload 3
invokevirtual android/view/View/setPadding(IIII)V
goto L4
.limit locals 6
.limit stack 5
.end method

.method private b()V
aload 0
getfield android/support/design/widget/Snackbar$SnackbarLayout/a Landroid/widget/TextView;
fconst_1
invokestatic android/support/v4/view/cx/c(Landroid/view/View;F)V
aload 0
getfield android/support/design/widget/Snackbar$SnackbarLayout/a Landroid/widget/TextView;
invokestatic android/support/v4/view/cx/p(Landroid/view/View;)Landroid/support/v4/view/fk;
fconst_0
invokevirtual android/support/v4/view/fk/a(F)Landroid/support/v4/view/fk;
ldc2_w 180L
invokevirtual android/support/v4/view/fk/a(J)Landroid/support/v4/view/fk;
lconst_0
invokevirtual android/support/v4/view/fk/b(J)Landroid/support/v4/view/fk;
invokevirtual android/support/v4/view/fk/b()V
aload 0
getfield android/support/design/widget/Snackbar$SnackbarLayout/b Landroid/widget/Button;
invokevirtual android/widget/Button/getVisibility()I
ifne L0
aload 0
getfield android/support/design/widget/Snackbar$SnackbarLayout/b Landroid/widget/Button;
fconst_1
invokestatic android/support/v4/view/cx/c(Landroid/view/View;F)V
aload 0
getfield android/support/design/widget/Snackbar$SnackbarLayout/b Landroid/widget/Button;
invokestatic android/support/v4/view/cx/p(Landroid/view/View;)Landroid/support/v4/view/fk;
fconst_0
invokevirtual android/support/v4/view/fk/a(F)Landroid/support/v4/view/fk;
ldc2_w 180L
invokevirtual android/support/v4/view/fk/a(J)Landroid/support/v4/view/fk;
lconst_0
invokevirtual android/support/v4/view/fk/b(J)Landroid/support/v4/view/fk;
invokevirtual android/support/v4/view/fk/b()V
L0:
return
.limit locals 1
.limit stack 3
.end method

.method getActionView()Landroid/widget/Button;
aload 0
getfield android/support/design/widget/Snackbar$SnackbarLayout/b Landroid/widget/Button;
areturn
.limit locals 1
.limit stack 1
.end method

.method getMessageView()Landroid/widget/TextView;
aload 0
getfield android/support/design/widget/Snackbar$SnackbarLayout/a Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected onFinishInflate()V
aload 0
invokespecial android/widget/LinearLayout/onFinishInflate()V
aload 0
aload 0
getstatic android/support/design/i/snackbar_text I
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield android/support/design/widget/Snackbar$SnackbarLayout/a Landroid/widget/TextView;
aload 0
aload 0
getstatic android/support/design/i/snackbar_action I
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield android/support/design/widget/Snackbar$SnackbarLayout/b Landroid/widget/Button;
return
.limit locals 1
.limit stack 3
.end method

.method protected onLayout(ZIIII)V
aload 0
iload 1
iload 2
iload 3
iload 4
iload 5
invokespecial android/widget/LinearLayout/onLayout(ZIIII)V
iload 1
ifeq L0
aload 0
getfield android/support/design/widget/Snackbar$SnackbarLayout/e Landroid/support/design/widget/bg;
ifnull L0
aload 0
getfield android/support/design/widget/Snackbar$SnackbarLayout/e Landroid/support/design/widget/bg;
invokeinterface android/support/design/widget/bg/a()V 0
L0:
return
.limit locals 6
.limit stack 6
.end method

.method protected onMeasure(II)V
aload 0
iload 1
iload 2
invokespecial android/widget/LinearLayout/onMeasure(II)V
iload 1
istore 3
aload 0
getfield android/support/design/widget/Snackbar$SnackbarLayout/c I
ifle L0
iload 1
istore 3
aload 0
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/getMeasuredWidth()I
aload 0
getfield android/support/design/widget/Snackbar$SnackbarLayout/c I
if_icmple L0
aload 0
getfield android/support/design/widget/Snackbar$SnackbarLayout/c I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 3
aload 0
iload 3
iload 2
invokespecial android/widget/LinearLayout/onMeasure(II)V
L0:
aload 0
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/getResources()Landroid/content/res/Resources;
getstatic android/support/design/g/design_snackbar_padding_vertical_2lines I
invokevirtual android/content/res/Resources/getDimensionPixelSize(I)I
istore 4
aload 0
invokevirtual android/support/design/widget/Snackbar$SnackbarLayout/getResources()Landroid/content/res/Resources;
getstatic android/support/design/g/design_snackbar_padding_vertical I
invokevirtual android/content/res/Resources/getDimensionPixelSize(I)I
istore 5
aload 0
getfield android/support/design/widget/Snackbar$SnackbarLayout/a Landroid/widget/TextView;
invokevirtual android/widget/TextView/getLayout()Landroid/text/Layout;
invokevirtual android/text/Layout/getLineCount()I
iconst_1
if_icmple L1
iconst_1
istore 1
L2:
iload 1
ifeq L3
aload 0
getfield android/support/design/widget/Snackbar$SnackbarLayout/d I
ifle L3
aload 0
getfield android/support/design/widget/Snackbar$SnackbarLayout/b Landroid/widget/Button;
invokevirtual android/widget/Button/getMeasuredWidth()I
aload 0
getfield android/support/design/widget/Snackbar$SnackbarLayout/d I
if_icmple L3
aload 0
iconst_1
iload 4
iload 4
iload 5
isub
invokespecial android/support/design/widget/Snackbar$SnackbarLayout/a(III)Z
ifeq L4
iconst_1
istore 1
L5:
iload 1
ifeq L6
aload 0
iload 3
iload 2
invokespecial android/widget/LinearLayout/onMeasure(II)V
L6:
return
L1:
iconst_0
istore 1
goto L2
L3:
iload 1
ifeq L7
iload 4
istore 1
L8:
aload 0
iconst_0
iload 1
iload 1
invokespecial android/support/design/widget/Snackbar$SnackbarLayout/a(III)Z
ifeq L4
iconst_1
istore 1
goto L5
L7:
iload 5
istore 1
goto L8
L4:
iconst_0
istore 1
goto L5
.limit locals 6
.limit stack 5
.end method

.method setOnLayoutChangeListener(Landroid/support/design/widget/bg;)V
aload 0
aload 1
putfield android/support/design/widget/Snackbar$SnackbarLayout/e Landroid/support/design/widget/bg;
return
.limit locals 2
.limit stack 2
.end method
