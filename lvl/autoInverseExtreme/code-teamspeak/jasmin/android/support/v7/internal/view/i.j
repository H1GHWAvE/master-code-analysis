.bytecode 50.0
.class public final synchronized android/support/v7/internal/view/i
.super java/lang/Object

.field public final 'a' Ljava/util/ArrayList;

.field 'b' Landroid/support/v4/view/gd;

.field 'c' Z

.field private 'd' J

.field private 'e' Landroid/view/animation/Interpolator;

.field private final 'f' Landroid/support/v4/view/ge;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
ldc2_w -1L
putfield android/support/v7/internal/view/i/d J
aload 0
new android/support/v7/internal/view/j
dup
aload 0
invokespecial android/support/v7/internal/view/j/<init>(Landroid/support/v7/internal/view/i;)V
putfield android/support/v7/internal/view/i/f Landroid/support/v4/view/ge;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v7/internal/view/i/a Ljava/util/ArrayList;
return
.limit locals 1
.limit stack 4
.end method

.method private static synthetic a(Landroid/support/v7/internal/view/i;)Landroid/support/v4/view/gd;
aload 0
getfield android/support/v7/internal/view/i/b Landroid/support/v4/view/gd;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Landroid/support/v4/view/fk;Landroid/support/v4/view/fk;)Landroid/support/v7/internal/view/i;
aload 0
getfield android/support/v7/internal/view/i/a Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 1
getfield android/support/v4/view/fk/a Ljava/lang/ref/WeakReference;
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/view/View
astore 1
aload 1
ifnull L0
getstatic android/support/v4/view/fk/c Landroid/support/v4/view/fu;
aload 1
invokeinterface android/support/v4/view/fu/a(Landroid/view/View;)J 1
lstore 3
L1:
aload 2
lload 3
invokevirtual android/support/v4/view/fk/b(J)Landroid/support/v4/view/fk;
pop
aload 0
getfield android/support/v7/internal/view/i/a Ljava/util/ArrayList;
aload 2
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 0
areturn
L0:
lconst_0
lstore 3
goto L1
.limit locals 5
.limit stack 3
.end method

.method private static synthetic b(Landroid/support/v7/internal/view/i;)V
aload 0
iconst_0
putfield android/support/v7/internal/view/i/c Z
return
.limit locals 1
.limit stack 2
.end method

.method private static synthetic c(Landroid/support/v7/internal/view/i;)Ljava/util/ArrayList;
aload 0
getfield android/support/v7/internal/view/i/a Ljava/util/ArrayList;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d()V
aload 0
iconst_0
putfield android/support/v7/internal/view/i/c Z
return
.limit locals 1
.limit stack 2
.end method

.method public final a(Landroid/support/v4/view/fk;)Landroid/support/v7/internal/view/i;
aload 0
getfield android/support/v7/internal/view/i/c Z
ifne L0
aload 0
getfield android/support/v7/internal/view/i/a Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L0:
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/support/v4/view/gd;)Landroid/support/v7/internal/view/i;
aload 0
getfield android/support/v7/internal/view/i/c Z
ifne L0
aload 0
aload 1
putfield android/support/v7/internal/view/i/b Landroid/support/v4/view/gd;
L0:
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/view/animation/Interpolator;)Landroid/support/v7/internal/view/i;
aload 0
getfield android/support/v7/internal/view/i/c Z
ifne L0
aload 0
aload 1
putfield android/support/v7/internal/view/i/e Landroid/view/animation/Interpolator;
L0:
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a()V
aload 0
getfield android/support/v7/internal/view/i/c Z
ifeq L0
return
L0:
aload 0
getfield android/support/v7/internal/view/i/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 1
L1:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/support/v4/view/fk
astore 2
aload 0
getfield android/support/v7/internal/view/i/d J
lconst_0
lcmp
iflt L3
aload 2
aload 0
getfield android/support/v7/internal/view/i/d J
invokevirtual android/support/v4/view/fk/a(J)Landroid/support/v4/view/fk;
pop
L3:
aload 0
getfield android/support/v7/internal/view/i/e Landroid/view/animation/Interpolator;
ifnull L4
aload 2
aload 0
getfield android/support/v7/internal/view/i/e Landroid/view/animation/Interpolator;
invokevirtual android/support/v4/view/fk/a(Landroid/view/animation/Interpolator;)Landroid/support/v4/view/fk;
pop
L4:
aload 0
getfield android/support/v7/internal/view/i/b Landroid/support/v4/view/gd;
ifnull L5
aload 2
aload 0
getfield android/support/v7/internal/view/i/f Landroid/support/v4/view/ge;
invokevirtual android/support/v4/view/fk/a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;
pop
L5:
aload 2
invokevirtual android/support/v4/view/fk/b()V
goto L1
L2:
aload 0
iconst_1
putfield android/support/v7/internal/view/i/c Z
return
.limit locals 3
.limit stack 4
.end method

.method public final b()V
aload 0
getfield android/support/v7/internal/view/i/c Z
ifne L0
return
L0:
aload 0
getfield android/support/v7/internal/view/i/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 1
L1:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/support/v4/view/fk
invokevirtual android/support/v4/view/fk/a()V
goto L1
L2:
aload 0
iconst_0
putfield android/support/v7/internal/view/i/c Z
return
.limit locals 2
.limit stack 2
.end method

.method public final c()Landroid/support/v7/internal/view/i;
aload 0
getfield android/support/v7/internal/view/i/c Z
ifne L0
aload 0
ldc2_w 250L
putfield android/support/v7/internal/view/i/d J
L0:
aload 0
areturn
.limit locals 1
.limit stack 3
.end method
