.bytecode 50.0
.class synchronized abstract android/support/v7/internal/view/menu/e
.super android/support/v7/internal/view/menu/f

.field final 'a' Landroid/content/Context;

.field 'b' Ljava/util/Map;

.field 'c' Ljava/util/Map;

.method <init>(Landroid/content/Context;Ljava/lang/Object;)V
aload 0
aload 2
invokespecial android/support/v7/internal/view/menu/f/<init>(Ljava/lang/Object;)V
aload 0
aload 1
putfield android/support/v7/internal/view/menu/e/a Landroid/content/Context;
return
.limit locals 3
.limit stack 2
.end method

.method private a(I)V
aload 0
getfield android/support/v7/internal/view/menu/e/b Ljava/util/Map;
ifnonnull L0
L1:
return
L0:
aload 0
getfield android/support/v7/internal/view/menu/e/b Ljava/util/Map;
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 2
L2:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
iload 1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/view/MenuItem
invokeinterface android/view/MenuItem/getGroupId()I 0
if_icmpne L2
aload 2
invokeinterface java/util/Iterator/remove()V 0
goto L2
.limit locals 3
.limit stack 2
.end method

.method private b()V
aload 0
getfield android/support/v7/internal/view/menu/e/b Ljava/util/Map;
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/e/b Ljava/util/Map;
invokeinterface java/util/Map/clear()V 0
L0:
aload 0
getfield android/support/v7/internal/view/menu/e/c Ljava/util/Map;
ifnull L1
aload 0
getfield android/support/v7/internal/view/menu/e/c Ljava/util/Map;
invokeinterface java/util/Map/clear()V 0
L1:
return
.limit locals 1
.limit stack 1
.end method

.method private b(I)V
aload 0
getfield android/support/v7/internal/view/menu/e/b Ljava/util/Map;
ifnonnull L0
L1:
return
L0:
aload 0
getfield android/support/v7/internal/view/menu/e/b Ljava/util/Map;
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 2
L2:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
iload 1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/view/MenuItem
invokeinterface android/view/MenuItem/getItemId()I 0
if_icmpne L2
aload 2
invokeinterface java/util/Iterator/remove()V 0
return
.limit locals 3
.limit stack 2
.end method

.method final a(Landroid/view/MenuItem;)Landroid/view/MenuItem;
aload 1
instanceof android/support/v4/g/a/b
ifeq L0
aload 1
checkcast android/support/v4/g/a/b
astore 3
aload 0
getfield android/support/v7/internal/view/menu/e/b Ljava/util/Map;
ifnonnull L1
aload 0
new android/support/v4/n/a
dup
invokespecial android/support/v4/n/a/<init>()V
putfield android/support/v7/internal/view/menu/e/b Ljava/util/Map;
L1:
aload 0
getfield android/support/v7/internal/view/menu/e/b Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast android/view/MenuItem
astore 2
aload 2
astore 1
aload 2
ifnonnull L2
aload 0
getfield android/support/v7/internal/view/menu/e/a Landroid/content/Context;
aload 3
invokestatic android/support/v7/internal/view/menu/ab/a(Landroid/content/Context;Landroid/support/v4/g/a/b;)Landroid/view/MenuItem;
astore 1
aload 0
getfield android/support/v7/internal/view/menu/e/b Ljava/util/Map;
aload 3
aload 1
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L2:
aload 1
areturn
L0:
aload 1
areturn
.limit locals 4
.limit stack 3
.end method

.method final a(Landroid/view/SubMenu;)Landroid/view/SubMenu;
aload 1
instanceof android/support/v4/g/a/c
ifeq L0
aload 1
checkcast android/support/v4/g/a/c
astore 3
aload 0
getfield android/support/v7/internal/view/menu/e/c Ljava/util/Map;
ifnonnull L1
aload 0
new android/support/v4/n/a
dup
invokespecial android/support/v4/n/a/<init>()V
putfield android/support/v7/internal/view/menu/e/c Ljava/util/Map;
L1:
aload 0
getfield android/support/v7/internal/view/menu/e/c Ljava/util/Map;
aload 3
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast android/view/SubMenu
astore 2
aload 2
astore 1
aload 2
ifnonnull L2
aload 0
getfield android/support/v7/internal/view/menu/e/a Landroid/content/Context;
astore 1
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmplt L3
new android/support/v7/internal/view/menu/ae
dup
aload 1
aload 3
invokespecial android/support/v7/internal/view/menu/ae/<init>(Landroid/content/Context;Landroid/support/v4/g/a/c;)V
astore 1
aload 0
getfield android/support/v7/internal/view/menu/e/c Ljava/util/Map;
aload 3
aload 1
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L2:
aload 1
areturn
L3:
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
L0:
aload 1
areturn
.limit locals 4
.limit stack 4
.end method
