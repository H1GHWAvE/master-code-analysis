.bytecode 50.0
.class public final synchronized android/support/v7/internal/widget/ae
.super java/lang/Object

.field public static final 'a' Landroid/graphics/Rect;

.field private static final 'b' Ljava/lang/String; = "DrawableUtils"

.field private static 'c' Ljava/lang/Class;

.method static <clinit>()V
.catch java/lang/ClassNotFoundException from L0 to L1 using L2
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putstatic android/support/v7/internal/widget/ae/a Landroid/graphics/Rect;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 18
if_icmplt L1
L0:
ldc "android.graphics.Insets"
invokestatic java/lang/Class/forName(Ljava/lang/String;)Ljava/lang/Class;
putstatic android/support/v7/internal/widget/ae/c Ljava/lang/Class;
L1:
return
L2:
astore 0
return
.limit locals 1
.limit stack 2
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Rect;
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L2
.catch java/lang/Exception from L7 to L8 using L2
.catch java/lang/Exception from L8 to L9 using L2
.catch java/lang/Exception from L10 to L11 using L2
.catch java/lang/Exception from L12 to L13 using L2
.catch java/lang/Exception from L14 to L15 using L2
.catch java/lang/Exception from L16 to L17 using L2
.catch java/lang/Exception from L18 to L19 using L2
.catch java/lang/Exception from L20 to L21 using L2
.catch java/lang/Exception from L22 to L23 using L2
getstatic android/support/v7/internal/widget/ae/c Ljava/lang/Class;
ifnull L24
L0:
aload 0
invokestatic android/support/v4/e/a/a/d(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
astore 0
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
ldc "getOpticalInsets"
iconst_0
anewarray java/lang/Class
invokevirtual java/lang/Class/getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
aload 0
iconst_0
anewarray java/lang/Object
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
astore 5
L1:
aload 5
ifnull L24
L3:
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
astore 4
getstatic android/support/v7/internal/widget/ae/c Ljava/lang/Class;
invokevirtual java/lang/Class/getFields()[Ljava/lang/reflect/Field;
astore 6
aload 6
arraylength
istore 3
L4:
iconst_0
istore 2
L25:
aload 4
astore 0
iload 2
iload 3
if_icmpge L26
aload 6
iload 2
aaload
astore 0
L5:
aload 0
invokevirtual java/lang/reflect/Field/getName()Ljava/lang/String;
astore 7
L6:
iconst_m1
istore 1
L7:
aload 7
invokevirtual java/lang/String/hashCode()I
lookupswitch
-1383228885 : L14
115029 : L10
3317767 : L8
108511772 : L12
default : L27
L8:
aload 7
ldc "left"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L27
L9:
iconst_0
istore 1
goto L27
L10:
aload 7
ldc "top"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L27
L11:
iconst_1
istore 1
goto L27
L12:
aload 7
ldc "right"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L27
L13:
iconst_2
istore 1
goto L27
L14:
aload 7
ldc "bottom"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L27
L15:
iconst_3
istore 1
goto L27
L16:
aload 4
aload 0
aload 5
invokevirtual java/lang/reflect/Field/getInt(Ljava/lang/Object;)I
putfield android/graphics/Rect/left I
L17:
goto L28
L2:
astore 0
ldc "DrawableUtils"
ldc "Couldn't obtain the optical insets. Ignoring."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
L24:
getstatic android/support/v7/internal/widget/ae/a Landroid/graphics/Rect;
astore 0
L26:
aload 0
areturn
L18:
aload 4
aload 0
aload 5
invokevirtual java/lang/reflect/Field/getInt(Ljava/lang/Object;)I
putfield android/graphics/Rect/top I
L19:
goto L28
L20:
aload 4
aload 0
aload 5
invokevirtual java/lang/reflect/Field/getInt(Ljava/lang/Object;)I
putfield android/graphics/Rect/right I
L21:
goto L28
L22:
aload 4
aload 0
aload 5
invokevirtual java/lang/reflect/Field/getInt(Ljava/lang/Object;)I
putfield android/graphics/Rect/bottom I
L23:
goto L28
L27:
iload 1
tableswitch 0
L16
L18
L20
L22
default : L28
L28:
iload 2
iconst_1
iadd
istore 2
goto L25
.limit locals 8
.limit stack 3
.end method
