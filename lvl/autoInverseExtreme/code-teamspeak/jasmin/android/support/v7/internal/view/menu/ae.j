.bytecode 50.0
.class final synchronized android/support/v7/internal/view/menu/ae
.super android/support/v7/internal/view/menu/ac
.implements android/view/SubMenu

.method <init>(Landroid/content/Context;Landroid/support/v4/g/a/c;)V
aload 0
aload 1
aload 2
invokespecial android/support/v7/internal/view/menu/ac/<init>(Landroid/content/Context;Landroid/support/v4/g/a/a;)V
return
.limit locals 3
.limit stack 3
.end method

.method private b()Landroid/support/v4/g/a/c;
aload 0
getfield android/support/v7/internal/view/menu/ae/d Ljava/lang/Object;
checkcast android/support/v4/g/a/c
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic a()Ljava/lang/Object;
aload 0
getfield android/support/v7/internal/view/menu/ae/d Ljava/lang/Object;
checkcast android/support/v4/g/a/c
areturn
.limit locals 1
.limit stack 1
.end method

.method public final clearHeader()V
aload 0
getfield android/support/v7/internal/view/menu/ae/d Ljava/lang/Object;
checkcast android/support/v4/g/a/c
invokeinterface android/support/v4/g/a/c/clearHeader()V 0
return
.limit locals 1
.limit stack 1
.end method

.method public final getItem()Landroid/view/MenuItem;
aload 0
aload 0
getfield android/support/v7/internal/view/menu/ae/d Ljava/lang/Object;
checkcast android/support/v4/g/a/c
invokeinterface android/support/v4/g/a/c/getItem()Landroid/view/MenuItem; 0
invokevirtual android/support/v7/internal/view/menu/ae/a(Landroid/view/MenuItem;)Landroid/view/MenuItem;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final setHeaderIcon(I)Landroid/view/SubMenu;
aload 0
getfield android/support/v7/internal/view/menu/ae/d Ljava/lang/Object;
checkcast android/support/v4/g/a/c
iload 1
invokeinterface android/support/v4/g/a/c/setHeaderIcon(I)Landroid/view/SubMenu; 1
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
aload 0
getfield android/support/v7/internal/view/menu/ae/d Ljava/lang/Object;
checkcast android/support/v4/g/a/c
aload 1
invokeinterface android/support/v4/g/a/c/setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu; 1
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final setHeaderTitle(I)Landroid/view/SubMenu;
aload 0
getfield android/support/v7/internal/view/menu/ae/d Ljava/lang/Object;
checkcast android/support/v4/g/a/c
iload 1
invokeinterface android/support/v4/g/a/c/setHeaderTitle(I)Landroid/view/SubMenu; 1
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
aload 0
getfield android/support/v7/internal/view/menu/ae/d Ljava/lang/Object;
checkcast android/support/v4/g/a/c
aload 1
invokeinterface android/support/v4/g/a/c/setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu; 1
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final setHeaderView(Landroid/view/View;)Landroid/view/SubMenu;
aload 0
getfield android/support/v7/internal/view/menu/ae/d Ljava/lang/Object;
checkcast android/support/v4/g/a/c
aload 1
invokeinterface android/support/v4/g/a/c/setHeaderView(Landroid/view/View;)Landroid/view/SubMenu; 1
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final setIcon(I)Landroid/view/SubMenu;
aload 0
getfield android/support/v7/internal/view/menu/ae/d Ljava/lang/Object;
checkcast android/support/v4/g/a/c
iload 1
invokeinterface android/support/v4/g/a/c/setIcon(I)Landroid/view/SubMenu; 1
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
aload 0
getfield android/support/v7/internal/view/menu/ae/d Ljava/lang/Object;
checkcast android/support/v4/g/a/c
aload 1
invokeinterface android/support/v4/g/a/c/setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu; 1
pop
aload 0
areturn
.limit locals 2
.limit stack 2
.end method
