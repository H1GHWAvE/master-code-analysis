.bytecode 50.0
.class public synchronized android/support/v7/internal/widget/l
.super android/database/DataSetObservable

.field public static final 'a' Ljava/lang/String; = "activity_choser_model_history.xml"

.field public static final 'b' I = 50


.field private static final 'h' Z = 0


.field private static final 'i' Ljava/lang/String;

.field private static final 'j' Ljava/lang/String; = "historical-records"

.field private static final 'k' Ljava/lang/String; = "historical-record"

.field private static final 'l' Ljava/lang/String; = "activity"

.field private static final 'm' Ljava/lang/String; = "time"

.field private static final 'n' Ljava/lang/String; = "weight"

.field private static final 'o' I = 5


.field private static final 'p' F = 1.0F


.field private static final 'q' Ljava/lang/String; = ".xml"

.field private static final 'r' I = -1


.field private static final 's' Ljava/lang/Object;

.field private static final 't' Ljava/util/Map;

.field private 'A' Z

.field private 'B' Z

.field public final 'c' Ljava/lang/Object;

.field final 'd' Ljava/util/List;

.field public 'e' Landroid/content/Intent;

.field public 'f' Z

.field public 'g' Landroid/support/v7/internal/widget/s;

.field private final 'u' Ljava/util/List;

.field private final 'v' Landroid/content/Context;

.field private final 'w' Ljava/lang/String;

.field private 'x' Landroid/support/v7/internal/widget/p;

.field private 'y' I

.field private 'z' Z

.method static <clinit>()V
ldc android/support/v7/internal/widget/l
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
putstatic android/support/v7/internal/widget/l/i Ljava/lang/String;
new java/lang/Object
dup
invokespecial java/lang/Object/<init>()V
putstatic android/support/v7/internal/widget/l/s Ljava/lang/Object;
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putstatic android/support/v7/internal/widget/l/t Ljava/util/Map;
return
.limit locals 0
.limit stack 2
.end method

.method private <init>(Landroid/content/Context;Ljava/lang/String;)V
aload 0
invokespecial android/database/DataSetObservable/<init>()V
aload 0
new java/lang/Object
dup
invokespecial java/lang/Object/<init>()V
putfield android/support/v7/internal/widget/l/c Ljava/lang/Object;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v7/internal/widget/l/d Ljava/util/List;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v7/internal/widget/l/u Ljava/util/List;
aload 0
new android/support/v7/internal/widget/q
dup
aload 0
iconst_0
invokespecial android/support/v7/internal/widget/q/<init>(Landroid/support/v7/internal/widget/l;B)V
putfield android/support/v7/internal/widget/l/x Landroid/support/v7/internal/widget/p;
aload 0
bipush 50
putfield android/support/v7/internal/widget/l/y I
aload 0
iconst_1
putfield android/support/v7/internal/widget/l/z Z
aload 0
iconst_0
putfield android/support/v7/internal/widget/l/A Z
aload 0
iconst_1
putfield android/support/v7/internal/widget/l/B Z
aload 0
iconst_0
putfield android/support/v7/internal/widget/l/f Z
aload 0
aload 1
invokevirtual android/content/Context/getApplicationContext()Landroid/content/Context;
putfield android/support/v7/internal/widget/l/v Landroid/content/Context;
aload 2
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L0
aload 2
ldc ".xml"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifne L0
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".xml"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
putfield android/support/v7/internal/widget/l/w Ljava/lang/String;
return
L0:
aload 0
aload 2
putfield android/support/v7/internal/widget/l/w Ljava/lang/String;
return
.limit locals 3
.limit stack 5
.end method

.method static synthetic a(Landroid/support/v7/internal/widget/l;)Landroid/content/Context;
aload 0
getfield android/support/v7/internal/widget/l/v Landroid/content/Context;
areturn
.limit locals 1
.limit stack 1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v7/internal/widget/l;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
getstatic android/support/v7/internal/widget/l/s Ljava/lang/Object;
astore 4
aload 4
monitorenter
L0:
getstatic android/support/v7/internal/widget/l/t Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast android/support/v7/internal/widget/l
astore 3
L1:
aload 3
astore 2
aload 3
ifnonnull L4
L3:
new android/support/v7/internal/widget/l
dup
aload 0
aload 1
invokespecial android/support/v7/internal/widget/l/<init>(Landroid/content/Context;Ljava/lang/String;)V
astore 2
getstatic android/support/v7/internal/widget/l/t Ljava/util/Map;
aload 1
aload 2
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L4:
aload 4
monitorexit
L5:
aload 2
areturn
L2:
astore 0
L6:
aload 4
monitorexit
L7:
aload 0
athrow
.limit locals 5
.limit stack 4
.end method

.method private a(Landroid/content/Intent;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
aload 0
getfield android/support/v7/internal/widget/l/c Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
getfield android/support/v7/internal/widget/l/e Landroid/content/Intent;
aload 1
if_acmpne L3
aload 2
monitorexit
L1:
return
L3:
aload 0
aload 1
putfield android/support/v7/internal/widget/l/e Landroid/content/Intent;
aload 0
iconst_1
putfield android/support/v7/internal/widget/l/f Z
aload 0
invokevirtual android/support/v7/internal/widget/l/d()V
aload 2
monitorexit
L4:
return
L2:
astore 1
L5:
aload 2
monitorexit
L6:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method private a(Landroid/support/v7/internal/widget/p;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
aload 0
getfield android/support/v7/internal/widget/l/c Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
getfield android/support/v7/internal/widget/l/x Landroid/support/v7/internal/widget/p;
aload 1
if_acmpne L3
aload 2
monitorexit
L1:
return
L3:
aload 0
aload 1
putfield android/support/v7/internal/widget/l/x Landroid/support/v7/internal/widget/p;
aload 0
invokespecial android/support/v7/internal/widget/l/i()Z
ifeq L4
aload 0
invokevirtual android/support/v7/internal/widget/l/notifyChanged()V
L4:
aload 2
monitorexit
L5:
return
L2:
astore 1
L6:
aload 2
monitorexit
L7:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method private a(Landroid/support/v7/internal/widget/s;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield android/support/v7/internal/widget/l/c Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
aload 1
putfield android/support/v7/internal/widget/l/g Landroid/support/v7/internal/widget/s;
aload 2
monitorexit
L1:
return
L2:
astore 1
L3:
aload 2
monitorexit
L4:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method static synthetic b(Landroid/support/v7/internal/widget/l;)Ljava/lang/String;
aload 0
getfield android/support/v7/internal/widget/l/w Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c(I)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
aload 0
getfield android/support/v7/internal/widget/l/c Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokevirtual android/support/v7/internal/widget/l/d()V
aload 0
getfield android/support/v7/internal/widget/l/d Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/support/v7/internal/widget/o
astore 4
aload 0
getfield android/support/v7/internal/widget/l/d Ljava/util/List;
iconst_0
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/support/v7/internal/widget/o
astore 5
L1:
aload 5
ifnull L8
L3:
aload 5
getfield android/support/v7/internal/widget/o/b F
aload 4
getfield android/support/v7/internal/widget/o/b F
fsub
ldc_w 5.0F
fadd
fstore 2
L4:
aload 0
new android/support/v7/internal/widget/r
dup
new android/content/ComponentName
dup
aload 4
getfield android/support/v7/internal/widget/o/a Landroid/content/pm/ResolveInfo;
getfield android/content/pm/ResolveInfo/activityInfo Landroid/content/pm/ActivityInfo;
getfield android/content/pm/ActivityInfo/packageName Ljava/lang/String;
aload 4
getfield android/support/v7/internal/widget/o/a Landroid/content/pm/ResolveInfo;
getfield android/content/pm/ResolveInfo/activityInfo Landroid/content/pm/ActivityInfo;
getfield android/content/pm/ActivityInfo/name Ljava/lang/String;
invokespecial android/content/ComponentName/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokestatic java/lang/System/currentTimeMillis()J
fload 2
invokespecial android/support/v7/internal/widget/r/<init>(Landroid/content/ComponentName;JF)V
invokevirtual android/support/v7/internal/widget/l/a(Landroid/support/v7/internal/widget/r;)Z
pop
aload 3
monitorexit
L5:
return
L2:
astore 4
L6:
aload 3
monitorexit
L7:
aload 4
athrow
L8:
fconst_1
fstore 2
goto L4
.limit locals 6
.limit stack 7
.end method

.method static synthetic c(Landroid/support/v7/internal/widget/l;)Z
aload 0
iconst_1
putfield android/support/v7/internal/widget/l/z Z
iconst_1
ireturn
.limit locals 1
.limit stack 2
.end method

.method private d(I)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
aload 0
getfield android/support/v7/internal/widget/l/c Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
getfield android/support/v7/internal/widget/l/y I
iload 1
if_icmpne L3
aload 2
monitorexit
L1:
return
L3:
aload 0
iload 1
putfield android/support/v7/internal/widget/l/y I
aload 0
invokespecial android/support/v7/internal/widget/l/l()V
aload 0
invokespecial android/support/v7/internal/widget/l/i()Z
ifeq L4
aload 0
invokevirtual android/support/v7/internal/widget/l/notifyChanged()V
L4:
aload 2
monitorexit
L5:
return
L2:
astore 3
L6:
aload 2
monitorexit
L7:
aload 3
athrow
.limit locals 4
.limit stack 2
.end method

.method static synthetic e()Ljava/lang/String;
getstatic android/support/v7/internal/widget/l/i Ljava/lang/String;
areturn
.limit locals 0
.limit stack 1
.end method

.method private f()Landroid/content/Intent;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield android/support/v7/internal/widget/l/c Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
getfield android/support/v7/internal/widget/l/e Landroid/content/Intent;
astore 2
aload 1
monitorexit
L1:
aload 2
areturn
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 1
.end method

.method private g()V
aload 0
getfield android/support/v7/internal/widget/l/A Z
ifne L0
new java/lang/IllegalStateException
dup
ldc "No preceding call to #readHistoricalData"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v7/internal/widget/l/B Z
ifne L1
L2:
return
L1:
aload 0
iconst_0
putfield android/support/v7/internal/widget/l/B Z
aload 0
getfield android/support/v7/internal/widget/l/w Ljava/lang/String;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L2
new android/support/v7/internal/widget/t
dup
aload 0
iconst_0
invokespecial android/support/v7/internal/widget/t/<init>(Landroid/support/v7/internal/widget/l;B)V
astore 1
iconst_2
anewarray java/lang/Object
astore 2
aload 2
iconst_0
new java/util/ArrayList
dup
aload 0
getfield android/support/v7/internal/widget/l/u Ljava/util/List;
invokespecial java/util/ArrayList/<init>(Ljava/util/Collection;)V
aastore
aload 2
iconst_1
aload 0
getfield android/support/v7/internal/widget/l/w Ljava/lang/String;
aastore
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L3
aload 1
getstatic android/os/AsyncTask/THREAD_POOL_EXECUTOR Ljava/util/concurrent/Executor;
aload 2
invokevirtual android/os/AsyncTask/executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;
pop
return
L3:
aload 1
aload 2
invokevirtual android/os/AsyncTask/execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
pop
return
.limit locals 3
.limit stack 5
.end method

.method private h()I
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield android/support/v7/internal/widget/l/c Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
getfield android/support/v7/internal/widget/l/y I
istore 1
aload 2
monitorexit
L1:
iload 1
ireturn
L2:
astore 3
L3:
aload 2
monitorexit
L4:
aload 3
athrow
.limit locals 4
.limit stack 1
.end method

.method private i()Z
aload 0
getfield android/support/v7/internal/widget/l/x Landroid/support/v7/internal/widget/p;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/l/e Landroid/content/Intent;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/l/d Ljava/util/List;
invokeinterface java/util/List/isEmpty()Z 0
ifne L0
aload 0
getfield android/support/v7/internal/widget/l/u Ljava/util/List;
invokeinterface java/util/List/isEmpty()Z 0
ifne L0
aload 0
getfield android/support/v7/internal/widget/l/x Landroid/support/v7/internal/widget/p;
aload 0
getfield android/support/v7/internal/widget/l/d Ljava/util/List;
aload 0
getfield android/support/v7/internal/widget/l/u Ljava/util/List;
invokestatic java/util/Collections/unmodifiableList(Ljava/util/List;)Ljava/util/List;
invokeinterface android/support/v7/internal/widget/p/a(Ljava/util/List;Ljava/util/List;)V 2
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 3
.end method

.method private j()Z
iconst_0
istore 4
iload 4
istore 3
aload 0
getfield android/support/v7/internal/widget/l/f Z
ifeq L0
iload 4
istore 3
aload 0
getfield android/support/v7/internal/widget/l/e Landroid/content/Intent;
ifnull L0
aload 0
iconst_0
putfield android/support/v7/internal/widget/l/f Z
aload 0
getfield android/support/v7/internal/widget/l/d Ljava/util/List;
invokeinterface java/util/List/clear()V 0
aload 0
getfield android/support/v7/internal/widget/l/v Landroid/content/Context;
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
aload 0
getfield android/support/v7/internal/widget/l/e Landroid/content/Intent;
iconst_0
invokevirtual android/content/pm/PackageManager/queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;
astore 5
aload 5
invokeinterface java/util/List/size()I 0
istore 2
iconst_0
istore 1
L1:
iload 1
iload 2
if_icmpge L2
aload 5
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/content/pm/ResolveInfo
astore 6
aload 0
getfield android/support/v7/internal/widget/l/d Ljava/util/List;
new android/support/v7/internal/widget/o
dup
aload 0
aload 6
invokespecial android/support/v7/internal/widget/o/<init>(Landroid/support/v7/internal/widget/l;Landroid/content/pm/ResolveInfo;)V
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
iconst_1
istore 3
L0:
iload 3
ireturn
.limit locals 7
.limit stack 5
.end method

.method private k()Z
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch org/xmlpull/v1/XmlPullParserException from L1 to L3 using L4
.catch java/io/IOException from L1 to L3 using L5
.catch all from L1 to L3 using L6
.catch org/xmlpull/v1/XmlPullParserException from L7 to L8 using L4
.catch java/io/IOException from L7 to L8 using L5
.catch all from L7 to L8 using L6
.catch org/xmlpull/v1/XmlPullParserException from L9 to L4 using L4
.catch java/io/IOException from L9 to L4 using L5
.catch all from L9 to L4 using L6
.catch all from L10 to L11 using L6
.catch java/io/IOException from L12 to L13 using L14
.catch org/xmlpull/v1/XmlPullParserException from L15 to L16 using L4
.catch java/io/IOException from L15 to L16 using L5
.catch all from L15 to L16 using L6
.catch org/xmlpull/v1/XmlPullParserException from L16 to L17 using L4
.catch java/io/IOException from L16 to L17 using L5
.catch all from L16 to L17 using L6
.catch org/xmlpull/v1/XmlPullParserException from L18 to L5 using L4
.catch java/io/IOException from L18 to L5 using L5
.catch all from L18 to L5 using L6
.catch all from L19 to L20 using L6
.catch java/io/IOException from L21 to L22 using L23
.catch org/xmlpull/v1/XmlPullParserException from L24 to L25 using L4
.catch java/io/IOException from L24 to L25 using L5
.catch all from L24 to L25 using L6
.catch java/io/IOException from L26 to L27 using L28
.catch java/io/IOException from L29 to L30 using L31
iconst_0
istore 1
aload 0
getfield android/support/v7/internal/widget/l/z Z
ifeq L32
aload 0
getfield android/support/v7/internal/widget/l/B Z
ifeq L32
aload 0
getfield android/support/v7/internal/widget/l/w Ljava/lang/String;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L32
aload 0
iconst_0
putfield android/support/v7/internal/widget/l/z Z
aload 0
iconst_1
putfield android/support/v7/internal/widget/l/A Z
L0:
aload 0
getfield android/support/v7/internal/widget/l/v Landroid/content/Context;
aload 0
getfield android/support/v7/internal/widget/l/w Ljava/lang/String;
invokevirtual android/content/Context/openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;
astore 2
L1:
invokestatic android/util/Xml/newPullParser()Lorg/xmlpull/v1/XmlPullParser;
astore 3
aload 3
aload 2
ldc "UTF-8"
invokeinterface org/xmlpull/v1/XmlPullParser/setInput(Ljava/io/InputStream;Ljava/lang/String;)V 2
L3:
iload 1
iconst_1
if_icmpeq L9
iload 1
iconst_2
if_icmpeq L9
L7:
aload 3
invokeinterface org/xmlpull/v1/XmlPullParser/next()I 0
istore 1
L8:
goto L3
L9:
ldc "historical-records"
aload 3
invokeinterface org/xmlpull/v1/XmlPullParser/getName()Ljava/lang/String; 0
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L15
new org/xmlpull/v1/XmlPullParserException
dup
ldc "Share records file does not start with historical-records tag."
invokespecial org/xmlpull/v1/XmlPullParserException/<init>(Ljava/lang/String;)V
athrow
L4:
astore 3
L10:
getstatic android/support/v7/internal/widget/l/i Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "Error reading historical recrod file: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield android/support/v7/internal/widget/l/w Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 3
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
L11:
aload 2
ifnull L13
L12:
aload 2
invokevirtual java/io/FileInputStream/close()V
L13:
iconst_1
ireturn
L15:
aload 0
getfield android/support/v7/internal/widget/l/u Ljava/util/List;
astore 4
aload 4
invokeinterface java/util/List/clear()V 0
L16:
aload 3
invokeinterface org/xmlpull/v1/XmlPullParser/next()I 0
istore 1
L17:
iload 1
iconst_1
if_icmpeq L33
iload 1
iconst_3
if_icmpeq L16
iload 1
iconst_4
if_icmpeq L16
L18:
ldc "historical-record"
aload 3
invokeinterface org/xmlpull/v1/XmlPullParser/getName()Ljava/lang/String; 0
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L24
new org/xmlpull/v1/XmlPullParserException
dup
ldc "Share records file not well-formed."
invokespecial org/xmlpull/v1/XmlPullParserException/<init>(Ljava/lang/String;)V
athrow
L5:
astore 3
L19:
getstatic android/support/v7/internal/widget/l/i Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "Error reading historical recrod file: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield android/support/v7/internal/widget/l/w Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 3
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
L20:
aload 2
ifnull L13
L21:
aload 2
invokevirtual java/io/FileInputStream/close()V
L22:
iconst_1
ireturn
L23:
astore 2
iconst_1
ireturn
L24:
aload 4
new android/support/v7/internal/widget/r
dup
aload 3
aconst_null
ldc "activity"
invokeinterface org/xmlpull/v1/XmlPullParser/getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
aload 3
aconst_null
ldc "time"
invokeinterface org/xmlpull/v1/XmlPullParser/getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
aload 3
aconst_null
ldc "weight"
invokeinterface org/xmlpull/v1/XmlPullParser/getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
invokestatic java/lang/Float/parseFloat(Ljava/lang/String;)F
invokespecial android/support/v7/internal/widget/r/<init>(Ljava/lang/String;JF)V
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L25:
goto L16
L6:
astore 3
aload 2
ifnull L27
L26:
aload 2
invokevirtual java/io/FileInputStream/close()V
L27:
aload 3
athrow
L33:
aload 2
ifnull L13
L29:
aload 2
invokevirtual java/io/FileInputStream/close()V
L30:
iconst_1
ireturn
L31:
astore 2
iconst_1
ireturn
L32:
iconst_0
ireturn
L14:
astore 2
iconst_1
ireturn
L28:
astore 2
goto L27
L2:
astore 2
iconst_1
ireturn
.limit locals 5
.limit stack 9
.end method

.method private l()V
aload 0
getfield android/support/v7/internal/widget/l/u Ljava/util/List;
invokeinterface java/util/List/size()I 0
aload 0
getfield android/support/v7/internal/widget/l/y I
isub
istore 2
iload 2
ifgt L0
L1:
return
L0:
aload 0
iconst_1
putfield android/support/v7/internal/widget/l/B Z
iconst_0
istore 1
L2:
iload 1
iload 2
if_icmpge L1
aload 0
getfield android/support/v7/internal/widget/l/u Ljava/util/List;
iconst_0
invokeinterface java/util/List/remove(I)Ljava/lang/Object; 1
pop
iload 1
iconst_1
iadd
istore 1
goto L2
.limit locals 3
.limit stack 2
.end method

.method private m()V
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch org/xmlpull/v1/XmlPullParserException from L1 to L3 using L4
.catch java/io/IOException from L1 to L3 using L5
.catch all from L1 to L3 using L6
.catch org/xmlpull/v1/XmlPullParserException from L7 to L8 using L4
.catch java/io/IOException from L7 to L8 using L5
.catch all from L7 to L8 using L6
.catch org/xmlpull/v1/XmlPullParserException from L9 to L4 using L4
.catch java/io/IOException from L9 to L4 using L5
.catch all from L9 to L4 using L6
.catch all from L10 to L11 using L6
.catch java/io/IOException from L12 to L13 using L14
.catch org/xmlpull/v1/XmlPullParserException from L15 to L16 using L4
.catch java/io/IOException from L15 to L16 using L5
.catch all from L15 to L16 using L6
.catch org/xmlpull/v1/XmlPullParserException from L16 to L17 using L4
.catch java/io/IOException from L16 to L17 using L5
.catch all from L16 to L17 using L6
.catch org/xmlpull/v1/XmlPullParserException from L18 to L5 using L4
.catch java/io/IOException from L18 to L5 using L5
.catch all from L18 to L5 using L6
.catch all from L19 to L20 using L6
.catch java/io/IOException from L21 to L22 using L23
.catch org/xmlpull/v1/XmlPullParserException from L24 to L25 using L4
.catch java/io/IOException from L24 to L25 using L5
.catch all from L24 to L25 using L6
.catch java/io/IOException from L26 to L27 using L28
.catch java/io/IOException from L29 to L30 using L31
L0:
aload 0
getfield android/support/v7/internal/widget/l/v Landroid/content/Context;
aload 0
getfield android/support/v7/internal/widget/l/w Ljava/lang/String;
invokevirtual android/content/Context/openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;
astore 2
L1:
invokestatic android/util/Xml/newPullParser()Lorg/xmlpull/v1/XmlPullParser;
astore 3
aload 3
aload 2
ldc "UTF-8"
invokeinterface org/xmlpull/v1/XmlPullParser/setInput(Ljava/io/InputStream;Ljava/lang/String;)V 2
L3:
iconst_0
istore 1
L32:
iload 1
iconst_1
if_icmpeq L9
iload 1
iconst_2
if_icmpeq L9
L7:
aload 3
invokeinterface org/xmlpull/v1/XmlPullParser/next()I 0
istore 1
L8:
goto L32
L9:
ldc "historical-records"
aload 3
invokeinterface org/xmlpull/v1/XmlPullParser/getName()Ljava/lang/String; 0
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L15
new org/xmlpull/v1/XmlPullParserException
dup
ldc "Share records file does not start with historical-records tag."
invokespecial org/xmlpull/v1/XmlPullParserException/<init>(Ljava/lang/String;)V
athrow
L4:
astore 3
L10:
getstatic android/support/v7/internal/widget/l/i Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "Error reading historical recrod file: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield android/support/v7/internal/widget/l/w Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 3
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
L11:
aload 2
ifnull L13
L12:
aload 2
invokevirtual java/io/FileInputStream/close()V
L13:
return
L15:
aload 0
getfield android/support/v7/internal/widget/l/u Ljava/util/List;
astore 4
aload 4
invokeinterface java/util/List/clear()V 0
L16:
aload 3
invokeinterface org/xmlpull/v1/XmlPullParser/next()I 0
istore 1
L17:
iload 1
iconst_1
if_icmpeq L33
iload 1
iconst_3
if_icmpeq L16
iload 1
iconst_4
if_icmpeq L16
L18:
ldc "historical-record"
aload 3
invokeinterface org/xmlpull/v1/XmlPullParser/getName()Ljava/lang/String; 0
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L24
new org/xmlpull/v1/XmlPullParserException
dup
ldc "Share records file not well-formed."
invokespecial org/xmlpull/v1/XmlPullParserException/<init>(Ljava/lang/String;)V
athrow
L5:
astore 3
L19:
getstatic android/support/v7/internal/widget/l/i Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "Error reading historical recrod file: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield android/support/v7/internal/widget/l/w Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 3
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
L20:
aload 2
ifnull L13
L21:
aload 2
invokevirtual java/io/FileInputStream/close()V
L22:
return
L23:
astore 2
return
L24:
aload 4
new android/support/v7/internal/widget/r
dup
aload 3
aconst_null
ldc "activity"
invokeinterface org/xmlpull/v1/XmlPullParser/getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
aload 3
aconst_null
ldc "time"
invokeinterface org/xmlpull/v1/XmlPullParser/getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
aload 3
aconst_null
ldc "weight"
invokeinterface org/xmlpull/v1/XmlPullParser/getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
invokestatic java/lang/Float/parseFloat(Ljava/lang/String;)F
invokespecial android/support/v7/internal/widget/r/<init>(Ljava/lang/String;JF)V
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L25:
goto L16
L6:
astore 3
aload 2
ifnull L27
L26:
aload 2
invokevirtual java/io/FileInputStream/close()V
L27:
aload 3
athrow
L33:
aload 2
ifnull L13
L29:
aload 2
invokevirtual java/io/FileInputStream/close()V
L30:
return
L31:
astore 2
return
L14:
astore 2
return
L28:
astore 2
goto L27
L2:
astore 2
return
.limit locals 5
.limit stack 9
.end method

.method public final a()I
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield android/support/v7/internal/widget/l/c Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokevirtual android/support/v7/internal/widget/l/d()V
aload 0
getfield android/support/v7/internal/widget/l/d Ljava/util/List;
invokeinterface java/util/List/size()I 0
istore 1
aload 2
monitorexit
L1:
iload 1
ireturn
L2:
astore 3
L3:
aload 2
monitorexit
L4:
aload 3
athrow
.limit locals 4
.limit stack 1
.end method

.method public final a(Landroid/content/pm/ResolveInfo;)I
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
aload 0
getfield android/support/v7/internal/widget/l/c Ljava/lang/Object;
astore 4
aload 4
monitorenter
L0:
aload 0
invokevirtual android/support/v7/internal/widget/l/d()V
aload 0
getfield android/support/v7/internal/widget/l/d Ljava/util/List;
astore 5
aload 5
invokeinterface java/util/List/size()I 0
istore 3
L1:
iconst_0
istore 2
L9:
iload 2
iload 3
if_icmpge L5
L3:
aload 5
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/support/v7/internal/widget/o
getfield android/support/v7/internal/widget/o/a Landroid/content/pm/ResolveInfo;
aload 1
if_acmpne L10
aload 4
monitorexit
L4:
iload 2
ireturn
L5:
aload 4
monitorexit
L6:
iconst_m1
ireturn
L2:
astore 1
L7:
aload 4
monitorexit
L8:
aload 1
athrow
L10:
iload 2
iconst_1
iadd
istore 2
goto L9
.limit locals 6
.limit stack 2
.end method

.method public final a(I)Landroid/content/pm/ResolveInfo;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield android/support/v7/internal/widget/l/c Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokevirtual android/support/v7/internal/widget/l/d()V
aload 0
getfield android/support/v7/internal/widget/l/d Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/support/v7/internal/widget/o
getfield android/support/v7/internal/widget/o/a Landroid/content/pm/ResolveInfo;
astore 3
aload 2
monitorexit
L1:
aload 3
areturn
L2:
astore 3
L3:
aload 2
monitorexit
L4:
aload 3
athrow
.limit locals 4
.limit stack 2
.end method

.method final a(Landroid/support/v7/internal/widget/r;)Z
aload 0
getfield android/support/v7/internal/widget/l/u Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
istore 2
iload 2
ifeq L0
aload 0
iconst_1
putfield android/support/v7/internal/widget/l/B Z
aload 0
invokespecial android/support/v7/internal/widget/l/l()V
aload 0
getfield android/support/v7/internal/widget/l/A Z
ifne L1
new java/lang/IllegalStateException
dup
ldc "No preceding call to #readHistoricalData"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
getfield android/support/v7/internal/widget/l/B Z
ifeq L2
aload 0
iconst_0
putfield android/support/v7/internal/widget/l/B Z
aload 0
getfield android/support/v7/internal/widget/l/w Ljava/lang/String;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L2
new android/support/v7/internal/widget/t
dup
aload 0
iconst_0
invokespecial android/support/v7/internal/widget/t/<init>(Landroid/support/v7/internal/widget/l;B)V
astore 1
iconst_2
anewarray java/lang/Object
astore 3
aload 3
iconst_0
new java/util/ArrayList
dup
aload 0
getfield android/support/v7/internal/widget/l/u Ljava/util/List;
invokespecial java/util/ArrayList/<init>(Ljava/util/Collection;)V
aastore
aload 3
iconst_1
aload 0
getfield android/support/v7/internal/widget/l/w Ljava/lang/String;
aastore
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L3
aload 1
getstatic android/os/AsyncTask/THREAD_POOL_EXECUTOR Ljava/util/concurrent/Executor;
aload 3
invokevirtual android/os/AsyncTask/executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;
pop
L2:
aload 0
invokespecial android/support/v7/internal/widget/l/i()Z
pop
aload 0
invokevirtual android/support/v7/internal/widget/l/notifyChanged()V
L0:
iload 2
ireturn
L3:
aload 1
aload 3
invokevirtual android/os/AsyncTask/execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
pop
goto L2
.limit locals 4
.limit stack 5
.end method

.method public final b(I)Landroid/content/Intent;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
aload 0
getfield android/support/v7/internal/widget/l/c Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
getfield android/support/v7/internal/widget/l/e Landroid/content/Intent;
ifnonnull L3
aload 2
monitorexit
L1:
aconst_null
areturn
L3:
aload 0
invokevirtual android/support/v7/internal/widget/l/d()V
aload 0
getfield android/support/v7/internal/widget/l/d Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/support/v7/internal/widget/o
astore 3
new android/content/ComponentName
dup
aload 3
getfield android/support/v7/internal/widget/o/a Landroid/content/pm/ResolveInfo;
getfield android/content/pm/ResolveInfo/activityInfo Landroid/content/pm/ActivityInfo;
getfield android/content/pm/ActivityInfo/packageName Ljava/lang/String;
aload 3
getfield android/support/v7/internal/widget/o/a Landroid/content/pm/ResolveInfo;
getfield android/content/pm/ResolveInfo/activityInfo Landroid/content/pm/ActivityInfo;
getfield android/content/pm/ActivityInfo/name Ljava/lang/String;
invokespecial android/content/ComponentName/<init>(Ljava/lang/String;Ljava/lang/String;)V
astore 3
new android/content/Intent
dup
aload 0
getfield android/support/v7/internal/widget/l/e Landroid/content/Intent;
invokespecial android/content/Intent/<init>(Landroid/content/Intent;)V
astore 4
aload 4
aload 3
invokevirtual android/content/Intent/setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
pop
aload 0
getfield android/support/v7/internal/widget/l/g Landroid/support/v7/internal/widget/s;
ifnull L4
new android/content/Intent
dup
aload 4
invokespecial android/content/Intent/<init>(Landroid/content/Intent;)V
pop
L4:
aload 0
new android/support/v7/internal/widget/r
dup
aload 3
invokestatic java/lang/System/currentTimeMillis()J
fconst_1
invokespecial android/support/v7/internal/widget/r/<init>(Landroid/content/ComponentName;JF)V
invokevirtual android/support/v7/internal/widget/l/a(Landroid/support/v7/internal/widget/r;)Z
pop
aload 2
monitorexit
L5:
aload 4
areturn
L2:
astore 3
L6:
aload 2
monitorexit
L7:
aload 3
athrow
.limit locals 5
.limit stack 7
.end method

.method public final b()Landroid/content/pm/ResolveInfo;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
aload 0
getfield android/support/v7/internal/widget/l/c Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
invokevirtual android/support/v7/internal/widget/l/d()V
aload 0
getfield android/support/v7/internal/widget/l/d Ljava/util/List;
invokeinterface java/util/List/isEmpty()Z 0
ifne L3
aload 0
getfield android/support/v7/internal/widget/l/d Ljava/util/List;
iconst_0
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/support/v7/internal/widget/o
getfield android/support/v7/internal/widget/o/a Landroid/content/pm/ResolveInfo;
astore 2
aload 1
monitorexit
L1:
aload 2
areturn
L3:
aload 1
monitorexit
L4:
aconst_null
areturn
L2:
astore 2
L5:
aload 1
monitorexit
L6:
aload 2
athrow
.limit locals 3
.limit stack 2
.end method

.method public final c()I
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield android/support/v7/internal/widget/l/c Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokevirtual android/support/v7/internal/widget/l/d()V
aload 0
getfield android/support/v7/internal/widget/l/u Ljava/util/List;
invokeinterface java/util/List/size()I 0
istore 1
aload 2
monitorexit
L1:
iload 1
ireturn
L2:
astore 3
L3:
aload 2
monitorexit
L4:
aload 3
athrow
.limit locals 4
.limit stack 1
.end method

.method public final d()V
iconst_1
istore 2
aload 0
getfield android/support/v7/internal/widget/l/f Z
ifeq L0
aload 0
getfield android/support/v7/internal/widget/l/e Landroid/content/Intent;
ifnull L0
aload 0
iconst_0
putfield android/support/v7/internal/widget/l/f Z
aload 0
getfield android/support/v7/internal/widget/l/d Ljava/util/List;
invokeinterface java/util/List/clear()V 0
aload 0
getfield android/support/v7/internal/widget/l/v Landroid/content/Context;
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
aload 0
getfield android/support/v7/internal/widget/l/e Landroid/content/Intent;
iconst_0
invokevirtual android/content/pm/PackageManager/queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;
astore 4
aload 4
invokeinterface java/util/List/size()I 0
istore 3
iconst_0
istore 1
L1:
iload 1
iload 3
if_icmpge L2
aload 4
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/content/pm/ResolveInfo
astore 5
aload 0
getfield android/support/v7/internal/widget/l/d Ljava/util/List;
new android/support/v7/internal/widget/o
dup
aload 0
aload 5
invokespecial android/support/v7/internal/widget/o/<init>(Landroid/support/v7/internal/widget/l;Landroid/content/pm/ResolveInfo;)V
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
iconst_1
istore 1
L3:
aload 0
getfield android/support/v7/internal/widget/l/z Z
ifeq L4
aload 0
getfield android/support/v7/internal/widget/l/B Z
ifeq L4
aload 0
getfield android/support/v7/internal/widget/l/w Ljava/lang/String;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L4
aload 0
iconst_0
putfield android/support/v7/internal/widget/l/z Z
aload 0
iconst_1
putfield android/support/v7/internal/widget/l/A Z
aload 0
invokespecial android/support/v7/internal/widget/l/m()V
L5:
aload 0
invokespecial android/support/v7/internal/widget/l/l()V
iload 1
iload 2
ior
ifeq L6
aload 0
invokespecial android/support/v7/internal/widget/l/i()Z
pop
aload 0
invokevirtual android/support/v7/internal/widget/l/notifyChanged()V
L6:
return
L0:
iconst_0
istore 1
goto L3
L4:
iconst_0
istore 2
goto L5
.limit locals 6
.limit stack 5
.end method
