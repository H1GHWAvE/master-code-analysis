.bytecode 50.0
.class public synchronized android/support/v7/internal/widget/FitWindowsLinearLayout
.super android/widget/LinearLayout
.implements android/support/v7/internal/widget/af

.field private 'a' Landroid/support/v7/internal/widget/ag;

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return
.limit locals 3
.limit stack 3
.end method

.method protected fitSystemWindows(Landroid/graphics/Rect;)Z
aload 0
getfield android/support/v7/internal/widget/FitWindowsLinearLayout/a Landroid/support/v7/internal/widget/ag;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/FitWindowsLinearLayout/a Landroid/support/v7/internal/widget/ag;
aload 1
invokeinterface android/support/v7/internal/widget/ag/a(Landroid/graphics/Rect;)V 1
L0:
aload 0
aload 1
invokespecial android/widget/LinearLayout/fitSystemWindows(Landroid/graphics/Rect;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public setOnFitSystemWindowsListener(Landroid/support/v7/internal/widget/ag;)V
aload 0
aload 1
putfield android/support/v7/internal/widget/FitWindowsLinearLayout/a Landroid/support/v7/internal/widget/ag;
return
.limit locals 2
.limit stack 2
.end method
