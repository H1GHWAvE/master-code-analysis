.bytecode 50.0
.class public final synchronized android/support/v7/internal/view/menu/g
.super java/lang/Object
.implements android/support/v7/internal/view/menu/x
.implements android/widget/AdapterView$OnItemClickListener

.field public static final 'i' Ljava/lang/String; = "android:menu:list"

.field private static final 'j' Ljava/lang/String; = "ListMenuPresenter"

.field 'a' Landroid/content/Context;

.field 'b' Landroid/view/LayoutInflater;

.field 'c' Landroid/support/v7/internal/view/menu/i;

.field 'd' Landroid/support/v7/internal/view/menu/ExpandedMenuView;

.field 'e' I

.field 'f' I

.field public 'g' Landroid/support/v7/internal/view/menu/y;

.field 'h' Landroid/support/v7/internal/view/menu/h;

.field private 'k' I

.field private 'l' I

.method private <init>(I)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iload 1
putfield android/support/v7/internal/view/menu/g/f I
aload 0
iconst_0
putfield android/support/v7/internal/view/menu/g/e I
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(Landroid/content/Context;I)V
aload 0
iload 2
invokespecial android/support/v7/internal/view/menu/g/<init>(I)V
aload 0
aload 1
putfield android/support/v7/internal/view/menu/g/a Landroid/content/Context;
aload 0
aload 0
getfield android/support/v7/internal/view/menu/g/a Landroid/content/Context;
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
putfield android/support/v7/internal/view/menu/g/b Landroid/view/LayoutInflater;
return
.limit locals 3
.limit stack 2
.end method

.method static synthetic a(Landroid/support/v7/internal/view/menu/g;)I
aload 0
getfield android/support/v7/internal/view/menu/g/k I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private a(I)V
aload 0
iload 1
putfield android/support/v7/internal/view/menu/g/k I
aload 0
getfield android/support/v7/internal/view/menu/g/d Landroid/support/v7/internal/view/menu/ExpandedMenuView;
ifnull L0
aload 0
iconst_0
invokevirtual android/support/v7/internal/view/menu/g/a(Z)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/os/Bundle;)V
new android/util/SparseArray
dup
invokespecial android/util/SparseArray/<init>()V
astore 2
aload 0
getfield android/support/v7/internal/view/menu/g/d Landroid/support/v7/internal/view/menu/ExpandedMenuView;
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/g/d Landroid/support/v7/internal/view/menu/ExpandedMenuView;
aload 2
invokevirtual android/view/View/saveHierarchyState(Landroid/util/SparseArray;)V
L0:
aload 1
ldc "android:menu:list"
aload 2
invokevirtual android/os/Bundle/putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V
return
.limit locals 3
.limit stack 3
.end method

.method private b(I)V
aload 0
iload 1
putfield android/support/v7/internal/view/menu/g/l I
return
.limit locals 2
.limit stack 2
.end method

.method private b(Landroid/os/Bundle;)V
aload 1
ldc "android:menu:list"
invokevirtual android/os/Bundle/getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;
astore 1
aload 1
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/g/d Landroid/support/v7/internal/view/menu/ExpandedMenuView;
aload 1
invokevirtual android/view/View/restoreHierarchyState(Landroid/util/SparseArray;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private e()I
aload 0
getfield android/support/v7/internal/view/menu/g/k I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/view/ViewGroup;)Landroid/support/v7/internal/view/menu/z;
aload 0
getfield android/support/v7/internal/view/menu/g/d Landroid/support/v7/internal/view/menu/ExpandedMenuView;
ifnonnull L0
aload 0
aload 0
getfield android/support/v7/internal/view/menu/g/b Landroid/view/LayoutInflater;
getstatic android/support/v7/a/k/abc_expanded_menu_layout I
aload 1
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
checkcast android/support/v7/internal/view/menu/ExpandedMenuView
putfield android/support/v7/internal/view/menu/g/d Landroid/support/v7/internal/view/menu/ExpandedMenuView;
aload 0
getfield android/support/v7/internal/view/menu/g/h Landroid/support/v7/internal/view/menu/h;
ifnonnull L1
aload 0
new android/support/v7/internal/view/menu/h
dup
aload 0
invokespecial android/support/v7/internal/view/menu/h/<init>(Landroid/support/v7/internal/view/menu/g;)V
putfield android/support/v7/internal/view/menu/g/h Landroid/support/v7/internal/view/menu/h;
L1:
aload 0
getfield android/support/v7/internal/view/menu/g/d Landroid/support/v7/internal/view/menu/ExpandedMenuView;
aload 0
getfield android/support/v7/internal/view/menu/g/h Landroid/support/v7/internal/view/menu/h;
invokevirtual android/support/v7/internal/view/menu/ExpandedMenuView/setAdapter(Landroid/widget/ListAdapter;)V
aload 0
getfield android/support/v7/internal/view/menu/g/d Landroid/support/v7/internal/view/menu/ExpandedMenuView;
aload 0
invokevirtual android/support/v7/internal/view/menu/ExpandedMenuView/setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
L0:
aload 0
getfield android/support/v7/internal/view/menu/g/d Landroid/support/v7/internal/view/menu/ExpandedMenuView;
areturn
.limit locals 2
.limit stack 5
.end method

.method public final a(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;)V
aload 0
getfield android/support/v7/internal/view/menu/g/e I
ifeq L0
aload 0
new android/view/ContextThemeWrapper
dup
aload 1
aload 0
getfield android/support/v7/internal/view/menu/g/e I
invokespecial android/view/ContextThemeWrapper/<init>(Landroid/content/Context;I)V
putfield android/support/v7/internal/view/menu/g/a Landroid/content/Context;
aload 0
aload 0
getfield android/support/v7/internal/view/menu/g/a Landroid/content/Context;
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
putfield android/support/v7/internal/view/menu/g/b Landroid/view/LayoutInflater;
L1:
aload 0
aload 2
putfield android/support/v7/internal/view/menu/g/c Landroid/support/v7/internal/view/menu/i;
aload 0
getfield android/support/v7/internal/view/menu/g/h Landroid/support/v7/internal/view/menu/h;
ifnull L2
aload 0
getfield android/support/v7/internal/view/menu/g/h Landroid/support/v7/internal/view/menu/h;
invokevirtual android/support/v7/internal/view/menu/h/notifyDataSetChanged()V
L2:
return
L0:
aload 0
getfield android/support/v7/internal/view/menu/g/a Landroid/content/Context;
ifnull L1
aload 0
aload 1
putfield android/support/v7/internal/view/menu/g/a Landroid/content/Context;
aload 0
getfield android/support/v7/internal/view/menu/g/b Landroid/view/LayoutInflater;
ifnonnull L1
aload 0
aload 0
getfield android/support/v7/internal/view/menu/g/a Landroid/content/Context;
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
putfield android/support/v7/internal/view/menu/g/b Landroid/view/LayoutInflater;
goto L1
.limit locals 3
.limit stack 5
.end method

.method public final a(Landroid/os/Parcelable;)V
aload 1
checkcast android/os/Bundle
ldc "android:menu:list"
invokevirtual android/os/Bundle/getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;
astore 1
aload 1
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/g/d Landroid/support/v7/internal/view/menu/ExpandedMenuView;
aload 1
invokevirtual android/view/View/restoreHierarchyState(Landroid/util/SparseArray;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/support/v7/internal/view/menu/i;Z)V
aload 0
getfield android/support/v7/internal/view/menu/g/g Landroid/support/v7/internal/view/menu/y;
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/g/g Landroid/support/v7/internal/view/menu/y;
aload 1
iload 2
invokeinterface android/support/v7/internal/view/menu/y/a(Landroid/support/v7/internal/view/menu/i;Z)V 2
L0:
return
.limit locals 3
.limit stack 3
.end method

.method public final a(Landroid/support/v7/internal/view/menu/y;)V
aload 0
aload 1
putfield android/support/v7/internal/view/menu/g/g Landroid/support/v7/internal/view/menu/y;
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Z)V
aload 0
getfield android/support/v7/internal/view/menu/g/h Landroid/support/v7/internal/view/menu/h;
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/g/h Landroid/support/v7/internal/view/menu/h;
invokevirtual android/support/v7/internal/view/menu/h/notifyDataSetChanged()V
L0:
return
.limit locals 2
.limit stack 1
.end method

.method public final a()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/support/v7/internal/view/menu/ad;)Z
aload 1
invokevirtual android/support/v7/internal/view/menu/ad/hasVisibleItems()Z
ifne L0
iconst_0
ireturn
L0:
new android/support/v7/internal/view/menu/l
dup
aload 1
invokespecial android/support/v7/internal/view/menu/l/<init>(Landroid/support/v7/internal/view/menu/i;)V
astore 2
aload 2
getfield android/support/v7/internal/view/menu/l/a Landroid/support/v7/internal/view/menu/i;
astore 4
new android/support/v7/app/ag
dup
aload 4
getfield android/support/v7/internal/view/menu/i/e Landroid/content/Context;
invokespecial android/support/v7/app/ag/<init>(Landroid/content/Context;)V
astore 3
aload 2
new android/support/v7/internal/view/menu/g
dup
aload 3
getfield android/support/v7/app/ag/a Landroid/support/v7/app/x;
getfield android/support/v7/app/x/a Landroid/content/Context;
getstatic android/support/v7/a/k/abc_list_menu_item_layout I
invokespecial android/support/v7/internal/view/menu/g/<init>(Landroid/content/Context;I)V
putfield android/support/v7/internal/view/menu/l/c Landroid/support/v7/internal/view/menu/g;
aload 2
getfield android/support/v7/internal/view/menu/l/c Landroid/support/v7/internal/view/menu/g;
aload 2
putfield android/support/v7/internal/view/menu/g/g Landroid/support/v7/internal/view/menu/y;
aload 2
getfield android/support/v7/internal/view/menu/l/a Landroid/support/v7/internal/view/menu/i;
aload 2
getfield android/support/v7/internal/view/menu/l/c Landroid/support/v7/internal/view/menu/g;
invokevirtual android/support/v7/internal/view/menu/i/a(Landroid/support/v7/internal/view/menu/x;)V
aload 2
getfield android/support/v7/internal/view/menu/l/c Landroid/support/v7/internal/view/menu/g;
invokevirtual android/support/v7/internal/view/menu/g/d()Landroid/widget/ListAdapter;
astore 5
aload 3
getfield android/support/v7/app/ag/a Landroid/support/v7/app/x;
aload 5
putfield android/support/v7/app/x/t Landroid/widget/ListAdapter;
aload 3
getfield android/support/v7/app/ag/a Landroid/support/v7/app/x;
aload 2
putfield android/support/v7/app/x/u Landroid/content/DialogInterface$OnClickListener;
aload 4
getfield android/support/v7/internal/view/menu/i/l Landroid/view/View;
astore 5
aload 5
ifnull L1
aload 3
getfield android/support/v7/app/ag/a Landroid/support/v7/app/x;
aload 5
putfield android/support/v7/app/x/g Landroid/view/View;
L2:
aload 3
getfield android/support/v7/app/ag/a Landroid/support/v7/app/x;
aload 2
putfield android/support/v7/app/x/r Landroid/content/DialogInterface$OnKeyListener;
aload 2
aload 3
invokevirtual android/support/v7/app/ag/a()Landroid/support/v7/app/af;
putfield android/support/v7/internal/view/menu/l/b Landroid/support/v7/app/af;
aload 2
getfield android/support/v7/internal/view/menu/l/b Landroid/support/v7/app/af;
aload 2
invokevirtual android/support/v7/app/af/setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V
aload 2
getfield android/support/v7/internal/view/menu/l/b Landroid/support/v7/app/af;
invokevirtual android/support/v7/app/af/getWindow()Landroid/view/Window;
invokevirtual android/view/Window/getAttributes()Landroid/view/WindowManager$LayoutParams;
astore 3
aload 3
sipush 1003
putfield android/view/WindowManager$LayoutParams/type I
aload 3
aload 3
getfield android/view/WindowManager$LayoutParams/flags I
ldc_w 131072
ior
putfield android/view/WindowManager$LayoutParams/flags I
aload 2
getfield android/support/v7/internal/view/menu/l/b Landroid/support/v7/app/af;
invokevirtual android/support/v7/app/af/show()V
aload 0
getfield android/support/v7/internal/view/menu/g/g Landroid/support/v7/internal/view/menu/y;
ifnull L3
aload 0
getfield android/support/v7/internal/view/menu/g/g Landroid/support/v7/internal/view/menu/y;
aload 1
invokeinterface android/support/v7/internal/view/menu/y/a_(Landroid/support/v7/internal/view/menu/i;)Z 1
pop
L3:
iconst_1
ireturn
L1:
aload 4
getfield android/support/v7/internal/view/menu/i/k Landroid/graphics/drawable/Drawable;
astore 5
aload 3
getfield android/support/v7/app/ag/a Landroid/support/v7/app/x;
aload 5
putfield android/support/v7/app/x/d Landroid/graphics/drawable/Drawable;
aload 4
getfield android/support/v7/internal/view/menu/i/j Ljava/lang/CharSequence;
astore 4
aload 3
getfield android/support/v7/app/ag/a Landroid/support/v7/app/x;
aload 4
putfield android/support/v7/app/x/f Ljava/lang/CharSequence;
goto L2
.limit locals 6
.limit stack 5
.end method

.method public final a(Landroid/support/v7/internal/view/menu/m;)Z
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final b()I
aload 0
getfield android/support/v7/internal/view/menu/g/l I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final b(Landroid/support/v7/internal/view/menu/m;)Z
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final c()Landroid/os/Parcelable;
aload 0
getfield android/support/v7/internal/view/menu/g/d Landroid/support/v7/internal/view/menu/ExpandedMenuView;
ifnonnull L0
aconst_null
areturn
L0:
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 1
new android/util/SparseArray
dup
invokespecial android/util/SparseArray/<init>()V
astore 2
aload 0
getfield android/support/v7/internal/view/menu/g/d Landroid/support/v7/internal/view/menu/ExpandedMenuView;
ifnull L1
aload 0
getfield android/support/v7/internal/view/menu/g/d Landroid/support/v7/internal/view/menu/ExpandedMenuView;
aload 2
invokevirtual android/view/View/saveHierarchyState(Landroid/util/SparseArray;)V
L1:
aload 1
ldc "android:menu:list"
aload 2
invokevirtual android/os/Bundle/putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V
aload 1
areturn
.limit locals 3
.limit stack 3
.end method

.method public final d()Landroid/widget/ListAdapter;
aload 0
getfield android/support/v7/internal/view/menu/g/h Landroid/support/v7/internal/view/menu/h;
ifnonnull L0
aload 0
new android/support/v7/internal/view/menu/h
dup
aload 0
invokespecial android/support/v7/internal/view/menu/h/<init>(Landroid/support/v7/internal/view/menu/g;)V
putfield android/support/v7/internal/view/menu/g/h Landroid/support/v7/internal/view/menu/h;
L0:
aload 0
getfield android/support/v7/internal/view/menu/g/h Landroid/support/v7/internal/view/menu/h;
areturn
.limit locals 1
.limit stack 4
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
aload 0
getfield android/support/v7/internal/view/menu/g/c Landroid/support/v7/internal/view/menu/i;
aload 0
getfield android/support/v7/internal/view/menu/g/h Landroid/support/v7/internal/view/menu/h;
iload 3
invokevirtual android/support/v7/internal/view/menu/h/a(I)Landroid/support/v7/internal/view/menu/m;
aload 0
iconst_0
invokevirtual android/support/v7/internal/view/menu/i/a(Landroid/view/MenuItem;Landroid/support/v7/internal/view/menu/x;I)Z
pop
return
.limit locals 6
.limit stack 4
.end method
