.bytecode 50.0
.class public final synchronized android/support/v7/internal/a/a
.super java/lang/Object

.field static final 'a' [Ljava/lang/Class;

.field private static final 'b' Ljava/lang/String; = "AppCompatViewInflater"

.field private static final 'c' Ljava/util/Map;

.field private final 'd' [Ljava/lang/Object;

.method static <clinit>()V
iconst_2
anewarray java/lang/Class
dup
iconst_0
ldc android/content/Context
aastore
dup
iconst_1
ldc android/util/AttributeSet
aastore
putstatic android/support/v7/internal/a/a/a [Ljava/lang/Class;
new android/support/v4/n/a
dup
invokespecial android/support/v4/n/a/<init>()V
putstatic android/support/v7/internal/a/a/c Ljava/util/Map;
return
.limit locals 0
.limit stack 4
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_2
anewarray java/lang/Object
putfield android/support/v7/internal/a/a/d [Ljava/lang/Object;
return
.limit locals 1
.limit stack 2
.end method

.method public static a(Landroid/content/Context;Landroid/util/AttributeSet;Z)Landroid/content/Context;
aload 0
aload 1
getstatic android/support/v7/a/n/View [I
iconst_0
iconst_0
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
astore 1
iload 2
ifeq L0
aload 1
getstatic android/support/v7/a/n/View_android_theme I
iconst_0
invokevirtual android/content/res/TypedArray/getResourceId(II)I
istore 3
L1:
iload 3
istore 4
iload 3
ifne L2
aload 1
getstatic android/support/v7/a/n/View_theme I
iconst_0
invokevirtual android/content/res/TypedArray/getResourceId(II)I
istore 3
iload 3
istore 4
iload 3
ifeq L2
ldc "AppCompatViewInflater"
ldc "app:theme is now deprecated. Please move to using android:theme instead."
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;)I
pop
iload 3
istore 4
L2:
aload 1
invokevirtual android/content/res/TypedArray/recycle()V
aload 0
astore 1
iload 4
ifeq L3
aload 0
instanceof android/support/v7/internal/view/b
ifeq L4
aload 0
astore 1
aload 0
checkcast android/support/v7/internal/view/b
getfield android/support/v7/internal/view/b/a I
iload 4
if_icmpeq L3
L4:
new android/support/v7/internal/view/b
dup
aload 0
iload 4
invokespecial android/support/v7/internal/view/b/<init>(Landroid/content/Context;I)V
astore 1
L3:
aload 1
areturn
L0:
iconst_0
istore 3
goto L1
.limit locals 5
.limit stack 5
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L4 to L5 using L2
.catch java/lang/Exception from L5 to L6 using L2
getstatic android/support/v7/internal/a/a/c Ljava/util/Map;
aload 2
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/reflect/Constructor
astore 5
aload 5
astore 4
aload 5
ifnonnull L5
L0:
aload 1
invokevirtual android/content/Context/getClassLoader()Ljava/lang/ClassLoader;
astore 4
L1:
aload 3
ifnull L7
L3:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
L4:
aload 4
aload 1
invokevirtual java/lang/ClassLoader/loadClass(Ljava/lang/String;)Ljava/lang/Class;
ldc android/view/View
invokevirtual java/lang/Class/asSubclass(Ljava/lang/Class;)Ljava/lang/Class;
getstatic android/support/v7/internal/a/a/a [Ljava/lang/Class;
invokevirtual java/lang/Class/getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
astore 4
getstatic android/support/v7/internal/a/a/c Ljava/util/Map;
aload 2
aload 4
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L5:
aload 4
iconst_1
invokevirtual java/lang/reflect/Constructor/setAccessible(Z)V
aload 4
aload 0
getfield android/support/v7/internal/a/a/d [Ljava/lang/Object;
invokevirtual java/lang/reflect/Constructor/newInstance([Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/view/View
astore 1
L6:
aload 1
areturn
L7:
aload 2
astore 1
goto L4
L2:
astore 1
aconst_null
areturn
.limit locals 6
.limit stack 3
.end method

.method private a(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;ZZ)Landroid/view/View;
.annotation invisibleparam 3 Landroid/support/a/y;
.end annotation
.annotation invisibleparam 4 Landroid/support/a/y;
.end annotation
iload 5
ifeq L0
aload 1
ifnull L0
aload 1
invokevirtual android/view/View/getContext()Landroid/content/Context;
astore 1
L1:
aload 1
aload 4
getstatic android/support/v7/a/n/View [I
iconst_0
iconst_0
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
astore 9
iload 6
ifeq L2
aload 9
getstatic android/support/v7/a/n/View_android_theme I
iconst_0
invokevirtual android/content/res/TypedArray/getResourceId(II)I
istore 7
L3:
iload 7
istore 8
iload 7
ifne L4
aload 9
getstatic android/support/v7/a/n/View_theme I
iconst_0
invokevirtual android/content/res/TypedArray/getResourceId(II)I
istore 7
iload 7
istore 8
iload 7
ifeq L4
ldc "AppCompatViewInflater"
ldc "app:theme is now deprecated. Please move to using android:theme instead."
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;)I
pop
iload 7
istore 8
L4:
aload 9
invokevirtual android/content/res/TypedArray/recycle()V
aload 1
astore 9
iload 8
ifeq L5
aload 1
instanceof android/support/v7/internal/view/b
ifeq L6
aload 1
astore 9
aload 1
checkcast android/support/v7/internal/view/b
getfield android/support/v7/internal/view/b/a I
iload 8
if_icmpeq L5
L6:
new android/support/v7/internal/view/b
dup
aload 1
iload 8
invokespecial android/support/v7/internal/view/b/<init>(Landroid/content/Context;I)V
astore 9
L5:
iconst_m1
istore 7
aload 2
invokevirtual java/lang/String/hashCode()I
lookupswitch
-1946472170 : L7
-1455429095 : L8
-1346021293 : L9
-938935918 : L10
-339785223 : L11
776382189 : L12
1413872058 : L13
1601505219 : L14
1666676343 : L15
2001146706 : L16
default : L17
L17:
iload 7
tableswitch 0
L18
L19
L20
L21
L22
L23
L24
L25
L26
L27
default : L28
L28:
aload 3
aload 9
if_acmpeq L29
aload 0
aload 9
aload 2
aload 4
invokevirtual android/support/v7/internal/a/a/a(Landroid/content/Context;Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/view/View;
areturn
L15:
aload 2
ldc "EditText"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L17
iconst_0
istore 7
goto L17
L11:
aload 2
ldc "Spinner"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L17
iconst_1
istore 7
goto L17
L14:
aload 2
ldc "CheckBox"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L17
iconst_2
istore 7
goto L17
L12:
aload 2
ldc "RadioButton"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L17
iconst_3
istore 7
goto L17
L8:
aload 2
ldc "CheckedTextView"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L17
iconst_4
istore 7
goto L17
L13:
aload 2
ldc "AutoCompleteTextView"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L17
iconst_5
istore 7
goto L17
L9:
aload 2
ldc "MultiAutoCompleteTextView"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L17
bipush 6
istore 7
goto L17
L7:
aload 2
ldc "RatingBar"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L17
bipush 7
istore 7
goto L17
L16:
aload 2
ldc "Button"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L17
bipush 8
istore 7
goto L17
L10:
aload 2
ldc "TextView"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L17
bipush 9
istore 7
goto L17
L18:
new android/support/v7/widget/w
dup
aload 9
aload 4
invokespecial android/support/v7/widget/w/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
areturn
L19:
new android/support/v7/widget/aa
dup
aload 9
aload 4
invokespecial android/support/v7/widget/aa/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
areturn
L20:
new android/support/v7/widget/s
dup
aload 9
aload 4
invokespecial android/support/v7/widget/s/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
areturn
L21:
new android/support/v7/widget/y
dup
aload 9
aload 4
invokespecial android/support/v7/widget/y/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
areturn
L22:
new android/support/v7/widget/t
dup
aload 9
aload 4
invokespecial android/support/v7/widget/t/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
areturn
L23:
new android/support/v7/widget/p
dup
aload 9
aload 4
invokespecial android/support/v7/widget/p/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
areturn
L24:
new android/support/v7/widget/x
dup
aload 9
aload 4
invokespecial android/support/v7/widget/x/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
areturn
L25:
new android/support/v7/widget/z
dup
aload 9
aload 4
invokespecial android/support/v7/widget/z/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
areturn
L26:
new android/support/v7/widget/r
dup
aload 9
aload 4
invokespecial android/support/v7/widget/r/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
areturn
L27:
new android/support/v7/widget/ai
dup
aload 9
aload 4
invokespecial android/support/v7/widget/ai/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
areturn
L29:
aconst_null
areturn
L2:
iconst_0
istore 7
goto L3
L0:
aload 3
astore 1
goto L1
.limit locals 10
.limit stack 5
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/view/View;
.catch java/lang/Exception from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/lang/Exception from L4 to L5 using L2
.catch all from L4 to L5 using L3
aload 2
astore 4
aload 2
ldc "view"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
aload 3
aconst_null
ldc "class"
invokeinterface android/util/AttributeSet/getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
astore 4
L0:
aload 0
getfield android/support/v7/internal/a/a/d [Ljava/lang/Object;
iconst_0
aload 1
aastore
aload 0
getfield android/support/v7/internal/a/a/d [Ljava/lang/Object;
iconst_1
aload 3
aastore
iconst_m1
aload 4
bipush 46
invokevirtual java/lang/String/indexOf(I)I
if_icmpne L4
aload 0
aload 1
aload 4
ldc "android.widget."
invokespecial android/support/v7/internal/a/a/a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;
astore 1
L1:
aload 0
getfield android/support/v7/internal/a/a/d [Ljava/lang/Object;
iconst_0
aconst_null
aastore
aload 0
getfield android/support/v7/internal/a/a/d [Ljava/lang/Object;
iconst_1
aconst_null
aastore
aload 1
areturn
L4:
aload 0
aload 1
aload 4
aconst_null
invokespecial android/support/v7/internal/a/a/a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;
astore 1
L5:
aload 0
getfield android/support/v7/internal/a/a/d [Ljava/lang/Object;
iconst_0
aconst_null
aastore
aload 0
getfield android/support/v7/internal/a/a/d [Ljava/lang/Object;
iconst_1
aconst_null
aastore
aload 1
areturn
L2:
astore 1
aload 0
getfield android/support/v7/internal/a/a/d [Ljava/lang/Object;
iconst_0
aconst_null
aastore
aload 0
getfield android/support/v7/internal/a/a/d [Ljava/lang/Object;
iconst_1
aconst_null
aastore
aconst_null
areturn
L3:
astore 1
aload 0
getfield android/support/v7/internal/a/a/d [Ljava/lang/Object;
iconst_0
aconst_null
aastore
aload 0
getfield android/support/v7/internal/a/a/d [Ljava/lang/Object;
iconst_1
aconst_null
aastore
aload 1
athrow
.limit locals 5
.limit stack 4
.end method
