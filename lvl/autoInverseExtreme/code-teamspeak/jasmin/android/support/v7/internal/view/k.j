.bytecode 50.0
.class public synchronized android/support/v7/internal/view/k
.super java/lang/Object
.implements android/view/Window$Callback

.field final 'd' Landroid/view/Window$Callback;

.method public <init>(Landroid/view/Window$Callback;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 1
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "Window callback may not be null"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 1
putfield android/support/v7/internal/view/k/d Landroid/view/Window$Callback;
return
.limit locals 2
.limit stack 3
.end method

.method public dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z
aload 0
getfield android/support/v7/internal/view/k/d Landroid/view/Window$Callback;
aload 1
invokeinterface android/view/Window$Callback/dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
aload 0
getfield android/support/v7/internal/view/k/d Landroid/view/Window$Callback;
aload 1
invokeinterface android/view/Window$Callback/dispatchKeyEvent(Landroid/view/KeyEvent;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public dispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z
aload 0
getfield android/support/v7/internal/view/k/d Landroid/view/Window$Callback;
aload 1
invokeinterface android/view/Window$Callback/dispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
aload 0
getfield android/support/v7/internal/view/k/d Landroid/view/Window$Callback;
aload 1
invokeinterface android/view/Window$Callback/dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
aload 0
getfield android/support/v7/internal/view/k/d Landroid/view/Window$Callback;
aload 1
invokeinterface android/view/Window$Callback/dispatchTouchEvent(Landroid/view/MotionEvent;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public dispatchTrackballEvent(Landroid/view/MotionEvent;)Z
aload 0
getfield android/support/v7/internal/view/k/d Landroid/view/Window$Callback;
aload 1
invokeinterface android/view/Window$Callback/dispatchTrackballEvent(Landroid/view/MotionEvent;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public onActionModeFinished(Landroid/view/ActionMode;)V
aload 0
getfield android/support/v7/internal/view/k/d Landroid/view/Window$Callback;
aload 1
invokeinterface android/view/Window$Callback/onActionModeFinished(Landroid/view/ActionMode;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public onActionModeStarted(Landroid/view/ActionMode;)V
aload 0
getfield android/support/v7/internal/view/k/d Landroid/view/Window$Callback;
aload 1
invokeinterface android/view/Window$Callback/onActionModeStarted(Landroid/view/ActionMode;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public onAttachedToWindow()V
aload 0
getfield android/support/v7/internal/view/k/d Landroid/view/Window$Callback;
invokeinterface android/view/Window$Callback/onAttachedToWindow()V 0
return
.limit locals 1
.limit stack 1
.end method

.method public onContentChanged()V
aload 0
getfield android/support/v7/internal/view/k/d Landroid/view/Window$Callback;
invokeinterface android/view/Window$Callback/onContentChanged()V 0
return
.limit locals 1
.limit stack 1
.end method

.method public onCreatePanelMenu(ILandroid/view/Menu;)Z
aload 0
getfield android/support/v7/internal/view/k/d Landroid/view/Window$Callback;
iload 1
aload 2
invokeinterface android/view/Window$Callback/onCreatePanelMenu(ILandroid/view/Menu;)Z 2
ireturn
.limit locals 3
.limit stack 3
.end method

.method public onCreatePanelView(I)Landroid/view/View;
aload 0
getfield android/support/v7/internal/view/k/d Landroid/view/Window$Callback;
iload 1
invokeinterface android/view/Window$Callback/onCreatePanelView(I)Landroid/view/View; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public onDetachedFromWindow()V
aload 0
getfield android/support/v7/internal/view/k/d Landroid/view/Window$Callback;
invokeinterface android/view/Window$Callback/onDetachedFromWindow()V 0
return
.limit locals 1
.limit stack 1
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
aload 0
getfield android/support/v7/internal/view/k/d Landroid/view/Window$Callback;
iload 1
aload 2
invokeinterface android/view/Window$Callback/onMenuItemSelected(ILandroid/view/MenuItem;)Z 2
ireturn
.limit locals 3
.limit stack 3
.end method

.method public onMenuOpened(ILandroid/view/Menu;)Z
aload 0
getfield android/support/v7/internal/view/k/d Landroid/view/Window$Callback;
iload 1
aload 2
invokeinterface android/view/Window$Callback/onMenuOpened(ILandroid/view/Menu;)Z 2
ireturn
.limit locals 3
.limit stack 3
.end method

.method public onPanelClosed(ILandroid/view/Menu;)V
aload 0
getfield android/support/v7/internal/view/k/d Landroid/view/Window$Callback;
iload 1
aload 2
invokeinterface android/view/Window$Callback/onPanelClosed(ILandroid/view/Menu;)V 2
return
.limit locals 3
.limit stack 3
.end method

.method public onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
aload 0
getfield android/support/v7/internal/view/k/d Landroid/view/Window$Callback;
iload 1
aload 2
aload 3
invokeinterface android/view/Window$Callback/onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z 3
ireturn
.limit locals 4
.limit stack 4
.end method

.method public onSearchRequested()Z
aload 0
getfield android/support/v7/internal/view/k/d Landroid/view/Window$Callback;
invokeinterface android/view/Window$Callback/onSearchRequested()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public onSearchRequested(Landroid/view/SearchEvent;)Z
aload 0
getfield android/support/v7/internal/view/k/d Landroid/view/Window$Callback;
aload 1
invokeinterface android/view/Window$Callback/onSearchRequested(Landroid/view/SearchEvent;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public onWindowAttributesChanged(Landroid/view/WindowManager$LayoutParams;)V
aload 0
getfield android/support/v7/internal/view/k/d Landroid/view/Window$Callback;
aload 1
invokeinterface android/view/Window$Callback/onWindowAttributesChanged(Landroid/view/WindowManager$LayoutParams;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public onWindowFocusChanged(Z)V
aload 0
getfield android/support/v7/internal/view/k/d Landroid/view/Window$Callback;
iload 1
invokeinterface android/view/Window$Callback/onWindowFocusChanged(Z)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public onWindowStartingActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
aload 0
getfield android/support/v7/internal/view/k/d Landroid/view/Window$Callback;
aload 1
invokeinterface android/view/Window$Callback/onWindowStartingActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public onWindowStartingActionMode(Landroid/view/ActionMode$Callback;I)Landroid/view/ActionMode;
aload 0
getfield android/support/v7/internal/view/k/d Landroid/view/Window$Callback;
aload 1
iload 2
invokeinterface android/view/Window$Callback/onWindowStartingActionMode(Landroid/view/ActionMode$Callback;I)Landroid/view/ActionMode; 2
areturn
.limit locals 3
.limit stack 3
.end method
