.bytecode 50.0
.class public synchronized android/support/v7/internal/widget/ActionBarContainer
.super android/widget/FrameLayout

.field 'a' Landroid/graphics/drawable/Drawable;

.field 'b' Landroid/graphics/drawable/Drawable;

.field 'c' Landroid/graphics/drawable/Drawable;

.field 'd' Z

.field 'e' Z

.field private 'f' Z

.field private 'g' Landroid/view/View;

.field private 'h' Landroid/view/View;

.field private 'i' Landroid/view/View;

.field private 'j' I

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
aconst_null
invokespecial android/support/v7/internal/widget/ActionBarContainer/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
invokespecial android/widget/FrameLayout/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmplt L0
iconst_1
istore 3
L1:
iload 3
ifeq L2
new android/support/v7/internal/widget/e
dup
aload 0
invokespecial android/support/v7/internal/widget/e/<init>(Landroid/support/v7/internal/widget/ActionBarContainer;)V
astore 5
L3:
aload 0
aload 5
invokevirtual android/support/v7/internal/widget/ActionBarContainer/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 1
aload 2
getstatic android/support/v7/a/n/ActionBar [I
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
astore 1
aload 0
aload 1
getstatic android/support/v7/a/n/ActionBar_background I
invokevirtual android/content/res/TypedArray/getDrawable(I)Landroid/graphics/drawable/Drawable;
putfield android/support/v7/internal/widget/ActionBarContainer/a Landroid/graphics/drawable/Drawable;
aload 0
aload 1
getstatic android/support/v7/a/n/ActionBar_backgroundStacked I
invokevirtual android/content/res/TypedArray/getDrawable(I)Landroid/graphics/drawable/Drawable;
putfield android/support/v7/internal/widget/ActionBarContainer/b Landroid/graphics/drawable/Drawable;
aload 0
aload 1
getstatic android/support/v7/a/n/ActionBar_height I
iconst_m1
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
putfield android/support/v7/internal/widget/ActionBarContainer/j I
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getId()I
getstatic android/support/v7/a/i/split_action_bar I
if_icmpne L4
aload 0
iconst_1
putfield android/support/v7/internal/widget/ActionBarContainer/d Z
aload 0
aload 1
getstatic android/support/v7/a/n/ActionBar_backgroundSplit I
invokevirtual android/content/res/TypedArray/getDrawable(I)Landroid/graphics/drawable/Drawable;
putfield android/support/v7/internal/widget/ActionBarContainer/c Landroid/graphics/drawable/Drawable;
L4:
aload 1
invokevirtual android/content/res/TypedArray/recycle()V
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/d Z
ifeq L5
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/c Landroid/graphics/drawable/Drawable;
ifnonnull L6
iconst_1
istore 4
L7:
aload 0
iload 4
invokevirtual android/support/v7/internal/widget/ActionBarContainer/setWillNotDraw(Z)V
return
L0:
iconst_0
istore 3
goto L1
L2:
new android/support/v7/internal/widget/d
dup
aload 0
invokespecial android/support/v7/internal/widget/d/<init>(Landroid/support/v7/internal/widget/ActionBarContainer;)V
astore 5
goto L3
L6:
iconst_0
istore 4
goto L7
L5:
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/a Landroid/graphics/drawable/Drawable;
ifnonnull L8
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/b Landroid/graphics/drawable/Drawable;
ifnonnull L8
iconst_1
istore 4
goto L7
L8:
iconst_0
istore 4
goto L7
.limit locals 6
.limit stack 4
.end method

.method private static a()Landroid/support/v7/c/a;
aconst_null
areturn
.limit locals 0
.limit stack 1
.end method

.method private static a(Landroid/view/View;)Z
aload 0
ifnull L0
aload 0
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L0
aload 0
invokevirtual android/view/View/getMeasuredHeight()I
ifne L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static b(Landroid/view/View;)I
aload 0
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/widget/FrameLayout$LayoutParams
astore 3
aload 0
invokevirtual android/view/View/getMeasuredHeight()I
istore 1
aload 3
getfield android/widget/FrameLayout$LayoutParams/topMargin I
istore 2
aload 3
getfield android/widget/FrameLayout$LayoutParams/bottomMargin I
iload 1
iload 2
iadd
iadd
ireturn
.limit locals 4
.limit stack 3
.end method

.method protected drawableStateChanged()V
aload 0
invokespecial android/widget/FrameLayout/drawableStateChanged()V
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/a Landroid/graphics/drawable/Drawable;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/a Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/isStateful()Z
ifeq L0
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/a Landroid/graphics/drawable/Drawable;
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getDrawableState()[I
invokevirtual android/graphics/drawable/Drawable/setState([I)Z
pop
L0:
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/b Landroid/graphics/drawable/Drawable;
ifnull L1
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/b Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/isStateful()Z
ifeq L1
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/b Landroid/graphics/drawable/Drawable;
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getDrawableState()[I
invokevirtual android/graphics/drawable/Drawable/setState([I)Z
pop
L1:
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/c Landroid/graphics/drawable/Drawable;
ifnull L2
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/c Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/isStateful()Z
ifeq L2
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/c Landroid/graphics/drawable/Drawable;
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getDrawableState()[I
invokevirtual android/graphics/drawable/Drawable/setState([I)Z
pop
L2:
return
.limit locals 1
.limit stack 2
.end method

.method public getTabContainer()Landroid/view/View;
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/g Landroid/view/View;
areturn
.limit locals 1
.limit stack 1
.end method

.method public jumpDrawablesToCurrentState()V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L0
aload 0
invokespecial android/widget/FrameLayout/jumpDrawablesToCurrentState()V
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/a Landroid/graphics/drawable/Drawable;
ifnull L1
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/a Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/jumpToCurrentState()V
L1:
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/b Landroid/graphics/drawable/Drawable;
ifnull L2
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/b Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/jumpToCurrentState()V
L2:
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/c Landroid/graphics/drawable/Drawable;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/c Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/jumpToCurrentState()V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public onFinishInflate()V
aload 0
invokespecial android/widget/FrameLayout/onFinishInflate()V
aload 0
aload 0
getstatic android/support/v7/a/i/action_bar I
invokevirtual android/support/v7/internal/widget/ActionBarContainer/findViewById(I)Landroid/view/View;
putfield android/support/v7/internal/widget/ActionBarContainer/h Landroid/view/View;
aload 0
aload 0
getstatic android/support/v7/a/i/action_context_bar I
invokevirtual android/support/v7/internal/widget/ActionBarContainer/findViewById(I)Landroid/view/View;
putfield android/support/v7/internal/widget/ActionBarContainer/i Landroid/view/View;
return
.limit locals 1
.limit stack 3
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/f Z
ifne L0
aload 0
aload 1
invokespecial android/widget/FrameLayout/onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public onLayout(ZIIII)V
iconst_1
istore 6
aload 0
iload 1
iload 2
iload 3
iload 4
iload 5
invokespecial android/widget/FrameLayout/onLayout(ZIIII)V
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/g Landroid/view/View;
astore 7
aload 7
ifnull L0
aload 7
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L0
iconst_1
istore 1
L1:
aload 7
ifnull L2
aload 7
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L2
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getMeasuredHeight()I
istore 3
aload 7
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/widget/FrameLayout$LayoutParams
astore 8
aload 7
iload 2
iload 3
aload 7
invokevirtual android/view/View/getMeasuredHeight()I
isub
aload 8
getfield android/widget/FrameLayout$LayoutParams/bottomMargin I
isub
iload 4
iload 3
aload 8
getfield android/widget/FrameLayout$LayoutParams/bottomMargin I
isub
invokevirtual android/view/View/layout(IIII)V
L2:
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/d Z
ifeq L3
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/c Landroid/graphics/drawable/Drawable;
ifnull L4
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/c Landroid/graphics/drawable/Drawable;
iconst_0
iconst_0
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getMeasuredWidth()I
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getMeasuredHeight()I
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
iload 6
istore 2
L5:
iload 2
ifeq L6
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContainer/invalidate()V
L6:
return
L0:
iconst_0
istore 1
goto L1
L3:
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/a Landroid/graphics/drawable/Drawable;
ifnull L7
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/h Landroid/view/View;
invokevirtual android/view/View/getVisibility()I
ifne L8
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/a Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/h Landroid/view/View;
invokevirtual android/view/View/getLeft()I
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/h Landroid/view/View;
invokevirtual android/view/View/getTop()I
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/h Landroid/view/View;
invokevirtual android/view/View/getRight()I
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/h Landroid/view/View;
invokevirtual android/view/View/getBottom()I
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
L9:
iconst_1
istore 2
L10:
aload 0
iload 1
putfield android/support/v7/internal/widget/ActionBarContainer/e Z
iload 1
ifeq L11
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/b Landroid/graphics/drawable/Drawable;
ifnull L11
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/b Landroid/graphics/drawable/Drawable;
aload 7
invokevirtual android/view/View/getLeft()I
aload 7
invokevirtual android/view/View/getTop()I
aload 7
invokevirtual android/view/View/getRight()I
aload 7
invokevirtual android/view/View/getBottom()I
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
iload 6
istore 2
goto L5
L8:
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/i Landroid/view/View;
ifnull L12
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/i Landroid/view/View;
invokevirtual android/view/View/getVisibility()I
ifne L12
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/a Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/i Landroid/view/View;
invokevirtual android/view/View/getLeft()I
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/i Landroid/view/View;
invokevirtual android/view/View/getTop()I
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/i Landroid/view/View;
invokevirtual android/view/View/getRight()I
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/i Landroid/view/View;
invokevirtual android/view/View/getBottom()I
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
goto L9
L12:
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/a Landroid/graphics/drawable/Drawable;
iconst_0
iconst_0
iconst_0
iconst_0
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
goto L9
L11:
goto L5
L7:
iconst_0
istore 2
goto L10
L4:
iconst_0
istore 2
goto L5
.limit locals 9
.limit stack 6
.end method

.method public onMeasure(II)V
iload 2
istore 3
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/h Landroid/view/View;
ifnonnull L0
iload 2
istore 3
iload 2
invokestatic android/view/View$MeasureSpec/getMode(I)I
ldc_w -2147483648
if_icmpne L0
iload 2
istore 3
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/j I
iflt L0
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/j I
iload 2
invokestatic android/view/View$MeasureSpec/getSize(I)I
invokestatic java/lang/Math/min(II)I
ldc_w -2147483648
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 3
L0:
aload 0
iload 1
iload 3
invokespecial android/widget/FrameLayout/onMeasure(II)V
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/h Landroid/view/View;
ifnonnull L1
L2:
return
L1:
iload 3
invokestatic android/view/View$MeasureSpec/getMode(I)I
istore 2
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/g Landroid/view/View;
ifnull L2
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/g Landroid/view/View;
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L2
iload 2
ldc_w 1073741824
if_icmpeq L2
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/h Landroid/view/View;
invokestatic android/support/v7/internal/widget/ActionBarContainer/a(Landroid/view/View;)Z
ifne L3
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/h Landroid/view/View;
invokestatic android/support/v7/internal/widget/ActionBarContainer/b(Landroid/view/View;)I
istore 1
L4:
iload 2
ldc_w -2147483648
if_icmpne L5
iload 3
invokestatic android/view/View$MeasureSpec/getSize(I)I
istore 2
L6:
aload 0
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getMeasuredWidth()I
iload 1
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/g Landroid/view/View;
invokestatic android/support/v7/internal/widget/ActionBarContainer/b(Landroid/view/View;)I
iadd
iload 2
invokestatic java/lang/Math/min(II)I
invokevirtual android/support/v7/internal/widget/ActionBarContainer/setMeasuredDimension(II)V
return
L3:
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/i Landroid/view/View;
invokestatic android/support/v7/internal/widget/ActionBarContainer/a(Landroid/view/View;)Z
ifne L7
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/i Landroid/view/View;
invokestatic android/support/v7/internal/widget/ActionBarContainer/b(Landroid/view/View;)I
istore 1
goto L4
L7:
iconst_0
istore 1
goto L4
L5:
ldc_w 2147483647
istore 2
goto L6
.limit locals 4
.limit stack 4
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
aload 0
aload 1
invokespecial android/widget/FrameLayout/onTouchEvent(Landroid/view/MotionEvent;)Z
pop
iconst_1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public setPrimaryBackground(Landroid/graphics/drawable/Drawable;)V
iconst_1
istore 2
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/a Landroid/graphics/drawable/Drawable;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/a Landroid/graphics/drawable/Drawable;
aconst_null
invokevirtual android/graphics/drawable/Drawable/setCallback(Landroid/graphics/drawable/Drawable$Callback;)V
aload 0
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/a Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v7/internal/widget/ActionBarContainer/unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V
L0:
aload 0
aload 1
putfield android/support/v7/internal/widget/ActionBarContainer/a Landroid/graphics/drawable/Drawable;
aload 1
ifnull L1
aload 1
aload 0
invokevirtual android/graphics/drawable/Drawable/setCallback(Landroid/graphics/drawable/Drawable$Callback;)V
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/h Landroid/view/View;
ifnull L1
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/a Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/h Landroid/view/View;
invokevirtual android/view/View/getLeft()I
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/h Landroid/view/View;
invokevirtual android/view/View/getTop()I
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/h Landroid/view/View;
invokevirtual android/view/View/getRight()I
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/h Landroid/view/View;
invokevirtual android/view/View/getBottom()I
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
L1:
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/d Z
ifeq L2
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/c Landroid/graphics/drawable/Drawable;
ifnonnull L3
L4:
aload 0
iload 2
invokevirtual android/support/v7/internal/widget/ActionBarContainer/setWillNotDraw(Z)V
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContainer/invalidate()V
return
L3:
iconst_0
istore 2
goto L4
L2:
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/a Landroid/graphics/drawable/Drawable;
ifnonnull L5
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/b Landroid/graphics/drawable/Drawable;
ifnull L4
L5:
iconst_0
istore 2
goto L4
.limit locals 3
.limit stack 5
.end method

.method public setSplitBackground(Landroid/graphics/drawable/Drawable;)V
iconst_1
istore 2
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/c Landroid/graphics/drawable/Drawable;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/c Landroid/graphics/drawable/Drawable;
aconst_null
invokevirtual android/graphics/drawable/Drawable/setCallback(Landroid/graphics/drawable/Drawable$Callback;)V
aload 0
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/c Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v7/internal/widget/ActionBarContainer/unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V
L0:
aload 0
aload 1
putfield android/support/v7/internal/widget/ActionBarContainer/c Landroid/graphics/drawable/Drawable;
aload 1
ifnull L1
aload 1
aload 0
invokevirtual android/graphics/drawable/Drawable/setCallback(Landroid/graphics/drawable/Drawable$Callback;)V
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/d Z
ifeq L1
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/c Landroid/graphics/drawable/Drawable;
ifnull L1
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/c Landroid/graphics/drawable/Drawable;
iconst_0
iconst_0
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getMeasuredWidth()I
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getMeasuredHeight()I
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
L1:
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/d Z
ifeq L2
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/c Landroid/graphics/drawable/Drawable;
ifnonnull L3
L4:
aload 0
iload 2
invokevirtual android/support/v7/internal/widget/ActionBarContainer/setWillNotDraw(Z)V
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContainer/invalidate()V
return
L3:
iconst_0
istore 2
goto L4
L2:
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/a Landroid/graphics/drawable/Drawable;
ifnonnull L5
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/b Landroid/graphics/drawable/Drawable;
ifnull L4
L5:
iconst_0
istore 2
goto L4
.limit locals 3
.limit stack 5
.end method

.method public setStackedBackground(Landroid/graphics/drawable/Drawable;)V
iconst_1
istore 2
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/b Landroid/graphics/drawable/Drawable;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/b Landroid/graphics/drawable/Drawable;
aconst_null
invokevirtual android/graphics/drawable/Drawable/setCallback(Landroid/graphics/drawable/Drawable$Callback;)V
aload 0
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/b Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v7/internal/widget/ActionBarContainer/unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V
L0:
aload 0
aload 1
putfield android/support/v7/internal/widget/ActionBarContainer/b Landroid/graphics/drawable/Drawable;
aload 1
ifnull L1
aload 1
aload 0
invokevirtual android/graphics/drawable/Drawable/setCallback(Landroid/graphics/drawable/Drawable$Callback;)V
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/e Z
ifeq L1
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/b Landroid/graphics/drawable/Drawable;
ifnull L1
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/b Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/g Landroid/view/View;
invokevirtual android/view/View/getLeft()I
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/g Landroid/view/View;
invokevirtual android/view/View/getTop()I
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/g Landroid/view/View;
invokevirtual android/view/View/getRight()I
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/g Landroid/view/View;
invokevirtual android/view/View/getBottom()I
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
L1:
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/d Z
ifeq L2
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/c Landroid/graphics/drawable/Drawable;
ifnonnull L3
L4:
aload 0
iload 2
invokevirtual android/support/v7/internal/widget/ActionBarContainer/setWillNotDraw(Z)V
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarContainer/invalidate()V
return
L3:
iconst_0
istore 2
goto L4
L2:
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/a Landroid/graphics/drawable/Drawable;
ifnonnull L5
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/b Landroid/graphics/drawable/Drawable;
ifnull L4
L5:
iconst_0
istore 2
goto L4
.limit locals 3
.limit stack 5
.end method

.method public setTabContainer(Landroid/support/v7/internal/widget/al;)V
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/g Landroid/view/View;
ifnull L0
aload 0
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/g Landroid/view/View;
invokevirtual android/support/v7/internal/widget/ActionBarContainer/removeView(Landroid/view/View;)V
L0:
aload 0
aload 1
putfield android/support/v7/internal/widget/ActionBarContainer/g Landroid/view/View;
aload 1
ifnull L1
aload 0
aload 1
invokevirtual android/support/v7/internal/widget/ActionBarContainer/addView(Landroid/view/View;)V
aload 1
invokevirtual android/support/v7/internal/widget/al/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
astore 2
aload 2
iconst_m1
putfield android/view/ViewGroup$LayoutParams/width I
aload 2
bipush -2
putfield android/view/ViewGroup$LayoutParams/height I
aload 1
iconst_0
invokevirtual android/support/v7/internal/widget/al/setAllowCollapse(Z)V
L1:
return
.limit locals 3
.limit stack 2
.end method

.method public setTransitioning(Z)V
aload 0
iload 1
putfield android/support/v7/internal/widget/ActionBarContainer/f Z
iload 1
ifeq L0
ldc_w 393216
istore 2
L1:
aload 0
iload 2
invokevirtual android/support/v7/internal/widget/ActionBarContainer/setDescendantFocusability(I)V
return
L0:
ldc_w 262144
istore 2
goto L1
.limit locals 3
.limit stack 2
.end method

.method public setVisibility(I)V
aload 0
iload 1
invokespecial android/widget/FrameLayout/setVisibility(I)V
iload 1
ifne L0
iconst_1
istore 2
L1:
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/a Landroid/graphics/drawable/Drawable;
ifnull L2
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/a Landroid/graphics/drawable/Drawable;
iload 2
iconst_0
invokevirtual android/graphics/drawable/Drawable/setVisible(ZZ)Z
pop
L2:
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/b Landroid/graphics/drawable/Drawable;
ifnull L3
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/b Landroid/graphics/drawable/Drawable;
iload 2
iconst_0
invokevirtual android/graphics/drawable/Drawable/setVisible(ZZ)Z
pop
L3:
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/c Landroid/graphics/drawable/Drawable;
ifnull L4
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/c Landroid/graphics/drawable/Drawable;
iload 2
iconst_0
invokevirtual android/graphics/drawable/Drawable/setVisible(ZZ)Z
pop
L4:
return
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 3
.end method

.method public startActionModeForChild(Landroid/view/View;Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
aconst_null
areturn
.limit locals 3
.limit stack 1
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
aload 1
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/a Landroid/graphics/drawable/Drawable;
if_acmpne L0
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/d Z
ifeq L1
L0:
aload 1
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/b Landroid/graphics/drawable/Drawable;
if_acmpne L2
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/e Z
ifne L1
L2:
aload 1
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/c Landroid/graphics/drawable/Drawable;
if_acmpne L3
aload 0
getfield android/support/v7/internal/widget/ActionBarContainer/d Z
ifne L1
L3:
aload 0
aload 1
invokespecial android/widget/FrameLayout/verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
ifeq L4
L1:
iconst_1
ireturn
L4:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method
