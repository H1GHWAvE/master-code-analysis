.bytecode 50.0
.class public final synchronized android/support/v7/internal/view/menu/a
.super java/lang/Object
.implements android/support/v4/g/a/b

.field private static final 's' I = 0


.field private static final 'u' I = 1


.field private static final 'v' I = 2


.field private static final 'w' I = 4


.field private static final 'x' I = 8


.field private static final 'y' I = 16


.field private final 'f' I

.field private final 'g' I

.field private final 'h' I

.field private final 'i' I

.field private 'j' Ljava/lang/CharSequence;

.field private 'k' Ljava/lang/CharSequence;

.field private 'l' Landroid/content/Intent;

.field private 'm' C

.field private 'n' C

.field private 'o' Landroid/graphics/drawable/Drawable;

.field private 'p' I

.field private 'q' Landroid/content/Context;

.field private 'r' Landroid/view/MenuItem$OnMenuItemClickListener;

.field private 't' I

.method public <init>(Landroid/content/Context;Ljava/lang/CharSequence;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield android/support/v7/internal/view/menu/a/p I
aload 0
bipush 16
putfield android/support/v7/internal/view/menu/a/t I
aload 0
aload 1
putfield android/support/v7/internal/view/menu/a/q Landroid/content/Context;
aload 0
ldc_w 16908332
putfield android/support/v7/internal/view/menu/a/f I
aload 0
iconst_0
putfield android/support/v7/internal/view/menu/a/g I
aload 0
iconst_0
putfield android/support/v7/internal/view/menu/a/h I
aload 0
iconst_0
putfield android/support/v7/internal/view/menu/a/i I
aload 0
aload 2
putfield android/support/v7/internal/view/menu/a/j Ljava/lang/CharSequence;
return
.limit locals 3
.limit stack 2
.end method

.method private a(I)Landroid/support/v4/g/a/b;
aload 0
iload 1
invokevirtual android/support/v7/internal/view/menu/a/setShowAsAction(I)V
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Z)Landroid/support/v7/internal/view/menu/a;
aload 0
getfield android/support/v7/internal/view/menu/a/t I
istore 3
iload 1
ifeq L0
iconst_4
istore 2
L1:
aload 0
iload 2
iload 3
bipush -5
iand
ior
putfield android/support/v7/internal/view/menu/a/t I
aload 0
areturn
L0:
iconst_0
istore 2
goto L1
.limit locals 4
.limit stack 4
.end method

.method private b()Z
aload 0
getfield android/support/v7/internal/view/menu/a/r Landroid/view/MenuItem$OnMenuItemClickListener;
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/a/r Landroid/view/MenuItem$OnMenuItemClickListener;
aload 0
invokeinterface android/view/MenuItem$OnMenuItemClickListener/onMenuItemClick(Landroid/view/MenuItem;)Z 1
ifeq L0
iconst_1
ireturn
L0:
aload 0
getfield android/support/v7/internal/view/menu/a/l Landroid/content/Intent;
ifnull L1
aload 0
getfield android/support/v7/internal/view/menu/a/q Landroid/content/Context;
aload 0
getfield android/support/v7/internal/view/menu/a/l Landroid/content/Intent;
invokevirtual android/content/Context/startActivity(Landroid/content/Intent;)V
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static c()Landroid/support/v4/g/a/b;
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 0
.limit stack 2
.end method

.method private static d()Landroid/support/v4/g/a/b;
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 0
.limit stack 2
.end method

.method public final a(Landroid/support/v4/view/bf;)Landroid/support/v4/g/a/b;
aload 0
areturn
.limit locals 2
.limit stack 1
.end method

.method public final a(Landroid/support/v4/view/n;)Landroid/support/v4/g/a/b;
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public final a()Landroid/support/v4/view/n;
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method public final collapseActionView()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final expandActionView()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getActionProvider()Landroid/view/ActionProvider;
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 1
.limit stack 2
.end method

.method public final getActionView()Landroid/view/View;
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getAlphabeticShortcut()C
aload 0
getfield android/support/v7/internal/view/menu/a/n C
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getGroupId()I
aload 0
getfield android/support/v7/internal/view/menu/a/g I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getIcon()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/internal/view/menu/a/o Landroid/graphics/drawable/Drawable;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getIntent()Landroid/content/Intent;
aload 0
getfield android/support/v7/internal/view/menu/a/l Landroid/content/Intent;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getItemId()I
aload 0
getfield android/support/v7/internal/view/menu/a/f I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getNumericShortcut()C
aload 0
getfield android/support/v7/internal/view/menu/a/m C
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getOrder()I
aload 0
getfield android/support/v7/internal/view/menu/a/i I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final getSubMenu()Landroid/view/SubMenu;
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getTitle()Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/view/menu/a/j Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getTitleCondensed()Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/view/menu/a/k Ljava/lang/CharSequence;
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/a/k Ljava/lang/CharSequence;
areturn
L0:
aload 0
getfield android/support/v7/internal/view/menu/a/j Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final hasSubMenu()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final isActionViewExpanded()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final isCheckable()Z
aload 0
getfield android/support/v7/internal/view/menu/a/t I
iconst_1
iand
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final isChecked()Z
aload 0
getfield android/support/v7/internal/view/menu/a/t I
iconst_2
iand
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final isEnabled()Z
aload 0
getfield android/support/v7/internal/view/menu/a/t I
bipush 16
iand
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final isVisible()Z
aload 0
getfield android/support/v7/internal/view/menu/a/t I
bipush 8
iand
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final setActionProvider(Landroid/view/ActionProvider;)Landroid/view/MenuItem;
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public final synthetic setActionView(I)Landroid/view/MenuItem;
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public final synthetic setActionView(Landroid/view/View;)Landroid/view/MenuItem;
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public final setAlphabeticShortcut(C)Landroid/view/MenuItem;
aload 0
iload 1
putfield android/support/v7/internal/view/menu/a/n C
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final setCheckable(Z)Landroid/view/MenuItem;
aload 0
getfield android/support/v7/internal/view/menu/a/t I
istore 3
iload 1
ifeq L0
iconst_1
istore 2
L1:
aload 0
iload 2
iload 3
bipush -2
iand
ior
putfield android/support/v7/internal/view/menu/a/t I
aload 0
areturn
L0:
iconst_0
istore 2
goto L1
.limit locals 4
.limit stack 4
.end method

.method public final setChecked(Z)Landroid/view/MenuItem;
aload 0
getfield android/support/v7/internal/view/menu/a/t I
istore 3
iload 1
ifeq L0
iconst_2
istore 2
L1:
aload 0
iload 2
iload 3
bipush -3
iand
ior
putfield android/support/v7/internal/view/menu/a/t I
aload 0
areturn
L0:
iconst_0
istore 2
goto L1
.limit locals 4
.limit stack 4
.end method

.method public final setEnabled(Z)Landroid/view/MenuItem;
aload 0
getfield android/support/v7/internal/view/menu/a/t I
istore 3
iload 1
ifeq L0
bipush 16
istore 2
L1:
aload 0
iload 2
iload 3
bipush -17
iand
ior
putfield android/support/v7/internal/view/menu/a/t I
aload 0
areturn
L0:
iconst_0
istore 2
goto L1
.limit locals 4
.limit stack 4
.end method

.method public final setIcon(I)Landroid/view/MenuItem;
aload 0
iload 1
putfield android/support/v7/internal/view/menu/a/p I
aload 0
aload 0
getfield android/support/v7/internal/view/menu/a/q Landroid/content/Context;
iload 1
invokestatic android/support/v4/c/h/a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
putfield android/support/v7/internal/view/menu/a/o Landroid/graphics/drawable/Drawable;
aload 0
areturn
.limit locals 2
.limit stack 3
.end method

.method public final setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;
aload 0
aload 1
putfield android/support/v7/internal/view/menu/a/o Landroid/graphics/drawable/Drawable;
aload 0
iconst_0
putfield android/support/v7/internal/view/menu/a/p I
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;
aload 0
aload 1
putfield android/support/v7/internal/view/menu/a/l Landroid/content/Intent;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final setNumericShortcut(C)Landroid/view/MenuItem;
aload 0
iload 1
putfield android/support/v7/internal/view/menu/a/m C
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public final setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
aload 0
aload 1
putfield android/support/v7/internal/view/menu/a/r Landroid/view/MenuItem$OnMenuItemClickListener;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final setShortcut(CC)Landroid/view/MenuItem;
aload 0
iload 1
putfield android/support/v7/internal/view/menu/a/m C
aload 0
iload 2
putfield android/support/v7/internal/view/menu/a/n C
aload 0
areturn
.limit locals 3
.limit stack 2
.end method

.method public final setShowAsAction(I)V
return
.limit locals 2
.limit stack 0
.end method

.method public final synthetic setShowAsActionFlags(I)Landroid/view/MenuItem;
aload 0
iload 1
invokevirtual android/support/v7/internal/view/menu/a/setShowAsAction(I)V
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final setTitle(I)Landroid/view/MenuItem;
aload 0
aload 0
getfield android/support/v7/internal/view/menu/a/q Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getString(I)Ljava/lang/String;
putfield android/support/v7/internal/view/menu/a/j Ljava/lang/CharSequence;
aload 0
areturn
.limit locals 2
.limit stack 3
.end method

.method public final setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
aload 0
aload 1
putfield android/support/v7/internal/view/menu/a/j Ljava/lang/CharSequence;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
aload 0
aload 1
putfield android/support/v7/internal/view/menu/a/k Ljava/lang/CharSequence;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public final setVisible(Z)Landroid/view/MenuItem;
aload 0
getfield android/support/v7/internal/view/menu/a/t I
istore 3
iload 1
ifeq L0
iconst_0
istore 2
L1:
aload 0
iload 2
iload 3
bipush 8
iand
ior
putfield android/support/v7/internal/view/menu/a/t I
aload 0
areturn
L0:
bipush 8
istore 2
goto L1
.limit locals 4
.limit stack 4
.end method
