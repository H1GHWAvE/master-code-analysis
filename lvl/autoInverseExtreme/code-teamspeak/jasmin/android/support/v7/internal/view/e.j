.bytecode 50.0
.class public final synchronized android/support/v7/internal/view/e
.super java/lang/Object
.implements android/support/v7/c/b

.field final 'a' Landroid/view/ActionMode$Callback;

.field final 'b' Landroid/content/Context;

.field final 'c' Ljava/util/ArrayList;

.field final 'd' Landroid/support/v4/n/v;

.method public <init>(Landroid/content/Context;Landroid/view/ActionMode$Callback;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v7/internal/view/e/b Landroid/content/Context;
aload 0
aload 2
putfield android/support/v7/internal/view/e/a Landroid/view/ActionMode$Callback;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v7/internal/view/e/c Ljava/util/ArrayList;
aload 0
new android/support/v4/n/v
dup
invokespecial android/support/v4/n/v/<init>()V
putfield android/support/v7/internal/view/e/d Landroid/support/v4/n/v;
return
.limit locals 3
.limit stack 3
.end method

.method private a(Landroid/view/Menu;)Landroid/view/Menu;
aload 0
getfield android/support/v7/internal/view/e/d Landroid/support/v4/n/v;
aload 1
invokevirtual android/support/v4/n/v/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/view/Menu
astore 3
aload 3
astore 2
aload 3
ifnonnull L0
aload 0
getfield android/support/v7/internal/view/e/b Landroid/content/Context;
aload 1
checkcast android/support/v4/g/a/a
invokestatic android/support/v7/internal/view/menu/ab/a(Landroid/content/Context;Landroid/support/v4/g/a/a;)Landroid/view/Menu;
astore 2
aload 0
getfield android/support/v7/internal/view/e/d Landroid/support/v4/n/v;
aload 1
aload 2
invokevirtual android/support/v4/n/v/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L0:
aload 2
areturn
.limit locals 4
.limit stack 3
.end method

.method public final a(Landroid/support/v7/c/a;)V
aload 0
getfield android/support/v7/internal/view/e/a Landroid/view/ActionMode$Callback;
aload 0
aload 1
invokevirtual android/support/v7/internal/view/e/b(Landroid/support/v7/c/a;)Landroid/view/ActionMode;
invokeinterface android/view/ActionMode$Callback/onDestroyActionMode(Landroid/view/ActionMode;)V 1
return
.limit locals 2
.limit stack 3
.end method

.method public final a(Landroid/support/v7/c/a;Landroid/view/Menu;)Z
aload 0
getfield android/support/v7/internal/view/e/a Landroid/view/ActionMode$Callback;
aload 0
aload 1
invokevirtual android/support/v7/internal/view/e/b(Landroid/support/v7/c/a;)Landroid/view/ActionMode;
aload 0
aload 2
invokespecial android/support/v7/internal/view/e/a(Landroid/view/Menu;)Landroid/view/Menu;
invokeinterface android/view/ActionMode$Callback/onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z 2
ireturn
.limit locals 3
.limit stack 4
.end method

.method public final a(Landroid/support/v7/c/a;Landroid/view/MenuItem;)Z
aload 0
getfield android/support/v7/internal/view/e/a Landroid/view/ActionMode$Callback;
aload 0
aload 1
invokevirtual android/support/v7/internal/view/e/b(Landroid/support/v7/c/a;)Landroid/view/ActionMode;
aload 0
getfield android/support/v7/internal/view/e/b Landroid/content/Context;
aload 2
checkcast android/support/v4/g/a/b
invokestatic android/support/v7/internal/view/menu/ab/a(Landroid/content/Context;Landroid/support/v4/g/a/b;)Landroid/view/MenuItem;
invokeinterface android/view/ActionMode$Callback/onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z 2
ireturn
.limit locals 3
.limit stack 4
.end method

.method public final b(Landroid/support/v7/c/a;)Landroid/view/ActionMode;
aload 0
getfield android/support/v7/internal/view/e/c Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 0
getfield android/support/v7/internal/view/e/c Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/view/d
astore 4
aload 4
ifnull L2
aload 4
getfield android/support/v7/internal/view/d/b Landroid/support/v7/c/a;
aload 1
if_acmpne L2
aload 4
areturn
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
new android/support/v7/internal/view/d
dup
aload 0
getfield android/support/v7/internal/view/e/b Landroid/content/Context;
aload 1
invokespecial android/support/v7/internal/view/d/<init>(Landroid/content/Context;Landroid/support/v7/c/a;)V
astore 1
aload 0
getfield android/support/v7/internal/view/e/c Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 1
areturn
.limit locals 5
.limit stack 4
.end method

.method public final b(Landroid/support/v7/c/a;Landroid/view/Menu;)Z
aload 0
getfield android/support/v7/internal/view/e/a Landroid/view/ActionMode$Callback;
aload 0
aload 1
invokevirtual android/support/v7/internal/view/e/b(Landroid/support/v7/c/a;)Landroid/view/ActionMode;
aload 0
aload 2
invokespecial android/support/v7/internal/view/e/a(Landroid/view/Menu;)Landroid/view/Menu;
invokeinterface android/view/ActionMode$Callback/onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z 2
ireturn
.limit locals 3
.limit stack 4
.end method
