.bytecode 50.0
.class public final synchronized android/support/v7/internal/widget/ay
.super java/lang/Object
.implements android/support/v7/internal/widget/ad

.field private static final 'e' Ljava/lang/String; = "ToolbarWidgetWrapper"

.field private static final 'f' I = 3


.field private static final 'g' J = 200L


.field 'a' Landroid/support/v7/widget/Toolbar;

.field 'b' Ljava/lang/CharSequence;

.field 'c' Landroid/view/Window$Callback;

.field 'd' Z

.field private 'h' I

.field private 'i' Landroid/view/View;

.field private 'j' Landroid/widget/Spinner;

.field private 'k' Landroid/view/View;

.field private 'l' Landroid/graphics/drawable/Drawable;

.field private 'm' Landroid/graphics/drawable/Drawable;

.field private 'n' Landroid/graphics/drawable/Drawable;

.field private 'o' Z

.field private 'p' Ljava/lang/CharSequence;

.field private 'q' Ljava/lang/CharSequence;

.field private 'r' Landroid/support/v7/widget/ActionMenuPresenter;

.field private 's' I

.field private final 't' Landroid/support/v7/internal/widget/av;

.field private 'u' I

.field private 'v' Landroid/graphics/drawable/Drawable;

.method public <init>(Landroid/support/v7/widget/Toolbar;Z)V
aload 0
aload 1
iload 2
getstatic android/support/v7/a/l/abc_action_bar_up_description I
getstatic android/support/v7/a/h/abc_ic_ab_back_mtrl_am_alpha I
invokespecial android/support/v7/internal/widget/ay/<init>(Landroid/support/v7/widget/Toolbar;ZII)V
return
.limit locals 3
.limit stack 5
.end method

.method private <init>(Landroid/support/v7/widget/Toolbar;ZII)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield android/support/v7/internal/widget/ay/s I
aload 0
iconst_0
putfield android/support/v7/internal/widget/ay/u I
aload 0
aload 1
putfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
aload 0
aload 1
invokevirtual android/support/v7/widget/Toolbar/getTitle()Ljava/lang/CharSequence;
putfield android/support/v7/internal/widget/ay/b Ljava/lang/CharSequence;
aload 0
aload 1
invokevirtual android/support/v7/widget/Toolbar/getSubtitle()Ljava/lang/CharSequence;
putfield android/support/v7/internal/widget/ay/p Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/widget/ay/b Ljava/lang/CharSequence;
ifnull L0
iconst_1
istore 7
L1:
aload 0
iload 7
putfield android/support/v7/internal/widget/ay/o Z
aload 0
aload 1
invokevirtual android/support/v7/widget/Toolbar/getNavigationIcon()Landroid/graphics/drawable/Drawable;
putfield android/support/v7/internal/widget/ay/n Landroid/graphics/drawable/Drawable;
iload 2
ifeq L2
aload 1
invokevirtual android/support/v7/widget/Toolbar/getContext()Landroid/content/Context;
aconst_null
getstatic android/support/v7/a/n/ActionBar [I
getstatic android/support/v7/a/d/actionBarStyle I
invokestatic android/support/v7/internal/widget/ax/a(Landroid/content/Context;Landroid/util/AttributeSet;[II)Landroid/support/v7/internal/widget/ax;
astore 1
aload 1
getstatic android/support/v7/a/n/ActionBar_title I
invokevirtual android/support/v7/internal/widget/ax/c(I)Ljava/lang/CharSequence;
astore 8
aload 8
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L3
aload 0
aload 8
invokevirtual android/support/v7/internal/widget/ay/b(Ljava/lang/CharSequence;)V
L3:
aload 1
getstatic android/support/v7/a/n/ActionBar_subtitle I
invokevirtual android/support/v7/internal/widget/ax/c(I)Ljava/lang/CharSequence;
astore 8
aload 8
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L4
aload 0
aload 8
invokevirtual android/support/v7/internal/widget/ay/c(Ljava/lang/CharSequence;)V
L4:
aload 1
getstatic android/support/v7/a/n/ActionBar_logo I
invokevirtual android/support/v7/internal/widget/ax/a(I)Landroid/graphics/drawable/Drawable;
astore 8
aload 8
ifnull L5
aload 0
aload 8
invokevirtual android/support/v7/internal/widget/ay/b(Landroid/graphics/drawable/Drawable;)V
L5:
aload 1
getstatic android/support/v7/a/n/ActionBar_icon I
invokevirtual android/support/v7/internal/widget/ax/a(I)Landroid/graphics/drawable/Drawable;
astore 8
aload 0
getfield android/support/v7/internal/widget/ay/n Landroid/graphics/drawable/Drawable;
ifnonnull L6
aload 8
ifnull L6
aload 0
aload 8
invokevirtual android/support/v7/internal/widget/ay/a(Landroid/graphics/drawable/Drawable;)V
L6:
aload 1
getstatic android/support/v7/a/n/ActionBar_homeAsUpIndicator I
invokevirtual android/support/v7/internal/widget/ax/a(I)Landroid/graphics/drawable/Drawable;
astore 8
aload 8
ifnull L7
aload 0
aload 8
invokevirtual android/support/v7/internal/widget/ay/c(Landroid/graphics/drawable/Drawable;)V
L7:
aload 0
aload 1
getstatic android/support/v7/a/n/ActionBar_displayOptions I
iconst_0
invokevirtual android/support/v7/internal/widget/ax/a(II)I
invokevirtual android/support/v7/internal/widget/ay/c(I)V
aload 1
getstatic android/support/v7/a/n/ActionBar_customNavigationLayout I
iconst_0
invokevirtual android/support/v7/internal/widget/ax/e(II)I
istore 5
iload 5
ifeq L8
aload 0
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
invokevirtual android/support/v7/widget/Toolbar/getContext()Landroid/content/Context;
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
iload 5
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
invokevirtual android/support/v7/internal/widget/ay/a(Landroid/view/View;)V
aload 0
aload 0
getfield android/support/v7/internal/widget/ay/h I
bipush 16
ior
invokevirtual android/support/v7/internal/widget/ay/c(I)V
L8:
aload 1
getstatic android/support/v7/a/n/ActionBar_height I
iconst_0
invokevirtual android/support/v7/internal/widget/ax/d(II)I
istore 5
iload 5
ifle L9
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
invokevirtual android/support/v7/widget/Toolbar/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
astore 8
aload 8
iload 5
putfield android/view/ViewGroup$LayoutParams/height I
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
aload 8
invokevirtual android/support/v7/widget/Toolbar/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
L9:
aload 1
getstatic android/support/v7/a/n/ActionBar_contentInsetStart I
iconst_m1
invokevirtual android/support/v7/internal/widget/ax/b(II)I
istore 6
aload 1
getstatic android/support/v7/a/n/ActionBar_contentInsetEnd I
iconst_m1
invokevirtual android/support/v7/internal/widget/ax/b(II)I
istore 5
iload 6
ifge L10
iload 5
iflt L11
L10:
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
astore 8
iload 6
iconst_0
invokestatic java/lang/Math/max(II)I
istore 6
iload 5
iconst_0
invokestatic java/lang/Math/max(II)I
istore 5
aload 8
getfield android/support/v7/widget/Toolbar/i Landroid/support/v7/internal/widget/ak;
iload 6
iload 5
invokevirtual android/support/v7/internal/widget/ak/a(II)V
L11:
aload 1
getstatic android/support/v7/a/n/ActionBar_titleTextStyle I
iconst_0
invokevirtual android/support/v7/internal/widget/ax/e(II)I
istore 5
iload 5
ifeq L12
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
astore 8
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
invokevirtual android/support/v7/widget/Toolbar/getContext()Landroid/content/Context;
astore 9
aload 8
iload 5
putfield android/support/v7/widget/Toolbar/g I
aload 8
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
ifnull L12
aload 8
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
aload 9
iload 5
invokevirtual android/widget/TextView/setTextAppearance(Landroid/content/Context;I)V
L12:
aload 1
getstatic android/support/v7/a/n/ActionBar_subtitleTextStyle I
iconst_0
invokevirtual android/support/v7/internal/widget/ax/e(II)I
istore 5
iload 5
ifeq L13
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
astore 8
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
invokevirtual android/support/v7/widget/Toolbar/getContext()Landroid/content/Context;
astore 9
aload 8
iload 5
putfield android/support/v7/widget/Toolbar/h I
aload 8
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
ifnull L13
aload 8
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
aload 9
iload 5
invokevirtual android/widget/TextView/setTextAppearance(Landroid/content/Context;I)V
L13:
aload 1
getstatic android/support/v7/a/n/ActionBar_popupTheme I
iconst_0
invokevirtual android/support/v7/internal/widget/ax/e(II)I
istore 5
iload 5
ifeq L14
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
iload 5
invokevirtual android/support/v7/widget/Toolbar/setPopupTheme(I)V
L14:
aload 1
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
invokevirtual android/content/res/TypedArray/recycle()V
aload 0
aload 1
invokevirtual android/support/v7/internal/widget/ax/a()Landroid/support/v7/internal/widget/av;
putfield android/support/v7/internal/widget/ay/t Landroid/support/v7/internal/widget/av;
L15:
iload 3
aload 0
getfield android/support/v7/internal/widget/ay/u I
if_icmpeq L16
aload 0
iload 3
putfield android/support/v7/internal/widget/ay/u I
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
invokevirtual android/support/v7/widget/Toolbar/getNavigationContentDescription()Ljava/lang/CharSequence;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L16
aload 0
aload 0
getfield android/support/v7/internal/widget/ay/u I
invokevirtual android/support/v7/internal/widget/ay/h(I)V
L16:
aload 0
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
invokevirtual android/support/v7/widget/Toolbar/getNavigationContentDescription()Ljava/lang/CharSequence;
putfield android/support/v7/internal/widget/ay/q Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/widget/ay/t Landroid/support/v7/internal/widget/av;
iload 4
iconst_0
invokevirtual android/support/v7/internal/widget/av/a(IZ)Landroid/graphics/drawable/Drawable;
astore 1
aload 0
getfield android/support/v7/internal/widget/ay/v Landroid/graphics/drawable/Drawable;
aload 1
if_acmpeq L17
aload 0
aload 1
putfield android/support/v7/internal/widget/ay/v Landroid/graphics/drawable/Drawable;
aload 0
invokespecial android/support/v7/internal/widget/ay/G()V
L17:
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
new android/support/v7/internal/widget/az
dup
aload 0
invokespecial android/support/v7/internal/widget/az/<init>(Landroid/support/v7/internal/widget/ay;)V
invokevirtual android/support/v7/widget/Toolbar/setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V
return
L0:
iconst_0
istore 7
goto L1
L2:
bipush 11
istore 5
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
invokevirtual android/support/v7/widget/Toolbar/getNavigationIcon()Landroid/graphics/drawable/Drawable;
ifnull L18
bipush 15
istore 5
L18:
aload 0
iload 5
putfield android/support/v7/internal/widget/ay/h I
aload 0
aload 1
invokevirtual android/support/v7/widget/Toolbar/getContext()Landroid/content/Context;
invokestatic android/support/v7/internal/widget/av/a(Landroid/content/Context;)Landroid/support/v7/internal/widget/av;
putfield android/support/v7/internal/widget/ay/t Landroid/support/v7/internal/widget/av;
goto L15
.limit locals 10
.limit stack 5
.end method

.method private C()I
bipush 11
istore 1
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
invokevirtual android/support/v7/widget/Toolbar/getNavigationIcon()Landroid/graphics/drawable/Drawable;
ifnull L0
bipush 15
istore 1
L0:
iload 1
ireturn
.limit locals 2
.limit stack 1
.end method

.method private D()V
aconst_null
astore 1
aload 0
getfield android/support/v7/internal/widget/ay/h I
iconst_2
iand
ifeq L0
aload 0
getfield android/support/v7/internal/widget/ay/h I
iconst_1
iand
ifeq L1
aload 0
getfield android/support/v7/internal/widget/ay/m Landroid/graphics/drawable/Drawable;
ifnull L2
aload 0
getfield android/support/v7/internal/widget/ay/m Landroid/graphics/drawable/Drawable;
astore 1
L0:
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
aload 1
invokevirtual android/support/v7/widget/Toolbar/setLogo(Landroid/graphics/drawable/Drawable;)V
return
L2:
aload 0
getfield android/support/v7/internal/widget/ay/l Landroid/graphics/drawable/Drawable;
astore 1
goto L0
L1:
aload 0
getfield android/support/v7/internal/widget/ay/l Landroid/graphics/drawable/Drawable;
astore 1
goto L0
.limit locals 2
.limit stack 2
.end method

.method private E()V
aload 0
getfield android/support/v7/internal/widget/ay/j Landroid/widget/Spinner;
ifnonnull L0
aload 0
new android/support/v7/widget/aa
dup
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
invokevirtual android/support/v7/widget/Toolbar/getContext()Landroid/content/Context;
aconst_null
getstatic android/support/v7/a/d/actionDropDownStyle I
invokespecial android/support/v7/widget/aa/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
putfield android/support/v7/internal/widget/ay/j Landroid/widget/Spinner;
new android/support/v7/widget/ck
dup
bipush -2
ldc_w 8388627
invokespecial android/support/v7/widget/ck/<init>(II)V
astore 1
aload 0
getfield android/support/v7/internal/widget/ay/j Landroid/widget/Spinner;
aload 1
invokevirtual android/widget/Spinner/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
L0:
return
.limit locals 2
.limit stack 6
.end method

.method private F()V
aload 0
getfield android/support/v7/internal/widget/ay/h I
iconst_4
iand
ifeq L0
aload 0
getfield android/support/v7/internal/widget/ay/q Ljava/lang/CharSequence;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L1
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
aload 0
getfield android/support/v7/internal/widget/ay/u I
invokevirtual android/support/v7/widget/Toolbar/setNavigationContentDescription(I)V
L0:
return
L1:
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
aload 0
getfield android/support/v7/internal/widget/ay/q Ljava/lang/CharSequence;
invokevirtual android/support/v7/widget/Toolbar/setNavigationContentDescription(Ljava/lang/CharSequence;)V
return
.limit locals 1
.limit stack 2
.end method

.method private G()V
aload 0
getfield android/support/v7/internal/widget/ay/h I
iconst_4
iand
ifeq L0
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
astore 2
aload 0
getfield android/support/v7/internal/widget/ay/n Landroid/graphics/drawable/Drawable;
ifnull L1
aload 0
getfield android/support/v7/internal/widget/ay/n Landroid/graphics/drawable/Drawable;
astore 1
L2:
aload 2
aload 1
invokevirtual android/support/v7/widget/Toolbar/setNavigationIcon(Landroid/graphics/drawable/Drawable;)V
L0:
return
L1:
aload 0
getfield android/support/v7/internal/widget/ay/v Landroid/graphics/drawable/Drawable;
astore 1
goto L2
.limit locals 3
.limit stack 2
.end method

.method private static synthetic a(Landroid/support/v7/internal/widget/ay;)Landroid/support/v7/widget/Toolbar;
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic b(Landroid/support/v7/internal/widget/ay;)Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/widget/ay/b Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic c(Landroid/support/v7/internal/widget/ay;)Landroid/view/Window$Callback;
aload 0
getfield android/support/v7/internal/widget/ay/c Landroid/view/Window$Callback;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic d(Landroid/support/v7/internal/widget/ay;)Z
aload 0
getfield android/support/v7/internal/widget/ay/d Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private e(Ljava/lang/CharSequence;)V
aload 0
aload 1
putfield android/support/v7/internal/widget/ay/b Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/widget/ay/h I
bipush 8
iand
ifeq L0
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
aload 1
invokevirtual android/support/v7/widget/Toolbar/setTitle(Ljava/lang/CharSequence;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final A()I
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
invokevirtual android/support/v7/widget/Toolbar/getVisibility()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final B()Landroid/view/Menu;
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
invokevirtual android/support/v7/widget/Toolbar/getMenu()Landroid/view/Menu;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(IJ)Landroid/support/v4/view/fk;
iload 1
bipush 8
if_icmpne L0
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
invokestatic android/support/v4/view/cx/p(Landroid/view/View;)Landroid/support/v4/view/fk;
fconst_0
invokevirtual android/support/v4/view/fk/a(F)Landroid/support/v4/view/fk;
astore 4
aload 4
lload 2
invokevirtual android/support/v4/view/fk/a(J)Landroid/support/v4/view/fk;
pop
aload 4
new android/support/v7/internal/widget/ba
dup
aload 0
invokespecial android/support/v7/internal/widget/ba/<init>(Landroid/support/v7/internal/widget/ay;)V
invokevirtual android/support/v4/view/fk/a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;
pop
aload 4
areturn
L0:
iload 1
ifne L1
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
invokestatic android/support/v4/view/cx/p(Landroid/view/View;)Landroid/support/v4/view/fk;
fconst_1
invokevirtual android/support/v4/view/fk/a(F)Landroid/support/v4/view/fk;
astore 4
aload 4
lload 2
invokevirtual android/support/v4/view/fk/a(J)Landroid/support/v4/view/fk;
pop
aload 4
new android/support/v7/internal/widget/bb
dup
aload 0
invokespecial android/support/v7/internal/widget/bb/<init>(Landroid/support/v7/internal/widget/ay;)V
invokevirtual android/support/v4/view/fk/a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;
pop
aload 4
areturn
L1:
aconst_null
areturn
.limit locals 5
.limit stack 4
.end method

.method public final a()Landroid/view/ViewGroup;
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(I)V
iload 1
ifeq L0
aload 0
getfield android/support/v7/internal/widget/ay/t Landroid/support/v7/internal/widget/av;
iload 1
iconst_0
invokevirtual android/support/v7/internal/widget/av/a(IZ)Landroid/graphics/drawable/Drawable;
astore 2
L1:
aload 0
aload 2
invokevirtual android/support/v7/internal/widget/ay/a(Landroid/graphics/drawable/Drawable;)V
return
L0:
aconst_null
astore 2
goto L1
.limit locals 3
.limit stack 3
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
aload 0
aload 1
putfield android/support/v7/internal/widget/ay/l Landroid/graphics/drawable/Drawable;
aload 0
invokespecial android/support/v7/internal/widget/ay/D()V
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/support/v7/internal/view/menu/y;Landroid/support/v7/internal/view/menu/j;)V
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
astore 3
aload 3
aload 1
putfield android/support/v7/widget/Toolbar/m Landroid/support/v7/internal/view/menu/y;
aload 3
aload 2
putfield android/support/v7/widget/Toolbar/n Landroid/support/v7/internal/view/menu/j;
return
.limit locals 4
.limit stack 2
.end method

.method public final a(Landroid/support/v7/internal/widget/al;)V
aload 0
getfield android/support/v7/internal/widget/ay/i Landroid/view/View;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/ay/i Landroid/view/View;
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
if_acmpne L0
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
aload 0
getfield android/support/v7/internal/widget/ay/i Landroid/view/View;
invokevirtual android/support/v7/widget/Toolbar/removeView(Landroid/view/View;)V
L0:
aload 0
aload 1
putfield android/support/v7/internal/widget/ay/i Landroid/view/View;
aload 1
ifnull L1
aload 0
getfield android/support/v7/internal/widget/ay/s I
iconst_2
if_icmpne L1
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
aload 0
getfield android/support/v7/internal/widget/ay/i Landroid/view/View;
iconst_0
invokevirtual android/support/v7/widget/Toolbar/addView(Landroid/view/View;I)V
aload 0
getfield android/support/v7/internal/widget/ay/i Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/ck
astore 2
aload 2
bipush -2
putfield android/support/v7/widget/ck/width I
aload 2
bipush -2
putfield android/support/v7/widget/ck/height I
aload 2
ldc_w 8388691
putfield android/support/v7/widget/ck/a I
aload 1
iconst_1
invokevirtual android/support/v7/internal/widget/al/setAllowCollapse(Z)V
L1:
return
.limit locals 3
.limit stack 3
.end method

.method public final a(Landroid/util/SparseArray;)V
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
aload 1
invokevirtual android/support/v7/widget/Toolbar/saveHierarchyState(Landroid/util/SparseArray;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/view/Menu;Landroid/support/v7/internal/view/menu/y;)V
aload 0
getfield android/support/v7/internal/widget/ay/r Landroid/support/v7/widget/ActionMenuPresenter;
ifnonnull L0
aload 0
new android/support/v7/widget/ActionMenuPresenter
dup
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
invokevirtual android/support/v7/widget/Toolbar/getContext()Landroid/content/Context;
invokespecial android/support/v7/widget/ActionMenuPresenter/<init>(Landroid/content/Context;)V
putfield android/support/v7/internal/widget/ay/r Landroid/support/v7/widget/ActionMenuPresenter;
aload 0
getfield android/support/v7/internal/widget/ay/r Landroid/support/v7/widget/ActionMenuPresenter;
getstatic android/support/v7/a/i/action_menu_presenter I
putfield android/support/v7/internal/view/menu/d/h I
L0:
aload 0
getfield android/support/v7/internal/widget/ay/r Landroid/support/v7/widget/ActionMenuPresenter;
aload 2
putfield android/support/v7/internal/view/menu/d/f Landroid/support/v7/internal/view/menu/y;
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
astore 2
aload 1
checkcast android/support/v7/internal/view/menu/i
astore 1
aload 0
getfield android/support/v7/internal/widget/ay/r Landroid/support/v7/widget/ActionMenuPresenter;
astore 3
aload 1
ifnonnull L1
aload 2
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
ifnull L2
L1:
aload 2
invokevirtual android/support/v7/widget/Toolbar/d()V
aload 2
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
getfield android/support/v7/widget/ActionMenuView/c Landroid/support/v7/internal/view/menu/i;
astore 4
aload 4
aload 1
if_acmpeq L2
aload 4
ifnull L3
aload 4
aload 2
getfield android/support/v7/widget/Toolbar/k Landroid/support/v7/widget/ActionMenuPresenter;
invokevirtual android/support/v7/internal/view/menu/i/b(Landroid/support/v7/internal/view/menu/x;)V
aload 4
aload 2
getfield android/support/v7/widget/Toolbar/l Landroid/support/v7/widget/cj;
invokevirtual android/support/v7/internal/view/menu/i/b(Landroid/support/v7/internal/view/menu/x;)V
L3:
aload 2
getfield android/support/v7/widget/Toolbar/l Landroid/support/v7/widget/cj;
ifnonnull L4
aload 2
new android/support/v7/widget/cj
dup
aload 2
iconst_0
invokespecial android/support/v7/widget/cj/<init>(Landroid/support/v7/widget/Toolbar;B)V
putfield android/support/v7/widget/Toolbar/l Landroid/support/v7/widget/cj;
L4:
aload 3
iconst_1
putfield android/support/v7/widget/ActionMenuPresenter/o Z
aload 1
ifnull L5
aload 1
aload 3
aload 2
getfield android/support/v7/widget/Toolbar/e Landroid/content/Context;
invokevirtual android/support/v7/internal/view/menu/i/a(Landroid/support/v7/internal/view/menu/x;Landroid/content/Context;)V
aload 1
aload 2
getfield android/support/v7/widget/Toolbar/l Landroid/support/v7/widget/cj;
aload 2
getfield android/support/v7/widget/Toolbar/e Landroid/content/Context;
invokevirtual android/support/v7/internal/view/menu/i/a(Landroid/support/v7/internal/view/menu/x;Landroid/content/Context;)V
L6:
aload 2
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
aload 2
getfield android/support/v7/widget/Toolbar/f I
invokevirtual android/support/v7/widget/ActionMenuView/setPopupTheme(I)V
aload 2
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
aload 3
invokevirtual android/support/v7/widget/ActionMenuView/setPresenter(Landroid/support/v7/widget/ActionMenuPresenter;)V
aload 2
aload 3
putfield android/support/v7/widget/Toolbar/k Landroid/support/v7/widget/ActionMenuPresenter;
L2:
return
L5:
aload 3
aload 2
getfield android/support/v7/widget/Toolbar/e Landroid/content/Context;
aconst_null
invokevirtual android/support/v7/widget/ActionMenuPresenter/a(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;)V
aload 2
getfield android/support/v7/widget/Toolbar/l Landroid/support/v7/widget/cj;
aload 2
getfield android/support/v7/widget/Toolbar/e Landroid/content/Context;
aconst_null
invokevirtual android/support/v7/widget/cj/a(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;)V
aload 3
iconst_1
invokevirtual android/support/v7/widget/ActionMenuPresenter/a(Z)V
aload 2
getfield android/support/v7/widget/Toolbar/l Landroid/support/v7/widget/cj;
iconst_1
invokevirtual android/support/v7/widget/cj/a(Z)V
goto L6
.limit locals 5
.limit stack 5
.end method

.method public final a(Landroid/view/View;)V
aload 0
getfield android/support/v7/internal/widget/ay/k Landroid/view/View;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/ay/h I
bipush 16
iand
ifeq L0
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
aload 0
getfield android/support/v7/internal/widget/ay/k Landroid/view/View;
invokevirtual android/support/v7/widget/Toolbar/removeView(Landroid/view/View;)V
L0:
aload 0
aload 1
putfield android/support/v7/internal/widget/ay/k Landroid/view/View;
aload 1
ifnull L1
aload 0
getfield android/support/v7/internal/widget/ay/h I
bipush 16
iand
ifeq L1
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
aload 0
getfield android/support/v7/internal/widget/ay/k Landroid/view/View;
invokevirtual android/support/v7/widget/Toolbar/addView(Landroid/view/View;)V
L1:
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/view/Window$Callback;)V
aload 0
aload 1
putfield android/support/v7/internal/widget/ay/c Landroid/view/Window$Callback;
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/widget/SpinnerAdapter;Landroid/widget/AdapterView$OnItemSelectedListener;)V
aload 0
invokespecial android/support/v7/internal/widget/ay/E()V
aload 0
getfield android/support/v7/internal/widget/ay/j Landroid/widget/Spinner;
aload 1
invokevirtual android/widget/Spinner/setAdapter(Landroid/widget/SpinnerAdapter;)V
aload 0
getfield android/support/v7/internal/widget/ay/j Landroid/widget/Spinner;
aload 2
invokevirtual android/widget/Spinner/setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
return
.limit locals 3
.limit stack 2
.end method

.method public final a(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v7/internal/widget/ay/o Z
ifne L0
aload 0
aload 1
invokespecial android/support/v7/internal/widget/ay/e(Ljava/lang/CharSequence;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Z)V
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
iload 1
invokevirtual android/support/v7/widget/Toolbar/setCollapsible(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method public final b()Landroid/content/Context;
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
invokevirtual android/support/v7/widget/Toolbar/getContext()Landroid/content/Context;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final b(I)V
iload 1
ifeq L0
aload 0
getfield android/support/v7/internal/widget/ay/t Landroid/support/v7/internal/widget/av;
iload 1
iconst_0
invokevirtual android/support/v7/internal/widget/av/a(IZ)Landroid/graphics/drawable/Drawable;
astore 2
L1:
aload 0
aload 2
invokevirtual android/support/v7/internal/widget/ay/b(Landroid/graphics/drawable/Drawable;)V
return
L0:
aconst_null
astore 2
goto L1
.limit locals 3
.limit stack 3
.end method

.method public final b(Landroid/graphics/drawable/Drawable;)V
aload 0
aload 1
putfield android/support/v7/internal/widget/ay/m Landroid/graphics/drawable/Drawable;
aload 0
invokespecial android/support/v7/internal/widget/ay/D()V
return
.limit locals 2
.limit stack 2
.end method

.method public final b(Landroid/util/SparseArray;)V
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
aload 1
invokevirtual android/support/v7/widget/Toolbar/restoreHierarchyState(Landroid/util/SparseArray;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final b(Ljava/lang/CharSequence;)V
aload 0
iconst_1
putfield android/support/v7/internal/widget/ay/o Z
aload 0
aload 1
invokespecial android/support/v7/internal/widget/ay/e(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final c(I)V
aload 0
getfield android/support/v7/internal/widget/ay/h I
iload 1
ixor
istore 2
aload 0
iload 1
putfield android/support/v7/internal/widget/ay/h I
iload 2
ifeq L0
iload 2
iconst_4
iand
ifeq L1
iload 1
iconst_4
iand
ifeq L2
aload 0
invokespecial android/support/v7/internal/widget/ay/G()V
aload 0
invokespecial android/support/v7/internal/widget/ay/F()V
L1:
iload 2
iconst_3
iand
ifeq L3
aload 0
invokespecial android/support/v7/internal/widget/ay/D()V
L3:
iload 2
bipush 8
iand
ifeq L4
iload 1
bipush 8
iand
ifeq L5
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
aload 0
getfield android/support/v7/internal/widget/ay/b Ljava/lang/CharSequence;
invokevirtual android/support/v7/widget/Toolbar/setTitle(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
aload 0
getfield android/support/v7/internal/widget/ay/p Ljava/lang/CharSequence;
invokevirtual android/support/v7/widget/Toolbar/setSubtitle(Ljava/lang/CharSequence;)V
L4:
iload 2
bipush 16
iand
ifeq L0
aload 0
getfield android/support/v7/internal/widget/ay/k Landroid/view/View;
ifnull L0
iload 1
bipush 16
iand
ifeq L6
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
aload 0
getfield android/support/v7/internal/widget/ay/k Landroid/view/View;
invokevirtual android/support/v7/widget/Toolbar/addView(Landroid/view/View;)V
L0:
return
L2:
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
aconst_null
invokevirtual android/support/v7/widget/Toolbar/setNavigationIcon(Landroid/graphics/drawable/Drawable;)V
goto L1
L5:
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
aconst_null
invokevirtual android/support/v7/widget/Toolbar/setTitle(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
aconst_null
invokevirtual android/support/v7/widget/Toolbar/setSubtitle(Ljava/lang/CharSequence;)V
goto L4
L6:
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
aload 0
getfield android/support/v7/internal/widget/ay/k Landroid/view/View;
invokevirtual android/support/v7/widget/Toolbar/removeView(Landroid/view/View;)V
return
.limit locals 3
.limit stack 2
.end method

.method public final c(Landroid/graphics/drawable/Drawable;)V
aload 0
aload 1
putfield android/support/v7/internal/widget/ay/n Landroid/graphics/drawable/Drawable;
aload 0
invokespecial android/support/v7/internal/widget/ay/G()V
return
.limit locals 2
.limit stack 2
.end method

.method public final c(Ljava/lang/CharSequence;)V
aload 0
aload 1
putfield android/support/v7/internal/widget/ay/p Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/widget/ay/h I
bipush 8
iand
ifeq L0
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
aload 1
invokevirtual android/support/v7/widget/Toolbar/setSubtitle(Ljava/lang/CharSequence;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final c()Z
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
astore 1
aload 1
getfield android/support/v7/widget/Toolbar/l Landroid/support/v7/widget/cj;
ifnull L0
aload 1
getfield android/support/v7/widget/Toolbar/l Landroid/support/v7/widget/cj;
getfield android/support/v7/widget/cj/b Landroid/support/v7/internal/view/menu/m;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final d()V
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
invokevirtual android/support/v7/widget/Toolbar/c()V
return
.limit locals 1
.limit stack 1
.end method

.method public final d(I)V
aload 0
getfield android/support/v7/internal/widget/ay/s I
istore 2
iload 1
iload 2
if_icmpeq L0
iload 2
tableswitch 1
L1
L2
default : L3
L3:
aload 0
iload 1
putfield android/support/v7/internal/widget/ay/s I
iload 1
tableswitch 0
L0
L4
L5
default : L6
L6:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "Invalid navigation mode "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
getfield android/support/v7/internal/widget/ay/j Landroid/widget/Spinner;
ifnull L3
aload 0
getfield android/support/v7/internal/widget/ay/j Landroid/widget/Spinner;
invokevirtual android/widget/Spinner/getParent()Landroid/view/ViewParent;
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
if_acmpne L3
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
aload 0
getfield android/support/v7/internal/widget/ay/j Landroid/widget/Spinner;
invokevirtual android/support/v7/widget/Toolbar/removeView(Landroid/view/View;)V
goto L3
L2:
aload 0
getfield android/support/v7/internal/widget/ay/i Landroid/view/View;
ifnull L3
aload 0
getfield android/support/v7/internal/widget/ay/i Landroid/view/View;
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
if_acmpne L3
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
aload 0
getfield android/support/v7/internal/widget/ay/i Landroid/view/View;
invokevirtual android/support/v7/widget/Toolbar/removeView(Landroid/view/View;)V
goto L3
L4:
aload 0
invokespecial android/support/v7/internal/widget/ay/E()V
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
aload 0
getfield android/support/v7/internal/widget/ay/j Landroid/widget/Spinner;
iconst_0
invokevirtual android/support/v7/widget/Toolbar/addView(Landroid/view/View;I)V
L0:
return
L5:
aload 0
getfield android/support/v7/internal/widget/ay/i Landroid/view/View;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
aload 0
getfield android/support/v7/internal/widget/ay/i Landroid/view/View;
iconst_0
invokevirtual android/support/v7/widget/Toolbar/addView(Landroid/view/View;I)V
aload 0
getfield android/support/v7/internal/widget/ay/i Landroid/view/View;
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/ck
astore 3
aload 3
bipush -2
putfield android/support/v7/widget/ck/width I
aload 3
bipush -2
putfield android/support/v7/widget/ck/height I
aload 3
ldc_w 8388691
putfield android/support/v7/widget/ck/a I
return
.limit locals 4
.limit stack 5
.end method

.method public final d(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield android/support/v7/internal/widget/ay/v Landroid/graphics/drawable/Drawable;
aload 1
if_acmpeq L0
aload 0
aload 1
putfield android/support/v7/internal/widget/ay/v Landroid/graphics/drawable/Drawable;
aload 0
invokespecial android/support/v7/internal/widget/ay/G()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final d(Ljava/lang/CharSequence;)V
aload 0
aload 1
putfield android/support/v7/internal/widget/ay/q Ljava/lang/CharSequence;
aload 0
invokespecial android/support/v7/internal/widget/ay/F()V
return
.limit locals 2
.limit stack 2
.end method

.method public final e()Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
invokevirtual android/support/v7/widget/Toolbar/getTitle()Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final e(I)V
aload 0
getfield android/support/v7/internal/widget/ay/j Landroid/widget/Spinner;
ifnonnull L0
new java/lang/IllegalStateException
dup
ldc "Can't set dropdown selected position without an adapter"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v7/internal/widget/ay/j Landroid/widget/Spinner;
iload 1
invokevirtual android/widget/Spinner/setSelection(I)V
return
.limit locals 2
.limit stack 3
.end method

.method public final e(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
aload 1
invokevirtual android/support/v7/widget/Toolbar/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final f()Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
invokevirtual android/support/v7/widget/Toolbar/getSubtitle()Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final f(I)V
aload 0
iload 1
ldc2_w 200L
invokevirtual android/support/v7/internal/widget/ay/a(IJ)Landroid/support/v4/view/fk;
astore 2
aload 2
ifnull L0
aload 2
invokevirtual android/support/v4/view/fk/b()V
L0:
return
.limit locals 3
.limit stack 4
.end method

.method public final g()V
ldc "ToolbarWidgetWrapper"
ldc "Progress display unsupported"
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;)I
pop
return
.limit locals 1
.limit stack 2
.end method

.method public final g(I)V
iload 1
ifeq L0
aload 0
getfield android/support/v7/internal/widget/ay/t Landroid/support/v7/internal/widget/av;
iload 1
iconst_0
invokevirtual android/support/v7/internal/widget/av/a(IZ)Landroid/graphics/drawable/Drawable;
astore 2
L1:
aload 0
aload 2
invokevirtual android/support/v7/internal/widget/ay/c(Landroid/graphics/drawable/Drawable;)V
return
L0:
aconst_null
astore 2
goto L1
.limit locals 3
.limit stack 3
.end method

.method public final h()V
ldc "ToolbarWidgetWrapper"
ldc "Progress display unsupported"
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;)I
pop
return
.limit locals 1
.limit stack 2
.end method

.method public final h(I)V
iload 1
ifne L0
aconst_null
astore 2
L1:
aload 0
aload 2
invokevirtual android/support/v7/internal/widget/ay/d(Ljava/lang/CharSequence;)V
return
L0:
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
invokevirtual android/support/v7/widget/Toolbar/getContext()Landroid/content/Context;
iload 1
invokevirtual android/content/Context/getString(I)Ljava/lang/String;
astore 2
goto L1
.limit locals 3
.limit stack 2
.end method

.method public final i(I)V
iload 1
aload 0
getfield android/support/v7/internal/widget/ay/u I
if_icmpne L0
L1:
return
L0:
aload 0
iload 1
putfield android/support/v7/internal/widget/ay/u I
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
invokevirtual android/support/v7/widget/Toolbar/getNavigationContentDescription()Ljava/lang/CharSequence;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L1
aload 0
aload 0
getfield android/support/v7/internal/widget/ay/u I
invokevirtual android/support/v7/internal/widget/ay/h(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public final i()Z
aload 0
getfield android/support/v7/internal/widget/ay/l Landroid/graphics/drawable/Drawable;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final j(I)V
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
iload 1
invokevirtual android/support/v7/widget/Toolbar/setVisibility(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public final j()Z
aload 0
getfield android/support/v7/internal/widget/ay/m Landroid/graphics/drawable/Drawable;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final k()Z
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
astore 1
aload 1
invokevirtual android/support/v7/widget/Toolbar/getVisibility()I
ifne L0
aload 1
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
ifnull L0
aload 1
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
getfield android/support/v7/widget/ActionMenuView/d Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final l()Z
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
invokevirtual android/support/v7/widget/Toolbar/a()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final m()Z
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
astore 2
aload 2
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
ifnull L0
aload 2
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
astore 2
aload 2
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
ifnull L1
aload 2
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
invokevirtual android/support/v7/widget/ActionMenuPresenter/j()Z
ifeq L1
iconst_1
istore 1
L2:
iload 1
ifeq L0
iconst_1
ireturn
L1:
iconst_0
istore 1
goto L2
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 1
.end method

.method public final n()Z
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
invokevirtual android/support/v7/widget/Toolbar/b()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final o()Z
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
astore 2
aload 2
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
ifnull L0
aload 2
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
astore 2
aload 2
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
ifnull L1
aload 2
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
invokevirtual android/support/v7/widget/ActionMenuPresenter/f()Z
ifeq L1
iconst_1
istore 1
L2:
iload 1
ifeq L0
iconst_1
ireturn
L1:
iconst_0
istore 1
goto L2
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 1
.end method

.method public final p()V
aload 0
iconst_1
putfield android/support/v7/internal/widget/ay/d Z
return
.limit locals 1
.limit stack 2
.end method

.method public final q()V
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
astore 1
aload 1
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
ifnull L0
aload 1
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
invokevirtual android/support/v7/widget/ActionMenuView/b()V
L0:
return
.limit locals 2
.limit stack 1
.end method

.method public final r()I
aload 0
getfield android/support/v7/internal/widget/ay/h I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final s()Z
aload 0
getfield android/support/v7/internal/widget/ay/i Landroid/view/View;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final t()Z
iconst_0
istore 4
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
astore 5
iload 4
istore 3
aload 5
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
ifnull L0
aload 5
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
invokevirtual android/widget/TextView/getLayout()Landroid/text/Layout;
astore 5
iload 4
istore 3
aload 5
ifnull L0
aload 5
invokevirtual android/text/Layout/getLineCount()I
istore 2
iconst_0
istore 1
L1:
iload 4
istore 3
iload 1
iload 2
if_icmpge L0
aload 5
iload 1
invokevirtual android/text/Layout/getEllipsisCount(I)I
ifle L2
iconst_1
istore 3
L0:
iload 3
ireturn
L2:
iload 1
iconst_1
iadd
istore 1
goto L1
.limit locals 6
.limit stack 2
.end method

.method public final u()V
return
.limit locals 1
.limit stack 0
.end method

.method public final v()I
aload 0
getfield android/support/v7/internal/widget/ay/s I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final w()I
aload 0
getfield android/support/v7/internal/widget/ay/j Landroid/widget/Spinner;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/ay/j Landroid/widget/Spinner;
invokevirtual android/widget/Spinner/getSelectedItemPosition()I
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final x()I
aload 0
getfield android/support/v7/internal/widget/ay/j Landroid/widget/Spinner;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/ay/j Landroid/widget/Spinner;
invokevirtual android/widget/Spinner/getCount()I
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final y()Landroid/view/View;
aload 0
getfield android/support/v7/internal/widget/ay/k Landroid/view/View;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final z()I
aload 0
getfield android/support/v7/internal/widget/ay/a Landroid/support/v7/widget/Toolbar;
invokevirtual android/support/v7/widget/Toolbar/getHeight()I
ireturn
.limit locals 1
.limit stack 1
.end method
