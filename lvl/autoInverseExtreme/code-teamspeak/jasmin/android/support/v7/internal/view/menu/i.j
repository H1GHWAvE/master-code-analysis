.bytecode 50.0
.class public synchronized android/support/v7/internal/view/menu/i
.super java/lang/Object
.implements android/support/v4/g/a/a

.field private static final 'p' Ljava/lang/String; = "MenuBuilder"

.field private static final 'q' Ljava/lang/String; = "android:menu:presenters"

.field private static final 'r' Ljava/lang/String; = "android:menu:actionviewstates"

.field private static final 's' Ljava/lang/String; = "android:menu:expandedactionview"

.field private static final 't' [I

.field private 'A' Z

.field private 'B' Landroid/view/ContextMenu$ContextMenuInfo;

.field private 'C' Landroid/util/SparseArray;

.field private 'D' Z

.field private 'E' Z

.field private 'F' Z

.field private 'G' Ljava/util/ArrayList;

.field private 'H' Ljava/util/concurrent/CopyOnWriteArrayList;

.field public final 'e' Landroid/content/Context;

.field public 'f' Landroid/support/v7/internal/view/menu/j;

.field 'g' Ljava/util/ArrayList;

.field public 'h' Ljava/util/ArrayList;

.field public 'i' I

.field 'j' Ljava/lang/CharSequence;

.field 'k' Landroid/graphics/drawable/Drawable;

.field 'l' Landroid/view/View;

.field 'm' Z

.field 'n' Landroid/support/v7/internal/view/menu/m;

.field public 'o' Z

.field private final 'u' Landroid/content/res/Resources;

.field private 'v' Z

.field private 'w' Z

.field private 'x' Ljava/util/ArrayList;

.field private 'y' Z

.field private 'z' Ljava/util/ArrayList;

.method static <clinit>()V
bipush 6
newarray int
dup
iconst_0
iconst_1
iastore
dup
iconst_1
iconst_4
iastore
dup
iconst_2
iconst_5
iastore
dup
iconst_3
iconst_3
iastore
dup
iconst_4
iconst_2
iastore
dup
iconst_5
iconst_0
iastore
putstatic android/support/v7/internal/view/menu/i/t [I
return
.limit locals 0
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield android/support/v7/internal/view/menu/i/i I
aload 0
iconst_0
putfield android/support/v7/internal/view/menu/i/D Z
aload 0
iconst_0
putfield android/support/v7/internal/view/menu/i/E Z
aload 0
iconst_0
putfield android/support/v7/internal/view/menu/i/m Z
aload 0
iconst_0
putfield android/support/v7/internal/view/menu/i/F Z
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v7/internal/view/menu/i/G Ljava/util/ArrayList;
aload 0
new java/util/concurrent/CopyOnWriteArrayList
dup
invokespecial java/util/concurrent/CopyOnWriteArrayList/<init>()V
putfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
aload 0
aload 1
putfield android/support/v7/internal/view/menu/i/e Landroid/content/Context;
aload 0
aload 1
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
putfield android/support/v7/internal/view/menu/i/u Landroid/content/res/Resources;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v7/internal/view/menu/i/g Ljava/util/ArrayList;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v7/internal/view/menu/i/x Ljava/util/ArrayList;
aload 0
iconst_1
putfield android/support/v7/internal/view/menu/i/y Z
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v7/internal/view/menu/i/h Ljava/util/ArrayList;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v7/internal/view/menu/i/z Ljava/util/ArrayList;
aload 0
iconst_1
putfield android/support/v7/internal/view/menu/i/A Z
aload 0
iconst_1
invokespecial android/support/v7/internal/view/menu/i/e(Z)V
return
.limit locals 2
.limit stack 3
.end method

.method private static a(Ljava/util/ArrayList;I)I
aload 0
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
istore 2
L0:
iload 2
iflt L1
aload 0
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/m
getfield android/support/v7/internal/view/menu/m/f I
iload 1
if_icmpgt L2
iload 2
iconst_1
iadd
ireturn
L2:
iload 2
iconst_1
isub
istore 2
goto L0
L1:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method private a(Landroid/view/View;)Landroid/support/v7/internal/view/menu/i;
aload 0
iconst_0
aconst_null
iconst_0
aconst_null
aload 1
invokevirtual android/support/v7/internal/view/menu/i/a(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V
aload 0
areturn
.limit locals 2
.limit stack 6
.end method

.method private a(IIIILjava/lang/CharSequence;I)Landroid/support/v7/internal/view/menu/m;
new android/support/v7/internal/view/menu/m
dup
aload 0
iload 1
iload 2
iload 3
iload 4
aload 5
iload 6
invokespecial android/support/v7/internal/view/menu/m/<init>(Landroid/support/v7/internal/view/menu/i;IIIILjava/lang/CharSequence;I)V
areturn
.limit locals 7
.limit stack 9
.end method

.method private a(ILandroid/view/KeyEvent;)Landroid/support/v7/internal/view/menu/m;
aload 0
getfield android/support/v7/internal/view/menu/i/G Ljava/util/ArrayList;
astore 9
aload 9
invokevirtual java/util/ArrayList/clear()V
aload 0
aload 9
iload 1
aload 2
invokespecial android/support/v7/internal/view/menu/i/a(Ljava/util/List;ILandroid/view/KeyEvent;)V
aload 9
invokevirtual java/util/ArrayList/isEmpty()Z
ifeq L0
aconst_null
astore 2
L1:
aload 2
areturn
L0:
aload 2
invokevirtual android/view/KeyEvent/getMetaState()I
istore 5
new android/view/KeyCharacterMap$KeyData
dup
invokespecial android/view/KeyCharacterMap$KeyData/<init>()V
astore 10
aload 2
aload 10
invokevirtual android/view/KeyEvent/getKeyData(Landroid/view/KeyCharacterMap$KeyData;)Z
pop
aload 9
invokevirtual java/util/ArrayList/size()I
istore 6
iload 6
iconst_1
if_icmpne L2
aload 9
iconst_0
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/m
areturn
L2:
aload 0
invokevirtual android/support/v7/internal/view/menu/i/b()Z
istore 7
iconst_0
istore 3
L3:
iload 3
iload 6
if_icmpge L4
aload 9
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/m
astore 8
iload 7
ifeq L5
aload 8
invokevirtual android/support/v7/internal/view/menu/m/getAlphabeticShortcut()C
istore 4
L6:
iload 4
aload 10
getfield android/view/KeyCharacterMap$KeyData/meta [C
iconst_0
caload
if_icmpne L7
aload 8
astore 2
iload 5
iconst_2
iand
ifeq L1
L7:
iload 4
aload 10
getfield android/view/KeyCharacterMap$KeyData/meta [C
iconst_2
caload
if_icmpne L8
aload 8
astore 2
iload 5
iconst_2
iand
ifne L1
L8:
iload 7
ifeq L9
iload 4
bipush 8
if_icmpne L9
aload 8
astore 2
iload 1
bipush 67
if_icmpeq L1
L9:
iload 3
iconst_1
iadd
istore 3
goto L3
L5:
aload 8
invokevirtual android/support/v7/internal/view/menu/m/getNumericShortcut()C
istore 4
goto L6
L4:
aconst_null
areturn
.limit locals 11
.limit stack 4
.end method

.method private a(I)V
aload 0
iload 1
iconst_1
invokespecial android/support/v7/internal/view/menu/i/a(IZ)V
return
.limit locals 2
.limit stack 3
.end method

.method private a(IZ)V
iload 1
iflt L0
iload 1
aload 0
getfield android/support/v7/internal/view/menu/i/g Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
if_icmplt L1
L0:
return
L1:
aload 0
getfield android/support/v7/internal/view/menu/i/g Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/remove(I)Ljava/lang/Object;
pop
iload 2
ifeq L0
aload 0
iconst_1
invokevirtual android/support/v7/internal/view/menu/i/c(Z)V
return
.limit locals 3
.limit stack 2
.end method

.method private a(Landroid/view/ContextMenu$ContextMenuInfo;)V
aload 0
aload 1
putfield android/support/v7/internal/view/menu/i/B Landroid/view/ContextMenu$ContextMenuInfo;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/view/MenuItem;)V
aload 1
invokeinterface android/view/MenuItem/getGroupId()I 0
istore 3
aload 0
getfield android/support/v7/internal/view/menu/i/g Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 4
iconst_0
istore 2
L0:
iload 2
iload 4
if_icmpge L1
aload 0
getfield android/support/v7/internal/view/menu/i/g Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/m
astore 6
aload 6
invokevirtual android/support/v7/internal/view/menu/m/getGroupId()I
iload 3
if_icmpne L2
aload 6
invokevirtual android/support/v7/internal/view/menu/m/e()Z
ifeq L2
aload 6
invokevirtual android/support/v7/internal/view/menu/m/isCheckable()Z
ifeq L2
aload 6
aload 1
if_acmpne L3
iconst_1
istore 5
L4:
aload 6
iload 5
invokevirtual android/support/v7/internal/view/menu/m/b(Z)V
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L3:
iconst_0
istore 5
goto L4
L1:
return
.limit locals 7
.limit stack 2
.end method

.method private a(Ljava/util/List;ILandroid/view/KeyEvent;)V
aload 0
invokevirtual android/support/v7/internal/view/menu/i/b()Z
istore 8
aload 3
invokevirtual android/view/KeyEvent/getMetaState()I
istore 6
new android/view/KeyCharacterMap$KeyData
dup
invokespecial android/view/KeyCharacterMap$KeyData/<init>()V
astore 9
aload 3
aload 9
invokevirtual android/view/KeyEvent/getKeyData(Landroid/view/KeyCharacterMap$KeyData;)Z
ifne L0
iload 2
bipush 67
if_icmpeq L0
L1:
return
L0:
aload 0
getfield android/support/v7/internal/view/menu/i/g Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 7
iconst_0
istore 4
L2:
iload 4
iload 7
if_icmpge L1
aload 0
getfield android/support/v7/internal/view/menu/i/g Ljava/util/ArrayList;
iload 4
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/m
astore 10
aload 10
invokevirtual android/support/v7/internal/view/menu/m/hasSubMenu()Z
ifeq L3
aload 10
invokevirtual android/support/v7/internal/view/menu/m/getSubMenu()Landroid/view/SubMenu;
checkcast android/support/v7/internal/view/menu/i
aload 1
iload 2
aload 3
invokespecial android/support/v7/internal/view/menu/i/a(Ljava/util/List;ILandroid/view/KeyEvent;)V
L3:
iload 8
ifeq L4
aload 10
invokevirtual android/support/v7/internal/view/menu/m/getAlphabeticShortcut()C
istore 5
L5:
iload 6
iconst_5
iand
ifne L6
iload 5
ifeq L6
iload 5
aload 9
getfield android/view/KeyCharacterMap$KeyData/meta [C
iconst_0
caload
if_icmpeq L7
iload 5
aload 9
getfield android/view/KeyCharacterMap$KeyData/meta [C
iconst_2
caload
if_icmpeq L7
iload 8
ifeq L6
iload 5
bipush 8
if_icmpne L6
iload 2
bipush 67
if_icmpne L6
L7:
aload 10
invokevirtual android/support/v7/internal/view/menu/m/isEnabled()Z
ifeq L6
aload 1
aload 10
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L6:
iload 4
iconst_1
iadd
istore 4
goto L2
L4:
aload 10
invokevirtual android/support/v7/internal/view/menu/m/getNumericShortcut()C
istore 5
goto L5
.limit locals 11
.limit stack 4
.end method

.method private a(Landroid/support/v7/internal/view/menu/ad;Landroid/support/v7/internal/view/menu/x;)Z
iconst_0
istore 3
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
invokevirtual java/util/concurrent/CopyOnWriteArrayList/isEmpty()Z
ifeq L0
iconst_0
ireturn
L0:
aload 2
ifnull L1
aload 2
aload 1
invokeinterface android/support/v7/internal/view/menu/x/a(Landroid/support/v7/internal/view/menu/ad;)Z 1
istore 3
L1:
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
invokevirtual java/util/concurrent/CopyOnWriteArrayList/iterator()Ljava/util/Iterator;
astore 2
L2:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/ref/WeakReference
astore 4
aload 4
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/x
astore 5
aload 5
ifnonnull L4
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
aload 4
invokevirtual java/util/concurrent/CopyOnWriteArrayList/remove(Ljava/lang/Object;)Z
pop
goto L2
L4:
iload 3
ifne L5
aload 5
aload 1
invokeinterface android/support/v7/internal/view/menu/x/a(Landroid/support/v7/internal/view/menu/ad;)Z 1
istore 3
L6:
goto L2
L3:
iload 3
ireturn
L5:
goto L6
.limit locals 6
.limit stack 2
.end method

.method private a(Landroid/view/MenuItem;I)Z
aload 0
aload 1
aconst_null
iload 2
invokevirtual android/support/v7/internal/view/menu/i/a(Landroid/view/MenuItem;Landroid/support/v7/internal/view/menu/x;I)Z
ireturn
.limit locals 3
.limit stack 4
.end method

.method private b(I)I
aload 0
invokevirtual android/support/v7/internal/view/menu/i/size()I
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 0
getfield android/support/v7/internal/view/menu/i/g Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/m
invokevirtual android/support/v7/internal/view/menu/m/getItemId()I
iload 1
if_icmpne L2
iload 2
ireturn
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
iconst_m1
ireturn
.limit locals 4
.limit stack 2
.end method

.method private c(I)I
aload 0
invokevirtual android/support/v7/internal/view/menu/i/size()I
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 0
getfield android/support/v7/internal/view/menu/i/g Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/m
invokevirtual android/support/v7/internal/view/menu/m/getGroupId()I
iload 1
if_icmpne L2
iload 2
ireturn
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
iconst_m1
ireturn
.limit locals 4
.limit stack 2
.end method

.method private d(I)I
aload 0
invokevirtual android/support/v7/internal/view/menu/i/size()I
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 0
getfield android/support/v7/internal/view/menu/i/g Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/m
invokevirtual android/support/v7/internal/view/menu/m/getGroupId()I
iload 1
if_icmpne L2
iload 2
ireturn
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
iconst_m1
ireturn
.limit locals 4
.limit stack 2
.end method

.method private d(Z)V
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
invokevirtual java/util/concurrent/CopyOnWriteArrayList/isEmpty()Z
ifeq L0
return
L0:
aload 0
invokevirtual android/support/v7/internal/view/menu/i/d()V
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
invokevirtual java/util/concurrent/CopyOnWriteArrayList/iterator()Ljava/util/Iterator;
astore 2
L1:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/ref/WeakReference
astore 3
aload 3
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/x
astore 4
aload 4
ifnonnull L3
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
aload 3
invokevirtual java/util/concurrent/CopyOnWriteArrayList/remove(Ljava/lang/Object;)Z
pop
goto L1
L3:
aload 4
iload 1
invokeinterface android/support/v7/internal/view/menu/x/a(Z)V 1
goto L1
L2:
aload 0
invokevirtual android/support/v7/internal/view/menu/i/e()V
return
.limit locals 5
.limit stack 2
.end method

.method private static e(I)I
ldc_w -65536
iload 0
iand
bipush 16
ishr
istore 1
iload 1
iflt L0
iload 1
getstatic android/support/v7/internal/view/menu/i/t [I
arraylength
if_icmplt L1
L0:
new java/lang/IllegalArgumentException
dup
ldc "order does not contain a valid category."
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
getstatic android/support/v7/internal/view/menu/i/t [I
iload 1
iaload
bipush 16
ishl
ldc_w 65535
iload 0
iand
ior
ireturn
.limit locals 2
.limit stack 3
.end method

.method private e(Landroid/os/Bundle;)V
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
invokevirtual java/util/concurrent/CopyOnWriteArrayList/isEmpty()Z
ifeq L0
return
L0:
new android/util/SparseArray
dup
invokespecial android/util/SparseArray/<init>()V
astore 3
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
invokevirtual java/util/concurrent/CopyOnWriteArrayList/iterator()Ljava/util/Iterator;
astore 4
L1:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/ref/WeakReference
astore 5
aload 5
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/x
astore 6
aload 6
ifnonnull L3
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
aload 5
invokevirtual java/util/concurrent/CopyOnWriteArrayList/remove(Ljava/lang/Object;)Z
pop
goto L1
L3:
aload 6
invokeinterface android/support/v7/internal/view/menu/x/b()I 0
istore 2
iload 2
ifle L1
aload 6
invokeinterface android/support/v7/internal/view/menu/x/c()Landroid/os/Parcelable; 0
astore 5
aload 5
ifnull L1
aload 3
iload 2
aload 5
invokevirtual android/util/SparseArray/put(ILjava/lang/Object;)V
goto L1
L2:
aload 1
ldc "android:menu:presenters"
aload 3
invokevirtual android/os/Bundle/putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V
return
.limit locals 7
.limit stack 3
.end method

.method private e(Z)V
iconst_1
istore 2
iload 1
ifeq L0
aload 0
getfield android/support/v7/internal/view/menu/i/u Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getConfiguration()Landroid/content/res/Configuration;
getfield android/content/res/Configuration/keyboard I
iconst_1
if_icmpeq L0
aload 0
getfield android/support/v7/internal/view/menu/i/u Landroid/content/res/Resources;
getstatic android/support/v7/a/e/abc_config_showMenuShortcutsWhenKeyboardPresent I
invokevirtual android/content/res/Resources/getBoolean(I)Z
ifeq L0
iload 2
istore 1
L1:
aload 0
iload 1
putfield android/support/v7/internal/view/menu/i/w Z
return
L0:
iconst_0
istore 1
goto L1
.limit locals 3
.limit stack 2
.end method

.method private f(I)Landroid/support/v7/internal/view/menu/i;
aload 0
iload 1
aconst_null
iconst_0
aconst_null
aconst_null
invokevirtual android/support/v7/internal/view/menu/i/a(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V
aload 0
areturn
.limit locals 2
.limit stack 6
.end method

.method private f(Landroid/os/Bundle;)V
aload 1
ldc "android:menu:presenters"
invokevirtual android/os/Bundle/getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;
astore 1
aload 1
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
invokevirtual java/util/concurrent/CopyOnWriteArrayList/isEmpty()Z
ifeq L1
L0:
return
L1:
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
invokevirtual java/util/concurrent/CopyOnWriteArrayList/iterator()Ljava/util/Iterator;
astore 3
L2:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/ref/WeakReference
astore 5
aload 5
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/x
astore 4
aload 4
ifnonnull L3
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
aload 5
invokevirtual java/util/concurrent/CopyOnWriteArrayList/remove(Ljava/lang/Object;)Z
pop
goto L2
L3:
aload 4
invokeinterface android/support/v7/internal/view/menu/x/b()I 0
istore 2
iload 2
ifle L2
aload 1
iload 2
invokevirtual android/util/SparseArray/get(I)Ljava/lang/Object;
checkcast android/os/Parcelable
astore 5
aload 5
ifnull L2
aload 4
aload 5
invokeinterface android/support/v7/internal/view/menu/x/a(Landroid/os/Parcelable;)V 1
goto L2
.limit locals 6
.limit stack 2
.end method

.method private f(Z)V
aload 0
iload 1
putfield android/support/v7/internal/view/menu/i/m Z
return
.limit locals 2
.limit stack 2
.end method

.method private g(I)Landroid/support/v7/internal/view/menu/i;
aload 0
iconst_0
aconst_null
iload 1
aconst_null
aconst_null
invokevirtual android/support/v7/internal/view/menu/i/a(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V
aload 0
areturn
.limit locals 2
.limit stack 6
.end method

.method private g(Z)V
aload 0
iload 1
putfield android/support/v7/internal/view/menu/i/o Z
return
.limit locals 2
.limit stack 2
.end method

.method private l()Landroid/support/v7/internal/view/menu/i;
aload 0
iconst_1
putfield android/support/v7/internal/view/menu/i/i I
aload 0
areturn
.limit locals 1
.limit stack 2
.end method

.method private m()V
aload 0
iconst_1
putfield android/support/v7/internal/view/menu/i/D Z
aload 0
invokevirtual android/support/v7/internal/view/menu/i/clear()V
aload 0
invokevirtual android/support/v7/internal/view/menu/i/clearHeader()V
aload 0
iconst_0
putfield android/support/v7/internal/view/menu/i/D Z
aload 0
iconst_0
putfield android/support/v7/internal/view/menu/i/E Z
aload 0
iconst_1
invokevirtual android/support/v7/internal/view/menu/i/c(Z)V
return
.limit locals 1
.limit stack 2
.end method

.method private n()Landroid/content/res/Resources;
aload 0
getfield android/support/v7/internal/view/menu/i/u Landroid/content/res/Resources;
areturn
.limit locals 1
.limit stack 1
.end method

.method private o()Landroid/content/Context;
aload 0
getfield android/support/v7/internal/view/menu/i/e Landroid/content/Context;
areturn
.limit locals 1
.limit stack 1
.end method

.method private p()V
aload 0
getfield android/support/v7/internal/view/menu/i/f Landroid/support/v7/internal/view/menu/j;
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/i/f Landroid/support/v7/internal/view/menu/j;
aload 0
invokeinterface android/support/v7/internal/view/menu/j/a(Landroid/support/v7/internal/view/menu/i;)V 1
L0:
return
.limit locals 1
.limit stack 2
.end method

.method private q()Ljava/util/ArrayList;
aload 0
invokevirtual android/support/v7/internal/view/menu/i/i()V
aload 0
getfield android/support/v7/internal/view/menu/i/h Ljava/util/ArrayList;
areturn
.limit locals 1
.limit stack 1
.end method

.method private r()Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/view/menu/i/j Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method private s()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/internal/view/menu/i/k Landroid/graphics/drawable/Drawable;
areturn
.limit locals 1
.limit stack 1
.end method

.method private t()Landroid/view/View;
aload 0
getfield android/support/v7/internal/view/menu/i/l Landroid/view/View;
areturn
.limit locals 1
.limit stack 1
.end method

.method private u()Z
aload 0
getfield android/support/v7/internal/view/menu/i/m Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private v()Landroid/support/v7/internal/view/menu/m;
aload 0
getfield android/support/v7/internal/view/menu/i/n Landroid/support/v7/internal/view/menu/m;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final a(Landroid/graphics/drawable/Drawable;)Landroid/support/v7/internal/view/menu/i;
aload 0
iconst_0
aconst_null
iconst_0
aload 1
aconst_null
invokevirtual android/support/v7/internal/view/menu/i/a(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V
aload 0
areturn
.limit locals 2
.limit stack 6
.end method

.method protected final a(Ljava/lang/CharSequence;)Landroid/support/v7/internal/view/menu/i;
aload 0
iconst_0
aload 1
iconst_0
aconst_null
aconst_null
invokevirtual android/support/v7/internal/view/menu/i/a(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V
aload 0
areturn
.limit locals 2
.limit stack 6
.end method

.method public final a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
ldc_w -65536
iload 3
iand
bipush 16
ishr
istore 5
iload 5
iflt L0
iload 5
getstatic android/support/v7/internal/view/menu/i/t [I
arraylength
if_icmplt L1
L0:
new java/lang/IllegalArgumentException
dup
ldc "order does not contain a valid category."
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
getstatic android/support/v7/internal/view/menu/i/t [I
iload 5
iaload
bipush 16
ishl
ldc_w 65535
iload 3
iand
ior
istore 5
new android/support/v7/internal/view/menu/m
dup
aload 0
iload 1
iload 2
iload 3
iload 5
aload 4
aload 0
getfield android/support/v7/internal/view/menu/i/i I
invokespecial android/support/v7/internal/view/menu/m/<init>(Landroid/support/v7/internal/view/menu/i;IIIILjava/lang/CharSequence;I)V
astore 4
aload 0
getfield android/support/v7/internal/view/menu/i/B Landroid/view/ContextMenu$ContextMenuInfo;
ifnull L2
aload 4
aload 0
getfield android/support/v7/internal/view/menu/i/B Landroid/view/ContextMenu$ContextMenuInfo;
putfield android/support/v7/internal/view/menu/m/k Landroid/view/ContextMenu$ContextMenuInfo;
L2:
aload 0
getfield android/support/v7/internal/view/menu/i/g Ljava/util/ArrayList;
aload 0
getfield android/support/v7/internal/view/menu/i/g Ljava/util/ArrayList;
iload 5
invokestatic android/support/v7/internal/view/menu/i/a(Ljava/util/ArrayList;I)I
aload 4
invokevirtual java/util/ArrayList/add(ILjava/lang/Object;)V
aload 0
iconst_1
invokevirtual android/support/v7/internal/view/menu/i/c(Z)V
aload 4
areturn
.limit locals 6
.limit stack 9
.end method

.method protected a()Ljava/lang/String;
ldc "android:menu:actionviewstates"
areturn
.limit locals 1
.limit stack 1
.end method

.method final a(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V
aload 0
getfield android/support/v7/internal/view/menu/i/u Landroid/content/res/Resources;
astore 6
aload 5
ifnull L0
aload 0
aload 5
putfield android/support/v7/internal/view/menu/i/l Landroid/view/View;
aload 0
aconst_null
putfield android/support/v7/internal/view/menu/i/j Ljava/lang/CharSequence;
aload 0
aconst_null
putfield android/support/v7/internal/view/menu/i/k Landroid/graphics/drawable/Drawable;
L1:
aload 0
iconst_0
invokevirtual android/support/v7/internal/view/menu/i/c(Z)V
return
L0:
iload 1
ifle L2
aload 0
aload 6
iload 1
invokevirtual android/content/res/Resources/getText(I)Ljava/lang/CharSequence;
putfield android/support/v7/internal/view/menu/i/j Ljava/lang/CharSequence;
L3:
iload 3
ifle L4
aload 0
aload 0
getfield android/support/v7/internal/view/menu/i/e Landroid/content/Context;
iload 3
invokestatic android/support/v4/c/h/a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
putfield android/support/v7/internal/view/menu/i/k Landroid/graphics/drawable/Drawable;
L5:
aload 0
aconst_null
putfield android/support/v7/internal/view/menu/i/l Landroid/view/View;
goto L1
L2:
aload 2
ifnull L3
aload 0
aload 2
putfield android/support/v7/internal/view/menu/i/j Ljava/lang/CharSequence;
goto L3
L4:
aload 4
ifnull L5
aload 0
aload 4
putfield android/support/v7/internal/view/menu/i/k Landroid/graphics/drawable/Drawable;
goto L5
.limit locals 7
.limit stack 3
.end method

.method public final a(Landroid/os/Bundle;)V
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
invokevirtual java/util/concurrent/CopyOnWriteArrayList/isEmpty()Z
ifne L0
new android/util/SparseArray
dup
invokespecial android/util/SparseArray/<init>()V
astore 3
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
invokevirtual java/util/concurrent/CopyOnWriteArrayList/iterator()Ljava/util/Iterator;
astore 4
L1:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/ref/WeakReference
astore 5
aload 5
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/x
astore 6
aload 6
ifnonnull L3
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
aload 5
invokevirtual java/util/concurrent/CopyOnWriteArrayList/remove(Ljava/lang/Object;)Z
pop
goto L1
L3:
aload 6
invokeinterface android/support/v7/internal/view/menu/x/b()I 0
istore 2
iload 2
ifle L1
aload 6
invokeinterface android/support/v7/internal/view/menu/x/c()Landroid/os/Parcelable; 0
astore 5
aload 5
ifnull L1
aload 3
iload 2
aload 5
invokevirtual android/util/SparseArray/put(ILjava/lang/Object;)V
goto L1
L2:
aload 1
ldc "android:menu:presenters"
aload 3
invokevirtual android/os/Bundle/putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V
L0:
return
.limit locals 7
.limit stack 3
.end method

.method public a(Landroid/support/v7/internal/view/menu/j;)V
aload 0
aload 1
putfield android/support/v7/internal/view/menu/i/f Landroid/support/v7/internal/view/menu/j;
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/support/v7/internal/view/menu/x;)V
aload 0
aload 1
aload 0
getfield android/support/v7/internal/view/menu/i/e Landroid/content/Context;
invokevirtual android/support/v7/internal/view/menu/i/a(Landroid/support/v7/internal/view/menu/x;Landroid/content/Context;)V
return
.limit locals 2
.limit stack 3
.end method

.method public final a(Landroid/support/v7/internal/view/menu/x;Landroid/content/Context;)V
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
new java/lang/ref/WeakReference
dup
aload 1
invokespecial java/lang/ref/WeakReference/<init>(Ljava/lang/Object;)V
invokevirtual java/util/concurrent/CopyOnWriteArrayList/add(Ljava/lang/Object;)Z
pop
aload 1
aload 2
aload 0
invokeinterface android/support/v7/internal/view/menu/x/a(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;)V 2
aload 0
iconst_1
putfield android/support/v7/internal/view/menu/i/A Z
return
.limit locals 3
.limit stack 4
.end method

.method public a(Z)V
aload 0
getfield android/support/v7/internal/view/menu/i/w Z
iload 1
if_icmpne L0
return
L0:
aload 0
iload 1
invokespecial android/support/v7/internal/view/menu/i/e(Z)V
aload 0
iconst_0
invokevirtual android/support/v7/internal/view/menu/i/c(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method a(Landroid/support/v7/internal/view/menu/i;Landroid/view/MenuItem;)Z
aload 0
getfield android/support/v7/internal/view/menu/i/f Landroid/support/v7/internal/view/menu/j;
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/i/f Landroid/support/v7/internal/view/menu/j;
aload 1
aload 2
invokeinterface android/support/v7/internal/view/menu/j/a(Landroid/support/v7/internal/view/menu/i;Landroid/view/MenuItem;)Z 2
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 3
.end method

.method public a(Landroid/support/v7/internal/view/menu/m;)Z
iconst_0
istore 3
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
invokevirtual java/util/concurrent/CopyOnWriteArrayList/isEmpty()Z
ifeq L0
L1:
iload 3
ireturn
L0:
aload 0
invokevirtual android/support/v7/internal/view/menu/i/d()V
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
invokevirtual java/util/concurrent/CopyOnWriteArrayList/iterator()Ljava/util/Iterator;
astore 4
iconst_0
istore 2
L2:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/ref/WeakReference
astore 5
aload 5
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/x
astore 6
aload 6
ifnonnull L4
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
aload 5
invokevirtual java/util/concurrent/CopyOnWriteArrayList/remove(Ljava/lang/Object;)Z
pop
goto L2
L4:
aload 6
aload 1
invokeinterface android/support/v7/internal/view/menu/x/a(Landroid/support/v7/internal/view/menu/m;)Z 1
istore 2
iload 2
ifeq L5
L6:
aload 0
invokevirtual android/support/v7/internal/view/menu/i/e()V
iload 2
istore 3
iload 2
ifeq L1
aload 0
aload 1
putfield android/support/v7/internal/view/menu/i/n Landroid/support/v7/internal/view/menu/m;
iload 2
ireturn
L5:
goto L2
L3:
goto L6
.limit locals 7
.limit stack 2
.end method

.method public final a(Landroid/view/MenuItem;Landroid/support/v7/internal/view/menu/x;I)Z
iconst_0
istore 6
iconst_0
istore 5
aload 1
checkcast android/support/v7/internal/view/menu/m
astore 1
aload 1
ifnull L0
aload 1
invokevirtual android/support/v7/internal/view/menu/m/isEnabled()Z
ifne L1
L0:
iconst_0
istore 5
L2:
iload 5
ireturn
L1:
aload 1
invokevirtual android/support/v7/internal/view/menu/m/b()Z
istore 7
aload 1
getfield android/support/v7/internal/view/menu/m/i Landroid/support/v4/view/n;
astore 8
aload 8
ifnull L3
aload 8
invokevirtual android/support/v4/view/n/f()Z
ifeq L3
iconst_1
istore 4
L4:
aload 1
invokevirtual android/support/v7/internal/view/menu/m/i()Z
ifeq L5
aload 1
invokevirtual android/support/v7/internal/view/menu/m/expandActionView()Z
iload 7
ior
istore 6
iload 6
istore 5
iload 6
ifeq L2
aload 0
iconst_1
invokevirtual android/support/v7/internal/view/menu/i/b(Z)V
iload 6
ireturn
L3:
iconst_0
istore 4
goto L4
L5:
aload 1
invokevirtual android/support/v7/internal/view/menu/m/hasSubMenu()Z
ifne L6
iload 4
ifeq L7
L6:
aload 0
iconst_0
invokevirtual android/support/v7/internal/view/menu/i/b(Z)V
aload 1
invokevirtual android/support/v7/internal/view/menu/m/hasSubMenu()Z
ifne L8
aload 1
new android/support/v7/internal/view/menu/ad
dup
aload 0
getfield android/support/v7/internal/view/menu/i/e Landroid/content/Context;
aload 0
aload 1
invokespecial android/support/v7/internal/view/menu/ad/<init>(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;Landroid/support/v7/internal/view/menu/m;)V
invokevirtual android/support/v7/internal/view/menu/m/a(Landroid/support/v7/internal/view/menu/ad;)V
L8:
aload 1
invokevirtual android/support/v7/internal/view/menu/m/getSubMenu()Landroid/view/SubMenu;
checkcast android/support/v7/internal/view/menu/ad
astore 1
iload 4
ifeq L9
aload 8
aload 1
invokevirtual android/support/v4/view/n/a(Landroid/view/SubMenu;)V
L9:
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
invokevirtual java/util/concurrent/CopyOnWriteArrayList/isEmpty()Z
ifeq L10
L11:
iload 7
iload 5
ior
istore 6
iload 6
istore 5
iload 6
ifne L2
aload 0
iconst_1
invokevirtual android/support/v7/internal/view/menu/i/b(Z)V
iload 6
ireturn
L10:
iload 6
istore 5
aload 2
ifnull L12
aload 2
aload 1
invokeinterface android/support/v7/internal/view/menu/x/a(Landroid/support/v7/internal/view/menu/ad;)Z 1
istore 5
L12:
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
invokevirtual java/util/concurrent/CopyOnWriteArrayList/iterator()Ljava/util/Iterator;
astore 2
L13:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L14
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/ref/WeakReference
astore 8
aload 8
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/x
astore 9
aload 9
ifnonnull L15
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
aload 8
invokevirtual java/util/concurrent/CopyOnWriteArrayList/remove(Ljava/lang/Object;)Z
pop
goto L13
L15:
iload 5
ifne L16
aload 9
aload 1
invokeinterface android/support/v7/internal/view/menu/x/a(Landroid/support/v7/internal/view/menu/ad;)Z 1
istore 5
L17:
goto L13
L14:
goto L11
L7:
iload 3
iconst_1
iand
ifne L18
aload 0
iconst_1
invokevirtual android/support/v7/internal/view/menu/i/b(Z)V
L18:
iload 7
ireturn
L16:
goto L17
.limit locals 10
.limit stack 6
.end method

.method public add(I)Landroid/view/MenuItem;
aload 0
iconst_0
iconst_0
iconst_0
aload 0
getfield android/support/v7/internal/view/menu/i/u Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getString(I)Ljava/lang/String;
invokevirtual android/support/v7/internal/view/menu/i/a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
areturn
.limit locals 2
.limit stack 6
.end method

.method public add(IIII)Landroid/view/MenuItem;
aload 0
iload 1
iload 2
iload 3
aload 0
getfield android/support/v7/internal/view/menu/i/u Landroid/content/res/Resources;
iload 4
invokevirtual android/content/res/Resources/getString(I)Ljava/lang/String;
invokevirtual android/support/v7/internal/view/menu/i/a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
areturn
.limit locals 5
.limit stack 6
.end method

.method public add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
aload 0
iload 1
iload 2
iload 3
aload 4
invokevirtual android/support/v7/internal/view/menu/i/a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
areturn
.limit locals 5
.limit stack 5
.end method

.method public add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
aload 0
iconst_0
iconst_0
iconst_0
aload 1
invokevirtual android/support/v7/internal/view/menu/i/a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
areturn
.limit locals 2
.limit stack 5
.end method

.method public addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I
aload 0
getfield android/support/v7/internal/view/menu/i/e Landroid/content/Context;
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
astore 10
aload 10
aload 4
aload 5
aload 6
iconst_0
invokevirtual android/content/pm/PackageManager/queryIntentActivityOptions(Landroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I)Ljava/util/List;
astore 11
aload 11
ifnull L0
aload 11
invokeinterface java/util/List/size()I 0
istore 9
L1:
iload 7
iconst_1
iand
ifne L2
aload 0
iload 1
invokevirtual android/support/v7/internal/view/menu/i/removeGroup(I)V
L2:
iconst_0
istore 7
L3:
iload 7
iload 9
if_icmpge L4
aload 11
iload 7
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/content/pm/ResolveInfo
astore 12
aload 12
getfield android/content/pm/ResolveInfo/specificIndex I
ifge L5
aload 6
astore 4
L6:
new android/content/Intent
dup
aload 4
invokespecial android/content/Intent/<init>(Landroid/content/Intent;)V
astore 4
aload 4
new android/content/ComponentName
dup
aload 12
getfield android/content/pm/ResolveInfo/activityInfo Landroid/content/pm/ActivityInfo;
getfield android/content/pm/ActivityInfo/applicationInfo Landroid/content/pm/ApplicationInfo;
getfield android/content/pm/ApplicationInfo/packageName Ljava/lang/String;
aload 12
getfield android/content/pm/ResolveInfo/activityInfo Landroid/content/pm/ActivityInfo;
getfield android/content/pm/ActivityInfo/name Ljava/lang/String;
invokespecial android/content/ComponentName/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokevirtual android/content/Intent/setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
pop
aload 0
iload 1
iload 2
iload 3
aload 12
aload 10
invokevirtual android/content/pm/ResolveInfo/loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
invokevirtual android/support/v7/internal/view/menu/i/add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
aload 12
aload 10
invokevirtual android/content/pm/ResolveInfo/loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;
invokeinterface android/view/MenuItem/setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem; 1
aload 4
invokeinterface android/view/MenuItem/setIntent(Landroid/content/Intent;)Landroid/view/MenuItem; 1
astore 4
aload 8
ifnull L7
aload 12
getfield android/content/pm/ResolveInfo/specificIndex I
iflt L7
aload 8
aload 12
getfield android/content/pm/ResolveInfo/specificIndex I
aload 4
aastore
L7:
iload 7
iconst_1
iadd
istore 7
goto L3
L0:
iconst_0
istore 9
goto L1
L5:
aload 5
aload 12
getfield android/content/pm/ResolveInfo/specificIndex I
aaload
astore 4
goto L6
L4:
iload 9
ireturn
.limit locals 13
.limit stack 6
.end method

.method public addSubMenu(I)Landroid/view/SubMenu;
aload 0
iconst_0
iconst_0
iconst_0
aload 0
getfield android/support/v7/internal/view/menu/i/u Landroid/content/res/Resources;
iload 1
invokevirtual android/content/res/Resources/getString(I)Ljava/lang/String;
invokevirtual android/support/v7/internal/view/menu/i/addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
areturn
.limit locals 2
.limit stack 6
.end method

.method public addSubMenu(IIII)Landroid/view/SubMenu;
aload 0
iload 1
iload 2
iload 3
aload 0
getfield android/support/v7/internal/view/menu/i/u Landroid/content/res/Resources;
iload 4
invokevirtual android/content/res/Resources/getString(I)Ljava/lang/String;
invokevirtual android/support/v7/internal/view/menu/i/addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
areturn
.limit locals 5
.limit stack 6
.end method

.method public addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
aload 0
iload 1
iload 2
iload 3
aload 4
invokevirtual android/support/v7/internal/view/menu/i/a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
checkcast android/support/v7/internal/view/menu/m
astore 4
new android/support/v7/internal/view/menu/ad
dup
aload 0
getfield android/support/v7/internal/view/menu/i/e Landroid/content/Context;
aload 0
aload 4
invokespecial android/support/v7/internal/view/menu/ad/<init>(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;Landroid/support/v7/internal/view/menu/m;)V
astore 5
aload 4
aload 5
invokevirtual android/support/v7/internal/view/menu/m/a(Landroid/support/v7/internal/view/menu/ad;)V
aload 5
areturn
.limit locals 6
.limit stack 5
.end method

.method public addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
aload 0
iconst_0
iconst_0
iconst_0
aload 1
invokevirtual android/support/v7/internal/view/menu/i/addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
areturn
.limit locals 2
.limit stack 5
.end method

.method public final b(Landroid/os/Bundle;)V
aload 1
ldc "android:menu:presenters"
invokevirtual android/os/Bundle/getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;
astore 1
aload 1
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
invokevirtual java/util/concurrent/CopyOnWriteArrayList/isEmpty()Z
ifeq L1
L0:
return
L1:
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
invokevirtual java/util/concurrent/CopyOnWriteArrayList/iterator()Ljava/util/Iterator;
astore 3
L2:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/ref/WeakReference
astore 5
aload 5
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/x
astore 4
aload 4
ifnonnull L3
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
aload 5
invokevirtual java/util/concurrent/CopyOnWriteArrayList/remove(Ljava/lang/Object;)Z
pop
goto L2
L3:
aload 4
invokeinterface android/support/v7/internal/view/menu/x/b()I 0
istore 2
iload 2
ifle L2
aload 1
iload 2
invokevirtual android/util/SparseArray/get(I)Ljava/lang/Object;
checkcast android/os/Parcelable
astore 5
aload 5
ifnull L2
aload 4
aload 5
invokeinterface android/support/v7/internal/view/menu/x/a(Landroid/os/Parcelable;)V 1
goto L2
.limit locals 6
.limit stack 2
.end method

.method public final b(Landroid/support/v7/internal/view/menu/x;)V
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
invokevirtual java/util/concurrent/CopyOnWriteArrayList/iterator()Ljava/util/Iterator;
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/ref/WeakReference
astore 3
aload 3
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/x
astore 4
aload 4
ifnull L2
aload 4
aload 1
if_acmpne L0
L2:
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
aload 3
invokevirtual java/util/concurrent/CopyOnWriteArrayList/remove(Ljava/lang/Object;)Z
pop
goto L0
L1:
return
.limit locals 5
.limit stack 2
.end method

.method public final b(Z)V
aload 0
getfield android/support/v7/internal/view/menu/i/F Z
ifeq L0
return
L0:
aload 0
iconst_1
putfield android/support/v7/internal/view/menu/i/F Z
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
invokevirtual java/util/concurrent/CopyOnWriteArrayList/iterator()Ljava/util/Iterator;
astore 2
L1:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/ref/WeakReference
astore 3
aload 3
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/x
astore 4
aload 4
ifnonnull L3
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
aload 3
invokevirtual java/util/concurrent/CopyOnWriteArrayList/remove(Ljava/lang/Object;)Z
pop
goto L1
L3:
aload 4
aload 0
iload 1
invokeinterface android/support/v7/internal/view/menu/x/a(Landroid/support/v7/internal/view/menu/i;Z)V 2
goto L1
L2:
aload 0
iconst_0
putfield android/support/v7/internal/view/menu/i/F Z
return
.limit locals 5
.limit stack 3
.end method

.method b()Z
aload 0
getfield android/support/v7/internal/view/menu/i/v Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public b(Landroid/support/v7/internal/view/menu/m;)Z
iconst_0
istore 2
iload 2
istore 3
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
invokevirtual java/util/concurrent/CopyOnWriteArrayList/isEmpty()Z
ifne L0
aload 0
getfield android/support/v7/internal/view/menu/i/n Landroid/support/v7/internal/view/menu/m;
aload 1
if_acmpeq L1
iload 2
istore 3
L0:
iload 3
ireturn
L1:
aload 0
invokevirtual android/support/v7/internal/view/menu/i/d()V
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
invokevirtual java/util/concurrent/CopyOnWriteArrayList/iterator()Ljava/util/Iterator;
astore 4
iconst_0
istore 2
L2:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/ref/WeakReference
astore 5
aload 5
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/x
astore 6
aload 6
ifnonnull L4
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
aload 5
invokevirtual java/util/concurrent/CopyOnWriteArrayList/remove(Ljava/lang/Object;)Z
pop
goto L2
L4:
aload 6
aload 1
invokeinterface android/support/v7/internal/view/menu/x/b(Landroid/support/v7/internal/view/menu/m;)Z 1
istore 2
iload 2
ifeq L5
L6:
aload 0
invokevirtual android/support/v7/internal/view/menu/i/e()V
iload 2
istore 3
iload 2
ifeq L0
aload 0
aconst_null
putfield android/support/v7/internal/view/menu/i/n Landroid/support/v7/internal/view/menu/m;
iload 2
ireturn
L5:
goto L2
L3:
goto L6
.limit locals 7
.limit stack 2
.end method

.method public final c(Landroid/os/Bundle;)V
aload 0
invokevirtual android/support/v7/internal/view/menu/i/size()I
istore 3
iconst_0
istore 2
aconst_null
astore 4
L0:
iload 2
iload 3
if_icmpge L1
aload 0
iload 2
invokevirtual android/support/v7/internal/view/menu/i/getItem(I)Landroid/view/MenuItem;
astore 7
aload 7
invokestatic android/support/v4/view/az/a(Landroid/view/MenuItem;)Landroid/view/View;
astore 8
aload 4
astore 6
aload 8
ifnull L2
aload 4
astore 6
aload 8
invokevirtual android/view/View/getId()I
iconst_m1
if_icmpeq L2
aload 4
astore 5
aload 4
ifnonnull L3
new android/util/SparseArray
dup
invokespecial android/util/SparseArray/<init>()V
astore 5
L3:
aload 8
aload 5
invokevirtual android/view/View/saveHierarchyState(Landroid/util/SparseArray;)V
aload 5
astore 6
aload 7
invokestatic android/support/v4/view/az/c(Landroid/view/MenuItem;)Z
ifeq L2
aload 1
ldc "android:menu:expandedactionview"
aload 7
invokeinterface android/view/MenuItem/getItemId()I 0
invokevirtual android/os/Bundle/putInt(Ljava/lang/String;I)V
aload 5
astore 6
L2:
aload 7
invokeinterface android/view/MenuItem/hasSubMenu()Z 0
ifeq L4
aload 7
invokeinterface android/view/MenuItem/getSubMenu()Landroid/view/SubMenu; 0
checkcast android/support/v7/internal/view/menu/ad
aload 1
invokevirtual android/support/v7/internal/view/menu/ad/c(Landroid/os/Bundle;)V
L4:
iload 2
iconst_1
iadd
istore 2
aload 6
astore 4
goto L0
L1:
aload 4
ifnull L5
aload 1
aload 0
invokevirtual android/support/v7/internal/view/menu/i/a()Ljava/lang/String;
aload 4
invokevirtual android/os/Bundle/putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V
L5:
return
.limit locals 9
.limit stack 3
.end method

.method public final c(Z)V
aload 0
getfield android/support/v7/internal/view/menu/i/D Z
ifne L0
iload 1
ifeq L1
aload 0
iconst_1
putfield android/support/v7/internal/view/menu/i/y Z
aload 0
iconst_1
putfield android/support/v7/internal/view/menu/i/A Z
L1:
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
invokevirtual java/util/concurrent/CopyOnWriteArrayList/isEmpty()Z
ifne L2
aload 0
invokevirtual android/support/v7/internal/view/menu/i/d()V
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
invokevirtual java/util/concurrent/CopyOnWriteArrayList/iterator()Ljava/util/Iterator;
astore 2
L3:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/ref/WeakReference
astore 3
aload 3
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/x
astore 4
aload 4
ifnonnull L5
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
aload 3
invokevirtual java/util/concurrent/CopyOnWriteArrayList/remove(Ljava/lang/Object;)Z
pop
goto L3
L5:
aload 4
iload 1
invokeinterface android/support/v7/internal/view/menu/x/a(Z)V 1
goto L3
L4:
aload 0
invokevirtual android/support/v7/internal/view/menu/i/e()V
L2:
return
L0:
aload 0
iconst_1
putfield android/support/v7/internal/view/menu/i/E Z
return
.limit locals 5
.limit stack 2
.end method

.method public c()Z
aload 0
getfield android/support/v7/internal/view/menu/i/w Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public clear()V
aload 0
getfield android/support/v7/internal/view/menu/i/n Landroid/support/v7/internal/view/menu/m;
ifnull L0
aload 0
aload 0
getfield android/support/v7/internal/view/menu/i/n Landroid/support/v7/internal/view/menu/m;
invokevirtual android/support/v7/internal/view/menu/i/b(Landroid/support/v7/internal/view/menu/m;)Z
pop
L0:
aload 0
getfield android/support/v7/internal/view/menu/i/g Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
aload 0
iconst_1
invokevirtual android/support/v7/internal/view/menu/i/c(Z)V
return
.limit locals 1
.limit stack 2
.end method

.method public clearHeader()V
aload 0
aconst_null
putfield android/support/v7/internal/view/menu/i/k Landroid/graphics/drawable/Drawable;
aload 0
aconst_null
putfield android/support/v7/internal/view/menu/i/j Ljava/lang/CharSequence;
aload 0
aconst_null
putfield android/support/v7/internal/view/menu/i/l Landroid/view/View;
aload 0
iconst_0
invokevirtual android/support/v7/internal/view/menu/i/c(Z)V
return
.limit locals 1
.limit stack 2
.end method

.method public close()V
aload 0
iconst_1
invokevirtual android/support/v7/internal/view/menu/i/b(Z)V
return
.limit locals 1
.limit stack 2
.end method

.method public final d()V
aload 0
getfield android/support/v7/internal/view/menu/i/D Z
ifne L0
aload 0
iconst_1
putfield android/support/v7/internal/view/menu/i/D Z
aload 0
iconst_0
putfield android/support/v7/internal/view/menu/i/E Z
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public final d(Landroid/os/Bundle;)V
aload 1
ifnonnull L0
L1:
return
L0:
aload 1
aload 0
invokevirtual android/support/v7/internal/view/menu/i/a()Ljava/lang/String;
invokevirtual android/os/Bundle/getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;
astore 4
aload 0
invokevirtual android/support/v7/internal/view/menu/i/size()I
istore 3
iconst_0
istore 2
L2:
iload 2
iload 3
if_icmpge L3
aload 0
iload 2
invokevirtual android/support/v7/internal/view/menu/i/getItem(I)Landroid/view/MenuItem;
astore 5
aload 5
invokestatic android/support/v4/view/az/a(Landroid/view/MenuItem;)Landroid/view/View;
astore 6
aload 6
ifnull L4
aload 6
invokevirtual android/view/View/getId()I
iconst_m1
if_icmpeq L4
aload 6
aload 4
invokevirtual android/view/View/restoreHierarchyState(Landroid/util/SparseArray;)V
L4:
aload 5
invokeinterface android/view/MenuItem/hasSubMenu()Z 0
ifeq L5
aload 5
invokeinterface android/view/MenuItem/getSubMenu()Landroid/view/SubMenu; 0
checkcast android/support/v7/internal/view/menu/ad
aload 1
invokevirtual android/support/v7/internal/view/menu/ad/d(Landroid/os/Bundle;)V
L5:
iload 2
iconst_1
iadd
istore 2
goto L2
L3:
aload 1
ldc "android:menu:expandedactionview"
invokevirtual android/os/Bundle/getInt(Ljava/lang/String;)I
istore 2
iload 2
ifle L1
aload 0
iload 2
invokevirtual android/support/v7/internal/view/menu/i/findItem(I)Landroid/view/MenuItem;
astore 1
aload 1
ifnull L1
aload 1
invokestatic android/support/v4/view/az/b(Landroid/view/MenuItem;)Z
pop
return
.limit locals 7
.limit stack 2
.end method

.method public final e()V
aload 0
iconst_0
putfield android/support/v7/internal/view/menu/i/D Z
aload 0
getfield android/support/v7/internal/view/menu/i/E Z
ifeq L0
aload 0
iconst_0
putfield android/support/v7/internal/view/menu/i/E Z
aload 0
iconst_1
invokevirtual android/support/v7/internal/view/menu/i/c(Z)V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method final f()V
aload 0
iconst_1
putfield android/support/v7/internal/view/menu/i/y Z
aload 0
iconst_1
invokevirtual android/support/v7/internal/view/menu/i/c(Z)V
return
.limit locals 1
.limit stack 2
.end method

.method public findItem(I)Landroid/view/MenuItem;
aload 0
invokevirtual android/support/v7/internal/view/menu/i/size()I
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 0
getfield android/support/v7/internal/view/menu/i/g Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/m
astore 4
aload 4
invokevirtual android/support/v7/internal/view/menu/m/getItemId()I
iload 1
if_icmpne L2
L3:
aload 4
areturn
L2:
aload 4
invokevirtual android/support/v7/internal/view/menu/m/hasSubMenu()Z
ifeq L4
aload 4
invokevirtual android/support/v7/internal/view/menu/m/getSubMenu()Landroid/view/SubMenu;
iload 1
invokeinterface android/view/SubMenu/findItem(I)Landroid/view/MenuItem; 1
astore 5
aload 5
astore 4
aload 5
ifnonnull L3
L4:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aconst_null
areturn
.limit locals 6
.limit stack 2
.end method

.method final g()V
aload 0
iconst_1
putfield android/support/v7/internal/view/menu/i/A Z
aload 0
iconst_1
invokevirtual android/support/v7/internal/view/menu/i/c(Z)V
return
.limit locals 1
.limit stack 2
.end method

.method public getItem(I)Landroid/view/MenuItem;
aload 0
getfield android/support/v7/internal/view/menu/i/g Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/view/MenuItem
areturn
.limit locals 2
.limit stack 2
.end method

.method public final h()Ljava/util/ArrayList;
aload 0
getfield android/support/v7/internal/view/menu/i/y Z
ifne L0
aload 0
getfield android/support/v7/internal/view/menu/i/x Ljava/util/ArrayList;
areturn
L0:
aload 0
getfield android/support/v7/internal/view/menu/i/x Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
aload 0
getfield android/support/v7/internal/view/menu/i/g Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 2
iconst_0
istore 1
L1:
iload 1
iload 2
if_icmpge L2
aload 0
getfield android/support/v7/internal/view/menu/i/g Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/m
astore 3
aload 3
invokevirtual android/support/v7/internal/view/menu/m/isVisible()Z
ifeq L3
aload 0
getfield android/support/v7/internal/view/menu/i/x Ljava/util/ArrayList;
aload 3
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L3:
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
aload 0
iconst_0
putfield android/support/v7/internal/view/menu/i/y Z
aload 0
iconst_1
putfield android/support/v7/internal/view/menu/i/A Z
aload 0
getfield android/support/v7/internal/view/menu/i/x Ljava/util/ArrayList;
areturn
.limit locals 4
.limit stack 2
.end method

.method public hasVisibleItems()Z
aload 0
getfield android/support/v7/internal/view/menu/i/o Z
ifeq L0
iconst_1
ireturn
L0:
aload 0
invokevirtual android/support/v7/internal/view/menu/i/size()I
istore 2
iconst_0
istore 1
L1:
iload 1
iload 2
if_icmpge L2
aload 0
getfield android/support/v7/internal/view/menu/i/g Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/m
invokevirtual android/support/v7/internal/view/menu/m/isVisible()Z
ifeq L3
iconst_1
ireturn
L3:
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method public final i()V
aload 0
invokevirtual android/support/v7/internal/view/menu/i/h()Ljava/util/ArrayList;
astore 3
aload 0
getfield android/support/v7/internal/view/menu/i/A Z
ifne L0
return
L0:
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
invokevirtual java/util/concurrent/CopyOnWriteArrayList/iterator()Ljava/util/Iterator;
astore 4
iconst_0
istore 1
L1:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/ref/WeakReference
astore 5
aload 5
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/x
astore 6
aload 6
ifnonnull L3
aload 0
getfield android/support/v7/internal/view/menu/i/H Ljava/util/concurrent/CopyOnWriteArrayList;
aload 5
invokevirtual java/util/concurrent/CopyOnWriteArrayList/remove(Ljava/lang/Object;)Z
pop
goto L1
L3:
aload 6
invokeinterface android/support/v7/internal/view/menu/x/a()Z 0
iload 1
ior
istore 1
goto L1
L2:
iload 1
ifeq L4
aload 0
getfield android/support/v7/internal/view/menu/i/h Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
aload 0
getfield android/support/v7/internal/view/menu/i/z Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
aload 3
invokevirtual java/util/ArrayList/size()I
istore 2
iconst_0
istore 1
L5:
iload 1
iload 2
if_icmpge L6
aload 3
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/m
astore 4
aload 4
invokevirtual android/support/v7/internal/view/menu/m/f()Z
ifeq L7
aload 0
getfield android/support/v7/internal/view/menu/i/h Ljava/util/ArrayList;
aload 4
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L8:
iload 1
iconst_1
iadd
istore 1
goto L5
L7:
aload 0
getfield android/support/v7/internal/view/menu/i/z Ljava/util/ArrayList;
aload 4
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
goto L8
L4:
aload 0
getfield android/support/v7/internal/view/menu/i/h Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
aload 0
getfield android/support/v7/internal/view/menu/i/z Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
aload 0
getfield android/support/v7/internal/view/menu/i/z Ljava/util/ArrayList;
aload 0
invokevirtual android/support/v7/internal/view/menu/i/h()Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/addAll(Ljava/util/Collection;)Z
pop
L6:
aload 0
iconst_0
putfield android/support/v7/internal/view/menu/i/A Z
return
.limit locals 7
.limit stack 2
.end method

.method public isShortcutKey(ILandroid/view/KeyEvent;)Z
aload 0
iload 1
aload 2
invokespecial android/support/v7/internal/view/menu/i/a(ILandroid/view/KeyEvent;)Landroid/support/v7/internal/view/menu/m;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final j()Ljava/util/ArrayList;
aload 0
invokevirtual android/support/v7/internal/view/menu/i/i()V
aload 0
getfield android/support/v7/internal/view/menu/i/z Ljava/util/ArrayList;
areturn
.limit locals 1
.limit stack 1
.end method

.method public k()Landroid/support/v7/internal/view/menu/i;
aload 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public performIdentifierAction(II)Z
aload 0
aload 0
iload 1
invokevirtual android/support/v7/internal/view/menu/i/findItem(I)Landroid/view/MenuItem;
aconst_null
iload 2
invokevirtual android/support/v7/internal/view/menu/i/a(Landroid/view/MenuItem;Landroid/support/v7/internal/view/menu/x;I)Z
ireturn
.limit locals 3
.limit stack 4
.end method

.method public performShortcut(ILandroid/view/KeyEvent;I)Z
aload 0
iload 1
aload 2
invokespecial android/support/v7/internal/view/menu/i/a(ILandroid/view/KeyEvent;)Landroid/support/v7/internal/view/menu/m;
astore 2
iconst_0
istore 4
aload 2
ifnull L0
aload 0
aload 2
aconst_null
iload 3
invokevirtual android/support/v7/internal/view/menu/i/a(Landroid/view/MenuItem;Landroid/support/v7/internal/view/menu/x;I)Z
istore 4
L0:
iload 3
iconst_2
iand
ifeq L1
aload 0
iconst_1
invokevirtual android/support/v7/internal/view/menu/i/b(Z)V
L1:
iload 4
ireturn
.limit locals 5
.limit stack 4
.end method

.method public removeGroup(I)V
aload 0
invokevirtual android/support/v7/internal/view/menu/i/size()I
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 0
getfield android/support/v7/internal/view/menu/i/g Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/m
invokevirtual android/support/v7/internal/view/menu/m/getGroupId()I
iload 1
if_icmpne L2
L3:
iload 2
iflt L4
aload 0
getfield android/support/v7/internal/view/menu/i/g Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 4
iconst_0
istore 3
L5:
iload 3
iload 4
iload 2
isub
if_icmpge L6
aload 0
getfield android/support/v7/internal/view/menu/i/g Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/m
invokevirtual android/support/v7/internal/view/menu/m/getGroupId()I
iload 1
if_icmpne L6
aload 0
iload 2
iconst_0
invokespecial android/support/v7/internal/view/menu/i/a(IZ)V
iload 3
iconst_1
iadd
istore 3
goto L5
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
iconst_m1
istore 2
goto L3
L6:
aload 0
iconst_1
invokevirtual android/support/v7/internal/view/menu/i/c(Z)V
L4:
return
.limit locals 5
.limit stack 3
.end method

.method public removeItem(I)V
aload 0
invokevirtual android/support/v7/internal/view/menu/i/size()I
istore 3
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 0
getfield android/support/v7/internal/view/menu/i/g Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/m
invokevirtual android/support/v7/internal/view/menu/m/getItemId()I
iload 1
if_icmpne L2
L3:
aload 0
iload 2
iconst_1
invokespecial android/support/v7/internal/view/menu/i/a(IZ)V
return
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
iconst_m1
istore 2
goto L3
.limit locals 4
.limit stack 3
.end method

.method public setGroupCheckable(IZZ)V
aload 0
getfield android/support/v7/internal/view/menu/i/g Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 5
iconst_0
istore 4
L0:
iload 4
iload 5
if_icmpge L1
aload 0
getfield android/support/v7/internal/view/menu/i/g Ljava/util/ArrayList;
iload 4
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/m
astore 6
aload 6
invokevirtual android/support/v7/internal/view/menu/m/getGroupId()I
iload 1
if_icmpne L2
aload 6
iload 3
invokevirtual android/support/v7/internal/view/menu/m/a(Z)V
aload 6
iload 2
invokevirtual android/support/v7/internal/view/menu/m/setCheckable(Z)Landroid/view/MenuItem;
pop
L2:
iload 4
iconst_1
iadd
istore 4
goto L0
L1:
return
.limit locals 7
.limit stack 2
.end method

.method public setGroupEnabled(IZ)V
aload 0
getfield android/support/v7/internal/view/menu/i/g Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 4
iconst_0
istore 3
L0:
iload 3
iload 4
if_icmpge L1
aload 0
getfield android/support/v7/internal/view/menu/i/g Ljava/util/ArrayList;
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/m
astore 5
aload 5
invokevirtual android/support/v7/internal/view/menu/m/getGroupId()I
iload 1
if_icmpne L2
aload 5
iload 2
invokevirtual android/support/v7/internal/view/menu/m/setEnabled(Z)Landroid/view/MenuItem;
pop
L2:
iload 3
iconst_1
iadd
istore 3
goto L0
L1:
return
.limit locals 6
.limit stack 2
.end method

.method public setGroupVisible(IZ)V
aload 0
getfield android/support/v7/internal/view/menu/i/g Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 5
iconst_0
istore 4
iconst_0
istore 3
L0:
iload 4
iload 5
if_icmpge L1
aload 0
getfield android/support/v7/internal/view/menu/i/g Ljava/util/ArrayList;
iload 4
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/view/menu/m
astore 6
aload 6
invokevirtual android/support/v7/internal/view/menu/m/getGroupId()I
iload 1
if_icmpne L2
aload 6
iload 2
invokevirtual android/support/v7/internal/view/menu/m/c(Z)Z
ifeq L2
iconst_1
istore 3
L3:
iload 4
iconst_1
iadd
istore 4
goto L0
L1:
iload 3
ifeq L4
aload 0
iconst_1
invokevirtual android/support/v7/internal/view/menu/i/c(Z)V
L4:
return
L2:
goto L3
.limit locals 7
.limit stack 2
.end method

.method public setQwertyMode(Z)V
aload 0
iload 1
putfield android/support/v7/internal/view/menu/i/v Z
aload 0
iconst_0
invokevirtual android/support/v7/internal/view/menu/i/c(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method public size()I
aload 0
getfield android/support/v7/internal/view/menu/i/g Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ireturn
.limit locals 1
.limit stack 1
.end method
