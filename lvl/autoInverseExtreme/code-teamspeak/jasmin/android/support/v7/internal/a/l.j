.bytecode 50.0
.class public synchronized android/support/v7/internal/a/l
.super android/support/v7/app/a
.implements android/support/v7/internal/widget/j

.field private static final 'L' I = -1


.field private static final 'M' J = 100L


.field private static final 'N' J = 200L


.field static final synthetic 'r' Z

.field private static final 's' Ljava/lang/String; = "WindowDecorActionBar"

.field private static final 't' Landroid/view/animation/Interpolator;

.field private static final 'u' Landroid/view/animation/Interpolator;

.field private static final 'v' Z

.field private 'A' Landroid/support/v7/internal/widget/ActionBarContainer;

.field private 'B' Landroid/support/v7/internal/widget/ad;

.field private 'C' Landroid/support/v7/internal/widget/ActionBarContextView;

.field private 'D' Landroid/view/View;

.field private 'E' Landroid/support/v7/internal/widget/al;

.field private 'F' Ljava/util/ArrayList;

.field private 'G' Landroid/support/v7/internal/a/q;

.field private 'H' I

.field private 'I' Z

.field private 'J' Z

.field private 'K' Ljava/util/ArrayList;

.field private 'O' Z

.field private 'P' I

.field private 'Q' Z

.field private 'R' Z

.field private 'S' Z

.field private 'T' Z

.field private 'U' Z

.field private 'V' Landroid/support/v7/internal/view/i;

.field private 'W' Z

.field 'i' Landroid/content/Context;

.field 'j' Landroid/support/v7/internal/a/p;

.field 'k' Landroid/support/v7/c/a;

.field 'l' Landroid/support/v7/c/b;

.field 'm' Z

.field 'n' Landroid/support/v7/internal/widget/av;

.field final 'o' Landroid/support/v4/view/gd;

.field final 'p' Landroid/support/v4/view/gd;

.field final 'q' Landroid/support/v4/view/gf;

.field private 'w' Landroid/content/Context;

.field private 'x' Landroid/app/Activity;

.field private 'y' Landroid/app/Dialog;

.field private 'z' Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

.method static <clinit>()V
iconst_1
istore 1
ldc android/support/v7/internal/a/l
invokevirtual java/lang/Class/desiredAssertionStatus()Z
ifne L0
iconst_1
istore 0
L1:
iload 0
putstatic android/support/v7/internal/a/l/r Z
new android/view/animation/AccelerateInterpolator
dup
invokespecial android/view/animation/AccelerateInterpolator/<init>()V
putstatic android/support/v7/internal/a/l/t Landroid/view/animation/Interpolator;
new android/view/animation/DecelerateInterpolator
dup
invokespecial android/view/animation/DecelerateInterpolator/<init>()V
putstatic android/support/v7/internal/a/l/u Landroid/view/animation/Interpolator;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 14
if_icmplt L2
iload 1
istore 0
L3:
iload 0
putstatic android/support/v7/internal/a/l/v Z
return
L0:
iconst_0
istore 0
goto L1
L2:
iconst_0
istore 0
goto L3
.limit locals 2
.limit stack 2
.end method

.method public <init>(Landroid/app/Activity;Z)V
aload 0
invokespecial android/support/v7/app/a/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v7/internal/a/l/F Ljava/util/ArrayList;
aload 0
iconst_m1
putfield android/support/v7/internal/a/l/H I
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v7/internal/a/l/K Ljava/util/ArrayList;
aload 0
iconst_0
putfield android/support/v7/internal/a/l/P I
aload 0
iconst_1
putfield android/support/v7/internal/a/l/Q Z
aload 0
iconst_1
putfield android/support/v7/internal/a/l/U Z
aload 0
new android/support/v7/internal/a/m
dup
aload 0
invokespecial android/support/v7/internal/a/m/<init>(Landroid/support/v7/internal/a/l;)V
putfield android/support/v7/internal/a/l/o Landroid/support/v4/view/gd;
aload 0
new android/support/v7/internal/a/n
dup
aload 0
invokespecial android/support/v7/internal/a/n/<init>(Landroid/support/v7/internal/a/l;)V
putfield android/support/v7/internal/a/l/p Landroid/support/v4/view/gd;
aload 0
new android/support/v7/internal/a/o
dup
aload 0
invokespecial android/support/v7/internal/a/o/<init>(Landroid/support/v7/internal/a/l;)V
putfield android/support/v7/internal/a/l/q Landroid/support/v4/view/gf;
aload 0
aload 1
putfield android/support/v7/internal/a/l/x Landroid/app/Activity;
aload 1
invokevirtual android/app/Activity/getWindow()Landroid/view/Window;
invokevirtual android/view/Window/getDecorView()Landroid/view/View;
astore 1
aload 0
aload 1
invokespecial android/support/v7/internal/a/l/b(Landroid/view/View;)V
iload 2
ifne L0
aload 0
aload 1
ldc_w 16908290
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
putfield android/support/v7/internal/a/l/D Landroid/view/View;
L0:
return
.limit locals 3
.limit stack 4
.end method

.method public <init>(Landroid/app/Dialog;)V
aload 0
invokespecial android/support/v7/app/a/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v7/internal/a/l/F Ljava/util/ArrayList;
aload 0
iconst_m1
putfield android/support/v7/internal/a/l/H I
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v7/internal/a/l/K Ljava/util/ArrayList;
aload 0
iconst_0
putfield android/support/v7/internal/a/l/P I
aload 0
iconst_1
putfield android/support/v7/internal/a/l/Q Z
aload 0
iconst_1
putfield android/support/v7/internal/a/l/U Z
aload 0
new android/support/v7/internal/a/m
dup
aload 0
invokespecial android/support/v7/internal/a/m/<init>(Landroid/support/v7/internal/a/l;)V
putfield android/support/v7/internal/a/l/o Landroid/support/v4/view/gd;
aload 0
new android/support/v7/internal/a/n
dup
aload 0
invokespecial android/support/v7/internal/a/n/<init>(Landroid/support/v7/internal/a/l;)V
putfield android/support/v7/internal/a/l/p Landroid/support/v4/view/gd;
aload 0
new android/support/v7/internal/a/o
dup
aload 0
invokespecial android/support/v7/internal/a/o/<init>(Landroid/support/v7/internal/a/l;)V
putfield android/support/v7/internal/a/l/q Landroid/support/v4/view/gf;
aload 0
aload 1
putfield android/support/v7/internal/a/l/y Landroid/app/Dialog;
aload 0
aload 1
invokevirtual android/app/Dialog/getWindow()Landroid/view/Window;
invokevirtual android/view/Window/getDecorView()Landroid/view/View;
invokespecial android/support/v7/internal/a/l/b(Landroid/view/View;)V
return
.limit locals 2
.limit stack 4
.end method

.method private <init>(Landroid/view/View;)V
aload 0
invokespecial android/support/v7/app/a/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v7/internal/a/l/F Ljava/util/ArrayList;
aload 0
iconst_m1
putfield android/support/v7/internal/a/l/H I
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v7/internal/a/l/K Ljava/util/ArrayList;
aload 0
iconst_0
putfield android/support/v7/internal/a/l/P I
aload 0
iconst_1
putfield android/support/v7/internal/a/l/Q Z
aload 0
iconst_1
putfield android/support/v7/internal/a/l/U Z
aload 0
new android/support/v7/internal/a/m
dup
aload 0
invokespecial android/support/v7/internal/a/m/<init>(Landroid/support/v7/internal/a/l;)V
putfield android/support/v7/internal/a/l/o Landroid/support/v4/view/gd;
aload 0
new android/support/v7/internal/a/n
dup
aload 0
invokespecial android/support/v7/internal/a/n/<init>(Landroid/support/v7/internal/a/l;)V
putfield android/support/v7/internal/a/l/p Landroid/support/v4/view/gd;
aload 0
new android/support/v7/internal/a/o
dup
aload 0
invokespecial android/support/v7/internal/a/o/<init>(Landroid/support/v7/internal/a/l;)V
putfield android/support/v7/internal/a/l/q Landroid/support/v4/view/gf;
getstatic android/support/v7/internal/a/l/r Z
ifne L0
aload 1
invokevirtual android/view/View/isInEditMode()Z
ifne L0
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L0:
aload 0
aload 1
invokespecial android/support/v7/internal/a/l/b(Landroid/view/View;)V
return
.limit locals 2
.limit stack 4
.end method

.method private E()V
aload 0
getfield android/support/v7/internal/a/l/E Landroid/support/v7/internal/widget/al;
ifnull L0
return
L0:
new android/support/v7/internal/widget/al
dup
aload 0
getfield android/support/v7/internal/a/l/i Landroid/content/Context;
invokespecial android/support/v7/internal/widget/al/<init>(Landroid/content/Context;)V
astore 1
aload 0
getfield android/support/v7/internal/a/l/O Z
ifeq L1
aload 1
iconst_0
invokevirtual android/support/v7/internal/widget/al/setVisibility(I)V
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
aload 1
invokeinterface android/support/v7/internal/widget/ad/a(Landroid/support/v7/internal/widget/al;)V 1
L2:
aload 0
aload 1
putfield android/support/v7/internal/a/l/E Landroid/support/v7/internal/widget/al;
return
L1:
aload 0
invokevirtual android/support/v7/internal/a/l/g()I
iconst_2
if_icmpne L3
aload 1
iconst_0
invokevirtual android/support/v7/internal/widget/al/setVisibility(I)V
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
ifnull L4
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
invokestatic android/support/v4/view/cx/t(Landroid/view/View;)V
L4:
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
aload 1
invokevirtual android/support/v7/internal/widget/ActionBarContainer/setTabContainer(Landroid/support/v7/internal/widget/al;)V
goto L2
L3:
aload 1
bipush 8
invokevirtual android/support/v7/internal/widget/al/setVisibility(I)V
goto L4
.limit locals 2
.limit stack 3
.end method

.method private F()V
aload 0
getfield android/support/v7/internal/a/l/l Landroid/support/v7/c/b;
ifnull L0
aload 0
getfield android/support/v7/internal/a/l/l Landroid/support/v7/c/b;
aload 0
getfield android/support/v7/internal/a/l/k Landroid/support/v7/c/a;
invokeinterface android/support/v7/c/b/a(Landroid/support/v7/c/a;)V 1
aload 0
aconst_null
putfield android/support/v7/internal/a/l/k Landroid/support/v7/c/a;
aload 0
aconst_null
putfield android/support/v7/internal/a/l/l Landroid/support/v7/c/b;
L0:
return
.limit locals 1
.limit stack 2
.end method

.method private G()V
aload 0
getfield android/support/v7/internal/a/l/G Landroid/support/v7/internal/a/q;
ifnull L0
aload 0
aconst_null
invokevirtual android/support/v7/internal/a/l/c(Landroid/support/v7/app/g;)V
L0:
aload 0
getfield android/support/v7/internal/a/l/F Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
aload 0
getfield android/support/v7/internal/a/l/E Landroid/support/v7/internal/widget/al;
ifnull L1
aload 0
getfield android/support/v7/internal/a/l/E Landroid/support/v7/internal/widget/al;
astore 1
aload 1
getfield android/support/v7/internal/widget/al/b Landroid/support/v7/widget/aj;
invokevirtual android/support/v7/widget/aj/removeAllViews()V
aload 1
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
ifnull L2
aload 1
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
invokevirtual android/widget/Spinner/getAdapter()Landroid/widget/SpinnerAdapter;
checkcast android/support/v7/internal/widget/an
invokevirtual android/support/v7/internal/widget/an/notifyDataSetChanged()V
L2:
aload 1
getfield android/support/v7/internal/widget/al/d Z
ifeq L1
aload 1
invokevirtual android/support/v7/internal/widget/al/requestLayout()V
L1:
aload 0
iconst_m1
putfield android/support/v7/internal/a/l/H I
return
.limit locals 2
.limit stack 2
.end method

.method private H()V
aload 0
getfield android/support/v7/internal/a/l/T Z
ifne L0
aload 0
iconst_1
putfield android/support/v7/internal/a/l/T Z
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
ifnull L1
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
iconst_1
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/setShowingForActionMode(Z)V
L1:
aload 0
iconst_0
invokespecial android/support/v7/internal/a/l/l(Z)V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method private I()V
aload 0
getfield android/support/v7/internal/a/l/T Z
ifeq L0
aload 0
iconst_0
putfield android/support/v7/internal/a/l/T Z
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
ifnull L1
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
iconst_0
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/setShowingForActionMode(Z)V
L1:
aload 0
iconst_0
invokespecial android/support/v7/internal/a/l/l(Z)V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method private J()Z
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/i()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private K()Z
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/j()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private L()Landroid/support/v7/internal/widget/av;
aload 0
getfield android/support/v7/internal/a/l/n Landroid/support/v7/internal/widget/av;
ifnonnull L0
aload 0
aload 0
getfield android/support/v7/internal/a/l/i Landroid/content/Context;
invokestatic android/support/v7/internal/widget/av/a(Landroid/content/Context;)Landroid/support/v7/internal/widget/av;
putfield android/support/v7/internal/a/l/n Landroid/support/v7/internal/widget/av;
L0:
aload 0
getfield android/support/v7/internal/a/l/n Landroid/support/v7/internal/widget/av;
areturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic a(Landroid/support/v7/internal/a/l;)Z
aload 0
getfield android/support/v7/internal/a/l/Q Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(ZZ)Z
iload 0
iload 1
iconst_0
invokestatic android/support/v7/internal/a/l/a(ZZZ)Z
ireturn
.limit locals 2
.limit stack 3
.end method

.method private static a(ZZZ)Z
iload 2
ifeq L0
L1:
iconst_1
ireturn
L0:
iload 0
ifne L2
iload 1
ifeq L1
L2:
iconst_0
ireturn
.limit locals 3
.limit stack 1
.end method

.method static synthetic b(Landroid/support/v7/internal/a/l;)Landroid/view/View;
aload 0
getfield android/support/v7/internal/a/l/D Landroid/view/View;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b(Landroid/support/v7/app/g;I)V
aload 1
checkcast android/support/v7/internal/a/q
astore 1
aload 1
getfield android/support/v7/internal/a/q/b Landroid/support/v7/app/h;
ifnonnull L0
new java/lang/IllegalStateException
dup
ldc "Action Bar Tab must have a Callback"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
iload 2
putfield android/support/v7/internal/a/q/c I
aload 0
getfield android/support/v7/internal/a/l/F Ljava/util/ArrayList;
iload 2
aload 1
invokevirtual java/util/ArrayList/add(ILjava/lang/Object;)V
aload 0
getfield android/support/v7/internal/a/l/F Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 3
iload 2
iconst_1
iadd
istore 2
L1:
iload 2
iload 3
if_icmpge L2
aload 0
getfield android/support/v7/internal/a/l/F Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/a/q
iload 2
putfield android/support/v7/internal/a/q/c I
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
return
.limit locals 4
.limit stack 3
.end method

.method private b(Landroid/view/View;)V
aload 0
aload 1
getstatic android/support/v7/a/i/decor_content_parent I
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/support/v7/internal/widget/ActionBarOverlayLayout
putfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
ifnull L0
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
aload 0
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/setActionBarVisibilityCallback(Landroid/support/v7/internal/widget/j;)V
L0:
aload 1
getstatic android/support/v7/a/i/action_bar I
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
astore 4
aload 4
instanceof android/support/v7/internal/widget/ad
ifeq L1
aload 4
checkcast android/support/v7/internal/widget/ad
astore 4
L2:
aload 0
aload 4
putfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
aload 0
aload 1
getstatic android/support/v7/a/i/action_context_bar I
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/support/v7/internal/widget/ActionBarContextView
putfield android/support/v7/internal/a/l/C Landroid/support/v7/internal/widget/ActionBarContextView;
aload 0
aload 1
getstatic android/support/v7/a/i/action_bar_container I
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/support/v7/internal/widget/ActionBarContainer
putfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
ifnull L3
aload 0
getfield android/support/v7/internal/a/l/C Landroid/support/v7/internal/widget/ActionBarContextView;
ifnull L3
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
ifnonnull L4
L3:
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " can only be used with a compatible window decor layout"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 4
instanceof android/support/v7/widget/Toolbar
ifeq L5
aload 4
checkcast android/support/v7/widget/Toolbar
invokevirtual android/support/v7/widget/Toolbar/getWrapper()Landroid/support/v7/internal/widget/ad;
astore 4
goto L2
L5:
new java/lang/StringBuilder
dup
ldc "Can't make a decor toolbar out of "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
ifnull L6
aload 4
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
astore 1
L7:
new java/lang/IllegalStateException
dup
aload 1
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L6:
ldc "null"
astore 1
goto L7
L4:
aload 0
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/b()Landroid/content/Context; 0
putfield android/support/v7/internal/a/l/i Landroid/content/Context;
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/r()I 0
iconst_4
iand
ifeq L8
iconst_1
istore 3
L9:
iload 3
ifeq L10
aload 0
iconst_1
putfield android/support/v7/internal/a/l/I Z
L10:
aload 0
getfield android/support/v7/internal/a/l/i Landroid/content/Context;
invokestatic android/support/v7/internal/view/a/a(Landroid/content/Context;)Landroid/support/v7/internal/view/a;
astore 1
aload 1
getfield android/support/v7/internal/view/a/a Landroid/content/Context;
invokevirtual android/content/Context/getApplicationInfo()Landroid/content/pm/ApplicationInfo;
getfield android/content/pm/ApplicationInfo/targetSdkVersion I
istore 3
aload 0
aload 1
invokevirtual android/support/v7/internal/view/a/a()Z
invokespecial android/support/v7/internal/a/l/k(Z)V
aload 0
getfield android/support/v7/internal/a/l/i Landroid/content/Context;
aconst_null
getstatic android/support/v7/a/n/ActionBar [I
getstatic android/support/v7/a/d/actionBarStyle I
iconst_0
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
astore 1
aload 1
getstatic android/support/v7/a/n/ActionBar_hideOnContentScroll I
iconst_0
invokevirtual android/content/res/TypedArray/getBoolean(IZ)Z
ifeq L11
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/a Z
ifne L12
new java/lang/IllegalStateException
dup
ldc "Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L8:
iconst_0
istore 3
goto L9
L12:
aload 0
iconst_1
putfield android/support/v7/internal/a/l/m Z
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
iconst_1
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/setHideOnContentScrollEnabled(Z)V
L11:
aload 1
getstatic android/support/v7/a/n/ActionBar_elevation I
iconst_0
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
istore 3
iload 3
ifeq L13
iload 3
i2f
fstore 2
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
fload 2
invokestatic android/support/v4/view/cx/f(Landroid/view/View;F)V
L13:
aload 1
invokevirtual android/content/res/TypedArray/recycle()V
return
.limit locals 5
.limit stack 5
.end method

.method static synthetic c(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ActionBarContainer;
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static c(Landroid/view/View;)Landroid/support/v7/internal/widget/ad;
aload 0
instanceof android/support/v7/internal/widget/ad
ifeq L0
aload 0
checkcast android/support/v7/internal/widget/ad
areturn
L0:
aload 0
instanceof android/support/v7/widget/Toolbar
ifeq L1
aload 0
checkcast android/support/v7/widget/Toolbar
invokevirtual android/support/v7/widget/Toolbar/getWrapper()Landroid/support/v7/internal/widget/ad;
areturn
L1:
new java/lang/StringBuilder
dup
ldc "Can't make a decor toolbar out of "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
ifnull L2
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
astore 0
L3:
new java/lang/IllegalStateException
dup
aload 0
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L2:
ldc "null"
astore 0
goto L3
.limit locals 1
.limit stack 3
.end method

.method static synthetic d(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/view/i;
aload 0
aconst_null
putfield android/support/v7/internal/a/l/V Landroid/support/v7/internal/view/i;
aconst_null
areturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic e(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic f(Landroid/support/v7/internal/a/l;)Z
aload 0
getfield android/support/v7/internal/a/l/R Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic g(Landroid/support/v7/internal/a/l;)Z
aload 0
getfield android/support/v7/internal/a/l/S Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic h(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ActionBarContextView;
aload 0
getfield android/support/v7/internal/a/l/C Landroid/support/v7/internal/widget/ActionBarContextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic i(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/ad;
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic j(Landroid/support/v7/internal/a/l;)Landroid/content/Context;
aload 0
getfield android/support/v7/internal/a/l/i Landroid/content/Context;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic k(Landroid/support/v7/internal/a/l;)Landroid/support/v7/internal/widget/al;
aload 0
getfield android/support/v7/internal/a/l/E Landroid/support/v7/internal/widget/al;
areturn
.limit locals 1
.limit stack 1
.end method

.method private k(Z)V
iconst_1
istore 3
aload 0
iload 1
putfield android/support/v7/internal/a/l/O Z
aload 0
getfield android/support/v7/internal/a/l/O Z
ifne L0
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
aconst_null
invokeinterface android/support/v7/internal/widget/ad/a(Landroid/support/v7/internal/widget/al;)V 1
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
aload 0
getfield android/support/v7/internal/a/l/E Landroid/support/v7/internal/widget/al;
invokevirtual android/support/v7/internal/widget/ActionBarContainer/setTabContainer(Landroid/support/v7/internal/widget/al;)V
L1:
aload 0
invokevirtual android/support/v7/internal/a/l/g()I
iconst_2
if_icmpne L2
iconst_1
istore 2
L3:
aload 0
getfield android/support/v7/internal/a/l/E Landroid/support/v7/internal/widget/al;
ifnull L4
iload 2
ifeq L5
aload 0
getfield android/support/v7/internal/a/l/E Landroid/support/v7/internal/widget/al;
iconst_0
invokevirtual android/support/v7/internal/widget/al/setVisibility(I)V
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
ifnull L4
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
invokestatic android/support/v4/view/cx/t(Landroid/view/View;)V
L4:
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
astore 4
aload 0
getfield android/support/v7/internal/a/l/O Z
ifne L6
iload 2
ifeq L6
iconst_1
istore 1
L7:
aload 4
iload 1
invokeinterface android/support/v7/internal/widget/ad/a(Z)V 1
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
astore 4
aload 0
getfield android/support/v7/internal/a/l/O Z
ifne L8
iload 2
ifeq L8
iload 3
istore 1
L9:
aload 4
iload 1
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/setHasNonEmbeddedTabs(Z)V
return
L0:
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
aconst_null
invokevirtual android/support/v7/internal/widget/ActionBarContainer/setTabContainer(Landroid/support/v7/internal/widget/al;)V
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
aload 0
getfield android/support/v7/internal/a/l/E Landroid/support/v7/internal/widget/al;
invokeinterface android/support/v7/internal/widget/ad/a(Landroid/support/v7/internal/widget/al;)V 1
goto L1
L2:
iconst_0
istore 2
goto L3
L5:
aload 0
getfield android/support/v7/internal/a/l/E Landroid/support/v7/internal/widget/al;
bipush 8
invokevirtual android/support/v7/internal/widget/al/setVisibility(I)V
goto L4
L6:
iconst_0
istore 1
goto L7
L8:
iconst_0
istore 1
goto L9
.limit locals 5
.limit stack 2
.end method

.method private l(Z)V
aload 0
getfield android/support/v7/internal/a/l/R Z
aload 0
getfield android/support/v7/internal/a/l/S Z
aload 0
getfield android/support/v7/internal/a/l/T Z
invokestatic android/support/v7/internal/a/l/a(ZZZ)Z
ifeq L0
aload 0
getfield android/support/v7/internal/a/l/U Z
ifne L1
aload 0
iconst_1
putfield android/support/v7/internal/a/l/U Z
aload 0
getfield android/support/v7/internal/a/l/V Landroid/support/v7/internal/view/i;
ifnull L2
aload 0
getfield android/support/v7/internal/a/l/V Landroid/support/v7/internal/view/i;
invokevirtual android/support/v7/internal/view/i/b()V
L2:
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
iconst_0
invokevirtual android/support/v7/internal/widget/ActionBarContainer/setVisibility(I)V
aload 0
getfield android/support/v7/internal/a/l/P I
ifne L3
getstatic android/support/v7/internal/a/l/v Z
ifeq L3
aload 0
getfield android/support/v7/internal/a/l/W Z
ifne L4
iload 1
ifeq L3
L4:
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
fconst_0
invokestatic android/support/v4/view/cx/b(Landroid/view/View;F)V
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getHeight()I
ineg
i2f
fstore 3
fload 3
fstore 2
iload 1
ifeq L5
iconst_2
newarray int
astore 4
aload 4
dup
iconst_0
iconst_0
iastore
dup
iconst_1
iconst_0
iastore
pop
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
aload 4
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getLocationInWindow([I)V
fload 3
aload 4
iconst_1
iaload
i2f
fsub
fstore 2
L5:
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
fload 2
invokestatic android/support/v4/view/cx/b(Landroid/view/View;F)V
new android/support/v7/internal/view/i
dup
invokespecial android/support/v7/internal/view/i/<init>()V
astore 4
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
invokestatic android/support/v4/view/cx/p(Landroid/view/View;)Landroid/support/v4/view/fk;
fconst_0
invokevirtual android/support/v4/view/fk/b(F)Landroid/support/v4/view/fk;
astore 5
aload 5
aload 0
getfield android/support/v7/internal/a/l/q Landroid/support/v4/view/gf;
invokevirtual android/support/v4/view/fk/a(Landroid/support/v4/view/gf;)Landroid/support/v4/view/fk;
pop
aload 4
aload 5
invokevirtual android/support/v7/internal/view/i/a(Landroid/support/v4/view/fk;)Landroid/support/v7/internal/view/i;
pop
aload 0
getfield android/support/v7/internal/a/l/Q Z
ifeq L6
aload 0
getfield android/support/v7/internal/a/l/D Landroid/view/View;
ifnull L6
aload 0
getfield android/support/v7/internal/a/l/D Landroid/view/View;
fload 2
invokestatic android/support/v4/view/cx/b(Landroid/view/View;F)V
aload 4
aload 0
getfield android/support/v7/internal/a/l/D Landroid/view/View;
invokestatic android/support/v4/view/cx/p(Landroid/view/View;)Landroid/support/v4/view/fk;
fconst_0
invokevirtual android/support/v4/view/fk/b(F)Landroid/support/v4/view/fk;
invokevirtual android/support/v7/internal/view/i/a(Landroid/support/v4/view/fk;)Landroid/support/v7/internal/view/i;
pop
L6:
aload 4
getstatic android/support/v7/internal/a/l/u Landroid/view/animation/Interpolator;
invokevirtual android/support/v7/internal/view/i/a(Landroid/view/animation/Interpolator;)Landroid/support/v7/internal/view/i;
pop
aload 4
invokevirtual android/support/v7/internal/view/i/c()Landroid/support/v7/internal/view/i;
pop
aload 4
aload 0
getfield android/support/v7/internal/a/l/p Landroid/support/v4/view/gd;
invokevirtual android/support/v7/internal/view/i/a(Landroid/support/v4/view/gd;)Landroid/support/v7/internal/view/i;
pop
aload 0
aload 4
putfield android/support/v7/internal/a/l/V Landroid/support/v7/internal/view/i;
aload 4
invokevirtual android/support/v7/internal/view/i/a()V
L7:
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
ifnull L1
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
invokestatic android/support/v4/view/cx/t(Landroid/view/View;)V
L1:
return
L3:
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
fconst_1
invokestatic android/support/v4/view/cx/c(Landroid/view/View;F)V
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
fconst_0
invokestatic android/support/v4/view/cx/b(Landroid/view/View;F)V
aload 0
getfield android/support/v7/internal/a/l/Q Z
ifeq L8
aload 0
getfield android/support/v7/internal/a/l/D Landroid/view/View;
ifnull L8
aload 0
getfield android/support/v7/internal/a/l/D Landroid/view/View;
fconst_0
invokestatic android/support/v4/view/cx/b(Landroid/view/View;F)V
L8:
aload 0
getfield android/support/v7/internal/a/l/p Landroid/support/v4/view/gd;
aconst_null
invokeinterface android/support/v4/view/gd/b(Landroid/view/View;)V 1
goto L7
L0:
aload 0
getfield android/support/v7/internal/a/l/U Z
ifeq L1
aload 0
iconst_0
putfield android/support/v7/internal/a/l/U Z
aload 0
getfield android/support/v7/internal/a/l/V Landroid/support/v7/internal/view/i;
ifnull L9
aload 0
getfield android/support/v7/internal/a/l/V Landroid/support/v7/internal/view/i;
invokevirtual android/support/v7/internal/view/i/b()V
L9:
aload 0
getfield android/support/v7/internal/a/l/P I
ifne L10
getstatic android/support/v7/internal/a/l/v Z
ifeq L10
aload 0
getfield android/support/v7/internal/a/l/W Z
ifne L11
iload 1
ifeq L10
L11:
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
fconst_1
invokestatic android/support/v4/view/cx/c(Landroid/view/View;F)V
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
iconst_1
invokevirtual android/support/v7/internal/widget/ActionBarContainer/setTransitioning(Z)V
new android/support/v7/internal/view/i
dup
invokespecial android/support/v7/internal/view/i/<init>()V
astore 4
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getHeight()I
ineg
i2f
fstore 3
fload 3
fstore 2
iload 1
ifeq L12
iconst_2
newarray int
astore 5
aload 5
dup
iconst_0
iconst_0
iastore
dup
iconst_1
iconst_0
iastore
pop
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
aload 5
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getLocationInWindow([I)V
fload 3
aload 5
iconst_1
iaload
i2f
fsub
fstore 2
L12:
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
invokestatic android/support/v4/view/cx/p(Landroid/view/View;)Landroid/support/v4/view/fk;
fload 2
invokevirtual android/support/v4/view/fk/b(F)Landroid/support/v4/view/fk;
astore 5
aload 5
aload 0
getfield android/support/v7/internal/a/l/q Landroid/support/v4/view/gf;
invokevirtual android/support/v4/view/fk/a(Landroid/support/v4/view/gf;)Landroid/support/v4/view/fk;
pop
aload 4
aload 5
invokevirtual android/support/v7/internal/view/i/a(Landroid/support/v4/view/fk;)Landroid/support/v7/internal/view/i;
pop
aload 0
getfield android/support/v7/internal/a/l/Q Z
ifeq L13
aload 0
getfield android/support/v7/internal/a/l/D Landroid/view/View;
ifnull L13
aload 4
aload 0
getfield android/support/v7/internal/a/l/D Landroid/view/View;
invokestatic android/support/v4/view/cx/p(Landroid/view/View;)Landroid/support/v4/view/fk;
fload 2
invokevirtual android/support/v4/view/fk/b(F)Landroid/support/v4/view/fk;
invokevirtual android/support/v7/internal/view/i/a(Landroid/support/v4/view/fk;)Landroid/support/v7/internal/view/i;
pop
L13:
aload 4
getstatic android/support/v7/internal/a/l/t Landroid/view/animation/Interpolator;
invokevirtual android/support/v7/internal/view/i/a(Landroid/view/animation/Interpolator;)Landroid/support/v7/internal/view/i;
pop
aload 4
invokevirtual android/support/v7/internal/view/i/c()Landroid/support/v7/internal/view/i;
pop
aload 4
aload 0
getfield android/support/v7/internal/a/l/o Landroid/support/v4/view/gd;
invokevirtual android/support/v7/internal/view/i/a(Landroid/support/v4/view/gd;)Landroid/support/v7/internal/view/i;
pop
aload 0
aload 4
putfield android/support/v7/internal/a/l/V Landroid/support/v7/internal/view/i;
aload 4
invokevirtual android/support/v7/internal/view/i/a()V
return
L10:
aload 0
getfield android/support/v7/internal/a/l/o Landroid/support/v4/view/gd;
aconst_null
invokeinterface android/support/v4/view/gd/b(Landroid/view/View;)V 1
return
.limit locals 6
.limit stack 4
.end method

.method private m(Z)V
aload 0
getfield android/support/v7/internal/a/l/V Landroid/support/v7/internal/view/i;
ifnull L0
aload 0
getfield android/support/v7/internal/a/l/V Landroid/support/v7/internal/view/i;
invokevirtual android/support/v7/internal/view/i/b()V
L0:
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
iconst_0
invokevirtual android/support/v7/internal/widget/ActionBarContainer/setVisibility(I)V
aload 0
getfield android/support/v7/internal/a/l/P I
ifne L1
getstatic android/support/v7/internal/a/l/v Z
ifeq L1
aload 0
getfield android/support/v7/internal/a/l/W Z
ifne L2
iload 1
ifeq L1
L2:
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
fconst_0
invokestatic android/support/v4/view/cx/b(Landroid/view/View;F)V
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getHeight()I
ineg
i2f
fstore 3
fload 3
fstore 2
iload 1
ifeq L3
iconst_2
newarray int
astore 4
aload 4
dup
iconst_0
iconst_0
iastore
dup
iconst_1
iconst_0
iastore
pop
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
aload 4
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getLocationInWindow([I)V
fload 3
aload 4
iconst_1
iaload
i2f
fsub
fstore 2
L3:
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
fload 2
invokestatic android/support/v4/view/cx/b(Landroid/view/View;F)V
new android/support/v7/internal/view/i
dup
invokespecial android/support/v7/internal/view/i/<init>()V
astore 4
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
invokestatic android/support/v4/view/cx/p(Landroid/view/View;)Landroid/support/v4/view/fk;
fconst_0
invokevirtual android/support/v4/view/fk/b(F)Landroid/support/v4/view/fk;
astore 5
aload 5
aload 0
getfield android/support/v7/internal/a/l/q Landroid/support/v4/view/gf;
invokevirtual android/support/v4/view/fk/a(Landroid/support/v4/view/gf;)Landroid/support/v4/view/fk;
pop
aload 4
aload 5
invokevirtual android/support/v7/internal/view/i/a(Landroid/support/v4/view/fk;)Landroid/support/v7/internal/view/i;
pop
aload 0
getfield android/support/v7/internal/a/l/Q Z
ifeq L4
aload 0
getfield android/support/v7/internal/a/l/D Landroid/view/View;
ifnull L4
aload 0
getfield android/support/v7/internal/a/l/D Landroid/view/View;
fload 2
invokestatic android/support/v4/view/cx/b(Landroid/view/View;F)V
aload 4
aload 0
getfield android/support/v7/internal/a/l/D Landroid/view/View;
invokestatic android/support/v4/view/cx/p(Landroid/view/View;)Landroid/support/v4/view/fk;
fconst_0
invokevirtual android/support/v4/view/fk/b(F)Landroid/support/v4/view/fk;
invokevirtual android/support/v7/internal/view/i/a(Landroid/support/v4/view/fk;)Landroid/support/v7/internal/view/i;
pop
L4:
aload 4
getstatic android/support/v7/internal/a/l/u Landroid/view/animation/Interpolator;
invokevirtual android/support/v7/internal/view/i/a(Landroid/view/animation/Interpolator;)Landroid/support/v7/internal/view/i;
pop
aload 4
invokevirtual android/support/v7/internal/view/i/c()Landroid/support/v7/internal/view/i;
pop
aload 4
aload 0
getfield android/support/v7/internal/a/l/p Landroid/support/v4/view/gd;
invokevirtual android/support/v7/internal/view/i/a(Landroid/support/v4/view/gd;)Landroid/support/v7/internal/view/i;
pop
aload 0
aload 4
putfield android/support/v7/internal/a/l/V Landroid/support/v7/internal/view/i;
aload 4
invokevirtual android/support/v7/internal/view/i/a()V
L5:
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
ifnull L6
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
invokestatic android/support/v4/view/cx/t(Landroid/view/View;)V
L6:
return
L1:
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
fconst_1
invokestatic android/support/v4/view/cx/c(Landroid/view/View;F)V
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
fconst_0
invokestatic android/support/v4/view/cx/b(Landroid/view/View;F)V
aload 0
getfield android/support/v7/internal/a/l/Q Z
ifeq L7
aload 0
getfield android/support/v7/internal/a/l/D Landroid/view/View;
ifnull L7
aload 0
getfield android/support/v7/internal/a/l/D Landroid/view/View;
fconst_0
invokestatic android/support/v4/view/cx/b(Landroid/view/View;F)V
L7:
aload 0
getfield android/support/v7/internal/a/l/p Landroid/support/v4/view/gd;
aconst_null
invokeinterface android/support/v4/view/gd/b(Landroid/view/View;)V 1
goto L5
.limit locals 6
.limit stack 4
.end method

.method private n(Z)V
aload 0
getfield android/support/v7/internal/a/l/V Landroid/support/v7/internal/view/i;
ifnull L0
aload 0
getfield android/support/v7/internal/a/l/V Landroid/support/v7/internal/view/i;
invokevirtual android/support/v7/internal/view/i/b()V
L0:
aload 0
getfield android/support/v7/internal/a/l/P I
ifne L1
getstatic android/support/v7/internal/a/l/v Z
ifeq L1
aload 0
getfield android/support/v7/internal/a/l/W Z
ifne L2
iload 1
ifeq L1
L2:
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
fconst_1
invokestatic android/support/v4/view/cx/c(Landroid/view/View;F)V
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
iconst_1
invokevirtual android/support/v7/internal/widget/ActionBarContainer/setTransitioning(Z)V
new android/support/v7/internal/view/i
dup
invokespecial android/support/v7/internal/view/i/<init>()V
astore 4
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getHeight()I
ineg
i2f
fstore 3
fload 3
fstore 2
iload 1
ifeq L3
iconst_2
newarray int
astore 5
aload 5
dup
iconst_0
iconst_0
iastore
dup
iconst_1
iconst_0
iastore
pop
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
aload 5
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getLocationInWindow([I)V
fload 3
aload 5
iconst_1
iaload
i2f
fsub
fstore 2
L3:
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
invokestatic android/support/v4/view/cx/p(Landroid/view/View;)Landroid/support/v4/view/fk;
fload 2
invokevirtual android/support/v4/view/fk/b(F)Landroid/support/v4/view/fk;
astore 5
aload 5
aload 0
getfield android/support/v7/internal/a/l/q Landroid/support/v4/view/gf;
invokevirtual android/support/v4/view/fk/a(Landroid/support/v4/view/gf;)Landroid/support/v4/view/fk;
pop
aload 4
aload 5
invokevirtual android/support/v7/internal/view/i/a(Landroid/support/v4/view/fk;)Landroid/support/v7/internal/view/i;
pop
aload 0
getfield android/support/v7/internal/a/l/Q Z
ifeq L4
aload 0
getfield android/support/v7/internal/a/l/D Landroid/view/View;
ifnull L4
aload 4
aload 0
getfield android/support/v7/internal/a/l/D Landroid/view/View;
invokestatic android/support/v4/view/cx/p(Landroid/view/View;)Landroid/support/v4/view/fk;
fload 2
invokevirtual android/support/v4/view/fk/b(F)Landroid/support/v4/view/fk;
invokevirtual android/support/v7/internal/view/i/a(Landroid/support/v4/view/fk;)Landroid/support/v7/internal/view/i;
pop
L4:
aload 4
getstatic android/support/v7/internal/a/l/t Landroid/view/animation/Interpolator;
invokevirtual android/support/v7/internal/view/i/a(Landroid/view/animation/Interpolator;)Landroid/support/v7/internal/view/i;
pop
aload 4
invokevirtual android/support/v7/internal/view/i/c()Landroid/support/v7/internal/view/i;
pop
aload 4
aload 0
getfield android/support/v7/internal/a/l/o Landroid/support/v4/view/gd;
invokevirtual android/support/v7/internal/view/i/a(Landroid/support/v4/view/gd;)Landroid/support/v7/internal/view/i;
pop
aload 0
aload 4
putfield android/support/v7/internal/a/l/V Landroid/support/v7/internal/view/i;
aload 4
invokevirtual android/support/v7/internal/view/i/a()V
return
L1:
aload 0
getfield android/support/v7/internal/a/l/o Landroid/support/v4/view/gd;
aconst_null
invokeinterface android/support/v4/view/gd/b(Landroid/view/View;)V 1
return
.limit locals 6
.limit stack 4
.end method

.method public final A()V
aload 0
getfield android/support/v7/internal/a/l/S Z
ifeq L0
aload 0
iconst_0
putfield android/support/v7/internal/a/l/S Z
aload 0
iconst_1
invokespecial android/support/v7/internal/a/l/l(Z)V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public final B()V
aload 0
getfield android/support/v7/internal/a/l/S Z
ifne L0
aload 0
iconst_1
putfield android/support/v7/internal/a/l/S Z
aload 0
iconst_1
invokespecial android/support/v7/internal/a/l/l(Z)V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public final C()V
aload 0
getfield android/support/v7/internal/a/l/V Landroid/support/v7/internal/view/i;
ifnull L0
aload 0
getfield android/support/v7/internal/a/l/V Landroid/support/v7/internal/view/i;
invokevirtual android/support/v7/internal/view/i/b()V
aload 0
aconst_null
putfield android/support/v7/internal/a/l/V Landroid/support/v7/internal/view/i;
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public final D()V
return
.limit locals 1
.limit stack 0
.end method

.method public final a()I
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/v()I 0
tableswitch 1
L0
L1
default : L2
L2:
iconst_m1
ireturn
L1:
aload 0
getfield android/support/v7/internal/a/l/G Landroid/support/v7/internal/a/q;
ifnull L2
aload 0
getfield android/support/v7/internal/a/l/G Landroid/support/v7/internal/a/q;
getfield android/support/v7/internal/a/q/c I
ireturn
L0:
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/w()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/support/v7/c/b;)Landroid/support/v7/c/a;
aload 0
getfield android/support/v7/internal/a/l/j Landroid/support/v7/internal/a/p;
ifnull L0
aload 0
getfield android/support/v7/internal/a/l/j Landroid/support/v7/internal/a/p;
invokevirtual android/support/v7/internal/a/p/c()V
L0:
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
iconst_0
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/setHideOnContentScrollEnabled(Z)V
aload 0
getfield android/support/v7/internal/a/l/C Landroid/support/v7/internal/widget/ActionBarContextView;
invokevirtual android/support/v7/internal/widget/ActionBarContextView/i()V
new android/support/v7/internal/a/p
dup
aload 0
aload 0
getfield android/support/v7/internal/a/l/C Landroid/support/v7/internal/widget/ActionBarContextView;
invokevirtual android/support/v7/internal/widget/ActionBarContextView/getContext()Landroid/content/Context;
aload 1
invokespecial android/support/v7/internal/a/p/<init>(Landroid/support/v7/internal/a/l;Landroid/content/Context;Landroid/support/v7/c/b;)V
astore 1
aload 1
invokevirtual android/support/v7/internal/a/p/e()Z
ifeq L1
aload 1
invokevirtual android/support/v7/internal/a/p/d()V
aload 0
getfield android/support/v7/internal/a/l/C Landroid/support/v7/internal/widget/ActionBarContextView;
aload 1
invokevirtual android/support/v7/internal/widget/ActionBarContextView/a(Landroid/support/v7/c/a;)V
aload 0
iconst_1
invokevirtual android/support/v7/internal/a/l/j(Z)V
aload 0
getfield android/support/v7/internal/a/l/C Landroid/support/v7/internal/widget/ActionBarContextView;
bipush 32
invokevirtual android/support/v7/internal/widget/ActionBarContextView/sendAccessibilityEvent(I)V
aload 0
aload 1
putfield android/support/v7/internal/a/l/j Landroid/support/v7/internal/a/p;
aload 1
areturn
L1:
aconst_null
areturn
.limit locals 2
.limit stack 5
.end method

.method public final a(F)V
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
fload 1
invokestatic android/support/v4/view/cx/f(Landroid/view/View;F)V
return
.limit locals 2
.limit stack 2
.end method

.method public final a(I)V
aload 0
aload 0
invokevirtual android/support/v7/internal/a/l/r()Landroid/content/Context;
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
iload 1
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/a()Landroid/view/ViewGroup; 0
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
invokevirtual android/support/v7/internal/a/l/a(Landroid/view/View;)V
return
.limit locals 2
.limit stack 5
.end method

.method public final a(II)V
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/r()I 0
istore 3
iload 2
iconst_4
iand
ifeq L0
aload 0
iconst_1
putfield android/support/v7/internal/a/l/I Z
L0:
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
iload 3
iload 2
iconst_m1
ixor
iand
iload 1
iload 2
iand
ior
invokeinterface android/support/v7/internal/widget/ad/c(I)V 1
return
.limit locals 4
.limit stack 4
.end method

.method public final a(Landroid/content/res/Configuration;)V
aload 0
aload 0
getfield android/support/v7/internal/a/l/i Landroid/content/Context;
invokestatic android/support/v7/internal/view/a/a(Landroid/content/Context;)Landroid/support/v7/internal/view/a;
invokevirtual android/support/v7/internal/view/a/a()Z
invokespecial android/support/v7/internal/a/l/k(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
aload 1
invokeinterface android/support/v7/internal/widget/ad/a(Landroid/graphics/drawable/Drawable;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/support/v7/app/e;)V
aload 0
getfield android/support/v7/internal/a/l/K Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/support/v7/app/g;)V
aload 0
getfield android/support/v7/internal/a/l/F Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/isEmpty()Z
istore 2
aload 0
invokespecial android/support/v7/internal/a/l/E()V
aload 0
getfield android/support/v7/internal/a/l/E Landroid/support/v7/internal/widget/al;
astore 3
aload 3
aload 1
iconst_0
invokevirtual android/support/v7/internal/widget/al/a(Landroid/support/v7/app/g;Z)Landroid/support/v7/internal/widget/ap;
astore 4
aload 3
getfield android/support/v7/internal/widget/al/b Landroid/support/v7/widget/aj;
aload 4
new android/support/v7/widget/al
dup
invokespecial android/support/v7/widget/al/<init>()V
invokevirtual android/support/v7/widget/aj/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
aload 3
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
ifnull L0
aload 3
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
invokevirtual android/widget/Spinner/getAdapter()Landroid/widget/SpinnerAdapter;
checkcast android/support/v7/internal/widget/an
invokevirtual android/support/v7/internal/widget/an/notifyDataSetChanged()V
L0:
iload 2
ifeq L1
aload 4
iconst_1
invokevirtual android/support/v7/internal/widget/ap/setSelected(Z)V
L1:
aload 3
getfield android/support/v7/internal/widget/al/d Z
ifeq L2
aload 3
invokevirtual android/support/v7/internal/widget/al/requestLayout()V
L2:
aload 0
aload 1
aload 0
getfield android/support/v7/internal/a/l/F Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
invokespecial android/support/v7/internal/a/l/b(Landroid/support/v7/app/g;I)V
iload 2
ifeq L3
aload 0
aload 1
invokevirtual android/support/v7/internal/a/l/c(Landroid/support/v7/app/g;)V
L3:
return
.limit locals 5
.limit stack 4
.end method

.method public final a(Landroid/support/v7/app/g;I)V
aload 0
getfield android/support/v7/internal/a/l/F Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/isEmpty()Z
istore 3
aload 0
invokespecial android/support/v7/internal/a/l/E()V
aload 0
getfield android/support/v7/internal/a/l/E Landroid/support/v7/internal/widget/al;
astore 4
aload 4
aload 1
iconst_0
invokevirtual android/support/v7/internal/widget/al/a(Landroid/support/v7/app/g;Z)Landroid/support/v7/internal/widget/ap;
astore 5
aload 4
getfield android/support/v7/internal/widget/al/b Landroid/support/v7/widget/aj;
aload 5
iload 2
new android/support/v7/widget/al
dup
invokespecial android/support/v7/widget/al/<init>()V
invokevirtual android/support/v7/widget/aj/addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
aload 4
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
ifnull L0
aload 4
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
invokevirtual android/widget/Spinner/getAdapter()Landroid/widget/SpinnerAdapter;
checkcast android/support/v7/internal/widget/an
invokevirtual android/support/v7/internal/widget/an/notifyDataSetChanged()V
L0:
iload 3
ifeq L1
aload 5
iconst_1
invokevirtual android/support/v7/internal/widget/ap/setSelected(Z)V
L1:
aload 4
getfield android/support/v7/internal/widget/al/d Z
ifeq L2
aload 4
invokevirtual android/support/v7/internal/widget/al/requestLayout()V
L2:
aload 0
aload 1
iload 2
invokespecial android/support/v7/internal/a/l/b(Landroid/support/v7/app/g;I)V
iload 3
ifeq L3
aload 0
aload 1
invokevirtual android/support/v7/internal/a/l/c(Landroid/support/v7/app/g;)V
L3:
return
.limit locals 6
.limit stack 5
.end method

.method public final a(Landroid/support/v7/app/g;IZ)V
aload 0
invokespecial android/support/v7/internal/a/l/E()V
aload 0
getfield android/support/v7/internal/a/l/E Landroid/support/v7/internal/widget/al;
astore 4
aload 4
aload 1
iconst_0
invokevirtual android/support/v7/internal/widget/al/a(Landroid/support/v7/app/g;Z)Landroid/support/v7/internal/widget/ap;
astore 5
aload 4
getfield android/support/v7/internal/widget/al/b Landroid/support/v7/widget/aj;
aload 5
iload 2
new android/support/v7/widget/al
dup
invokespecial android/support/v7/widget/al/<init>()V
invokevirtual android/support/v7/widget/aj/addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
aload 4
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
ifnull L0
aload 4
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
invokevirtual android/widget/Spinner/getAdapter()Landroid/widget/SpinnerAdapter;
checkcast android/support/v7/internal/widget/an
invokevirtual android/support/v7/internal/widget/an/notifyDataSetChanged()V
L0:
iload 3
ifeq L1
aload 5
iconst_1
invokevirtual android/support/v7/internal/widget/ap/setSelected(Z)V
L1:
aload 4
getfield android/support/v7/internal/widget/al/d Z
ifeq L2
aload 4
invokevirtual android/support/v7/internal/widget/al/requestLayout()V
L2:
aload 0
aload 1
iload 2
invokespecial android/support/v7/internal/a/l/b(Landroid/support/v7/app/g;I)V
iload 3
ifeq L3
aload 0
aload 1
invokevirtual android/support/v7/internal/a/l/c(Landroid/support/v7/app/g;)V
L3:
return
.limit locals 6
.limit stack 5
.end method

.method public final a(Landroid/support/v7/app/g;Z)V
aload 0
invokespecial android/support/v7/internal/a/l/E()V
aload 0
getfield android/support/v7/internal/a/l/E Landroid/support/v7/internal/widget/al;
astore 3
aload 3
aload 1
iconst_0
invokevirtual android/support/v7/internal/widget/al/a(Landroid/support/v7/app/g;Z)Landroid/support/v7/internal/widget/ap;
astore 4
aload 3
getfield android/support/v7/internal/widget/al/b Landroid/support/v7/widget/aj;
aload 4
new android/support/v7/widget/al
dup
invokespecial android/support/v7/widget/al/<init>()V
invokevirtual android/support/v7/widget/aj/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
aload 3
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
ifnull L0
aload 3
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
invokevirtual android/widget/Spinner/getAdapter()Landroid/widget/SpinnerAdapter;
checkcast android/support/v7/internal/widget/an
invokevirtual android/support/v7/internal/widget/an/notifyDataSetChanged()V
L0:
iload 2
ifeq L1
aload 4
iconst_1
invokevirtual android/support/v7/internal/widget/ap/setSelected(Z)V
L1:
aload 3
getfield android/support/v7/internal/widget/al/d Z
ifeq L2
aload 3
invokevirtual android/support/v7/internal/widget/al/requestLayout()V
L2:
aload 0
aload 1
aload 0
getfield android/support/v7/internal/a/l/F Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
invokespecial android/support/v7/internal/a/l/b(Landroid/support/v7/app/g;I)V
iload 2
ifeq L3
aload 0
aload 1
invokevirtual android/support/v7/internal/a/l/c(Landroid/support/v7/app/g;)V
L3:
return
.limit locals 5
.limit stack 4
.end method

.method public final a(Landroid/view/View;)V
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
aload 1
invokeinterface android/support/v7/internal/widget/ad/a(Landroid/view/View;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/view/View;Landroid/support/v7/app/c;)V
aload 1
aload 2
invokevirtual android/view/View/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
aload 1
invokeinterface android/support/v7/internal/widget/ad/a(Landroid/view/View;)V 1
return
.limit locals 3
.limit stack 2
.end method

.method public final a(Landroid/widget/SpinnerAdapter;Landroid/support/v7/app/f;)V
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
aload 1
new android/support/v7/internal/a/b
dup
aload 2
invokespecial android/support/v7/internal/a/b/<init>(Landroid/support/v7/app/f;)V
invokeinterface android/support/v7/internal/widget/ad/a(Landroid/widget/SpinnerAdapter;Landroid/widget/AdapterView$OnItemSelectedListener;)V 2
return
.limit locals 3
.limit stack 5
.end method

.method public final a(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
aload 1
invokeinterface android/support/v7/internal/widget/ad/b(Ljava/lang/CharSequence;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Z)V
iload 1
ifeq L0
iconst_1
istore 2
L1:
aload 0
iload 2
iconst_1
invokevirtual android/support/v7/internal/a/l/a(II)V
return
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 3
.end method

.method public final b()I
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/v()I 0
tableswitch 1
L0
L1
default : L2
L2:
iconst_0
ireturn
L1:
aload 0
getfield android/support/v7/internal/a/l/F Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ireturn
L0:
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/x()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final b(I)V
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
iload 1
invokeinterface android/support/v7/internal/widget/ad/a(I)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public final b(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
aload 1
invokeinterface android/support/v7/internal/widget/ad/b(Landroid/graphics/drawable/Drawable;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public final b(Landroid/support/v7/app/e;)V
aload 0
getfield android/support/v7/internal/a/l/K Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/remove(Ljava/lang/Object;)Z
pop
return
.limit locals 2
.limit stack 2
.end method

.method public final b(Landroid/support/v7/app/g;)V
aload 1
invokevirtual android/support/v7/app/g/a()I
istore 4
aload 0
getfield android/support/v7/internal/a/l/E Landroid/support/v7/internal/widget/al;
ifnull L0
aload 0
getfield android/support/v7/internal/a/l/G Landroid/support/v7/internal/a/q;
ifnull L1
aload 0
getfield android/support/v7/internal/a/l/G Landroid/support/v7/internal/a/q;
getfield android/support/v7/internal/a/q/c I
istore 2
L2:
aload 0
getfield android/support/v7/internal/a/l/E Landroid/support/v7/internal/widget/al;
astore 1
aload 1
getfield android/support/v7/internal/widget/al/b Landroid/support/v7/widget/aj;
iload 4
invokevirtual android/support/v7/widget/aj/removeViewAt(I)V
aload 1
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
ifnull L3
aload 1
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
invokevirtual android/widget/Spinner/getAdapter()Landroid/widget/SpinnerAdapter;
checkcast android/support/v7/internal/widget/an
invokevirtual android/support/v7/internal/widget/an/notifyDataSetChanged()V
L3:
aload 1
getfield android/support/v7/internal/widget/al/d Z
ifeq L4
aload 1
invokevirtual android/support/v7/internal/widget/al/requestLayout()V
L4:
aload 0
getfield android/support/v7/internal/a/l/F Ljava/util/ArrayList;
iload 4
invokevirtual java/util/ArrayList/remove(I)Ljava/lang/Object;
checkcast android/support/v7/internal/a/q
astore 1
aload 1
ifnull L5
aload 1
iconst_m1
putfield android/support/v7/internal/a/q/c I
L5:
aload 0
getfield android/support/v7/internal/a/l/F Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 5
iload 4
istore 3
L6:
iload 3
iload 5
if_icmpge L7
aload 0
getfield android/support/v7/internal/a/l/F Ljava/util/ArrayList;
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/a/q
iload 3
putfield android/support/v7/internal/a/q/c I
iload 3
iconst_1
iadd
istore 3
goto L6
L1:
aload 0
getfield android/support/v7/internal/a/l/H I
istore 2
goto L2
L7:
iload 2
iload 4
if_icmpne L0
aload 0
getfield android/support/v7/internal/a/l/F Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/isEmpty()Z
ifeq L8
aconst_null
astore 1
L9:
aload 0
aload 1
invokevirtual android/support/v7/internal/a/l/c(Landroid/support/v7/app/g;)V
L0:
return
L8:
aload 0
getfield android/support/v7/internal/a/l/F Ljava/util/ArrayList;
iconst_0
iload 4
iconst_1
isub
invokestatic java/lang/Math/max(II)I
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/a/q
astore 1
goto L9
.limit locals 6
.limit stack 4
.end method

.method public final b(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
aload 1
invokeinterface android/support/v7/internal/widget/ad/c(Ljava/lang/CharSequence;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public final b(Z)V
iload 1
ifeq L0
iconst_2
istore 2
L1:
aload 0
iload 2
iconst_2
invokevirtual android/support/v7/internal/a/l/a(II)V
return
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 3
.end method

.method public final c()V
return
.limit locals 1
.limit stack 0
.end method

.method public final c(I)V
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
iload 1
invokeinterface android/support/v7/internal/widget/ad/b(I)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public final c(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
aload 1
invokevirtual android/support/v7/internal/widget/ActionBarContainer/setPrimaryBackground(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final c(Landroid/support/v7/app/g;)V
iconst_m1
istore 2
aload 0
invokevirtual android/support/v7/internal/a/l/g()I
iconst_2
if_icmpeq L0
aload 1
ifnull L1
aload 1
invokevirtual android/support/v7/app/g/a()I
istore 2
L2:
aload 0
iload 2
putfield android/support/v7/internal/a/l/H I
L3:
return
L1:
iconst_m1
istore 2
goto L2
L0:
aload 0
getfield android/support/v7/internal/a/l/x Landroid/app/Activity;
instanceof android/support/v4/app/bb
ifeq L4
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/a()Landroid/view/ViewGroup; 0
invokevirtual android/view/ViewGroup/isInEditMode()Z
ifne L4
aload 0
getfield android/support/v7/internal/a/l/x Landroid/app/Activity;
checkcast android/support/v4/app/bb
invokevirtual android/support/v4/app/bb/c()Landroid/support/v4/app/bi;
invokevirtual android/support/v4/app/bi/a()Landroid/support/v4/app/cd;
invokevirtual android/support/v4/app/cd/h()Landroid/support/v4/app/cd;
astore 3
L5:
aload 0
getfield android/support/v7/internal/a/l/G Landroid/support/v7/internal/a/q;
aload 1
if_acmpne L6
aload 0
getfield android/support/v7/internal/a/l/G Landroid/support/v7/internal/a/q;
ifnull L7
aload 0
getfield android/support/v7/internal/a/l/E Landroid/support/v7/internal/widget/al;
aload 1
invokevirtual android/support/v7/app/g/a()I
invokevirtual android/support/v7/internal/widget/al/a(I)V
L7:
aload 3
ifnull L3
aload 3
invokevirtual android/support/v4/app/cd/l()Z
ifne L3
aload 3
invokevirtual android/support/v4/app/cd/i()I
pop
return
L4:
aconst_null
astore 3
goto L5
L6:
aload 0
getfield android/support/v7/internal/a/l/E Landroid/support/v7/internal/widget/al;
astore 4
aload 1
ifnull L8
aload 1
invokevirtual android/support/v7/app/g/a()I
istore 2
L8:
aload 4
iload 2
invokevirtual android/support/v7/internal/widget/al/setTabSelected(I)V
aload 0
aload 1
checkcast android/support/v7/internal/a/q
putfield android/support/v7/internal/a/l/G Landroid/support/v7/internal/a/q;
goto L7
.limit locals 5
.limit stack 2
.end method

.method public final c(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
aload 1
invokeinterface android/support/v7/internal/widget/ad/d(Ljava/lang/CharSequence;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public final c(Z)V
iload 1
ifeq L0
iconst_4
istore 2
L1:
aload 0
iload 2
iconst_4
invokevirtual android/support/v7/internal/a/l/a(II)V
return
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 3
.end method

.method public final d()Landroid/view/View;
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/y()Landroid/view/View; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public final d(I)V
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/v()I 0
tableswitch 1
L0
L1
default : L2
L2:
new java/lang/IllegalStateException
dup
ldc "setSelectedNavigationIndex not valid for current navigation mode"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
aload 0
getfield android/support/v7/internal/a/l/F Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/app/g
invokevirtual android/support/v7/internal/a/l/c(Landroid/support/v7/app/g;)V
return
L0:
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
iload 1
invokeinterface android/support/v7/internal/widget/ad/e(I)V 1
return
.limit locals 2
.limit stack 3
.end method

.method public final d(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
aload 1
invokevirtual android/support/v7/internal/widget/ActionBarContainer/setStackedBackground(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final d(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
aload 1
invokeinterface android/support/v7/internal/widget/ad/a(Ljava/lang/CharSequence;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public final d(Z)V
iload 1
ifeq L0
bipush 8
istore 2
L1:
aload 0
iload 2
bipush 8
invokevirtual android/support/v7/internal/a/l/a(II)V
return
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 3
.end method

.method public final e()Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/e()Ljava/lang/CharSequence; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public final e(I)V
aload 0
aload 0
getfield android/support/v7/internal/a/l/i Landroid/content/Context;
iload 1
invokevirtual android/content/Context/getString(I)Ljava/lang/String;
invokevirtual android/support/v7/internal/a/l/a(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 3
.end method

.method public final e(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
aload 1
invokeinterface android/support/v7/internal/widget/ad/c(Landroid/graphics/drawable/Drawable;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public final e(Z)V
iload 1
ifeq L0
bipush 16
istore 2
L1:
aload 0
iload 2
bipush 16
invokevirtual android/support/v7/internal/a/l/a(II)V
return
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 3
.end method

.method public final f()Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/f()Ljava/lang/CharSequence; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public final f(I)V
aload 0
aload 0
getfield android/support/v7/internal/a/l/i Landroid/content/Context;
iload 1
invokevirtual android/content/Context/getString(I)Ljava/lang/String;
invokevirtual android/support/v7/internal/a/l/b(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 3
.end method

.method public final f(Z)V
aload 0
getfield android/support/v7/internal/a/l/I Z
ifne L0
iload 1
ifeq L1
iconst_4
istore 2
L2:
aload 0
iload 2
iconst_4
invokevirtual android/support/v7/internal/a/l/a(II)V
L0:
return
L1:
iconst_0
istore 2
goto L2
.limit locals 3
.limit stack 3
.end method

.method public final g()I
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/v()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final g(I)V
iload 1
iconst_4
iand
ifeq L0
aload 0
iconst_1
putfield android/support/v7/internal/a/l/I Z
L0:
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
iload 1
invokeinterface android/support/v7/internal/widget/ad/c(I)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public final g(Z)V
aload 0
iload 1
putfield android/support/v7/internal/a/l/W Z
iload 1
ifne L0
aload 0
getfield android/support/v7/internal/a/l/V Landroid/support/v7/internal/view/i;
ifnull L0
aload 0
getfield android/support/v7/internal/a/l/V Landroid/support/v7/internal/view/i;
invokevirtual android/support/v7/internal/view/i/b()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final h()I
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/r()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final h(I)V
iconst_1
istore 5
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/v()I 0
istore 3
iload 3
tableswitch 2
L0
default : L1
L1:
iload 3
iload 1
if_icmpeq L2
aload 0
getfield android/support/v7/internal/a/l/O Z
ifne L2
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
ifnull L2
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
invokestatic android/support/v4/view/cx/t(Landroid/view/View;)V
L2:
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
iload 1
invokeinterface android/support/v7/internal/widget/ad/d(I)V 1
iload 1
tableswitch 2
L3
default : L4
L4:
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
astore 6
iload 1
iconst_2
if_icmpne L5
aload 0
getfield android/support/v7/internal/a/l/O Z
ifne L5
iconst_1
istore 4
L6:
aload 6
iload 4
invokeinterface android/support/v7/internal/widget/ad/a(Z)V 1
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
astore 6
iload 1
iconst_2
if_icmpne L7
aload 0
getfield android/support/v7/internal/a/l/O Z
ifne L7
iload 5
istore 4
L8:
aload 6
iload 4
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/setHasNonEmbeddedTabs(Z)V
return
L0:
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/v()I 0
tableswitch 1
L9
L10
default : L11
L11:
iconst_m1
istore 2
L12:
aload 0
iload 2
putfield android/support/v7/internal/a/l/H I
aload 0
aconst_null
invokevirtual android/support/v7/internal/a/l/c(Landroid/support/v7/app/g;)V
aload 0
getfield android/support/v7/internal/a/l/E Landroid/support/v7/internal/widget/al;
bipush 8
invokevirtual android/support/v7/internal/widget/al/setVisibility(I)V
goto L1
L10:
aload 0
getfield android/support/v7/internal/a/l/G Landroid/support/v7/internal/a/q;
ifnull L13
aload 0
getfield android/support/v7/internal/a/l/G Landroid/support/v7/internal/a/q;
getfield android/support/v7/internal/a/q/c I
istore 2
goto L12
L13:
iconst_m1
istore 2
goto L12
L9:
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/w()I 0
istore 2
goto L12
L3:
aload 0
invokespecial android/support/v7/internal/a/l/E()V
aload 0
getfield android/support/v7/internal/a/l/E Landroid/support/v7/internal/widget/al;
iconst_0
invokevirtual android/support/v7/internal/widget/al/setVisibility(I)V
aload 0
getfield android/support/v7/internal/a/l/H I
iconst_m1
if_icmpeq L4
aload 0
getfield android/support/v7/internal/a/l/H I
istore 2
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/v()I 0
tableswitch 1
L14
L15
default : L16
L16:
new java/lang/IllegalStateException
dup
ldc "setSelectedNavigationIndex not valid for current navigation mode"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L15:
aload 0
aload 0
getfield android/support/v7/internal/a/l/F Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/app/g
invokevirtual android/support/v7/internal/a/l/c(Landroid/support/v7/app/g;)V
L17:
aload 0
iconst_m1
putfield android/support/v7/internal/a/l/H I
goto L4
L14:
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
iload 2
invokeinterface android/support/v7/internal/widget/ad/e(I)V 1
goto L17
L5:
iconst_0
istore 4
goto L6
L7:
iconst_0
istore 4
goto L8
.limit locals 7
.limit stack 3
.end method

.method public final h(Z)V
iload 1
aload 0
getfield android/support/v7/internal/a/l/J Z
if_icmpne L0
L1:
return
L0:
aload 0
iload 1
putfield android/support/v7/internal/a/l/J Z
aload 0
getfield android/support/v7/internal/a/l/K Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 3
iconst_0
istore 2
L2:
iload 2
iload 3
if_icmpge L1
aload 0
getfield android/support/v7/internal/a/l/K Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
pop
iload 2
iconst_1
iadd
istore 2
goto L2
.limit locals 4
.limit stack 2
.end method

.method public final i()Landroid/support/v7/app/g;
new android/support/v7/internal/a/q
dup
aload 0
invokespecial android/support/v7/internal/a/q/<init>(Landroid/support/v7/internal/a/l;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public final i(I)V
aload 0
getfield android/support/v7/internal/a/l/E Landroid/support/v7/internal/widget/al;
ifnonnull L0
L1:
return
L0:
aload 0
getfield android/support/v7/internal/a/l/G Landroid/support/v7/internal/a/q;
ifnull L2
aload 0
getfield android/support/v7/internal/a/l/G Landroid/support/v7/internal/a/q;
getfield android/support/v7/internal/a/q/c I
istore 2
L3:
aload 0
getfield android/support/v7/internal/a/l/E Landroid/support/v7/internal/widget/al;
astore 5
aload 5
getfield android/support/v7/internal/widget/al/b Landroid/support/v7/widget/aj;
iload 1
invokevirtual android/support/v7/widget/aj/removeViewAt(I)V
aload 5
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
ifnull L4
aload 5
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
invokevirtual android/widget/Spinner/getAdapter()Landroid/widget/SpinnerAdapter;
checkcast android/support/v7/internal/widget/an
invokevirtual android/support/v7/internal/widget/an/notifyDataSetChanged()V
L4:
aload 5
getfield android/support/v7/internal/widget/al/d Z
ifeq L5
aload 5
invokevirtual android/support/v7/internal/widget/al/requestLayout()V
L5:
aload 0
getfield android/support/v7/internal/a/l/F Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/remove(I)Ljava/lang/Object;
checkcast android/support/v7/internal/a/q
astore 5
aload 5
ifnull L6
aload 5
iconst_m1
putfield android/support/v7/internal/a/q/c I
L6:
aload 0
getfield android/support/v7/internal/a/l/F Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 4
iload 1
istore 3
L7:
iload 3
iload 4
if_icmpge L8
aload 0
getfield android/support/v7/internal/a/l/F Ljava/util/ArrayList;
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/a/q
iload 3
putfield android/support/v7/internal/a/q/c I
iload 3
iconst_1
iadd
istore 3
goto L7
L2:
aload 0
getfield android/support/v7/internal/a/l/H I
istore 2
goto L3
L8:
iload 2
iload 1
if_icmpne L1
aload 0
getfield android/support/v7/internal/a/l/F Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/isEmpty()Z
ifeq L9
aconst_null
astore 5
L10:
aload 0
aload 5
invokevirtual android/support/v7/internal/a/l/c(Landroid/support/v7/app/g;)V
return
L9:
aload 0
getfield android/support/v7/internal/a/l/F Ljava/util/ArrayList;
iconst_0
iload 1
iconst_1
isub
invokestatic java/lang/Math/max(II)I
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/internal/a/q
astore 5
goto L10
.limit locals 6
.limit stack 4
.end method

.method public final i(Z)V
aload 0
iload 1
putfield android/support/v7/internal/a/l/Q Z
return
.limit locals 2
.limit stack 2
.end method

.method public final j(I)Landroid/support/v7/app/g;
aload 0
getfield android/support/v7/internal/a/l/F Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/support/v7/app/g
areturn
.limit locals 2
.limit stack 2
.end method

.method public final j()V
aload 0
getfield android/support/v7/internal/a/l/G Landroid/support/v7/internal/a/q;
ifnull L0
aload 0
aconst_null
invokevirtual android/support/v7/internal/a/l/c(Landroid/support/v7/app/g;)V
L0:
aload 0
getfield android/support/v7/internal/a/l/F Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
aload 0
getfield android/support/v7/internal/a/l/E Landroid/support/v7/internal/widget/al;
ifnull L1
aload 0
getfield android/support/v7/internal/a/l/E Landroid/support/v7/internal/widget/al;
astore 1
aload 1
getfield android/support/v7/internal/widget/al/b Landroid/support/v7/widget/aj;
invokevirtual android/support/v7/widget/aj/removeAllViews()V
aload 1
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
ifnull L2
aload 1
getfield android/support/v7/internal/widget/al/c Landroid/widget/Spinner;
invokevirtual android/widget/Spinner/getAdapter()Landroid/widget/SpinnerAdapter;
checkcast android/support/v7/internal/widget/an
invokevirtual android/support/v7/internal/widget/an/notifyDataSetChanged()V
L2:
aload 1
getfield android/support/v7/internal/widget/al/d Z
ifeq L1
aload 1
invokevirtual android/support/v7/internal/widget/al/requestLayout()V
L1:
aload 0
iconst_m1
putfield android/support/v7/internal/a/l/H I
return
.limit locals 2
.limit stack 2
.end method

.method public final j(Z)V
iload 1
ifeq L0
aload 0
getfield android/support/v7/internal/a/l/T Z
ifne L1
aload 0
iconst_1
putfield android/support/v7/internal/a/l/T Z
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
ifnull L2
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
iconst_1
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/setShowingForActionMode(Z)V
L2:
aload 0
iconst_0
invokespecial android/support/v7/internal/a/l/l(Z)V
L1:
iload 1
ifeq L3
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
bipush 8
ldc2_w 100L
invokeinterface android/support/v7/internal/widget/ad/a(IJ)Landroid/support/v4/view/fk; 3
astore 5
aload 0
getfield android/support/v7/internal/a/l/C Landroid/support/v7/internal/widget/ActionBarContextView;
iconst_0
ldc2_w 200L
invokevirtual android/support/v7/internal/widget/ActionBarContextView/a(IJ)Landroid/support/v4/view/fk;
astore 4
L4:
new android/support/v7/internal/view/i
dup
invokespecial android/support/v7/internal/view/i/<init>()V
astore 6
aload 6
getfield android/support/v7/internal/view/i/a Ljava/util/ArrayList;
aload 5
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 5
getfield android/support/v4/view/fk/a Ljava/lang/ref/WeakReference;
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/view/View
astore 5
aload 5
ifnull L5
getstatic android/support/v4/view/fk/c Landroid/support/v4/view/fu;
aload 5
invokeinterface android/support/v4/view/fu/a(Landroid/view/View;)J 1
lstore 2
L6:
aload 4
lload 2
invokevirtual android/support/v4/view/fk/b(J)Landroid/support/v4/view/fk;
pop
aload 6
getfield android/support/v7/internal/view/i/a Ljava/util/ArrayList;
aload 4
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
aload 6
invokevirtual android/support/v7/internal/view/i/a()V
return
L0:
aload 0
getfield android/support/v7/internal/a/l/T Z
ifeq L1
aload 0
iconst_0
putfield android/support/v7/internal/a/l/T Z
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
ifnull L7
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
iconst_0
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/setShowingForActionMode(Z)V
L7:
aload 0
iconst_0
invokespecial android/support/v7/internal/a/l/l(Z)V
goto L1
L3:
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
iconst_0
ldc2_w 200L
invokeinterface android/support/v7/internal/widget/ad/a(IJ)Landroid/support/v4/view/fk; 3
astore 4
aload 0
getfield android/support/v7/internal/a/l/C Landroid/support/v7/internal/widget/ActionBarContextView;
bipush 8
ldc2_w 100L
invokevirtual android/support/v7/internal/widget/ActionBarContextView/a(IJ)Landroid/support/v4/view/fk;
astore 5
goto L4
L5:
lconst_0
lstore 2
goto L6
.limit locals 7
.limit stack 4
.end method

.method public final k()Landroid/support/v7/app/g;
aload 0
getfield android/support/v7/internal/a/l/G Landroid/support/v7/internal/a/q;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final k(I)V
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
iload 1
invokeinterface android/support/v7/internal/widget/ad/g(I)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public final l()I
aload 0
getfield android/support/v7/internal/a/l/F Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final l(I)V
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
iload 1
invokeinterface android/support/v7/internal/widget/ad/h(I)V 1
return
.limit locals 2
.limit stack 2
.end method

.method public final m()I
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getHeight()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final m(I)V
iload 1
ifeq L0
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/a Z
ifne L0
new java/lang/IllegalStateException
dup
ldc "Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to set a non-zero hide offset"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
iload 1
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/setActionBarHideOffset(I)V
return
.limit locals 2
.limit stack 3
.end method

.method public final n()V
aload 0
getfield android/support/v7/internal/a/l/R Z
ifeq L0
aload 0
iconst_0
putfield android/support/v7/internal/a/l/R Z
aload 0
iconst_0
invokespecial android/support/v7/internal/a/l/l(Z)V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public final n(I)V
aload 0
iload 1
putfield android/support/v7/internal/a/l/P I
return
.limit locals 2
.limit stack 2
.end method

.method public final o()V
aload 0
getfield android/support/v7/internal/a/l/R Z
ifne L0
aload 0
iconst_1
putfield android/support/v7/internal/a/l/R Z
aload 0
iconst_0
invokespecial android/support/v7/internal/a/l/l(Z)V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public final p()Z
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
invokevirtual android/support/v7/internal/widget/ActionBarContainer/getHeight()I
istore 1
aload 0
getfield android/support/v7/internal/a/l/U Z
ifeq L0
iload 1
ifeq L1
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/getActionBarHideOffset()I
iload 1
if_icmpge L0
L1:
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final q()V
return
.limit locals 1
.limit stack 0
.end method

.method public final r()Landroid/content/Context;
aload 0
getfield android/support/v7/internal/a/l/w Landroid/content/Context;
ifnonnull L0
new android/util/TypedValue
dup
invokespecial android/util/TypedValue/<init>()V
astore 2
aload 0
getfield android/support/v7/internal/a/l/i Landroid/content/Context;
invokevirtual android/content/Context/getTheme()Landroid/content/res/Resources$Theme;
getstatic android/support/v7/a/d/actionBarWidgetTheme I
aload 2
iconst_1
invokevirtual android/content/res/Resources$Theme/resolveAttribute(ILandroid/util/TypedValue;Z)Z
pop
aload 2
getfield android/util/TypedValue/resourceId I
istore 1
iload 1
ifeq L1
aload 0
new android/view/ContextThemeWrapper
dup
aload 0
getfield android/support/v7/internal/a/l/i Landroid/content/Context;
iload 1
invokespecial android/view/ContextThemeWrapper/<init>(Landroid/content/Context;I)V
putfield android/support/v7/internal/a/l/w Landroid/content/Context;
L0:
aload 0
getfield android/support/v7/internal/a/l/w Landroid/content/Context;
areturn
L1:
aload 0
aload 0
getfield android/support/v7/internal/a/l/i Landroid/content/Context;
putfield android/support/v7/internal/a/l/w Landroid/content/Context;
goto L0
.limit locals 3
.limit stack 5
.end method

.method public final s()Z
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
ifnull L0
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/t()Z 0
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final t()V
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/a Z
ifne L0
new java/lang/IllegalStateException
dup
ldc "Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
iconst_1
putfield android/support/v7/internal/a/l/m Z
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
iconst_1
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/setHideOnContentScrollEnabled(Z)V
return
.limit locals 1
.limit stack 3
.end method

.method public final u()Z
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
getfield android/support/v7/internal/widget/ActionBarOverlayLayout/b Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final v()I
aload 0
getfield android/support/v7/internal/a/l/z Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
invokevirtual android/support/v7/internal/widget/ActionBarOverlayLayout/getActionBarHideOffset()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final w()F
aload 0
getfield android/support/v7/internal/a/l/A Landroid/support/v7/internal/widget/ActionBarContainer;
invokestatic android/support/v4/view/cx/r(Landroid/view/View;)F
freturn
.limit locals 1
.limit stack 1
.end method

.method public final z()Z
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
ifnull L0
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/c()Z 0
ifeq L0
aload 0
getfield android/support/v7/internal/a/l/B Landroid/support/v7/internal/widget/ad;
invokeinterface android/support/v7/internal/widget/ad/d()V 0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method
