.bytecode 50.0
.class public synchronized android/support/v7/internal/widget/TintImageView
.super android/widget/ImageView

.field private static final 'a' [I

.field private final 'b' Landroid/support/v7/internal/widget/av;

.method static <clinit>()V
iconst_2
newarray int
dup
iconst_0
ldc_w 16842964
iastore
dup
iconst_1
ldc_w 16843033
iastore
putstatic android/support/v7/internal/widget/TintImageView/a [I
return
.limit locals 0
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
aconst_null
invokespecial android/support/v7/internal/widget/TintImageView/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
iconst_0
invokespecial android/support/v7/internal/widget/TintImageView/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return
.limit locals 3
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
aload 1
aload 2
iload 3
invokespecial android/widget/ImageView/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
invokevirtual android/support/v7/internal/widget/TintImageView/getContext()Landroid/content/Context;
aload 2
getstatic android/support/v7/internal/widget/TintImageView/a [I
iload 3
invokestatic android/support/v7/internal/widget/ax/a(Landroid/content/Context;Landroid/util/AttributeSet;[II)Landroid/support/v7/internal/widget/ax;
astore 1
aload 1
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
invokevirtual android/content/res/TypedArray/length()I
ifle L0
aload 1
iconst_0
invokevirtual android/support/v7/internal/widget/ax/e(I)Z
ifeq L1
aload 0
aload 1
iconst_0
invokevirtual android/support/v7/internal/widget/ax/a(I)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v7/internal/widget/TintImageView/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
L1:
aload 1
iconst_1
invokevirtual android/support/v7/internal/widget/ax/e(I)Z
ifeq L0
aload 0
aload 1
iconst_1
invokevirtual android/support/v7/internal/widget/ax/a(I)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v7/internal/widget/TintImageView/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
L0:
aload 1
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
invokevirtual android/content/res/TypedArray/recycle()V
aload 0
aload 1
invokevirtual android/support/v7/internal/widget/ax/a()Landroid/support/v7/internal/widget/av;
putfield android/support/v7/internal/widget/TintImageView/b Landroid/support/v7/internal/widget/av;
return
.limit locals 4
.limit stack 4
.end method

.method public setImageResource(I)V
.annotation invisibleparam 1 Landroid/support/a/m;
.end annotation
aload 0
aload 0
getfield android/support/v7/internal/widget/TintImageView/b Landroid/support/v7/internal/widget/av;
iload 1
iconst_0
invokevirtual android/support/v7/internal/widget/av/a(IZ)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v7/internal/widget/TintImageView/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 2
.limit stack 4
.end method
