.bytecode 50.0
.class public synchronized android/support/v7/internal/view/menu/ListMenuItemView
.super android/widget/LinearLayout
.implements android/support/v7/internal/view/menu/aa

.field private static final 'a' Ljava/lang/String; = "ListMenuItemView"

.field private 'b' Landroid/support/v7/internal/view/menu/m;

.field private 'c' Landroid/widget/ImageView;

.field private 'd' Landroid/widget/RadioButton;

.field private 'e' Landroid/widget/TextView;

.field private 'f' Landroid/widget/CheckBox;

.field private 'g' Landroid/widget/TextView;

.field private 'h' Landroid/graphics/drawable/Drawable;

.field private 'i' I

.field private 'j' Landroid/content/Context;

.field private 'k' Z

.field private 'l' I

.field private 'm' Landroid/content/Context;

.field private 'n' Landroid/view/LayoutInflater;

.field private 'o' Z

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
iconst_0
invokespecial android/support/v7/internal/view/menu/ListMenuItemView/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return
.limit locals 3
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
aload 1
aload 2
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
putfield android/support/v7/internal/view/menu/ListMenuItemView/m Landroid/content/Context;
aload 1
aload 2
getstatic android/support/v7/a/n/MenuView [I
iload 3
iconst_0
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
astore 2
aload 0
aload 2
getstatic android/support/v7/a/n/MenuView_android_itemBackground I
invokevirtual android/content/res/TypedArray/getDrawable(I)Landroid/graphics/drawable/Drawable;
putfield android/support/v7/internal/view/menu/ListMenuItemView/h Landroid/graphics/drawable/Drawable;
aload 0
aload 2
getstatic android/support/v7/a/n/MenuView_android_itemTextAppearance I
iconst_m1
invokevirtual android/content/res/TypedArray/getResourceId(II)I
putfield android/support/v7/internal/view/menu/ListMenuItemView/i I
aload 0
aload 2
getstatic android/support/v7/a/n/MenuView_preserveIconSpacing I
iconst_0
invokevirtual android/content/res/TypedArray/getBoolean(IZ)Z
putfield android/support/v7/internal/view/menu/ListMenuItemView/k Z
aload 0
aload 1
putfield android/support/v7/internal/view/menu/ListMenuItemView/j Landroid/content/Context;
aload 2
invokevirtual android/content/res/TypedArray/recycle()V
return
.limit locals 4
.limit stack 5
.end method

.method private c()V
aload 0
aload 0
invokespecial android/support/v7/internal/view/menu/ListMenuItemView/getInflater()Landroid/view/LayoutInflater;
getstatic android/support/v7/a/k/abc_list_menu_item_icon I
aload 0
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
checkcast android/widget/ImageView
putfield android/support/v7/internal/view/menu/ListMenuItemView/c Landroid/widget/ImageView;
aload 0
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/c Landroid/widget/ImageView;
iconst_0
invokevirtual android/support/v7/internal/view/menu/ListMenuItemView/addView(Landroid/view/View;I)V
return
.limit locals 1
.limit stack 5
.end method

.method private d()V
aload 0
aload 0
invokespecial android/support/v7/internal/view/menu/ListMenuItemView/getInflater()Landroid/view/LayoutInflater;
getstatic android/support/v7/a/k/abc_list_menu_item_radio I
aload 0
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
checkcast android/widget/RadioButton
putfield android/support/v7/internal/view/menu/ListMenuItemView/d Landroid/widget/RadioButton;
aload 0
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/d Landroid/widget/RadioButton;
invokevirtual android/support/v7/internal/view/menu/ListMenuItemView/addView(Landroid/view/View;)V
return
.limit locals 1
.limit stack 5
.end method

.method private e()V
aload 0
aload 0
invokespecial android/support/v7/internal/view/menu/ListMenuItemView/getInflater()Landroid/view/LayoutInflater;
getstatic android/support/v7/a/k/abc_list_menu_item_checkbox I
aload 0
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
checkcast android/widget/CheckBox
putfield android/support/v7/internal/view/menu/ListMenuItemView/f Landroid/widget/CheckBox;
aload 0
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/f Landroid/widget/CheckBox;
invokevirtual android/support/v7/internal/view/menu/ListMenuItemView/addView(Landroid/view/View;)V
return
.limit locals 1
.limit stack 5
.end method

.method private getInflater()Landroid/view/LayoutInflater;
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/n Landroid/view/LayoutInflater;
ifnonnull L0
aload 0
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/m Landroid/content/Context;
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
putfield android/support/v7/internal/view/menu/ListMenuItemView/n Landroid/view/LayoutInflater;
L0:
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/n Landroid/view/LayoutInflater;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final a(Landroid/support/v7/internal/view/menu/m;)V
iconst_0
istore 4
aload 0
aload 1
putfield android/support/v7/internal/view/menu/ListMenuItemView/b Landroid/support/v7/internal/view/menu/m;
aload 0
iconst_0
putfield android/support/v7/internal/view/menu/ListMenuItemView/l I
aload 1
invokevirtual android/support/v7/internal/view/menu/m/isVisible()Z
ifeq L0
iconst_0
istore 3
L1:
aload 0
iload 3
invokevirtual android/support/v7/internal/view/menu/ListMenuItemView/setVisibility(I)V
aload 0
aload 1
aload 0
invokevirtual android/support/v7/internal/view/menu/m/a(Landroid/support/v7/internal/view/menu/aa;)Ljava/lang/CharSequence;
invokevirtual android/support/v7/internal/view/menu/ListMenuItemView/setTitle(Ljava/lang/CharSequence;)V
aload 0
aload 1
invokevirtual android/support/v7/internal/view/menu/m/isCheckable()Z
invokevirtual android/support/v7/internal/view/menu/ListMenuItemView/setCheckable(Z)V
aload 1
invokevirtual android/support/v7/internal/view/menu/m/d()Z
istore 5
aload 1
invokevirtual android/support/v7/internal/view/menu/m/c()C
pop
iload 5
ifeq L2
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/b Landroid/support/v7/internal/view/menu/m;
invokevirtual android/support/v7/internal/view/menu/m/d()Z
ifeq L2
iload 4
istore 3
L3:
iload 3
ifne L4
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/g Landroid/widget/TextView;
astore 7
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/b Landroid/support/v7/internal/view/menu/m;
invokevirtual android/support/v7/internal/view/menu/m/c()C
istore 2
iload 2
ifne L5
ldc ""
astore 6
L6:
aload 7
aload 6
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L4:
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/g Landroid/widget/TextView;
invokevirtual android/widget/TextView/getVisibility()I
iload 3
if_icmpeq L7
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/g Landroid/widget/TextView;
iload 3
invokevirtual android/widget/TextView/setVisibility(I)V
L7:
aload 0
aload 1
invokevirtual android/support/v7/internal/view/menu/m/getIcon()Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v7/internal/view/menu/ListMenuItemView/setIcon(Landroid/graphics/drawable/Drawable;)V
aload 0
aload 1
invokevirtual android/support/v7/internal/view/menu/m/isEnabled()Z
invokevirtual android/support/v7/internal/view/menu/ListMenuItemView/setEnabled(Z)V
return
L0:
bipush 8
istore 3
goto L1
L2:
bipush 8
istore 3
goto L3
L5:
new java/lang/StringBuilder
dup
getstatic android/support/v7/internal/view/menu/m/l Ljava/lang/String;
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
astore 6
iload 2
lookupswitch
8 : L8
10 : L9
32 : L10
default : L11
L11:
aload 6
iload 2
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
L12:
aload 6
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 6
goto L6
L9:
aload 6
getstatic android/support/v7/internal/view/menu/m/m Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L12
L8:
aload 6
getstatic android/support/v7/internal/view/menu/m/n Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L12
L10:
aload 6
getstatic android/support/v7/internal/view/menu/m/o Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L12
.limit locals 8
.limit stack 3
.end method

.method public final a()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final b()Z
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/o Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getItemData()Landroid/support/v7/internal/view/menu/m;
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/b Landroid/support/v7/internal/view/menu/m;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected onFinishInflate()V
aload 0
invokespecial android/widget/LinearLayout/onFinishInflate()V
aload 0
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/h Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v7/internal/view/menu/ListMenuItemView/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
aload 0
getstatic android/support/v7/a/i/title I
invokevirtual android/support/v7/internal/view/menu/ListMenuItemView/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield android/support/v7/internal/view/menu/ListMenuItemView/e Landroid/widget/TextView;
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/i I
iconst_m1
if_icmpeq L0
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/e Landroid/widget/TextView;
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/j Landroid/content/Context;
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/i I
invokevirtual android/widget/TextView/setTextAppearance(Landroid/content/Context;I)V
L0:
aload 0
aload 0
getstatic android/support/v7/a/i/shortcut I
invokevirtual android/support/v7/internal/view/menu/ListMenuItemView/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield android/support/v7/internal/view/menu/ListMenuItemView/g Landroid/widget/TextView;
return
.limit locals 1
.limit stack 3
.end method

.method protected onMeasure(II)V
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/c Landroid/widget/ImageView;
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/k Z
ifeq L0
aload 0
invokevirtual android/support/v7/internal/view/menu/ListMenuItemView/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
astore 3
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/c Landroid/widget/ImageView;
invokevirtual android/widget/ImageView/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/widget/LinearLayout$LayoutParams
astore 4
aload 3
getfield android/view/ViewGroup$LayoutParams/height I
ifle L0
aload 4
getfield android/widget/LinearLayout$LayoutParams/width I
ifgt L0
aload 4
aload 3
getfield android/view/ViewGroup$LayoutParams/height I
putfield android/widget/LinearLayout$LayoutParams/width I
L0:
aload 0
iload 1
iload 2
invokespecial android/widget/LinearLayout/onMeasure(II)V
return
.limit locals 5
.limit stack 3
.end method

.method public setCheckable(Z)V
iload 1
ifne L0
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/d Landroid/widget/RadioButton;
ifnonnull L0
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/f Landroid/widget/CheckBox;
ifnonnull L0
L1:
return
L0:
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/b Landroid/support/v7/internal/view/menu/m;
invokevirtual android/support/v7/internal/view/menu/m/e()Z
ifeq L2
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/d Landroid/widget/RadioButton;
ifnonnull L3
aload 0
invokespecial android/support/v7/internal/view/menu/ListMenuItemView/d()V
L3:
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/d Landroid/widget/RadioButton;
astore 3
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/f Landroid/widget/CheckBox;
astore 4
L4:
iload 1
ifeq L5
aload 3
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/b Landroid/support/v7/internal/view/menu/m;
invokevirtual android/support/v7/internal/view/menu/m/isChecked()Z
invokevirtual android/widget/CompoundButton/setChecked(Z)V
iload 1
ifeq L6
iconst_0
istore 2
L7:
aload 3
invokevirtual android/widget/CompoundButton/getVisibility()I
iload 2
if_icmpeq L8
aload 3
iload 2
invokevirtual android/widget/CompoundButton/setVisibility(I)V
L8:
aload 4
ifnull L1
aload 4
invokevirtual android/widget/CompoundButton/getVisibility()I
bipush 8
if_icmpeq L1
aload 4
bipush 8
invokevirtual android/widget/CompoundButton/setVisibility(I)V
return
L2:
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/f Landroid/widget/CheckBox;
ifnonnull L9
aload 0
invokespecial android/support/v7/internal/view/menu/ListMenuItemView/e()V
L9:
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/f Landroid/widget/CheckBox;
astore 3
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/d Landroid/widget/RadioButton;
astore 4
goto L4
L6:
bipush 8
istore 2
goto L7
L5:
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/f Landroid/widget/CheckBox;
ifnull L10
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/f Landroid/widget/CheckBox;
bipush 8
invokevirtual android/widget/CheckBox/setVisibility(I)V
L10:
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/d Landroid/widget/RadioButton;
ifnull L1
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/d Landroid/widget/RadioButton;
bipush 8
invokevirtual android/widget/RadioButton/setVisibility(I)V
return
.limit locals 5
.limit stack 2
.end method

.method public setChecked(Z)V
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/b Landroid/support/v7/internal/view/menu/m;
invokevirtual android/support/v7/internal/view/menu/m/e()Z
ifeq L0
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/d Landroid/widget/RadioButton;
ifnonnull L1
aload 0
invokespecial android/support/v7/internal/view/menu/ListMenuItemView/d()V
L1:
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/d Landroid/widget/RadioButton;
astore 2
L2:
aload 2
iload 1
invokevirtual android/widget/CompoundButton/setChecked(Z)V
return
L0:
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/f Landroid/widget/CheckBox;
ifnonnull L3
aload 0
invokespecial android/support/v7/internal/view/menu/ListMenuItemView/e()V
L3:
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/f Landroid/widget/CheckBox;
astore 2
goto L2
.limit locals 3
.limit stack 2
.end method

.method public setForceShowIcon(Z)V
aload 0
iload 1
putfield android/support/v7/internal/view/menu/ListMenuItemView/o Z
aload 0
iload 1
putfield android/support/v7/internal/view/menu/ListMenuItemView/k Z
return
.limit locals 2
.limit stack 2
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/b Landroid/support/v7/internal/view/menu/m;
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
getfield android/support/v7/internal/view/menu/i/m Z
ifne L0
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/o Z
ifeq L1
L0:
iconst_1
istore 2
L2:
iload 2
ifne L3
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/k Z
ifne L3
L4:
return
L1:
iconst_0
istore 2
goto L2
L3:
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/c Landroid/widget/ImageView;
ifnonnull L5
aload 1
ifnonnull L5
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/k Z
ifeq L4
L5:
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/c Landroid/widget/ImageView;
ifnonnull L6
aload 0
aload 0
invokespecial android/support/v7/internal/view/menu/ListMenuItemView/getInflater()Landroid/view/LayoutInflater;
getstatic android/support/v7/a/k/abc_list_menu_item_icon I
aload 0
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
checkcast android/widget/ImageView
putfield android/support/v7/internal/view/menu/ListMenuItemView/c Landroid/widget/ImageView;
aload 0
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/c Landroid/widget/ImageView;
iconst_0
invokevirtual android/support/v7/internal/view/menu/ListMenuItemView/addView(Landroid/view/View;I)V
L6:
aload 1
ifnonnull L7
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/k Z
ifeq L8
L7:
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/c Landroid/widget/ImageView;
astore 3
iload 2
ifeq L9
L10:
aload 3
aload 1
invokevirtual android/widget/ImageView/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/c Landroid/widget/ImageView;
invokevirtual android/widget/ImageView/getVisibility()I
ifeq L4
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/c Landroid/widget/ImageView;
iconst_0
invokevirtual android/widget/ImageView/setVisibility(I)V
return
L9:
aconst_null
astore 1
goto L10
L8:
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/c Landroid/widget/ImageView;
bipush 8
invokevirtual android/widget/ImageView/setVisibility(I)V
return
.limit locals 4
.limit stack 5
.end method

.method public final setShortcut$25d965e(Z)V
iload 1
ifeq L0
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/b Landroid/support/v7/internal/view/menu/m;
invokevirtual android/support/v7/internal/view/menu/m/d()Z
ifeq L0
iconst_0
istore 3
L1:
iload 3
ifne L2
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/g Landroid/widget/TextView;
astore 5
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/b Landroid/support/v7/internal/view/menu/m;
invokevirtual android/support/v7/internal/view/menu/m/c()C
istore 2
iload 2
ifne L3
ldc ""
astore 4
L4:
aload 5
aload 4
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L2:
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/g Landroid/widget/TextView;
invokevirtual android/widget/TextView/getVisibility()I
iload 3
if_icmpeq L5
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/g Landroid/widget/TextView;
iload 3
invokevirtual android/widget/TextView/setVisibility(I)V
L5:
return
L0:
bipush 8
istore 3
goto L1
L3:
new java/lang/StringBuilder
dup
getstatic android/support/v7/internal/view/menu/m/l Ljava/lang/String;
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
astore 4
iload 2
lookupswitch
8 : L6
10 : L7
32 : L8
default : L9
L9:
aload 4
iload 2
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
L10:
aload 4
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 4
goto L4
L7:
aload 4
getstatic android/support/v7/internal/view/menu/m/m Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L10
L6:
aload 4
getstatic android/support/v7/internal/view/menu/m/n Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L10
L8:
aload 4
getstatic android/support/v7/internal/view/menu/m/o Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L10
.limit locals 6
.limit stack 3
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
aload 1
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/e Landroid/widget/TextView;
aload 1
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/e Landroid/widget/TextView;
invokevirtual android/widget/TextView/getVisibility()I
ifeq L1
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/e Landroid/widget/TextView;
iconst_0
invokevirtual android/widget/TextView/setVisibility(I)V
L1:
return
L0:
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/e Landroid/widget/TextView;
invokevirtual android/widget/TextView/getVisibility()I
bipush 8
if_icmpeq L1
aload 0
getfield android/support/v7/internal/view/menu/ListMenuItemView/e Landroid/widget/TextView;
bipush 8
invokevirtual android/widget/TextView/setVisibility(I)V
return
.limit locals 2
.limit stack 2
.end method
