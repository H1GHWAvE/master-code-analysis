.bytecode 50.0
.class public final synchronized android/support/v7/internal/widget/ar
.super java/lang/Object

.field static final 'a' [I

.field static final 'b' [I

.field static final 'c' [I

.field static final 'd' [I

.field static final 'e' [I

.field static final 'f' [I

.field static final 'g' [I

.field static final 'h' [I

.field private static final 'i' Ljava/lang/ThreadLocal;

.field private static final 'j' [I

.method static <clinit>()V
new java/lang/ThreadLocal
dup
invokespecial java/lang/ThreadLocal/<init>()V
putstatic android/support/v7/internal/widget/ar/i Ljava/lang/ThreadLocal;
iconst_1
newarray int
dup
iconst_0
ldc_w -16842910
iastore
putstatic android/support/v7/internal/widget/ar/a [I
iconst_1
newarray int
dup
iconst_0
ldc_w 16842908
iastore
putstatic android/support/v7/internal/widget/ar/b [I
iconst_1
newarray int
dup
iconst_0
ldc_w 16843518
iastore
putstatic android/support/v7/internal/widget/ar/c [I
iconst_1
newarray int
dup
iconst_0
ldc_w 16842919
iastore
putstatic android/support/v7/internal/widget/ar/d [I
iconst_1
newarray int
dup
iconst_0
ldc_w 16842912
iastore
putstatic android/support/v7/internal/widget/ar/e [I
iconst_1
newarray int
dup
iconst_0
ldc_w 16842913
iastore
putstatic android/support/v7/internal/widget/ar/f [I
iconst_2
newarray int
dup
iconst_0
ldc_w -16842919
iastore
dup
iconst_1
ldc_w -16842908
iastore
putstatic android/support/v7/internal/widget/ar/g [I
iconst_0
newarray int
putstatic android/support/v7/internal/widget/ar/h [I
iconst_1
newarray int
putstatic android/support/v7/internal/widget/ar/j [I
return
.limit locals 0
.limit stack 4
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Landroid/content/Context;I)I
.catch all from L0 to L1 using L2
getstatic android/support/v7/internal/widget/ar/j [I
iconst_0
iload 1
iastore
aload 0
aconst_null
getstatic android/support/v7/internal/widget/ar/j [I
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
astore 0
L0:
aload 0
iconst_0
iconst_0
invokevirtual android/content/res/TypedArray/getColor(II)I
istore 1
L1:
aload 0
invokevirtual android/content/res/TypedArray/recycle()V
iload 1
ireturn
L2:
astore 2
aload 0
invokevirtual android/content/res/TypedArray/recycle()V
aload 2
athrow
.limit locals 3
.limit stack 3
.end method

.method static a(Landroid/content/Context;IF)I
aload 0
iload 1
invokestatic android/support/v7/internal/widget/ar/a(Landroid/content/Context;I)I
istore 1
iload 1
iload 1
invokestatic android/graphics/Color/alpha(I)I
i2f
fload 2
fmul
invokestatic java/lang/Math/round(F)I
invokestatic android/support/v4/e/j/b(II)I
ireturn
.limit locals 3
.limit stack 3
.end method

.method public static a(II)Landroid/content/res/ColorStateList;
new android/content/res/ColorStateList
dup
iconst_2
anewarray [I
dup
iconst_0
getstatic android/support/v7/internal/widget/ar/a [I
aastore
dup
iconst_1
getstatic android/support/v7/internal/widget/ar/h [I
aastore
iconst_2
newarray int
dup
iconst_0
iload 1
iastore
dup
iconst_1
iload 0
iastore
invokespecial android/content/res/ColorStateList/<init>([[I[I)V
areturn
.limit locals 2
.limit stack 7
.end method

.method private static a()Landroid/util/TypedValue;
getstatic android/support/v7/internal/widget/ar/i Ljava/lang/ThreadLocal;
invokevirtual java/lang/ThreadLocal/get()Ljava/lang/Object;
checkcast android/util/TypedValue
astore 1
aload 1
astore 0
aload 1
ifnonnull L0
new android/util/TypedValue
dup
invokespecial android/util/TypedValue/<init>()V
astore 0
getstatic android/support/v7/internal/widget/ar/i Ljava/lang/ThreadLocal;
aload 0
invokevirtual java/lang/ThreadLocal/set(Ljava/lang/Object;)V
L0:
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public static b(Landroid/content/Context;I)Landroid/content/res/ColorStateList;
.catch all from L0 to L1 using L2
getstatic android/support/v7/internal/widget/ar/j [I
iconst_0
iload 1
iastore
aload 0
aconst_null
getstatic android/support/v7/internal/widget/ar/j [I
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
astore 0
L0:
aload 0
iconst_0
invokevirtual android/content/res/TypedArray/getColorStateList(I)Landroid/content/res/ColorStateList;
astore 2
L1:
aload 0
invokevirtual android/content/res/TypedArray/recycle()V
aload 2
areturn
L2:
astore 2
aload 0
invokevirtual android/content/res/TypedArray/recycle()V
aload 2
athrow
.limit locals 3
.limit stack 3
.end method

.method public static c(Landroid/content/Context;I)I
aload 0
iload 1
invokestatic android/support/v7/internal/widget/ar/b(Landroid/content/Context;I)Landroid/content/res/ColorStateList;
astore 2
aload 2
ifnull L0
aload 2
invokevirtual android/content/res/ColorStateList/isStateful()Z
ifeq L0
aload 2
getstatic android/support/v7/internal/widget/ar/a [I
aload 2
invokevirtual android/content/res/ColorStateList/getDefaultColor()I
invokevirtual android/content/res/ColorStateList/getColorForState([II)I
ireturn
L0:
getstatic android/support/v7/internal/widget/ar/i Ljava/lang/ThreadLocal;
invokevirtual java/lang/ThreadLocal/get()Ljava/lang/Object;
checkcast android/util/TypedValue
astore 3
aload 3
astore 2
aload 3
ifnonnull L1
new android/util/TypedValue
dup
invokespecial android/util/TypedValue/<init>()V
astore 2
getstatic android/support/v7/internal/widget/ar/i Ljava/lang/ThreadLocal;
aload 2
invokevirtual java/lang/ThreadLocal/set(Ljava/lang/Object;)V
L1:
aload 0
invokevirtual android/content/Context/getTheme()Landroid/content/res/Resources$Theme;
ldc_w 16842803
aload 2
iconst_1
invokevirtual android/content/res/Resources$Theme/resolveAttribute(ILandroid/util/TypedValue;Z)Z
pop
aload 0
iload 1
aload 2
invokevirtual android/util/TypedValue/getFloat()F
invokestatic android/support/v7/internal/widget/ar/a(Landroid/content/Context;IF)I
ireturn
.limit locals 4
.limit stack 4
.end method
