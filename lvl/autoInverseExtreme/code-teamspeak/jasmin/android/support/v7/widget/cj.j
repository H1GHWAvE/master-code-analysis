.bytecode 50.0
.class public final synchronized android/support/v7/widget/cj
.super java/lang/Object
.implements android/support/v7/internal/view/menu/x

.field 'a' Landroid/support/v7/internal/view/menu/i;

.field public 'b' Landroid/support/v7/internal/view/menu/m;

.field final synthetic 'c' Landroid/support/v7/widget/Toolbar;

.method private <init>(Landroid/support/v7/widget/Toolbar;)V
aload 0
aload 1
putfield android/support/v7/widget/cj/c Landroid/support/v7/widget/Toolbar;
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 2
.limit stack 2
.end method

.method public synthetic <init>(Landroid/support/v7/widget/Toolbar;B)V
aload 0
aload 1
invokespecial android/support/v7/widget/cj/<init>(Landroid/support/v7/widget/Toolbar;)V
return
.limit locals 3
.limit stack 2
.end method

.method public final a(Landroid/view/ViewGroup;)Landroid/support/v7/internal/view/menu/z;
aconst_null
areturn
.limit locals 2
.limit stack 1
.end method

.method public final a(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;)V
aload 0
getfield android/support/v7/widget/cj/a Landroid/support/v7/internal/view/menu/i;
ifnull L0
aload 0
getfield android/support/v7/widget/cj/b Landroid/support/v7/internal/view/menu/m;
ifnull L0
aload 0
getfield android/support/v7/widget/cj/a Landroid/support/v7/internal/view/menu/i;
aload 0
getfield android/support/v7/widget/cj/b Landroid/support/v7/internal/view/menu/m;
invokevirtual android/support/v7/internal/view/menu/i/b(Landroid/support/v7/internal/view/menu/m;)Z
pop
L0:
aload 0
aload 2
putfield android/support/v7/widget/cj/a Landroid/support/v7/internal/view/menu/i;
return
.limit locals 3
.limit stack 2
.end method

.method public final a(Landroid/os/Parcelable;)V
return
.limit locals 2
.limit stack 0
.end method

.method public final a(Landroid/support/v7/internal/view/menu/i;Z)V
return
.limit locals 3
.limit stack 0
.end method

.method public final a(Landroid/support/v7/internal/view/menu/y;)V
return
.limit locals 2
.limit stack 0
.end method

.method public final a(Z)V
iconst_0
istore 4
aload 0
getfield android/support/v7/widget/cj/b Landroid/support/v7/internal/view/menu/m;
ifnull L0
iload 4
istore 3
aload 0
getfield android/support/v7/widget/cj/a Landroid/support/v7/internal/view/menu/i;
ifnull L1
aload 0
getfield android/support/v7/widget/cj/a Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/size()I
istore 5
iconst_0
istore 2
L2:
iload 4
istore 3
iload 2
iload 5
if_icmpge L1
aload 0
getfield android/support/v7/widget/cj/a Landroid/support/v7/internal/view/menu/i;
iload 2
invokevirtual android/support/v7/internal/view/menu/i/getItem(I)Landroid/view/MenuItem;
aload 0
getfield android/support/v7/widget/cj/b Landroid/support/v7/internal/view/menu/m;
if_acmpne L3
iconst_1
istore 3
L1:
iload 3
ifne L0
aload 0
aload 0
getfield android/support/v7/widget/cj/b Landroid/support/v7/internal/view/menu/m;
invokevirtual android/support/v7/widget/cj/b(Landroid/support/v7/internal/view/menu/m;)Z
pop
L0:
return
L3:
iload 2
iconst_1
iadd
istore 2
goto L2
.limit locals 6
.limit stack 2
.end method

.method public final a()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/support/v7/internal/view/menu/ad;)Z
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final a(Landroid/support/v7/internal/view/menu/m;)Z
aload 0
getfield android/support/v7/widget/cj/c Landroid/support/v7/widget/Toolbar;
invokestatic android/support/v7/widget/Toolbar/b(Landroid/support/v7/widget/Toolbar;)V
aload 0
getfield android/support/v7/widget/cj/c Landroid/support/v7/widget/Toolbar;
invokestatic android/support/v7/widget/Toolbar/c(Landroid/support/v7/widget/Toolbar;)Landroid/widget/ImageButton;
invokevirtual android/widget/ImageButton/getParent()Landroid/view/ViewParent;
aload 0
getfield android/support/v7/widget/cj/c Landroid/support/v7/widget/Toolbar;
if_acmpeq L0
aload 0
getfield android/support/v7/widget/cj/c Landroid/support/v7/widget/Toolbar;
aload 0
getfield android/support/v7/widget/cj/c Landroid/support/v7/widget/Toolbar;
invokestatic android/support/v7/widget/Toolbar/c(Landroid/support/v7/widget/Toolbar;)Landroid/widget/ImageButton;
invokevirtual android/support/v7/widget/Toolbar/addView(Landroid/view/View;)V
L0:
aload 0
getfield android/support/v7/widget/cj/c Landroid/support/v7/widget/Toolbar;
aload 1
invokevirtual android/support/v7/internal/view/menu/m/getActionView()Landroid/view/View;
putfield android/support/v7/widget/Toolbar/d Landroid/view/View;
aload 0
aload 1
putfield android/support/v7/widget/cj/b Landroid/support/v7/internal/view/menu/m;
aload 0
getfield android/support/v7/widget/cj/c Landroid/support/v7/widget/Toolbar;
getfield android/support/v7/widget/Toolbar/d Landroid/view/View;
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
aload 0
getfield android/support/v7/widget/cj/c Landroid/support/v7/widget/Toolbar;
if_acmpeq L1
invokestatic android/support/v7/widget/Toolbar/e()Landroid/support/v7/widget/ck;
astore 3
aload 3
ldc_w 8388611
aload 0
getfield android/support/v7/widget/cj/c Landroid/support/v7/widget/Toolbar;
invokestatic android/support/v7/widget/Toolbar/d(Landroid/support/v7/widget/Toolbar;)I
bipush 112
iand
ior
putfield android/support/v7/widget/ck/a I
aload 3
iconst_2
putfield android/support/v7/widget/ck/e I
aload 0
getfield android/support/v7/widget/cj/c Landroid/support/v7/widget/Toolbar;
getfield android/support/v7/widget/Toolbar/d Landroid/view/View;
aload 3
invokevirtual android/view/View/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield android/support/v7/widget/cj/c Landroid/support/v7/widget/Toolbar;
aload 0
getfield android/support/v7/widget/cj/c Landroid/support/v7/widget/Toolbar;
getfield android/support/v7/widget/Toolbar/d Landroid/view/View;
invokevirtual android/support/v7/widget/Toolbar/addView(Landroid/view/View;)V
L1:
aload 0
getfield android/support/v7/widget/cj/c Landroid/support/v7/widget/Toolbar;
astore 3
aload 3
invokevirtual android/support/v7/widget/Toolbar/getChildCount()I
iconst_1
isub
istore 2
L2:
iload 2
iflt L3
aload 3
iload 2
invokevirtual android/support/v7/widget/Toolbar/getChildAt(I)Landroid/view/View;
astore 4
aload 4
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/ck
getfield android/support/v7/widget/ck/e I
iconst_2
if_icmpeq L4
aload 4
aload 3
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
if_acmpeq L4
aload 3
iload 2
invokevirtual android/support/v7/widget/Toolbar/removeViewAt(I)V
aload 3
getfield android/support/v7/widget/Toolbar/j Ljava/util/ArrayList;
aload 4
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L4:
iload 2
iconst_1
isub
istore 2
goto L2
L3:
aload 0
getfield android/support/v7/widget/cj/c Landroid/support/v7/widget/Toolbar;
invokevirtual android/support/v7/widget/Toolbar/requestLayout()V
aload 1
iconst_1
invokevirtual android/support/v7/internal/view/menu/m/e(Z)V
aload 0
getfield android/support/v7/widget/cj/c Landroid/support/v7/widget/Toolbar;
getfield android/support/v7/widget/Toolbar/d Landroid/view/View;
instanceof android/support/v7/c/c
ifeq L5
aload 0
getfield android/support/v7/widget/cj/c Landroid/support/v7/widget/Toolbar;
getfield android/support/v7/widget/Toolbar/d Landroid/view/View;
checkcast android/support/v7/c/c
invokeinterface android/support/v7/c/c/a()V 0
L5:
iconst_1
ireturn
.limit locals 5
.limit stack 4
.end method

.method public final b()I
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final b(Landroid/support/v7/internal/view/menu/m;)Z
aload 0
getfield android/support/v7/widget/cj/c Landroid/support/v7/widget/Toolbar;
getfield android/support/v7/widget/Toolbar/d Landroid/view/View;
instanceof android/support/v7/c/c
ifeq L0
aload 0
getfield android/support/v7/widget/cj/c Landroid/support/v7/widget/Toolbar;
getfield android/support/v7/widget/Toolbar/d Landroid/view/View;
checkcast android/support/v7/c/c
invokeinterface android/support/v7/c/c/b()V 0
L0:
aload 0
getfield android/support/v7/widget/cj/c Landroid/support/v7/widget/Toolbar;
aload 0
getfield android/support/v7/widget/cj/c Landroid/support/v7/widget/Toolbar;
getfield android/support/v7/widget/Toolbar/d Landroid/view/View;
invokevirtual android/support/v7/widget/Toolbar/removeView(Landroid/view/View;)V
aload 0
getfield android/support/v7/widget/cj/c Landroid/support/v7/widget/Toolbar;
aload 0
getfield android/support/v7/widget/cj/c Landroid/support/v7/widget/Toolbar;
invokestatic android/support/v7/widget/Toolbar/c(Landroid/support/v7/widget/Toolbar;)Landroid/widget/ImageButton;
invokevirtual android/support/v7/widget/Toolbar/removeView(Landroid/view/View;)V
aload 0
getfield android/support/v7/widget/cj/c Landroid/support/v7/widget/Toolbar;
aconst_null
putfield android/support/v7/widget/Toolbar/d Landroid/view/View;
aload 0
getfield android/support/v7/widget/cj/c Landroid/support/v7/widget/Toolbar;
astore 3
aload 3
getfield android/support/v7/widget/Toolbar/j Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
istore 2
L1:
iload 2
iflt L2
aload 3
aload 3
getfield android/support/v7/widget/Toolbar/j Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/view/View
invokevirtual android/support/v7/widget/Toolbar/addView(Landroid/view/View;)V
iload 2
iconst_1
isub
istore 2
goto L1
L2:
aload 3
getfield android/support/v7/widget/Toolbar/j Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
aload 0
aconst_null
putfield android/support/v7/widget/cj/b Landroid/support/v7/internal/view/menu/m;
aload 0
getfield android/support/v7/widget/cj/c Landroid/support/v7/widget/Toolbar;
invokevirtual android/support/v7/widget/Toolbar/requestLayout()V
aload 1
iconst_0
invokevirtual android/support/v7/internal/view/menu/m/e(Z)V
iconst_1
ireturn
.limit locals 4
.limit stack 3
.end method

.method public final c()Landroid/os/Parcelable;
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method
