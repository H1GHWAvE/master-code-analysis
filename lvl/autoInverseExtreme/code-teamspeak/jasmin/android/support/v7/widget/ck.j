.bytecode 50.0
.class public final synchronized android/support/v7/widget/ck
.super android/support/v7/app/c

.field static final 'b' I = 0


.field static final 'c' I = 1


.field static final 'd' I = 2


.field 'e' I

.method public <init>()V
aload 0
bipush -2
invokespecial android/support/v7/app/c/<init>(I)V
aload 0
iconst_0
putfield android/support/v7/widget/ck/e I
aload 0
ldc_w 8388627
putfield android/support/v7/widget/ck/a I
return
.limit locals 1
.limit stack 2
.end method

.method private <init>(I)V
aload 0
iconst_m1
iload 1
invokespecial android/support/v7/widget/ck/<init>(II)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(II)V
aload 0
iload 1
invokespecial android/support/v7/app/c/<init>(I)V
aload 0
iconst_0
putfield android/support/v7/widget/ck/e I
aload 0
iload 2
putfield android/support/v7/widget/ck/a I
return
.limit locals 3
.limit stack 2
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
aload 0
aload 1
aload 2
invokespecial android/support/v7/app/c/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
iconst_0
putfield android/support/v7/widget/ck/e I
return
.limit locals 3
.limit stack 3
.end method

.method public <init>(Landroid/support/v7/app/c;)V
aload 0
aload 1
invokespecial android/support/v7/app/c/<init>(Landroid/support/v7/app/c;)V
aload 0
iconst_0
putfield android/support/v7/widget/ck/e I
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(Landroid/support/v7/widget/ck;)V
aload 0
aload 1
invokespecial android/support/v7/app/c/<init>(Landroid/support/v7/app/c;)V
aload 0
iconst_0
putfield android/support/v7/widget/ck/e I
aload 0
aload 1
getfield android/support/v7/widget/ck/e I
putfield android/support/v7/widget/ck/e I
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
aload 1
invokespecial android/support/v7/app/c/<init>(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
iconst_0
putfield android/support/v7/widget/ck/e I
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
aload 0
aload 1
invokespecial android/support/v7/app/c/<init>(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
iconst_0
putfield android/support/v7/widget/ck/e I
aload 0
aload 1
getfield android/view/ViewGroup$MarginLayoutParams/leftMargin I
putfield android/support/v7/widget/ck/leftMargin I
aload 0
aload 1
getfield android/view/ViewGroup$MarginLayoutParams/topMargin I
putfield android/support/v7/widget/ck/topMargin I
aload 0
aload 1
getfield android/view/ViewGroup$MarginLayoutParams/rightMargin I
putfield android/support/v7/widget/ck/rightMargin I
aload 0
aload 1
getfield android/view/ViewGroup$MarginLayoutParams/bottomMargin I
putfield android/support/v7/widget/ck/bottomMargin I
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/view/ViewGroup$MarginLayoutParams;)V
aload 0
aload 1
getfield android/view/ViewGroup$MarginLayoutParams/leftMargin I
putfield android/support/v7/widget/ck/leftMargin I
aload 0
aload 1
getfield android/view/ViewGroup$MarginLayoutParams/topMargin I
putfield android/support/v7/widget/ck/topMargin I
aload 0
aload 1
getfield android/view/ViewGroup$MarginLayoutParams/rightMargin I
putfield android/support/v7/widget/ck/rightMargin I
aload 0
aload 1
getfield android/view/ViewGroup$MarginLayoutParams/bottomMargin I
putfield android/support/v7/widget/ck/bottomMargin I
return
.limit locals 2
.limit stack 2
.end method
