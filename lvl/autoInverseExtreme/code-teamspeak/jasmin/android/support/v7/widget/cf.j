.bytecode 50.0
.class public final synchronized android/support/v7/widget/cf
.super java/lang/Object

.field private final 'a' Landroid/content/Context;

.field private final 'b' Landroid/view/LayoutInflater;

.field private 'c' Landroid/view/LayoutInflater;

.method private <init>(Landroid/content/Context;)V
.annotation invisibleparam 1 Landroid/support/a/y;
.end annotation
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v7/widget/cf/a Landroid/content/Context;
aload 0
aload 1
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
putfield android/support/v7/widget/cf/b Landroid/view/LayoutInflater;
return
.limit locals 2
.limit stack 2
.end method

.method private a()Landroid/content/res/Resources$Theme;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/v7/widget/cf/c Landroid/view/LayoutInflater;
ifnonnull L0
aconst_null
areturn
L0:
aload 0
getfield android/support/v7/widget/cf/c Landroid/view/LayoutInflater;
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getTheme()Landroid/content/res/Resources$Theme;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Landroid/content/res/Resources$Theme;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 1
ifnonnull L0
aload 0
aconst_null
putfield android/support/v7/widget/cf/c Landroid/view/LayoutInflater;
return
L0:
aload 1
aload 0
getfield android/support/v7/widget/cf/a Landroid/content/Context;
invokevirtual android/content/Context/getTheme()Landroid/content/res/Resources$Theme;
if_acmpne L1
aload 0
aload 0
getfield android/support/v7/widget/cf/b Landroid/view/LayoutInflater;
putfield android/support/v7/widget/cf/c Landroid/view/LayoutInflater;
return
L1:
aload 0
new android/support/v7/internal/view/b
dup
aload 0
getfield android/support/v7/widget/cf/a Landroid/content/Context;
aload 1
invokespecial android/support/v7/internal/view/b/<init>(Landroid/content/Context;Landroid/content/res/Resources$Theme;)V
invokestatic android/view/LayoutInflater/from(Landroid/content/Context;)Landroid/view/LayoutInflater;
putfield android/support/v7/widget/cf/c Landroid/view/LayoutInflater;
return
.limit locals 2
.limit stack 5
.end method

.method private b()Landroid/view/LayoutInflater;
.annotation invisible Landroid/support/a/y;
.end annotation
aload 0
getfield android/support/v7/widget/cf/c Landroid/view/LayoutInflater;
ifnull L0
aload 0
getfield android/support/v7/widget/cf/c Landroid/view/LayoutInflater;
areturn
L0:
aload 0
getfield android/support/v7/widget/cf/b Landroid/view/LayoutInflater;
areturn
.limit locals 1
.limit stack 1
.end method
