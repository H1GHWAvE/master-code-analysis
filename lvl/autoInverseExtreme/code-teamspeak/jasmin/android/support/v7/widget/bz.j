.bytecode 50.0
.class final synchronized android/support/v7/widget/bz
.super android/support/v4/widget/bz
.implements android/view/View$OnClickListener

.field static final 'l' I = 0


.field static final 'm' I = 1


.field static final 'n' I = 2


.field static final 'p' I = -1


.field private static final 'q' Z = 0


.field private static final 'r' Ljava/lang/String; = "SuggestionsAdapter"

.field private static final 's' I = 50


.field private 'A' Landroid/content/res/ColorStateList;

.field private 'B' I

.field private 'C' I

.field private 'D' I

.field private 'E' I

.field private 'F' I

.field private 'G' I

.field 'o' I

.field private final 't' Landroid/app/SearchManager;

.field private final 'u' Landroid/support/v7/widget/SearchView;

.field private final 'v' Landroid/app/SearchableInfo;

.field private final 'w' Landroid/content/Context;

.field private final 'x' Ljava/util/WeakHashMap;

.field private final 'y' I

.field private 'z' Z

.method public <init>(Landroid/content/Context;Landroid/support/v7/widget/SearchView;Landroid/app/SearchableInfo;Ljava/util/WeakHashMap;)V
aload 0
aload 1
aload 2
invokevirtual android/support/v7/widget/SearchView/getSuggestionRowLayout()I
invokespecial android/support/v4/widget/bz/<init>(Landroid/content/Context;I)V
aload 0
iconst_0
putfield android/support/v7/widget/bz/z Z
aload 0
iconst_1
putfield android/support/v7/widget/bz/o I
aload 0
iconst_m1
putfield android/support/v7/widget/bz/B I
aload 0
iconst_m1
putfield android/support/v7/widget/bz/C I
aload 0
iconst_m1
putfield android/support/v7/widget/bz/D I
aload 0
iconst_m1
putfield android/support/v7/widget/bz/E I
aload 0
iconst_m1
putfield android/support/v7/widget/bz/F I
aload 0
iconst_m1
putfield android/support/v7/widget/bz/G I
aload 0
aload 0
getfield android/support/v7/widget/bz/d Landroid/content/Context;
ldc "search"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/app/SearchManager
putfield android/support/v7/widget/bz/t Landroid/app/SearchManager;
aload 0
aload 2
putfield android/support/v7/widget/bz/u Landroid/support/v7/widget/SearchView;
aload 0
aload 3
putfield android/support/v7/widget/bz/v Landroid/app/SearchableInfo;
aload 0
aload 2
invokevirtual android/support/v7/widget/SearchView/getSuggestionCommitIconResId()I
putfield android/support/v7/widget/bz/y I
aload 0
aload 1
putfield android/support/v7/widget/bz/w Landroid/content/Context;
aload 0
aload 4
putfield android/support/v7/widget/bz/x Ljava/util/WeakHashMap;
return
.limit locals 5
.limit stack 3
.end method

.method private a(Landroid/app/SearchableInfo;Ljava/lang/String;)Landroid/database/Cursor;
aload 1
ifnonnull L0
L1:
aconst_null
areturn
L0:
aload 1
invokevirtual android/app/SearchableInfo/getSuggestAuthority()Ljava/lang/String;
astore 3
aload 3
ifnull L1
new android/net/Uri$Builder
dup
invokespecial android/net/Uri$Builder/<init>()V
ldc "content"
invokevirtual android/net/Uri$Builder/scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;
aload 3
invokevirtual android/net/Uri$Builder/authority(Ljava/lang/String;)Landroid/net/Uri$Builder;
ldc ""
invokevirtual android/net/Uri$Builder/query(Ljava/lang/String;)Landroid/net/Uri$Builder;
ldc ""
invokevirtual android/net/Uri$Builder/fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;
astore 3
aload 1
invokevirtual android/app/SearchableInfo/getSuggestPath()Ljava/lang/String;
astore 4
aload 4
ifnull L2
aload 3
aload 4
invokevirtual android/net/Uri$Builder/appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;
pop
L2:
aload 3
ldc "search_suggest_query"
invokevirtual android/net/Uri$Builder/appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;
pop
aload 1
invokevirtual android/app/SearchableInfo/getSuggestSelection()Ljava/lang/String;
astore 4
aload 4
ifnull L3
iconst_1
anewarray java/lang/String
astore 1
aload 1
iconst_0
aload 2
aastore
L4:
aload 3
ldc "limit"
ldc "50"
invokevirtual android/net/Uri$Builder/appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;
pop
aload 3
invokevirtual android/net/Uri$Builder/build()Landroid/net/Uri;
astore 2
aload 0
getfield android/support/v7/widget/bz/d Landroid/content/Context;
invokevirtual android/content/Context/getContentResolver()Landroid/content/ContentResolver;
aload 2
aconst_null
aload 4
aload 1
aconst_null
invokevirtual android/content/ContentResolver/query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
areturn
L3:
aload 3
aload 2
invokevirtual android/net/Uri$Builder/appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;
pop
aconst_null
astore 1
goto L4
.limit locals 5
.limit stack 6
.end method

.method private a(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/content/ComponentName/flattenToShortString()Ljava/lang/String;
astore 2
aload 0
getfield android/support/v7/widget/bz/x Ljava/util/WeakHashMap;
aload 2
invokevirtual java/util/WeakHashMap/containsKey(Ljava/lang/Object;)Z
ifeq L0
aload 0
getfield android/support/v7/widget/bz/x Ljava/util/WeakHashMap;
aload 2
invokevirtual java/util/WeakHashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/graphics/drawable/Drawable$ConstantState
astore 1
aload 1
ifnonnull L1
aconst_null
areturn
L1:
aload 1
aload 0
getfield android/support/v7/widget/bz/w Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/graphics/drawable/Drawable$ConstantState/newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
areturn
L0:
aload 0
aload 1
invokespecial android/support/v7/widget/bz/b(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;
astore 3
aload 3
ifnonnull L2
aconst_null
astore 1
L3:
aload 0
getfield android/support/v7/widget/bz/x Ljava/util/WeakHashMap;
aload 2
aload 1
invokevirtual java/util/WeakHashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 3
areturn
L2:
aload 3
invokevirtual android/graphics/drawable/Drawable/getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
astore 1
goto L3
.limit locals 4
.limit stack 3
.end method

.method private a(Landroid/net/Uri;)Landroid/graphics/drawable/Drawable;
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch android/content/res/Resources$NotFoundException from L3 to L4 using L5
.catch java/io/FileNotFoundException from L3 to L4 using L2
.catch java/io/FileNotFoundException from L6 to L2 using L2
.catch java/io/FileNotFoundException from L7 to L8 using L2
.catch java/io/FileNotFoundException from L9 to L10 using L2
.catch all from L10 to L11 using L12
.catch java/io/IOException from L11 to L13 using L14
.catch java/io/FileNotFoundException from L11 to L13 using L2
.catch java/io/FileNotFoundException from L15 to L16 using L2
.catch java/io/IOException from L17 to L18 using L19
.catch java/io/FileNotFoundException from L17 to L18 using L2
.catch java/io/FileNotFoundException from L18 to L19 using L2
.catch java/io/FileNotFoundException from L20 to L21 using L2
L0:
ldc "android.resource"
aload 1
invokevirtual android/net/Uri/getScheme()Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
istore 2
L1:
iload 2
ifeq L7
L3:
aload 0
aload 1
invokespecial android/support/v7/widget/bz/b(Landroid/net/Uri;)Landroid/graphics/drawable/Drawable;
astore 3
L4:
aload 3
areturn
L5:
astore 3
L6:
new java/io/FileNotFoundException
dup
new java/lang/StringBuilder
dup
ldc "Resource does not exist: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileNotFoundException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 3
ldc "SuggestionsAdapter"
new java/lang/StringBuilder
dup
ldc "Icon not found: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/io/FileNotFoundException/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
aconst_null
areturn
L7:
aload 0
getfield android/support/v7/widget/bz/w Landroid/content/Context;
invokevirtual android/content/Context/getContentResolver()Landroid/content/ContentResolver;
aload 1
invokevirtual android/content/ContentResolver/openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
astore 3
L8:
aload 3
ifnonnull L10
L9:
new java/io/FileNotFoundException
dup
new java/lang/StringBuilder
dup
ldc "Failed to open "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileNotFoundException/<init>(Ljava/lang/String;)V
athrow
L10:
aload 3
aconst_null
invokestatic android/graphics/drawable/Drawable/createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
astore 4
L11:
aload 3
invokevirtual java/io/InputStream/close()V
L13:
aload 4
areturn
L14:
astore 3
L15:
ldc "SuggestionsAdapter"
new java/lang/StringBuilder
dup
ldc "Error closing icon stream for "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 3
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
L16:
aload 4
areturn
L12:
astore 4
L17:
aload 3
invokevirtual java/io/InputStream/close()V
L18:
aload 4
athrow
L19:
astore 3
L20:
ldc "SuggestionsAdapter"
new java/lang/StringBuilder
dup
ldc "Error closing icon stream for "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 3
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
L21:
goto L18
.limit locals 5
.limit stack 5
.end method

.method private a(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
.catch java/lang/NumberFormatException from L0 to L1 using L2
.catch android/content/res/Resources$NotFoundException from L0 to L1 using L3
.catch java/lang/NumberFormatException from L4 to L5 using L2
.catch android/content/res/Resources$NotFoundException from L4 to L5 using L3
aload 1
ifnull L6
aload 1
invokevirtual java/lang/String/length()I
ifeq L6
ldc "0"
aload 1
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
L6:
aconst_null
astore 3
L7:
aload 3
areturn
L0:
aload 1
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
istore 2
new java/lang/StringBuilder
dup
ldc "android.resource://"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield android/support/v7/widget/bz/w Landroid/content/Context;
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 5
aload 0
aload 5
invokespecial android/support/v7/widget/bz/b(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
astore 4
L1:
aload 4
astore 3
aload 4
ifnonnull L7
L4:
aload 0
getfield android/support/v7/widget/bz/w Landroid/content/Context;
iload 2
invokestatic android/support/v4/c/h/a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
astore 3
aload 0
aload 5
aload 3
invokespecial android/support/v7/widget/bz/a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
L5:
aload 3
areturn
L2:
astore 3
aload 0
aload 1
invokespecial android/support/v7/widget/bz/b(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
astore 4
aload 4
astore 3
aload 4
ifnonnull L7
aload 0
aload 1
invokestatic android/net/Uri/parse(Ljava/lang/String;)Landroid/net/Uri;
invokespecial android/support/v7/widget/bz/a(Landroid/net/Uri;)Landroid/graphics/drawable/Drawable;
astore 3
aload 0
aload 1
aload 3
invokespecial android/support/v7/widget/bz/a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
aload 3
areturn
L3:
astore 3
ldc "SuggestionsAdapter"
new java/lang/StringBuilder
dup
ldc "Icon resource not found: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
aconst_null
areturn
.limit locals 6
.limit stack 4
.end method

.method private static a(Landroid/database/Cursor;I)Ljava/lang/String;
.catch java/lang/Exception from L0 to L1 using L2
iload 1
iconst_m1
if_icmpne L0
aconst_null
areturn
L0:
aload 0
iload 1
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
astore 0
L1:
aload 0
areturn
L2:
astore 0
ldc "SuggestionsAdapter"
ldc "unexpected error retrieving valid column from cursor, did the remote process die?"
aload 0
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
aconst_null
areturn
.limit locals 2
.limit stack 3
.end method

.method public static a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
aload 0
aload 0
aload 1
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
invokestatic android/support/v7/widget/bz/a(Landroid/database/Cursor;I)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 3
.end method

.method private a(I)V
aload 0
iload 1
putfield android/support/v7/widget/bz/o I
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;I)V
aload 0
aload 1
invokevirtual android/widget/ImageView/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
aload 1
ifnonnull L0
aload 0
iload 2
invokevirtual android/widget/ImageView/setVisibility(I)V
return
L0:
aload 0
iconst_0
invokevirtual android/widget/ImageView/setVisibility(I)V
aload 1
iconst_0
iconst_0
invokevirtual android/graphics/drawable/Drawable/setVisible(ZZ)Z
pop
aload 1
iconst_1
iconst_0
invokevirtual android/graphics/drawable/Drawable/setVisible(ZZ)Z
pop
return
.limit locals 3
.limit stack 3
.end method

.method private static a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
aload 0
aload 1
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 1
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L0
aload 0
bipush 8
invokevirtual android/widget/TextView/setVisibility(I)V
return
L0:
aload 0
iconst_0
invokevirtual android/widget/TextView/setVisibility(I)V
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
aload 2
ifnull L0
aload 0
getfield android/support/v7/widget/bz/x Ljava/util/WeakHashMap;
aload 1
aload 2
invokevirtual android/graphics/drawable/Drawable/getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
invokevirtual java/util/WeakHashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L0:
return
.limit locals 3
.limit stack 3
.end method

.method private b(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
aload 0
getfield android/support/v7/widget/bz/d Landroid/content/Context;
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
astore 3
L0:
aload 3
aload 1
sipush 128
invokevirtual android/content/pm/PackageManager/getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
astore 4
L1:
aload 4
invokevirtual android/content/pm/ActivityInfo/getIconResource()I
istore 2
iload 2
ifne L3
aconst_null
areturn
L2:
astore 1
ldc "SuggestionsAdapter"
aload 1
invokevirtual android/content/pm/PackageManager$NameNotFoundException/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
aconst_null
areturn
L3:
aload 3
aload 1
invokevirtual android/content/ComponentName/getPackageName()Ljava/lang/String;
iload 2
aload 4
getfield android/content/pm/ActivityInfo/applicationInfo Landroid/content/pm/ApplicationInfo;
invokevirtual android/content/pm/PackageManager/getDrawable(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;
astore 3
aload 3
ifnonnull L4
ldc "SuggestionsAdapter"
new java/lang/StringBuilder
dup
ldc "Invalid icon resource "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " for "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual android/content/ComponentName/flattenToShortString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
aconst_null
areturn
L4:
aload 3
areturn
.limit locals 5
.limit stack 4
.end method

.method private b(Landroid/net/Uri;)Landroid/graphics/drawable/Drawable;
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
.catch java/lang/NumberFormatException from L3 to L4 using L5
aload 1
invokevirtual android/net/Uri/getAuthority()Ljava/lang/String;
astore 3
aload 3
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L0
new java/io/FileNotFoundException
dup
new java/lang/StringBuilder
dup
ldc "No authority: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileNotFoundException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
getfield android/support/v7/widget/bz/d Landroid/content/Context;
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
aload 3
invokevirtual android/content/pm/PackageManager/getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;
astore 4
L1:
aload 1
invokevirtual android/net/Uri/getPathSegments()Ljava/util/List;
astore 5
aload 5
ifnonnull L6
new java/io/FileNotFoundException
dup
new java/lang/StringBuilder
dup
ldc "No path: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileNotFoundException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 3
new java/io/FileNotFoundException
dup
new java/lang/StringBuilder
dup
ldc "No package found for authority: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileNotFoundException/<init>(Ljava/lang/String;)V
athrow
L6:
aload 5
invokeinterface java/util/List/size()I 0
istore 2
iload 2
iconst_1
if_icmpne L7
L3:
aload 5
iconst_0
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast java/lang/String
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
istore 2
L4:
iload 2
ifne L8
new java/io/FileNotFoundException
dup
new java/lang/StringBuilder
dup
ldc "No resource found for: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileNotFoundException/<init>(Ljava/lang/String;)V
athrow
L5:
astore 3
new java/io/FileNotFoundException
dup
new java/lang/StringBuilder
dup
ldc "Single path segment is not a resource ID: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileNotFoundException/<init>(Ljava/lang/String;)V
athrow
L7:
iload 2
iconst_2
if_icmpne L9
aload 4
aload 5
iconst_1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast java/lang/String
aload 5
iconst_0
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast java/lang/String
aload 3
invokevirtual android/content/res/Resources/getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
istore 2
goto L4
L9:
new java/io/FileNotFoundException
dup
new java/lang/StringBuilder
dup
ldc "More than two path segments: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/FileNotFoundException/<init>(Ljava/lang/String;)V
athrow
L8:
aload 4
iload 2
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
areturn
.limit locals 6
.limit stack 5
.end method

.method private b(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/widget/bz/x Ljava/util/WeakHashMap;
aload 1
invokevirtual java/util/WeakHashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/graphics/drawable/Drawable$ConstantState
astore 1
aload 1
ifnonnull L0
aconst_null
areturn
L0:
aload 1
invokevirtual android/graphics/drawable/Drawable$ConstantState/newDrawable()Landroid/graphics/drawable/Drawable;
areturn
.limit locals 2
.limit stack 2
.end method

.method private b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/widget/bz/A Landroid/content/res/ColorStateList;
ifnonnull L0
new android/util/TypedValue
dup
invokespecial android/util/TypedValue/<init>()V
astore 2
aload 0
getfield android/support/v7/widget/bz/d Landroid/content/Context;
invokevirtual android/content/Context/getTheme()Landroid/content/res/Resources$Theme;
getstatic android/support/v7/a/d/textColorSearchUrl I
aload 2
iconst_1
invokevirtual android/content/res/Resources$Theme/resolveAttribute(ILandroid/util/TypedValue;Z)Z
pop
aload 0
aload 0
getfield android/support/v7/widget/bz/d Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
aload 2
getfield android/util/TypedValue/resourceId I
invokevirtual android/content/res/Resources/getColorStateList(I)Landroid/content/res/ColorStateList;
putfield android/support/v7/widget/bz/A Landroid/content/res/ColorStateList;
L0:
new android/text/SpannableString
dup
aload 1
invokespecial android/text/SpannableString/<init>(Ljava/lang/CharSequence;)V
astore 2
aload 2
new android/text/style/TextAppearanceSpan
dup
aconst_null
iconst_0
iconst_0
aload 0
getfield android/support/v7/widget/bz/A Landroid/content/res/ColorStateList;
aconst_null
invokespecial android/text/style/TextAppearanceSpan/<init>(Ljava/lang/String;IILandroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;)V
iconst_0
aload 1
invokeinterface java/lang/CharSequence/length()I 0
bipush 33
invokevirtual android/text/SpannableString/setSpan(Ljava/lang/Object;III)V
aload 2
areturn
.limit locals 3
.limit stack 8
.end method

.method private c()I
aload 0
getfield android/support/v7/widget/bz/o I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d()V
aload 0
aconst_null
invokevirtual android/support/v7/widget/bz/a(Landroid/database/Cursor;)V
aload 0
iconst_1
putfield android/support/v7/widget/bz/z Z
return
.limit locals 1
.limit stack 2
.end method

.method private static d(Landroid/database/Cursor;)V
aload 0
ifnull L0
aload 0
invokeinterface android/database/Cursor/getExtras()Landroid/os/Bundle; 0
astore 0
L1:
aload 0
ifnull L2
aload 0
ldc "in_progress"
invokevirtual android/os/Bundle/getBoolean(Ljava/lang/String;)Z
ifeq L2
L2:
return
L0:
aconst_null
astore 0
goto L1
.limit locals 1
.limit stack 2
.end method

.method private e()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/widget/bz/v Landroid/app/SearchableInfo;
invokevirtual android/app/SearchableInfo/getSearchActivity()Landroid/content/ComponentName;
astore 1
aload 1
invokevirtual android/content/ComponentName/flattenToShortString()Ljava/lang/String;
astore 3
aload 0
getfield android/support/v7/widget/bz/x Ljava/util/WeakHashMap;
aload 3
invokevirtual java/util/WeakHashMap/containsKey(Ljava/lang/Object;)Z
ifeq L0
aload 0
getfield android/support/v7/widget/bz/x Ljava/util/WeakHashMap;
aload 3
invokevirtual java/util/WeakHashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/graphics/drawable/Drawable$ConstantState
astore 1
aload 1
ifnonnull L1
aconst_null
astore 1
L2:
aload 1
ifnull L3
aload 1
areturn
L1:
aload 1
aload 0
getfield android/support/v7/widget/bz/w Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/graphics/drawable/Drawable$ConstantState/newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
astore 1
goto L2
L0:
aload 0
aload 1
invokespecial android/support/v7/widget/bz/b(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;
astore 2
aload 2
ifnonnull L4
aconst_null
astore 1
L5:
aload 0
getfield android/support/v7/widget/bz/x Ljava/util/WeakHashMap;
aload 3
aload 1
invokevirtual java/util/WeakHashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 2
astore 1
goto L2
L4:
aload 2
invokevirtual android/graphics/drawable/Drawable/getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
astore 1
goto L5
L3:
aload 0
getfield android/support/v7/widget/bz/d Landroid/content/Context;
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
invokevirtual android/content/pm/PackageManager/getDefaultActivityIcon()Landroid/graphics/drawable/Drawable;
areturn
.limit locals 4
.limit stack 3
.end method

.method private e(Landroid/database/Cursor;)Landroid/graphics/drawable/Drawable;
aconst_null
astore 2
aconst_null
astore 3
aload 0
getfield android/support/v7/widget/bz/E I
iconst_m1
if_icmpne L0
aload 3
astore 2
L1:
aload 2
areturn
L0:
aload 0
aload 1
aload 0
getfield android/support/v7/widget/bz/E I
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
invokespecial android/support/v7/widget/bz/a(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
astore 1
aload 1
ifnull L2
aload 1
areturn
L2:
aload 0
getfield android/support/v7/widget/bz/v Landroid/app/SearchableInfo;
invokevirtual android/app/SearchableInfo/getSearchActivity()Landroid/content/ComponentName;
astore 1
aload 1
invokevirtual android/content/ComponentName/flattenToShortString()Ljava/lang/String;
astore 3
aload 0
getfield android/support/v7/widget/bz/x Ljava/util/WeakHashMap;
aload 3
invokevirtual java/util/WeakHashMap/containsKey(Ljava/lang/Object;)Z
ifeq L3
aload 0
getfield android/support/v7/widget/bz/x Ljava/util/WeakHashMap;
aload 3
invokevirtual java/util/WeakHashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/graphics/drawable/Drawable$ConstantState
astore 1
aload 1
ifnonnull L4
aload 2
astore 1
L5:
aload 1
astore 2
aload 1
ifnonnull L1
aload 0
getfield android/support/v7/widget/bz/d Landroid/content/Context;
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
invokevirtual android/content/pm/PackageManager/getDefaultActivityIcon()Landroid/graphics/drawable/Drawable;
areturn
L4:
aload 1
aload 0
getfield android/support/v7/widget/bz/w Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/graphics/drawable/Drawable$ConstantState/newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
astore 1
goto L5
L3:
aload 0
aload 1
invokespecial android/support/v7/widget/bz/b(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;
astore 2
aload 2
ifnonnull L6
aconst_null
astore 1
L7:
aload 0
getfield android/support/v7/widget/bz/x Ljava/util/WeakHashMap;
aload 3
aload 1
invokevirtual java/util/WeakHashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 2
astore 1
goto L5
L6:
aload 2
invokevirtual android/graphics/drawable/Drawable/getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
astore 1
goto L7
.limit locals 4
.limit stack 3
.end method

.method private f(Landroid/database/Cursor;)Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/widget/bz/F I
iconst_m1
if_icmpne L0
aconst_null
areturn
L0:
aload 0
aload 1
aload 0
getfield android/support/v7/widget/bz/F I
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
invokespecial android/support/v7/widget/bz/a(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
areturn
.limit locals 2
.limit stack 3
.end method

.method public final a(Ljava/lang/CharSequence;)Landroid/database/Cursor;
.catch java/lang/RuntimeException from L0 to L1 using L2
.catch java/lang/RuntimeException from L3 to L4 using L2
.catch java/lang/RuntimeException from L5 to L6 using L2
.catch java/lang/RuntimeException from L7 to L8 using L2
.catch java/lang/RuntimeException from L9 to L10 using L2
.catch java/lang/RuntimeException from L10 to L11 using L2
.catch java/lang/RuntimeException from L12 to L13 using L2
.catch java/lang/RuntimeException from L14 to L15 using L2
.catch java/lang/RuntimeException from L16 to L17 using L2
aload 1
ifnonnull L18
ldc ""
astore 1
L19:
aload 0
getfield android/support/v7/widget/bz/u Landroid/support/v7/widget/SearchView;
invokevirtual android/support/v7/widget/SearchView/getVisibility()I
ifne L20
aload 0
getfield android/support/v7/widget/bz/u Landroid/support/v7/widget/SearchView;
invokevirtual android/support/v7/widget/SearchView/getWindowVisibility()I
ifeq L0
L20:
aconst_null
areturn
L18:
aload 1
invokeinterface java/lang/CharSequence/toString()Ljava/lang/String; 0
astore 1
goto L19
L0:
aload 0
getfield android/support/v7/widget/bz/v Landroid/app/SearchableInfo;
astore 2
L1:
aload 2
ifnonnull L5
aconst_null
astore 1
L21:
aload 1
ifnull L22
L3:
aload 1
invokeinterface android/database/Cursor/getCount()I 0
pop
L4:
aload 1
areturn
L2:
astore 1
ldc "SuggestionsAdapter"
ldc "Search suggestions query threw an exception."
aload 1
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
L22:
aconst_null
areturn
L5:
aload 2
invokevirtual android/app/SearchableInfo/getSuggestAuthority()Ljava/lang/String;
astore 3
L6:
aload 3
ifnonnull L7
aconst_null
astore 1
goto L21
L7:
new android/net/Uri$Builder
dup
invokespecial android/net/Uri$Builder/<init>()V
ldc "content"
invokevirtual android/net/Uri$Builder/scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;
aload 3
invokevirtual android/net/Uri$Builder/authority(Ljava/lang/String;)Landroid/net/Uri$Builder;
ldc ""
invokevirtual android/net/Uri$Builder/query(Ljava/lang/String;)Landroid/net/Uri$Builder;
ldc ""
invokevirtual android/net/Uri$Builder/fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;
astore 3
aload 2
invokevirtual android/app/SearchableInfo/getSuggestPath()Ljava/lang/String;
astore 4
L8:
aload 4
ifnull L10
L9:
aload 3
aload 4
invokevirtual android/net/Uri$Builder/appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;
pop
L10:
aload 3
ldc "search_suggest_query"
invokevirtual android/net/Uri$Builder/appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;
pop
aload 2
invokevirtual android/app/SearchableInfo/getSuggestSelection()Ljava/lang/String;
astore 4
L11:
aload 4
ifnull L16
L12:
iconst_1
anewarray java/lang/String
astore 2
L13:
aload 2
iconst_0
aload 1
aastore
aload 2
astore 1
L14:
aload 3
ldc "limit"
ldc "50"
invokevirtual android/net/Uri$Builder/appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;
pop
aload 3
invokevirtual android/net/Uri$Builder/build()Landroid/net/Uri;
astore 2
aload 0
getfield android/support/v7/widget/bz/d Landroid/content/Context;
invokevirtual android/content/Context/getContentResolver()Landroid/content/ContentResolver;
aload 2
aconst_null
aload 4
aload 1
aconst_null
invokevirtual android/content/ContentResolver/query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
astore 1
L15:
goto L21
L16:
aload 3
aload 1
invokevirtual android/net/Uri$Builder/appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;
pop
L17:
aconst_null
astore 1
goto L14
.limit locals 5
.limit stack 6
.end method

.method public final a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
aload 0
aload 1
aload 2
aload 3
invokespecial android/support/v4/widget/bz/a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
astore 1
aload 1
new android/support/v7/widget/ca
dup
aload 1
invokespecial android/support/v7/widget/ca/<init>(Landroid/view/View;)V
invokevirtual android/view/View/setTag(Ljava/lang/Object;)V
aload 1
getstatic android/support/v7/a/i/edit_query I
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/ImageView
aload 0
getfield android/support/v7/widget/bz/y I
invokevirtual android/widget/ImageView/setImageResource(I)V
aload 1
areturn
.limit locals 4
.limit stack 4
.end method

.method public final a(Landroid/database/Cursor;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
aload 0
getfield android/support/v7/widget/bz/z Z
ifeq L0
ldc "SuggestionsAdapter"
ldc "Tried to change cursor after adapter was closed."
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 1
ifnull L5
aload 1
invokeinterface android/database/Cursor/close()V 0
L5:
return
L0:
aload 0
aload 1
invokespecial android/support/v4/widget/bz/a(Landroid/database/Cursor;)V
L1:
aload 1
ifnull L5
L3:
aload 0
aload 1
ldc "suggest_text_1"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
putfield android/support/v7/widget/bz/B I
aload 0
aload 1
ldc "suggest_text_2"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
putfield android/support/v7/widget/bz/C I
aload 0
aload 1
ldc "suggest_text_2_url"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
putfield android/support/v7/widget/bz/D I
aload 0
aload 1
ldc "suggest_icon_1"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
putfield android/support/v7/widget/bz/E I
aload 0
aload 1
ldc "suggest_icon_2"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
putfield android/support/v7/widget/bz/F I
aload 0
aload 1
ldc "suggest_flags"
invokeinterface android/database/Cursor/getColumnIndex(Ljava/lang/String;)I 1
putfield android/support/v7/widget/bz/G I
L4:
return
L2:
astore 1
ldc "SuggestionsAdapter"
ldc "error changing cursor and caching columns"
aload 1
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
.limit locals 2
.limit stack 3
.end method

.method public final a(Landroid/view/View;Landroid/database/Cursor;)V
aconst_null
astore 5
aload 1
invokevirtual android/view/View/getTag()Ljava/lang/Object;
checkcast android/support/v7/widget/ca
astore 6
aload 0
getfield android/support/v7/widget/bz/G I
iconst_m1
if_icmpeq L0
aload 2
aload 0
getfield android/support/v7/widget/bz/G I
invokeinterface android/database/Cursor/getInt(I)I 1
istore 3
L1:
aload 6
getfield android/support/v7/widget/ca/a Landroid/widget/TextView;
ifnull L2
aload 2
aload 0
getfield android/support/v7/widget/bz/B I
invokestatic android/support/v7/widget/bz/a(Landroid/database/Cursor;I)Ljava/lang/String;
astore 1
aload 6
getfield android/support/v7/widget/ca/a Landroid/widget/TextView;
aload 1
invokestatic android/support/v7/widget/bz/a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
L2:
aload 6
getfield android/support/v7/widget/ca/b Landroid/widget/TextView;
ifnull L3
aload 2
aload 0
getfield android/support/v7/widget/bz/D I
invokestatic android/support/v7/widget/bz/a(Landroid/database/Cursor;I)Ljava/lang/String;
astore 4
aload 4
ifnull L4
aload 0
getfield android/support/v7/widget/bz/A Landroid/content/res/ColorStateList;
ifnonnull L5
new android/util/TypedValue
dup
invokespecial android/util/TypedValue/<init>()V
astore 1
aload 0
getfield android/support/v7/widget/bz/d Landroid/content/Context;
invokevirtual android/content/Context/getTheme()Landroid/content/res/Resources$Theme;
getstatic android/support/v7/a/d/textColorSearchUrl I
aload 1
iconst_1
invokevirtual android/content/res/Resources$Theme/resolveAttribute(ILandroid/util/TypedValue;Z)Z
pop
aload 0
aload 0
getfield android/support/v7/widget/bz/d Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
aload 1
getfield android/util/TypedValue/resourceId I
invokevirtual android/content/res/Resources/getColorStateList(I)Landroid/content/res/ColorStateList;
putfield android/support/v7/widget/bz/A Landroid/content/res/ColorStateList;
L5:
new android/text/SpannableString
dup
aload 4
invokespecial android/text/SpannableString/<init>(Ljava/lang/CharSequence;)V
astore 1
aload 1
new android/text/style/TextAppearanceSpan
dup
aconst_null
iconst_0
iconst_0
aload 0
getfield android/support/v7/widget/bz/A Landroid/content/res/ColorStateList;
aconst_null
invokespecial android/text/style/TextAppearanceSpan/<init>(Ljava/lang/String;IILandroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;)V
iconst_0
aload 4
invokeinterface java/lang/CharSequence/length()I 0
bipush 33
invokevirtual android/text/SpannableString/setSpan(Ljava/lang/Object;III)V
L6:
aload 1
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L7
aload 6
getfield android/support/v7/widget/ca/a Landroid/widget/TextView;
ifnull L8
aload 6
getfield android/support/v7/widget/ca/a Landroid/widget/TextView;
iconst_0
invokevirtual android/widget/TextView/setSingleLine(Z)V
aload 6
getfield android/support/v7/widget/ca/a Landroid/widget/TextView;
iconst_2
invokevirtual android/widget/TextView/setMaxLines(I)V
L8:
aload 6
getfield android/support/v7/widget/ca/b Landroid/widget/TextView;
aload 1
invokestatic android/support/v7/widget/bz/a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
L3:
aload 6
getfield android/support/v7/widget/ca/c Landroid/widget/ImageView;
ifnull L9
aload 6
getfield android/support/v7/widget/ca/c Landroid/widget/ImageView;
astore 7
aload 0
getfield android/support/v7/widget/bz/E I
iconst_m1
if_icmpne L10
aconst_null
astore 1
L11:
aload 7
aload 1
iconst_4
invokestatic android/support/v7/widget/bz/a(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;I)V
L9:
aload 6
getfield android/support/v7/widget/ca/d Landroid/widget/ImageView;
ifnull L12
aload 6
getfield android/support/v7/widget/ca/d Landroid/widget/ImageView;
astore 4
aload 0
getfield android/support/v7/widget/bz/F I
iconst_m1
if_icmpne L13
aload 5
astore 1
L14:
aload 4
aload 1
bipush 8
invokestatic android/support/v7/widget/bz/a(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;I)V
L12:
aload 0
getfield android/support/v7/widget/bz/o I
iconst_2
if_icmpeq L15
aload 0
getfield android/support/v7/widget/bz/o I
iconst_1
if_icmpne L16
iload 3
iconst_1
iand
ifeq L16
L15:
aload 6
getfield android/support/v7/widget/ca/e Landroid/widget/ImageView;
iconst_0
invokevirtual android/widget/ImageView/setVisibility(I)V
aload 6
getfield android/support/v7/widget/ca/e Landroid/widget/ImageView;
aload 6
getfield android/support/v7/widget/ca/a Landroid/widget/TextView;
invokevirtual android/widget/TextView/getText()Ljava/lang/CharSequence;
invokevirtual android/widget/ImageView/setTag(Ljava/lang/Object;)V
aload 6
getfield android/support/v7/widget/ca/e Landroid/widget/ImageView;
aload 0
invokevirtual android/widget/ImageView/setOnClickListener(Landroid/view/View$OnClickListener;)V
return
L4:
aload 2
aload 0
getfield android/support/v7/widget/bz/C I
invokestatic android/support/v7/widget/bz/a(Landroid/database/Cursor;I)Ljava/lang/String;
astore 1
goto L6
L7:
aload 6
getfield android/support/v7/widget/ca/a Landroid/widget/TextView;
ifnull L8
aload 6
getfield android/support/v7/widget/ca/a Landroid/widget/TextView;
iconst_1
invokevirtual android/widget/TextView/setSingleLine(Z)V
aload 6
getfield android/support/v7/widget/ca/a Landroid/widget/TextView;
iconst_1
invokevirtual android/widget/TextView/setMaxLines(I)V
goto L8
L10:
aload 0
aload 2
aload 0
getfield android/support/v7/widget/bz/E I
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
invokespecial android/support/v7/widget/bz/a(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
astore 4
aload 4
astore 1
aload 4
ifnonnull L11
aload 0
getfield android/support/v7/widget/bz/v Landroid/app/SearchableInfo;
invokevirtual android/app/SearchableInfo/getSearchActivity()Landroid/content/ComponentName;
astore 1
aload 1
invokevirtual android/content/ComponentName/flattenToShortString()Ljava/lang/String;
astore 8
aload 0
getfield android/support/v7/widget/bz/x Ljava/util/WeakHashMap;
aload 8
invokevirtual java/util/WeakHashMap/containsKey(Ljava/lang/Object;)Z
ifeq L17
aload 0
getfield android/support/v7/widget/bz/x Ljava/util/WeakHashMap;
aload 8
invokevirtual java/util/WeakHashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast android/graphics/drawable/Drawable$ConstantState
astore 1
aload 1
ifnonnull L18
aconst_null
astore 4
L19:
aload 4
astore 1
aload 4
ifnonnull L11
aload 0
getfield android/support/v7/widget/bz/d Landroid/content/Context;
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
invokevirtual android/content/pm/PackageManager/getDefaultActivityIcon()Landroid/graphics/drawable/Drawable;
astore 1
goto L11
L18:
aload 1
aload 0
getfield android/support/v7/widget/bz/w Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/graphics/drawable/Drawable$ConstantState/newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
astore 4
goto L19
L17:
aload 0
aload 1
invokespecial android/support/v7/widget/bz/b(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;
astore 4
aload 4
ifnonnull L20
aconst_null
astore 1
L21:
aload 0
getfield android/support/v7/widget/bz/x Ljava/util/WeakHashMap;
aload 8
aload 1
invokevirtual java/util/WeakHashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
goto L19
L20:
aload 4
invokevirtual android/graphics/drawable/Drawable/getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
astore 1
goto L21
L13:
aload 0
aload 2
aload 0
getfield android/support/v7/widget/bz/F I
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
invokespecial android/support/v7/widget/bz/a(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
astore 1
goto L14
L16:
aload 6
getfield android/support/v7/widget/ca/e Landroid/widget/ImageView;
bipush 8
invokevirtual android/widget/ImageView/setVisibility(I)V
return
L0:
iconst_0
istore 3
goto L1
.limit locals 9
.limit stack 8
.end method

.method public final c(Landroid/database/Cursor;)Ljava/lang/CharSequence;
aload 1
ifnonnull L0
L1:
aconst_null
areturn
L0:
aload 1
ldc "suggest_intent_query"
invokestatic android/support/v7/widget/bz/a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
astore 2
aload 2
ifnull L2
aload 2
areturn
L2:
aload 0
getfield android/support/v7/widget/bz/v Landroid/app/SearchableInfo;
invokevirtual android/app/SearchableInfo/shouldRewriteQueryFromData()Z
ifeq L3
aload 1
ldc "suggest_intent_data"
invokestatic android/support/v7/widget/bz/a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
astore 2
aload 2
ifnull L3
aload 2
areturn
L3:
aload 0
getfield android/support/v7/widget/bz/v Landroid/app/SearchableInfo;
invokevirtual android/app/SearchableInfo/shouldRewriteQueryFromText()Z
ifeq L1
aload 1
ldc "suggest_text_1"
invokestatic android/support/v7/widget/bz/a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
astore 1
aload 1
ifnull L1
aload 1
areturn
.limit locals 3
.limit stack 2
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.catch java/lang/RuntimeException from L0 to L1 using L2
L0:
aload 0
iload 1
aload 2
aload 3
invokespecial android/support/v4/widget/bz/getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
astore 2
L1:
aload 2
areturn
L2:
astore 2
ldc "SuggestionsAdapter"
ldc "Search suggestions cursor threw exception."
aload 2
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
aload 0
aload 0
getfield android/support/v7/widget/bz/d Landroid/content/Context;
aload 0
getfield android/support/v7/widget/bz/c Landroid/database/Cursor;
aload 3
invokevirtual android/support/v7/widget/bz/a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
astore 3
aload 3
ifnull L3
aload 3
invokevirtual android/view/View/getTag()Ljava/lang/Object;
checkcast android/support/v7/widget/ca
getfield android/support/v7/widget/ca/a Landroid/widget/TextView;
aload 2
invokevirtual java/lang/RuntimeException/toString()Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L3:
aload 3
areturn
.limit locals 4
.limit stack 4
.end method

.method public final hasStableIds()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final notifyDataSetChanged()V
aload 0
invokespecial android/support/v4/widget/bz/notifyDataSetChanged()V
aload 0
getfield android/support/v4/widget/r/c Landroid/database/Cursor;
invokestatic android/support/v7/widget/bz/d(Landroid/database/Cursor;)V
return
.limit locals 1
.limit stack 1
.end method

.method public final notifyDataSetInvalidated()V
aload 0
invokespecial android/support/v4/widget/bz/notifyDataSetInvalidated()V
aload 0
getfield android/support/v4/widget/r/c Landroid/database/Cursor;
invokestatic android/support/v7/widget/bz/d(Landroid/database/Cursor;)V
return
.limit locals 1
.limit stack 1
.end method

.method public final onClick(Landroid/view/View;)V
aload 1
invokevirtual android/view/View/getTag()Ljava/lang/Object;
astore 1
aload 1
instanceof java/lang/CharSequence
ifeq L0
aload 0
getfield android/support/v7/widget/bz/u Landroid/support/v7/widget/SearchView;
aload 1
checkcast java/lang/CharSequence
invokevirtual android/support/v7/widget/SearchView/setQuery(Ljava/lang/CharSequence;)V
L0:
return
.limit locals 2
.limit stack 2
.end method
