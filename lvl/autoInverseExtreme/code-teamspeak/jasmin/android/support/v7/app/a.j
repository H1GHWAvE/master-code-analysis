.bytecode 50.0
.class public synchronized abstract android/support/v7/app/a
.super java/lang/Object

.field public static final 'a' I = 0


.field public static final 'b' I = 1


.field public static final 'c' I = 2


.field public static final 'd' I = 1


.field public static final 'e' I = 2


.field public static final 'f' I = 4


.field public static final 'g' I = 8


.field public static final 'h' I = 16


.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public abstract a()I
.end method

.method public a(Landroid/support/v7/c/b;)Landroid/support/v7/c/a;
aconst_null
areturn
.limit locals 2
.limit stack 1
.end method

.method public a(F)V
fload 1
fconst_0
fcmpl
ifeq L0
new java/lang/UnsupportedOperationException
dup
ldc "Setting a non-zero elevation is not supported in this action bar configuration."
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
L0:
return
.limit locals 2
.limit stack 3
.end method

.method public abstract a(I)V
.end method

.method public abstract a(II)V
.end method

.method public a(Landroid/content/res/Configuration;)V
return
.limit locals 2
.limit stack 0
.end method

.method public abstract a(Landroid/graphics/drawable/Drawable;)V
.end method

.method public abstract a(Landroid/support/v7/app/e;)V
.end method

.method public abstract a(Landroid/support/v7/app/g;)V
.end method

.method public abstract a(Landroid/support/v7/app/g;I)V
.end method

.method public abstract a(Landroid/support/v7/app/g;IZ)V
.end method

.method public abstract a(Landroid/support/v7/app/g;Z)V
.end method

.method public abstract a(Landroid/view/View;)V
.end method

.method public abstract a(Landroid/view/View;Landroid/support/v7/app/c;)V
.end method

.method public abstract a(Landroid/widget/SpinnerAdapter;Landroid/support/v7/app/f;)V
.end method

.method public abstract a(Ljava/lang/CharSequence;)V
.end method

.method public abstract a(Z)V
.end method

.method public a(ILandroid/view/KeyEvent;)Z
iconst_0
ireturn
.limit locals 3
.limit stack 1
.end method

.method public a(Landroid/view/KeyEvent;)Z
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method public abstract b()I
.end method

.method public abstract b(I)V
.annotation invisibleparam 1 Landroid/support/a/m;
.end annotation
.end method

.method public abstract b(Landroid/graphics/drawable/Drawable;)V
.end method

.method public abstract b(Landroid/support/v7/app/e;)V
.end method

.method public abstract b(Landroid/support/v7/app/g;)V
.end method

.method public abstract b(Ljava/lang/CharSequence;)V
.end method

.method public abstract b(Z)V
.end method

.method public c()V
return
.limit locals 1
.limit stack 0
.end method

.method public abstract c(I)V
.annotation invisibleparam 1 Landroid/support/a/m;
.end annotation
.end method

.method public abstract c(Landroid/graphics/drawable/Drawable;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
.end method

.method public abstract c(Landroid/support/v7/app/g;)V
.end method

.method public c(Ljava/lang/CharSequence;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
return
.limit locals 2
.limit stack 0
.end method

.method public abstract c(Z)V
.end method

.method public abstract d()Landroid/view/View;
.end method

.method public abstract d(I)V
.end method

.method public d(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 2
.limit stack 0
.end method

.method public d(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 0
.end method

.method public abstract d(Z)V
.end method

.method public abstract e()Ljava/lang/CharSequence;
.annotation invisible Landroid/support/a/z;
.end annotation
.end method

.method public abstract e(I)V
.annotation invisibleparam 1 Landroid/support/a/ah;
.end annotation
.end method

.method public e(Landroid/graphics/drawable/Drawable;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
return
.limit locals 2
.limit stack 0
.end method

.method public abstract e(Z)V
.end method

.method public abstract f()Ljava/lang/CharSequence;
.annotation invisible Landroid/support/a/z;
.end annotation
.end method

.method public abstract f(I)V
.end method

.method public f(Z)V
return
.limit locals 2
.limit stack 0
.end method

.method public abstract g()I
.end method

.method public abstract g(I)V
.end method

.method public g(Z)V
return
.limit locals 2
.limit stack 0
.end method

.method public abstract h()I
.end method

.method public abstract h(I)V
.end method

.method public h(Z)V
return
.limit locals 2
.limit stack 0
.end method

.method public abstract i()Landroid/support/v7/app/g;
.end method

.method public abstract i(I)V
.end method

.method public abstract j(I)Landroid/support/v7/app/g;
.end method

.method public abstract j()V
.end method

.method public abstract k()Landroid/support/v7/app/g;
.annotation invisible Landroid/support/a/z;
.end annotation
.end method

.method public k(I)V
.annotation invisibleparam 1 Landroid/support/a/m;
.end annotation
return
.limit locals 2
.limit stack 0
.end method

.method public abstract l()I
.end method

.method public l(I)V
.annotation invisibleparam 1 Landroid/support/a/ah;
.end annotation
return
.limit locals 2
.limit stack 0
.end method

.method public abstract m()I
.end method

.method public m(I)V
iload 1
ifeq L0
new java/lang/UnsupportedOperationException
dup
ldc "Setting an explicit action bar hide offset is not supported in this action bar configuration."
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
L0:
return
.limit locals 2
.limit stack 3
.end method

.method public abstract n()V
.end method

.method public abstract o()V
.end method

.method public abstract p()Z
.end method

.method public q()V
return
.limit locals 1
.limit stack 0
.end method

.method public r()Landroid/content/Context;
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method public s()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public t()V
new java/lang/UnsupportedOperationException
dup
ldc "Hide on content scroll is not supported in this action bar configuration."
invokespecial java/lang/UnsupportedOperationException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method public u()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public v()I
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public w()F
fconst_0
freturn
.limit locals 1
.limit stack 1
.end method

.method public x()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public y()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public z()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method
