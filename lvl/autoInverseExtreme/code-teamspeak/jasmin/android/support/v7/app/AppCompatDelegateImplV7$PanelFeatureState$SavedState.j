.bytecode 50.0
.class synchronized android/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState$SavedState
.super java/lang/Object
.implements android/os/Parcelable

.field public static final 'CREATOR' Landroid/os/Parcelable$Creator;

.field 'a' I

.field 'b' Z

.field 'c' Landroid/os/Bundle;

.method static <clinit>()V
new android/support/v7/app/bd
dup
invokespecial android/support/v7/app/bd/<init>()V
putstatic android/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState$SavedState/CREATOR Landroid/os/Parcelable$Creator;
return
.limit locals 0
.limit stack 2
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method synthetic <init>(B)V
aload 0
invokespecial android/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState$SavedState/<init>()V
return
.limit locals 2
.limit stack 1
.end method

.method static synthetic a(Landroid/os/Parcel;)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState$SavedState;
iconst_1
istore 1
new android/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState$SavedState
dup
invokespecial android/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState$SavedState/<init>()V
astore 2
aload 2
aload 0
invokevirtual android/os/Parcel/readInt()I
putfield android/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState$SavedState/a I
aload 0
invokevirtual android/os/Parcel/readInt()I
iconst_1
if_icmpne L0
L1:
aload 2
iload 1
putfield android/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState$SavedState/b Z
aload 2
getfield android/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState$SavedState/b Z
ifeq L2
aload 2
aload 0
invokevirtual android/os/Parcel/readBundle()Landroid/os/Bundle;
putfield android/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState$SavedState/c Landroid/os/Bundle;
L2:
aload 2
areturn
L0:
iconst_0
istore 1
goto L1
.limit locals 3
.limit stack 2
.end method

.method private static b(Landroid/os/Parcel;)Landroid/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState$SavedState;
iconst_1
istore 1
new android/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState$SavedState
dup
invokespecial android/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState$SavedState/<init>()V
astore 2
aload 2
aload 0
invokevirtual android/os/Parcel/readInt()I
putfield android/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState$SavedState/a I
aload 0
invokevirtual android/os/Parcel/readInt()I
iconst_1
if_icmpne L0
L1:
aload 2
iload 1
putfield android/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState$SavedState/b Z
aload 2
getfield android/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState$SavedState/b Z
ifeq L2
aload 2
aload 0
invokevirtual android/os/Parcel/readBundle()Landroid/os/Bundle;
putfield android/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState$SavedState/c Landroid/os/Bundle;
L2:
aload 2
areturn
L0:
iconst_0
istore 1
goto L1
.limit locals 3
.limit stack 2
.end method

.method public describeContents()I
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
aload 1
aload 0
getfield android/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState$SavedState/a I
invokevirtual android/os/Parcel/writeInt(I)V
aload 0
getfield android/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState$SavedState/b Z
ifeq L0
iconst_1
istore 2
L1:
aload 1
iload 2
invokevirtual android/os/Parcel/writeInt(I)V
aload 0
getfield android/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState$SavedState/b Z
ifeq L2
aload 1
aload 0
getfield android/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState$SavedState/c Landroid/os/Bundle;
invokevirtual android/os/Parcel/writeBundle(Landroid/os/Bundle;)V
L2:
return
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 2
.end method
