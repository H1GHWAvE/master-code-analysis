.bytecode 50.0
.class synchronized abstract android/support/v7/app/ak
.super android/support/v7/app/aj

.field final 'e' Landroid/content/Context;

.field final 'f' Landroid/view/Window;

.field final 'g' Landroid/view/Window$Callback;

.field final 'h' Landroid/view/Window$Callback;

.field final 'i' Landroid/support/v7/app/ai;

.field 'j' Landroid/support/v7/app/a;

.field 'k' Landroid/view/MenuInflater;

.field 'l' Z

.field 'm' Z

.field 'n' Z

.field 'o' Z

.field 'p' Z

.field 'q' Ljava/lang/CharSequence;

.field 'r' Z

.method <init>(Landroid/content/Context;Landroid/view/Window;Landroid/support/v7/app/ai;)V
aload 0
invokespecial android/support/v7/app/aj/<init>()V
aload 0
aload 1
putfield android/support/v7/app/ak/e Landroid/content/Context;
aload 0
aload 2
putfield android/support/v7/app/ak/f Landroid/view/Window;
aload 0
aload 3
putfield android/support/v7/app/ak/i Landroid/support/v7/app/ai;
aload 0
aload 0
getfield android/support/v7/app/ak/f Landroid/view/Window;
invokevirtual android/view/Window/getCallback()Landroid/view/Window$Callback;
putfield android/support/v7/app/ak/g Landroid/view/Window$Callback;
aload 0
getfield android/support/v7/app/ak/g Landroid/view/Window$Callback;
instanceof android/support/v7/app/an
ifeq L0
new java/lang/IllegalStateException
dup
ldc "AppCompat has already installed itself into the Window"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aload 0
aload 0
getfield android/support/v7/app/ak/g Landroid/view/Window$Callback;
invokevirtual android/support/v7/app/ak/a(Landroid/view/Window$Callback;)Landroid/view/Window$Callback;
putfield android/support/v7/app/ak/h Landroid/view/Window$Callback;
aload 0
getfield android/support/v7/app/ak/f Landroid/view/Window;
aload 0
getfield android/support/v7/app/ak/h Landroid/view/Window$Callback;
invokevirtual android/view/Window/setCallback(Landroid/view/Window$Callback;)V
return
.limit locals 4
.limit stack 3
.end method

.method private n()Landroid/support/v7/app/a;
aload 0
getfield android/support/v7/app/ak/j Landroid/support/v7/app/a;
areturn
.limit locals 1
.limit stack 1
.end method

.method private o()Z
aload 0
getfield android/support/v7/app/ak/r Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private p()Landroid/view/Window$Callback;
aload 0
getfield android/support/v7/app/ak/f Landroid/view/Window;
invokevirtual android/view/Window/getCallback()Landroid/view/Window$Callback;
areturn
.limit locals 1
.limit stack 1
.end method

.method private q()Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/app/ak/g Landroid/view/Window$Callback;
instanceof android/app/Activity
ifeq L0
aload 0
getfield android/support/v7/app/ak/g Landroid/view/Window$Callback;
checkcast android/app/Activity
invokevirtual android/app/Activity/getTitle()Ljava/lang/CharSequence;
areturn
L0:
aload 0
getfield android/support/v7/app/ak/q Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a()Landroid/support/v7/app/a;
aload 0
invokevirtual android/support/v7/app/ak/l()V
aload 0
getfield android/support/v7/app/ak/j Landroid/support/v7/app/a;
areturn
.limit locals 1
.limit stack 1
.end method

.method a(Landroid/view/Window$Callback;)Landroid/view/Window$Callback;
new android/support/v7/app/an
dup
aload 0
aload 1
invokespecial android/support/v7/app/an/<init>(Landroid/support/v7/app/ak;Landroid/view/Window$Callback;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public final a(Ljava/lang/CharSequence;)V
aload 0
aload 1
putfield android/support/v7/app/ak/q Ljava/lang/CharSequence;
aload 0
aload 1
invokevirtual android/support/v7/app/ak/b(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 2
.end method

.method public a(Z)V
return
.limit locals 2
.limit stack 0
.end method

.method abstract a(ILandroid/view/KeyEvent;)Z
.end method

.method abstract a(Landroid/view/KeyEvent;)Z
.end method

.method abstract b(Landroid/support/v7/c/b;)Landroid/support/v7/c/a;
.end method

.method public final b()Landroid/view/MenuInflater;
aload 0
getfield android/support/v7/app/ak/k Landroid/view/MenuInflater;
ifnonnull L0
aload 0
invokevirtual android/support/v7/app/ak/l()V
aload 0
getfield android/support/v7/app/ak/j Landroid/support/v7/app/a;
ifnull L1
aload 0
getfield android/support/v7/app/ak/j Landroid/support/v7/app/a;
invokevirtual android/support/v7/app/a/r()Landroid/content/Context;
astore 1
L2:
aload 0
new android/support/v7/internal/view/f
dup
aload 1
invokespecial android/support/v7/internal/view/f/<init>(Landroid/content/Context;)V
putfield android/support/v7/app/ak/k Landroid/view/MenuInflater;
L0:
aload 0
getfield android/support/v7/app/ak/k Landroid/view/MenuInflater;
areturn
L1:
aload 0
getfield android/support/v7/app/ak/e Landroid/content/Context;
astore 1
goto L2
.limit locals 2
.limit stack 4
.end method

.method abstract b(Ljava/lang/CharSequence;)V
.end method

.method abstract d(I)V
.end method

.method abstract e(I)Z
.end method

.method public final h()V
aload 0
iconst_1
putfield android/support/v7/app/ak/r Z
return
.limit locals 1
.limit stack 2
.end method

.method public final i()Landroid/support/v7/app/l;
new android/support/v7/app/am
dup
aload 0
iconst_0
invokespecial android/support/v7/app/am/<init>(Landroid/support/v7/app/ak;B)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public k()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method abstract l()V
.end method

.method final m()Landroid/content/Context;
aconst_null
astore 1
aload 0
invokevirtual android/support/v7/app/ak/a()Landroid/support/v7/app/a;
astore 2
aload 2
ifnull L0
aload 2
invokevirtual android/support/v7/app/a/r()Landroid/content/Context;
astore 1
L0:
aload 1
astore 2
aload 1
ifnonnull L1
aload 0
getfield android/support/v7/app/ak/e Landroid/content/Context;
astore 2
L1:
aload 2
areturn
.limit locals 3
.limit stack 1
.end method
