.bytecode 50.0
.class final synchronized net/hockeyapp/android/d/f
.super android/os/AsyncTask

.field private final 'a' Lnet/hockeyapp/android/d/e;

.field private final 'b' Landroid/os/Handler;

.field private 'c' Ljava/io/File;

.field private 'd' Landroid/graphics/Bitmap;

.field private 'e' I

.method public <init>(Lnet/hockeyapp/android/d/e;Landroid/os/Handler;)V
aload 0
invokespecial android/os/AsyncTask/<init>()V
aload 0
aload 1
putfield net/hockeyapp/android/d/f/a Lnet/hockeyapp/android/d/e;
aload 0
aload 2
putfield net/hockeyapp/android/d/f/b Landroid/os/Handler;
aload 0
invokestatic net/hockeyapp/android/a/a()Ljava/io/File;
putfield net/hockeyapp/android/d/f/c Ljava/io/File;
aload 0
aconst_null
putfield net/hockeyapp/android/d/f/d Landroid/graphics/Bitmap;
aload 0
iconst_0
putfield net/hockeyapp/android/d/f/e I
return
.limit locals 3
.limit stack 2
.end method

.method private transient a()Ljava/lang/Boolean;
iconst_0
istore 2
aload 0
getfield net/hockeyapp/android/d/f/a Lnet/hockeyapp/android/d/e;
getfield net/hockeyapp/android/d/e/a Lnet/hockeyapp/android/c/e;
astore 4
invokestatic net/hockeyapp/android/a/a()Ljava/io/File;
astore 5
iload 2
istore 1
aload 5
invokevirtual java/io/File/exists()Z
ifeq L0
iload 2
istore 1
aload 5
invokevirtual java/io/File/isDirectory()Z
ifeq L0
aload 5
new net/hockeyapp/android/c/f
dup
aload 4
invokespecial net/hockeyapp/android/c/f/<init>(Lnet/hockeyapp/android/c/e;)V
invokevirtual java/io/File/listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;
astore 5
iload 2
istore 1
aload 5
ifnull L0
iload 2
istore 1
aload 5
arraylength
iconst_1
if_icmpne L0
iconst_1
istore 1
L0:
iload 1
ifeq L1
ldc "HockeyApp"
ldc "Cached..."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
invokespecial net/hockeyapp/android/d/f/c()V
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
areturn
L1:
ldc "HockeyApp"
ldc "Downloading..."
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
aload 4
getfield net/hockeyapp/android/c/e/d Ljava/lang/String;
aload 4
invokevirtual net/hockeyapp/android/c/e/a()Ljava/lang/String;
invokespecial net/hockeyapp/android/d/f/a(Ljava/lang/String;Ljava/lang/String;)Z
istore 3
iload 3
ifeq L2
aload 0
invokespecial net/hockeyapp/android/d/f/c()V
L2:
iload 3
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
areturn
.limit locals 6
.limit stack 4
.end method

.method private static a(Ljava/net/URL;)Ljava/net/URLConnection;
aload 0
invokevirtual java/net/URL/openConnection()Ljava/net/URLConnection;
checkcast java/net/HttpURLConnection
astore 0
aload 0
ldc "User-Agent"
ldc "HockeySDK/Android"
invokevirtual java/net/HttpURLConnection/addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
aload 0
iconst_1
invokevirtual java/net/HttpURLConnection/setInstanceFollowRedirects(Z)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 9
if_icmpgt L0
aload 0
ldc "connection"
ldc "close"
invokevirtual java/net/HttpURLConnection/setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
L0:
aload 0
areturn
.limit locals 1
.limit stack 3
.end method

.method private a(Ljava/lang/Boolean;)V
iconst_1
istore 2
aload 0
getfield net/hockeyapp/android/d/f/a Lnet/hockeyapp/android/d/e;
getfield net/hockeyapp/android/d/e/b Lnet/hockeyapp/android/f/b;
astore 3
aload 0
getfield net/hockeyapp/android/d/f/a Lnet/hockeyapp/android/d/e;
aload 1
invokevirtual java/lang/Boolean/booleanValue()Z
putfield net/hockeyapp/android/d/e/c Z
aload 1
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L0
aload 0
getfield net/hockeyapp/android/d/f/d Landroid/graphics/Bitmap;
astore 1
aload 0
getfield net/hockeyapp/android/d/f/e I
istore 2
aload 3
getfield net/hockeyapp/android/f/b/c Landroid/widget/TextView;
aload 3
getfield net/hockeyapp/android/f/b/b Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 3
iload 2
putfield net/hockeyapp/android/f/b/d I
aload 1
ifnonnull L1
aload 3
iconst_1
invokevirtual net/hockeyapp/android/f/b/a(Z)V
L2:
aload 0
getfield net/hockeyapp/android/d/f/b Landroid/os/Handler;
iconst_0
invokevirtual android/os/Handler/sendEmptyMessage(I)Z
pop
return
L1:
aload 3
aload 1
iconst_1
invokevirtual net/hockeyapp/android/f/b/a(Landroid/graphics/Bitmap;Z)V
goto L2
L0:
aload 0
getfield net/hockeyapp/android/d/f/a Lnet/hockeyapp/android/d/e;
getfield net/hockeyapp/android/d/e/d I
ifle L3
L4:
iload 2
ifne L2
aload 3
getfield net/hockeyapp/android/f/b/c Landroid/widget/TextView;
ldc "Error"
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
goto L2
L3:
iconst_0
istore 2
goto L4
.limit locals 4
.limit stack 3
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Z
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
.catch java/lang/Exception from L4 to L5 using L2
.catch java/lang/Exception from L6 to L7 using L2
.catch java/lang/Exception from L8 to L9 using L2
.catch java/lang/Exception from L10 to L11 using L2
.catch java/lang/Exception from L12 to L13 using L2
L0:
new java/net/URL
dup
aload 1
invokespecial java/net/URL/<init>(Ljava/lang/String;)V
invokevirtual java/net/URL/openConnection()Ljava/net/URLConnection;
checkcast java/net/HttpURLConnection
astore 1
aload 1
ldc "User-Agent"
ldc "HockeySDK/Android"
invokevirtual java/net/HttpURLConnection/addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
aload 1
iconst_1
invokevirtual java/net/HttpURLConnection/setInstanceFollowRedirects(Z)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 9
if_icmpgt L1
aload 1
ldc "connection"
ldc "close"
invokevirtual java/net/HttpURLConnection/setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
L1:
aload 1
invokevirtual java/net/URLConnection/connect()V
aload 1
invokevirtual java/net/URLConnection/getContentLength()I
istore 3
aload 1
ldc "Status"
invokevirtual java/net/URLConnection/getHeaderField(Ljava/lang/String;)Ljava/lang/String;
astore 7
L3:
aload 7
ifnull L6
L4:
aload 7
ldc "200"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifne L6
L5:
iconst_0
ireturn
L6:
new java/io/File
dup
aload 0
getfield net/hockeyapp/android/d/f/c Ljava/io/File;
aload 2
invokespecial java/io/File/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 2
new java/io/BufferedInputStream
dup
aload 1
invokevirtual java/net/URLConnection/getInputStream()Ljava/io/InputStream;
invokespecial java/io/BufferedInputStream/<init>(Ljava/io/InputStream;)V
astore 1
new java/io/FileOutputStream
dup
aload 2
invokespecial java/io/FileOutputStream/<init>(Ljava/io/File;)V
astore 2
sipush 1024
newarray byte
astore 7
L7:
lconst_0
lstore 5
L8:
aload 1
aload 7
invokevirtual java/io/InputStream/read([B)I
istore 4
L9:
iload 4
iconst_m1
if_icmpeq L12
lload 5
iload 4
i2l
ladd
lstore 5
L10:
aload 0
iconst_1
anewarray java/lang/Integer
dup
iconst_0
ldc2_w 100L
lload 5
lmul
iload 3
i2l
ldiv
l2i
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokevirtual net/hockeyapp/android/d/f/publishProgress([Ljava/lang/Object;)V
aload 2
aload 7
iconst_0
iload 4
invokevirtual java/io/OutputStream/write([BII)V
L11:
goto L8
L2:
astore 1
aload 1
invokevirtual java/lang/Exception/printStackTrace()V
iconst_0
ireturn
L12:
aload 2
invokevirtual java/io/OutputStream/flush()V
aload 2
invokevirtual java/io/OutputStream/close()V
aload 1
invokevirtual java/io/InputStream/close()V
L13:
lload 5
lconst_0
lcmp
ifle L14
iconst_1
ireturn
L14:
iconst_0
ireturn
.limit locals 8
.limit stack 8
.end method

.method private static transient b()V
return
.limit locals 0
.limit stack 0
.end method

.method private c()V
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L1 to L3 using L2
.catch java/io/IOException from L3 to L4 using L2
.catch java/io/IOException from L5 to L6 using L2
.catch java/io/IOException from L7 to L8 using L2
L0:
aload 0
getfield net/hockeyapp/android/d/f/a Lnet/hockeyapp/android/d/e;
getfield net/hockeyapp/android/d/e/a Lnet/hockeyapp/android/c/e;
invokevirtual net/hockeyapp/android/c/e/a()Ljava/lang/String;
astore 3
aload 0
getfield net/hockeyapp/android/d/f/a Lnet/hockeyapp/android/d/e;
getfield net/hockeyapp/android/d/e/b Lnet/hockeyapp/android/f/b;
astore 4
aload 0
new java/io/File
dup
aload 0
getfield net/hockeyapp/android/d/f/c Ljava/io/File;
aload 3
invokespecial java/io/File/<init>(Ljava/io/File;Ljava/lang/String;)V
invokestatic net/hockeyapp/android/e/m/a(Ljava/io/File;)I
putfield net/hockeyapp/android/d/f/e I
aload 0
getfield net/hockeyapp/android/d/f/e I
iconst_1
if_icmpne L5
aload 4
invokevirtual net/hockeyapp/android/f/b/getWidthLandscape()I
istore 1
L1:
aload 0
getfield net/hockeyapp/android/d/f/e I
iconst_1
if_icmpne L7
aload 4
invokevirtual net/hockeyapp/android/f/b/getMaxHeightLandscape()I
istore 2
L3:
new java/io/File
dup
aload 0
getfield net/hockeyapp/android/d/f/c Ljava/io/File;
aload 3
invokespecial java/io/File/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 3
new android/graphics/BitmapFactory$Options
dup
invokespecial android/graphics/BitmapFactory$Options/<init>()V
astore 4
aload 4
iconst_1
putfield android/graphics/BitmapFactory$Options/inJustDecodeBounds Z
aload 3
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aload 4
invokestatic android/graphics/BitmapFactory/decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
pop
aload 4
aload 4
iload 1
iload 2
invokestatic net/hockeyapp/android/e/m/a(Landroid/graphics/BitmapFactory$Options;II)I
putfield android/graphics/BitmapFactory$Options/inSampleSize I
aload 4
iconst_0
putfield android/graphics/BitmapFactory$Options/inJustDecodeBounds Z
aload 0
aload 3
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
aload 4
invokestatic android/graphics/BitmapFactory/decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
putfield net/hockeyapp/android/d/f/d Landroid/graphics/Bitmap;
L4:
return
L5:
aload 4
invokevirtual net/hockeyapp/android/f/b/getWidthPortrait()I
istore 1
L6:
goto L1
L7:
aload 4
invokevirtual net/hockeyapp/android/f/b/getMaxHeightPortrait()I
istore 2
L8:
goto L3
L2:
astore 3
aload 3
invokevirtual java/io/IOException/printStackTrace()V
aload 0
aconst_null
putfield net/hockeyapp/android/d/f/d Landroid/graphics/Bitmap;
return
.limit locals 5
.limit stack 5
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
aload 0
invokespecial net/hockeyapp/android/d/f/a()Ljava/lang/Boolean;
areturn
.limit locals 2
.limit stack 1
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
iconst_1
istore 2
aload 1
checkcast java/lang/Boolean
astore 3
aload 0
getfield net/hockeyapp/android/d/f/a Lnet/hockeyapp/android/d/e;
getfield net/hockeyapp/android/d/e/b Lnet/hockeyapp/android/f/b;
astore 1
aload 0
getfield net/hockeyapp/android/d/f/a Lnet/hockeyapp/android/d/e;
aload 3
invokevirtual java/lang/Boolean/booleanValue()Z
putfield net/hockeyapp/android/d/e/c Z
aload 3
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L0
aload 0
getfield net/hockeyapp/android/d/f/d Landroid/graphics/Bitmap;
astore 3
aload 0
getfield net/hockeyapp/android/d/f/e I
istore 2
aload 1
getfield net/hockeyapp/android/f/b/c Landroid/widget/TextView;
aload 1
getfield net/hockeyapp/android/f/b/b Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 1
iload 2
putfield net/hockeyapp/android/f/b/d I
aload 3
ifnonnull L1
aload 1
iconst_1
invokevirtual net/hockeyapp/android/f/b/a(Z)V
L2:
aload 0
getfield net/hockeyapp/android/d/f/b Landroid/os/Handler;
iconst_0
invokevirtual android/os/Handler/sendEmptyMessage(I)Z
pop
return
L1:
aload 1
aload 3
iconst_1
invokevirtual net/hockeyapp/android/f/b/a(Landroid/graphics/Bitmap;Z)V
goto L2
L0:
aload 0
getfield net/hockeyapp/android/d/f/a Lnet/hockeyapp/android/d/e;
getfield net/hockeyapp/android/d/e/d I
ifle L3
L4:
iload 2
ifne L2
aload 1
getfield net/hockeyapp/android/f/b/c Landroid/widget/TextView;
ldc "Error"
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
goto L2
L3:
iconst_0
istore 2
goto L4
.limit locals 4
.limit stack 3
.end method

.method protected final onPreExecute()V
return
.limit locals 1
.limit stack 0
.end method

.method protected final volatile synthetic onProgressUpdate([Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 0
.end method
