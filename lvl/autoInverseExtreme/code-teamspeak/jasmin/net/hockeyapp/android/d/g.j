.bytecode 50.0
.class public synchronized net/hockeyapp/android/d/g
.super android/os/AsyncTask

.field protected static final 'a' Ljava/lang/String; = "apk"

.field protected static final 'b' Ljava/lang/String; = "url"

.field protected static final 'c' Ljava/lang/String; = "json"

.field private static final 'h' I = 25


.field protected 'd' Ljava/lang/String;

.field protected 'e' Ljava/lang/String;

.field protected 'f' Ljava/lang/Boolean;

.field protected 'g' Lnet/hockeyapp/android/az;

.field private 'i' Landroid/content/Context;

.field private 'j' J

.method private <init>(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V
aload 0
aload 1
aload 2
iconst_0
invokespecial net/hockeyapp/android/d/g/<init>(Ljava/lang/ref/WeakReference;Ljava/lang/String;B)V
return
.limit locals 3
.limit stack 4
.end method

.method private <init>(Ljava/lang/ref/WeakReference;Ljava/lang/String;B)V
aload 0
aload 1
aload 2
aconst_null
aconst_null
invokespecial net/hockeyapp/android/d/g/<init>(Ljava/lang/ref/WeakReference;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;)V
return
.limit locals 4
.limit stack 5
.end method

.method public <init>(Ljava/lang/ref/WeakReference;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;)V
aload 0
invokespecial android/os/AsyncTask/<init>()V
aload 0
aconst_null
putfield net/hockeyapp/android/d/g/d Ljava/lang/String;
aload 0
aconst_null
putfield net/hockeyapp/android/d/g/e Ljava/lang/String;
aload 0
aconst_null
putfield net/hockeyapp/android/d/g/i Landroid/content/Context;
aload 0
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
putfield net/hockeyapp/android/d/g/f Ljava/lang/Boolean;
aload 0
lconst_0
putfield net/hockeyapp/android/d/g/j J
aload 0
aload 3
putfield net/hockeyapp/android/d/g/e Ljava/lang/String;
aload 0
aload 2
putfield net/hockeyapp/android/d/g/d Ljava/lang/String;
aload 0
aload 4
putfield net/hockeyapp/android/d/g/g Lnet/hockeyapp/android/az;
aload 1
ifnull L0
aload 1
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/content/Context
astore 1
L1:
aload 1
ifnull L2
aload 0
aload 1
invokevirtual android/content/Context/getApplicationContext()Landroid/content/Context;
putfield net/hockeyapp/android/d/g/i Landroid/content/Context;
aload 1
invokestatic net/hockeyapp/android/ak/a(Landroid/content/Context;)Z
ifne L3
lconst_0
lstore 5
L4:
aload 0
lload 5
putfield net/hockeyapp/android/d/g/j J
aload 1
invokestatic net/hockeyapp/android/a/a(Landroid/content/Context;)V
L2:
return
L3:
aload 1
ldc "HockeyApp"
iconst_0
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
new java/lang/StringBuilder
dup
ldc "usageTime"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
getstatic net/hockeyapp/android/a/b Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
lconst_0
invokeinterface android/content/SharedPreferences/getLong(Ljava/lang/String;J)J 3
ldc2_w 1000L
ldiv
lstore 5
goto L4
L0:
aconst_null
astore 1
goto L1
.limit locals 7
.limit stack 4
.end method

.method private static a(Ljava/io/InputStream;)Ljava/lang/String;
.catch java/io/IOException from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/io/IOException from L4 to L5 using L2
.catch all from L4 to L5 using L3
.catch all from L6 to L7 using L3
.catch java/io/IOException from L7 to L8 using L9
.catch java/io/IOException from L10 to L11 using L12
.catch java/io/IOException from L13 to L14 using L15
new java/io/BufferedReader
dup
new java/io/InputStreamReader
dup
aload 0
invokespecial java/io/InputStreamReader/<init>(Ljava/io/InputStream;)V
sipush 1024
invokespecial java/io/BufferedReader/<init>(Ljava/io/Reader;I)V
astore 2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 1
L0:
aload 2
invokevirtual java/io/BufferedReader/readLine()Ljava/lang/String;
astore 3
L1:
aload 3
ifnull L10
L4:
aload 1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L5:
goto L0
L2:
astore 2
L6:
aload 2
invokevirtual java/io/IOException/printStackTrace()V
L7:
aload 0
invokevirtual java/io/InputStream/close()V
L8:
aload 1
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L10:
aload 0
invokevirtual java/io/InputStream/close()V
L11:
goto L8
L12:
astore 0
aload 0
invokevirtual java/io/IOException/printStackTrace()V
goto L8
L9:
astore 0
aload 0
invokevirtual java/io/IOException/printStackTrace()V
goto L8
L3:
astore 1
L13:
aload 0
invokevirtual java/io/InputStream/close()V
L14:
aload 1
athrow
L15:
astore 0
aload 0
invokevirtual java/io/IOException/printStackTrace()V
goto L14
.limit locals 4
.limit stack 5
.end method

.method private static a(Ljava/net/URL;)Ljava/net/URLConnection;
aload 0
invokevirtual java/net/URL/openConnection()Ljava/net/URLConnection;
astore 0
aload 0
ldc "User-Agent"
ldc "HockeySDK/Android"
invokevirtual java/net/URLConnection/addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 9
if_icmpgt L0
aload 0
ldc "connection"
ldc "close"
invokevirtual java/net/URLConnection/setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
L0:
aload 0
areturn
.limit locals 1
.limit stack 3
.end method

.method private a(Lorg/json/JSONArray;I)Z
.catch org/json/JSONException from L0 to L1 using L2
.catch org/json/JSONException from L3 to L4 using L2
.catch org/json/JSONException from L5 to L6 using L2
.catch org/json/JSONException from L7 to L8 using L2
.catch org/json/JSONException from L9 to L10 using L2
.catch org/json/JSONException from L11 to L12 using L2
.catch org/json/JSONException from L13 to L14 using L2
.catch org/json/JSONException from L15 to L16 using L2
iconst_0
istore 3
iconst_0
istore 7
L17:
iload 7
istore 8
L0:
iload 3
aload 1
invokevirtual org/json/JSONArray/length()I
if_icmpge L18
aload 1
iload 3
invokevirtual org/json/JSONArray/getJSONObject(I)Lorg/json/JSONObject;
astore 11
aload 11
ldc "version"
invokevirtual org/json/JSONObject/getInt(Ljava/lang/String;)I
iload 2
if_icmple L19
L1:
iconst_1
istore 4
L3:
aload 11
ldc "version"
invokevirtual org/json/JSONObject/getInt(Ljava/lang/String;)I
iload 2
if_icmpne L20
aload 0
getfield net/hockeyapp/android/d/g/i Landroid/content/Context;
aload 11
ldc "timestamp"
invokevirtual org/json/JSONObject/getLong(Ljava/lang/String;)J
invokestatic net/hockeyapp/android/e/y/a(Landroid/content/Context;J)Z
ifeq L20
L4:
iconst_1
istore 5
L5:
aload 11
ldc "minimum_os_version"
invokevirtual org/json/JSONObject/getString(Ljava/lang/String;)Ljava/lang/String;
astore 12
getstatic android/os/Build$VERSION/RELEASE Ljava/lang/String;
astore 10
L6:
aload 10
ifnull L21
L7:
aload 10
ldc "L"
invokevirtual java/lang/String/equalsIgnoreCase(Ljava/lang/String;)Z
ifeq L13
L8:
goto L21
L9:
aload 12
aload 9
invokestatic net/hockeyapp/android/e/y/a(Ljava/lang/String;Ljava/lang/String;)I
ifgt L22
L10:
iconst_1
istore 6
goto L23
L24:
iload 7
istore 8
iload 6
ifeq L25
L11:
aload 11
ldc "mandatory"
invokevirtual org/json/JSONObject/has(Ljava/lang/String;)Z
ifeq L26
aload 0
aload 0
getfield net/hockeyapp/android/d/g/f Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
aload 11
ldc "mandatory"
invokevirtual org/json/JSONObject/getBoolean(Ljava/lang/String;)Z
ior
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
putfield net/hockeyapp/android/d/g/f Ljava/lang/Boolean;
L12:
goto L26
L13:
aload 10
ldc "M"
invokevirtual java/lang/String/equalsIgnoreCase(Ljava/lang/String;)Z
ifeq L27
L14:
ldc "6.0"
astore 9
goto L9
L27:
aload 10
astore 9
L15:
ldc "^[a-zA-Z]+"
aload 10
invokestatic java/util/regex/Pattern/matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z
ifeq L9
L16:
ldc "99.0"
astore 9
goto L9
L22:
iconst_0
istore 6
goto L23
L2:
astore 1
iconst_0
istore 8
L18:
iload 8
ireturn
L21:
ldc "5.0"
astore 9
goto L9
L23:
iload 4
ifne L24
iload 7
istore 8
iload 5
ifeq L25
goto L24
L26:
iconst_1
istore 8
L25:
iload 3
iconst_1
iadd
istore 3
iload 8
istore 7
goto L17
L19:
iconst_0
istore 4
goto L3
L20:
iconst_0
istore 5
goto L5
.limit locals 13
.limit stack 4
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
.catch java/io/UnsupportedEncodingException from L0 to L1 using L2
L0:
aload 0
ldc "UTF-8"
invokestatic java/net/URLEncoder/encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 0
L1:
aload 0
areturn
L2:
astore 0
ldc ""
areturn
.limit locals 1
.limit stack 2
.end method

.method private static b(Lorg/json/JSONArray;)Lorg/json/JSONArray;
.catch org/json/JSONException from L0 to L1 using L2
new org/json/JSONArray
dup
invokespecial org/json/JSONArray/<init>()V
astore 2
iconst_0
istore 1
L3:
iload 1
aload 0
invokevirtual org/json/JSONArray/length()I
bipush 25
invokestatic java/lang/Math/min(II)I
if_icmpge L4
L0:
aload 2
aload 0
iload 1
invokevirtual org/json/JSONArray/get(I)Ljava/lang/Object;
invokevirtual org/json/JSONArray/put(Ljava/lang/Object;)Lorg/json/JSONArray;
pop
L1:
iload 1
iconst_1
iadd
istore 1
goto L3
L4:
aload 2
areturn
L2:
astore 3
goto L1
.limit locals 4
.limit stack 3
.end method

.method private static c()I
getstatic net/hockeyapp/android/a/b Ljava/lang/String;
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
ireturn
.limit locals 0
.limit stack 1
.end method

.method private transient d()Lorg/json/JSONArray;
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L4 to L5 using L2
L0:
getstatic net/hockeyapp/android/a/b Ljava/lang/String;
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
istore 1
new org/json/JSONArray
dup
aload 0
getfield net/hockeyapp/android/d/g/i Landroid/content/Context;
invokestatic net/hockeyapp/android/e/x/a(Landroid/content/Context;)Ljava/lang/String;
invokespecial org/json/JSONArray/<init>(Ljava/lang/String;)V
astore 2
aload 0
aload 2
iload 1
invokespecial net/hockeyapp/android/d/g/a(Lorg/json/JSONArray;I)Z
ifeq L3
L1:
aload 2
areturn
L3:
new java/net/URL
dup
aload 0
ldc "json"
invokevirtual net/hockeyapp/android/d/g/a(Ljava/lang/String;)Ljava/lang/String;
invokespecial java/net/URL/<init>(Ljava/lang/String;)V
invokevirtual java/net/URL/openConnection()Ljava/net/URLConnection;
astore 2
aload 2
ldc "User-Agent"
ldc "HockeySDK/Android"
invokevirtual java/net/URLConnection/addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 9
if_icmpgt L4
aload 2
ldc "connection"
ldc "close"
invokevirtual java/net/URLConnection/setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
L4:
aload 2
invokevirtual java/net/URLConnection/connect()V
new java/io/BufferedInputStream
dup
aload 2
invokevirtual java/net/URLConnection/getInputStream()Ljava/io/InputStream;
invokespecial java/io/BufferedInputStream/<init>(Ljava/io/InputStream;)V
astore 2
aload 2
invokestatic net/hockeyapp/android/d/g/a(Ljava/io/InputStream;)Ljava/lang/String;
astore 3
aload 2
invokevirtual java/io/InputStream/close()V
new org/json/JSONArray
dup
aload 3
invokespecial org/json/JSONArray/<init>(Ljava/lang/String;)V
astore 2
aload 0
aload 2
iload 1
invokespecial net/hockeyapp/android/d/g/a(Lorg/json/JSONArray;I)Z
ifeq L6
aload 2
invokestatic net/hockeyapp/android/d/g/b(Lorg/json/JSONArray;)Lorg/json/JSONArray;
astore 2
L5:
aload 2
areturn
L2:
astore 2
aload 2
invokevirtual java/lang/Exception/printStackTrace()V
L6:
aconst_null
areturn
.limit locals 4
.limit stack 4
.end method

.method private static e()Z
iconst_1
ireturn
.limit locals 0
.limit stack 1
.end method

.method protected final a(Ljava/lang/String;)Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 3
aload 3
aload 0
getfield net/hockeyapp/android/d/g/d Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 3
ldc "api/2/apps/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 0
getfield net/hockeyapp/android/d/g/e Ljava/lang/String;
ifnull L0
aload 0
getfield net/hockeyapp/android/d/g/e Ljava/lang/String;
astore 2
L1:
aload 3
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 3
new java/lang/StringBuilder
dup
ldc "?format="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 0
getfield net/hockeyapp/android/d/g/i Landroid/content/Context;
invokevirtual android/content/Context/getContentResolver()Landroid/content/ContentResolver;
ldc "android_id"
invokestatic android/provider/Settings$Secure/getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
ifnull L2
aload 3
new java/lang/StringBuilder
dup
ldc "&udid="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield net/hockeyapp/android/d/g/i Landroid/content/Context;
invokevirtual android/content/Context/getContentResolver()Landroid/content/ContentResolver;
ldc "android_id"
invokestatic android/provider/Settings$Secure/getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
invokestatic net/hockeyapp/android/d/g/b(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L2:
aload 3
ldc "&os=Android"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 3
new java/lang/StringBuilder
dup
ldc "&os_version="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
getstatic net/hockeyapp/android/a/e Ljava/lang/String;
invokestatic net/hockeyapp/android/d/g/b(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 3
new java/lang/StringBuilder
dup
ldc "&device="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
getstatic net/hockeyapp/android/a/f Ljava/lang/String;
invokestatic net/hockeyapp/android/d/g/b(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 3
new java/lang/StringBuilder
dup
ldc "&oem="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
getstatic net/hockeyapp/android/a/g Ljava/lang/String;
invokestatic net/hockeyapp/android/d/g/b(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 3
new java/lang/StringBuilder
dup
ldc "&app_version="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
getstatic net/hockeyapp/android/a/b Ljava/lang/String;
invokestatic net/hockeyapp/android/d/g/b(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 3
new java/lang/StringBuilder
dup
ldc "&sdk="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
ldc "HockeySDK"
invokestatic net/hockeyapp/android/d/g/b(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 3
new java/lang/StringBuilder
dup
ldc "&sdk_version="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
ldc "3.6.0"
invokestatic net/hockeyapp/android/d/g/b(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 3
new java/lang/StringBuilder
dup
ldc "&lang="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
invokestatic java/util/Locale/getDefault()Ljava/util/Locale;
invokevirtual java/util/Locale/getLanguage()Ljava/lang/String;
invokestatic net/hockeyapp/android/d/g/b(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 3
new java/lang/StringBuilder
dup
ldc "&usage_time="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield net/hockeyapp/android/d/g/j J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 3
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L0:
aload 0
getfield net/hockeyapp/android/d/g/i Landroid/content/Context;
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
astore 2
goto L1
.limit locals 4
.limit stack 4
.end method

.method public a()V
aload 0
aconst_null
putfield net/hockeyapp/android/d/g/i Landroid/content/Context;
return
.limit locals 1
.limit stack 2
.end method

.method public final a(Ljava/lang/ref/WeakReference;)V
aconst_null
astore 2
aload 1
ifnull L0
aload 1
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/content/Context
astore 2
L0:
aload 2
ifnull L1
aload 0
aload 2
invokevirtual android/content/Context/getApplicationContext()Landroid/content/Context;
putfield net/hockeyapp/android/d/g/i Landroid/content/Context;
aload 2
invokestatic net/hockeyapp/android/a/a(Landroid/content/Context;)V
L1:
return
.limit locals 3
.limit stack 2
.end method

.method protected a(Lorg/json/JSONArray;)V
aload 1
ifnull L0
aload 0
getfield net/hockeyapp/android/d/g/g Lnet/hockeyapp/android/az;
ifnull L0
aload 0
ldc "apk"
invokevirtual net/hockeyapp/android/d/g/a(Ljava/lang/String;)Ljava/lang/String;
pop
L0:
return
.limit locals 2
.limit stack 2
.end method

.method protected b()V
aload 0
aconst_null
putfield net/hockeyapp/android/d/g/d Ljava/lang/String;
aload 0
aconst_null
putfield net/hockeyapp/android/d/g/e Ljava/lang/String;
return
.limit locals 1
.limit stack 2
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
aload 0
invokespecial net/hockeyapp/android/d/g/d()Lorg/json/JSONArray;
areturn
.limit locals 2
.limit stack 1
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
aload 0
aload 1
checkcast org/json/JSONArray
invokevirtual net/hockeyapp/android/d/g/a(Lorg/json/JSONArray;)V
return
.limit locals 2
.limit stack 2
.end method
