.bytecode 50.0
.class public final synchronized net/hockeyapp/android/aj
.super java/lang/Object

.field public static final 'A' I = 1034


.field public static final 'B' I = 1035


.field public static final 'C' I = 1036


.field public static final 'D' I = 1037


.field public static final 'E' I = 1038


.field public static final 'F' I = 1039


.field public static final 'G' I = 1040


.field public static final 'H' I = 1041


.field public static final 'I' I = 1042


.field public static final 'J' I = 1043


.field public static final 'K' I = 1044


.field public static final 'L' I = 1045


.field public static final 'M' I = 1046


.field public static final 'N' I = 1047


.field public static final 'O' I = 1048


.field public static final 'P' I = 1280


.field public static final 'Q' I = 1281


.field public static final 'R' I = 1282


.field public static final 'S' I = 1283


.field public static final 'T' I = 1284


.field public static final 'U' I = 1536


.field public static final 'V' I = 1537


.field public static final 'W' I = 1538


.field public static final 'X' I = 1539


.field public static final 'Y' I = 1540


.field public static final 'Z' I = 1541


.field public static final 'a' I = 0


.field public static final 'aa' I = 1542


.field public static final 'ab' I = 1792


.field public static final 'ac' I = 1793


.field public static final 'ad' I = 1794


.field public static final 'ae' I = 1795


.field public static final 'af' I = 2048


.field public static final 'ag' I = 2049


.field public static final 'ah' I = 2050


.field public static final 'ai' I = 2051


.field private static final 'aj' Ljava/util/Map;

.field public static final 'b' I = 1


.field public static final 'c' I = 2


.field public static final 'd' I = 3


.field public static final 'e' I = 4


.field public static final 'f' I = 256


.field public static final 'g' I = 257


.field public static final 'h' I = 258


.field public static final 'i' I = 259


.field public static final 'j' I = 512


.field public static final 'k' I = 513


.field public static final 'l' I = 514


.field public static final 'm' I = 515


.field public static final 'n' I = 516


.field public static final 'o' I = 768


.field public static final 'p' I = 769


.field public static final 'q' I = 1024


.field public static final 'r' I = 1025


.field public static final 's' I = 1026


.field public static final 't' I = 1027


.field public static final 'u' I = 1028


.field public static final 'v' I = 1029


.field public static final 'w' I = 1030


.field public static final 'x' I = 1031


.field public static final 'y' I = 1032


.field public static final 'z' I = 1033


.method static <clinit>()V
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 0
aload 0
putstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
aload 0
iconst_0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Crash Data"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
iconst_1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "The app found information about previous crashes. Would you like to send this data to the developer?"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
iconst_2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Dismiss"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
iconst_3
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Always send"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
iconst_4
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Send"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 256
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Download Failed"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 257
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "The update could not be downloaded. Would you like to try again?"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 258
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Cancel"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 259
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Retry"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 512
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Please install the latest version to continue to use this app."
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 513
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Update Available"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 514
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Show information about the new update?"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 515
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Dismiss"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 516
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Show"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 768
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Build Expired"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 769
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "This has build has expired. Please check HockeyApp for any updates."
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1024
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Feedback Failed"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1025
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Would you like to send your feedback again?"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1026
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Name"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1027
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Email"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1028
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Subject"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1029
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Message"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1030
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Last Updated: "
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1031
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Add Attachment"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1032
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Send Feedback"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1033
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Add a Response"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1034
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Refresh"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1035
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Feedback"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1036
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Message couldn't be posted. Please check your input values and your connection, then try again."
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1037
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "No response from server. Please check your connection, then try again."
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1038
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Please enter a subject"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1041
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Please enter a name"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1042
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Please enter an email address"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1043
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Please enter a feedback text"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1039
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Message couldn't be posted. Please check the format of your email address."
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1040
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "An error has occurred"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1044
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Attach File"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1045
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Attach Picture"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1046
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Select File"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1047
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Select Picture"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1048
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Only %s attachments allowed."
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1280
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Please enter your account credentials."
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1281
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Please fill in the missing account credentials."
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1282
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Email"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1283
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Password"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1284
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Login"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1536
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Draw something!"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1537
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Save"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1538
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Undo"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1539
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Clear"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1540
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Discard your drawings?"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1541
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "No"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1542
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Yes"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1792
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Need storage access"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1793
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "In order to download and install app updates you will have to allow the app to access your device storage."
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1794
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Cancel"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 1795
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Retry"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 2048
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "OK"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 2049
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Cancel"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 2050
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "Error"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
sipush 2051
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
ldc "An error has occured"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
return
.limit locals 1
.limit stack 3
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(I)Ljava/lang/String;
aconst_null
iload 0
invokestatic net/hockeyapp/android/aj/a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public static a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;
aconst_null
astore 2
aload 0
ifnull L0
aload 0
iload 1
invokevirtual net/hockeyapp/android/ai/a(I)Ljava/lang/String;
astore 2
L0:
aload 2
astore 0
aload 2
ifnonnull L1
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
astore 0
L1:
aload 0
areturn
.limit locals 3
.limit stack 2
.end method

.method public static a(ILjava/lang/String;)V
aload 1
ifnull L0
getstatic net/hockeyapp/android/aj/aj Ljava/util/Map;
iload 0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aload 1
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L0:
return
.limit locals 2
.limit stack 3
.end method
