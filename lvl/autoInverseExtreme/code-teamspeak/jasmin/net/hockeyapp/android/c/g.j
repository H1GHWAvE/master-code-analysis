.bytecode 50.0
.class public final synchronized net/hockeyapp/android/c/g
.super java/lang/Object
.implements java/io/Serializable

.field private static final 'o' J = -8773015828853994624L


.field public 'a' Ljava/lang/String;

.field public 'b' Ljava/lang/String;

.field public 'c' Ljava/lang/String;

.field public 'd' Ljava/lang/String;

.field public 'e' Ljava/lang/String;

.field public 'f' Ljava/lang/String;

.field public 'g' I

.field public 'h' Ljava/lang/String;

.field public 'i' I

.field public 'j' Ljava/lang/String;

.field public 'k' Ljava/lang/String;

.field public 'l' Ljava/lang/String;

.field public 'm' Ljava/lang/String;

.field public 'n' Ljava/util/List;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private a()Ljava/lang/String;
aload 0
getfield net/hockeyapp/android/c/g/a Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(I)V
aload 0
iload 1
putfield net/hockeyapp/android/c/g/g I
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/String;)V
aload 0
aload 1
putfield net/hockeyapp/android/c/g/a Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/util/List;)V
aload 0
aload 1
putfield net/hockeyapp/android/c/g/n Ljava/util/List;
return
.limit locals 2
.limit stack 2
.end method

.method private b()Ljava/lang/String;
aload 0
getfield net/hockeyapp/android/c/g/b Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b(I)V
aload 0
iload 1
putfield net/hockeyapp/android/c/g/i I
return
.limit locals 2
.limit stack 2
.end method

.method private b(Ljava/lang/String;)V
aload 0
aload 1
putfield net/hockeyapp/android/c/g/b Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method private c()Ljava/lang/String;
aload 0
getfield net/hockeyapp/android/c/g/c Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c(Ljava/lang/String;)V
aload 0
aload 1
putfield net/hockeyapp/android/c/g/c Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method private d()Ljava/lang/String;
aload 0
getfield net/hockeyapp/android/c/g/d Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d(Ljava/lang/String;)V
aload 0
aload 1
putfield net/hockeyapp/android/c/g/d Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method private e()Ljava/lang/String;
aload 0
getfield net/hockeyapp/android/c/g/e Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e(Ljava/lang/String;)V
aload 0
aload 1
putfield net/hockeyapp/android/c/g/e Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method private f()Ljava/lang/String;
aload 0
getfield net/hockeyapp/android/c/g/f Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private f(Ljava/lang/String;)V
aload 0
aload 1
putfield net/hockeyapp/android/c/g/f Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method private g()I
aload 0
getfield net/hockeyapp/android/c/g/g I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private g(Ljava/lang/String;)V
aload 0
aload 1
putfield net/hockeyapp/android/c/g/h Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method private h()Ljava/lang/String;
aload 0
getfield net/hockeyapp/android/c/g/h Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private h(Ljava/lang/String;)V
aload 0
aload 1
putfield net/hockeyapp/android/c/g/j Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method private i()I
aload 0
getfield net/hockeyapp/android/c/g/i I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private i(Ljava/lang/String;)V
aload 0
aload 1
putfield net/hockeyapp/android/c/g/k Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method private j()Ljava/lang/String;
aload 0
getfield net/hockeyapp/android/c/g/j Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private j(Ljava/lang/String;)V
aload 0
aload 1
putfield net/hockeyapp/android/c/g/l Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method private k()Ljava/lang/String;
aload 0
getfield net/hockeyapp/android/c/g/k Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private k(Ljava/lang/String;)V
aload 0
aload 1
putfield net/hockeyapp/android/c/g/m Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method private l()Ljava/lang/String;
aload 0
getfield net/hockeyapp/android/c/g/l Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private m()Ljava/lang/String;
aload 0
getfield net/hockeyapp/android/c/g/m Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private n()Ljava/util/List;
aload 0
getfield net/hockeyapp/android/c/g/n Ljava/util/List;
areturn
.limit locals 1
.limit stack 1
.end method
