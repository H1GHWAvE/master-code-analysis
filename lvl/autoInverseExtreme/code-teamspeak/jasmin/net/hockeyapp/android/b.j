.bytecode 50.0
.class public final synchronized net/hockeyapp/android/b
.super java/lang/Object

.field private static 'a' Ljava/lang/String;

.field private static 'b' Ljava/lang/String;

.field private static 'c' Z = 0


.field private static final 'd' Ljava/lang/String; = "always_send_crash_reports"

.method static <clinit>()V
aconst_null
putstatic net/hockeyapp/android/b/a Ljava/lang/String;
aconst_null
putstatic net/hockeyapp/android/b/b Ljava/lang/String;
iconst_0
putstatic net/hockeyapp/android/b/c Z
return
.limit locals 0
.limit stack 1
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/ref/WeakReference;)I
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
iconst_0
istore 1
invokestatic net/hockeyapp/android/b/c()[Ljava/lang/String;
astore 6
aconst_null
astore 5
aload 6
ifnull L5
aload 6
arraylength
ifle L5
L0:
aload 0
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/content/Context
astore 0
L1:
aload 0
ifnull L6
L3:
aload 0
ldc "HockeySDK"
iconst_0
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
ldc "ConfirmedFilenames"
ldc ""
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
ldc "\\|"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
astore 0
L4:
aload 0
ifnull L7
iconst_2
istore 3
aload 6
arraylength
istore 4
L8:
iload 3
istore 2
iload 1
iload 4
if_icmpge L9
aload 0
aload 6
iload 1
aaload
invokeinterface java/util/List/contains(Ljava/lang/Object;)Z 1
ifeq L7
iload 1
iconst_1
iadd
istore 1
goto L8
L7:
iconst_1
istore 2
L9:
iload 2
ireturn
L2:
astore 0
aload 5
astore 0
goto L4
L6:
aconst_null
astore 0
goto L4
L5:
iconst_0
ireturn
.limit locals 7
.limit stack 3
.end method

.method private static a([Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 3
iconst_0
istore 2
L0:
iload 2
aload 0
arraylength
if_icmpge L1
aload 3
aload 0
iload 2
aaload
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
iload 2
aload 0
arraylength
iconst_1
isub
if_icmpge L2
aload 3
aload 1
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 3
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 4
.limit stack 3
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
aload 0
ldc "https://sdk.hockeyapp.net/"
aload 1
aconst_null
invokestatic net/hockeyapp/android/b/a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/i;)V
return
.limit locals 2
.limit stack 4
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/i;)V
aload 0
aload 1
aload 2
aload 3
iconst_0
invokestatic net/hockeyapp/android/b/a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/i;Z)V
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
astore 6
new java/lang/ref/WeakReference
dup
aload 0
invokespecial java/lang/ref/WeakReference/<init>(Ljava/lang/Object;)V
astore 2
aload 2
invokestatic net/hockeyapp/android/b/a(Ljava/lang/ref/WeakReference;)I
istore 4
iload 4
iconst_1
if_icmpne L0
aload 0
instanceof android/app/Activity
ifne L1
iconst_1
istore 5
L2:
aload 0
invokestatic android/preference/PreferenceManager/getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;
astore 0
iload 5
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
aload 0
ldc "always_send_crash_reports"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ior
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
astore 1
aload 1
astore 0
aload 3
ifnull L3
aload 1
invokevirtual java/lang/Boolean/booleanValue()Z
iconst_0
ior
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
iconst_0
ior
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
astore 0
L3:
aload 0
invokevirtual java/lang/Boolean/booleanValue()Z
ifne L4
aload 6
invokevirtual java/lang/Boolean/booleanValue()Z
istore 5
aload 2
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/content/Context
astore 0
aload 0
ifnull L5
new android/app/AlertDialog$Builder
dup
aload 0
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
astore 0
aload 0
aload 3
iconst_0
invokestatic net/hockeyapp/android/aj/a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;
invokevirtual android/app/AlertDialog$Builder/setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
pop
aload 0
aload 3
iconst_1
invokestatic net/hockeyapp/android/aj/a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;
invokevirtual android/app/AlertDialog$Builder/setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
pop
aload 0
aload 3
iconst_2
invokestatic net/hockeyapp/android/aj/a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;
new net/hockeyapp/android/c
dup
aload 3
aload 2
iload 5
invokespecial net/hockeyapp/android/c/<init>(Lnet/hockeyapp/android/i;Ljava/lang/ref/WeakReference;Z)V
invokevirtual android/app/AlertDialog$Builder/setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
pop
aload 0
aload 3
iconst_3
invokestatic net/hockeyapp/android/aj/a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;
new net/hockeyapp/android/d
dup
aload 3
aload 2
iload 5
invokespecial net/hockeyapp/android/d/<init>(Lnet/hockeyapp/android/i;Ljava/lang/ref/WeakReference;Z)V
invokevirtual android/app/AlertDialog$Builder/setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
pop
aload 0
aload 3
iconst_4
invokestatic net/hockeyapp/android/aj/a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;
new net/hockeyapp/android/e
dup
aload 3
aload 2
iload 5
invokespecial net/hockeyapp/android/e/<init>(Lnet/hockeyapp/android/i;Ljava/lang/ref/WeakReference;Z)V
invokevirtual android/app/AlertDialog$Builder/setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
pop
aload 0
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
invokevirtual android/app/AlertDialog/show()V
L5:
return
L1:
iconst_0
istore 5
goto L2
L4:
aload 2
aload 3
aload 6
invokevirtual java/lang/Boolean/booleanValue()Z
invokestatic net/hockeyapp/android/b/c(Ljava/lang/ref/WeakReference;Lnet/hockeyapp/android/i;Z)V
return
L0:
iload 4
iconst_2
if_icmpne L6
aload 2
aload 3
aload 6
invokevirtual java/lang/Boolean/booleanValue()Z
invokestatic net/hockeyapp/android/b/c(Ljava/lang/ref/WeakReference;Lnet/hockeyapp/android/i;Z)V
return
L6:
aload 3
aload 6
invokevirtual java/lang/Boolean/booleanValue()Z
invokestatic net/hockeyapp/android/b/a(Lnet/hockeyapp/android/i;Z)V
return
.limit locals 7
.limit stack 7
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/i;Z)V
aload 0
ifnull L0
aload 1
putstatic net/hockeyapp/android/b/b Ljava/lang/String;
aload 2
invokestatic net/hockeyapp/android/e/w/c(Ljava/lang/String;)Ljava/lang/String;
putstatic net/hockeyapp/android/b/a Ljava/lang/String;
aload 0
invokestatic net/hockeyapp/android/a/a(Landroid/content/Context;)V
getstatic net/hockeyapp/android/b/a Ljava/lang/String;
ifnonnull L1
getstatic net/hockeyapp/android/a/d Ljava/lang/String;
putstatic net/hockeyapp/android/b/a Ljava/lang/String;
L1:
iload 4
ifeq L0
new java/lang/ref/WeakReference
dup
aload 0
invokespecial java/lang/ref/WeakReference/<init>(Ljava/lang/Object;)V
pop
aload 3
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
invokestatic net/hockeyapp/android/b/a(Lnet/hockeyapp/android/i;Z)V
L0:
return
.limit locals 5
.limit stack 3
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/i;)V
aload 0
ldc "https://sdk.hockeyapp.net/"
aload 1
aload 2
invokestatic net/hockeyapp/android/b/a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/i;)V
return
.limit locals 3
.limit stack 4
.end method

.method private static a(Landroid/content/Context;Lnet/hockeyapp/android/i;)V
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
astore 6
new java/lang/ref/WeakReference
dup
aload 0
invokespecial java/lang/ref/WeakReference/<init>(Ljava/lang/Object;)V
astore 5
aload 5
invokestatic net/hockeyapp/android/b/a(Ljava/lang/ref/WeakReference;)I
istore 2
iload 2
iconst_1
if_icmpne L0
aload 0
instanceof android/app/Activity
ifne L1
iconst_1
istore 3
L2:
aload 0
invokestatic android/preference/PreferenceManager/getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;
astore 0
iload 3
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
aload 0
ldc "always_send_crash_reports"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ior
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
astore 4
aload 4
astore 0
aload 1
ifnull L3
aload 4
invokevirtual java/lang/Boolean/booleanValue()Z
iconst_0
ior
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
iconst_0
ior
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
astore 0
L3:
aload 0
invokevirtual java/lang/Boolean/booleanValue()Z
ifne L4
aload 6
invokevirtual java/lang/Boolean/booleanValue()Z
istore 3
aload 5
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/content/Context
astore 0
aload 0
ifnull L5
new android/app/AlertDialog$Builder
dup
aload 0
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
astore 0
aload 0
aload 1
iconst_0
invokestatic net/hockeyapp/android/aj/a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;
invokevirtual android/app/AlertDialog$Builder/setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
pop
aload 0
aload 1
iconst_1
invokestatic net/hockeyapp/android/aj/a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;
invokevirtual android/app/AlertDialog$Builder/setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
pop
aload 0
aload 1
iconst_2
invokestatic net/hockeyapp/android/aj/a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;
new net/hockeyapp/android/c
dup
aload 1
aload 5
iload 3
invokespecial net/hockeyapp/android/c/<init>(Lnet/hockeyapp/android/i;Ljava/lang/ref/WeakReference;Z)V
invokevirtual android/app/AlertDialog$Builder/setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
pop
aload 0
aload 1
iconst_3
invokestatic net/hockeyapp/android/aj/a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;
new net/hockeyapp/android/d
dup
aload 1
aload 5
iload 3
invokespecial net/hockeyapp/android/d/<init>(Lnet/hockeyapp/android/i;Ljava/lang/ref/WeakReference;Z)V
invokevirtual android/app/AlertDialog$Builder/setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
pop
aload 0
aload 1
iconst_4
invokestatic net/hockeyapp/android/aj/a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;
new net/hockeyapp/android/e
dup
aload 1
aload 5
iload 3
invokespecial net/hockeyapp/android/e/<init>(Lnet/hockeyapp/android/i;Ljava/lang/ref/WeakReference;Z)V
invokevirtual android/app/AlertDialog$Builder/setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
pop
aload 0
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
invokevirtual android/app/AlertDialog/show()V
L5:
return
L1:
iconst_0
istore 3
goto L2
L4:
aload 5
aload 1
aload 6
invokevirtual java/lang/Boolean/booleanValue()Z
invokestatic net/hockeyapp/android/b/c(Ljava/lang/ref/WeakReference;Lnet/hockeyapp/android/i;Z)V
return
L0:
iload 2
iconst_2
if_icmpne L6
aload 5
aload 1
aload 6
invokevirtual java/lang/Boolean/booleanValue()Z
invokestatic net/hockeyapp/android/b/c(Ljava/lang/ref/WeakReference;Lnet/hockeyapp/android/i;Z)V
return
L6:
aload 1
aload 6
invokevirtual java/lang/Boolean/booleanValue()Z
invokestatic net/hockeyapp/android/b/a(Lnet/hockeyapp/android/i;Z)V
return
.limit locals 7
.limit stack 7
.end method

.method private static a(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V
aload 0
ifnull L0
aload 0
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/content/Context
astore 0
aload 0
ifnull L0
aload 0
ldc "HockeySDK"
iconst_0
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
astore 0
aload 0
new java/lang/StringBuilder
dup
ldc "RETRY_COUNT: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokeinterface android/content/SharedPreferences$Editor/remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 1
pop
aload 0
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
L0:
return
.limit locals 2
.limit stack 4
.end method

.method private static a(Ljava/lang/ref/WeakReference;Ljava/lang/String;I)V
iload 2
iconst_m1
if_icmpne L0
L1:
return
L0:
aload 0
ifnull L1
aload 0
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/content/Context
astore 4
aload 4
ifnull L1
aload 4
ldc "HockeySDK"
iconst_0
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
astore 4
aload 4
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
astore 5
aload 4
new java/lang/StringBuilder
dup
ldc "RETRY_COUNT: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
iconst_0
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
istore 3
iload 3
iload 2
if_icmplt L2
aload 0
aload 1
invokestatic net/hockeyapp/android/b/b(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V
aload 0
aload 1
invokestatic net/hockeyapp/android/b/a(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V
return
L2:
aload 5
new java/lang/StringBuilder
dup
ldc "RETRY_COUNT: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
iload 3
iconst_1
iadd
invokeinterface android/content/SharedPreferences$Editor/putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor; 2
pop
aload 5
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
return
.limit locals 6
.limit stack 4
.end method

.method private static a(Ljava/lang/ref/WeakReference;Lnet/hockeyapp/android/i;)V
aload 0
aload 1
aconst_null
invokestatic net/hockeyapp/android/b/a(Ljava/lang/ref/WeakReference;Lnet/hockeyapp/android/i;Lnet/hockeyapp/android/c/b;)V
return
.limit locals 2
.limit stack 3
.end method

.method public static a(Ljava/lang/ref/WeakReference;Lnet/hockeyapp/android/i;Lnet/hockeyapp/android/c/b;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/lang/Exception from L4 to L5 using L2
.catch all from L4 to L5 using L3
.catch java/lang/Exception from L6 to L7 using L2
.catch all from L6 to L7 using L3
.catch java/lang/Exception from L8 to L9 using L2
.catch all from L8 to L9 using L3
.catch java/lang/Exception from L10 to L11 using L2
.catch all from L10 to L11 using L3
.catch java/lang/Exception from L12 to L13 using L2
.catch all from L12 to L13 using L3
.catch java/lang/Exception from L14 to L15 using L2
.catch all from L14 to L15 using L3
.catch java/lang/Exception from L16 to L17 using L2
.catch all from L16 to L17 using L3
.catch java/lang/Exception from L18 to L19 using L2
.catch all from L18 to L19 using L3
.catch java/lang/Exception from L20 to L21 using L2
.catch all from L20 to L21 using L3
.catch java/lang/Exception from L22 to L23 using L2
.catch all from L22 to L23 using L3
.catch java/lang/Exception from L24 to L25 using L2
.catch all from L24 to L25 using L3
.catch java/lang/Exception from L26 to L27 using L2
.catch all from L26 to L27 using L3
.catch java/lang/Exception from L28 to L29 using L2
.catch all from L28 to L29 using L3
.catch java/lang/Exception from L30 to L31 using L2
.catch all from L30 to L31 using L3
.catch java/lang/Exception from L32 to L33 using L2
.catch all from L32 to L33 using L3
.catch java/lang/Exception from L34 to L35 using L2
.catch all from L34 to L35 using L3
.catch java/lang/Exception from L36 to L37 using L2
.catch all from L36 to L37 using L3
.catch java/lang/Exception from L38 to L39 using L2
.catch all from L38 to L39 using L3
.catch java/lang/Exception from L40 to L41 using L2
.catch all from L40 to L41 using L3
.catch java/lang/Exception from L42 to L43 using L2
.catch all from L42 to L43 using L3
.catch java/lang/Exception from L44 to L45 using L2
.catch all from L44 to L45 using L3
.catch java/lang/Exception from L46 to L47 using L2
.catch all from L46 to L47 using L3
.catch java/lang/Exception from L48 to L49 using L2
.catch all from L48 to L49 using L3
.catch java/lang/Exception from L50 to L51 using L2
.catch all from L50 to L51 using L52
.catch java/lang/Exception from L53 to L54 using L2
.catch all from L53 to L54 using L3
.catch all from L55 to L56 using L52
invokestatic net/hockeyapp/android/b/c()[Ljava/lang/String;
astore 14
aload 14
ifnull L57
aload 14
arraylength
ifle L57
ldc "HockeyApp"
new java/lang/StringBuilder
dup
ldc "Found "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 14
arraylength
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " stacktrace(s)."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
astore 6
iconst_0
istore 3
L58:
iload 3
aload 14
arraylength
if_icmpge L57
aconst_null
astore 12
aconst_null
astore 9
aload 14
iload 3
aaload
astore 16
aload 12
astore 8
L0:
aload 0
aload 16
invokestatic net/hockeyapp/android/b/c(Ljava/lang/ref/WeakReference;Ljava/lang/String;)Ljava/lang/String;
astore 15
L1:
aload 6
astore 7
aload 12
astore 8
L4:
aload 15
invokevirtual java/lang/String/length()I
ifle L59
L5:
aload 12
astore 8
L6:
ldc "HockeyApp"
new java/lang/StringBuilder
dup
ldc "Transmitting crash data: \n"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 15
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
L7:
aload 12
astore 8
L8:
aload 0
aload 16
ldc ".stacktrace"
ldc ".user"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokestatic net/hockeyapp/android/b/c(Ljava/lang/ref/WeakReference;Ljava/lang/String;)Ljava/lang/String;
astore 7
L9:
aload 12
astore 8
L10:
aload 0
aload 16
ldc ".stacktrace"
ldc ".contact"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokestatic net/hockeyapp/android/b/c(Ljava/lang/ref/WeakReference;Ljava/lang/String;)Ljava/lang/String;
astore 9
L11:
aload 2
ifnull L60
aload 12
astore 8
L12:
aload 2
getfield net/hockeyapp/android/c/b/c Ljava/lang/String;
astore 10
L13:
aload 10
ifnull L61
aload 12
astore 8
L14:
aload 10
invokevirtual java/lang/String/length()I
ifle L61
L15:
aload 10
astore 7
L62:
aload 12
astore 8
L16:
aload 2
getfield net/hockeyapp/android/c/b/b Ljava/lang/String;
astore 13
L17:
aload 9
astore 10
aload 7
astore 11
aload 13
ifnull L63
aload 9
astore 10
aload 7
astore 11
aload 12
astore 8
L18:
aload 13
invokevirtual java/lang/String/length()I
ifle L63
L19:
aload 13
astore 10
aload 7
astore 11
L63:
aload 12
astore 8
L20:
aload 0
aload 16
ldc ".stacktrace"
ldc ".description"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokestatic net/hockeyapp/android/b/c(Ljava/lang/ref/WeakReference;Ljava/lang/String;)Ljava/lang/String;
astore 13
L21:
aload 2
ifnull L64
aload 12
astore 8
L22:
aload 2
getfield net/hockeyapp/android/c/b/a Ljava/lang/String;
astore 9
L23:
aload 9
astore 7
aload 13
ifnull L29
aload 9
astore 7
aload 12
astore 8
L24:
aload 13
invokevirtual java/lang/String/length()I
ifle L29
L25:
aload 9
ifnull L65
aload 12
astore 8
L26:
aload 9
invokevirtual java/lang/String/length()I
ifle L65
L27:
aload 12
astore 8
L28:
ldc "%s\n\nLog:\n%s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 9
aastore
dup
iconst_1
aload 13
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
astore 7
L29:
aload 12
astore 8
L30:
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 9
L31:
aload 12
astore 8
L32:
aload 9
ldc "raw"
aload 15
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L33:
aload 12
astore 8
L34:
aload 9
ldc "userID"
aload 11
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L35:
aload 12
astore 8
L36:
aload 9
ldc "contact"
aload 10
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L37:
aload 12
astore 8
L38:
aload 9
ldc "description"
aload 7
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L39:
aload 12
astore 8
L40:
aload 9
ldc "sdk"
ldc "HockeySDK"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L41:
aload 12
astore 8
L42:
aload 9
ldc "sdk_version"
ldc "3.6.0"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L43:
aload 12
astore 8
L44:
new net/hockeyapp/android/e/l
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic net/hockeyapp/android/b/b Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "api/2/apps/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic net/hockeyapp/android/b/a Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/crashes/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial net/hockeyapp/android/e/l/<init>(Ljava/lang/String;)V
astore 7
L45:
aload 12
astore 8
L46:
aload 7
ldc "POST"
putfield net/hockeyapp/android/e/l/b Ljava/lang/String;
L47:
aload 12
astore 8
L48:
aload 7
aload 9
invokevirtual net/hockeyapp/android/e/l/a(Ljava/util/Map;)Lnet/hockeyapp/android/e/l;
invokevirtual net/hockeyapp/android/e/l/a()Ljava/net/HttpURLConnection;
astore 7
L49:
aload 7
astore 8
aload 7
astore 9
L50:
aload 7
invokevirtual java/net/HttpURLConnection/getResponseCode()I
istore 4
L51:
iload 4
sipush 202
if_icmpeq L66
iload 4
sipush 201
if_icmpne L67
goto L66
L68:
iload 5
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
astore 6
aload 7
astore 9
aload 6
astore 7
L59:
aload 9
ifnull L69
aload 9
invokevirtual java/net/HttpURLConnection/disconnect()V
L69:
aload 7
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L70
ldc "HockeyApp"
ldc "Transmission succeeded"
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
aload 14
iload 3
aaload
invokestatic net/hockeyapp/android/b/b(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V
aload 7
astore 8
aload 1
ifnull L71
aload 14
iload 3
aaload
astore 8
aload 7
astore 6
aload 8
astore 7
L72:
aload 0
aload 7
invokestatic net/hockeyapp/android/b/a(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V
aload 6
astore 8
L71:
iload 3
iconst_1
iadd
istore 3
aload 8
astore 6
goto L58
L64:
ldc ""
astore 9
goto L23
L65:
aload 12
astore 8
L53:
ldc "Log:\n%s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 13
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
astore 7
L54:
goto L29
L67:
iconst_0
istore 5
goto L68
L2:
astore 7
aload 8
astore 9
L55:
aload 7
invokevirtual java/lang/Exception/printStackTrace()V
L56:
aload 8
ifnull L73
aload 8
invokevirtual java/net/HttpURLConnection/disconnect()V
L73:
aload 6
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L74
ldc "HockeyApp"
ldc "Transmission succeeded"
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
aload 14
iload 3
aaload
invokestatic net/hockeyapp/android/b/b(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V
aload 6
astore 8
aload 1
ifnull L71
aload 14
iload 3
aaload
astore 7
goto L72
L3:
astore 2
aconst_null
astore 7
L75:
aload 7
ifnull L76
aload 7
invokevirtual java/net/HttpURLConnection/disconnect()V
L76:
aload 6
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L77
ldc "HockeyApp"
ldc "Transmission succeeded"
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
aload 14
iload 3
aaload
invokestatic net/hockeyapp/android/b/b(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V
aload 1
ifnull L78
aload 0
aload 14
iload 3
aaload
invokestatic net/hockeyapp/android/b/a(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V
L78:
aload 2
athrow
L57:
return
L77:
ldc "HockeyApp"
ldc "Transmission failed, will retry on next register() call"
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 1
ifnull L78
aload 14
iload 3
aaload
astore 1
aload 0
ifnull L78
aload 0
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/content/Context
astore 6
aload 6
ifnull L78
aload 6
ldc "HockeySDK"
iconst_0
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
astore 6
aload 6
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
astore 7
aload 6
new java/lang/StringBuilder
dup
ldc "RETRY_COUNT: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
iconst_0
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
istore 3
iload 3
ifle L79
aload 0
aload 1
invokestatic net/hockeyapp/android/b/b(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V
aload 0
aload 1
invokestatic net/hockeyapp/android/b/a(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V
goto L78
L79:
aload 7
new java/lang/StringBuilder
dup
ldc "RETRY_COUNT: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
iload 3
iconst_1
iadd
invokeinterface android/content/SharedPreferences$Editor/putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor; 2
pop
aload 7
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
goto L78
L74:
ldc "HockeyApp"
ldc "Transmission failed, will retry on next register() call"
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 6
astore 8
aload 1
ifnull L71
aload 14
iload 3
aaload
astore 7
aload 6
astore 8
aload 0
ifnull L71
aload 0
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/content/Context
astore 9
aload 6
astore 8
aload 9
ifnull L71
aload 9
ldc "HockeySDK"
iconst_0
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
astore 9
aload 9
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
astore 8
aload 9
new java/lang/StringBuilder
dup
ldc "RETRY_COUNT: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 7
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
iconst_0
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
istore 4
iload 4
ifle L80
aload 0
aload 7
invokestatic net/hockeyapp/android/b/b(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V
L81:
aload 0
aload 7
invokestatic net/hockeyapp/android/b/a(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V
aload 6
astore 8
goto L71
L80:
aload 8
new java/lang/StringBuilder
dup
ldc "RETRY_COUNT: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 7
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
iload 4
iconst_1
iadd
invokeinterface android/content/SharedPreferences$Editor/putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor; 2
pop
aload 8
astore 7
L82:
aload 7
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
aload 6
astore 8
goto L71
L70:
ldc "HockeyApp"
ldc "Transmission failed, will retry on next register() call"
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 7
astore 8
aload 1
ifnull L71
aload 14
iload 3
aaload
astore 9
aload 7
astore 8
aload 0
ifnull L71
aload 0
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/content/Context
astore 6
aload 7
astore 8
aload 6
ifnull L71
aload 6
ldc "HockeySDK"
iconst_0
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
astore 6
aload 6
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
astore 8
aload 6
new java/lang/StringBuilder
dup
ldc "RETRY_COUNT: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 9
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
iconst_0
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
istore 4
iload 4
ifle L83
aload 0
aload 9
invokestatic net/hockeyapp/android/b/b(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V
aload 7
astore 6
aload 9
astore 7
goto L81
L83:
aload 8
new java/lang/StringBuilder
dup
ldc "RETRY_COUNT: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 9
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
iload 4
iconst_1
iadd
invokeinterface android/content/SharedPreferences$Editor/putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor; 2
pop
aload 7
astore 6
aload 8
astore 7
goto L82
L52:
astore 2
aload 9
astore 7
goto L75
L61:
goto L62
L60:
aload 9
astore 10
aload 7
astore 11
goto L63
L66:
iconst_1
istore 5
goto L68
.limit locals 17
.limit stack 5
.end method

.method private static a(Ljava/lang/ref/WeakReference;Lnet/hockeyapp/android/i;Z)V
aload 0
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/content/Context
astore 3
aload 3
ifnonnull L0
return
L0:
new android/app/AlertDialog$Builder
dup
aload 3
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
astore 3
aload 3
aload 1
iconst_0
invokestatic net/hockeyapp/android/aj/a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;
invokevirtual android/app/AlertDialog$Builder/setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
pop
aload 3
aload 1
iconst_1
invokestatic net/hockeyapp/android/aj/a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;
invokevirtual android/app/AlertDialog$Builder/setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
pop
aload 3
aload 1
iconst_2
invokestatic net/hockeyapp/android/aj/a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;
new net/hockeyapp/android/c
dup
aload 1
aload 0
iload 2
invokespecial net/hockeyapp/android/c/<init>(Lnet/hockeyapp/android/i;Ljava/lang/ref/WeakReference;Z)V
invokevirtual android/app/AlertDialog$Builder/setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
pop
aload 3
aload 1
iconst_3
invokestatic net/hockeyapp/android/aj/a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;
new net/hockeyapp/android/d
dup
aload 1
aload 0
iload 2
invokespecial net/hockeyapp/android/d/<init>(Lnet/hockeyapp/android/i;Ljava/lang/ref/WeakReference;Z)V
invokevirtual android/app/AlertDialog$Builder/setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
pop
aload 3
aload 1
iconst_4
invokestatic net/hockeyapp/android/aj/a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;
new net/hockeyapp/android/e
dup
aload 1
aload 0
iload 2
invokespecial net/hockeyapp/android/e/<init>(Lnet/hockeyapp/android/i;Ljava/lang/ref/WeakReference;Z)V
invokevirtual android/app/AlertDialog$Builder/setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
pop
aload 3
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
invokevirtual android/app/AlertDialog/show()V
return
.limit locals 4
.limit stack 7
.end method

.method private static a(Lnet/hockeyapp/android/i;Z)V
getstatic net/hockeyapp/android/a/b Ljava/lang/String;
ifnull L0
getstatic net/hockeyapp/android/a/d Ljava/lang/String;
ifnull L0
invokestatic java/lang/Thread/getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;
astore 2
aload 2
ifnull L1
ldc "HockeyApp"
new java/lang/StringBuilder
dup
ldc "Current handler class = "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
L1:
aload 2
instanceof net/hockeyapp/android/j
ifeq L2
aload 2
checkcast net/hockeyapp/android/j
aload 0
putfield net/hockeyapp/android/j/a Lnet/hockeyapp/android/i;
return
L2:
new net/hockeyapp/android/j
dup
aload 2
aload 0
iload 1
invokespecial net/hockeyapp/android/j/<init>(Ljava/lang/Thread$UncaughtExceptionHandler;Lnet/hockeyapp/android/i;Z)V
invokestatic java/lang/Thread/setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V
return
L0:
ldc "HockeyApp"
ldc "Exception handler not set because version or package is null."
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
return
.limit locals 3
.limit stack 5
.end method

.method static synthetic a()Z
iconst_0
putstatic net/hockeyapp/android/b/c Z
iconst_0
ireturn
.limit locals 0
.limit stack 1
.end method

.method public static a(Lnet/hockeyapp/android/c/a;Lnet/hockeyapp/android/i;Ljava/lang/ref/WeakReference;Z)Z
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
iconst_0
istore 4
getstatic net/hockeyapp/android/h/a [I
aload 0
invokevirtual net/hockeyapp/android/c/a/ordinal()I
iaload
tableswitch 1
L5
L6
L7
default : L8
L8:
iconst_0
ireturn
L5:
invokestatic net/hockeyapp/android/b/c()[Ljava/lang/String;
astore 0
aload 0
ifnull L9
aload 0
arraylength
ifle L9
ldc "HockeyApp"
new java/lang/StringBuilder
dup
ldc "Found "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
arraylength
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " stacktrace(s)."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
L10:
iload 4
aload 0
arraylength
if_icmpge L9
aload 2
ifnull L4
L0:
ldc "HockeyApp"
new java/lang/StringBuilder
dup
ldc "Delete stacktrace "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
iload 4
aaload
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 2
aload 0
iload 4
aaload
invokestatic net/hockeyapp/android/b/b(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V
aload 2
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/content/Context
astore 5
L1:
aload 5
ifnull L4
L3:
aload 5
aload 0
iload 4
aaload
invokevirtual android/content/Context/deleteFile(Ljava/lang/String;)Z
pop
L4:
iload 4
iconst_1
iadd
istore 4
goto L10
L2:
astore 5
aload 5
invokevirtual java/lang/Exception/printStackTrace()V
goto L4
L9:
aload 1
iload 3
invokestatic net/hockeyapp/android/b/a(Lnet/hockeyapp/android/i;Z)V
iconst_1
ireturn
L6:
aconst_null
astore 0
aload 2
ifnull L11
aload 2
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/content/Context
astore 0
L11:
aload 0
ifnull L8
aload 0
invokestatic android/preference/PreferenceManager/getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "always_send_crash_reports"
iconst_1
invokeinterface android/content/SharedPreferences$Editor/putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
aload 2
aload 1
iload 3
invokestatic net/hockeyapp/android/b/c(Ljava/lang/ref/WeakReference;Lnet/hockeyapp/android/i;Z)V
iconst_1
ireturn
L7:
aload 2
aload 1
iload 3
invokestatic net/hockeyapp/android/b/c(Ljava/lang/ref/WeakReference;Lnet/hockeyapp/android/i;Z)V
iconst_1
ireturn
.limit locals 6
.limit stack 4
.end method

.method private static b()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic net/hockeyapp/android/b/b Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "api/2/apps/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic net/hockeyapp/android/b/a Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/crashes/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 0
.limit stack 2
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/i;)V
aload 0
aload 1
aload 2
aload 3
iconst_1
invokestatic net/hockeyapp/android/b/a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/i;Z)V
return
.limit locals 4
.limit stack 5
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/i;)V
aload 0
ldc "https://sdk.hockeyapp.net/"
aload 1
aload 2
iconst_1
invokestatic net/hockeyapp/android/b/a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/i;Z)V
return
.limit locals 3
.limit stack 5
.end method

.method private static b(Ljava/lang/ref/WeakReference;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
invokestatic net/hockeyapp/android/b/c()[Ljava/lang/String;
astore 2
aload 2
ifnull L5
aload 2
arraylength
ifle L5
ldc "HockeyApp"
new java/lang/StringBuilder
dup
ldc "Found "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
arraylength
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc " stacktrace(s)."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
iconst_0
istore 1
L6:
iload 1
aload 2
arraylength
if_icmpge L5
aload 0
ifnull L4
L0:
ldc "HockeyApp"
new java/lang/StringBuilder
dup
ldc "Delete stacktrace "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
iload 1
aaload
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
aload 2
iload 1
aaload
invokestatic net/hockeyapp/android/b/b(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V
aload 0
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/content/Context
astore 3
L1:
aload 3
ifnull L4
L3:
aload 3
aload 2
iload 1
aaload
invokevirtual android/content/Context/deleteFile(Ljava/lang/String;)Z
pop
L4:
iload 1
iconst_1
iadd
istore 1
goto L6
L2:
astore 3
aload 3
invokevirtual java/lang/Exception/printStackTrace()V
goto L4
L5:
return
.limit locals 4
.limit stack 4
.end method

.method private static b(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V
aload 0
ifnull L0
aload 0
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/content/Context
astore 0
aload 0
ifnull L0
aload 0
aload 1
invokevirtual android/content/Context/deleteFile(Ljava/lang/String;)Z
pop
aload 0
aload 1
ldc ".stacktrace"
ldc ".user"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual android/content/Context/deleteFile(Ljava/lang/String;)Z
pop
aload 0
aload 1
ldc ".stacktrace"
ldc ".contact"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual android/content/Context/deleteFile(Ljava/lang/String;)Z
pop
aload 0
aload 1
ldc ".stacktrace"
ldc ".description"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual android/content/Context/deleteFile(Ljava/lang/String;)Z
pop
L0:
return
.limit locals 2
.limit stack 4
.end method

.method private static b(Ljava/lang/ref/WeakReference;Lnet/hockeyapp/android/i;Z)V
aload 0
aload 1
iload 2
invokestatic net/hockeyapp/android/b/c(Ljava/lang/ref/WeakReference;Lnet/hockeyapp/android/i;Z)V
return
.limit locals 3
.limit stack 3
.end method

.method private static c(Ljava/lang/ref/WeakReference;Ljava/lang/String;)Ljava/lang/String;
.catch java/io/FileNotFoundException from L0 to L1 using L2
.catch java/io/IOException from L0 to L1 using L3
.catch all from L0 to L1 using L4
.catch java/io/FileNotFoundException from L5 to L6 using L7
.catch java/io/IOException from L5 to L6 using L8
.catch all from L5 to L6 using L9
.catch java/io/FileNotFoundException from L10 to L11 using L7
.catch java/io/IOException from L10 to L11 using L8
.catch all from L10 to L11 using L9
.catch java/io/FileNotFoundException from L12 to L13 using L7
.catch java/io/IOException from L12 to L13 using L8
.catch all from L12 to L13 using L9
.catch java/io/IOException from L14 to L15 using L16
.catch all from L17 to L18 using L9
.catch java/io/IOException from L19 to L20 using L16
.catch java/io/IOException from L21 to L22 using L23
.catch java/io/IOException from L24 to L25 using L16
aconst_null
astore 2
aload 0
ifnull L26
aload 0
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/content/Context
astore 0
aload 0
ifnull L26
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 3
L0:
new java/io/BufferedReader
dup
new java/io/InputStreamReader
dup
aload 0
aload 1
invokevirtual android/content/Context/openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;
invokespecial java/io/InputStreamReader/<init>(Ljava/io/InputStream;)V
invokespecial java/io/BufferedReader/<init>(Ljava/io/Reader;)V
astore 0
L1:
aload 0
astore 1
L5:
aload 0
invokevirtual java/io/BufferedReader/readLine()Ljava/lang/String;
astore 2
L6:
aload 2
ifnull L27
aload 0
astore 1
L10:
aload 3
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L11:
aload 0
astore 1
L12:
aload 3
ldc "line.separator"
invokestatic java/lang/System/getProperty(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L13:
goto L1
L7:
astore 1
L28:
aload 0
ifnull L15
L14:
aload 0
invokevirtual java/io/BufferedReader/close()V
L15:
aload 3
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L3:
astore 2
aconst_null
astore 0
L29:
aload 0
astore 1
L17:
aload 2
invokevirtual java/io/IOException/printStackTrace()V
L18:
aload 0
ifnull L15
L19:
aload 0
invokevirtual java/io/BufferedReader/close()V
L20:
goto L15
L16:
astore 0
goto L15
L4:
astore 0
aconst_null
astore 1
L30:
aload 1
ifnull L22
L21:
aload 1
invokevirtual java/io/BufferedReader/close()V
L22:
aload 0
athrow
L26:
aconst_null
areturn
L23:
astore 1
goto L22
L27:
aload 0
ifnull L15
L24:
aload 0
invokevirtual java/io/BufferedReader/close()V
L25:
goto L15
L9:
astore 0
goto L30
L8:
astore 2
goto L29
L2:
astore 0
aload 2
astore 0
goto L28
.limit locals 4
.limit stack 6
.end method

.method private static c(Ljava/lang/ref/WeakReference;)V
aload 0
ifnull L0
aload 0
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/content/Context
astore 0
aload 0
ifnull L0
aload 0
invokestatic android/preference/PreferenceManager/getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "always_send_crash_reports"
invokeinterface android/content/SharedPreferences$Editor/remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 1
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
L0:
return
.limit locals 1
.limit stack 2
.end method

.method private static c(Ljava/lang/ref/WeakReference;Lnet/hockeyapp/android/i;Z)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L2
aload 0
ifnull L6
aload 0
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/content/Context
astore 5
aload 5
ifnull L6
L0:
invokestatic net/hockeyapp/android/b/c()[Ljava/lang/String;
astore 4
aload 5
ldc "HockeySDK"
iconst_0
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
astore 5
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 6
L1:
iconst_0
istore 3
L3:
iload 3
aload 4
arraylength
if_icmpge L5
aload 6
aload 4
iload 3
aaload
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
iload 3
aload 4
arraylength
iconst_1
isub
if_icmpge L7
aload 6
ldc "|"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L4:
goto L7
L5:
aload 5
ldc "ConfirmedFilenames"
aload 6
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokeinterface android/content/SharedPreferences$Editor/putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 2
pop
aload 5
invokestatic net/hockeyapp/android/e/n/a(Landroid/content/SharedPreferences$Editor;)V
L6:
aload 1
iload 2
invokestatic net/hockeyapp/android/b/a(Lnet/hockeyapp/android/i;Z)V
getstatic net/hockeyapp/android/b/c Z
ifne L8
iconst_1
putstatic net/hockeyapp/android/b/c Z
new net/hockeyapp/android/f
dup
aload 0
aload 1
invokespecial net/hockeyapp/android/f/<init>(Ljava/lang/ref/WeakReference;Lnet/hockeyapp/android/i;)V
invokevirtual net/hockeyapp/android/f/start()V
L8:
return
L2:
astore 4
goto L6
L7:
iload 3
iconst_1
iadd
istore 3
goto L3
.limit locals 7
.limit stack 4
.end method

.method private static c()[Ljava/lang/String;
getstatic net/hockeyapp/android/a/a Ljava/lang/String;
ifnull L0
ldc "HockeyApp"
new java/lang/StringBuilder
dup
ldc "Looking for exceptions in: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
getstatic net/hockeyapp/android/a/a Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic net/hockeyapp/android/a/a Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
aload 0
invokevirtual java/io/File/mkdir()Z
ifne L1
aload 0
invokevirtual java/io/File/exists()Z
ifne L1
iconst_0
anewarray java/lang/String
areturn
L1:
aload 0
new net/hockeyapp/android/g
dup
invokespecial net/hockeyapp/android/g/<init>()V
invokevirtual java/io/File/list(Ljava/io/FilenameFilter;)[Ljava/lang/String;
areturn
L0:
ldc "HockeyApp"
ldc "Can't search for exception as file path is null."
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
aconst_null
areturn
.limit locals 1
.limit stack 4
.end method

.method private static d(Ljava/lang/ref/WeakReference;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L2
aload 0
ifnull L6
aload 0
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/content/Context
astore 2
aload 2
ifnull L6
L0:
invokestatic net/hockeyapp/android/b/c()[Ljava/lang/String;
astore 0
aload 2
ldc "HockeySDK"
iconst_0
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
astore 2
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 3
L1:
iconst_0
istore 1
L3:
iload 1
aload 0
arraylength
if_icmpge L5
aload 3
aload 0
iload 1
aaload
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
iload 1
aload 0
arraylength
iconst_1
isub
if_icmpge L7
aload 3
ldc "|"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L4:
goto L7
L5:
aload 2
ldc "ConfirmedFilenames"
aload 3
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokeinterface android/content/SharedPreferences$Editor/putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 2
pop
aload 2
invokestatic net/hockeyapp/android/e/n/a(Landroid/content/SharedPreferences$Editor;)V
L6:
return
L2:
astore 0
return
L7:
iload 1
iconst_1
iadd
istore 1
goto L3
.limit locals 4
.limit stack 3
.end method
