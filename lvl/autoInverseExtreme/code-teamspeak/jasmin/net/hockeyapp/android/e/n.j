.bytecode 50.0
.class public final synchronized net/hockeyapp/android/e/n
.super java/lang/Object

.field public 'a' Landroid/content/SharedPreferences;

.field public 'b' Landroid/content/SharedPreferences$Editor;

.field private 'c' Landroid/content/SharedPreferences;

.field private 'd' Landroid/content/SharedPreferences$Editor;

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method synthetic <init>(B)V
aload 0
invokespecial net/hockeyapp/android/e/n/<init>()V
return
.limit locals 2
.limit stack 1
.end method

.method private static a()Lnet/hockeyapp/android/e/n;
getstatic net/hockeyapp/android/e/p/a Lnet/hockeyapp/android/e/n;
areturn
.limit locals 0
.limit stack 1
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
aload 1
ifnull L0
aload 0
aload 1
ldc "net.hockeyapp.android.prefs_name_email"
iconst_0
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
putfield net/hockeyapp/android/e/n/a Landroid/content/SharedPreferences;
aload 0
getfield net/hockeyapp/android/e/n/a Landroid/content/SharedPreferences;
ifnull L0
aload 0
aload 0
getfield net/hockeyapp/android/e/n/a Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
putfield net/hockeyapp/android/e/n/b Landroid/content/SharedPreferences$Editor;
aload 2
ifnull L1
aload 3
ifnull L1
aload 4
ifnonnull L2
L1:
aload 0
getfield net/hockeyapp/android/e/n/b Landroid/content/SharedPreferences$Editor;
ldc "net.hockeyapp.android.prefs_key_name_email"
aconst_null
invokeinterface android/content/SharedPreferences$Editor/putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 2
pop
L3:
aload 0
getfield net/hockeyapp/android/e/n/b Landroid/content/SharedPreferences$Editor;
invokestatic net/hockeyapp/android/e/n/a(Landroid/content/SharedPreferences$Editor;)V
L0:
return
L2:
aload 0
getfield net/hockeyapp/android/e/n/b Landroid/content/SharedPreferences$Editor;
ldc "net.hockeyapp.android.prefs_key_name_email"
ldc "%s|%s|%s"
iconst_3
anewarray java/lang/Object
dup
iconst_0
aload 2
aastore
dup
iconst_1
aload 3
aastore
dup
iconst_2
aload 4
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface android/content/SharedPreferences$Editor/putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 2
pop
goto L3
.limit locals 5
.limit stack 7
.end method

.method public static a(Landroid/content/SharedPreferences$Editor;)V
invokestatic net/hockeyapp/android/e/n/b()Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L0
aload 0
invokeinterface android/content/SharedPreferences$Editor/apply()V 0
return
L0:
aload 0
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
return
.limit locals 1
.limit stack 1
.end method

.method private static b()Ljava/lang/Boolean;
.catch java/lang/NoClassDefFoundError from L0 to L1 using L2
L0:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 9
if_icmplt L3
L1:
iconst_1
istore 0
L4:
iload 0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
areturn
L3:
iconst_0
istore 0
goto L4
L2:
astore 1
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
areturn
.limit locals 2
.limit stack 2
.end method

.method private b(Landroid/content/Context;)Ljava/lang/String;
aload 1
ifnonnull L0
L1:
aconst_null
areturn
L0:
aload 0
aload 1
ldc "net.hockeyapp.android.prefs_name_email"
iconst_0
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
putfield net/hockeyapp/android/e/n/a Landroid/content/SharedPreferences;
aload 0
getfield net/hockeyapp/android/e/n/a Landroid/content/SharedPreferences;
ifnull L1
aload 0
getfield net/hockeyapp/android/e/n/a Landroid/content/SharedPreferences;
ldc "net.hockeyapp.android.prefs_key_name_email"
aconst_null
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
areturn
.limit locals 2
.limit stack 4
.end method

.method public final a(Landroid/content/Context;)Ljava/lang/String;
aload 1
ifnonnull L0
L1:
aconst_null
areturn
L0:
aload 0
aload 1
ldc "net.hockeyapp.android.prefs_feedback_token"
iconst_0
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
putfield net/hockeyapp/android/e/n/c Landroid/content/SharedPreferences;
aload 0
getfield net/hockeyapp/android/e/n/c Landroid/content/SharedPreferences;
ifnull L1
aload 0
getfield net/hockeyapp/android/e/n/c Landroid/content/SharedPreferences;
ldc "net.hockeyapp.android.prefs_key_feedback_token"
aconst_null
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
areturn
.limit locals 2
.limit stack 4
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;)V
aload 1
ifnull L0
aload 0
aload 1
ldc "net.hockeyapp.android.prefs_feedback_token"
iconst_0
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
putfield net/hockeyapp/android/e/n/c Landroid/content/SharedPreferences;
aload 0
getfield net/hockeyapp/android/e/n/c Landroid/content/SharedPreferences;
ifnull L0
aload 0
aload 0
getfield net/hockeyapp/android/e/n/c Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
putfield net/hockeyapp/android/e/n/d Landroid/content/SharedPreferences$Editor;
aload 0
getfield net/hockeyapp/android/e/n/d Landroid/content/SharedPreferences$Editor;
ldc "net.hockeyapp.android.prefs_key_feedback_token"
aload 2
invokeinterface android/content/SharedPreferences$Editor/putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 2
pop
aload 0
getfield net/hockeyapp/android/e/n/d Landroid/content/SharedPreferences$Editor;
invokestatic net/hockeyapp/android/e/n/a(Landroid/content/SharedPreferences$Editor;)V
L0:
return
.limit locals 3
.limit stack 4
.end method
