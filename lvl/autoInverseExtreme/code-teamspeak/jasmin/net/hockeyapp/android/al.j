.bytecode 50.0
.class public synchronized net/hockeyapp/android/al
.super android/app/Activity
.implements android/view/View$OnClickListener
.implements net/hockeyapp/android/as
.implements net/hockeyapp/android/ax

.field protected 'a' Lnet/hockeyapp/android/d/l;

.field protected 'b' Lnet/hockeyapp/android/e/y;

.field private final 'c' I

.field private 'd' Lnet/hockeyapp/android/c/c;

.field private 'e' Landroid/content/Context;

.method public <init>()V
aload 0
invokespecial android/app/Activity/<init>()V
aload 0
iconst_0
putfield net/hockeyapp/android/al/c I
return
.limit locals 1
.limit stack 2
.end method

.method static synthetic a(Lnet/hockeyapp/android/al;)Lnet/hockeyapp/android/c/c;
aload 0
aconst_null
putfield net/hockeyapp/android/al/d Lnet/hockeyapp/android/c/c;
aconst_null
areturn
.limit locals 1
.limit stack 2
.end method

.method private a(Ljava/lang/String;)V
aload 0
new net/hockeyapp/android/d/l
dup
aload 0
aload 1
new net/hockeyapp/android/ao
dup
aload 0
invokespecial net/hockeyapp/android/ao/<init>(Lnet/hockeyapp/android/al;)V
invokespecial net/hockeyapp/android/d/l/<init>(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/b/a;)V
putfield net/hockeyapp/android/al/a Lnet/hockeyapp/android/d/l;
aload 0
getfield net/hockeyapp/android/al/a Lnet/hockeyapp/android/d/l;
invokestatic net/hockeyapp/android/e/a/a(Landroid/os/AsyncTask;)V
return
.limit locals 2
.limit stack 8
.end method

.method private a(Ljava/lang/String;Lnet/hockeyapp/android/b/a;)V
aload 0
new net/hockeyapp/android/d/l
dup
aload 0
aload 1
aload 2
invokespecial net/hockeyapp/android/d/l/<init>(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/b/a;)V
putfield net/hockeyapp/android/al/a Lnet/hockeyapp/android/d/l;
return
.limit locals 3
.limit stack 6
.end method

.method private static a(Landroid/content/Context;)Z
aload 0
ldc "android.permission.WRITE_EXTERNAL_STORAGE"
invokevirtual android/content/Context/checkCallingOrSelfPermission(Ljava/lang/String;)I
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private e()V
aload 0
sipush 4098
invokevirtual net/hockeyapp/android/al/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
aload 0
invokespecial net/hockeyapp/android/al/h()Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
sipush 4099
invokevirtual net/hockeyapp/android/al/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
astore 4
new java/lang/StringBuilder
dup
ldc "Version "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield net/hockeyapp/android/al/b Lnet/hockeyapp/android/e/y;
invokevirtual net/hockeyapp/android/e/y/a()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 5
aload 0
getfield net/hockeyapp/android/al/b Lnet/hockeyapp/android/e/y;
invokevirtual net/hockeyapp/android/e/y/b()Ljava/lang/String;
astore 6
ldc "Unknown size"
astore 3
aload 0
getfield net/hockeyapp/android/al/b Lnet/hockeyapp/android/e/y;
invokevirtual net/hockeyapp/android/e/y/c()J
lstore 1
lload 1
lconst_0
lcmp
iflt L0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "%.2f"
iconst_1
anewarray java/lang/Object
dup
iconst_0
lload 1
l2f
ldc_w 1048576.0F
fdiv
invokestatic java/lang/Float/valueOf(F)Ljava/lang/Float;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " MB"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 3
L1:
aload 4
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " - "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
sipush 4100
invokevirtual net/hockeyapp/android/al/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
aload 0
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
sipush 4101
invokevirtual net/hockeyapp/android/al/findViewById(I)Landroid/view/View;
checkcast android/webkit/WebView
astore 3
aload 3
iconst_1
invokevirtual android/webkit/WebView/clearCache(Z)V
aload 3
invokevirtual android/webkit/WebView/destroyDrawingCache()V
aload 3
ldc "https://sdk.hockeyapp.net/"
aload 0
getfield net/hockeyapp/android/al/b Lnet/hockeyapp/android/e/y;
invokevirtual net/hockeyapp/android/e/y/d()Ljava/lang/String;
ldc "text/html"
ldc "utf-8"
aconst_null
invokevirtual android/webkit/WebView/loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
return
L0:
new net/hockeyapp/android/d/o
dup
aload 0
aload 0
invokevirtual net/hockeyapp/android/al/getIntent()Landroid/content/Intent;
ldc "url"
invokevirtual android/content/Intent/getStringExtra(Ljava/lang/String;)Ljava/lang/String;
new net/hockeyapp/android/am
dup
aload 0
aload 4
aload 5
aload 6
invokespecial net/hockeyapp/android/am/<init>(Lnet/hockeyapp/android/al;Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V
invokespecial net/hockeyapp/android/d/o/<init>(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/b/a;)V
invokestatic net/hockeyapp/android/e/a/a(Landroid/os/AsyncTask;)V
goto L1
.limit locals 7
.limit stack 10
.end method

.method private f()Ljava/lang/String;
aload 0
getfield net/hockeyapp/android/al/b Lnet/hockeyapp/android/e/y;
invokevirtual net/hockeyapp/android/e/y/d()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private g()Landroid/view/ViewGroup;
new net/hockeyapp/android/f/m
dup
aload 0
invokespecial net/hockeyapp/android/f/m/<init>(Landroid/content/Context;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private h()Ljava/lang/String;
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
L0:
aload 0
invokevirtual net/hockeyapp/android/al/getPackageManager()Landroid/content/pm/PackageManager;
astore 1
aload 1
aload 1
aload 0
invokevirtual net/hockeyapp/android/al/getPackageName()Ljava/lang/String;
iconst_0
invokevirtual android/content/pm/PackageManager/getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
invokevirtual android/content/pm/PackageManager/getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
invokeinterface java/lang/CharSequence/toString()Ljava/lang/String; 0
astore 1
L1:
aload 1
areturn
L2:
astore 1
ldc ""
areturn
.limit locals 2
.limit stack 4
.end method

.method private i()Z
.catch android/provider/Settings$SettingNotFoundException from L0 to L1 using L2
.catch android/provider/Settings$SettingNotFoundException from L3 to L4 using L2
L0:
getstatic android/os/Build$VERSION/SDK_INT I
bipush 17
if_icmplt L3
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmpge L3
aload 0
invokevirtual net/hockeyapp/android/al/getContentResolver()Landroid/content/ContentResolver;
ldc "install_non_market_apps"
invokestatic android/provider/Settings$Global/getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
iconst_1
if_icmpne L5
L1:
iconst_1
ireturn
L3:
aload 0
invokevirtual net/hockeyapp/android/al/getContentResolver()Landroid/content/ContentResolver;
ldc "install_non_market_apps"
invokestatic android/provider/Settings$Secure/getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
istore 1
L4:
iload 1
iconst_1
if_icmpeq L6
iconst_0
ireturn
L2:
astore 2
L6:
iconst_1
ireturn
L5:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method protected final a()V
aload 0
new net/hockeyapp/android/d/l
dup
aload 0
aload 0
invokevirtual net/hockeyapp/android/al/getIntent()Landroid/content/Intent;
ldc "url"
invokevirtual android/content/Intent/getStringExtra(Ljava/lang/String;)Ljava/lang/String;
new net/hockeyapp/android/ao
dup
aload 0
invokespecial net/hockeyapp/android/ao/<init>(Lnet/hockeyapp/android/al;)V
invokespecial net/hockeyapp/android/d/l/<init>(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/b/a;)V
putfield net/hockeyapp/android/al/a Lnet/hockeyapp/android/d/l;
aload 0
getfield net/hockeyapp/android/al/a Lnet/hockeyapp/android/d/l;
invokestatic net/hockeyapp/android/e/a/a(Landroid/os/AsyncTask;)V
return
.limit locals 1
.limit stack 8
.end method

.method public final b()V
aload 0
sipush 4100
invokevirtual net/hockeyapp/android/al/findViewById(I)Landroid/view/View;
iconst_1
invokevirtual android/view/View/setEnabled(Z)V
return
.limit locals 1
.limit stack 2
.end method

.method protected final c()V
aload 0
getfield net/hockeyapp/android/al/e Landroid/content/Context;
ldc "android.permission.WRITE_EXTERNAL_STORAGE"
invokevirtual android/content/Context/checkCallingOrSelfPermission(Ljava/lang/String;)I
ifne L0
iconst_1
istore 1
L1:
iload 1
ifne L2
getstatic android/os/Build$VERSION/SDK_INT I
bipush 23
if_icmplt L3
aload 0
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "android.permission.WRITE_EXTERNAL_STORAGE"
aastore
iconst_1
invokevirtual net/hockeyapp/android/al/requestPermissions([Ljava/lang/String;I)V
return
L0:
iconst_0
istore 1
goto L1
L3:
aload 0
new net/hockeyapp/android/c/c
dup
invokespecial net/hockeyapp/android/c/c/<init>()V
putfield net/hockeyapp/android/al/d Lnet/hockeyapp/android/c/c;
aload 0
getfield net/hockeyapp/android/al/d Lnet/hockeyapp/android/c/c;
ldc "The permission to access the external storage permission is not set. Please contact the developer."
putfield net/hockeyapp/android/c/c/a Ljava/lang/String;
aload 0
new net/hockeyapp/android/ap
dup
aload 0
invokespecial net/hockeyapp/android/ap/<init>(Lnet/hockeyapp/android/al;)V
invokevirtual net/hockeyapp/android/al/runOnUiThread(Ljava/lang/Runnable;)V
return
L2:
aload 0
invokespecial net/hockeyapp/android/al/i()Z
ifne L4
aload 0
new net/hockeyapp/android/c/c
dup
invokespecial net/hockeyapp/android/c/c/<init>()V
putfield net/hockeyapp/android/al/d Lnet/hockeyapp/android/c/c;
aload 0
getfield net/hockeyapp/android/al/d Lnet/hockeyapp/android/c/c;
ldc "The installation from unknown sources is not enabled. Please check the device settings."
putfield net/hockeyapp/android/c/c/a Ljava/lang/String;
aload 0
new net/hockeyapp/android/aq
dup
aload 0
invokespecial net/hockeyapp/android/aq/<init>(Lnet/hockeyapp/android/al;)V
invokevirtual net/hockeyapp/android/al/runOnUiThread(Ljava/lang/Runnable;)V
return
L4:
aload 0
invokevirtual net/hockeyapp/android/al/a()V
return
.limit locals 2
.limit stack 5
.end method

.method public final synthetic d()Landroid/view/View;
aload 0
invokespecial net/hockeyapp/android/al/g()Landroid/view/ViewGroup;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getCurrentVersionCode()I
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
L0:
aload 0
invokevirtual net/hockeyapp/android/al/getPackageManager()Landroid/content/pm/PackageManager;
aload 0
invokevirtual net/hockeyapp/android/al/getPackageName()Ljava/lang/String;
sipush 128
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
getfield android/content/pm/PackageInfo/versionCode I
istore 1
L1:
iload 1
ireturn
L2:
astore 2
iconst_m1
ireturn
.limit locals 3
.limit stack 3
.end method

.method public onClick(Landroid/view/View;)V
aload 0
invokevirtual net/hockeyapp/android/al/c()V
aload 1
iconst_0
invokevirtual android/view/View/setEnabled(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method public onCreate(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/app/Activity/onCreate(Landroid/os/Bundle;)V
aload 0
ldc "App Update"
invokevirtual net/hockeyapp/android/al/setTitle(Ljava/lang/CharSequence;)V
aload 0
aload 0
invokespecial net/hockeyapp/android/al/g()Landroid/view/ViewGroup;
invokevirtual net/hockeyapp/android/al/setContentView(Landroid/view/View;)V
aload 0
aload 0
putfield net/hockeyapp/android/al/e Landroid/content/Context;
aload 0
new net/hockeyapp/android/e/y
dup
aload 0
aload 0
invokevirtual net/hockeyapp/android/al/getIntent()Landroid/content/Intent;
ldc "json"
invokevirtual android/content/Intent/getStringExtra(Ljava/lang/String;)Ljava/lang/String;
aload 0
invokespecial net/hockeyapp/android/e/y/<init>(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/ax;)V
putfield net/hockeyapp/android/al/b Lnet/hockeyapp/android/e/y;
aload 0
sipush 4098
invokevirtual net/hockeyapp/android/al/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
aload 0
invokespecial net/hockeyapp/android/al/h()Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
sipush 4099
invokevirtual net/hockeyapp/android/al/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
astore 4
new java/lang/StringBuilder
dup
ldc "Version "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield net/hockeyapp/android/al/b Lnet/hockeyapp/android/e/y;
invokevirtual net/hockeyapp/android/e/y/a()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 5
aload 0
getfield net/hockeyapp/android/al/b Lnet/hockeyapp/android/e/y;
invokevirtual net/hockeyapp/android/e/y/b()Ljava/lang/String;
astore 6
ldc "Unknown size"
astore 1
aload 0
getfield net/hockeyapp/android/al/b Lnet/hockeyapp/android/e/y;
invokevirtual net/hockeyapp/android/e/y/c()J
lstore 2
lload 2
lconst_0
lcmp
iflt L0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "%.2f"
iconst_1
anewarray java/lang/Object
dup
iconst_0
lload 2
l2f
ldc_w 1048576.0F
fdiv
invokestatic java/lang/Float/valueOf(F)Ljava/lang/Float;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " MB"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
L1:
aload 4
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " - "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
sipush 4100
invokevirtual net/hockeyapp/android/al/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
aload 0
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
sipush 4101
invokevirtual net/hockeyapp/android/al/findViewById(I)Landroid/view/View;
checkcast android/webkit/WebView
astore 1
aload 1
iconst_1
invokevirtual android/webkit/WebView/clearCache(Z)V
aload 1
invokevirtual android/webkit/WebView/destroyDrawingCache()V
aload 1
ldc "https://sdk.hockeyapp.net/"
aload 0
getfield net/hockeyapp/android/al/b Lnet/hockeyapp/android/e/y;
invokevirtual net/hockeyapp/android/e/y/d()Ljava/lang/String;
ldc "text/html"
ldc "utf-8"
aconst_null
invokevirtual android/webkit/WebView/loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
aload 0
aload 0
invokevirtual net/hockeyapp/android/al/getLastNonConfigurationInstance()Ljava/lang/Object;
checkcast net/hockeyapp/android/d/l
putfield net/hockeyapp/android/al/a Lnet/hockeyapp/android/d/l;
aload 0
getfield net/hockeyapp/android/al/a Lnet/hockeyapp/android/d/l;
ifnull L2
aload 0
getfield net/hockeyapp/android/al/a Lnet/hockeyapp/android/d/l;
aload 0
invokevirtual net/hockeyapp/android/d/l/a(Landroid/content/Context;)V
L2:
return
L0:
new net/hockeyapp/android/d/o
dup
aload 0
aload 0
invokevirtual net/hockeyapp/android/al/getIntent()Landroid/content/Intent;
ldc "url"
invokevirtual android/content/Intent/getStringExtra(Ljava/lang/String;)Ljava/lang/String;
new net/hockeyapp/android/am
dup
aload 0
aload 4
aload 5
aload 6
invokespecial net/hockeyapp/android/am/<init>(Lnet/hockeyapp/android/al;Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V
invokespecial net/hockeyapp/android/d/o/<init>(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/b/a;)V
invokestatic net/hockeyapp/android/e/a/a(Landroid/os/AsyncTask;)V
goto L1
.limit locals 7
.limit stack 10
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
aload 0
iload 1
aconst_null
invokevirtual net/hockeyapp/android/al/onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
areturn
.limit locals 2
.limit stack 3
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
iload 1
tableswitch 0
L0
default : L1
L1:
aconst_null
areturn
L0:
new android/app/AlertDialog$Builder
dup
aload 0
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
ldc "An error has occured"
invokevirtual android/app/AlertDialog$Builder/setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
iconst_0
invokevirtual android/app/AlertDialog$Builder/setCancelable(Z)Landroid/app/AlertDialog$Builder;
ldc "Error"
invokevirtual android/app/AlertDialog$Builder/setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
ldc_w 17301543
invokevirtual android/app/AlertDialog$Builder/setIcon(I)Landroid/app/AlertDialog$Builder;
ldc "OK"
new net/hockeyapp/android/ar
dup
aload 0
invokespecial net/hockeyapp/android/ar/<init>(Lnet/hockeyapp/android/al;)V
invokevirtual android/app/AlertDialog$Builder/setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
areturn
.limit locals 3
.limit stack 5
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
iload 1
tableswitch 0
L0
default : L1
L1:
return
L0:
aload 2
checkcast android/app/AlertDialog
astore 2
aload 0
getfield net/hockeyapp/android/al/d Lnet/hockeyapp/android/c/c;
ifnull L2
aload 2
aload 0
getfield net/hockeyapp/android/al/d Lnet/hockeyapp/android/c/c;
getfield net/hockeyapp/android/c/c/a Ljava/lang/String;
invokevirtual android/app/AlertDialog/setMessage(Ljava/lang/CharSequence;)V
return
L2:
aload 2
ldc "An unknown error has occured."
invokevirtual android/app/AlertDialog/setMessage(Ljava/lang/CharSequence;)V
return
.limit locals 3
.limit stack 2
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
aload 0
invokevirtual net/hockeyapp/android/al/b()V
aload 2
arraylength
ifeq L0
aload 3
arraylength
ifne L1
L0:
return
L1:
iload 1
iconst_1
if_icmpne L0
aload 3
iconst_0
iaload
ifne L2
aload 0
invokevirtual net/hockeyapp/android/al/c()V
return
L2:
ldc "HockeyApp"
ldc "User denied write permission, can't continue with updater task."
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
invokestatic net/hockeyapp/android/ay/a()Lnet/hockeyapp/android/az;
ifnonnull L0
new android/app/AlertDialog$Builder
dup
aload 0
getfield net/hockeyapp/android/al/e Landroid/content/Context;
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
sipush 1792
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/app/AlertDialog$Builder/setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
sipush 1793
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/app/AlertDialog$Builder/setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
sipush 1794
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
aconst_null
invokevirtual android/app/AlertDialog$Builder/setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
sipush 1795
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
new net/hockeyapp/android/an
dup
aload 0
aload 0
invokespecial net/hockeyapp/android/an/<init>(Lnet/hockeyapp/android/al;Lnet/hockeyapp/android/al;)V
invokevirtual android/app/AlertDialog$Builder/setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
invokevirtual android/app/AlertDialog/show()V
return
.limit locals 4
.limit stack 6
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
aload 0
getfield net/hockeyapp/android/al/a Lnet/hockeyapp/android/d/l;
ifnull L0
aload 0
getfield net/hockeyapp/android/al/a Lnet/hockeyapp/android/d/l;
invokevirtual net/hockeyapp/android/d/l/a()V
L0:
aload 0
getfield net/hockeyapp/android/al/a Lnet/hockeyapp/android/d/l;
areturn
.limit locals 1
.limit stack 1
.end method
