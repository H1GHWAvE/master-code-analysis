package android.support.v4.app;
abstract class as extends android.support.v4.app.ar {

    as()
    {
        return;
    }

    public android.view.View onCreateView(android.view.View p4, String p5, android.content.Context p6, android.util.AttributeSet p7)
    {
        android.view.View v0 = this.a(p4, p5, p6, p7);
        if ((v0 == null) && (android.os.Build$VERSION.SDK_INT >= 11)) {
            v0 = super.onCreateView(p4, p5, p6, p7);
        }
        return v0;
    }
}
