package android.support.v4.app;
public abstract class bh extends android.support.v4.app.bf {
    final android.app.Activity b;
    final android.content.Context c;
    final android.os.Handler d;
    final int e;
    final android.support.v4.app.bl f;
    android.support.v4.n.v g;
    android.support.v4.app.ct h;
    boolean i;
    boolean j;

    private bh(android.app.Activity p2, android.content.Context p3, android.os.Handler p4, int p5)
    {
        this.f = new android.support.v4.app.bl();
        this.b = p2;
        this.c = p3;
        this.d = p4;
        this.e = p5;
        return;
    }

    private bh(android.content.Context p2, android.os.Handler p3, int p4)
    {
        this(0, p2, p3, p4);
        return;
    }

    bh(android.support.v4.app.bb p3)
    {
        this(p3, p3, p3.d, 0);
        return;
    }

    private void a(android.support.v4.n.v p1)
    {
        this.g = p1;
        return;
    }

    private void a(String p4, java.io.FileDescriptor p5, java.io.PrintWriter p6, String[] p7)
    {
        p6.print(p4);
        p6.print("mLoadersStarted=");
        p6.println(this.j);
        if (this.h != null) {
            p6.print(p4);
            p6.print("Loader Manager ");
            p6.print(Integer.toHexString(System.identityHashCode(this.h)));
            p6.println(":");
            this.h.a(new StringBuilder().append(p4).append("  ").toString(), p5, p6, p7);
        }
        return;
    }

    private void a(boolean p2)
    {
        if ((this.h != null) && (this.j)) {
            this.j = 0;
            if (!p2) {
                this.h.c();
            } else {
                this.h.d();
            }
        }
        return;
    }

    private android.app.Activity j()
    {
        return this.b;
    }

    private android.content.Context k()
    {
        return this.c;
    }

    private android.os.Handler l()
    {
        return this.d;
    }

    private android.support.v4.app.bl m()
    {
        return this.f;
    }

    private android.support.v4.app.ct n()
    {
        android.support.v4.app.ct v0_3;
        if (this.h == null) {
            this.i = 1;
            this.h = this.a("(root)", this.j, 1);
            v0_3 = this.h;
        } else {
            v0_3 = this.h;
        }
        return v0_3;
    }

    private void o()
    {
        if (!this.j) {
            this.j = 1;
            if (this.h == null) {
                if (!this.i) {
                    this.h = this.a("(root)", this.j, 0);
                    if ((this.h != null) && (!this.h.f)) {
                        this.h.b();
                    }
                }
            } else {
                this.h.b();
            }
            this.i = 1;
        }
        return;
    }

    private void p()
    {
        if (this.h != null) {
            this.h.d();
        }
        return;
    }

    private void q()
    {
        if (this.h != null) {
            this.h.g();
        }
        return;
    }

    private void r()
    {
        if (this.g != null) {
            int v4 = this.g.size();
            android.support.v4.app.ct[] v5 = new android.support.v4.app.ct[v4];
            int v1_0 = (v4 - 1);
            while (v1_0 >= 0) {
                v5[v1_0] = ((android.support.v4.app.ct) this.g.c(v1_0));
                v1_0--;
            }
            int v2 = 0;
            while (v2 < v4) {
                android.support.v4.app.ct v6 = v5[v2];
                if (v6.g) {
                    if (android.support.v4.app.ct.b) {
                        android.util.Log.v("LoaderManager", new StringBuilder("Finished Retaining in ").append(v6).toString());
                    }
                    v6.g = 0;
                    int v1_5 = (v6.c.a() - 1);
                    while (v1_5 >= 0) {
                        int v0_12 = ((android.support.v4.app.cu) v6.c.d(v1_5));
                        if (v0_12.i) {
                            if (android.support.v4.app.ct.b) {
                                android.util.Log.v("LoaderManager", new StringBuilder("  Finished Retaining: ").append(v0_12).toString());
                            }
                            v0_12.i = 0;
                            if ((v0_12.h != v0_12.j) && (!v0_12.h)) {
                                v0_12.b();
                            }
                        }
                        if ((v0_12.h) && ((v0_12.e) && (!v0_12.k))) {
                            v0_12.b(v0_12.d, v0_12.g);
                        }
                        v1_5--;
                    }
                }
                v6.f();
                v2++;
            }
        }
        return;
    }

    final android.support.v4.app.ct a(String p3, boolean p4, boolean p5)
    {
        if (this.g == null) {
            this.g = new android.support.v4.n.v();
        }
        android.support.v4.app.ct v0_5 = ((android.support.v4.app.ct) this.g.get(p3));
        if (v0_5 != null) {
            v0_5.j = this;
        } else {
            if (p5) {
                v0_5 = new android.support.v4.app.ct(p3, this, p4);
                this.g.put(p3, v0_5);
            }
        }
        return v0_5;
    }

    public android.view.View a(int p2)
    {
        return 0;
    }

    public void a(android.support.v4.app.Fragment p3, android.content.Intent p4, int p5)
    {
        if (p5 == -1) {
            this.c.startActivity(p4);
            return;
        } else {
            throw new IllegalStateException("Starting activity with a requestCode requires a FragmentActivity host");
        }
    }

    public void a(android.support.v4.app.Fragment p1, String[] p2, int p3)
    {
        return;
    }

    public void a(String p1, java.io.PrintWriter p2, String[] p3)
    {
        return;
    }

    public boolean a()
    {
        return 1;
    }

    public boolean a(String p2)
    {
        return 0;
    }

    final void b(String p3)
    {
        if (this.g != null) {
            android.support.v4.n.v v0_3 = ((android.support.v4.app.ct) this.g.get(p3));
            if ((v0_3 != null) && (!v0_3.g)) {
                v0_3.g();
                this.g.remove(p3);
            }
        }
        return;
    }

    public boolean b()
    {
        return 1;
    }

    public android.view.LayoutInflater c()
    {
        return ((android.view.LayoutInflater) this.c.getSystemService("layout_inflater"));
    }

    public void d()
    {
        return;
    }

    public boolean e()
    {
        return 1;
    }

    public int f()
    {
        return this.e;
    }

    void g()
    {
        return;
    }

    public abstract Object h();

    final android.support.v4.n.v i()
    {
        int v0_1;
        int v1 = 0;
        if (this.g == null) {
            v0_1 = 0;
        } else {
            int v3 = this.g.size();
            android.support.v4.app.ct[] v4 = new android.support.v4.app.ct[v3];
            String v2_0 = (v3 - 1);
            while (v2_0 >= null) {
                v4[v2_0] = ((android.support.v4.app.ct) this.g.c(v2_0));
                v2_0--;
            }
            v0_1 = 0;
            while (v1 < v3) {
                String v2_1 = v4[v1];
                if (!v2_1.g) {
                    v2_1.g();
                    this.g.remove(v2_1.e);
                } else {
                    v0_1 = 1;
                }
                v1++;
            }
        }
        int v0_4;
        if (v0_1 == 0) {
            v0_4 = 0;
        } else {
            v0_4 = this.g;
        }
        return v0_4;
    }
}
