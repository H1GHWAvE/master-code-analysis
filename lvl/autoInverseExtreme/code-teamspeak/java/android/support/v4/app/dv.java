package android.support.v4.app;
 class dv extends android.support.v4.app.ec {

    dv()
    {
        return;
    }

    public android.app.Notification a(android.support.v4.app.dm p27, android.support.v4.app.dn p28)
    {
        android.app.Notification v2_1 = new android.support.v4.app.eg(p27.a, p27.B, p27.b, p27.c, p27.h, p27.f, p27.i, p27.d, p27.e, p27.g, p27.o, p27.p, p27.q, p27.k, p27.l, p27.j, p27.n, p27.v, p27.C, p27.x, p27.r, p27.s, p27.t);
        android.support.v4.app.dd.a(v2_1, p27.u);
        android.support.v4.app.dd.a(v2_1, p27.m);
        return p28.a(p27, v2_1);
    }

    public final android.support.v4.app.df a(android.app.Notification p4, int p5)
    {
        return ((android.support.v4.app.df) android.support.v4.app.ef.a(p4.actions[p5], android.support.v4.app.df.e, android.support.v4.app.fn.c));
    }

    public final java.util.ArrayList a(android.support.v4.app.df[] p5)
    {
        java.util.ArrayList v0_1;
        if (p5 != null) {
            v0_1 = new java.util.ArrayList(p5.length);
            int v2 = p5.length;
            int v1_1 = 0;
            while (v1_1 < v2) {
                v0_1.add(android.support.v4.app.ef.a(p5[v1_1]));
                v1_1++;
            }
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    public final android.support.v4.app.df[] a(java.util.ArrayList p6)
    {
        android.support.v4.app.ek[] v0_3;
        android.support.v4.app.el v3 = android.support.v4.app.df.e;
        if (p6 != null) {
            android.support.v4.app.ek[] v2 = v3.a(p6.size());
            int v1 = 0;
            while (v1 < v2.length) {
                v2[v1] = android.support.v4.app.ef.a(((android.app.Notification$Action) p6.get(v1)), v3, android.support.v4.app.fn.c);
                v1++;
            }
            v0_3 = v2;
        } else {
            v0_3 = 0;
        }
        return ((android.support.v4.app.df[]) ((android.support.v4.app.df[]) v0_3));
    }

    public final boolean d(android.app.Notification p2)
    {
        int v0_2;
        if ((p2.flags & 256) == 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final String e(android.app.Notification p2)
    {
        return p2.getGroup();
    }

    public final boolean f(android.app.Notification p2)
    {
        int v0_2;
        if ((p2.flags & 512) == 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final String g(android.app.Notification p2)
    {
        return p2.getSortKey();
    }
}
