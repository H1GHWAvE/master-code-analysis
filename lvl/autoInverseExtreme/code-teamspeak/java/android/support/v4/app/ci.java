package android.support.v4.app;
final class ci implements android.view.ViewTreeObserver$OnPreDrawListener {
    final synthetic android.view.View a;
    final synthetic android.transition.Transition b;
    final synthetic java.util.ArrayList c;
    final synthetic android.transition.Transition d;
    final synthetic java.util.ArrayList e;
    final synthetic android.transition.Transition f;
    final synthetic java.util.ArrayList g;
    final synthetic java.util.Map h;
    final synthetic java.util.ArrayList i;
    final synthetic android.transition.Transition j;
    final synthetic android.view.View k;

    ci(android.view.View p1, android.transition.Transition p2, java.util.ArrayList p3, android.transition.Transition p4, java.util.ArrayList p5, android.transition.Transition p6, java.util.ArrayList p7, java.util.Map p8, java.util.ArrayList p9, android.transition.Transition p10, android.view.View p11)
    {
        this.a = p1;
        this.b = p2;
        this.c = p3;
        this.d = p4;
        this.e = p5;
        this.f = p6;
        this.g = p7;
        this.h = p8;
        this.i = p9;
        this.j = p10;
        this.k = p11;
        return;
    }

    public final boolean onPreDraw()
    {
        this.a.getViewTreeObserver().removeOnPreDrawListener(this);
        if (this.b != null) {
            android.support.v4.app.ce.a(this.b, this.c);
        }
        if (this.d != null) {
            android.support.v4.app.ce.a(this.d, this.e);
        }
        if (this.f != null) {
            android.support.v4.app.ce.a(this.f, this.g);
        }
        int v3_0 = this.h.entrySet().iterator();
        while (v3_0.hasNext()) {
            int v0_19 = ((java.util.Map$Entry) v3_0.next());
            ((android.view.View) v0_19.getValue()).setTransitionName(((String) v0_19.getKey()));
        }
        int v3_1 = this.i.size();
        android.view.View v1_3 = 0;
        while (v1_3 < v3_1) {
            this.j.excludeTarget(((android.view.View) this.i.get(v1_3)), 0);
            v1_3++;
        }
        this.j.excludeTarget(this.k, 0);
        return 1;
    }
}
