package android.support.v4.app;
final class fu {

    fu()
    {
        return;
    }

    private static android.os.Bundle a(android.content.Intent p1)
    {
        return android.app.RemoteInput.getResultsFromIntent(p1);
    }

    private static void a(android.support.v4.app.fw[] p1, android.content.Intent p2, android.os.Bundle p3)
    {
        android.app.RemoteInput.addResultsToIntent(android.support.v4.app.fu.a(p1), p2, p3);
        return;
    }

    static android.app.RemoteInput[] a(android.support.v4.app.fw[] p5)
    {
        android.app.RemoteInput[] v0_2;
        if (p5 != null) {
            android.app.RemoteInput[] v1 = new android.app.RemoteInput[p5.length];
            android.app.RemoteInput[] v0_1 = 0;
            while (v0_1 < p5.length) {
                android.app.RemoteInput v2_1 = p5[v0_1];
                v1[v0_1] = new android.app.RemoteInput$Builder(v2_1.a()).setLabel(v2_1.b()).setChoices(v2_1.c()).setAllowFreeFormInput(v2_1.d()).addExtras(v2_1.e()).build();
                v0_1++;
            }
            v0_2 = v1;
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    private static android.support.v4.app.fw[] a(android.app.RemoteInput[] p8, android.support.v4.app.fx p9)
    {
        android.support.v4.app.fw[] v0_3;
        if (p8 != null) {
            android.support.v4.app.fw[] v7 = p9.a(p8.length);
            int v6 = 0;
            while (v6 < p8.length) {
                android.support.v4.app.fw[] v0_4 = p8[v6];
                v7[v6] = p9.a(v0_4.getResultKey(), v0_4.getLabel(), v0_4.getChoices(), v0_4.getAllowFreeFormInput(), v0_4.getExtras());
                v6++;
            }
            v0_3 = v7;
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }
}
