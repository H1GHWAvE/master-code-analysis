package android.support.v4.app;
final class ay extends android.support.v4.app.bf {
    final synthetic android.support.v4.app.Fragment a;

    ay(android.support.v4.app.Fragment p1)
    {
        this.a = p1;
        return;
    }

    public final android.view.View a(int p3)
    {
        if (this.a.ac != null) {
            return this.a.ac.findViewById(p3);
        } else {
            throw new IllegalStateException("Fragment does not have a view");
        }
    }

    public final boolean a()
    {
        int v0_2;
        if (this.a.ac == null) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }
}
