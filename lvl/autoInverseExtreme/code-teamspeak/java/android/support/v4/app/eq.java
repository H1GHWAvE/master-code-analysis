package android.support.v4.app;
final class eq {

    eq()
    {
        return;
    }

    private static android.app.Notification a(android.content.Context p6, android.app.Notification p7, CharSequence p8, CharSequence p9, CharSequence p10, android.widget.RemoteViews p11, int p12, android.app.PendingIntent p13, android.app.PendingIntent p14, android.graphics.Bitmap p15)
    {
        android.app.Notification v0_10;
        int v1 = 1;
        int v3_6 = new android.app.Notification$Builder(p6).setWhen(p7.when).setSmallIcon(p7.icon, p7.iconLevel).setContent(p7.contentView).setTicker(p7.tickerText, p11).setSound(p7.sound, p7.audioStreamType).setVibrate(p7.vibrate).setLights(p7.ledARGB, p7.ledOnMS, p7.ledOffMS);
        if ((p7.flags & 2) == 0) {
            v0_10 = 0;
        } else {
            v0_10 = 1;
        }
        android.app.Notification v0_13;
        int v3_7 = v3_6.setOngoing(v0_10);
        if ((p7.flags & 8) == 0) {
            v0_13 = 0;
        } else {
            v0_13 = 1;
        }
        android.app.Notification v0_16;
        int v3_8 = v3_7.setOnlyAlertOnce(v0_13);
        if ((p7.flags & 16) == 0) {
            v0_16 = 0;
        } else {
            v0_16 = 1;
        }
        android.app.Notification v0_23 = v3_8.setAutoCancel(v0_16).setDefaults(p7.defaults).setContentTitle(p8).setContentText(p9).setContentInfo(p10).setContentIntent(p13).setDeleteIntent(p7.deleteIntent);
        if ((p7.flags & 128) == 0) {
            v1 = 0;
        }
        return v0_23.setFullScreenIntent(p14, v1).setLargeIcon(p15).setNumber(p12).getNotification();
    }
}
