package android.support.v4.app;
final class u {

    u()
    {
        return;
    }

    private static void a(android.app.Activity p1, String[] p2, int p3)
    {
        if ((p1 instanceof android.support.v4.app.v)) {
            ((android.support.v4.app.v) p1).a(p3);
        }
        p1.requestPermissions(p2, p3);
        return;
    }

    private static boolean a(android.app.Activity p1, String p2)
    {
        return p1.shouldShowRequestPermissionRationale(p2);
    }
}
