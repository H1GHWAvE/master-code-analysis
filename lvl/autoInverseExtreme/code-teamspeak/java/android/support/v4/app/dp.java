package android.support.v4.app;
public final class dp extends android.support.v4.app.em {
    static final android.support.v4.app.en a;
    private final String[] b;
    private final android.support.v4.app.fn c;
    private final android.app.PendingIntent d;
    private final android.app.PendingIntent e;
    private final String[] f;
    private final long g;

    static dp()
    {
        android.support.v4.app.dp.a = new android.support.v4.app.dq();
        return;
    }

    dp(String[] p1, android.support.v4.app.fn p2, android.app.PendingIntent p3, android.app.PendingIntent p4, String[] p5, long p6)
    {
        this.b = p1;
        this.c = p2;
        this.e = p4;
        this.d = p3;
        this.f = p5;
        this.g = p6;
        return;
    }

    private android.support.v4.app.fn h()
    {
        return this.c;
    }

    public final String[] a()
    {
        return this.b;
    }

    public final android.app.PendingIntent b()
    {
        return this.d;
    }

    public final android.app.PendingIntent c()
    {
        return this.e;
    }

    public final String[] d()
    {
        return this.f;
    }

    public final String e()
    {
        int v0_2;
        if (this.f.length <= 0) {
            v0_2 = 0;
        } else {
            v0_2 = this.f[0];
        }
        return v0_2;
    }

    public final long f()
    {
        return this.g;
    }

    public final bridge synthetic android.support.v4.app.fw g()
    {
        return this.c;
    }
}
