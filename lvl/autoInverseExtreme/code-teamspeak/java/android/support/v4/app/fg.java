package android.support.v4.app;
final class fg implements android.support.v4.app.fk {
    final String a;
    final int b;
    final String c;
    final android.app.Notification d;

    public fg(String p2, int p3, android.app.Notification p4)
    {
        this.a = p2;
        this.b = p3;
        this.c = 0;
        this.d = p4;
        return;
    }

    public final void a(android.support.v4.app.cl p5)
    {
        p5.a(this.a, this.b, this.c, this.d);
        return;
    }

    public final String toString()
    {
        String v0_1 = new StringBuilder("NotifyTask[");
        v0_1.append("packageName:").append(this.a);
        v0_1.append(", id:").append(this.b);
        v0_1.append(", tag:").append(this.c);
        v0_1.append("]");
        return v0_1.toString();
    }
}
