package android.support.v4.app;
public class bb extends android.support.v4.app.as implements android.support.v4.app.o, android.support.v4.app.v {
    static final String a = "android:support:fragments";
    static final int b = 1;
    static final int c = 2;
    private static final String m = "FragmentActivity";
    private static final int n = 11;
    final android.os.Handler d;
    final android.support.v4.app.bg e;
    boolean f;
    boolean g;
    boolean h;
    boolean i;
    boolean j;
    boolean k;
    boolean l;

    public bb()
    {
        this.d = new android.support.v4.app.bc(this);
        this.e = new android.support.v4.app.bg(new android.support.v4.app.bd(this));
        return;
    }

    private static String a(android.view.View p7)
    {
        String v1_0 = 70;
        String v2_0 = 46;
        StringBuilder v4_1 = new StringBuilder(128);
        v4_1.append(p7.getClass().getName());
        v4_1.append(123);
        v4_1.append(Integer.toHexString(System.identityHashCode(p7)));
        v4_1.append(32);
        switch (p7.getVisibility()) {
            case 0:
                v4_1.append(86);
                break;
            case 4:
                v4_1.append(73);
                break;
            case 8:
                v4_1.append(71);
                break;
            default:
                v4_1.append(46);
        }
        String v0_10;
        if (!p7.isFocusable()) {
            v0_10 = 46;
        } else {
            v0_10 = 70;
        }
        String v0_12;
        v4_1.append(v0_10);
        if (!p7.isEnabled()) {
            v0_12 = 46;
        } else {
            v0_12 = 69;
        }
        String v0_14;
        v4_1.append(v0_12);
        if (!p7.willNotDraw()) {
            v0_14 = 68;
        } else {
            v0_14 = 46;
        }
        String v0_16;
        v4_1.append(v0_14);
        if (!p7.isHorizontalScrollBarEnabled()) {
            v0_16 = 46;
        } else {
            v0_16 = 72;
        }
        String v0_18;
        v4_1.append(v0_16);
        if (!p7.isVerticalScrollBarEnabled()) {
            v0_18 = 46;
        } else {
            v0_18 = 86;
        }
        String v0_20;
        v4_1.append(v0_18);
        if (!p7.isClickable()) {
            v0_20 = 46;
        } else {
            v0_20 = 67;
        }
        String v0_22;
        v4_1.append(v0_20);
        if (!p7.isLongClickable()) {
            v0_22 = 46;
        } else {
            v0_22 = 76;
        }
        v4_1.append(v0_22);
        v4_1.append(32);
        if (!p7.isFocused()) {
            v1_0 = 46;
        }
        String v0_25;
        v4_1.append(v1_0);
        if (!p7.isSelected()) {
            v0_25 = 46;
        } else {
            v0_25 = 83;
        }
        v4_1.append(v0_25);
        if (p7.isPressed()) {
            v2_0 = 80;
        }
        v4_1.append(v2_0);
        v4_1.append(32);
        v4_1.append(p7.getLeft());
        v4_1.append(44);
        v4_1.append(p7.getTop());
        v4_1.append(45);
        v4_1.append(p7.getRight());
        v4_1.append(44);
        v4_1.append(p7.getBottom());
        String v1_1 = p7.getId();
        if (v1_1 != -1) {
            v4_1.append(" #");
            v4_1.append(Integer.toHexString(v1_1));
            String v2_1 = p7.getResources();
            if ((v1_1 != null) && (v2_1 != null)) {
                try {
                    String v0_37;
                    switch ((-16777216 & v1_1)) {
                        case 16777216:
                            v0_37 = "android";
                            String v3_1 = v2_1.getResourceTypeName(v1_1);
                            String v1_2 = v2_1.getResourceEntryName(v1_1);
                            v4_1.append(" ");
                            v4_1.append(v0_37);
                            v4_1.append(":");
                            v4_1.append(v3_1);
                            v4_1.append("/");
                            v4_1.append(v1_2);
                            break;
                        case 2130706432:
                            v0_37 = "app";
                            break;
                        default:
                            v0_37 = v2_1.getResourcePackageName(v1_1);
                    }
                } catch (String v0) {
                }
            }
        }
        v4_1.append("}");
        return v4_1.toString();
    }

    private void a(android.support.v4.app.Fragment p3, String[] p4, int p5)
    {
        if (p5 != -1) {
            if ((p5 & -256) == 0) {
                this.l = 1;
                android.support.v4.app.m.a(this, p4, (((p3.z + 1) << 8) + (p5 & 255)));
            } else {
                throw new IllegalArgumentException("Can only use lower 8 bits for requestCode");
            }
        } else {
            android.support.v4.app.m.a(this, p4, p5);
        }
        return;
    }

    static synthetic void a(android.support.v4.app.bb p2, android.support.v4.app.Fragment p3, String[] p4, int p5)
    {
        if (p5 != -1) {
            if ((p5 & -256) == 0) {
                p2.l = 1;
                android.support.v4.app.m.a(p2, p4, (((p3.z + 1) << 8) + (p5 & 255)));
            } else {
                throw new IllegalArgumentException("Can only use lower 8 bits for requestCode");
            }
        } else {
            android.support.v4.app.m.a(p2, p4, p5);
        }
        return;
    }

    private void a(android.support.v4.app.gj p3)
    {
        if (android.os.Build$VERSION.SDK_INT >= 21) {
            this.setEnterSharedElementCallback(android.support.v4.app.q.a(android.support.v4.app.m.a(p3)));
        }
        return;
    }

    private void a(String p5, java.io.PrintWriter p6, android.view.View p7)
    {
        p6.print(p5);
        if (p7 != null) {
            p6.println(android.support.v4.app.bb.a(p7));
            if ((p7 instanceof android.view.ViewGroup)) {
                int v1 = ((android.view.ViewGroup) p7).getChildCount();
                if (v1 > 0) {
                    String v2_1 = new StringBuilder().append(p5).append("  ").toString();
                    int v0_6 = 0;
                    while (v0_6 < v1) {
                        this.a(v2_1, p6, ((android.view.ViewGroup) p7).getChildAt(v0_6));
                        v0_6++;
                    }
                }
            }
        } else {
            p6.println("null");
        }
        return;
    }

    private boolean a(android.view.View p2, android.view.Menu p3)
    {
        return super.onPreparePanel(0, p2, p3);
    }

    private void b(android.support.v4.app.gj p3)
    {
        if (android.os.Build$VERSION.SDK_INT >= 21) {
            this.setExitSharedElementCallback(android.support.v4.app.q.a(android.support.v4.app.m.a(p3)));
        }
        return;
    }

    private void d()
    {
        if (android.os.Build$VERSION.SDK_INT < 21) {
            this.finish();
        } else {
            this.finishAfterTransition();
        }
        return;
    }

    private void e()
    {
        if (android.os.Build$VERSION.SDK_INT >= 21) {
            this.postponeEnterTransition();
        }
        return;
    }

    private void f()
    {
        if (android.os.Build$VERSION.SDK_INT >= 21) {
            this.startPostponedEnterTransition();
        }
        return;
    }

    private static Object g()
    {
        return 0;
    }

    private Object h()
    {
        int v0_2;
        int v0_1 = ((android.support.v4.app.be) this.getLastNonConfigurationInstance());
        if (v0_1 == 0) {
            v0_2 = 0;
        } else {
            v0_2 = v0_1.a;
        }
        return v0_2;
    }

    private void i()
    {
        int v1_0 = this.j;
        android.support.v4.app.ct v0_1 = this.e.a;
        if ((v0_1.h != null) && (v0_1.j)) {
            v0_1.j = 0;
            if (v1_0 == 0) {
                v0_1.h.c();
            } else {
                v0_1.h.d();
            }
        }
        this.e.a.f.c(2);
        return;
    }

    private static void j()
    {
        return;
    }

    private android.support.v4.app.cr k()
    {
        android.support.v4.app.ct v0_2;
        android.support.v4.app.ct v0_1 = this.e.a;
        if (v0_1.h == null) {
            v0_1.i = 1;
            v0_1.h = v0_1.a("(root)", v0_1.j, 1);
            v0_2 = v0_1.h;
        } else {
            v0_2 = v0_1.h;
        }
        return v0_2;
    }

    final android.view.View a(android.view.View p2, String p3, android.content.Context p4, android.util.AttributeSet p5)
    {
        return this.e.a.f.a(p2, p3, p4, p5);
    }

    public final void a(int p3)
    {
        if (!this.l) {
            if ((p3 & -256) != 0) {
                throw new IllegalArgumentException("Can only use lower 8 bits for requestCode");
            }
        } else {
            this.l = 0;
        }
        return;
    }

    public final void a(android.support.v4.app.Fragment p3, android.content.Intent p4, int p5)
    {
        if (p5 != -1) {
            if ((-65536 & p5) == 0) {
                super.startActivityForResult(p4, (((p3.z + 1) << 16) + (65535 & p5)));
            } else {
                throw new IllegalArgumentException("Can only use lower 16 bits for requestCode");
            }
        } else {
            super.startActivityForResult(p4, -1);
        }
        return;
    }

    final void a(boolean p4)
    {
        if (!this.i) {
            this.i = 1;
            this.j = p4;
            this.d.removeMessages(1);
            int v1_1 = this.j;
            android.support.v4.app.ct v0_3 = this.e.a;
            if ((v0_3.h != null) && (v0_3.j)) {
                v0_3.j = 0;
                if (v1_1 == 0) {
                    v0_3.h.c();
                } else {
                    v0_3.h.d();
                }
            }
            this.e.a.f.c(2);
        }
        return;
    }

    public void b()
    {
        if (android.os.Build$VERSION.SDK_INT < 11) {
            this.k = 1;
        } else {
            this.invalidateOptionsMenu();
        }
        return;
    }

    public final android.support.v4.app.bi c()
    {
        return this.e.a.f;
    }

    public void dump(String p4, java.io.FileDescriptor p5, java.io.PrintWriter p6, String[] p7)
    {
        p6.print(p4);
        p6.print("Local FragmentActivity ");
        p6.print(Integer.toHexString(System.identityHashCode(this)));
        p6.println(" State:");
        String v0_8 = new StringBuilder().append(p4).append("  ").toString();
        p6.print(v0_8);
        p6.print("mCreated=");
        p6.print(this.f);
        p6.print("mResumed=");
        p6.print(this.g);
        p6.print(" mStopped=");
        p6.print(this.h);
        p6.print(" mReallyStopped=");
        p6.println(this.i);
        android.view.View v1_10 = this.e.a;
        p6.print(v0_8);
        p6.print("mLoadersStarted=");
        p6.println(v1_10.j);
        if (v1_10.h != null) {
            p6.print(v0_8);
            p6.print("Loader Manager ");
            p6.print(Integer.toHexString(System.identityHashCode(v1_10.h)));
            p6.println(":");
            v1_10.h.a(new StringBuilder().append(v0_8).append("  ").toString(), p5, p6, p7);
        }
        this.e.a.f.a(p4, p5, p6, p7);
        p6.print(p4);
        p6.println("View Hierarchy:");
        this.a(new StringBuilder().append(p4).append("  ").toString(), p6, this.getWindow().getDecorView());
        return;
    }

    protected final void o_()
    {
        this.e.a.f.q();
        return;
    }

    public void onActivityResult(int p5, int p6, android.content.Intent p7)
    {
        this.e.b();
        String v0_1 = (p5 >> 16);
        if (v0_1 == null) {
            super.onActivityResult(p5, p6, p7);
        } else {
            String v0_2 = (v0_1 - 1);
            String v1_1 = this.e.a();
            if ((v1_1 != null) && ((v0_2 >= null) && (v0_2 < v1_1))) {
                if (((android.support.v4.app.Fragment) this.e.a(new java.util.ArrayList(v1_1)).get(v0_2)) != null) {
                    android.support.v4.app.Fragment.o();
                } else {
                    android.util.Log.w("FragmentActivity", new StringBuilder("Activity result no fragment exists for index: 0x").append(Integer.toHexString(p5)).toString());
                }
            } else {
                android.util.Log.w("FragmentActivity", new StringBuilder("Activity result fragment index out of range: 0x").append(Integer.toHexString(p5)).toString());
            }
        }
        return;
    }

    public void onBackPressed()
    {
        if (!this.e.a.f.d()) {
            if (android.os.Build$VERSION.SDK_INT < 21) {
                this.finish();
            } else {
                this.finishAfterTransition();
            }
        }
        return;
    }

    public void onConfigurationChanged(android.content.res.Configuration p2)
    {
        super.onConfigurationChanged(p2);
        this.e.a.f.a(p2);
        return;
    }

    public void onCreate(android.os.Bundle p5)
    {
        int v0_0 = this.e;
        v0_0.a.f.a(v0_0.a, v0_0.a, 0);
        super.onCreate(p5);
        int v0_3 = ((android.support.v4.app.be) this.getLastNonConfigurationInstance());
        if (v0_3 != 0) {
            this.e.a.g = v0_3.c;
        }
        if (p5 != null) {
            int v0_4;
            android.os.Parcelable v2_5 = p5.getParcelable("android:support:fragments");
            if (v0_3 == 0) {
                v0_4 = 0;
            } else {
                v0_4 = v0_3.b;
            }
            this.e.a.f.a(v2_5, v0_4);
        }
        this.e.a.f.n();
        return;
    }

    public boolean onCreatePanelMenu(int p4, android.view.Menu p5)
    {
        int v0_0;
        if (p4 != 0) {
            v0_0 = super.onCreatePanelMenu(p4, p5);
        } else {
            v0_0 = (super.onCreatePanelMenu(p4, p5) | this.e.a.f.a(p5, this.getMenuInflater()));
            if (android.os.Build$VERSION.SDK_INT < 11) {
                v0_0 = 1;
            }
        }
        return v0_0;
    }

    public bridge synthetic android.view.View onCreateView(android.view.View p2, String p3, android.content.Context p4, android.util.AttributeSet p5)
    {
        return super.onCreateView(p2, p3, p4, p5);
    }

    public bridge synthetic android.view.View onCreateView(String p2, android.content.Context p3, android.util.AttributeSet p4)
    {
        return super.onCreateView(p2, p3, p4);
    }

    public void onDestroy()
    {
        super.onDestroy();
        this.a(0);
        this.e.a.f.s();
        android.support.v4.app.ct v0_5 = this.e.a;
        if (v0_5.h != null) {
            v0_5.h.g();
        }
        return;
    }

    public boolean onKeyDown(int p3, android.view.KeyEvent p4)
    {
        if ((android.os.Build$VERSION.SDK_INT >= 5) || ((p3 != 4) || (p4.getRepeatCount() != 0))) {
            int v0_3 = super.onKeyDown(p3, p4);
        } else {
            this.onBackPressed();
            v0_3 = 1;
        }
        return v0_3;
    }

    public void onLowMemory()
    {
        super.onLowMemory();
        this.e.a.f.t();
        return;
    }

    public boolean onMenuItemSelected(int p2, android.view.MenuItem p3)
    {
        boolean v0_4;
        if (!super.onMenuItemSelected(p2, p3)) {
            switch (p2) {
                case 0:
                    v0_4 = this.e.a.f.a(p3);
                    break;
                case 6:
                    v0_4 = this.e.a.f.b(p3);
                    break;
                default:
                    v0_4 = 0;
            }
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    public void onNewIntent(android.content.Intent p2)
    {
        super.onNewIntent(p2);
        this.e.b();
        return;
    }

    public void onPanelClosed(int p2, android.view.Menu p3)
    {
        switch (p2) {
            case 0:
                this.e.a.f.b(p3);
                break;
        }
        super.onPanelClosed(p2, p3);
        return;
    }

    public void onPause()
    {
        super.onPause();
        this.g = 0;
        if (this.d.hasMessages(2)) {
            this.d.removeMessages(2);
            this.o_();
        }
        this.e.a.f.c(4);
        return;
    }

    public void onPostResume()
    {
        super.onPostResume();
        this.d.removeMessages(2);
        this.o_();
        this.e.c();
        return;
    }

    public boolean onPreparePanel(int p3, android.view.View p4, android.view.Menu p5)
    {
        if ((p3 != 0) || (p5 == null)) {
            int v0_0 = super.onPreparePanel(p3, p4, p5);
        } else {
            if (this.k) {
                this.k = 0;
                p5.clear();
                this.onCreatePanelMenu(p3, p5);
            }
            v0_0 = (super.onPreparePanel(0, p4, p5) | this.e.a.f.a(p5));
        }
        return v0_0;
    }

    public void onRequestPermissionsResult(int p5, String[] p6, int[] p7)
    {
        String v0_1 = ((p5 >> 8) & 255);
        if (v0_1 != null) {
            String v0_2 = (v0_1 - 1);
            String v1_1 = this.e.a();
            if ((v1_1 != null) && ((v0_2 >= null) && (v0_2 < v1_1))) {
                if (((android.support.v4.app.Fragment) this.e.a(new java.util.ArrayList(v1_1)).get(v0_2)) != null) {
                    android.support.v4.app.Fragment.p();
                } else {
                    android.util.Log.w("FragmentActivity", new StringBuilder("Activity result no fragment exists for index: 0x").append(Integer.toHexString(p5)).toString());
                }
            } else {
                android.util.Log.w("FragmentActivity", new StringBuilder("Activity result fragment index out of range: 0x").append(Integer.toHexString(p5)).toString());
            }
        }
        return;
    }

    public void onResume()
    {
        super.onResume();
        this.d.sendEmptyMessage(2);
        this.g = 1;
        this.e.c();
        return;
    }

    public final Object onRetainNonConfigurationInstance()
    {
        android.support.v4.app.be v4 = 0;
        if (this.h) {
            this.a(1);
        }
        java.util.ArrayList v1_0;
        android.support.v4.app.bl v5 = this.e.a.f;
        if (v5.l == null) {
            v1_0 = 0;
        } else {
            int v3 = 0;
            v1_0 = 0;
            while (v3 < v5.l.size()) {
                String v0_13 = ((android.support.v4.app.Fragment) v5.l.get(v3));
                if ((v0_13 != null) && (v0_13.V)) {
                    if (v1_0 == null) {
                        v1_0 = new java.util.ArrayList();
                    }
                    String v2_3;
                    v1_0.add(v0_13);
                    v0_13.W = 1;
                    if (v0_13.C == null) {
                        v2_3 = -1;
                    } else {
                        v2_3 = v0_13.C.z;
                    }
                    v0_13.D = v2_3;
                    if (android.support.v4.app.bl.b) {
                        android.util.Log.v("FragmentManager", new StringBuilder("retainNonConfig: keeping retained ").append(v0_13).toString());
                    }
                }
                v3++;
            }
        }
        String v2_0 = this.e.a.i();
        if ((v1_0 != null) || (v2_0 != null)) {
            String v0_10 = new android.support.v4.app.be();
            v0_10.a = 0;
            v0_10.b = v1_0;
            v0_10.c = v2_0;
            v4 = v0_10;
        }
        return v4;
    }

    public void onSaveInstanceState(android.os.Bundle p3)
    {
        super.onSaveInstanceState(p3);
        android.os.Parcelable v0_3 = this.e.a.f.m();
        if (v0_3 != null) {
            p3.putParcelable("android:support:fragments", v0_3);
        }
        return;
    }

    protected void onStart()
    {
        super.onStart();
        this.h = 0;
        this.i = 0;
        this.d.removeMessages(1);
        if (!this.f) {
            this.f = 1;
            this.e.a.f.o();
        }
        this.e.b();
        this.e.c();
        int v0_8 = this.e.a;
        if (!v0_8.j) {
            v0_8.j = 1;
            if (v0_8.h == null) {
                if (!v0_8.i) {
                    v0_8.h = v0_8.a("(root)", v0_8.j, 0);
                    if ((v0_8.h != null) && (!v0_8.h.f)) {
                        v0_8.h.b();
                    }
                }
            } else {
                v0_8.h.b();
            }
            v0_8.i = 1;
        }
        this.e.a.f.p();
        int v3_1 = this.e.a;
        if (v3_1.g != null) {
            int v4_1 = v3_1.g.size();
            android.support.v4.app.ct[] v5 = new android.support.v4.app.ct[v4_1];
            int v1_10 = (v4_1 - 1);
            while (v1_10 >= 0) {
                v5[v1_10] = ((android.support.v4.app.ct) v3_1.g.c(v1_10));
                v1_10--;
            }
            int v1_11 = 0;
            while (v1_11 < v4_1) {
                android.support.v4.app.ct v6 = v5[v1_11];
                if (v6.g) {
                    if (android.support.v4.app.ct.b) {
                        android.util.Log.v("LoaderManager", new StringBuilder("Finished Retaining in ").append(v6).toString());
                    }
                    v6.g = 0;
                    int v3_6 = (v6.c.a() - 1);
                    while (v3_6 >= 0) {
                        int v0_25 = ((android.support.v4.app.cu) v6.c.d(v3_6));
                        if (v0_25.i) {
                            if (android.support.v4.app.ct.b) {
                                android.util.Log.v("LoaderManager", new StringBuilder("  Finished Retaining: ").append(v0_25).toString());
                            }
                            v0_25.i = 0;
                            if ((v0_25.h != v0_25.j) && (!v0_25.h)) {
                                v0_25.b();
                            }
                        }
                        if ((v0_25.h) && ((v0_25.e) && (!v0_25.k))) {
                            v0_25.b(v0_25.d, v0_25.g);
                        }
                        v3_6--;
                    }
                }
                v6.f();
                v1_11++;
            }
        }
        return;
    }

    public void onStateNotSaved()
    {
        this.e.b();
        return;
    }

    public void onStop()
    {
        super.onStop();
        this.h = 1;
        this.d.sendEmptyMessage(1);
        this.e.a.f.r();
        return;
    }

    public void startActivityForResult(android.content.Intent p3, int p4)
    {
        if ((p4 == -1) || ((-65536 & p4) == 0)) {
            super.startActivityForResult(p3, p4);
            return;
        } else {
            throw new IllegalArgumentException("Can only use lower 16 bits for requestCode");
        }
    }
}
