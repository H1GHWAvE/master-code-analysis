package android.support.v4.app;
public abstract class ex extends android.app.Service {

    public ex()
    {
        return;
    }

    private void a(int p5, String p6)
    {
        String v1_0 = this.getPackageManager().getPackagesForUid(p5);
        int v0_1 = 0;
        while (v0_1 < v1_0.length) {
            if (!v1_0[v0_1].equals(p6)) {
                v0_1++;
            } else {
                return;
            }
        }
        throw new SecurityException(new StringBuilder("NotificationSideChannelService: Uid ").append(p5).append(" is not authorized for package ").append(p6).toString());
    }

    static synthetic void a(android.support.v4.app.ex p4, int p5, String p6)
    {
        String v1_0 = p4.getPackageManager().getPackagesForUid(p5);
        int v0_1 = 0;
        while (v0_1 < v1_0.length) {
            if (v1_0[v0_1].equals(p6)) {
                return;
            } else {
                v0_1++;
            }
        }
        throw new SecurityException(new StringBuilder("NotificationSideChannelService: Uid ").append(p5).append(" is not authorized for package ").append(p6).toString());
    }

    public abstract void a();

    public abstract void b();

    public abstract void c();

    public android.os.IBinder onBind(android.content.Intent p4)
    {
        android.support.v4.app.ez v0_0 = 0;
        if ((p4.getAction().equals("android.support.BIND_NOTIFICATION_SIDE_CHANNEL")) && (android.os.Build$VERSION.SDK_INT <= 19)) {
            v0_0 = new android.support.v4.app.ez(this, 0);
        }
        return v0_0;
    }
}
