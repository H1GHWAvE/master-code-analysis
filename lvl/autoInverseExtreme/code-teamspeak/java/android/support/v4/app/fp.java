package android.support.v4.app;
public final class fp {
    private final String a;
    private CharSequence b;
    private CharSequence[] c;
    private boolean d;
    private android.os.Bundle e;

    private fp(String p3)
    {
        this.d = 1;
        this.e = new android.os.Bundle();
        if (p3 != null) {
            this.a = p3;
            return;
        } else {
            throw new IllegalArgumentException("Result key can\'t be null");
        }
    }

    private android.os.Bundle a()
    {
        return this.e;
    }

    private android.support.v4.app.fp a(android.os.Bundle p2)
    {
        if (p2 != null) {
            this.e.putAll(p2);
        }
        return this;
    }

    private android.support.v4.app.fp a(CharSequence p1)
    {
        this.b = p1;
        return this;
    }

    private android.support.v4.app.fp a(boolean p1)
    {
        this.d = p1;
        return this;
    }

    private android.support.v4.app.fp a(CharSequence[] p1)
    {
        this.c = p1;
        return this;
    }

    private android.support.v4.app.fn b()
    {
        return new android.support.v4.app.fn(this.a, this.b, this.c, this.d, this.e);
    }
}
