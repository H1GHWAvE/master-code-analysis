package android.support.v4.app;
 class ec extends android.support.v4.app.eb {

    ec()
    {
        return;
    }

    public android.app.Notification a(android.support.v4.app.dm p27, android.support.v4.app.dn p28)
    {
        android.app.Notification v2_1 = new android.support.v4.app.ew(p27.a, p27.B, p27.b, p27.c, p27.h, p27.f, p27.i, p27.d, p27.e, p27.g, p27.o, p27.p, p27.q, p27.k, p27.l, p27.j, p27.n, p27.v, p27.C, p27.x, p27.r, p27.s, p27.t);
        android.support.v4.app.dd.a(v2_1, p27.u);
        android.support.v4.app.dd.a(v2_1, p27.m);
        return p28.a(p27, v2_1);
    }

    public final android.os.Bundle a(android.app.Notification p2)
    {
        return p2.extras;
    }

    public android.support.v4.app.df a(android.app.Notification p7, int p8)
    {
        android.app.PendingIntent v4_0 = p7.actions[p8];
        android.os.Bundle v5 = 0;
        int v2_2 = p7.extras.getSparseParcelableArray("android.support.actionExtras");
        if (v2_2 != 0) {
            v5 = ((android.os.Bundle) v2_2.get(p8));
        }
        return ((android.support.v4.app.df) android.support.v4.app.et.a(android.support.v4.app.df.e, android.support.v4.app.fn.c, v4_0.icon, v4_0.title, v4_0.actionIntent, v5));
    }

    public final int b(android.app.Notification p2)
    {
        int v0_1;
        if (p2.actions == null) {
            v0_1 = 0;
        } else {
            v0_1 = p2.actions.length;
        }
        return v0_1;
    }

    public boolean d(android.app.Notification p3)
    {
        return p3.extras.getBoolean("android.support.localOnly");
    }

    public String e(android.app.Notification p3)
    {
        return p3.extras.getString("android.support.groupKey");
    }

    public boolean f(android.app.Notification p3)
    {
        return p3.extras.getBoolean("android.support.isGroupSummary");
    }

    public String g(android.app.Notification p3)
    {
        return p3.extras.getString("android.support.sortKey");
    }
}
