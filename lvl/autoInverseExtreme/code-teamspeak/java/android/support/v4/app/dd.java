package android.support.v4.app;
public class dd {
    public static final String A = "android.bigText";
    public static final String B = "android.icon";
    public static final String C = "android.largeIcon";
    public static final String D = "android.largeIcon.big";
    public static final String E = "android.progress";
    public static final String F = "android.progressMax";
    public static final String G = "android.progressIndeterminate";
    public static final String H = "android.showChronometer";
    public static final String I = "android.showWhen";
    public static final String J = "android.picture";
    public static final String K = "android.textLines";
    public static final String L = "android.template";
    public static final String M = "android.people";
    public static final String N = "android.backgroundImageUri";
    public static final String O = "android.mediaSession";
    public static final String P = "android.compactActions";
    public static final int Q = 0;
    public static final int R = 1;
    public static final int S = 0;
    public static final int T = 255;
    public static final String U = "call";
    public static final String V = "msg";
    public static final String W = "email";
    public static final String X = "event";
    public static final String Y = "promo";
    public static final String Z = "alarm";
    public static final int a = 255;
    public static final String aa = "progress";
    public static final String ab = "social";
    public static final String ac = "err";
    public static final String ad = "transport";
    public static final String ae = "sys";
    public static final String af = "service";
    public static final String ag = "recommendation";
    public static final String ah = "status";
    private static final android.support.v4.app.du ai = None;
    public static final int b = 1;
    public static final int c = 2;
    public static final int d = 4;
    public static final int e = 255;
    public static final int f = 1;
    public static final int g = 2;
    public static final int h = 4;
    public static final int i = 8;
    public static final int j = 16;
    public static final int k = 32;
    public static final int l = 64;
    public static final int m = 128;
    public static final int n = 256;
    public static final int o = 512;
    public static final int p = 0;
    public static final int q = 255;
    public static final int r = 254;
    public static final int s = 1;
    public static final int t = 2;
    public static final String u = "android.title";
    public static final String v = "android.title.big";
    public static final String w = "android.text";
    public static final String x = "android.subText";
    public static final String y = "android.infoText";
    public static final String z = "android.summaryText";

    static dd()
    {
        if (android.os.Build$VERSION.SDK_INT < 21) {
            if (android.os.Build$VERSION.SDK_INT < 20) {
                if (android.os.Build$VERSION.SDK_INT < 19) {
                    if (android.os.Build$VERSION.SDK_INT < 16) {
                        if (android.os.Build$VERSION.SDK_INT < 14) {
                            if (android.os.Build$VERSION.SDK_INT < 11) {
                                if (android.os.Build$VERSION.SDK_INT < 9) {
                                    android.support.v4.app.dd.ai = new android.support.v4.app.dx();
                                } else {
                                    android.support.v4.app.dd.ai = new android.support.v4.app.dy();
                                }
                            } else {
                                android.support.v4.app.dd.ai = new android.support.v4.app.dz();
                            }
                        } else {
                            android.support.v4.app.dd.ai = new android.support.v4.app.ea();
                        }
                    } else {
                        android.support.v4.app.dd.ai = new android.support.v4.app.eb();
                    }
                } else {
                    android.support.v4.app.dd.ai = new android.support.v4.app.ec();
                }
            } else {
                android.support.v4.app.dd.ai = new android.support.v4.app.dv();
            }
        } else {
            android.support.v4.app.dd.ai = new android.support.v4.app.dw();
        }
        return;
    }

    public dd()
    {
        return;
    }

    public static android.os.Bundle a(android.app.Notification p1)
    {
        return android.support.v4.app.dd.ai.a(p1);
    }

    private static android.support.v4.app.df a(android.app.Notification p1, int p2)
    {
        return android.support.v4.app.dd.ai.a(p1, p2);
    }

    static synthetic android.support.v4.app.du a()
    {
        return android.support.v4.app.dd.ai;
    }

    static synthetic void a(android.support.v4.app.db p2, java.util.ArrayList p3)
    {
        java.util.Iterator v1 = p3.iterator();
        while (v1.hasNext()) {
            p2.a(((android.support.v4.app.df) v1.next()));
        }
        return;
    }

    static synthetic void a(android.support.v4.app.dc p7, android.support.v4.app.ed p8)
    {
        if (p8 != null) {
            if (!(p8 instanceof android.support.v4.app.dl)) {
                if (!(p8 instanceof android.support.v4.app.dt)) {
                    if ((p8 instanceof android.support.v4.app.dk)) {
                        android.support.v4.app.et.a(p7, ((android.support.v4.app.dk) p8).e, ((android.support.v4.app.dk) p8).g, ((android.support.v4.app.dk) p8).f, ((android.support.v4.app.dk) p8).a, ((android.support.v4.app.dk) p8).b, ((android.support.v4.app.dk) p8).c);
                    }
                } else {
                    android.support.v4.app.et.a(p7, ((android.support.v4.app.dt) p8).e, ((android.support.v4.app.dt) p8).g, ((android.support.v4.app.dt) p8).f, ((android.support.v4.app.dt) p8).a);
                }
            } else {
                android.support.v4.app.et.a(p7, ((android.support.v4.app.dl) p8).e, ((android.support.v4.app.dl) p8).g, ((android.support.v4.app.dl) p8).f, ((android.support.v4.app.dl) p8).a);
            }
        }
        return;
    }

    static synthetic android.app.Notification[] a(android.os.Bundle p4, String p5)
    {
        android.app.Notification[] v0_1;
        android.app.Notification[] v0_0 = p4.getParcelableArray(p5);
        if ((!(v0_0 instanceof android.app.Notification[])) && (v0_0 != null)) {
            android.app.Notification[] v3 = new android.app.Notification[v0_0.length];
            int v2 = 0;
            while (v2 < v0_0.length) {
                v3[v2] = ((android.app.Notification) v0_0[v2]);
                v2++;
            }
            p4.putParcelableArray(p5, v3);
            v0_1 = v3;
        } else {
            v0_1 = ((android.app.Notification[]) ((android.app.Notification[]) v0_0));
        }
        return v0_1;
    }

    private static int b(android.app.Notification p1)
    {
        return android.support.v4.app.dd.ai.b(p1);
    }

    private static void b(android.support.v4.app.db p2, java.util.ArrayList p3)
    {
        java.util.Iterator v1 = p3.iterator();
        while (v1.hasNext()) {
            p2.a(((android.support.v4.app.df) v1.next()));
        }
        return;
    }

    private static void b(android.support.v4.app.dc p7, android.support.v4.app.ed p8)
    {
        if (p8 != null) {
            if (!(p8 instanceof android.support.v4.app.dl)) {
                if (!(p8 instanceof android.support.v4.app.dt)) {
                    if ((p8 instanceof android.support.v4.app.dk)) {
                        android.support.v4.app.et.a(p7, ((android.support.v4.app.dk) p8).e, ((android.support.v4.app.dk) p8).g, ((android.support.v4.app.dk) p8).f, ((android.support.v4.app.dk) p8).a, ((android.support.v4.app.dk) p8).b, ((android.support.v4.app.dk) p8).c);
                    }
                } else {
                    android.support.v4.app.et.a(p7, ((android.support.v4.app.dt) p8).e, ((android.support.v4.app.dt) p8).g, ((android.support.v4.app.dt) p8).f, ((android.support.v4.app.dt) p8).a);
                }
            } else {
                android.support.v4.app.et.a(p7, ((android.support.v4.app.dl) p8).e, ((android.support.v4.app.dl) p8).g, ((android.support.v4.app.dl) p8).f, ((android.support.v4.app.dl) p8).a);
            }
        }
        return;
    }

    private static android.app.Notification[] b(android.os.Bundle p4, String p5)
    {
        android.app.Notification[] v0_1;
        android.app.Notification[] v0_0 = p4.getParcelableArray(p5);
        if ((!(v0_0 instanceof android.app.Notification[])) && (v0_0 != null)) {
            android.app.Notification[] v3 = new android.app.Notification[v0_0.length];
            int v2 = 0;
            while (v2 < v0_0.length) {
                v3[v2] = ((android.app.Notification) v0_0[v2]);
                v2++;
            }
            p4.putParcelableArray(p5, v3);
            v0_1 = v3;
        } else {
            v0_1 = ((android.app.Notification[]) ((android.app.Notification[]) v0_0));
        }
        return v0_1;
    }

    private static String c(android.app.Notification p1)
    {
        return android.support.v4.app.dd.ai.c(p1);
    }

    private static boolean d(android.app.Notification p1)
    {
        return android.support.v4.app.dd.ai.d(p1);
    }

    private static String e(android.app.Notification p1)
    {
        return android.support.v4.app.dd.ai.e(p1);
    }

    private static boolean f(android.app.Notification p1)
    {
        return android.support.v4.app.dd.ai.f(p1);
    }

    private static String g(android.app.Notification p1)
    {
        return android.support.v4.app.dd.ai.g(p1);
    }
}
