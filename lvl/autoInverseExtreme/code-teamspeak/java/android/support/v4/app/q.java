package android.support.v4.app;
final class q {

    q()
    {
        return;
    }

    static android.app.SharedElementCallback a(android.support.v4.app.r p1)
    {
        android.support.v4.app.s v0_0 = 0;
        if (p1 != null) {
            v0_0 = new android.support.v4.app.s(p1);
        }
        return v0_0;
    }

    private static void a(android.app.Activity p0)
    {
        p0.finishAfterTransition();
        return;
    }

    private static void a(android.app.Activity p1, android.support.v4.app.r p2)
    {
        p1.setEnterSharedElementCallback(android.support.v4.app.q.a(p2));
        return;
    }

    private static void b(android.app.Activity p0)
    {
        p0.postponeEnterTransition();
        return;
    }

    private static void b(android.app.Activity p1, android.support.v4.app.r p2)
    {
        p1.setExitSharedElementCallback(android.support.v4.app.q.a(p2));
        return;
    }

    private static void c(android.app.Activity p0)
    {
        p0.startPostponedEnterTransition();
        return;
    }
}
