package android.support.v4.app;
final class ch extends android.transition.Transition$EpicenterCallback {
    final synthetic android.support.v4.app.cj a;
    private android.graphics.Rect b;

    ch(android.support.v4.app.cj p1)
    {
        this.a = p1;
        return;
    }

    public final android.graphics.Rect onGetEpicenter(android.transition.Transition p2)
    {
        if ((this.b == null) && (this.a.a != null)) {
            this.b = android.support.v4.app.ce.a(this.a.a);
        }
        return this.b;
    }
}
