package android.support.v4.app;
public final class af {
    public static final int a = 0;
    public static final int b = 1;
    public static final int c = 3;
    private static final android.support.v4.app.ai d;

    static af()
    {
        if (android.os.Build$VERSION.SDK_INT < 23) {
            android.support.v4.app.af.d = new android.support.v4.app.ai(0);
        } else {
            android.support.v4.app.af.d = new android.support.v4.app.ah(0);
        }
        return;
    }

    public af()
    {
        return;
    }

    private static int a(android.content.Context p1, String p2, int p3, String p4)
    {
        return android.support.v4.app.af.d.a(p1, p2, p3, p4);
    }

    public static int a(android.content.Context p1, String p2, String p3)
    {
        return android.support.v4.app.af.d.a(p1, p2, p3);
    }

    public static String a(String p1)
    {
        return android.support.v4.app.af.d.a(p1);
    }
}
