package android.support.v4.app;
final class ac extends android.support.v4.app.aa {
    private final android.support.v4.app.ae a;

    ac(android.support.v4.app.ae p1)
    {
        this.a = p1;
        return;
    }

    public final android.os.Bundle a()
    {
        return this.a.a.toBundle();
    }

    public final void a(android.support.v4.app.aa p3)
    {
        if ((p3 instanceof android.support.v4.app.ac)) {
            this.a.a.update(((android.support.v4.app.ac) p3).a.a);
        }
        return;
    }
}
