package android.support.v4.app;
final class cg implements android.view.ViewTreeObserver$OnPreDrawListener {
    final synthetic android.view.View a;
    final synthetic android.transition.Transition b;
    final synthetic android.view.View c;
    final synthetic android.support.v4.app.ck d;
    final synthetic java.util.Map e;
    final synthetic java.util.Map f;
    final synthetic java.util.ArrayList g;

    cg(android.view.View p1, android.transition.Transition p2, android.view.View p3, android.support.v4.app.ck p4, java.util.Map p5, java.util.Map p6, java.util.ArrayList p7)
    {
        this.a = p1;
        this.b = p2;
        this.c = p3;
        this.d = p4;
        this.e = p5;
        this.f = p6;
        this.g = p7;
        return;
    }

    public final boolean onPreDraw()
    {
        this.a.getViewTreeObserver().removeOnPreDrawListener(this);
        if (this.b != null) {
            this.b.removeTarget(this.c);
        }
        android.view.View v2 = this.d.a();
        if (v2 != null) {
            if (!this.e.isEmpty()) {
                android.support.v4.app.ce.a(this.f, v2);
                this.f.keySet().retainAll(this.e.values());
                java.util.Iterator v3 = this.e.entrySet().iterator();
                while (v3.hasNext()) {
                    String v0_20 = ((java.util.Map$Entry) v3.next());
                    android.view.View v1_10 = ((android.view.View) this.f.get(((String) v0_20.getValue())));
                    if (v1_10 != null) {
                        v1_10.setTransitionName(((String) v0_20.getKey()));
                    }
                }
            }
            if (this.b != null) {
                android.support.v4.app.ce.a(this.g, v2);
                this.g.removeAll(this.f.values());
                this.g.add(this.c);
                android.support.v4.app.ce.b(this.b, this.g);
            }
        }
        return 1;
    }
}
