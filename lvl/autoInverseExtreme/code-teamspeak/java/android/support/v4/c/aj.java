package android.support.v4.c;
final class aj implements java.util.concurrent.ThreadFactory {
    private final java.util.concurrent.atomic.AtomicInteger a;

    aj()
    {
        this.a = new java.util.concurrent.atomic.AtomicInteger(1);
        return;
    }

    public final Thread newThread(Runnable p4)
    {
        return new Thread(p4, new StringBuilder("ModernAsyncTask #").append(this.a.getAndIncrement()).toString());
    }
}
