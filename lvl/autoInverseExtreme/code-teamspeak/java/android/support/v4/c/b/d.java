package android.support.v4.c.b;
public final class d {

    public d()
    {
        return;
    }

    private static int a(android.content.res.TypedArray p1, int p2, int p3, int p4)
    {
        return p1.getInt(p2, p1.getInt(p3, p4));
    }

    private static android.graphics.drawable.Drawable a(android.content.res.TypedArray p1, int p2, int p3)
    {
        android.graphics.drawable.Drawable v0 = p1.getDrawable(p2);
        if (v0 == null) {
            v0 = p1.getDrawable(p3);
        }
        return v0;
    }

    private static boolean a(android.content.res.TypedArray p1, int p2, int p3, boolean p4)
    {
        return p1.getBoolean(p2, p1.getBoolean(p3, p4));
    }

    private static int b(android.content.res.TypedArray p1, int p2, int p3, int p4)
    {
        return p1.getResourceId(p2, p1.getResourceId(p3, p4));
    }

    private static String b(android.content.res.TypedArray p1, int p2, int p3)
    {
        String v0 = p1.getString(p2);
        if (v0 == null) {
            v0 = p1.getString(p3);
        }
        return v0;
    }

    private static CharSequence[] c(android.content.res.TypedArray p1, int p2, int p3)
    {
        CharSequence[] v0 = p1.getTextArray(p2);
        if (v0 == null) {
            v0 = p1.getTextArray(p3);
        }
        return v0;
    }
}
