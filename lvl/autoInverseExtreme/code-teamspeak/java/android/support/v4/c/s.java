package android.support.v4.c;
final class s implements android.support.v4.c.r {
    final java.util.HashMap a;
    private final String b;

    public s(String p2)
    {
        this.a = new java.util.HashMap();
        this.b = p2;
        return;
    }

    private void a(String p5, java.io.File p6)
    {
        if (!android.text.TextUtils.isEmpty(p5)) {
            try {
                this.a.put(p5, p6.getCanonicalFile());
                return;
            } catch (java.io.IOException v0_2) {
                throw new IllegalArgumentException(new StringBuilder("Failed to resolve canonical path for ").append(p6).toString(), v0_2);
            }
        } else {
            throw new IllegalArgumentException("Name must not be empty");
        }
    }

    public final android.net.Uri a(java.io.File p7)
    {
        try {
            StringBuilder v3_0 = p7.getCanonicalPath();
            String v2_0 = 0;
            java.util.Iterator v4 = this.a.entrySet().iterator();
        } catch (android.net.Uri v0) {
            throw new IllegalArgumentException(new StringBuilder("Failed to resolve canonical path for ").append(p7).toString());
        }
        while (v4.hasNext()) {
            android.net.Uri v0_25 = ((java.util.Map$Entry) v4.next());
            android.net.Uri$Builder v1_18 = ((java.io.File) v0_25.getValue()).getPath();
            if ((!v3_0.startsWith(v1_18)) || ((v2_0 != null) && (v1_18.length() <= ((java.io.File) v2_0.getValue()).getPath().length()))) {
                v0_25 = v2_0;
            }
            v2_0 = v0_25;
        }
        if (v2_0 != null) {
            android.net.Uri$Builder v1_6;
            android.net.Uri v0_7 = ((java.io.File) v2_0.getValue()).getPath();
            if (!v0_7.endsWith("/")) {
                v1_6 = v3_0.substring((v0_7.length() + 1));
            } else {
                v1_6 = v3_0.substring(v0_7.length());
            }
            return new android.net.Uri$Builder().scheme("content").authority(this.b).encodedPath(new StringBuilder().append(android.net.Uri.encode(((String) v2_0.getKey()))).append(47).append(android.net.Uri.encode(v1_6, "/")).toString()).build();
        } else {
            throw new IllegalArgumentException(new StringBuilder("Failed to find configured root that contains ").append(v3_0).toString());
        }
    }

    public final java.io.File a(android.net.Uri p5)
    {
        SecurityException v0_0 = p5.getEncodedPath();
        String v1_1 = v0_0.indexOf(47, 1);
        String v2_2 = android.net.Uri.decode(v0_0.substring(1, v1_1));
        String v1_3 = android.net.Uri.decode(v0_0.substring((v1_1 + 1)));
        SecurityException v0_4 = ((java.io.File) this.a.get(v2_2));
        if (v0_4 != null) {
            try {
                String v1_4 = new java.io.File(v0_4, v1_3).getCanonicalFile();
                String v2_5 = v1_4.getPath();
            } catch (SecurityException v0) {
                throw new IllegalArgumentException(new StringBuilder("Failed to resolve canonical path for ").append(v2_5).toString());
            }
            if (v2_5.startsWith(v0_4.getPath())) {
                return v1_4;
            } else {
                throw new SecurityException("Resolved path jumped beyond configured root");
            }
        } else {
            throw new IllegalArgumentException(new StringBuilder("Unable to find configured root for ").append(p5).toString());
        }
    }
}
