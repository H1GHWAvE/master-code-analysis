package android.support.v4.c;
public final class o extends android.support.v4.c.a {
    final android.support.v4.c.ab h;
    android.net.Uri i;
    String[] j;
    String k;
    String[] l;
    String m;
    android.database.Cursor n;
    android.support.v4.i.c o;

    private o(android.content.Context p2)
    {
        this(p2);
        this.h = new android.support.v4.c.ab(this);
        return;
    }

    private o(android.content.Context p2, android.net.Uri p3, String[] p4, String p5, String[] p6, String p7)
    {
        this(p2);
        this.h = new android.support.v4.c.ab(this);
        this.i = p3;
        this.j = p4;
        this.k = p5;
        this.l = p6;
        this.m = p7;
        return;
    }

    private void a(android.database.Cursor p3)
    {
        if (!this.v) {
            android.database.Cursor v0_1 = this.n;
            this.n = p3;
            if (this.t) {
                super.b(p3);
            }
            if ((v0_1 != null) && ((v0_1 != p3) && (!v0_1.isClosed()))) {
                v0_1.close();
            }
        } else {
            if (p3 != null) {
                p3.close();
            }
        }
        return;
    }

    private void a(android.net.Uri p1)
    {
        this.i = p1;
        return;
    }

    private void a(String p1)
    {
        this.k = p1;
        return;
    }

    private void a(String[] p1)
    {
        this.j = p1;
        return;
    }

    private static void b(android.database.Cursor p1)
    {
        if ((p1 != null) && (!p1.isClosed())) {
            p1.close();
        }
        return;
    }

    private void b(String p1)
    {
        this.m = p1;
        return;
    }

    private void b(String[] p1)
    {
        this.l = p1;
        return;
    }

    private android.database.Cursor o()
    {
        try {
            Throwable v0_1;
            if (this.d == null) {
                v0_1 = 0;
            } else {
                v0_1 = 1;
            }
        } catch (Throwable v0_14) {
            throw v0_14;
        }
        if (v0_1 == null) {
            this.o = new android.support.v4.i.c();
            try {
                int v1_1 = android.support.v4.c.c.a(this.s.getContentResolver(), this.i, this.j, this.k, this.l, this.m, this.o);
            } catch (Throwable v0_8) {
                this.o = 0;
                throw v0_8;
            }
            if (v1_1 != 0) {
                try {
                    v1_1.getCount();
                    v1_1.registerContentObserver(this.h);
                } catch (Throwable v0_7) {
                    v1_1.close();
                    throw v0_7;
                }
            }
            this.o = 0;
            return v1_1;
        } else {
            throw new android.support.v4.i.h();
        }
    }

    private android.net.Uri p()
    {
        return this.i;
    }

    private String[] q()
    {
        return this.j;
    }

    private String r()
    {
        return this.k;
    }

    private String[] s()
    {
        return this.l;
    }

    private String t()
    {
        return this.m;
    }

    public final synthetic void a(Object p2)
    {
        if ((((android.database.Cursor) p2) != null) && (!((android.database.Cursor) p2).isClosed())) {
            ((android.database.Cursor) p2).close();
        }
        return;
    }

    public final void a(String p2, java.io.FileDescriptor p3, java.io.PrintWriter p4, String[] p5)
    {
        super.a(p2, p3, p4, p5);
        p4.print(p2);
        p4.print("mUri=");
        p4.println(this.i);
        p4.print(p2);
        p4.print("mProjection=");
        p4.println(java.util.Arrays.toString(this.j));
        p4.print(p2);
        p4.print("mSelection=");
        p4.println(this.k);
        p4.print(p2);
        p4.print("mSelectionArgs=");
        p4.println(java.util.Arrays.toString(this.l));
        p4.print(p2);
        p4.print("mSortOrder=");
        p4.println(this.m);
        p4.print(p2);
        p4.print("mCursor=");
        p4.println(this.n);
        p4.print(p2);
        p4.print("mContentChanged=");
        p4.println(this.w);
        return;
    }

    public final synthetic void b(Object p1)
    {
        this.a(((android.database.Cursor) p1));
        return;
    }

    public final synthetic Object d()
    {
        return this.o();
    }

    public final void e()
    {
        super.e();
        try {
            if (this.o != null) {
                android.support.v4.i.c v1 = this.o;
                if (!v1.a) {
                    v1.a = 1;
                    v1.c = 1;
                    Throwable v0_4 = v1.b;
                    if (v0_4 != null) {
                        try {
                            ((android.os.CancellationSignal) v0_4).cancel();
                        } catch (Throwable v0_6) {
                            v1.c = 0;
                            v1.notifyAll();
                            throw v0_6;
                        }
                    }
                    v1.c = 0;
                    v1.notifyAll();
                } else {
                }
            }
        } catch (Throwable v0_11) {
            throw v0_11;
        }
        return;
    }

    protected final void f()
    {
        if (this.n != null) {
            this.a(this.n);
        }
        android.database.Cursor v0_2 = this.w;
        this.w = 0;
        this.x = (this.x | v0_2);
        if ((v0_2 != null) || (this.n == null)) {
            this.k();
        }
        return;
    }

    protected final void g()
    {
        this.j();
        return;
    }

    protected final void h()
    {
        super.h();
        this.j();
        if ((this.n != null) && (!this.n.isClosed())) {
            this.n.close();
        }
        this.n = 0;
        return;
    }
}
