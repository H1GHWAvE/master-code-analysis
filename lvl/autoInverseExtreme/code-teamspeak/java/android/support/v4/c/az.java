package android.support.v4.c;
public abstract class az extends android.content.BroadcastReceiver {
    private static final String a = "android.support.content.wakelockid";
    private static final android.util.SparseArray b;
    private static int c;

    static az()
    {
        android.support.v4.c.az.b = new android.util.SparseArray();
        android.support.v4.c.az.c = 1;
        return;
    }

    public az()
    {
        return;
    }

    private static android.content.ComponentName a(android.content.Context p7, android.content.Intent p8)
    {
        try {
            int v3 = android.support.v4.c.az.c;
            android.content.ComponentName v0_1 = (android.support.v4.c.az.c + 1);
            android.support.v4.c.az.c = v0_1;
        } catch (android.content.ComponentName v0_9) {
            throw v0_9;
        }
        if (v0_1 <= null) {
            android.support.v4.c.az.c = 1;
        }
        android.content.ComponentName v0_8;
        p8.putExtra("android.support.content.wakelockid", v3);
        android.content.ComponentName v1 = p7.startService(p8);
        if (v1 != null) {
            android.content.ComponentName v0_7 = ((android.os.PowerManager) p7.getSystemService("power")).newWakeLock(1, new StringBuilder("wake:").append(v1.flattenToShortString()).toString());
            v0_7.setReferenceCounted(0);
            v0_7.acquire(60000);
            android.support.v4.c.az.b.put(v3, v0_7);
            v0_8 = v1;
        } else {
            v0_8 = 0;
        }
        return v0_8;
    }

    private static boolean a(android.content.Intent p6)
    {
        int v0_0 = 0;
        String v2_1 = p6.getIntExtra("android.support.content.wakelockid", 0);
        if (v2_1 != null) {
            try {
                int v0_3 = ((android.os.PowerManager$WakeLock) android.support.v4.c.az.b.get(v2_1));
            } catch (int v0_6) {
                throw v0_6;
            }
            if (v0_3 == 0) {
                android.util.Log.w("WakefulBroadcastReceiver", new StringBuilder("No active wake lock id #").append(v2_1).toString());
                v0_0 = 1;
            } else {
                v0_3.release();
                android.support.v4.c.az.b.remove(v2_1);
                v0_0 = 1;
            }
        }
        return v0_0;
    }
}
