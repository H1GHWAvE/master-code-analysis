package android.support.v4.c;
final class al extends java.util.concurrent.FutureTask {
    final synthetic android.support.v4.c.ai a;

    al(android.support.v4.c.ai p1, java.util.concurrent.Callable p2)
    {
        this.a = p1;
        this(p2);
        return;
    }

    protected final void done()
    {
        try {
            android.support.v4.c.ai.b(this.a, this.get());
        } catch (Throwable v0_1) {
            throw new RuntimeException("An error occurred while executing doInBackground()", v0_1);
        } catch (Throwable v0) {
            android.support.v4.c.ai.b(this.a, 0);
        } catch (Throwable v0_5) {
            android.util.Log.w("AsyncTask", v0_5);
        } catch (Throwable v0_3) {
            throw new RuntimeException("An error occurred while executing doInBackground()", v0_3.getCause());
        }
        return;
    }
}
