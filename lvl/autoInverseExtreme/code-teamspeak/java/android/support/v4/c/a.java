package android.support.v4.c;
public abstract class a extends android.support.v4.c.aa {
    static final String a = "AsyncTaskLoader";
    static final boolean b;
    volatile android.support.v4.c.b c;
    volatile android.support.v4.c.b d;
    long e;
    long f;
    android.os.Handler g;
    private final java.util.concurrent.Executor h;

    public a(android.content.Context p2)
    {
        this(p2, android.support.v4.c.ai.d);
        return;
    }

    private a(android.content.Context p3, java.util.concurrent.Executor p4)
    {
        this(p3);
        this.f = -10000;
        this.h = p4;
        return;
    }

    private void a(long p4)
    {
        this.e = p4;
        if (p4 != 0) {
            this.g = new android.os.Handler();
        }
        return;
    }

    private void b(android.support.v4.c.b p3, Object p4)
    {
        if (this.c == p3) {
            if (!this.u) {
                this.x = 0;
                this.f = android.os.SystemClock.uptimeMillis();
                this.c = 0;
                this.b(p4);
            } else {
                this.a(p4);
            }
        } else {
            this.a(p3, p4);
        }
        return;
    }

    private Object o()
    {
        return this.d();
    }

    private boolean p()
    {
        int v0_1;
        if (this.d == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    private void q()
    {
        InterruptedException v0_0 = this.c;
        if (v0_0 != null) {
            try {
                v0_0.a.await();
            } catch (InterruptedException v0) {
            }
        }
        return;
    }

    protected final void a()
    {
        super.a();
        this.j();
        this.c = new android.support.v4.c.b(this);
        this.c();
        return;
    }

    final void a(android.support.v4.c.b p3, Object p4)
    {
        this.a(p4);
        if (this.d == p3) {
            if (this.x) {
                this.w = 1;
            }
            this.f = android.os.SystemClock.uptimeMillis();
            this.d = 0;
            if (this.r != null) {
                this.r.d();
            }
            this.c();
        }
        return;
    }

    public void a(Object p1)
    {
        return;
    }

    public void a(String p5, java.io.FileDescriptor p6, java.io.PrintWriter p7, String[] p8)
    {
        super.a(p5, p6, p7, p8);
        if (this.c != null) {
            p7.print(p5);
            p7.print("mTask=");
            p7.print(this.c);
            p7.print(" waiting=");
            p7.println(this.c.b);
        }
        if (this.d != null) {
            p7.print(p5);
            p7.print("mCancellingTask=");
            p7.print(this.d);
            p7.print(" waiting=");
            p7.println(this.d.b);
        }
        if (this.e != 0) {
            p7.print(p5);
            p7.print("mUpdateThrottle=");
            android.support.v4.n.x.a(this.e, p7);
            p7.print(" mLastLoadCompleteTime=");
            android.support.v4.n.x.a(this.f, android.os.SystemClock.uptimeMillis(), p7);
            p7.println();
        }
        return;
    }

    protected final boolean b()
    {
        boolean v0 = 0;
        if (this.c != null) {
            if (this.d == null) {
                if (!this.c.b) {
                    v0 = this.c.e.cancel(0);
                    if (v0) {
                        this.d = this.c;
                        this.e();
                    }
                    this.c = 0;
                } else {
                    this.c.b = 0;
                    this.g.removeCallbacks(this.c);
                    this.c = 0;
                }
            } else {
                if (this.c.b) {
                    this.c.b = 0;
                    this.g.removeCallbacks(this.c);
                }
                this.c = 0;
            }
        }
        return v0;
    }

    final void c()
    {
        if ((this.d == null) && (this.c != null)) {
            if (this.c.b) {
                this.c.b = 0;
                this.g.removeCallbacks(this.c);
            }
            if ((this.e <= 0) || (android.os.SystemClock.uptimeMillis() >= (this.f + this.e))) {
                this.c.a(this.h, 0);
            } else {
                this.c.b = 1;
                this.g.postAtTime(this.c, (this.f + this.e));
            }
        }
        return;
    }

    public abstract Object d();

    public void e()
    {
        return;
    }
}
