package android.support.v4.f.a;
public abstract class a {
    public static final String a = "android.hardware.display.category.PRESENTATION";
    private static final java.util.WeakHashMap b;

    static a()
    {
        android.support.v4.f.a.a.b = new java.util.WeakHashMap();
        return;
    }

    a()
    {
        return;
    }

    private static android.support.v4.f.a.a a(android.content.Context p3)
    {
        try {
            android.support.v4.f.a.c v0_2 = ((android.support.v4.f.a.a) android.support.v4.f.a.a.b.get(p3));
        } catch (android.support.v4.f.a.c v0_6) {
            throw v0_6;
        }
        if (v0_2 == null) {
            if (android.os.Build$VERSION.SDK_INT < 17) {
                v0_2 = new android.support.v4.f.a.c(p3);
            } else {
                v0_2 = new android.support.v4.f.a.b(p3);
            }
            android.support.v4.f.a.a.b.put(p3, v0_2);
        }
        return v0_2;
    }

    public abstract android.view.Display a();

    public abstract android.view.Display[] a();

    public abstract android.view.Display[] a();
}
