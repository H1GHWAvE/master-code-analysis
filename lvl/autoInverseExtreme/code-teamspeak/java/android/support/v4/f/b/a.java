package android.support.v4.f.b;
public final class a {
    static final android.support.v4.f.b.g a;
    private android.content.Context b;

    static a()
    {
        if (android.os.Build$VERSION.SDK_INT < 23) {
            android.support.v4.f.b.a.a = new android.support.v4.f.b.h();
        } else {
            android.support.v4.f.b.a.a = new android.support.v4.f.b.b();
        }
        return;
    }

    private a(android.content.Context p1)
    {
        this.b = p1;
        return;
    }

    private static android.support.v4.f.b.a a(android.content.Context p1)
    {
        return new android.support.v4.f.b.a(p1);
    }

    private void a(android.support.v4.f.b.f p8, int p9, android.support.v4.i.c p10, android.support.v4.f.b.d p11, android.os.Handler p12)
    {
        android.support.v4.f.b.a.a.a(this.b, p8, p9, p10, p11, p12);
        return;
    }

    private boolean a()
    {
        return android.support.v4.f.b.a.a.a(this.b);
    }

    private boolean b()
    {
        return android.support.v4.f.b.a.a.b(this.b);
    }
}
