package android.support.v4.widget;
public interface ef {

    public abstract android.content.res.ColorStateList getSupportButtonTintList();

    public abstract android.graphics.PorterDuff$Mode getSupportButtonTintMode();

    public abstract void setSupportButtonTintList();

    public abstract void setSupportButtonTintMode();
}
