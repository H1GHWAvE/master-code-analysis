package android.support.v4.widget;
final class dd extends android.support.v4.widget.ej {
    final synthetic android.support.v4.widget.SlidingPaneLayout a;

    private dd(android.support.v4.widget.SlidingPaneLayout p1)
    {
        this.a = p1;
        return;
    }

    synthetic dd(android.support.v4.widget.SlidingPaneLayout p1, byte p2)
    {
        this(p1);
        return;
    }

    public final int a(android.view.View p4, int p5)
    {
        int v0_7;
        int v0_3 = ((android.support.v4.widget.de) android.support.v4.widget.SlidingPaneLayout.d(this.a).getLayoutParams());
        if (!android.support.v4.widget.SlidingPaneLayout.e(this.a)) {
            int v0_5 = (v0_3.leftMargin + this.a.getPaddingLeft());
            v0_7 = Math.min(Math.max(p5, v0_5), (android.support.v4.widget.SlidingPaneLayout.f(this.a) + v0_5));
        } else {
            int v0_11 = (this.a.getWidth() - ((v0_3.rightMargin + this.a.getPaddingRight()) + android.support.v4.widget.SlidingPaneLayout.d(this.a).getWidth()));
            v0_7 = Math.max(Math.min(p5, v0_11), (v0_11 - android.support.v4.widget.SlidingPaneLayout.f(this.a)));
        }
        return v0_7;
    }

    public final void a(int p4)
    {
        if (android.support.v4.widget.SlidingPaneLayout.b(this.a).m == 0) {
            if (android.support.v4.widget.SlidingPaneLayout.c(this.a) != 0) {
                android.support.v4.widget.SlidingPaneLayout v0_6 = this.a;
                android.support.v4.widget.SlidingPaneLayout.d(this.a);
                v0_6.sendAccessibilityEvent(32);
                android.support.v4.widget.SlidingPaneLayout.a(this.a, 1);
            } else {
                this.a.a(android.support.v4.widget.SlidingPaneLayout.d(this.a));
                android.support.v4.widget.SlidingPaneLayout v0_9 = this.a;
                android.support.v4.widget.SlidingPaneLayout.d(this.a);
                v0_9.sendAccessibilityEvent(32);
                android.support.v4.widget.SlidingPaneLayout.a(this.a, 0);
            }
        }
        return;
    }

    public final void a(int p3, int p4)
    {
        android.support.v4.widget.SlidingPaneLayout.b(this.a).a(android.support.v4.widget.SlidingPaneLayout.d(this.a), p4);
        return;
    }

    public final void a(android.view.View p5, float p6)
    {
        android.support.v4.widget.SlidingPaneLayout v0_3;
        android.support.v4.widget.SlidingPaneLayout v0_1 = ((android.support.v4.widget.de) p5.getLayoutParams());
        if (!android.support.v4.widget.SlidingPaneLayout.e(this.a)) {
            v0_3 = (v0_1.leftMargin + this.a.getPaddingLeft());
            if ((p6 > 0) || ((p6 == 0) && (android.support.v4.widget.SlidingPaneLayout.c(this.a) > 1056964608))) {
                v0_3 += android.support.v4.widget.SlidingPaneLayout.f(this.a);
            }
        } else {
            android.support.v4.widget.SlidingPaneLayout v0_5 = (v0_1.rightMargin + this.a.getPaddingRight());
            if ((p6 < 0) || ((p6 == 0) && (android.support.v4.widget.SlidingPaneLayout.c(this.a) > 1056964608))) {
                v0_5 += android.support.v4.widget.SlidingPaneLayout.f(this.a);
            }
            v0_3 = ((this.a.getWidth() - v0_5) - android.support.v4.widget.SlidingPaneLayout.d(this.a).getWidth());
        }
        android.support.v4.widget.SlidingPaneLayout.b(this.a).a(v0_3, p5.getTop());
        this.a.invalidate();
        return;
    }

    public final boolean a(android.view.View p2)
    {
        boolean v0_4;
        if (!android.support.v4.widget.SlidingPaneLayout.a(this.a)) {
            v0_4 = ((android.support.v4.widget.de) p2.getLayoutParams()).b;
        } else {
            v0_4 = 0;
        }
        return v0_4;
    }

    public final int b(android.view.View p2)
    {
        return android.support.v4.widget.SlidingPaneLayout.f(this.a);
    }

    public final void b(android.view.View p2, int p3)
    {
        android.support.v4.widget.SlidingPaneLayout.a(this.a, p3);
        this.a.invalidate();
        return;
    }

    public final int c(android.view.View p2)
    {
        return p2.getTop();
    }

    public final void d(android.view.View p2)
    {
        this.a.a();
        return;
    }
}
