package android.support.v4.widget;
public final class bj {
    static final android.support.v4.widget.bm a;

    static bj()
    {
        if (android.os.Build$VERSION.SDK_INT < 19) {
            android.support.v4.widget.bj.a = new android.support.v4.widget.bk();
        } else {
            android.support.v4.widget.bj.a = new android.support.v4.widget.bl();
        }
        return;
    }

    private bj()
    {
        return;
    }

    private static android.view.View$OnTouchListener a(Object p1)
    {
        return android.support.v4.widget.bj.a.a(p1);
    }
}
