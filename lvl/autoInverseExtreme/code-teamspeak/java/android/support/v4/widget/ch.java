package android.support.v4.widget;
public final class ch {
    private static final android.support.v4.widget.co a;

    static ch()
    {
        if (android.os.Build$VERSION.SDK_INT < 14) {
            if (android.os.Build$VERSION.SDK_INT < 11) {
                android.support.v4.widget.ch.a = new android.support.v4.widget.cp();
            } else {
                android.support.v4.widget.ch.a = new android.support.v4.widget.ck();
            }
        } else {
            android.support.v4.widget.ch.a = new android.support.v4.widget.cn();
        }
        return;
    }

    private ch()
    {
        return;
    }

    static synthetic android.support.v4.widget.co a()
    {
        return android.support.v4.widget.ch.a;
    }

    private static android.view.View a(android.content.Context p1)
    {
        return android.support.v4.widget.ch.a.a(p1);
    }

    private static CharSequence a(android.view.View p1)
    {
        return android.support.v4.widget.ch.a.a(p1);
    }

    private static void a(android.view.View p1, int p2)
    {
        android.support.v4.widget.ch.a.b(p1, p2);
        return;
    }

    private static void a(android.view.View p1, android.content.ComponentName p2)
    {
        android.support.v4.widget.ch.a.a(p1, p2);
        return;
    }

    private static void a(android.view.View p2, android.support.v4.widget.ci p3)
    {
        android.support.v4.widget.ch.a.b(p2, p3.a);
        return;
    }

    private static void a(android.view.View p2, android.support.v4.widget.cj p3)
    {
        android.support.v4.widget.ch.a.a(p2, p3.a);
        return;
    }

    private static void a(android.view.View p1, CharSequence p2)
    {
        android.support.v4.widget.ch.a.a(p1, p2);
        return;
    }

    private static void a(android.view.View p1, CharSequence p2, boolean p3)
    {
        android.support.v4.widget.ch.a.a(p1, p2, p3);
        return;
    }

    private static void a(android.view.View p1, boolean p2)
    {
        android.support.v4.widget.ch.a.a(p1, p2);
        return;
    }

    private static void b(android.view.View p1, int p2)
    {
        android.support.v4.widget.ch.a.c(p1, p2);
        return;
    }

    private static void b(android.view.View p1, boolean p2)
    {
        android.support.v4.widget.ch.a.b(p1, p2);
        return;
    }

    private static boolean b(android.view.View p1)
    {
        return android.support.v4.widget.ch.a.b(p1);
    }

    private static void c(android.view.View p1, int p2)
    {
        android.support.v4.widget.ch.a.a(p1, p2);
        return;
    }

    private static void c(android.view.View p1, boolean p2)
    {
        android.support.v4.widget.ch.a.c(p1, p2);
        return;
    }

    private static boolean c(android.view.View p1)
    {
        return android.support.v4.widget.ch.a.c(p1);
    }

    private static boolean d(android.view.View p1)
    {
        return android.support.v4.widget.ch.a.d(p1);
    }
}
