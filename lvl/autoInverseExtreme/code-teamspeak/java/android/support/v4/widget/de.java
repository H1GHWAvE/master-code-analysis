package android.support.v4.widget;
public final class de extends android.view.ViewGroup$MarginLayoutParams {
    private static final int[] e;
    public float a;
    boolean b;
    boolean c;
    android.graphics.Paint d;

    static de()
    {
        int[] v0_1 = new int[1];
        v0_1[0] = 16843137;
        android.support.v4.widget.de.e = v0_1;
        return;
    }

    public de()
    {
        this(-1, -1);
        this.a = 0;
        return;
    }

    private de(int p2, int p3)
    {
        this(p2, p3);
        this.a = 0;
        return;
    }

    public de(android.content.Context p4, android.util.AttributeSet p5)
    {
        this(p4, p5);
        this.a = 0;
        android.content.res.TypedArray v0_1 = p4.obtainStyledAttributes(p5, android.support.v4.widget.de.e);
        this.a = v0_1.getFloat(0, 0);
        v0_1.recycle();
        return;
    }

    private de(android.support.v4.widget.de p2)
    {
        this(p2);
        this.a = 0;
        this.a = p2.a;
        return;
    }

    public de(android.view.ViewGroup$LayoutParams p2)
    {
        this(p2);
        this.a = 0;
        return;
    }

    public de(android.view.ViewGroup$MarginLayoutParams p2)
    {
        this(p2);
        this.a = 0;
        return;
    }
}
