package android.support.v4.widget;
 class ck extends android.support.v4.widget.cp {

    ck()
    {
        return;
    }

    public android.view.View a(android.content.Context p2)
    {
        return new android.widget.SearchView(p2);
    }

    public final CharSequence a(android.view.View p2)
    {
        return ((android.widget.SearchView) p2).getQuery();
    }

    public final Object a(android.support.v4.widget.ci p3)
    {
        return new android.support.v4.widget.cs(new android.support.v4.widget.cm(this, p3));
    }

    public final Object a(android.support.v4.widget.cj p3)
    {
        return new android.support.v4.widget.cr(new android.support.v4.widget.cl(this, p3));
    }

    public final void a(android.view.View p1, int p2)
    {
        ((android.widget.SearchView) p1).setMaxWidth(p2);
        return;
    }

    public final void a(android.view.View p3, android.content.ComponentName p4)
    {
        ((android.widget.SearchView) p3).setSearchableInfo(((android.app.SearchManager) ((android.widget.SearchView) p3).getContext().getSystemService("search")).getSearchableInfo(p4));
        return;
    }

    public final void a(android.view.View p1, CharSequence p2)
    {
        ((android.widget.SearchView) p1).setQueryHint(p2);
        return;
    }

    public final void a(android.view.View p1, CharSequence p2, boolean p3)
    {
        ((android.widget.SearchView) p1).setQuery(p2, p3);
        return;
    }

    public final void a(android.view.View p1, boolean p2)
    {
        ((android.widget.SearchView) p1).setIconified(p2);
        return;
    }

    public final void a(Object p1, Object p2)
    {
        ((android.widget.SearchView) p1).setOnQueryTextListener(((android.widget.SearchView$OnQueryTextListener) p2));
        return;
    }

    public final void b(android.view.View p1, boolean p2)
    {
        ((android.widget.SearchView) p1).setSubmitButtonEnabled(p2);
        return;
    }

    public final void b(Object p1, Object p2)
    {
        ((android.widget.SearchView) p1).setOnCloseListener(((android.widget.SearchView$OnCloseListener) p2));
        return;
    }

    public final boolean b(android.view.View p2)
    {
        return ((android.widget.SearchView) p2).isIconified();
    }

    public final void c(android.view.View p1, boolean p2)
    {
        ((android.widget.SearchView) p1).setQueryRefinementEnabled(p2);
        return;
    }

    public final boolean c(android.view.View p2)
    {
        return ((android.widget.SearchView) p2).isSubmitButtonEnabled();
    }

    public final boolean d(android.view.View p2)
    {
        return ((android.widget.SearchView) p2).isQueryRefinementEnabled();
    }
}
