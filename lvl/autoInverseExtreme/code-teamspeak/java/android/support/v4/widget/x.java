package android.support.v4.widget;
final class x extends android.support.v4.view.a {
    final synthetic android.support.v4.widget.DrawerLayout a;
    private final android.graphics.Rect c;

    x(android.support.v4.widget.DrawerLayout p2)
    {
        this.a = p2;
        this.c = new android.graphics.Rect();
        return;
    }

    private void a(android.support.v4.view.a.q p2, android.support.v4.view.a.q p3)
    {
        int v0_0 = this.c;
        p3.a(v0_0);
        p2.b(v0_0);
        p3.c(v0_0);
        p2.d(v0_0);
        p2.c(p3.e());
        p2.a(p3.k());
        p2.b(p3.l());
        p2.c(p3.n());
        p2.h(p3.j());
        p2.f(p3.h());
        p2.a(p3.c());
        p2.b(p3.d());
        p2.d(p3.f());
        p2.e(p3.g());
        p2.g(p3.i());
        p2.a(p3.b());
        return;
    }

    private static void a(android.support.v4.view.a.q p4, android.view.ViewGroup p5)
    {
        int v1 = p5.getChildCount();
        int v0 = 0;
        while (v0 < v1) {
            android.view.View v2 = p5.getChildAt(v0);
            if (android.support.v4.widget.DrawerLayout.f(v2)) {
                p4.c(v2);
            }
            v0++;
        }
        return;
    }

    public final void a(android.view.View p6, android.support.v4.view.a.q p7)
    {
        if (!android.support.v4.widget.DrawerLayout.f()) {
            int v2_0 = android.support.v4.view.a.q.a(p7);
            super.a(p6, v2_0);
            p7.b(p6);
            int v0_1 = android.support.v4.view.cx.g(p6);
            if ((v0_1 instanceof android.view.View)) {
                p7.d(((android.view.View) v0_1));
            }
            int v0_3 = this.c;
            v2_0.a(v0_3);
            p7.b(v0_3);
            v2_0.c(v0_3);
            p7.d(v0_3);
            p7.c(v2_0.e());
            p7.a(v2_0.k());
            p7.b(v2_0.l());
            p7.c(v2_0.n());
            p7.h(v2_0.j());
            p7.f(v2_0.h());
            p7.a(v2_0.c());
            p7.b(v2_0.d());
            p7.d(v2_0.f());
            p7.e(v2_0.g());
            p7.g(v2_0.i());
            p7.a(v2_0.b());
            v2_0.o();
            int v2_1 = ((android.view.ViewGroup) p6).getChildCount();
            int v0_16 = 0;
            while (v0_16 < v2_1) {
                android.view.View v3_1 = ((android.view.ViewGroup) p6).getChildAt(v0_16);
                if (android.support.v4.widget.DrawerLayout.f(v3_1)) {
                    p7.c(v3_1);
                }
                v0_16++;
            }
        } else {
            super.a(p6, p7);
        }
        p7.b(android.support.v4.widget.DrawerLayout.getName());
        p7.a(0);
        p7.b(0);
        p7.a(android.support.v4.view.a.s.a);
        p7.a(android.support.v4.view.a.s.b);
        return;
    }

    public final void a(android.view.View p2, android.view.accessibility.AccessibilityEvent p3)
    {
        super.a(p2, p3);
        p3.setClassName(android.support.v4.widget.DrawerLayout.getName());
        return;
    }

    public final boolean a(android.view.ViewGroup p2, android.view.View p3, android.view.accessibility.AccessibilityEvent p4)
    {
        if ((!android.support.v4.widget.DrawerLayout.f()) && (!android.support.v4.widget.DrawerLayout.f(p3))) {
            int v0_2 = 0;
        } else {
            v0_2 = super.a(p2, p3, p4);
        }
        return v0_2;
    }

    public final boolean d(android.view.View p5, android.view.accessibility.AccessibilityEvent p6)
    {
        int v0_1;
        if (p6.getEventType() != 32) {
            v0_1 = super.d(p5, p6);
        } else {
            java.util.List v1_1 = p6.getText();
            int v0_3 = android.support.v4.widget.DrawerLayout.a(this.a);
            if (v0_3 != 0) {
                int v0_6;
                android.support.v4.widget.DrawerLayout v2_1 = this.a;
                int v0_5 = android.support.v4.view.v.a(this.a.c(v0_3), android.support.v4.view.cx.f(v2_1));
                if (v0_5 != 3) {
                    if (v0_5 != 5) {
                        v0_6 = 0;
                    } else {
                        v0_6 = v2_1.m;
                    }
                } else {
                    v0_6 = v2_1.l;
                }
                if (v0_6 != 0) {
                    v1_1.add(v0_6);
                }
            }
            v0_1 = 1;
        }
        return v0_1;
    }
}
