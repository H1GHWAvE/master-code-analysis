package android.support.v4.widget;
final class dk extends android.support.v4.widget.dj {
    private reflect.Method a;
    private reflect.Field b;

    dk()
    {
        try {
            this.a = android.view.View.getDeclaredMethod("getDisplayList", 0);
            try {
                this.b = android.view.View.getDeclaredField("mRecreateDisplayList");
                this.b.setAccessible(1);
            } catch (NoSuchFieldException v0_6) {
                android.util.Log.e("SlidingPaneLayout", "Couldn\'t fetch mRecreateDisplayList field; dimming will be slow.", v0_6);
            }
            return;
        } catch (NoSuchFieldException v0_2) {
            android.util.Log.e("SlidingPaneLayout", "Couldn\'t fetch getDisplayList method; dimming won\'t work right.", v0_2);
        }
    }

    public final void a(android.support.v4.widget.SlidingPaneLayout p4, android.view.View p5)
    {
        if ((this.a == null) || (this.b == null)) {
            p5.invalidate();
        } else {
            try {
                this.b.setBoolean(p5, 1);
                this.a.invoke(p5, 0);
            } catch (Exception v0_4) {
                android.util.Log.e("SlidingPaneLayout", "Error refreshing display list state", v0_4);
            }
            super.a(p4, p5);
        }
        return;
    }
}
