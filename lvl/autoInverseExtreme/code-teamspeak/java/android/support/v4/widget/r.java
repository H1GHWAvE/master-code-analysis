package android.support.v4.widget;
public abstract class r extends android.widget.BaseAdapter implements android.support.v4.widget.w, android.widget.Filterable {
    public static final int j = 1;
    public static final int k = 2;
    protected boolean a;
    protected boolean b;
    public android.database.Cursor c;
    public android.content.Context d;
    protected int e;
    protected android.support.v4.widget.t f;
    protected android.database.DataSetObserver g;
    protected android.support.v4.widget.v h;
    protected android.widget.FilterQueryProvider i;

    public r(android.content.Context p3)
    {
        this.a(p3, 0, 1);
        return;
    }

    public r(android.content.Context p2, android.database.Cursor p3)
    {
        this.a(p2, p3, 1);
        return;
    }

    public r(android.content.Context p1, android.database.Cursor p2, int p3)
    {
        this.a(p1, p2, p3);
        return;
    }

    private void a(android.content.Context p6, android.database.Cursor p7, int p8)
    {
        android.database.DataSetObserver v0_0 = 1;
        if ((p8 & 1) != 1) {
            this.b = 0;
        } else {
            p8 |= 2;
            this.b = 1;
        }
        if (p7 == null) {
            v0_0 = 0;
        }
        android.support.v4.widget.u v2_1;
        this.c = p7;
        this.a = v0_0;
        this.d = p6;
        if (v0_0 == null) {
            v2_1 = -1;
        } else {
            v2_1 = p7.getColumnIndexOrThrow("_id");
        }
        this.e = v2_1;
        if ((p8 & 2) != 2) {
            this.f = 0;
            this.g = 0;
        } else {
            this.f = new android.support.v4.widget.t(this);
            this.g = new android.support.v4.widget.u(this, 0);
        }
        if (v0_0 != null) {
            if (this.f != null) {
                p7.registerContentObserver(this.f);
            }
            if (this.g != null) {
                p7.registerDataSetObserver(this.g);
            }
        }
        return;
    }

    private void a(android.content.Context p2, android.database.Cursor p3, boolean p4)
    {
        int v0;
        if (!p4) {
            v0 = 2;
        } else {
            v0 = 1;
        }
        this.a(p2, p3, v0);
        return;
    }

    private void a(android.widget.FilterQueryProvider p1)
    {
        this.i = p1;
        return;
    }

    private android.widget.FilterQueryProvider c()
    {
        return this.i;
    }

    public final android.database.Cursor a()
    {
        return this.c;
    }

    public android.database.Cursor a(CharSequence p2)
    {
        android.database.Cursor v0_1;
        if (this.i == null) {
            v0_1 = this.c;
        } else {
            v0_1 = this.i.runQuery(p2);
        }
        return v0_1;
    }

    public abstract android.view.View a();

    public void a(android.database.Cursor p2)
    {
        android.database.Cursor v0 = this.b(p2);
        if (v0 != null) {
            v0.close();
        }
        return;
    }

    public abstract void a();

    public android.database.Cursor b(android.database.Cursor p3)
    {
        android.database.Cursor v0_1;
        if (p3 != this.c) {
            v0_1 = this.c;
            if (v0_1 != null) {
                if (this.f != null) {
                    v0_1.unregisterContentObserver(this.f);
                }
                if (this.g != null) {
                    v0_1.unregisterDataSetObserver(this.g);
                }
            }
            this.c = p3;
            if (p3 == null) {
                this.e = -1;
                this.a = 0;
                this.notifyDataSetInvalidated();
            } else {
                if (this.f != null) {
                    p3.registerContentObserver(this.f);
                }
                if (this.g != null) {
                    p3.registerDataSetObserver(this.g);
                }
                this.e = p3.getColumnIndexOrThrow("_id");
                this.a = 1;
                this.notifyDataSetChanged();
            }
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    public android.view.View b(android.content.Context p2, android.database.Cursor p3, android.view.ViewGroup p4)
    {
        return this.a(p2, p3, p4);
    }

    protected final void b()
    {
        if ((this.b) && ((this.c != null) && (!this.c.isClosed()))) {
            this.a = this.c.requery();
        }
        return;
    }

    public CharSequence c(android.database.Cursor p2)
    {
        String v0;
        if (p2 != null) {
            v0 = p2.toString();
        } else {
            v0 = "";
        }
        return v0;
    }

    public int getCount()
    {
        if ((!this.a) || (this.c == null)) {
            int v0_2 = 0;
        } else {
            v0_2 = this.c.getCount();
        }
        return v0_2;
    }

    public android.view.View getDropDownView(int p3, android.view.View p4, android.view.ViewGroup p5)
    {
        if (!this.a) {
            p4 = 0;
        } else {
            this.c.moveToPosition(p3);
            if (p4 == null) {
                p4 = this.b(this.d, this.c, p5);
            }
            this.a(p4, this.c);
        }
        return p4;
    }

    public android.widget.Filter getFilter()
    {
        if (this.h == null) {
            this.h = new android.support.v4.widget.v(this);
        }
        return this.h;
    }

    public Object getItem(int p2)
    {
        if ((!this.a) || (this.c == null)) {
            android.database.Cursor v0_2 = 0;
        } else {
            this.c.moveToPosition(p2);
            v0_2 = this.c;
        }
        return v0_2;
    }

    public long getItemId(int p4)
    {
        long v0_0 = 0;
        if ((this.a) && ((this.c != null) && (this.c.moveToPosition(p4)))) {
            v0_0 = this.c.getLong(this.e);
        }
        return v0_0;
    }

    public android.view.View getView(int p4, android.view.View p5, android.view.ViewGroup p6)
    {
        if (this.a) {
            if (this.c.moveToPosition(p4)) {
                if (p5 == null) {
                    p5 = this.a(this.d, this.c, p6);
                }
                this.a(p5, this.c);
                return p5;
            } else {
                throw new IllegalStateException(new StringBuilder("couldn\'t move cursor to position ").append(p4).toString());
            }
        } else {
            throw new IllegalStateException("this should only be called when the cursor is valid");
        }
    }

    public boolean hasStableIds()
    {
        return 1;
    }
}
