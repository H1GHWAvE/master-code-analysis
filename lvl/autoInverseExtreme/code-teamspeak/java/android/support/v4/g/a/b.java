package android.support.v4.g.a;
public interface b implements android.view.MenuItem {
    public static final int a = 0;
    public static final int b = 1;
    public static final int c = 2;
    public static final int d = 4;
    public static final int e = 8;

    public abstract android.support.v4.g.a.b a();

    public abstract android.support.v4.g.a.b a();

    public abstract android.support.v4.view.n a();

    public abstract boolean collapseActionView();

    public abstract boolean expandActionView();

    public abstract android.view.View getActionView();

    public abstract boolean isActionViewExpanded();

    public abstract android.view.MenuItem setActionView();

    public abstract android.view.MenuItem setActionView();

    public abstract void setShowAsAction();

    public abstract android.view.MenuItem setShowAsActionFlags();
}
