package android.support.v4.b;
final class j implements android.support.v4.b.l {
    final android.animation.Animator a;

    public j(android.animation.Animator p1)
    {
        this.a = p1;
        return;
    }

    public final void a()
    {
        this.a.start();
        return;
    }

    public final void a(long p2)
    {
        this.a.setDuration(p2);
        return;
    }

    public final void a(android.support.v4.b.b p3)
    {
        this.a.addListener(new android.support.v4.b.i(p3, this));
        return;
    }

    public final void a(android.support.v4.b.d p3)
    {
        if ((this.a instanceof android.animation.ValueAnimator)) {
            ((android.animation.ValueAnimator) this.a).addUpdateListener(new android.support.v4.b.k(this, p3));
        }
        return;
    }

    public final void a(android.view.View p2)
    {
        this.a.setTarget(p2);
        return;
    }

    public final void c()
    {
        this.a.cancel();
        return;
    }

    public final float d()
    {
        return ((android.animation.ValueAnimator) this.a).getAnimatedFraction();
    }
}
