package android.support.v4.media;
public final class n {

    public n()
    {
        return;
    }

    private static Object a()
    {
        return new android.media.MediaMetadata$Builder();
    }

    private static Object a(Object p1)
    {
        return ((android.media.MediaMetadata$Builder) p1).build();
    }

    private static void a(Object p0, String p1, long p2)
    {
        ((android.media.MediaMetadata$Builder) p0).putLong(p1, p2);
        return;
    }

    private static void a(Object p0, String p1, android.graphics.Bitmap p2)
    {
        ((android.media.MediaMetadata$Builder) p0).putBitmap(p1, p2);
        return;
    }

    private static void a(Object p0, String p1, CharSequence p2)
    {
        ((android.media.MediaMetadata$Builder) p0).putText(p1, p2);
        return;
    }

    private static void a(Object p0, String p1, Object p2)
    {
        ((android.media.MediaMetadata$Builder) p0).putRating(p1, ((android.media.Rating) p2));
        return;
    }

    private static void a(Object p0, String p1, String p2)
    {
        ((android.media.MediaMetadata$Builder) p0).putString(p1, p2);
        return;
    }
}
