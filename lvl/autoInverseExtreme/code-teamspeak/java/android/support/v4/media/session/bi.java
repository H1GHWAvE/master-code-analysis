package android.support.v4.media.session;
final class bi implements android.os.Parcelable$Creator {

    bi()
    {
        return;
    }

    private static android.support.v4.media.session.ParcelableVolumeInfo a(android.os.Parcel p1)
    {
        return new android.support.v4.media.session.ParcelableVolumeInfo(p1);
    }

    private static android.support.v4.media.session.ParcelableVolumeInfo[] a(int p1)
    {
        android.support.v4.media.session.ParcelableVolumeInfo[] v0 = new android.support.v4.media.session.ParcelableVolumeInfo[p1];
        return v0;
    }

    public final synthetic Object createFromParcel(android.os.Parcel p2)
    {
        return new android.support.v4.media.session.ParcelableVolumeInfo(p2);
    }

    public final bridge synthetic Object[] newArray(int p2)
    {
        android.support.v4.media.session.ParcelableVolumeInfo[] v0 = new android.support.v4.media.session.ParcelableVolumeInfo[p2];
        return v0;
    }
}
