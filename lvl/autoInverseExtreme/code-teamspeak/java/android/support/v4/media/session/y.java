package android.support.v4.media.session;
public final class y {
    private static final int a = 4;
    private static final int b = 6;
    private static final int c = 7;

    public y()
    {
        return;
    }

    private static int a(android.media.AudioAttributes p4)
    {
        int v0 = 3;
        if ((p4.getFlags() & 1) != 1) {
            if ((p4.getFlags() & 4) != 4) {
                switch (p4.getUsage()) {
                    case 1:
                    case 11:
                    case 12:
                    case 14:
                        break;
                    case 2:
                        v0 = 0;
                        break;
                    case 3:
                        v0 = 8;
                        break;
                    case 4:
                        v0 = 4;
                        break;
                    case 5:
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                        v0 = 5;
                        break;
                    case 6:
                        v0 = 2;
                        break;
                    case 1:
                    case 11:
                    case 12:
                    case 14:
                        break;
                    case 1:
                    case 11:
                    case 12:
                    case 14:
                        break;
                    case 13:
                        v0 = 1;
                    case 1:
                    case 11:
                    case 12:
                    case 14:
                        break;
                    default:
                }
            } else {
                v0 = 6;
            }
        } else {
            v0 = 7;
        }
        return v0;
    }

    private static int a(Object p1)
    {
        return ((android.media.session.MediaController$PlaybackInfo) p1).getPlaybackType();
    }

    private static android.media.AudioAttributes b(Object p1)
    {
        return ((android.media.session.MediaController$PlaybackInfo) p1).getAudioAttributes();
    }

    private static int c(Object p5)
    {
        int v0 = 3;
        int v3_0 = ((android.media.session.MediaController$PlaybackInfo) p5).getAudioAttributes();
        if ((v3_0.getFlags() & 1) != 1) {
            if ((v3_0.getFlags() & 4) != 4) {
                switch (v3_0.getUsage()) {
                    case 1:
                    case 11:
                    case 12:
                    case 14:
                        break;
                    case 2:
                        v0 = 0;
                        break;
                    case 3:
                        v0 = 8;
                        break;
                    case 4:
                        v0 = 4;
                        break;
                    case 5:
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                        v0 = 5;
                        break;
                    case 6:
                        v0 = 2;
                        break;
                    case 1:
                    case 11:
                    case 12:
                    case 14:
                        break;
                    case 1:
                    case 11:
                    case 12:
                    case 14:
                        break;
                    case 13:
                        v0 = 1;
                    case 1:
                    case 11:
                    case 12:
                    case 14:
                        break;
                    default:
                }
            } else {
                v0 = 6;
            }
        } else {
            v0 = 7;
        }
        return v0;
    }

    private static int d(Object p1)
    {
        return ((android.media.session.MediaController$PlaybackInfo) p1).getVolumeControl();
    }

    private static int e(Object p1)
    {
        return ((android.media.session.MediaController$PlaybackInfo) p1).getMaxVolume();
    }

    private static int f(Object p1)
    {
        return ((android.media.session.MediaController$PlaybackInfo) p1).getCurrentVolume();
    }
}
