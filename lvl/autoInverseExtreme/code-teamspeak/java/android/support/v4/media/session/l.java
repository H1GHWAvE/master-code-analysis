package android.support.v4.media.session;
final class l extends android.support.v4.media.session.b {
    final synthetic android.support.v4.media.session.i i;

    private l(android.support.v4.media.session.i p1)
    {
        this.i = p1;
        return;
    }

    synthetic l(android.support.v4.media.session.i p1, byte p2)
    {
        this(p1);
        return;
    }

    public final void a()
    {
        android.support.v4.media.session.i.a(this.i).a(8, 0);
        return;
    }

    public final void a(android.os.Bundle p3)
    {
        android.support.v4.media.session.i.a(this.i).a(7, p3);
        return;
    }

    public final void a(android.support.v4.media.MediaMetadataCompat p3)
    {
        android.support.v4.media.session.i.a(this.i).a(3, p3);
        return;
    }

    public final void a(android.support.v4.media.session.ParcelableVolumeInfo p7)
    {
        android.support.v4.media.session.q v0_0 = 0;
        if (p7 != null) {
            v0_0 = new android.support.v4.media.session.q(p7.a, p7.b, p7.c, p7.d, p7.e);
        }
        android.support.v4.media.session.i.a(this.i).a(4, v0_0);
        return;
    }

    public final void a(android.support.v4.media.session.PlaybackStateCompat p3)
    {
        android.support.v4.media.session.i.a(this.i).a(2, p3);
        return;
    }

    public final void a(CharSequence p3)
    {
        android.support.v4.media.session.i.a(this.i).a(6, p3);
        return;
    }

    public final void a(String p3, android.os.Bundle p4)
    {
        android.support.v4.media.session.i.a(this.i).a(1, p3);
        return;
    }

    public final void a(java.util.List p3)
    {
        android.support.v4.media.session.i.a(this.i).a(5, p3);
        return;
    }
}
