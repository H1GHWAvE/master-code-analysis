package android.support.v4.media;
final class u implements android.support.v4.media.w {
    final synthetic android.support.v4.media.t a;

    u(android.support.v4.media.t p1)
    {
        this.a = p1;
        return;
    }

    public final long a()
    {
        return this.a.b.e();
    }

    public final void a(int p10)
    {
        int v0 = 0;
        switch (p10) {
            case -1:
                v0 = 127;
                break;
        }
        if (v0 != 0) {
            long v2 = android.os.SystemClock.uptimeMillis();
            new android.view.KeyEvent(v2, v2, 0, 127, 0);
            new android.view.KeyEvent(v2, v2, 1, 127, 0);
        }
        return;
    }

    public final void a(android.view.KeyEvent p2)
    {
        p2.dispatch(this.a.t);
        return;
    }

    public final void b()
    {
        return;
    }
}
