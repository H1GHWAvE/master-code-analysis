package android.support.v4.media.session;
public final class bn {
    private final String a;
    private final CharSequence b;
    private final int c;
    private android.os.Bundle d;

    private bn(String p3, CharSequence p4, int p5)
    {
        if (!android.text.TextUtils.isEmpty(p3)) {
            if (!android.text.TextUtils.isEmpty(p4)) {
                if (p5 != 0) {
                    this.a = p3;
                    this.b = p4;
                    this.c = p5;
                    return;
                } else {
                    throw new IllegalArgumentException("You must specify an icon resource id to build a CustomAction.");
                }
            } else {
                throw new IllegalArgumentException("You must specify a name to build a CustomAction.");
            }
        } else {
            throw new IllegalArgumentException("You must specify an action to build a CustomAction.");
        }
    }

    private android.support.v4.media.session.PlaybackStateCompat$CustomAction a()
    {
        return new android.support.v4.media.session.PlaybackStateCompat$CustomAction(this.a, this.b, this.c, this.d, 0);
    }

    private android.support.v4.media.session.bn a(android.os.Bundle p1)
    {
        this.d = p1;
        return this;
    }
}
