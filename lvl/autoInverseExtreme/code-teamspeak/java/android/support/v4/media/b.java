package android.support.v4.media;
public final class b {
    String a;
    CharSequence b;
    CharSequence c;
    CharSequence d;
    android.graphics.Bitmap e;
    android.net.Uri f;
    android.os.Bundle g;
    android.net.Uri h;

    public b()
    {
        return;
    }

    private android.support.v4.media.b a(android.graphics.Bitmap p1)
    {
        this.e = p1;
        return this;
    }

    private android.support.v4.media.b a(android.net.Uri p1)
    {
        this.f = p1;
        return this;
    }

    private android.support.v4.media.b a(android.os.Bundle p1)
    {
        this.g = p1;
        return this;
    }

    private android.support.v4.media.b a(CharSequence p1)
    {
        this.b = p1;
        return this;
    }

    private android.support.v4.media.b a(String p1)
    {
        this.a = p1;
        return this;
    }

    private android.support.v4.media.b b(android.net.Uri p1)
    {
        this.h = p1;
        return this;
    }

    private android.support.v4.media.b b(CharSequence p1)
    {
        this.c = p1;
        return this;
    }

    private android.support.v4.media.b c(CharSequence p1)
    {
        this.d = p1;
        return this;
    }

    public final android.support.v4.media.MediaDescriptionCompat a()
    {
        return new android.support.v4.media.MediaDescriptionCompat(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, 0);
    }
}
