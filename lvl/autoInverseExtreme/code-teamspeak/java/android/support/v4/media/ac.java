package android.support.v4.media;
public abstract class ac {
    static final int a = 1;
    static final int b = 2;
    static final int c = 3;
    static final int d = 255;
    static final int e = 254;
    static final int f = 253;

    public ac()
    {
        return;
    }

    private static boolean a(int p1)
    {
        switch (p1) {
            case 86:
            case 126:
            case 127:
            default:
                return 1;
        }
    }

    private static void b(int p9)
    {
        int v0 = 0;
        switch (p9) {
            case -1:
                v0 = 127;
                break;
        }
        if (v0 != 0) {
            long v2 = android.os.SystemClock.uptimeMillis();
            new android.view.KeyEvent(v2, v2, 0, 127, 0);
            new android.view.KeyEvent(v2, v2, 1, 127, 0);
        }
        return;
    }

    private static int h()
    {
        return 100;
    }

    private static int i()
    {
        return 60;
    }

    private static boolean j()
    {
        return 1;
    }

    public abstract void a();

    public abstract void b();

    public abstract void c();

    public abstract long d();

    public abstract long e();

    public abstract void f();

    public abstract boolean g();
}
