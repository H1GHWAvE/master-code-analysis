package android.support.v4.h;
public final class j {
    private static final android.support.v4.h.p a;

    static j()
    {
        if (android.os.Build$VERSION.SDK_INT < 14) {
            android.support.v4.h.j.a = new android.support.v4.h.l();
        } else {
            android.support.v4.h.j.a = new android.support.v4.h.o();
        }
        return;
    }

    public j()
    {
        return;
    }

    private static void a()
    {
        android.support.v4.h.j.a.a();
        return;
    }

    private static void a(int p1)
    {
        android.support.v4.h.j.a.a(p1);
        return;
    }

    private static void a(int p1, int p2)
    {
        android.support.v4.h.j.a.a(p1, p2);
        return;
    }

    private static void a(java.net.Socket p1)
    {
        android.support.v4.h.j.a.a(p1);
        return;
    }

    private static int b()
    {
        return android.support.v4.h.j.a.b();
    }

    private static void b(int p1)
    {
        android.support.v4.h.j.a.b(p1);
        return;
    }

    private static void b(java.net.Socket p1)
    {
        android.support.v4.h.j.a.b(p1);
        return;
    }
}
