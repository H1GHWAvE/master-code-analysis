package android.support.v4.n;
public final class d {
    private Object[] a;
    private int b;
    private int c;
    private int d;

    public d()
    {
        this(0);
        return;
    }

    private d(byte p4)
    {
        Object[] v0_0 = 8;
        if (Integer.bitCount(8) != 1) {
            v0_0 = (1 << (Integer.highestOneBit(8) + 1));
        }
        this.d = (v0_0 - 1);
        Object[] v0_3 = new Object[v0_0];
        this.a = ((Object[]) v0_3);
        return;
    }

    private void a()
    {
        String v1_0 = this.a.length;
        int v2 = (v1_0 - this.b);
        int v3 = (v1_0 << 1);
        if (v3 >= 0) {
            int v0_2 = new Object[v3];
            System.arraycopy(this.a, this.b, v0_2, 0, v2);
            System.arraycopy(this.a, 0, v0_2, v2, this.b);
            this.a = ((Object[]) v0_2);
            this.b = 0;
            this.c = v1_0;
            this.d = (v3 - 1);
            return;
        } else {
            throw new RuntimeException("Max array capacity exceeded");
        }
    }

    private void a(int p5)
    {
        if (p5 > 0) {
            if (p5 <= this.g()) {
                int v0_2 = this.a.length;
                if (p5 < (v0_2 - this.b)) {
                    v0_2 = (this.b + p5);
                }
                int v1_2 = this.b;
                while (v1_2 < v0_2) {
                    this.a[v1_2] = 0;
                    v1_2++;
                }
                int v0_4 = (v0_2 - this.b);
                int v1_4 = (p5 - v0_4);
                this.b = ((v0_4 + this.b) & this.d);
                if (v1_4 > 0) {
                    int v0_7 = 0;
                    while (v0_7 < v1_4) {
                        this.a[v0_7] = 0;
                        v0_7++;
                    }
                    this.b = v1_4;
                }
            } else {
                throw new ArrayIndexOutOfBoundsException();
            }
        }
        return;
    }

    private void a(Object p3)
    {
        this.b = ((this.b - 1) & this.d);
        this.a[this.b] = p3;
        if (this.b == this.c) {
            this.a();
        }
        return;
    }

    private Object b()
    {
        if (this.b != this.c) {
            Object v0_2 = this.a[this.b];
            this.a[this.b] = 0;
            this.b = ((this.b + 1) & this.d);
            return v0_2;
        } else {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    private void b(int p5)
    {
        if (p5 > 0) {
            if (p5 <= this.g()) {
                int v0_1 = 0;
                if (p5 < this.c) {
                    v0_1 = (this.c - p5);
                }
                int v1_1 = v0_1;
                while (v1_1 < this.c) {
                    this.a[v1_1] = 0;
                    v1_1++;
                }
                int v0_3 = (this.c - v0_1);
                int v1_3 = (p5 - v0_3);
                this.c = (this.c - v0_3);
                if (v1_3 > 0) {
                    this.c = this.a.length;
                    int v1_4 = (this.c - v1_3);
                    int v0_8 = v1_4;
                    while (v0_8 < this.c) {
                        this.a[v0_8] = 0;
                        v0_8++;
                    }
                    this.c = v1_4;
                }
            } else {
                throw new ArrayIndexOutOfBoundsException();
            }
        }
        return;
    }

    private void b(Object p3)
    {
        this.a[this.c] = p3;
        this.c = ((this.c + 1) & this.d);
        if (this.c == this.b) {
            this.a();
        }
        return;
    }

    private Object c()
    {
        if (this.b != this.c) {
            int v0_3 = ((this.c - 1) & this.d);
            Object v1_3 = this.a[v0_3];
            this.a[v0_3] = 0;
            this.c = v0_3;
            return v1_3;
        } else {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    private Object c(int p4)
    {
        if ((p4 >= 0) && (p4 < this.g())) {
            return this.a[((this.b + p4) & this.d)];
        } else {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    private void d()
    {
        Object[] v2_0 = this.g();
        if (v2_0 > null) {
            if (v2_0 <= this.g()) {
                int v0_2 = this.a.length;
                if (v2_0 < (v0_2 - this.b)) {
                    v0_2 = (this.b + v2_0);
                }
                int v1_2 = this.b;
                while (v1_2 < v0_2) {
                    this.a[v1_2] = 0;
                    v1_2++;
                }
                int v0_4 = (v0_2 - this.b);
                int v1_4 = (v2_0 - v0_4);
                this.b = ((v0_4 + this.b) & this.d);
                if (v1_4 > 0) {
                    int v0_7 = 0;
                    while (v0_7 < v1_4) {
                        this.a[v0_7] = 0;
                        v0_7++;
                    }
                    this.b = v1_4;
                }
            } else {
                throw new ArrayIndexOutOfBoundsException();
            }
        }
        return;
    }

    private Object e()
    {
        if (this.b != this.c) {
            return this.a[this.b];
        } else {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    private Object f()
    {
        if (this.b != this.c) {
            return this.a[((this.c - 1) & this.d)];
        } else {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    private int g()
    {
        return ((this.c - this.b) & this.d);
    }

    private boolean h()
    {
        int v0_1;
        if (this.b != this.c) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }
}
