package android.support.v4.n;
final class b extends android.support.v4.n.k {
    final synthetic android.support.v4.n.a a;

    b(android.support.v4.n.a p1)
    {
        this.a = p1;
        return;
    }

    protected final int a()
    {
        return this.a.h;
    }

    protected final int a(Object p2)
    {
        return this.a.a(p2);
    }

    protected final Object a(int p3, int p4)
    {
        return this.a.g[((p3 << 1) + p4)];
    }

    protected final Object a(int p2, Object p3)
    {
        return this.a.a(p2, p3);
    }

    protected final void a(int p2)
    {
        this.a.d(p2);
        return;
    }

    protected final void a(Object p2, Object p3)
    {
        this.a.put(p2, p3);
        return;
    }

    protected final int b(Object p2)
    {
        return this.a.b(p2);
    }

    protected final java.util.Map b()
    {
        return this.a;
    }

    protected final void c()
    {
        this.a.clear();
        return;
    }
}
