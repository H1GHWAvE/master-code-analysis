package android.support.v4.j;
final class i {
    public static final int d = 1;
    public static final int e = 2;
    public static final int f = 1;
    public static final int g = 2;
    public static final int h = 1;
    public static final int i = 2;
    private static final String m = "PrintHelperKitkat";
    private static final int n = 3500;
    final android.content.Context a;
    android.graphics.BitmapFactory$Options b;
    final Object c;
    int j;
    int k;
    int l;

    i(android.content.Context p3)
    {
        this.b = 0;
        this.c = new Object();
        this.j = 2;
        this.k = 2;
        this.l = 1;
        this.a = p3;
        return;
    }

    private int a()
    {
        return this.j;
    }

    static synthetic android.graphics.Bitmap a(android.graphics.Bitmap p6, int p7)
    {
        if (p7 == 1) {
            android.graphics.Bitmap v0_2 = android.graphics.Bitmap.createBitmap(p6.getWidth(), p6.getHeight(), android.graphics.Bitmap$Config.ARGB_8888);
            android.graphics.Canvas v1_2 = new android.graphics.Canvas(v0_2);
            int v2_2 = new android.graphics.Paint();
            android.graphics.ColorMatrix v3_1 = new android.graphics.ColorMatrix();
            v3_1.setSaturation(0);
            v2_2.setColorFilter(new android.graphics.ColorMatrixColorFilter(v3_1));
            v1_2.drawBitmap(p6, 0, 0, v2_2);
            v1_2.setBitmap(0);
            p6 = v0_2;
        }
        return p6;
    }

    private android.graphics.Bitmap a(android.net.Uri p5, android.graphics.BitmapFactory$Options p6)
    {
        if ((p5 != null) && (this.a != null)) {
            try {
                java.io.IOException v1_0 = this.a.getContentResolver().openInputStream(p5);
                Throwable v0_4 = android.graphics.BitmapFactory.decodeStream(v1_0, 0, p6);
            } catch (Throwable v0_5) {
                if (v1_0 != null) {
                    try {
                        v1_0.close();
                    } catch (java.io.IOException v1_1) {
                        android.util.Log.w("PrintHelperKitkat", "close fail ", v1_1);
                    }
                }
                throw v0_5;
            }
            if (v1_0 != null) {
                try {
                    v1_0.close();
                } catch (java.io.IOException v1_2) {
                    android.util.Log.w("PrintHelperKitkat", "close fail ", v1_2);
                }
            }
            return v0_4;
        } else {
            throw new IllegalArgumentException("bad argument to loadBitmap");
        }
    }

    private static synthetic android.graphics.Bitmap a(android.support.v4.j.i p1, android.net.Uri p2)
    {
        return p1.a(p2);
    }

    static synthetic android.graphics.Matrix a(int p6, int p7, android.graphics.RectF p8, int p9)
    {
        float v0_2;
        android.graphics.Matrix v1_1 = new android.graphics.Matrix();
        float v0_1 = (p8.width() / ((float) p6));
        if (p9 != 2) {
            v0_2 = Math.min(v0_1, (p8.height() / ((float) p7)));
        } else {
            v0_2 = Math.max(v0_1, (p8.height() / ((float) p7)));
        }
        v1_1.postScale(v0_2, v0_2);
        v1_1.postTranslate(((p8.width() - (((float) p6) * v0_2)) / 1073741824), ((p8.height() - (v0_2 * ((float) p7))) / 1073741824));
        return v1_1;
    }

    private static synthetic Object a(android.support.v4.j.i p1)
    {
        return p1.c;
    }

    private void a(int p1)
    {
        this.j = p1;
        return;
    }

    private void a(String p9, android.graphics.Bitmap p10, android.support.v4.j.n p11)
    {
        if (p10 != null) {
            android.support.v4.j.j v0_2 = android.print.PrintAttributes$MediaSize.UNKNOWN_PORTRAIT;
            if (p10.getWidth() > p10.getHeight()) {
                v0_2 = android.print.PrintAttributes$MediaSize.UNKNOWN_LANDSCAPE;
            }
            ((android.print.PrintManager) this.a.getSystemService("print")).print(p9, new android.support.v4.j.j(this, p9, p10, this.j, p11), new android.print.PrintAttributes$Builder().setMediaSize(v0_2).setColorMode(this.k).build());
        }
        return;
    }

    private void a(String p7, android.net.Uri p8, android.support.v4.j.n p9)
    {
        android.support.v4.j.k v0_1 = new android.support.v4.j.k(this, p7, p8, p9, this.j);
        android.print.PrintManager v1_3 = ((android.print.PrintManager) this.a.getSystemService("print"));
        android.print.PrintAttributes v2_3 = new android.print.PrintAttributes$Builder();
        v2_3.setColorMode(this.k);
        if (this.l != 1) {
            if (this.l == 2) {
                v2_3.setMediaSize(android.print.PrintAttributes$MediaSize.UNKNOWN_PORTRAIT);
            }
        } else {
            v2_3.setMediaSize(android.print.PrintAttributes$MediaSize.UNKNOWN_LANDSCAPE);
        }
        v1_3.print(p7, v0_1, v2_3.build());
        return;
    }

    private int b()
    {
        return this.l;
    }

    private static android.graphics.Bitmap b(android.graphics.Bitmap p6, int p7)
    {
        if (p7 == 1) {
            android.graphics.Bitmap v0_2 = android.graphics.Bitmap.createBitmap(p6.getWidth(), p6.getHeight(), android.graphics.Bitmap$Config.ARGB_8888);
            android.graphics.Canvas v1_2 = new android.graphics.Canvas(v0_2);
            int v2_2 = new android.graphics.Paint();
            android.graphics.ColorMatrix v3_1 = new android.graphics.ColorMatrix();
            v3_1.setSaturation(0);
            v2_2.setColorFilter(new android.graphics.ColorMatrixColorFilter(v3_1));
            v1_2.drawBitmap(p6, 0, 0, v2_2);
            v1_2.setBitmap(0);
            p6 = v0_2;
        }
        return p6;
    }

    private static android.graphics.Matrix b(int p6, int p7, android.graphics.RectF p8, int p9)
    {
        float v0_2;
        android.graphics.Matrix v1_1 = new android.graphics.Matrix();
        float v0_1 = (p8.width() / ((float) p6));
        if (p9 != 2) {
            v0_2 = Math.min(v0_1, (p8.height() / ((float) p7)));
        } else {
            v0_2 = Math.max(v0_1, (p8.height() / ((float) p7)));
        }
        v1_1.postScale(v0_2, v0_2);
        v1_1.postTranslate(((p8.width() - (((float) p6) * v0_2)) / 1073741824), ((p8.height() - (v0_2 * ((float) p7))) / 1073741824));
        return v1_1;
    }

    private void b(int p1)
    {
        this.k = p1;
        return;
    }

    private int c()
    {
        return this.k;
    }

    private void c(int p1)
    {
        this.l = p1;
        return;
    }

    final android.graphics.Bitmap a(android.net.Uri p7)
    {
        Object v1_0 = 1;
        Throwable v0_0 = 0;
        if ((p7 != null) && (this.a != null)) {
            int v2_2 = new android.graphics.BitmapFactory$Options();
            v2_2.inJustDecodeBounds = 1;
            this.a(p7, v2_2);
            int v3_0 = v2_2.outWidth;
            int v4 = v2_2.outHeight;
            if ((v3_0 > 0) && (v4 > 0)) {
                int v2_3 = Math.max(v3_0, v4);
                while (v2_3 > 3500) {
                    v2_3 >>= 1;
                    v1_0 <<= 1;
                }
                if ((v1_0 > null) && ((Math.min(v3_0, v4) / v1_0) > 0)) {
                    this.b = new android.graphics.BitmapFactory$Options();
                    this.b.inMutable = 1;
                    this.b.inSampleSize = v1_0;
                    try {
                        v0_0 = this.a(p7, this.b);
                    } catch (Throwable v0_7) {
                        this.b = 0;
                        throw v0_7;
                    }
                    try {
                        this.b = 0;
                    } catch (Throwable v0_9) {
                        throw v0_9;
                    }
                }
            }
            return v0_0;
        } else {
            throw new IllegalArgumentException("bad argument to getScaledBitmap");
        }
    }
}
