package android.support.v4.j;
public final class a {
    public static final int a = 1;
    public static final int b = 2;
    public static final int c = 1;
    public static final int d = 2;
    public static final int e = 1;
    public static final int f = 2;
    android.support.v4.j.h g;

    private a(android.content.Context p4)
    {
        android.support.v4.j.g v0_1;
        if (android.os.Build$VERSION.SDK_INT < 19) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        if (v0_1 == null) {
            this.g = new android.support.v4.j.g(0);
        } else {
            this.g = new android.support.v4.j.d(p4);
        }
        return;
    }

    private void a(int p2)
    {
        this.g.a(p2);
        return;
    }

    private void a(String p3, android.graphics.Bitmap p4)
    {
        this.g.a(p3, p4, 0);
        return;
    }

    private void a(String p2, android.graphics.Bitmap p3, android.support.v4.j.c p4)
    {
        this.g.a(p2, p3, p4);
        return;
    }

    private void a(String p3, android.net.Uri p4)
    {
        this.g.a(p3, p4, 0);
        return;
    }

    private void a(String p2, android.net.Uri p3, android.support.v4.j.c p4)
    {
        this.g.a(p2, p3, p4);
        return;
    }

    private static boolean a()
    {
        int v0_1;
        if (android.os.Build$VERSION.SDK_INT < 19) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    private int b()
    {
        return this.g.a();
    }

    private void b(int p2)
    {
        this.g.b(p2);
        return;
    }

    private int c()
    {
        return this.g.b();
    }

    private void c(int p2)
    {
        this.g.c(p2);
        return;
    }

    private int d()
    {
        return this.g.c();
    }
}
