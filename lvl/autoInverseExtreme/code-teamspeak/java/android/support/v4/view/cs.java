package android.support.v4.view;
public final class cs {
    static final android.support.v4.view.cv a;

    static cs()
    {
        if (android.os.Build$VERSION.SDK_INT < 11) {
            android.support.v4.view.cs.a = new android.support.v4.view.ct();
        } else {
            android.support.v4.view.cs.a = new android.support.v4.view.cu();
        }
        return;
    }

    public cs()
    {
        return;
    }

    public static float a(android.view.VelocityTracker p1, int p2)
    {
        return android.support.v4.view.cs.a.a(p1, p2);
    }

    public static float b(android.view.VelocityTracker p1, int p2)
    {
        return android.support.v4.view.cs.a.b(p1, p2);
    }
}
