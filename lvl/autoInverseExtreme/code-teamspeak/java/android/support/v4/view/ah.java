package android.support.v4.view;
final class ah {

    ah()
    {
        return;
    }

    private static int a(int p1)
    {
        return android.view.KeyEvent.normalizeMetaState(p1);
    }

    private static boolean a(int p1, int p2)
    {
        return android.view.KeyEvent.metaStateHasModifiers(p1, p2);
    }

    private static boolean b(int p1)
    {
        return android.view.KeyEvent.metaStateHasNoModifiers(p1);
    }
}
