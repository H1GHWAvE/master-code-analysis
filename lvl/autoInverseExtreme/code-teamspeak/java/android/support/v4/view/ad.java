package android.support.v4.view;
 class ad extends android.support.v4.view.ac {

    ad()
    {
        return;
    }

    public final Object a(android.view.View p2)
    {
        return p2.getKeyDispatcherState();
    }

    public final void a(android.view.KeyEvent p1)
    {
        p1.startTracking();
        return;
    }

    public final boolean a(android.view.KeyEvent p2, android.view.KeyEvent$Callback p3, Object p4, Object p5)
    {
        return p2.dispatch(p3, ((android.view.KeyEvent$DispatcherState) p4), p5);
    }

    public final boolean b(android.view.KeyEvent p2)
    {
        return p2.isTracking();
    }
}
