package android.support.v4.view;
final class ce extends android.database.DataSetObserver implements android.support.v4.view.eu, android.support.v4.view.ev {
    final synthetic android.support.v4.view.cc a;
    private int b;

    private ce(android.support.v4.view.cc p1)
    {
        this.a = p1;
        return;
    }

    synthetic ce(android.support.v4.view.cc p1, byte p2)
    {
        this(p1);
        return;
    }

    public final void a(int p1)
    {
        this.b = p1;
        return;
    }

    public final void a(int p3, float p4)
    {
        if (p4 > 1056964608) {
            p3++;
        }
        this.a.a(p3, p4, 0);
        return;
    }

    public final void a(android.support.v4.view.by p2, android.support.v4.view.by p3)
    {
        this.a.a(p2, p3);
        return;
    }

    public final void b(int p5)
    {
        float v0_0 = 0;
        if (this.b == 0) {
            android.support.v4.view.cc v1_1 = this.a;
            int v2_2 = this.a.a.getCurrentItem();
            this.a.a.getAdapter();
            v1_1.a(v2_2);
            if (android.support.v4.view.cc.a(this.a) >= 0) {
                v0_0 = android.support.v4.view.cc.a(this.a);
            }
            this.a.a(this.a.a.getCurrentItem(), v0_0, 1);
        }
        return;
    }

    public final void onChanged()
    {
        float v0_0 = 0;
        android.support.v4.view.cc v1_0 = this.a;
        int v2_2 = this.a.a.getCurrentItem();
        this.a.a.getAdapter();
        v1_0.a(v2_2);
        if (android.support.v4.view.cc.a(this.a) >= 0) {
            v0_0 = android.support.v4.view.cc.a(this.a);
        }
        this.a.a(this.a.a.getCurrentItem(), v0_0, 1);
        return;
    }
}
