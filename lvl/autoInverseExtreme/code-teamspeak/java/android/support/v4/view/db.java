package android.support.v4.view;
 class db extends android.support.v4.view.da {

    db()
    {
        return;
    }

    public final float A(android.view.View p2)
    {
        return p2.getRotation();
    }

    public final float B(android.view.View p2)
    {
        return p2.getRotationX();
    }

    public final float C(android.view.View p2)
    {
        return p2.getRotationY();
    }

    public final float D(android.view.View p2)
    {
        return p2.getScaleX();
    }

    public final float E(android.view.View p2)
    {
        return p2.getScaleY();
    }

    public final float I(android.view.View p2)
    {
        return p2.getPivotX();
    }

    public final float J(android.view.View p2)
    {
        return p2.getPivotY();
    }

    public final void R(android.view.View p1)
    {
        p1.jumpDrawablesToCurrentState();
        return;
    }

    public final void S(android.view.View p2)
    {
        p2.setSaveFromParentEnabled(0);
        return;
    }

    public final int a(int p2, int p3)
    {
        return android.view.View.combineMeasuredStates(p2, p3);
    }

    public final int a(int p2, int p3, int p4)
    {
        return android.view.View.resolveSizeAndState(p2, p3, p4);
    }

    final long a()
    {
        return android.animation.ValueAnimator.getFrameDelay();
    }

    public final void a(android.view.View p1, float p2)
    {
        p1.setRotation(p2);
        return;
    }

    public final void a(android.view.View p1, int p2, android.graphics.Paint p3)
    {
        p1.setLayerType(p2, p3);
        return;
    }

    public void a(android.view.View p2, android.graphics.Paint p3)
    {
        p2.setLayerType(p2.getLayerType(), p3);
        p2.invalidate();
        return;
    }

    public final void b(android.view.View p1, float p2)
    {
        p1.setTranslationX(p2);
        return;
    }

    public final void c(android.view.View p1, float p2)
    {
        p1.setTranslationY(p2);
        return;
    }

    public final void c(android.view.View p1, boolean p2)
    {
        p1.setActivated(p2);
        return;
    }

    public final void d(android.view.View p1, float p2)
    {
        p1.setAlpha(p2);
        return;
    }

    public final void e(android.view.View p1, float p2)
    {
        p1.setRotationX(p2);
        return;
    }

    public final void f(android.view.View p1, float p2)
    {
        p1.setRotationY(p2);
        return;
    }

    public final void g(android.view.View p1, float p2)
    {
        p1.setScaleX(p2);
        return;
    }

    public final float h(android.view.View p2)
    {
        return p2.getAlpha();
    }

    public final void h(android.view.View p1, float p2)
    {
        p1.setScaleY(p2);
        return;
    }

    public final int i(android.view.View p2)
    {
        return p2.getLayerType();
    }

    public final void i(android.view.View p1, float p2)
    {
        p1.setX(p2);
        return;
    }

    public final void j(android.view.View p1, float p2)
    {
        p1.setY(p2);
        return;
    }

    public final void k(android.view.View p1, float p2)
    {
        p1.setPivotX(p2);
        return;
    }

    public final void l(android.view.View p1, float p2)
    {
        p1.setPivotY(p2);
        return;
    }

    public final int n(android.view.View p2)
    {
        return p2.getMeasuredWidthAndState();
    }

    public final int o(android.view.View p2)
    {
        return p2.getMeasuredHeightAndState();
    }

    public final int p(android.view.View p2)
    {
        return p2.getMeasuredState();
    }

    public final float w(android.view.View p2)
    {
        return p2.getTranslationX();
    }

    public final float x(android.view.View p2)
    {
        return p2.getTranslationY();
    }

    public final float y(android.view.View p2)
    {
        return p2.getX();
    }

    public final float z(android.view.View p2)
    {
        return p2.getY();
    }
}
