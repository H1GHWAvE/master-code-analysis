package android.support.v4.view;
public final class ab {
    static final android.support.v4.view.af a;

    static ab()
    {
        if (android.os.Build$VERSION.SDK_INT < 11) {
            android.support.v4.view.ab.a = new android.support.v4.view.ac();
        } else {
            android.support.v4.view.ab.a = new android.support.v4.view.ae();
        }
        return;
    }

    public ab()
    {
        return;
    }

    private static int a(int p1)
    {
        return android.support.v4.view.ab.a.a(p1);
    }

    public static Object a(android.view.View p1)
    {
        return android.support.v4.view.ab.a.a(p1);
    }

    private static boolean a(int p1, int p2)
    {
        return android.support.v4.view.ab.a.a(p1, p2);
    }

    public static boolean a(android.view.KeyEvent p3)
    {
        return android.support.v4.view.ab.a.a(p3.getMetaState(), 1);
    }

    public static boolean a(android.view.KeyEvent p1, android.view.KeyEvent$Callback p2, Object p3, Object p4)
    {
        return android.support.v4.view.ab.a.a(p1, p2, p3, p4);
    }

    private static boolean b(int p1)
    {
        return android.support.v4.view.ab.a.b(p1);
    }

    public static boolean b(android.view.KeyEvent p2)
    {
        return android.support.v4.view.ab.a.b(p2.getMetaState());
    }

    public static void c(android.view.KeyEvent p1)
    {
        android.support.v4.view.ab.a.a(p1);
        return;
    }

    private static boolean d(android.view.KeyEvent p1)
    {
        return android.support.v4.view.ab.a.b(p1);
    }
}
