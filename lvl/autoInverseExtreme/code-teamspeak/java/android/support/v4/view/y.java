package android.support.v4.view;
final class y implements android.support.v4.view.w {

    y()
    {
        return;
    }

    public final int a(int p2, int p3)
    {
        return android.view.Gravity.getAbsoluteGravity(p2, p3);
    }

    public final void a(int p1, int p2, int p3, android.graphics.Rect p4, int p5, int p6, android.graphics.Rect p7, int p8)
    {
        android.view.Gravity.apply(p1, p2, p3, p4, p5, p6, p7, p8);
        return;
    }

    public final void a(int p1, int p2, int p3, android.graphics.Rect p4, android.graphics.Rect p5, int p6)
    {
        android.view.Gravity.apply(p1, p2, p3, p4, p5, p6);
        return;
    }

    public final void a(int p1, android.graphics.Rect p2, android.graphics.Rect p3, int p4)
    {
        android.view.Gravity.applyDisplay(p1, p2, p3, p4);
        return;
    }
}
