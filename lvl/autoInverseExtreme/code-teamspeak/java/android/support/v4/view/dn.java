package android.support.v4.view;
final class dn {

    dn()
    {
        return;
    }

    private static void a(android.view.View p0, android.view.accessibility.AccessibilityEvent p1)
    {
        p0.onPopulateAccessibilityEvent(p1);
        return;
    }

    private static void a(android.view.View p0, Object p1)
    {
        p0.setAccessibilityDelegate(((android.view.View$AccessibilityDelegate) p1));
        return;
    }

    private static void a(android.view.View p0, boolean p1)
    {
        p0.setFitsSystemWindows(p1);
        return;
    }

    private static boolean a(android.view.View p1, int p2)
    {
        return p1.canScrollHorizontally(p2);
    }

    private static void b(android.view.View p0, android.view.accessibility.AccessibilityEvent p1)
    {
        p0.onInitializeAccessibilityEvent(p1);
        return;
    }

    private static void b(android.view.View p0, Object p1)
    {
        p0.onInitializeAccessibilityNodeInfo(((android.view.accessibility.AccessibilityNodeInfo) p1));
        return;
    }

    private static boolean b(android.view.View p1, int p2)
    {
        return p1.canScrollVertically(p2);
    }
}
