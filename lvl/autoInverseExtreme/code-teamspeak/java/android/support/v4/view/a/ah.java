package android.support.v4.view.a;
final class ah {

    ah()
    {
        return;
    }

    private static Object a(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).getTraversalBefore();
    }

    private static void a(Object p0, android.view.View p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setTraversalBefore(p1);
        return;
    }

    private static void a(Object p0, android.view.View p1, int p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setTraversalBefore(p1, p2);
        return;
    }

    private static Object b(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).getTraversalAfter();
    }

    private static void b(Object p0, android.view.View p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setTraversalAfter(p1);
        return;
    }

    private static void b(Object p0, android.view.View p1, int p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setTraversalAfter(p1, p2);
        return;
    }
}
