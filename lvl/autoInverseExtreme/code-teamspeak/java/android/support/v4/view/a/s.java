package android.support.v4.view.a;
public final class s {
    public static final android.support.v4.view.a.s a;
    public static final android.support.v4.view.a.s b;
    public static final android.support.v4.view.a.s c;
    public static final android.support.v4.view.a.s d;
    public static final android.support.v4.view.a.s e;
    public static final android.support.v4.view.a.s f;
    public static final android.support.v4.view.a.s g;
    public static final android.support.v4.view.a.s h;
    public static final android.support.v4.view.a.s i;
    public static final android.support.v4.view.a.s j;
    public static final android.support.v4.view.a.s k;
    public static final android.support.v4.view.a.s l;
    public static final android.support.v4.view.a.s m;
    public static final android.support.v4.view.a.s n;
    public static final android.support.v4.view.a.s o;
    public static final android.support.v4.view.a.s p;
    public static final android.support.v4.view.a.s q;
    public static final android.support.v4.view.a.s r;
    public static final android.support.v4.view.a.s s;
    public static final android.support.v4.view.a.s t;
    public static final android.support.v4.view.a.s u;
    public static final android.support.v4.view.a.s v;
    private final Object w;

    static s()
    {
        android.support.v4.view.a.s.a = new android.support.v4.view.a.s(1);
        android.support.v4.view.a.s.b = new android.support.v4.view.a.s(2);
        android.support.v4.view.a.s.c = new android.support.v4.view.a.s(4);
        android.support.v4.view.a.s.d = new android.support.v4.view.a.s(8);
        android.support.v4.view.a.s.e = new android.support.v4.view.a.s(16);
        android.support.v4.view.a.s.f = new android.support.v4.view.a.s(32);
        android.support.v4.view.a.s.g = new android.support.v4.view.a.s(64);
        android.support.v4.view.a.s.h = new android.support.v4.view.a.s(128);
        android.support.v4.view.a.s.i = new android.support.v4.view.a.s(256);
        android.support.v4.view.a.s.j = new android.support.v4.view.a.s(512);
        android.support.v4.view.a.s.k = new android.support.v4.view.a.s(1024);
        android.support.v4.view.a.s.l = new android.support.v4.view.a.s(2048);
        android.support.v4.view.a.s.m = new android.support.v4.view.a.s(4096);
        android.support.v4.view.a.s.n = new android.support.v4.view.a.s(8192);
        android.support.v4.view.a.s.o = new android.support.v4.view.a.s(16384);
        android.support.v4.view.a.s.p = new android.support.v4.view.a.s(32768);
        android.support.v4.view.a.s.q = new android.support.v4.view.a.s(65536);
        android.support.v4.view.a.s.r = new android.support.v4.view.a.s(131072);
        android.support.v4.view.a.s.s = new android.support.v4.view.a.s(262144);
        android.support.v4.view.a.s.t = new android.support.v4.view.a.s(524288);
        android.support.v4.view.a.s.u = new android.support.v4.view.a.s(1048576);
        android.support.v4.view.a.s.v = new android.support.v4.view.a.s(2097152);
        return;
    }

    private s(int p2)
    {
        this(android.support.v4.view.a.q.p().a(p2));
        return;
    }

    private s(Object p1)
    {
        this.w = p1;
        return;
    }

    synthetic s(Object p1, byte p2)
    {
        this(p1);
        return;
    }

    private int a()
    {
        return android.support.v4.view.a.q.p().b(this.w);
    }

    static synthetic Object a(android.support.v4.view.a.s p1)
    {
        return p1.w;
    }

    private CharSequence b()
    {
        return android.support.v4.view.a.q.p().c(this.w);
    }
}
