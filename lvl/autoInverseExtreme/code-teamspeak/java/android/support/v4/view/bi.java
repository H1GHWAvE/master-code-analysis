package android.support.v4.view;
final class bi implements android.view.MenuItem$OnActionExpandListener {
    private android.support.v4.view.bj a;

    public bi(android.support.v4.view.bj p1)
    {
        this.a = p1;
        return;
    }

    public final boolean onMenuItemActionCollapse(android.view.MenuItem p2)
    {
        return this.a.b(p2);
    }

    public final boolean onMenuItemActionExpand(android.view.MenuItem p2)
    {
        return this.a.a(p2);
    }
}
