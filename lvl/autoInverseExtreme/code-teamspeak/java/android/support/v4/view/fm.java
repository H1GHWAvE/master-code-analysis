package android.support.v4.view;
 class fm implements android.support.v4.view.fu {
    java.util.WeakHashMap a;

    fm()
    {
        this.a = 0;
        return;
    }

    private static synthetic void a(android.support.v4.view.fm p0, android.support.v4.view.fk p1, android.view.View p2)
    {
        p0.f(p1, p2);
        return;
    }

    private void d(android.view.View p2)
    {
        if (this.a != null) {
            Runnable v0_3 = ((Runnable) this.a.get(p2));
            if (v0_3 != null) {
                p2.removeCallbacks(v0_3);
            }
        }
        return;
    }

    private void g(android.support.v4.view.fk p3, android.view.View p4)
    {
        android.support.v4.view.fn v0_0 = 0;
        if (this.a != null) {
            v0_0 = ((Runnable) this.a.get(p4));
        }
        if (v0_0 == null) {
            v0_0 = new android.support.v4.view.fn(this, p3, p4, 0);
            if (this.a == null) {
                this.a = new java.util.WeakHashMap();
            }
            this.a.put(p4, v0_0);
        }
        p4.removeCallbacks(v0_0);
        p4.post(v0_0);
        return;
    }

    public long a(android.view.View p3)
    {
        return 0;
    }

    public void a(android.support.v4.view.fk p1, android.view.View p2)
    {
        this.g(p1, p2);
        return;
    }

    public void a(android.support.v4.view.fk p1, android.view.View p2, float p3)
    {
        this.g(p1, p2);
        return;
    }

    public void a(android.support.v4.view.fk p2, android.view.View p3, android.support.v4.view.gd p4)
    {
        p3.setTag(2113929216, p4);
        return;
    }

    public void a(android.support.v4.view.fk p1, android.view.View p2, Runnable p3)
    {
        android.support.v4.view.fk.a(p1, p3);
        this.g(p1, p2);
        return;
    }

    public void a(android.view.View p1, float p2)
    {
        return;
    }

    public void a(android.view.View p1, long p2)
    {
        return;
    }

    public void a(android.view.View p1, android.support.v4.view.gf p2)
    {
        return;
    }

    public void a(android.view.View p1, android.view.animation.Interpolator p2)
    {
        return;
    }

    public android.view.animation.Interpolator b(android.view.View p2)
    {
        return 0;
    }

    public void b(android.support.v4.view.fk p1, android.view.View p2)
    {
        this.g(p1, p2);
        return;
    }

    public void b(android.support.v4.view.fk p1, android.view.View p2, float p3)
    {
        this.g(p1, p2);
        return;
    }

    public void b(android.support.v4.view.fk p1, android.view.View p2, Runnable p3)
    {
        android.support.v4.view.fk.b(p1, p3);
        this.g(p1, p2);
        return;
    }

    public void b(android.view.View p1, float p2)
    {
        return;
    }

    public void b(android.view.View p1, long p2)
    {
        return;
    }

    public long c(android.view.View p3)
    {
        return 0;
    }

    public void c(android.support.v4.view.fk p1, android.view.View p2)
    {
        this.g(p1, p2);
        return;
    }

    public void c(android.support.v4.view.fk p1, android.view.View p2, float p3)
    {
        this.g(p1, p2);
        return;
    }

    public void c(android.view.View p1, float p2)
    {
        return;
    }

    public void d(android.support.v4.view.fk p2, android.view.View p3)
    {
        if (this.a != null) {
            Runnable v0_3 = ((Runnable) this.a.get(p3));
            if (v0_3 != null) {
                p3.removeCallbacks(v0_3);
            }
        }
        this.f(p2, p3);
        return;
    }

    public void d(android.support.v4.view.fk p1, android.view.View p2, float p3)
    {
        this.g(p1, p2);
        return;
    }

    public void d(android.view.View p1, float p2)
    {
        return;
    }

    public void e(android.support.v4.view.fk p1, android.view.View p2)
    {
        return;
    }

    public void e(android.support.v4.view.fk p1, android.view.View p2, float p3)
    {
        this.g(p1, p2);
        return;
    }

    final void f(android.support.v4.view.fk p4, android.view.View p5)
    {
        java.util.WeakHashMap v0_2;
        java.util.WeakHashMap v0_1 = p5.getTag(2113929216);
        if (!(v0_1 instanceof android.support.v4.view.gd)) {
            v0_2 = 0;
        } else {
            v0_2 = ((android.support.v4.view.gd) v0_1);
        }
        Runnable v1_1 = android.support.v4.view.fk.a(p4);
        Runnable v2_1 = android.support.v4.view.fk.b(p4);
        if (v1_1 != null) {
            v1_1.run();
        }
        if (v0_2 != null) {
            v0_2.a(p5);
            v0_2.b(p5);
        }
        if (v2_1 != null) {
            v2_1.run();
        }
        if (this.a != null) {
            this.a.remove(p5);
        }
        return;
    }

    public void f(android.support.v4.view.fk p1, android.view.View p2, float p3)
    {
        this.g(p1, p2);
        return;
    }

    public void g(android.support.v4.view.fk p1, android.view.View p2, float p3)
    {
        this.g(p1, p2);
        return;
    }

    public void h(android.support.v4.view.fk p1, android.view.View p2, float p3)
    {
        this.g(p1, p2);
        return;
    }

    public void i(android.support.v4.view.fk p1, android.view.View p2, float p3)
    {
        this.g(p1, p2);
        return;
    }

    public void j(android.support.v4.view.fk p1, android.view.View p2, float p3)
    {
        this.g(p1, p2);
        return;
    }

    public void k(android.support.v4.view.fk p1, android.view.View p2, float p3)
    {
        this.g(p1, p2);
        return;
    }

    public void l(android.support.v4.view.fk p1, android.view.View p2, float p3)
    {
        this.g(p1, p2);
        return;
    }

    public void m(android.support.v4.view.fk p1, android.view.View p2, float p3)
    {
        this.g(p1, p2);
        return;
    }

    public void n(android.support.v4.view.fk p1, android.view.View p2, float p3)
    {
        this.g(p1, p2);
        return;
    }

    public void o(android.support.v4.view.fk p1, android.view.View p2, float p3)
    {
        this.g(p1, p2);
        return;
    }

    public void p(android.support.v4.view.fk p1, android.view.View p2, float p3)
    {
        this.g(p1, p2);
        return;
    }

    public void q(android.support.v4.view.fk p1, android.view.View p2, float p3)
    {
        this.g(p1, p2);
        return;
    }

    public void r(android.support.v4.view.fk p1, android.view.View p2, float p3)
    {
        this.g(p1, p2);
        return;
    }
}
