package android.support.v4.view;
final class s implements android.support.v4.view.r {
    private static final int e = 0;
    private static final int f = 0;
    private static final int g = 0;
    private static final int h = 1;
    private static final int i = 2;
    private static final int j = 3;
    private android.view.VelocityTracker A;
    private int a;
    private int b;
    private int c;
    private int d;
    private final android.os.Handler k;
    private final android.view.GestureDetector$OnGestureListener l;
    private android.view.GestureDetector$OnDoubleTapListener m;
    private boolean n;
    private boolean o;
    private boolean p;
    private boolean q;
    private boolean r;
    private android.view.MotionEvent s;
    private android.view.MotionEvent t;
    private boolean u;
    private float v;
    private float w;
    private float x;
    private float y;
    private boolean z;

    static s()
    {
        android.support.v4.view.s.e = android.view.ViewConfiguration.getLongPressTimeout();
        android.support.v4.view.s.f = android.view.ViewConfiguration.getTapTimeout();
        android.support.v4.view.s.g = android.view.ViewConfiguration.getDoubleTapTimeout();
        return;
    }

    public s(android.content.Context p5, android.view.GestureDetector$OnGestureListener p6)
    {
        this.k = new android.support.v4.view.t(this);
        this.l = p6;
        if ((p6 instanceof android.view.GestureDetector$OnDoubleTapListener)) {
            this.m = ((android.view.GestureDetector$OnDoubleTapListener) p6);
        }
        if (p5 != null) {
            if (this.l != null) {
                this.z = 1;
                int v0_5 = android.view.ViewConfiguration.get(p5);
                int v1_0 = v0_5.getScaledTouchSlop();
                int v2 = v0_5.getScaledDoubleTapSlop();
                this.c = v0_5.getScaledMinimumFlingVelocity();
                this.d = v0_5.getScaledMaximumFlingVelocity();
                this.a = (v1_0 * v1_0);
                this.b = (v2 * v2);
                return;
            } else {
                throw new IllegalArgumentException("OnGestureListener must not be null");
            }
        } else {
            throw new IllegalArgumentException("Context must not be null");
        }
    }

    static synthetic android.view.MotionEvent a(android.support.v4.view.s p1)
    {
        return p1.s;
    }

    private void a(android.content.Context p5)
    {
        if (p5 != null) {
            if (this.l != null) {
                this.z = 1;
                int v0_2 = android.view.ViewConfiguration.get(p5);
                int v1_0 = v0_2.getScaledTouchSlop();
                int v2 = v0_2.getScaledDoubleTapSlop();
                this.c = v0_2.getScaledMinimumFlingVelocity();
                this.d = v0_2.getScaledMaximumFlingVelocity();
                this.a = (v1_0 * v1_0);
                this.b = (v2 * v2);
                return;
            } else {
                throw new IllegalArgumentException("OnGestureListener must not be null");
            }
        } else {
            throw new IllegalArgumentException("Context must not be null");
        }
    }

    private boolean a(android.view.MotionEvent p7, android.view.MotionEvent p8, android.view.MotionEvent p9)
    {
        int v0 = 0;
        if ((this.r) && (((p9.getEventTime() - p8.getEventTime()) <= ((long) android.support.v4.view.s.g)) && ((((((int) p7.getX()) - ((int) p9.getX())) * (((int) p7.getX()) - ((int) p9.getX()))) + ((((int) p7.getY()) - ((int) p9.getY())) * (((int) p7.getY()) - ((int) p9.getY())))) < this.b))) {
            v0 = 1;
        }
        return v0;
    }

    static synthetic android.view.GestureDetector$OnGestureListener b(android.support.v4.view.s p1)
    {
        return p1.l;
    }

    private void b()
    {
        this.k.removeMessages(1);
        this.k.removeMessages(2);
        this.k.removeMessages(3);
        this.A.recycle();
        this.A = 0;
        this.u = 0;
        this.n = 0;
        this.q = 0;
        this.r = 0;
        this.o = 0;
        if (this.p) {
            this.p = 0;
        }
        return;
    }

    private void c()
    {
        this.k.removeMessages(1);
        this.k.removeMessages(2);
        this.k.removeMessages(3);
        this.u = 0;
        this.q = 0;
        this.r = 0;
        this.o = 0;
        if (this.p) {
            this.p = 0;
        }
        return;
    }

    static synthetic void c(android.support.v4.view.s p2)
    {
        p2.k.removeMessages(3);
        p2.o = 0;
        p2.p = 1;
        p2.l.onLongPress(p2.s);
        return;
    }

    static synthetic android.view.GestureDetector$OnDoubleTapListener d(android.support.v4.view.s p1)
    {
        return p1.m;
    }

    private void d()
    {
        this.k.removeMessages(3);
        this.o = 0;
        this.p = 1;
        this.l.onLongPress(this.s);
        return;
    }

    static synthetic boolean e(android.support.v4.view.s p1)
    {
        return p1.n;
    }

    static synthetic boolean f(android.support.v4.view.s p1)
    {
        p1.o = 1;
        return 1;
    }

    public final void a(android.view.GestureDetector$OnDoubleTapListener p1)
    {
        this.m = p1;
        return;
    }

    public final void a(boolean p1)
    {
        this.z = p1;
        return;
    }

    public final boolean a()
    {
        return this.z;
    }

    public final boolean a(android.view.MotionEvent p14)
    {
        boolean v3_0 = 0;
        android.view.VelocityTracker v9_0 = p14.getAction();
        if (this.A == null) {
            this.A = android.view.VelocityTracker.obtain();
        }
        long v8_0;
        this.A.addMovement(p14);
        if ((v9_0 & 255) != 6) {
            v8_0 = 0;
        } else {
            v8_0 = 1;
        }
        int v0_4;
        if (v8_0 == 0) {
            v0_4 = -1;
        } else {
            v0_4 = android.support.v4.view.bk.b(p14);
        }
        boolean v5_0 = android.support.v4.view.bk.c(p14);
        int v6_0 = 0;
        int v1_1 = 0;
        android.view.GestureDetector$OnDoubleTapListener v2_0 = 0;
        while (v6_0 < v5_0) {
            if (v0_4 != v6_0) {
                v2_0 += android.support.v4.view.bk.c(p14, v6_0);
                v1_1 += android.support.v4.view.bk.d(p14, v6_0);
            }
            v6_0++;
        }
        int v0_5;
        if (v8_0 == 0) {
            v0_5 = v5_0;
        } else {
            v0_5 = (v5_0 - 1);
        }
        android.view.GestureDetector$OnDoubleTapListener v2_1 = (v2_0 / ((float) v0_5));
        int v1_2 = (v1_1 / ((float) v0_5));
        switch ((v9_0 & 255)) {
            case 0:
                int v0_49;
                if (this.m == null) {
                    v0_49 = 0;
                } else {
                    int v0_40 = this.k.hasMessages(3);
                    if (v0_40 != 0) {
                        this.k.removeMessages(3);
                    }
                    if ((this.s != null) && ((this.t != null) && (v0_40 != 0))) {
                        int v0_47;
                        int v0_41 = this.s;
                        if ((!this.r) || (((p14.getEventTime() - this.t.getEventTime()) > ((long) android.support.v4.view.s.g)) || ((((((int) v0_41.getY()) - ((int) p14.getY())) * (((int) v0_41.getY()) - ((int) p14.getY()))) + ((((int) v0_41.getX()) - ((int) p14.getX())) * (((int) v0_41.getX()) - ((int) p14.getX())))) >= this.b))) {
                            v0_47 = 0;
                        } else {
                            v0_47 = 1;
                        }
                        if (v0_47 != 0) {
                            this.u = 1;
                            v0_49 = ((this.m.onDoubleTap(this.s) | 0) | this.m.onDoubleTapEvent(p14));
                            this.v = v2_1;
                            this.x = v2_1;
                            this.w = v1_2;
                            this.y = v1_2;
                            if (this.s != null) {
                                this.s.recycle();
                            }
                            this.s = android.view.MotionEvent.obtain(p14);
                            this.q = 1;
                            this.r = 1;
                            this.n = 1;
                            this.p = 0;
                            this.o = 0;
                            if (this.z) {
                                this.k.removeMessages(2);
                                this.k.sendEmptyMessageAtTime(2, ((this.s.getDownTime() + ((long) android.support.v4.view.s.f)) + ((long) android.support.v4.view.s.e)));
                            }
                            this.k.sendEmptyMessageAtTime(1, (this.s.getDownTime() + ((long) android.support.v4.view.s.f)));
                            v3_0 = (v0_49 | this.l.onDown(p14));
                        }
                    }
                    this.k.sendEmptyMessageDelayed(3, ((long) android.support.v4.view.s.g));
                }
                break;
            case 1:
                int v0_33;
                this.n = 0;
                int v1_9 = android.view.MotionEvent.obtain(p14);
                if (!this.u) {
                    if (!this.p) {
                        if (!this.q) {
                            int v0_31 = this.A;
                            android.view.GestureDetector$OnDoubleTapListener v2_6 = android.support.v4.view.bk.b(p14, 0);
                            v0_31.computeCurrentVelocity(1000, ((float) this.d));
                            boolean v5_4 = android.support.v4.view.cs.b(v0_31, v2_6);
                            int v0_32 = android.support.v4.view.cs.a(v0_31, v2_6);
                            if ((Math.abs(v5_4) <= ((float) this.c)) && (Math.abs(v0_32) <= ((float) this.c))) {
                                v0_33 = 0;
                            } else {
                                v0_33 = this.l.onFling(this.s, p14, v0_32, v5_4);
                            }
                        } else {
                            v0_33 = this.l.onSingleTapUp(p14);
                            if ((this.o) && (this.m != null)) {
                                this.m.onSingleTapConfirmed(p14);
                            }
                        }
                    } else {
                        this.k.removeMessages(3);
                        this.p = 0;
                        v0_33 = 0;
                    }
                } else {
                    v0_33 = (this.m.onDoubleTapEvent(p14) | 0);
                }
                if (this.t != null) {
                    this.t.recycle();
                }
                this.t = v1_9;
                if (this.A != null) {
                    this.A.recycle();
                    this.A = 0;
                }
                this.u = 0;
                this.o = 0;
                this.k.removeMessages(1);
                this.k.removeMessages(2);
                v3_0 = v0_33;
                break;
            case 2:
                if (this.p) {
                } else {
                    int v0_24 = (this.v - v2_1);
                    boolean v5_2 = (this.w - v1_2);
                    if (!this.u) {
                        if (!this.q) {
                            if ((Math.abs(v0_24) < 1065353216) && (Math.abs(v5_2) < 1065353216)) {
                            } else {
                                v3_0 = this.l.onScroll(this.s, p14, v0_24, v5_2);
                                this.v = v2_1;
                                this.w = v1_2;
                            }
                        } else {
                            int v0_25;
                            int v6_15 = ((((int) (v2_1 - this.x)) * ((int) (v2_1 - this.x))) + (((int) (v1_2 - this.y)) * ((int) (v1_2 - this.y))));
                            if (v6_15 <= this.a) {
                                v0_25 = 0;
                            } else {
                                v0_25 = this.l.onScroll(this.s, p14, v0_24, v5_2);
                                this.v = v2_1;
                                this.w = v1_2;
                                this.q = 0;
                                this.k.removeMessages(3);
                                this.k.removeMessages(1);
                                this.k.removeMessages(2);
                            }
                            if (v6_15 > this.a) {
                                this.r = 0;
                            }
                            v3_0 = v0_25;
                        }
                    } else {
                        v3_0 = (this.m.onDoubleTapEvent(p14) | 0);
                    }
                }
                break;
            case 3:
                this.k.removeMessages(1);
                this.k.removeMessages(2);
                this.k.removeMessages(3);
                this.A.recycle();
                this.A = 0;
                this.u = 0;
                this.n = 0;
                this.q = 0;
                this.r = 0;
                this.o = 0;
                if (!this.p) {
                } else {
                    this.p = 0;
                }
            case 4:
            default:
                break;
            case 5:
                this.v = v2_1;
                this.x = v2_1;
                this.w = v1_2;
                this.y = v1_2;
                this.k.removeMessages(1);
                this.k.removeMessages(2);
                this.k.removeMessages(3);
                this.u = 0;
                this.q = 0;
                this.r = 0;
                this.o = 0;
                if (!this.p) {
                } else {
                    this.p = 0;
                }
                break;
            case 6:
                this.v = v2_1;
                this.x = v2_1;
                this.w = v1_2;
                this.y = v1_2;
                this.A.computeCurrentVelocity(1000, ((float) this.d));
                int v1_4 = android.support.v4.view.bk.b(p14);
                int v0_9 = android.support.v4.view.bk.b(p14, v1_4);
                android.view.GestureDetector$OnDoubleTapListener v2_5 = android.support.v4.view.cs.a(this.A, v0_9);
                android.view.MotionEvent v4_2 = android.support.v4.view.cs.b(this.A, v0_9);
                int v0_10 = 0;
                while (v0_10 < v5_0) {
                    if (v0_10 != v1_4) {
                        int v6_2 = android.support.v4.view.bk.b(p14, v0_10);
                        if (((android.support.v4.view.cs.b(this.A, v6_2) * v4_2) + (android.support.v4.view.cs.a(this.A, v6_2) * v2_5)) < 0) {
                            this.A.clear();
                            break;
                        }
                    }
                    v0_10++;
                }
                break;
        }
        return v3_0;
    }
}
