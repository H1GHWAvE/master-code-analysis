package android.support.v4.view;
 class fo extends android.support.v4.view.fm {
    java.util.WeakHashMap b;

    fo()
    {
        this.b = 0;
        return;
    }

    public final long a(android.view.View p3)
    {
        return p3.animate().getDuration();
    }

    public final void a(android.support.v4.view.fk p3, android.view.View p4)
    {
        p4.animate().scaleX(1065353216);
        return;
    }

    public final void a(android.support.v4.view.fk p2, android.view.View p3, float p4)
    {
        p3.animate().alpha(p4);
        return;
    }

    public void a(android.support.v4.view.fk p2, android.view.View p3, android.support.v4.view.gd p4)
    {
        p3.setTag(2113929216, p4);
        android.support.v4.view.fv.a(p3, new android.support.v4.view.fp(p2));
        return;
    }

    public void a(android.support.v4.view.fk p2, android.view.View p3, Runnable p4)
    {
        android.support.v4.view.fv.a(p3, new android.support.v4.view.fp(p2));
        android.support.v4.view.fk.a(p2, p4);
        return;
    }

    public final void a(android.view.View p3, long p4)
    {
        p3.animate().setDuration(p4);
        return;
    }

    public final void a(android.view.View p2, android.view.animation.Interpolator p3)
    {
        p2.animate().setInterpolator(p3);
        return;
    }

    public final void b(android.support.v4.view.fk p3, android.view.View p4)
    {
        p4.animate().scaleY(1065353216);
        return;
    }

    public final void b(android.support.v4.view.fk p2, android.view.View p3, float p4)
    {
        p3.animate().translationX(p4);
        return;
    }

    public void b(android.support.v4.view.fk p2, android.view.View p3, Runnable p4)
    {
        android.support.v4.view.fv.a(p3, new android.support.v4.view.fp(p2));
        android.support.v4.view.fk.b(p2, p4);
        return;
    }

    public final void b(android.view.View p3, long p4)
    {
        p3.animate().setStartDelay(p4);
        return;
    }

    public final long c(android.view.View p3)
    {
        return p3.animate().getStartDelay();
    }

    public final void c(android.support.v4.view.fk p2, android.view.View p3)
    {
        p3.animate().cancel();
        return;
    }

    public final void c(android.support.v4.view.fk p2, android.view.View p3, float p4)
    {
        p3.animate().translationY(p4);
        return;
    }

    public final void d(android.support.v4.view.fk p2, android.view.View p3)
    {
        p3.animate().start();
        return;
    }

    public final void d(android.support.v4.view.fk p2, android.view.View p3, float p4)
    {
        p3.animate().alphaBy(p4);
        return;
    }

    public void e(android.support.v4.view.fk p2, android.view.View p3)
    {
        android.support.v4.view.fk.a(p2, android.support.v4.view.cx.e(p3));
        android.support.v4.view.fv.a(p3, new android.support.v4.view.fp(p2));
        return;
    }

    public final void e(android.support.v4.view.fk p2, android.view.View p3, float p4)
    {
        p3.animate().rotation(p4);
        return;
    }

    public final void f(android.support.v4.view.fk p2, android.view.View p3, float p4)
    {
        p3.animate().rotationBy(p4);
        return;
    }

    public final void g(android.support.v4.view.fk p2, android.view.View p3, float p4)
    {
        p3.animate().rotationX(p4);
        return;
    }

    public final void h(android.support.v4.view.fk p2, android.view.View p3, float p4)
    {
        p3.animate().rotationXBy(p4);
        return;
    }

    public final void i(android.support.v4.view.fk p2, android.view.View p3, float p4)
    {
        p3.animate().rotationY(p4);
        return;
    }

    public final void j(android.support.v4.view.fk p2, android.view.View p3, float p4)
    {
        p3.animate().rotationYBy(p4);
        return;
    }

    public final void k(android.support.v4.view.fk p2, android.view.View p3, float p4)
    {
        p3.animate().scaleXBy(p4);
        return;
    }

    public final void l(android.support.v4.view.fk p2, android.view.View p3, float p4)
    {
        p3.animate().scaleYBy(p4);
        return;
    }

    public final void m(android.support.v4.view.fk p2, android.view.View p3, float p4)
    {
        p3.animate().x(p4);
        return;
    }

    public final void n(android.support.v4.view.fk p2, android.view.View p3, float p4)
    {
        p3.animate().xBy(p4);
        return;
    }

    public final void o(android.support.v4.view.fk p2, android.view.View p3, float p4)
    {
        p3.animate().y(p4);
        return;
    }

    public final void p(android.support.v4.view.fk p2, android.view.View p3, float p4)
    {
        p3.animate().yBy(p4);
        return;
    }

    public final void q(android.support.v4.view.fk p2, android.view.View p3, float p4)
    {
        p3.animate().translationXBy(p4);
        return;
    }

    public final void r(android.support.v4.view.fk p2, android.view.View p3, float p4)
    {
        p3.animate().translationYBy(p4);
        return;
    }
}
