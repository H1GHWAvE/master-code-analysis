package android.support.v4.view;
final class bh {

    bh()
    {
        return;
    }

    public static android.view.MenuItem a(android.view.MenuItem p1, android.support.v4.view.bj p2)
    {
        return p1.setOnActionExpandListener(new android.support.v4.view.bi(p2));
    }

    private static boolean a(android.view.MenuItem p1)
    {
        return p1.expandActionView();
    }

    private static boolean b(android.view.MenuItem p1)
    {
        return p1.collapseActionView();
    }

    private static boolean c(android.view.MenuItem p1)
    {
        return p1.isActionViewExpanded();
    }
}
