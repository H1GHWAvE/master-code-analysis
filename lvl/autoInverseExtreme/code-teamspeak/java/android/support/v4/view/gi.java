package android.support.v4.view;
final class gi extends android.support.v4.view.gh {
    final android.view.WindowInsets a;

    gi(android.view.WindowInsets p1)
    {
        this.a = p1;
        return;
    }

    private android.view.WindowInsets p()
    {
        return this.a;
    }

    public final int a()
    {
        return this.a.getSystemWindowInsetLeft();
    }

    public final android.support.v4.view.gh a(int p3, int p4, int p5, int p6)
    {
        return new android.support.v4.view.gi(this.a.replaceSystemWindowInsets(p3, p4, p5, p6));
    }

    public final android.support.v4.view.gh a(android.graphics.Rect p3)
    {
        return new android.support.v4.view.gi(this.a.replaceSystemWindowInsets(p3));
    }

    public final int b()
    {
        return this.a.getSystemWindowInsetTop();
    }

    public final int c()
    {
        return this.a.getSystemWindowInsetRight();
    }

    public final int d()
    {
        return this.a.getSystemWindowInsetBottom();
    }

    public final boolean e()
    {
        return this.a.hasSystemWindowInsets();
    }

    public final boolean f()
    {
        return this.a.hasInsets();
    }

    public final boolean g()
    {
        return this.a.isConsumed();
    }

    public final boolean h()
    {
        return this.a.isRound();
    }

    public final android.support.v4.view.gh i()
    {
        return new android.support.v4.view.gi(this.a.consumeSystemWindowInsets());
    }

    public final int j()
    {
        return this.a.getStableInsetTop();
    }

    public final int k()
    {
        return this.a.getStableInsetLeft();
    }

    public final int l()
    {
        return this.a.getStableInsetRight();
    }

    public final int m()
    {
        return this.a.getStableInsetBottom();
    }

    public final boolean n()
    {
        return this.a.hasStableInsets();
    }

    public final android.support.v4.view.gh o()
    {
        return new android.support.v4.view.gi(this.a.consumeStableInsets());
    }
}
