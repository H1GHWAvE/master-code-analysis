package android.support.v4.view;
 class fr extends android.support.v4.view.fo {

    fr()
    {
        return;
    }

    public final void a(android.support.v4.view.fk p3, android.view.View p4, android.support.v4.view.gd p5)
    {
        if (p5 == null) {
            p4.animate().setListener(0);
        } else {
            p4.animate().setListener(new android.support.v4.view.fy(p5, p4));
        }
        return;
    }

    public final void a(android.support.v4.view.fk p2, android.view.View p3, Runnable p4)
    {
        p3.animate().withEndAction(p4);
        return;
    }

    public final void b(android.support.v4.view.fk p2, android.view.View p3, Runnable p4)
    {
        p3.animate().withStartAction(p4);
        return;
    }

    public final void e(android.support.v4.view.fk p2, android.view.View p3)
    {
        p3.animate().withLayer();
        return;
    }
}
