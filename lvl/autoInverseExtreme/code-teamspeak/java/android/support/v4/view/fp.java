package android.support.v4.view;
final class fp implements android.support.v4.view.gd {
    android.support.v4.view.fk a;

    fp(android.support.v4.view.fk p1)
    {
        this.a = p1;
        return;
    }

    public final void a(android.view.View p4)
    {
        if (android.support.v4.view.fk.c(this.a) >= 0) {
            android.support.v4.view.cx.a(p4, 2, 0);
        }
        if (android.support.v4.view.fk.a(this.a) != null) {
            android.support.v4.view.fk.a(this.a).run();
        }
        int v0_9;
        int v0_8 = p4.getTag(2113929216);
        if (!(v0_8 instanceof android.support.v4.view.gd)) {
            v0_9 = 0;
        } else {
            v0_9 = ((android.support.v4.view.gd) v0_8);
        }
        if (v0_9 != 0) {
            v0_9.a(p4);
        }
        return;
    }

    public final void b(android.view.View p4)
    {
        if (android.support.v4.view.fk.c(this.a) >= 0) {
            android.support.v4.view.cx.a(p4, android.support.v4.view.fk.c(this.a), 0);
            android.support.v4.view.fk.a(this.a, -1);
        }
        if (android.support.v4.view.fk.b(this.a) != null) {
            android.support.v4.view.fk.b(this.a).run();
        }
        int v0_11;
        int v0_10 = p4.getTag(2113929216);
        if (!(v0_10 instanceof android.support.v4.view.gd)) {
            v0_11 = 0;
        } else {
            v0_11 = ((android.support.v4.view.gd) v0_10);
        }
        if (v0_11 != 0) {
            v0_11.b(p4);
        }
        return;
    }

    public final void c(android.view.View p4)
    {
        int v0_2;
        int v0_1 = p4.getTag(2113929216);
        if (!(v0_1 instanceof android.support.v4.view.gd)) {
            v0_2 = 0;
        } else {
            v0_2 = ((android.support.v4.view.gd) v0_1);
        }
        if (v0_2 != 0) {
            v0_2.c(p4);
        }
        return;
    }
}
