package android.support.v4.view;
public final class du {
    static final android.support.v4.view.dz a;

    static du()
    {
        if (android.os.Build$VERSION.SDK_INT < 14) {
            if (android.os.Build$VERSION.SDK_INT < 11) {
                if (android.os.Build$VERSION.SDK_INT < 8) {
                    android.support.v4.view.du.a = new android.support.v4.view.dv();
                } else {
                    android.support.v4.view.du.a = new android.support.v4.view.dw();
                }
            } else {
                android.support.v4.view.du.a = new android.support.v4.view.dx();
            }
        } else {
            android.support.v4.view.du.a = new android.support.v4.view.dy();
        }
        return;
    }

    public du()
    {
        return;
    }

    public static int a(android.view.ViewConfiguration p1)
    {
        return android.support.v4.view.du.a.a(p1);
    }

    public static boolean b(android.view.ViewConfiguration p1)
    {
        return android.support.v4.view.du.a.b(p1);
    }
}
