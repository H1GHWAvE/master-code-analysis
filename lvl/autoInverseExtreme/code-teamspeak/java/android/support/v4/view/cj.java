package android.support.v4.view;
final class cj extends android.text.method.SingleLineTransformationMethod {
    private static final String a = "SingleLineAllCapsTransform";
    private java.util.Locale b;

    public cj(android.content.Context p2)
    {
        this.b = p2.getResources().getConfiguration().locale;
        return;
    }

    public final CharSequence getTransformation(CharSequence p3, android.view.View p4)
    {
        int v0_1;
        int v0_0 = super.getTransformation(p3, p4);
        if (v0_0 == 0) {
            v0_1 = 0;
        } else {
            v0_1 = v0_0.toString().toUpperCase(this.b);
        }
        return v0_1;
    }
}
