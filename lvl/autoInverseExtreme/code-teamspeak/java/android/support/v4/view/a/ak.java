package android.support.v4.view.a;
final class ak {

    ak()
    {
        return;
    }

    private static Object a(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).getLabelFor();
    }

    private static void a(Object p0, android.view.View p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setLabelFor(p1);
        return;
    }

    private static void a(Object p0, android.view.View p1, int p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setLabelFor(p1, p2);
        return;
    }

    private static Object b(Object p1)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p1).getLabeledBy();
    }

    private static void b(Object p0, android.view.View p1)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setLabeledBy(p1);
        return;
    }

    private static void b(Object p0, android.view.View p1, int p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p0).setLabeledBy(p1, p2);
        return;
    }
}
