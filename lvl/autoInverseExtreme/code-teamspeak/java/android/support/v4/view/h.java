package android.support.v4.view;
final class h {

    h()
    {
        return;
    }

    private static Object a()
    {
        return new android.view.View$AccessibilityDelegate();
    }

    private static Object a(android.support.v4.view.j p1)
    {
        return new android.support.v4.view.i(p1);
    }

    private static void a(Object p0, android.view.View p1, int p2)
    {
        ((android.view.View$AccessibilityDelegate) p0).sendAccessibilityEvent(p1, p2);
        return;
    }

    private static void a(Object p0, android.view.View p1, Object p2)
    {
        ((android.view.View$AccessibilityDelegate) p0).onInitializeAccessibilityNodeInfo(p1, ((android.view.accessibility.AccessibilityNodeInfo) p2));
        return;
    }

    private static boolean a(Object p1, android.view.View p2, android.view.accessibility.AccessibilityEvent p3)
    {
        return ((android.view.View$AccessibilityDelegate) p1).dispatchPopulateAccessibilityEvent(p2, p3);
    }

    private static boolean a(Object p1, android.view.ViewGroup p2, android.view.View p3, android.view.accessibility.AccessibilityEvent p4)
    {
        return ((android.view.View$AccessibilityDelegate) p1).onRequestSendAccessibilityEvent(p2, p3, p4);
    }

    private static void b(Object p0, android.view.View p1, android.view.accessibility.AccessibilityEvent p2)
    {
        ((android.view.View$AccessibilityDelegate) p0).onInitializeAccessibilityEvent(p1, p2);
        return;
    }

    private static void c(Object p0, android.view.View p1, android.view.accessibility.AccessibilityEvent p2)
    {
        ((android.view.View$AccessibilityDelegate) p0).onPopulateAccessibilityEvent(p1, p2);
        return;
    }

    private static void d(Object p0, android.view.View p1, android.view.accessibility.AccessibilityEvent p2)
    {
        ((android.view.View$AccessibilityDelegate) p0).sendAccessibilityEventUnchecked(p1, p2);
        return;
    }
}
