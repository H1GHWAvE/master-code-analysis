package android.support.v4.view;
final class ap {
    private static final String a = "LayoutInflaterCompatHC";
    private static reflect.Field b;
    private static boolean c;

    ap()
    {
        return;
    }

    private static void a(android.view.LayoutInflater p3, android.support.v4.view.as p4)
    {
        int v1;
        if (p4 == null) {
            v1 = 0;
        } else {
            v1 = new android.support.v4.view.aq(p4);
        }
        p3.setFactory2(v1);
        android.view.LayoutInflater$Factory2 v0_3 = p3.getFactory();
        if (!(v0_3 instanceof android.view.LayoutInflater$Factory2)) {
            android.support.v4.view.ap.a(p3, v1);
        } else {
            android.support.v4.view.ap.a(p3, ((android.view.LayoutInflater$Factory2) v0_3));
        }
        return;
    }

    static void a(android.view.LayoutInflater p5, android.view.LayoutInflater$Factory2 p6)
    {
        if (!android.support.v4.view.ap.c) {
            try {
                IllegalAccessException v0_2 = android.view.LayoutInflater.getDeclaredField("mFactory2");
                android.support.v4.view.ap.b = v0_2;
                v0_2.setAccessible(1);
            } catch (IllegalAccessException v0_3) {
                android.util.Log.e("LayoutInflaterCompatHC", new StringBuilder("forceSetFactory2 Could not find field \'mFactory2\' on class ").append(android.view.LayoutInflater.getName()).append("; inflation may have unexpected results.").toString(), v0_3);
            }
            android.support.v4.view.ap.c = 1;
        }
        if (android.support.v4.view.ap.b != null) {
            try {
                android.support.v4.view.ap.b.set(p5, p6);
            } catch (IllegalAccessException v0_6) {
                android.util.Log.e("LayoutInflaterCompatHC", new StringBuilder("forceSetFactory2 could not set the Factory2 on LayoutInflater ").append(p5).append("; inflation may have unexpected results.").toString(), v0_6);
            }
        }
        return;
    }
}
