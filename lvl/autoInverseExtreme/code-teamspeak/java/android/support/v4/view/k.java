package android.support.v4.view;
final class k {

    k()
    {
        return;
    }

    private static Object a(android.support.v4.view.m p1)
    {
        return new android.support.v4.view.l(p1);
    }

    private static Object a(Object p1, android.view.View p2)
    {
        return ((android.view.View$AccessibilityDelegate) p1).getAccessibilityNodeProvider(p2);
    }

    private static boolean a(Object p1, android.view.View p2, int p3, android.os.Bundle p4)
    {
        return ((android.view.View$AccessibilityDelegate) p1).performAccessibilityAction(p2, p3, p4);
    }
}
