package android.support.v4.e.a;
 class e extends android.support.v4.e.a.d {

    e()
    {
        return;
    }

    public void b(android.graphics.drawable.Drawable p7, int p8)
    {
        if (!android.support.v4.e.a.n.b) {
            try {
                String v2_1 = new Class[1];
                v2_1[0] = Integer.TYPE;
                int v0_2 = android.graphics.drawable.Drawable.getDeclaredMethod("setLayoutDirection", v2_1);
                android.support.v4.e.a.n.a = v0_2;
                v0_2.setAccessible(1);
            } catch (int v0_3) {
                android.util.Log.i("DrawableCompatJellybeanMr1", "Failed to retrieve setLayoutDirection(int) method", v0_3);
            }
            android.support.v4.e.a.n.b = 1;
        }
        if (android.support.v4.e.a.n.a != null) {
            try {
                String v1_4 = new Object[1];
                v1_4[0] = Integer.valueOf(p8);
                android.support.v4.e.a.n.a.invoke(p7, v1_4);
            } catch (int v0_6) {
                android.util.Log.i("DrawableCompatJellybeanMr1", "Failed to invoke setLayoutDirection(int) via reflection", v0_6);
                android.support.v4.e.a.n.a = 0;
            }
        }
        return;
    }

    public int d(android.graphics.drawable.Drawable p2)
    {
        int v0 = android.support.v4.e.a.n.a(p2);
        if (v0 >= 0) {
            v0 = 0;
        }
        return v0;
    }
}
