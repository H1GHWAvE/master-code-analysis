package android.support.v4.k;
public abstract class a {
    static final String a = "DocumentFile";
    private final android.support.v4.k.a b;

    a(android.support.v4.k.a p1)
    {
        this.b = p1;
        return;
    }

    private static android.support.v4.k.a a(android.content.Context p2, android.net.Uri p3)
    {
        int v0_1;
        if (android.os.Build$VERSION.SDK_INT < 19) {
            v0_1 = 0;
        } else {
            v0_1 = new android.support.v4.k.e(p2, p3);
        }
        return v0_1;
    }

    private static android.support.v4.k.a a(java.io.File p2)
    {
        return new android.support.v4.k.d(0, p2);
    }

    private static android.support.v4.k.a b(android.content.Context p3, android.net.Uri p4)
    {
        int v0_1;
        if (android.os.Build$VERSION.SDK_INT < 21) {
            v0_1 = 0;
        } else {
            v0_1 = new android.support.v4.k.f(0, p3, android.provider.DocumentsContract.buildDocumentUriUsingTree(p4, android.provider.DocumentsContract.getTreeDocumentId(p4)));
        }
        return v0_1;
    }

    private android.support.v4.k.a c(String p6)
    {
        android.support.v4.k.a[] v2 = this.l();
        int v1 = 0;
        while (v1 < v2.length) {
            int v0_1 = v2[v1];
            if (!p6.equals(v0_1.b())) {
                v1++;
            }
            return v0_1;
        }
        v0_1 = 0;
        return v0_1;
    }

    private static boolean c(android.content.Context p2, android.net.Uri p3)
    {
        int v0_1;
        if (android.os.Build$VERSION.SDK_INT < 19) {
            v0_1 = 0;
        } else {
            v0_1 = android.provider.DocumentsContract.isDocumentUri(p2, p3);
        }
        return v0_1;
    }

    private android.support.v4.k.a m()
    {
        return this.b;
    }

    public abstract android.net.Uri a();

    public abstract android.support.v4.k.a a();

    public abstract android.support.v4.k.a a();

    public abstract String b();

    public abstract boolean b();

    public abstract String c();

    public abstract boolean d();

    public abstract boolean e();

    public abstract long f();

    public abstract long g();

    public abstract boolean h();

    public abstract boolean i();

    public abstract boolean j();

    public abstract boolean k();

    public abstract android.support.v4.k.a[] l();
}
