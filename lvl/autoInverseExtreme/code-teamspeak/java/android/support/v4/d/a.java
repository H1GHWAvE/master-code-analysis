package android.support.v4.d;
public final class a {

    private a()
    {
        return;
    }

    private static String a(String p2, String p3)
    {
        if (!android.text.TextUtils.isEmpty(p2)) {
            if (!android.text.TextUtils.isEmpty(p3)) {
                p3 = new StringBuilder("(").append(p2).append(") AND (").append(p3).append(")").toString();
            } else {
                p3 = p2;
            }
        }
        return p3;
    }

    private static String[] a(String[] p4, String[] p5)
    {
        if ((p4 != null) && (p4.length != 0)) {
            String[] v0_3 = new String[(p4.length + p5.length)];
            System.arraycopy(p4, 0, v0_3, 0, p4.length);
            System.arraycopy(p5, 0, v0_3, p4.length, p5.length);
            p5 = v0_3;
        }
        return p5;
    }
}
