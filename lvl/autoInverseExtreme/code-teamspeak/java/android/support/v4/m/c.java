package android.support.v4.m;
public final class c {
    private boolean a;
    private int b;
    private android.support.v4.m.l c;

    public c()
    {
        this.a(android.support.v4.m.a.a(java.util.Locale.getDefault()));
        return;
    }

    public c(java.util.Locale p2)
    {
        this.a(android.support.v4.m.a.a(p2));
        return;
    }

    public c(boolean p1)
    {
        this.a(p1);
        return;
    }

    private android.support.v4.m.c a(android.support.v4.m.l p1)
    {
        this.c = p1;
        return this;
    }

    private void a(boolean p2)
    {
        this.a = p2;
        this.c = android.support.v4.m.a.a();
        this.b = 2;
        return;
    }

    private android.support.v4.m.c b(boolean p2)
    {
        if (!p2) {
            this.b = (this.b & -3);
        } else {
            this.b = (this.b | 2);
        }
        return this;
    }

    private static android.support.v4.m.a c(boolean p1)
    {
        android.support.v4.m.a v0;
        if (!p1) {
            v0 = android.support.v4.m.a.c();
        } else {
            v0 = android.support.v4.m.a.b();
        }
        return v0;
    }

    public final android.support.v4.m.a a()
    {
        if ((this.b != 2) || (this.c != android.support.v4.m.a.a())) {
            android.support.v4.m.a v0_3 = new android.support.v4.m.a(this.a, this.b, this.c, 0);
        } else {
            if (!this.a) {
                v0_3 = android.support.v4.m.a.c();
            } else {
                v0_3 = android.support.v4.m.a.b();
            }
        }
        return v0_3;
    }
}
