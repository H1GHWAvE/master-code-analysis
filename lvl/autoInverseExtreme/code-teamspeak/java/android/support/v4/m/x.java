package android.support.v4.m;
final class x extends android.support.v4.m.w {

    private x()
    {
        this(0);
        return;
    }

    synthetic x(byte p1)
    {
        return;
    }

    public final int a(java.util.Locale p2)
    {
        return android.text.TextUtils.getLayoutDirectionFromLocale(p2);
    }

    public final String a(String p2)
    {
        return android.text.TextUtils.htmlEncode(p2);
    }
}
