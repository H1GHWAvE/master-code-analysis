package android.support.v4.m;
final class d {
    private static final int f = 1792;
    private static final byte[] g;
    final String a;
    final boolean b;
    final int c;
    int d;
    char e;

    static d()
    {
        int v0_0 = new byte[1792];
        android.support.v4.m.d.g = v0_0;
        int v0_1 = 0;
        while (v0_1 < 1792) {
            android.support.v4.m.d.g[v0_1] = Character.getDirectionality(v0_1);
            v0_1++;
        }
        return;
    }

    d(String p2)
    {
        this.a = p2;
        this.b = 0;
        this.c = p2.length();
        return;
    }

    static byte a(char p1)
    {
        byte v0_1;
        if (p1 >= 1792) {
            v0_1 = Character.getDirectionality(p1);
        } else {
            v0_1 = android.support.v4.m.d.g[p1];
        }
        return v0_1;
    }

    private int b()
    {
        int v4 = -1;
        this.d = 0;
        int v0 = 0;
        int v3_0 = 0;
        int v2 = 0;
        while ((this.d < this.c) && (v0 == 0)) {
            int v6_8;
            this.e = this.a.charAt(this.d);
            if (!Character.isHighSurrogate(this.e)) {
                this.d = (this.d + 1);
                v6_8 = android.support.v4.m.d.a(this.e);
                if (this.b) {
                    if (this.e != 60) {
                        if (this.e != 38) {
                            switch (v6_8) {
                                case 0:
                                    if (v2 != 0) {
                                        v0 = v2;
                                    }
                                    break;
                                case 1:
                                case 2:
                                    if (v2 != 0) {
                                        v0 = v2;
                                    } else {
                                        v4 = 1;
                                    }
                                    break;
                                case 3:
                                case 4:
                                case 5:
                                case 6:
                                case 7:
                                case 8:
                                case 10:
                                case 11:
                                case 12:
                                case 13:
                                default:
                                    v0 = v2;
                                    break;
                                case 14:
                                case 15:
                                    v2++;
                                    v3_0 = -1;
                                    break;
                                case 16:
                                case 17:
                                    v2++;
                                    v3_0 = 1;
                                    break;
                                case 18:
                                    v2--;
                                    v3_0 = 0;
                                    break;
                            }
                            return v4;
                        }
                        while (this.d < this.c) {
                            int v6_10 = this.a;
                            char v8_6 = this.d;
                            this.d = (v8_6 + 1);
                            int v6_11 = v6_10.charAt(v8_6);
                            this.e = v6_11;
                            if (v6_11 == 59) {
                                break;
                            }
                        }
                        v6_8 = 12;
                    } else {
                        int v6_12 = this.d;
                        while (this.d < this.c) {
                            char v8_9 = this.a;
                            char v9_3 = this.d;
                            this.d = (v9_3 + 1);
                            this.e = v8_9.charAt(v9_3);
                            if (this.e != 62) {
                                if ((this.e == 34) || (this.e == 39)) {
                                    char v8_14 = this.e;
                                    while (this.d < this.c) {
                                        char v9_8 = this.a;
                                        int v10_2 = this.d;
                                        this.d = (v10_2 + 1);
                                        char v9_9 = v9_8.charAt(v10_2);
                                        this.e = v9_9;
                                        if (v9_9 == v8_14) {
                                            break;
                                        }
                                    }
                                }
                            } else {
                                v6_8 = 12;
                            }
                        }
                        this.d = v6_12;
                        this.e = 60;
                        v6_8 = 13;
                    }
                }
            } else {
                int v6_14 = Character.codePointAt(this.a, this.d);
                this.d = (this.d + Character.charCount(v6_14));
                v6_8 = Character.getDirectionality(v6_14);
            }
        }
        if (v0 != 0) {
            if (v3_0 != 0) {
                v4 = v3_0;
                return v4;
            }
            while (this.d > 0) {
                switch (this.a()) {
                    case 14:
                    case 15:
                        if (v0 != v2) {
                            v2--;
                        }
                    case 16:
                    case 17:
                        if (v0 != v2) {
                            v2--;
                        } else {
                            v4 = 1;
                        }
                        break;
                    case 18:
                        v2++;
                        break;
                    default:
                }
            }
            v4 = 0;
        } else {
            v4 = 0;
        }
        return v4;
    }

    private int c()
    {
        int v1 = 0;
        this.d = this.c;
        int v0_1 = 0;
        int v2 = 0;
        while (this.d > 0) {
            switch (this.a()) {
                case 0:
                    if (v2 != 0) {
                        if (v0_1 == 0) {
                            v0_1 = v2;
                        }
                    } else {
                        v1 = -1;
                        break;
                    }
                    break;
                case 1:
                case 2:
                    if (v2 != 0) {
                        if (v0_1 == 0) {
                            v0_1 = v2;
                        }
                    } else {
                        v1 = 1;
                        break;
                    }
                    break;
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 10:
                case 11:
                case 12:
                case 13:
                default:
                    if (v0_1 == 0) {
                        v0_1 = v2;
                    }
                    break;
                case 14:
                case 15:
                    if (v0_1 != v2) {
                        v2--;
                    } else {
                        v1 = -1;
                        break;
                    }
                    break;
                case 16:
                case 17:
                    if (v0_1 != v2) {
                        v2--;
                    } else {
                        v1 = 1;
                        break;
                    }
                    break;
                case 18:
                    v2++;
                    break;
            }
        }
        return v1;
    }

    private byte d()
    {
        int v0_0 = 12;
        this.e = this.a.charAt(this.d);
        if (!Character.isHighSurrogate(this.e)) {
            this.d = (this.d + 1);
            char v1_7 = android.support.v4.m.d.a(this.e);
            if (this.b) {
                if (this.e != 60) {
                    if (this.e != 38) {
                        v0_0 = v1_7;
                        return v0_0;
                    }
                    while (this.d < this.c) {
                        char v1_9 = this.a;
                        char v2_5 = this.d;
                        this.d = (v2_5 + 1);
                        char v1_10 = v1_9.charAt(v2_5);
                        this.e = v1_10;
                        if (v1_10 == 59) {
                            break;
                        }
                    }
                    return v0_0;
                } else {
                    char v1_11 = this.d;
                    while (this.d < this.c) {
                        char v2_8 = this.a;
                        char v3_3 = this.d;
                        this.d = (v3_3 + 1);
                        this.e = v2_8.charAt(v3_3);
                        if (this.e != 62) {
                            if ((this.e == 34) || (this.e == 39)) {
                                char v2_13 = this.e;
                                while (this.d < this.c) {
                                    char v3_8 = this.a;
                                    int v4_2 = this.d;
                                    this.d = (v4_2 + 1);
                                    char v3_9 = v3_8.charAt(v4_2);
                                    this.e = v3_9;
                                    if (v3_9 == v2_13) {
                                        break;
                                    }
                                }
                            }
                        }
                        return v0_0;
                    }
                    this.d = v1_11;
                    this.e = 60;
                    v0_0 = 13;
                    return v0_0;
                }
            }
        } else {
            int v0_2 = Character.codePointAt(this.a, this.d);
            this.d = (this.d + Character.charCount(v0_2));
            v0_0 = Character.getDirectionality(v0_2);
        }
        return v0_0;
    }

    private byte e()
    {
        int v0_0 = this.d;
        while (this.d < this.c) {
            char v1_1 = this.a;
            char v2_1 = this.d;
            this.d = (v2_1 + 1);
            this.e = v1_1.charAt(v2_1);
            if (this.e != 62) {
                if ((this.e == 34) || (this.e == 39)) {
                    char v1_6 = this.e;
                    while (this.d < this.c) {
                        char v2_6 = this.a;
                        int v3_2 = this.d;
                        this.d = (v3_2 + 1);
                        char v2_7 = v2_6.charAt(v3_2);
                        this.e = v2_7;
                        if (v2_7 == v1_6) {
                            break;
                        }
                    }
                }
            } else {
                int v0_2 = 12;
            }
            return v0_2;
        }
        this.d = v0_0;
        this.e = 60;
        v0_2 = 13;
        return v0_2;
    }

    private byte f()
    {
        int v0_0 = this.d;
        while (this.d > 0) {
            char v1_1 = this.a;
            char v2_1 = (this.d - 1);
            this.d = v2_1;
            this.e = v1_1.charAt(v2_1);
            if (this.e != 60) {
                if (this.e == 62) {
                    break;
                }
                if ((this.e == 34) || (this.e == 39)) {
                    char v1_7 = this.e;
                    while (this.d > 0) {
                        char v2_6 = this.a;
                        int v3_1 = (this.d - 1);
                        this.d = v3_1;
                        char v2_7 = v2_6.charAt(v3_1);
                        this.e = v2_7;
                        if (v2_7 == v1_7) {
                            break;
                        }
                    }
                }
            } else {
                int v0_1 = 12;
            }
            return v0_1;
        }
        this.d = v0_0;
        this.e = 62;
        v0_1 = 13;
        return v0_1;
    }

    private byte g()
    {
        if ((int v0_2 == 59) || (this.d >= this.c)) {
            return 12;
        } else {
            int v0_1 = this.a;
            int v1_1 = this.d;
            this.d = (v1_1 + 1);
            v0_2 = v0_1.charAt(v1_1);
            this.e = v0_2;
        }
    }

    private byte h()
    {
        int v0_0 = this.d;
        while (this.d > 0) {
            char v1_1 = this.a;
            int v2_1 = (this.d - 1);
            this.d = v2_1;
            this.e = v1_1.charAt(v2_1);
            if (this.e != 38) {
                if (this.e == 59) {
                    break;
                }
            } else {
                int v0_1 = 12;
            }
            return v0_1;
        }
        this.d = v0_0;
        this.e = 59;
        v0_1 = 13;
        return v0_1;
    }

    final byte a()
    {
        int v0_0 = 12;
        this.e = this.a.charAt((this.d - 1));
        if (!Character.isLowSurrogate(this.e)) {
            this.d = (this.d - 1);
            int v2_7 = android.support.v4.m.d.a(this.e);
            if (this.b) {
                if (this.e != 62) {
                    if (this.e == 59) {
                        int v2_8 = this.d;
                        while (this.d > 0) {
                            char v3_6 = this.a;
                            char v4_1 = (this.d - 1);
                            this.d = v4_1;
                            this.e = v3_6.charAt(v4_1);
                            if (this.e == 38) {
                                return v0_0;
                            } else {
                                if (this.e == 59) {
                                    break;
                                }
                            }
                        }
                        this.d = v2_8;
                        this.e = 59;
                        v0_0 = 13;
                        return v0_0;
                    }
                } else {
                    int v2_9 = this.d;
                    while (this.d > 0) {
                        char v3_11 = this.a;
                        char v4_4 = (this.d - 1);
                        this.d = v4_4;
                        this.e = v3_11.charAt(v4_4);
                        if (this.e == 60) {
                            return v0_0;
                        } else {
                            if (this.e == 62) {
                                break;
                            }
                            if ((this.e == 34) || (this.e == 39)) {
                                char v3_17 = this.e;
                                while (this.d > 0) {
                                    char v4_9 = this.a;
                                    int v5_2 = (this.d - 1);
                                    this.d = v5_2;
                                    char v4_10 = v4_9.charAt(v5_2);
                                    this.e = v4_10;
                                    if (v4_10 == v3_17) {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    this.d = v2_9;
                    this.e = 62;
                    v0_0 = 13;
                    return v0_0;
                }
            }
            v0_0 = v2_7;
        } else {
            int v0_2 = Character.codePointBefore(this.a, this.d);
            this.d = (this.d - Character.charCount(v0_2));
            v0_0 = Character.getDirectionality(v0_2);
        }
        return v0_0;
    }
}
