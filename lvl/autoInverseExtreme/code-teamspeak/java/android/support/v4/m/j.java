package android.support.v4.m;
public final class j {
    private static final String a = "ICUCompatIcs";
    private static reflect.Method b;

    static j()
    {
        try {
            Exception v0_1 = Class.forName("libcore.icu.ICU");
            Class[] v2_1 = new Class[1];
            v2_1[0] = java.util.Locale;
            android.support.v4.m.j.b = v0_1.getMethod("addLikelySubtags", v2_1);
            return;
        } catch (Exception v0_3) {
            throw new IllegalStateException(v0_3);
        }
    }

    public j()
    {
        return;
    }

    public static String a(java.util.Locale p3)
    {
        try {
            String v0_1 = new Object[1];
            v0_1[0] = p3;
            String v0_4 = ((java.util.Locale) android.support.v4.m.j.b.invoke(0, v0_1)).getScript();
        } catch (String v0_5) {
            android.util.Log.w("ICUCompatIcs", v0_5);
            v0_4 = p3.getScript();
        } catch (String v0_6) {
            android.util.Log.w("ICUCompatIcs", v0_6);
        }
        return v0_4;
    }
}
