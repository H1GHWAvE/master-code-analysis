package android.support.v7.app;
public class ah extends android.support.v4.app.bb implements android.support.v4.app.gm, android.support.v7.app.ai, android.support.v7.app.m {
    private android.support.v7.app.aj m;

    public ah()
    {
        return;
    }

    private android.support.v7.c.a a(android.support.v7.c.b p2)
    {
        return this.i().a(p2);
    }

    private void a(android.support.v4.app.gl p3)
    {
        android.content.ComponentName v0_0 = 0;
        if ((this instanceof android.support.v4.app.gm)) {
            v0_0 = ((android.support.v4.app.gm) this).a_();
        }
        android.content.Intent v1_1;
        if (v0_0 != null) {
            v1_1 = v0_0;
        } else {
            v1_1 = android.support.v4.app.cv.a(this);
        }
        if (v1_1 != null) {
            android.content.ComponentName v0_4 = v1_1.getComponent();
            if (v0_4 == null) {
                v0_4 = v1_1.resolveActivity(p3.b.getPackageManager());
            }
            p3.a(v0_4);
            p3.a(v1_1);
        }
        return;
    }

    private void a(android.support.v7.widget.Toolbar p2)
    {
        this.i().a(p2);
        return;
    }

    private boolean a(android.content.Intent p2)
    {
        return android.support.v4.app.cv.a(this, p2);
    }

    private void b(android.content.Intent p1)
    {
        android.support.v4.app.cv.b(this, p1);
        return;
    }

    private boolean b(int p2)
    {
        return this.i().b(p2);
    }

    private android.support.v7.app.a j()
    {
        return this.i().a();
    }

    private static void k()
    {
        return;
    }

    private static void l()
    {
        return;
    }

    private static void m()
    {
        return;
    }

    private static void n()
    {
        return;
    }

    private static void o()
    {
        return;
    }

    private static void p()
    {
        return;
    }

    public final android.content.Intent a_()
    {
        return android.support.v4.app.cv.a(this);
    }

    public void addContentView(android.view.View p2, android.view.ViewGroup$LayoutParams p3)
    {
        this.i().b(p2, p3);
        return;
    }

    public final void b()
    {
        this.i().g();
        return;
    }

    public final android.support.v7.app.l d()
    {
        return this.i().i();
    }

    public final void e()
    {
        return;
    }

    public final void f()
    {
        return;
    }

    public final android.support.v7.c.a g()
    {
        return 0;
    }

    public android.view.MenuInflater getMenuInflater()
    {
        return this.i().b();
    }

    public boolean h()
    {
        int v0_1;
        int v0_0 = android.support.v4.app.cv.a(this);
        try {
            if (v0_0 == 0) {
                v0_1 = 0;
            } else {
                if (!android.support.v4.app.cv.a(this, v0_0)) {
                    android.support.v4.app.cv.b(this, v0_0);
                    v0_1 = 1;
                } else {
                    android.support.v4.app.gl v3 = android.support.v4.app.gl.a(this);
                    int v0_2 = 0;
                    if ((this instanceof android.support.v4.app.gm)) {
                        v0_2 = ((android.support.v4.app.gm) this).a_();
                    }
                    int v2_2;
                    if (v0_2 != 0) {
                        v2_2 = v0_2;
                    } else {
                        v2_2 = android.support.v4.app.cv.a(this);
                    }
                    if (v2_2 != 0) {
                        int v0_6 = v2_2.getComponent();
                        if (v0_6 == 0) {
                            v0_6 = v2_2.resolveActivity(v3.b.getPackageManager());
                        }
                        v3.a(v0_6);
                        v3.a(v2_2);
                    }
                    if (!v3.a.isEmpty()) {
                        int v2_5 = new android.content.Intent[v3.a.size()];
                        int v0_13 = ((android.content.Intent[]) v3.a.toArray(v2_5));
                        v0_13[0] = new android.content.Intent(v0_13[0]).addFlags(268484608);
                        if (!android.support.v4.c.h.a(v3.b, v0_13)) {
                            int v1_4 = new android.content.Intent(v0_13[(v0_13.length - 1)]);
                            v1_4.addFlags(268435456);
                            v3.b.startActivity(v1_4);
                        }
                        if (android.os.Build$VERSION.SDK_INT < 16) {
                            this.finish();
                        } else {
                            this.finishAffinity();
                        }
                    } else {
                        throw new IllegalStateException("No intents added to TaskStackBuilder; cannot startActivities");
                    }
                }
            }
        } catch (int v0) {
            this.finish();
        }
        return v0_1;
    }

    public final android.support.v7.app.aj i()
    {
        if (this.m == null) {
            this.m = android.support.v7.app.aj.a(this, this.getWindow(), this);
        }
        return this.m;
    }

    public void invalidateOptionsMenu()
    {
        this.i().g();
        return;
    }

    public void onConfigurationChanged(android.content.res.Configuration p2)
    {
        super.onConfigurationChanged(p2);
        this.i().a(p2);
        return;
    }

    public void onContentChanged()
    {
        return;
    }

    public void onCreate(android.os.Bundle p2)
    {
        this.i().j();
        this.i().c();
        super.onCreate(p2);
        return;
    }

    public void onDestroy()
    {
        super.onDestroy();
        this.i().h();
        return;
    }

    public final boolean onMenuItemSelected(int p4, android.view.MenuItem p5)
    {
        boolean v0_5;
        if (!super.onMenuItemSelected(p4, p5)) {
            boolean v0_2 = this.i().a();
            if ((p5.getItemId() != 16908332) || ((!v0_2) || ((v0_2.h() & 4) == 0))) {
                v0_5 = 0;
            } else {
                v0_5 = this.h();
            }
        } else {
            v0_5 = 1;
        }
        return v0_5;
    }

    public boolean onMenuOpened(int p2, android.view.Menu p3)
    {
        return super.onMenuOpened(p2, p3);
    }

    public void onPanelClosed(int p1, android.view.Menu p2)
    {
        super.onPanelClosed(p1, p2);
        return;
    }

    protected void onPostCreate(android.os.Bundle p2)
    {
        super.onPostCreate(p2);
        this.i().d();
        return;
    }

    protected void onPostResume()
    {
        super.onPostResume();
        this.i().f();
        return;
    }

    protected void onStop()
    {
        super.onStop();
        this.i().e();
        return;
    }

    protected void onTitleChanged(CharSequence p2, int p3)
    {
        super.onTitleChanged(p2, p3);
        this.i().a(p2);
        return;
    }

    public void setContentView(int p2)
    {
        this.i().a(p2);
        return;
    }

    public void setContentView(android.view.View p2)
    {
        this.i().a(p2);
        return;
    }

    public void setContentView(android.view.View p2, android.view.ViewGroup$LayoutParams p3)
    {
        this.i().a(p2, p3);
        return;
    }
}
