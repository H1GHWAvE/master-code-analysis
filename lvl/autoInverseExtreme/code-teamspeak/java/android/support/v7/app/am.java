package android.support.v7.app;
final class am implements android.support.v7.app.l {
    final synthetic android.support.v7.app.ak a;

    private am(android.support.v7.app.ak p1)
    {
        this.a = p1;
        return;
    }

    synthetic am(android.support.v7.app.ak p1, byte p2)
    {
        this(p1);
        return;
    }

    public final android.graphics.drawable.Drawable a()
    {
        android.content.res.TypedArray v0_1 = this.a.m();
        int[] v2_1 = new int[1];
        v2_1[0] = android.support.v7.a.d.homeAsUpIndicator;
        android.content.res.TypedArray v0_2 = android.support.v7.internal.widget.ax.a(v0_1, 0, v2_1);
        android.graphics.drawable.Drawable v1_1 = v0_2.a(0);
        v0_2.a.recycle();
        return v1_1;
    }

    public final void a(int p2)
    {
        android.support.v7.app.a v0_1 = this.a.a();
        if (v0_1 != null) {
            v0_1.l(p2);
        }
        return;
    }

    public final void a(android.graphics.drawable.Drawable p2, int p3)
    {
        android.support.v7.app.a v0_1 = this.a.a();
        if (v0_1 != null) {
            v0_1.e(p2);
            v0_1.l(p3);
        }
        return;
    }

    public final android.content.Context b()
    {
        return this.a.m();
    }

    public final boolean c()
    {
        int v0_4;
        int v0_1 = this.a.a();
        if ((v0_1 == 0) || ((v0_1.h() & 4) == 0)) {
            v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }
}
