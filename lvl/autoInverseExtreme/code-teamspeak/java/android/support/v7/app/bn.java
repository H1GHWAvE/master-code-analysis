package android.support.v7.app;
public final class bn extends android.support.v4.app.ed {
    int[] a;
    android.support.v4.media.session.MediaSessionCompat$Token b;
    boolean c;
    android.app.PendingIntent h;

    public bn()
    {
        this.a = 0;
        return;
    }

    private bn(android.support.v4.app.dm p2)
    {
        this.a = 0;
        this.a(p2);
        return;
    }

    private android.support.v7.app.bn a(android.app.PendingIntent p1)
    {
        this.h = p1;
        return this;
    }

    private android.support.v7.app.bn a(android.support.v4.media.session.MediaSessionCompat$Token p1)
    {
        this.b = p1;
        return this;
    }

    private android.support.v7.app.bn a(boolean p1)
    {
        this.c = p1;
        return this;
    }

    private varargs android.support.v7.app.bn a(int[] p1)
    {
        this.a = p1;
        return this;
    }
}
