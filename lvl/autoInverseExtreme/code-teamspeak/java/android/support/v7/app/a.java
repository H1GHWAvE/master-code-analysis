package android.support.v7.app;
public abstract class a {
    public static final int a = 0;
    public static final int b = 1;
    public static final int c = 2;
    public static final int d = 1;
    public static final int e = 2;
    public static final int f = 4;
    public static final int g = 8;
    public static final int h = 16;

    public a()
    {
        return;
    }

    public abstract int a();

    public android.support.v7.c.a a(android.support.v7.c.b p2)
    {
        return 0;
    }

    public void a(float p3)
    {
        if (p3 == 0) {
            return;
        } else {
            throw new UnsupportedOperationException("Setting a non-zero elevation is not supported in this action bar configuration.");
        }
    }

    public abstract void a();

    public abstract void a();

    public void a(android.content.res.Configuration p1)
    {
        return;
    }

    public abstract void a();

    public abstract void a();

    public abstract void a();

    public abstract void a();

    public abstract void a();

    public abstract void a();

    public abstract void a();

    public abstract void a();

    public abstract void a();

    public abstract void a();

    public abstract void a();

    public boolean a(int p2, android.view.KeyEvent p3)
    {
        return 0;
    }

    public boolean a(android.view.KeyEvent p2)
    {
        return 0;
    }

    public abstract int b();

    public abstract void b();

    public abstract void b();

    public abstract void b();

    public abstract void b();

    public abstract void b();

    public abstract void b();

    public void c()
    {
        return;
    }

    public abstract void c();

    public abstract void c();

    public abstract void c();

    public void c(CharSequence p1)
    {
        return;
    }

    public abstract void c();

    public abstract android.view.View d();

    public abstract void d();

    public void d(android.graphics.drawable.Drawable p1)
    {
        return;
    }

    public void d(CharSequence p1)
    {
        return;
    }

    public abstract void d();

    public abstract CharSequence e();

    public abstract void e();

    public void e(android.graphics.drawable.Drawable p1)
    {
        return;
    }

    public abstract void e();

    public abstract CharSequence f();

    public abstract void f();

    public void f(boolean p1)
    {
        return;
    }

    public abstract int g();

    public abstract void g();

    public void g(boolean p1)
    {
        return;
    }

    public abstract int h();

    public abstract void h();

    public void h(boolean p1)
    {
        return;
    }

    public abstract android.support.v7.app.g i();

    public abstract void i();

    public abstract android.support.v7.app.g j();

    public abstract void j();

    public abstract android.support.v7.app.g k();

    public void k(int p1)
    {
        return;
    }

    public abstract int l();

    public void l(int p1)
    {
        return;
    }

    public abstract int m();

    public void m(int p3)
    {
        if (p3 == 0) {
            return;
        } else {
            throw new UnsupportedOperationException("Setting an explicit action bar hide offset is not supported in this action bar configuration.");
        }
    }

    public abstract void n();

    public abstract void o();

    public abstract boolean p();

    public void q()
    {
        return;
    }

    public android.content.Context r()
    {
        return 0;
    }

    public boolean s()
    {
        return 0;
    }

    public void t()
    {
        throw new UnsupportedOperationException("Hide on content scroll is not supported in this action bar configuration.");
    }

    public boolean u()
    {
        return 0;
    }

    public int v()
    {
        return 0;
    }

    public float w()
    {
        return 0;
    }

    public boolean x()
    {
        return 0;
    }

    public boolean y()
    {
        return 0;
    }

    public boolean z()
    {
        return 0;
    }
}
