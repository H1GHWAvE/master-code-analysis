package android.support.v7.app;
 class aq extends android.support.v7.app.an {
    final synthetic android.support.v7.app.ap b;

    aq(android.support.v7.app.ap p1, android.view.Window$Callback p2)
    {
        this.b = p1;
        this(p1, p2);
        return;
    }

    final android.view.ActionMode a(android.view.ActionMode$Callback p3)
    {
        int v0_2;
        int v0_1 = new android.support.v7.internal.view.e(this.b.e, p3);
        android.support.v7.c.a v1_3 = this.b.a(v0_1);
        if (v1_3 == null) {
            v0_2 = 0;
        } else {
            v0_2 = v0_1.b(v1_3);
        }
        return v0_2;
    }

    public android.view.ActionMode onWindowStartingActionMode(android.view.ActionMode$Callback p2)
    {
        android.view.ActionMode v0_2;
        if (!this.b.s) {
            v0_2 = super.onWindowStartingActionMode(p2);
        } else {
            v0_2 = this.a(p2);
        }
        return v0_2;
    }
}
