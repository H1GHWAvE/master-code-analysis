package android.support.v7.app;
public class c extends android.view.ViewGroup$MarginLayoutParams {
    public int a;

    public c(int p2)
    {
        this(-2, p2);
        this.a = 0;
        this.a = 8388627;
        return;
    }

    private c(int p3, byte p4)
    {
        this(-2, -1);
        this.a = 0;
        this.a = p3;
        return;
    }

    private c(int p2, char p3)
    {
        this(p2, 0);
        return;
    }

    public c(android.content.Context p4, android.util.AttributeSet p5)
    {
        this(p4, p5);
        this.a = 0;
        android.content.res.TypedArray v0_1 = p4.obtainStyledAttributes(p5, android.support.v7.a.n.ActionBarLayout);
        this.a = v0_1.getInt(android.support.v7.a.n.ActionBarLayout_android_layout_gravity, 0);
        v0_1.recycle();
        return;
    }

    public c(android.support.v7.app.c p2)
    {
        this(p2);
        this.a = 0;
        this.a = p2.a;
        return;
    }

    public c(android.view.ViewGroup$LayoutParams p2)
    {
        this(p2);
        this.a = 0;
        return;
    }
}
