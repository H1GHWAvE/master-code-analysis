package android.support.v7.app;
final class ab implements android.widget.AdapterView$OnItemClickListener {
    final synthetic android.widget.ListView a;
    final synthetic android.support.v7.app.v b;
    final synthetic android.support.v7.app.x c;

    ab(android.support.v7.app.x p1, android.widget.ListView p2, android.support.v7.app.v p3)
    {
        this.c = p1;
        this.a = p2;
        this.b = p3;
        return;
    }

    public final void onItemClick(android.widget.AdapterView p4, android.view.View p5, int p6, long p7)
    {
        if (this.c.C != null) {
            this.c.C[p6] = this.a.isItemChecked(p6);
        }
        this.c.G.onClick(this.b.b, p6, this.a.isItemChecked(p6));
        return;
    }
}
