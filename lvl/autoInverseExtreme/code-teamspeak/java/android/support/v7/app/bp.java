package android.support.v7.app;
final class bp {
    private static final String a = "TwilightManager";
    private static final int b = 6;
    private static final int c = 22;
    private static final android.support.v7.app.br d;
    private final android.content.Context e;
    private final android.location.LocationManager f;

    static bp()
    {
        android.support.v7.app.bp.d = new android.support.v7.app.br(0);
        return;
    }

    private bp(android.content.Context p2)
    {
        this.e = p2;
        this.f = ((android.location.LocationManager) p2.getSystemService("location"));
        return;
    }

    private android.location.Location a(String p4)
    {
        try {
            if ((this.f == null) || (!this.f.isProviderEnabled(p4))) {
                android.location.Location v0_4 = 0;
            } else {
                v0_4 = this.f.getLastKnownLocation(p4);
            }
        } catch (android.location.Location v0_3) {
            android.util.Log.d("TwilightManager", "Failed to get last known location", v0_3);
        }
        return v0_4;
    }

    private static void a(android.location.Location p20)
    {
        android.support.v7.app.br v10 = android.support.v7.app.bp.d;
        long v12 = System.currentTimeMillis();
        if (android.support.v7.app.bo.a == null) {
            android.support.v7.app.bo.a = new android.support.v7.app.bo();
        }
        int v2_4;
        long v3_0 = android.support.v7.app.bo.a;
        v3_0.a((v12 - 86400000), p20.getLatitude(), p20.getLongitude());
        long v14 = v3_0.d;
        v3_0.a(v12, p20.getLatitude(), p20.getLongitude());
        if (v3_0.f != 1) {
            v2_4 = 0;
        } else {
            v2_4 = 1;
        }
        long v4_12;
        long v16 = v3_0.e;
        long v18 = v3_0.d;
        v3_0.a((86400000 + v12), p20.getLatitude(), p20.getLongitude());
        long v6_3 = v3_0.e;
        if ((v16 != -1) && (v18 != -1)) {
            long v4_9;
            if (v12 <= v18) {
                if (v12 <= v16) {
                    v4_9 = (0 + v16);
                } else {
                    v4_9 = (0 + v18);
                }
            } else {
                v4_9 = (0 + v6_3);
            }
            v4_12 = (v4_9 + 60000);
        } else {
            v4_12 = (43200000 + v12);
        }
        v10.a = v2_4;
        v10.b = v14;
        v10.c = v16;
        v10.d = v18;
        v10.e = v6_3;
        v10.f = v4_12;
        return;
    }

    private boolean a()
    {
        boolean v2_2;
        android.support.v7.app.br v11 = android.support.v7.app.bp.d;
        if ((v11 == null) || (v11.f <= System.currentTimeMillis())) {
            v2_2 = 0;
        } else {
            v2_2 = 1;
        }
        boolean v2_8;
        if (!v2_2) {
            boolean v2_3 = 0;
            long v3_0 = 0;
            if (android.support.v4.c.ar.a(this.e, "android.permission.ACCESS_FINE_LOCATION") == 0) {
                v2_3 = this.a("network");
            }
            if (android.support.v4.c.ar.a(this.e, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
                v3_0 = this.a("gps");
            }
            if ((!v2_3) || (v3_0 == 0)) {
                if (v3_0 == 0) {
                    android.location.Location v10 = v2_3;
                } else {
                    v10 = v3_0;
                }
            } else {
                if (v3_0.getTime() <= v2_3.getTime()) {
                    v10 = v2_3;
                } else {
                    v10 = v3_0;
                }
            }
            if (v10 == null) {
                android.util.Log.i("TwilightManager", "Could not get last known location. This is probably because the app does not have any location permissions. Falling back to hardcoded sunrise/sunset values.");
                boolean v2_7 = java.util.Calendar.getInstance().get(11);
                if ((v2_7 >= 6) && (v2_7 < 22)) {
                    v2_8 = 0;
                } else {
                    v2_8 = 1;
                }
            } else {
                android.support.v7.app.br v12 = android.support.v7.app.bp.d;
                long v14 = System.currentTimeMillis();
                if (android.support.v7.app.bo.a == null) {
                    android.support.v7.app.bo.a = new android.support.v7.app.bo();
                }
                boolean v2_13;
                long v3_6 = android.support.v7.app.bo.a;
                v3_6.a((v14 - 86400000), v10.getLatitude(), v10.getLongitude());
                long v16 = v3_6.d;
                v3_6.a(v14, v10.getLatitude(), v10.getLongitude());
                if (v3_6.f != 1) {
                    v2_13 = 0;
                } else {
                    v2_13 = 1;
                }
                long v4_19;
                long v18 = v3_6.e;
                long v20 = v3_6.d;
                v3_6.a((86400000 + v14), v10.getLatitude(), v10.getLongitude());
                long v6_4 = v3_6.e;
                if ((v18 != -1) && (v20 != -1)) {
                    long v4_16;
                    if (v14 <= v20) {
                        if (v14 <= v18) {
                            v4_16 = (0 + v18);
                        } else {
                            v4_16 = (0 + v20);
                        }
                    } else {
                        v4_16 = (0 + v6_4);
                    }
                    v4_19 = (v4_16 + 60000);
                } else {
                    v4_19 = (43200000 + v14);
                }
                v12.a = v2_13;
                v12.b = v16;
                v12.c = v18;
                v12.d = v20;
                v12.e = v6_4;
                v12.f = v4_19;
                v2_8 = v11.a;
            }
        } else {
            v2_8 = v11.a;
        }
        return v2_8;
    }

    private static boolean a(android.support.v7.app.br p4)
    {
        if ((p4 == null) || (p4.f <= System.currentTimeMillis())) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private android.location.Location b()
    {
        android.location.Location v0_2;
        android.location.Location v1_0 = 0;
        if (android.support.v4.c.ar.a(this.e, "android.permission.ACCESS_FINE_LOCATION") != 0) {
            v0_2 = 0;
        } else {
            v0_2 = this.a("network");
        }
        if (android.support.v4.c.ar.a(this.e, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            v1_0 = this.a("gps");
        }
        if ((v0_2 == null) || (v1_0 == null)) {
            if (v1_0 != null) {
                v0_2 = v1_0;
            }
        } else {
            if (v1_0.getTime() > v0_2.getTime()) {
                v0_2 = v1_0;
            }
        }
        return v0_2;
    }
}
