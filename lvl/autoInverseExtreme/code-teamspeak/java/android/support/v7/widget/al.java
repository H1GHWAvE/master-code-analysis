package android.support.v7.widget;
public class al extends android.view.ViewGroup$MarginLayoutParams {
    public float g;
    public int h;

    public al()
    {
        this(0, -1);
        this.h = -1;
        this.g = 1065353216;
        return;
    }

    public al(int p2, int p3)
    {
        this(p2, p3);
        this.h = -1;
        this.g = 0;
        return;
    }

    public al(android.content.Context p5, android.util.AttributeSet p6)
    {
        this(p5, p6);
        this.h = -1;
        android.content.res.TypedArray v0_1 = p5.obtainStyledAttributes(p6, android.support.v7.a.n.LinearLayoutCompat_Layout);
        this.g = v0_1.getFloat(android.support.v7.a.n.LinearLayoutCompat_Layout_android_layout_weight, 0);
        this.h = v0_1.getInt(android.support.v7.a.n.LinearLayoutCompat_Layout_android_layout_gravity, -1);
        v0_1.recycle();
        return;
    }

    private al(android.support.v7.widget.al p2)
    {
        this(p2);
        this.h = -1;
        this.g = p2.g;
        this.h = p2.h;
        return;
    }

    public al(android.view.ViewGroup$LayoutParams p2)
    {
        this(p2);
        this.h = -1;
        return;
    }

    private al(android.view.ViewGroup$MarginLayoutParams p2)
    {
        this(p2);
        this.h = -1;
        return;
    }
}
