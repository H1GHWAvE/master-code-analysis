package android.support.v7.widget;
public final class cf {
    private final android.content.Context a;
    private final android.view.LayoutInflater b;
    private android.view.LayoutInflater c;

    private cf(android.content.Context p2)
    {
        this.a = p2;
        this.b = android.view.LayoutInflater.from(p2);
        return;
    }

    private android.content.res.Resources$Theme a()
    {
        android.content.res.Resources$Theme v0_3;
        if (this.c != null) {
            v0_3 = this.c.getContext().getTheme();
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    private void a(android.content.res.Resources$Theme p3)
    {
        if (p3 != null) {
            if (p3 != this.a.getTheme()) {
                this.c = android.view.LayoutInflater.from(new android.support.v7.internal.view.b(this.a, p3));
            } else {
                this.c = this.b;
            }
        } else {
            this.c = 0;
        }
        return;
    }

    private android.view.LayoutInflater b()
    {
        android.view.LayoutInflater v0_1;
        if (this.c == null) {
            v0_1 = this.b;
        } else {
            v0_1 = this.c;
        }
        return v0_1;
    }
}
