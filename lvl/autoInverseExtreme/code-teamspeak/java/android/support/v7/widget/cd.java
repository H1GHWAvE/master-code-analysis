package android.support.v7.widget;
final class cd extends android.view.animation.Animation {
    final float a;
    final float b;
    final float c;
    final synthetic android.support.v7.widget.cb d;

    private cd(android.support.v7.widget.cb p2, float p3, float p4)
    {
        this.d = p2;
        this.a = p3;
        this.b = p4;
        this.c = (p4 - p3);
        return;
    }

    synthetic cd(android.support.v7.widget.cb p1, float p2, float p3, byte p4)
    {
        this(p1, p2, p3);
        return;
    }

    protected final void applyTransformation(float p4, android.view.animation.Transformation p5)
    {
        android.support.v7.widget.cb.a(this.d, (this.a + (this.c * p4)));
        return;
    }
}
