package android.support.v7.widget;
public class ActionMenuView extends android.support.v7.widget.aj implements android.support.v7.internal.view.menu.k, android.support.v7.internal.view.menu.z {
    static final int a = 56;
    static final int b = 4;
    private static final String n = "ActionMenuView";
    public android.support.v7.internal.view.menu.i c;
    public boolean d;
    public android.support.v7.widget.ActionMenuPresenter e;
    android.support.v7.internal.view.menu.y f;
    android.support.v7.internal.view.menu.j g;
    private android.content.Context o;
    private int p;
    private boolean q;
    private int r;
    private int s;
    private int t;
    private android.support.v7.widget.o u;

    public ActionMenuView(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    public ActionMenuView(android.content.Context p4, android.util.AttributeSet p5)
    {
        this(p4, p5);
        this.setBaselineAligned(0);
        int v0_2 = p4.getResources().getDisplayMetrics().density;
        this.s = ((int) (1113587712 * v0_2));
        this.t = ((int) (v0_2 * 1082130432));
        this.o = p4;
        this.p = 0;
        return;
    }

    static int a(android.view.View p8, int p9, int p10, int p11, int p12)
    {
        boolean v1_3;
        int v2_0 = 0;
        int v0_1 = ((android.support.v7.widget.m) p8.getLayoutParams());
        int v6 = android.view.View$MeasureSpec.makeMeasureSpec((android.view.View$MeasureSpec.getSize(p11) - p12), android.view.View$MeasureSpec.getMode(p11));
        if (!(p8 instanceof android.support.v7.internal.view.menu.ActionMenuItemView)) {
            v1_3 = 0;
        } else {
            v1_3 = ((android.support.v7.internal.view.menu.ActionMenuItemView) p8);
        }
        if ((!v1_3) || (!v1_3.c())) {
            int v5_1 = 0;
        } else {
            v5_1 = 1;
        }
        if ((p10 <= 0) || ((v5_1 != 0) && (p10 < 2))) {
            boolean v1_6 = 0;
        } else {
            p8.measure(android.view.View$MeasureSpec.makeMeasureSpec((p9 * p10), -2147483648), v6);
            int v7_1 = p8.getMeasuredWidth();
            v1_6 = (v7_1 / p9);
            if ((v7_1 % p9) != 0) {
                v1_6++;
            }
            if ((v5_1 != 0) && (v1_6 < 2)) {
                v1_6 = 2;
            }
        }
        if ((!v0_1.a) && (v5_1 != 0)) {
            v2_0 = 1;
        }
        v0_1.d = v2_0;
        v0_1.b = v1_6;
        p8.measure(android.view.View$MeasureSpec.makeMeasureSpec((v1_6 * p9), 1073741824), v6);
        return v1_6;
    }

    public static android.support.v7.widget.m a()
    {
        android.support.v7.widget.m v0 = android.support.v7.widget.ActionMenuView.e();
        v0.a = 1;
        return v0;
    }

    protected static android.support.v7.widget.m a(android.view.ViewGroup$LayoutParams p2)
    {
        android.support.v7.widget.m v0_0;
        if (p2 == null) {
            v0_0 = android.support.v7.widget.ActionMenuView.e();
        } else {
            if (!(p2 instanceof android.support.v7.widget.m)) {
                v0_0 = new android.support.v7.widget.m(p2);
            } else {
                v0_0 = new android.support.v7.widget.m(((android.support.v7.widget.m) p2));
            }
            if (v0_0.h <= 0) {
                v0_0.h = 16;
            }
        }
        return v0_0;
    }

    static synthetic android.support.v7.widget.o a(android.support.v7.widget.ActionMenuView p1)
    {
        return p1.u;
    }

    private void a(int p35, int p36)
    {
        int v23 = android.view.View$MeasureSpec.getMode(p36);
        int v6_0 = android.view.View$MeasureSpec.getSize(p35);
        int v17 = android.view.View$MeasureSpec.getSize(p36);
        int v7_1 = (this.getPaddingLeft() + this.getPaddingRight());
        int v19_0 = (this.getPaddingTop() + this.getPaddingBottom());
        int v24 = android.support.v7.widget.ActionMenuView.getChildMeasureSpec(p36, v19_0, -2);
        int v25 = (v6_0 - v7_1);
        int v9_1 = (v25 / this.s);
        if (v9_1 != 0) {
            int v26 = (this.s + ((v25 % this.s) / v9_1));
            int v16 = 0;
            int v15_0 = 0;
            int v10_0 = 0;
            int v7_3 = 0;
            long v11_0 = 0;
            int v12_0 = 0;
            int v27 = this.getChildCount();
            long v18_0 = 0;
            while (v18_0 < v27) {
                int v12_3;
                int v9_11;
                int v6_65;
                int v8_22;
                int v13;
                int v8_21 = this.getChildAt(v18_0);
                if (v8_21.getVisibility() == 8) {
                    v8_22 = v7_3;
                    v6_65 = v12_0;
                    v12_3 = v16;
                    v13 = v9_1;
                    v9_11 = v15_0;
                } else {
                    int v20_6 = (v8_21 instanceof android.support.v7.internal.view.menu.ActionMenuItemView);
                    long v14_5 = (v7_3 + 1);
                    if (v20_6 != 0) {
                        v8_21.setPadding(this.t, 0, this.t, 0);
                    }
                    int v7_24;
                    int v6_68 = ((android.support.v7.widget.m) v8_21.getLayoutParams());
                    v6_68.f = 0;
                    v6_68.c = 0;
                    v6_68.b = 0;
                    v6_68.d = 0;
                    v6_68.leftMargin = 0;
                    v6_68.rightMargin = 0;
                    if ((v20_6 == 0) || (!((android.support.v7.internal.view.menu.ActionMenuItemView) v8_21).c())) {
                        v7_24 = 0;
                    } else {
                        v7_24 = 1;
                    }
                    int v7_26;
                    v6_68.e = v7_24;
                    if (!v6_68.a) {
                        v7_26 = v9_1;
                    } else {
                        v7_26 = 1;
                    }
                    int v7_28;
                    int v20_7 = android.support.v7.widget.ActionMenuView.a(v8_21, v26, v7_26, v24, v19_0);
                    int v15_1 = Math.max(v15_0, v20_7);
                    if (!v6_68.d) {
                        v7_28 = v10_0;
                    } else {
                        v7_28 = (v10_0 + 1);
                    }
                    int v6_70;
                    if (!v6_68.a) {
                        v6_70 = v11_0;
                    } else {
                        v6_70 = 1;
                    }
                    long v11_1 = (v9_1 - v20_7);
                    int v10_11 = Math.max(v16, v8_21.getMeasuredHeight());
                    if (v20_7 != 1) {
                        v8_22 = v14_5;
                        v9_11 = v15_1;
                        v12_3 = v10_11;
                        v13 = v11_1;
                        v11_0 = v6_70;
                        v10_0 = v7_28;
                        v6_65 = v12_0;
                    } else {
                        v12_3 = v10_11;
                        v13 = v11_1;
                        v10_0 = v7_28;
                        v11_0 = v6_70;
                        v6_65 = (((long) (1 << v18_0)) | v12_0);
                        v9_11 = v15_1;
                        v8_22 = v14_5;
                    }
                }
                v18_0++;
                v15_0 = v9_11;
                v16 = v12_3;
                v9_1 = v13;
                v12_0 = v6_65;
                v7_3 = v8_22;
            }
            if ((v11_0 == 0) || (v7_3 != 2)) {
                int v8_3 = 0;
            } else {
                v8_3 = 1;
            }
            long v18_1 = 0;
            int v20_0 = v12_0;
            int v19_1 = v9_1;
            while ((v10_0 > 0) && (v19_1 > 0)) {
                long v14_0 = 2147483647;
                int v12_1 = 0;
                int v9_2 = 0;
                int v22_0 = 0;
                while (v22_0 < v27) {
                    int v6_59;
                    int v9_10;
                    int v6_57 = ((android.support.v7.widget.m) this.getChildAt(v22_0).getLayoutParams());
                    if (!v6_57.d) {
                        v6_59 = v9_2;
                        v9_10 = v14_0;
                    } else {
                        if (v6_57.b >= v14_0) {
                            if (v6_57.b != v14_0) {
                            } else {
                                v12_1 |= ((long) (1 << v22_0));
                                v6_59 = (v9_2 + 1);
                                v9_10 = v14_0;
                            }
                        } else {
                            v9_10 = v6_57.b;
                            v12_1 = ((long) (1 << v22_0));
                            v6_59 = 1;
                        }
                    }
                    v22_0++;
                    v14_0 = v9_10;
                    v9_2 = v6_59;
                }
                v20_0 |= v12_1;
                if (v9_2 > v19_1) {
                    break;
                }
                int v22_1 = (v14_0 + 1);
                long v14_2 = 0;
                int v9_8 = v19_1;
                long v18_2 = v20_0;
                while (v14_2 < v27) {
                    int v6_51;
                    int v20_1 = this.getChildAt(v14_2);
                    int v6_50 = ((android.support.v7.widget.m) v20_1.getLayoutParams());
                    if ((((long) (1 << v14_2)) & v12_1) != 0) {
                        if ((v8_3 != 0) && ((v6_50.e) && (v9_8 == 1))) {
                            v20_1.setPadding((this.t + v26), 0, this.t, 0);
                        }
                        v6_50.b = (v6_50.b + 1);
                        v6_50.f = 1;
                        v6_51 = (v9_8 - 1);
                    } else {
                        if (v6_50.b != v22_1) {
                            v6_51 = v9_8;
                        } else {
                            v18_2 |= ((long) (1 << v14_2));
                            v6_51 = v9_8;
                        }
                    }
                    v14_2++;
                    v9_8 = v6_51;
                }
                v20_0 = v18_2;
                v18_1 = 1;
                v19_1 = v9_8;
            }
            int v6_11;
            int v12_2 = v20_0;
            if ((v11_0 != 0) || (v7_3 != 1)) {
                v6_11 = 0;
            } else {
                v6_11 = 1;
            }
            if ((v19_1 <= 0) || ((v12_2 == 0) || ((v19_1 >= (v7_3 - 1)) && ((v6_11 == 0) && (v15_0 <= 1))))) {
                int v8_6 = v18_1;
            } else {
                int v6_27;
                int v7_7 = ((float) Long.bitCount(v12_2));
                if (v6_11 != 0) {
                    v6_27 = v7_7;
                } else {
                    if (((1 & v12_2) != 0) && (!((android.support.v7.widget.m) this.getChildAt(0).getLayoutParams()).e)) {
                        v7_7 -= 1056964608;
                    }
                    if (((((long) (1 << (v27 - 1))) & v12_2) == 0) || (((android.support.v7.widget.m) this.getChildAt((v27 - 1)).getLayoutParams()).e)) {
                    } else {
                        v6_27 = (v7_7 - 1056964608);
                    }
                }
                int v7_10;
                if (v6_27 <= 0) {
                    v7_10 = 0;
                } else {
                    v7_10 = ((int) (((float) (v19_1 * v26)) / v6_27));
                }
                int v9_3 = 0;
                v8_6 = v18_1;
                while (v9_3 < v27) {
                    int v6_46;
                    if ((((long) (1 << v9_3)) & v12_2) == 0) {
                        v6_46 = v8_6;
                    } else {
                        int v10_5 = this.getChildAt(v9_3);
                        int v6_45 = ((android.support.v7.widget.m) v10_5.getLayoutParams());
                        if (!(v10_5 instanceof android.support.v7.internal.view.menu.ActionMenuItemView)) {
                            if (!v6_45.a) {
                                if (v9_3 != 0) {
                                    v6_45.leftMargin = (v7_10 / 2);
                                }
                                if (v9_3 == (v27 - 1)) {
                                } else {
                                    v6_45.rightMargin = (v7_10 / 2);
                                }
                            } else {
                                v6_45.c = v7_10;
                                v6_45.f = 1;
                                v6_45.rightMargin = ((- v7_10) / 2);
                                v6_46 = 1;
                            }
                        } else {
                            v6_45.c = v7_10;
                            v6_45.f = 1;
                            if ((v9_3 == 0) && (!v6_45.e)) {
                                v6_45.leftMargin = ((- v7_10) / 2);
                            }
                            v6_46 = 1;
                        }
                    }
                    v9_3++;
                    v8_6 = v6_46;
                }
            }
            if (v8_6 != 0) {
                int v7_13 = 0;
                while (v7_13 < v27) {
                    int v8_12 = this.getChildAt(v7_13);
                    int v6_36 = ((android.support.v7.widget.m) v8_12.getLayoutParams());
                    if (v6_36.f) {
                        v8_12.measure(android.view.View$MeasureSpec.makeMeasureSpec((v6_36.c + (v6_36.b * v26)), 1073741824), v24);
                    }
                    v7_13++;
                }
            }
            if (v23 == 1073741824) {
                v16 = v17;
            }
            this.setMeasuredDimension(v25, v16);
        } else {
            this.setMeasuredDimension(v25, 0);
        }
        return;
    }

    private void a(android.support.v7.internal.view.menu.y p1, android.support.v7.internal.view.menu.j p2)
    {
        this.f = p1;
        this.g = p2;
        return;
    }

    private boolean a(int p5)
    {
        int v0_5;
        int v2 = 0;
        if (p5 != 0) {
            int v0_1 = this.getChildAt((p5 - 1));
            android.view.View v1 = this.getChildAt(p5);
            if ((p5 < this.getChildCount()) && ((v0_1 instanceof android.support.v7.widget.k))) {
                v2 = (((android.support.v7.widget.k) v0_1).e() | 0);
            }
            if ((p5 <= 0) || (!(v1 instanceof android.support.v7.widget.k))) {
                v0_5 = v2;
            } else {
                v0_5 = (((android.support.v7.widget.k) v1).d() | v2);
            }
        } else {
            v0_5 = 0;
        }
        return v0_5;
    }

    static synthetic android.support.v7.internal.view.menu.j b(android.support.v7.widget.ActionMenuView p1)
    {
        return p1.g;
    }

    private android.support.v7.widget.m b(android.util.AttributeSet p3)
    {
        return new android.support.v7.widget.m(this.getContext(), p3);
    }

    private boolean d()
    {
        return this.d;
    }

    private static android.support.v7.widget.m e()
    {
        android.support.v7.widget.m v0_1 = new android.support.v7.widget.m();
        v0_1.h = 16;
        return v0_1;
    }

    private android.support.v7.internal.view.menu.i f()
    {
        return this.c;
    }

    private boolean g()
    {
        if ((this.e == null) || (!this.e.e())) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    private boolean h()
    {
        if ((this.e == null) || (!this.e.f())) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    private boolean i()
    {
        if ((this.e == null) || (!this.e.i())) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    private boolean j()
    {
        if ((this.e == null) || (!this.e.j())) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final synthetic android.support.v7.widget.al a(android.util.AttributeSet p2)
    {
        return this.b(p2);
    }

    public final void a(android.support.v7.internal.view.menu.i p1)
    {
        this.c = p1;
        return;
    }

    public final boolean a(android.support.v7.internal.view.menu.m p4)
    {
        return this.c.a(p4, 0, 0);
    }

    protected final synthetic android.support.v7.widget.al b(android.view.ViewGroup$LayoutParams p2)
    {
        return android.support.v7.widget.ActionMenuView.a(p2);
    }

    public final void b()
    {
        if (this.e != null) {
            this.e.g();
        }
        return;
    }

    protected final synthetic android.support.v7.widget.al c()
    {
        return android.support.v7.widget.ActionMenuView.e();
    }

    protected boolean checkLayoutParams(android.view.ViewGroup$LayoutParams p2)
    {
        if ((p2 == null) || (!(p2 instanceof android.support.v7.widget.m))) {
            int v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public boolean dispatchPopulateAccessibilityEvent(android.view.accessibility.AccessibilityEvent p2)
    {
        return 0;
    }

    protected synthetic android.view.ViewGroup$LayoutParams generateDefaultLayoutParams()
    {
        return android.support.v7.widget.ActionMenuView.e();
    }

    public synthetic android.view.ViewGroup$LayoutParams generateLayoutParams(android.util.AttributeSet p2)
    {
        return this.b(p2);
    }

    protected synthetic android.view.ViewGroup$LayoutParams generateLayoutParams(android.view.ViewGroup$LayoutParams p2)
    {
        return android.support.v7.widget.ActionMenuView.a(p2);
    }

    public android.view.Menu getMenu()
    {
        if (this.c == null) {
            android.support.v7.widget.ActionMenuPresenter v0_5;
            android.support.v7.widget.ActionMenuPresenter v0_1 = this.getContext();
            this.c = new android.support.v7.internal.view.menu.i(v0_1);
            this.c.a(new android.support.v7.widget.n(this, 0));
            this.e = new android.support.v7.widget.ActionMenuPresenter(v0_1);
            this.e.d();
            android.support.v7.widget.ActionMenuPresenter v1_5 = this.e;
            if (this.f == null) {
                v0_5 = new android.support.v7.widget.l(this, 0);
            } else {
                v0_5 = this.f;
            }
            v1_5.f = v0_5;
            this.c.a(this.e, this.o);
            this.e.a(this);
        }
        return this.c;
    }

    public android.graphics.drawable.Drawable getOverflowIcon()
    {
        int v0_1;
        this.getMenu();
        int v0_0 = this.e;
        if (v0_0.i == null) {
            if (!v0_0.k) {
                v0_1 = 0;
            } else {
                v0_1 = v0_0.j;
            }
        } else {
            v0_1 = v0_0.i.getDrawable();
        }
        return v0_1;
    }

    public int getPopupTheme()
    {
        return this.p;
    }

    public int getWindowAnimations()
    {
        return 0;
    }

    public void onConfigurationChanged(android.content.res.Configuration p3)
    {
        if (android.os.Build$VERSION.SDK_INT >= 8) {
            super.onConfigurationChanged(p3);
        }
        if (this.e != null) {
            this.e.a(0);
            if (this.e.i()) {
                this.e.f();
                this.e.e();
            }
        }
        return;
    }

    public void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        this.b();
        return;
    }

    protected void onLayout(boolean p14, int p15, int p16, int p17, int p18)
    {
        if (this.q) {
            int v6 = this.getChildCount();
            int v7 = ((p18 - p16) / 2);
            int v8_0 = this.getDividerWidth();
            android.view.View v4_0 = 0;
            int v3_0 = (((p17 - p15) - this.getPaddingRight()) - this.getPaddingLeft());
            int v2_0 = 0;
            int v9_0 = android.support.v7.internal.widget.bd.a(this);
            int v5_0 = 0;
            while (v5_0 < v6) {
                int v0_27;
                int v1_13;
                int v10_2 = this.getChildAt(v5_0);
                if (v10_2.getVisibility() == 8) {
                    v0_27 = v3_0;
                    v1_13 = v4_0;
                } else {
                    int v0_29 = ((android.support.v7.widget.m) v10_2.getLayoutParams());
                    if (!v0_29.a) {
                        v0_27 = (v3_0 - (v0_29.rightMargin + (v10_2.getMeasuredWidth() + v0_29.leftMargin)));
                        this.a(v5_0);
                        v1_13 = (v4_0 + 1);
                    } else {
                        int v1_17 = v10_2.getMeasuredWidth();
                        if (this.a(v5_0)) {
                            v1_17 += v8_0;
                        }
                        int v0_33;
                        int v2_8;
                        int v11_0 = v10_2.getMeasuredHeight();
                        if (v9_0 == 0) {
                            v2_8 = ((this.getWidth() - this.getPaddingRight()) - v0_29.rightMargin);
                            v0_33 = (v2_8 - v1_17);
                        } else {
                            v0_33 = (v0_29.leftMargin + this.getPaddingLeft());
                            v2_8 = (v0_33 + v1_17);
                        }
                        int v12_2 = (v7 - (v11_0 / 2));
                        v10_2.layout(v0_33, v12_2, v2_8, (v11_0 + v12_2));
                        v0_27 = (v3_0 - v1_17);
                        v2_0 = 1;
                        v1_13 = v4_0;
                    }
                }
                v5_0++;
                v4_0 = v1_13;
                v3_0 = v0_27;
            }
            if ((v6 != 1) || (v2_0 != 0)) {
                int v0_6;
                if (v2_0 == 0) {
                    v0_6 = 1;
                } else {
                    v0_6 = 0;
                }
                int v0_8;
                int v0_7 = (v4_0 - v0_6);
                if (v0_7 <= 0) {
                    v0_8 = 0;
                } else {
                    v0_8 = (v3_0 / v0_7);
                }
                int v3_1 = Math.max(0, v0_8);
                if (v9_0 == 0) {
                    int v1_3 = this.getPaddingLeft();
                    int v2_1 = 0;
                    while (v2_1 < v6) {
                        int v0_12;
                        android.view.View v4_1 = this.getChildAt(v2_1);
                        int v0_11 = ((android.support.v7.widget.m) v4_1.getLayoutParams());
                        if ((v4_1.getVisibility() == 8) || (v0_11.a)) {
                            v0_12 = v1_3;
                        } else {
                            int v1_4 = (v1_3 + v0_11.leftMargin);
                            int v5_4 = v4_1.getMeasuredWidth();
                            int v8_2 = v4_1.getMeasuredHeight();
                            int v9_2 = (v7 - (v8_2 / 2));
                            v4_1.layout(v1_4, v9_2, (v1_4 + v5_4), (v8_2 + v9_2));
                            v0_12 = (((v0_11.rightMargin + v5_4) + v3_1) + v1_4);
                        }
                        v2_1++;
                        v1_3 = v0_12;
                    }
                } else {
                    int v1_7 = (this.getWidth() - this.getPaddingRight());
                    int v2_2 = 0;
                    while (v2_2 < v6) {
                        int v0_20;
                        android.view.View v4_2 = this.getChildAt(v2_2);
                        int v0_19 = ((android.support.v7.widget.m) v4_2.getLayoutParams());
                        if ((v4_2.getVisibility() == 8) || (v0_19.a)) {
                            v0_20 = v1_7;
                        } else {
                            int v1_8 = (v1_7 - v0_19.rightMargin);
                            int v5_8 = v4_2.getMeasuredWidth();
                            int v8_5 = v4_2.getMeasuredHeight();
                            int v9_4 = (v7 - (v8_5 / 2));
                            v4_2.layout((v1_8 - v5_8), v9_4, v1_8, (v8_5 + v9_4));
                            v0_20 = (v1_8 - ((v0_19.leftMargin + v5_8) + v3_1));
                        }
                        v2_2++;
                        v1_7 = v0_20;
                    }
                }
            } else {
                int v0_25 = this.getChildAt(0);
                int v1_10 = v0_25.getMeasuredWidth();
                int v2_3 = v0_25.getMeasuredHeight();
                int v3_4 = (((p17 - p15) / 2) - (v1_10 / 2));
                android.view.View v4_5 = (v7 - (v2_3 / 2));
                v0_25.layout(v3_4, v4_5, (v1_10 + v3_4), (v2_3 + v4_5));
            }
        } else {
            super.onLayout(p14, p15, p16, p17, p18);
        }
        return;
    }

    protected void onMeasure(int p35, int p36)
    {
        int v6_1;
        int v7_0 = this.q;
        if (android.view.View$MeasureSpec.getMode(p35) != 1073741824) {
            v6_1 = 0;
        } else {
            v6_1 = 1;
        }
        this.q = v6_1;
        if (v7_0 != this.q) {
            this.r = 0;
        }
        int v6_4 = android.view.View$MeasureSpec.getSize(p35);
        if ((this.q) && ((this.c != null) && (v6_4 != this.r))) {
            this.r = v6_4;
            this.c.c(1);
        }
        int v8_1 = this.getChildCount();
        if ((!this.q) || (v8_1 <= 0)) {
            int v7_5 = 0;
            while (v7_5 < v8_1) {
                int v6_10 = ((android.support.v7.widget.m) this.getChildAt(v7_5).getLayoutParams());
                v6_10.rightMargin = 0;
                v6_10.leftMargin = 0;
                v7_5++;
            }
            super.onMeasure(p35, p36);
        } else {
            int v23 = android.view.View$MeasureSpec.getMode(p36);
            int v6_12 = android.view.View$MeasureSpec.getSize(p35);
            int v17 = android.view.View$MeasureSpec.getSize(p36);
            int v7_7 = (this.getPaddingLeft() + this.getPaddingRight());
            int v19_0 = (this.getPaddingTop() + this.getPaddingBottom());
            int v24 = android.support.v7.widget.ActionMenuView.getChildMeasureSpec(p36, v19_0, -2);
            int v25 = (v6_12 - v7_7);
            int v9_2 = (v25 / this.s);
            if (v9_2 != 0) {
                int v26 = (this.s + ((v25 % this.s) / v9_2));
                int v16 = 0;
                int v15_0 = 0;
                int v10_0 = 0;
                int v7_9 = 0;
                long v11_0 = 0;
                int v12_0 = 0;
                int v27 = this.getChildCount();
                long v18_0 = 0;
                while (v18_0 < v27) {
                    int v12_3;
                    int v13;
                    int v6_77;
                    int v8_24;
                    int v9_12;
                    int v8_23 = this.getChildAt(v18_0);
                    if (v8_23.getVisibility() == 8) {
                        v8_24 = v7_9;
                        v6_77 = v12_0;
                        v12_3 = v16;
                        v13 = v9_2;
                        v9_12 = v15_0;
                    } else {
                        int v20_6 = (v8_23 instanceof android.support.v7.internal.view.menu.ActionMenuItemView);
                        long v14_5 = (v7_9 + 1);
                        if (v20_6 != 0) {
                            v8_23.setPadding(this.t, 0, this.t, 0);
                        }
                        int v7_30;
                        int v6_80 = ((android.support.v7.widget.m) v8_23.getLayoutParams());
                        v6_80.f = 0;
                        v6_80.c = 0;
                        v6_80.b = 0;
                        v6_80.d = 0;
                        v6_80.leftMargin = 0;
                        v6_80.rightMargin = 0;
                        if ((v20_6 == 0) || (!((android.support.v7.internal.view.menu.ActionMenuItemView) v8_23).c())) {
                            v7_30 = 0;
                        } else {
                            v7_30 = 1;
                        }
                        int v7_32;
                        v6_80.e = v7_30;
                        if (!v6_80.a) {
                            v7_32 = v9_2;
                        } else {
                            v7_32 = 1;
                        }
                        int v7_34;
                        int v20_7 = android.support.v7.widget.ActionMenuView.a(v8_23, v26, v7_32, v24, v19_0);
                        int v15_1 = Math.max(v15_0, v20_7);
                        if (!v6_80.d) {
                            v7_34 = v10_0;
                        } else {
                            v7_34 = (v10_0 + 1);
                        }
                        int v6_82;
                        if (!v6_80.a) {
                            v6_82 = v11_0;
                        } else {
                            v6_82 = 1;
                        }
                        long v11_1 = (v9_2 - v20_7);
                        int v10_11 = Math.max(v16, v8_23.getMeasuredHeight());
                        if (v20_7 != 1) {
                            v8_24 = v14_5;
                            v9_12 = v15_1;
                            v12_3 = v10_11;
                            v13 = v11_1;
                            v11_0 = v6_82;
                            v10_0 = v7_34;
                            v6_77 = v12_0;
                        } else {
                            v12_3 = v10_11;
                            v13 = v11_1;
                            v10_0 = v7_34;
                            v11_0 = v6_82;
                            v6_77 = (((long) (1 << v18_0)) | v12_0);
                            v9_12 = v15_1;
                            v8_24 = v14_5;
                        }
                    }
                    v18_0++;
                    v15_0 = v9_12;
                    v16 = v12_3;
                    v9_2 = v13;
                    v12_0 = v6_77;
                    v7_9 = v8_24;
                }
                if ((v11_0 == 0) || (v7_9 != 2)) {
                    int v8_5 = 0;
                } else {
                    v8_5 = 1;
                }
                long v18_1 = 0;
                int v20_0 = v12_0;
                int v19_1 = v9_2;
                while ((v10_0 > 0) && (v19_1 > 0)) {
                    long v14_0 = 2147483647;
                    int v12_1 = 0;
                    int v9_3 = 0;
                    int v22_0 = 0;
                    while (v22_0 < v27) {
                        int v9_11;
                        int v6_71;
                        int v6_69 = ((android.support.v7.widget.m) this.getChildAt(v22_0).getLayoutParams());
                        if (!v6_69.d) {
                            v6_71 = v9_3;
                            v9_11 = v14_0;
                        } else {
                            if (v6_69.b >= v14_0) {
                                if (v6_69.b != v14_0) {
                                } else {
                                    v12_1 |= ((long) (1 << v22_0));
                                    v6_71 = (v9_3 + 1);
                                    v9_11 = v14_0;
                                }
                            } else {
                                v9_11 = v6_69.b;
                                v12_1 = ((long) (1 << v22_0));
                                v6_71 = 1;
                            }
                        }
                        v22_0++;
                        v14_0 = v9_11;
                        v9_3 = v6_71;
                    }
                    v20_0 |= v12_1;
                    if (v9_3 > v19_1) {
                        break;
                    }
                    int v22_1 = (v14_0 + 1);
                    long v14_2 = 0;
                    int v9_9 = v19_1;
                    long v18_2 = v20_0;
                    while (v14_2 < v27) {
                        int v6_63;
                        int v20_1 = this.getChildAt(v14_2);
                        int v6_62 = ((android.support.v7.widget.m) v20_1.getLayoutParams());
                        if ((((long) (1 << v14_2)) & v12_1) != 0) {
                            if ((v8_5 != 0) && ((v6_62.e) && (v9_9 == 1))) {
                                v20_1.setPadding((this.t + v26), 0, this.t, 0);
                            }
                            v6_62.b = (v6_62.b + 1);
                            v6_62.f = 1;
                            v6_63 = (v9_9 - 1);
                        } else {
                            if (v6_62.b != v22_1) {
                                v6_63 = v9_9;
                            } else {
                                v18_2 |= ((long) (1 << v14_2));
                                v6_63 = v9_9;
                            }
                        }
                        v14_2++;
                        v9_9 = v6_63;
                    }
                    v20_0 = v18_2;
                    v18_1 = 1;
                    v19_1 = v9_9;
                }
                int v6_23;
                int v12_2 = v20_0;
                if ((v11_0 != 0) || (v7_9 != 1)) {
                    v6_23 = 0;
                } else {
                    v6_23 = 1;
                }
                if ((v19_1 <= 0) || ((v12_2 == 0) || ((v19_1 >= (v7_9 - 1)) && ((v6_23 == 0) && (v15_0 <= 1))))) {
                    int v8_8 = v18_1;
                } else {
                    int v6_39;
                    int v7_13 = ((float) Long.bitCount(v12_2));
                    if (v6_23 != 0) {
                        v6_39 = v7_13;
                    } else {
                        if (((1 & v12_2) != 0) && (!((android.support.v7.widget.m) this.getChildAt(0).getLayoutParams()).e)) {
                            v7_13 -= 1056964608;
                        }
                        if (((((long) (1 << (v27 - 1))) & v12_2) == 0) || (((android.support.v7.widget.m) this.getChildAt((v27 - 1)).getLayoutParams()).e)) {
                        } else {
                            v6_39 = (v7_13 - 1056964608);
                        }
                    }
                    int v7_16;
                    if (v6_39 <= 0) {
                        v7_16 = 0;
                    } else {
                        v7_16 = ((int) (((float) (v19_1 * v26)) / v6_39));
                    }
                    int v9_4 = 0;
                    v8_8 = v18_1;
                    while (v9_4 < v27) {
                        int v6_58;
                        if ((((long) (1 << v9_4)) & v12_2) == 0) {
                            v6_58 = v8_8;
                        } else {
                            int v10_5 = this.getChildAt(v9_4);
                            int v6_57 = ((android.support.v7.widget.m) v10_5.getLayoutParams());
                            if (!(v10_5 instanceof android.support.v7.internal.view.menu.ActionMenuItemView)) {
                                if (!v6_57.a) {
                                    if (v9_4 != 0) {
                                        v6_57.leftMargin = (v7_16 / 2);
                                    }
                                    if (v9_4 == (v27 - 1)) {
                                    } else {
                                        v6_57.rightMargin = (v7_16 / 2);
                                    }
                                } else {
                                    v6_57.c = v7_16;
                                    v6_57.f = 1;
                                    v6_57.rightMargin = ((- v7_16) / 2);
                                    v6_58 = 1;
                                }
                            } else {
                                v6_57.c = v7_16;
                                v6_57.f = 1;
                                if ((v9_4 == 0) && (!v6_57.e)) {
                                    v6_57.leftMargin = ((- v7_16) / 2);
                                }
                                v6_58 = 1;
                            }
                        }
                        v9_4++;
                        v8_8 = v6_58;
                    }
                }
                if (v8_8 != 0) {
                    int v7_19 = 0;
                    while (v7_19 < v27) {
                        int v8_14 = this.getChildAt(v7_19);
                        int v6_48 = ((android.support.v7.widget.m) v8_14.getLayoutParams());
                        if (v6_48.f) {
                            v8_14.measure(android.view.View$MeasureSpec.makeMeasureSpec((v6_48.c + (v6_48.b * v26)), 1073741824), v24);
                        }
                        v7_19++;
                    }
                }
                if (v23 == 1073741824) {
                    v16 = v17;
                }
                this.setMeasuredDimension(v25, v16);
            } else {
                this.setMeasuredDimension(v25, 0);
            }
        }
        return;
    }

    public void setExpandedActionViewsExclusive(boolean p2)
    {
        this.e.o = p2;
        return;
    }

    public void setOnMenuItemClickListener(android.support.v7.widget.o p1)
    {
        this.u = p1;
        return;
    }

    public void setOverflowIcon(android.graphics.drawable.Drawable p3)
    {
        this.getMenu();
        android.support.v7.widget.e v0_0 = this.e;
        if (v0_0.i == null) {
            v0_0.k = 1;
            v0_0.j = p3;
        } else {
            v0_0.i.setImageDrawable(p3);
        }
        return;
    }

    public void setOverflowReserved(boolean p1)
    {
        this.d = p1;
        return;
    }

    public void setPopupTheme(int p3)
    {
        if (this.p != p3) {
            this.p = p3;
            if (p3 != 0) {
                this.o = new android.view.ContextThemeWrapper(this.getContext(), p3);
            } else {
                this.o = this.getContext();
            }
        }
        return;
    }

    public void setPresenter(android.support.v7.widget.ActionMenuPresenter p2)
    {
        this.e = p2;
        this.e.a(this);
        return;
    }
}
