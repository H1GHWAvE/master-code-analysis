package android.support.v7.widget;
public class Toolbar$SavedState extends android.view.View$BaseSavedState {
    public static final android.os.Parcelable$Creator CREATOR;
    int a;
    boolean b;

    static Toolbar$SavedState()
    {
        android.support.v7.widget.Toolbar$SavedState.CREATOR = new android.support.v7.widget.cm();
        return;
    }

    public Toolbar$SavedState(android.os.Parcel p2)
    {
        int v0_2;
        this(p2);
        this.a = p2.readInt();
        if (p2.readInt() == 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        this.b = v0_2;
        return;
    }

    public Toolbar$SavedState(android.os.Parcelable p1)
    {
        this(p1);
        return;
    }

    public void writeToParcel(android.os.Parcel p2, int p3)
    {
        int v0_2;
        super.writeToParcel(p2, p3);
        p2.writeInt(this.a);
        if (!this.b) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        p2.writeInt(v0_2);
        return;
    }
}
