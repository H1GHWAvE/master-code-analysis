package android.support.v7.widget;
final class f extends android.support.v7.widget.as {
    final synthetic android.support.v7.widget.ActionMenuPresenter a;
    final synthetic android.support.v7.widget.e b;

    f(android.support.v7.widget.e p1, android.view.View p2, android.support.v7.widget.ActionMenuPresenter p3)
    {
        this.b = p1;
        this.a = p3;
        this(p2);
        return;
    }

    public final android.support.v7.widget.an a()
    {
        android.support.v7.widget.an v0_6;
        if (this.b.a.p != null) {
            v0_6 = this.b.a.p.c;
        } else {
            v0_6 = 0;
        }
        return v0_6;
    }

    public final boolean b()
    {
        this.b.a.e();
        return 1;
    }

    public final boolean c()
    {
        int v0_5;
        if (this.b.a.r == null) {
            this.b.a.f();
            v0_5 = 1;
        } else {
            v0_5 = 0;
        }
        return v0_5;
    }
}
