package android.support.v7.b.a;
public class c extends android.graphics.drawable.Drawable {
    public static final int a = 0;
    public static final int b = 1;
    public static final int c = 2;
    public static final int d = 3;
    private static final float g;
    public float e;
    private final android.graphics.Paint f;
    private float h;
    private float i;
    private float j;
    private float k;
    private boolean l;
    private final android.graphics.Path m;
    private final int n;
    private boolean o;
    private float p;
    private int q;

    static c()
    {
        android.support.v7.b.a.c.g = ((float) Math.toRadians(45.0));
        return;
    }

    public c(android.content.Context p10)
    {
        this.f = new android.graphics.Paint();
        this.m = new android.graphics.Path();
        this.o = 0;
        this.q = 2;
        this.f.setStyle(android.graphics.Paint$Style.STROKE);
        this.f.setStrokeJoin(android.graphics.Paint$Join.MITER);
        this.f.setStrokeCap(android.graphics.Paint$Cap.BUTT);
        this.f.setAntiAlias(1);
        android.content.res.TypedArray v0_10 = p10.getTheme().obtainStyledAttributes(0, android.support.v7.a.n.DrawerArrowToggle, android.support.v7.a.d.drawerArrowStyle, android.support.v7.a.m.Base_Widget_AppCompat_DrawerArrowToggle);
        float v1_5 = v0_10.getColor(android.support.v7.a.n.DrawerArrowToggle_color, 0);
        if (v1_5 != this.f.getColor()) {
            this.f.setColor(v1_5);
            this.invalidateSelf();
        }
        float v1_7 = v0_10.getDimension(android.support.v7.a.n.DrawerArrowToggle_thickness, 0);
        if (this.f.getStrokeWidth() != v1_7) {
            this.f.setStrokeWidth(v1_7);
            this.p = ((float) (((double) (v1_7 / 1073741824)) * Math.cos(((double) android.support.v7.b.a.c.g))));
            this.invalidateSelf();
        }
        float v1_12 = v0_10.getBoolean(android.support.v7.a.n.DrawerArrowToggle_spinBars, 1);
        if (this.l != v1_12) {
            this.l = v1_12;
            this.invalidateSelf();
        }
        float v1_16 = ((float) Math.round(v0_10.getDimension(android.support.v7.a.n.DrawerArrowToggle_gapBetweenBars, 0)));
        if (v1_16 != this.k) {
            this.k = v1_16;
            this.invalidateSelf();
        }
        this.n = v0_10.getDimensionPixelSize(android.support.v7.a.n.DrawerArrowToggle_drawableSize, 0);
        this.i = ((float) Math.round(v0_10.getDimension(android.support.v7.a.n.DrawerArrowToggle_barLength, 0)));
        this.h = ((float) Math.round(v0_10.getDimension(android.support.v7.a.n.DrawerArrowToggle_arrowHeadLength, 0)));
        this.j = v0_10.getDimension(android.support.v7.a.n.DrawerArrowToggle_arrowShaftLength, 0);
        v0_10.recycle();
        return;
    }

    private float a()
    {
        return this.h;
    }

    private static float a(float p1, float p2, float p3)
    {
        return (((p2 - p1) * p3) + p1);
    }

    private void a(float p2)
    {
        if (this.h != p2) {
            this.h = p2;
            this.invalidateSelf();
        }
        return;
    }

    private void a(int p2)
    {
        if (p2 != this.f.getColor()) {
            this.f.setColor(p2);
            this.invalidateSelf();
        }
        return;
    }

    private float b()
    {
        return this.j;
    }

    private void b(float p2)
    {
        if (this.j != p2) {
            this.j = p2;
            this.invalidateSelf();
        }
        return;
    }

    private void b(int p2)
    {
        if (p2 != this.q) {
            this.q = p2;
            this.invalidateSelf();
        }
        return;
    }

    private void b(boolean p2)
    {
        if (this.l != p2) {
            this.l = p2;
            this.invalidateSelf();
        }
        return;
    }

    private float c()
    {
        return this.i;
    }

    private void c(float p2)
    {
        if (this.i != p2) {
            this.i = p2;
            this.invalidateSelf();
        }
        return;
    }

    private int d()
    {
        return this.f.getColor();
    }

    private void d(float p5)
    {
        if (this.f.getStrokeWidth() != p5) {
            this.f.setStrokeWidth(p5);
            this.p = ((float) (((double) (p5 / 1073741824)) * Math.cos(((double) android.support.v7.b.a.c.g))));
            this.invalidateSelf();
        }
        return;
    }

    private float e()
    {
        return this.f.getStrokeWidth();
    }

    private void e(float p2)
    {
        if (p2 != this.k) {
            this.k = p2;
            this.invalidateSelf();
        }
        return;
    }

    private float f()
    {
        return this.k;
    }

    private void f(float p2)
    {
        if (this.e != p2) {
            this.e = p2;
            this.invalidateSelf();
        }
        return;
    }

    private boolean g()
    {
        return this.l;
    }

    private int h()
    {
        return this.q;
    }

    private float i()
    {
        return this.e;
    }

    public final void a(boolean p2)
    {
        if (this.o != p2) {
            this.o = p2;
            this.invalidateSelf();
        }
        return;
    }

    public void draw(android.graphics.Canvas p13)
    {
        float v0_2;
        float v3_0 = this.getBounds();
        switch (this.q) {
            case 0:
                v0_2 = 0;
                break;
            case 1:
                v0_2 = 1;
                break;
            case 2:
            default:
                if (android.support.v4.e.a.a.e(this) != 1) {
                    v0_2 = 0;
                } else {
                    v0_2 = 1;
                }
                break;
            case 3:
                if (android.support.v4.e.a.a.e(this) != 0) {
                    v0_2 = 0;
                } else {
                    v0_2 = 1;
                }
                break;
        }
        boolean v2_12;
        boolean v2_2 = this.i;
        double v4_3 = (v2_2 + ((((float) Math.sqrt(((double) ((this.h * this.h) * 1073741824)))) - v2_2) * this.e));
        android.graphics.Paint v1_7 = this.i;
        float v5_1 = (v1_7 + ((this.j - v1_7) * this.e));
        double v6_1 = ((float) Math.round((0 + ((this.p - 0) * this.e))));
        float v7_2 = (0 + ((android.support.v7.b.a.c.g - 0) * this.e));
        if (v0_2 == 0) {
            v2_12 = -1020002304;
        } else {
            v2_12 = 0;
        }
        android.graphics.Paint v1_14;
        if (v0_2 == 0) {
            v1_14 = 0;
        } else {
            v1_14 = 1127481344;
        }
        android.graphics.Paint v1_17 = (((v1_14 - v2_12) * this.e) + v2_12);
        boolean v2_13 = ((float) Math.round((((double) v4_3) * Math.cos(((double) v7_2)))));
        double v4_4 = ((float) Math.round((((double) v4_3) * Math.sin(((double) v7_2)))));
        this.m.rewind();
        float v7_5 = (this.k + this.f.getStrokeWidth());
        float v7_6 = (v7_5 + (((- this.p) - v7_5) * this.e));
        double v8_15 = ((- v5_1) / 1073741824);
        this.m.moveTo((v8_15 + v6_1), 0);
        this.m.rLineTo((v5_1 - (v6_1 * 1073741824)), 0);
        this.m.moveTo(v8_15, v7_6);
        this.m.rLineTo(v2_13, v4_4);
        this.m.moveTo(v8_15, (- v7_6));
        this.m.rLineTo(v2_13, (- v4_4));
        this.m.close();
        p13.save();
        boolean v2_16 = this.f.getStrokeWidth();
        p13.translate(((float) v3_0.centerX()), ((float) (((double) ((float) ((((int) ((((float) v3_0.height()) - (1077936128 * v2_16)) - (this.k * 1073741824))) / 4) * 2))) + ((((double) v2_16) * 1.5) + ((double) this.k)))));
        if (!this.l) {
            if (v0_2 != 0) {
                p13.rotate(1127481344);
            }
        } else {
            float v0_6;
            if ((v0_2 ^ this.o) == 0) {
                v0_6 = 1;
            } else {
                v0_6 = -1;
            }
            p13.rotate((((float) v0_6) * v1_17));
        }
        p13.drawPath(this.m, this.f);
        p13.restore();
        return;
    }

    public int getIntrinsicHeight()
    {
        return this.n;
    }

    public int getIntrinsicWidth()
    {
        return this.n;
    }

    public int getOpacity()
    {
        return -3;
    }

    public void setAlpha(int p2)
    {
        if (p2 != this.f.getAlpha()) {
            this.f.setAlpha(p2);
            this.invalidateSelf();
        }
        return;
    }

    public void setColorFilter(android.graphics.ColorFilter p2)
    {
        this.f.setColorFilter(p2);
        this.invalidateSelf();
        return;
    }
}
