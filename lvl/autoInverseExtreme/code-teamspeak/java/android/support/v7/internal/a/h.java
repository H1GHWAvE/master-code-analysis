package android.support.v7.internal.a;
final class h implements android.support.v7.internal.view.menu.y {
    final synthetic android.support.v7.internal.a.e a;
    private boolean b;

    private h(android.support.v7.internal.a.e p1)
    {
        this.a = p1;
        return;
    }

    synthetic h(android.support.v7.internal.a.e p1, byte p2)
    {
        this(p1);
        return;
    }

    public final void a(android.support.v7.internal.view.menu.i p3, boolean p4)
    {
        if (!this.b) {
            this.b = 1;
            this.a.i.q();
            if (this.a.k != null) {
                this.a.k.onPanelClosed(108, p3);
            }
            this.b = 0;
        }
        return;
    }

    public final boolean a_(android.support.v7.internal.view.menu.i p3)
    {
        int v0_2;
        if (this.a.k == null) {
            v0_2 = 0;
        } else {
            this.a.k.onMenuOpened(108, p3);
            v0_2 = 1;
        }
        return v0_2;
    }
}
