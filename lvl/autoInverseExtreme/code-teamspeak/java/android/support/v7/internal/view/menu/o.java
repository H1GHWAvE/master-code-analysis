package android.support.v7.internal.view.menu;
public class o extends android.support.v7.internal.view.menu.e implements android.view.MenuItem {
    static final String e = "MenuItemWrapper";
    public reflect.Method f;

    o(android.content.Context p1, android.support.v4.g.a.b p2)
    {
        this(p1, p2);
        return;
    }

    private void b()
    {
        try {
            if (this.f != null) {
                reflect.Method v0_5 = this.f;
                Object v1_1 = this.d;
                Object[] v2_3 = new Object[1];
                v2_3[0] = Boolean.valueOf(1);
                v0_5.invoke(v1_1, v2_3);
            } else {
                reflect.Method v0_3 = ((android.support.v4.g.a.b) this.d).getClass();
                Object[] v2_1 = new Class[1];
                v2_1[0] = Boolean.TYPE;
                this.f = v0_3.getDeclaredMethod("setExclusiveCheckable", v2_1);
            }
        } catch (reflect.Method v0_6) {
            android.util.Log.w("MenuItemWrapper", "Error while calling setExclusiveCheckable", v0_6);
        }
        return;
    }

    android.support.v7.internal.view.menu.p a(android.view.ActionProvider p3)
    {
        return new android.support.v7.internal.view.menu.p(this, this.a, p3);
    }

    public boolean collapseActionView()
    {
        return ((android.support.v4.g.a.b) this.d).collapseActionView();
    }

    public boolean expandActionView()
    {
        return ((android.support.v4.g.a.b) this.d).expandActionView();
    }

    public android.view.ActionProvider getActionProvider()
    {
        int v0_3;
        int v0_2 = ((android.support.v4.g.a.b) this.d).a();
        if (!(v0_2 instanceof android.support.v7.internal.view.menu.p)) {
            v0_3 = 0;
        } else {
            v0_3 = ((android.support.v7.internal.view.menu.p) v0_2).c;
        }
        return v0_3;
    }

    public android.view.View getActionView()
    {
        android.view.View v0_2 = ((android.support.v4.g.a.b) this.d).getActionView();
        if ((v0_2 instanceof android.support.v7.internal.view.menu.q)) {
            v0_2 = ((android.view.View) ((android.support.v7.internal.view.menu.q) v0_2).a);
        }
        return v0_2;
    }

    public char getAlphabeticShortcut()
    {
        return ((android.support.v4.g.a.b) this.d).getAlphabeticShortcut();
    }

    public int getGroupId()
    {
        return ((android.support.v4.g.a.b) this.d).getGroupId();
    }

    public android.graphics.drawable.Drawable getIcon()
    {
        return ((android.support.v4.g.a.b) this.d).getIcon();
    }

    public android.content.Intent getIntent()
    {
        return ((android.support.v4.g.a.b) this.d).getIntent();
    }

    public int getItemId()
    {
        return ((android.support.v4.g.a.b) this.d).getItemId();
    }

    public android.view.ContextMenu$ContextMenuInfo getMenuInfo()
    {
        return ((android.support.v4.g.a.b) this.d).getMenuInfo();
    }

    public char getNumericShortcut()
    {
        return ((android.support.v4.g.a.b) this.d).getNumericShortcut();
    }

    public int getOrder()
    {
        return ((android.support.v4.g.a.b) this.d).getOrder();
    }

    public android.view.SubMenu getSubMenu()
    {
        return this.a(((android.support.v4.g.a.b) this.d).getSubMenu());
    }

    public CharSequence getTitle()
    {
        return ((android.support.v4.g.a.b) this.d).getTitle();
    }

    public CharSequence getTitleCondensed()
    {
        return ((android.support.v4.g.a.b) this.d).getTitleCondensed();
    }

    public boolean hasSubMenu()
    {
        return ((android.support.v4.g.a.b) this.d).hasSubMenu();
    }

    public boolean isActionViewExpanded()
    {
        return ((android.support.v4.g.a.b) this.d).isActionViewExpanded();
    }

    public boolean isCheckable()
    {
        return ((android.support.v4.g.a.b) this.d).isCheckable();
    }

    public boolean isChecked()
    {
        return ((android.support.v4.g.a.b) this.d).isChecked();
    }

    public boolean isEnabled()
    {
        return ((android.support.v4.g.a.b) this.d).isEnabled();
    }

    public boolean isVisible()
    {
        return ((android.support.v4.g.a.b) this.d).isVisible();
    }

    public android.view.MenuItem setActionProvider(android.view.ActionProvider p3)
    {
        int v1;
        android.support.v4.g.a.b v0_1 = ((android.support.v4.g.a.b) this.d);
        if (p3 == null) {
            v1 = 0;
        } else {
            v1 = this.a(p3);
        }
        v0_1.a(v1);
        return this;
    }

    public android.view.MenuItem setActionView(int p4)
    {
        ((android.support.v4.g.a.b) this.d).setActionView(p4);
        android.view.View v1 = ((android.support.v4.g.a.b) this.d).getActionView();
        if ((v1 instanceof android.view.CollapsibleActionView)) {
            ((android.support.v4.g.a.b) this.d).setActionView(new android.support.v7.internal.view.menu.q(v1));
        }
        return this;
    }

    public android.view.MenuItem setActionView(android.view.View p2)
    {
        if ((p2 instanceof android.view.CollapsibleActionView)) {
            p2 = new android.support.v7.internal.view.menu.q(p2);
        }
        ((android.support.v4.g.a.b) this.d).setActionView(p2);
        return this;
    }

    public android.view.MenuItem setAlphabeticShortcut(char p2)
    {
        ((android.support.v4.g.a.b) this.d).setAlphabeticShortcut(p2);
        return this;
    }

    public android.view.MenuItem setCheckable(boolean p2)
    {
        ((android.support.v4.g.a.b) this.d).setCheckable(p2);
        return this;
    }

    public android.view.MenuItem setChecked(boolean p2)
    {
        ((android.support.v4.g.a.b) this.d).setChecked(p2);
        return this;
    }

    public android.view.MenuItem setEnabled(boolean p2)
    {
        ((android.support.v4.g.a.b) this.d).setEnabled(p2);
        return this;
    }

    public android.view.MenuItem setIcon(int p2)
    {
        ((android.support.v4.g.a.b) this.d).setIcon(p2);
        return this;
    }

    public android.view.MenuItem setIcon(android.graphics.drawable.Drawable p2)
    {
        ((android.support.v4.g.a.b) this.d).setIcon(p2);
        return this;
    }

    public android.view.MenuItem setIntent(android.content.Intent p2)
    {
        ((android.support.v4.g.a.b) this.d).setIntent(p2);
        return this;
    }

    public android.view.MenuItem setNumericShortcut(char p2)
    {
        ((android.support.v4.g.a.b) this.d).setNumericShortcut(p2);
        return this;
    }

    public android.view.MenuItem setOnActionExpandListener(android.view.MenuItem$OnActionExpandListener p3)
    {
        int v1_0;
        android.support.v4.g.a.b v0_1 = ((android.support.v4.g.a.b) this.d);
        if (p3 == null) {
            v1_0 = 0;
        } else {
            v1_0 = new android.support.v7.internal.view.menu.r(this, p3);
        }
        v0_1.a(v1_0);
        return this;
    }

    public android.view.MenuItem setOnMenuItemClickListener(android.view.MenuItem$OnMenuItemClickListener p3)
    {
        int v1_0;
        android.support.v4.g.a.b v0_1 = ((android.support.v4.g.a.b) this.d);
        if (p3 == null) {
            v1_0 = 0;
        } else {
            v1_0 = new android.support.v7.internal.view.menu.s(this, p3);
        }
        v0_1.setOnMenuItemClickListener(v1_0);
        return this;
    }

    public android.view.MenuItem setShortcut(char p2, char p3)
    {
        ((android.support.v4.g.a.b) this.d).setShortcut(p2, p3);
        return this;
    }

    public void setShowAsAction(int p2)
    {
        ((android.support.v4.g.a.b) this.d).setShowAsAction(p2);
        return;
    }

    public android.view.MenuItem setShowAsActionFlags(int p2)
    {
        ((android.support.v4.g.a.b) this.d).setShowAsActionFlags(p2);
        return this;
    }

    public android.view.MenuItem setTitle(int p2)
    {
        ((android.support.v4.g.a.b) this.d).setTitle(p2);
        return this;
    }

    public android.view.MenuItem setTitle(CharSequence p2)
    {
        ((android.support.v4.g.a.b) this.d).setTitle(p2);
        return this;
    }

    public android.view.MenuItem setTitleCondensed(CharSequence p2)
    {
        ((android.support.v4.g.a.b) this.d).setTitleCondensed(p2);
        return this;
    }

    public android.view.MenuItem setVisible(boolean p2)
    {
        return ((android.support.v4.g.a.b) this.d).setVisible(p2);
    }
}
