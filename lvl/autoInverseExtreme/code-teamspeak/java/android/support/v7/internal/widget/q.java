package android.support.v7.internal.widget;
final class q implements android.support.v7.internal.widget.p {
    private static final float b = 1064514355;
    final synthetic android.support.v7.internal.widget.l a;
    private final java.util.Map c;

    private q(android.support.v7.internal.widget.l p2)
    {
        this.a = p2;
        this.c = new java.util.HashMap();
        return;
    }

    synthetic q(android.support.v7.internal.widget.l p1, byte p2)
    {
        this(p1);
        return;
    }

    public final void a(java.util.List p8, java.util.List p9)
    {
        java.util.Map v4 = this.c;
        v4.clear();
        int v2_0 = p8.size();
        int v1_0 = 0;
        while (v1_0 < v2_0) {
            int v0_10 = ((android.support.v7.internal.widget.o) p8.get(v1_0));
            v0_10.b = 0;
            v4.put(new android.content.ComponentName(v0_10.a.activityInfo.packageName, v0_10.a.activityInfo.name), v0_10);
            v1_0++;
        }
        int v2_1 = 1065353216;
        int v3_0 = (p9.size() - 1);
        while (v3_0 >= 0) {
            int v0_7;
            int v1_2 = ((android.support.v7.internal.widget.r) p9.get(v3_0));
            int v0_6 = ((android.support.v7.internal.widget.o) v4.get(v1_2.a));
            if (v0_6 == 0) {
                v0_7 = v2_1;
            } else {
                v0_6.b = ((v1_2.c * v2_1) + v0_6.b);
                v0_7 = (1064514355 * v2_1);
            }
            v3_0--;
            v2_1 = v0_7;
        }
        java.util.Collections.sort(p8);
        return;
    }
}
