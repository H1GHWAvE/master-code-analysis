package android.support.v7.internal.view.menu;
public abstract class d implements android.support.v7.internal.view.menu.x {
    public android.content.Context a;
    public android.content.Context b;
    public android.support.v7.internal.view.menu.i c;
    protected android.view.LayoutInflater d;
    protected android.view.LayoutInflater e;
    public android.support.v7.internal.view.menu.y f;
    public android.support.v7.internal.view.menu.z g;
    public int h;
    private int i;
    private int j;

    public d(android.content.Context p2, int p3, int p4)
    {
        this.a = p2;
        this.d = android.view.LayoutInflater.from(p2);
        this.i = p3;
        this.j = p4;
        return;
    }

    private void a(int p1)
    {
        this.h = p1;
        return;
    }

    private void a(android.view.View p2, int p3)
    {
        android.view.ViewGroup v0_1 = ((android.view.ViewGroup) p2.getParent());
        if (v0_1 != null) {
            v0_1.removeView(p2);
        }
        ((android.view.ViewGroup) this.g).addView(p2, p3);
        return;
    }

    private android.support.v7.internal.view.menu.aa b(android.view.ViewGroup p4)
    {
        return ((android.support.v7.internal.view.menu.aa) this.d.inflate(this.j, p4, 0));
    }

    private android.support.v7.internal.view.menu.y d()
    {
        return this.f;
    }

    public android.support.v7.internal.view.menu.z a(android.view.ViewGroup p4)
    {
        if (this.g == null) {
            this.g = ((android.support.v7.internal.view.menu.z) this.d.inflate(this.i, p4, 0));
            this.g.a(this.c);
            this.a(1);
        }
        return this.g;
    }

    public android.view.View a(android.support.v7.internal.view.menu.m p4, android.view.View p5, android.view.ViewGroup p6)
    {
        android.view.View v0_3;
        if (!(p5 instanceof android.support.v7.internal.view.menu.aa)) {
            v0_3 = ((android.support.v7.internal.view.menu.aa) this.d.inflate(this.j, p6, 0));
        } else {
            v0_3 = ((android.support.v7.internal.view.menu.aa) p5);
        }
        this.a(p4, v0_3);
        return ((android.view.View) v0_3);
    }

    public void a(android.content.Context p2, android.support.v7.internal.view.menu.i p3)
    {
        this.b = p2;
        this.e = android.view.LayoutInflater.from(this.b);
        this.c = p3;
        return;
    }

    public void a(android.support.v7.internal.view.menu.i p2, boolean p3)
    {
        if (this.f != null) {
            this.f.a(p2, p3);
        }
        return;
    }

    public abstract void a();

    public final void a(android.support.v7.internal.view.menu.y p1)
    {
        this.f = p1;
        return;
    }

    public void a(boolean p11)
    {
        android.view.ViewGroup v0_1 = ((android.view.ViewGroup) this.g);
        if (v0_1 != null) {
            int v4;
            if (this.c == null) {
                v4 = 0;
            } else {
                this.c.i();
                java.util.ArrayList v7 = this.c.h();
                int v8 = v7.size();
                int v6 = 0;
                v4 = 0;
                while (v6 < v8) {
                    android.view.ViewGroup v1_7;
                    android.view.ViewGroup v1_6 = ((android.support.v7.internal.view.menu.m) v7.get(v6));
                    if (!this.c(v1_6)) {
                        v1_7 = v4;
                    } else {
                        int v2_2;
                        android.view.View v3 = v0_1.getChildAt(v4);
                        if (!(v3 instanceof android.support.v7.internal.view.menu.aa)) {
                            v2_2 = 0;
                        } else {
                            v2_2 = ((android.support.v7.internal.view.menu.aa) v3).getItemData();
                        }
                        android.view.View v9 = this.a(v1_6, v3, v0_1);
                        if (v1_6 != v2_2) {
                            v9.setPressed(0);
                            android.support.v4.view.cx.v(v9);
                        }
                        if (v9 != v3) {
                            android.view.ViewGroup v1_9 = ((android.view.ViewGroup) v9.getParent());
                            if (v1_9 != null) {
                                v1_9.removeView(v9);
                            }
                            ((android.view.ViewGroup) this.g).addView(v9, v4);
                        }
                        v1_7 = (v4 + 1);
                    }
                    v6++;
                    v4 = v1_7;
                }
            }
            while (v4 < v0_1.getChildCount()) {
                if (!this.a(v0_1, v4)) {
                    v4++;
                }
            }
        }
        return;
    }

    public boolean a()
    {
        return 0;
    }

    public boolean a(android.support.v7.internal.view.menu.ad p2)
    {
        int v0_1;
        if (this.f == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.f.a_(p2);
        }
        return v0_1;
    }

    public final boolean a(android.support.v7.internal.view.menu.m p2)
    {
        return 0;
    }

    public boolean a(android.view.ViewGroup p2, int p3)
    {
        p2.removeViewAt(p3);
        return 1;
    }

    public final int b()
    {
        return this.h;
    }

    public final boolean b(android.support.v7.internal.view.menu.m p2)
    {
        return 0;
    }

    public boolean c(android.support.v7.internal.view.menu.m p2)
    {
        return 1;
    }
}
