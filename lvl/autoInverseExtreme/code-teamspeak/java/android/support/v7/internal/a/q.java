package android.support.v7.internal.a;
public final class q extends android.support.v7.app.g {
    android.support.v7.app.h b;
    int c;
    final synthetic android.support.v7.internal.a.l d;
    private Object e;
    private android.graphics.drawable.Drawable f;
    private CharSequence g;
    private CharSequence h;
    private android.view.View i;

    public q(android.support.v7.internal.a.l p2)
    {
        this.d = p2;
        this.c = -1;
        return;
    }

    private void e(int p1)
    {
        this.c = p1;
        return;
    }

    private android.support.v7.app.h h()
    {
        return this.b;
    }

    public final int a()
    {
        return this.c;
    }

    public final android.support.v7.app.g a(int p3)
    {
        android.support.v7.internal.widget.al v0_0 = this.d;
        if (v0_0.n == null) {
            v0_0.n = android.support.v7.internal.widget.av.a(v0_0.i);
        }
        this.f = v0_0.n.a(p3, 0);
        if (this.c >= 0) {
            android.support.v7.internal.a.l.k(this.d).b(this.c);
        }
        return this;
    }

    public final android.support.v7.app.g a(android.graphics.drawable.Drawable p3)
    {
        this.f = p3;
        if (this.c >= 0) {
            android.support.v7.internal.a.l.k(this.d).b(this.c);
        }
        return this;
    }

    public final android.support.v7.app.g a(android.support.v7.app.h p1)
    {
        this.b = p1;
        return this;
    }

    public final android.support.v7.app.g a(android.view.View p3)
    {
        this.i = p3;
        if (this.c >= 0) {
            android.support.v7.internal.a.l.k(this.d).b(this.c);
        }
        return this;
    }

    public final android.support.v7.app.g a(CharSequence p3)
    {
        this.g = p3;
        if (this.c >= 0) {
            android.support.v7.internal.a.l.k(this.d).b(this.c);
        }
        return this;
    }

    public final android.support.v7.app.g a(Object p1)
    {
        this.e = p1;
        return this;
    }

    public final android.graphics.drawable.Drawable b()
    {
        return this.f;
    }

    public final android.support.v7.app.g b(int p3)
    {
        this.g = android.support.v7.internal.a.l.j(this.d).getResources().getText(p3);
        if (this.c >= 0) {
            android.support.v7.internal.a.l.k(this.d).b(this.c);
        }
        return this;
    }

    public final android.support.v7.app.g b(CharSequence p3)
    {
        this.h = p3;
        if (this.c >= 0) {
            android.support.v7.internal.a.l.k(this.d).b(this.c);
        }
        return this;
    }

    public final android.support.v7.app.g c(int p3)
    {
        this.i = android.view.LayoutInflater.from(this.d.r()).inflate(p3, 0);
        if (this.c >= 0) {
            android.support.v7.internal.a.l.k(this.d).b(this.c);
        }
        return this;
    }

    public final CharSequence c()
    {
        return this.g;
    }

    public final android.support.v7.app.g d(int p3)
    {
        this.h = android.support.v7.internal.a.l.j(this.d).getResources().getText(p3);
        if (this.c >= 0) {
            android.support.v7.internal.a.l.k(this.d).b(this.c);
        }
        return this;
    }

    public final android.view.View d()
    {
        return this.i;
    }

    public final Object e()
    {
        return this.e;
    }

    public final void f()
    {
        this.d.c(this);
        return;
    }

    public final CharSequence g()
    {
        return this.h;
    }
}
