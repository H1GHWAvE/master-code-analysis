package android.support.v7.internal.a;
final class m extends android.support.v4.view.ge {
    final synthetic android.support.v7.internal.a.l a;

    m(android.support.v7.internal.a.l p1)
    {
        this.a = p1;
        return;
    }

    public final void b(android.view.View p5)
    {
        if ((android.support.v7.internal.a.l.a(this.a)) && (android.support.v7.internal.a.l.b(this.a) != null)) {
            android.support.v4.view.cx.b(android.support.v7.internal.a.l.b(this.a), 0);
            android.support.v4.view.cx.b(android.support.v7.internal.a.l.c(this.a), 0);
        }
        android.support.v7.internal.a.l.c(this.a).setVisibility(8);
        android.support.v7.internal.a.l.c(this.a).setTransitioning(0);
        android.support.v7.internal.a.l.d(this.a);
        android.support.v7.internal.widget.ActionBarOverlayLayout v0_13 = this.a;
        if (v0_13.l != null) {
            v0_13.l.a(v0_13.k);
            v0_13.k = 0;
            v0_13.l = 0;
        }
        if (android.support.v7.internal.a.l.e(this.a) != null) {
            android.support.v4.view.cx.t(android.support.v7.internal.a.l.e(this.a));
        }
        return;
    }
}
