package android.support.v7.internal.widget;
public class l extends android.database.DataSetObservable {
    public static final String a = "activity_choser_model_history.xml";
    public static final int b = 50;
    private static final boolean h = False;
    private static final String i = "None";
    private static final String j = "historical-records";
    private static final String k = "historical-record";
    private static final String l = "activity";
    private static final String m = "time";
    private static final String n = "weight";
    private static final int o = 5;
    private static final float p = 16256;
    private static final String q = ".xml";
    private static final int r = 255;
    private static final Object s;
    private static final java.util.Map t;
    private boolean A;
    private boolean B;
    public final Object c;
    final java.util.List d;
    public android.content.Intent e;
    public boolean f;
    public android.support.v7.internal.widget.s g;
    private final java.util.List u;
    private final android.content.Context v;
    private final String w;
    private android.support.v7.internal.widget.p x;
    private int y;
    private boolean z;

    static l()
    {
        android.support.v7.internal.widget.l.i = android.support.v7.internal.widget.l.getSimpleName();
        android.support.v7.internal.widget.l.s = new Object();
        android.support.v7.internal.widget.l.t = new java.util.HashMap();
        return;
    }

    private l(android.content.Context p4, String p5)
    {
        this.c = new Object();
        this.d = new java.util.ArrayList();
        this.u = new java.util.ArrayList();
        this.x = new android.support.v7.internal.widget.q(this, 0);
        this.y = 50;
        this.z = 1;
        this.A = 0;
        this.B = 1;
        this.f = 0;
        this.v = p4.getApplicationContext();
        if ((android.text.TextUtils.isEmpty(p5)) || (p5.endsWith(".xml"))) {
            this.w = p5;
        } else {
            this.w = new StringBuilder().append(p5).append(".xml").toString();
        }
        return;
    }

    static synthetic android.content.Context a(android.support.v7.internal.widget.l p1)
    {
        return p1.v;
    }

    public static android.support.v7.internal.widget.l a(android.content.Context p3, String p4)
    {
        try {
            android.support.v7.internal.widget.l v0_2 = ((android.support.v7.internal.widget.l) android.support.v7.internal.widget.l.t.get(p4));
        } catch (android.support.v7.internal.widget.l v0_4) {
            throw v0_4;
        }
        if (v0_2 == null) {
            v0_2 = new android.support.v7.internal.widget.l(p3, p4);
            android.support.v7.internal.widget.l.t.put(p4, v0_2);
        }
        return v0_2;
    }

    private void a(android.content.Intent p3)
    {
        try {
            if (this.e != p3) {
                this.e = p3;
                this.f = 1;
                this.d();
            } else {
            }
        } catch (int v0_2) {
            throw v0_2;
        }
        return;
    }

    private void a(android.support.v7.internal.widget.p p3)
    {
        try {
            if (this.x != p3) {
                this.x = p3;
                if (this.i()) {
                    this.notifyChanged();
                }
            } else {
            }
        } catch (boolean v0_2) {
            throw v0_2;
        }
        return;
    }

    private void a(android.support.v7.internal.widget.s p3)
    {
        try {
            this.g = p3;
            return;
        } catch (Throwable v0) {
            throw v0;
        }
    }

    static synthetic String b(android.support.v7.internal.widget.l p1)
    {
        return p1.w;
    }

    private void c(int p7)
    {
        try {
            int v1_3;
            this.d();
            android.support.v7.internal.widget.r v0_2 = ((android.support.v7.internal.widget.o) this.d.get(p7));
            int v1_2 = ((android.support.v7.internal.widget.o) this.d.get(0));
        } catch (android.support.v7.internal.widget.r v0_8) {
            throw v0_8;
        }
        if (v1_2 == 0) {
            v1_3 = 1065353216;
        } else {
            v1_3 = ((v1_2.b - v0_2.b) + 1084227584);
        }
        this.a(new android.support.v7.internal.widget.r(new android.content.ComponentName(v0_2.a.activityInfo.packageName, v0_2.a.activityInfo.name), System.currentTimeMillis(), v1_3));
        return;
    }

    static synthetic boolean c(android.support.v7.internal.widget.l p1)
    {
        p1.z = 1;
        return 1;
    }

    private void d(int p3)
    {
        try {
            if (this.y != p3) {
                this.y = p3;
                this.l();
                if (this.i()) {
                    this.notifyChanged();
                }
            } else {
            }
        } catch (boolean v0_2) {
            throw v0_2;
        }
        return;
    }

    static synthetic String e()
    {
        return android.support.v7.internal.widget.l.i;
    }

    private android.content.Intent f()
    {
        try {
            return this.e;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    private void g()
    {
        if (this.A) {
            if (this.B) {
                this.B = 0;
                if (!android.text.TextUtils.isEmpty(this.w)) {
                    android.support.v7.internal.widget.t v0_5 = new android.support.v7.internal.widget.t(this, 0);
                    Object[] v1_1 = new Object[2];
                    v1_1[0] = new java.util.ArrayList(this.u);
                    v1_1[1] = this.w;
                    if (android.os.Build$VERSION.SDK_INT < 11) {
                        v0_5.execute(v1_1);
                    } else {
                        v0_5.executeOnExecutor(android.os.AsyncTask.THREAD_POOL_EXECUTOR, v1_1);
                    }
                }
            }
            return;
        } else {
            throw new IllegalStateException("No preceding call to #readHistoricalData");
        }
    }

    private int h()
    {
        try {
            return this.y;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    private boolean i()
    {
        if ((this.x == null) || ((this.e == null) || ((this.d.isEmpty()) || (this.u.isEmpty())))) {
            int v0_6 = 0;
        } else {
            this.x.a(this.d, java.util.Collections.unmodifiableList(this.u));
            v0_6 = 1;
        }
        return v0_6;
    }

    private boolean j()
    {
        int v0_0 = 0;
        if ((this.f) && (this.e != null)) {
            this.f = 0;
            this.d.clear();
            java.util.List v2_1 = this.v.getPackageManager().queryIntentActivities(this.e, 0);
            int v3 = v2_1.size();
            int v1_5 = 0;
            while (v1_5 < v3) {
                this.d.add(new android.support.v7.internal.widget.o(this, ((android.content.pm.ResolveInfo) v2_1.get(v1_5))));
                v1_5++;
            }
            v0_0 = 1;
        }
        return v0_0;
    }

    private boolean k()
    {
        org.xmlpull.v1.XmlPullParserException v1_0 = 0;
        Throwable v0_0 = 1;
        if ((!this.z) || ((!this.B) || (android.text.TextUtils.isEmpty(this.w)))) {
            v0_0 = 0;
        } else {
            this.z = 0;
            this.A = 1;
            try {
                java.io.FileInputStream v2_5 = this.v.openFileInput(this.w);
            } catch (org.xmlpull.v1.XmlPullParserException v1) {
            }
            try {
                String v3_1 = android.util.Xml.newPullParser();
                v3_1.setInput(v2_5, "UTF-8");
            } catch (org.xmlpull.v1.XmlPullParserException v1_9) {
                android.util.Log.e(android.support.v7.internal.widget.l.i, new StringBuilder("Error reading historical recrod file: ").append(this.w).toString(), v1_9);
                if (v2_5 != null) {
                    try {
                        v2_5.close();
                    } catch (org.xmlpull.v1.XmlPullParserException v1) {
                    }
                }
            } catch (Throwable v0_1) {
                if (v2_5 != null) {
                    try {
                        v2_5.close();
                    } catch (org.xmlpull.v1.XmlPullParserException v1) {
                    }
                }
                throw v0_1;
            } catch (org.xmlpull.v1.XmlPullParserException v1_8) {
                android.util.Log.e(android.support.v7.internal.widget.l.i, new StringBuilder("Error reading historical recrod file: ").append(this.w).toString(), v1_8);
                if (v2_5 != null) {
                    try {
                        v2_5.close();
                    } catch (org.xmlpull.v1.XmlPullParserException v1) {
                    }
                }
            }
            while ((v1_0 != 1) && (v1_0 != 2)) {
                v1_0 = v3_1.next();
            }
            if ("historical-records".equals(v3_1.getName())) {
                org.xmlpull.v1.XmlPullParserException v1_3 = this.u;
                v1_3.clear();
                while(true) {
                    String v4_3 = v3_1.next();
                    if (v4_3 == 1) {
                        if (v2_5 != null) {
                            try {
                                v2_5.close();
                            } catch (org.xmlpull.v1.XmlPullParserException v1) {
                            }
                        }
                    } else {
                        if ((v4_3 != 3) && (v4_3 != 4)) {
                            if (!"historical-record".equals(v3_1.getName())) {
                                break;
                            }
                            v1_3.add(new android.support.v7.internal.widget.r(v3_1.getAttributeValue(0, "activity"), Long.parseLong(v3_1.getAttributeValue(0, "time")), Float.parseFloat(v3_1.getAttributeValue(0, "weight"))));
                        }
                    }
                }
                throw new org.xmlpull.v1.XmlPullParserException("Share records file not well-formed.");
            } else {
                throw new org.xmlpull.v1.XmlPullParserException("Share records file does not start with historical-records tag.");
            }
        }
        return v0_0;
    }

    private void l()
    {
        int v2_1 = (this.u.size() - this.y);
        if (v2_1 > 0) {
            this.B = 1;
            int v0_3 = 0;
            while (v0_3 < v2_1) {
                this.u.remove(0);
                v0_3++;
            }
        }
        return;
    }

    private void m()
    {
        try {
            java.io.IOException v1_1 = this.v.openFileInput(this.w);
        } catch (org.xmlpull.v1.XmlPullParserException v0) {
            return;
        }
        try {
            String v2_0 = android.util.Xml.newPullParser();
            v2_0.setInput(v1_1, "UTF-8");
            org.xmlpull.v1.XmlPullParserException v0_2 = 0;
        } catch (org.xmlpull.v1.XmlPullParserException v0_12) {
            if (v1_1 != null) {
                try {
                    v1_1.close();
                } catch (java.io.IOException v1) {
                }
            }
            throw v0_12;
        } catch (org.xmlpull.v1.XmlPullParserException v0_10) {
            android.util.Log.e(android.support.v7.internal.widget.l.i, new StringBuilder("Error reading historical recrod file: ").append(this.w).toString(), v0_10);
            if (v1_1 == null) {
                return;
            } else {
                try {
                    v1_1.close();
                } catch (org.xmlpull.v1.XmlPullParserException v0) {
                }
                return;
            }
        } catch (org.xmlpull.v1.XmlPullParserException v0_11) {
            android.util.Log.e(android.support.v7.internal.widget.l.i, new StringBuilder("Error reading historical recrod file: ").append(this.w).toString(), v0_11);
            if (v1_1 == null) {
                return;
            } else {
                try {
                    v1_1.close();
                } catch (org.xmlpull.v1.XmlPullParserException v0) {
                }
                return;
            }
        }
        while ((v0_2 != 1) && (v0_2 != 2)) {
            v0_2 = v2_0.next();
        }
        if ("historical-records".equals(v2_0.getName())) {
            org.xmlpull.v1.XmlPullParserException v0_5 = this.u;
            v0_5.clear();
            while(true) {
                String v3_2 = v2_0.next();
                if (v3_2 == 1) {
                    if (v1_1 == null) {
                        return;
                    } else {
                        try {
                            v1_1.close();
                        } catch (org.xmlpull.v1.XmlPullParserException v0) {
                        }
                        return;
                    }
                } else {
                    if ((v3_2 != 3) && (v3_2 != 4)) {
                        if (!"historical-record".equals(v2_0.getName())) {
                            break;
                        }
                        v0_5.add(new android.support.v7.internal.widget.r(v2_0.getAttributeValue(0, "activity"), Long.parseLong(v2_0.getAttributeValue(0, "time")), Float.parseFloat(v2_0.getAttributeValue(0, "weight"))));
                    }
                }
            }
            throw new org.xmlpull.v1.XmlPullParserException("Share records file not well-formed.");
        } else {
            throw new org.xmlpull.v1.XmlPullParserException("Share records file does not start with historical-records tag.");
        }
    }

    public final int a()
    {
        try {
            this.d();
            return this.d.size();
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public final int a(android.content.pm.ResolveInfo p6)
    {
        try {
            this.d();
            java.util.List v3 = this.d;
            int v4 = v3.size();
            int v1 = 0;
        } catch (int v0_4) {
            throw v0_4;
        }
        while (v1 < v4) {
            if (((android.support.v7.internal.widget.o) v3.get(v1)).a != p6) {
                v1++;
            } else {
                int v0_0 = v1;
            }
            return v0_0;
        }
        v0_0 = -1;
        return v0_0;
    }

    public final android.content.pm.ResolveInfo a(int p3)
    {
        try {
            this.d();
            return ((android.support.v7.internal.widget.o) this.d.get(p3)).a;
        } catch (Throwable v0_4) {
            throw v0_4;
        }
    }

    final boolean a(android.support.v7.internal.widget.r p8)
    {
        IllegalStateException v0_1 = this.u.add(p8);
        if (v0_1 != null) {
            this.B = 1;
            this.l();
            if (this.A) {
                if (this.B) {
                    this.B = 0;
                    if (!android.text.TextUtils.isEmpty(this.w)) {
                        android.support.v7.internal.widget.t v1_5 = new android.support.v7.internal.widget.t(this, 0);
                        Object[] v2_1 = new Object[2];
                        v2_1[0] = new java.util.ArrayList(this.u);
                        v2_1[1] = this.w;
                        if (android.os.Build$VERSION.SDK_INT < 11) {
                            v1_5.execute(v2_1);
                        } else {
                            v1_5.executeOnExecutor(android.os.AsyncTask.THREAD_POOL_EXECUTOR, v2_1);
                        }
                    }
                }
                this.i();
                this.notifyChanged();
            } else {
                throw new IllegalStateException("No preceding call to #readHistoricalData");
            }
        }
        return v0_1;
    }

    public final android.content.Intent b(int p8)
    {
        try {
            android.content.Intent v0_8;
            if (this.e != null) {
                this.d();
                android.content.Intent v0_3 = ((android.support.v7.internal.widget.o) this.d.get(p8));
                android.content.ComponentName v2_1 = new android.content.ComponentName(v0_3.a.activityInfo.packageName, v0_3.a.activityInfo.name);
                v0_8 = new android.content.Intent(this.e);
                v0_8.setComponent(v2_1);
                if (this.g != null) {
                    new android.content.Intent(v0_8);
                }
                this.a(new android.support.v7.internal.widget.r(v2_1, System.currentTimeMillis(), 1065353216));
            } else {
                v0_8 = 0;
            }
        } catch (android.content.Intent v0_9) {
            throw v0_9;
        }
        return v0_8;
    }

    public final android.content.pm.ResolveInfo b()
    {
        try {
            int v0_2;
            this.d();
        } catch (int v0_6) {
            throw v0_6;
        }
        if (this.d.isEmpty()) {
            v0_2 = 0;
        } else {
            v0_2 = ((android.support.v7.internal.widget.o) this.d.get(0)).a;
        }
        return v0_2;
    }

    public final int c()
    {
        try {
            this.d();
            return this.u.size();
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public final void d()
    {
        int v0_2;
        int v1 = 1;
        if ((!this.f) || (this.e == null)) {
            v0_2 = 0;
        } else {
            this.f = 0;
            this.d.clear();
            java.util.List v4 = this.v.getPackageManager().queryIntentActivities(this.e, 0);
            int v5 = v4.size();
            boolean v3_1 = 0;
            while (v3_1 < v5) {
                this.d.add(new android.support.v7.internal.widget.o(this, ((android.content.pm.ResolveInfo) v4.get(v3_1))));
                v3_1++;
            }
            v0_2 = 1;
        }
        if ((!this.z) || ((!this.B) || (android.text.TextUtils.isEmpty(this.w)))) {
            v1 = 0;
        } else {
            this.z = 0;
            this.A = 1;
            this.m();
        }
        int v0_6 = (v0_2 | v1);
        this.l();
        if (v0_6 != 0) {
            this.i();
            this.notifyChanged();
        }
        return;
    }
}
