package android.support.v7.internal.widget;
final class y extends android.widget.BaseAdapter {
    public static final int a = 2147483647;
    public static final int b = 4;
    private static final int f = 0;
    private static final int g = 1;
    private static final int h = 3;
    android.support.v7.internal.widget.l c;
    boolean d;
    final synthetic android.support.v7.internal.widget.ActivityChooserView e;
    private int i;
    private boolean j;
    private boolean k;

    private y(android.support.v7.internal.widget.ActivityChooserView p2)
    {
        this.e = p2;
        this.i = 4;
        return;
    }

    synthetic y(android.support.v7.internal.widget.ActivityChooserView p1, byte p2)
    {
        this(p1);
        return;
    }

    private void a(android.support.v7.internal.widget.l p3)
    {
        android.database.DataSetObserver v0_2 = android.support.v7.internal.widget.ActivityChooserView.a(this.e).c;
        if ((v0_2 != null) && (this.e.isShown())) {
            v0_2.unregisterObserver(android.support.v7.internal.widget.ActivityChooserView.i(this.e));
        }
        this.c = p3;
        if ((p3 != null) && (this.e.isShown())) {
            p3.registerObserver(android.support.v7.internal.widget.ActivityChooserView.i(this.e));
        }
        this.notifyDataSetChanged();
        return;
    }

    private android.content.pm.ResolveInfo b()
    {
        return this.c.b();
    }

    private int c()
    {
        return this.c.a();
    }

    private int d()
    {
        return this.c.c();
    }

    private android.support.v7.internal.widget.l e()
    {
        return this.c;
    }

    private boolean f()
    {
        return this.d;
    }

    public final int a()
    {
        int v0 = 0;
        int v4 = this.i;
        this.i = 2147483647;
        int v5 = android.view.View$MeasureSpec.makeMeasureSpec(0, 0);
        int v6 = android.view.View$MeasureSpec.makeMeasureSpec(0, 0);
        int v7 = this.getCount();
        android.view.View v1_1 = 0;
        int v3 = 0;
        while (v0 < v7) {
            v1_1 = this.getView(v0, v1_1, 0);
            v1_1.measure(v5, v6);
            v3 = Math.max(v3, v1_1.getMeasuredWidth());
            v0++;
        }
        this.i = v4;
        return v3;
    }

    public final void a(int p2)
    {
        if (this.i != p2) {
            this.i = p2;
            this.notifyDataSetChanged();
        }
        return;
    }

    public final void a(boolean p2)
    {
        if (this.k != p2) {
            this.k = p2;
            this.notifyDataSetChanged();
        }
        return;
    }

    public final void a(boolean p2, boolean p3)
    {
        if ((this.d != p2) || (this.j != p3)) {
            this.d = p2;
            this.j = p3;
            this.notifyDataSetChanged();
        }
        return;
    }

    public final int getCount()
    {
        int v0_1 = this.c.a();
        if ((!this.d) && (this.c.b() != null)) {
            v0_1--;
        }
        int v0_2 = Math.min(v0_1, this.i);
        if (this.k) {
            v0_2++;
        }
        return v0_2;
    }

    public final Object getItem(int p2)
    {
        android.content.pm.ResolveInfo v0_1;
        switch (this.getItemViewType(p2)) {
            case 0:
                if ((!this.d) && (this.c.b() != null)) {
                    p2++;
                }
                v0_1 = this.c.a(p2);
                break;
            case 1:
                v0_1 = 0;
                break;
            default:
                throw new IllegalArgumentException();
        }
        return v0_1;
    }

    public final long getItemId(int p3)
    {
        return ((long) p3);
    }

    public final int getItemViewType(int p2)
    {
        if ((!this.k) || (p2 != (this.getCount() - 1))) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final android.view.View getView(int p7, android.view.View p8, android.view.ViewGroup p9)
    {
        switch (this.getItemViewType(p7)) {
            case 0:
                if ((p8 == null) || (p8.getId() != android.support.v7.a.i.list_item)) {
                    p8 = android.view.LayoutInflater.from(this.e.getContext()).inflate(android.support.v7.a.k.abc_activity_chooser_view_list_item, p9, 0);
                }
                android.content.pm.PackageManager v2_1 = this.e.getContext().getPackageManager();
                CharSequence v1_7 = ((android.content.pm.ResolveInfo) this.getItem(p7));
                ((android.widget.ImageView) p8.findViewById(android.support.v7.a.i.icon)).setImageDrawable(v1_7.loadIcon(v2_1));
                ((android.widget.TextView) p8.findViewById(android.support.v7.a.i.title)).setText(v1_7.loadLabel(v2_1));
                if ((!this.d) || ((p7 != 0) || (!this.j))) {
                    android.support.v4.view.cx.b(p8, 0);
                } else {
                    android.support.v4.view.cx.b(p8, 1);
                }
                break;
            case 1:
                if ((p8 != null) && (p8.getId() == 1)) {
                } else {
                    p8 = android.view.LayoutInflater.from(this.e.getContext()).inflate(android.support.v7.a.k.abc_activity_chooser_view_list_item, p9, 0);
                    p8.setId(1);
                    ((android.widget.TextView) p8.findViewById(android.support.v7.a.i.title)).setText(this.e.getContext().getString(android.support.v7.a.l.abc_activity_chooser_view_see_all));
                }
                break;
            default:
                throw new IllegalArgumentException();
        }
        return p8;
    }

    public final int getViewTypeCount()
    {
        return 3;
    }
}
