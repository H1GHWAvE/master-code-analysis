package android.support.v7.internal.widget;
public class ActivityChooserView$InnerLayout extends android.support.v7.widget.aj {
    private static final int[] a;

    static ActivityChooserView$InnerLayout()
    {
        int[] v0_1 = new int[1];
        v0_1[0] = 16842964;
        android.support.v7.internal.widget.ActivityChooserView$InnerLayout.a = v0_1;
        return;
    }

    public ActivityChooserView$InnerLayout(android.content.Context p3, android.util.AttributeSet p4)
    {
        this(p3, p4);
        android.content.res.TypedArray v0_1 = android.support.v7.internal.widget.ax.a(p3, p4, android.support.v7.internal.widget.ActivityChooserView$InnerLayout.a);
        this.setBackgroundDrawable(v0_1.a(0));
        v0_1.a.recycle();
        return;
    }
}
