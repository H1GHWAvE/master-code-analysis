package android.support.v7.internal.view.menu;
public class i implements android.support.v4.g.a.a {
    private static final String p = "MenuBuilder";
    private static final String q = "android:menu:presenters";
    private static final String r = "android:menu:actionviewstates";
    private static final String s = "android:menu:expandedactionview";
    private static final int[] t;
    private boolean A;
    private android.view.ContextMenu$ContextMenuInfo B;
    private android.util.SparseArray C;
    private boolean D;
    private boolean E;
    private boolean F;
    private java.util.ArrayList G;
    private java.util.concurrent.CopyOnWriteArrayList H;
    public final android.content.Context e;
    public android.support.v7.internal.view.menu.j f;
    java.util.ArrayList g;
    public java.util.ArrayList h;
    public int i;
    CharSequence j;
    android.graphics.drawable.Drawable k;
    android.view.View l;
    boolean m;
    android.support.v7.internal.view.menu.m n;
    public boolean o;
    private final android.content.res.Resources u;
    private boolean v;
    private boolean w;
    private java.util.ArrayList x;
    private boolean y;
    private java.util.ArrayList z;

    static i()
    {
        int[] v0_1 = new int[6];
        v0_1 = {1, 4, 5, 3, 2, 0};
        android.support.v7.internal.view.menu.i.t = v0_1;
        return;
    }

    public i(android.content.Context p3)
    {
        this.i = 0;
        this.D = 0;
        this.E = 0;
        this.m = 0;
        this.F = 0;
        this.G = new java.util.ArrayList();
        this.H = new java.util.concurrent.CopyOnWriteArrayList();
        this.e = p3;
        this.u = p3.getResources();
        this.g = new java.util.ArrayList();
        this.x = new java.util.ArrayList();
        this.y = 1;
        this.h = new java.util.ArrayList();
        this.z = new java.util.ArrayList();
        this.A = 1;
        this.e(1);
        return;
    }

    private static int a(java.util.ArrayList p2, int p3)
    {
        int v1 = (p2.size() - 1);
        while (v1 >= 0) {
            if (((android.support.v7.internal.view.menu.m) p2.get(v1)).f > p3) {
                v1--;
            } else {
                int v0_2 = (v1 + 1);
            }
            return v0_2;
        }
        v0_2 = 0;
        return v0_2;
    }

    private android.support.v7.internal.view.menu.i a(android.view.View p7)
    {
        this.a(0, 0, 0, 0, p7);
        return this;
    }

    private android.support.v7.internal.view.menu.m a(int p9, int p10, int p11, int p12, CharSequence p13, int p14)
    {
        return new android.support.v7.internal.view.menu.m(this, p9, p10, p11, p12, p13, p14);
    }

    private android.support.v7.internal.view.menu.m a(int p13, android.view.KeyEvent p14)
    {
        int v0_2;
        java.util.ArrayList v5 = this.G;
        v5.clear();
        this.a(v5, p13, p14);
        if (!v5.isEmpty()) {
            int v6 = p14.getMetaState();
            android.view.KeyCharacterMap$KeyData v7_1 = new android.view.KeyCharacterMap$KeyData();
            p14.getKeyData(v7_1);
            int v8 = v5.size();
            if (v8 != 1) {
                boolean v9 = this.b();
                int v3 = 0;
                while (v3 < v8) {
                    int v1_0;
                    v0_2 = ((android.support.v7.internal.view.menu.m) v5.get(v3));
                    if (!v9) {
                        v1_0 = v0_2.getNumericShortcut();
                    } else {
                        v1_0 = v0_2.getAlphabeticShortcut();
                    }
                    if (((v1_0 != v7_1.meta[0]) || ((v6 & 2) != 0)) && (((v1_0 != v7_1.meta[2]) || ((v6 & 2) == 0)) && ((!v9) || ((v1_0 != 8) || (p13 != 67))))) {
                        v3++;
                    }
                }
                v0_2 = 0;
            } else {
                v0_2 = ((android.support.v7.internal.view.menu.m) v5.get(0));
            }
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    private void a(int p2)
    {
        this.a(p2, 1);
        return;
    }

    private void a(int p2, boolean p3)
    {
        if ((p2 >= 0) && (p2 < this.g.size())) {
            this.g.remove(p2);
            if (p3) {
                this.c(1);
            }
        }
        return;
    }

    private void a(android.view.ContextMenu$ContextMenuInfo p1)
    {
        this.B = p1;
        return;
    }

    private void a(android.view.MenuItem p7)
    {
        int v4 = p7.getGroupId();
        int v5 = this.g.size();
        int v3 = 0;
        while (v3 < v5) {
            int v0_3 = ((android.support.v7.internal.view.menu.m) this.g.get(v3));
            if ((v0_3.getGroupId() == v4) && ((v0_3.e()) && (v0_3.isCheckable()))) {
                int v1_3;
                if (v0_3 != p7) {
                    v1_3 = 0;
                } else {
                    v1_3 = 1;
                }
                v0_3.b(v1_3);
            }
            v3++;
        }
        return;
    }

    private void a(java.util.List p12, int p13, android.view.KeyEvent p14)
    {
        boolean v4 = this.b();
        int v5 = p14.getMetaState();
        android.view.KeyCharacterMap$KeyData v6_1 = new android.view.KeyCharacterMap$KeyData();
        if ((p14.getKeyData(v6_1)) || (p13 == 67)) {
            int v7 = this.g.size();
            int v2 = 0;
            while (v2 < v7) {
                int v0_4 = ((android.support.v7.internal.view.menu.m) this.g.get(v2));
                if (v0_4.hasSubMenu()) {
                    ((android.support.v7.internal.view.menu.i) v0_4.getSubMenu()).a(p12, p13, p14);
                }
                boolean v1_3;
                if (!v4) {
                    v1_3 = v0_4.getNumericShortcut();
                } else {
                    v1_3 = v0_4.getAlphabeticShortcut();
                }
                if ((((v5 & 5) == 0) && ((v1_3) && ((v1_3 == v6_1.meta[0]) || ((v1_3 == v6_1.meta[2]) || ((v4) && ((v1_3 == 8) && (p13 == 67))))))) && (v0_4.isEnabled())) {
                    p12.add(v0_4);
                }
                v2++;
            }
        }
        return;
    }

    private boolean a(android.support.v7.internal.view.menu.ad p5, android.support.v7.internal.view.menu.x p6)
    {
        boolean v0_0 = 0;
        if (!this.H.isEmpty()) {
            if (p6 != null) {
                v0_0 = p6.a(p5);
            }
            java.util.Iterator v3 = this.H.iterator();
            boolean v2 = v0_0;
            while (v3.hasNext()) {
                boolean v0_3 = ((ref.WeakReference) v3.next());
                java.util.concurrent.CopyOnWriteArrayList v1_4 = ((android.support.v7.internal.view.menu.x) v0_3.get());
                if (v1_4 != null) {
                    boolean v0_4;
                    if (v2) {
                        v0_4 = v2;
                    } else {
                        v0_4 = v1_4.a(p5);
                    }
                    v2 = v0_4;
                } else {
                    this.H.remove(v0_3);
                }
            }
            v0_0 = v2;
        }
        return v0_0;
    }

    private boolean a(android.view.MenuItem p2, int p3)
    {
        return this.a(p2, 0, p3);
    }

    private int b(int p4)
    {
        int v2 = this.size();
        int v1 = 0;
        while (v1 < v2) {
            if (((android.support.v7.internal.view.menu.m) this.g.get(v1)).getItemId() != p4) {
                v1++;
            } else {
                int v0_0 = v1;
            }
            return v0_0;
        }
        v0_0 = -1;
        return v0_0;
    }

    private int c(int p4)
    {
        int v2 = this.size();
        int v1 = 0;
        while (v1 < v2) {
            if (((android.support.v7.internal.view.menu.m) this.g.get(v1)).getGroupId() != p4) {
                v1++;
            } else {
                int v0_0 = v1;
            }
            return v0_0;
        }
        v0_0 = -1;
        return v0_0;
    }

    private int d(int p4)
    {
        int v2 = this.size();
        int v1 = 0;
        while (v1 < v2) {
            if (((android.support.v7.internal.view.menu.m) this.g.get(v1)).getGroupId() != p4) {
                v1++;
            } else {
                int v0_0 = v1;
            }
            return v0_0;
        }
        v0_0 = -1;
        return v0_0;
    }

    private void d(boolean p4)
    {
        if (!this.H.isEmpty()) {
            this.d();
            java.util.Iterator v2 = this.H.iterator();
            while (v2.hasNext()) {
                ref.WeakReference v0_5 = ((ref.WeakReference) v2.next());
                java.util.concurrent.CopyOnWriteArrayList v1_1 = ((android.support.v7.internal.view.menu.x) v0_5.get());
                if (v1_1 != null) {
                    v1_1.a(p4);
                } else {
                    this.H.remove(v0_5);
                }
            }
            this.e();
        }
        return;
    }

    private static int e(int p2)
    {
        int v0_2 = ((-65536 & p2) >> 16);
        if ((v0_2 >= 0) && (v0_2 < android.support.v7.internal.view.menu.i.t.length)) {
            return ((android.support.v7.internal.view.menu.i.t[v0_2] << 16) | (65535 & p2));
        } else {
            throw new IllegalArgumentException("order does not contain a valid category.");
        }
    }

    private void e(android.os.Bundle p5)
    {
        if (!this.H.isEmpty()) {
            android.util.SparseArray v2_1 = new android.util.SparseArray();
            java.util.Iterator v3 = this.H.iterator();
            while (v3.hasNext()) {
                int v0_6 = ((ref.WeakReference) v3.next());
                android.os.Parcelable v1_1 = ((android.support.v7.internal.view.menu.x) v0_6.get());
                if (v1_1 != null) {
                    int v0_7 = v1_1.b();
                    if (v0_7 > 0) {
                        android.os.Parcelable v1_2 = v1_1.c();
                        if (v1_2 != null) {
                            v2_1.put(v0_7, v1_2);
                        }
                    }
                } else {
                    this.H.remove(v0_6);
                }
            }
            p5.putSparseParcelableArray("android:menu:presenters", v2_1);
        }
        return;
    }

    private void e(boolean p4)
    {
        int v0 = 1;
        if ((!p4) || ((this.u.getConfiguration().keyboard == 1) || (!this.u.getBoolean(android.support.v7.a.e.abc_config_showMenuShortcutsWhenKeyboardPresent)))) {
            v0 = 0;
        }
        this.w = v0;
        return;
    }

    private android.support.v7.internal.view.menu.i f(int p7)
    {
        this.a(p7, 0, 0, 0, 0);
        return this;
    }

    private void f(android.os.Bundle p5)
    {
        android.util.SparseArray v2 = p5.getSparseParcelableArray("android:menu:presenters");
        if ((v2 != null) && (!this.H.isEmpty())) {
            java.util.Iterator v3 = this.H.iterator();
            while (v3.hasNext()) {
                android.os.Parcelable v0_6 = ((ref.WeakReference) v3.next());
                java.util.concurrent.CopyOnWriteArrayList v1_1 = ((android.support.v7.internal.view.menu.x) v0_6.get());
                if (v1_1 != null) {
                    android.os.Parcelable v0_7 = v1_1.b();
                    if (v0_7 > null) {
                        android.os.Parcelable v0_9 = ((android.os.Parcelable) v2.get(v0_7));
                        if (v0_9 != null) {
                            v1_1.a(v0_9);
                        }
                    }
                } else {
                    this.H.remove(v0_6);
                }
            }
        }
        return;
    }

    private void f(boolean p1)
    {
        this.m = p1;
        return;
    }

    private android.support.v7.internal.view.menu.i g(int p7)
    {
        this.a(0, 0, p7, 0, 0);
        return this;
    }

    private void g(boolean p1)
    {
        this.o = p1;
        return;
    }

    private android.support.v7.internal.view.menu.i l()
    {
        this.i = 1;
        return this;
    }

    private void m()
    {
        this.D = 1;
        this.clear();
        this.clearHeader();
        this.D = 0;
        this.E = 0;
        this.c(1);
        return;
    }

    private android.content.res.Resources n()
    {
        return this.u;
    }

    private android.content.Context o()
    {
        return this.e;
    }

    private void p()
    {
        if (this.f != null) {
            this.f.a(this);
        }
        return;
    }

    private java.util.ArrayList q()
    {
        this.i();
        return this.h;
    }

    private CharSequence r()
    {
        return this.j;
    }

    private android.graphics.drawable.Drawable s()
    {
        return this.k;
    }

    private android.view.View t()
    {
        return this.l;
    }

    private boolean u()
    {
        return this.m;
    }

    private android.support.v7.internal.view.menu.m v()
    {
        return this.n;
    }

    protected final android.support.v7.internal.view.menu.i a(android.graphics.drawable.Drawable p7)
    {
        this.a(0, 0, 0, p7, 0);
        return this;
    }

    protected final android.support.v7.internal.view.menu.i a(CharSequence p7)
    {
        this.a(0, p7, 0, 0, 0);
        return this;
    }

    public final android.view.MenuItem a(int p9, int p10, int p11, CharSequence p12)
    {
        android.support.v7.internal.view.menu.m v0_2 = ((-65536 & p11) >> 16);
        if ((v0_2 >= null) && (v0_2 < android.support.v7.internal.view.menu.i.t.length)) {
            int v5 = ((android.support.v7.internal.view.menu.i.t[v0_2] << 16) | (65535 & p11));
            android.support.v7.internal.view.menu.m v0_6 = new android.support.v7.internal.view.menu.m(this, p9, p10, p11, v5, p12, this.i);
            if (this.B != null) {
                v0_6.k = this.B;
            }
            this.g.add(android.support.v7.internal.view.menu.i.a(this.g, v5), v0_6);
            this.c(1);
            return v0_6;
        } else {
            throw new IllegalArgumentException("order does not contain a valid category.");
        }
    }

    protected String a()
    {
        return "android:menu:actionviewstates";
    }

    final void a(int p3, CharSequence p4, int p5, android.graphics.drawable.Drawable p6, android.view.View p7)
    {
        if (p7 == null) {
            if (p3 <= 0) {
                if (p4 != null) {
                    this.j = p4;
                }
            } else {
                this.j = this.u.getText(p3);
            }
            if (p5 <= 0) {
                if (p6 != null) {
                    this.k = p6;
                }
            } else {
                this.k = android.support.v4.c.h.a(this.e, p5);
            }
            this.l = 0;
        } else {
            this.l = p7;
            this.j = 0;
            this.k = 0;
        }
        this.c(0);
        return;
    }

    public final void a(android.os.Bundle p5)
    {
        if (!this.H.isEmpty()) {
            android.util.SparseArray v2_1 = new android.util.SparseArray();
            java.util.Iterator v3 = this.H.iterator();
            while (v3.hasNext()) {
                int v0_6 = ((ref.WeakReference) v3.next());
                android.os.Parcelable v1_1 = ((android.support.v7.internal.view.menu.x) v0_6.get());
                if (v1_1 != null) {
                    int v0_7 = v1_1.b();
                    if (v0_7 > 0) {
                        android.os.Parcelable v1_2 = v1_1.c();
                        if (v1_2 != null) {
                            v2_1.put(v0_7, v1_2);
                        }
                    }
                } else {
                    this.H.remove(v0_6);
                }
            }
            p5.putSparseParcelableArray("android:menu:presenters", v2_1);
        }
        return;
    }

    public void a(android.support.v7.internal.view.menu.j p1)
    {
        this.f = p1;
        return;
    }

    public final void a(android.support.v7.internal.view.menu.x p2)
    {
        this.a(p2, this.e);
        return;
    }

    public final void a(android.support.v7.internal.view.menu.x p3, android.content.Context p4)
    {
        this.H.add(new ref.WeakReference(p3));
        p3.a(p4, this);
        this.A = 1;
        return;
    }

    public void a(boolean p2)
    {
        if (this.w != p2) {
            this.e(p2);
            this.c(0);
        }
        return;
    }

    boolean a(android.support.v7.internal.view.menu.i p2, android.view.MenuItem p3)
    {
        if ((this.f == null) || (!this.f.a(p2, p3))) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public boolean a(android.support.v7.internal.view.menu.m p5)
    {
        boolean v0_0 = 0;
        if (!this.H.isEmpty()) {
            this.d();
            java.util.Iterator v3 = this.H.iterator();
            boolean v2 = 0;
            while (v3.hasNext()) {
                boolean v0_3 = ((ref.WeakReference) v3.next());
                java.util.concurrent.CopyOnWriteArrayList v1_4 = ((android.support.v7.internal.view.menu.x) v0_3.get());
                if (v1_4 != null) {
                    v0_0 = v1_4.a(p5);
                    if (!v0_0) {
                        v2 = v0_0;
                    }
                } else {
                    this.H.remove(v0_3);
                }
                this.e();
                if (v0_0) {
                    this.n = p5;
                }
            }
            v0_0 = v2;
        }
        return v0_0;
    }

    public final boolean a(android.view.MenuItem p8, android.support.v7.internal.view.menu.x p9, int p10)
    {
        int v0_5;
        boolean v2_0 = 0;
        if ((((android.support.v7.internal.view.menu.m) p8) != null) && (((android.support.v7.internal.view.menu.m) p8).isEnabled())) {
            ref.WeakReference v1_0;
            boolean v5 = ((android.support.v7.internal.view.menu.m) p8).b();
            boolean v4_0 = ((android.support.v7.internal.view.menu.m) p8).i;
            if ((!v4_0) || (!v4_0.f())) {
                v1_0 = 0;
            } else {
                v1_0 = 1;
            }
            if (!((android.support.v7.internal.view.menu.m) p8).i()) {
                if ((!((android.support.v7.internal.view.menu.m) p8).hasSubMenu()) && (v1_0 == null)) {
                    if ((p10 & 1) == 0) {
                        this.b(1);
                    }
                    v0_5 = v5;
                } else {
                    this.b(0);
                    if (!((android.support.v7.internal.view.menu.m) p8).hasSubMenu()) {
                        ((android.support.v7.internal.view.menu.m) p8).a(new android.support.v7.internal.view.menu.ad(this.e, this, ((android.support.v7.internal.view.menu.m) p8)));
                    }
                    int v0_10 = ((android.support.v7.internal.view.menu.ad) ((android.support.v7.internal.view.menu.m) p8).getSubMenu());
                    if (v1_0 != null) {
                        v4_0.a(v0_10);
                    }
                    if (!this.H.isEmpty()) {
                        if (p9 != null) {
                            v2_0 = p9.a(v0_10);
                        }
                        java.util.Iterator v6_1 = this.H.iterator();
                        boolean v4_1 = v2_0;
                        while (v6_1.hasNext()) {
                            ref.WeakReference v1_6 = ((ref.WeakReference) v6_1.next());
                            boolean v2_2 = ((android.support.v7.internal.view.menu.x) v1_6.get());
                            if (v2_2) {
                                boolean v2_3;
                                if (v4_1) {
                                    v2_3 = v4_1;
                                } else {
                                    v2_3 = v2_2.a(v0_10);
                                }
                                v4_1 = v2_3;
                            } else {
                                this.H.remove(v1_6);
                            }
                        }
                        v2_0 = v4_1;
                    }
                    v0_5 = (v5 | v2_0);
                    if (v0_5 == 0) {
                        this.b(1);
                    }
                }
            } else {
                v0_5 = (((android.support.v7.internal.view.menu.m) p8).expandActionView() | v5);
                if (v0_5 != 0) {
                    this.b(1);
                }
            }
        } else {
            v0_5 = 0;
        }
        return v0_5;
    }

    public android.view.MenuItem add(int p3)
    {
        return this.a(0, 0, 0, this.u.getString(p3));
    }

    public android.view.MenuItem add(int p2, int p3, int p4, int p5)
    {
        return this.a(p2, p3, p4, this.u.getString(p5));
    }

    public android.view.MenuItem add(int p2, int p3, int p4, CharSequence p5)
    {
        return this.a(p2, p3, p4, p5);
    }

    public android.view.MenuItem add(CharSequence p2)
    {
        return this.a(0, 0, 0, p2);
    }

    public int addIntentOptions(int p10, int p11, int p12, android.content.ComponentName p13, android.content.Intent[] p14, android.content.Intent p15, int p16, android.view.MenuItem[] p17)
    {
        int v3;
        android.content.pm.PackageManager v4 = this.e.getPackageManager();
        java.util.List v5 = v4.queryIntentActivityOptions(p13, p14, p15, 0);
        if (v5 == null) {
            v3 = 0;
        } else {
            v3 = v5.size();
        }
        if ((p16 & 1) == 0) {
            this.removeGroup(p10);
        }
        int v2 = 0;
        while (v2 < v3) {
            android.view.MenuItem v1_2;
            int v0_7 = ((android.content.pm.ResolveInfo) v5.get(v2));
            if (v0_7.specificIndex >= 0) {
                v1_2 = p14[v0_7.specificIndex];
            } else {
                v1_2 = p15;
            }
            int v6_1 = new android.content.Intent(v1_2);
            v6_1.setComponent(new android.content.ComponentName(v0_7.activityInfo.applicationInfo.packageName, v0_7.activityInfo.name));
            android.view.MenuItem v1_8 = this.add(p10, p11, p12, v0_7.loadLabel(v4)).setIcon(v0_7.loadIcon(v4)).setIntent(v6_1);
            if ((p17 != null) && (v0_7.specificIndex >= 0)) {
                p17[v0_7.specificIndex] = v1_8;
            }
            v2++;
        }
        return v3;
    }

    public android.view.SubMenu addSubMenu(int p3)
    {
        return this.addSubMenu(0, 0, 0, this.u.getString(p3));
    }

    public android.view.SubMenu addSubMenu(int p2, int p3, int p4, int p5)
    {
        return this.addSubMenu(p2, p3, p4, this.u.getString(p5));
    }

    public android.view.SubMenu addSubMenu(int p4, int p5, int p6, CharSequence p7)
    {
        android.support.v7.internal.view.menu.m v0_1 = ((android.support.v7.internal.view.menu.m) this.a(p4, p5, p6, p7));
        android.support.v7.internal.view.menu.ad v1_1 = new android.support.v7.internal.view.menu.ad(this.e, this, v0_1);
        v0_1.a(v1_1);
        return v1_1;
    }

    public android.view.SubMenu addSubMenu(CharSequence p2)
    {
        return this.addSubMenu(0, 0, 0, p2);
    }

    public final void b(android.os.Bundle p5)
    {
        android.util.SparseArray v2 = p5.getSparseParcelableArray("android:menu:presenters");
        if ((v2 != null) && (!this.H.isEmpty())) {
            java.util.Iterator v3 = this.H.iterator();
            while (v3.hasNext()) {
                android.os.Parcelable v0_6 = ((ref.WeakReference) v3.next());
                java.util.concurrent.CopyOnWriteArrayList v1_1 = ((android.support.v7.internal.view.menu.x) v0_6.get());
                if (v1_1 != null) {
                    android.os.Parcelable v0_7 = v1_1.b();
                    if (v0_7 > null) {
                        android.os.Parcelable v0_9 = ((android.os.Parcelable) v2.get(v0_7));
                        if (v0_9 != null) {
                            v1_1.a(v0_9);
                        }
                    }
                } else {
                    this.H.remove(v0_6);
                }
            }
        }
        return;
    }

    public final void b(android.support.v7.internal.view.menu.x p4)
    {
        java.util.Iterator v2 = this.H.iterator();
        while (v2.hasNext()) {
            ref.WeakReference v0_3 = ((ref.WeakReference) v2.next());
            java.util.concurrent.CopyOnWriteArrayList v1_1 = ((android.support.v7.internal.view.menu.x) v0_3.get());
            if ((v1_1 == null) || (v1_1 == p4)) {
                this.H.remove(v0_3);
            }
        }
        return;
    }

    public final void b(boolean p4)
    {
        if (!this.F) {
            this.F = 1;
            java.util.Iterator v2 = this.H.iterator();
            while (v2.hasNext()) {
                int v0_6 = ((ref.WeakReference) v2.next());
                java.util.concurrent.CopyOnWriteArrayList v1_1 = ((android.support.v7.internal.view.menu.x) v0_6.get());
                if (v1_1 != null) {
                    v1_1.a(this, p4);
                } else {
                    this.H.remove(v0_6);
                }
            }
            this.F = 0;
        }
        return;
    }

    boolean b()
    {
        return this.v;
    }

    public boolean b(android.support.v7.internal.view.menu.m p5)
    {
        boolean v0_0 = 0;
        if ((!this.H.isEmpty()) && (this.n == p5)) {
            this.d();
            java.util.Iterator v3 = this.H.iterator();
            boolean v2 = 0;
            while (v3.hasNext()) {
                boolean v0_3 = ((ref.WeakReference) v3.next());
                int v1_5 = ((android.support.v7.internal.view.menu.x) v0_3.get());
                if (v1_5 != 0) {
                    v0_0 = v1_5.b(p5);
                    if (!v0_0) {
                        v2 = v0_0;
                    }
                } else {
                    this.H.remove(v0_3);
                }
                this.e();
                if (v0_0) {
                    this.n = 0;
                }
            }
            v0_0 = v2;
        }
        return v0_0;
    }

    public final void c(android.os.Bundle p8)
    {
        int v3 = this.size();
        int v2 = 0;
        android.util.SparseArray v0_1 = 0;
        while (v2 < v3) {
            android.view.MenuItem v4 = this.getItem(v2);
            String v1_2 = android.support.v4.view.az.a(v4);
            if ((v1_2 != null) && (v1_2.getId() != -1)) {
                if (v0_1 == null) {
                    v0_1 = new android.util.SparseArray();
                }
                v1_2.saveHierarchyState(v0_1);
                if (android.support.v4.view.az.c(v4)) {
                    p8.putInt("android:menu:expandedactionview", v4.getItemId());
                }
            }
            String v1_5 = v0_1;
            if (v4.hasSubMenu()) {
                ((android.support.v7.internal.view.menu.ad) v4.getSubMenu()).c(p8);
            }
            v2++;
            v0_1 = v1_5;
        }
        if (v0_1 != null) {
            p8.putSparseParcelableArray(this.a(), v0_1);
        }
        return;
    }

    public final void c(boolean p4)
    {
        if (this.D) {
            this.E = 1;
        } else {
            if (p4) {
                this.y = 1;
                this.A = 1;
            }
            if (!this.H.isEmpty()) {
                this.d();
                java.util.Iterator v2 = this.H.iterator();
                while (v2.hasNext()) {
                    ref.WeakReference v0_6 = ((ref.WeakReference) v2.next());
                    java.util.concurrent.CopyOnWriteArrayList v1_2 = ((android.support.v7.internal.view.menu.x) v0_6.get());
                    if (v1_2 != null) {
                        v1_2.a(p4);
                    } else {
                        this.H.remove(v0_6);
                    }
                }
                this.e();
            }
        }
        return;
    }

    public boolean c()
    {
        return this.w;
    }

    public void clear()
    {
        if (this.n != null) {
            this.b(this.n);
        }
        this.g.clear();
        this.c(1);
        return;
    }

    public void clearHeader()
    {
        this.k = 0;
        this.j = 0;
        this.l = 0;
        this.c(0);
        return;
    }

    public void close()
    {
        this.b(1);
        return;
    }

    public final void d()
    {
        if (!this.D) {
            this.D = 1;
            this.E = 0;
        }
        return;
    }

    public final void d(android.os.Bundle p8)
    {
        if (p8 != null) {
            android.util.SparseArray v2 = p8.getSparseParcelableArray(this.a());
            int v3 = this.size();
            int v1 = 0;
            while (v1 < v3) {
                int v0_5 = this.getItem(v1);
                boolean v4_0 = android.support.v4.view.az.a(v0_5);
                if ((v4_0) && (v4_0.getId() != -1)) {
                    v4_0.restoreHierarchyState(v2);
                }
                if (v0_5.hasSubMenu()) {
                    ((android.support.v7.internal.view.menu.ad) v0_5.getSubMenu()).d(p8);
                }
                v1++;
            }
            int v0_3 = p8.getInt("android:menu:expandedactionview");
            if (v0_3 > 0) {
                int v0_4 = this.findItem(v0_3);
                if (v0_4 != 0) {
                    android.support.v4.view.az.b(v0_4);
                }
            }
        }
        return;
    }

    public final void e()
    {
        this.D = 0;
        if (this.E) {
            this.E = 0;
            this.c(1);
        }
        return;
    }

    final void f()
    {
        this.y = 1;
        this.c(1);
        return;
    }

    public android.view.MenuItem findItem(int p5)
    {
        int v2 = this.size();
        int v1 = 0;
        while (v1 < v2) {
            int v0_1 = ((android.support.v7.internal.view.menu.m) this.g.get(v1));
            if (v0_1.getItemId() != p5) {
                if (v0_1.hasSubMenu()) {
                    v0_1 = v0_1.getSubMenu().findItem(p5);
                    if (v0_1 != 0) {
                        return v0_1;
                    }
                }
                v1++;
            }
            return v0_1;
        }
        v0_1 = 0;
        return v0_1;
    }

    final void g()
    {
        this.A = 1;
        this.c(1);
        return;
    }

    public android.view.MenuItem getItem(int p2)
    {
        return ((android.view.MenuItem) this.g.get(p2));
    }

    public final java.util.ArrayList h()
    {
        int v0_4;
        if (this.y) {
            this.x.clear();
            int v3 = this.g.size();
            int v1 = 0;
            while (v1 < v3) {
                int v0_7 = ((android.support.v7.internal.view.menu.m) this.g.get(v1));
                if (v0_7.isVisible()) {
                    this.x.add(v0_7);
                }
                v1++;
            }
            this.y = 0;
            this.A = 1;
            v0_4 = this.x;
        } else {
            v0_4 = this.x;
        }
        return v0_4;
    }

    public boolean hasVisibleItems()
    {
        int v0_1;
        if (!this.o) {
            int v4 = this.size();
            int v3 = 0;
            while (v3 < v4) {
                if (!((android.support.v7.internal.view.menu.m) this.g.get(v3)).isVisible()) {
                    v3++;
                } else {
                    v0_1 = 1;
                }
            }
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final void i()
    {
        java.util.ArrayList v4 = this.h();
        if (this.A) {
            java.util.ArrayList v5_0 = this.H.iterator();
            int v2_0 = 0;
            while (v5_0.hasNext()) {
                int v0_12 = ((ref.WeakReference) v5_0.next());
                int v1_3 = ((android.support.v7.internal.view.menu.x) v0_12.get());
                if (v1_3 != 0) {
                    v2_0 = (v1_3.a() | v2_0);
                } else {
                    this.H.remove(v0_12);
                }
            }
            if (v2_0 == 0) {
                this.h.clear();
                this.z.clear();
                this.z.addAll(this.h());
            } else {
                this.h.clear();
                this.z.clear();
                int v2_1 = v4.size();
                int v1_1 = 0;
                while (v1_1 < v2_1) {
                    int v0_9 = ((android.support.v7.internal.view.menu.m) v4.get(v1_1));
                    if (!v0_9.f()) {
                        this.z.add(v0_9);
                    } else {
                        this.h.add(v0_9);
                    }
                    v1_1++;
                }
            }
            this.A = 0;
        }
        return;
    }

    public boolean isShortcutKey(int p2, android.view.KeyEvent p3)
    {
        int v0_1;
        if (this.a(p2, p3) == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final java.util.ArrayList j()
    {
        this.i();
        return this.z;
    }

    public android.support.v7.internal.view.menu.i k()
    {
        return this;
    }

    public boolean performIdentifierAction(int p3, int p4)
    {
        return this.a(this.findItem(p3), 0, p4);
    }

    public boolean performShortcut(int p3, android.view.KeyEvent p4, int p5)
    {
        int v1_0 = this.a(p3, p4);
        boolean v0_0 = 0;
        if (v1_0 != 0) {
            v0_0 = this.a(v1_0, 0, p5);
        }
        if ((p5 & 2) != 0) {
            this.b(1);
        }
        return v0_0;
    }

    public void removeGroup(int p6)
    {
        int v3_0 = this.size();
        int v2_0 = 0;
        while (v2_0 < v3_0) {
            if (((android.support.v7.internal.view.menu.m) this.g.get(v2_0)).getGroupId() != p6) {
                v2_0++;
            } else {
                int v3_1 = v2_0;
            }
            if (v3_1 >= 0) {
                int v4 = (this.g.size() - v3_1);
                int v0_7 = 0;
                while(true) {
                    int v2_1 = (v0_7 + 1);
                    if ((v0_7 >= v4) || (((android.support.v7.internal.view.menu.m) this.g.get(v3_1)).getGroupId() != p6)) {
                        break;
                    }
                    this.a(v3_1, 0);
                    v0_7 = v2_1;
                }
                this.c(1);
            }
            return;
        }
        v3_1 = -1;
    }

    public void removeItem(int p4)
    {
        int v2 = this.size();
        int v1_0 = 0;
        while (v1_0 < v2) {
            if (((android.support.v7.internal.view.menu.m) this.g.get(v1_0)).getItemId() != p4) {
                v1_0++;
            } else {
                int v0_0 = v1_0;
            }
            this.a(v0_0, 1);
            return;
        }
        v0_0 = -1;
        this.a(v0_0, 1);
        return;
    }

    public void setGroupCheckable(int p5, boolean p6, boolean p7)
    {
        int v2 = this.g.size();
        int v1 = 0;
        while (v1 < v2) {
            int v0_4 = ((android.support.v7.internal.view.menu.m) this.g.get(v1));
            if (v0_4.getGroupId() == p5) {
                v0_4.a(p7);
                v0_4.setCheckable(p6);
            }
            v1++;
        }
        return;
    }

    public void setGroupEnabled(int p5, boolean p6)
    {
        int v2 = this.g.size();
        int v1 = 0;
        while (v1 < v2) {
            int v0_4 = ((android.support.v7.internal.view.menu.m) this.g.get(v1));
            if (v0_4.getGroupId() == p5) {
                v0_4.setEnabled(p6);
            }
            v1++;
        }
        return;
    }

    public void setGroupVisible(int p7, boolean p8)
    {
        int v4 = this.g.size();
        int v3 = 0;
        int v2_1 = 0;
        while (v3 < v4) {
            int v0_5;
            int v0_3 = ((android.support.v7.internal.view.menu.m) this.g.get(v3));
            if ((v0_3.getGroupId() != p7) || (!v0_3.c(p8))) {
                v0_5 = v2_1;
            } else {
                v0_5 = 1;
            }
            v3++;
            v2_1 = v0_5;
        }
        if (v2_1 != 0) {
            this.c(1);
        }
        return;
    }

    public void setQwertyMode(boolean p2)
    {
        this.v = p2;
        this.c(0);
        return;
    }

    public int size()
    {
        return this.g.size();
    }
}
