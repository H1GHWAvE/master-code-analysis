package android.support.v7.internal.view;
public class k implements android.view.Window$Callback {
    final android.view.Window$Callback d;

    public k(android.view.Window$Callback p3)
    {
        if (p3 != null) {
            this.d = p3;
            return;
        } else {
            throw new IllegalArgumentException("Window callback may not be null");
        }
    }

    public boolean dispatchGenericMotionEvent(android.view.MotionEvent p2)
    {
        return this.d.dispatchGenericMotionEvent(p2);
    }

    public boolean dispatchKeyEvent(android.view.KeyEvent p2)
    {
        return this.d.dispatchKeyEvent(p2);
    }

    public boolean dispatchKeyShortcutEvent(android.view.KeyEvent p2)
    {
        return this.d.dispatchKeyShortcutEvent(p2);
    }

    public boolean dispatchPopulateAccessibilityEvent(android.view.accessibility.AccessibilityEvent p2)
    {
        return this.d.dispatchPopulateAccessibilityEvent(p2);
    }

    public boolean dispatchTouchEvent(android.view.MotionEvent p2)
    {
        return this.d.dispatchTouchEvent(p2);
    }

    public boolean dispatchTrackballEvent(android.view.MotionEvent p2)
    {
        return this.d.dispatchTrackballEvent(p2);
    }

    public void onActionModeFinished(android.view.ActionMode p2)
    {
        this.d.onActionModeFinished(p2);
        return;
    }

    public void onActionModeStarted(android.view.ActionMode p2)
    {
        this.d.onActionModeStarted(p2);
        return;
    }

    public void onAttachedToWindow()
    {
        this.d.onAttachedToWindow();
        return;
    }

    public void onContentChanged()
    {
        this.d.onContentChanged();
        return;
    }

    public boolean onCreatePanelMenu(int p2, android.view.Menu p3)
    {
        return this.d.onCreatePanelMenu(p2, p3);
    }

    public android.view.View onCreatePanelView(int p2)
    {
        return this.d.onCreatePanelView(p2);
    }

    public void onDetachedFromWindow()
    {
        this.d.onDetachedFromWindow();
        return;
    }

    public boolean onMenuItemSelected(int p2, android.view.MenuItem p3)
    {
        return this.d.onMenuItemSelected(p2, p3);
    }

    public boolean onMenuOpened(int p2, android.view.Menu p3)
    {
        return this.d.onMenuOpened(p2, p3);
    }

    public void onPanelClosed(int p2, android.view.Menu p3)
    {
        this.d.onPanelClosed(p2, p3);
        return;
    }

    public boolean onPreparePanel(int p2, android.view.View p3, android.view.Menu p4)
    {
        return this.d.onPreparePanel(p2, p3, p4);
    }

    public boolean onSearchRequested()
    {
        return this.d.onSearchRequested();
    }

    public boolean onSearchRequested(android.view.SearchEvent p2)
    {
        return this.d.onSearchRequested(p2);
    }

    public void onWindowAttributesChanged(android.view.WindowManager$LayoutParams p2)
    {
        this.d.onWindowAttributesChanged(p2);
        return;
    }

    public void onWindowFocusChanged(boolean p2)
    {
        this.d.onWindowFocusChanged(p2);
        return;
    }

    public android.view.ActionMode onWindowStartingActionMode(android.view.ActionMode$Callback p2)
    {
        return this.d.onWindowStartingActionMode(p2);
    }

    public android.view.ActionMode onWindowStartingActionMode(android.view.ActionMode$Callback p2, int p3)
    {
        return this.d.onWindowStartingActionMode(p2, p3);
    }
}
