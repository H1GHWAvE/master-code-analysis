package android.support.v7.internal.view.menu;
public class ListMenuItemView extends android.widget.LinearLayout implements android.support.v7.internal.view.menu.aa {
    private static final String a = "ListMenuItemView";
    private android.support.v7.internal.view.menu.m b;
    private android.widget.ImageView c;
    private android.widget.RadioButton d;
    private android.widget.TextView e;
    private android.widget.CheckBox f;
    private android.widget.TextView g;
    private android.graphics.drawable.Drawable h;
    private int i;
    private android.content.Context j;
    private boolean k;
    private int l;
    private android.content.Context m;
    private android.view.LayoutInflater n;
    private boolean o;

    public ListMenuItemView(android.content.Context p2, android.util.AttributeSet p3)
    {
        this(p2, p3, 0);
        return;
    }

    public ListMenuItemView(android.content.Context p5, android.util.AttributeSet p6, int p7)
    {
        this(p5, p6);
        this.m = p5;
        android.content.res.TypedArray v0_1 = p5.obtainStyledAttributes(p6, android.support.v7.a.n.MenuView, p7, 0);
        this.h = v0_1.getDrawable(android.support.v7.a.n.MenuView_android_itemBackground);
        this.i = v0_1.getResourceId(android.support.v7.a.n.MenuView_android_itemTextAppearance, -1);
        this.k = v0_1.getBoolean(android.support.v7.a.n.MenuView_preserveIconSpacing, 0);
        this.j = p5;
        v0_1.recycle();
        return;
    }

    private void c()
    {
        this.c = ((android.widget.ImageView) this.getInflater().inflate(android.support.v7.a.k.abc_list_menu_item_icon, this, 0));
        this.addView(this.c, 0);
        return;
    }

    private void d()
    {
        this.d = ((android.widget.RadioButton) this.getInflater().inflate(android.support.v7.a.k.abc_list_menu_item_radio, this, 0));
        this.addView(this.d);
        return;
    }

    private void e()
    {
        this.f = ((android.widget.CheckBox) this.getInflater().inflate(android.support.v7.a.k.abc_list_menu_item_checkbox, this, 0));
        this.addView(this.f);
        return;
    }

    private android.view.LayoutInflater getInflater()
    {
        if (this.n == null) {
            this.n = android.view.LayoutInflater.from(this.m);
        }
        return this.n;
    }

    public final void a(android.support.v7.internal.view.menu.m p6)
    {
        String v0_1;
        int v1 = 0;
        this.b = p6;
        this.l = 0;
        if (!p6.isVisible()) {
            v0_1 = 8;
        } else {
            v0_1 = 0;
        }
        this.setVisibility(v0_1);
        this.setTitle(p6.a(this));
        this.setCheckable(p6.isCheckable());
        String v0_4 = p6.d();
        p6.c();
        if ((v0_4 == null) || (!this.b.d())) {
            v1 = 8;
        }
        if (v1 == 0) {
            String v0_12;
            android.widget.TextView v2_1 = this.g;
            String v0_8 = this.b.c();
            if (v0_8 != null) {
                StringBuilder v3_1 = new StringBuilder(android.support.v7.internal.view.menu.m.l);
                switch (v0_8) {
                    case 8:
                        v3_1.append(android.support.v7.internal.view.menu.m.n);
                        break;
                    case 10:
                        v3_1.append(android.support.v7.internal.view.menu.m.m);
                        break;
                    case 32:
                        v3_1.append(android.support.v7.internal.view.menu.m.o);
                        break;
                    default:
                        v3_1.append(v0_8);
                }
                v0_12 = v3_1.toString();
            } else {
                v0_12 = "";
            }
            v2_1.setText(v0_12);
        }
        if (this.g.getVisibility() != v1) {
            this.g.setVisibility(v1);
        }
        this.setIcon(p6.getIcon());
        this.setEnabled(p6.isEnabled());
        return;
    }

    public final boolean a()
    {
        return 0;
    }

    public final boolean b()
    {
        return this.o;
    }

    public android.support.v7.internal.view.menu.m getItemData()
    {
        return this.b;
    }

    protected void onFinishInflate()
    {
        super.onFinishInflate();
        this.setBackgroundDrawable(this.h);
        this.e = ((android.widget.TextView) this.findViewById(android.support.v7.a.i.title));
        if (this.i != -1) {
            this.e.setTextAppearance(this.j, this.i);
        }
        this.g = ((android.widget.TextView) this.findViewById(android.support.v7.a.i.shortcut));
        return;
    }

    protected void onMeasure(int p4, int p5)
    {
        if ((this.c != null) && (this.k)) {
            int v1_0 = this.getLayoutParams();
            android.widget.LinearLayout$LayoutParams v0_4 = ((android.widget.LinearLayout$LayoutParams) this.c.getLayoutParams());
            if ((v1_0.height > 0) && (v0_4.width <= 0)) {
                v0_4.width = v1_0.height;
            }
        }
        super.onMeasure(p4, p5);
        return;
    }

    public void setCheckable(boolean p6)
    {
        if ((p6) || ((this.d != null) || (this.f != null))) {
            android.widget.RadioButton v2_1;
            android.widget.CheckBox v3;
            if (!this.b.e()) {
                if (this.f == null) {
                    this.e();
                }
                v3 = this.f;
                v2_1 = this.d;
            } else {
                if (this.d == null) {
                    this.d();
                }
                v3 = this.d;
                v2_1 = this.f;
            }
            if (!p6) {
                if (this.f != null) {
                    this.f.setVisibility(8);
                }
                if (this.d != null) {
                    this.d.setVisibility(8);
                }
            } else {
                int v0_14;
                v3.setChecked(this.b.isChecked());
                if (!p6) {
                    v0_14 = 8;
                } else {
                    v0_14 = 0;
                }
                if (v3.getVisibility() != v0_14) {
                    v3.setVisibility(v0_14);
                }
                if ((v2_1 != null) && (v2_1.getVisibility() != 8)) {
                    v2_1.setVisibility(8);
                }
            }
        }
        return;
    }

    public void setChecked(boolean p2)
    {
        android.widget.CheckBox v0_3;
        if (!this.b.e()) {
            if (this.f == null) {
                this.e();
            }
            v0_3 = this.f;
        } else {
            if (this.d == null) {
                this.d();
            }
            v0_3 = this.d;
        }
        v0_3.setChecked(p2);
        return;
    }

    public void setForceShowIcon(boolean p1)
    {
        this.o = p1;
        this.k = p1;
        return;
    }

    public void setIcon(android.graphics.drawable.Drawable p5)
    {
        if ((!this.b.g.m) && (!this.o)) {
            int v1_0 = 0;
        } else {
            v1_0 = 1;
        }
        if (((v1_0 != 0) || (this.k)) && ((this.c != null) || ((p5 != 0) || (this.k)))) {
            if (this.c == null) {
                this.c = ((android.widget.ImageView) this.getInflater().inflate(android.support.v7.a.k.abc_list_menu_item_icon, this, 0));
                this.addView(this.c, 0);
            }
            if ((p5 == 0) && (!this.k)) {
                this.c.setVisibility(8);
            } else {
                if (v1_0 == 0) {
                    p5 = 0;
                }
                this.c.setImageDrawable(p5);
                if (this.c.getVisibility() != 0) {
                    this.c.setVisibility(0);
                }
            }
        }
        return;
    }

    public final void setShortcut$25d965e(boolean p6)
    {
        if ((!p6) || (!this.b.d())) {
            int v1 = 8;
        } else {
            v1 = 0;
        }
        if (v1 == 0) {
            String v0_9;
            android.widget.TextView v2 = this.g;
            String v0_5 = this.b.c();
            if (v0_5 != null) {
                StringBuilder v3_1 = new StringBuilder(android.support.v7.internal.view.menu.m.l);
                switch (v0_5) {
                    case 8:
                        v3_1.append(android.support.v7.internal.view.menu.m.n);
                        break;
                    case 10:
                        v3_1.append(android.support.v7.internal.view.menu.m.m);
                        break;
                    case 32:
                        v3_1.append(android.support.v7.internal.view.menu.m.o);
                        break;
                    default:
                        v3_1.append(v0_5);
                }
                v0_9 = v3_1.toString();
            } else {
                v0_9 = "";
            }
            v2.setText(v0_9);
        }
        if (this.g.getVisibility() != v1) {
            this.g.setVisibility(v1);
        }
        return;
    }

    public void setTitle(CharSequence p3)
    {
        if (p3 == null) {
            if (this.e.getVisibility() != 8) {
                this.e.setVisibility(8);
            }
        } else {
            this.e.setText(p3);
            if (this.e.getVisibility() != 0) {
                this.e.setVisibility(0);
            }
        }
        return;
    }
}
