package android.support.v7.internal.widget;
public final class aa extends android.widget.PopupWindow {
    private static final String a = "AppCompatPopupWindow";
    private static final boolean b;
    private boolean c;

    static aa()
    {
        int v0_1;
        if (android.os.Build$VERSION.SDK_INT >= 21) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        android.support.v7.internal.widget.aa.b = v0_1;
        return;
    }

    public aa(android.content.Context p5, android.util.AttributeSet p6, int p7)
    {
        this(p5, p6, p7);
        Exception v0_1 = android.support.v7.internal.widget.ax.a(p5, p6, android.support.v7.a.n.PopupWindow, p7);
        if (v0_1.e(android.support.v7.a.n.PopupWindow_overlapAnchor)) {
            String v1_3 = v0_1.a(android.support.v7.a.n.PopupWindow_overlapAnchor, 0);
            if (!android.support.v7.internal.widget.aa.b) {
                android.support.v4.widget.bo.a(this, v1_3);
            } else {
                this.c = v1_3;
            }
        }
        this.setBackgroundDrawable(v0_1.a(android.support.v7.a.n.PopupWindow_android_popupBackground));
        v0_1.a.recycle();
        if (android.os.Build$VERSION.SDK_INT < 14) {
            try {
                String v1_8 = android.widget.PopupWindow.getDeclaredField("mAnchor");
                v1_8.setAccessible(1);
                String v2_3 = android.widget.PopupWindow.getDeclaredField("mOnScrollChangedListener");
                v2_3.setAccessible(1);
                v2_3.set(this, new android.support.v7.internal.widget.ab(v1_8, this, ((android.view.ViewTreeObserver$OnScrollChangedListener) v2_3.get(this))));
            } catch (Exception v0_10) {
                android.util.Log.d("AppCompatPopupWindow", "Exception while installing workaround OnScrollChangedListener", v0_10);
            }
        }
        return;
    }

    private static void a(android.widget.PopupWindow p4)
    {
        try {
            String v1_1 = android.widget.PopupWindow.getDeclaredField("mAnchor");
            v1_1.setAccessible(1);
            String v2_1 = android.widget.PopupWindow.getDeclaredField("mOnScrollChangedListener");
            v2_1.setAccessible(1);
            v2_1.set(p4, new android.support.v7.internal.widget.ab(v1_1, p4, ((android.view.ViewTreeObserver$OnScrollChangedListener) v2_1.get(p4))));
        } catch (Exception v0_6) {
            android.util.Log.d("AppCompatPopupWindow", "Exception while installing workaround OnScrollChangedListener", v0_6);
        }
        return;
    }

    private void a(boolean p2)
    {
        if (!android.support.v7.internal.widget.aa.b) {
            android.support.v4.widget.bo.a(this, p2);
        } else {
            this.c = p2;
        }
        return;
    }

    private boolean a()
    {
        boolean v0_1;
        if (!android.support.v7.internal.widget.aa.b) {
            v0_1 = android.support.v4.widget.bo.a(this);
        } else {
            v0_1 = this.c;
        }
        return v0_1;
    }

    public final void showAsDropDown(android.view.View p2, int p3, int p4)
    {
        if ((android.support.v7.internal.widget.aa.b) && (this.c)) {
            p4 -= p2.getHeight();
        }
        super.showAsDropDown(p2, p3, p4);
        return;
    }

    public final void showAsDropDown(android.view.View p2, int p3, int p4, int p5)
    {
        if ((android.support.v7.internal.widget.aa.b) && (this.c)) {
            p4 -= p2.getHeight();
        }
        super.showAsDropDown(p2, p3, p4, p5);
        return;
    }

    public final void update(android.view.View p7, int p8, int p9, int p10, int p11)
    {
        if ((!android.support.v7.internal.widget.aa.b) || (!this.c)) {
            int v3 = p9;
        } else {
            v3 = (p9 - p7.getHeight());
        }
        super.update(p7, p8, v3, p10, p11);
        return;
    }
}
