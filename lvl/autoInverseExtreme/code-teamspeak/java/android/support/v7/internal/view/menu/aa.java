package android.support.v7.internal.view.menu;
public interface aa {

    public abstract void a();

    public abstract boolean a();

    public abstract boolean b();

    public abstract android.support.v7.internal.view.menu.m getItemData();

    public abstract void setCheckable();

    public abstract void setChecked();

    public abstract void setEnabled();

    public abstract void setIcon();

    public abstract void setShortcut$25d965e();

    public abstract void setTitle();
}
