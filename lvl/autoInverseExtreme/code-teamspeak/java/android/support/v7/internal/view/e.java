package android.support.v7.internal.view;
public final class e implements android.support.v7.c.b {
    final android.view.ActionMode$Callback a;
    final android.content.Context b;
    final java.util.ArrayList c;
    final android.support.v4.n.v d;

    public e(android.content.Context p2, android.view.ActionMode$Callback p3)
    {
        this.b = p2;
        this.a = p3;
        this.c = new java.util.ArrayList();
        this.d = new android.support.v4.n.v();
        return;
    }

    private android.view.Menu a(android.view.Menu p3)
    {
        android.view.Menu v0_2 = ((android.view.Menu) this.d.get(p3));
        if (v0_2 == null) {
            v0_2 = android.support.v7.internal.view.menu.ab.a(this.b, ((android.support.v4.g.a.a) p3));
            this.d.put(p3, v0_2);
        }
        return v0_2;
    }

    public final void a(android.support.v7.c.a p3)
    {
        this.a.onDestroyActionMode(this.b(p3));
        return;
    }

    public final boolean a(android.support.v7.c.a p4, android.view.Menu p5)
    {
        return this.a.onCreateActionMode(this.b(p4), this.a(p5));
    }

    public final boolean a(android.support.v7.c.a p4, android.view.MenuItem p5)
    {
        return this.a.onActionItemClicked(this.b(p4), android.support.v7.internal.view.menu.ab.a(this.b, ((android.support.v4.g.a.b) p5)));
    }

    public final android.view.ActionMode b(android.support.v7.c.a p5)
    {
        int v2 = this.c.size();
        int v1_1 = 0;
        while (v1_1 < v2) {
            int v0_2 = ((android.support.v7.internal.view.d) this.c.get(v1_1));
            if ((v0_2 == 0) || (v0_2.b != p5)) {
                v1_1++;
            }
            return v0_2;
        }
        v0_2 = new android.support.v7.internal.view.d(this.b, p5);
        this.c.add(v0_2);
        return v0_2;
    }

    public final boolean b(android.support.v7.c.a p4, android.view.Menu p5)
    {
        return this.a.onPrepareActionMode(this.b(p4), this.a(p5));
    }
}
