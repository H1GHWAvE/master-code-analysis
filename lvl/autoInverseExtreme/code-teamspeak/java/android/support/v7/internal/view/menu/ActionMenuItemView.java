package android.support.v7.internal.view.menu;
public class ActionMenuItemView extends android.support.v7.widget.ai implements android.support.v7.internal.view.menu.aa, android.support.v7.widget.k, android.view.View$OnClickListener, android.view.View$OnLongClickListener {
    private static final String a = "ActionMenuItemView";
    private static final int l = 32;
    private android.support.v7.internal.view.menu.m b;
    private CharSequence c;
    private android.graphics.drawable.Drawable d;
    private android.support.v7.internal.view.menu.k e;
    private android.support.v7.widget.as f;
    private android.support.v7.internal.view.menu.c g;
    private boolean h;
    private boolean i;
    private int j;
    private int k;
    private int m;

    public ActionMenuItemView(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    public ActionMenuItemView(android.content.Context p2, android.util.AttributeSet p3)
    {
        this(p2, p3, 0);
        return;
    }

    public ActionMenuItemView(android.content.Context p5, android.util.AttributeSet p6, int p7)
    {
        this(p5, p6, p7);
        int v0_0 = p5.getResources();
        this.h = v0_0.getBoolean(android.support.v7.a.e.abc_config_allowActionMenuItemTextWithIcon);
        int v1_3 = p5.obtainStyledAttributes(p6, android.support.v7.a.n.ActionMenuItemView, p7, 0);
        this.j = v1_3.getDimensionPixelSize(android.support.v7.a.n.ActionMenuItemView_android_minWidth, 0);
        v1_3.recycle();
        this.m = ((int) ((v0_0.getDisplayMetrics().density * 1107296256) + 1056964608));
        this.setOnClickListener(this);
        this.setOnLongClickListener(this);
        this.k = -1;
        return;
    }

    static synthetic android.support.v7.internal.view.menu.c a(android.support.v7.internal.view.menu.ActionMenuItemView p1)
    {
        return p1.g;
    }

    static synthetic android.support.v7.internal.view.menu.k b(android.support.v7.internal.view.menu.ActionMenuItemView p1)
    {
        return p1.e;
    }

    static synthetic android.support.v7.internal.view.menu.m c(android.support.v7.internal.view.menu.ActionMenuItemView p1)
    {
        return p1.b;
    }

    private void f()
    {
        int v0_2;
        int v2 = 0;
        if (android.text.TextUtils.isEmpty(this.c)) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        if (this.d == null) {
            v2 = 1;
        } else {
            boolean v3_4;
            if ((this.b.h & 4) != 4) {
                v3_4 = 0;
            } else {
                v3_4 = 1;
            }
            if ((v3_4) && ((this.h) || (this.i))) {
            }
        }
        int v0_4;
        if ((v0_2 & v2) == 0) {
            v0_4 = 0;
        } else {
            v0_4 = this.c;
        }
        this.setText(v0_4);
        return;
    }

    public final void a(android.support.v7.internal.view.menu.m p2)
    {
        android.support.v7.internal.view.menu.b v0_4;
        this.b = p2;
        this.setIcon(p2.getIcon());
        this.setTitle(p2.a(this));
        this.setId(p2.getItemId());
        if (!p2.isVisible()) {
            v0_4 = 8;
        } else {
            v0_4 = 0;
        }
        this.setVisibility(v0_4);
        this.setEnabled(p2.isEnabled());
        if ((p2.hasSubMenu()) && (this.f == null)) {
            this.f = new android.support.v7.internal.view.menu.b(this);
        }
        return;
    }

    public final boolean a()
    {
        return 1;
    }

    public final boolean b()
    {
        return 1;
    }

    public final boolean c()
    {
        int v0_2;
        if (android.text.TextUtils.isEmpty(this.getText())) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final boolean d()
    {
        if ((!this.c()) || (this.b.getIcon() != null)) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final boolean e()
    {
        return this.c();
    }

    public android.support.v7.internal.view.menu.m getItemData()
    {
        return this.b;
    }

    public void onClick(android.view.View p3)
    {
        if (this.e != null) {
            this.e.a(this.b);
        }
        return;
    }

    public void onConfigurationChanged(android.content.res.Configuration p3)
    {
        if (android.os.Build$VERSION.SDK_INT >= 8) {
            super.onConfigurationChanged(p3);
        }
        this.h = this.getContext().getResources().getBoolean(android.support.v7.a.e.abc_config_allowActionMenuItemTextWithIcon);
        this.f();
        return;
    }

    public boolean onLongClick(android.view.View p10)
    {
        int v0_6;
        if (!this.c()) {
            int v3_0 = new int[2];
            int v4_1 = new android.graphics.Rect();
            this.getLocationOnScreen(v3_0);
            this.getWindowVisibleDisplayFrame(v4_1);
            android.widget.Toast v5_0 = this.getContext();
            int v0_2 = this.getWidth();
            int v6 = this.getHeight();
            int v7_1 = (v3_0[1] + (v6 / 2));
            int v0_4 = ((v0_2 / 2) + v3_0[0]);
            if (android.support.v4.view.cx.f(p10) == 0) {
                v0_4 = (v5_0.getResources().getDisplayMetrics().widthPixels - v0_4);
            }
            android.widget.Toast v5_1 = android.widget.Toast.makeText(v5_0, this.b.getTitle(), 0);
            if (v7_1 >= v4_1.height()) {
                v5_1.setGravity(81, 0, v6);
            } else {
                v5_1.setGravity(8388661, v0_4, ((v3_0[1] + v6) - v4_1.top));
            }
            v5_1.show();
            v0_6 = 1;
        } else {
            v0_6 = 0;
        }
        return v0_6;
    }

    protected void onMeasure(int p7, int p8)
    {
        int v1_0 = this.c();
        if ((v1_0 != 0) && (this.k >= 0)) {
            super.setPadding(this.k, this.getPaddingTop(), this.getPaddingRight(), this.getPaddingBottom());
        }
        int v0_3;
        super.onMeasure(p7, p8);
        int v2_1 = android.view.View$MeasureSpec.getMode(p7);
        int v0_2 = android.view.View$MeasureSpec.getSize(p7);
        int v3_1 = this.getMeasuredWidth();
        if (v2_1 != -2147483648) {
            v0_3 = this.j;
        } else {
            v0_3 = Math.min(v0_2, this.j);
        }
        if ((v2_1 != 1073741824) && ((this.j > 0) && (v3_1 < v0_3))) {
            super.onMeasure(android.view.View$MeasureSpec.makeMeasureSpec(v0_3, 1073741824), p8);
        }
        if ((v1_0 == 0) && (this.d != null)) {
            super.setPadding(((this.getMeasuredWidth() - this.d.getBounds().width()) / 2), this.getPaddingTop(), this.getPaddingRight(), this.getPaddingBottom());
        }
        return;
    }

    public boolean onTouchEvent(android.view.MotionEvent p2)
    {
        if ((!this.b.hasSubMenu()) || ((this.f == null) || (!this.f.onTouch(this, p2)))) {
            int v0_5 = super.onTouchEvent(p2);
        } else {
            v0_5 = 1;
        }
        return v0_5;
    }

    public void setCheckable(boolean p1)
    {
        return;
    }

    public void setChecked(boolean p1)
    {
        return;
    }

    public void setExpandedFormat(boolean p2)
    {
        if (this.i != p2) {
            this.i = p2;
            if (this.b != null) {
                this.b.g.g();
            }
        }
        return;
    }

    public void setIcon(android.graphics.drawable.Drawable p6)
    {
        this.d = p6;
        if (p6 != null) {
            int v1_0 = p6.getIntrinsicWidth();
            int v0_0 = p6.getIntrinsicHeight();
            if (v1_0 > this.m) {
                float v2_3 = (((float) this.m) / ((float) v1_0));
                v1_0 = this.m;
                v0_0 = ((int) (((float) v0_0) * v2_3));
            }
            if (v0_0 > this.m) {
                float v2_7 = (((float) this.m) / ((float) v0_0));
                v0_0 = this.m;
                v1_0 = ((int) (((float) v1_0) * v2_7));
            }
            p6.setBounds(0, 0, v1_0, v0_0);
        }
        this.setCompoundDrawables(p6, 0, 0, 0);
        this.f();
        return;
    }

    public void setItemInvoker(android.support.v7.internal.view.menu.k p1)
    {
        this.e = p1;
        return;
    }

    public void setPadding(int p1, int p2, int p3, int p4)
    {
        this.k = p1;
        super.setPadding(p1, p2, p3, p4);
        return;
    }

    public void setPopupCallback(android.support.v7.internal.view.menu.c p1)
    {
        this.g = p1;
        return;
    }

    public final void setShortcut$25d965e(boolean p1)
    {
        return;
    }

    public void setTitle(CharSequence p2)
    {
        this.c = p2;
        this.setContentDescription(this.c);
        this.f();
        return;
    }
}
