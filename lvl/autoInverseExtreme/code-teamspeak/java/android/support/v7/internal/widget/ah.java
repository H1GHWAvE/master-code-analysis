package android.support.v7.internal.widget;
public class ah extends android.widget.ListView {
    public static final int a = 255;
    public static final int b = 255;
    private static final int[] h;
    final android.graphics.Rect c;
    int d;
    int e;
    int f;
    int g;
    private reflect.Field i;
    private android.support.v7.internal.widget.ai j;

    static ah()
    {
        int[] v0_1 = new int[1];
        v0_1[0] = 0;
        android.support.v7.internal.widget.ah.h = v0_1;
        return;
    }

    private ah(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    private ah(android.content.Context p2, byte p3)
    {
        this(p2, 0);
        return;
    }

    public ah(android.content.Context p3, int p4)
    {
        this(p3, 0, p4);
        this.c = new android.graphics.Rect();
        this.d = 0;
        this.e = 0;
        this.f = 0;
        this.g = 0;
        try {
            this.i = android.widget.AbsListView.getDeclaredField("mIsChildViewEnabled");
            this.i.setAccessible(1);
        } catch (NoSuchFieldException v0_6) {
            v0_6.printStackTrace();
        }
        return;
    }

    private void a(int p11, android.view.View p12)
    {
        float v3_0;
        int v0 = 1;
        android.graphics.drawable.Drawable v4 = this.getSelector();
        if ((v4 == null) || (p11 == -1)) {
            v3_0 = 0;
        } else {
            v3_0 = 1;
        }
        if (v3_0 != 0) {
            v4.setVisible(0, 0);
        }
        Boolean v2_0 = this.c;
        v2_0.set(p12.getLeft(), p12.getTop(), p12.getRight(), p12.getBottom());
        v2_0.left = (v2_0.left - this.d);
        v2_0.top = (v2_0.top - this.e);
        v2_0.right = (v2_0.right + this.f);
        v2_0.bottom = (v2_0.bottom + this.g);
        try {
            Boolean v2_2 = this.i.getBoolean(this);
        } catch (Boolean v2_5) {
            v2_5.printStackTrace();
            if (v3_0 != 0) {
                Boolean v2_6 = this.c;
                float v3_1 = v2_6.exactCenterX();
                Boolean v2_7 = v2_6.exactCenterY();
                if (this.getVisibility() != 0) {
                    v0 = 0;
                }
                v4.setVisible(v0, 0);
                android.support.v4.e.a.a.a(v4, v3_1, v2_7);
            }
            return;
        }
        if (p12.isEnabled() == v2_2) {
        } else {
            Boolean v2_3;
            if (v2_2 != null) {
                v2_3 = 0;
            } else {
                v2_3 = 1;
            }
            this.i.set(this, Boolean.valueOf(v2_3));
            if (p11 == -1) {
            } else {
                this.refreshDrawableState();
            }
        }
    }

    private void a(android.graphics.Canvas p3)
    {
        if (!this.c.isEmpty()) {
            android.graphics.drawable.Drawable v0_2 = this.getSelector();
            if (v0_2 != null) {
                v0_2.setBounds(this.c);
                v0_2.draw(p3);
            }
        }
        return;
    }

    private void b()
    {
        android.graphics.drawable.Drawable v1 = this.getSelector();
        if (v1 != null) {
            if ((!this.a()) || (!this.isPressed())) {
                int[] v0_2 = 0;
            } else {
                v0_2 = 1;
            }
            if (v0_2 != null) {
                v1.setState(this.getDrawableState());
            }
        }
        return;
    }

    private void b(int p6, android.view.View p7)
    {
        int v0_0 = this.c;
        v0_0.set(p7.getLeft(), p7.getTop(), p7.getRight(), p7.getBottom());
        v0_0.left = (v0_0.left - this.d);
        v0_0.top = (v0_0.top - this.e);
        v0_0.right = (v0_0.right + this.f);
        v0_0.bottom = (v0_0.bottom + this.g);
        try {
            int v0_2 = this.i.getBoolean(this);
        } catch (int v0_6) {
            v0_6.printStackTrace();
            return;
        }
        if (p7.isEnabled() == v0_2) {
            return;
        } else {
            int v0_3;
            if (v0_2 != 0) {
                v0_3 = 0;
            } else {
                v0_3 = 1;
            }
            this.i.set(this, Boolean.valueOf(v0_3));
            if (p6 == -1) {
                return;
            } else {
                this.refreshDrawableState();
                return;
            }
        }
    }

    private boolean c()
    {
        if ((!this.a()) || (!this.isPressed())) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final int a(int p12, int p13)
    {
        int v2_0 = this.getListPaddingTop();
        int v3_0 = this.getListPaddingBottom();
        this.getListPaddingLeft();
        this.getListPaddingRight();
        int v0 = this.getDividerHeight();
        int v4_0 = this.getDivider();
        android.widget.ListAdapter v8 = this.getAdapter();
        if (v8 != null) {
            int v3_1 = (v3_0 + v2_0);
            if ((v0 <= 0) || (v4_0 == 0)) {
                v0 = 0;
            }
            int v9 = v8.getCount();
            int v7 = 0;
            int v4_1 = 0;
            android.view.View v6 = 0;
            while (v7 < v9) {
                int v2_2;
                int v2_1 = v8.getItemViewType(v7);
                if (v2_1 == v4_1) {
                    v2_2 = v6;
                } else {
                    v4_1 = v2_1;
                    v2_2 = 0;
                }
                int v2_4;
                v6 = v8.getView(v7, v2_2, this);
                int v2_3 = v6.getLayoutParams();
                if ((v2_3 == 0) || (v2_3.height <= 0)) {
                    v2_4 = android.view.View$MeasureSpec.makeMeasureSpec(0, 0);
                } else {
                    v2_4 = android.view.View$MeasureSpec.makeMeasureSpec(v2_3.height, 1073741824);
                }
                int v2_6;
                v6.measure(p12, v2_4);
                if (v7 <= 0) {
                    v2_6 = v3_1;
                } else {
                    v2_6 = (v3_1 + v0);
                }
                v3_1 = (v6.getMeasuredHeight() + v2_6);
                if (v3_1 < p13) {
                    v7++;
                }
            }
            p13 = v3_1;
        } else {
            p13 = (v2_0 + v3_0);
        }
        return p13;
    }

    public final int a(int p5, boolean p6)
    {
        android.widget.ListAdapter v1 = this.getAdapter();
        if ((v1 != null) && (!this.isInTouchMode())) {
            int v2_1 = v1.getCount();
            if (this.getAdapter().areAllItemsEnabled()) {
                if ((p5 < 0) || (p5 >= v2_1)) {
                    p5 = -1;
                }
            } else {
                if (!p6) {
                    p5 = Math.min(p5, (v2_1 - 1));
                    while ((p5 >= 0) && (!v1.isEnabled(p5))) {
                        p5--;
                    }
                } else {
                    p5 = Math.max(0, p5);
                    while ((p5 < v2_1) && (!v1.isEnabled(p5))) {
                        p5++;
                    }
                }
                if ((p5 < 0) || (p5 >= v2_1)) {
                    p5 = -1;
                }
            }
        } else {
            p5 = -1;
        }
        return p5;
    }

    public final void a(int p11, android.view.View p12, float p13, float p14)
    {
        float v3_0;
        int v0_0 = 1;
        android.graphics.drawable.Drawable v4 = this.getSelector();
        if ((v4 == null) || (p11 == -1)) {
            v3_0 = 0;
        } else {
            v3_0 = 1;
        }
        if (v3_0 != 0) {
            v4.setVisible(0, 0);
        }
        Boolean v2_0 = this.c;
        v2_0.set(p12.getLeft(), p12.getTop(), p12.getRight(), p12.getBottom());
        v2_0.left = (v2_0.left - this.d);
        v2_0.top = (v2_0.top - this.e);
        v2_0.right = (v2_0.right + this.f);
        v2_0.bottom = (v2_0.bottom + this.g);
        try {
            Boolean v2_2 = this.i.getBoolean(this);
        } catch (Boolean v2_5) {
            v2_5.printStackTrace();
            if (v3_0 != 0) {
                Boolean v2_6 = this.c;
                float v3_1 = v2_6.exactCenterX();
                Boolean v2_7 = v2_6.exactCenterY();
                if (this.getVisibility() != 0) {
                    v0_0 = 0;
                }
                v4.setVisible(v0_0, 0);
                android.support.v4.e.a.a.a(v4, v3_1, v2_7);
            }
            int v0_1 = this.getSelector();
            if ((v0_1 != 0) && (p11 != -1)) {
                android.support.v4.e.a.a.a(v0_1, p13, p14);
            }
            return;
        }
        if (p12.isEnabled() == v2_2) {
        } else {
            Boolean v2_3;
            if (v2_2 != null) {
                v2_3 = 0;
            } else {
                v2_3 = 1;
            }
            this.i.set(this, Boolean.valueOf(v2_3));
            if (p11 == -1) {
            } else {
                this.refreshDrawableState();
            }
        }
    }

    public boolean a()
    {
        return 0;
    }

    protected void dispatchDraw(android.graphics.Canvas p3)
    {
        if (!this.c.isEmpty()) {
            android.graphics.drawable.Drawable v0_2 = this.getSelector();
            if (v0_2 != null) {
                v0_2.setBounds(this.c);
                v0_2.draw(p3);
            }
        }
        super.dispatchDraw(p3);
        return;
    }

    public void drawableStateChanged()
    {
        int[] v0_0 = 1;
        super.drawableStateChanged();
        this.setSelectorEnabled(1);
        android.graphics.drawable.Drawable v1 = this.getSelector();
        if (v1 != null) {
            if ((!this.a()) || (!this.isPressed())) {
                v0_0 = 0;
            }
            if (v0_0 != null) {
                v1.setState(this.getDrawableState());
            }
        }
        return;
    }

    public void setSelector(android.graphics.drawable.Drawable p3)
    {
        int v0_0;
        if (p3 == null) {
            v0_0 = 0;
        } else {
            v0_0 = new android.support.v7.internal.widget.ai(p3);
        }
        this.j = v0_0;
        super.setSelector(this.j);
        int v0_4 = new android.graphics.Rect();
        if (p3 != null) {
            p3.getPadding(v0_4);
        }
        this.d = v0_4.left;
        this.e = v0_4.top;
        this.f = v0_4.right;
        this.g = v0_4.bottom;
        return;
    }

    public void setSelectorEnabled(boolean p2)
    {
        if (this.j != null) {
            this.j.a = p2;
        }
        return;
    }
}
