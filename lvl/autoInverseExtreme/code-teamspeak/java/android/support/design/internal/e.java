package android.support.design.internal;
public final class e extends android.support.v7.internal.view.menu.ad {

    public e(android.content.Context p1, android.support.design.internal.a p2, android.support.v7.internal.view.menu.m p3)
    {
        this(p1, p2, p3);
        return;
    }

    private void l()
    {
        ((android.support.v7.internal.view.menu.i) this.p).c(1);
        return;
    }

    public final android.view.MenuItem add(int p2)
    {
        android.view.MenuItem v0 = super.add(p2);
        this.l();
        return v0;
    }

    public final android.view.MenuItem add(int p2, int p3, int p4, int p5)
    {
        android.view.MenuItem v0 = super.add(p2, p3, p4, p5);
        this.l();
        return v0;
    }

    public final android.view.MenuItem add(int p2, int p3, int p4, CharSequence p5)
    {
        android.view.MenuItem v0 = super.add(p2, p3, p4, p5);
        this.l();
        return v0;
    }

    public final android.view.MenuItem add(CharSequence p2)
    {
        android.view.MenuItem v0 = super.add(p2);
        this.l();
        return v0;
    }
}
