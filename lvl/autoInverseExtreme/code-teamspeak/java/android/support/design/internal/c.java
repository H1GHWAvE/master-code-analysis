package android.support.design.internal;
public final class c extends android.widget.BaseAdapter {
    private static final String e = "android:menu:checked";
    private static final int f = 0;
    private static final int g = 1;
    private static final int h = 2;
    final java.util.ArrayList a;
    android.support.v7.internal.view.menu.m b;
    boolean c;
    final synthetic android.support.design.internal.b d;
    private android.graphics.drawable.ColorDrawable i;

    c(android.support.design.internal.b p2)
    {
        this.d = p2;
        this.a = new java.util.ArrayList();
        this.a();
        return;
    }

    private void a(int p4, int p5)
    {
        while (p4 < p5) {
            android.support.v7.internal.view.menu.m v0_3 = ((android.support.design.internal.d) this.a.get(p4)).a;
            if (v0_3.getIcon() == null) {
                if (this.i == null) {
                    this.i = new android.graphics.drawable.ColorDrawable(17170445);
                }
                v0_3.setIcon(this.i);
            }
            p4++;
        }
        return;
    }

    private void a(android.os.Bundle p6)
    {
        int v1 = p6.getInt("android:menu:checked", 0);
        if (v1 != 0) {
            this.c = 1;
            java.util.Iterator v2 = this.a.iterator();
            while (v2.hasNext()) {
                android.support.v7.internal.view.menu.m v0_6 = ((android.support.design.internal.d) v2.next()).a;
                if ((v0_6 != null) && (v0_6.getItemId() == v1)) {
                    this.a(v0_6);
                    break;
                }
            }
            this.c = 0;
            this.a();
        }
        return;
    }

    private void a(boolean p1)
    {
        this.c = p1;
        return;
    }

    private android.os.Bundle b()
    {
        android.os.Bundle v0_1 = new android.os.Bundle();
        if (this.b != null) {
            v0_1.putInt("android:menu:checked", this.b.getItemId());
        }
        return v0_1;
    }

    public final android.support.design.internal.d a(int p2)
    {
        return ((android.support.design.internal.d) this.a.get(p2));
    }

    final void a()
    {
        if (!this.c) {
            this.c = 1;
            this.a.clear();
            int v6_0 = -1;
            int v10 = android.support.design.internal.b.g(this.d).h().size();
            int v9 = 0;
            int v4_0 = 0;
            int v5_0 = 0;
            while (v9 < v10) {
                int v0_9 = ((android.support.v7.internal.view.menu.m) android.support.design.internal.b.g(this.d).h().get(v9));
                if (v0_9.isChecked()) {
                    this.a(v0_9);
                }
                if (v0_9.isCheckable()) {
                    v0_9.a(0);
                }
                android.support.design.internal.d v1_10;
                int v2_4;
                int v0_11;
                if (!v0_9.hasSubMenu()) {
                    android.support.design.internal.d v1_6;
                    int v2_0;
                    int v7_0 = v0_9.getGroupId();
                    if (v7_0 == v6_0) {
                        if ((v4_0 != 0) || (v0_9.getIcon() == null)) {
                            v1_6 = v4_0;
                            v2_0 = v5_0;
                        } else {
                            this.a(v5_0, this.a.size());
                            v1_6 = 1;
                            v2_0 = v5_0;
                        }
                    } else {
                        v5_0 = this.a.size();
                        if (v0_9.getIcon() == null) {
                            v4_0 = 0;
                        } else {
                            v4_0 = 1;
                        }
                        if (v9 == 0) {
                        } else {
                            int v5_1 = (v5_0 + 1);
                            this.a.add(android.support.design.internal.d.a(android.support.design.internal.b.h(this.d), android.support.design.internal.b.h(this.d)));
                            v1_6 = v4_0;
                            v2_0 = v5_1;
                        }
                    }
                    if ((v1_6 != null) && (v0_9.getIcon() == null)) {
                        v0_9.setIcon(17170445);
                    }
                    this.a.add(android.support.design.internal.d.a(v0_9));
                    v0_11 = v1_6;
                    v1_10 = v2_0;
                    v2_4 = v7_0;
                } else {
                    android.view.SubMenu v11 = v0_9.getSubMenu();
                    if (v11.hasVisibleItems()) {
                        if (v9 != 0) {
                            this.a.add(android.support.design.internal.d.a(android.support.design.internal.b.h(this.d), 0));
                        }
                        this.a.add(android.support.design.internal.d.a(v0_9));
                        int v12 = this.a.size();
                        int v13 = v11.size();
                        int v7_1 = 0;
                        int v2_9 = 0;
                        while (v7_1 < v13) {
                            android.support.design.internal.d v1_16 = ((android.support.v7.internal.view.menu.m) v11.getItem(v7_1));
                            if (v1_16.isVisible()) {
                                if ((v2_9 == 0) && (v1_16.getIcon() != null)) {
                                    v2_9 = 1;
                                }
                                if (v1_16.isCheckable()) {
                                    v1_16.a(0);
                                }
                                if (v0_9.isChecked()) {
                                    this.a(v0_9);
                                }
                                this.a.add(android.support.design.internal.d.a(v1_16));
                            }
                            v7_1++;
                        }
                        if (v2_9 != 0) {
                            this.a(v12, this.a.size());
                        }
                    }
                    v0_11 = v4_0;
                    v1_10 = v5_0;
                    v2_4 = v6_0;
                }
                v9++;
                v5_0 = v1_10;
                v6_0 = v2_4;
                v4_0 = v0_11;
            }
            this.c = 0;
        }
        return;
    }

    public final void a(android.support.v7.internal.view.menu.m p3)
    {
        if ((this.b != p3) && (p3.isCheckable())) {
            if (this.b != null) {
                this.b.setChecked(0);
            }
            this.b = p3;
            p3.setChecked(1);
        }
        return;
    }

    public final boolean areAllItemsEnabled()
    {
        return 0;
    }

    public final int getCount()
    {
        return this.a.size();
    }

    public final synthetic Object getItem(int p2)
    {
        return this.a(p2);
    }

    public final long getItemId(int p3)
    {
        return ((long) p3);
    }

    public final int getItemViewType(int p5)
    {
        boolean v2_1;
        int v0 = 1;
        android.support.design.internal.d v3 = this.a(p5);
        if (v3.a != null) {
            v2_1 = 0;
        } else {
            v2_1 = 1;
        }
        if (!v2_1) {
            if (!v3.a.hasSubMenu()) {
                v0 = 0;
            }
        } else {
            v0 = 2;
        }
        return v0;
    }

    public final android.view.View getView(int p6, android.view.View p7, android.view.ViewGroup p8)
    {
        int v1_2;
        android.support.design.internal.d v3 = this.a(p6);
        switch (this.getItemViewType(p6)) {
            case 0:
                if (p7 != null) {
                    v1_2 = p7;
                } else {
                    v1_2 = android.support.design.internal.b.a(this.d).inflate(android.support.design.k.design_navigation_item, p8, 0);
                }
                android.widget.TextView v0_11 = ((android.support.design.internal.NavigationMenuItemView) v1_2);
                v0_11.setIconTintList(android.support.design.internal.b.b(this.d));
                if (android.support.design.internal.b.c(this.d)) {
                    v0_11.setTextAppearance(v0_11.getContext(), android.support.design.internal.b.d(this.d));
                }
                if (android.support.design.internal.b.e(this.d) != null) {
                    v0_11.setTextColor(android.support.design.internal.b.e(this.d));
                }
                android.support.v7.internal.view.menu.m v2_14;
                if (android.support.design.internal.b.f(this.d) == null) {
                    v2_14 = 0;
                } else {
                    v2_14 = android.support.design.internal.b.f(this.d).getConstantState().newDrawable();
                }
                v0_11.setBackgroundDrawable(v2_14);
                v0_11.a(v3.a);
                break;
            case 1:
                if (p7 != null) {
                    v1_2 = p7;
                } else {
                    v1_2 = android.support.design.internal.b.a(this.d).inflate(android.support.design.k.design_navigation_item_subheader, p8, 0);
                }
                ((android.widget.TextView) v1_2).setText(v3.a.getTitle());
                break;
            case 2:
                if (p7 == null) {
                    p7 = android.support.design.internal.b.a(this.d).inflate(android.support.design.k.design_navigation_item_separator, p8, 0);
                }
                p7.setPadding(0, v3.b, 0, v3.c);
                v1_2 = p7;
                break;
            default:
        }
        return v1_2;
    }

    public final int getViewTypeCount()
    {
        return 3;
    }

    public final boolean isEnabled(int p3)
    {
        int v0_3;
        int v0_0 = this.a(p3);
        if ((v0_0.a == null) || ((v0_0.a.hasSubMenu()) || (!v0_0.a.isEnabled()))) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final void notifyDataSetChanged()
    {
        this.a();
        super.notifyDataSetChanged();
        return;
    }
}
