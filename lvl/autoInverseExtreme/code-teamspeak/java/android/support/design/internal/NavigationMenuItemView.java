package android.support.design.internal;
public class NavigationMenuItemView extends android.widget.TextView implements android.support.v7.internal.view.menu.aa {
    private static final int[] a;
    private int b;
    private android.support.v7.internal.view.menu.m c;
    private android.content.res.ColorStateList d;

    static NavigationMenuItemView()
    {
        int[] v0_1 = new int[1];
        v0_1[0] = 16842912;
        android.support.design.internal.NavigationMenuItemView.a = v0_1;
        return;
    }

    public NavigationMenuItemView(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    public NavigationMenuItemView(android.content.Context p2, android.util.AttributeSet p3)
    {
        this(p2, p3, 0);
        return;
    }

    public NavigationMenuItemView(android.content.Context p3, android.util.AttributeSet p4, int p5)
    {
        this(p3, p4, p5);
        this.b = p3.getResources().getDimensionPixelSize(android.support.design.g.design_navigation_icon_size);
        return;
    }

    private android.graphics.drawable.StateListDrawable c()
    {
        int v0_3;
        int[] v1_1 = new android.util.TypedValue();
        if (!this.getContext().getTheme().resolveAttribute(android.support.design.d.colorControlHighlight, v1_1, 1)) {
            v0_3 = 0;
        } else {
            v0_3 = new android.graphics.drawable.StateListDrawable();
            v0_3.addState(android.support.design.internal.NavigationMenuItemView.a, new android.graphics.drawable.ColorDrawable(v1_1.data));
            v0_3.addState(android.support.design.internal.NavigationMenuItemView.EMPTY_STATE_SET, new android.graphics.drawable.ColorDrawable(0));
        }
        return v0_3;
    }

    public final void a(android.support.v7.internal.view.menu.m p6)
    {
        int v0_1;
        this.c = p6;
        if (!p6.isVisible()) {
            v0_1 = 8;
        } else {
            v0_1 = 0;
        }
        this.setVisibility(v0_1);
        if (this.getBackground() == null) {
            int v0_6;
            int[] v2_1 = new android.util.TypedValue();
            if (!this.getContext().getTheme().resolveAttribute(android.support.design.d.colorControlHighlight, v2_1, 1)) {
                v0_6 = 0;
            } else {
                v0_6 = new android.graphics.drawable.StateListDrawable();
                v0_6.addState(android.support.design.internal.NavigationMenuItemView.a, new android.graphics.drawable.ColorDrawable(v2_1.data));
                v0_6.addState(android.support.design.internal.NavigationMenuItemView.EMPTY_STATE_SET, new android.graphics.drawable.ColorDrawable(0));
            }
            this.setBackgroundDrawable(v0_6);
        }
        this.setCheckable(p6.isCheckable());
        this.setChecked(p6.isChecked());
        this.setEnabled(p6.isEnabled());
        this.setTitle(p6.getTitle());
        this.setIcon(p6.getIcon());
        return;
    }

    public final boolean a()
    {
        return 0;
    }

    public final boolean b()
    {
        return 1;
    }

    public android.support.v7.internal.view.menu.m getItemData()
    {
        return this.c;
    }

    protected int[] onCreateDrawableState(int p3)
    {
        int[] v0_1 = super.onCreateDrawableState((p3 + 1));
        if ((this.c != null) && ((this.c.isCheckable()) && (this.c.isChecked()))) {
            android.support.design.internal.NavigationMenuItemView.mergeDrawableStates(v0_1, android.support.design.internal.NavigationMenuItemView.a);
        }
        return v0_1;
    }

    public void setCheckable(boolean p1)
    {
        this.refreshDrawableState();
        return;
    }

    public void setChecked(boolean p1)
    {
        this.refreshDrawableState();
        return;
    }

    public void setIcon(android.graphics.drawable.Drawable p4)
    {
        if (p4 != null) {
            p4 = android.support.v4.e.a.a.c(p4.getConstantState().newDrawable()).mutate();
            p4.setBounds(0, 0, this.b, this.b);
            android.support.v4.e.a.a.a(p4, this.d);
        }
        android.support.v4.widget.dy.a(this, p4);
        return;
    }

    void setIconTintList(android.content.res.ColorStateList p2)
    {
        this.d = p2;
        if (this.c != null) {
            this.setIcon(this.c.getIcon());
        }
        return;
    }

    public final void setShortcut$25d965e(boolean p1)
    {
        return;
    }

    public void setTitle(CharSequence p1)
    {
        this.setText(p1);
        return;
    }
}
