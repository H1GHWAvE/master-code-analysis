package android.support.design.internal;
public final class a extends android.support.v7.internal.view.menu.i {

    public a(android.content.Context p1)
    {
        this(p1);
        return;
    }

    public final android.view.SubMenu addSubMenu(int p4, int p5, int p6, CharSequence p7)
    {
        android.support.v7.internal.view.menu.m v0_1 = ((android.support.v7.internal.view.menu.m) this.a(p4, p5, p6, p7));
        android.support.design.internal.e v1_1 = new android.support.design.internal.e(this.e, this, v0_1);
        v0_1.a(v1_1);
        return v1_1;
    }
}
