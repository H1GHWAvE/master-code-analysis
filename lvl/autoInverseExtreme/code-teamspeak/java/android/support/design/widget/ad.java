package android.support.design.widget;
 class ad extends android.support.design.widget.al {
    android.support.design.widget.ar a;
    private android.graphics.drawable.Drawable h;
    private android.graphics.drawable.Drawable i;
    private android.graphics.drawable.Drawable j;
    private float k;
    private float l;
    private int m;
    private android.support.design.widget.bl n;
    private boolean o;

    ad(android.view.View p9, android.support.design.widget.as p10)
    {
        this(p9, p10);
        this.m = p9.getResources().getInteger(17694720);
        this.n = new android.support.design.widget.bl();
        android.support.design.widget.ah v3_0 = this.n;
        int v0_4 = v3_0.a();
        if (v0_4 != p9) {
            if (v0_4 != 0) {
                android.view.View v4 = v3_0.a();
                int v5 = v3_0.a.size();
                int v1_1 = 0;
                while (v1_1 < v5) {
                    if (v4.getAnimation() == ((android.support.design.widget.bn) v3_0.a.get(v1_1)).b) {
                        v4.clearAnimation();
                    }
                    v1_1++;
                }
                v3_0.d = 0;
                v3_0.b = 0;
                v3_0.c = 0;
            }
            if (p9 != null) {
                v3_0.d = new ref.WeakReference(p9);
            }
        }
        this.n.a(android.support.design.widget.ad.c, this.a(new android.support.design.widget.ag(this, 0)));
        this.n.a(android.support.design.widget.ad.d, this.a(new android.support.design.widget.ag(this, 0)));
        this.n.a(android.support.design.widget.ad.e, this.a(new android.support.design.widget.ah(this, 0)));
        return;
    }

    static synthetic float a(android.support.design.widget.ad p1)
    {
        return p1.k;
    }

    private android.view.animation.Animation a(android.view.animation.Animation p3)
    {
        p3.setInterpolator(android.support.design.widget.a.b);
        p3.setDuration(((long) this.m));
        return p3;
    }

    static synthetic boolean a(android.support.design.widget.ad p0, boolean p1)
    {
        p0.o = p1;
        return p1;
    }

    static synthetic float b(android.support.design.widget.ad p1)
    {
        return p1.l;
    }

    private static android.content.res.ColorStateList b(int p6)
    {
        int[][] v0 = new int[][3];
        int[] v1_1 = new int[3];
        v0[0] = android.support.design.widget.ad.d;
        v1_1[0] = p6;
        v0[1] = android.support.design.widget.ad.c;
        v1_1[1] = p6;
        android.content.res.ColorStateList v2_2 = new int[0];
        v0[2] = v2_2;
        v1_1[2] = 0;
        return new android.content.res.ColorStateList(v0, v1_1);
    }

    private void e()
    {
        int v0_1 = new android.graphics.Rect();
        this.a.getPadding(v0_1);
        this.g.a(v0_1.left, v0_1.top, v0_1.right, v0_1.bottom);
        return;
    }

    void a()
    {
        android.view.animation.Animation v0_0 = this.n;
        if (v0_0.c != null) {
            android.view.View v1_1 = v0_0.a();
            if ((v1_1 != null) && (v1_1.getAnimation() == v0_0.c)) {
                v1_1.clearAnimation();
            }
        }
        return;
    }

    void a(float p3)
    {
        if ((this.k != p3) && (this.a != null)) {
            this.a.a(p3, (this.l + p3));
            this.k = p3;
            this.e();
        }
        return;
    }

    void a(int p3)
    {
        android.support.v4.e.a.a.a(this.i, android.support.design.widget.ad.b(p3));
        return;
    }

    void a(android.content.res.ColorStateList p2)
    {
        android.support.v4.e.a.a.a(this.h, p2);
        if (this.j != null) {
            android.support.v4.e.a.a.a(this.j, p2);
        }
        return;
    }

    void a(android.graphics.PorterDuff$Mode p2)
    {
        android.support.v4.e.a.a.a(this.h, p2);
        return;
    }

    void a(android.graphics.drawable.Drawable p9, android.content.res.ColorStateList p10, android.graphics.PorterDuff$Mode p11, int p12, int p13)
    {
        this.h = android.support.v4.e.a.a.c(p9.mutate());
        android.support.v4.e.a.a.a(this.h, p10);
        if (p11 != null) {
            android.support.v4.e.a.a.a(this.h, p11);
        }
        float v3_1;
        android.support.design.widget.as v0_5 = new android.graphics.drawable.GradientDrawable();
        v0_5.setShape(1);
        v0_5.setColor(-1);
        v0_5.setCornerRadius(this.g.a());
        this.i = android.support.v4.e.a.a.c(v0_5);
        android.support.v4.e.a.a.a(this.i, android.support.design.widget.ad.b(p12));
        android.support.v4.e.a.a.a(this.i, android.graphics.PorterDuff$Mode.MULTIPLY);
        if (p13 <= 0) {
            this.j = 0;
            android.support.design.widget.as v0_10 = new android.graphics.drawable.Drawable[2];
            v0_10[0] = this.h;
            v0_10[1] = this.i;
            v3_1 = v0_10;
        } else {
            this.j = this.a(p13, p10);
            android.support.design.widget.as v0_13 = new android.graphics.drawable.Drawable[3];
            v0_13[0] = this.j;
            v0_13[1] = this.h;
            v0_13[2] = this.i;
            v3_1 = v0_13;
        }
        this.a = new android.support.design.widget.ar(this.f.getResources(), new android.graphics.drawable.LayerDrawable(v3_1), this.g.a(), this.k, (this.k + this.l));
        android.support.design.widget.as v0_16 = this.a;
        v0_16.o = 0;
        v0_16.invalidateSelf();
        this.g.a(this.a);
        this.e();
        return;
    }

    void a(int[] p7)
    {
        android.support.design.widget.bl v3 = this.n;
        android.view.animation.Animation v4_0 = v3.a.size();
        android.view.View v2_0 = 0;
        while (v2_0 < v4_0) {
            android.view.View v0_2 = ((android.support.design.widget.bn) v3.a.get(v2_0));
            if (!android.util.StateSet.stateSetMatches(v0_2.a, p7)) {
                v2_0++;
            }
            if (v0_2 != v3.b) {
                if ((v3.b != null) && (v3.c != null)) {
                    android.view.View v2_4 = v3.a();
                    if ((v2_4 != null) && (v2_4.getAnimation() == v3.c)) {
                        v2_4.clearAnimation();
                    }
                    v3.c = 0;
                }
                v3.b = v0_2;
                if (v0_2 != null) {
                    v3.c = v0_2.b;
                    android.view.View v0_7 = v3.a();
                    if (v0_7 != null) {
                        v0_7.startAnimation(v3.c);
                    }
                }
            }
            return;
        }
        v0_2 = 0;
    }

    void b()
    {
        if ((!this.o) && (this.f.getVisibility() == 0)) {
            android.view.animation.Animation v0_5 = android.view.animation.AnimationUtils.loadAnimation(this.f.getContext(), android.support.design.c.design_fab_out);
            v0_5.setInterpolator(android.support.design.widget.a.b);
            v0_5.setDuration(200);
            v0_5.setAnimationListener(new android.support.design.widget.ae(this));
            this.f.startAnimation(v0_5);
        }
        return;
    }

    void b(float p4)
    {
        if ((this.l != p4) && (this.a != null)) {
            this.l = p4;
            android.support.design.widget.ar v0_3 = this.a;
            v0_3.a(v0_3.n, (this.k + p4));
            this.e();
        }
        return;
    }

    void c()
    {
        if ((this.f.getVisibility() != 0) || (this.o)) {
            this.f.clearAnimation();
            this.f.setVisibility(0);
            android.view.animation.Animation v0_7 = android.view.animation.AnimationUtils.loadAnimation(this.f.getContext(), android.support.design.c.design_fab_in);
            v0_7.setDuration(200);
            v0_7.setInterpolator(android.support.design.widget.a.b);
            this.f.startAnimation(v0_7);
        }
        return;
    }
}
