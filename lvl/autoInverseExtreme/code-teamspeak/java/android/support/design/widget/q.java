package android.support.design.widget;
final class q implements android.support.design.widget.i {
    final synthetic android.support.design.widget.m a;

    private q(android.support.design.widget.m p1)
    {
        this.a = p1;
        return;
    }

    synthetic q(android.support.design.widget.m p1, byte p2)
    {
        this(p1);
        return;
    }

    public final void a(android.support.design.widget.AppBarLayout p9, int p10)
    {
        android.support.design.widget.l v1_0;
        float v2_0 = 0;
        android.support.design.widget.m.b(this.a, p10);
        if (android.support.design.widget.m.a(this.a) == null) {
            v1_0 = 0;
        } else {
            v1_0 = android.support.design.widget.m.a(this.a).b();
        }
        int v3 = p9.getTotalScrollRange();
        int v4 = this.a.getChildCount();
        while (v2_0 < v4) {
            float v5_0 = this.a.getChildAt(v2_0);
            int v0_30 = ((android.support.design.widget.p) v5_0.getLayoutParams());
            android.support.design.widget.dg v6 = android.support.design.widget.m.a(v5_0);
            switch (v0_30.d) {
                case 1:
                    if (((this.a.getHeight() - v1_0) + p10) < v5_0.getHeight()) {
                    } else {
                        v6.a((- p10));
                    }
                    break;
                case 2:
                    v6.a(Math.round((v0_30.e * ((float) (- p10)))));
                    break;
            }
            v2_0++;
        }
        if ((android.support.design.widget.m.b(this.a) != null) || (android.support.design.widget.m.c(this.a) != null)) {
            if ((this.a.getHeight() + p10) >= (this.a.getScrimTriggerOffset() + v1_0)) {
                android.support.design.widget.m.e(this.a);
            } else {
                android.support.design.widget.m.d(this.a);
            }
        }
        if ((android.support.design.widget.m.c(this.a) != null) && (v1_0 > null)) {
            android.support.v4.view.cx.b(this.a);
        }
        android.support.design.widget.m.f(this.a).a((((float) Math.abs(p10)) / ((float) ((this.a.getHeight() - android.support.v4.view.cx.o(this.a)) - v1_0))));
        if (Math.abs(p10) != v3) {
            android.support.v4.view.cx.f(p9, 0);
        } else {
            android.support.v4.view.cx.f(p9, p9.getTargetElevation());
        }
        return;
    }
}
