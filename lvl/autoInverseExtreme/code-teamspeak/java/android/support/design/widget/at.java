package android.support.design.widget;
final class at implements android.os.Handler$Callback {

    at()
    {
        return;
    }

    public final boolean handleMessage(android.os.Message p9)
    {
        android.support.design.widget.Snackbar$SnackbarLayout v0_5;
        int v3_0 = 0;
        switch (p9.what) {
            case 0:
                android.support.design.widget.Snackbar$SnackbarLayout v0_7 = ((android.support.design.widget.Snackbar) p9.obj);
                if (v0_7.e.getParent() == null) {
                    android.view.animation.Animation v1_21 = v0_7.e.getLayoutParams();
                    if ((v1_21 instanceof android.support.design.widget.w)) {
                        android.support.design.widget.bc v4_3 = new android.support.design.widget.bc(v0_7);
                        v4_3.k = android.support.design.widget.SwipeDismissBehavior.a(1036831949);
                        v4_3.l = android.support.design.widget.SwipeDismissBehavior.a(1058642330);
                        v4_3.j = 0;
                        v4_3.i = new android.support.design.widget.aw(v0_7);
                        ((android.support.design.widget.w) v1_21).a(v4_3);
                    }
                    v0_7.d.addView(v0_7.e);
                }
                if (!android.support.v4.view.cx.B(v0_7.e)) {
                    v0_7.e.setOnLayoutChangeListener(new android.support.design.widget.ax(v0_7));
                } else {
                    v0_7.a();
                }
                v0_5 = 1;
                break;
            case 1:
                android.support.design.widget.Snackbar$SnackbarLayout v0_2 = ((android.support.design.widget.Snackbar) p9.obj);
                android.support.design.widget.bc v4_0 = p9.arg1;
                if (v0_2.e.getVisibility() != 0) {
                    v0_2.b();
                } else {
                    android.view.animation.Animation v1_3 = v0_2.e.getLayoutParams();
                    if ((v1_3 instanceof android.support.design.widget.w)) {
                        android.view.animation.Animation v1_5 = ((android.support.design.widget.w) v1_3).a;
                        if ((v1_5 instanceof android.support.design.widget.SwipeDismissBehavior)) {
                            android.view.animation.Animation v1_7;
                            android.view.animation.Animation v1_6 = ((android.support.design.widget.SwipeDismissBehavior) v1_5);
                            if (v1_6.h == null) {
                                v1_7 = 0;
                            } else {
                                v1_7 = v1_6.h.m;
                            }
                            if (v1_7 != null) {
                                v3_0 = 1;
                            }
                        }
                    }
                    if (v3_0 == 0) {
                        if (android.os.Build$VERSION.SDK_INT < 14) {
                            android.view.animation.Animation v1_12 = android.view.animation.AnimationUtils.loadAnimation(v0_2.e.getContext(), android.support.design.c.design_snackbar_out);
                            v1_12.setInterpolator(android.support.design.widget.a.b);
                            v1_12.setDuration(250);
                            v1_12.setAnimationListener(new android.support.design.widget.bb(v0_2, v4_0));
                            v0_2.e.startAnimation(v1_12);
                        } else {
                            android.support.v4.view.cx.p(v0_2.e).b(((float) v0_2.e.getHeight())).a(android.support.design.widget.a.b).a(250).a(new android.support.design.widget.ba(v0_2, v4_0)).b();
                        }
                    }
                }
                v0_5 = 1;
                break;
            default:
                v0_5 = 0;
        }
        return v0_5;
    }
}
