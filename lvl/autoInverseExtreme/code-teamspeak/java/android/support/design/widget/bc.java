package android.support.design.widget;
final class bc extends android.support.design.widget.SwipeDismissBehavior {
    final synthetic android.support.design.widget.Snackbar a;

    bc(android.support.design.widget.Snackbar p1)
    {
        this.a = p1;
        return;
    }

    private boolean a(android.support.design.widget.CoordinatorLayout p3, android.support.design.widget.Snackbar$SnackbarLayout p4, android.view.MotionEvent p5)
    {
        if (p3.a(p4, ((int) p5.getX()), ((int) p5.getY()))) {
            switch (p5.getActionMasked()) {
                case 0:
                    android.support.design.widget.bh.a().b(android.support.design.widget.Snackbar.a(this.a));
                    break;
                case 1:
                case 3:
                    android.support.design.widget.bh.a().c(android.support.design.widget.Snackbar.a(this.a));
                case 2:
                default:
                    break;
            }
        }
        return super.b(p3, p4, p5);
    }

    public final synthetic boolean b(android.support.design.widget.CoordinatorLayout p2, android.view.View p3, android.view.MotionEvent p4)
    {
        return this.a(p2, ((android.support.design.widget.Snackbar$SnackbarLayout) p3), p4);
    }
}
