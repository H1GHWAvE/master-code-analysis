package android.support.design.widget;
public class CoordinatorLayout extends android.view.ViewGroup implements android.support.v4.view.bv {
    static final String a = "CoordinatorLayout";
    static final String b = "None";
    static final Class[] c = None;
    static final ThreadLocal d = None;
    static final java.util.Comparator f = None;
    static final android.support.design.widget.aa g = None;
    private static final int k = 0;
    private static final int l = 1;
    private android.graphics.drawable.Drawable A;
    private android.view.ViewGroup$OnHierarchyChangeListener B;
    private final android.support.v4.view.bw C;
    final java.util.Comparator e;
    final java.util.List h;
    final android.graphics.Rect i;
    final android.graphics.Rect j;
    private final java.util.List m;
    private final java.util.List n;
    private final android.graphics.Rect o;
    private final int[] p;
    private android.graphics.Paint q;
    private boolean r;
    private int[] s;
    private android.view.View t;
    private android.view.View u;
    private android.view.View v;
    private android.support.design.widget.x w;
    private boolean x;
    private android.support.v4.view.gh y;
    private boolean z;

    static CoordinatorLayout()
    {
        android.support.design.widget.CoordinatorLayout.b = android.support.design.widget.CoordinatorLayout.getPackage().getName();
        if (android.os.Build$VERSION.SDK_INT < 21) {
            android.support.design.widget.CoordinatorLayout.f = 0;
            android.support.design.widget.CoordinatorLayout.g = 0;
        } else {
            android.support.design.widget.CoordinatorLayout.f = new android.support.design.widget.z();
            android.support.design.widget.CoordinatorLayout.g = new android.support.design.widget.ab();
        }
        ThreadLocal v0_9 = new Class[2];
        v0_9[0] = android.content.Context;
        v0_9[1] = android.util.AttributeSet;
        android.support.design.widget.CoordinatorLayout.c = v0_9;
        android.support.design.widget.CoordinatorLayout.d = new ThreadLocal();
        return;
    }

    private CoordinatorLayout(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    private CoordinatorLayout(android.content.Context p2, byte p3)
    {
        this(p2, 0);
        return;
    }

    private CoordinatorLayout(android.content.Context p7, char p8)
    {
        int v0_0 = 0;
        this(p7, 0, 0);
        this.e = new android.support.design.widget.r(this);
        this.h = new java.util.ArrayList();
        this.m = new java.util.ArrayList();
        this.n = new java.util.ArrayList();
        this.i = new android.graphics.Rect();
        this.j = new android.graphics.Rect();
        this.o = new android.graphics.Rect();
        android.support.design.widget.s v1_15 = new int[2];
        this.p = v1_15;
        this.C = new android.support.v4.view.bw(this);
        android.support.design.widget.s v1_19 = p7.obtainStyledAttributes(0, android.support.design.n.CoordinatorLayout, 0, android.support.design.m.Widget_Design_CoordinatorLayout);
        float v2_2 = v1_19.getResourceId(android.support.design.n.CoordinatorLayout_keylines, 0);
        if (v2_2 != 0) {
            int v3_1 = p7.getResources();
            this.s = v3_1.getIntArray(v2_2);
            int v3_3 = this.s.length;
            while (v0_0 < v3_3) {
                int[] v4 = this.s;
                v4[v0_0] = ((int) (((float) v4[v0_0]) * v3_1.getDisplayMetrics().density));
                v0_0++;
            }
        }
        this.A = v1_19.getDrawable(android.support.design.n.CoordinatorLayout_statusBarBackground);
        v1_19.recycle();
        if (android.support.design.widget.CoordinatorLayout.g != null) {
            android.support.design.widget.CoordinatorLayout.g.a(this, new android.support.design.widget.s(this));
        }
        super.setOnHierarchyChangeListener(new android.support.design.widget.v(this));
        return;
    }

    private int a(int p5)
    {
        int v0_0 = 0;
        if (this.s != null) {
            if ((p5 >= 0) && (p5 < this.s.length)) {
                v0_0 = this.s[p5];
            } else {
                android.util.Log.e("CoordinatorLayout", new StringBuilder("Keyline index ").append(p5).append(" out of range for ").append(this).toString());
            }
        } else {
            android.util.Log.e("CoordinatorLayout", new StringBuilder("No keylines defined for ").append(this).append(" - attempted index lookup ").append(p5).toString());
        }
        return v0_0;
    }

    static android.support.design.widget.t a(android.content.Context p4, android.util.AttributeSet p5, String p6)
    {
        android.support.design.widget.t v0_24;
        if (!android.text.TextUtils.isEmpty(p6)) {
            if (!p6.startsWith(".")) {
                if (p6.indexOf(46) < 0) {
                    p6 = new StringBuilder().append(android.support.design.widget.CoordinatorLayout.b).append(46).append(p6).toString();
                }
            } else {
                p6 = new StringBuilder().append(p4.getPackageName()).append(p6).toString();
            }
            try {
                Object[] v1_2;
                android.support.design.widget.t v0_15 = ((java.util.Map) android.support.design.widget.CoordinatorLayout.d.get());
            } catch (android.support.design.widget.t v0_22) {
                throw new RuntimeException(new StringBuilder("Could not inflate Behavior subclass ").append(p6).toString(), v0_22);
            }
            if (v0_15 != null) {
                v1_2 = v0_15;
            } else {
                android.support.design.widget.t v0_17 = new java.util.HashMap();
                android.support.design.widget.CoordinatorLayout.d.set(v0_17);
                v1_2 = v0_17;
            }
            android.support.design.widget.t v0_19 = ((reflect.Constructor) v1_2.get(p6));
            if (v0_19 == null) {
                v0_19 = Class.forName(p6, 1, p4.getClassLoader()).getConstructor(android.support.design.widget.CoordinatorLayout.c);
                v0_19.setAccessible(1);
                v1_2.put(p6, v0_19);
            }
            Object[] v1_7 = new Object[2];
            v1_7[0] = p4;
            v1_7[1] = p5;
            v0_24 = ((android.support.design.widget.t) v0_19.newInstance(v1_7));
        } else {
            v0_24 = 0;
        }
        return v0_24;
    }

    private android.support.design.widget.w a(android.util.AttributeSet p3)
    {
        return new android.support.design.widget.w(this.getContext(), p3);
    }

    private static android.support.design.widget.w a(android.view.ViewGroup$LayoutParams p1)
    {
        android.support.design.widget.w v0_3;
        if (!(p1 instanceof android.support.design.widget.w)) {
            if (!(p1 instanceof android.view.ViewGroup$MarginLayoutParams)) {
                v0_3 = new android.support.design.widget.w(p1);
            } else {
                v0_3 = new android.support.design.widget.w(((android.view.ViewGroup$MarginLayoutParams) p1));
            }
        } else {
            v0_3 = new android.support.design.widget.w(((android.support.design.widget.w) p1));
        }
        return v0_3;
    }

    static synthetic android.view.ViewGroup$OnHierarchyChangeListener a(android.support.design.widget.CoordinatorLayout p1)
    {
        return p1.B;
    }

    private void a()
    {
        if (this.t != null) {
            android.support.design.widget.t v8 = ((android.support.design.widget.w) this.t.getLayoutParams()).a;
            if (v8 != null) {
                int v0_4 = android.os.SystemClock.uptimeMillis();
                int v0_5 = android.view.MotionEvent.obtain(v0_4, v0_4, 3, 0, 0, 0);
                v8.a(this, this.t, v0_5);
                v0_5.recycle();
            }
            this.t = 0;
        }
        long v2_1 = this.getChildCount();
        int v1_1 = 0;
        while (v1_1 < v2_1) {
            ((android.support.design.widget.w) this.getChildAt(v1_1).getLayoutParams()).i = 0;
            v1_1++;
        }
        return;
    }

    static synthetic void a(android.support.design.widget.CoordinatorLayout p0, android.support.v4.view.gh p1)
    {
        p0.setWindowInsets(p1);
        return;
    }

    private void a(android.support.v4.view.gh p6)
    {
        if (!p6.g()) {
            int v3 = this.getChildCount();
            int v2 = 0;
            android.support.v4.view.gh v1 = p6;
            while (v2 < v3) {
                android.view.View v4 = this.getChildAt(v2);
                if (android.support.v4.view.cx.u(v4)) {
                    if ((((android.support.design.widget.w) v4.getLayoutParams()).a != null) && (v1.g())) {
                        break;
                    }
                    v1 = android.support.v4.view.cx.b(v4, v1);
                    if (v1.g()) {
                        break;
                    }
                }
                v2++;
            }
        }
        return;
    }

    private void a(android.view.View p10, int p11, android.graphics.Rect p12, android.graphics.Rect p13)
    {
        int v0_1 = ((android.support.design.widget.w) p10.getLayoutParams());
        int v1_0 = v0_1.c;
        if (v1_0 == 0) {
            v1_0 = 17;
        }
        int v2_3;
        int v1_1 = android.support.v4.view.v.a(v1_0, p11);
        int v2_2 = android.support.v4.view.v.a(android.support.design.widget.CoordinatorLayout.b(v0_1.d), p11);
        int v3_0 = (v1_1 & 7);
        int v4_0 = (v1_1 & 112);
        int v1_2 = (v2_2 & 7);
        int v5_0 = (v2_2 & 112);
        int v6 = p10.getMeasuredWidth();
        int v7 = p10.getMeasuredHeight();
        switch (v1_2) {
            case 1:
                v2_3 = ((p12.width() / 2) + p12.left);
                break;
            case 5:
                v2_3 = p12.right;
                break;
            default:
                v2_3 = p12.left;
        }
        int v1_4;
        switch (v5_0) {
            case 16:
                v1_4 = (p12.top + (p12.height() / 2));
                break;
            case 80:
                v1_4 = p12.bottom;
                break;
            default:
                v1_4 = p12.top;
        }
        switch (v3_0) {
            case 1:
                v2_3 -= (v6 / 2);
            case 5:
                break;
            default:
                v2_3 -= v6;
        }
        switch (v4_0) {
            case 16:
                v1_4 -= (v7 / 2);
            case 80:
                break;
            default:
                v1_4 -= v7;
        }
        int v3_3 = this.getWidth();
        int v4_1 = this.getHeight();
        int v2_7 = Math.max((this.getPaddingLeft() + v0_1.leftMargin), Math.min(v2_3, (((v3_3 - this.getPaddingRight()) - v6) - v0_1.rightMargin)));
        int v0_5 = Math.max((this.getPaddingTop() + v0_1.topMargin), Math.min(v1_4, (((v4_1 - this.getPaddingBottom()) - v7) - v0_1.bottomMargin)));
        p13.set(v2_7, v0_5, (v2_7 + v6), (v0_5 + v7));
        return;
    }

    private void a(android.view.View p1, android.graphics.Rect p2)
    {
        android.support.design.widget.cz.a(this, p1, p2);
        return;
    }

    private void a(android.view.View p5, android.view.View p6, int p7)
    {
        p5.getLayoutParams();
        int v0_0 = this.i;
        int v1_0 = this.j;
        android.support.design.widget.cz.a(this, p6, v0_0);
        this.a(p5, p7, v0_0, v1_0);
        p5.layout(v1_0.left, v1_0.top, v1_0.right, v1_0.bottom);
        return;
    }

    private void a(java.util.List p5)
    {
        p5.clear();
        boolean v2 = this.isChildrenDrawingOrderEnabled();
        int v3 = this.getChildCount();
        int v1 = (v3 - 1);
        while (v1 >= 0) {
            android.view.View v0_2;
            if (!v2) {
                v0_2 = v1;
            } else {
                v0_2 = this.getChildDrawingOrder(v3, v1);
            }
            p5.add(this.getChildAt(v0_2));
            v1--;
        }
        if (android.support.design.widget.CoordinatorLayout.f != null) {
            java.util.Collections.sort(p5, android.support.design.widget.CoordinatorLayout.f);
        }
        return;
    }

    private boolean a(android.view.MotionEvent p22, int p23)
    {
        boolean v14 = 0;
        int v13 = 0;
        int v16 = android.support.v4.view.bk.a(p22);
        java.util.List v17 = this.m;
        v17.clear();
        int v7_0 = this.isChildrenDrawingOrderEnabled();
        int v8_0 = this.getChildCount();
        int v5_0 = (v8_0 - 1);
        while (v5_0 >= 0) {
            int v4_10;
            if (v7_0 == 0) {
                v4_10 = v5_0;
            } else {
                v4_10 = this.getChildDrawingOrder(v8_0, v5_0);
            }
            v17.add(this.getChildAt(v4_10));
            v5_0--;
        }
        if (android.support.design.widget.CoordinatorLayout.f != null) {
            java.util.Collections.sort(v17, android.support.design.widget.CoordinatorLayout.f);
        }
        int v18 = v17.size();
        int v15 = 0;
        int v5_1 = 0;
        while (v15 < v18) {
            boolean v6_1;
            int v4_8;
            int v5_2;
            android.view.View v12_1 = ((android.view.View) v17.get(v15));
            int v4_5 = ((android.support.design.widget.w) v12_1.getLayoutParams());
            android.support.design.widget.t v19 = v4_5.a;
            if (((!v14) && (v13 == 0)) || (v16 == 0)) {
                if ((!v14) && (v19 != null)) {
                    switch (p23) {
                        case 0:
                            v14 = v19.b(this, v12_1, p22);
                            break;
                        case 1:
                            v14 = v19.a(this, v12_1, p22);
                            break;
                    }
                    if (v14) {
                        this.t = v12_1;
                    }
                }
                v6_1 = v14;
                if (v4_5.a == null) {
                    v4_5.i = 0;
                }
                int v7_5;
                int v8_1 = v4_5.i;
                if (!v4_5.i) {
                    v7_5 = (v4_5.i | 0);
                    v4_5.i = v7_5;
                } else {
                    v7_5 = 1;
                }
                if ((v7_5 == 0) || (v8_1 != 0)) {
                    int v4_7 = 0;
                } else {
                    v4_7 = 1;
                }
                if ((v7_5 != 0) && (v4_7 == 0)) {
                    v17.clear();
                    return v6_1;
                } else {
                    v5_2 = v4_7;
                    v4_8 = v5_1;
                }
            } else {
                if (v19 == null) {
                    v4_8 = v5_1;
                    v6_1 = v14;
                    v5_2 = v13;
                } else {
                    if (v5_1 != 0) {
                        v4_8 = v5_1;
                    } else {
                        int v4_9 = android.os.SystemClock.uptimeMillis();
                        v4_8 = android.view.MotionEvent.obtain(v4_9, v4_9, 3, 0, 0, 0);
                    }
                    switch (p23) {
                        case 0:
                            v19.b(this, v12_1, v4_8);
                            v5_2 = v13;
                            v6_1 = v14;
                        case 1:
                            v19.a(this, v12_1, v4_8);
                            break;
                    }
                    v5_2 = v13;
                    v6_1 = v14;
                }
            }
            v15++;
            v13 = v5_2;
            v14 = v6_1;
            v5_1 = v4_8;
        }
        v6_1 = v14;
        v17.clear();
        return v6_1;
    }

    private boolean a(android.view.View p7, android.view.View p8)
    {
        int v2 = 0;
        if ((p7.getVisibility() == 0) && (p8.getVisibility() == 0)) {
            int v0_3;
            int v3_0 = this.i;
            if (p7.getParent() == this) {
                v0_3 = 0;
            } else {
                v0_3 = 1;
            }
            int v0_5;
            this.a(p7, v0_3, v3_0);
            android.graphics.Rect v4 = this.j;
            if (p8.getParent() == this) {
                v0_5 = 0;
            } else {
                v0_5 = 1;
            }
            this.a(p8, v0_5, v4);
            if ((v3_0.left <= v4.right) && ((v3_0.top <= v4.bottom) && ((v3_0.right >= v4.left) && (v3_0.bottom >= v4.top)))) {
                v2 = 1;
            }
        }
        return v2;
    }

    private static int b(int p0)
    {
        if (p0 == 0) {
            p0 = 8388659;
        }
        return p0;
    }

    private static android.support.design.widget.w b(android.view.View p6)
    {
        android.support.design.widget.w v0_1 = ((android.support.design.widget.w) p6.getLayoutParams());
        if (!v0_1.b) {
            String v2_0 = p6.getClass();
            Exception v1_1 = 0;
            while (v2_0 != null) {
                v1_1 = ((android.support.design.widget.u) v2_0.getAnnotation(android.support.design.widget.u));
                if (v1_1 != null) {
                    break;
                }
                v2_0 = v2_0.getSuperclass();
            }
            String v2_1 = v1_1;
            if (v2_1 != null) {
                try {
                    v0_1.a(((android.support.design.widget.t) v2_1.a().newInstance()));
                } catch (Exception v1_7) {
                    android.util.Log.e("CoordinatorLayout", new StringBuilder("Default behavior class ").append(v2_1.a().getName()).append(" could not be instantiated. Did you forget a default constructor?").toString(), v1_7);
                }
            }
            v0_1.b = 1;
        }
        return v0_1;
    }

    private void b()
    {
        String v2_0;
        int v4 = 0;
        int v6 = this.getChildCount();
        if (this.h.size() == v6) {
            v2_0 = 0;
        } else {
            v2_0 = 1;
        }
        int v5 = 0;
        while (v5 < v6) {
            android.view.View v7 = this.getChildAt(v5);
            android.support.design.widget.w v8 = android.support.design.widget.CoordinatorLayout.b(v7);
            if (v8.f != -1) {
                if (v8.g != null) {
                    android.view.View v0_10;
                    if (v8.g.getId() == v8.f) {
                        android.view.View v0_9 = v8.g;
                        android.view.ViewParent v1_5 = v8.g.getParent();
                        while (v1_5 != this) {
                            if ((v1_5 != null) && (v1_5 != v7)) {
                                if ((v1_5 instanceof android.view.View)) {
                                    v0_9 = ((android.view.View) v1_5);
                                }
                                v1_5 = v1_5.getParent();
                            } else {
                                v8.h = 0;
                                v8.g = 0;
                                v0_10 = 0;
                            }
                        }
                        v8.h = v0_9;
                        v0_10 = 1;
                    } else {
                        v0_10 = 0;
                    }
                    if (v0_10 != null) {
                        v5++;
                    }
                }
                v8.g = this.findViewById(v8.f);
                if (v8.g == null) {
                    if (!this.isInEditMode()) {
                        throw new IllegalStateException(new StringBuilder("Could not find CoordinatorLayout descendant view with id ").append(this.getResources().getResourceName(v8.f)).append(" to anchor view ").append(v7).toString());
                    } else {
                        v8.h = 0;
                        v8.g = 0;
                    }
                } else {
                    android.view.View v0_18 = v8.g;
                    android.view.ViewParent v1_13 = v8.g.getParent();
                    while ((v1_13 != this) && (v1_13 != null)) {
                        if (v1_13 != v7) {
                            if ((v1_13 instanceof android.view.View)) {
                                v0_18 = ((android.view.View) v1_13);
                            }
                            v1_13 = v1_13.getParent();
                        } else {
                            if (!this.isInEditMode()) {
                                throw new IllegalStateException("Anchor must not be a descendant of the anchored view");
                            } else {
                                v8.h = 0;
                                v8.g = 0;
                            }
                        }
                    }
                    v8.h = v0_18;
                }
            } else {
                v8.h = 0;
                v8.g = 0;
            }
        }
        if (v2_0 != null) {
            this.h.clear();
            while (v4 < v6) {
                this.h.add(this.getChildAt(v4));
                v4++;
            }
            java.util.Collections.sort(this.h, this.e);
        }
        return;
    }

    private void b(android.view.View p8, int p9)
    {
        int v0_1 = ((android.support.design.widget.w) p8.getLayoutParams());
        int v3_0 = this.i;
        v3_0.set((this.getPaddingLeft() + v0_1.leftMargin), (this.getPaddingTop() + v0_1.topMargin), ((this.getWidth() - this.getPaddingRight()) - v0_1.rightMargin), ((this.getHeight() - this.getPaddingBottom()) - v0_1.bottomMargin));
        if ((this.y != null) && ((android.support.v4.view.cx.u(this)) && (!android.support.v4.view.cx.u(p8)))) {
            v3_0.left = (v3_0.left + this.y.a());
            v3_0.top = (v3_0.top + this.y.b());
            v3_0.right = (v3_0.right - this.y.c());
            v3_0.bottom = (v3_0.bottom - this.y.d());
        }
        android.graphics.Rect v4_4 = this.j;
        android.support.v4.view.v.a(android.support.design.widget.CoordinatorLayout.b(v0_1.c), p8.getMeasuredWidth(), p8.getMeasuredHeight(), v3_0, v4_4, p9);
        p8.layout(v4_4.left, v4_4.top, v4_4.right, v4_4.bottom);
        return;
    }

    private void b(android.view.View p10, int p11, int p12)
    {
        int v0_1 = ((android.support.design.widget.w) p10.getLayoutParams());
        int v1_2 = android.support.v4.view.v.a(android.support.design.widget.CoordinatorLayout.c(v0_1.c), p12);
        int v3_0 = (v1_2 & 7);
        int v4_0 = (v1_2 & 112);
        int v5_0 = this.getWidth();
        int v6 = this.getHeight();
        int v7 = p10.getMeasuredWidth();
        int v8 = p10.getMeasuredHeight();
        if (p12 == 1) {
            p11 = (v5_0 - p11);
        }
        int v2_0 = (this.a(p11) - v7);
        int v1_5 = 0;
        switch (v3_0) {
            case 1:
                v2_0 += (v7 / 2);
                break;
            case 5:
                v2_0 += v7;
                break;
        }
        switch (v4_0) {
            case 16:
                v1_5 = ((v8 / 2) + 0);
                break;
            case 80:
                v1_5 = (v8 + 0);
                break;
        }
        int v2_2 = Math.max((this.getPaddingLeft() + v0_1.leftMargin), Math.min(v2_0, (((v5_0 - this.getPaddingRight()) - v7) - v0_1.rightMargin)));
        int v0_5 = Math.max((this.getPaddingTop() + v0_1.topMargin), Math.min(v1_5, (((v6 - this.getPaddingBottom()) - v8) - v0_1.bottomMargin)));
        p10.layout(v2_2, v0_5, (v2_2 + v7), (v0_5 + v8));
        return;
    }

    private static void b(android.view.View p1, android.graphics.Rect p2)
    {
        ((android.support.design.widget.w) p1.getLayoutParams()).l.set(p2);
        return;
    }

    private static int c(int p0)
    {
        if (p0 == 0) {
            p0 = 8388661;
        }
        return p0;
    }

    private void c()
    {
        int v5 = this.getChildCount();
        int v4 = 0;
        while (v4 < v5) {
            android.view.ViewTreeObserver v0_3;
            android.view.View v6 = this.getChildAt(v4);
            android.view.ViewTreeObserver v0_2 = ((android.support.design.widget.w) v6.getLayoutParams());
            if (v0_2.g == null) {
                int v7 = this.getChildCount();
                int v3_1 = 0;
                while (v3_1 < v7) {
                    boolean v8_0 = this.getChildAt(v3_1);
                    if ((v8_0 == v6) || (!v0_2.a(v8_0))) {
                        v3_1++;
                    } else {
                        v0_3 = 1;
                    }
                }
                v0_3 = 0;
            } else {
                v0_3 = 1;
            }
            if (v0_3 == null) {
                v4++;
            } else {
                android.view.ViewTreeObserver v0_0 = 1;
            }
            if (v0_0 != this.x) {
                if (v0_0 == null) {
                    if ((this.r) && (this.w != null)) {
                        this.getViewTreeObserver().removeOnPreDrawListener(this.w);
                    }
                    this.x = 0;
                } else {
                    if (this.r) {
                        if (this.w == null) {
                            this.w = new android.support.design.widget.x(this);
                        }
                        this.getViewTreeObserver().addOnPreDrawListener(this.w);
                    }
                    this.x = 1;
                }
            }
            return;
        }
        v0_0 = 0;
    }

    private void c(android.view.View p6)
    {
        int v2 = this.getChildCount();
        int v1 = 0;
        while (v1 < v2) {
            android.view.View v3 = this.getChildAt(v1);
            int v0_3 = ((android.support.design.widget.w) v3.getLayoutParams()).a;
            if ((v0_3 != 0) && (v0_3.b(p6))) {
                v0_3.a(v3, p6);
            }
            v1++;
        }
        return;
    }

    private void c(android.view.View p6, int p7)
    {
        android.view.View v0_1 = ((android.support.design.widget.w) p6.getLayoutParams());
        if (v0_1.g != null) {
            android.support.design.widget.t v1_1 = this.i;
            int v2_0 = this.j;
            int v3_0 = this.o;
            android.support.design.widget.cz.a(this, v0_1.g, v1_1);
            this.a(p6, 0, v2_0);
            this.a(p6, p7, v1_1, v3_0);
            android.support.design.widget.t v1_3 = (v3_0.left - v2_0.left);
            int v2_2 = (v3_0.top - v2_0.top);
            if (v1_3 != null) {
                p6.offsetLeftAndRight(v1_3);
            }
            if (v2_2 != 0) {
                p6.offsetTopAndBottom(v2_2);
            }
            if ((v1_3 != null) || (v2_2 != 0)) {
                android.support.design.widget.t v1_4 = v0_1.a;
                if (v1_4 != null) {
                    v1_4.a(this, p6, v0_1.g);
                }
            }
        }
        return;
    }

    private static void c(android.view.View p1, android.graphics.Rect p2)
    {
        p2.set(((android.support.design.widget.w) p1.getLayoutParams()).l);
        return;
    }

    private static int d(int p0)
    {
        if (p0 == 0) {
            p0 = 17;
        }
        return p0;
    }

    private void d()
    {
        if (this.r) {
            if (this.w == null) {
                this.w = new android.support.design.widget.x(this);
            }
            this.getViewTreeObserver().addOnPreDrawListener(this.w);
        }
        this.x = 1;
        return;
    }

    private void d(android.view.View p7)
    {
        int v4 = this.h.size();
        int v3 = 0;
        int v2 = 0;
        while (v3 < v4) {
            int v0_4;
            int v0_3 = ((android.view.View) this.h.get(v3));
            if (v0_3 != p7) {
                if (v2 != 0) {
                    boolean v1_2 = ((android.support.design.widget.w) v0_3.getLayoutParams());
                    android.support.design.widget.t v5 = v1_2.a;
                    if ((v5 != null) && (v1_2.a(p7))) {
                        v5.a(this, v0_3, p7);
                    }
                }
                v0_4 = v2;
            } else {
                v0_4 = 1;
            }
            v3++;
            v2 = v0_4;
        }
        return;
    }

    private void e()
    {
        if ((this.r) && (this.w != null)) {
            this.getViewTreeObserver().removeOnPreDrawListener(this.w);
        }
        this.x = 0;
        return;
    }

    private boolean e(android.view.View p7)
    {
        int v0_2;
        int v0_1 = ((android.support.design.widget.w) p7.getLayoutParams());
        if (v0_1.g == null) {
            int v4 = this.getChildCount();
            int v3_1 = 0;
            while (v3_1 < v4) {
                boolean v5_0 = this.getChildAt(v3_1);
                if ((v5_0 == p7) || (!v0_1.a(v5_0))) {
                    v3_1++;
                } else {
                    v0_2 = 1;
                }
            }
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private static android.support.design.widget.w f()
    {
        return new android.support.design.widget.w();
    }

    private void setWindowInsets(android.support.v4.view.gh p6)
    {
        android.support.v4.view.gh v1_0 = 1;
        int v2 = 0;
        if (this.y != p6) {
            boolean v0_2;
            this.y = p6;
            if ((p6 == null) || (p6.b() <= 0)) {
                v0_2 = 0;
            } else {
                v0_2 = 1;
            }
            this.z = v0_2;
            if ((this.z) || (this.getBackground() != null)) {
                v1_0 = 0;
            }
            this.setWillNotDraw(v1_0);
            if (!p6.g()) {
                int v3 = this.getChildCount();
                android.support.v4.view.gh v1_1 = p6;
                while (v2 < v3) {
                    android.view.View v4 = this.getChildAt(v2);
                    if (android.support.v4.view.cx.u(v4)) {
                        if ((((android.support.design.widget.w) v4.getLayoutParams()).a != null) && (v1_1.g())) {
                            break;
                        }
                        v1_1 = android.support.v4.view.cx.b(v4, v1_1);
                        if (v1_1.g()) {
                            break;
                        }
                    }
                    v2++;
                }
            }
            this.requestLayout();
        }
        return;
    }

    public final java.util.List a(android.view.View p7)
    {
        android.support.design.widget.w v0_1 = ((android.support.design.widget.w) p7.getLayoutParams());
        java.util.List v2 = this.n;
        v2.clear();
        int v3 = this.getChildCount();
        int v1 = 0;
        while (v1 < v3) {
            android.view.View v4 = this.getChildAt(v1);
            if ((v4 != p7) && (v0_1.a(v4))) {
                v2.add(v4);
            }
            v1++;
        }
        return v2;
    }

    public final void a(android.view.View p11, int p12)
    {
        int v1_2;
        int v0_1 = ((android.support.design.widget.w) p11.getLayoutParams());
        if ((v0_1.g != null) || (v0_1.f == -1)) {
            v1_2 = 0;
        } else {
            v1_2 = 1;
        }
        if (v1_2 == 0) {
            if (v0_1.g == null) {
                if (v0_1.e < 0) {
                    int v0_3 = ((android.support.design.widget.w) p11.getLayoutParams());
                    int v3_1 = this.i;
                    v3_1.set((this.getPaddingLeft() + v0_3.leftMargin), (this.getPaddingTop() + v0_3.topMargin), ((this.getWidth() - this.getPaddingRight()) - v0_3.rightMargin), ((this.getHeight() - this.getPaddingBottom()) - v0_3.bottomMargin));
                    if ((this.y != null) && ((android.support.v4.view.cx.u(this)) && (!android.support.v4.view.cx.u(p11)))) {
                        v3_1.left = (v3_1.left + this.y.a());
                        v3_1.top = (v3_1.top + this.y.b());
                        v3_1.right = (v3_1.right - this.y.c());
                        v3_1.bottom = (v3_1.bottom - this.y.d());
                    }
                    int v4_5 = this.j;
                    android.support.v4.view.v.a(android.support.design.widget.CoordinatorLayout.b(v0_3.c), p11.getMeasuredWidth(), p11.getMeasuredHeight(), v3_1, v4_5, p12);
                    p11.layout(v4_5.left, v4_5.top, v4_5.right, v4_5.bottom);
                } else {
                    int v1_20 = v0_1.e;
                    int v0_8 = ((android.support.design.widget.w) p11.getLayoutParams());
                    int v4_8 = android.support.v4.view.v.a(android.support.design.widget.CoordinatorLayout.c(v0_8.c), p12);
                    int v5_6 = (v4_8 & 7);
                    int v4_9 = (v4_8 & 112);
                    int v6_2 = this.getWidth();
                    int v7 = this.getHeight();
                    int v8 = p11.getMeasuredWidth();
                    int v9 = p11.getMeasuredHeight();
                    if (p12 == 1) {
                        v1_20 = (v6_2 - v1_20);
                    }
                    int v3_3;
                    int v1_22 = (this.a(v1_20) - v8);
                    switch (v5_6) {
                        case 1:
                            v3_3 = (v1_22 + (v8 / 2));
                            break;
                        case 5:
                            v3_3 = (v1_22 + v8);
                            break;
                        default:
                            v3_3 = v1_22;
                    }
                    int v1_25;
                    switch (v4_9) {
                        case 16:
                            v1_25 = ((v9 / 2) + 0);
                            break;
                        case 80:
                            v1_25 = (v9 + 0);
                            break;
                        default:
                            v1_25 = 0;
                    }
                    int v2_16 = Math.max((this.getPaddingLeft() + v0_8.leftMargin), Math.min(v3_3, (((v6_2 - this.getPaddingRight()) - v8) - v0_8.rightMargin)));
                    int v0_12 = Math.max((this.getPaddingTop() + v0_8.topMargin), Math.min(v1_25, (((v7 - this.getPaddingBottom()) - v9) - v0_8.bottomMargin)));
                    p11.layout(v2_16, v0_12, (v2_16 + v8), (v0_12 + v9));
                }
            } else {
                int v0_13 = v0_1.g;
                p11.getLayoutParams();
                int v1_28 = this.i;
                int v2_17 = this.j;
                android.support.design.widget.cz.a(this, v0_13, v1_28);
                this.a(p11, p12, v1_28, v2_17);
                p11.layout(v2_17.left, v2_17.top, v2_17.right, v2_17.bottom);
            }
            return;
        } else {
            throw new IllegalStateException("An anchor may not be changed after CoordinatorLayout measurement begins before layout is complete.");
        }
    }

    public final void a(android.view.View p7, int p8, int p9, int p10)
    {
        this.measureChildWithMargins(p7, p8, p9, p10, 0);
        return;
    }

    final void a(android.view.View p5, boolean p6, android.graphics.Rect p7)
    {
        if ((!p5.isLayoutRequested()) && (p5.getVisibility() != 8)) {
            if (!p6) {
                p7.set(p5.getLeft(), p5.getTop(), p5.getRight(), p5.getBottom());
            } else {
                android.support.design.widget.cz.a(this, p5, p7);
            }
        } else {
            p7.set(0, 0, 0, 0);
        }
        return;
    }

    final void a(boolean p13)
    {
        int v6 = android.support.v4.view.cx.f(this);
        int v7 = this.h.size();
        int v5 = 0;
        while (v5 < v7) {
            int v0_3 = ((android.view.View) this.h.get(v5));
            int v1_1 = ((android.support.design.widget.w) v0_3.getLayoutParams());
            int v3_0 = 0;
            while (v3_0 < v5) {
                if (v1_1.h == ((android.view.View) this.h.get(v3_0))) {
                    android.view.View v2_7 = ((android.support.design.widget.w) v0_3.getLayoutParams());
                    if (v2_7.g != null) {
                        android.support.design.widget.t v8_3 = this.i;
                        boolean v9_2 = this.j;
                        int v10_0 = this.o;
                        android.support.design.widget.cz.a(this, v2_7.g, v8_3);
                        this.a(v0_3, 0, v9_2);
                        this.a(v0_3, v6, v8_3, v10_0);
                        android.support.design.widget.t v8_5 = (v10_0.left - v9_2.left);
                        boolean v9_4 = (v10_0.top - v9_2.top);
                        if (v8_5 != null) {
                            v0_3.offsetLeftAndRight(v8_5);
                        }
                        if (v9_4) {
                            v0_3.offsetTopAndBottom(v9_4);
                        }
                        if ((v8_5 != null) || (v9_4)) {
                            android.support.design.widget.t v8_6 = v2_7.a;
                            if (v8_6 != null) {
                                v8_6.a(this, v0_3, v2_7.g);
                            }
                        }
                    }
                }
                v3_0++;
            }
            android.view.View v2_0 = this.i;
            int v3_1 = this.j;
            v2_0.set(((android.support.design.widget.w) v0_3.getLayoutParams()).l);
            this.a(v0_3, 1, v3_1);
            if (!v2_0.equals(v3_1)) {
                ((android.support.design.widget.w) v0_3.getLayoutParams()).l.set(v3_1);
                int v3_2 = (v5 + 1);
                while (v3_2 < v7) {
                    int v1_13 = ((android.view.View) this.h.get(v3_2));
                    android.view.View v2_2 = ((android.support.design.widget.w) v1_13.getLayoutParams());
                    android.support.design.widget.t v8_0 = v2_2.a;
                    if ((v8_0 != null) && (v8_0.b(v0_3))) {
                        if ((p13) || (!v2_2.k)) {
                            v8_0.a(this, v1_13, v0_3);
                            if (p13) {
                                v2_2.k = 0;
                            }
                        } else {
                            v2_2.k = 0;
                        }
                    }
                    v3_2++;
                }
            }
            v5++;
        }
        return;
    }

    public final boolean a(android.view.View p2, int p3, int p4)
    {
        boolean v0_0 = this.i;
        android.support.design.widget.cz.a(this, p2, v0_0);
        return v0_0.contains(p3, p4);
    }

    protected boolean checkLayoutParams(android.view.ViewGroup$LayoutParams p2)
    {
        if ((!(p2 instanceof android.support.design.widget.w)) || (!super.checkLayoutParams(p2))) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    protected boolean drawChild(android.graphics.Canvas p2, android.view.View p3, long p4)
    {
        p3.getLayoutParams();
        return super.drawChild(p2, p3, p4);
    }

    protected synthetic android.view.ViewGroup$LayoutParams generateDefaultLayoutParams()
    {
        return new android.support.design.widget.w();
    }

    public synthetic android.view.ViewGroup$LayoutParams generateLayoutParams(android.util.AttributeSet p3)
    {
        return new android.support.design.widget.w(this.getContext(), p3);
    }

    protected synthetic android.view.ViewGroup$LayoutParams generateLayoutParams(android.view.ViewGroup$LayoutParams p2)
    {
        android.support.design.widget.w v0_3;
        if (!(p2 instanceof android.support.design.widget.w)) {
            if (!(p2 instanceof android.view.ViewGroup$MarginLayoutParams)) {
                v0_3 = new android.support.design.widget.w(p2);
            } else {
                v0_3 = new android.support.design.widget.w(((android.view.ViewGroup$MarginLayoutParams) p2));
            }
        } else {
            v0_3 = new android.support.design.widget.w(((android.support.design.widget.w) p2));
        }
        return v0_3;
    }

    public int getNestedScrollAxes()
    {
        return this.C.a;
    }

    public android.graphics.drawable.Drawable getStatusBarBackground()
    {
        return this.A;
    }

    protected int getSuggestedMinimumHeight()
    {
        return Math.max(super.getSuggestedMinimumHeight(), (this.getPaddingTop() + this.getPaddingBottom()));
    }

    protected int getSuggestedMinimumWidth()
    {
        return Math.max(super.getSuggestedMinimumWidth(), (this.getPaddingLeft() + this.getPaddingRight()));
    }

    public void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        this.a();
        if (this.x) {
            if (this.w == null) {
                this.w = new android.support.design.widget.x(this);
            }
            this.getViewTreeObserver().addOnPreDrawListener(this.w);
        }
        if ((this.y == null) && (android.support.v4.view.cx.u(this))) {
            android.support.v4.view.cx.t(this);
        }
        this.r = 1;
        return;
    }

    public void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        this.a();
        if ((this.x) && (this.w != null)) {
            this.getViewTreeObserver().removeOnPreDrawListener(this.w);
        }
        if (this.v != null) {
            this.onStopNestedScroll(this.v);
        }
        this.r = 0;
        return;
    }

    public void onDraw(android.graphics.Canvas p5)
    {
        super.onDraw(p5);
        if ((this.z) && (this.A != null)) {
            android.graphics.drawable.Drawable v0_3;
            if (this.y == null) {
                v0_3 = 0;
            } else {
                v0_3 = this.y.b();
            }
            if (v0_3 > null) {
                this.A.setBounds(0, 0, this.getWidth(), v0_3);
                this.A.draw(p5);
            }
        }
        return;
    }

    public boolean onInterceptTouchEvent(android.view.MotionEvent p4)
    {
        int v0 = android.support.v4.view.bk.a(p4);
        if (v0 == 0) {
            this.a();
        }
        boolean v1_1 = this.a(p4, 0);
        if ((v0 == 1) || (v0 == 3)) {
            this.a();
        }
        return v1_1;
    }

    protected void onLayout(boolean p6, int p7, int p8, int p9, int p10)
    {
        int v3 = android.support.v4.view.cx.f(this);
        int v4 = this.h.size();
        int v2 = 0;
        while (v2 < v4) {
            int v0_4 = ((android.view.View) this.h.get(v2));
            boolean v1_2 = ((android.support.design.widget.w) v0_4.getLayoutParams()).a;
            if ((!v1_2) || (!v1_2.a(this, v0_4, v3))) {
                this.a(v0_4, v3);
            }
            v2++;
        }
        return;
    }

    protected void onMeasure(int p25, int p26)
    {
        android.view.View v3_0;
        int v5_0 = this.getChildCount();
        if (this.h.size() == v5_0) {
            v3_0 = 0;
        } else {
            v3_0 = 1;
        }
        int v4_0 = 0;
        while (v4_0 < v5_0) {
            int v6_1 = this.getChildAt(v4_0);
            android.support.design.widget.w v7_2 = android.support.design.widget.CoordinatorLayout.b(v6_1);
            if (v7_2.f != -1) {
                if (v7_2.g != null) {
                    int v1_50;
                    if (v7_2.g.getId() == v7_2.f) {
                        int v1_49 = v7_2.g;
                        int v2_26 = v7_2.g.getParent();
                        while (v2_26 != this) {
                            if ((v2_26 != 0) && (v2_26 != v6_1)) {
                                if ((v2_26 instanceof android.view.View)) {
                                    v1_49 = ((android.view.View) v2_26);
                                }
                                v2_26 = v2_26.getParent();
                            } else {
                                v7_2.h = 0;
                                v7_2.g = 0;
                                v1_50 = 0;
                            }
                        }
                        v7_2.h = v1_49;
                        v1_50 = 1;
                    } else {
                        v1_50 = 0;
                    }
                    if (v1_50 != 0) {
                        v4_0++;
                    }
                }
                v7_2.g = this.findViewById(v7_2.f);
                if (v7_2.g == null) {
                    if (!this.isInEditMode()) {
                        throw new IllegalStateException(new StringBuilder("Could not find CoordinatorLayout descendant view with id ").append(this.getResources().getResourceName(v7_2.f)).append(" to anchor view ").append(v6_1).toString());
                    } else {
                        v7_2.h = 0;
                        v7_2.g = 0;
                    }
                } else {
                    int v1_60 = v7_2.g;
                    int v2_34 = v7_2.g.getParent();
                    while ((v2_34 != this) && (v2_34 != 0)) {
                        if (v2_34 != v6_1) {
                            if ((v2_34 instanceof android.view.View)) {
                                v1_60 = ((android.view.View) v2_34);
                            }
                            v2_34 = v2_34.getParent();
                        } else {
                            if (!this.isInEditMode()) {
                                throw new IllegalStateException("Anchor must not be a descendant of the anchored view");
                            } else {
                                v7_2.h = 0;
                                v7_2.g = 0;
                            }
                        }
                    }
                    v7_2.h = v1_60;
                }
            } else {
                v7_2.h = 0;
                v7_2.g = 0;
            }
        }
        if (v3_0 != null) {
            this.h.clear();
            int v1_6 = 0;
            while (v1_6 < v5_0) {
                this.h.add(this.getChildAt(v1_6));
                v1_6++;
            }
            java.util.Collections.sort(this.h, this.e);
        }
        boolean v8_0;
        void v24_1 = this.c();
        int v14 = v24_1.getPaddingLeft();
        int v2_1 = v24_1.getPaddingTop();
        int v15 = v24_1.getPaddingRight();
        android.view.View v3_1 = v24_1.getPaddingBottom();
        int v16 = android.support.v4.view.cx.f(v24_1);
        if (v16 != 1) {
            v8_0 = 0;
        } else {
            v8_0 = 1;
        }
        int v9;
        int v17 = android.view.View$MeasureSpec.getMode(p25);
        int v18 = android.view.View$MeasureSpec.getSize(p25);
        int v19 = android.view.View$MeasureSpec.getMode(p26);
        int v20 = android.view.View$MeasureSpec.getSize(p26);
        int v21 = (v14 + v15);
        int v22 = (v2_1 + v3_1);
        int v4_1 = v24_1.getSuggestedMinimumWidth();
        android.view.View v3_2 = v24_1.getSuggestedMinimumHeight();
        if ((v24_1.y == null) || (!android.support.v4.view.cx.u(v24_1))) {
            v9 = 0;
        } else {
            v9 = 1;
        }
        int v23 = v24_1.h.size();
        int v10 = 0;
        int v11 = 0;
        int v12 = v3_2;
        int v13 = v4_1;
        while (v10 < v23) {
            android.view.View v3_4 = ((android.view.View) v24_1.h.get(v10));
            android.support.design.widget.w v7_1 = ((android.support.design.widget.w) v3_4.getLayoutParams());
            int v5_1 = 0;
            if ((v7_1.e >= 0) && (v17 != 0)) {
                int v1_24 = v24_1.a(v7_1.e);
                int v2_8 = (android.support.v4.view.v.a(android.support.design.widget.CoordinatorLayout.c(v7_1.c), v16) & 7);
                if (((v2_8 != 3) || (v8_0)) && ((v2_8 != 5) || (!v8_0))) {
                    if (((v2_8 == 5) && (!v8_0)) || ((v2_8 == 3) && (v8_0))) {
                        v5_1 = Math.max(0, (v1_24 - v14));
                    }
                } else {
                    v5_1 = Math.max(0, ((v18 - v15) - v1_24));
                }
            }
            if ((v9 == 0) || (android.support.v4.view.cx.u(v3_4))) {
                int v6_0 = p26;
                int v4_7 = p25;
            } else {
                int v1_30 = (v24_1.y.a() + v24_1.y.c());
                int v2_15 = (v24_1.y.b() + v24_1.y.d());
                v4_7 = android.view.View$MeasureSpec.makeMeasureSpec((v18 - v1_30), v17);
                v6_0 = android.view.View$MeasureSpec.makeMeasureSpec((v20 - v2_15), v19);
            }
            int v1_33 = v7_1.a;
            if ((v1_33 == 0) || (!v1_33.a(v24_1, v3_4, v4_7, v5_1, v6_0))) {
                v24_1.a(v3_4, v4_7, v5_1, v6_0);
            }
            v10++;
            v11 = android.support.v4.view.cx.a(v11, android.support.v4.view.cx.j(v3_4));
            v12 = Math.max(v12, (((v3_4.getMeasuredHeight() + v22) + v7_1.topMargin) + v7_1.bottomMargin));
            v13 = Math.max(v13, (((v3_4.getMeasuredWidth() + v21) + v7_1.leftMargin) + v7_1.rightMargin));
        }
        v24_1.setMeasuredDimension(android.support.v4.view.cx.a(v13, p25, (-16777216 & v11)), android.support.v4.view.cx.a(v12, p26, (v11 << 16)));
        return;
    }

    public boolean onNestedFling(android.view.View p7, float p8, float p9, boolean p10)
    {
        int v3 = this.getChildCount();
        int v2 = 0;
        int v1_0 = 0;
        while (v2 < v3) {
            int v0_5;
            android.view.View v4 = this.getChildAt(v2);
            int v0_3 = ((android.support.design.widget.w) v4.getLayoutParams());
            if (!v0_3.j) {
                v0_5 = v1_0;
            } else {
                int v0_4 = v0_3.a;
                if (v0_4 == 0) {
                } else {
                    v0_5 = (v0_4.a(this, v4, p9, p10) | v1_0);
                }
            }
            v2++;
            v1_0 = v0_5;
        }
        if (v1_0 != 0) {
            this.a(1);
        }
        return v1_0;
    }

    public boolean onNestedPreFling(android.view.View p5, float p6, float p7)
    {
        int v2 = this.getChildCount();
        int v0 = 0;
        while (v0 < v2) {
            this.getChildAt(v0).getLayoutParams();
            v0++;
        }
        return 0;
    }

    public void onNestedPreScroll(android.view.View p11, int p12, int p13, int[] p14)
    {
        int v7 = this.getChildCount();
        int v5 = 0;
        int v2_0 = 0;
        int v3_0 = 0;
        int v4_0 = 0;
        while (v5 < v7) {
            int v3_1;
            int v2_1;
            int v0_3;
            android.view.View v8 = this.getChildAt(v5);
            int v0_1 = ((android.support.design.widget.w) v8.getLayoutParams());
            if (!v0_1.j) {
                v0_3 = v2_0;
                v2_1 = v3_0;
                v3_1 = v4_0;
            } else {
                int v0_2 = v0_1.a;
                if (v0_2 == 0) {
                } else {
                    int v2_4;
                    int v2_2 = this.p;
                    this.p[1] = 0;
                    v2_2[0] = 0;
                    v0_2.a(this, v8, p13, this.p);
                    if (p12 <= 0) {
                        v2_4 = Math.min(v4_0, this.p[0]);
                    } else {
                        v2_4 = Math.max(v4_0, this.p[0]);
                    }
                    int v0_10;
                    if (p13 <= 0) {
                        v0_10 = Math.min(v3_0, this.p[1]);
                    } else {
                        v0_10 = Math.max(v3_0, this.p[1]);
                    }
                    v3_1 = v2_4;
                    v2_1 = v0_10;
                    v0_3 = 1;
                }
            }
            v5++;
            v4_0 = v3_1;
            v3_0 = v2_1;
            v2_0 = v0_3;
        }
        p14[0] = v4_0;
        p14[1] = v3_0;
        if (v2_0 != 0) {
            this.a(1);
        }
        return;
    }

    public void onNestedScroll(android.view.View p8, int p9, int p10, int p11, int p12)
    {
        int v4 = this.getChildCount();
        int v3 = 0;
        int v2_0 = 0;
        while (v3 < v4) {
            int v0_4;
            android.view.View v5 = this.getChildAt(v3);
            int v0_2 = ((android.support.design.widget.w) v5.getLayoutParams());
            if (!v0_2.j) {
                v0_4 = v2_0;
            } else {
                int v0_3 = v0_2.a;
                if (v0_3 == 0) {
                } else {
                    v0_3.b(this, v5, p12);
                    v0_4 = 1;
                }
            }
            v3++;
            v2_0 = v0_4;
        }
        if (v2_0 != 0) {
            this.a(1);
        }
        return;
    }

    public void onNestedScrollAccepted(android.view.View p4, android.view.View p5, int p6)
    {
        this.C.a = p6;
        this.u = p4;
        this.v = p5;
        int v1 = this.getChildCount();
        int v0_1 = 0;
        while (v0_1 < v1) {
            this.getChildAt(v0_1).getLayoutParams();
            v0_1++;
        }
        return;
    }

    protected void onRestoreInstanceState(android.os.Parcelable p8)
    {
        super.onRestoreInstanceState(((android.support.design.widget.CoordinatorLayout$SavedState) p8).getSuperState());
        android.util.SparseArray v2 = ((android.support.design.widget.CoordinatorLayout$SavedState) p8).a;
        int v3 = this.getChildCount();
        int v1 = 0;
        while (v1 < v3) {
            android.view.View v4 = this.getChildAt(v1);
            android.os.Parcelable v0_2 = v4.getId();
            android.support.design.widget.t v5_1 = android.support.design.widget.CoordinatorLayout.b(v4).a;
            if ((v0_2 != -1) && (v5_1 != null)) {
                android.os.Parcelable v0_4 = ((android.os.Parcelable) v2.get(v0_2));
                if (v0_4 != null) {
                    v5_1.a(this, v4, v0_4);
                }
            }
            v1++;
        }
        return;
    }

    protected android.os.Parcelable onSaveInstanceState()
    {
        android.support.design.widget.CoordinatorLayout$SavedState v2_1 = new android.support.design.widget.CoordinatorLayout$SavedState(super.onSaveInstanceState());
        android.util.SparseArray v3_1 = new android.util.SparseArray();
        int v4 = this.getChildCount();
        int v1 = 0;
        while (v1 < v4) {
            android.view.View v5 = this.getChildAt(v1);
            int v6 = v5.getId();
            android.os.Parcelable v0_4 = ((android.support.design.widget.w) v5.getLayoutParams()).a;
            if ((v6 != -1) && (v0_4 != null)) {
                android.os.Parcelable v0_5 = v0_4.a(this, v5);
                if (v0_5 != null) {
                    v3_1.append(v6, v0_5);
                }
            }
            v1++;
        }
        v2_1.a = v3_1;
        return v2_1;
    }

    public boolean onStartNestedScroll(android.view.View p8, android.view.View p9, int p10)
    {
        int v4 = this.getChildCount();
        int v2 = 0;
        int v1_0 = 0;
        while (v2 < v4) {
            int v0_2;
            boolean v5_0 = this.getChildAt(v2);
            int v0_1 = ((android.support.design.widget.w) v5_0.getLayoutParams());
            android.support.design.widget.t v6 = v0_1.a;
            if (v6 == null) {
                v0_1.j = 0;
                v0_2 = v1_0;
            } else {
                boolean v5_1 = v6.a(this, v5_0, p8, p10);
                int v1_1 = (v1_0 | v5_1);
                v0_1.j = v5_1;
                v0_2 = v1_1;
            }
            v2++;
            v1_0 = v0_2;
        }
        return v1_0;
    }

    public void onStopNestedScroll(android.view.View p7)
    {
        this.C.a = 0;
        int v3 = this.getChildCount();
        int v1 = 0;
        while (v1 < v3) {
            int v0_3 = ((android.support.design.widget.w) this.getChildAt(v1).getLayoutParams());
            if (v0_3.j) {
                android.support.design.widget.t v4_1 = v0_3.a;
                if (v4_1 != null) {
                    v4_1.a(p7);
                }
                v0_3.j = 0;
                v0_3.k = 0;
            }
            v1++;
        }
        this.u = 0;
        this.v = 0;
        return;
    }

    public boolean onTouchEvent(android.view.MotionEvent p12)
    {
        int v8;
        boolean v1;
        int v9 = android.support.v4.view.bk.a(p12);
        if (this.t != null) {
            v1 = 0;
            long v0_5 = ((android.support.design.widget.w) this.t.getLayoutParams()).a;
            if (v0_5 == 0) {
                v8 = 0;
            } else {
                v8 = v0_5.a(this, this.t, p12);
            }
        } else {
            long v0_1 = this.a(p12, 1);
            if (v0_1 == 0) {
                v1 = v0_1;
                v8 = 0;
            } else {
                v1 = v0_1;
            }
        }
        long v0_8;
        if (this.t != null) {
            if (!v1) {
                v0_8 = 0;
            } else {
                long v0_9 = android.os.SystemClock.uptimeMillis();
                v0_8 = android.view.MotionEvent.obtain(v0_9, v0_9, 3, 0, 0, 0);
                super.onTouchEvent(v0_8);
            }
        } else {
            v8 |= super.onTouchEvent(p12);
            v0_8 = 0;
        }
        if (v0_8 != 0) {
            v0_8.recycle();
        }
        if ((v9 == 1) || (v9 == 3)) {
            this.a();
        }
        return v8;
    }

    public void requestDisallowInterceptTouchEvent(boolean p1)
    {
        super.requestDisallowInterceptTouchEvent(p1);
        if (p1) {
            this.a();
        }
        return;
    }

    public void setOnHierarchyChangeListener(android.view.ViewGroup$OnHierarchyChangeListener p1)
    {
        this.B = p1;
        return;
    }

    public void setStatusBarBackground(android.graphics.drawable.Drawable p1)
    {
        this.A = p1;
        this.invalidate();
        return;
    }

    public void setStatusBarBackgroundColor(int p2)
    {
        this.setStatusBarBackground(new android.graphics.drawable.ColorDrawable(p2));
        return;
    }

    public void setStatusBarBackgroundResource(int p2)
    {
        int v0_0;
        if (p2 == 0) {
            v0_0 = 0;
        } else {
            v0_0 = android.support.v4.c.h.a(this.getContext(), p2);
        }
        this.setStatusBarBackground(v0_0);
        return;
    }
}
