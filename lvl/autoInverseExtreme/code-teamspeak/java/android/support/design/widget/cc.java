package android.support.design.widget;
final class cc extends android.widget.LinearLayout implements android.view.View$OnLongClickListener {
    final android.support.design.widget.bz a;
    final synthetic android.support.design.widget.br b;
    private android.widget.TextView c;
    private android.widget.ImageView d;
    private android.view.View e;
    private android.widget.TextView f;
    private android.widget.ImageView g;

    public cc(android.support.design.widget.br p5, android.content.Context p6, android.support.design.widget.bz p7)
    {
        this.b = p5;
        this(p6);
        this.a = p7;
        if (android.support.design.widget.br.a(p5) != 0) {
            this.setBackgroundDrawable(android.support.v7.internal.widget.av.a(p6, android.support.design.widget.br.a(p5)));
        }
        android.support.v4.view.cx.b(this, android.support.design.widget.br.b(p5), android.support.design.widget.br.c(p5), android.support.design.widget.br.d(p5), android.support.design.widget.br.e(p5));
        this.setGravity(17);
        this.a();
        return;
    }

    private void a(android.support.design.widget.bz p6, android.widget.TextView p7, android.widget.ImageView p8)
    {
        boolean v0_0 = p6.b;
        CharSequence v2_0 = p6.c;
        if (p8 != null) {
            if (!v0_0) {
                p8.setVisibility(8);
                p8.setImageDrawable(0);
            } else {
                p8.setImageDrawable(v0_0);
                p8.setVisibility(0);
                this.setVisibility(0);
            }
            p8.setContentDescription(p6.d);
        }
        boolean v0_3;
        if (android.text.TextUtils.isEmpty(v2_0)) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        if (p7 != null) {
            if (!v0_3) {
                p7.setVisibility(8);
                p7.setText(0);
            } else {
                p7.setText(v2_0);
                p7.setContentDescription(p6.d);
                p7.setVisibility(0);
                this.setVisibility(0);
            }
        }
        if ((v0_3) || (android.text.TextUtils.isEmpty(p6.d))) {
            this.setOnLongClickListener(0);
            this.setLongClickable(0);
        } else {
            this.setOnLongClickListener(this);
        }
        return;
    }

    private android.support.design.widget.bz b()
    {
        return this.a;
    }

    final void a()
    {
        android.support.design.widget.bz v1 = this.a;
        android.widget.ImageView v2_0 = v1.f;
        if (v2_0 == null) {
            if (this.e != null) {
                this.removeView(this.e);
                this.e = 0;
            }
            this.f = 0;
            this.g = 0;
        } else {
            android.widget.TextView v0_2 = v2_0.getParent();
            if (v0_2 != this) {
                if (v0_2 != null) {
                    ((android.view.ViewGroup) v0_2).removeView(v2_0);
                }
                this.addView(v2_0);
            }
            this.e = v2_0;
            if (this.c != null) {
                this.c.setVisibility(8);
            }
            if (this.d != null) {
                this.d.setVisibility(8);
                this.d.setImageDrawable(0);
            }
            this.f = ((android.widget.TextView) v2_0.findViewById(16908308));
            this.g = ((android.widget.ImageView) v2_0.findViewById(16908294));
        }
        if (this.e != null) {
            if ((this.f != null) || (this.g != null)) {
                this.a(v1, this.f, this.g);
            }
        } else {
            if (this.d == null) {
                android.widget.TextView v0_23 = ((android.widget.ImageView) android.view.LayoutInflater.from(this.getContext()).inflate(android.support.design.k.design_layout_tab_icon, this, 0));
                this.addView(v0_23, 0);
                this.d = v0_23;
            }
            if (this.c == null) {
                android.widget.TextView v0_28 = ((android.widget.TextView) android.view.LayoutInflater.from(this.getContext()).inflate(android.support.design.k.design_layout_tab_text, this, 0));
                this.addView(v0_28);
                this.c = v0_28;
            }
            this.c.setTextAppearance(this.getContext(), android.support.design.widget.br.h(this.b));
            if (android.support.design.widget.br.i(this.b) != null) {
                this.c.setTextColor(android.support.design.widget.br.i(this.b));
            }
            this.a(v1, this.c, this.d);
        }
        return;
    }

    public final void onInitializeAccessibilityEvent(android.view.accessibility.AccessibilityEvent p2)
    {
        super.onInitializeAccessibilityEvent(p2);
        p2.setClassName(android.support.v7.app.g.getName());
        return;
    }

    public final void onInitializeAccessibilityNodeInfo(android.view.accessibility.AccessibilityNodeInfo p2)
    {
        super.onInitializeAccessibilityNodeInfo(p2);
        p2.setClassName(android.support.v7.app.g.getName());
        return;
    }

    public final boolean onLongClick(android.view.View p8)
    {
        int v0_1 = new int[2];
        this.getLocationOnScreen(v0_1);
        android.widget.Toast v1_0 = this.getContext();
        int v2_0 = this.getWidth();
        int v3 = this.getHeight();
        int v4_2 = v1_0.getResources().getDisplayMetrics().widthPixels;
        android.widget.Toast v1_1 = android.widget.Toast.makeText(v1_0, this.a.d, 0);
        v1_1.setGravity(49, ((v0_1[0] + (v2_0 / 2)) - (v4_2 / 2)), v3);
        v1_1.show();
        return 1;
    }

    public final void onMeasure(int p4, int p5)
    {
        super.onMeasure(p4, p5);
        int v0_0 = this.getMeasuredWidth();
        if ((v0_0 < android.support.design.widget.br.f(this.b)) || (v0_0 > android.support.design.widget.br.g(this.b))) {
            super.onMeasure(android.view.View$MeasureSpec.makeMeasureSpec(android.support.design.widget.an.a(v0_0, android.support.design.widget.br.f(this.b), android.support.design.widget.br.g(this.b)), 1073741824), p5);
        }
        return;
    }

    public final void setSelected(boolean p2)
    {
        android.widget.ImageView v0_1;
        if (this.isSelected() == p2) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        super.setSelected(p2);
        if ((v0_1 != null) && (p2)) {
            this.sendAccessibilityEvent(4);
            if (this.c != null) {
                this.c.setSelected(p2);
            }
            if (this.d != null) {
                this.d.setSelected(p2);
            }
        }
        return;
    }
}
