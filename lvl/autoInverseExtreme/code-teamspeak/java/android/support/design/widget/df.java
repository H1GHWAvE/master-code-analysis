package android.support.design.widget;
 class df extends android.support.design.widget.t {
    private android.support.design.widget.dg a;
    private int b;
    private int c;

    public df()
    {
        this.b = 0;
        this.c = 0;
        return;
    }

    public df(android.content.Context p2, android.util.AttributeSet p3)
    {
        this(p2, p3);
        this.b = 0;
        this.c = 0;
        return;
    }

    public boolean a(int p2)
    {
        int v0_1;
        if (this.a == null) {
            this.c = p2;
            v0_1 = 0;
        } else {
            v0_1 = this.a.b(p2);
        }
        return v0_1;
    }

    public boolean a(android.support.design.widget.CoordinatorLayout p4, android.view.View p5, int p6)
    {
        p4.a(p5, p6);
        if (this.a == null) {
            this.a = new android.support.design.widget.dg(p5);
        }
        this.a.a();
        if (this.b != 0) {
            this.a.a(this.b);
            this.b = 0;
        }
        if (this.c != 0) {
            this.a.b(this.c);
            this.c = 0;
        }
        return 1;
    }

    public int b()
    {
        int v0_1;
        if (this.a == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.a.b;
        }
        return v0_1;
    }

    public boolean b(int p2)
    {
        int v0_1;
        if (this.a == null) {
            this.b = p2;
            v0_1 = 0;
        } else {
            v0_1 = this.a.a(p2);
        }
        return v0_1;
    }

    public int c()
    {
        int v0_1;
        if (this.a == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.a.a;
        }
        return v0_1;
    }
}
