package android.support.design.widget;
final class k extends android.support.design.widget.j {
    private android.content.res.ColorStateList k;
    private android.graphics.PorterDuff$Mode l;
    private android.graphics.PorterDuffColorFilter m;

    k()
    {
        this.l = android.graphics.PorterDuff$Mode.SRC_IN;
        return;
    }

    private android.graphics.PorterDuffColorFilter a(android.content.res.ColorStateList p3, android.graphics.PorterDuff$Mode p4)
    {
        if ((p3 != null) && (p4 != null)) {
            android.graphics.PorterDuffColorFilter v0_2 = new android.graphics.PorterDuffColorFilter(p3.getColorForState(this.getState(), 0), p4);
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public final void draw(android.graphics.Canvas p3)
    {
        if ((this.m == null) || (this.a.getColorFilter() != null)) {
            android.graphics.Paint v0_3 = 0;
        } else {
            this.a.setColorFilter(this.m);
            v0_3 = 1;
        }
        super.draw(p3);
        if (v0_3 != null) {
            this.a.setColorFilter(0);
        }
        return;
    }

    public final void getOutline(android.graphics.Outline p2)
    {
        this.copyBounds(this.b);
        p2.setOval(this.b);
        return;
    }

    public final void setTintList(android.content.res.ColorStateList p2)
    {
        this.k = p2;
        this.m = this.a(p2, this.l);
        this.invalidateSelf();
        return;
    }

    public final void setTintMode(android.graphics.PorterDuff$Mode p2)
    {
        this.l = p2;
        this.m = this.a(this.k, p2);
        this.invalidateSelf();
        return;
    }
}
