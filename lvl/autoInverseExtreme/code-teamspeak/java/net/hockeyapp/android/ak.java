package net.hockeyapp.android;
public final class ak {
    private static final String a = "startTime";
    private static final String b = "usageTime";

    public ak()
    {
        return;
    }

    private static void a(android.app.Activity p5)
    {
        long v0 = System.currentTimeMillis();
        if (p5 != null) {
            android.content.SharedPreferences$Editor v2_2 = p5.getSharedPreferences("HockeyApp", 0).edit();
            v2_2.putLong(new StringBuilder("startTime").append(p5.hashCode()).toString(), v0);
            net.hockeyapp.android.e.n.a(v2_2);
        }
        return;
    }

    public static boolean a(android.content.Context p1)
    {
        int v0_2;
        if (net.hockeyapp.android.a.b != null) {
            v0_2 = 1;
        } else {
            net.hockeyapp.android.a.a(p1);
            if (net.hockeyapp.android.a.b != null) {
            } else {
                v0_2 = 0;
            }
        }
        return v0_2;
    }

    private static long b(android.content.Context p5)
    {
        long v0_0 = 0;
        if (net.hockeyapp.android.ak.a(p5)) {
            v0_0 = (p5.getSharedPreferences("HockeyApp", 0).getLong(new StringBuilder("usageTime").append(net.hockeyapp.android.a.b).toString(), 0) / 1000);
        }
        return v0_0;
    }

    private static void b(android.app.Activity p10)
    {
        long v0_0 = System.currentTimeMillis();
        if ((p10 != null) && (net.hockeyapp.android.ak.a(p10))) {
            android.content.SharedPreferences$Editor v2_2 = p10.getSharedPreferences("HockeyApp", 0);
            String v4_2 = v2_2.getLong(new StringBuilder("startTime").append(p10.hashCode()).toString(), 0);
            long v6_2 = v2_2.getLong(new StringBuilder("usageTime").append(net.hockeyapp.android.a.b).toString(), 0);
            if (v4_2 > 0) {
                long v0_1 = (v0_0 - v4_2);
                android.content.SharedPreferences$Editor v2_3 = v2_2.edit();
                v2_3.putLong(new StringBuilder("usageTime").append(net.hockeyapp.android.a.b).toString(), (v0_1 + v6_2));
                net.hockeyapp.android.e.n.a(v2_3);
            }
        }
        return;
    }

    private static android.content.SharedPreferences c(android.content.Context p2)
    {
        return p2.getSharedPreferences("HockeyApp", 0);
    }
}
