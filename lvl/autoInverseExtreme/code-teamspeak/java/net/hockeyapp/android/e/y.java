package net.hockeyapp.android.e;
public final class y {
    public static final String a = "99.0";
    private java.util.ArrayList b;
    private org.json.JSONObject c;
    private net.hockeyapp.android.ax d;
    private int e;

    public y(android.content.Context p3, String p4, net.hockeyapp.android.ax p5)
    {
        this.d = p5;
        this.a(p3, p4);
        java.util.Collections.sort(this.b, new net.hockeyapp.android.e.z(this));
        return;
    }

    public static int a(String p7, String p8)
    {
        boolean v0 = -1;
        if ((p7 != null) && (p8 != null)) {
            try {
                boolean v3_1 = new java.util.Scanner(p7.replaceAll("\\-.*", ""));
                java.util.Scanner v4_3 = new java.util.Scanner(p8.replaceAll("\\-.*", ""));
                v3_1.useDelimiter("\\.");
                v4_3.useDelimiter("\\.");
            } catch (boolean v0) {
                v0 = 0;
            }
            while ((v3_1.hasNextInt()) && (v4_3.hasNextInt())) {
                int v5_7 = v3_1.nextInt();
                int v6_1 = v4_3.nextInt();
                if (v5_7 >= v6_1) {
                    if (v5_7 > v6_1) {
                        v0 = 1;
                    }
                }
            }
            if (!v3_1.hasNextInt()) {
                if (!v4_3.hasNextInt()) {
                    v0 = 0;
                }
            } else {
                v0 = 1;
            }
        } else {
            v0 = 0;
        }
        return v0;
    }

    private static long a(org.json.JSONObject p2, String p3)
    {
        try {
            long v0 = p2.getLong(p3);
        } catch (long v0) {
            v0 = 0;
        }
        return v0;
    }

    private String a(int p7, org.json.JSONObject p8)
    {
        String v0_1 = new StringBuilder();
        String v1_1 = net.hockeyapp.android.e.y.c(this.c);
        int v2 = net.hockeyapp.android.e.y.c(p8);
        String v3_0 = net.hockeyapp.android.e.y.d(p8);
        v0_1.append("<div style=\'padding: 20px 10px 10px;\'><strong>");
        if (p7 != 0) {
            v0_1.append(new StringBuilder("Version ").append(v3_0).append(" (").append(v2).append("): ").toString());
            if ((v2 != v1_1) && (v2 == this.e)) {
                this.e = -1;
                v0_1.append("[INSTALLED]");
            }
        } else {
            v0_1.append("Newest version:");
        }
        v0_1.append("</strong></div>");
        return v0_1.toString();
    }

    private static String a(String p1)
    {
        if ((p1 != null) && (!p1.equalsIgnoreCase("L"))) {
            if (!p1.equalsIgnoreCase("M")) {
                if (java.util.regex.Pattern.matches("^[a-zA-Z]+", p1)) {
                    p1 = "99.0";
                }
            } else {
                p1 = "6.0";
            }
        } else {
            p1 = "5.0";
        }
        return p1;
    }

    private static String a(org.json.JSONObject p4)
    {
        String v0_1 = new StringBuilder();
        String v1_0 = net.hockeyapp.android.e.y.b(p4);
        if (v1_0.length() > 0) {
            v0_1.append(new StringBuilder("<a href=\'restore:").append(v1_0).append("\'  style=\'background: #c8c8c8; color: #000; display: block; float: right; padding: 7px; margin: 0px 10px 10px; text-decoration: none;\'>Restore</a>").toString());
        }
        return v0_1.toString();
    }

    private static String a(org.json.JSONObject p1, String p2, String p3)
    {
        try {
            p3 = p1.getString(p2);
        } catch (org.json.JSONException v0) {
        }
        return p3;
    }

    private void a(android.content.Context p11, String p12)
    {
        this.c = new org.json.JSONObject();
        this.b = new java.util.ArrayList();
        this.e = this.d.getCurrentVersionCode();
        try {
            org.json.JSONArray v6_1 = new org.json.JSONArray(p12);
            int v0_7 = this.d.getCurrentVersionCode();
            int v5 = 0;
        } catch (int v0) {
            return;
        } catch (int v0) {
            return;
        }
        while (v5 < v6_1.length()) {
            int v4;
            org.json.JSONObject v7 = v6_1.getJSONObject(v5);
            if (v7.getInt("version") <= v0_7) {
                v4 = 0;
            } else {
                v4 = 1;
            }
            if ((v7.getInt("version") != v0_7) || (!net.hockeyapp.android.e.y.a(p11, v7.getLong("timestamp")))) {
                int v1_7 = 0;
            } else {
                v1_7 = 1;
            }
            if ((v4 != 0) || (v1_7 != 0)) {
                this.c = v7;
                v0_7 = v7.getInt("version");
            }
            this.b.add(v7);
            v5++;
        }
        return;
    }

    public static boolean a(android.content.Context p7, long p8)
    {
        int v0 = 0;
        try {
            if ((p7 != null) && (p8 > ((new java.io.File(p7.getPackageManager().getApplicationInfo(p7.getPackageName(), 0).sourceDir).lastModified() / 1000) + 1800))) {
                v0 = 1;
            }
        } catch (android.content.pm.PackageManager$NameNotFoundException v1_4) {
            v1_4.printStackTrace();
        }
        return v0;
    }

    private static String b(org.json.JSONObject p2)
    {
        try {
            String v0 = p2.getString("id");
        } catch (org.json.JSONException v1) {
        }
        return v0;
    }

    private static int c(org.json.JSONObject p2)
    {
        try {
            int v0 = p2.getInt("version");
        } catch (org.json.JSONException v1) {
        }
        return v0;
    }

    private static String d(org.json.JSONObject p2)
    {
        try {
            String v0 = p2.getString("shortversion");
        } catch (org.json.JSONException v1) {
        }
        return v0;
    }

    private static String e(org.json.JSONObject p3)
    {
        String v0_1 = new StringBuilder();
        String v1_1 = net.hockeyapp.android.e.y.a(p3, "notes", "");
        v0_1.append("<div style=\'padding: 0px 10px;\'>");
        if (v1_1.trim().length() != 0) {
            v0_1.append(v1_1);
        } else {
            v0_1.append("<em>No information.</em>");
        }
        v0_1.append("</div>");
        return v0_1.toString();
    }

    private void e()
    {
        java.util.Collections.sort(this.b, new net.hockeyapp.android.e.z(this));
        return;
    }

    private static Object f()
    {
        return "<hr style=\'border-top: 1px solid #c8c8c8; border-bottom: 0px; margin: 40px 10px 0px 10px;\' />";
    }

    public final String a()
    {
        return new StringBuilder().append(net.hockeyapp.android.e.y.a(this.c, "shortversion", "")).append(" (").append(net.hockeyapp.android.e.y.a(this.c, "version", "")).append(")").toString();
    }

    public final String b()
    {
        return new java.text.SimpleDateFormat("dd.MM.yyyy").format(new java.util.Date((net.hockeyapp.android.e.y.a(this.c, "timestamp") * 1000)));
    }

    public final long c()
    {
        long v2_1 = Boolean.valueOf(net.hockeyapp.android.e.y.a(this.c, "external", "false")).booleanValue();
        long v0_4 = net.hockeyapp.android.e.y.a(this.c, "appsize");
        if ((v2_1 != 0) && (v0_4 == 0)) {
            v0_4 = -1;
        }
        return v0_4;
    }

    public final String d()
    {
        StringBuilder v2_1 = new StringBuilder();
        v2_1.append("<html>");
        v2_1.append("<body style=\'padding: 0px 0px 20px 0px\'>");
        java.util.Iterator v3 = this.b.iterator();
        int v1_1 = 0;
        while (v3.hasNext()) {
            int v0_8 = ((org.json.JSONObject) v3.next());
            if (v1_1 > 0) {
                v2_1.append("<hr style=\'border-top: 1px solid #c8c8c8; border-bottom: 0px; margin: 40px 10px 0px 10px;\' />");
            }
            StringBuilder v4_2 = new StringBuilder();
            String v5_1 = net.hockeyapp.android.e.y.c(this.c);
            String v6_0 = net.hockeyapp.android.e.y.c(v0_8);
            String v7_0 = net.hockeyapp.android.e.y.d(v0_8);
            v4_2.append("<div style=\'padding: 20px 10px 10px;\'><strong>");
            if (v1_1 != 0) {
                v4_2.append(new StringBuilder("Version ").append(v7_0).append(" (").append(v6_0).append("): ").toString());
                if ((v6_0 != v5_1) && (v6_0 == this.e)) {
                    this.e = -1;
                    v4_2.append("[INSTALLED]");
                }
            } else {
                v4_2.append("Newest version:");
            }
            v4_2.append("</strong></div>");
            v2_1.append(v4_2.toString());
            StringBuilder v4_5 = new StringBuilder();
            int v0_9 = net.hockeyapp.android.e.y.a(v0_8, "notes", "");
            v4_5.append("<div style=\'padding: 0px 10px;\'>");
            if (v0_9.trim().length() != 0) {
                v4_5.append(v0_9);
            } else {
                v4_5.append("<em>No information.</em>");
            }
            v4_5.append("</div>");
            v2_1.append(v4_5.toString());
            v1_1++;
        }
        v2_1.append("</body>");
        v2_1.append("</html>");
        return v2_1.toString();
    }
}
