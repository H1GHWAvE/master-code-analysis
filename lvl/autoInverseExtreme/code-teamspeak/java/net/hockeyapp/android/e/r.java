package net.hockeyapp.android.e;
public final class r {

    private r()
    {
        return;
    }

    synthetic r(byte p1)
    {
        return;
    }

    private static net.hockeyapp.android.e.r a()
    {
        return net.hockeyapp.android.e.v.a;
    }

    private void a(ref.WeakReference p3, android.app.ProgressDialog p4)
    {
        if (p3 != null) {
            android.app.Activity v0_1 = ((android.app.Activity) p3.get());
            if (v0_1 != null) {
                v0_1.runOnUiThread(new net.hockeyapp.android.e.t(this, p4));
            }
        }
        return;
    }

    private void a(ref.WeakReference p3, android.app.ProgressDialog p4, int p5)
    {
        if (p3 != null) {
            android.app.Activity v0_1 = ((android.app.Activity) p3.get());
            if (v0_1 != null) {
                v0_1.runOnUiThread(new net.hockeyapp.android.e.s(this, p4, v0_1, p5));
            }
        }
        return;
    }

    private void a(ref.WeakReference p3, String p4, int p5)
    {
        if (p3 != null) {
            android.app.Activity v0_1 = ((android.app.Activity) p3.get());
            if (v0_1 != null) {
                v0_1.runOnUiThread(new net.hockeyapp.android.e.u(this, v0_1, p4, p5));
            }
        }
        return;
    }
}
