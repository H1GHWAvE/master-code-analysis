package net.hockeyapp.android.e;
public final class n {
    public android.content.SharedPreferences a;
    public android.content.SharedPreferences$Editor b;
    private android.content.SharedPreferences c;
    private android.content.SharedPreferences$Editor d;

    private n()
    {
        return;
    }

    synthetic n(byte p1)
    {
        return;
    }

    private static net.hockeyapp.android.e.n a()
    {
        return net.hockeyapp.android.e.p.a;
    }

    private void a(android.content.Context p6, String p7, String p8, String p9)
    {
        if (p6 != null) {
            this.a = p6.getSharedPreferences("net.hockeyapp.android.prefs_name_email", 0);
            if (this.a != null) {
                this.b = this.a.edit();
                if ((p7 != null) && ((p8 != null) && (p9 != null))) {
                    android.content.SharedPreferences$Editor v0_5 = this.b;
                    Object[] v3_1 = new Object[3];
                    v3_1[0] = p7;
                    v3_1[1] = p8;
                    v3_1[2] = p9;
                    v0_5.putString("net.hockeyapp.android.prefs_key_name_email", String.format("%s|%s|%s", v3_1));
                } else {
                    this.b.putString("net.hockeyapp.android.prefs_key_name_email", 0);
                }
                net.hockeyapp.android.e.n.a(this.b);
            }
        }
        return;
    }

    public static void a(android.content.SharedPreferences$Editor p1)
    {
        if (!net.hockeyapp.android.e.n.b().booleanValue()) {
            p1.commit();
        } else {
            p1.apply();
        }
        return;
    }

    private static Boolean b()
    {
        try {
            Boolean v0_1;
            if (android.os.Build$VERSION.SDK_INT < 9) {
                v0_1 = 0;
                Boolean v0_2 = Boolean.valueOf(v0_1);
            } else {
                v0_1 = 1;
            }
        } catch (Boolean v0) {
            v0_2 = Boolean.valueOf(0);
        }
        return v0_2;
    }

    private String b(android.content.Context p4)
    {
        String v0 = 0;
        if (p4 != null) {
            this.a = p4.getSharedPreferences("net.hockeyapp.android.prefs_name_email", 0);
            if (this.a != null) {
                v0 = this.a.getString("net.hockeyapp.android.prefs_key_name_email", 0);
            }
        }
        return v0;
    }

    public final String a(android.content.Context p4)
    {
        String v0 = 0;
        if (p4 != null) {
            this.c = p4.getSharedPreferences("net.hockeyapp.android.prefs_feedback_token", 0);
            if (this.c != null) {
                v0 = this.c.getString("net.hockeyapp.android.prefs_key_feedback_token", 0);
            }
        }
        return v0;
    }

    public final void a(android.content.Context p3, String p4)
    {
        if (p3 != null) {
            this.c = p3.getSharedPreferences("net.hockeyapp.android.prefs_feedback_token", 0);
            if (this.c != null) {
                this.d = this.c.edit();
                this.d.putString("net.hockeyapp.android.prefs_key_feedback_token", p4);
                net.hockeyapp.android.e.n.a(this.d);
            }
        }
        return;
    }
}
