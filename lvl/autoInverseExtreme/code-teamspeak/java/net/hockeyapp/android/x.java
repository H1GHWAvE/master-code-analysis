package net.hockeyapp.android;
final class x implements android.media.MediaScannerConnection$MediaScannerConnectionClient {
    android.media.MediaScannerConnection a;
    private String b;

    private x(String p2)
    {
        this.a = 0;
        this.b = p2;
        return;
    }

    synthetic x(String p1, byte p2)
    {
        this(p1);
        return;
    }

    private void a(android.media.MediaScannerConnection p1)
    {
        this.a = p1;
        return;
    }

    public final void onMediaScannerConnected()
    {
        if (this.a != null) {
            this.a.scanFile(this.b, 0);
        }
        return;
    }

    public final void onScanCompleted(String p6, android.net.Uri p7)
    {
        Object[] v2_1 = new Object[2];
        v2_1[0] = p6;
        v2_1[1] = p7.toString();
        android.util.Log.i("HockeyApp", String.format("Scanned path %s -> URI = %s", v2_1));
        this.a.disconnect();
        return;
    }
}
