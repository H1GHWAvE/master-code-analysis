package net.hockeyapp.android.d;
public final class r extends net.hockeyapp.android.d.k {
    public boolean a;
    public int b;
    private android.content.Context c;
    private android.os.Handler d;
    private String e;
    private String f;
    private String g;
    private String h;
    private String i;
    private java.util.List j;
    private String k;
    private boolean l;
    private android.app.ProgressDialog m;
    private java.net.HttpURLConnection n;

    public r(android.content.Context p2, String p3, String p4, String p5, String p6, String p7, java.util.List p8, String p9, android.os.Handler p10, boolean p11)
    {
        this.c = p2;
        this.e = p3;
        this.f = p4;
        this.g = p5;
        this.h = p6;
        this.i = p7;
        this.j = p8;
        this.k = p9;
        this.d = p10;
        this.l = p11;
        this.a = 1;
        this.b = -1;
        if (p2 != null) {
            net.hockeyapp.android.a.a(p2);
        }
        return;
    }

    private void a(int p1)
    {
        this.b = p1;
        return;
    }

    private void a(android.content.Context p1)
    {
        this.c = p1;
        return;
    }

    private void a(java.util.HashMap p5)
    {
        if (this.m != null) {
            try {
                this.m.dismiss();
            } catch (android.os.Handler v0_2) {
                v0_2.printStackTrace();
            }
        }
        if (this.d != null) {
            android.os.Message v1_1 = new android.os.Message();
            android.os.Bundle v2_1 = new android.os.Bundle();
            if (p5 == null) {
                v2_1.putString("request_type", "unknown");
            } else {
                v2_1.putString("request_type", ((String) p5.get("type")));
                v2_1.putString("feedback_response", ((String) p5.get("response")));
                v2_1.putString("feedback_status", ((String) p5.get("status")));
            }
            v1_1.setData(v2_1);
            this.d.sendMessage(v1_1);
        }
        return;
    }

    private void b()
    {
        this.a = 0;
        return;
    }

    private varargs java.util.HashMap c()
    {
        if ((!this.l) || (this.k == null)) {
            if (this.l) {
                int v0_3 = 0;
            } else {
                if (!this.j.isEmpty()) {
                    java.util.HashMap v1 = this.e();
                    int v0_8 = ((String) v1.get("status"));
                    if ((v0_8 != 0) && ((v0_8.startsWith("2")) && (this.c != null))) {
                        int v0_12 = new java.io.File(this.c.getCacheDir(), "HockeyApp");
                        if (v0_12.exists()) {
                            java.io.File[] v2_4 = v0_12.listFiles();
                            int v3_1 = v2_4.length;
                            int v0_13 = 0;
                            while (v0_13 < v3_1) {
                                v2_4[v0_13].delete();
                                v0_13++;
                            }
                        }
                    }
                    v0_3 = v1;
                } else {
                    v0_3 = this.d();
                }
            }
        } else {
            v0_3 = this.f();
        }
        return v0_3;
    }

    private java.util.HashMap d()
    {
        java.util.HashMap v2_1 = new java.util.HashMap();
        v2_1.put("type", "send");
        try {
            String v3_1 = new java.util.HashMap();
            v3_1.put("name", this.f);
            v3_1.put("email", this.g);
            v3_1.put("subject", this.h);
            v3_1.put("text", this.i);
            v3_1.put("bundle_identifier", net.hockeyapp.android.a.d);
            v3_1.put("bundle_short_version", net.hockeyapp.android.a.c);
            v3_1.put("bundle_version", net.hockeyapp.android.a.b);
            v3_1.put("os_version", net.hockeyapp.android.a.e);
            v3_1.put("oem", net.hockeyapp.android.a.g);
            v3_1.put("model", net.hockeyapp.android.a.f);
        } catch (String v0_22) {
            if (0 != 0) {
                0.disconnect();
            }
            throw v0_22;
        } catch (String v0_21) {
            v0_21.printStackTrace();
            if (0 == 0) {
                return v2_1;
            } else {
                0.disconnect();
                return v2_1;
            }
            net.hockeyapp.android.e.l v4_14.b = String v0_20;
            java.net.HttpURLConnection v1_2 = v4_14.a(v3_1).a();
            v1_2.connect();
            v2_1.put("status", String.valueOf(v1_2.getResponseCode()));
            v2_1.put("response", net.hockeyapp.android.d.r.a(v1_2));
            if (v1_2 == null) {
                return v2_1;
            } else {
                v1_2.disconnect();
                return v2_1;
            }
        }
        if (this.k != null) {
            this.e = new StringBuilder().append(this.e).append(this.k).append("/").toString();
        }
        v4_14 = new net.hockeyapp.android.e.l(this.e);
        if (this.k == null) {
            v0_20 = "POST";
        } else {
            v0_20 = "PUT";
        }
    }

    private java.util.HashMap e()
    {
        java.util.HashMap v2_1 = new java.util.HashMap();
        v2_1.put("type", "send");
        try {
            String v3_1 = new java.util.HashMap();
            v3_1.put("name", this.f);
            v3_1.put("email", this.g);
            v3_1.put("subject", this.h);
            v3_1.put("text", this.i);
            v3_1.put("bundle_identifier", net.hockeyapp.android.a.d);
            v3_1.put("bundle_short_version", net.hockeyapp.android.a.c);
            v3_1.put("bundle_version", net.hockeyapp.android.a.b);
            v3_1.put("os_version", net.hockeyapp.android.a.e);
            v3_1.put("oem", net.hockeyapp.android.a.g);
            v3_1.put("model", net.hockeyapp.android.a.f);
        } catch (String v0_21) {
            v0_21.printStackTrace();
            if (0 == 0) {
                return v2_1;
            } else {
                0.disconnect();
                return v2_1;
            }
            net.hockeyapp.android.e.l v4_14.b = String v0_20;
            java.net.HttpURLConnection v1_2 = v4_14.a(v3_1, this.c, this.j).a();
            v1_2.connect();
            v2_1.put("status", String.valueOf(v1_2.getResponseCode()));
            v2_1.put("response", net.hockeyapp.android.d.r.a(v1_2));
            if (v1_2 == null) {
                return v2_1;
            } else {
                v1_2.disconnect();
                return v2_1;
            }
        } catch (String v0_22) {
            if (0 != 0) {
                0.disconnect();
            }
            throw v0_22;
        }
        if (this.k != null) {
            this.e = new StringBuilder().append(this.e).append(this.k).append("/").toString();
        }
        v4_14 = new net.hockeyapp.android.e.l(this.e);
        if (this.k == null) {
            v0_20 = "POST";
        } else {
            v0_20 = "PUT";
        }
    }

    private java.util.HashMap f()
    {
        java.io.IOException v0_1 = new StringBuilder();
        v0_1.append(new StringBuilder().append(this.e).append(net.hockeyapp.android.e.w.a(this.k)).toString());
        if (this.b != -1) {
            v0_1.append(new StringBuilder("?last_message_id=").append(this.b).toString());
        }
        java.util.HashMap v2_7 = new java.util.HashMap();
        try {
            java.net.HttpURLConnection v1_10 = new net.hockeyapp.android.e.l(v0_1.toString()).a();
            v2_7.put("type", "fetch");
            v1_10.connect();
            v2_7.put("status", String.valueOf(v1_10.getResponseCode()));
            v2_7.put("response", net.hockeyapp.android.d.r.a(v1_10));
        } catch (java.io.IOException v0_6) {
            v0_6.printStackTrace();
            if (v1_10 == null) {
                return v2_7;
            } else {
                v1_10.disconnect();
                return v2_7;
            }
        } catch (java.io.IOException v0_7) {
            if (v1_10 != null) {
                v1_10.disconnect();
            }
            throw v0_7;
        }
        if (v1_10 == null) {
            return v2_7;
        } else {
            v1_10.disconnect();
            return v2_7;
        }
    }

    public final void a()
    {
        this.c = 0;
        if (this.m != null) {
            this.m.dismiss();
        }
        this.m = 0;
        return;
    }

    protected final synthetic Object doInBackground(Object[] p6)
    {
        if ((!this.l) || (this.k == null)) {
            if (this.l) {
                int v0_3 = 0;
            } else {
                if (!this.j.isEmpty()) {
                    java.util.HashMap v1 = this.e();
                    int v0_8 = ((String) v1.get("status"));
                    if ((v0_8 != 0) && ((v0_8.startsWith("2")) && (this.c != null))) {
                        int v0_12 = new java.io.File(this.c.getCacheDir(), "HockeyApp");
                        if (v0_12.exists()) {
                            java.io.File[] v2_4 = v0_12.listFiles();
                            int v3_1 = v2_4.length;
                            int v0_13 = 0;
                            while (v0_13 < v3_1) {
                                v2_4[v0_13].delete();
                                v0_13++;
                            }
                        }
                    }
                    v0_3 = v1;
                } else {
                    v0_3 = this.d();
                }
            }
        } else {
            v0_3 = this.f();
        }
        return v0_3;
    }

    protected final synthetic void onPostExecute(Object p5)
    {
        if (this.m != null) {
            try {
                this.m.dismiss();
            } catch (android.os.Handler v0_2) {
                v0_2.printStackTrace();
            }
        }
        if (this.d != null) {
            android.os.Message v1_1 = new android.os.Message();
            android.os.Bundle v2_1 = new android.os.Bundle();
            if (((java.util.HashMap) p5) == null) {
                v2_1.putString("request_type", "unknown");
            } else {
                v2_1.putString("request_type", ((String) ((java.util.HashMap) p5).get("type")));
                v2_1.putString("feedback_response", ((String) ((java.util.HashMap) p5).get("response")));
                v2_1.putString("feedback_status", ((String) ((java.util.HashMap) p5).get("status")));
            }
            v1_1.setData(v2_1);
            this.d.sendMessage(v1_1);
        }
        return;
    }

    protected final void onPreExecute()
    {
        android.app.ProgressDialog v0_0 = "Sending feedback..";
        if (this.l) {
            v0_0 = "Retrieving discussions...";
        }
        if (((this.m == null) || (!this.m.isShowing())) && (this.a)) {
            this.m = android.app.ProgressDialog.show(this.c, "", v0_0, 1, 0);
        }
        return;
    }
}
