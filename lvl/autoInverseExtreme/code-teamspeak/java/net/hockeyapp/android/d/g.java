package net.hockeyapp.android.d;
public class g extends android.os.AsyncTask {
    protected static final String a = "apk";
    protected static final String b = "url";
    protected static final String c = "json";
    private static final int h = 25;
    protected String d;
    protected String e;
    protected Boolean f;
    protected net.hockeyapp.android.az g;
    private android.content.Context i;
    private long j;

    private g(ref.WeakReference p2, String p3)
    {
        this(p2, p3, 0);
        return;
    }

    private g(ref.WeakReference p2, String p3, byte p4)
    {
        this(p2, p3, 0, 0);
        return;
    }

    public g(ref.WeakReference p7, String p8, String p9, net.hockeyapp.android.az p10)
    {
        android.content.Context v4;
        this.d = 0;
        this.e = 0;
        this.i = 0;
        this.f = Boolean.valueOf(0);
        this.j = 0;
        this.e = p9;
        this.d = p8;
        this.g = p10;
        if (p7 == null) {
            v4 = 0;
        } else {
            v4 = ((android.content.Context) p7.get());
        }
        if (v4 != null) {
            long v0_8;
            this.i = v4.getApplicationContext();
            if (net.hockeyapp.android.ak.a(v4)) {
                v0_8 = (v4.getSharedPreferences("HockeyApp", 0).getLong(new StringBuilder("usageTime").append(net.hockeyapp.android.a.b).toString(), 0) / 1000);
            } else {
                v0_8 = 0;
            }
            this.j = v0_8;
            net.hockeyapp.android.a.a(v4);
        }
        return;
    }

    private static String a(java.io.InputStream p4)
    {
        java.io.IOException v0_1 = new java.io.BufferedReader(new java.io.InputStreamReader(p4), 1024);
        java.io.IOException v1_3 = new StringBuilder();
        try {
            while(true) {
                String v2_1 = v0_1.readLine();
                v1_3.append(new StringBuilder().append(v2_1).append("\n").toString());
            }
            try {
                p4.close();
            } catch (java.io.IOException v0_5) {
                v0_5.printStackTrace();
            }
            return v1_3.toString();
        } catch (java.io.IOException v0_4) {
            try {
                p4.close();
            } catch (java.io.IOException v1_4) {
                v1_4.printStackTrace();
            }
            throw v0_4;
        } catch (java.io.IOException v0_2) {
            v0_2.printStackTrace();
            try {
                p4.close();
            } catch (java.io.IOException v0_3) {
                v0_3.printStackTrace();
            }
            return v1_3.toString();
        }
        if (v2_1 != null) {
        }
    }

    private static java.net.URLConnection a(java.net.URL p3)
    {
        java.net.URLConnection v0 = p3.openConnection();
        v0.addRequestProperty("User-Agent", "HockeySDK/Android");
        if (android.os.Build$VERSION.SDK_INT <= 9) {
            v0.setRequestProperty("connection", "close");
        }
        return v0;
    }

    private boolean a(org.json.JSONArray p11, int p12)
    {
        int v6 = 0;
        int v0_0 = 0;
        try {
            while (v6 < p11.length()) {
                int v5;
                org.json.JSONObject v7 = p11.getJSONObject(v6);
                if (v7.getInt("version") <= p12) {
                    v5 = 0;
                } else {
                    v5 = 1;
                }
                if ((v7.getInt("version") != p12) || (!net.hockeyapp.android.e.y.a(this.i, v7.getLong("timestamp")))) {
                    int v4_1 = 0;
                } else {
                    v4_1 = 1;
                }
                String v8_1 = v7.getString("minimum_os_version");
                boolean v3_8 = android.os.Build$VERSION.RELEASE;
                if ((v3_8) && (!v3_8.equalsIgnoreCase("L"))) {
                    if (!v3_8.equalsIgnoreCase("M")) {
                        if (java.util.regex.Pattern.matches("^[a-zA-Z]+", v3_8)) {
                            v3_8 = "99.0";
                        }
                    } else {
                        v3_8 = "6.0";
                    }
                } else {
                    v3_8 = "5.0";
                }
                boolean v3_10;
                if (net.hockeyapp.android.e.y.a(v8_1, v3_8) > 0) {
                    v3_10 = 0;
                } else {
                    v3_10 = 1;
                }
                if (((v5 != 0) || (v4_1 != 0)) && (v3_10)) {
                    if (v7.has("mandatory")) {
                        this.f = Boolean.valueOf((this.f.booleanValue() | v7.getBoolean("mandatory")));
                    }
                    v0_0 = 1;
                }
                v6++;
            }
        } catch (int v0) {
            v0_0 = 0;
        }
        return v0_0;
    }

    private static String b(String p1)
    {
        try {
            String v0_1 = java.net.URLEncoder.encode(p1, "UTF-8");
        } catch (String v0) {
            v0_1 = "";
        }
        return v0_1;
    }

    private static org.json.JSONArray b(org.json.JSONArray p4)
    {
        org.json.JSONArray v1_1 = new org.json.JSONArray();
        int v0 = 0;
        while (v0 < Math.min(p4.length(), 25)) {
            try {
                v1_1.put(p4.get(v0));
            } catch (org.json.JSONException v2) {
            }
            v0++;
        }
        return v1_1;
    }

    private static int c()
    {
        return Integer.parseInt(net.hockeyapp.android.a.b);
    }

    private varargs org.json.JSONArray d()
    {
        try {
            int v1 = Integer.parseInt(net.hockeyapp.android.a.b);
            org.json.JSONArray v0_2 = new org.json.JSONArray(net.hockeyapp.android.e.x.a(this.i));
        } catch (org.json.JSONArray v0_9) {
            v0_9.printStackTrace();
            v0_2 = 0;
            return v0_2;
        }
        if (!this.a(v0_2, v1)) {
            org.json.JSONArray v0_5 = new java.net.URL(this.a("json")).openConnection();
            v0_5.addRequestProperty("User-Agent", "HockeySDK/Android");
            if (android.os.Build$VERSION.SDK_INT <= 9) {
                v0_5.setRequestProperty("connection", "close");
            }
            v0_5.connect();
            org.json.JSONArray v2_9 = new java.io.BufferedInputStream(v0_5.getInputStream());
            org.json.JSONArray v0_7 = net.hockeyapp.android.d.g.a(v2_9);
            v2_9.close();
            org.json.JSONArray v2_11 = new org.json.JSONArray(v0_7);
            if (!this.a(v2_11, v1)) {
            } else {
                v0_2 = net.hockeyapp.android.d.g.b(v2_11);
                return v0_2;
            }
        } else {
            return v0_2;
        }
    }

    private static boolean e()
    {
        return 1;
    }

    protected final String a(String p5)
    {
        String v0_4;
        StringBuilder v1_1 = new StringBuilder();
        v1_1.append(this.d);
        v1_1.append("api/2/apps/");
        if (this.e == null) {
            v0_4 = this.i.getPackageName();
        } else {
            v0_4 = this.e;
        }
        v1_1.append(v0_4);
        v1_1.append(new StringBuilder("?format=").append(p5).toString());
        if (android.provider.Settings$Secure.getString(this.i.getContentResolver(), "android_id") != null) {
            v1_1.append(new StringBuilder("&udid=").append(net.hockeyapp.android.d.g.b(android.provider.Settings$Secure.getString(this.i.getContentResolver(), "android_id"))).toString());
        }
        v1_1.append("&os=Android");
        v1_1.append(new StringBuilder("&os_version=").append(net.hockeyapp.android.d.g.b(net.hockeyapp.android.a.e)).toString());
        v1_1.append(new StringBuilder("&device=").append(net.hockeyapp.android.d.g.b(net.hockeyapp.android.a.f)).toString());
        v1_1.append(new StringBuilder("&oem=").append(net.hockeyapp.android.d.g.b(net.hockeyapp.android.a.g)).toString());
        v1_1.append(new StringBuilder("&app_version=").append(net.hockeyapp.android.d.g.b(net.hockeyapp.android.a.b)).toString());
        v1_1.append(new StringBuilder("&sdk=").append(net.hockeyapp.android.d.g.b("HockeySDK")).toString());
        v1_1.append(new StringBuilder("&sdk_version=").append(net.hockeyapp.android.d.g.b("3.6.0")).toString());
        v1_1.append(new StringBuilder("&lang=").append(net.hockeyapp.android.d.g.b(java.util.Locale.getDefault().getLanguage())).toString());
        v1_1.append(new StringBuilder("&usage_time=").append(this.j).toString());
        return v1_1.toString();
    }

    public void a()
    {
        this.i = 0;
        return;
    }

    public final void a(ref.WeakReference p3)
    {
        android.content.Context v0_0 = 0;
        if (p3 != null) {
            v0_0 = ((android.content.Context) p3.get());
        }
        if (v0_0 != null) {
            this.i = v0_0.getApplicationContext();
            net.hockeyapp.android.a.a(v0_0);
        }
        return;
    }

    protected void a(org.json.JSONArray p2)
    {
        if ((p2 != null) && (this.g != null)) {
            this.a("apk");
        }
        return;
    }

    protected void b()
    {
        this.d = 0;
        this.e = 0;
        return;
    }

    protected synthetic Object doInBackground(Object[] p2)
    {
        return this.d();
    }

    protected synthetic void onPostExecute(Object p1)
    {
        this.a(((org.json.JSONArray) p1));
        return;
    }
}
