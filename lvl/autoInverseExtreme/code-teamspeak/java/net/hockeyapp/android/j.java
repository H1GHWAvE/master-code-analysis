package net.hockeyapp.android;
public final class j implements java.lang.Thread$UncaughtExceptionHandler {
    net.hockeyapp.android.i a;
    private boolean b;
    private Thread$UncaughtExceptionHandler c;

    public j(Thread$UncaughtExceptionHandler p2, net.hockeyapp.android.i p3, boolean p4)
    {
        this.b = 0;
        this.c = p2;
        this.b = p4;
        this.a = p3;
        return;
    }

    private static String a()
    {
        return 0;
    }

    private static void a(String p3)
    {
        try {
            int v0_5 = new StringBuilder().append(net.hockeyapp.android.a.a).append("/").append(p3).toString();
        } catch (int v0) {
            return;
        }
        if (0.trim().length() <= 0) {
            return;
        } else {
            java.io.BufferedWriter v1_6 = new java.io.BufferedWriter(new java.io.FileWriter(v0_5));
            v1_6.write(0);
            v1_6.flush();
            v1_6.close();
            return;
        }
    }

    private static void a(Throwable p7, net.hockeyapp.android.i p8)
    {
        String v0_1 = new java.util.Date();
        String v1_1 = new java.io.StringWriter();
        p7.printStackTrace(new java.io.PrintWriter(v1_1));
        try {
            String v2_3 = java.util.UUID.randomUUID().toString();
            String v3_6 = new StringBuilder().append(net.hockeyapp.android.a.a).append("/").append(v2_3).append(".stacktrace").toString();
            android.util.Log.d("HockeyApp", new StringBuilder("Writing unhandled exception to: ").append(v3_6).toString());
            java.io.BufferedWriter v4_5 = new java.io.BufferedWriter(new java.io.FileWriter(v3_6));
            v4_5.write(new StringBuilder("Package: ").append(net.hockeyapp.android.a.d).append("\n").toString());
            v4_5.write(new StringBuilder("Version Code: ").append(net.hockeyapp.android.a.b).append("\n").toString());
            v4_5.write(new StringBuilder("Version Name: ").append(net.hockeyapp.android.a.c).append("\n").toString());
            v4_5.write(new StringBuilder("Android: ").append(net.hockeyapp.android.a.e).append("\n").toString());
            v4_5.write(new StringBuilder("Manufacturer: ").append(net.hockeyapp.android.a.g).append("\n").toString());
            v4_5.write(new StringBuilder("Model: ").append(net.hockeyapp.android.a.f).append("\n").toString());
        } catch (String v0_22) {
            android.util.Log.e("HockeyApp", "Error saving exception stacktrace!\n", v0_22);
            return;
        }
        if (net.hockeyapp.android.a.h != null) {
            v4_5.write(new StringBuilder("CrashReporter Key: ").append(net.hockeyapp.android.a.h).append("\n").toString());
        }
        v4_5.write(new StringBuilder("Date: ").append(v0_1).append("\n").toString());
        v4_5.write("\n");
        v4_5.write(v1_1.toString());
        v4_5.flush();
        v4_5.close();
        if (p8 == null) {
            return;
        } else {
            net.hockeyapp.android.j.a(new StringBuilder().append(v2_3).append(".user").toString());
            net.hockeyapp.android.j.a(new StringBuilder().append(v2_3).append(".contact").toString());
            net.hockeyapp.android.j.a(new StringBuilder().append(v2_3).append(".description").toString());
            return;
        }
    }

    private void a(net.hockeyapp.android.i p1)
    {
        this.a = p1;
        return;
    }

    public final void uncaughtException(Thread p9, Throwable p10)
    {
        if (net.hockeyapp.android.a.a != null) {
            int v0_1 = this.a;
            String v1_1 = new java.util.Date();
            String v2_1 = new java.io.StringWriter();
            p10.printStackTrace(new java.io.PrintWriter(v2_1));
            try {
                String v3_3 = java.util.UUID.randomUUID().toString();
                String v4_6 = new StringBuilder().append(net.hockeyapp.android.a.a).append("/").append(v3_3).append(".stacktrace").toString();
                android.util.Log.d("HockeyApp", new StringBuilder("Writing unhandled exception to: ").append(v4_6).toString());
                java.io.BufferedWriter v5_5 = new java.io.BufferedWriter(new java.io.FileWriter(v4_6));
                v5_5.write(new StringBuilder("Package: ").append(net.hockeyapp.android.a.d).append("\n").toString());
                v5_5.write(new StringBuilder("Version Code: ").append(net.hockeyapp.android.a.b).append("\n").toString());
                v5_5.write(new StringBuilder("Version Name: ").append(net.hockeyapp.android.a.c).append("\n").toString());
                v5_5.write(new StringBuilder("Android: ").append(net.hockeyapp.android.a.e).append("\n").toString());
                v5_5.write(new StringBuilder("Manufacturer: ").append(net.hockeyapp.android.a.g).append("\n").toString());
                v5_5.write(new StringBuilder("Model: ").append(net.hockeyapp.android.a.f).append("\n").toString());
            } catch (int v0_17) {
                android.util.Log.e("HockeyApp", "Error saving exception stacktrace!\n", v0_17);
                if (this.b) {
                    android.os.Process.killProcess(android.os.Process.myPid());
                    System.exit(10);
                } else {
                    this.c.uncaughtException(p9, p10);
                }
            }
            if (net.hockeyapp.android.a.h != null) {
                v5_5.write(new StringBuilder("CrashReporter Key: ").append(net.hockeyapp.android.a.h).append("\n").toString());
            }
            v5_5.write(new StringBuilder("Date: ").append(v1_1).append("\n").toString());
            v5_5.write("\n");
            v5_5.write(v2_1.toString());
            v5_5.flush();
            v5_5.close();
            if (v0_1 == 0) {
            } else {
                net.hockeyapp.android.j.a(new StringBuilder().append(v3_3).append(".user").toString());
                net.hockeyapp.android.j.a(new StringBuilder().append(v3_3).append(".contact").toString());
                net.hockeyapp.android.j.a(new StringBuilder().append(v3_3).append(".description").toString());
            }
        } else {
            this.c.uncaughtException(p9, p10);
        }
        return;
    }
}
