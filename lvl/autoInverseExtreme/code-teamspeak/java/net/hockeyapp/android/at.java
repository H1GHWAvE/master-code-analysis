package net.hockeyapp.android;
public class at extends android.app.DialogFragment implements android.view.View$OnClickListener, net.hockeyapp.android.ax {
    private net.hockeyapp.android.d.l downloadTask;
    private String urlString;
    private net.hockeyapp.android.e.y versionHelper;
    private org.json.JSONArray versionInfo;

    public at()
    {
        return;
    }

    static synthetic void access$000(net.hockeyapp.android.at p0, android.app.Activity p1)
    {
        p0.startDownloadTask(p1);
        return;
    }

    public static net.hockeyapp.android.at newInstance(org.json.JSONArray p3, String p4)
    {
        android.os.Bundle v0_1 = new android.os.Bundle();
        v0_1.putString("url", p4);
        v0_1.putString("versionInfo", p3.toString());
        net.hockeyapp.android.at v1_3 = new net.hockeyapp.android.at();
        v1_3.setArguments(v0_1);
        return v1_3;
    }

    private void startDownloadTask(android.app.Activity p4)
    {
        this.downloadTask = new net.hockeyapp.android.d.l(p4, this.urlString, new net.hockeyapp.android.aw(this, p4));
        net.hockeyapp.android.e.a.a(this.downloadTask);
        return;
    }

    public String getAppName()
    {
        String v0_0 = this.getActivity();
        try {
            android.content.pm.PackageManager v1 = v0_0.getPackageManager();
            String v0_4 = v1.getApplicationLabel(v1.getApplicationInfo(v0_0.getPackageName(), 0)).toString();
        } catch (String v0) {
            v0_4 = "";
        }
        return v0_4;
    }

    public int getCurrentVersionCode()
    {
        try {
            int v0 = this.getActivity().getPackageManager().getPackageInfo(this.getActivity().getPackageName(), 128).versionCode;
        } catch (NullPointerException v1) {
        } catch (NullPointerException v1) {
        }
        return v0;
    }

    public android.view.View getLayoutView()
    {
        return new net.hockeyapp.android.f.m(this.getActivity(), 0, 1);
    }

    public void onClick(android.view.View p1)
    {
        this.prepareDownload();
        return;
    }

    public void onCreate(android.os.Bundle p4)
    {
        super.onCreate(p4);
        try {
            this.urlString = this.getArguments().getString("url");
            this.versionInfo = new org.json.JSONArray(this.getArguments().getString("versionInfo"));
            this.setStyle(1, 16973939);
        } catch (org.json.JSONException v0) {
            this.dismiss();
        }
        return;
    }

    public android.view.View onCreateView(android.view.LayoutInflater p12, android.view.ViewGroup p13, android.os.Bundle p14)
    {
        android.view.View v6 = this.getLayoutView();
        this.versionHelper = new net.hockeyapp.android.e.y(this.getActivity(), this.versionInfo.toString(), this);
        ((android.widget.TextView) v6.findViewById(4098)).setText(this.getAppName());
        android.webkit.WebView v0_7 = ((android.widget.TextView) v6.findViewById(4099));
        String v2_5 = new StringBuilder("Version ").append(this.versionHelper.a()).toString();
        String v3_0 = this.versionHelper.b();
        String v1_6 = "Unknown size";
        String v4_1 = this.versionHelper.c();
        if (v4_1 < 0) {
            net.hockeyapp.android.e.a.a(new net.hockeyapp.android.d.o(this.getActivity(), this.urlString, new net.hockeyapp.android.au(this, v0_7, v2_5, v3_0)));
        } else {
            String v1_8 = new StringBuilder();
            net.hockeyapp.android.au v8_3 = new Object[1];
            v8_3[0] = Float.valueOf((((float) v4_1) / 1233125376));
            v1_6 = v1_8.append(String.format("%.2f", v8_3)).append(" MB").toString();
        }
        v0_7.setText(new StringBuilder().append(v2_5).append("\n").append(v3_0).append(" - ").append(v1_6).toString());
        ((android.widget.Button) v6.findViewById(4100)).setOnClickListener(this);
        android.webkit.WebView v0_13 = ((android.webkit.WebView) v6.findViewById(4101));
        v0_13.clearCache(1);
        v0_13.destroyDrawingCache();
        v0_13.loadDataWithBaseURL("https://sdk.hockeyapp.net/", this.versionHelper.d(), "text/html", "utf-8", 0);
        return v6;
    }

    public void onRequestPermissionsResult(int p4, String[] p5, int[] p6)
    {
        if ((p5.length != 0) && ((p6.length != 0) && (p4 == 1))) {
            if (p6[0] != 0) {
                android.util.Log.w("HockeyApp", "User denied write permission, can\'t continue with updater task.");
                if (net.hockeyapp.android.ay.a() == null) {
                    new android.app.AlertDialog$Builder(this.getActivity()).setTitle(net.hockeyapp.android.aj.a(1792)).setMessage(net.hockeyapp.android.aj.a(1793)).setNegativeButton(net.hockeyapp.android.aj.a(1794), 0).setPositiveButton(net.hockeyapp.android.aj.a(1795), new net.hockeyapp.android.av(this, this)).create().show();
                }
            } else {
                this.startDownloadTask(this.getActivity());
            }
        }
        return;
    }

    public void prepareDownload()
    {
        if ((android.os.Build$VERSION.SDK_INT < 23) || (this.getActivity().checkSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE") == 0)) {
            this.startDownloadTask(this.getActivity());
            this.dismiss();
        } else {
            String[] v0_4 = new String[1];
            v0_4[0] = "android.permission.WRITE_EXTERNAL_STORAGE";
            this.requestPermissions(v0_4, 1);
        }
        return;
    }
}
