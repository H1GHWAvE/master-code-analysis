package net.hockeyapp.android;
final class m extends android.os.Handler {
    final synthetic net.hockeyapp.android.FeedbackActivity a;

    m(net.hockeyapp.android.FeedbackActivity p1)
    {
        this.a = p1;
        return;
    }

    public final void handleMessage(android.os.Message p9)
    {
        net.hockeyapp.android.c.c v0_0 = 0;
        net.hockeyapp.android.FeedbackActivity.a(this.a, new net.hockeyapp.android.c.c());
        if ((p9 == null) || (p9.getData() == null)) {
            net.hockeyapp.android.FeedbackActivity.a(this.a).a = net.hockeyapp.android.aj.a(1036);
        } else {
            String v2_3 = p9.getData();
            String v3_3 = v2_3.getString("feedback_response");
            int v4_1 = v2_3.getString("feedback_status");
            String v2_4 = v2_3.getString("request_type");
            if ((!v2_4.equals("send")) || ((v3_3 != null) && (Integer.parseInt(v4_1) == 201))) {
                if ((!v2_4.equals("fetch")) || ((v4_1 == 0) || ((Integer.parseInt(v4_1) != 404) && (Integer.parseInt(v4_1) != 422)))) {
                    if (v3_3 == null) {
                        net.hockeyapp.android.FeedbackActivity.a(this.a).a = net.hockeyapp.android.aj.a(1037);
                    } else {
                        net.hockeyapp.android.FeedbackActivity.a(this.a, v3_3, v2_4);
                        v0_0 = 1;
                    }
                } else {
                    net.hockeyapp.android.FeedbackActivity.b(this.a);
                    v0_0 = 1;
                }
            } else {
                net.hockeyapp.android.FeedbackActivity.a(this.a).a = net.hockeyapp.android.aj.a(1036);
            }
        }
        if (v0_0 == null) {
            this.a.runOnUiThread(new net.hockeyapp.android.n(this));
        }
        return;
    }
}
