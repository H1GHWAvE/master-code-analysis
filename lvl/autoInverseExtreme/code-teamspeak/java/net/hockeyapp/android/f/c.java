package net.hockeyapp.android.f;
final class c extends android.os.AsyncTask {
    final synthetic net.hockeyapp.android.f.b a;

    c(net.hockeyapp.android.f.b p1)
    {
        this.a = p1;
        return;
    }

    private varargs android.graphics.Bitmap a()
    {
        return net.hockeyapp.android.f.b.a(this.a);
    }

    private void a(android.graphics.Bitmap p2)
    {
        if (p2 == null) {
            net.hockeyapp.android.f.b.b(this.a);
        } else {
            net.hockeyapp.android.f.b.a(this.a, p2);
        }
        return;
    }

    protected final synthetic Object doInBackground(Object[] p2)
    {
        return net.hockeyapp.android.f.b.a(this.a);
    }

    protected final synthetic void onPostExecute(Object p2)
    {
        if (((android.graphics.Bitmap) p2) == null) {
            net.hockeyapp.android.f.b.b(this.a);
        } else {
            net.hockeyapp.android.f.b.a(this.a, ((android.graphics.Bitmap) p2));
        }
        return;
    }
}
