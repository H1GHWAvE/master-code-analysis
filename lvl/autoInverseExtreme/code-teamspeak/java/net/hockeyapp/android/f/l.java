package net.hockeyapp.android.f;
final class l extends android.os.AsyncTask {
    final synthetic net.hockeyapp.android.f.k a;

    l(net.hockeyapp.android.f.k p1)
    {
        this.a = p1;
        return;
    }

    private static varargs android.graphics.Bitmap a(Object[] p4)
    {
        try {
            int v0_4 = net.hockeyapp.android.f.k.a(((android.content.Context) p4[0]).getContentResolver(), ((android.net.Uri) p4[1]), ((Integer) p4[2]).intValue(), ((Integer) p4[3]).intValue());
        } catch (int v0_5) {
            android.util.Log.e("HockeyApp", "Could not load image into ImageView.", v0_5);
            v0_4 = 0;
        }
        return v0_4;
    }

    private void a(android.graphics.Bitmap p2)
    {
        if (p2 != null) {
            this.a.setImageBitmap(p2);
        }
        return;
    }

    protected final synthetic Object doInBackground(Object[] p2)
    {
        return net.hockeyapp.android.f.l.a(p2);
    }

    protected final synthetic void onPostExecute(Object p2)
    {
        if (((android.graphics.Bitmap) p2) != null) {
            this.a.setImageBitmap(((android.graphics.Bitmap) p2));
        }
        return;
    }

    protected final void onPreExecute()
    {
        this.a.setAdjustViewBounds(1);
        return;
    }
}
