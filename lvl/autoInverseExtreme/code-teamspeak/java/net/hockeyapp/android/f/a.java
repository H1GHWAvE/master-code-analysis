package net.hockeyapp.android.f;
public class a extends android.view.ViewGroup {
    static final synthetic boolean a;
    private int b;

    static a()
    {
        int v0_2;
        if (net.hockeyapp.android.f.a.desiredAssertionStatus()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        net.hockeyapp.android.f.a.a = v0_2;
        return;
    }

    public a(android.content.Context p1)
    {
        this(p1);
        return;
    }

    protected boolean checkLayoutParams(android.view.ViewGroup$LayoutParams p2)
    {
        return (p2 instanceof android.view.ViewGroup$LayoutParams);
    }

    protected android.view.ViewGroup$LayoutParams generateDefaultLayoutParams()
    {
        return new android.view.ViewGroup$LayoutParams(1, 1);
    }

    public java.util.ArrayList getAttachments()
    {
        java.util.ArrayList v2_1 = new java.util.ArrayList();
        int v1 = 0;
        while (v1 < this.getChildCount()) {
            v2_1.add(((net.hockeyapp.android.f.b) this.getChildAt(v1)).getAttachmentUri());
            v1++;
        }
        return v2_1;
    }

    protected void onLayout(boolean p11, int p12, int p13, int p14, int p15)
    {
        int v4 = this.getChildCount();
        int v5 = (p14 - p12);
        int v2 = this.getPaddingLeft();
        int v1 = this.getPaddingTop();
        int v3 = 0;
        while (v3 < v4) {
            int v0_1 = this.getChildAt(v3);
            if (v0_1.getVisibility() != 8) {
                v0_1.invalidate();
                int v6_1 = v0_1.getMeasuredWidth();
                int v7_1 = v0_1.getMeasuredHeight();
                android.view.ViewGroup$LayoutParams v8 = v0_1.getLayoutParams();
                if ((v2 + v6_1) > v5) {
                    v2 = this.getPaddingLeft();
                    v1 += this.b;
                }
                v0_1.layout(v2, v1, (v2 + v6_1), (v7_1 + v1));
                v2 += (((net.hockeyapp.android.f.b) v0_1).getGap() + (v6_1 + v8.width));
            }
            v3++;
        }
        return;
    }

    protected void onMeasure(int p13, int p14)
    {
        int v0_0 = 0;
        if ((net.hockeyapp.android.f.a.a) || (android.view.View$MeasureSpec.getMode(p13) != 0)) {
            int v7 = android.view.View$MeasureSpec.getSize(p13);
            int v8 = this.getChildCount();
            int v3_0 = this.getPaddingLeft();
            int v2_0 = this.getPaddingTop();
            int v5 = 0;
            int v4_0 = 0;
            while (v5 < v8) {
                int v0_7;
                int v1_9;
                int v1_8 = this.getChildAt(v5);
                int v0_4 = ((net.hockeyapp.android.f.b) v1_8);
                int v6_1 = (v0_4.getEffectiveMaxHeight() + v0_4.getPaddingTop());
                if (v1_8.getVisibility() == 8) {
                    v0_7 = v2_0;
                    v1_9 = v4_0;
                } else {
                    int v1_11;
                    android.view.ViewGroup$LayoutParams v9_1 = v1_8.getLayoutParams();
                    v1_8.measure(android.view.View$MeasureSpec.makeMeasureSpec(v7, -2147483648), android.view.View$MeasureSpec.makeMeasureSpec(v6_1, -2147483648));
                    int v10_1 = v1_8.getMeasuredWidth();
                    int v4_1 = Math.max(v4_0, (v1_8.getMeasuredHeight() + v9_1.height));
                    if ((v3_0 + v10_1) <= v7) {
                        v0_7 = v2_0;
                        v1_11 = v3_0;
                    } else {
                        v1_11 = this.getPaddingLeft();
                        v0_7 = (v2_0 + v4_1);
                    }
                    v3_0 = (v1_11 + (v9_1.width + v10_1));
                    v1_9 = v4_1;
                }
                v5++;
                v4_0 = v1_9;
                v2_0 = v0_7;
                v0_0 = v6_1;
            }
            this.b = v4_0;
            if (android.view.View$MeasureSpec.getMode(p14) != 0) {
                if ((android.view.View$MeasureSpec.getMode(p14) == -2147483648) && (((v2_0 + v4_0) + this.getPaddingBottom()) < v0_0)) {
                    v0_0 = ((v2_0 + v4_0) + this.getPaddingBottom());
                }
            } else {
                v0_0 = ((v2_0 + v4_0) + this.getPaddingBottom());
            }
            this.setMeasuredDimension(v7, v0_0);
            return;
        } else {
            throw new AssertionError();
        }
    }
}
