package net.hockeyapp.android.c;
public final enum class a extends java.lang.Enum {
    public static final enum net.hockeyapp.android.c.a a;
    public static final enum net.hockeyapp.android.c.a b;
    public static final enum net.hockeyapp.android.c.a c;
    private static final synthetic net.hockeyapp.android.c.a[] e;
    private final int d;

    static a()
    {
        net.hockeyapp.android.c.a.a = new net.hockeyapp.android.c.a("CrashManagerUserInputDontSend", 0, 0);
        net.hockeyapp.android.c.a.b = new net.hockeyapp.android.c.a("CrashManagerUserInputSend", 1, 1);
        net.hockeyapp.android.c.a.c = new net.hockeyapp.android.c.a("CrashManagerUserInputAlwaysSend", 2, 2);
        net.hockeyapp.android.c.a[] v0_7 = new net.hockeyapp.android.c.a[3];
        v0_7[0] = net.hockeyapp.android.c.a.a;
        v0_7[1] = net.hockeyapp.android.c.a.b;
        v0_7[2] = net.hockeyapp.android.c.a.c;
        net.hockeyapp.android.c.a.e = v0_7;
        return;
    }

    private a(String p1, int p2, int p3)
    {
        this(p1, p2);
        this.d = p3;
        return;
    }

    private int a()
    {
        return this.d;
    }

    public static net.hockeyapp.android.c.a valueOf(String p1)
    {
        return ((net.hockeyapp.android.c.a) Enum.valueOf(net.hockeyapp.android.c.a, p1));
    }

    public static net.hockeyapp.android.c.a[] values()
    {
        return ((net.hockeyapp.android.c.a[]) net.hockeyapp.android.c.a.e.clone());
    }
}
