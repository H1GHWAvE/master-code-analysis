package com.teamspeak.ts3client.jni.events;
public class ServerEdited implements com.teamspeak.ts3client.jni.k {
    private long a;
    private int b;
    private String c;
    private String d;

    public ServerEdited()
    {
        return;
    }

    private ServerEdited(long p2, int p4, String p5, String p6)
    {
        this.a = p2;
        this.b = p4;
        this.c = p5;
        this.d = p6;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private int a()
    {
        return this.b;
    }

    private String b()
    {
        return this.c;
    }

    private String c()
    {
        return this.d;
    }

    private long d()
    {
        return this.a;
    }

    public String toString()
    {
        return new StringBuilder("ServerEdited [serverConnectionHandlerID=").append(this.a).append(", editerID=").append(this.b).append(", editerName=").append(this.c).append(", editerUniqueIdentifier=").append(this.d).append("]").toString();
    }
}
