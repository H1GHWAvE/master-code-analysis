package com.teamspeak.ts3client.jni.events.rare;
public class ClientChatComposing implements com.teamspeak.ts3client.jni.k {
    private long a;
    private int b;
    private String c;

    public ClientChatComposing()
    {
        return;
    }

    private ClientChatComposing(long p2, int p4, String p5)
    {
        this.a = p2;
        this.b = p4;
        this.c = p5;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private int a()
    {
        return this.b;
    }

    private String b()
    {
        return this.c;
    }

    private long c()
    {
        return this.a;
    }

    public String toString()
    {
        return new StringBuilder("ClientChatComposing [serverConnectionHandlerID=").append(this.a).append(", clientID=").append(this.b).append(", clientUniqueIdentity=").append(this.c).append("]").toString();
    }
}
