package com.teamspeak.ts3client.jni.events;
public class ClientIDs implements com.teamspeak.ts3client.jni.k {
    public String a;
    public int b;
    private long c;
    private String d;

    public ClientIDs()
    {
        return;
    }

    private ClientIDs(long p2, String p4, int p5, String p6)
    {
        this.c = p2;
        this.a = p4;
        this.b = p5;
        this.d = p6;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private long a()
    {
        return this.c;
    }

    private String b()
    {
        return this.a;
    }

    private int c()
    {
        return this.b;
    }

    private String d()
    {
        return this.d;
    }

    public String toString()
    {
        return new StringBuilder("ClientIDs [serverConnectionHandlerID=").append(this.c).append(", uniqueClientIdentifier=").append(this.a).append(", clientID=").append(this.b).append(", clientName=").append(this.d).append("]").toString();
    }
}
