package com.teamspeak.ts3client.jni.events.rare;
public class ServerGroupListFinished implements com.teamspeak.ts3client.jni.k {
    private long a;

    public ServerGroupListFinished()
    {
        return;
    }

    private ServerGroupListFinished(long p2)
    {
        this.a = p2;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private long a()
    {
        return this.a;
    }

    public String toString()
    {
        return new StringBuilder("ServerGrouplistFinished [serverConnectionHandlerID=").append(this.a).append("]").toString();
    }
}
