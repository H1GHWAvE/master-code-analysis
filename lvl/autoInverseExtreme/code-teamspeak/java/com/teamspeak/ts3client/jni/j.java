package com.teamspeak.ts3client.jni;
public final enum class j extends java.lang.Enum {
    public static final enum com.teamspeak.ts3client.jni.j a;
    public static final enum com.teamspeak.ts3client.jni.j b;
    public static final enum com.teamspeak.ts3client.jni.j c;
    private static final synthetic com.teamspeak.ts3client.jni.j[] e;
    private int d;

    static j()
    {
        com.teamspeak.ts3client.jni.j.a = new com.teamspeak.ts3client.jni.j("ENTER_VISIBILITY", 0, 0);
        com.teamspeak.ts3client.jni.j.b = new com.teamspeak.ts3client.jni.j("RETAIN_VISIBILITY", 1, 1);
        com.teamspeak.ts3client.jni.j.c = new com.teamspeak.ts3client.jni.j("LEAVE_VISIBILITY", 2, 2);
        com.teamspeak.ts3client.jni.j[] v0_7 = new com.teamspeak.ts3client.jni.j[3];
        v0_7[0] = com.teamspeak.ts3client.jni.j.a;
        v0_7[1] = com.teamspeak.ts3client.jni.j.b;
        v0_7[2] = com.teamspeak.ts3client.jni.j.c;
        com.teamspeak.ts3client.jni.j.e = v0_7;
        return;
    }

    private j(String p1, int p2, int p3)
    {
        this(p1, p2);
        this.d = p3;
        return;
    }

    private int a()
    {
        return this.d;
    }

    private static int a(int p0)
    {
        return p0;
    }

    public static com.teamspeak.ts3client.jni.j valueOf(String p1)
    {
        return ((com.teamspeak.ts3client.jni.j) Enum.valueOf(com.teamspeak.ts3client.jni.j, p1));
    }

    public static com.teamspeak.ts3client.jni.j[] values()
    {
        return ((com.teamspeak.ts3client.jni.j[]) com.teamspeak.ts3client.jni.j.e.clone());
    }
}
