package com.teamspeak.ts3client.jni;
public class Ts3Jni {
    private static com.teamspeak.ts3client.jni.Ts3Jni a;
    private static int b;

    public Ts3Jni()
    {
        return;
    }

    public static int a()
    {
        return com.teamspeak.ts3client.jni.Ts3Jni.b;
    }

    private int a(long p10, long p12, com.teamspeak.ts3client.jni.i p14, double p15)
    {
        return this.ts3client_setServerVariableAsDouble(p10, p12, p14.aD, p15);
    }

    private int a(long p10, long p12, com.teamspeak.ts3client.jni.i p14, int p15)
    {
        return this.ts3client_setServerVariableAsInt(p10, p12, p14.aD, p15);
    }

    private int a(long p10, long p12, com.teamspeak.ts3client.jni.i p14, long p15)
    {
        return this.ts3client_setServerVariableAsUint64(p10, p12, p14.aD, p15);
    }

    private float b(long p2, com.teamspeak.ts3client.jni.f p4)
    {
        return this.ts3client_getServerConnectionVariableAsFloat(p2, p4.ap);
    }

    public static declared_synchronized com.teamspeak.ts3client.jni.Ts3Jni b()
    {
        try {
            if (com.teamspeak.ts3client.jni.Ts3Jni.a == null) {
                try {
                    System.loadLibrary("ts3client_android");
                    System.out.println("Load Lib");
                } catch (int v0) {
                }
                int v0_4 = new com.teamspeak.ts3client.jni.Ts3Jni();
                com.teamspeak.ts3client.jni.Ts3Jni.a = v0_4;
                com.teamspeak.ts3client.jni.Ts3Jni.b = v0_4.ts3client_startInit();
            }
        } catch (int v0_6) {
            throw v0_6;
        }
        return com.teamspeak.ts3client.jni.Ts3Jni.a;
    }

    private String b(long p2, com.teamspeak.ts3client.jni.d p4)
    {
        return this.ts3client_getClientSelfVariableAsString(p2, p4.ak);
    }

    public static void c()
    {
        if (com.teamspeak.ts3client.jni.Ts3Jni.a != null) {
            try {
                com.teamspeak.ts3client.jni.Ts3Jni.a.ts3client_destroyClientLib();
            } catch (int v0) {
            }
            com.teamspeak.ts3client.jni.Ts3Jni.a = 0;
        }
        return;
    }

    public final int a(long p2, int p4, com.teamspeak.ts3client.jni.d p5)
    {
        return this.ts3client_getClientVariableAsInt(p2, p4, p5.ak);
    }

    public final int a(long p8, long p10, com.teamspeak.ts3client.jni.c p12)
    {
        return this.ts3client_getChannelVariableAsInt(p8, p10, p12.I);
    }

    public final int a(long p10, long p12, com.teamspeak.ts3client.jni.c p14, int p15)
    {
        return this.ts3client_setChannelVariableAsInt(p10, p12, p14.I, p15);
    }

    public final int a(long p10, long p12, com.teamspeak.ts3client.jni.c p14, long p15)
    {
        return this.ts3client_setChannelVariableAsUInt64(p10, p12, p14.I, p15);
    }

    public final int a(long p10, long p12, com.teamspeak.ts3client.jni.c p14, String p15)
    {
        return this.ts3client_setChannelVariableAsString(p10, p12, p14.I, p15);
    }

    public final int a(long p10, long p12, com.teamspeak.ts3client.jni.i p14, String p15)
    {
        return this.ts3client_setServerVariableAsString(p10, p12, p14.aD, p15);
    }

    public final int a(long p2, com.teamspeak.ts3client.jni.d p4)
    {
        return this.ts3client_getClientSelfVariableAsInt(p2, p4.ak);
    }

    public final int a(long p2, com.teamspeak.ts3client.jni.d p4, int p5)
    {
        return this.ts3client_setClientSelfVariableAsInt(p2, p4.ak, p5);
    }

    public final int a(long p2, com.teamspeak.ts3client.jni.d p4, String p5)
    {
        return this.ts3client_setClientSelfVariableAsString(p2, p4.ak, p5);
    }

    public final int a(long p2, com.teamspeak.ts3client.jni.i p4)
    {
        return this.ts3client_getServerVariableAsInt(p2, p4.aD);
    }

    public final long a(long p4, int p6, com.teamspeak.ts3client.jni.f p7)
    {
        return this.ts3client_getConnectionVariableAsUInt64(p4, p6, p7.ap);
    }

    public final long a(long p4, com.teamspeak.ts3client.jni.f p6)
    {
        return this.ts3client_getServerConnectionVariableAsUInt64(p4, p6.ap);
    }

    public final double b(long p4, int p6, com.teamspeak.ts3client.jni.f p7)
    {
        return this.ts3client_getConnectionVariableAsDouble(p4, p6, p7.ap);
    }

    public final String b(long p2, int p4, com.teamspeak.ts3client.jni.d p5)
    {
        return this.ts3client_getClientVariableAsString(p2, p4, p5.ak);
    }

    public final String b(long p8, long p10, com.teamspeak.ts3client.jni.c p12)
    {
        return this.ts3client_getChannelVariableAsString(p8, p10, p12.I);
    }

    public final String b(long p2, com.teamspeak.ts3client.jni.i p4)
    {
        return this.ts3client_getServerVariableAsString(p2, p4.aD);
    }

    public final long c(long p4, int p6, com.teamspeak.ts3client.jni.d p7)
    {
        return this.ts3client_getClientVariableAsUInt64(p4, p6, p7.ak);
    }

    public final long c(long p8, long p10, com.teamspeak.ts3client.jni.c p12)
    {
        return this.ts3client_getChannelVariableAsUInt64(p8, p10, p12.I);
    }

    public final long c(long p4, com.teamspeak.ts3client.jni.i p6)
    {
        return this.ts3client_getServerVariableAsUInt64(p4, p6.aD);
    }

    public final String c(long p2, int p4, com.teamspeak.ts3client.jni.f p5)
    {
        return this.ts3client_getConnectionVariableAsString(p2, p4, p5.ap);
    }

    public native short[] requestAudioData();

    public native int ts3client_activateCaptureDevice();

    public native int ts3client_allowWhispersFrom();

    public native int ts3client_android_checkSignatureData();

    public native int ts3client_android_getLicenseAgreementVersion();

    public native String ts3client_android_getUpdaterURL();

    public native boolean ts3client_android_parseProtobufDataResult();

    public native int ts3client_banadd();

    public native int ts3client_banclient();

    public native int ts3client_banclientUID();

    public native int ts3client_banclientdbid();

    public native int ts3client_bandel();

    public native int ts3client_bandelall();

    public native boolean ts3client_bbcode_shouldPrependHTTP();

    public native int ts3client_cleanUpConnectionInfo();

    public native int ts3client_clientChatClosed();

    public native int ts3client_clientChatComposing();

    public native int ts3client_closeCaptureDevice();

    public native int ts3client_closePlaybackDevice();

    public native String ts3client_convertBBCodetoHTMLtags_v2();

    public native String ts3client_createIdentity();

    public native int ts3client_destroyClientLib();

    public native int ts3client_destroyServerConnectionHandler();

    public native int ts3client_flushChannelCreation();

    public native int ts3client_flushChannelUpdates();

    public native int ts3client_flushClientSelfUpdates();

    public native int ts3client_flushServerUpdates();

    public native int[] ts3client_getChannelClientList();

    public native long[] ts3client_getChannelList();

    public native long ts3client_getChannelOfClient();

    public native int ts3client_getChannelVariableAsInt();

    public native String ts3client_getChannelVariableAsString();

    public native long ts3client_getChannelVariableAsUInt64();

    public native int ts3client_getClientID();

    public native String ts3client_getClientLibVersion();

    public native int ts3client_getClientLibVersionNumber();

    public native int[] ts3client_getClientList();

    public native int ts3client_getClientSelfVariableAsInt();

    public native String ts3client_getClientSelfVariableAsString();

    public native int ts3client_getClientVariableAsInt();

    public native String ts3client_getClientVariableAsString();

    public native long ts3client_getClientVariableAsUInt64();

    public native double ts3client_getConnectionVariableAsDouble();

    public native String ts3client_getConnectionVariableAsString();

    public native long ts3client_getConnectionVariableAsUInt64();

    public native long ts3client_getParentChannelOfChannel();

    public native float ts3client_getPlaybackConfigValueAsFloat();

    public native String ts3client_getPreProcessorConfigValue();

    public native float ts3client_getPreProcessorInfoValueFloat();

    public native float ts3client_getServerConnectionVariableAsFloat();

    public native long ts3client_getServerConnectionVariableAsUInt64();

    public native int ts3client_getServerLicenseType();

    public native int ts3client_getServerVariableAsInt();

    public native String ts3client_getServerVariableAsString();

    public native long ts3client_getServerVariableAsUInt64();

    public native String ts3client_identityStringToFilename();

    public native int ts3client_openCaptureDevice();

    public native int ts3client_openPlaybackDevice();

    public native int ts3client_prepareAudioDevice();

    public native int ts3client_privilegeKeyUse();

    public native int ts3client_processCustomCaptureData();

    public native int ts3client_removeFromAllowedWhispersFrom();

    public native int ts3client_requestBanList();

    public native int ts3client_requestChannelClientAddPerm();

    public native int ts3client_requestChannelClientDelPerm();

    public native int ts3client_requestChannelDelete();

    public native int ts3client_requestChannelDescription();

    public native int ts3client_requestChannelGroupList();

    public native int ts3client_requestChannelMove();

    public native int ts3client_requestChannelSubscribe();

    public native int ts3client_requestChannelSubscribeAll();

    public native int ts3client_requestChannelUnsubscribe();

    public native int ts3client_requestChannelUnsubscribeAll();

    public native int ts3client_requestClientIDs();

    public native int ts3client_requestClientKickFromChannel();

    public native int ts3client_requestClientKickFromServer();

    public native int ts3client_requestClientMove();

    public native int ts3client_requestClientPoke();

    public native int ts3client_requestClientSetIsTalker();

    public native int ts3client_requestClientUIDfromClientID();

    public native int ts3client_requestClientVariables();

    public native int ts3client_requestComplainAdd();

    public native int ts3client_requestConnectionInfo();

    public native int ts3client_requestFile();

    public native int ts3client_requestFileInfo();

    public native int ts3client_requestIsTalker();

    public native int ts3client_requestMuteClients();

    public native int ts3client_requestPermissionList();

    public native int ts3client_requestSendChannelTextMsg();

    public native int ts3client_requestSendPrivateTextMsg();

    public native int ts3client_requestSendServerTextMsg();

    public native int ts3client_requestServerConnectionInfo();

    public native int ts3client_requestServerGroupAddClient();

    public native int ts3client_requestServerGroupDelClient();

    public native int ts3client_requestServerGroupList();

    public native int ts3client_requestServerTemporaryPasswordAdd();

    public native int ts3client_requestServerTemporaryPasswordDel();

    public native int ts3client_requestServerTemporaryPasswordList();

    public native int ts3client_requestServerVariables();

    public native int ts3client_requestSetClientChannelGroup();

    public native int ts3client_requestUnmuteClients();

    public native int ts3client_setChannelVariableAsInt();

    public native int ts3client_setChannelVariableAsString();

    public native int ts3client_setChannelVariableAsUInt64();

    public native int ts3client_setClientSelfVariableAsInt();

    public native int ts3client_setClientSelfVariableAsString();

    public native int ts3client_setClientVolumeModifier();

    public native int ts3client_setLocalTestMode();

    public native int ts3client_setPlaybackConfigValue();

    public native int ts3client_setPreProcessorConfigValue();

    public native int ts3client_setServerVariableAsDouble();

    public native int ts3client_setServerVariableAsInt();

    public native int ts3client_setServerVariableAsString();

    public native int ts3client_setServerVariableAsUint64();

    public native long ts3client_spawnNewServerConnectionHandler();

    public native int ts3client_startConnection();

    public native int ts3client_startConnectionEx();

    public native int ts3client_startInit();

    public native int ts3client_stopConnection();

    public native int ts3client_unregisterCustomDevice();

    public native int ts3client_verifyChannelPassword();
}
