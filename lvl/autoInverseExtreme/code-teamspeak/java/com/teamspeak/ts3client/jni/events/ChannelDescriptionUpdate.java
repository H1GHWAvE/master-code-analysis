package com.teamspeak.ts3client.jni.events;
public class ChannelDescriptionUpdate implements com.teamspeak.ts3client.jni.k {
    private long a;
    private long b;

    public ChannelDescriptionUpdate()
    {
        return;
    }

    private ChannelDescriptionUpdate(long p2, long p4)
    {
        this.a = p2;
        this.b = p4;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private long a()
    {
        return this.b;
    }

    private long b()
    {
        return this.a;
    }

    public String toString()
    {
        return new StringBuilder("ChannelDescriptionUpdate [serverConnectionHandlerID=").append(this.a).append(", channelID=").append(this.b).append("]").toString();
    }
}
