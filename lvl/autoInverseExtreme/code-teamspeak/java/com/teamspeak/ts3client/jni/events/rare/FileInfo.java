package com.teamspeak.ts3client.jni.events.rare;
public class FileInfo implements com.teamspeak.ts3client.jni.k {
    public String a;
    public long b;
    private long c;
    private long d;
    private long e;

    public FileInfo()
    {
        return;
    }

    private FileInfo(long p1, long p3, String p5, long p6, long p8)
    {
        this.c = p1;
        this.d = p3;
        this.a = p5;
        this.b = p6;
        this.e = p8;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private long a()
    {
        return this.c;
    }

    private long b()
    {
        return this.d;
    }

    private String c()
    {
        return this.a;
    }

    private long d()
    {
        return this.b;
    }

    private long e()
    {
        return this.e;
    }

    public String toString()
    {
        return new StringBuilder("FileInfo [serverConnectionHandlerID=").append(this.c).append(", channelID=").append(this.d).append(", name=").append(this.a).append(", size=").append(this.b).append(", datetime=").append(this.e).append("]").toString();
    }
}
