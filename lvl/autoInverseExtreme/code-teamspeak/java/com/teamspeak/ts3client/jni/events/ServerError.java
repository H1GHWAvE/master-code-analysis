package com.teamspeak.ts3client.jni.events;
public class ServerError implements com.teamspeak.ts3client.jni.k {
    public String a;
    public int b;
    public String c;
    private long d;
    private String e;

    public ServerError()
    {
        return;
    }

    private ServerError(long p2, String p4, int p5, String p6, String p7)
    {
        this.d = p2;
        this.a = p4;
        this.b = p5;
        this.c = p6;
        this.e = p7;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private long e()
    {
        return this.d;
    }

    public final int a()
    {
        return this.b;
    }

    public final String b()
    {
        return this.a;
    }

    public final String c()
    {
        return this.e;
    }

    public final String d()
    {
        return this.c;
    }

    public String toString()
    {
        return new StringBuilder("ServerError [serverConnectionHandlerID=").append(this.d).append(", errorMessage=").append(this.a).append(", error=").append(this.b).append(", returnCode=").append(this.c).append(", extraMessage=").append(this.e).append("]").toString();
    }
}
