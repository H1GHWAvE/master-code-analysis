package com.teamspeak.ts3client.jni.events;
public class IgnoredWhisper implements com.teamspeak.ts3client.jni.k {
    private long a;
    private int b;

    public IgnoredWhisper()
    {
        return;
    }

    private IgnoredWhisper(long p2, int p4)
    {
        this.a = p2;
        this.b = p4;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    public final int a()
    {
        return this.b;
    }

    public final long b()
    {
        return this.a;
    }

    public String toString()
    {
        return new StringBuilder("IgnoredWhisper [serverConnectionHandlerID=").append(this.a).append(", clientID=").append(this.b).append("]").toString();
    }
}
