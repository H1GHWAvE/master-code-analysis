package com.teamspeak.ts3client.jni.events.rare;
public class ClientChatClosed implements com.teamspeak.ts3client.jni.k {
    private long a;
    private int b;
    private String c;

    public ClientChatClosed()
    {
        return;
    }

    private ClientChatClosed(long p2, int p4, String p5)
    {
        this.a = p2;
        this.b = p4;
        this.c = p5;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private int b()
    {
        return this.b;
    }

    private long c()
    {
        return this.a;
    }

    public final String a()
    {
        return this.c;
    }

    public String toString()
    {
        return new StringBuilder("ClientChatClosed [serverConnectionHandlerID=").append(this.a).append(", clientID=").append(this.b).append(", clientUniqueIdentity=").append(this.c).append("]").toString();
    }
}
