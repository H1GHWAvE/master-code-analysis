package com.teamspeak.ts3client.jni.events.rare;
public class ChannelGroupList implements com.teamspeak.ts3client.jni.k {
    private long a;
    private long b;
    private String c;
    private int d;
    private long e;
    private int f;
    private int g;
    private int h;
    private int i;
    private int j;
    private int k;

    public ChannelGroupList()
    {
        return;
    }

    private ChannelGroupList(long p6, long p8, String p10, int p11, long p12, int p14, int p15, int p16, int p17, int p18, int p19)
    {
        this.a = p6;
        this.b = p8;
        this.c = p10;
        this.d = p11;
        this.e = (2.1219957905e-314 & p12);
        this.f = p14;
        this.g = p15;
        this.h = p16;
        this.i = p17;
        this.j = p18;
        this.k = p19;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private long c()
    {
        return this.b;
    }

    private String d()
    {
        return this.c;
    }

    private int e()
    {
        return this.h;
    }

    private int f()
    {
        return this.j;
    }

    private int g()
    {
        return this.k;
    }

    private int h()
    {
        return this.i;
    }

    private int i()
    {
        return this.f;
    }

    private long j()
    {
        return this.a;
    }

    private int k()
    {
        return this.g;
    }

    private int l()
    {
        return this.d;
    }

    public final com.teamspeak.ts3client.data.a.a a()
    {
        return new com.teamspeak.ts3client.data.a.a(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k);
    }

    public final long b()
    {
        return this.e;
    }

    public String toString()
    {
        return new StringBuilder("ChannelGroupList [serverConnectionHandlerID=").append(this.a).append(", channelGroupID=").append(this.b).append(", name=").append(this.c).append(", type=").append(this.d).append(", iconID=").append(this.e).append(", saveDB=").append(this.f).append(", sortID=").append(this.g).append(", nameMode=").append(this.h).append(", neededModifyPower=").append(this.i).append(", neededMemberAddPower=").append(this.j).append(", neededMemberRemovePower=").append(this.k).append("]").toString();
    }
}
