package com.teamspeak.ts3client.jni.events;
public class ServerConnectionInfo implements com.teamspeak.ts3client.jni.k {
    private long a;

    public ServerConnectionInfo()
    {
        return;
    }

    private ServerConnectionInfo(long p2)
    {
        this.a = p2;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private long a()
    {
        return this.a;
    }

    public String toString()
    {
        return new StringBuilder("ServerConnectionInfo [serverConnectionHandlerID=").append(this.a).append("]").toString();
    }
}
