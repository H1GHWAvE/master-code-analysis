package com.teamspeak.ts3client.jni.events;
public class ChannelUnsubscribe implements com.teamspeak.ts3client.jni.k {
    private long a;
    private long b;

    public ChannelUnsubscribe()
    {
        return;
    }

    private ChannelUnsubscribe(long p2, long p4)
    {
        this.a = p2;
        this.b = p4;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private long b()
    {
        return this.a;
    }

    public final long a()
    {
        return this.b;
    }

    public String toString()
    {
        return new StringBuilder("ChannelUnsubscribe [serverConnectionHandlerID=").append(this.a).append(", channelID=").append(this.b).append("]").toString();
    }
}
