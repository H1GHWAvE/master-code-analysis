package com.teamspeak.ts3client.jni.events;
public class UpdateClient implements com.teamspeak.ts3client.jni.k {
    public int a;
    private long b;
    private int c;
    private String d;
    private String e;

    public UpdateClient()
    {
        return;
    }

    private UpdateClient(long p2, int p4, int p5, String p6, String p7)
    {
        this.b = p2;
        this.a = p4;
        this.c = p5;
        this.d = p6;
        this.e = p7;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private int b()
    {
        return this.c;
    }

    private String c()
    {
        return this.d;
    }

    private String d()
    {
        return this.e;
    }

    private long e()
    {
        return this.b;
    }

    public final int a()
    {
        return this.a;
    }

    public String toString()
    {
        return new StringBuilder("UpdateClient [serverConnectionHandlerID=").append(this.b).append(", clientID=").append(this.a).append(", invokerID=").append(this.c).append(", invokerName=").append(this.d).append(", invokerUniqueIdentifier=").append(this.e).append("]").toString();
    }
}
