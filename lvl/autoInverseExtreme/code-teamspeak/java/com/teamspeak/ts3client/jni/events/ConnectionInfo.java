package com.teamspeak.ts3client.jni.events;
public class ConnectionInfo implements com.teamspeak.ts3client.jni.k {
    public int a;
    private long b;

    public ConnectionInfo()
    {
        return;
    }

    private ConnectionInfo(long p2, int p4)
    {
        this.b = p2;
        this.a = p4;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private long a()
    {
        return this.b;
    }

    private int b()
    {
        return this.a;
    }

    public String toString()
    {
        return new StringBuilder("ConnectionInfo [serverConnectionHandlerID=").append(this.b).append(", clientID=").append(this.a).append("]").toString();
    }
}
