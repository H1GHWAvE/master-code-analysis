package com.teamspeak.ts3client.jni.events.rare;
public class ServerPermissionError implements com.teamspeak.ts3client.jni.k {
    public int a;
    private String b;
    private int c;
    private long d;

    public ServerPermissionError()
    {
        return;
    }

    private ServerPermissionError(long p2, String p4, int p5, int p6)
    {
        this.b = p4;
        this.c = p5;
        this.a = p6;
        this.d = p2;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private long d()
    {
        return this.d;
    }

    public final int a()
    {
        return this.c;
    }

    public final String b()
    {
        return this.b;
    }

    public final int c()
    {
        return this.a;
    }

    public String toString()
    {
        return new StringBuilder("ServerPermissionError [errorMessage=").append(this.b).append(", error=").append(this.c).append(", failedPermissionID=").append(this.a).append(", serverConnectionHandlerID=").append(this.d).append("]").toString();
    }
}
