package com.teamspeak.ts3client;
public class StartGUIFragment extends android.support.v7.app.i implements android.hardware.SensorEventListener, android.support.v4.app.bk, com.teamspeak.ts3client.d.l, com.teamspeak.ts3client.data.z {
    public static final String m = "preferences";
    private static final String t = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAo0t0xR6Ch8m7hhrWgMBjBBdSaOA/wFTSk27DMKnqsYrw/AgxJjK07KyxMnRYojLsX34Cy3TCmPbcK3V9mKCjwlakgBYTObiVKRnnF1wu20WmsOb0wjbRbhYGDbhmAwc7Sy8sXNehiuz/O+PTSHcwWvHL20zum2cmHDmCAjREb1ZlA4myNN/tY9DYNMkQjOpMqgOCIQbyXdJLinOzwrgeX7x2IXMOJ7bbxWEmuyRXviOijp/1hQfpweo6slwfuc9iQDPBQl77M9dObMUrr+/23pxEE2i+U8WsePwTIw4uxkyEjew3ZRkVmFTtTqdp77tcOOFJ2hn4KkhLkM0hSaLLRQIDAQAB";
    private static final byte[] u = None;
    private static final String v = "Axdk3D";
    private static final int w = 7474;
    private static String x;
    private com.teamspeak.ts3client.data.b.c A;
    private android.app.AlertDialog B;
    private boolean C;
    private int D;
    private com.teamspeak.ts3client.co E;
    private android.os.PowerManager$WakeLock F;
    private android.hardware.SensorManager G;
    private android.hardware.Sensor H;
    private int I;
    private android.os.Handler J;
    private com.teamspeak.ts3client.cm K;
    private com.a.a.a.a.k L;
    private android.view.View M;
    private String N;
    private com.teamspeak.ts3client.d.c.a O;
    private com.teamspeak.ts3client.d.t P;
    public com.teamspeak.ts3client.Ts3Application n;
    public com.teamspeak.ts3client.bookmark.e o;
    boolean p;
    public com.teamspeak.ts3client.b q;
    boolean r;
    boolean s;
    private android.os.Bundle y;
    private com.teamspeak.ts3client.data.b.f z;

    static StartGUIFragment()
    {
        int v0_1 = new byte[20];
        v0_1 = {111, -46, 92, 23, -111, 95, 16, 23, -105, 71, -62, 78, 35, 47, -121, -12, -108, 12, -35, -46};
        com.teamspeak.ts3client.StartGUIFragment.u = v0_1;
        com.teamspeak.ts3client.StartGUIFragment.x = 0;
        return;
    }

    public StartGUIFragment()
    {
        this.C = 1;
        this.p = 0;
        this.D = 2;
        this.N = "734ea6e2d3c02045caa058dd1a7bb1b8";
        return;
    }

    private void A()
    {
        android.net.wifi.WifiManager$WifiLock v0_4 = ((android.net.wifi.WifiManager) this.n.getApplicationContext().getSystemService("wifi")).createWifiLock(3, "Ts3WifiLOCK");
        v0_4.acquire();
        this.n.n = v0_4;
        return;
    }

    private void B()
    {
        net.hockeyapp.android.b.a(this, this.N);
        return;
    }

    private void C()
    {
        this.r = 1;
        return;
    }

    private void D()
    {
        int v0_0 = 0;
        java.util.logging.Logger v1_1 = java.util.logging.Logger.getLogger("com.teamspeak.ts");
        try {
            java.util.logging.SimpleFormatter v2_1 = new java.io.File(new StringBuilder().append(android.os.Environment.getExternalStorageDirectory()).append("/TS3/logs/").toString());
        } catch (int v0) {
            v1_1.setLevel(java.util.logging.Level.ALL);
            this.n.d = v1_1;
            return;
        }
        if (!v2_1.exists()) {
            v2_1.mkdirs();
        }
        java.util.logging.SimpleFormatter v2_2 = v2_1.listFiles();
        java.util.Date v4_3 = (System.currentTimeMillis() - 604800000);
        if (v2_2 != null) {
            String v3_6 = v2_2.length;
            while (v0_0 < v3_6) {
                java.util.logging.SimpleFormatter v6_1 = v2_2[v0_0];
                if (v6_1.lastModified() < v4_3) {
                    v6_1.delete();
                }
                v0_0++;
            }
        }
        if (!this.n.e.getBoolean("create_debuglog", 0)) {
            v1_1.setLevel(java.util.logging.Level.ALL);
            this.n.d = v1_1;
            return;
        } else {
            int v0_5 = new java.util.logging.FileHandler(new StringBuilder().append(android.os.Environment.getExternalStorageDirectory()).append("/TS3/logs/").append(new java.text.SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(new java.util.Date())).append(".log").toString());
            v0_5.setFormatter(new java.util.logging.SimpleFormatter());
            v1_1.addHandler(v0_5);
            v1_1.setLevel(java.util.logging.Level.ALL);
            this.n.d = v1_1;
            return;
        }
    }

    private void E()
    {
        java.util.logging.Logger v0_1 = new java.io.File(new StringBuilder().append(android.os.Environment.getExternalStorageDirectory()).append("/TS3/content/lang").toString());
        if (!v0_1.exists()) {
            v0_1.mkdirs();
        }
        java.util.logging.Level v1_12 = new java.io.File(new StringBuilder().append(new java.io.File(new StringBuilder().append(android.os.Environment.getExternalStorageDirectory()).append("/TS3").toString()).getAbsoluteFile()).append("/Update.apk").toString());
        if ((v1_12.exists()) && (v1_12.isFile())) {
            v1_12.delete();
        }
        java.util.logging.Logger v0_11 = new java.io.File(new StringBuilder().append(android.os.Environment.getExternalStorageDirectory()).append("/TS3/cache").toString());
        if (!v0_11.exists()) {
            v0_11.mkdirs();
        }
        java.util.logging.Level v1_20 = new java.io.File(new StringBuilder().append(v0_11.getAbsoluteFile()).append("/.nomedia").toString());
        if (!v1_20.exists()) {
            try {
                v1_20.createNewFile();
            } catch (java.util.logging.Logger v0) {
                this.n.d.log(java.util.logging.Level.SEVERE, "Could not create .nomedia File");
            }
        }
        return;
    }

    private void F()
    {
        if (this.c().f() > 0) {
            this.c().e();
        }
        com.teamspeak.ts3client.t v0_4 = this.c().a();
        v0_4.b(this.n.c);
        v0_4.f(this.c().a("Bookmark"));
        v0_4.a(8194);
        v0_4.i();
        this.l();
        com.teamspeak.ts3client.t v0_7 = this.n.a.k;
        v0_7.B();
        v0_7.a.c = v0_7;
        return;
    }

    private com.teamspeak.ts3client.b G()
    {
        return this.q;
    }

    private void H()
    {
        android.content.SharedPreferences$Editor v1_1 = this.getSharedPreferences("preferences", 0);
        String v2_1 = v1_1.getInt("version", -1);
        try {
            com.teamspeak.ts3client.b v0_1 = this.getPackageManager().getPackageInfo(this.getPackageName(), 0).versionCode;
        } catch (NullPointerException v3) {
        } catch (NullPointerException v3) {
        }
        if (v2_1 < v0_1) {
            android.content.SharedPreferences$Editor v1_2 = v1_1.edit();
            v1_2.putInt("version", v0_1);
            v1_2.commit();
            if (this.B == null) {
                this.q.show();
            }
        }
        return;
    }

    private void I()
    {
        this.B = new android.app.AlertDialog$Builder(this).create();
        this.B.setTitle("Permission missing");
        this.B.setMessage("We are missing a required permission!");
        this.B.setButton(-2, com.teamspeak.ts3client.data.e.a.a("button.exit"), new com.teamspeak.ts3client.cb(this));
        this.B.setCancelable(0);
        this.B.show();
        return;
    }

    private void J()
    {
        if (android.os.Build$VERSION.SDK_INT >= 23) {
            if (android.support.v4.c.h.a(this, "android.permission.RECORD_AUDIO") == 0) {
                this.w();
                this.L();
            } else {
                String[] v0_3 = new String[1];
                v0_3[0] = "android.permission.RECORD_AUDIO";
                android.support.v4.app.m.a(this, v0_3, 1);
            }
        } else {
            this.w();
        }
        return;
    }

    private void K()
    {
        if (android.support.v4.c.h.a(this, "android.permission.RECORD_AUDIO") == 0) {
            this.w();
            this.L();
        } else {
            String[] v0_2 = new String[1];
            v0_2[0] = "android.permission.RECORD_AUDIO";
            android.support.v4.app.m.a(this, v0_2, 1);
        }
        return;
    }

    private void L()
    {
        if (android.support.v4.c.h.a(this, "android.permission.READ_EXTERNAL_STORAGE") == 0) {
            this.M();
        } else {
            String[] v0_3 = new String[1];
            v0_3[0] = "android.permission.READ_EXTERNAL_STORAGE";
            android.support.v4.app.m.a(this, v0_3, 2);
        }
        return;
    }

    private void M()
    {
        if (android.support.v4.c.h.a(this, "android.permission.READ_EXTERNAL_STORAGE") != 0) {
            String[] v0_3 = new String[1];
            v0_3[0] = "android.permission.READ_PHONE_STATE";
            android.support.v4.app.m.a(this, v0_3, 3);
        }
        return;
    }

    static synthetic int a(com.teamspeak.ts3client.StartGUIFragment p0, int p1)
    {
        p0.D = p1;
        return p1;
    }

    static synthetic android.app.AlertDialog a(com.teamspeak.ts3client.StartGUIFragment p1)
    {
        return p1.B;
    }

    private static declared_synchronized String a(android.content.Context p4)
    {
        try {
            if (com.teamspeak.ts3client.StartGUIFragment.x == null) {
                android.content.SharedPreferences$Editor v0_2 = p4.getSharedPreferences("Axdk3D", 0);
                String v2_2 = v0_2.getString("Axdk3D", 0);
                com.teamspeak.ts3client.StartGUIFragment.x = v2_2;
                if (v2_2 == null) {
                    com.teamspeak.ts3client.StartGUIFragment.x = java.util.UUID.randomUUID().toString();
                    android.content.SharedPreferences$Editor v0_3 = v0_2.edit();
                    v0_3.putString("Axdk3D", com.teamspeak.ts3client.StartGUIFragment.x);
                    v0_3.commit();
                }
            }
        } catch (android.content.SharedPreferences$Editor v0_4) {
            throw v0_4;
        }
        return com.teamspeak.ts3client.StartGUIFragment.x;
    }

    private void a(android.net.Uri p9)
    {
        com.teamspeak.ts3client.bookmark.e v0_0 = 0;
        if (p9 != null) {
            try {
                com.teamspeak.ts3client.data.ab v1_1 = java.net.URLDecoder.decode(p9.getQuery(), "UTF-8");
                java.util.HashMap v2_2 = new java.util.HashMap();
            } catch (com.teamspeak.ts3client.bookmark.e v0_50) {
                v0_50.printStackTrace();
            }
            if (!v1_1.contains("&")) {
                com.teamspeak.ts3client.bookmark.e v0_2 = v1_1.split("=");
                if (v0_2.length <= 1) {
                    v2_2.put(v0_2[0], "");
                } else {
                    v2_2.put(v0_2[0], v0_2[1]);
                }
            } else {
                com.teamspeak.ts3client.data.ab v1_7 = v1_1.split("&");
                StringBuilder v3_4 = v1_7.length;
                while (v0_0 < v3_4) {
                    String v4_1 = v1_7[v0_0].split("=");
                    if (v4_1.length <= 1) {
                        v2_2.put(v4_1[0], "");
                    } else {
                        v2_2.put(v4_1[0], v4_1[1]);
                    }
                    v0_0++;
                }
            }
            com.teamspeak.ts3client.data.ab v1_9 = new com.teamspeak.ts3client.data.ab();
            v1_9.a(p9.getHost());
            if (v2_2.containsKey("port")) {
                v1_9.a(new StringBuilder().append(v1_9.c).append(":").append(((String) v2_2.get("port"))).toString());
            }
            if (v2_2.containsKey("nickname")) {
                v1_9.f = ((String) v2_2.get("nickname"));
            }
            if (v2_2.containsKey("password")) {
                v1_9.e = ((String) v2_2.get("password"));
            }
            if (v2_2.containsKey("channel")) {
                v1_9.g = ((String) v2_2.get("channel"));
            }
            if (v2_2.containsKey("channelpassword")) {
                v1_9.h = ((String) v2_2.get("channelpassword"));
            }
            if (v2_2.containsKey("addbookmark")) {
                v1_9.a = ((String) v2_2.get("addbookmark"));
            }
            if (v2_2.containsKey("token")) {
                v1_9.i = ((String) v2_2.get("token"));
            }
            v1_9.d = com.teamspeak.ts3client.data.b.f.a().d().a;
            this.o.a(v1_9);
        }
        return;
    }

    static synthetic void a(com.teamspeak.ts3client.StartGUIFragment p8, android.net.Uri p9)
    {
        com.teamspeak.ts3client.bookmark.e v0_0 = 0;
        if (p9 != null) {
            try {
                com.teamspeak.ts3client.data.ab v1_1 = java.net.URLDecoder.decode(p9.getQuery(), "UTF-8");
                java.util.HashMap v2_2 = new java.util.HashMap();
            } catch (com.teamspeak.ts3client.bookmark.e v0_50) {
                v0_50.printStackTrace();
            }
            if (!v1_1.contains("&")) {
                com.teamspeak.ts3client.bookmark.e v0_2 = v1_1.split("=");
                if (v0_2.length <= 1) {
                    v2_2.put(v0_2[0], "");
                } else {
                    v2_2.put(v0_2[0], v0_2[1]);
                }
            } else {
                com.teamspeak.ts3client.data.ab v1_7 = v1_1.split("&");
                StringBuilder v3_4 = v1_7.length;
                while (v0_0 < v3_4) {
                    String v4_1 = v1_7[v0_0].split("=");
                    if (v4_1.length <= 1) {
                        v2_2.put(v4_1[0], "");
                    } else {
                        v2_2.put(v4_1[0], v4_1[1]);
                    }
                    v0_0++;
                }
            }
            com.teamspeak.ts3client.data.ab v1_9 = new com.teamspeak.ts3client.data.ab();
            v1_9.a(p9.getHost());
            if (v2_2.containsKey("port")) {
                v1_9.a(new StringBuilder().append(v1_9.c).append(":").append(((String) v2_2.get("port"))).toString());
            }
            if (v2_2.containsKey("nickname")) {
                v1_9.f = ((String) v2_2.get("nickname"));
            }
            if (v2_2.containsKey("password")) {
                v1_9.e = ((String) v2_2.get("password"));
            }
            if (v2_2.containsKey("channel")) {
                v1_9.g = ((String) v2_2.get("channel"));
            }
            if (v2_2.containsKey("channelpassword")) {
                v1_9.h = ((String) v2_2.get("channelpassword"));
            }
            if (v2_2.containsKey("addbookmark")) {
                v1_9.a = ((String) v2_2.get("addbookmark"));
            }
            if (v2_2.containsKey("token")) {
                v1_9.i = ((String) v2_2.get("token"));
            }
            v1_9.d = com.teamspeak.ts3client.data.b.f.a().d().a;
            p8.o.a(v1_9);
        }
        return;
    }

    private void a(com.teamspeak.ts3client.data.ab p13)
    {
        if (p13.n <= (Integer.parseInt(this.n.e.getString("reconnect.limit", "15")) - 1)) {
            com.teamspeak.ts3client.bookmark.e v0_2 = new android.content.Intent(this.n, com.teamspeak.ts3client.StartGUIFragment);
            v0_2.setAction("android.intent.action.MAIN");
            v0_2.addCategory("android.intent.category.LAUNCHER");
            com.teamspeak.ts3client.bookmark.e v0_3 = android.app.PendingIntent.getActivity(this.n, 0, v0_2, 0);
            android.app.NotificationManager v1_10 = new android.support.v4.app.dm(this.n.getApplicationContext());
            v1_10.a(2130837622).c(com.teamspeak.ts3client.data.e.a.a("connection.reconnect.retry")).a(System.currentTimeMillis()).a().a(com.teamspeak.ts3client.data.e.a.a("connection.reconnect.retry")).b(com.teamspeak.ts3client.data.e.a.a("connection.reconnect.retry")).d = v0_3;
            com.teamspeak.ts3client.bookmark.e v0_4 = v1_10.c();
            v0_4.flags = (v0_4.flags | 20);
            this.n.l.notify(0, 101, v0_4);
            p13.n = (p13.n + 1);
            this.o.a(p13);
        } else {
            com.teamspeak.ts3client.bookmark.e v0_9 = new android.content.Intent(this.n, com.teamspeak.ts3client.StartGUIFragment);
            v0_9.setAction("android.intent.action.MAIN");
            v0_9.addCategory("android.intent.category.LAUNCHER");
            com.teamspeak.ts3client.bookmark.e v0_10 = android.app.PendingIntent.getActivity(this.n, 0, v0_9, 0);
            android.app.NotificationManager v1_20 = new android.support.v4.app.dm(this.n.getApplicationContext());
            android.support.v4.app.dm v2_17 = v1_20.a(2130837622).c(com.teamspeak.ts3client.data.e.a.a("connection.reconnect")).a(System.currentTimeMillis()).a().a(com.teamspeak.ts3client.data.e.a.a("connection.reconnect"));
            long v4_3 = new Object[1];
            v4_3[0] = Integer.valueOf(Integer.parseInt(this.n.e.getString("reconnect.limit", "15")));
            v2_17.b(com.teamspeak.ts3client.data.e.a.a("connection.reconnect.text", v4_3)).d = v0_10;
            com.teamspeak.ts3client.bookmark.e v0_11 = v1_20.c();
            v0_11.flags = (v0_11.flags | 20);
            this.n.l.notify(0, 101, v0_11);
            this.p = 0;
            this.s = 1;
        }
        return;
    }

    static synthetic com.teamspeak.ts3client.cm b(com.teamspeak.ts3client.StartGUIFragment p1)
    {
        return p1.K;
    }

    private void b(int p7)
    {
        android.widget.TextView v0_6;
        this.B = new android.app.AlertDialog$Builder(this).create();
        this.B.setTitle(com.teamspeak.ts3client.data.e.a.a("critical.payment"));
        if (p7 != 291) {
            String v2_0 = new Object[1];
            v2_0[0] = Integer.valueOf(p7);
            v0_6 = new android.text.SpannableString(com.teamspeak.ts3client.data.e.a.a("critical.payment.other", v2_0));
        } else {
            String v2_1 = new Object[1];
            v2_1[0] = Integer.valueOf(p7);
            v0_6 = new android.text.SpannableString(com.teamspeak.ts3client.data.e.a.a("critical.payment.network", v2_1));
        }
        android.text.method.MovementMethod v1_7 = new android.text.SpannableString("\nhttp://play.google.com/store/apps/details?id=com.teamspeak.ts3client");
        android.text.util.Linkify.addLinks(v1_7, 15);
        String v2_4 = this.B;
        com.teamspeak.ts3client.cc v3_3 = new CharSequence[2];
        v3_3[0] = v0_6;
        v3_3[1] = v1_7;
        v2_4.setMessage(((android.text.Spanned) android.text.TextUtils.concat(v3_3)));
        this.B.setButton(-2, com.teamspeak.ts3client.data.e.a.a("button.exit"), new com.teamspeak.ts3client.bu(this));
        this.B.setButton(-3, com.teamspeak.ts3client.data.e.a.a("button.retry"), new com.teamspeak.ts3client.cc(this));
        this.B.setCancelable(0);
        this.B.show();
        ((android.widget.TextView) this.B.findViewById(16908299)).setMovementMethod(android.text.method.LinkMovementMethod.getInstance());
        return;
    }

    private void b(android.net.Uri p10)
    {
        com.teamspeak.ts3client.bookmark.a v0_0 = 0;
        android.support.v4.app.bi v1_1 = new com.teamspeak.ts3client.data.ab();
        if (p10 != null) {
            try {
                String v2_1 = new java.util.HashMap();
            } catch (com.teamspeak.ts3client.bookmark.a v0_48) {
                v0_48.printStackTrace();
            }
            if (p10.getQuery() != null) {
                String v3_2 = java.net.URLDecoder.decode(p10.getQuery(), "UTF-8");
                if (!v3_2.contains("&")) {
                    com.teamspeak.ts3client.bookmark.a v0_2 = v3_2.split("=");
                    if (v0_2.length <= 1) {
                        v2_1.put(v0_2[0], "");
                    } else {
                        v2_1.put(v0_2[0], v0_2[1]);
                    }
                } else {
                    String v3_8 = v3_2.split("&");
                    int v4_5 = v3_8.length;
                    while (v0_0 < v4_5) {
                        String v5_1 = v3_8[v0_0].split("=");
                        if (v5_1.length <= 1) {
                            v2_1.put(v5_1[0], "");
                        } else {
                            v2_1.put(v5_1[0], v5_1[1]);
                        }
                        v0_0++;
                    }
                }
            }
            v1_1.a(p10.getHost());
            if (v2_1.containsKey("port")) {
                v1_1.a(new StringBuilder().append(v1_1.c).append(":").append(((String) v2_1.get("port"))).toString());
            }
            if (v2_1.containsKey("nickname")) {
                v1_1.f = ((String) v2_1.get("nickname"));
            }
            if (v2_1.containsKey("password")) {
                v1_1.e = ((String) v2_1.get("password"));
            }
            if (v2_1.containsKey("channel")) {
                v1_1.g = ((String) v2_1.get("channel"));
            }
            if (v2_1.containsKey("channelpassword")) {
                v1_1.h = ((String) v2_1.get("channelpassword"));
            }
            if (v2_1.containsKey("addbookmark")) {
                v1_1.a = ((String) v2_1.get("addbookmark"));
            }
            if (v2_1.containsKey("token")) {
                v1_1.i = ((String) v2_1.get("token"));
            }
            new com.teamspeak.ts3client.bookmark.a(com.teamspeak.ts3client.data.b.a.a, v1_1).a(this.c(), "AddBookmark");
        }
        return;
    }

    static synthetic void b(com.teamspeak.ts3client.StartGUIFragment p6, int p7)
    {
        android.widget.TextView v0_6;
        p6.B = new android.app.AlertDialog$Builder(p6).create();
        p6.B.setTitle(com.teamspeak.ts3client.data.e.a.a("critical.payment"));
        if (p7 != 291) {
            String v2_0 = new Object[1];
            v2_0[0] = Integer.valueOf(p7);
            v0_6 = new android.text.SpannableString(com.teamspeak.ts3client.data.e.a.a("critical.payment.other", v2_0));
        } else {
            String v2_1 = new Object[1];
            v2_1[0] = Integer.valueOf(p7);
            v0_6 = new android.text.SpannableString(com.teamspeak.ts3client.data.e.a.a("critical.payment.network", v2_1));
        }
        android.text.method.MovementMethod v1_7 = new android.text.SpannableString("\nhttp://play.google.com/store/apps/details?id=com.teamspeak.ts3client");
        android.text.util.Linkify.addLinks(v1_7, 15);
        String v2_4 = p6.B;
        com.teamspeak.ts3client.cc v3_3 = new CharSequence[2];
        v3_3[0] = v0_6;
        v3_3[1] = v1_7;
        v2_4.setMessage(((android.text.Spanned) android.text.TextUtils.concat(v3_3)));
        p6.B.setButton(-2, com.teamspeak.ts3client.data.e.a.a("button.exit"), new com.teamspeak.ts3client.bu(p6));
        p6.B.setButton(-3, com.teamspeak.ts3client.data.e.a.a("button.retry"), new com.teamspeak.ts3client.cc(p6));
        p6.B.setCancelable(0);
        p6.B.show();
        ((android.widget.TextView) p6.B.findViewById(16908299)).setMovementMethod(android.text.method.LinkMovementMethod.getInstance());
        return;
    }

    static synthetic void b(com.teamspeak.ts3client.StartGUIFragment p0, android.net.Uri p1)
    {
        p0.b(p1);
        return;
    }

    static synthetic com.a.a.a.a.k c(com.teamspeak.ts3client.StartGUIFragment p1)
    {
        return p1.L;
    }

    private void c(android.net.Uri p1)
    {
        this.b(p1);
        return;
    }

    static synthetic com.teamspeak.ts3client.Ts3Application d(com.teamspeak.ts3client.StartGUIFragment p1)
    {
        return p1.n;
    }

    static synthetic void e(com.teamspeak.ts3client.StartGUIFragment p0)
    {
        p0.r();
        return;
    }

    static synthetic android.view.View f(com.teamspeak.ts3client.StartGUIFragment p1)
    {
        return p1.M;
    }

    static synthetic void g(com.teamspeak.ts3client.StartGUIFragment p4)
    {
        if (android.os.Build$VERSION.SDK_INT >= 23) {
            if (android.support.v4.c.h.a(p4, "android.permission.RECORD_AUDIO") == 0) {
                p4.w().L();
            } else {
                String[] v0_3 = new String[1];
                v0_3[0] = "android.permission.RECORD_AUDIO";
                android.support.v4.app.m.a(p4, v0_3, 1);
            }
        } else {
            p4.w();
        }
        return;
    }

    static synthetic void h(com.teamspeak.ts3client.StartGUIFragment p6)
    {
        android.content.SharedPreferences$Editor v1_1 = p6.getSharedPreferences("preferences", 0);
        String v2_1 = v1_1.getInt("version", -1);
        try {
            com.teamspeak.ts3client.b v0_1 = p6.getPackageManager().getPackageInfo(p6.getPackageName(), 0).versionCode;
        } catch (NullPointerException v3) {
        } catch (NullPointerException v3) {
        }
        if (v2_1 < v0_1) {
            android.content.SharedPreferences$Editor v1_2 = v1_1.edit();
            v1_2.putInt("version", v0_1);
            v1_2.commit();
            if (p6.B == null) {
                p6.q.show();
            }
        }
        return;
    }

    static synthetic void i(com.teamspeak.ts3client.StartGUIFragment p4)
    {
        android.view.View v0_1 = p4.findViewById(2131493316);
        com.teamspeak.ts3client.cl v1_1 = android.view.animation.AnimationUtils.loadAnimation(p4, 2130968590);
        v1_1.setDuration(2000);
        v1_1.setAnimationListener(new com.teamspeak.ts3client.ci(p4, v1_1));
        p4.runOnUiThread(new com.teamspeak.ts3client.ck(p4, v0_1, v1_1));
        v0_1.setOnTouchListener(new com.teamspeak.ts3client.cl(p4));
        return;
    }

    static synthetic int j(com.teamspeak.ts3client.StartGUIFragment p1)
    {
        return p1.D;
    }

    static synthetic android.os.Handler k(com.teamspeak.ts3client.StartGUIFragment p1)
    {
        return p1.J;
    }

    private com.teamspeak.ts3client.data.b.a p()
    {
        return this.o.b;
    }

    private void q()
    {
        if (this.P == null) {
            if ((com.teamspeak.ts3client.data.af.a().a == this.n.e.getInt("LicenseAgreement", 0)) || (this.n.a != null)) {
                this.r();
            } else {
                this.P = new com.teamspeak.ts3client.d.t(this);
                this.P.setCancelable(0);
                this.P.c = com.teamspeak.ts3client.data.af.a().a;
                this.P.a = new com.teamspeak.ts3client.ce(this);
                this.P.b = new com.teamspeak.ts3client.cf(this);
                this.P.d = new com.teamspeak.ts3client.ch(this);
                com.teamspeak.ts3client.d.t v0_12 = this.P;
                v0_12.f = com.teamspeak.ts3client.Ts3Application.a().e.getString("lang_tag", java.util.Locale.getDefault().getLanguage());
                v0_12.e = this;
                v0_12.b();
            }
        }
        return;
    }

    private void r()
    {
        com.a.a.a.a.k v0_4 = new StringBuilder().append(android.provider.Settings$Secure.getString(this.getContentResolver(), "android_id")).append(com.teamspeak.ts3client.StartGUIFragment.a(this.getBaseContext())).toString();
        this.K = new com.teamspeak.ts3client.cm(this, 0);
        this.L = new com.a.a.a.a.k(this, new com.a.a.a.a.v(this, new com.a.a.a.a.a(com.teamspeak.ts3client.StartGUIFragment.u, this.getPackageName(), v0_4)), "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAo0t0xR6Ch8m7hhrWgMBjBBdSaOA/wFTSk27DMKnqsYrw/AgxJjK07KyxMnRYojLsX34Cy3TCmPbcK3V9mKCjwlakgBYTObiVKRnnF1wu20WmsOb0wjbRbhYGDbhmAwc7Sy8sXNehiuz/O+PTSHcwWvHL20zum2cmHDmCAjREb1ZlA4myNN/tY9DYNMkQjOpMqgOCIQbyXdJLinOzwrgeX7x2IXMOJ7bbxWEmuyRXviOijp/1hQfpweo6slwfuc9iQDPBQl77M9dObMUrr+/23pxEE2i+U8WsePwTIw4uxkyEjew3ZRkVmFTtTqdp77tcOOFJ2hn4KkhLkM0hSaLLRQIDAQAB");
        this.L.a(this.K);
        com.a.a.a.a.k v0_7 = this.L;
        v0_7.a = 0;
        v0_7.a = (v0_7.a + 16);
        return;
    }

    private void s()
    {
        android.view.View v0_1 = this.findViewById(2131493316);
        com.teamspeak.ts3client.cl v1_1 = android.view.animation.AnimationUtils.loadAnimation(this, 2130968590);
        v1_1.setDuration(2000);
        v1_1.setAnimationListener(new com.teamspeak.ts3client.ci(this, v1_1));
        this.runOnUiThread(new com.teamspeak.ts3client.ck(this, v0_1, v1_1));
        v0_1.setOnTouchListener(new com.teamspeak.ts3client.cl(this));
        return;
    }

    private void t()
    {
        if (this.getIntent().hasExtra("UrlStart")) {
            int v0_5 = ((android.net.Uri) this.getIntent().getExtras().get("uri"));
            android.app.AlertDialog v1_4 = new android.app.AlertDialog$Builder(this).create();
            v1_4.setTitle(com.teamspeak.ts3client.data.e.a.a("gui.extern.info"));
            com.teamspeak.ts3client.bx v3_1 = new Object[1];
            v3_1[0] = this.getIntent().getExtras().get("address");
            v1_4.setMessage(com.teamspeak.ts3client.data.e.a.a("gui.extern.text", v3_1));
            v1_4.setButton(-1, com.teamspeak.ts3client.data.e.a.a("gui.extern.button1"), new com.teamspeak.ts3client.bv(this, v0_5, v1_4));
            v1_4.setButton(-3, com.teamspeak.ts3client.data.e.a.a("gui.extern.button2"), new com.teamspeak.ts3client.bw(this, v0_5, v1_4));
            v1_4.setButton(-2, com.teamspeak.ts3client.data.e.a.a("button.cancel"), new com.teamspeak.ts3client.bx(this, v1_4));
            v1_4.setCancelable(0);
            v1_4.show();
        }
        return;
    }

    private boolean u()
    {
        try {
            com.teamspeak.ts3client.jni.Ts3Jni.b();
            int v0_0 = 1;
        } catch (int v0) {
            this.n.d.log(java.util.logging.Level.SEVERE, "No libs found, missing CPU support!");
            v0_0 = 0;
        }
        return v0_0;
    }

    private void v()
    {
        new com.teamspeak.ts3client.data.e.a(this.getApplicationContext(), this.n.e.getString("lang_tag", ""));
        if (this.n.e.getInt("sampleRec", 0) > 44100) {
            this.n.e.edit().remove("samplePlay").commit();
            this.n.e.edit().remove("sampleRec").commit();
            this.n.d.log(java.util.logging.Level.INFO, "Cleared sample rates higher than 44100");
        }
        if (this.n.e.getInt("samplePlay", -1) == 0) {
            this.n.e.edit().remove("samplePlay").commit();
        }
        if (this.n.e.getInt("sampleRec", -1) == 0) {
            this.n.e.edit().remove("sampleRec").commit();
        }
        this.q = new com.teamspeak.ts3client.b(this);
        this.z = com.teamspeak.ts3client.data.b.f.a();
        this.A = new com.teamspeak.ts3client.data.b.c(this);
        this.o = new com.teamspeak.ts3client.bookmark.e();
        this.o.e(this.getIntent().getExtras());
        this.n.l = ((android.app.NotificationManager) this.n.getSystemService("notification"));
        this.n.l.cancelAll();
        this.n.h = ((android.net.ConnectivityManager) this.getSystemService("connectivity"));
        int v1_14 = this.n.b();
        v1_14.a = new com.teamspeak.ts3client.data.d.g(v1_14, ((((android.app.ActivityManager) v1_14.b.getSystemService("activity")).getMemoryClass() * 1048576) / 10));
        v1_14.a();
        try {
            this.n.g();
        } catch (android.app.AlertDialog v0) {
            if (this.n.e.getInt("sound_pack", 0) != 1) {
            } else {
                this.n.e.edit().putInt("sound_pack", 0).commit();
                this.n.g();
                android.app.AlertDialog v0_62 = new android.app.AlertDialog$Builder(this).create();
                v0_62.setTitle(com.teamspeak.ts3client.data.e.a.a("critical.tts"));
                v0_62.setMessage(com.teamspeak.ts3client.data.e.a.a("critical.tts.text"));
                v0_62.setButton(-3, com.teamspeak.ts3client.data.e.a.a("button.ok"), new com.teamspeak.ts3client.by(this, v0_62));
                v0_62.setCancelable(0);
                v0_62.show();
            }
        }
        return;
    }

    private void w()
    {
        this.n.j = com.teamspeak.ts3client.a.k.a(this.getApplication(), 0, 0);
        this.n.d.log(java.util.logging.Level.INFO, new StringBuilder("Found Audio RECSampleRate:").append(this.n.h()).toString());
        this.n.i = com.teamspeak.ts3client.a.k.a(this.getApplication(), 0);
        this.n.d.log(java.util.logging.Level.INFO, new StringBuilder("Found Audio PLAYSampleRate:").append(this.n.i()).toString());
        if ((this.n.h() == 0) || (this.n.i() == 0)) {
            this.B = new android.app.AlertDialog$Builder(this).create();
            this.B.setTitle(com.teamspeak.ts3client.data.e.a.a("critical.audio"));
            this.B.setMessage(com.teamspeak.ts3client.data.e.a.a("critical.audio.text"));
            this.B.setButton(-2, com.teamspeak.ts3client.data.e.a.a("button.exit"), new com.teamspeak.ts3client.bz(this));
            this.B.setCancelable(0);
            this.B.show();
        }
        return;
    }

    private void x()
    {
        if (this.findViewById(2131493319) != null) {
            this.n.c = this.o;
            this.c().a().a(2131493319, this.o, "Bookmark").i();
        }
        this.n.d.log(java.util.logging.Level.INFO, "Finished loading GUI");
        switch (this.n.e.getInt("screen_rotation", 0)) {
            case 0:
                this.setRequestedOrientation(-1);
                break;
            case 1:
                this.setRequestedOrientation(1);
                break;
            case 2:
                this.setRequestedOrientation(0);
                break;
            default:
                this.setRequestedOrientation(-1);
        }
        return;
    }

    private void y()
    {
        android.os.PowerManager$WakeLock v0_4 = ((android.os.PowerManager) this.n.getApplicationContext().getSystemService("power")).newWakeLock(1, "Ts3WakeLOCK");
        v0_4.acquire();
        this.n.b = v0_4;
        return;
    }

    private void z()
    {
        android.os.PowerManager$WakeLock v0_4 = ((android.os.PowerManager) this.n.getApplicationContext().getSystemService("power")).newWakeLock(32, "Ts3WakeLOCKSensor");
        v0_4.setReferenceCounted(0);
        this.n.m = v0_4;
        return;
    }

    public final void a()
    {
        this.o.a();
        return;
    }

    public final boolean h()
    {
        this.finish();
        return 1;
    }

    public final void j()
    {
        this.G.unregisterListener(this);
        return;
    }

    public final void k()
    {
        this.G.registerListener(this, this.H, 3);
        return;
    }

    public final void l()
    {
        this.n.c = this.o;
        int v0_2 = this.c().a();
        v0_2.e(this.n.c);
        v0_2.b(this.n.a.k, "Connection");
        v0_2.a(4097);
        if (!this.C) {
            v0_2.j();
            this.p = 1;
        } else {
            v0_2.i();
            this.p = 0;
        }
        return;
    }

    public final void m()
    {
        if (this.P == null) {
            if ((com.teamspeak.ts3client.data.af.a().a == this.n.e.getInt("LicenseAgreement", 0)) || (this.n.a != null)) {
                this.r();
            } else {
                this.P = new com.teamspeak.ts3client.d.t(this);
                this.P.setCancelable(0);
                this.P.c = com.teamspeak.ts3client.data.af.a().a;
                this.P.a = new com.teamspeak.ts3client.ce(this);
                this.P.b = new com.teamspeak.ts3client.cf(this);
                this.P.d = new com.teamspeak.ts3client.ch(this);
                com.teamspeak.ts3client.d.t v0_12 = this.P;
                v0_12.f = com.teamspeak.ts3client.Ts3Application.a().e.getString("lang_tag", java.util.Locale.getDefault().getLanguage());
                v0_12.e = this;
                v0_12.b();
            }
        }
        return;
    }

    public final void n()
    {
        if (this.P != null) {
            this.P.show();
        }
        return;
    }

    public final void o()
    {
        this.r();
        return;
    }

    public void onAccuracyChanged(android.hardware.Sensor p1, int p2)
    {
        return;
    }

    public void onActivityResult(int p4, int p5, android.content.Intent p6)
    {
        switch (p4) {
            case 666:
                if (p5 != -1) {
                } else {
                    System.exit(2);
                }
                break;
            case 7474:
                if (android.os.Build$VERSION.SDK_INT < 23) {
                } else {
                    if (!android.provider.Settings.canDrawOverlays(this)) {
                        this.n.e.edit().putBoolean("android_overlay", 0).commit();
                        this.n.e.edit().putBoolean("android_overlay_setting", 0).commit();
                    } else {
                        this.n.e.edit().putBoolean("android_overlay", 1).commit();
                    }
                }
                break;
        }
        return;
    }

    public void onBackPressed()
    {
        super.onBackPressed();
        return;
    }

    public void onConfigurationChanged(android.content.res.Configuration p1)
    {
        super.onConfigurationChanged(p1);
        return;
    }

    public void onCreate(android.os.Bundle p14)
    {
        super.onCreate(p14);
        this.n = ((com.teamspeak.ts3client.Ts3Application) this.getApplicationContext());
        this.n.e = this.getSharedPreferences("TS3Settings", 0);
        if (this.O == null) {
            this.O = new com.teamspeak.ts3client.d.c.a();
        }
        this.setContentView(2130903124);
        android.content.SharedPreferences v0_9 = ((android.support.v7.widget.Toolbar) this.findViewById(2131493318));
        if (this.n.e.getBoolean("enable_fullscreen", 1)) {
            this.getWindow().setFlags(1024, 1024);
        }
        if (v0_9 != null) {
            this.i().a(v0_9);
        }
        if (android.os.Build$VERSION.SDK_INT >= 21) {
            this.getWindow().addFlags(-2147483648);
        }
        this.y = p14;
        this.n.o = this.i().a();
        this.i().a();
        this.n.o.a("TeamSpeak");
        this.n.o.n();
        java.util.logging.Level v2_12 = java.util.logging.Logger.getLogger("com.teamspeak.ts");
        try {
            android.content.SharedPreferences v0_20 = new java.io.File(new StringBuilder().append(android.os.Environment.getExternalStorageDirectory()).append("/TS3/logs/").toString());
        } catch (android.content.SharedPreferences v0) {
            v2_12.setLevel(java.util.logging.Level.ALL);
            this.n.d = v2_12;
            android.content.SharedPreferences v0_30 = new java.io.File(new StringBuilder().append(android.os.Environment.getExternalStorageDirectory()).append("/TS3/content/lang").toString());
            if (!v0_30.exists()) {
                v0_30.mkdirs();
            }
            java.util.logging.Level v2_25 = new java.io.File(new StringBuilder().append(new java.io.File(new StringBuilder().append(android.os.Environment.getExternalStorageDirectory()).append("/TS3").toString()).getAbsoluteFile()).append("/Update.apk").toString());
            if ((v2_25.exists()) && (v2_25.isFile())) {
                v2_25.delete();
            }
            android.content.SharedPreferences v0_40 = new java.io.File(new StringBuilder().append(android.os.Environment.getExternalStorageDirectory()).append("/TS3/cache").toString());
            if (!v0_40.exists()) {
                v0_40.mkdirs();
            }
            java.util.logging.Level v2_33 = new java.io.File(new StringBuilder().append(v0_40.getAbsoluteFile()).append("/.nomedia").toString());
            if (!v2_33.exists()) {
                try {
                    v2_33.createNewFile();
                } catch (android.content.SharedPreferences v0) {
                    this.n.d.log(java.util.logging.Level.SEVERE, "Could not create .nomedia File");
                }
            }
            this.n.d.log(java.util.logging.Level.INFO, "Start");
            android.content.SharedPreferences v0_51 = Boolean.valueOf(this.u());
            this.M = this.getLayoutInflater().inflate(2130903123, 0);
            this.addContentView(this.M, new android.view.ViewGroup$LayoutParams(-1, -1));
            if (v0_51.booleanValue()) {
                this.n.f = this;
                new com.teamspeak.ts3client.data.e.a(this.getApplicationContext(), this.n.e.getString("lang_tag", ""));
                if (this.n.e.getInt("sampleRec", 0) > 44100) {
                    this.n.e.edit().remove("samplePlay").commit();
                    this.n.e.edit().remove("sampleRec").commit();
                    this.n.d.log(java.util.logging.Level.INFO, "Cleared sample rates higher than 44100");
                }
                if (this.n.e.getInt("samplePlay", -1) == 0) {
                    this.n.e.edit().remove("samplePlay").commit();
                }
                if (this.n.e.getInt("sampleRec", -1) == 0) {
                    this.n.e.edit().remove("sampleRec").commit();
                }
                this.q = new com.teamspeak.ts3client.b(this);
                this.z = com.teamspeak.ts3client.data.b.f.a();
                this.A = new com.teamspeak.ts3client.data.b.c(this);
                this.o = new com.teamspeak.ts3client.bookmark.e();
                this.o.e(this.getIntent().getExtras());
                this.n.l = ((android.app.NotificationManager) this.n.getSystemService("notification"));
                this.n.l.cancelAll();
                this.n.h = ((android.net.ConnectivityManager) this.getSystemService("connectivity"));
                java.util.logging.Level v2_53 = this.n.b();
                v2_53.a = new com.teamspeak.ts3client.data.d.g(v2_53, ((((android.app.ActivityManager) v2_53.b.getSystemService("activity")).getMemoryClass() * 1048576) / 10));
                v2_53.a();
                try {
                    this.n.g();
                } catch (android.content.SharedPreferences v0) {
                    if (this.n.e.getInt("sound_pack", 0) != 1) {
                    } else {
                        this.n.e.edit().putInt("sound_pack", 0).commit();
                        this.n.g();
                        android.content.SharedPreferences v0_116 = new android.app.AlertDialog$Builder(this).create();
                        v0_116.setTitle(com.teamspeak.ts3client.data.e.a.a("critical.tts"));
                        v0_116.setMessage(com.teamspeak.ts3client.data.e.a.a("critical.tts.text"));
                        v0_116.setButton(-3, com.teamspeak.ts3client.data.e.a.a("button.ok"), new com.teamspeak.ts3client.by(this, v0_116));
                        v0_116.setCancelable(0);
                        v0_116.show();
                    }
                }
                if (this.findViewById(2131493319) != null) {
                    this.n.c = this.o;
                    this.c().a().a(2131493319, this.o, "Bookmark").i();
                }
                this.n.d.log(java.util.logging.Level.INFO, "Finished loading GUI");
                switch (this.n.e.getInt("screen_rotation", 0)) {
                    case 0:
                        this.setRequestedOrientation(-1);
                        break;
                    case 1:
                        this.setRequestedOrientation(1);
                        break;
                    case 2:
                        this.setRequestedOrientation(0);
                        break;
                    default:
                        this.setRequestedOrientation(-1);
                }
                com.teamspeak.ts3client.data.af.a().d = this;
                this.G = ((android.hardware.SensorManager) this.getSystemService("sensor"));
                this.H = this.G.getDefaultSensor(1);
                this.J = new android.os.Handler();
                android.content.SharedPreferences v0_137 = this.getSharedPreferences("preferences", 0).getInt("version", -1);
                if (v0_137 < 12) {
                    this.getSharedPreferences("com.android.vending.licensing.ServerManagedPolicy", 0).edit().clear().commit();
                }
                if (v0_137 < 57) {
                    this.n.e.edit().remove("samplePlay").commit();
                    this.n.e.edit().remove("sampleRec").commit();
                    this.n.d.log(java.util.logging.Level.INFO, "Cleared pre 57 audio settings setting");
                }
                if (v0_137 < 70) {
                    this.n.e.edit().remove("android_overlay").commit();
                }
                this.E = new com.teamspeak.ts3client.co(this.n);
                this.n.e.registerOnSharedPreferenceChangeListener(this.E);
            } else {
                this.B = new android.app.AlertDialog$Builder(this).create();
                this.B.setTitle("Not supported");
                this.B.setMessage("The device you are using seems to be an ARMv6 device, but TeamSpeak3 requires at least an ARMv7 device.\nIf you think this is an error on our side please visit our forum @ http://forum.teamspeak.com");
                this.B.setButton(-2, "EXIT", new com.teamspeak.ts3client.cd(this));
                this.B.setCancelable(0);
                this.B.show();
            }
            return;
        }
        if (!v0_20.exists()) {
            v0_20.mkdirs();
        }
        String v3_7 = v0_20.listFiles();
        com.teamspeak.ts3client.by v4_4 = (System.currentTimeMillis() - 604800000);
        if (v3_7 != null) {
            int v6_1 = v3_7.length;
            android.content.SharedPreferences v0_21 = 0;
            while (v0_21 < v6_1) {
                String v7 = v3_7[v0_21];
                if (v7.lastModified() < v4_4) {
                    v7.delete();
                }
                v0_21++;
            }
        }
        if (!this.n.e.getBoolean("create_debuglog", 0)) {
        } else {
            android.content.SharedPreferences v0_26 = new java.util.logging.FileHandler(new StringBuilder().append(android.os.Environment.getExternalStorageDirectory()).append("/TS3/logs/").append(new java.text.SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(new java.util.Date())).append(".log").toString());
            v0_26.setFormatter(new java.util.logging.SimpleFormatter());
            v2_12.addHandler(v0_26);
        }
    }

    public boolean onCreateOptionsMenu(android.view.Menu p3)
    {
        p3.clear();
        this.O.a(p3, this.n);
        return 1;
    }

    protected void onDestroy()
    {
        super.onDestroy();
        if (this.n.k != null) {
            this.n.stopService(this.n.k);
        }
        com.teamspeak.ts3client.jni.Ts3Jni.c();
        return;
    }

    public boolean onKeyDown(int p8, android.view.KeyEvent p9)
    {
        boolean v0 = 1;
        if (!com.teamspeak.ts3client.data.v.a().a(p9)) {
            switch (p8) {
                case 4:
                    if (this.n.a == null) {
                        v0 = super.onKeyDown(p8, p9);
                    } else {
                        if ((this.n.c != this.n.a.k) || (!this.n.a.l.g)) {
                            if ((this.n.c != this.n.a.k) || (this.n.a.l.g)) {
                                if (!(this.n.c instanceof com.teamspeak.ts3client.bookmark.a)) {
                                    if (!(this.n.c instanceof com.teamspeak.ts3client.h)) {
                                        if (!(this.n.c instanceof com.teamspeak.ts3client.c)) {
                                            if (this.n.c != this.o) {
                                                if (!(this.n.c instanceof com.teamspeak.ts3client.f.as)) {
                                                    if (!(this.n.c instanceof com.teamspeak.ts3client.c.b)) {
                                                        if (!(this.n.c instanceof com.teamspeak.ts3client.e.b)) {
                                                            if (!(this.n.c instanceof com.teamspeak.ts3client.d.d.i)) {
                                                                if (!(this.n.c instanceof com.teamspeak.ts3client.chat.c)) {
                                                                    if (!(this.n.c instanceof com.teamspeak.ts3client.chat.j)) {
                                                                        if ((this.n.c instanceof com.teamspeak.ts3client.b.b)) {
                                                                            v0 = super.onKeyDown(p8, p9);
                                                                        }
                                                                    } else {
                                                                        v0 = super.onKeyDown(p8, p9);
                                                                    }
                                                                } else {
                                                                    v0 = super.onKeyDown(p8, p9);
                                                                }
                                                            } else {
                                                                v0 = super.onKeyDown(p8, p9);
                                                            }
                                                        } else {
                                                            v0 = super.onKeyDown(p8, p9);
                                                        }
                                                    } else {
                                                        v0 = super.onKeyDown(p8, p9);
                                                    }
                                                } else {
                                                    v0 = super.onKeyDown(p8, p9);
                                                }
                                            } else {
                                                v0 = super.onKeyDown(p8, p9);
                                            }
                                        } else {
                                            v0 = super.onKeyDown(p8, p9);
                                        }
                                    } else {
                                        v0 = super.onKeyDown(p8, p9);
                                    }
                                } else {
                                    v0 = super.onKeyDown(p8, p9);
                                }
                            } else {
                                v0 = super.onKeyDown(p8, p9);
                            }
                        } else {
                            com.teamspeak.ts3client.bookmark.e v2_23 = this.n.a.k;
                            android.app.AlertDialog v3_5 = new android.app.AlertDialog$Builder(v2_23.i()).create();
                            com.teamspeak.ts3client.data.d.q.a(v3_5);
                            v3_5.setTitle(com.teamspeak.ts3client.data.e.a.a("disconnect.info"));
                            v3_5.setMessage(com.teamspeak.ts3client.data.e.a.a("disconnect.text"));
                            v3_5.setButton(-1, com.teamspeak.ts3client.data.e.a.a("disconnect.button"), new com.teamspeak.ts3client.az(v2_23));
                            v3_5.setButton(-2, com.teamspeak.ts3client.data.e.a.a("button.cancel"), new com.teamspeak.ts3client.ba(v2_23, v3_5));
                            v3_5.setCancelable(0);
                            v3_5.show();
                        }
                    }
                    break;
                case 24:
                    if (this.n.a == null) {
                        v0 = 0;
                    } else {
                        this.n.a.b.a(1);
                    }
                    break;
                case 25:
                    if (this.n.a == null) {
                        v0 = 0;
                    } else {
                        this.n.a.b.a(0);
                    }
                    break;
                default:
                    v0 = super.onKeyDown(p8, p9);
            }
        }
        return v0;
    }

    public boolean onKeyUp(int p2, android.view.KeyEvent p3)
    {
        boolean v0_2;
        if (!com.teamspeak.ts3client.data.v.a().a(p3)) {
            v0_2 = super.onKeyUp(p2, p3);
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    protected void onNewIntent(android.content.Intent p3)
    {
        super.onNewIntent(p3);
        if (p3.hasExtra("bookmark")) {
            this.b(((android.net.Uri) p3.getExtras().get("uri")));
        }
        return;
    }

    public boolean onOptionsItemSelected(android.view.MenuItem p3)
    {
        return this.O.a(p3, this.n);
    }

    protected void onPause()
    {
        super.onPause();
        this.n.g = 0;
        try {
            if (com.teamspeak.ts3client.ConnectionBackground.a() != null) {
                new android.os.Handler().postDelayed(new com.teamspeak.ts3client.ca(this), 500);
            }
        } catch (com.teamspeak.ts3client.ConnectionBackground v0) {
        }
        if (this.n.a != null) {
            this.n.a.k.A();
            this.n.a.l.c();
        }
        this.C = 0;
        return;
    }

    public void onRequestPermissionsResult(int p3, String[] p4, int[] p5)
    {
        switch (p3) {
            case 1:
                if ((p5.length <= 0) || (p5[0] != 0)) {
                    this.I();
                } else {
                    this.w();
                    this.L();
                }
            case 2:
                if ((p5.length <= 0) || (p5[0] != 0)) {
                    this.I();
                } else {
                    this.M();
                }
            case 3:
                if ((p5.length > 0) && (p5[0] == 0)) {
                } else {
                    this.I();
                }
                break;
        }
        return;
    }

    protected void onRestoreInstanceState(android.os.Bundle p1)
    {
        super.onRestoreInstanceState(p1);
        return;
    }

    protected void onResume()
    {
        super.onResume();
        net.hockeyapp.android.b.a(this, this.N);
        this.n.g = 1;
        new android.content.IntentFilter();
        this.n.e.getBoolean("use_proximity", 1);
        if (this.r) {
            if (this.n.g) {
                if (this.c().f() > 0) {
                    this.c().e();
                }
                com.teamspeak.ts3client.data.z v0_12 = this.c().a();
                v0_12.b(this.n.c);
                v0_12.f(this.c().a("Bookmark"));
                v0_12.a(8194);
                v0_12.i();
            }
            this.r = 0;
        }
        this.C = 1;
        if (this.p) {
            if (this.c().f() > 0) {
                this.c().e();
            }
            com.teamspeak.ts3client.data.z v0_18 = this.c().a();
            v0_18.b(this.n.c);
            v0_18.f(this.c().a("Bookmark"));
            v0_18.a(8194);
            v0_18.i();
            this.l();
            this.n.a.k.I();
        }
        if (this.s) {
            if (this.c().f() > 0) {
                this.c().e();
            }
            com.teamspeak.ts3client.data.z v0_27 = this.c().a();
            if (!(this.n.c instanceof com.teamspeak.ts3client.bookmark.e)) {
                v0_27.b(this.n.c);
            }
            v0_27.f(this.c().a("Bookmark"));
            v0_27.a(8194);
            v0_27.i();
            this.s = 0;
        }
        if (this.n.a != null) {
            this.n.a.l.d();
        }
        com.teamspeak.ts3client.data.z v0_33 = com.teamspeak.ts3client.data.af.a();
        if (!v0_33.e) {
            String v1_17 = java.util.Calendar.getInstance();
            if ((com.teamspeak.ts3client.Ts3Application.a().e.getInt("LicenseAgreement", 0) != 0) && (com.teamspeak.ts3client.Ts3Application.a().e.getLong("la_lastcheck", 0) >= (v1_17.getTimeInMillis() - 86400000))) {
                android.util.Log.i("UpdateServerData", "skipped downloading new update info");
                v0_33.d.o();
            } else {
                v0_33.b = com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_android_getUpdaterURL();
                v0_33.e = 1;
                String v1_23 = new com.teamspeak.ts3client.data.ah(v0_33, 0);
                String v2_10 = new String[1];
                v2_10[0] = v0_33.b;
                v1_23.execute(v2_10);
            }
        }
        return;
    }

    protected void onSaveInstanceState(android.os.Bundle p1)
    {
        return;
    }

    public void onSensorChanged(android.hardware.SensorEvent p7)
    {
        if (((Math.atan2(Math.sqrt(((double) ((p7.values[0] * p7.values[0]) + (p7.values[1] * p7.values[1])))), ((double) p7.values[2])) * 180.0) / 3.141592653589793) <= 65.0) {
            if (this.I != 0) {
                this.I = 0;
                this.n.m.release();
            }
        } else {
            if (this.I != 1) {
                this.I = 1;
                this.n.m.acquire();
            }
        }
        return;
    }

    public void sendNewBookmark(android.view.View p4)
    {
        this.n.d.log(java.util.logging.Level.INFO, "NewBookmark");
        new com.teamspeak.ts3client.bookmark.a(com.teamspeak.ts3client.data.b.a.a).a(this.c(), "AddBookmark");
        return;
    }
}
