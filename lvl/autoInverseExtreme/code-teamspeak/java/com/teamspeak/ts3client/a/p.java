package com.teamspeak.ts3client.a;
public final class p implements android.content.SharedPreferences$OnSharedPreferenceChangeListener {
    public boolean a;
    private com.teamspeak.ts3client.Ts3Application b;
    private com.teamspeak.ts3client.a.n c;
    private int d;

    public p(android.content.Context p4)
    {
        this.a = 0;
        this.b = ((com.teamspeak.ts3client.Ts3Application) p4.getApplicationContext());
        this.d = this.b.e.getInt("sound_pack", 0);
        this.a(0);
        this.b.e.registerOnSharedPreferenceChangeListener(this);
        return;
    }

    private void a()
    {
        this.c = new com.teamspeak.ts3client.a.r();
        this.c.a(this.b.getApplicationContext());
        return;
    }

    private void a(String p2)
    {
        if (!this.a) {
            switch (this.d) {
                case 1:
                    this.c.a(p2);
                    break;
                default:
            }
        }
        return;
    }

    private void a(boolean p3)
    {
        if ((p3) && (this.c != null)) {
            this.c.b();
        }
        switch (this.d) {
            case 0:
                this.c = new com.teamspeak.ts3client.a.t();
                this.c.a(this.b.getApplicationContext());
                break;
            case 1:
                this.c = new com.teamspeak.ts3client.a.r();
                this.c.a(this.b.getApplicationContext());
                break;
        }
        return;
    }

    private void b()
    {
        this.c = new com.teamspeak.ts3client.a.t();
        this.c.a(this.b.getApplicationContext());
        return;
    }

    private void b(boolean p1)
    {
        this.a = p1;
        return;
    }

    private void c()
    {
        this.c.a();
        return;
    }

    private void d()
    {
        if (this.c != null) {
            this.c.b();
        }
        return;
    }

    private void e()
    {
        this.a = 0;
        return;
    }

    public final void a(com.teamspeak.ts3client.jni.h p2, com.teamspeak.ts3client.a.o p3)
    {
        if (!this.a) {
            switch (this.d) {
                case 0:
                    this.c.a(p2, p3);
                    break;
                case 1:
                    this.c.a(p2, p3);
                    break;
                default:
            }
        }
        return;
    }

    public final void onSharedPreferenceChanged(android.content.SharedPreferences p6, String p7)
    {
        if (p7.equals("sound_pack")) {
            this.d = this.b.e.getInt("sound_pack", 0);
            try {
                this.a(1);
            } catch (android.app.AlertDialog v0) {
                if (this.b.e.getInt("sound_pack", 0) == 1) {
                    this.b.e.edit().putInt("sound_pack", 0).commit();
                    this.b.g();
                    android.app.AlertDialog v0_16 = new android.app.AlertDialog$Builder(this.b.f).create();
                    v0_16.setTitle(com.teamspeak.ts3client.data.e.a.a("critical.tts"));
                    v0_16.setMessage(com.teamspeak.ts3client.data.e.a.a("critical.tts.text"));
                    v0_16.setButton(-3, com.teamspeak.ts3client.data.e.a.a("button.ok"), new com.teamspeak.ts3client.a.q(this, v0_16));
                    v0_16.setCancelable(0);
                    v0_16.show();
                }
            }
        }
        return;
    }
}
