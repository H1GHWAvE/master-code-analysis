package com.teamspeak.ts3client.data;
final class o implements android.view.View$OnTouchListener {
    final android.view.GestureDetector a;
    final synthetic com.teamspeak.ts3client.data.g b;

    o(com.teamspeak.ts3client.data.g p4)
    {
        this.b = p4;
        this.a = new android.view.GestureDetector(com.teamspeak.ts3client.data.g.c(this.b), new com.teamspeak.ts3client.data.p(this));
        return;
    }

    public final boolean onTouch(android.view.View p4, android.view.MotionEvent p5)
    {
        boolean v0_1 = this.a.onTouchEvent(p5);
        if (android.os.Build$VERSION.SDK_INT >= 11) {
            if (p5.getAction() == 0) {
                p4.setBackgroundColor(858678526);
            }
            if ((p5.getAction() == 1) || (p5.getAction() == 3)) {
                p4.setBackgroundColor(0);
            }
        }
        return v0_1;
    }
}
