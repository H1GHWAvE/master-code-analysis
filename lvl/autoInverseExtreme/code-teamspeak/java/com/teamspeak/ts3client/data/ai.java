package com.teamspeak.ts3client.data;
public final class ai {
    private static com.teamspeak.ts3client.data.ai a;
    private static android.os.Vibrator b;

    public ai()
    {
        return;
    }

    public static declared_synchronized com.teamspeak.ts3client.data.ai a()
    {
        try {
            if (com.teamspeak.ts3client.data.ai.a == null) {
                com.teamspeak.ts3client.data.ai.a = new com.teamspeak.ts3client.data.ai();
                com.teamspeak.ts3client.data.ai.b = ((android.os.Vibrator) com.teamspeak.ts3client.Ts3Application.a().getSystemService("vibrator"));
            }
        } catch (com.teamspeak.ts3client.data.ai v0_6) {
            throw v0_6;
        }
        return com.teamspeak.ts3client.data.ai.a;
    }

    private static void a(int p4)
    {
        if (com.teamspeak.ts3client.data.ai.b != null) {
            com.teamspeak.ts3client.data.ai.b.vibrate(((long) p4));
        }
        return;
    }

    public static void a(long[] p2)
    {
        if (com.teamspeak.ts3client.data.ai.b != null) {
            com.teamspeak.ts3client.data.ai.b.vibrate(p2, -1);
        }
        return;
    }
}
