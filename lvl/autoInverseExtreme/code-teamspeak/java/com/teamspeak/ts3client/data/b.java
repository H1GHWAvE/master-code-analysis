package com.teamspeak.ts3client.data;
public final class b {
    public java.util.concurrent.ConcurrentHashMap a;
    public com.teamspeak.ts3client.data.a b;

    public b()
    {
        this.b = 0;
        this.a = new java.util.concurrent.ConcurrentHashMap();
        return;
    }

    public final com.teamspeak.ts3client.data.a a(Long p2)
    {
        return ((com.teamspeak.ts3client.data.a) this.a.get(p2));
    }

    public final Long a(Long p7, int p8)
    {
        java.util.Iterator v1 = this.a.values().iterator();
        while (v1.hasNext()) {
            Long v0_5 = ((com.teamspeak.ts3client.data.a) v1.next());
            if ((v0_5.g == p7.longValue()) && (v0_5.f == p8)) {
                Long v0_3 = Long.valueOf(v0_5.b);
            }
            return v0_3;
        }
        v0_3 = 0;
        return v0_3;
    }

    public final java.util.concurrent.ConcurrentHashMap a()
    {
        return this.a;
    }

    public final void a(com.teamspeak.ts3client.data.a p5)
    {
        this.a.put(Long.valueOf(p5.b), p5);
        return;
    }

    public final com.teamspeak.ts3client.data.a b()
    {
        return this.b;
    }

    public final Long b(Long p7)
    {
        java.util.Iterator v1 = this.a.values().iterator();
        while (v1.hasNext()) {
            Long v0_5 = ((com.teamspeak.ts3client.data.a) v1.next());
            if (v0_5.g == p7.longValue()) {
                Long v0_3 = Long.valueOf(v0_5.b);
            }
            return v0_3;
        }
        v0_3 = 0;
        return v0_3;
    }

    public final void b(com.teamspeak.ts3client.data.a p1)
    {
        this.b = p1;
        return;
    }

    public final void c(Long p2)
    {
        this.a.remove(p2);
        return;
    }

    public final boolean d(Long p2)
    {
        return this.a.containsKey(p2);
    }
}
