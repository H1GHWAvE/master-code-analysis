package com.teamspeak.ts3client.f;
final class s implements android.view.View$OnClickListener {
    final synthetic com.teamspeak.ts3client.Ts3Application a;
    final synthetic android.widget.RelativeLayout b;
    final synthetic com.teamspeak.ts3client.f.r c;

    s(com.teamspeak.ts3client.f.r p1, com.teamspeak.ts3client.Ts3Application p2, android.widget.RelativeLayout p3)
    {
        this.c = p1;
        this.a = p2;
        this.b = p3;
        return;
    }

    public final void onClick(android.view.View p5)
    {
        String v0_3 = "";
        int v1_1 = this.c.at.nextSetBit(0);
        while (v1_1 >= 0) {
            if (!v0_3.isEmpty()) {
                v0_3 = new StringBuilder().append(v0_3).append(",").append(v1_1).toString();
            } else {
                v0_3 = new StringBuilder().append(v0_3).append(v1_1).toString();
            }
            v1_1 = this.c.at.nextSetBit((v1_1 + 1));
        }
        this.a.e.edit().putString(com.teamspeak.ts3client.f.p.a(this.c.aw), v0_3.toString()).commit();
        this.a.e.edit().putBoolean(new StringBuilder().append(com.teamspeak.ts3client.f.p.a(this.c.aw)).append("_intercept").toString(), com.teamspeak.ts3client.f.r.a(this.c).isChecked()).commit();
        com.teamspeak.ts3client.data.v.a().a(this.c.at, com.teamspeak.ts3client.f.r.a(this.c).isChecked());
        p5.performHapticFeedback(1);
        com.teamspeak.ts3client.data.v.a().c = 0;
        com.teamspeak.ts3client.f.p.d(this.c.aw).b();
        this.b.removeAllViews();
        return;
    }
}
