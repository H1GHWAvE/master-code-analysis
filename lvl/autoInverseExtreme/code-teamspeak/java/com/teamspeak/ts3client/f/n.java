package com.teamspeak.ts3client.f;
final class n implements android.widget.SeekBar$OnSeekBarChangeListener {
    final synthetic com.teamspeak.ts3client.Ts3Application a;
    final synthetic com.teamspeak.ts3client.f.m b;

    n(com.teamspeak.ts3client.f.m p1, com.teamspeak.ts3client.Ts3Application p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final void onProgressChanged(android.widget.SeekBar p8, int p9, boolean p10)
    {
        com.teamspeak.ts3client.f.k.c(this.b.at).setText(new StringBuilder().append((((float) (p9 - 60)) * 1056964608)).append(" / 30").toString());
        if (this.a.a != null) {
            com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_setPlaybackConfigValue(this.a.a.e, "volume_modifier", new StringBuilder().append((((float) (p9 - 60)) * 1056964608)).toString());
        }
        return;
    }

    public final void onStartTrackingTouch(android.widget.SeekBar p1)
    {
        return;
    }

    public final void onStopTrackingTouch(android.widget.SeekBar p1)
    {
        return;
    }
}
