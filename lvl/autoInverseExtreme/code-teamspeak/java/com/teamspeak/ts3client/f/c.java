package com.teamspeak.ts3client.f;
public final class c extends android.widget.RelativeLayout implements android.view.View$OnClickListener {
    private android.widget.CheckBox a;
    private String b;
    private String c;
    private boolean d;
    private android.view.View e;

    public c(android.content.Context p10, String p11, Boolean p12, String p13, String p14)
    {
        this(p10);
        this.c = "";
        this.d = 0;
        this.setLayoutParams(new android.widget.RelativeLayout$LayoutParams(-1, -2));
        this.setPadding(0, 18, 0, 18);
        this.setBackgroundResource(2130837686);
        android.view.View v1_3 = new android.widget.TextView(p10);
        android.widget.TextView v2_2 = new android.widget.TextView(p10);
        this.e = ((android.view.LayoutInflater) p10.getSystemService("layout_inflater")).inflate(2130903109, 0);
        this.a = ((android.widget.CheckBox) this.e.findViewById(2131493263));
        this.b = p11;
        android.widget.RelativeLayout$LayoutParams v0_13 = ((com.teamspeak.ts3client.Ts3Application) p10.getApplicationContext());
        int v3_4 = Boolean.valueOf(v0_13.e.getBoolean(p11, p12.booleanValue()));
        v1_3.setText(p13);
        v1_3.setTextSize(1101004800);
        v1_3.setTypeface(0, 1);
        v1_3.setId(1);
        v2_2.setText(p14);
        v2_2.setTextSize(1092616192);
        v2_2.setTypeface(0, 2);
        v2_2.setId(2);
        this.a.setChecked(v3_4.booleanValue());
        this.a.setContentDescription(new StringBuilder().append(p13).append(" Checkbox").toString());
        this.a.setOnCheckedChangeListener(new com.teamspeak.ts3client.f.d(this, v0_13));
        android.widget.RelativeLayout$LayoutParams v0_15 = new android.widget.RelativeLayout$LayoutParams(-2, -2);
        v0_15.addRule(10);
        this.addView(v1_3, v0_15);
        android.widget.RelativeLayout$LayoutParams v0_17 = new android.widget.RelativeLayout$LayoutParams(-2, -2);
        v0_17.addRule(3, v1_3.getId());
        this.addView(v2_2, v0_17);
        android.widget.RelativeLayout$LayoutParams v0_19 = new android.widget.RelativeLayout$LayoutParams(-2, -2);
        v0_19.addRule(11);
        this.addView(this.e, v0_19);
        this.setClickable(1);
        this.setOnClickListener(this);
        return;
    }

    public c(android.content.Context p2, String p3, Boolean p4, String p5, String p6, String p7)
    {
        com.teamspeak.ts3client.f.c v1_1 = this(p2, p3, p4, p5, p6);
        v1_1.c = p7;
        v1_1.d = 1;
        return;
    }

    public c(android.content.Context p1, String p2, Boolean p3, String p4, String p5, String p6, byte p7)
    {
        this(p1, p2, p3, p4, p5).c = p6;
        return;
    }

    static synthetic String a(com.teamspeak.ts3client.f.c p1)
    {
        return p1.c;
    }

    static synthetic String b(com.teamspeak.ts3client.f.c p1)
    {
        return p1.b;
    }

    static synthetic boolean c(com.teamspeak.ts3client.f.c p1)
    {
        return p1.d;
    }

    public final void onClick(android.view.View p3)
    {
        int v0_2;
        if (this.a.isChecked()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        this.a.setChecked(v0_2);
        return;
    }
}
