package com.teamspeak.ts3client.f;
public final class f extends android.widget.RelativeLayout implements android.view.View$OnClickListener {
    private android.widget.EditText a;
    private String b;
    private com.teamspeak.ts3client.f.h c;
    private android.support.v4.app.bi d;
    private int e;
    private String f;
    private String g;
    private String h;
    private int i;

    public f(android.content.Context p11, String p12, String p13, String p14, String p15, android.support.v4.app.bi p16, int p17)
    {
        this(p11, p12, p13, p14, p15, p16, 2, 0);
        this.i = p17;
        return;
    }

    public f(android.content.Context p8, String p9, String p10, String p11, String p12, android.support.v4.app.bi p13, int p14, byte p15)
    {
        this(p8);
        this.i = 0;
        this.setLayoutParams(new android.widget.RelativeLayout$LayoutParams(-1, -2));
        this.setPadding(0, 18, 0, 18);
        this.setBackgroundResource(2130837686);
        this.d = p13;
        this.b = p11;
        this.h = p12;
        this.e = p14;
        this.f = p9;
        this.g = p10;
        int v0_4 = new android.widget.TextView(p8);
        android.widget.TextView v1_2 = new android.widget.TextView(p8);
        v0_4.setText(this.b);
        v0_4.setTextSize(1101004800);
        v0_4.setTypeface(0, 1);
        v0_4.setId(1);
        v1_2.setText(this.h);
        v1_2.setTextSize(1092616192);
        v1_2.setTypeface(0, 2);
        v1_2.setId(2);
        android.widget.RelativeLayout$LayoutParams v2_8 = new android.widget.RelativeLayout$LayoutParams(-2, -2);
        v2_8.addRule(10);
        this.addView(v0_4, v2_8);
        android.widget.RelativeLayout$LayoutParams v2_10 = new android.widget.RelativeLayout$LayoutParams(-2, -2);
        v2_10.addRule(3, v0_4.getId());
        this.addView(v1_2, v2_10);
        this.setClickable(1);
        this.setOnClickListener(this);
        return;
    }

    static synthetic android.widget.EditText a(com.teamspeak.ts3client.f.f p0, android.widget.EditText p1)
    {
        p0.a = p1;
        return p1;
    }

    static synthetic String a(com.teamspeak.ts3client.f.f p1)
    {
        return p1.f;
    }

    static synthetic String b(com.teamspeak.ts3client.f.f p1)
    {
        return p1.g;
    }

    static synthetic android.widget.EditText c(com.teamspeak.ts3client.f.f p1)
    {
        return p1.a;
    }

    static synthetic int d(com.teamspeak.ts3client.f.f p1)
    {
        return p1.e;
    }

    static synthetic int e(com.teamspeak.ts3client.f.f p1)
    {
        return p1.i;
    }

    static synthetic com.teamspeak.ts3client.f.h f(com.teamspeak.ts3client.f.f p1)
    {
        return p1.c;
    }

    static synthetic String g(com.teamspeak.ts3client.f.f p1)
    {
        return p1.b;
    }

    public final void onClick(android.view.View p4)
    {
        this.c = new com.teamspeak.ts3client.f.h(this, 0);
        this.c.a(this.d, this.b);
        p4.performHapticFeedback(1);
        return;
    }
}
