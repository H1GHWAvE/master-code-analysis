package com.teamspeak.ts3client.d.b;
public final class am implements android.view.View$OnClickListener {
    java.util.ArrayList a;
    android.widget.CheckBox b;
    boolean c;
    final synthetic com.teamspeak.ts3client.d.b.aj d;

    public am(com.teamspeak.ts3client.d.b.aj p2, boolean p3)
    {
        this.d = p2;
        this.a = new java.util.ArrayList();
        this.c = 0;
        this.c = p3;
        return;
    }

    private void a(android.widget.CheckBox p2)
    {
        this.a.add(p2);
        p2.setOnClickListener(this);
        return;
    }

    private void b(android.widget.CheckBox p1)
    {
        this.b = p1;
        return;
    }

    public final void onClick(android.view.View p13)
    {
        long v2_0 = ((android.widget.CompoundButton) p13).isChecked();
        if (!this.c) {
            if (!p13.getTag().equals(this.b.getTag())) {
                this.b.setChecked(0);
                java.util.Iterator v3_0 = this.a.iterator();
                int v1_2 = 0;
                while (v3_0.hasNext()) {
                    int v0_39;
                    int v0_38 = ((android.widget.CheckBox) v3_0.next());
                    if (!v0_38.isChecked()) {
                        v0_39 = v1_2;
                    } else {
                        if (com.teamspeak.ts3client.d.b.aj.d(this.d) >= com.teamspeak.ts3client.d.b.aj.b(this.d).a.o.b(Long.parseLong(v0_38.getTag().toString())).f) {
                            v0_38.setEnabled(1);
                            v0_39 = 1;
                        } else {
                            v0_38.setEnabled(0);
                            v1_2 = 1;
                        }
                    }
                    v1_2 = v0_39;
                }
                if (v1_2 == 0) {
                    this.b.setChecked(1);
                }
            }
            if (v2_0 == 0) {
                com.teamspeak.ts3client.d.b.aj.b(this.d).a.a.ts3client_requestServerGroupDelClient(com.teamspeak.ts3client.d.b.aj.b(this.d).a.e, Long.parseLong(p13.getTag().toString()), com.teamspeak.ts3client.d.b.aj.c(this.d), new StringBuilder().append(com.teamspeak.ts3client.d.b.aj.c(this.d)).append(" added in sgroup ").append(p13.getTag().toString()).toString());
            } else {
                com.teamspeak.ts3client.d.b.aj.b(this.d).a.a.ts3client_requestServerGroupAddClient(com.teamspeak.ts3client.d.b.aj.b(this.d).a.e, Long.parseLong(p13.getTag().toString()), com.teamspeak.ts3client.d.b.aj.c(this.d), new StringBuilder().append(com.teamspeak.ts3client.d.b.aj.c(this.d)).append(" added in sgroup ").append(p13.getTag().toString()).toString());
            }
        } else {
            long[] v6_2 = com.teamspeak.ts3client.d.b.aj.b(this.d).a.c().a(((Long) com.teamspeak.ts3client.d.b.aj.b(this.d).a.i.get(Integer.valueOf(com.teamspeak.ts3client.d.b.aj.a(this.d).c))));
            int v1_8 = this.a.iterator();
            while (v1_8.hasNext()) {
                ((android.widget.CheckBox) v1_8.next()).setChecked(0);
            }
            if (v2_0 == 0) {
                int v1_9 = com.teamspeak.ts3client.d.b.aj.b(this.d).a.a;
                long v2_3 = com.teamspeak.ts3client.d.b.aj.b(this.d).a.e;
                int v4_9 = new long[1];
                v4_9[0] = Long.parseLong(this.b.getTag().toString());
                String v5_2 = new long[1];
                v5_2[0] = v6_2.b;
                long[] v6_3 = new long[1];
                v6_3[0] = com.teamspeak.ts3client.d.b.aj.c(this.d);
                v1_9.ts3client_requestSetClientChannelGroup(v2_3, v4_9, v5_2, v6_3, 1, new StringBuilder().append(com.teamspeak.ts3client.d.b.aj.c(this.d)).append(" set to channelgroup ").append(this.b.getTag().toString()).toString());
                this.b.setChecked(1);
            } else {
                ((android.widget.CompoundButton) p13).setChecked(1);
                int v1_10 = com.teamspeak.ts3client.d.b.aj.b(this.d).a.a;
                long v2_4 = com.teamspeak.ts3client.d.b.aj.b(this.d).a.e;
                int v4_10 = new long[1];
                v4_10[0] = Long.parseLong(p13.getTag().toString());
                String v5_3 = new long[1];
                v5_3[0] = v6_2.b;
                long[] v6_4 = new long[1];
                v6_4[0] = com.teamspeak.ts3client.d.b.aj.c(this.d);
                v1_10.ts3client_requestSetClientChannelGroup(v2_4, v4_10, v5_3, v6_4, 1, new StringBuilder().append(com.teamspeak.ts3client.d.b.aj.c(this.d)).append(" set to channelgroup ").append(p13.getTag().toString()).toString());
            }
        }
        return;
    }
}
