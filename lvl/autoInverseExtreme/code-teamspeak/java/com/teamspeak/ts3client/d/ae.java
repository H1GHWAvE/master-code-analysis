package com.teamspeak.ts3client.d;
public final class ae extends android.support.v4.app.ax implements com.teamspeak.ts3client.data.w {
    private String aA;
    private String aB;
    private int aC;
    private int aD;
    private long aE;
    private android.widget.Spinner aF;
    private boolean aG;
    private com.teamspeak.ts3client.Ts3Application at;
    private android.widget.EditText au;
    private android.widget.EditText av;
    private android.widget.EditText aw;
    private android.widget.EditText ax;
    private android.widget.Button ay;
    private String az;

    public ae()
    {
        this.aG = 0;
        return;
    }

    static synthetic int a(com.teamspeak.ts3client.d.ae p0, int p1)
    {
        p0.aC = p1;
        return p1;
    }

    static synthetic long a(com.teamspeak.ts3client.d.ae p1, long p2)
    {
        p1.aE = p2;
        return p2;
    }

    static synthetic android.widget.EditText a(com.teamspeak.ts3client.d.ae p1)
    {
        return p1.ax;
    }

    static synthetic String a(com.teamspeak.ts3client.d.ae p0, String p1)
    {
        p0.az = p1;
        return p1;
    }

    static synthetic int b(com.teamspeak.ts3client.d.ae p0, int p1)
    {
        p0.aD = p1;
        return p1;
    }

    static synthetic String b(com.teamspeak.ts3client.d.ae p1)
    {
        return p1.aB;
    }

    static synthetic String b(com.teamspeak.ts3client.d.ae p0, String p1)
    {
        p0.aA = p1;
        return p1;
    }

    static synthetic android.widget.EditText c(com.teamspeak.ts3client.d.ae p1)
    {
        return p1.au;
    }

    static synthetic String c(com.teamspeak.ts3client.d.ae p0, String p1)
    {
        p0.aB = p1;
        return p1;
    }

    static synthetic com.teamspeak.ts3client.Ts3Application d(com.teamspeak.ts3client.d.ae p1)
    {
        return p1.at;
    }

    static synthetic long e(com.teamspeak.ts3client.d.ae p2)
    {
        return p2.aE;
    }

    static synthetic String f(com.teamspeak.ts3client.d.ae p1)
    {
        return p1.aA;
    }

    static synthetic android.widget.EditText g(com.teamspeak.ts3client.d.ae p1)
    {
        return p1.av;
    }

    static synthetic String h(com.teamspeak.ts3client.d.ae p1)
    {
        return p1.az;
    }

    static synthetic android.widget.EditText i(com.teamspeak.ts3client.d.ae p1)
    {
        return p1.aw;
    }

    static synthetic android.widget.Spinner j(com.teamspeak.ts3client.d.ae p1)
    {
        return p1.aF;
    }

    static synthetic int k(com.teamspeak.ts3client.d.ae p1)
    {
        return p1.aD;
    }

    static synthetic boolean l(com.teamspeak.ts3client.d.ae p1)
    {
        return p1.aG;
    }

    static synthetic boolean m(com.teamspeak.ts3client.d.ae p1)
    {
        p1.aG = 1;
        return 1;
    }

    static synthetic int n(com.teamspeak.ts3client.d.ae p1)
    {
        return p1.aC;
    }

    private void y()
    {
        this.i().runOnUiThread(new com.teamspeak.ts3client.d.aj(this));
        return;
    }

    public final android.view.View a(android.view.LayoutInflater p6, android.view.ViewGroup p7)
    {
        this.at = ((com.teamspeak.ts3client.Ts3Application) p6.getContext().getApplicationContext());
        android.widget.ScrollView v0_5 = ((android.widget.ScrollView) p6.inflate(2130903130, p7, 0));
        this.j.setTitle(this.at.a.m());
        this.au = ((android.widget.EditText) v0_5.findViewById(2131493354));
        this.av = ((android.widget.EditText) v0_5.findViewById(2131493358));
        this.aw = ((android.widget.EditText) v0_5.findViewById(2131493361));
        this.ax = ((android.widget.EditText) v0_5.findViewById(2131493356));
        this.ay = ((android.widget.Button) v0_5.findViewById(2131493362));
        this.ay.setText(com.teamspeak.ts3client.data.e.a.a("button.save"));
        com.teamspeak.ts3client.data.e.a.a("virtualservereditdialog.name", v0_5, 2131493353);
        com.teamspeak.ts3client.data.e.a.a("virtualservereditdialog.password", v0_5, 2131493355);
        com.teamspeak.ts3client.data.e.a.a("virtualservereditdialog.wmessage", v0_5, 2131493360);
        com.teamspeak.ts3client.data.e.a.a("virtualservereditdialog.hmessage", v0_5, 2131493357);
        this.aF = ((android.widget.Spinner) v0_5.findViewById(2131493359));
        this.aF.setAdapter(com.teamspeak.ts3client.data.e.a.a("virtualservereditdialog.hmessage.array", v0_5.getContext(), 4));
        this.ax.setOnFocusChangeListener(new com.teamspeak.ts3client.d.af(this));
        this.ay.setOnClickListener(new com.teamspeak.ts3client.d.ag(this));
        this.at.a.c.a(this);
        this.at.a.a.ts3client_requestServerVariables(this.at.a.e);
        return v0_5;
    }

    public final void a(com.teamspeak.ts3client.jni.k p3)
    {
        if ((p3 instanceof com.teamspeak.ts3client.jni.events.ServerUpdated)) {
            this.at.a.c.b(this);
            this.i().runOnUiThread(new com.teamspeak.ts3client.d.aj(this));
        }
        return;
    }

    public final void e()
    {
        super.e();
        com.teamspeak.ts3client.data.d.q.a(this);
        this.aF.setOnItemSelectedListener(new com.teamspeak.ts3client.d.ah(this));
        return;
    }

    public final void f()
    {
        super.f();
        com.teamspeak.ts3client.data.d.q.b(this);
        return;
    }
}
