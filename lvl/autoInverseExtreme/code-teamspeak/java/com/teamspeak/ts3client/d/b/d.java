package com.teamspeak.ts3client.d.b;
final class d implements android.view.View$OnClickListener {
    final synthetic com.teamspeak.ts3client.Ts3Application a;
    final synthetic com.teamspeak.ts3client.d.b.a b;

    d(com.teamspeak.ts3client.d.b.a p1, com.teamspeak.ts3client.Ts3Application p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final void onClick(android.view.View p15)
    {
        long v6 = this.a.a.a.c(this.a.a.e, com.teamspeak.ts3client.d.b.a.a(this.b).c, com.teamspeak.ts3client.jni.d.G);
        if (this.a.a.a.a(this.a.a.e, com.teamspeak.ts3client.d.b.a.a(this.b).c, com.teamspeak.ts3client.jni.d.Z) != 0) {
            if (this.a.a.a.a(this.a.a.e, com.teamspeak.ts3client.d.b.a.a(this.b).c, com.teamspeak.ts3client.jni.d.Z) == 1) {
                com.teamspeak.ts3client.jni.Ts3Jni v1_16 = this.a.a.a;
                long v2_3 = this.a.a.e;
                long v4_7 = ((Long) this.a.a.i.get(Integer.valueOf(com.teamspeak.ts3client.d.b.a.a(this.b).c))).longValue();
                long[] v8_0 = new long[1];
                v8_0[0] = ((long) this.a.a.u.a(com.teamspeak.ts3client.jni.g.cu));
                v1_16.ts3client_requestChannelClientDelPerm(v2_3, v4_7, v6, v8_0, 1, new StringBuilder("Remove PRIORITY_SPEAKER for:").append(v6).append(" in ").append(((Long) this.a.a.i.get(Integer.valueOf(com.teamspeak.ts3client.d.b.a.a(this.b).c))).longValue()).toString());
            }
        } else {
            com.teamspeak.ts3client.jni.Ts3Jni v1_17 = this.a.a.a;
            long v2_4 = this.a.a.e;
            long v4_12 = ((Long) this.a.a.i.get(Integer.valueOf(com.teamspeak.ts3client.d.b.a.a(this.b).c))).longValue();
            long[] v8_1 = new long[1];
            v8_1[0] = ((long) this.a.a.u.a(com.teamspeak.ts3client.jni.g.cu));
            int v9_9 = new int[1];
            v9_9[0] = 1;
            v1_17.ts3client_requestChannelClientAddPerm(v2_4, v4_12, v6, v8_1, v9_9, 1, new StringBuilder("PRIORITY_SPEAKER for:").append(v6).append(" in ").append(((Long) this.a.a.i.get(Integer.valueOf(com.teamspeak.ts3client.d.b.a.a(this.b).c))).longValue()).toString());
        }
        this.a.a.k.y();
        this.b.b();
        return;
    }
}
