package com.teamspeak.ts3client.d.d;
public final class d extends android.support.v4.app.ax {
    private android.widget.Button aA;
    private com.teamspeak.ts3client.Ts3Application at;
    private android.widget.EditText au;
    private android.widget.EditText av;
    private android.widget.EditText aw;
    private android.widget.Spinner ax;
    private android.widget.Spinner ay;
    private android.widget.EditText az;

    public d()
    {
        return;
    }

    static synthetic android.widget.EditText a(com.teamspeak.ts3client.d.d.d p1)
    {
        return p1.aw;
    }

    static synthetic android.widget.Button b(com.teamspeak.ts3client.d.d.d p1)
    {
        return p1.aA;
    }

    static synthetic android.widget.EditText c(com.teamspeak.ts3client.d.d.d p1)
    {
        return p1.au;
    }

    static synthetic android.widget.EditText d(com.teamspeak.ts3client.d.d.d p1)
    {
        return p1.az;
    }

    static synthetic android.widget.EditText e(com.teamspeak.ts3client.d.d.d p1)
    {
        return p1.av;
    }

    static synthetic android.widget.Spinner f(com.teamspeak.ts3client.d.d.d p1)
    {
        return p1.ax;
    }

    static synthetic android.widget.Spinner g(com.teamspeak.ts3client.d.d.d p1)
    {
        return p1.ay;
    }

    static synthetic com.teamspeak.ts3client.Ts3Application h(com.teamspeak.ts3client.d.d.d p1)
    {
        return p1.at;
    }

    public final android.view.View a(android.view.LayoutInflater p7, android.view.ViewGroup p8)
    {
        this.at = ((com.teamspeak.ts3client.Ts3Application) p7.getContext().getApplicationContext());
        android.widget.ScrollView v0_5 = ((android.widget.ScrollView) p7.inflate(2130903126, p8, 0));
        this.j.setTitle(com.teamspeak.ts3client.data.e.a.a("temppass.title"));
        com.teamspeak.ts3client.data.e.a.a("temppass.create.password", v0_5, 2131493321);
        com.teamspeak.ts3client.data.e.a.a("temppass.create.description", v0_5, 2131493323);
        com.teamspeak.ts3client.data.e.a.a("temppass.create.duration", v0_5, 2131493325);
        com.teamspeak.ts3client.data.e.a.a("temppass.create.channel", v0_5, 2131493328);
        com.teamspeak.ts3client.data.e.a.a("temppass.create.channelpw", v0_5, 2131493330);
        this.aA = ((android.widget.Button) v0_5.findViewById(2131493332));
        this.au = ((android.widget.EditText) v0_5.findViewById(2131493322));
        this.av = ((android.widget.EditText) v0_5.findViewById(2131493324));
        this.aw = ((android.widget.EditText) v0_5.findViewById(2131493326));
        this.au.addTextChangedListener(new com.teamspeak.ts3client.d.d.e(this));
        this.aw.addTextChangedListener(new com.teamspeak.ts3client.d.d.f(this));
        this.ax = ((android.widget.Spinner) v0_5.findViewById(2131493327));
        this.ax.setAdapter(com.teamspeak.ts3client.data.e.a.a("temppass.create.duration.array", this.i(), 3));
        this.ay = ((android.widget.Spinner) v0_5.findViewById(2131493329));
        this.az = ((android.widget.EditText) v0_5.findViewById(2131493331));
        android.widget.Button v1_33 = this.at.a.f.a;
        com.teamspeak.ts3client.d.d.h v2_14 = new java.util.ArrayList();
        android.content.Context v3_1 = v1_33.iterator();
        while (v3_1.hasNext()) {
            v2_14.add(((com.teamspeak.ts3client.data.a) v3_1.next()));
        }
        android.widget.Button v1_36 = new com.teamspeak.ts3client.data.a();
        v1_36.a(com.teamspeak.ts3client.data.e.a.a("temppass.create.notarget"));
        v2_14.remove(0);
        v2_14.add(0, v1_36);
        this.ay.setAdapter(new android.widget.ArrayAdapter(v0_5.getContext(), 17367049, v2_14));
        this.ay.setOnItemSelectedListener(new com.teamspeak.ts3client.d.d.g(this));
        this.aA.setText(com.teamspeak.ts3client.data.e.a.a("button.save"));
        this.aA.setOnClickListener(new com.teamspeak.ts3client.d.d.h(this));
        return v0_5;
    }

    public final void e()
    {
        super.e();
        com.teamspeak.ts3client.data.d.q.a(this);
        return;
    }

    public final void f()
    {
        super.f();
        com.teamspeak.ts3client.data.d.q.b(this);
        return;
    }
}
