package com.teamspeak.ts3client.d.b;
public final class aj extends android.support.v4.app.ax {
    private int aA;
    private com.teamspeak.ts3client.data.c at;
    private com.teamspeak.ts3client.Ts3Application au;
    private android.widget.TableLayout av;
    private android.widget.TableLayout aw;
    private android.widget.Button ax;
    private long ay;
    private int az;

    public aj(com.teamspeak.ts3client.data.c p1)
    {
        this.at = p1;
        return;
    }

    private int a(float p3)
    {
        return ((int) ((this.au.getResources().getDisplayMetrics().density * p3) + 1056964608));
    }

    static synthetic com.teamspeak.ts3client.data.c a(com.teamspeak.ts3client.d.b.aj p1)
    {
        return p1.at;
    }

    private void a(android.widget.TableLayout p4, String p5, long p6, boolean p8, boolean p9, com.teamspeak.ts3client.d.b.am p10, boolean p11)
    {
        android.widget.TableRow v0_3 = ((android.widget.TableRow) android.view.LayoutInflater.from(this.i()).inflate(2130903083, 0));
        android.widget.CheckBox v1_3 = ((android.widget.CheckBox) v0_3.findViewById(2131493171));
        v1_3.setText(p5);
        v1_3.setChecked(p8);
        v1_3.setEnabled(p9);
        v1_3.setTag(Long.valueOf(p6));
        p10.a.add(v1_3);
        v1_3.setOnClickListener(p10);
        if (p11) {
            p10.b = v1_3;
        }
        p4.addView(v0_3);
        return;
    }

    static synthetic com.teamspeak.ts3client.Ts3Application b(com.teamspeak.ts3client.d.b.aj p1)
    {
        return p1.au;
    }

    static synthetic long c(com.teamspeak.ts3client.d.b.aj p2)
    {
        return p2.ay;
    }

    static synthetic int d(com.teamspeak.ts3client.d.b.aj p1)
    {
        return p1.aA;
    }

    public final android.view.View a(android.view.LayoutInflater p18, android.view.ViewGroup p19)
    {
        com.teamspeak.ts3client.data.a.a v2_2 = ((com.teamspeak.ts3client.Ts3Application) p18.getContext().getApplicationContext());
        android.widget.LinearLayout v12_1 = ((android.widget.FrameLayout) p18.inflate(2130903082, p19, 0));
        this.j.setTitle(this.at.a);
        this.az = v2_2.a.s.b(v2_2.a.u.a(com.teamspeak.ts3client.jni.g.cg));
        this.aA = v2_2.a.s.b(v2_2.a.u.a(com.teamspeak.ts3client.jni.g.ci));
        com.teamspeak.ts3client.data.e.a.a("groupdialog.servertext", v12_1, 2131493165);
        com.teamspeak.ts3client.data.e.a.a("groupdialog.channeltext", v12_1, 2131493167);
        this.av = ((android.widget.TableLayout) v12_1.findViewById(2131493166));
        this.aw = ((android.widget.TableLayout) v12_1.findViewById(2131493168));
        this.ax = ((android.widget.Button) v12_1.findViewById(2131493169));
        this.ax.setText(com.teamspeak.ts3client.data.e.a.a("button.close"));
        this.ax.setOnClickListener(new com.teamspeak.ts3client.d.b.ak(this));
        com.teamspeak.ts3client.d.b.am v10_1 = new com.teamspeak.ts3client.d.b.am(this, 0);
        long v14_0 = v2_2.a.a.c(v2_2.a.e, com.teamspeak.ts3client.jni.i.z);
        java.util.ArrayList v13_1 = new java.util.ArrayList();
        void v3_27 = v2_2.a.a.b(v2_2.a.e, this.at.c, com.teamspeak.ts3client.jni.d.I);
        if (v3_27 != 0) {
            android.widget.TableLayout v4_20 = v3_27.split(",");
            String v5_2 = v4_20.length;
            void v3_28 = 0;
            while (v3_28 < v5_2) {
                v13_1.add(Long.valueOf(Long.parseLong(v4_20[v3_28])));
                v3_28++;
            }
            void v3_30 = v2_2.a.o;
            android.widget.TableLayout v4_22 = new java.util.ArrayList();
            v4_22.addAll(v3_30.a.values());
            java.util.Collections.sort(v4_22);
            java.util.Iterator v16_0 = v4_22.iterator();
            while (v16_0.hasNext()) {
                void v3_49 = ((com.teamspeak.ts3client.data.g.a) v16_0.next());
                if (v3_49.c == 1) {
                    android.widget.TableLayout v4_38 = 0;
                    int v8_1 = 0;
                    int v11_1 = 0;
                    if ((v3_49.e <= this.az) && (v3_49.a != v14_0)) {
                        v4_38 = 1;
                    }
                    if (v13_1.contains(Long.valueOf(v3_49.a))) {
                        v8_1 = 1;
                    }
                    if ((v8_1 == 0) || (v3_49.f <= this.aA)) {
                        int v9_1 = v4_38;
                    } else {
                        v9_1 = 0;
                    }
                    if (v3_49.a == v14_0) {
                        v11_1 = 1;
                    }
                    this.a(this.av, v3_49.b, v3_49.a, v8_1, v9_1, v10_1, v11_1);
                }
            }
            com.teamspeak.ts3client.d.b.am v10_3 = new com.teamspeak.ts3client.d.b.am(this, 1);
            long v14_1 = v2_2.a.a.c(v2_2.a.e, com.teamspeak.ts3client.jni.i.A);
            java.util.ArrayList v13_3 = new java.util.ArrayList();
            android.widget.TableLayout v4_28 = v2_2.a.a.b(v2_2.a.e, this.at.c, com.teamspeak.ts3client.jni.d.H).split(",");
            String v5_3 = v4_28.length;
            void v3_40 = 0;
            while (v3_40 < v5_3) {
                v13_3.add(Long.valueOf(Long.parseLong(v4_28[v3_40])));
                v3_40++;
            }
            com.teamspeak.ts3client.data.a.a v2_4 = v2_2.a.p;
            void v3_42 = new java.util.ArrayList();
            v3_42.addAll(v2_4.a.values());
            java.util.Collections.sort(v3_42);
            java.util.Iterator v16_1 = v3_42.iterator();
            while (v16_1.hasNext()) {
                com.teamspeak.ts3client.data.a.a v2_9 = ((com.teamspeak.ts3client.data.a.a) v16_1.next());
                if (v2_9.c == 1) {
                    void v3_44 = 0;
                    int v8_0 = 0;
                    int v11_0 = 0;
                    if (v2_9.e <= this.az) {
                        v3_44 = 1;
                    }
                    if (v13_3.contains(Long.valueOf(v2_9.a))) {
                        v8_0 = 1;
                    }
                    if ((v8_0 == 0) || (v2_9.f <= this.aA)) {
                        int v9_0 = v3_44;
                    } else {
                        v9_0 = 0;
                    }
                    if (v2_9.a == v14_1) {
                        v11_0 = 1;
                    }
                    this.a(this.aw, v2_9.b, v2_9.a, v8_0, v9_0, v10_3, v11_0);
                }
            }
        } else {
            v12_1 = new android.widget.LinearLayout(this.i());
            v12_1.setOrientation(1);
            com.teamspeak.ts3client.data.a.a v2_13 = new android.widget.TextView(this.i());
            v2_13.setText("The Client already left the Server");
            v12_1.addView(v2_13);
            com.teamspeak.ts3client.data.a.a v2_15 = new android.widget.Button(this.i());
            v2_15.setText("Close");
            v2_15.setOnClickListener(new com.teamspeak.ts3client.d.b.al(this));
            v12_1.addView(v2_15);
        }
        return v12_1;
    }

    public final void a(android.os.Bundle p6)
    {
        super.a(p6);
        this.au = ((com.teamspeak.ts3client.Ts3Application) this.i().getApplicationContext());
        this.ay = this.au.a.a.c(this.au.a.e, this.at.c, com.teamspeak.ts3client.jni.d.G);
        return;
    }

    public final void e()
    {
        super.e();
        com.teamspeak.ts3client.data.d.q.a(this);
        return;
    }

    public final void f()
    {
        super.f();
        com.teamspeak.ts3client.data.d.q.b(this);
        return;
    }
}
