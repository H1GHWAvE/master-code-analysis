package com.teamspeak.ts3client.d.d;
public final class a extends android.widget.BaseAdapter {
    java.util.ArrayList a;
    private android.content.Context b;
    private android.view.LayoutInflater c;
    private android.support.v4.app.bi d;

    public a(android.content.Context p2, android.support.v4.app.bi p3)
    {
        this.b = p2;
        this.a = new java.util.ArrayList();
        this.c = android.view.LayoutInflater.from(p2);
        this.d = p3;
        return;
    }

    static synthetic java.util.ArrayList a(com.teamspeak.ts3client.d.d.a p1)
    {
        return p1.a;
    }

    private void a()
    {
        this.a.clear();
        return;
    }

    private void a(com.teamspeak.ts3client.d.d.c p2)
    {
        if (!this.a.contains(p2)) {
            this.a.add(p2);
        }
        return;
    }

    static synthetic android.support.v4.app.bi b(com.teamspeak.ts3client.d.d.a p1)
    {
        return p1.d;
    }

    private void b(com.teamspeak.ts3client.d.d.c p2)
    {
        if (!this.a.contains(p2)) {
            this.a.remove(p2);
        }
        return;
    }

    public final int getCount()
    {
        return this.a.size();
    }

    public final Object getItem(int p2)
    {
        return this.a.get(p2);
    }

    public final long getItemId(int p3)
    {
        return 0;
    }

    public final android.view.View getView(int p15, android.view.View p16, android.view.ViewGroup p17)
    {
        android.view.View v3 = this.c.inflate(2130903129, 0);
        com.teamspeak.ts3client.d.d.b v0_3 = ((android.widget.TextView) v3.findViewById(2131493351));
        android.widget.TextView v1_3 = ((android.widget.TextView) v3.findViewById(2131493352));
        int v5_0 = new Object[1];
        v5_0[0] = ((com.teamspeak.ts3client.d.d.c) this.a.get(p15)).c;
        v0_3.setText(com.teamspeak.ts3client.data.e.a.a("temppass.entry.text", v5_0));
        Object[] v4_1 = new Object[1];
        v4_1[0] = new StringBuilder(" ").append(java.text.DateFormat.getDateTimeInstance().format(new java.util.Date((((com.teamspeak.ts3client.d.d.c) this.a.get(p15)).e * 1000)))).toString();
        v1_3.setText(com.teamspeak.ts3client.data.e.a.a("temppass.entry.until", v4_1));
        v3.setOnClickListener(new com.teamspeak.ts3client.d.d.b(this, p15));
        return v3;
    }
}
