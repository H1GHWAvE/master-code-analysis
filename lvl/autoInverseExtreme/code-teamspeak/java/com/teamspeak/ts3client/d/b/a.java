package com.teamspeak.ts3client.d.b;
public final class a extends android.support.v4.app.ax {
    private android.widget.Button aA;
    private android.widget.Button aB;
    private android.widget.Button aC;
    private android.widget.LinearLayout aD;
    private boolean aE;
    private boolean aF;
    private com.teamspeak.ts3client.data.c at;
    private android.widget.Button au;
    private android.widget.Button av;
    private android.widget.Button aw;
    private android.widget.Button ax;
    private android.widget.Button ay;
    private android.widget.Button az;

    public a(com.teamspeak.ts3client.data.c p3, boolean p4)
    {
        this.aF = 0;
        this.at = p3;
        if (p3.c == com.teamspeak.ts3client.Ts3Application.a().a.h) {
            this.aF = 1;
        }
        this.aE = p4;
        return;
    }

    private android.view.View a(android.view.LayoutInflater p10, android.view.ViewGroup p11, com.teamspeak.ts3client.Ts3Application p12)
    {
        android.widget.LinearLayout v0_2 = ((android.widget.LinearLayout) p10.inflate(2130903084, p11, 0));
        this.j.setTitle(this.at.a);
        this.aA = ((android.widget.Button) v0_2.findViewById(2131493175));
        com.teamspeak.ts3client.data.e.a.a("dialog.client.contact.text", v0_2, 2131493175);
        this.aA.setOnClickListener(new com.teamspeak.ts3client.d.b.y(this));
        this.au = ((android.widget.Button) v0_2.findViewById(2131493185));
        com.teamspeak.ts3client.data.e.a.a("dialog.client.kickserver.text", v0_2, 2131493185);
        this.au.setOnClickListener(new com.teamspeak.ts3client.d.b.z(this));
        this.av = ((android.widget.Button) v0_2.findViewById(2131493184));
        com.teamspeak.ts3client.data.e.a.a("dialog.client.kickchannel.text", v0_2, 2131493184);
        this.av.setOnClickListener(new com.teamspeak.ts3client.d.b.aa(this));
        this.aw = ((android.widget.Button) v0_2.findViewById(2131493176));
        com.teamspeak.ts3client.data.e.a.a("dialog.client.poke.text", v0_2, 2131493176);
        this.aw.setOnClickListener(new com.teamspeak.ts3client.d.b.c(this));
        this.aB = ((android.widget.Button) v0_2.findViewById(2131493182));
        com.teamspeak.ts3client.data.e.a.a("dialog.client.priorityspeaker.text", v0_2, 2131493182);
        this.aB.setOnClickListener(new com.teamspeak.ts3client.d.b.d(this, p12));
        this.aC = ((android.widget.Button) v0_2.findViewById(2131493181));
        com.teamspeak.ts3client.data.e.a.a("dialog.client.volumemodifier.text", v0_2, 2131493181);
        this.aC.setOnClickListener(new com.teamspeak.ts3client.d.b.e(this, p12));
        this.ax = ((android.widget.Button) v0_2.findViewById(2131493178));
        com.teamspeak.ts3client.data.e.a.a("dialog.client.complain.text", v0_2, 2131493178);
        this.ax.setOnClickListener(new com.teamspeak.ts3client.d.b.h(this));
        this.ay = ((android.widget.Button) v0_2.findViewById(2131493183));
        com.teamspeak.ts3client.data.e.a.a("dialog.client.ban.text", v0_2, 2131493183);
        this.ay.setOnClickListener(new com.teamspeak.ts3client.d.b.i(this));
        this.az = ((android.widget.Button) v0_2.findViewById(2131493179));
        com.teamspeak.ts3client.data.e.a.a("dialog.client.mute.text1", v0_2, 2131493179);
        if (p12.a.a.a(p12.a.e, this.at.c, com.teamspeak.ts3client.jni.d.q) == 1) {
            com.teamspeak.ts3client.data.e.a.a("dialog.client.mute.text2", v0_2, 2131493179);
        }
        this.az.setOnClickListener(new com.teamspeak.ts3client.d.b.j(this, p12));
        this.aB = ((android.widget.Button) v0_2.findViewById(2131493182));
        com.teamspeak.ts3client.data.e.a.a("dialog.client.priorityspeaker.text", v0_2, 2131493182);
        this.aB.setOnClickListener(new com.teamspeak.ts3client.d.b.k(this, p12));
        this.aD = ((android.widget.LinearLayout) v0_2.findViewById(2131493180));
        if (this.aE) {
            android.widget.Button v1_55 = new android.widget.Button(this.i());
            v1_55.setText(com.teamspeak.ts3client.data.e.a.a("dialog.client.info.text"));
            v1_55.setOnClickListener(new com.teamspeak.ts3client.d.b.l(this, p12));
            this.aD.addView(v1_55);
        }
        android.widget.Button v1_57 = new android.widget.Button(v0_2.getContext());
        v1_57.setText(com.teamspeak.ts3client.data.e.a.a("dialog.client.move.text"));
        v1_57.setOnClickListener(new com.teamspeak.ts3client.d.b.m(this, p12));
        this.aD.addView(v1_57);
        android.widget.Button v1_59 = new android.widget.Button(v0_2.getContext());
        v1_59.setText(com.teamspeak.ts3client.data.e.a.a("dialog.client.chat.text"));
        v1_59.setOnClickListener(new com.teamspeak.ts3client.d.b.o(this, p12));
        this.aD.addView(v1_59);
        if (p12.a.i.get(Integer.valueOf(this.at.c)) != null) {
            android.widget.LinearLayout v2_52 = p12.a.s.a(com.teamspeak.ts3client.jni.g.do);
            android.widget.Button v1_70 = p12.a.c().a(((Long) p12.a.i.get(Integer.valueOf(this.at.c))));
            if ((this.at.p.equals("0")) && ((v1_70.h <= 0) || ((this.at.o >= v1_70.h) || (this.at.r)))) {
                if (this.at.r) {
                    android.widget.Button v1_77 = new android.widget.Button(v0_2.getContext());
                    v1_77.setText(com.teamspeak.ts3client.data.e.a.a("dialog.client.talkpower.text2"));
                    v1_77.setOnClickListener(new com.teamspeak.ts3client.d.b.r(this, p12));
                    v1_77.setEnabled(v2_52);
                    this.aD.addView(v1_77);
                }
            } else {
                android.widget.Button v1_79 = new android.widget.Button(v0_2.getContext());
                v1_79.setText(com.teamspeak.ts3client.data.e.a.a("dialog.client.talkpower.text1"));
                v1_79.setOnClickListener(new com.teamspeak.ts3client.d.b.q(this, p12));
                v1_79.setEnabled(v2_52);
                this.aD.addView(v1_79);
            }
            android.widget.Button v1_81 = new android.widget.Button(v0_2.getContext());
            v1_81.setText(com.teamspeak.ts3client.data.e.a.a("dialog.client.group.text"));
            v1_81.setOnClickListener(new com.teamspeak.ts3client.d.b.s(this));
            this.aD.addView(v1_81);
            android.widget.Button v1_83 = new android.widget.Button(v0_2.getContext());
            v1_83.setText(com.teamspeak.ts3client.data.e.a.a("dialog.client.cinfo.text"));
            v1_83.setOnClickListener(new com.teamspeak.ts3client.d.b.t(this));
            this.aD.addView(v1_83);
            if (this.at.v != 0) {
                this.ax.setEnabled(0);
                this.ay.setEnabled(0);
                this.aA.setEnabled(0);
                this.av.setEnabled(0);
                this.aD.setEnabled(0);
                this.az.setEnabled(0);
                this.aw.setEnabled(0);
                this.aB.setEnabled(0);
                this.au.setEnabled(0);
            }
        }
        return v0_2;
    }

    private android.view.View a(com.teamspeak.ts3client.Ts3Application p6)
    {
        android.widget.LinearLayout v1_1 = new android.widget.LinearLayout(this.i());
        this.j.setTitle(this.at.a);
        v1_1.setOrientation(1);
        v1_1.setLayoutParams(new android.view.ViewGroup$LayoutParams(-1, -2));
        android.widget.Button v0_6 = new android.widget.Button(v1_1.getContext());
        v0_6.setText(com.teamspeak.ts3client.data.e.a.a("dialog.client.priorityspeaker.text"));
        v0_6.setOnClickListener(new com.teamspeak.ts3client.d.b.b(this, p6));
        v1_1.addView(v0_6);
        if (this.aE) {
            android.widget.Button v0_9 = new android.widget.Button(this.i());
            v0_9.setText(com.teamspeak.ts3client.data.e.a.a("dialog.client.info.text"));
            v0_9.setOnClickListener(new com.teamspeak.ts3client.d.b.p(this, p6));
            v1_1.addView(v0_9);
        }
        com.teamspeak.ts3client.d.b.x v2_14 = p6.a.s.a(com.teamspeak.ts3client.jni.g.do);
        if (p6.a.i.get(Integer.valueOf(this.at.c)) != null) {
            android.widget.Button v0_20 = p6.a.c().a(((Long) p6.a.i.get(Integer.valueOf(this.at.c))));
            if ((this.at.p.equals("0")) && ((v0_20.h <= 0) || ((this.at.o >= v0_20.h) || (this.at.r)))) {
                if (this.at.r) {
                    android.widget.Button v0_27 = new android.widget.Button(v1_1.getContext());
                    v0_27.setText(com.teamspeak.ts3client.data.e.a.a("dialog.client.talkpower.text2"));
                    v0_27.setOnClickListener(new com.teamspeak.ts3client.d.b.v(this, p6));
                    v0_27.setEnabled(v2_14);
                    v1_1.addView(v0_27);
                }
            } else {
                android.widget.Button v0_29 = new android.widget.Button(v1_1.getContext());
                v0_29.setText(com.teamspeak.ts3client.data.e.a.a("dialog.client.talkpower.text1"));
                v0_29.setOnClickListener(new com.teamspeak.ts3client.d.b.u(this, p6));
                v0_29.setEnabled(v2_14);
                v1_1.addView(v0_29);
            }
        }
        android.widget.Button v0_31 = new android.widget.Button(v1_1.getContext());
        v0_31.setText(com.teamspeak.ts3client.data.e.a.a("dialog.client.group.text"));
        v0_31.setOnClickListener(new com.teamspeak.ts3client.d.b.w(this));
        v1_1.addView(v0_31);
        android.widget.Button v0_33 = new android.widget.Button(v1_1.getContext());
        v0_33.setText(com.teamspeak.ts3client.data.e.a.a("dialog.client.cinfo.text"));
        v0_33.setOnClickListener(new com.teamspeak.ts3client.d.b.x(this));
        v1_1.addView(v0_33);
        return v1_1;
    }

    static synthetic com.teamspeak.ts3client.data.c a(com.teamspeak.ts3client.d.b.a p1)
    {
        return p1.at;
    }

    public final android.view.View a(android.view.LayoutInflater p12, android.view.ViewGroup p13)
    {
        android.widget.Button v1_40;
        android.widget.Button v0_2 = ((com.teamspeak.ts3client.Ts3Application) p12.getContext().getApplicationContext());
        if (this.aF) {
            android.widget.Button v2_1 = new android.widget.LinearLayout(this.i());
            this.j.setTitle(this.at.a);
            v2_1.setOrientation(1);
            v2_1.setLayoutParams(new android.view.ViewGroup$LayoutParams(-1, -2));
            android.widget.Button v1_6 = new android.widget.Button(v2_1.getContext());
            v1_6.setText(com.teamspeak.ts3client.data.e.a.a("dialog.client.priorityspeaker.text"));
            v1_6.setOnClickListener(new com.teamspeak.ts3client.d.b.b(this, v0_2));
            v2_1.addView(v1_6);
            if (this.aE) {
                android.widget.Button v1_9 = new android.widget.Button(this.i());
                v1_9.setText(com.teamspeak.ts3client.data.e.a.a("dialog.client.info.text"));
                v1_9.setOnClickListener(new com.teamspeak.ts3client.d.b.p(this, v0_2));
                v2_1.addView(v1_9);
            }
            boolean v3_14 = v0_2.a.s.a(com.teamspeak.ts3client.jni.g.do);
            if (v0_2.a.i.get(Integer.valueOf(this.at.c)) != null) {
                android.widget.Button v1_20 = v0_2.a.c().a(((Long) v0_2.a.i.get(Integer.valueOf(this.at.c))));
                if ((this.at.p.equals("0")) && ((v1_20.h <= 0) || ((this.at.o >= v1_20.h) || (this.at.r)))) {
                    if (this.at.r) {
                        android.widget.Button v1_27 = new android.widget.Button(v2_1.getContext());
                        v1_27.setText(com.teamspeak.ts3client.data.e.a.a("dialog.client.talkpower.text2"));
                        v1_27.setOnClickListener(new com.teamspeak.ts3client.d.b.v(this, v0_2));
                        v1_27.setEnabled(v3_14);
                        v2_1.addView(v1_27);
                    }
                } else {
                    android.widget.Button v1_29 = new android.widget.Button(v2_1.getContext());
                    v1_29.setText(com.teamspeak.ts3client.data.e.a.a("dialog.client.talkpower.text1"));
                    v1_29.setOnClickListener(new com.teamspeak.ts3client.d.b.u(this, v0_2));
                    v1_29.setEnabled(v3_14);
                    v2_1.addView(v1_29);
                }
            }
            android.widget.Button v0_4 = new android.widget.Button(v2_1.getContext());
            v0_4.setText(com.teamspeak.ts3client.data.e.a.a("dialog.client.group.text"));
            v0_4.setOnClickListener(new com.teamspeak.ts3client.d.b.w(this));
            v2_1.addView(v0_4);
            android.widget.Button v0_6 = new android.widget.Button(v2_1.getContext());
            v0_6.setText(com.teamspeak.ts3client.data.e.a.a("dialog.client.cinfo.text"));
            v0_6.setOnClickListener(new com.teamspeak.ts3client.d.b.x(this));
            v2_1.addView(v0_6);
            v1_40 = v2_1;
        } else {
            v1_40 = ((android.widget.LinearLayout) p12.inflate(2130903084, p13, 0));
            this.j.setTitle(this.at.a);
            this.aA = ((android.widget.Button) v1_40.findViewById(2131493175));
            com.teamspeak.ts3client.data.e.a.a("dialog.client.contact.text", v1_40, 2131493175);
            this.aA.setOnClickListener(new com.teamspeak.ts3client.d.b.y(this));
            this.au = ((android.widget.Button) v1_40.findViewById(2131493185));
            com.teamspeak.ts3client.data.e.a.a("dialog.client.kickserver.text", v1_40, 2131493185);
            this.au.setOnClickListener(new com.teamspeak.ts3client.d.b.z(this));
            this.av = ((android.widget.Button) v1_40.findViewById(2131493184));
            com.teamspeak.ts3client.data.e.a.a("dialog.client.kickchannel.text", v1_40, 2131493184);
            this.av.setOnClickListener(new com.teamspeak.ts3client.d.b.aa(this));
            this.aw = ((android.widget.Button) v1_40.findViewById(2131493176));
            com.teamspeak.ts3client.data.e.a.a("dialog.client.poke.text", v1_40, 2131493176);
            this.aw.setOnClickListener(new com.teamspeak.ts3client.d.b.c(this));
            this.aB = ((android.widget.Button) v1_40.findViewById(2131493182));
            com.teamspeak.ts3client.data.e.a.a("dialog.client.priorityspeaker.text", v1_40, 2131493182);
            this.aB.setOnClickListener(new com.teamspeak.ts3client.d.b.d(this, v0_2));
            this.aC = ((android.widget.Button) v1_40.findViewById(2131493181));
            com.teamspeak.ts3client.data.e.a.a("dialog.client.volumemodifier.text", v1_40, 2131493181);
            this.aC.setOnClickListener(new com.teamspeak.ts3client.d.b.e(this, v0_2));
            this.ax = ((android.widget.Button) v1_40.findViewById(2131493178));
            com.teamspeak.ts3client.data.e.a.a("dialog.client.complain.text", v1_40, 2131493178);
            this.ax.setOnClickListener(new com.teamspeak.ts3client.d.b.h(this));
            this.ay = ((android.widget.Button) v1_40.findViewById(2131493183));
            com.teamspeak.ts3client.data.e.a.a("dialog.client.ban.text", v1_40, 2131493183);
            this.ay.setOnClickListener(new com.teamspeak.ts3client.d.b.i(this));
            this.az = ((android.widget.Button) v1_40.findViewById(2131493179));
            com.teamspeak.ts3client.data.e.a.a("dialog.client.mute.text1", v1_40, 2131493179);
            if (v0_2.a.a.a(v0_2.a.e, this.at.c, com.teamspeak.ts3client.jni.d.q) == 1) {
                com.teamspeak.ts3client.data.e.a.a("dialog.client.mute.text2", v1_40, 2131493179);
            }
            this.az.setOnClickListener(new com.teamspeak.ts3client.d.b.j(this, v0_2));
            this.aB = ((android.widget.Button) v1_40.findViewById(2131493182));
            com.teamspeak.ts3client.data.e.a.a("dialog.client.priorityspeaker.text", v1_40, 2131493182);
            this.aB.setOnClickListener(new com.teamspeak.ts3client.d.b.k(this, v0_2));
            this.aD = ((android.widget.LinearLayout) v1_40.findViewById(2131493180));
            if (this.aE) {
                android.widget.Button v2_58 = new android.widget.Button(this.i());
                v2_58.setText(com.teamspeak.ts3client.data.e.a.a("dialog.client.info.text"));
                v2_58.setOnClickListener(new com.teamspeak.ts3client.d.b.l(this, v0_2));
                this.aD.addView(v2_58);
            }
            android.widget.Button v2_60 = new android.widget.Button(v1_40.getContext());
            v2_60.setText(com.teamspeak.ts3client.data.e.a.a("dialog.client.move.text"));
            v2_60.setOnClickListener(new com.teamspeak.ts3client.d.b.m(this, v0_2));
            this.aD.addView(v2_60);
            android.widget.Button v2_62 = new android.widget.Button(v1_40.getContext());
            v2_62.setText(com.teamspeak.ts3client.data.e.a.a("dialog.client.chat.text"));
            v2_62.setOnClickListener(new com.teamspeak.ts3client.d.b.o(this, v0_2));
            this.aD.addView(v2_62);
            if (v0_2.a.i.get(Integer.valueOf(this.at.c)) != null) {
                boolean v3_68 = v0_2.a.s.a(com.teamspeak.ts3client.jni.g.do);
                android.widget.Button v2_73 = v0_2.a.c().a(((Long) v0_2.a.i.get(Integer.valueOf(this.at.c))));
                if ((this.at.p.equals("0")) && ((v2_73.h <= 0) || ((this.at.o >= v2_73.h) || (this.at.r)))) {
                    if (this.at.r) {
                        android.widget.Button v2_80 = new android.widget.Button(v1_40.getContext());
                        v2_80.setText(com.teamspeak.ts3client.data.e.a.a("dialog.client.talkpower.text2"));
                        v2_80.setOnClickListener(new com.teamspeak.ts3client.d.b.r(this, v0_2));
                        v2_80.setEnabled(v3_68);
                        this.aD.addView(v2_80);
                    }
                } else {
                    android.widget.Button v2_82 = new android.widget.Button(v1_40.getContext());
                    v2_82.setText(com.teamspeak.ts3client.data.e.a.a("dialog.client.talkpower.text1"));
                    v2_82.setOnClickListener(new com.teamspeak.ts3client.d.b.q(this, v0_2));
                    v2_82.setEnabled(v3_68);
                    this.aD.addView(v2_82);
                }
                android.widget.Button v0_10 = new android.widget.Button(v1_40.getContext());
                v0_10.setText(com.teamspeak.ts3client.data.e.a.a("dialog.client.group.text"));
                v0_10.setOnClickListener(new com.teamspeak.ts3client.d.b.s(this));
                this.aD.addView(v0_10);
                android.widget.Button v0_12 = new android.widget.Button(v1_40.getContext());
                v0_12.setText(com.teamspeak.ts3client.data.e.a.a("dialog.client.cinfo.text"));
                v0_12.setOnClickListener(new com.teamspeak.ts3client.d.b.t(this));
                this.aD.addView(v0_12);
                if (this.at.v != 0) {
                    this.ax.setEnabled(0);
                    this.ay.setEnabled(0);
                    this.aA.setEnabled(0);
                    this.av.setEnabled(0);
                    this.aD.setEnabled(0);
                    this.az.setEnabled(0);
                    this.aw.setEnabled(0);
                    this.aB.setEnabled(0);
                    this.au.setEnabled(0);
                }
            }
        }
        return v1_40;
    }

    public final void e()
    {
        super.e();
        com.teamspeak.ts3client.data.d.q.a(this, new StringBuilder("cid_").append(this.at.c).toString());
        return;
    }

    public final void f()
    {
        super.f();
        com.teamspeak.ts3client.data.d.q.b(this);
        return;
    }
}
