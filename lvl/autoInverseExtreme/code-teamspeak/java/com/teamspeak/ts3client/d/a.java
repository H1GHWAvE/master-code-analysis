package com.teamspeak.ts3client.d;
public final class a extends android.support.v4.app.ax implements com.teamspeak.ts3client.data.w {
    private boolean aA;
    private android.widget.TextView aB;
    private android.widget.TextView aC;
    private android.widget.TextView aD;
    private android.widget.TextView aE;
    private android.widget.TextView aF;
    private android.widget.TextView aG;
    private android.widget.TextView aH;
    private android.widget.TextView aI;
    private android.widget.TextView aJ;
    private android.widget.TextView aK;
    private android.widget.TextView aL;
    private android.widget.TextView aM;
    private android.widget.TextView aN;
    private com.teamspeak.ts3client.data.c aO;
    private android.widget.TableRow aP;
    private android.widget.TextView at;
    private android.widget.TextView au;
    private android.widget.TextView av;
    private android.widget.TextView aw;
    private android.widget.TextView ax;
    private com.teamspeak.ts3client.Ts3Application ay;
    private Thread az;

    public a(com.teamspeak.ts3client.data.c p2)
    {
        this.aA = 1;
        this.aO = p2;
        return;
    }

    static synthetic boolean a(com.teamspeak.ts3client.d.a p1)
    {
        return p1.aA;
    }

    static synthetic com.teamspeak.ts3client.data.c b(com.teamspeak.ts3client.d.a p1)
    {
        return p1.aO;
    }

    static synthetic com.teamspeak.ts3client.Ts3Application c(com.teamspeak.ts3client.d.a p1)
    {
        return p1.ay;
    }

    static synthetic void d(com.teamspeak.ts3client.d.a p0)
    {
        p0.y();
        return;
    }

    static synthetic android.widget.TextView e(com.teamspeak.ts3client.d.a p1)
    {
        return p1.at;
    }

    static synthetic android.widget.TextView f(com.teamspeak.ts3client.d.a p1)
    {
        return p1.au;
    }

    static synthetic android.widget.TextView g(com.teamspeak.ts3client.d.a p1)
    {
        return p1.av;
    }

    static synthetic android.widget.TextView h(com.teamspeak.ts3client.d.a p1)
    {
        return p1.aw;
    }

    static synthetic android.widget.TextView i(com.teamspeak.ts3client.d.a p1)
    {
        return p1.ax;
    }

    static synthetic android.widget.TextView j(com.teamspeak.ts3client.d.a p1)
    {
        return p1.aL;
    }

    static synthetic android.widget.TextView k(com.teamspeak.ts3client.d.a p1)
    {
        return p1.aM;
    }

    static synthetic android.widget.TextView l(com.teamspeak.ts3client.d.a p1)
    {
        return p1.aN;
    }

    static synthetic android.widget.TextView m(com.teamspeak.ts3client.d.a p1)
    {
        return p1.aB;
    }

    static synthetic android.widget.TextView n(com.teamspeak.ts3client.d.a p1)
    {
        return p1.aC;
    }

    static synthetic android.widget.TextView o(com.teamspeak.ts3client.d.a p1)
    {
        return p1.aE;
    }

    static synthetic android.widget.TextView p(com.teamspeak.ts3client.d.a p1)
    {
        return p1.aD;
    }

    static synthetic android.widget.TextView q(com.teamspeak.ts3client.d.a p1)
    {
        return p1.aF;
    }

    static synthetic android.widget.TextView r(com.teamspeak.ts3client.d.a p1)
    {
        return p1.aG;
    }

    static synthetic android.widget.TextView s(com.teamspeak.ts3client.d.a p1)
    {
        return p1.aH;
    }

    static synthetic android.widget.TextView t(com.teamspeak.ts3client.d.a p1)
    {
        return p1.aI;
    }

    static synthetic android.widget.TextView u(com.teamspeak.ts3client.d.a p1)
    {
        return p1.aJ;
    }

    static synthetic android.widget.TextView v(com.teamspeak.ts3client.d.a p1)
    {
        return p1.aK;
    }

    private void y()
    {
        if (this.l()) {
            this.i().runOnUiThread(new com.teamspeak.ts3client.d.c(this, new java.text.DecimalFormat("0.00"), new java.text.DecimalFormat("0.00%")));
        }
        return;
    }

    public final android.view.View a(android.view.LayoutInflater p5, android.view.ViewGroup p6)
    {
        android.widget.HorizontalScrollView v0_2 = ((android.widget.HorizontalScrollView) p5.inflate(2130903081, p6, 0));
        this.j.setTitle(com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.title"));
        this.ay = com.teamspeak.ts3client.Ts3Application.a();
        this.at = ((android.widget.TextView) v0_2.findViewById(2131493122));
        this.au = ((android.widget.TextView) v0_2.findViewById(2131493124));
        this.av = ((android.widget.TextView) v0_2.findViewById(2131493127));
        this.aw = ((android.widget.TextView) v0_2.findViewById(2131493129));
        this.ax = ((android.widget.TextView) v0_2.findViewById(2131493132));
        this.aL = ((android.widget.TextView) v0_2.findViewById(2131493135));
        this.aB = ((android.widget.TextView) v0_2.findViewById(2131493146));
        this.aC = ((android.widget.TextView) v0_2.findViewById(2131493147));
        this.aE = ((android.widget.TextView) v0_2.findViewById(2131493150));
        this.aD = ((android.widget.TextView) v0_2.findViewById(2131493151));
        this.aF = ((android.widget.TextView) v0_2.findViewById(2131493154));
        this.aG = ((android.widget.TextView) v0_2.findViewById(2131493155));
        this.aH = ((android.widget.TextView) v0_2.findViewById(2131493158));
        this.aI = ((android.widget.TextView) v0_2.findViewById(2131493159));
        this.aJ = ((android.widget.TextView) v0_2.findViewById(2131493162));
        this.aK = ((android.widget.TextView) v0_2.findViewById(2131493163));
        this.aM = ((android.widget.TextView) v0_2.findViewById(2131493142));
        this.aN = ((android.widget.TextView) v0_2.findViewById(2131493143));
        this.aP = ((android.widget.TableRow) v0_2.findViewById(2131493130));
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.loading", this.at);
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.loading", this.au);
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.loading", this.av);
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.loading", this.aw);
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.loading", this.ax);
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.loading", this.aL);
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.loading", this.aB);
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.loading", this.aC);
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.loading", this.aE);
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.loading", this.aD);
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.loading", this.aF);
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.loading", this.aG);
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.loading", this.aH);
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.loading", this.aI);
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.loading", this.aJ);
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.loading", this.aK);
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.loading", this.aM);
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.loading", this.aN);
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.name", v0_2, 2131493121);
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.contime", v0_2, 2131493123);
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.idle", v0_2, 2131493126);
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.ping", v0_2, 2131493128);
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.saddress", v0_2, 2131493131);
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.caddress", v0_2, 2131493134);
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.packettrans", v0_2, 2131493145);
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.bytetrans", v0_2, 2131493149);
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.bandwidthsec", v0_2, 2131493153);
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.bandwidthmin", v0_2, 2131493157);
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.filebandwidth", v0_2, 2131493161);
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.packloss", v0_2, 2131493141);
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.tablein", v0_2, 2131493138);
        com.teamspeak.ts3client.data.e.a.a("connectioninfoclient.tableout", v0_2, 2131493139);
        if (this.aO.c == this.ay.a.h) {
            this.aP.setVisibility(0);
        } else {
            this.aP.setVisibility(8);
        }
        return v0_2;
    }

    public final void a(android.os.Bundle p1)
    {
        super.a(p1);
        return;
    }

    public final void a(com.teamspeak.ts3client.jni.k p5)
    {
        if (((p5 instanceof com.teamspeak.ts3client.jni.events.ConnectionInfo)) && (((com.teamspeak.ts3client.jni.events.ConnectionInfo) p5).a == this.aO.c)) {
            this.y();
        }
        if (((p5 instanceof com.teamspeak.ts3client.jni.events.ServerError)) && ((((com.teamspeak.ts3client.jni.events.ServerError) p5).b == 512) && (((com.teamspeak.ts3client.jni.events.ServerError) p5).c.equals(new StringBuilder("connectioninfo_").append(this.aO.c).toString())))) {
            this.ay.a.c.b(this);
            this.aA = 0;
            this.az.interrupt();
            if (this.aO.c != this.ay.a.h) {
                this.ay.a.a.ts3client_cleanUpConnectionInfo(this.ay.a.e, this.aO.c);
            }
            this.i().runOnUiThread(new com.teamspeak.ts3client.d.d(this));
        }
        return;
    }

    public final void b()
    {
        super.b();
        return;
    }

    public final void e()
    {
        super.e();
        com.teamspeak.ts3client.data.d.q.a(this);
        return;
    }

    public final void f()
    {
        super.f();
        com.teamspeak.ts3client.data.d.q.b(this);
        return;
    }

    public final void s()
    {
        super.s();
        this.ay.a.c.a(this);
        this.aA = 1;
        this.az = new Thread(new com.teamspeak.ts3client.d.b(this));
        this.az.start();
        return;
    }

    public final void t()
    {
        super.t();
        this.aA = 0;
        this.az.interrupt();
        try {
            this.ay.a.c.b(this);
        } catch (com.teamspeak.ts3client.jni.Ts3Jni v0) {
            return;
        }
        if (this.aO.c == this.ay.a.h) {
            return;
        } else {
            this.ay.a.a.ts3client_cleanUpConnectionInfo(this.ay.a.e, this.aO.c);
            return;
        }
    }
}
