package com.teamspeak.ts3client.d;
public final class z extends android.support.v4.app.ax implements com.teamspeak.ts3client.data.w {
    private static java.util.regex.Pattern at;
    private android.widget.TextView aA;
    private android.widget.TextView aB;
    private android.widget.TextView aC;
    private com.teamspeak.ts3client.Ts3Application aD;
    private Thread aE;
    private boolean aF;
    private android.widget.TextView aG;
    private android.widget.TextView aH;
    private android.widget.TextView aI;
    private android.widget.TextView aJ;
    private android.widget.TextView aK;
    private android.widget.TextView aL;
    private android.widget.TextView aM;
    private android.widget.TextView aN;
    private android.widget.TextView aO;
    private android.widget.TextView aP;
    private android.widget.TextView aQ;
    private android.widget.TextView aR;
    private android.widget.TextView au;
    private android.widget.TextView av;
    private android.widget.TextView aw;
    private android.widget.TextView ax;
    private android.widget.TextView ay;
    private android.widget.TextView az;

    static z()
    {
        com.teamspeak.ts3client.d.z.at = java.util.regex.Pattern.compile(".*(\\[Build: (\\d+)\\]).*");
        return;
    }

    public z()
    {
        this.aF = 1;
        return;
    }

    private void A()
    {
        if (this.l()) {
            this.i().runOnUiThread(new com.teamspeak.ts3client.d.ac(this, new java.text.DecimalFormat("0.00%")));
        }
        return;
    }

    private String B()
    {
        String v0_5;
        switch (this.aD.a.a.ts3client_getServerLicenseType(this.aD.a.e)) {
            case 0:
                v0_5 = com.teamspeak.ts3client.data.e.a.a("license.no");
                break;
            case 1:
                v0_5 = com.teamspeak.ts3client.data.e.a.a("license.licensed");
                break;
            case 2:
                v0_5 = com.teamspeak.ts3client.data.e.a.a("license.offline");
                break;
            case 3:
                v0_5 = com.teamspeak.ts3client.data.e.a.a("license.npl");
                break;
            default:
                v0_5 = "Error";
        }
        return v0_5;
    }

    static synthetic boolean a(com.teamspeak.ts3client.d.z p1)
    {
        return p1.aF;
    }

    static synthetic com.teamspeak.ts3client.Ts3Application b(com.teamspeak.ts3client.d.z p1)
    {
        return p1.aD;
    }

    static synthetic android.widget.TextView c(com.teamspeak.ts3client.d.z p1)
    {
        return p1.ay;
    }

    static synthetic android.widget.TextView d(com.teamspeak.ts3client.d.z p1)
    {
        return p1.au;
    }

    static synthetic android.widget.TextView e(com.teamspeak.ts3client.d.z p1)
    {
        return p1.aB;
    }

    static synthetic android.widget.TextView f(com.teamspeak.ts3client.d.z p1)
    {
        return p1.aC;
    }

    static synthetic android.widget.TextView g(com.teamspeak.ts3client.d.z p1)
    {
        return p1.aG;
    }

    static synthetic android.widget.TextView h(com.teamspeak.ts3client.d.z p1)
    {
        return p1.aH;
    }

    static synthetic android.widget.TextView i(com.teamspeak.ts3client.d.z p1)
    {
        return p1.aJ;
    }

    static synthetic android.widget.TextView j(com.teamspeak.ts3client.d.z p1)
    {
        return p1.aI;
    }

    static synthetic android.widget.TextView k(com.teamspeak.ts3client.d.z p1)
    {
        return p1.aK;
    }

    static synthetic android.widget.TextView l(com.teamspeak.ts3client.d.z p1)
    {
        return p1.aL;
    }

    static synthetic android.widget.TextView m(com.teamspeak.ts3client.d.z p1)
    {
        return p1.aM;
    }

    static synthetic android.widget.TextView n(com.teamspeak.ts3client.d.z p1)
    {
        return p1.aN;
    }

    static synthetic android.widget.TextView o(com.teamspeak.ts3client.d.z p1)
    {
        return p1.aO;
    }

    static synthetic android.widget.TextView p(com.teamspeak.ts3client.d.z p1)
    {
        return p1.aP;
    }

    static synthetic android.widget.TextView q(com.teamspeak.ts3client.d.z p1)
    {
        return p1.aQ;
    }

    static synthetic android.widget.TextView r(com.teamspeak.ts3client.d.z p1)
    {
        return p1.aR;
    }

    private void y()
    {
        android.widget.TextView v0_3 = this.aD.a.a.b(this.aD.a.e, com.teamspeak.ts3client.jni.i.e);
        String v1_4 = com.teamspeak.ts3client.d.z.at.matcher(v0_3);
        if ((v1_4.find()) && (Long.parseLong(v1_4.group(2)) > 20000)) {
            v0_3 = v0_3.replace(v1_4.group(1), new StringBuilder("(").append(java.text.DateFormat.getDateTimeInstance(2, 2).format(new java.util.Date((Long.parseLong(v1_4.group(2)) * 1000)))).append(")").toString());
        }
        android.widget.TextView v0_10;
        this.av.setText(v0_3);
        this.aw.setText(this.aD.a.a.b(this.aD.a.e, com.teamspeak.ts3client.jni.i.d));
        String v1_15 = this.ax;
        switch (this.aD.a.a.ts3client_getServerLicenseType(this.aD.a.e)) {
            case 0:
                v0_10 = com.teamspeak.ts3client.data.e.a.a("license.no");
                break;
            case 1:
                v0_10 = com.teamspeak.ts3client.data.e.a.a("license.licensed");
                break;
            case 2:
                v0_10 = com.teamspeak.ts3client.data.e.a.a("license.offline");
                break;
            case 3:
                v0_10 = com.teamspeak.ts3client.data.e.a.a("license.npl");
                break;
            default:
                v0_10 = "Error";
        }
        v1_15.setText(v0_10);
        this.aA.setText(new StringBuilder().append(this.aD.a.v.o).append(":").append(this.aD.a.v.p).toString());
        this.az.setText(this.aD.a.v.c);
        return;
    }

    private void z()
    {
        if (this.l()) {
            this.i().runOnUiThread(new com.teamspeak.ts3client.d.ab(this));
        }
        return;
    }

    public final android.view.View a(android.view.LayoutInflater p4, android.view.ViewGroup p5)
    {
        android.widget.HorizontalScrollView v0_2 = ((android.widget.HorizontalScrollView) p4.inflate(2130903122, p5, 0));
        this.j.setTitle(com.teamspeak.ts3client.data.e.a.a("connectioninfo.title"));
        this.aD = com.teamspeak.ts3client.Ts3Application.a();
        this.au = ((android.widget.TextView) v0_2.findViewById(2131493279));
        this.av = ((android.widget.TextView) v0_2.findViewById(2131493281));
        this.aw = ((android.widget.TextView) v0_2.findViewById(2131493283));
        this.ax = ((android.widget.TextView) v0_2.findViewById(2131493285));
        this.ay = ((android.widget.TextView) v0_2.findViewById(2131493287));
        this.az = ((android.widget.TextView) v0_2.findViewById(2131493289));
        this.aA = ((android.widget.TextView) v0_2.findViewById(2131493291));
        this.aB = ((android.widget.TextView) v0_2.findViewById(2131493293));
        this.aC = ((android.widget.TextView) v0_2.findViewById(2131493295));
        this.aG = ((android.widget.TextView) v0_2.findViewById(2131493299));
        this.aH = ((android.widget.TextView) v0_2.findViewById(2131493300));
        this.aJ = ((android.widget.TextView) v0_2.findViewById(2131493302));
        this.aI = ((android.widget.TextView) v0_2.findViewById(2131493303));
        this.aK = ((android.widget.TextView) v0_2.findViewById(2131493305));
        this.aL = ((android.widget.TextView) v0_2.findViewById(2131493306));
        this.aM = ((android.widget.TextView) v0_2.findViewById(2131493308));
        this.aN = ((android.widget.TextView) v0_2.findViewById(2131493309));
        this.aO = ((android.widget.TextView) v0_2.findViewById(2131493311));
        this.aP = ((android.widget.TextView) v0_2.findViewById(2131493312));
        this.aQ = ((android.widget.TextView) v0_2.findViewById(2131493314));
        this.aR = ((android.widget.TextView) v0_2.findViewById(2131493315));
        com.teamspeak.ts3client.data.e.a.a("connectioninfo.name", v0_2, 2131493278);
        com.teamspeak.ts3client.data.e.a.a("connectioninfo.version", v0_2, 2131493280);
        com.teamspeak.ts3client.data.e.a.a("connectioninfo.platform", v0_2, 2131493282);
        com.teamspeak.ts3client.data.e.a.a("connectioninfo.license", v0_2, 2131493284);
        com.teamspeak.ts3client.data.e.a.a("connectioninfo.uptime", v0_2, 2131493286);
        com.teamspeak.ts3client.data.e.a.a("connectioninfo.address", v0_2, 2131493288);
        com.teamspeak.ts3client.data.e.a.a("connectioninfo.ip", v0_2, 2131493290);
        com.teamspeak.ts3client.data.e.a.a("connectioninfo.ping", v0_2, 2131493292);
        com.teamspeak.ts3client.data.e.a.a("connectioninfo.packetloss", v0_2, 2131493294);
        com.teamspeak.ts3client.data.e.a.a("connectioninfo.packettrans", v0_2, 2131493298);
        com.teamspeak.ts3client.data.e.a.a("connectioninfo.bytetrans", v0_2, 2131493301);
        com.teamspeak.ts3client.data.e.a.a("connectioninfo.bandwidthsec", v0_2, 2131493304);
        com.teamspeak.ts3client.data.e.a.a("connectioninfo.bandwidthmin", v0_2, 2131493307);
        com.teamspeak.ts3client.data.e.a.a("connectioninfo.filebandwidth", v0_2, 2131493310);
        com.teamspeak.ts3client.data.e.a.a("connectioninfo.filebyte", v0_2, 2131493313);
        com.teamspeak.ts3client.data.e.a.a("connectioninfo.tablein", v0_2, 2131493296);
        com.teamspeak.ts3client.data.e.a.a("connectioninfo.tableout", v0_2, 2131493297);
        return v0_2;
    }

    public final void a(android.os.Bundle p1)
    {
        super.a(p1);
        return;
    }

    public final void a(com.teamspeak.ts3client.jni.k p4)
    {
        if (((p4 instanceof com.teamspeak.ts3client.jni.events.ServerUpdated)) && (this.l())) {
            this.i().runOnUiThread(new com.teamspeak.ts3client.d.ab(this));
        }
        if (((p4 instanceof com.teamspeak.ts3client.jni.events.ServerConnectionInfo)) && (this.l())) {
            this.i().runOnUiThread(new com.teamspeak.ts3client.d.ac(this, new java.text.DecimalFormat("0.00%")));
        }
        if (((p4 instanceof com.teamspeak.ts3client.jni.events.rare.ServerPermissionError)) && (((com.teamspeak.ts3client.jni.events.rare.ServerPermissionError) p4).a == this.aD.a.u.a(com.teamspeak.ts3client.jni.g.x))) {
            this.i().runOnUiThread(new com.teamspeak.ts3client.d.ad(this));
        }
        return;
    }

    public final void b()
    {
        super.b();
        return;
    }

    public final void e()
    {
        super.e();
        com.teamspeak.ts3client.data.d.q.a(this);
        return;
    }

    public final void f()
    {
        super.f();
        com.teamspeak.ts3client.data.d.q.b(this);
        return;
    }

    public final void s()
    {
        super.s();
        Thread v0_3 = this.aD.a.a.b(this.aD.a.e, com.teamspeak.ts3client.jni.i.e);
        com.teamspeak.ts3client.d.aa v1_4 = com.teamspeak.ts3client.d.z.at.matcher(v0_3);
        if ((v1_4.find()) && (Long.parseLong(v1_4.group(2)) > 20000)) {
            v0_3 = v0_3.replace(v1_4.group(1), new StringBuilder("(").append(java.text.DateFormat.getDateTimeInstance(2, 2).format(new java.util.Date((Long.parseLong(v1_4.group(2)) * 1000)))).append(")").toString());
        }
        Thread v0_10;
        this.av.setText(v0_3);
        this.aw.setText(this.aD.a.a.b(this.aD.a.e, com.teamspeak.ts3client.jni.i.d));
        com.teamspeak.ts3client.d.aa v1_15 = this.ax;
        switch (this.aD.a.a.ts3client_getServerLicenseType(this.aD.a.e)) {
            case 0:
                v0_10 = com.teamspeak.ts3client.data.e.a.a("license.no");
                break;
            case 1:
                v0_10 = com.teamspeak.ts3client.data.e.a.a("license.licensed");
                break;
            case 2:
                v0_10 = com.teamspeak.ts3client.data.e.a.a("license.offline");
                break;
            case 3:
                v0_10 = com.teamspeak.ts3client.data.e.a.a("license.npl");
                break;
            default:
                v0_10 = "Error";
        }
        v1_15.setText(v0_10);
        this.aA.setText(new StringBuilder().append(this.aD.a.v.o).append(":").append(this.aD.a.v.p).toString());
        this.az.setText(this.aD.a.v.c);
        this.aD.a.c.a(this);
        this.aD.a.a.ts3client_requestServerVariables(this.aD.a.e);
        this.aF = 1;
        this.aE = new Thread(new com.teamspeak.ts3client.d.aa(this));
        this.aE.start();
        return;
    }

    public final void t()
    {
        super.t();
        this.aD.a.c.b(this);
        this.aF = 0;
        this.aE.interrupt();
        return;
    }
}
