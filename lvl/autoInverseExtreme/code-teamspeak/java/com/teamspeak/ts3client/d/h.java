package com.teamspeak.ts3client.d;
public final class h extends android.support.v4.app.ax {
    private android.text.Spanned aA;
    private android.widget.TextView aB;
    private boolean aC;
    public boolean at;
    private android.widget.TextView au;
    private com.teamspeak.ts3client.Ts3Application av;
    private android.widget.Button aw;
    private String ax;
    private String ay;
    private String az;

    public h(android.content.Context p3, android.text.Spanned p4, String p5, String p6)
    {
        this.at = 0;
        this.ax = "";
        this.ay = "";
        this.az = "";
        this.aA = 0;
        this.aC = 0;
        this.au = new android.widget.TextView(p3);
        this.aw = new android.widget.Button(p3);
        this.aw.setVisibility(8);
        this.aA = p4;
        this.ay = p5;
        this.az = p6;
        return;
    }

    public h(android.content.Context p3, String p4, String p5, String p6)
    {
        this.at = 0;
        this.ax = "";
        this.ay = "";
        this.az = "";
        this.aA = 0;
        this.aC = 0;
        this.au = new android.widget.TextView(p3);
        this.aw = new android.widget.Button(p3);
        this.aw.setVisibility(8);
        this.ax = p4;
        this.ay = p5;
        this.az = p6;
        return;
    }

    public h(android.content.Context p3, String p4, String p5, String p6, byte p7)
    {
        this.at = 0;
        this.ax = "";
        this.ay = "";
        this.az = "";
        this.aA = 0;
        this.aC = 0;
        this.au = new android.widget.TextView(p3);
        this.aw = new android.widget.Button(p3);
        this.ax = p4;
        this.ay = p5;
        this.az = p6;
        this.aC = 1;
        return;
    }

    static synthetic void a(com.teamspeak.ts3client.d.h p2)
    {
        if (((p2.at) || (p2.aC)) && (p2.av.a != null)) {
            p2.av.a.k.d(0);
        }
        if (p2.aC) {
            p2.av.l.cancelAll();
        }
        return;
    }

    static synthetic void b(com.teamspeak.ts3client.d.h p0)
    {
        p0.b();
        return;
    }

    private void d(boolean p1)
    {
        this.at = p1;
        return;
    }

    private void y()
    {
        if (((this.at) || (this.aC)) && (this.av.a != null)) {
            this.av.a.k.d(0);
        }
        if (this.aC) {
            this.av.l.cancelAll();
        }
        return;
    }

    private void z()
    {
        this.b();
        return;
    }

    public final android.view.View a(android.view.LayoutInflater p10, android.view.ViewGroup p11)
    {
        this.j.requestWindowFeature(1);
        this.av = ((com.teamspeak.ts3client.Ts3Application) this.i().getApplicationContext());
        android.widget.RelativeLayout v0_5 = new android.widget.RelativeLayout(p10.getContext());
        v0_5.setLayoutParams(new android.widget.RelativeLayout$LayoutParams(-1, -2));
        this.aB = new android.widget.TextView(this.i());
        this.aB.setId(10);
        this.aB.setBackgroundDrawable(this.i().getResources().getDrawable(2130837577));
        this.aB.setTextAppearance(this.i(), 2131165363);
        this.aB.setSingleLine(1);
        this.aB.setText(this.ay);
        this.aB.setEllipsize(android.text.TextUtils$TruncateAt.END);
        this.aB.setPadding(15, 15, 15, 15);
        android.widget.RelativeLayout$LayoutParams v1_13 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        v1_13.addRule(10);
        v0_5.addView(this.aB, v1_13);
        this.au.setId(1);
        this.au.setTypeface(0, 1);
        this.au.setTextSize(1098907648);
        if (this.aA == null) {
            this.au.setText(this.ax);
        } else {
            this.au.setText(this.aA);
            this.au.setMovementMethod(com.teamspeak.ts3client.data.d.n.a());
        }
        this.au.setPadding(5, 5, 5, 5);
        android.widget.RelativeLayout$LayoutParams v1_23 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        v1_23.addRule(3, this.aB.getId());
        v0_5.addView(this.au, v1_23);
        android.widget.RelativeLayout$LayoutParams v1_25 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        v1_25.addRule(3, this.au.getId());
        this.aw.setText(this.az);
        this.aw.setOnClickListener(new com.teamspeak.ts3client.d.i(this, v0_5));
        v0_5.addView(this.aw, v1_25);
        return v0_5;
    }

    public final void b(String p3)
    {
        this.au.setText(p3);
        this.au.setTextColor(-65536);
        this.aw.setVisibility(0);
        return;
    }

    public final void c(String p3)
    {
        this.au.setText(p3);
        if (this.at) {
            this.aw.setVisibility(0);
        }
        return;
    }

    public final void e()
    {
        super.e();
        com.teamspeak.ts3client.data.d.q.a(this);
        return;
    }

    public final void f()
    {
        super.f();
        com.teamspeak.ts3client.data.d.q.b(this);
        return;
    }
}
