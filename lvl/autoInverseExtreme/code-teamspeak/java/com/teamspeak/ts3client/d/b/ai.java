package com.teamspeak.ts3client.d.b;
final class ai implements android.view.View$OnClickListener {
    final synthetic com.teamspeak.ts3client.Ts3Application a;
    final synthetic android.widget.RelativeLayout b;
    final synthetic com.teamspeak.ts3client.d.b.ah c;

    ai(com.teamspeak.ts3client.d.b.ah p1, com.teamspeak.ts3client.Ts3Application p2, android.widget.RelativeLayout p3)
    {
        this.c = p1;
        this.a = p2;
        this.b = p3;
        return;
    }

    public final void onClick(android.view.View p8)
    {
        p8.performHapticFeedback(1);
        this.a.a.a.ts3client_requestClientPoke(this.a.a.e, com.teamspeak.ts3client.d.b.ah.a(this.c).c, com.teamspeak.ts3client.d.b.ah.b(this.c).getText().toString(), new StringBuilder("Poke ").append(com.teamspeak.ts3client.d.b.ah.a(this.c).c).toString());
        this.c.b();
        this.b.removeAllViews();
        return;
    }
}
