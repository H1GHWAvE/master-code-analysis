package com.teamspeak.ts3client.customs;
final class c implements android.widget.AdapterView$OnItemSelectedListener {
    final synthetic com.teamspeak.ts3client.customs.CustomCodecSettings a;

    c(com.teamspeak.ts3client.customs.CustomCodecSettings p1)
    {
        this.a = p1;
        return;
    }

    public final void onItemSelected(android.widget.AdapterView p7, android.view.View p8, int p9, long p10)
    {
        if (!com.teamspeak.ts3client.customs.CustomCodecSettings.a(this.a)) {
            com.teamspeak.ts3client.data.d.ab v0_5 = com.teamspeak.ts3client.data.d.y.a(p9, com.teamspeak.ts3client.customs.CustomCodecSettings.d(this.a).getProgress(), com.teamspeak.ts3client.customs.CustomCodecSettings.b(this.a), com.teamspeak.ts3client.customs.CustomCodecSettings.c(this.a));
            com.teamspeak.ts3client.customs.CustomCodecSettings.a(this.a, 1);
            switch (com.teamspeak.ts3client.customs.e.a[v0_5.ordinal()]) {
                case 1:
                    com.teamspeak.ts3client.customs.CustomCodecSettings.h(this.a).setSelection(0);
                    break;
                case 2:
                    com.teamspeak.ts3client.customs.CustomCodecSettings.h(this.a).setSelection(1);
                    break;
                case 3:
                    com.teamspeak.ts3client.customs.CustomCodecSettings.h(this.a).setSelection(2);
                    break;
                case 4:
                    com.teamspeak.ts3client.customs.CustomCodecSettings.h(this.a).setSelection(3);
                    break;
                default:
                    com.teamspeak.ts3client.customs.CustomCodecSettings.h(this.a).setSelection(3);
            }
            com.teamspeak.ts3client.customs.CustomCodecSettings.a(this.a, 0);
            com.teamspeak.ts3client.customs.CustomCodecSettings.g(this.a);
            com.teamspeak.ts3client.customs.CustomCodecSettings.f(this.a).a = p9;
        }
        return;
    }

    public final void onNothingSelected(android.widget.AdapterView p1)
    {
        return;
    }
}
