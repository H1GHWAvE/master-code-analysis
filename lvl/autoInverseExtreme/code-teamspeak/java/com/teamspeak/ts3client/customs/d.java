package com.teamspeak.ts3client.customs;
final class d implements android.widget.SeekBar$OnSeekBarChangeListener {
    final synthetic com.teamspeak.ts3client.customs.CustomCodecSettings a;

    d(com.teamspeak.ts3client.customs.CustomCodecSettings p1)
    {
        this.a = p1;
        return;
    }

    public final void onProgressChanged(android.widget.SeekBar p7, int p8, boolean p9)
    {
        if ((com.teamspeak.ts3client.customs.CustomCodecSettings.c(this.a) >= p8) || (!p9)) {
            com.teamspeak.ts3client.customs.CustomCodecSettings.k(this.a).setText(String.valueOf(p8));
            if (!com.teamspeak.ts3client.customs.CustomCodecSettings.a(this.a)) {
                com.teamspeak.ts3client.data.d.ab v0_9 = com.teamspeak.ts3client.data.d.y.a(com.teamspeak.ts3client.customs.CustomCodecSettings.e(this.a).getSelectedItemPosition(), p8, com.teamspeak.ts3client.customs.CustomCodecSettings.b(this.a), com.teamspeak.ts3client.customs.CustomCodecSettings.c(this.a));
                com.teamspeak.ts3client.customs.CustomCodecSettings.a(this.a, 1);
                switch (com.teamspeak.ts3client.customs.e.a[v0_9.ordinal()]) {
                    case 1:
                        com.teamspeak.ts3client.customs.CustomCodecSettings.h(this.a).setSelection(0);
                        break;
                    case 2:
                        com.teamspeak.ts3client.customs.CustomCodecSettings.h(this.a).setSelection(1);
                        break;
                    case 3:
                        com.teamspeak.ts3client.customs.CustomCodecSettings.h(this.a).setSelection(2);
                        break;
                    case 4:
                        com.teamspeak.ts3client.customs.CustomCodecSettings.h(this.a).setSelection(3);
                        break;
                    default:
                        com.teamspeak.ts3client.customs.CustomCodecSettings.h(this.a).setSelection(3);
                }
                com.teamspeak.ts3client.customs.CustomCodecSettings.a(this.a, 0);
                com.teamspeak.ts3client.customs.CustomCodecSettings.g(this.a);
                com.teamspeak.ts3client.customs.CustomCodecSettings.f(this.a).b = p8;
            }
        } else {
            if (!com.teamspeak.ts3client.customs.CustomCodecSettings.i(this.a)) {
                p7.setProgress(com.teamspeak.ts3client.customs.CustomCodecSettings.j(this.a));
            } else {
                p7.setProgress(com.teamspeak.ts3client.customs.CustomCodecSettings.c(this.a));
            }
        }
        return;
    }

    public final void onStartTrackingTouch(android.widget.SeekBar p1)
    {
        p1.requestFocus();
        return;
    }

    public final void onStopTrackingTouch(android.widget.SeekBar p1)
    {
        p1.requestFocus();
        return;
    }
}
