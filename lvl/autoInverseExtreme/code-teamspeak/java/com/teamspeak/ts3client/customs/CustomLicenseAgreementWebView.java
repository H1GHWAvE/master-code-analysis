package com.teamspeak.ts3client.customs;
public class CustomLicenseAgreementWebView extends android.webkit.WebView {
    private com.teamspeak.ts3client.customs.f a;

    public CustomLicenseAgreementWebView(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    public CustomLicenseAgreementWebView(android.content.Context p2, android.util.AttributeSet p3)
    {
        this(p2, p3, 0);
        return;
    }

    public CustomLicenseAgreementWebView(android.content.Context p1, android.util.AttributeSet p2, int p3)
    {
        this(p1, p2, p3);
        return;
    }

    protected void onScrollChanged(int p3, int p4, int p5, int p6)
    {
        if ((this.a != null) && (((((float) this.getContentHeight()) * this.getScale()) - ((float) (this.getHeight() + p4))) < 1084227584)) {
            this.a.a();
        }
        super.onScrollChanged(p3, p4, p5, p6);
        return;
    }

    public void setOnBottomReachedListener(com.teamspeak.ts3client.customs.f p1)
    {
        this.a = p1;
        return;
    }
}
