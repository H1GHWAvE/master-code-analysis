package com.teamspeak.ts3client;
public final class h extends android.support.v4.app.ax implements com.teamspeak.ts3client.data.d.s, com.teamspeak.ts3client.data.w {
    private static com.teamspeak.ts3client.h at;
    private static java.util.regex.Pattern au;
    private static com.teamspeak.ts3client.data.c av;
    private android.widget.TableLayout aA;
    private android.support.v4.app.bi aB;
    private com.teamspeak.ts3client.d.b.a aC;
    private android.widget.TextView aD;
    private int aE;
    private android.widget.LinearLayout aF;
    private android.content.Context aG;
    private android.widget.TextView aH;
    private android.widget.TableRow aI;
    private com.teamspeak.ts3client.customs.FloatingButton aJ;
    private com.teamspeak.ts3client.Ts3Application aw;
    private android.widget.TextView ax;
    private android.widget.TextView ay;
    private android.widget.TableLayout az;

    public h()
    {
        return;
    }

    static synthetic java.util.regex.Pattern A()
    {
        return com.teamspeak.ts3client.h.au;
    }

    private int B()
    {
        return ((int) ((this.aw.getResources().getDisplayMetrics().density * 1098907648) + 1056964608));
    }

    private void C()
    {
        if (this.l()) {
            this.i().runOnUiThread(new com.teamspeak.ts3client.j(this));
        }
        return;
    }

    static synthetic android.support.v4.app.bi a(com.teamspeak.ts3client.h p1)
    {
        return p1.aB;
    }

    static synthetic com.teamspeak.ts3client.d.b.a a(com.teamspeak.ts3client.h p0, com.teamspeak.ts3client.d.b.a p1)
    {
        p0.aC = p1;
        return p1;
    }

    public static com.teamspeak.ts3client.h a(com.teamspeak.ts3client.data.c p1)
    {
        com.teamspeak.ts3client.h v0_5;
        if (com.teamspeak.ts3client.h.av != p1) {
            com.teamspeak.ts3client.h.at = new com.teamspeak.ts3client.h();
            com.teamspeak.ts3client.h.av = p1;
            com.teamspeak.ts3client.h.au = java.util.regex.Pattern.compile(".*(\\[Build: (\\d+)\\]).*");
            v0_5 = com.teamspeak.ts3client.h.at;
        } else {
            v0_5 = com.teamspeak.ts3client.h.at;
        }
        return v0_5;
    }

    private void a(android.widget.TableLayout p4, String p5, android.graphics.drawable.Drawable p6)
    {
        android.widget.TableRow v0_3 = ((android.widget.TableRow) android.view.LayoutInflater.from(this.i()).inflate(2130903087, 0));
        if (p6 != null) {
            android.widget.TextView v1_3 = ((android.widget.ImageView) v0_3.findViewById(2131493210));
            v1_3.setMinimumHeight(this.B());
            v1_3.setMinimumWidth(this.B());
            v1_3.setImageDrawable(p6);
        }
        ((android.widget.TextView) v0_3.findViewById(2131493211)).setText(p5);
        p4.addView(v0_3);
        return;
    }

    static synthetic void a(com.teamspeak.ts3client.h p3, android.widget.TableLayout p4, String p5, android.graphics.drawable.Drawable p6)
    {
        android.widget.TableRow v0_3 = ((android.widget.TableRow) android.view.LayoutInflater.from(p3.i()).inflate(2130903087, 0));
        if (p6 != null) {
            android.widget.TextView v1_3 = ((android.widget.ImageView) v0_3.findViewById(2131493210));
            v1_3.setMinimumHeight(p3.B());
            v1_3.setMinimumWidth(p3.B());
            v1_3.setImageDrawable(p6);
        }
        ((android.widget.TextView) v0_3.findViewById(2131493211)).setText(p5);
        p4.addView(v0_3);
        return;
    }

    static synthetic com.teamspeak.ts3client.d.b.a b(com.teamspeak.ts3client.h p1)
    {
        return p1.aC;
    }

    static synthetic android.widget.TextView c(com.teamspeak.ts3client.h p1)
    {
        return p1.ax;
    }

    static synthetic int d(com.teamspeak.ts3client.h p1)
    {
        return p1.aE;
    }

    static synthetic com.teamspeak.ts3client.Ts3Application e(com.teamspeak.ts3client.h p1)
    {
        return p1.aw;
    }

    static synthetic android.widget.TableRow f(com.teamspeak.ts3client.h p1)
    {
        return p1.aI;
    }

    static synthetic android.widget.TextView g(com.teamspeak.ts3client.h p1)
    {
        return p1.aH;
    }

    static synthetic android.widget.TextView h(com.teamspeak.ts3client.h p1)
    {
        return p1.ay;
    }

    static synthetic android.widget.TextView i(com.teamspeak.ts3client.h p1)
    {
        return p1.aD;
    }

    static synthetic android.widget.TableLayout j(com.teamspeak.ts3client.h p1)
    {
        return p1.az;
    }

    static synthetic android.widget.TableLayout k(com.teamspeak.ts3client.h p1)
    {
        return p1.aA;
    }

    static synthetic android.widget.LinearLayout l(com.teamspeak.ts3client.h p1)
    {
        return p1.aF;
    }

    static synthetic android.content.Context m(com.teamspeak.ts3client.h p1)
    {
        return p1.aG;
    }

    static synthetic com.teamspeak.ts3client.data.c z()
    {
        return com.teamspeak.ts3client.h.av;
    }

    public final android.view.View a(android.view.LayoutInflater p5, android.view.ViewGroup p6)
    {
        android.view.View v1_1 = p5.inflate(2130903086, p6, 0);
        this.aG = v1_1.getContext();
        this.aB = this.M;
        this.ax = ((android.widget.TextView) v1_1.findViewById(2131493197));
        this.ay = ((android.widget.TextView) v1_1.findViewById(2131493201));
        this.aD = ((android.widget.TextView) v1_1.findViewById(2131493203));
        this.az = ((android.widget.TableLayout) v1_1.findViewById(2131493205));
        this.aA = ((android.widget.TableLayout) v1_1.findViewById(2131493207));
        this.aF = ((android.widget.LinearLayout) v1_1.findViewById(2131493208));
        this.aJ = ((com.teamspeak.ts3client.customs.FloatingButton) v1_1.findViewById(2131493209));
        this.aJ.setBitmap(com.teamspeak.ts3client.data.d.t.a(2130837621, 1102053376, 1102053376));
        this.aH = ((android.widget.TextView) v1_1.findViewById(2131493199));
        this.aI = ((android.widget.TableRow) v1_1.findViewById(2131493025));
        com.teamspeak.ts3client.data.e.a.a("clientinfo.nickname", v1_1, 2131493196);
        com.teamspeak.ts3client.data.e.a.a("clientinfo.version", v1_1, 2131493200);
        com.teamspeak.ts3client.data.e.a.a("clientinfo.connected", v1_1, 2131493202);
        com.teamspeak.ts3client.data.e.a.a("clientinfo.sgroup", v1_1, 2131493204);
        com.teamspeak.ts3client.data.e.a.a("clientinfo.cgroup", v1_1, 2131493206);
        com.teamspeak.ts3client.data.e.a.a("clientinfo.description", v1_1, 2131493198);
        this.n();
        this.aw.o.b(com.teamspeak.ts3client.data.e.a.a("dialog.client.info.text"));
        this.aJ.setOnClickListener(new com.teamspeak.ts3client.i(this));
        return v1_1;
    }

    public final void a(android.os.Bundle p3)
    {
        super.a(p3);
        this.aw = ((com.teamspeak.ts3client.Ts3Application) this.i().getApplicationContext());
        this.aE = new android.widget.TextView(this.i().getBaseContext()).getTextColors().getDefaultColor();
        return;
    }

    public final void a(android.view.Menu p1, android.view.MenuInflater p2)
    {
        p1.clear();
        super.a(p1, p2);
        return;
    }

    public final void a(com.teamspeak.ts3client.jni.k p3)
    {
        if (((p3 instanceof com.teamspeak.ts3client.jni.events.UpdateClient)) && (((com.teamspeak.ts3client.jni.events.UpdateClient) p3).a == com.teamspeak.ts3client.h.av.c)) {
            if (this.l()) {
                this.i().runOnUiThread(new com.teamspeak.ts3client.j(this));
            }
            this.aw.a.c.b(this);
        }
        return;
    }

    public final void c(android.os.Bundle p6)
    {
        super.c(p6);
        if (this.aw.a != null) {
            this.aw.a.c.a(this);
            if (com.teamspeak.ts3client.h.av != null) {
                this.aw.a.a.ts3client_requestClientVariables(this.aw.a.e, com.teamspeak.ts3client.h.av.c, "ClientInfoFragment");
                if (com.teamspeak.ts3client.h.av.w == null) {
                    if (com.teamspeak.ts3client.h.av.y == null) {
                        com.teamspeak.ts3client.h.av.b();
                        if ((com.teamspeak.ts3client.h.av.b() != null) && (!com.teamspeak.ts3client.h.av.b().equals(""))) {
                            com.teamspeak.ts3client.h.av.a(this);
                        }
                    }
                } else {
                    if ((!com.teamspeak.ts3client.h.av.w.k) && (com.teamspeak.ts3client.h.av.y == null)) {
                        com.teamspeak.ts3client.h.av.b();
                        if (!com.teamspeak.ts3client.h.av.b().equals("")) {
                            com.teamspeak.ts3client.h.av.a(this);
                        }
                    }
                }
            }
            this.aw.c = this;
        }
        return;
    }

    public final void y()
    {
        if (this.l()) {
            this.i().runOnUiThread(new com.teamspeak.ts3client.k(this));
        }
        return;
    }
}
