package com.teamspeak.ts3client;
final class bf implements android.content.SharedPreferences$OnSharedPreferenceChangeListener {
    private com.teamspeak.ts3client.Ts3Application a;

    public bf(com.teamspeak.ts3client.Ts3Application p1)
    {
        this.a = p1;
        return;
    }

    public final void onSharedPreferenceChanged(android.content.SharedPreferences p3, String p4)
    {
        if (p4.equals("lock_rotation")) {
            if (!p3.getBoolean("lock_rotation", 0)) {
                this.a.f.setRequestedOrientation(5);
            } else {
                this.a.f.setRequestedOrientation(4);
            }
        }
        return;
    }
}
