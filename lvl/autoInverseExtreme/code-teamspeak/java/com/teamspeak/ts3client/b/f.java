package com.teamspeak.ts3client.b;
public final class f extends android.widget.ArrayAdapter {
    com.teamspeak.ts3client.b.j a;
    final synthetic com.teamspeak.ts3client.b.b b;
    private android.view.LayoutInflater c;

    public f(com.teamspeak.ts3client.b.b p2, android.content.Context p3)
    {
        this.b = p2;
        this(p3, 17367043);
        this.c = android.view.LayoutInflater.from(p3);
        return;
    }

    private android.widget.TableRow a(String p5, String p6)
    {
        android.widget.TableRow v0_1 = new android.widget.TableRow(this.getContext());
        android.widget.TextView v1_2 = new android.widget.TextView(this.getContext());
        v1_2.setText(p5);
        v1_2.setPadding(0, 0, 5, 0);
        v1_2.setTextAppearance(this.getContext(), 2131165314);
        android.widget.TextView v2_4 = new android.widget.TextView(this.getContext());
        v2_4.setText(p6);
        v0_1.addView(v1_2);
        v0_1.addView(v2_4);
        return v0_1;
    }

    static synthetic void a(com.teamspeak.ts3client.b.f p0, com.teamspeak.ts3client.jni.events.rare.BanList p1, android.widget.TableLayout p2)
    {
        p0.a(p1, p2);
        return;
    }

    private void a(com.teamspeak.ts3client.jni.events.rare.BanList p3, android.widget.TableLayout p4)
    {
        if (!p3.c.equals("")) {
            p4.addView(this.a("Name", p3.c));
        }
        if (!p3.b.equals("")) {
            p4.addView(this.a("IP", p3.b));
        }
        if (!p3.d.equals("")) {
            p4.addView(this.a("Uid", p3.d));
        }
        if (!p3.h.equals("")) {
            p4.addView(this.a("Nick", p3.h));
        }
        p4.addView(this.a(com.teamspeak.ts3client.data.e.a.a("banlist.creator"), p3.e));
        return;
    }

    public final android.widget.Filter getFilter()
    {
        if (this.a == null) {
            this.a = new com.teamspeak.ts3client.b.j(this);
        }
        return this.a;
    }

    public final android.view.View getView(int p7, android.view.View p8, android.view.ViewGroup p9)
    {
        android.view.View v4 = this.c.inflate(2130903067, 0);
        int v1_3 = ((android.widget.TextView) v4.findViewById(2131492998));
        android.widget.TextView v2_3 = ((android.widget.TextView) v4.findViewById(2131493000));
        com.teamspeak.ts3client.jni.events.rare.BanList v3_1 = ((com.teamspeak.ts3client.jni.events.rare.BanList) this.getItem(p7));
        this.a(v3_1, ((android.widget.TableLayout) v4.findViewById(2131492999)));
        v1_3.setText(new StringBuilder().append(com.teamspeak.ts3client.data.e.a.a("banlist.reason")).append(v3_1.g).toString());
        v1_3.setTextAppearance(this.getContext(), 2131165315);
        v2_3.setText(new StringBuilder().append(com.teamspeak.ts3client.data.e.a.a("banlist.created")).append("\n").append(v3_1.k).append("\n").append(com.teamspeak.ts3client.data.e.a.a("banlist.expires")).append("\n").append(v3_1.j).toString());
        v2_3.setTextAppearance(this.getContext(), 2131165313);
        v4.setOnLongClickListener(new com.teamspeak.ts3client.b.g(this, v3_1));
        return v4;
    }
}
