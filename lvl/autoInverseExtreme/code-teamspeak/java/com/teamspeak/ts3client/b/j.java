package com.teamspeak.ts3client.b;
public final class j extends android.widget.Filter {
    java.util.Vector a;
    final synthetic com.teamspeak.ts3client.b.f b;

    public j(com.teamspeak.ts3client.b.f p2)
    {
        this.b = p2;
        this.a = new java.util.Vector();
        return;
    }

    protected final android.widget.Filter$FilterResults performFiltering(CharSequence p5)
    {
        android.widget.Filter$FilterResults v1_1 = new android.widget.Filter$FilterResults();
        if ((p5 == null) || (p5.toString().length() <= 0)) {
            v1_1.count = com.teamspeak.ts3client.b.b.b(this.b.b).size();
            v1_1.values = com.teamspeak.ts3client.b.b.b(this.b.b);
        } else {
            this.a = new java.util.Vector();
            java.util.Iterator v2 = com.teamspeak.ts3client.b.b.b(this.b.b).iterator();
            while (v2.hasNext()) {
                java.util.Vector v0_19 = ((com.teamspeak.ts3client.jni.events.rare.BanList) v2.next());
                if (v0_19.i.contains(p5)) {
                    this.a.add(v0_19);
                }
            }
            v1_1.count = this.a.size();
            v1_1.values = this.a;
        }
        return v1_1;
    }

    protected final void publishResults(CharSequence p3, android.widget.Filter$FilterResults p4)
    {
        com.teamspeak.ts3client.Ts3Application.a().c.i().runOnUiThread(new com.teamspeak.ts3client.b.k(this));
        return;
    }
}
