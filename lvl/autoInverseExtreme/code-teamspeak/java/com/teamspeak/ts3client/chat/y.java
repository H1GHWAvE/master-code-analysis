package com.teamspeak.ts3client.chat;
public final class y {
    String a;
    String b;
    android.text.Spanned c;
    boolean d;
    java.util.Date e;
    boolean f;
    private String g;

    public y(String p7, String p8, String p9, Boolean p10)
    {
        this(p7, p8, p9, p10, Boolean.valueOf(0));
        return;
    }

    public y(String p3, String p4, String p5, Boolean p6, Boolean p7)
    {
        this.d = 0;
        this.f = 0;
        if (p7.booleanValue()) {
            p5 = new StringBuilder("*** ").append(p5).toString();
        }
        if (p3 != null) {
            this.a = com.teamspeak.ts3client.data.d.x.a(p3);
        }
        this.g = p4;
        this.b = com.teamspeak.ts3client.data.d.x.a(p5);
        this.c = com.teamspeak.ts3client.data.d.a.a(this.b);
        this.e = new java.util.Date();
        this.d = p6.booleanValue();
        this.f = p7.booleanValue();
        return;
    }

    private android.text.Spanned b()
    {
        return this.c;
    }

    private String c()
    {
        return this.a;
    }

    private String d()
    {
        return this.g;
    }

    private String e()
    {
        return this.b;
    }

    private java.util.Date f()
    {
        return this.e;
    }

    private boolean g()
    {
        return this.f;
    }

    public final boolean a()
    {
        return this.d;
    }
}
