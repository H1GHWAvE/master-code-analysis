package com.teamspeak.ts3client.e;
public final class e extends android.widget.BaseAdapter {
    java.util.ArrayList a;
    private android.content.Context b;
    private android.view.LayoutInflater c;

    public e(android.content.Context p2)
    {
        this.b = p2;
        this.a = new java.util.ArrayList();
        this.c = android.view.LayoutInflater.from(p2);
        return;
    }

    static synthetic android.content.Context a(com.teamspeak.ts3client.e.e p1)
    {
        return p1.b;
    }

    private void a(com.teamspeak.ts3client.e.a p2)
    {
        if (!this.a.contains(p2)) {
            this.a.add(p2);
        }
        return;
    }

    static synthetic java.util.ArrayList b(com.teamspeak.ts3client.e.e p1)
    {
        return p1.a;
    }

    private void b(com.teamspeak.ts3client.e.a p2)
    {
        if (!this.a.contains(p2)) {
            this.a.remove(p2);
        }
        return;
    }

    public final int getCount()
    {
        return this.a.size();
    }

    public final Object getItem(int p2)
    {
        return this.a.get(p2);
    }

    public final long getItemId(int p3)
    {
        return 0;
    }

    public final android.view.View getView(int p7, android.view.View p8, android.view.ViewGroup p9)
    {
        android.view.View v4 = this.c.inflate(2130903106, 0);
        android.widget.TextView v0_3 = ((android.widget.TextView) v4.findViewById(2131493256));
        String v1_3 = ((android.widget.ImageView) v4.findViewById(2131493257));
        com.teamspeak.ts3client.e.i v2_3 = ((android.widget.ImageView) v4.findViewById(2131493258));
        if (!((com.teamspeak.ts3client.e.a) this.a.get(p7)).e) {
            v2_3.setOnClickListener(new com.teamspeak.ts3client.e.f(this, p7));
        } else {
            v2_3.setColorFilter(-8947849, android.graphics.PorterDuff$Mode.MULTIPLY);
        }
        if (!((com.teamspeak.ts3client.e.a) this.a.get(p7)).e) {
            v1_3.setOnClickListener(new com.teamspeak.ts3client.e.i(this, p7));
        } else {
            v1_3.setColorFilter(-8947849, android.graphics.PorterDuff$Mode.MULTIPLY);
        }
        v0_3.setText(((com.teamspeak.ts3client.e.a) this.a.get(p7)).b);
        return v4;
    }
}
