package com.teamspeak.ts3client.e;
public final class b extends android.support.v4.app.Fragment implements com.teamspeak.ts3client.data.b.e {
    private com.teamspeak.ts3client.Ts3Application a;
    private com.teamspeak.ts3client.data.b.f b;
    private com.teamspeak.ts3client.e.e c;
    private android.widget.ListView d;
    private com.teamspeak.ts3client.customs.FloatingButton e;

    public b()
    {
        return;
    }

    public final android.view.View a(android.view.LayoutInflater p5, android.view.ViewGroup p6)
    {
        this.a = ((com.teamspeak.ts3client.Ts3Application) this.i().getApplicationContext());
        this.n();
        this.a.o.b(com.teamspeak.ts3client.data.e.a.a("menu.identity"));
        this.a.c = this;
        android.view.View v1_3 = p5.inflate(2130903105, p6, 0);
        this.d = ((android.widget.ListView) v1_3.findViewById(2131493254));
        this.e = ((com.teamspeak.ts3client.customs.FloatingButton) v1_3.findViewById(2131493255));
        this.e.setDrawable(this.j().getDrawable(2130837630));
        this.e.setOnClickListener(new com.teamspeak.ts3client.e.c(this, p5));
        return v1_3;
    }

    public final void a()
    {
        this.b();
        return;
    }

    public final void a(android.os.Bundle p2)
    {
        super.a(p2);
        this.b = com.teamspeak.ts3client.data.b.f.a();
        com.teamspeak.ts3client.data.b.f.a(this);
        return;
    }

    public final void a(android.view.Menu p1, android.view.MenuInflater p2)
    {
        p1.clear();
        super.a(p1, p2);
        return;
    }

    public final void b()
    {
        java.util.ArrayList v2;
        this.c = new com.teamspeak.ts3client.e.e(this.i());
        int v0_3 = this.b.c();
        if ((v0_3 != 0) && (v0_3.size() != 0)) {
            v2 = v0_3;
        } else {
            this.b.b();
            v2 = this.b.c();
        }
        if (v2 != null) {
            int v1_2 = 0;
            while (v1_2 < v2.size()) {
                java.util.ArrayList v3_0 = this.c;
                int v0_12 = ((com.teamspeak.ts3client.e.a) v2.get(v1_2));
                if (!v3_0.a.contains(v0_12)) {
                    v3_0.a.add(v0_12);
                }
                v1_2++;
            }
        }
        this.d.setAdapter(this.c);
        this.c.notifyDataSetChanged();
        return;
    }

    public final void c(android.os.Bundle p1)
    {
        super.c(p1);
        this.b();
        return;
    }
}
