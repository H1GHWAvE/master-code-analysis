package com.teamspeak.ts3client.bookmark;
public final class c extends android.widget.BaseAdapter {
    java.util.ArrayList a;
    private android.content.Context b;
    private android.view.LayoutInflater c;
    private android.widget.ImageView d;

    public c(android.content.Context p2)
    {
        this.b = p2;
        this.a = new java.util.ArrayList();
        this.c = android.view.LayoutInflater.from(p2);
        return;
    }

    private void a(com.teamspeak.ts3client.data.ab p2)
    {
        if (!this.a.contains(p2)) {
            this.a.add(p2);
        }
        return;
    }

    private void b(com.teamspeak.ts3client.data.ab p2)
    {
        if (!this.a.contains(p2)) {
            this.a.remove(p2);
        }
        return;
    }

    public final int getCount()
    {
        return this.a.size();
    }

    public final Object getItem(int p2)
    {
        return this.a.get(p2);
    }

    public final long getItemId(int p3)
    {
        return 0;
    }

    public final android.view.View getView(int p5, android.view.View p6, android.view.ViewGroup p7)
    {
        android.widget.TextView v1_0;
        if (p6 != null) {
            v1_0 = ((android.widget.TextView) p6.getTag());
        } else {
            p6 = this.c.inflate(2130903071, 0);
            this.d = ((android.widget.ImageView) p6.findViewById(2131493023));
            this.d.setImageBitmap(com.teamspeak.ts3client.data.d.t.a(2130837579, 1107296256, 1107296256));
            String v0_9 = ((android.widget.TextView) p6.findViewById(2131493024));
            p6.setTag(v0_9);
            v1_0 = v0_9;
        }
        v1_0.setText(((com.teamspeak.ts3client.data.ab) this.a.get(p5)).a);
        return p6;
    }
}
