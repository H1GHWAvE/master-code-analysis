package com.a.a.a.a;
final class g implements com.a.a.a.a.e {
    private android.os.IBinder a;

    g(android.os.IBinder p1)
    {
        this.a = p1;
        return;
    }

    private static String a()
    {
        return "com.android.vending.licensing.ILicenseResultListener";
    }

    public final void a(int p6, String p7, String p8)
    {
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v1.writeInterfaceToken("com.android.vending.licensing.ILicenseResultListener");
            v1.writeInt(p6);
            v1.writeString(p7);
            v1.writeString(p8);
            this.a.transact(1, v1, 0, 1);
            v1.recycle();
            return;
        } catch (Throwable v0_2) {
            v1.recycle();
            throw v0_2;
        }
    }

    public final android.os.IBinder asBinder()
    {
        return this.a;
    }
}
