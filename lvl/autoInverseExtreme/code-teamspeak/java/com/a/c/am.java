package com.a.c;
final class am implements com.a.c.ap {
    private final com.a.c.c.a a;
    private final boolean b;
    private final Class c;
    private final com.a.c.ae d;
    private final com.a.c.v e;

    private am(Object p3, com.a.c.c.a p4, boolean p5, Class p6)
    {
        int v0_1;
        if (!(p3 instanceof com.a.c.ae)) {
            v0_1 = 0;
        } else {
            v0_1 = ((com.a.c.ae) p3);
        }
        int v3_1;
        this.d = v0_1;
        if (!(p3 instanceof com.a.c.v)) {
            v3_1 = 0;
        } else {
            v3_1 = ((com.a.c.v) p3);
        }
        int v0_6;
        this.e = v3_1;
        if ((this.d == null) && (this.e == null)) {
            v0_6 = 0;
        } else {
            v0_6 = 1;
        }
        com.a.c.b.a.a(v0_6);
        this.a = p4;
        this.b = p5;
        this.c = p6;
        return;
    }

    synthetic am(Object p1, com.a.c.c.a p2, boolean p3, Class p4, byte p5)
    {
        this(p1, p2, p3, p4);
        return;
    }

    public final com.a.c.an a(com.a.c.k p8, com.a.c.c.a p9)
    {
        int v0_2;
        if (this.a == null) {
            v0_2 = this.c.isAssignableFrom(p9.a);
        } else {
            if ((!this.a.equals(p9)) && ((!this.b) || (this.a.b != p9.a))) {
                v0_2 = 0;
            } else {
                v0_2 = 1;
            }
        }
        int v0_8;
        if (v0_2 == 0) {
            v0_8 = 0;
        } else {
            v0_8 = new com.a.c.ak(this.d, this.e, p8, p9, this, 0);
        }
        return v0_8;
    }
}
