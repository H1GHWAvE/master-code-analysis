package com.a.c.b.a;
final class aw implements com.a.c.ap {
    final synthetic Class a;
    final synthetic Class b;
    final synthetic com.a.c.an c;

    aw(Class p1, Class p2, com.a.c.an p3)
    {
        this.a = p1;
        this.b = p2;
        this.c = p3;
        return;
    }

    public final com.a.c.an a(com.a.c.k p3, com.a.c.c.a p4)
    {
        int v0_1;
        int v0_0 = p4.a;
        if ((v0_0 != this.a) && (v0_0 != this.b)) {
            v0_1 = 0;
        } else {
            v0_1 = this.c;
        }
        return v0_1;
    }

    public final String toString()
    {
        return new StringBuilder("Factory[type=").append(this.b.getName()).append("+").append(this.a.getName()).append(",adapter=").append(this.c).append("]").toString();
    }
}
