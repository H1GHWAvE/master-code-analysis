package com.a.c.b.a;
public final class g implements com.a.c.ap {
    private final com.a.c.b.f a;

    public g(com.a.c.b.f p1)
    {
        this.a = p1;
        return;
    }

    static com.a.c.an a(com.a.c.b.f p2, com.a.c.k p3, com.a.c.c.a p4, com.a.c.a.b p5)
    {
        IllegalArgumentException v0_7;
        IllegalArgumentException v0_0 = p5.a();
        if (!com.a.c.an.isAssignableFrom(v0_0)) {
            if (!com.a.c.ap.isAssignableFrom(v0_0)) {
                throw new IllegalArgumentException("@JsonAdapter value must be TypeAdapter or TypeAdapterFactory reference.");
            } else {
                v0_7 = ((com.a.c.ap) p2.a(com.a.c.c.a.a(v0_0)).a()).a(p3, p4);
            }
        } else {
            v0_7 = ((com.a.c.an) p2.a(com.a.c.c.a.a(v0_0)).a());
        }
        return v0_7;
    }

    public final com.a.c.an a(com.a.c.k p3, com.a.c.c.a p4)
    {
        com.a.c.an v0_3;
        com.a.c.an v0_2 = ((com.a.c.a.b) p4.a.getAnnotation(com.a.c.a.b));
        if (v0_2 != null) {
            v0_3 = com.a.c.b.a.g.a(this.a, p3, p4, v0_2);
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }
}
