package com.a.c.b.a;
final class aq extends com.a.c.an {
    private static final String a = "year";
    private static final String b = "month";
    private static final String c = "dayOfMonth";
    private static final String d = "hourOfDay";
    private static final String e = "minute";
    private static final String f = "second";

    aq()
    {
        return;
    }

    private static void a(com.a.c.d.e p2, java.util.Calendar p3)
    {
        if (p3 != null) {
            p2.d();
            p2.a("year");
            p2.a(((long) p3.get(1)));
            p2.a("month");
            p2.a(((long) p3.get(2)));
            p2.a("dayOfMonth");
            p2.a(((long) p3.get(5)));
            p2.a("hourOfDay");
            p2.a(((long) p3.get(11)));
            p2.a("minute");
            p2.a(((long) p3.get(12)));
            p2.a("second");
            p2.a(((long) p3.get(13)));
            p2.e();
        } else {
            p2.f();
        }
        return;
    }

    private static java.util.Calendar b(com.a.c.d.a p9)
    {
        java.util.GregorianCalendar v0_3;
        java.util.GregorianCalendar v6 = 0;
        if (p9.f() != com.a.c.d.d.i) {
            p9.c();
            java.util.GregorianCalendar v5 = 0;
            java.util.GregorianCalendar v4 = 0;
            java.util.GregorianCalendar v3 = 0;
            java.util.GregorianCalendar v2 = 0;
            java.util.GregorianCalendar v1_1 = 0;
            while (p9.f() != com.a.c.d.d.d) {
                boolean v7_1 = p9.h();
                java.util.GregorianCalendar v0_4 = p9.n();
                if (!"year".equals(v7_1)) {
                    if (!"month".equals(v7_1)) {
                        if (!"dayOfMonth".equals(v7_1)) {
                            if (!"hourOfDay".equals(v7_1)) {
                                if (!"minute".equals(v7_1)) {
                                    if ("second".equals(v7_1)) {
                                        v6 = v0_4;
                                    }
                                } else {
                                    v5 = v0_4;
                                }
                            } else {
                                v4 = v0_4;
                            }
                        } else {
                            v3 = v0_4;
                        }
                    } else {
                        v2 = v0_4;
                    }
                } else {
                    v1_1 = v0_4;
                }
            }
            p9.d();
            v0_3 = new java.util.GregorianCalendar(v1_1, v2, v3, v4, v5, v6);
        } else {
            p9.k();
            v0_3 = 0;
        }
        return v0_3;
    }

    public final synthetic Object a(com.a.c.d.a p10)
    {
        java.util.GregorianCalendar v0_3;
        java.util.GregorianCalendar v6 = 0;
        if (p10.f() != com.a.c.d.d.i) {
            p10.c();
            java.util.GregorianCalendar v5 = 0;
            java.util.GregorianCalendar v4 = 0;
            java.util.GregorianCalendar v3 = 0;
            java.util.GregorianCalendar v2 = 0;
            java.util.GregorianCalendar v1_1 = 0;
            while (p10.f() != com.a.c.d.d.d) {
                boolean v7_1 = p10.h();
                java.util.GregorianCalendar v0_4 = p10.n();
                if (!"year".equals(v7_1)) {
                    if (!"month".equals(v7_1)) {
                        if (!"dayOfMonth".equals(v7_1)) {
                            if (!"hourOfDay".equals(v7_1)) {
                                if (!"minute".equals(v7_1)) {
                                    if ("second".equals(v7_1)) {
                                        v6 = v0_4;
                                    }
                                } else {
                                    v5 = v0_4;
                                }
                            } else {
                                v4 = v0_4;
                            }
                        } else {
                            v3 = v0_4;
                        }
                    } else {
                        v2 = v0_4;
                    }
                } else {
                    v1_1 = v0_4;
                }
            }
            p10.d();
            v0_3 = new java.util.GregorianCalendar(v1_1, v2, v3, v4, v5, v6);
        } else {
            p10.k();
            v0_3 = 0;
        }
        return v0_3;
    }

    public final synthetic void a(com.a.c.d.e p3, Object p4)
    {
        if (((java.util.Calendar) p4) != null) {
            p3.d();
            p3.a("year");
            p3.a(((long) ((java.util.Calendar) p4).get(1)));
            p3.a("month");
            p3.a(((long) ((java.util.Calendar) p4).get(2)));
            p3.a("dayOfMonth");
            p3.a(((long) ((java.util.Calendar) p4).get(5)));
            p3.a("hourOfDay");
            p3.a(((long) ((java.util.Calendar) p4).get(11)));
            p3.a("minute");
            p3.a(((long) ((java.util.Calendar) p4).get(12)));
            p3.a("second");
            p3.a(((long) ((java.util.Calendar) p4).get(13)));
            p3.e();
        } else {
            p3.f();
        }
        return;
    }
}
