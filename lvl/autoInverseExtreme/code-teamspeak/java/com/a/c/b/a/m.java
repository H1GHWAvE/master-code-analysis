package com.a.c.b.a;
final class m extends com.a.c.an {
    final synthetic com.a.c.b.a.l a;
    private final com.a.c.an b;
    private final com.a.c.an c;
    private final com.a.c.b.ao d;

    public m(com.a.c.b.a.l p2, com.a.c.k p3, reflect.Type p4, com.a.c.an p5, reflect.Type p6, com.a.c.an p7, com.a.c.b.ao p8)
    {
        this.a = p2;
        this.b = new com.a.c.b.a.y(p3, p5, p4);
        this.c = new com.a.c.b.a.y(p3, p7, p6);
        this.d = p8;
        return;
    }

    private static String a(com.a.c.w p2)
    {
        AssertionError v0_4;
        if (!(p2 instanceof com.a.c.ac)) {
            if (!(p2 instanceof com.a.c.y)) {
                throw new AssertionError();
            } else {
                v0_4 = "null";
            }
        } else {
            AssertionError v0_5 = p2.n();
            if (!(v0_5.a instanceof Number)) {
                if (!(v0_5.a instanceof Boolean)) {
                    if (!(v0_5.a instanceof String)) {
                        throw new AssertionError();
                    } else {
                        v0_4 = v0_5.b();
                    }
                } else {
                    v0_4 = Boolean.toString(v0_5.l());
                }
            } else {
                v0_4 = String.valueOf(v0_5.a());
            }
        }
        return v0_4;
    }

    private void a(com.a.c.d.e p9, java.util.Map p10)
    {
        int v2_0 = 0;
        if (p10 != null) {
            if (com.a.c.b.a.l.a(this.a)) {
                java.util.ArrayList v3_1 = new java.util.ArrayList(p10.size());
                java.util.ArrayList v4_1 = new java.util.ArrayList(p10.size());
                java.util.Iterator v5 = p10.entrySet().iterator();
                boolean v1_0 = 0;
                while (v5.hasNext()) {
                    AssertionError v0_28;
                    AssertionError v0_24 = ((java.util.Map$Entry) v5.next());
                    com.a.c.w v6_1 = this.b.a(v0_24.getKey());
                    v3_1.add(v6_1);
                    v4_1.add(v0_24.getValue());
                    if ((!(v6_1 instanceof com.a.c.t)) && (!(v6_1 instanceof com.a.c.z))) {
                        v0_28 = 0;
                    } else {
                        v0_28 = 1;
                    }
                    v1_0 = (v0_28 | v1_0);
                }
                if (!v1_0) {
                    p9.d();
                    while (v2_0 < v3_1.size()) {
                        AssertionError v0_12;
                        AssertionError v0_8 = ((com.a.c.w) v3_1.get(v2_0));
                        if (!(v0_8 instanceof com.a.c.ac)) {
                            if (!(v0_8 instanceof com.a.c.y)) {
                                throw new AssertionError();
                            } else {
                                v0_12 = "null";
                            }
                        } else {
                            AssertionError v0_13 = v0_8.n();
                            if (!(v0_13.a instanceof Number)) {
                                if (!(v0_13.a instanceof Boolean)) {
                                    if (!(v0_13.a instanceof String)) {
                                        throw new AssertionError();
                                    } else {
                                        v0_12 = v0_13.b();
                                    }
                                } else {
                                    v0_12 = Boolean.toString(v0_13.l());
                                }
                            } else {
                                v0_12 = String.valueOf(v0_13.a());
                            }
                        }
                        p9.a(v0_12);
                        this.c.a(p9, v4_1.get(v2_0));
                        v2_0++;
                    }
                    p9.e();
                } else {
                    p9.b();
                    while (v2_0 < v3_1.size()) {
                        p9.b();
                        com.a.c.b.aq.a(((com.a.c.w) v3_1.get(v2_0)), p9);
                        this.c.a(p9, v4_1.get(v2_0));
                        p9.c();
                        v2_0++;
                    }
                    p9.c();
                }
            } else {
                p9.d();
                boolean v1_10 = p10.entrySet().iterator();
                while (v1_10.hasNext()) {
                    AssertionError v0_33 = ((java.util.Map$Entry) v1_10.next());
                    p9.a(String.valueOf(v0_33.getKey()));
                    this.c.a(p9, v0_33.getValue());
                }
                p9.e();
            }
        } else {
            p9.f();
        }
        return;
    }

    private java.util.Map b(com.a.c.d.a p5)
    {
        com.a.c.ag v0_3;
        String v1_0 = p5.f();
        if (v1_0 != com.a.c.d.d.i) {
            v0_3 = ((java.util.Map) this.d.a());
            if (v1_0 != com.a.c.d.d.a) {
                p5.c();
                while (p5.e()) {
                    com.a.c.b.u.a.a(p5);
                    String v1_4 = this.b.a(p5);
                    if (v0_3.put(v1_4, this.c.a(p5)) != null) {
                        throw new com.a.c.ag(new StringBuilder("duplicate key: ").append(v1_4).toString());
                    }
                }
                p5.d();
            } else {
                p5.a();
                while (p5.e()) {
                    p5.a();
                    String v1_9 = this.b.a(p5);
                    if (v0_3.put(v1_9, this.c.a(p5)) == null) {
                        p5.b();
                    } else {
                        throw new com.a.c.ag(new StringBuilder("duplicate key: ").append(v1_9).toString());
                    }
                }
                p5.b();
            }
        } else {
            p5.k();
            v0_3 = 0;
        }
        return v0_3;
    }

    public final synthetic Object a(com.a.c.d.a p5)
    {
        com.a.c.ag v0_3;
        String v1_0 = p5.f();
        if (v1_0 != com.a.c.d.d.i) {
            v0_3 = ((java.util.Map) this.d.a());
            if (v1_0 != com.a.c.d.d.a) {
                p5.c();
                while (p5.e()) {
                    com.a.c.b.u.a.a(p5);
                    String v1_4 = this.b.a(p5);
                    if (v0_3.put(v1_4, this.c.a(p5)) != null) {
                        throw new com.a.c.ag(new StringBuilder("duplicate key: ").append(v1_4).toString());
                    }
                }
                p5.d();
            } else {
                p5.a();
                while (p5.e()) {
                    p5.a();
                    String v1_9 = this.b.a(p5);
                    if (v0_3.put(v1_9, this.c.a(p5)) == null) {
                        p5.b();
                    } else {
                        throw new com.a.c.ag(new StringBuilder("duplicate key: ").append(v1_9).toString());
                    }
                }
                p5.b();
            }
        } else {
            p5.k();
            v0_3 = 0;
        }
        return v0_3;
    }

    public final synthetic void a(com.a.c.d.e p9, Object p10)
    {
        int v2_0 = 0;
        if (((java.util.Map) p10) != null) {
            if (com.a.c.b.a.l.a(this.a)) {
                java.util.ArrayList v3_1 = new java.util.ArrayList(((java.util.Map) p10).size());
                java.util.ArrayList v4_1 = new java.util.ArrayList(((java.util.Map) p10).size());
                java.util.Iterator v5 = ((java.util.Map) p10).entrySet().iterator();
                boolean v1_0 = 0;
                while (v5.hasNext()) {
                    AssertionError v0_28;
                    AssertionError v0_24 = ((java.util.Map$Entry) v5.next());
                    com.a.c.w v6_1 = this.b.a(v0_24.getKey());
                    v3_1.add(v6_1);
                    v4_1.add(v0_24.getValue());
                    if ((!(v6_1 instanceof com.a.c.t)) && (!(v6_1 instanceof com.a.c.z))) {
                        v0_28 = 0;
                    } else {
                        v0_28 = 1;
                    }
                    v1_0 = (v0_28 | v1_0);
                }
                if (!v1_0) {
                    p9.d();
                    while (v2_0 < v3_1.size()) {
                        AssertionError v0_12;
                        AssertionError v0_8 = ((com.a.c.w) v3_1.get(v2_0));
                        if (!(v0_8 instanceof com.a.c.ac)) {
                            if (!(v0_8 instanceof com.a.c.y)) {
                                throw new AssertionError();
                            } else {
                                v0_12 = "null";
                            }
                        } else {
                            AssertionError v0_13 = v0_8.n();
                            if (!(v0_13.a instanceof Number)) {
                                if (!(v0_13.a instanceof Boolean)) {
                                    if (!(v0_13.a instanceof String)) {
                                        throw new AssertionError();
                                    } else {
                                        v0_12 = v0_13.b();
                                    }
                                } else {
                                    v0_12 = Boolean.toString(v0_13.l());
                                }
                            } else {
                                v0_12 = String.valueOf(v0_13.a());
                            }
                        }
                        p9.a(v0_12);
                        this.c.a(p9, v4_1.get(v2_0));
                        v2_0++;
                    }
                    p9.e();
                } else {
                    p9.b();
                    while (v2_0 < v3_1.size()) {
                        p9.b();
                        com.a.c.b.aq.a(((com.a.c.w) v3_1.get(v2_0)), p9);
                        this.c.a(p9, v4_1.get(v2_0));
                        p9.c();
                        v2_0++;
                    }
                    p9.c();
                }
            } else {
                p9.d();
                boolean v1_10 = ((java.util.Map) p10).entrySet().iterator();
                while (v1_10.hasNext()) {
                    AssertionError v0_33 = ((java.util.Map$Entry) v1_10.next());
                    p9.a(String.valueOf(v0_33.getKey()));
                    this.c.a(p9, v0_33.getValue());
                }
                p9.e();
            }
        } else {
            p9.f();
        }
        return;
    }
}
