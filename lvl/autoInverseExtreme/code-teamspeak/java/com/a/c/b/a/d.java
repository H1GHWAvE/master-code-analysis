package com.a.c.b.a;
final class d extends com.a.c.an {
    private final com.a.c.an a;
    private final com.a.c.b.ao b;

    public d(com.a.c.k p2, reflect.Type p3, com.a.c.an p4, com.a.c.b.ao p5)
    {
        this.a = new com.a.c.b.a.y(p2, p4, p3);
        this.b = p5;
        return;
    }

    private void a(com.a.c.d.e p4, java.util.Collection p5)
    {
        if (p5 != null) {
            p4.b();
            java.util.Iterator v0 = p5.iterator();
            while (v0.hasNext()) {
                this.a.a(p4, v0.next());
            }
            p4.c();
        } else {
            p4.f();
        }
        return;
    }

    private java.util.Collection b(com.a.c.d.a p3)
    {
        java.util.Collection v0_3;
        if (p3.f() != com.a.c.d.d.i) {
            v0_3 = ((java.util.Collection) this.b.a());
            p3.a();
            while (p3.e()) {
                v0_3.add(this.a.a(p3));
            }
            p3.b();
        } else {
            p3.k();
            v0_3 = 0;
        }
        return v0_3;
    }

    public final synthetic Object a(com.a.c.d.a p3)
    {
        java.util.Collection v0_3;
        if (p3.f() != com.a.c.d.d.i) {
            v0_3 = ((java.util.Collection) this.b.a());
            p3.a();
            while (p3.e()) {
                v0_3.add(this.a.a(p3));
            }
            p3.b();
        } else {
            p3.k();
            v0_3 = 0;
        }
        return v0_3;
    }

    public final synthetic void a(com.a.c.d.e p4, Object p5)
    {
        if (((java.util.Collection) p5) != null) {
            p4.b();
            java.util.Iterator v0 = ((java.util.Collection) p5).iterator();
            while (v0.hasNext()) {
                this.a.a(p4, v0.next());
            }
            p4.c();
        } else {
            p4.f();
        }
        return;
    }
}
