package com.a.c;
public final class ab {

    public ab()
    {
        return;
    }

    private static com.a.c.w a(com.a.c.d.a p5)
    {
        boolean v1 = p5.b;
        p5.b = 1;
        try {
            OutOfMemoryError v0_1 = com.a.c.b.aq.a(p5);
            p5.b = v1;
            return v0_1;
        } catch (OutOfMemoryError v0_4) {
            p5.b = v1;
            throw v0_4;
        } catch (OutOfMemoryError v0_3) {
            throw new com.a.c.aa(new StringBuilder("Failed parsing JSON source: ").append(p5).append(" to Json").toString(), v0_3);
        } catch (OutOfMemoryError v0_2) {
            throw new com.a.c.aa(new StringBuilder("Failed parsing JSON source: ").append(p5).append(" to Json").toString(), v0_2);
        }
    }

    private static com.a.c.w a(java.io.Reader p3)
    {
        try {
            com.a.c.ag v0_1 = new com.a.c.d.a(p3);
            String v1_0 = com.a.c.ab.a(v0_1);
        } catch (com.a.c.ag v0_6) {
            throw new com.a.c.x(v0_6);
        } catch (com.a.c.ag v0_7) {
            throw new com.a.c.ag(v0_7);
        } catch (com.a.c.ag v0_5) {
            throw new com.a.c.ag(v0_5);
        }
        if (((v1_0 instanceof com.a.c.y)) || (v0_1.f() == com.a.c.d.d.j)) {
            return v1_0;
        } else {
            throw new com.a.c.ag("Did not consume the entire document.");
        }
    }

    private static com.a.c.w a(String p1)
    {
        return com.a.c.ab.a(new java.io.StringReader(p1));
    }
}
