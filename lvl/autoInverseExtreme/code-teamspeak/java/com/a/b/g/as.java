package com.a.b.g;
final class as extends com.a.b.g.b {
    private final int b;

    varargs as(com.a.b.g.ak[] p5)
    {
        int v0 = 0;
        this(p5);
        int v2 = p5.length;
        int v1 = 0;
        while (v0 < v2) {
            v1 += p5[v0].b();
            v0++;
        }
        this.b = v1;
        return;
    }

    final com.a.b.g.ag a(com.a.b.g.al[] p10)
    {
        byte[] v3 = new byte[(this.b / 8)];
        int v4 = p10.length;
        com.a.b.g.ag v0_2 = 0;
        int v2 = 0;
        while (v0_2 < v4) {
            com.a.b.g.ag v5_1 = p10[v0_2].a();
            int v7_1 = new int[2];
            v7_1[0] = (v5_1.a() / 8);
            v7_1[1] = (v5_1.a() / 8);
            int v6_3 = com.a.b.l.q.a(v7_1);
            com.a.b.b.cn.a(v2, (v2 + v6_3), v3.length);
            v5_1.a(v3, v2, v6_3);
            v2 += v6_3;
            v0_2++;
        }
        return com.a.b.g.ag.a(v3);
    }

    public final int b()
    {
        return this.b;
    }

    public final boolean equals(Object p5)
    {
        int v1 = 0;
        if (((p5 instanceof com.a.b.g.as)) && ((this.b == ((com.a.b.g.as) p5).b) && (this.a.length == ((com.a.b.g.as) p5).a.length))) {
            int v0_4 = 0;
            while (v0_4 < this.a.length) {
                if (this.a[v0_4].equals(((com.a.b.g.as) p5).a[v0_4])) {
                    v0_4++;
                }
            }
            v1 = 1;
        }
        return v1;
    }

    public final int hashCode()
    {
        int v1 = this.b;
        com.a.b.g.ak[] v2 = this.a;
        int v3 = v2.length;
        int v0 = 0;
        while (v0 < v3) {
            v1 ^= v2[v0].hashCode();
            v0++;
        }
        return v1;
    }
}
