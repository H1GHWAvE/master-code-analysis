package com.a.b.g;
final class q {
    final long[] a;
    long b;

    q(long p4)
    {
        long[] v0_3 = new long[com.a.b.l.q.a(com.a.b.j.i.d(p4, java.math.RoundingMode.CEILING))];
        this(v0_3);
        return;
    }

    q(long[] p7)
    {
        int v0_1;
        int v1 = 0;
        if (p7.length <= 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.a(v0_1, "data length is zero!");
        this.a = p7;
        long v2_1 = 0;
        while (v1 < p7.length) {
            v2_1 += ((long) Long.bitCount(p7[v1]));
            v1++;
        }
        this.b = v2_1;
        return;
    }

    private void a(com.a.b.g.q p9)
    {
        long v0_2;
        int v2 = 0;
        if (this.a.length != p9.a.length) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        long v4_1 = new Object[2];
        v4_1[0] = Integer.valueOf(this.a.length);
        v4_1[1] = Integer.valueOf(p9.a.length);
        com.a.b.b.cn.a(v0_2, "BitArrays must be of equal length (%s != %s)", v4_1);
        this.b = 0;
        while (v2 < this.a.length) {
            long v0_6 = this.a;
            v0_6[v2] = (v0_6[v2] | p9.a[v2]);
            this.b = (this.b + ((long) Long.bitCount(this.a[v2])));
            v2++;
        }
        return;
    }

    private long c()
    {
        return this.b;
    }

    final long a()
    {
        return (((long) this.a.length) * 64);
    }

    final boolean a(long p10)
    {
        int v0_1;
        if (this.b(p10)) {
            v0_1 = 0;
        } else {
            int v0_2 = this.a;
            int v1_1 = ((int) (p10 >> 6));
            v0_2[v1_1] = (v0_2[v1_1] | (1 << ((int) p10)));
            this.b = (this.b + 1);
            v0_1 = 1;
        }
        return v0_1;
    }

    final com.a.b.g.q b()
    {
        return new com.a.b.g.q(((long[]) this.a.clone()));
    }

    final boolean b(long p6)
    {
        int v0_4;
        if ((this.a[((int) (p6 >> 6))] & (1 << ((int) p6))) == 0) {
            v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    public final boolean equals(Object p3)
    {
        int v0_1;
        if (!(p3 instanceof com.a.b.g.q)) {
            v0_1 = 0;
        } else {
            v0_1 = java.util.Arrays.equals(this.a, ((com.a.b.g.q) p3).a);
        }
        return v0_1;
    }

    public final int hashCode()
    {
        return java.util.Arrays.hashCode(this.a);
    }
}
