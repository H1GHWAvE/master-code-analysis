package com.a.b.g;
abstract class a extends com.a.b.g.d {
    private final java.nio.ByteBuffer a;

    a()
    {
        this.a = java.nio.ByteBuffer.allocate(8).order(java.nio.ByteOrder.LITTLE_ENDIAN);
        return;
    }

    private com.a.b.g.al c(int p3)
    {
        try {
            this.a(this.a.array(), 0, p3);
            this.a.clear();
            return this;
        } catch (Throwable v0_3) {
            this.a.clear();
            throw v0_3;
        }
    }

    public final com.a.b.g.al a(char p2)
    {
        this.a.putChar(p2);
        return this.c(2);
    }

    public final com.a.b.g.al a(int p2)
    {
        this.a.putInt(p2);
        return this.c(4);
    }

    public final com.a.b.g.al a(long p2)
    {
        this.a.putLong(p2);
        return this.c(8);
    }

    public final com.a.b.g.al a(Object p1, com.a.b.g.w p2)
    {
        p2.a(p1, this);
        return this;
    }

    public final com.a.b.g.al a(short p2)
    {
        this.a.putShort(p2);
        return this.c(2);
    }

    protected abstract void a();

    protected void a(byte[] p3)
    {
        this.a(p3, 0, p3.length);
        return;
    }

    protected void a(byte[] p3, int p4, int p5)
    {
        int v0 = p4;
        while (v0 < (p4 + p5)) {
            this.a(p3[v0]);
            v0++;
        }
        return;
    }

    public final com.a.b.g.al b(byte p1)
    {
        this.a(p1);
        return this;
    }

    public final com.a.b.g.al b(byte[] p1)
    {
        com.a.b.b.cn.a(p1);
        this.a(p1);
        return this;
    }

    public final com.a.b.g.al b(byte[] p3, int p4, int p5)
    {
        com.a.b.b.cn.a(p4, (p4 + p5), p3.length);
        this.a(p3, p4, p5);
        return this;
    }

    public final synthetic com.a.b.g.bn b(char p2)
    {
        return this.a(p2);
    }

    public final synthetic com.a.b.g.bn b(int p2)
    {
        return this.a(p2);
    }

    public final synthetic com.a.b.g.bn b(long p2)
    {
        return this.a(p2);
    }

    public final synthetic com.a.b.g.bn b(short p2)
    {
        return this.a(p2);
    }

    public final synthetic com.a.b.g.bn c(byte p1)
    {
        this.a(p1);
        return this;
    }

    public final synthetic com.a.b.g.bn c(byte[] p2)
    {
        return this.b(p2);
    }

    public final synthetic com.a.b.g.bn c(byte[] p2, int p3, int p4)
    {
        return this.b(p2, p3, p4);
    }
}
