package com.a.b.g;
final class bh extends com.a.b.g.a {
    private final java.security.MessageDigest a;
    private final int b;
    private boolean c;

    private bh(java.security.MessageDigest p1, int p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    synthetic bh(java.security.MessageDigest p1, int p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    private void b()
    {
        int v0_1;
        if (this.c) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.b(v0_1, "Cannot re-use a Hasher after calling hash() on it");
        return;
    }

    public final com.a.b.g.ag a()
    {
        com.a.b.g.ag v0_5;
        this.b();
        this.c = 1;
        if (this.b != this.a.getDigestLength()) {
            v0_5 = com.a.b.g.ag.a(java.util.Arrays.copyOf(this.a.digest(), this.b));
        } else {
            v0_5 = com.a.b.g.ag.a(this.a.digest());
        }
        return v0_5;
    }

    protected final void a(byte p2)
    {
        this.b();
        this.a.update(p2);
        return;
    }

    protected final void a(byte[] p2)
    {
        this.b();
        this.a.update(p2);
        return;
    }

    protected final void a(byte[] p2, int p3, int p4)
    {
        this.b();
        this.a.update(p2, p3, p4);
        return;
    }
}
