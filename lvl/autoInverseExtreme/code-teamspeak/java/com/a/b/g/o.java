package com.a.b.g;
final enum class o extends com.a.b.g.n {

    o(String p2)
    {
        this(p2, 0, 0);
        return;
    }

    public final boolean a(Object p11, com.a.b.g.w p12, int p13, com.a.b.g.q p14)
    {
        long v4 = p14.a();
        int v0_2 = com.a.b.g.am.b().a(p11, p12).c();
        int v3 = ((int) v0_2);
        int v6 = ((int) (v0_2 >> 32));
        int v2_1 = 0;
        int v1_1 = 1;
        while (v1_1 <= p13) {
            int v0_6 = ((v1_1 * v6) + v3);
            if (v0_6 < 0) {
                v0_6 ^= -1;
            }
            v2_1 |= p14.a((((long) v0_6) % v4));
            v1_1++;
        }
        return v2_1;
    }

    public final boolean b(Object p11, com.a.b.g.w p12, int p13, com.a.b.g.q p14)
    {
        long v4 = p14.a();
        int v2_0 = com.a.b.g.am.b().a(p11, p12).c();
        int v2_2 = 1;
        while (v2_2 <= p13) {
            int v0_5 = ((v2_2 * ((int) (v2_0 >> 32))) + ((int) v2_0));
            if (v0_5 < 0) {
                v0_5 ^= -1;
            }
            if (p14.b((((long) v0_5) % v4))) {
                v2_2++;
            } else {
                int v0_3 = 0;
            }
            return v0_3;
        }
        v0_3 = 1;
        return v0_3;
    }
}
