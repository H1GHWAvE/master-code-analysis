package com.a.b.k;
public final class b {
    private final String a;

    private b(String p1)
    {
        this.a = p1;
        return;
    }

    private static com.a.b.k.b a(String p4)
    {
        String v0_1;
        IllegalArgumentException v1_0 = com.a.b.k.a.a(p4);
        if (v1_0.a()) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.a(v0_1);
        int v2_0 = v1_0.a;
        try {
            String v0_2 = com.a.b.k.d.a(v2_0);
            IllegalArgumentException v1_1 = v0_2;
        } catch (IllegalArgumentException v1) {
            v1_1 = v0_2;
        }
        String v0_8;
        if (v1_1 == null) {
            IllegalArgumentException v1_2 = com.a.b.k.f.a(v2_0);
            if (!v1_2.a()) {
                String v0_6;
                String v0_4 = String.valueOf(v2_0);
                if (v0_4.length() == 0) {
                    v0_6 = new String("Domain name does not have a recognized public suffix: ");
                } else {
                    v0_6 = "Domain name does not have a recognized public suffix: ".concat(v0_4);
                }
                throw new IllegalArgumentException(v0_6);
            } else {
                v0_8 = new com.a.b.k.b(v1_2.toString());
            }
        } else {
            v0_8 = new com.a.b.k.b(com.a.b.k.d.a(v1_1));
        }
        return v0_8;
    }

    private static com.a.b.k.b b(String p5)
    {
        try {
            return com.a.b.k.b.a(p5);
        } catch (IllegalArgumentException v1) {
            String v0_1 = String.valueOf(p5);
            if (v0_1.length() == 0) {
                String v0_3 = new String("Invalid host specifier: ");
            } else {
                v0_3 = "Invalid host specifier: ".concat(v0_1);
            }
            java.text.ParseException v2_1 = new java.text.ParseException(v0_3, 0);
            v2_1.initCause(v1);
            throw v2_1;
        }
    }

    private static boolean c(String p1)
    {
        try {
            com.a.b.k.b.a(p1);
            int v0 = 1;
        } catch (int v0) {
            v0 = 0;
        }
        return v0;
    }

    public final boolean equals(Object p3)
    {
        int v0_1;
        if (this != p3) {
            if (!(p3 instanceof com.a.b.k.b)) {
                v0_1 = 0;
            } else {
                v0_1 = this.a.equals(((com.a.b.k.b) p3).a);
            }
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final int hashCode()
    {
        return this.a.hashCode();
    }

    public final String toString()
    {
        return this.a;
    }
}
