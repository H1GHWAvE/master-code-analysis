package com.a.b.k;
public final class k extends com.a.b.e.p {
    private static final char[] a;
    private static final char[] b;
    private final boolean c;
    private final boolean[] d;

    static k()
    {
        char[] v0_1 = new char[1];
        v0_1[0] = 43;
        com.a.b.k.k.a = v0_1;
        com.a.b.k.k.b = "0123456789ABCDEF".toCharArray();
        return;
    }

    public k(String p3, boolean p4)
    {
        com.a.b.b.cn.a(p3);
        if (!p3.matches(".*[0-9A-Za-z].*")) {
            IllegalArgumentException v0_3 = String.valueOf(p3).concat("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");
            if ((!p4) || (!v0_3.contains(" "))) {
                this.c = p4;
                this.d = com.a.b.k.k.b(v0_3);
                return;
            } else {
                throw new IllegalArgumentException("plusForSpace cannot be specified when space is a \'safe\' character");
            }
        } else {
            throw new IllegalArgumentException("Alphanumeric characters are always \'safe\' and should not be explicitly specified");
        }
    }

    private static boolean[] b(String p6)
    {
        int v0 = 0;
        char[] v3 = p6.toCharArray();
        char v4_0 = v3.length;
        int v2_0 = -1;
        boolean[] v1_1 = 0;
        while (v1_1 < v4_0) {
            v2_0 = Math.max(v3[v1_1], v2_0);
            v1_1++;
        }
        boolean[] v1_3 = new boolean[(v2_0 + 1)];
        int v2_1 = v3.length;
        while (v0 < v2_1) {
            v1_3[v3[v0]] = 1;
            v0++;
        }
        return v1_3;
    }

    protected final int a(CharSequence p3, int p4, int p5)
    {
        com.a.b.b.cn.a(p3);
        while (p4 < p5) {
            boolean v0_0 = p3.charAt(p4);
            if ((v0_0 >= this.d.length) || (!this.d[v0_0])) {
                break;
            }
            p4++;
        }
        return p4;
    }

    public final String a(String p5)
    {
        com.a.b.b.cn.a(p5);
        int v1 = p5.length();
        int v0 = 0;
        while (v0 < v1) {
            boolean v2_0 = p5.charAt(v0);
            if ((v2_0 < this.d.length) && (this.d[v2_0])) {
                v0++;
            } else {
                p5 = this.a(p5, v0);
                break;
            }
        }
        return p5;
    }

    protected final char[] a(int p8)
    {
        if ((p8 >= this.d.length) || (!this.d[p8])) {
            if ((p8 != 32) || (!this.c)) {
                if (p8 > 127) {
                    if (p8 > 2047) {
                        if (p8 > 65535) {
                            if (p8 > 1114111) {
                                throw new IllegalArgumentException(new StringBuilder(43).append("Invalid unicode character value ").append(p8).toString());
                            } else {
                                IllegalArgumentException v0_13 = new char[12];
                                v0_13[0] = 37;
                                v0_13[1] = 70;
                                v0_13[3] = 37;
                                v0_13[6] = 37;
                                v0_13[9] = 37;
                                v0_13[11] = com.a.b.k.k.b[(p8 & 15)];
                                String v1_10 = (p8 >> 4);
                                v0_13[10] = com.a.b.k.k.b[((v1_10 & 3) | 8)];
                                String v1_11 = (v1_10 >> 2);
                                v0_13[8] = com.a.b.k.k.b[(v1_11 & 15)];
                                String v1_12 = (v1_11 >> 4);
                                v0_13[7] = com.a.b.k.k.b[((v1_12 & 3) | 8)];
                                String v1_13 = (v1_12 >> 2);
                                v0_13[5] = com.a.b.k.k.b[(v1_13 & 15)];
                                String v1_14 = (v1_13 >> 4);
                                v0_13[4] = com.a.b.k.k.b[((v1_14 & 3) | 8)];
                                v0_13[2] = com.a.b.k.k.b[((v1_14 >> 2) & 7)];
                            }
                        } else {
                            v0_13 = new char[9];
                            v0_13[0] = 37;
                            v0_13[1] = 69;
                            v0_13[3] = 37;
                            v0_13[6] = 37;
                            v0_13[8] = com.a.b.k.k.b[(p8 & 15)];
                            String v1_21 = (p8 >> 4);
                            v0_13[7] = com.a.b.k.k.b[((v1_21 & 3) | 8)];
                            String v1_22 = (v1_21 >> 2);
                            v0_13[5] = com.a.b.k.k.b[(v1_22 & 15)];
                            String v1_23 = (v1_22 >> 4);
                            v0_13[4] = com.a.b.k.k.b[((v1_23 & 3) | 8)];
                            v0_13[2] = com.a.b.k.k.b[(v1_23 >> 2)];
                        }
                    } else {
                        v0_13 = new char[6];
                        v0_13[0] = 37;
                        v0_13[3] = 37;
                        v0_13[5] = com.a.b.k.k.b[(p8 & 15)];
                        String v1_27 = (p8 >> 4);
                        v0_13[4] = com.a.b.k.k.b[((v1_27 & 3) | 8)];
                        String v1_28 = (v1_27 >> 2);
                        v0_13[2] = com.a.b.k.k.b[(v1_28 & 15)];
                        v0_13[1] = com.a.b.k.k.b[((v1_28 >> 4) | 12)];
                    }
                } else {
                    v0_13 = new char[3];
                    v0_13[0] = 37;
                    v0_13[2] = com.a.b.k.k.b[(p8 & 15)];
                    v0_13[1] = com.a.b.k.k.b[(p8 >> 4)];
                }
            } else {
                v0_13 = com.a.b.k.k.a;
            }
        } else {
            v0_13 = 0;
        }
        return v0_13;
    }
}
