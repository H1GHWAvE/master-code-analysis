package com.a.b.l;
public final class d {

    private d()
    {
        return;
    }

    private static int a(byte p0)
    {
        return p0;
    }

    static int a(byte[] p2, byte p3, int p4, int p5)
    {
        int v0 = p4;
        while (v0 < p5) {
            if (p2[v0] != p3) {
                v0++;
            }
            return v0;
        }
        v0 = -1;
        return v0;
    }

    private static int a(byte[] p5, byte[] p6)
    {
        int v1 = 0;
        com.a.b.b.cn.a(p5, "array");
        com.a.b.b.cn.a(p6, "target");
        if (p6.length != 0) {
            int v0_3 = 0;
            while (v0_3 < ((p5.length - p6.length) + 1)) {
                int v2_3 = 0;
                while (v2_3 < p6.length) {
                    if (p5[(v0_3 + v2_3)] != p6[v2_3]) {
                        v0_3++;
                    } else {
                        v2_3++;
                    }
                }
                v1 = v0_3;
            }
            v1 = -1;
        }
        return v1;
    }

    private static varargs java.util.List a(byte[] p1)
    {
        com.a.b.l.e v0_2;
        if (p1.length != 0) {
            v0_2 = new com.a.b.l.e(p1);
        } else {
            v0_2 = java.util.Collections.emptyList();
        }
        return v0_2;
    }

    private static boolean a(byte[] p4, byte p5)
    {
        int v0 = 0;
        int v1 = 0;
        while (v1 < p4.length) {
            if (p4[v1] != p5) {
                v1++;
            } else {
                v0 = 1;
                break;
            }
        }
        return v0;
    }

    private static byte[] a(java.util.Collection p5)
    {
        byte[] v0_1;
        int v2 = 0;
        if (!(p5 instanceof com.a.b.l.e)) {
            Object[] v3_0 = p5.toArray();
            int v4_0 = v3_0.length;
            byte[] v1_0 = new byte[v4_0];
            while (v2 < v4_0) {
                v1_0[v2] = ((Number) com.a.b.b.cn.a(v3_0[v2])).byteValue();
                v2++;
            }
            v0_1 = v1_0;
        } else {
            byte[] v1_1 = ((com.a.b.l.e) p5).size();
            v0_1 = new byte[v1_1];
            System.arraycopy(((com.a.b.l.e) p5).a, ((com.a.b.l.e) p5).b, v0_1, 0, v1_1);
        }
        return v0_1;
    }

    private static byte[] a(byte[] p3, int p4)
    {
        byte[] v0 = new byte[p4];
        System.arraycopy(p3, 0, v0, 0, Math.min(p3.length, p4));
        return v0;
    }

    private static byte[] a(byte[] p6, int p7, int p8)
    {
        byte[] v0_0;
        if (p7 < 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        byte[] v0_1;
        Integer v4_0 = new Object[1];
        v4_0[0] = Integer.valueOf(p7);
        com.a.b.b.cn.a(v0_0, "Invalid minLength: %s", v4_0);
        if (p8 < 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        int v1_1 = new Object[1];
        v1_1[0] = Integer.valueOf(p8);
        com.a.b.b.cn.a(v0_1, "Invalid padding: %s", v1_1);
        if (p6.length < p7) {
            int v1_2 = (p7 + p8);
            byte[] v0_3 = new byte[v1_2];
            System.arraycopy(p6, 0, v0_3, 0, Math.min(p6.length, v1_2));
            p6 = v0_3;
        }
        return p6;
    }

    private static varargs byte[] a(byte[][] p7)
    {
        int v0_0 = 0;
        int v2_0 = 0;
        while (v0_0 < p7.length) {
            v2_0 += p7[v0_0].length;
            v0_0++;
        }
        byte[] v3_1 = new byte[v2_0];
        int v4_0 = p7.length;
        int v0_1 = 0;
        int v2_1 = 0;
        while (v2_1 < v4_0) {
            int v5_0 = p7[v2_1];
            System.arraycopy(v5_0, 0, v3_1, v0_1, v5_0.length);
            v0_1 += v5_0.length;
            v2_1++;
        }
        return v3_1;
    }

    private static int b(byte[] p2, byte p3)
    {
        return com.a.b.l.d.a(p2, p3, 0, p2.length);
    }

    static int b(byte[] p2, byte p3, int p4, int p5)
    {
        int v0 = (p5 - 1);
        while (v0 >= p4) {
            if (p2[v0] != p3) {
                v0--;
            }
            return v0;
        }
        v0 = -1;
        return v0;
    }

    private static int c(byte[] p2, byte p3)
    {
        return com.a.b.l.d.b(p2, p3, 0, p2.length);
    }

    private static synthetic int c(byte[] p1, byte p2, int p3, int p4)
    {
        return com.a.b.l.d.a(p1, p2, p3, p4);
    }

    private static synthetic int d(byte[] p1, byte p2, int p3, int p4)
    {
        return com.a.b.l.d.b(p1, p2, p3, p4);
    }
}
