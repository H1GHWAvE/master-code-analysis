package com.a.b.l;
final enum class p extends java.lang.Enum implements java.util.Comparator {
    public static final enum com.a.b.l.p a;
    private static final synthetic com.a.b.l.p[] b;

    static p()
    {
        com.a.b.l.p.a = new com.a.b.l.p("INSTANCE");
        com.a.b.l.p[] v0_3 = new com.a.b.l.p[1];
        v0_3[0] = com.a.b.l.p.a;
        com.a.b.l.p.b = v0_3;
        return;
    }

    private p(String p2)
    {
        this(p2, 0);
        return;
    }

    private static int a(float[] p4, float[] p5)
    {
        int v2 = Math.min(p4.length, p5.length);
        int v1_1 = 0;
        while (v1_1 < v2) {
            int v0_3 = Float.compare(p4[v1_1], p5[v1_1]);
            if (v0_3 == 0) {
                v1_1++;
            }
            return v0_3;
        }
        v0_3 = (p4.length - p5.length);
        return v0_3;
    }

    public static com.a.b.l.p valueOf(String p1)
    {
        return ((com.a.b.l.p) Enum.valueOf(com.a.b.l.p, p1));
    }

    public static com.a.b.l.p[] values()
    {
        return ((com.a.b.l.p[]) com.a.b.l.p.b.clone());
    }

    public final synthetic int compare(Object p5, Object p6)
    {
        int v2 = Math.min(((float[]) p5).length, ((float[]) p6).length);
        int v1_1 = 0;
        while (v1_1 < v2) {
            int v0_3 = Float.compare(((float[]) p5)[v1_1], ((float[]) p6)[v1_1]);
            if (v0_3 == 0) {
                v1_1++;
            }
            return v0_3;
        }
        v0_3 = (((float[]) p5).length - ((float[]) p6).length);
        return v0_3;
    }
}
