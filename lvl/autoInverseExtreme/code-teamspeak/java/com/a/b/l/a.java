package com.a.b.l;
public final class a {

    private a()
    {
        return;
    }

    private static int a(boolean p1)
    {
        int v0;
        if (!p1) {
            v0 = 1237;
        } else {
            v0 = 1231;
        }
        return v0;
    }

    public static int a(boolean p1, boolean p2)
    {
        int v0;
        if (p1 != p2) {
            if (!p1) {
                v0 = -1;
            } else {
                v0 = 1;
            }
        } else {
            v0 = 0;
        }
        return v0;
    }

    static int a(boolean[] p2, boolean p3, int p4, int p5)
    {
        int v0 = p4;
        while (v0 < p5) {
            if (p2[v0] != p3) {
                v0++;
            }
            return v0;
        }
        v0 = -1;
        return v0;
    }

    private static int a(boolean[] p5, boolean[] p6)
    {
        int v1 = 0;
        com.a.b.b.cn.a(p5, "array");
        com.a.b.b.cn.a(p6, "target");
        if (p6.length != 0) {
            int v0_3 = 0;
            while (v0_3 < ((p5.length - p6.length) + 1)) {
                int v2_3 = 0;
                while (v2_3 < p6.length) {
                    if (p5[(v0_3 + v2_3)] != p6[v2_3]) {
                        v0_3++;
                    } else {
                        v2_3++;
                    }
                }
                v1 = v0_3;
            }
            v1 = -1;
        }
        return v1;
    }

    private static varargs String a(String p4, boolean[] p5)
    {
        String v0_6;
        com.a.b.b.cn.a(p4);
        if (p5.length != 0) {
            StringBuilder v1_1 = new StringBuilder((p5.length * 7));
            v1_1.append(p5[0]);
            String v0_5 = 1;
            while (v0_5 < p5.length) {
                v1_1.append(p4).append(p5[v0_5]);
                v0_5++;
            }
            v0_6 = v1_1.toString();
        } else {
            v0_6 = "";
        }
        return v0_6;
    }

    private static java.util.Comparator a()
    {
        return com.a.b.l.c.a;
    }

    private static varargs java.util.List a(boolean[] p1)
    {
        com.a.b.l.b v0_2;
        if (p1.length != 0) {
            v0_2 = new com.a.b.l.b(p1);
        } else {
            v0_2 = java.util.Collections.emptyList();
        }
        return v0_2;
    }

    private static boolean a(boolean[] p4, boolean p5)
    {
        int v0 = 0;
        int v1 = 0;
        while (v1 < p4.length) {
            if (p4[v1] != p5) {
                v1++;
            } else {
                v0 = 1;
                break;
            }
        }
        return v0;
    }

    private static boolean[] a(java.util.Collection p5)
    {
        boolean[] v0_1;
        int v2 = 0;
        if (!(p5 instanceof com.a.b.l.b)) {
            Object[] v3_0 = p5.toArray();
            int v4_0 = v3_0.length;
            boolean[] v1_0 = new boolean[v4_0];
            while (v2 < v4_0) {
                v1_0[v2] = ((Boolean) com.a.b.b.cn.a(v3_0[v2])).booleanValue();
                v2++;
            }
            v0_1 = v1_0;
        } else {
            boolean[] v1_1 = ((com.a.b.l.b) p5).size();
            v0_1 = new boolean[v1_1];
            System.arraycopy(((com.a.b.l.b) p5).a, ((com.a.b.l.b) p5).b, v0_1, 0, v1_1);
        }
        return v0_1;
    }

    private static boolean[] a(boolean[] p3, int p4)
    {
        boolean[] v0 = new boolean[p4];
        System.arraycopy(p3, 0, v0, 0, Math.min(p3.length, p4));
        return v0;
    }

    private static boolean[] a(boolean[] p6, int p7, int p8)
    {
        boolean[] v0_0;
        if (p7 < 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        boolean[] v0_1;
        Integer v4_0 = new Object[1];
        v4_0[0] = Integer.valueOf(p7);
        com.a.b.b.cn.a(v0_0, "Invalid minLength: %s", v4_0);
        if (p8 < 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        int v1_1 = new Object[1];
        v1_1[0] = Integer.valueOf(p8);
        com.a.b.b.cn.a(v0_1, "Invalid padding: %s", v1_1);
        if (p6.length < p7) {
            int v1_2 = (p7 + p8);
            boolean[] v0_3 = new boolean[v1_2];
            System.arraycopy(p6, 0, v0_3, 0, Math.min(p6.length, v1_2));
            p6 = v0_3;
        }
        return p6;
    }

    private static varargs boolean[] a(boolean[][] p7)
    {
        int v0_0 = 0;
        int v2_0 = 0;
        while (v0_0 < p7.length) {
            v2_0 += p7[v0_0].length;
            v0_0++;
        }
        boolean[] v3_1 = new boolean[v2_0];
        int v4_0 = p7.length;
        int v0_1 = 0;
        int v2_1 = 0;
        while (v2_1 < v4_0) {
            int v5_0 = p7[v2_1];
            System.arraycopy(v5_0, 0, v3_1, v0_1, v5_0.length);
            v0_1 += v5_0.length;
            v2_1++;
        }
        return v3_1;
    }

    private static varargs int b(boolean[] p4)
    {
        int v0 = 0;
        int v1 = 0;
        while (v1 < p4.length) {
            if (p4[v1]) {
                v0++;
            }
            v1++;
        }
        return v0;
    }

    private static int b(boolean[] p2, boolean p3)
    {
        return com.a.b.l.a.a(p2, p3, 0, p2.length);
    }

    static int b(boolean[] p2, boolean p3, int p4, int p5)
    {
        int v0 = (p5 - 1);
        while (v0 >= p4) {
            if (p2[v0] != p3) {
                v0--;
            }
            return v0;
        }
        v0 = -1;
        return v0;
    }

    private static int c(boolean[] p2, boolean p3)
    {
        return com.a.b.l.a.b(p2, p3, 0, p2.length);
    }

    private static synthetic int c(boolean[] p1, boolean p2, int p3, int p4)
    {
        return com.a.b.l.a.a(p1, p2, p3, p4);
    }

    private static synthetic int d(boolean[] p1, boolean p2, int p3, int p4)
    {
        return com.a.b.l.a.b(p1, p2, p3, p4);
    }
}
