package com.a.b.l;
public final class ap {
    public static final long a = 255;
    private static final long[] b;
    private static final int[] c;
    private static final int[] d;

    static ap()
    {
        int v0_0 = new long[37];
        com.a.b.l.ap.b = v0_0;
        int v0_1 = new int[37];
        com.a.b.l.ap.c = v0_1;
        int v0_2 = new int[37];
        com.a.b.l.ap.d = v0_2;
        java.math.BigInteger v1_2 = new java.math.BigInteger("10000000000000000", 16);
        int v0_4 = 2;
        while (v0_4 <= 36) {
            com.a.b.l.ap.b[v0_4] = com.a.b.l.ap.b(-1, ((long) v0_4));
            com.a.b.l.ap.c[v0_4] = ((int) com.a.b.l.ap.c(-1, ((long) v0_4)));
            com.a.b.l.ap.d[v0_4] = (v1_2.toString(v0_4).length() - 1);
            v0_4++;
        }
        return;
    }

    private ap()
    {
        return;
    }

    public static int a(long p4, long p6)
    {
        return com.a.b.l.u.a((p4 ^ -0.0), (-0.0 ^ p6));
    }

    private static long a(String p2)
    {
        return com.a.b.l.ap.a(p2, 10);
    }

    public static long a(String p12, int p13)
    {
        com.a.b.b.cn.a(p12);
        if (p12.length() != 0) {
            if ((p13 >= 2) && (p13 <= 36)) {
                int v7 = (com.a.b.l.ap.d[p13] - 1);
                String v0_5 = 0;
                String v2_0 = 0;
                while (v0_5 < p12.length()) {
                    long v8_0 = Character.digit(p12.charAt(v0_5), p13);
                    if (v8_0 != -1) {
                        if (v0_5 > v7) {
                            NumberFormatException v6_10;
                            if (v2_0 < 0) {
                                v6_10 = 1;
                            } else {
                                if (v2_0 >= com.a.b.l.ap.b[p13]) {
                                    if ((v2_0 > com.a.b.l.ap.b[p13]) || (v8_0 > com.a.b.l.ap.c[p13])) {
                                    } else {
                                        v6_10 = 0;
                                    }
                                } else {
                                    v6_10 = 0;
                                }
                            }
                            if (v6_10 != null) {
                                String v0_8;
                                String v0_6 = String.valueOf(p12);
                                if (v0_6.length() == 0) {
                                    v0_8 = new String("Too large for unsigned long: ");
                                } else {
                                    v0_8 = "Too large for unsigned long: ".concat(v0_6);
                                }
                                throw new NumberFormatException(v0_8);
                            }
                        }
                        v2_0 = ((v2_0 * ((long) p13)) + ((long) v8_0));
                        v0_5++;
                    } else {
                        throw new NumberFormatException(p12);
                    }
                }
                return v2_0;
            } else {
                throw new NumberFormatException(new StringBuilder(26).append("illegal radix: ").append(p13).toString());
            }
        } else {
            throw new NumberFormatException("empty string");
        }
    }

    private static varargs long a(long[] p8)
    {
        long v0_1;
        int v1 = 1;
        if (p8.length <= 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.a(v0_1);
        long v2_2 = (p8[0] ^ -0.0);
        while (v1 < p8.length) {
            long v4_1 = (p8[v1] ^ -0.0);
            if (v4_1 < v2_2) {
                v2_2 = v4_1;
            }
            v1++;
        }
        return (v2_2 ^ -0.0);
    }

    public static String a(long p2)
    {
        return com.a.b.l.ap.a(p2, 10);
    }

    public static String a(long p10, int p12)
    {
        if ((p12 < 2) || (p12 > 36)) {
            String v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        String v0_9;
        int v1_1 = new Object[1];
        v1_1[0] = Integer.valueOf(p12);
        com.a.b.b.cn.a(v0_2, "radix (%s) must be between Character.MIN_RADIX and Character.MAX_RADIX", v1_1);
        if (p10 != 0) {
            int v1_2;
            char[] v2_1 = new char[64];
            if (p10 >= 0) {
                v1_2 = 64;
            } else {
                String v0_6 = com.a.b.l.ap.b(p10, ((long) p12));
                v2_1[63] = Character.forDigit(((int) (p10 - (((long) p12) * v0_6))), p12);
                p10 = v0_6;
                v1_2 = 63;
            }
            while (p10 > 0) {
                String v0_10 = (v1_2 - 1);
                v2_1[v0_10] = Character.forDigit(((int) (p10 % ((long) p12))), p12);
                p10 /= ((long) p12);
                v1_2 = v0_10;
            }
            v0_9 = new String(v2_1, v1_2, (64 - v1_2));
        } else {
            v0_9 = "0";
        }
        return v0_9;
    }

    private static varargs String a(String p6, long[] p7)
    {
        String v0_6;
        com.a.b.b.cn.a(p6);
        if (p7.length != 0) {
            StringBuilder v1_1 = new StringBuilder((p7.length * 5));
            v1_1.append(com.a.b.l.ap.a(p7[0]));
            String v0_5 = 1;
            while (v0_5 < p7.length) {
                v1_1.append(p6).append(com.a.b.l.ap.a(p7[v0_5]));
                v0_5++;
            }
            v0_6 = v1_1.toString();
        } else {
            v0_6 = "";
        }
        return v0_6;
    }

    private static java.util.Comparator a()
    {
        return com.a.b.l.aq.a;
    }

    private static boolean a(long p4, int p6, int p7)
    {
        int v0 = 0;
        if (p4 < 0) {
            v0 = 1;
        } else {
            if (p4 >= com.a.b.l.ap.b[p7]) {
                if (p4 <= com.a.b.l.ap.b[p7]) {
                    if (p6 > com.a.b.l.ap.c[p7]) {
                        v0 = 1;
                    }
                } else {
                    v0 = 1;
                }
            }
        }
        return v0;
    }

    private static long b(long p2)
    {
        return (-0.0 ^ p2);
    }

    public static long b(long p6, long p8)
    {
        int v0_2;
        int v0_0 = 1;
        if (p8 >= 0) {
            if (p6 < 0) {
                long v2_3 = (((p6 >> 1) / p8) << 1);
                if (com.a.b.l.ap.a((p6 - (v2_3 * p8)), p8) < 0) {
                    v0_0 = 0;
                }
                v0_2 = (((long) v0_0) + v2_3);
            } else {
                v0_2 = (p6 / p8);
            }
        } else {
            if (com.a.b.l.ap.a(p6, p8) >= 0) {
                v0_2 = 1;
            } else {
                v0_2 = 0;
            }
        }
        return v0_2;
    }

    private static long b(String p5)
    {
        String v0_0 = com.a.b.l.y.a(p5);
        try {
            return com.a.b.l.ap.a(v0_0.a, v0_0.b);
        } catch (String v0_3) {
            NumberFormatException v1_1 = v0_3;
            String v0_4 = String.valueOf(p5);
            if (v0_4.length() == 0) {
                String v0_6 = new String("Error parsing value: ");
            } else {
                v0_6 = "Error parsing value: ".concat(v0_4);
            }
            NumberFormatException v2_1 = new NumberFormatException(v0_6);
            v2_1.initCause(v1_1);
            throw v2_1;
        }
    }

    private static varargs long b(long[] p8)
    {
        long v0_1;
        int v1 = 1;
        if (p8.length <= 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.a(v0_1);
        long v2_2 = (p8[0] ^ -0.0);
        while (v1 < p8.length) {
            long v4_1 = (p8[v1] ^ -0.0);
            if (v4_1 > v2_2) {
                v2_2 = v4_1;
            }
            v1++;
        }
        return (v2_2 ^ -0.0);
    }

    public static long c(long p6, long p8)
    {
        if (p8 >= 0) {
            if (p6 < 0) {
                long v2_6 = (p6 - ((((p6 >> 1) / p8) << 1) * p8));
                if (com.a.b.l.ap.a(v2_6, p8) < 0) {
                    p8 = 0;
                }
                p6 = (v2_6 - p8);
            } else {
                p6 %= p8;
            }
        } else {
            if (com.a.b.l.ap.a(p6, p8) >= 0) {
                p6 -= p8;
            }
        }
        return p6;
    }
}
