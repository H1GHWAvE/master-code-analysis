package com.a.b.l;
final enum class aj extends java.lang.Enum implements java.util.Comparator {
    public static final enum com.a.b.l.aj a;
    static final boolean b;
    static final sun.misc.Unsafe c;
    static final int d;
    private static final synthetic com.a.b.l.aj[] e;

    static aj()
    {
        com.a.b.l.aj.a = new com.a.b.l.aj("INSTANCE");
        AssertionError v0_2 = new com.a.b.l.aj[1];
        v0_2[0] = com.a.b.l.aj.a;
        com.a.b.l.aj.e = v0_2;
        com.a.b.l.aj.b = java.nio.ByteOrder.nativeOrder().equals(java.nio.ByteOrder.BIG_ENDIAN);
        AssertionError v0_5 = com.a.b.l.aj.a();
        com.a.b.l.aj.c = v0_5;
        com.a.b.l.aj.d = v0_5.arrayBaseOffset(byte[]);
        if (com.a.b.l.aj.c.arrayIndexScale(byte[]) == 1) {
            return;
        } else {
            throw new AssertionError();
        }
    }

    private aj(String p2)
    {
        this(p2, 0);
        return;
    }

    private static int a(byte[] p12, byte[] p13)
    {
        long v2_0 = Math.min(p12.length, p13.length);
        int v1_1 = (v2_0 / 8);
        int v0_1 = 0;
        while (v0_1 < (v1_1 * 8)) {
            long v4_3 = com.a.b.l.aj.c.getLong(p12, (((long) com.a.b.l.aj.d) + ((long) v0_1)));
            long v6_4 = com.a.b.l.aj.c.getLong(p13, (((long) com.a.b.l.aj.d) + ((long) v0_1)));
            if (v4_3 == v6_4) {
                v0_1 += 8;
            } else {
                if (!com.a.b.l.aj.b) {
                    int v0_10 = (Long.numberOfTrailingZeros((v4_3 ^ v6_4)) & -8);
                    int v0_4 = ((int) (((v4_3 >> v0_10) & 255) - ((v6_4 >> v0_10) & 255)));
                } else {
                    v0_4 = com.a.b.l.ap.a(v4_3, v6_4);
                }
            }
            return v0_4;
        }
        int v1_2 = (v1_1 * 8);
        while (v1_2 < v2_0) {
            v0_4 = com.a.b.l.ag.a(p12[v1_2], p13[v1_2]);
            if (v0_4 == 0) {
                v1_2++;
            }
            return v0_4;
        }
        v0_4 = (p12.length - p13.length);
        return v0_4;
    }

    private static sun.misc.Unsafe a()
    {
        try {
            Throwable v0_0 = sun.misc.Unsafe.getUnsafe();
        } catch (Throwable v0) {
            try {
                v0_0 = ((sun.misc.Unsafe) java.security.AccessController.doPrivileged(new com.a.b.l.ak()));
            } catch (Throwable v0_4) {
                throw new RuntimeException("Could not initialize intrinsics", v0_4.getCause());
            }
        }
        return v0_0;
    }

    public static com.a.b.l.aj valueOf(String p1)
    {
        return ((com.a.b.l.aj) Enum.valueOf(com.a.b.l.aj, p1));
    }

    public static com.a.b.l.aj[] values()
    {
        return ((com.a.b.l.aj[]) com.a.b.l.aj.e.clone());
    }

    public final synthetic int compare(Object p13, Object p14)
    {
        long v2_0 = Math.min(((byte[]) p13).length, ((byte[]) p14).length);
        int v1_1 = (v2_0 / 8);
        int v0_1 = 0;
        while (v0_1 < (v1_1 * 8)) {
            long v4_3 = com.a.b.l.aj.c.getLong(((byte[]) p13), (((long) com.a.b.l.aj.d) + ((long) v0_1)));
            long v6_4 = com.a.b.l.aj.c.getLong(((byte[]) p14), (((long) com.a.b.l.aj.d) + ((long) v0_1)));
            if (v4_3 == v6_4) {
                v0_1 += 8;
            } else {
                if (!com.a.b.l.aj.b) {
                    int v0_10 = (Long.numberOfTrailingZeros((v4_3 ^ v6_4)) & -8);
                    int v0_4 = ((int) (((v4_3 >> v0_10) & 255) - ((v6_4 >> v0_10) & 255)));
                } else {
                    v0_4 = com.a.b.l.ap.a(v4_3, v6_4);
                }
            }
            return v0_4;
        }
        int v1_2 = (v1_1 * 8);
        while (v1_2 < v2_0) {
            v0_4 = com.a.b.l.ag.a(((byte[]) p13)[v1_2], ((byte[]) p14)[v1_2]);
            if (v0_4 == 0) {
                v1_2++;
            }
            return v0_4;
        }
        v0_4 = (((byte[]) p13).length - ((byte[]) p14).length);
        return v0_4;
    }
}
