package com.a.b.f;
final class b implements com.a.b.f.r {
    private static final com.a.b.c.an a;

    static b()
    {
        com.a.b.f.b.a = com.a.b.c.f.a().a(com.a.b.c.bw.c).a(new com.a.b.f.c());
        return;
    }

    b()
    {
        return;
    }

    static synthetic com.a.b.d.jl a(Class p9)
    {
        IllegalArgumentException v0_2 = com.a.b.m.ae.a(p9).b().d();
        String v1_0 = com.a.b.d.sz.c();
        String v2_0 = v0_2.iterator();
        while (v2_0.hasNext()) {
            String v3_0 = ((Class) v2_0.next()).getMethods();
            String v4_0 = v3_0.length;
            IllegalArgumentException v0_8 = 0;
            while (v0_8 < v4_0) {
                reflect.Method v5 = v3_0[v0_8];
                if ((v5.isAnnotationPresent(com.a.b.f.o)) && (!v5.isBridge())) {
                    com.a.b.f.d v6_3 = v5.getParameterTypes();
                    if (v6_3.length == 1) {
                        com.a.b.f.d v6_5 = new com.a.b.f.d(v5);
                        if (!v1_0.containsKey(v6_5)) {
                            v1_0.put(v6_5, v5);
                        }
                    } else {
                        String v1_2 = String.valueOf(String.valueOf(v5));
                        throw new IllegalArgumentException(new StringBuilder((v1_2.length() + 128)).append("Method ").append(v1_2).append(" has @Subscribe annotation, but requires ").append(v6_3.length).append(" arguments.  Event subscriber methods must require a single argument.").toString());
                    }
                }
                v0_8++;
            }
        }
        return com.a.b.d.jl.a(v1_0.values());
    }

    private static com.a.b.f.n a(Object p1, reflect.Method p2)
    {
        com.a.b.f.s v0_2;
        if (p2.getAnnotation(com.a.b.f.a) == null) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        com.a.b.f.s v0_4;
        if (v0_2 == null) {
            v0_4 = new com.a.b.f.s(p1, p2);
        } else {
            v0_4 = new com.a.b.f.n(p1, p2);
        }
        return v0_4;
    }

    private static boolean a(reflect.Method p1)
    {
        int v0_2;
        if (p1.getAnnotation(com.a.b.f.a) == null) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private static com.a.b.d.jl b(Class p1)
    {
        try {
            return ((com.a.b.d.jl) com.a.b.f.b.a.b(p1));
        } catch (RuntimeException v0_3) {
            throw com.a.b.b.ei.a(v0_3.getCause());
        }
    }

    private static com.a.b.d.jl c(Class p9)
    {
        IllegalArgumentException v0_2 = com.a.b.m.ae.a(p9).b().d();
        String v1_0 = com.a.b.d.sz.c();
        String v2_0 = v0_2.iterator();
        while (v2_0.hasNext()) {
            String v3_0 = ((Class) v2_0.next()).getMethods();
            String v4_0 = v3_0.length;
            IllegalArgumentException v0_8 = 0;
            while (v0_8 < v4_0) {
                reflect.Method v5 = v3_0[v0_8];
                if ((v5.isAnnotationPresent(com.a.b.f.o)) && (!v5.isBridge())) {
                    com.a.b.f.d v6_3 = v5.getParameterTypes();
                    if (v6_3.length == 1) {
                        com.a.b.f.d v6_5 = new com.a.b.f.d(v5);
                        if (!v1_0.containsKey(v6_5)) {
                            v1_0.put(v6_5, v5);
                        }
                    } else {
                        String v1_2 = String.valueOf(String.valueOf(v5));
                        throw new IllegalArgumentException(new StringBuilder((v1_2.length() + 128)).append("Method ").append(v1_2).append(" has @Subscribe annotation, but requires ").append(v6_3.length).append(" arguments.  Event subscriber methods must require a single argument.").toString());
                    }
                }
                v0_8++;
            }
        }
        return com.a.b.d.jl.a(v1_0.values());
    }

    public final com.a.b.d.vi a(Object p7)
    {
        com.a.b.d.io v3 = com.a.b.d.io.v();
        java.util.Iterator v4 = com.a.b.f.b.b(p7.getClass()).iterator();
        while (v4.hasNext()) {
            com.a.b.f.s v1_3;
            com.a.b.f.s v0_4 = ((reflect.Method) v4.next());
            if (v0_4.getAnnotation(com.a.b.f.a) == null) {
                v1_3 = 0;
            } else {
                v1_3 = 1;
            }
            com.a.b.f.s v0_5;
            if (v1_3 == null) {
                v0_5 = new com.a.b.f.s(p7, v0_4);
            } else {
                v0_5 = new com.a.b.f.n(p7, v0_4);
            }
            v3.a(v0_4.getParameterTypes()[0], v0_5);
        }
        return v3;
    }
}
