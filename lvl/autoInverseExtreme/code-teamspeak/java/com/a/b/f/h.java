package com.a.b.f;
public class h {
    private static final com.a.b.c.an a;
    private final com.a.b.d.aac b;
    private final java.util.concurrent.locks.ReadWriteLock c;
    private final com.a.b.f.r d;
    private final ThreadLocal e;
    private final ThreadLocal f;
    private com.a.b.f.q g;

    static h()
    {
        com.a.b.f.h.a = com.a.b.c.f.a().a(com.a.b.c.bw.c).a(new com.a.b.f.i());
        return;
    }

    public h()
    {
        this("default");
        return;
    }

    public h(com.a.b.f.q p2)
    {
        this.b = com.a.b.d.io.v();
        this.c = new java.util.concurrent.locks.ReentrantReadWriteLock();
        this.d = new com.a.b.f.b();
        this.e = new com.a.b.f.j(this);
        this.f = new com.a.b.f.k(this);
        this.g = ((com.a.b.f.q) com.a.b.b.cn.a(p2));
        return;
    }

    public h(String p2)
    {
        this(new com.a.b.f.m(p2));
        return;
    }

    private static java.util.Set a(Class p1)
    {
        try {
            return ((java.util.Set) com.a.b.f.h.a.b(p1));
        } catch (RuntimeException v0_3) {
            throw com.a.b.b.ei.a(v0_3.getCause());
        }
    }

    private void a(Object p3)
    {
        Throwable v0_1 = this.d.a(p3);
        this.c.writeLock().lock();
        try {
            this.b.a(v0_1);
            this.c.writeLock().unlock();
            return;
        } catch (Throwable v0_4) {
            this.c.writeLock().unlock();
            throw v0_4;
        }
    }

    private void b(Object p5)
    {
        String v2_0 = this.d.a(p5).b().entrySet().iterator();
        while (v2_0.hasNext()) {
            java.util.concurrent.locks.Lock v0_6 = ((java.util.Map$Entry) v2_0.next());
            String v1_1 = ((Class) v0_6.getKey());
            java.util.concurrent.locks.Lock v0_8 = ((java.util.Collection) v0_6.getValue());
            this.c.writeLock().lock();
            try {
                String v1_2 = this.b.a(v1_1);
            } catch (java.util.concurrent.locks.Lock v0_13) {
                this.c.writeLock().unlock();
                throw v0_13;
            }
            if (v1_2.containsAll(v0_8)) {
                v1_2.removeAll(v0_8);
                this.c.writeLock().unlock();
            } else {
                String v1_4 = String.valueOf(String.valueOf(p5));
                throw new IllegalArgumentException(new StringBuilder((v1_4.length() + 65)).append("missing event subscriber for an annotated method. Is ").append(v1_4).append(" registered?").toString());
            }
        }
        return;
    }

    private void c(Object p4)
    {
        java.util.concurrent.locks.Lock v1_0 = com.a.b.f.h.a(p4.getClass()).iterator();
        while (v1_0.hasNext()) {
            com.a.b.f.n v0_4 = ((Class) v1_0.next());
            this.c.readLock().lock();
            try {
                com.a.b.f.n v0_5 = this.b.a(v0_4);
            } catch (com.a.b.f.n v0_7) {
                this.c.readLock().unlock();
                throw v0_7;
            }
            if (!v0_5.isEmpty()) {
                java.util.Iterator v2_4 = v0_5.iterator();
                while (v2_4.hasNext()) {
                    this.a(p4, ((com.a.b.f.n) v2_4.next()));
                }
            }
            this.c.readLock().unlock();
        }
        this.a();
        return;
    }

    void a()
    {
        if (!((Boolean) this.f.get()).booleanValue()) {
            this.f.set(Boolean.valueOf(1));
            try {
                ThreadLocal v0_7 = ((java.util.Queue) this.e.get());
            } catch (ThreadLocal v0_8) {
                this.f.remove();
                this.e.remove();
                throw v0_8;
            }
            while(true) {
                com.a.b.f.n v1_3 = ((com.a.b.f.l) v0_7.poll());
                if (v1_3 == null) {
                    break;
                }
                this.b(v1_3.a, v1_3.b);
            }
            this.f.remove();
            this.e.remove();
        }
        return;
    }

    void a(Object p3, com.a.b.f.n p4)
    {
        ((java.util.Queue) this.e.get()).offer(new com.a.b.f.l(p3, p4));
        return;
    }

    void b(Object p8, com.a.b.f.n p9)
    {
        try {
            p9.a(p8);
        } catch (String v1_0) {
            try {
                this.g.a(v1_0.getCause(), new com.a.b.f.p(this, p8, p9.a, p9.b));
            } catch (Throwable v0_1) {
                java.util.logging.Logger v2_3 = java.util.logging.Logger.getLogger(com.a.b.f.h.getName());
                Object[] v5_2 = new Object[2];
                v5_2[0] = v0_1;
                v5_2[1] = v1_0.getCause();
                v2_3.log(java.util.logging.Level.SEVERE, String.format("Exception %s thrown while handling exception: %s", v5_2), v0_1);
            }
        }
        return;
    }
}
