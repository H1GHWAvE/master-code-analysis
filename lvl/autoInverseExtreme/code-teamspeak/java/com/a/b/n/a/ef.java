package com.a.b.n.a;
public final enum class ef extends java.lang.Enum implements java.util.concurrent.Executor {
    public static final enum com.a.b.n.a.ef a;
    private static final synthetic com.a.b.n.a.ef[] b;

    static ef()
    {
        com.a.b.n.a.ef.a = new com.a.b.n.a.ef("INSTANCE");
        com.a.b.n.a.ef[] v0_3 = new com.a.b.n.a.ef[1];
        v0_3[0] = com.a.b.n.a.ef.a;
        com.a.b.n.a.ef.b = v0_3;
        return;
    }

    private ef(String p2)
    {
        this(p2, 0);
        return;
    }

    public static com.a.b.n.a.ef valueOf(String p1)
    {
        return ((com.a.b.n.a.ef) Enum.valueOf(com.a.b.n.a.ef, p1));
    }

    public static com.a.b.n.a.ef[] values()
    {
        return ((com.a.b.n.a.ef[]) com.a.b.n.a.ef.b.clone());
    }

    public final void execute(Runnable p1)
    {
        p1.run();
        return;
    }
}
