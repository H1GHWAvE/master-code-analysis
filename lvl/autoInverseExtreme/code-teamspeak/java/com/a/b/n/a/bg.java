package com.a.b.n.a;
final class bg extends java.util.concurrent.locks.ReentrantLock implements com.a.b.n.a.bf {
    final synthetic com.a.b.n.a.bd a;
    private final com.a.b.n.a.bl b;

    private bg(com.a.b.n.a.bd p2, com.a.b.n.a.bl p3)
    {
        this.a = p2;
        this(0);
        this.b = ((com.a.b.n.a.bl) com.a.b.b.cn.a(p3));
        return;
    }

    synthetic bg(com.a.b.n.a.bd p1, com.a.b.n.a.bl p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    public final com.a.b.n.a.bl a()
    {
        return this.b;
    }

    public final boolean b()
    {
        return this.isHeldByCurrentThread();
    }

    public final void lock()
    {
        com.a.b.n.a.bd.a(this.a, this);
        try {
            super.lock();
            com.a.b.n.a.bd.a(this);
            return;
        } catch (Throwable v0_1) {
            com.a.b.n.a.bd.a(this);
            throw v0_1;
        }
    }

    public final void lockInterruptibly()
    {
        com.a.b.n.a.bd.a(this.a, this);
        try {
            super.lockInterruptibly();
            com.a.b.n.a.bd.a(this);
            return;
        } catch (Throwable v0_1) {
            com.a.b.n.a.bd.a(this);
            throw v0_1;
        }
    }

    public final boolean tryLock()
    {
        com.a.b.n.a.bd.a(this.a, this);
        try {
            Throwable v0_1 = super.tryLock();
            com.a.b.n.a.bd.a(this);
            return v0_1;
        } catch (Throwable v0_2) {
            com.a.b.n.a.bd.a(this);
            throw v0_2;
        }
    }

    public final boolean tryLock(long p2, java.util.concurrent.TimeUnit p4)
    {
        com.a.b.n.a.bd.a(this.a, this);
        try {
            Throwable v0_1 = super.tryLock(p2, p4);
            com.a.b.n.a.bd.a(this);
            return v0_1;
        } catch (Throwable v0_2) {
            com.a.b.n.a.bd.a(this);
            throw v0_2;
        }
    }

    public final void unlock()
    {
        try {
            super.unlock();
            com.a.b.n.a.bd.a(this);
            return;
        } catch (Throwable v0) {
            com.a.b.n.a.bd.a(this);
            throw v0;
        }
    }
}
