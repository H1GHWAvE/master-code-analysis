package com.a.b.n.a;
final class gm implements java.util.concurrent.ThreadFactory {
    final synthetic java.util.concurrent.ThreadFactory a;
    final synthetic String b;
    final synthetic java.util.concurrent.atomic.AtomicLong c;
    final synthetic Boolean d;
    final synthetic Integer e;
    final synthetic Thread$UncaughtExceptionHandler f;

    gm(java.util.concurrent.ThreadFactory p1, String p2, java.util.concurrent.atomic.AtomicLong p3, Boolean p4, Integer p5, Thread$UncaughtExceptionHandler p6)
    {
        this.a = p1;
        this.b = p2;
        this.c = p3;
        this.d = p4;
        this.e = p5;
        this.f = p6;
        return;
    }

    public final Thread newThread(Runnable p7)
    {
        Thread v0_1 = this.a.newThread(p7);
        if (this.b != null) {
            Thread$UncaughtExceptionHandler v1_1 = this.b;
            Object[] v2_1 = new Object[1];
            v2_1[0] = Long.valueOf(this.c.getAndIncrement());
            v0_1.setName(String.format(v1_1, v2_1));
        }
        if (this.d != null) {
            v0_1.setDaemon(this.d.booleanValue());
        }
        if (this.e != null) {
            v0_1.setPriority(this.e.intValue());
        }
        if (this.f != null) {
            v0_1.setUncaughtExceptionHandler(this.f);
        }
        return v0_1;
    }
}
