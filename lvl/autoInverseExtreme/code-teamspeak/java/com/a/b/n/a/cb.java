package com.a.b.n.a;
public abstract class cb extends com.a.b.d.hg implements java.util.concurrent.Future {

    protected cb()
    {
        return;
    }

    protected abstract java.util.concurrent.Future b();

    public boolean cancel(boolean p2)
    {
        return this.b().cancel(p2);
    }

    public Object get()
    {
        return this.b().get();
    }

    public Object get(long p2, java.util.concurrent.TimeUnit p4)
    {
        return this.b().get(p2, p4);
    }

    public boolean isCancelled()
    {
        return this.b().isCancelled();
    }

    public boolean isDone()
    {
        return this.b().isDone();
    }

    protected synthetic Object k_()
    {
        return this.b();
    }
}
