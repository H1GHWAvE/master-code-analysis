package com.a.b.n.a;
final class ge extends com.a.b.n.a.gi {
    private final Object[] a;

    private ge(int p4, com.a.b.b.dz p5)
    {
        Object[] v0_1;
        int v1 = 0;
        this(p4);
        if (p4 > 1073741824) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.a(v0_1, "Stripes must be <= 2^30)");
        Object[] v0_4 = new Object[(this.d + 1)];
        this.a = v0_4;
        while (v1 < this.a.length) {
            this.a[v1] = p5.a();
            v1++;
        }
        return;
    }

    synthetic ge(int p1, com.a.b.b.dz p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    public final int a()
    {
        return this.a.length;
    }

    public final Object a(int p2)
    {
        return this.a[p2];
    }
}
