package com.a.b.n.a;
public abstract enum class ew extends java.lang.Enum {
    public static final enum com.a.b.n.a.ew a;
    public static final enum com.a.b.n.a.ew b;
    public static final enum com.a.b.n.a.ew c;
    public static final enum com.a.b.n.a.ew d;
    public static final enum com.a.b.n.a.ew e;
    public static final enum com.a.b.n.a.ew f;
    private static final synthetic com.a.b.n.a.ew[] g;

    static ew()
    {
        com.a.b.n.a.ew.a = new com.a.b.n.a.ex("NEW");
        com.a.b.n.a.ew.b = new com.a.b.n.a.ey("STARTING");
        com.a.b.n.a.ew.c = new com.a.b.n.a.ez("RUNNING");
        com.a.b.n.a.ew.d = new com.a.b.n.a.fa("STOPPING");
        com.a.b.n.a.ew.e = new com.a.b.n.a.fb("TERMINATED");
        com.a.b.n.a.ew.f = new com.a.b.n.a.fc("FAILED");
        com.a.b.n.a.ew[] v0_13 = new com.a.b.n.a.ew[6];
        v0_13[0] = com.a.b.n.a.ew.a;
        v0_13[1] = com.a.b.n.a.ew.b;
        v0_13[2] = com.a.b.n.a.ew.c;
        v0_13[3] = com.a.b.n.a.ew.d;
        v0_13[4] = com.a.b.n.a.ew.e;
        v0_13[5] = com.a.b.n.a.ew.f;
        com.a.b.n.a.ew.g = v0_13;
        return;
    }

    private ew(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    synthetic ew(String p1, int p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    public static com.a.b.n.a.ew valueOf(String p1)
    {
        return ((com.a.b.n.a.ew) Enum.valueOf(com.a.b.n.a.ew, p1));
    }

    public static com.a.b.n.a.ew[] values()
    {
        return ((com.a.b.n.a.ew[]) com.a.b.n.a.ew.g.clone());
    }

    abstract boolean a();
}
