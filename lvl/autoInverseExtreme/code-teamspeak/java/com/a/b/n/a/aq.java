package com.a.b.n.a;
final class aq extends com.a.b.n.a.cd {
    private final com.a.b.n.a.as a;
    private final com.a.b.n.a.dp b;

    private aq()
    {
        this.a = new com.a.b.n.a.as(0);
        this.b = com.a.b.n.a.ci.a(this.a);
        return;
    }

    public static com.a.b.n.a.aq a()
    {
        return new com.a.b.n.a.aq();
    }

    private boolean a(Object p2)
    {
        return this.a(com.a.b.n.a.ci.a(p2));
    }

    private boolean a(Throwable p2)
    {
        return this.a(com.a.b.n.a.ci.a(p2));
    }

    private boolean d()
    {
        return this.a.isDone();
    }

    public final boolean a(com.a.b.n.a.dp p3)
    {
        return this.a.a(((com.a.b.n.a.dp) com.a.b.b.cn.a(p3)));
    }

    protected final bridge synthetic java.util.concurrent.Future b()
    {
        return this.b;
    }

    protected final com.a.b.n.a.dp c()
    {
        return this.b;
    }

    protected final bridge synthetic Object k_()
    {
        return this.b;
    }
}
