package com.a.b.n.a;
public abstract class el {
    private final com.a.b.n.a.em a;
    private volatile Object b;

    el(com.a.b.n.a.em p2)
    {
        this.a = ((com.a.b.n.a.em) com.a.b.b.cn.a(p2));
        return;
    }

    private static com.a.b.n.a.el a(double p2)
    {
        return new com.a.b.n.a.fw(new com.a.b.n.a.en()).b(p2);
    }

    private static com.a.b.n.a.el a(double p6, long p8, java.util.concurrent.TimeUnit p10)
    {
        com.a.b.n.a.en v0_1;
        if (p8 < 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        void v1_1 = new Object[1];
        v1_1[0] = Long.valueOf(p8);
        com.a.b.b.cn.a(v0_1, "warmupPeriod must not be negative: %s", v1_1);
        return new com.a.b.n.a.fx(new com.a.b.n.a.en(), p8, p10).b(p6);
    }

    private static com.a.b.n.a.el a(com.a.b.n.a.em p1, double p2)
    {
        return new com.a.b.n.a.fw(p1).b(p2);
    }

    private static com.a.b.n.a.el a(com.a.b.n.a.em p1, double p2, long p4, java.util.concurrent.TimeUnit p6)
    {
        return new com.a.b.n.a.fx(p1, p4, p6).b(p2);
    }

    private boolean a(int p4)
    {
        return this.a(p4, 0, java.util.concurrent.TimeUnit.MICROSECONDS);
    }

    private boolean a(int p11, long p12, java.util.concurrent.TimeUnit p14)
    {
        int v0_0 = 0;
        long v2_1 = Math.max(p14.toMicros(p12), 0);
        com.a.b.n.a.el.b(p11);
        this.c();
        long v2_4;
        long v6 = this.a.a();
        if ((this.b() - v2_1) > v6) {
            v2_4 = 0;
        } else {
            v2_4 = 1;
        }
        if (v2_4 != 0) {
            this.a.a(this.b(p11, v6));
            v0_0 = 1;
        } else {
        }
        return v0_0;
    }

    private boolean a(long p4, long p6)
    {
        int v0_3;
        if ((this.b() - p6) > p4) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    private boolean a(long p2, java.util.concurrent.TimeUnit p4)
    {
        return this.a(1, p2, p4);
    }

    private static int b(int p5)
    {
        int v0;
        if (p5 <= 0) {
            v0 = 0;
        } else {
            v0 = 1;
        }
        Object[] v1_1 = new Object[1];
        v1_1[0] = Integer.valueOf(p5);
        com.a.b.b.cn.a(v0, "Requested permits (%s) must be positive", v1_1);
        return p5;
    }

    private long b(int p5, long p6)
    {
        return Math.max((this.a(p5, p6) - p6), 0);
    }

    private void b(double p6)
    {
        try {
            if ((p6 <= 0) || (Double.isNaN(p6))) {
                com.a.b.n.a.em v0_3 = 0;
            } else {
                v0_3 = 1;
            }
        } catch (com.a.b.n.a.em v0_5) {
            throw v0_5;
        }
        com.a.b.b.cn.a(v0_3, "rate must be positive");
        this.c();
        this.a(p6, this.a.a());
        return;
    }

    private Object c()
    {
        Object v0_0 = this.b;
        try {
            if (v0_0 == null) {
                v0_0 = this.b;
                if (v0_0 == null) {
                    v0_0 = new Object();
                    this.b = v0_0;
                }
            }
        } catch (Object v0_2) {
            throw v0_2;
        }
        return v0_0;
    }

    private double d()
    {
        this.c();
        try {
            return this.a();
        } catch (Throwable v0) {
            throw v0;
        }
    }

    private double e()
    {
        double v0_0 = this.g();
        this.a.a(v0_0);
        return ((((double) v0_0) * 1.0) / ((double) java.util.concurrent.TimeUnit.SECONDS.toMicros(1)));
    }

    private double f()
    {
        double v0_0 = this.g();
        this.a.a(v0_0);
        return ((((double) v0_0) * 1.0) / ((double) java.util.concurrent.TimeUnit.SECONDS.toMicros(1)));
    }

    private long g()
    {
        com.a.b.n.a.el.b(1);
        this.c();
        try {
            return this.b(1, this.a.a());
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    private boolean h()
    {
        return this.a(1, 0, java.util.concurrent.TimeUnit.MICROSECONDS);
    }

    abstract double a();

    abstract long a();

    abstract void a();

    abstract long b();

    public String toString()
    {
        Object[] v1_1 = new Object[1];
        v1_1[0] = Double.valueOf(this.d());
        return String.format("RateLimiter[stableRate=%3.1fqps]", v1_1);
    }
}
