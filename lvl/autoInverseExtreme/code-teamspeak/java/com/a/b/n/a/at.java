package com.a.b.n.a;
public class at extends java.lang.Number implements java.io.Serializable {
    private static final long a;
    private static final java.util.concurrent.atomic.AtomicLongFieldUpdater c;
    private transient volatile long b;

    static at()
    {
        com.a.b.n.a.at.c = java.util.concurrent.atomic.AtomicLongFieldUpdater.newUpdater(com.a.b.n.a.at, "b");
        return;
    }

    public at()
    {
        return;
    }

    private at(double p4)
    {
        this.b = Double.doubleToRawLongBits(p4);
        return;
    }

    private double a()
    {
        return Double.longBitsToDouble(this.b);
    }

    private void a(double p4)
    {
        this.b = Double.doubleToRawLongBits(p4);
        return;
    }

    private void a(java.io.ObjectInputStream p3)
    {
        p3.defaultReadObject();
        this.a(p3.readDouble());
        return;
    }

    private void a(java.io.ObjectOutputStream p3)
    {
        p3.defaultWriteObject();
        p3.writeDouble(Double.longBitsToDouble(this.b));
        return;
    }

    private boolean a(double p8, double p10)
    {
        return com.a.b.n.a.at.c.compareAndSet(this, Double.doubleToRawLongBits(p8), Double.doubleToRawLongBits(p10));
    }

    private void b(double p2)
    {
        this.a(p2);
        return;
    }

    private boolean b(double p8, double p10)
    {
        return com.a.b.n.a.at.c.weakCompareAndSet(this, Double.doubleToRawLongBits(p8), Double.doubleToRawLongBits(p10));
    }

    private double c(double p4)
    {
        return Double.longBitsToDouble(com.a.b.n.a.at.c.getAndSet(this, Double.doubleToRawLongBits(p4)));
    }

    private double d(double p10)
    {
        do {
            long v2 = this.b;
            double v6 = Double.longBitsToDouble(v2);
        } while(!com.a.b.n.a.at.c.compareAndSet(this, v2, Double.doubleToRawLongBits((v6 + p10))));
        return v6;
    }

    private double e(double p10)
    {
        do {
            long v2 = this.b;
            double v6 = (Double.longBitsToDouble(v2) + p10);
        } while(!com.a.b.n.a.at.c.compareAndSet(this, v2, Double.doubleToRawLongBits(v6)));
        return v6;
    }

    public double doubleValue()
    {
        return Double.longBitsToDouble(this.b);
    }

    public float floatValue()
    {
        return ((float) Double.longBitsToDouble(this.b));
    }

    public int intValue()
    {
        return ((int) Double.longBitsToDouble(this.b));
    }

    public long longValue()
    {
        return ((long) Double.longBitsToDouble(this.b));
    }

    public String toString()
    {
        return Double.toString(Double.longBitsToDouble(this.b));
    }
}
