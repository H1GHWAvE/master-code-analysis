package com.a.b.n.a;
 class eh extends com.a.b.n.a.o {
    private final java.util.concurrent.ExecutorService a;

    eh(java.util.concurrent.ExecutorService p2)
    {
        this.a = ((java.util.concurrent.ExecutorService) com.a.b.b.cn.a(p2));
        return;
    }

    public boolean awaitTermination(long p2, java.util.concurrent.TimeUnit p4)
    {
        return this.a.awaitTermination(p2, p4);
    }

    public void execute(Runnable p2)
    {
        this.a.execute(p2);
        return;
    }

    public boolean isShutdown()
    {
        return this.a.isShutdown();
    }

    public boolean isTerminated()
    {
        return this.a.isTerminated();
    }

    public void shutdown()
    {
        this.a.shutdown();
        return;
    }

    public java.util.List shutdownNow()
    {
        return this.a.shutdownNow();
    }
}
