package com.a.b.n.a;
final class en extends com.a.b.n.a.em {
    final com.a.b.b.dw a;

    en()
    {
        this.a = com.a.b.b.dw.a();
        return;
    }

    final long a()
    {
        return this.a.a(java.util.concurrent.TimeUnit.MICROSECONDS);
    }

    final void a(long p8)
    {
        if (p8 > 0) {
            int v1_0 = 0;
            try {
                long v2_0 = java.util.concurrent.TimeUnit.MICROSECONDS.toNanos(p8);
                long v4_1 = (System.nanoTime() + v2_0);
                try {
                    while(true) {
                        java.util.concurrent.TimeUnit.NANOSECONDS.sleep(v2_0);
                        v2_0 = (v4_1 - System.nanoTime());
                    }
                } catch (Thread v0) {
                    v1_0 = 1;
                }
                if (v1_0 != 0) {
                    Thread.currentThread().interrupt();
                }
            } catch (Thread v0_4) {
                if (v1_0 != 0) {
                    Thread.currentThread().interrupt();
                }
                throw v0_4;
            }
        }
        return;
    }
}
