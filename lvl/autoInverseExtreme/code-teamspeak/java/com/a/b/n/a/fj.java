package com.a.b.n.a;
final class fj extends com.a.b.n.a.ev {
    final com.a.b.n.a.et a;
    final ref.WeakReference b;

    fj(com.a.b.n.a.et p1, ref.WeakReference p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    public final void a()
    {
        java.util.logging.Logger v0_2 = ((com.a.b.n.a.fk) this.b.get());
        if (v0_2 != null) {
            v0_2.a(this.a, com.a.b.n.a.ew.a, com.a.b.n.a.ew.b);
            if (!(this.a instanceof com.a.b.n.a.fi)) {
                com.a.b.n.a.fd.a().log(java.util.logging.Level.FINE, "Starting {0}.", this.a);
            }
        }
        return;
    }

    public final void a(com.a.b.n.a.ew p8)
    {
        com.a.b.n.a.fk v0_2 = ((com.a.b.n.a.fk) this.b.get());
        if (v0_2 != null) {
            if (!(this.a instanceof com.a.b.n.a.fi)) {
                com.a.b.n.a.et v1_2 = com.a.b.n.a.fd.a();
                Object[] v4_1 = new Object[2];
                v4_1[0] = this.a;
                v4_1[1] = p8;
                v1_2.log(java.util.logging.Level.FINE, "Service {0} has terminated. Previous state was: {1}", v4_1);
            }
            v0_2.a(this.a, p8, com.a.b.n.a.ew.e);
        }
        return;
    }

    public final void a(com.a.b.n.a.ew p9, Throwable p10)
    {
        com.a.b.n.a.fk v0_2 = ((com.a.b.n.a.fk) this.b.get());
        if (v0_2 != null) {
            if (!(this.a instanceof com.a.b.n.a.fi)) {
                com.a.b.n.a.et v1_2 = com.a.b.n.a.fd.a();
                String v3_2 = String.valueOf(String.valueOf(this.a));
                String v4_1 = String.valueOf(String.valueOf(p9));
                v1_2.log(java.util.logging.Level.SEVERE, new StringBuilder(((v3_2.length() + 34) + v4_1.length())).append("Service ").append(v3_2).append(" has failed in the ").append(v4_1).append(" state.").toString(), p10);
            }
            v0_2.a(this.a, p9, com.a.b.n.a.ew.f);
        }
        return;
    }

    public final void b()
    {
        com.a.b.n.a.fk v0_2 = ((com.a.b.n.a.fk) this.b.get());
        if (v0_2 != null) {
            v0_2.a(this.a, com.a.b.n.a.ew.b, com.a.b.n.a.ew.c);
        }
        return;
    }

    public final void b(com.a.b.n.a.ew p4)
    {
        com.a.b.n.a.fk v0_2 = ((com.a.b.n.a.fk) this.b.get());
        if (v0_2 != null) {
            v0_2.a(this.a, p4, com.a.b.n.a.ew.d);
        }
        return;
    }
}
