package com.a.b.n.a;
public final class dy {

    private dy()
    {
        return;
    }

    private static com.a.b.n.a.dp a(com.a.b.n.a.du p3, java.util.concurrent.Callable p4, java.util.concurrent.BlockingQueue p5)
    {
        com.a.b.n.a.dp v0 = p3.a(p4);
        v0.a(new com.a.b.n.a.dz(p5, v0), com.a.b.n.a.ef.a);
        return v0;
    }

    private static com.a.b.n.a.du a(java.util.concurrent.ExecutorService p1)
    {
        com.a.b.n.a.eh v1_1;
        if (!(p1 instanceof com.a.b.n.a.du)) {
            if (!(p1 instanceof java.util.concurrent.ScheduledExecutorService)) {
                v1_1 = new com.a.b.n.a.eh(p1);
            } else {
                v1_1 = new com.a.b.n.a.ei(((java.util.concurrent.ScheduledExecutorService) p1));
            }
        } else {
            v1_1 = ((com.a.b.n.a.du) p1);
        }
        return v1_1;
    }

    private static com.a.b.n.a.dv a(java.util.concurrent.ScheduledExecutorService p1)
    {
        com.a.b.n.a.ei v1_1;
        if (!(p1 instanceof com.a.b.n.a.dv)) {
            v1_1 = new com.a.b.n.a.ei(p1);
        } else {
            v1_1 = ((com.a.b.n.a.dv) p1);
        }
        return v1_1;
    }

    private static Object a(com.a.b.n.a.du p19, java.util.Collection p20, boolean p21, long p22)
    {
        java.util.concurrent.Future v2_0;
        com.a.b.b.cn.a(p19);
        java.util.concurrent.ExecutionException v3_0 = p20.size();
        if (v3_0 <= null) {
            v2_0 = 0;
        } else {
            v2_0 = 1;
        }
        long v6_0;
        com.a.b.b.cn.a(v2_0);
        java.util.ArrayList v12 = com.a.b.d.ov.a(v3_0);
        java.util.concurrent.LinkedBlockingQueue v13_1 = new java.util.concurrent.LinkedBlockingQueue();
        RuntimeException v4_0 = 0;
        try {
            if (!p21) {
                v6_0 = 0;
            } else {
                v6_0 = System.nanoTime();
            }
        } catch (java.util.concurrent.Future v2_16) {
            java.util.concurrent.ExecutionException v3_10 = v2_16;
            RuntimeException v4_4 = v12.iterator();
        }
        java.util.Iterator v14 = p20.iterator();
        v12.add(com.a.b.n.a.dy.a(p19, ((java.util.concurrent.Callable) v14.next()), v13_1));
        java.util.concurrent.ExecutionException v3_1 = (v3_0 - 1);
        long v10 = p22;
        int v5_1 = 1;
        while(true) {
            int v5_2;
            java.util.concurrent.ExecutionException v3_2;
            java.util.concurrent.Future v2_7;
            long v8_0;
            java.util.concurrent.Future v2_6 = ((java.util.concurrent.Future) v13_1.poll());
            if (v2_6 != null) {
                v8_0 = v10;
                v5_2 = v3_1;
                v3_2 = v2_6;
                v2_7 = v5_1;
            } else {
                if (v3_1 <= null) {
                    if (v5_1 == 0) {
                        break;
                    }
                    if (!p21) {
                        v8_0 = v10;
                        v5_2 = v3_1;
                        v3_2 = ((java.util.concurrent.Future) v13_1.take());
                        v2_7 = v5_1;
                    } else {
                        java.util.concurrent.Future v2_13 = ((java.util.concurrent.Future) v13_1.poll(v10, java.util.concurrent.TimeUnit.NANOSECONDS));
                        if (v2_13 != null) {
                            long v8_1 = System.nanoTime();
                            v2_7 = v5_1;
                            v5_2 = v3_1;
                            v3_2 = v2_13;
                            v8_0 = (v10 - (v8_1 - v6_0));
                            v6_0 = v8_1;
                        } else {
                            throw new java.util.concurrent.TimeoutException();
                        }
                    }
                } else {
                    long v8_2 = (v3_1 - 1);
                    v12.add(com.a.b.n.a.dy.a(p19, ((java.util.concurrent.Callable) v14.next()), v13_1));
                    v5_2 = v8_2;
                    v8_0 = v10;
                    v2_7 = (v5_1 + 1);
                    v3_2 = v2_6;
                }
            }
            if (v3_2 == null) {
                java.util.concurrent.ExecutionException v3_7 = v4_0;
            } else {
                v2_7--;
                java.util.concurrent.ExecutionException v3_8 = v3_2.get();
                RuntimeException v4_2 = v12.iterator();
                while (v4_2.hasNext()) {
                    ((java.util.concurrent.Future) v4_2.next()).cancel(1);
                }
                return v3_8;
            }
            v4_0 = v3_7;
            v10 = v8_0;
            v3_1 = v5_2;
            v5_1 = v2_7;
RuntimeException v4_0            v3_1 = v5_2;
            v10 = v8_0;
            v5_1 = v2_7;
        }
        if (v4_0 == null) {
            v4_0 = new java.util.concurrent.ExecutionException(0);
        }
        throw v4_0;
    }

    static Thread a(String p2, Runnable p3)
    {
        com.a.b.b.cn.a(p2);
        com.a.b.b.cn.a(p3);
        Thread v0_1 = com.a.b.n.a.dy.e().newThread(p3);
        try {
            v0_1.setName(p2);
        } catch (SecurityException v1) {
        }
        return v0_1;
    }

    static java.util.concurrent.Executor a(java.util.concurrent.Executor p1, com.a.b.b.dz p2)
    {
        com.a.b.b.cn.a(p1);
        com.a.b.b.cn.a(p2);
        if (!com.a.b.n.a.dy.a()) {
            p1 = new com.a.b.n.a.ea(p1, p2);
        }
        return p1;
    }

    private static java.util.concurrent.ExecutorService a(java.util.concurrent.ExecutorService p1, com.a.b.b.dz p2)
    {
        com.a.b.b.cn.a(p1);
        com.a.b.b.cn.a(p2);
        if (!com.a.b.n.a.dy.a()) {
            p1 = new com.a.b.n.a.eb(p1, p2);
        }
        return p1;
    }

    private static java.util.concurrent.ExecutorService a(java.util.concurrent.ThreadPoolExecutor p1, long p2, java.util.concurrent.TimeUnit p4)
    {
        return new com.a.b.n.a.ed().a(p1, p2, p4);
    }

    private static java.util.concurrent.ScheduledExecutorService a(java.util.concurrent.ScheduledExecutorService p1, com.a.b.b.dz p2)
    {
        com.a.b.b.cn.a(p1);
        com.a.b.b.cn.a(p2);
        if (!com.a.b.n.a.dy.a()) {
            p1 = new com.a.b.n.a.ec(p1, p2);
        }
        return p1;
    }

    private static java.util.concurrent.ScheduledExecutorService a(java.util.concurrent.ScheduledThreadPoolExecutor p4)
    {
        return new com.a.b.n.a.ed().a(p4, 120, java.util.concurrent.TimeUnit.SECONDS);
    }

    private static java.util.concurrent.ScheduledExecutorService a(java.util.concurrent.ScheduledThreadPoolExecutor p1, long p2, java.util.concurrent.TimeUnit p4)
    {
        return new com.a.b.n.a.ed().a(p1, p2, p4);
    }

    private static void a(java.util.concurrent.ExecutorService p1, long p2, java.util.concurrent.TimeUnit p4)
    {
        new com.a.b.n.a.ed().a(p1, p2, p4);
        return;
    }

    static synthetic void a(java.util.concurrent.ThreadPoolExecutor p2)
    {
        com.a.b.n.a.gl v1 = new com.a.b.n.a.gl().a();
        v1.b = ((java.util.concurrent.ThreadFactory) com.a.b.b.cn.a(p2.getThreadFactory()));
        p2.setThreadFactory(v1.b());
        return;
    }

    static boolean a()
    {
        int v0 = 0;
        if (System.getProperty("com.google.appengine.runtime.environment") != null) {
            try {
                Object[] v3_1 = new Class[0];
                Object[] v3_3 = new Object[0];
            } catch (NoSuchMethodException v1) {
            } catch (NoSuchMethodException v1) {
            } catch (NoSuchMethodException v1) {
            } catch (NoSuchMethodException v1) {
            }
            if (Class.forName("com.google.apphosting.api.ApiProxy").getMethod("getCurrentEnvironment", v3_1).invoke(0, v3_3) != null) {
                v0 = 1;
            }
        }
        return v0;
    }

    private static com.a.b.n.a.du b()
    {
        return new com.a.b.n.a.eg(0);
    }

    private static java.util.concurrent.ExecutorService b(java.util.concurrent.ThreadPoolExecutor p4)
    {
        return new com.a.b.n.a.ed().a(p4, 120, java.util.concurrent.TimeUnit.SECONDS);
    }

    private static boolean b(java.util.concurrent.ExecutorService p5, long p6, java.util.concurrent.TimeUnit p8)
    {
        com.a.b.b.cn.a(p8);
        p5.shutdown();
        try {
            boolean v0_2 = (java.util.concurrent.TimeUnit.NANOSECONDS.convert(p6, p8) / 2);
        } catch (boolean v0) {
            Thread.currentThread().interrupt();
            p5.shutdownNow();
            return p5.isTerminated();
        }
        if (p5.awaitTermination(v0_2, java.util.concurrent.TimeUnit.NANOSECONDS)) {
            return p5.isTerminated();
        } else {
            p5.shutdownNow();
            p5.awaitTermination(v0_2, java.util.concurrent.TimeUnit.NANOSECONDS);
            return p5.isTerminated();
        }
    }

    private static com.a.b.n.a.du c()
    {
        return new com.a.b.n.a.eg(0);
    }

    private static void c(java.util.concurrent.ThreadPoolExecutor p2)
    {
        com.a.b.n.a.gl v1 = new com.a.b.n.a.gl().a();
        v1.b = ((java.util.concurrent.ThreadFactory) com.a.b.b.cn.a(p2.getThreadFactory()));
        p2.setThreadFactory(v1.b());
        return;
    }

    private static java.util.concurrent.Executor d()
    {
        return com.a.b.n.a.ef.a;
    }

    private static java.util.concurrent.ThreadFactory e()
    {
        RuntimeException v0_5;
        if (com.a.b.n.a.dy.a()) {
            try {
                String v2_1 = new Class[0];
                String v2_3 = new Object[0];
                v0_5 = ((java.util.concurrent.ThreadFactory) Class.forName("com.google.appengine.api.ThreadManager").getMethod("currentRequestThreadFactory", v2_1).invoke(0, v2_3));
            } catch (RuntimeException v0_9) {
                throw new RuntimeException("Couldn\'t invoke ThreadManager.currentRequestThreadFactory", v0_9);
            } catch (RuntimeException v0_6) {
                throw com.a.b.b.ei.a(v0_6.getCause());
            } catch (RuntimeException v0_11) {
                throw new RuntimeException("Couldn\'t invoke ThreadManager.currentRequestThreadFactory", v0_11);
            } catch (RuntimeException v0_10) {
                throw new RuntimeException("Couldn\'t invoke ThreadManager.currentRequestThreadFactory", v0_10);
            }
        } else {
            v0_5 = java.util.concurrent.Executors.defaultThreadFactory();
        }
        return v0_5;
    }
}
