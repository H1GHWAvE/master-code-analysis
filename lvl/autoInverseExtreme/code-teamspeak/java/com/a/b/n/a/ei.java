package com.a.b.n.a;
final class ei extends com.a.b.n.a.eh implements com.a.b.n.a.dv {
    final java.util.concurrent.ScheduledExecutorService a;

    ei(java.util.concurrent.ScheduledExecutorService p2)
    {
        this(p2);
        this.a = ((java.util.concurrent.ScheduledExecutorService) com.a.b.b.cn.a(p2));
        return;
    }

    public final com.a.b.n.a.dr a(Runnable p9, long p10, long p12, java.util.concurrent.TimeUnit p14)
    {
        com.a.b.n.a.ek v1_1 = new com.a.b.n.a.ek(p9);
        return new com.a.b.n.a.ej(v1_1, this.a.scheduleAtFixedRate(v1_1, p10, p12, p14));
    }

    public final com.a.b.n.a.dr a(Runnable p5, long p6, java.util.concurrent.TimeUnit p8)
    {
        com.a.b.n.a.dq v0_1 = com.a.b.n.a.dq.a(p5, 0);
        return new com.a.b.n.a.ej(v0_1, this.a.schedule(v0_1, p6, p8));
    }

    public final com.a.b.n.a.dr a(java.util.concurrent.Callable p5, long p6, java.util.concurrent.TimeUnit p8)
    {
        com.a.b.n.a.dq v0 = com.a.b.n.a.dq.a(p5);
        return new com.a.b.n.a.ej(v0, this.a.schedule(v0, p6, p8));
    }

    public final com.a.b.n.a.dr b(Runnable p9, long p10, long p12, java.util.concurrent.TimeUnit p14)
    {
        com.a.b.n.a.ek v1_1 = new com.a.b.n.a.ek(p9);
        return new com.a.b.n.a.ej(v1_1, this.a.scheduleWithFixedDelay(v1_1, p10, p12, p14));
    }

    public final synthetic java.util.concurrent.ScheduledFuture schedule(Runnable p5, long p6, java.util.concurrent.TimeUnit p8)
    {
        com.a.b.n.a.dq v0_1 = com.a.b.n.a.dq.a(p5, 0);
        return new com.a.b.n.a.ej(v0_1, this.a.schedule(v0_1, p6, p8));
    }

    public final synthetic java.util.concurrent.ScheduledFuture schedule(java.util.concurrent.Callable p5, long p6, java.util.concurrent.TimeUnit p8)
    {
        com.a.b.n.a.dq v0 = com.a.b.n.a.dq.a(p5);
        return new com.a.b.n.a.ej(v0, this.a.schedule(v0, p6, p8));
    }

    public final synthetic java.util.concurrent.ScheduledFuture scheduleAtFixedRate(Runnable p9, long p10, long p12, java.util.concurrent.TimeUnit p14)
    {
        com.a.b.n.a.ek v1_1 = new com.a.b.n.a.ek(p9);
        return new com.a.b.n.a.ej(v1_1, this.a.scheduleAtFixedRate(v1_1, p10, p12, p14));
    }

    public final synthetic java.util.concurrent.ScheduledFuture scheduleWithFixedDelay(Runnable p9, long p10, long p12, java.util.concurrent.TimeUnit p14)
    {
        com.a.b.n.a.ek v1_1 = new com.a.b.n.a.ek(p9);
        return new com.a.b.n.a.ej(v1_1, this.a.scheduleWithFixedDelay(v1_1, p10, p12, p14));
    }
}
