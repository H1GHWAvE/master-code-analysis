package com.a.b.n.a;
public final class av {
    private final java.util.concurrent.ConcurrentHashMap a;
    private transient java.util.Map b;

    private av(java.util.concurrent.ConcurrentHashMap p2)
    {
        this.a = ((java.util.concurrent.ConcurrentHashMap) com.a.b.b.cn.a(p2));
        return;
    }

    private long a(Object p3)
    {
        long v0_3;
        long v0_2 = ((java.util.concurrent.atomic.AtomicLong) this.a.get(p3));
        if (v0_2 != 0) {
            v0_3 = v0_2.get();
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    private long a(Object p7, long p8)
    {
        do {
            boolean v0_2 = ((java.util.concurrent.atomic.AtomicLong) this.a.get(p7));
            if (v0_2) {
                do {
                    long v4 = v0_2.get();
                    if (v4 != 0) {
                        long v2_1 = (v4 + p8);
                    } else {
                        if (this.a.replace(p7, v0_2, new java.util.concurrent.atomic.AtomicLong(p8))) {
                        }
                    }
                } while(!v0_2.compareAndSet(v4, v2_1));
                p8 = v2_1;
                break;
            } else {
                v0_2 = ((java.util.concurrent.atomic.AtomicLong) this.a.putIfAbsent(p7, new java.util.concurrent.atomic.AtomicLong(p8)));
                if (!v0_2) {
                    break;
                }
            }
        } while(this.a.replace(p7, v0_2, new java.util.concurrent.atomic.AtomicLong(p8)));
        return p8;
    }

    private static com.a.b.n.a.av a()
    {
        return new com.a.b.n.a.av(new java.util.concurrent.ConcurrentHashMap());
    }

    private static com.a.b.n.a.av a(java.util.Map p2)
    {
        return new com.a.b.n.a.av(new java.util.concurrent.ConcurrentHashMap()).b(p2);
    }

    private boolean a(Object p9, long p10, long p12)
    {
        int v0_4;
        if (p10 != 0) {
            int v0_3 = ((java.util.concurrent.atomic.AtomicLong) this.a.get(p9));
            if (v0_3 != 0) {
                v0_4 = v0_3.compareAndSet(p10, p12);
            } else {
                v0_4 = 0;
            }
        } else {
            do {
                long v2_2;
                int v0_7 = ((java.util.concurrent.atomic.AtomicLong) this.a.get(p9));
                if (v0_7 != 0) {
                    v2_2 = v0_7.get();
                    if (v2_2 == 0) {
                    }
                } else {
                    v0_7 = ((java.util.concurrent.atomic.AtomicLong) this.a.putIfAbsent(p9, new java.util.concurrent.atomic.AtomicLong(p12)));
                    if (v0_7 != 0) {
                    } else {
                        v2_2 = 0;
                    }
                }
                if (v2_2 != 0) {
                    v0_4 = 0;
                } else {
                    v0_4 = 1;
                }
            } while(!this.a.replace(p9, v0_7, new java.util.concurrent.atomic.AtomicLong(p12)));
            v2_2 = 0;
        }
        return v0_4;
    }

    private long b(Object p3)
    {
        return this.a(p3, 1);
    }

    private long b(Object p9, long p10)
    {
        while(true) {
            java.util.concurrent.atomic.AtomicLong v0_5;
            java.util.concurrent.atomic.AtomicLong v0_2 = ((java.util.concurrent.atomic.AtomicLong) this.a.get(p9));
            if (v0_2 != null) {
                do {
                    java.util.concurrent.atomic.AtomicLong v4_0 = v0_2.get();
                    if (v4_0 != 0) {
                    } else {
                        if (this.a.replace(p9, v0_2, new java.util.concurrent.atomic.AtomicLong(p10))) {
                            v0_5 = 0;
                        }
                    }
                } while(!v0_2.compareAndSet(v4_0, (v4_0 + p10)));
            } else {
                v0_2 = ((java.util.concurrent.atomic.AtomicLong) this.a.putIfAbsent(p9, new java.util.concurrent.atomic.AtomicLong(p10)));
                if (v0_2 != null) {
                } else {
                    v0_5 = 0;
                }
            }
            return v0_5;
        }
        v0_5 = v4_0;
        return v0_5;
    }

    private void b()
    {
        java.util.Iterator v1 = this.a.keySet().iterator();
        while (v1.hasNext()) {
            Object v2 = v1.next();
            java.util.concurrent.atomic.AtomicLong v0_5 = ((java.util.concurrent.atomic.AtomicLong) this.a.get(v2));
            if ((v0_5 != null) && (v0_5.get() == 0)) {
                this.a.remove(v2, v0_5);
            }
        }
        return;
    }

    private void b(java.util.Map p11)
    {
        java.util.Iterator v1 = p11.entrySet().iterator();
        while (v1.hasNext()) {
            boolean v0_3 = ((java.util.Map$Entry) v1.next());
            Object v2 = v0_3.getKey();
            long v4 = ((Long) v0_3.getValue()).longValue();
            do {
                boolean v0_8 = ((java.util.concurrent.atomic.AtomicLong) this.a.get(v2));
                if (!v0_8) {
                    v0_8 = ((java.util.concurrent.atomic.AtomicLong) this.a.putIfAbsent(v2, new java.util.concurrent.atomic.AtomicLong(v4)));
                    if (!v0_8) {
                        break;
                    }
                }
                do {
                    java.util.concurrent.atomic.AtomicLong v6_0 = v0_8.get();
                    if (v6_0 != 0) {
                    } else {
                        if (this.a.replace(v2, v0_8, new java.util.concurrent.atomic.AtomicLong(v4))) {
                            break;
                        }
                    }
                } while(!v0_8.compareAndSet(v6_0, v4));
            } while(!v0_8.compareAndSet(v6_0, v4));
        }
        return;
    }

    private long c()
    {
        java.util.Iterator v4 = this.a.values().iterator();
        long v2_2 = 0;
        while (v4.hasNext()) {
            v2_2 = (((java.util.concurrent.atomic.AtomicLong) v4.next()).get() + v2_2);
        }
        return v2_2;
    }

    private long c(Object p3)
    {
        return this.a(p3, -1);
    }

    private long c(Object p7, long p8)
    {
        while(true) {
            java.util.concurrent.atomic.AtomicLong v0_5;
            java.util.concurrent.atomic.AtomicLong v0_2 = ((java.util.concurrent.atomic.AtomicLong) this.a.get(p7));
            if (v0_2 != null) {
                do {
                    java.util.concurrent.atomic.AtomicLong v4_0 = v0_2.get();
                    if (v4_0 != 0) {
                    } else {
                        if (this.a.replace(p7, v0_2, new java.util.concurrent.atomic.AtomicLong(p8))) {
                            v0_5 = 0;
                        }
                    }
                } while(!v0_2.compareAndSet(v4_0, p8));
            } else {
                v0_2 = ((java.util.concurrent.atomic.AtomicLong) this.a.putIfAbsent(p7, new java.util.concurrent.atomic.AtomicLong(p8)));
                if (v0_2 != null) {
                } else {
                    v0_5 = 0;
                }
            }
            return v0_5;
        }
        v0_5 = v4_0;
        return v0_5;
    }

    private long d(Object p3)
    {
        return this.b(p3, 1);
    }

    private long d(Object p7, long p8)
    {
        do {
            long v0_5;
            long v0_2 = ((java.util.concurrent.atomic.AtomicLong) this.a.get(p7));
            if (v0_2 != 0) {
                java.util.concurrent.atomic.AtomicLong v4_0 = v0_2.get();
                if (v4_0 != 0) {
                    v0_5 = v4_0;
                } else {
                }
            } else {
                v0_2 = ((java.util.concurrent.atomic.AtomicLong) this.a.putIfAbsent(p7, new java.util.concurrent.atomic.AtomicLong(p8)));
                if (v0_2 != 0) {
                } else {
                    v0_5 = 0;
                }
            }
            return v0_5;
        } while(!this.a.replace(p7, v0_2, new java.util.concurrent.atomic.AtomicLong(p8)));
        v0_5 = 0;
        return v0_5;
    }

    private java.util.Map d()
    {
        java.util.Map v0_0 = this.b;
        if (v0_0 == null) {
            v0_0 = java.util.Collections.unmodifiableMap(com.a.b.d.sz.a(this.a, new com.a.b.n.a.aw(this)));
            this.b = v0_0;
        }
        return v0_0;
    }

    private long e(Object p3)
    {
        return this.b(p3, -1);
    }

    private java.util.Map e()
    {
        return java.util.Collections.unmodifiableMap(com.a.b.d.sz.a(this.a, new com.a.b.n.a.aw(this)));
    }

    private boolean e(Object p9, long p10)
    {
        java.util.concurrent.ConcurrentHashMap v0_3;
        java.util.concurrent.ConcurrentHashMap v0_2 = ((java.util.concurrent.atomic.AtomicLong) this.a.get(p9));
        if (v0_2 != null) {
            boolean v2_0 = v0_2.get();
            if (v2_0 == p10) {
                if ((v2_0 != 0) && (!v0_2.compareAndSet(v2_0, 0))) {
                    v0_3 = 0;
                } else {
                    this.a.remove(p9, v0_2);
                    v0_3 = 1;
                }
            } else {
                v0_3 = 0;
            }
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    private int f()
    {
        return this.a.size();
    }

    private long f(Object p7)
    {
        long v0_3;
        long v0_2 = ((java.util.concurrent.atomic.AtomicLong) this.a.get(p7));
        if (v0_2 != 0) {
            do {
                long v4 = v0_2.get();
            } while((v4 != 0) && (!v0_2.compareAndSet(v4, 0)));
            this.a.remove(p7, v0_2);
            v0_3 = v4;
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    private boolean g()
    {
        return this.a.isEmpty();
    }

    private boolean g(Object p2)
    {
        return this.a.containsKey(p2);
    }

    private void h()
    {
        this.a.clear();
        return;
    }

    public final String toString()
    {
        return this.a.toString();
    }
}
