package com.a.b.n.a;
public abstract class ad implements com.a.b.n.a.et {
    private static final com.a.b.n.a.dt a;
    private static final com.a.b.n.a.dt b;
    private static final com.a.b.n.a.dt c;
    private static final com.a.b.n.a.dt d;
    private static final com.a.b.n.a.dt e;
    private static final com.a.b.n.a.dt f;
    private static final com.a.b.n.a.dt g;
    private final com.a.b.n.a.dw h;
    private final com.a.b.n.a.dx i;
    private final com.a.b.n.a.dx j;
    private final com.a.b.n.a.dx k;
    private final com.a.b.n.a.dx l;
    private final java.util.List m;
    private volatile com.a.b.n.a.ao n;

    static ad()
    {
        com.a.b.n.a.ad.a = new com.a.b.n.a.ae("starting()");
        com.a.b.n.a.ad.b = new com.a.b.n.a.ag("running()");
        com.a.b.n.a.ad.c = com.a.b.n.a.ad.b(com.a.b.n.a.ew.b);
        com.a.b.n.a.ad.d = com.a.b.n.a.ad.b(com.a.b.n.a.ew.c);
        com.a.b.n.a.ad.e = com.a.b.n.a.ad.a(com.a.b.n.a.ew.a);
        com.a.b.n.a.ad.f = com.a.b.n.a.ad.a(com.a.b.n.a.ew.c);
        com.a.b.n.a.ad.g = com.a.b.n.a.ad.a(com.a.b.n.a.ew.d);
        return;
    }

    protected ad()
    {
        this.h = new com.a.b.n.a.dw();
        this.i = new com.a.b.n.a.aj(this, this.h);
        this.j = new com.a.b.n.a.ak(this, this.h);
        this.k = new com.a.b.n.a.al(this, this.h);
        this.l = new com.a.b.n.a.am(this, this.h);
        this.m = java.util.Collections.synchronizedList(new java.util.ArrayList());
        this.n = new com.a.b.n.a.ao(com.a.b.n.a.ew.a);
        return;
    }

    private static com.a.b.n.a.dt a(com.a.b.n.a.ew p4)
    {
        String v1_1 = String.valueOf(String.valueOf(p4));
        return new com.a.b.n.a.ah(new StringBuilder((v1_1.length() + 21)).append("terminated({from = ").append(v1_1).append("})").toString(), p4);
    }

    private void a(com.a.b.n.a.ew p7, Throwable p8)
    {
        java.util.List v1_1 = String.valueOf(String.valueOf(p7));
        String v2_1 = String.valueOf(String.valueOf(p8));
        new com.a.b.n.a.an(this, new StringBuilder(((v1_1.length() + 27) + v2_1.length())).append("failed({from = ").append(v1_1).append(", cause = ").append(v2_1).append("})").toString(), p7, p8).a(this.m);
        return;
    }

    private static com.a.b.n.a.dt b(com.a.b.n.a.ew p4)
    {
        String v1_1 = String.valueOf(String.valueOf(p4));
        return new com.a.b.n.a.ai(new StringBuilder((v1_1.length() + 19)).append("stopping({from = ").append(v1_1).append("})").toString(), p4);
    }

    private void c(com.a.b.n.a.ew p7)
    {
        String v0_0 = this.f();
        if (v0_0 == p7) {
            return;
        } else {
            if (v0_0 != com.a.b.n.a.ew.f) {
                StringBuilder v2_1 = String.valueOf(String.valueOf(p7));
                String v0_2 = String.valueOf(String.valueOf(v0_0));
                throw new IllegalStateException(new StringBuilder(((v2_1.length() + 37) + v0_2.length())).append("Expected the service to be ").append(v2_1).append(", but was ").append(v0_2).toString());
            } else {
                IllegalStateException v1_4 = String.valueOf(String.valueOf(p7));
                throw new IllegalStateException(new StringBuilder((v1_4.length() + 55)).append("Expected the service to be ").append(v1_4).append(", but the service has FAILED").toString(), this.g());
            }
        }
    }

    private void d(com.a.b.n.a.ew p3)
    {
        if (p3 != com.a.b.n.a.ew.b) {
            if (p3 != com.a.b.n.a.ew.c) {
                throw new AssertionError();
            } else {
                com.a.b.n.a.ad.d.a(this.m);
            }
        } else {
            com.a.b.n.a.ad.c.a(this.m);
        }
        return;
    }

    private void e(com.a.b.n.a.ew p3)
    {
        switch (com.a.b.n.a.af.a[p3.ordinal()]) {
            case 1:
                com.a.b.n.a.ad.e.a(this.m);
                break;
            case 2:
            default:
                throw new AssertionError();
                break;
            case 3:
                com.a.b.n.a.ad.f.a(this.m);
                break;
            case 4:
                com.a.b.n.a.ad.g.a(this.m);
                break;
        }
        return;
    }

    private void l()
    {
        if (!this.h.a.isHeldByCurrentThread()) {
            int v1 = 0;
            while (v1 < this.m.size()) {
                ((com.a.b.n.a.ds) this.m.get(v1)).a();
                v1++;
            }
        }
        return;
    }

    private void m()
    {
        com.a.b.n.a.ad.a.a(this.m);
        return;
    }

    private void n()
    {
        com.a.b.n.a.ad.b.a(this.m);
        return;
    }

    protected abstract void a();

    public final void a(long p8, java.util.concurrent.TimeUnit p10)
    {
        if (!this.h.a(this.k, p8, p10)) {
            com.a.b.n.a.dw v1_2 = String.valueOf(String.valueOf(this));
            String v2_2 = String.valueOf(String.valueOf(this.f()));
            throw new java.util.concurrent.TimeoutException(new StringBuilder(((v1_2.length() + 66) + v2_2.length())).append("Timed out waiting for ").append(v1_2).append(" to reach the RUNNING state. Current state: ").append(v2_2).toString());
        } else {
            try {
                this.c(com.a.b.n.a.ew.c);
                this.h.a();
                return;
            } catch (Throwable v0_6) {
                this.h.a();
                throw v0_6;
            }
        }
    }

    public final void a(com.a.b.n.a.ev p3, java.util.concurrent.Executor p4)
    {
        com.a.b.b.cn.a(p3, "listener");
        com.a.b.b.cn.a(p4, "executor");
        this.h.a.lock();
        try {
            if (!this.f().a()) {
                this.m.add(new com.a.b.n.a.ds(p3, p4));
            }
        } catch (com.a.b.n.a.dw v0_7) {
            this.h.a();
            throw v0_7;
        }
        this.h.a();
        return;
    }

    protected final void a(Throwable p8)
    {
        com.a.b.b.cn.a(p8);
        this.h.a.lock();
        try {
            com.a.b.n.a.dw v0_2 = this.f();
            switch (com.a.b.n.a.af.a[v0_2.ordinal()]) {
                case 1:
                case 5:
                    com.a.b.n.a.dw v0_6 = String.valueOf(String.valueOf(v0_2));
                    throw new IllegalStateException(new StringBuilder((v0_6.length() + 22)).append("Failed while in state:").append(v0_6).toString(), p8);
                    break;
                case 2:
                case 3:
                case 4:
                    this.n = new com.a.b.n.a.ao(com.a.b.n.a.ew.f, 0, p8);
                    String v2_3 = String.valueOf(String.valueOf(v0_2));
                    String v3_2 = String.valueOf(String.valueOf(p8));
                    new com.a.b.n.a.an(this, new StringBuilder(((v2_3.length() + 27) + v3_2.length())).append("failed({from = ").append(v2_3).append(", cause = ").append(v3_2).append("})").toString(), v0_2, p8).a(this.m);
                    this.h.a();
                    this.l();
                    return;
                default:
                    com.a.b.n.a.dw v0_10 = String.valueOf(String.valueOf(v0_2));
                    throw new AssertionError(new StringBuilder((v0_10.length() + 18)).append("Unexpected state: ").append(v0_10).toString());
            }
        } catch (com.a.b.n.a.dw v0_13) {
        }
        this.h.a();
        this.l();
        throw v0_13;
    }

    protected abstract void b();

    public final void b(long p8, java.util.concurrent.TimeUnit p10)
    {
        if (!this.h.a(this.l, p8, p10)) {
            com.a.b.n.a.dw v1_2 = String.valueOf(String.valueOf(this));
            String v2_2 = String.valueOf(String.valueOf(this.f()));
            throw new java.util.concurrent.TimeoutException(new StringBuilder(((v1_2.length() + 65) + v2_2.length())).append("Timed out waiting for ").append(v1_2).append(" to reach a terminal state. Current state: ").append(v2_2).toString());
        } else {
            try {
                this.c(com.a.b.n.a.ew.e);
                this.h.a();
                return;
            } catch (Throwable v0_6) {
                this.h.a();
                throw v0_6;
            }
        }
    }

    protected final void c()
    {
        this.h.a.lock();
        try {
            if (this.n.a == com.a.b.n.a.ew.b) {
                if (!this.n.b) {
                    this.n = new com.a.b.n.a.ao(com.a.b.n.a.ew.c);
                    com.a.b.n.a.ad.b.a(this.m);
                } else {
                    this.n = new com.a.b.n.a.ao(com.a.b.n.a.ew.d);
                    this.b();
                }
                this.h.a();
                this.l();
                return;
            } else {
                java.util.List v1_7 = String.valueOf(String.valueOf(this.n.a));
                com.a.b.n.a.dw v0_13 = new IllegalStateException(new StringBuilder((v1_7.length() + 43)).append("Cannot notifyStarted() when the service is ").append(v1_7).toString());
                this.a(v0_13);
                throw v0_13;
            }
        } catch (com.a.b.n.a.dw v0_14) {
            this.h.a();
            this.l();
            throw v0_14;
        }
    }

    protected final void d()
    {
        this.h.a.lock();
        try {
            String v0_3 = this.n.a;
        } catch (String v0_9) {
            this.h.a();
            this.l();
            throw v0_9;
        }
        if ((v0_3 == com.a.b.n.a.ew.d) || (v0_3 == com.a.b.n.a.ew.c)) {
            this.n = new com.a.b.n.a.ao(com.a.b.n.a.ew.e);
            this.e(v0_3);
            this.h.a();
            this.l();
            return;
        } else {
            String v0_6 = String.valueOf(String.valueOf(v0_3));
            IllegalStateException v1_5 = new IllegalStateException(new StringBuilder((v0_6.length() + 43)).append("Cannot notifyStopped() when the service is ").append(v0_6).toString());
            this.a(v1_5);
            throw v1_5;
        }
    }

    public final boolean e()
    {
        int v0_1;
        if (this.f() != com.a.b.n.a.ew.c) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final com.a.b.n.a.ew f()
    {
        com.a.b.n.a.ew v0_1;
        com.a.b.n.a.ew v0_0 = this.n;
        if ((!v0_0.b) || (v0_0.a != com.a.b.n.a.ew.b)) {
            v0_1 = v0_0.a;
        } else {
            v0_1 = com.a.b.n.a.ew.d;
        }
        return v0_1;
    }

    public final Throwable g()
    {
        Throwable v0_1;
        com.a.b.n.a.ao v3 = this.n;
        if (v3.a != com.a.b.n.a.ew.f) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        Object[] v1_1 = new Object[1];
        v1_1[0] = v3.a;
        com.a.b.b.cn.b(v0_1, "failureCause() is only valid if the service has failed, service is %s", v1_1);
        return v3.c;
    }

    public final com.a.b.n.a.et h()
    {
        if (!this.h.b(this.i)) {
            com.a.b.n.a.dw v1_2 = String.valueOf(String.valueOf(this));
            throw new IllegalStateException(new StringBuilder((v1_2.length() + 33)).append("Service ").append(v1_2).append(" has already been started").toString());
        } else {
            try {
                this.n = new com.a.b.n.a.ao(com.a.b.n.a.ew.b);
                com.a.b.n.a.ad.a.a(this.m);
                this.a();
                this.h.a();
                this.l();
            } catch (com.a.b.n.a.dw v0_8) {
                this.a(v0_8);
                this.h.a();
                this.l();
            } catch (com.a.b.n.a.dw v0_10) {
                this.h.a();
                this.l();
                throw v0_10;
            }
            return this;
        }
    }

    public final com.a.b.n.a.et i()
    {
        if (this.h.b(this.j)) {
            try {
                com.a.b.n.a.dw v0_2 = this.f();
                switch (com.a.b.n.a.af.a[v0_2.ordinal()]) {
                    case 1:
                        this.n = new com.a.b.n.a.ao(com.a.b.n.a.ew.e);
                        this.e(com.a.b.n.a.ew.a);
                        this.h.a();
                        this.l();
                        break;
                    case 2:
                        this.n = new com.a.b.n.a.ao(com.a.b.n.a.ew.b, 1, 0);
                        this.d(com.a.b.n.a.ew.b);
                        break;
                    case 3:
                        this.n = new com.a.b.n.a.ao(com.a.b.n.a.ew.d);
                        this.d(com.a.b.n.a.ew.c);
                        this.b();
                        break;
                    case 4:
                    case 5:
                    case 6:
                        com.a.b.n.a.dw v0_4 = String.valueOf(String.valueOf(v0_2));
                        throw new AssertionError(new StringBuilder((v0_4.length() + 45)).append("isStoppable is incorrectly implemented, saw: ").append(v0_4).toString());
                        break;
                    default:
                        com.a.b.n.a.dw v0_18 = String.valueOf(String.valueOf(v0_2));
                        throw new AssertionError(new StringBuilder((v0_18.length() + 18)).append("Unexpected state: ").append(v0_18).toString());
                }
            } catch (com.a.b.n.a.dw v0_21) {
                this.a(v0_21);
                this.h.a();
                this.l();
            } catch (com.a.b.n.a.dw v0_23) {
            }
            this.h.a();
            this.l();
            throw v0_23;
        }
        return this;
    }

    public final void j()
    {
        this.h.a(this.k);
        try {
            this.c(com.a.b.n.a.ew.c);
            this.h.a();
            return;
        } catch (Throwable v0_3) {
            this.h.a();
            throw v0_3;
        }
    }

    public final void k()
    {
        this.h.a(this.l);
        try {
            this.c(com.a.b.n.a.ew.e);
            this.h.a();
            return;
        } catch (Throwable v0_3) {
            this.h.a();
            throw v0_3;
        }
    }

    public String toString()
    {
        String v0_3 = String.valueOf(String.valueOf(this.getClass().getSimpleName()));
        String v1_2 = String.valueOf(String.valueOf(this.f()));
        return new StringBuilder(((v0_3.length() + 3) + v1_2.length())).append(v0_3).append(" [").append(v1_2).append("]").toString();
    }
}
