package com.a.b.n.a;
final class eg extends com.a.b.n.a.o {
    private final java.util.concurrent.locks.Lock a;
    private final java.util.concurrent.locks.Condition b;
    private int c;
    private boolean d;

    private eg()
    {
        this.a = new java.util.concurrent.locks.ReentrantLock();
        this.b = this.a.newCondition();
        this.c = 0;
        this.d = 0;
        return;
    }

    synthetic eg(byte p1)
    {
        return;
    }

    private void a()
    {
        this.a.lock();
        try {
            if (!this.isShutdown()) {
                this.c = (this.c + 1);
                this.a.unlock();
                return;
            } else {
                throw new java.util.concurrent.RejectedExecutionException("Executor already shutdown");
            }
        } catch (java.util.concurrent.locks.Lock v0_7) {
            this.a.unlock();
            throw v0_7;
        }
    }

    private void b()
    {
        this.a.lock();
        try {
            this.c = (this.c - 1);
        } catch (java.util.concurrent.locks.Lock v0_5) {
            this.a.unlock();
            throw v0_5;
        }
        if (this.isTerminated()) {
            this.b.signalAll();
        }
        this.a.unlock();
        return;
    }

    public final boolean awaitTermination(long p6, java.util.concurrent.TimeUnit p8)
    {
        long v0_0 = p8.toNanos(p6);
        this.a.lock();
        try {
            while (!this.isTerminated()) {
                if (v0_0 > 0) {
                    v0_0 = this.b.awaitNanos(v0_0);
                } else {
                    this.a.unlock();
                    long v0_3 = 0;
                }
                return v0_3;
            }
        } catch (long v0_1) {
            this.a.unlock();
            throw v0_1;
        }
        this.a.unlock();
        v0_3 = 1;
        return v0_3;
    }

    public final void execute(Runnable p3)
    {
        this.a.lock();
        try {
            if (!this.isShutdown()) {
                this.c = (this.c + 1);
                this.a.unlock();
                try {
                    p3.run();
                    this.b();
                    return;
                } catch (Throwable v0_5) {
                    this.b();
                    throw v0_5;
                }
            } else {
                throw new java.util.concurrent.RejectedExecutionException("Executor already shutdown");
            }
        } catch (Throwable v0_8) {
            this.a.unlock();
            throw v0_8;
        }
    }

    public final boolean isShutdown()
    {
        this.a.lock();
        try {
            Throwable v0_1 = this.d;
            this.a.unlock();
            return v0_1;
        } catch (Throwable v0_2) {
            this.a.unlock();
            throw v0_2;
        }
    }

    public final boolean isTerminated()
    {
        this.a.lock();
        try {
            if ((!this.d) || (this.c != 0)) {
                int v0_4 = 0;
            } else {
                v0_4 = 1;
            }
        } catch (int v0_2) {
            this.a.unlock();
            throw v0_2;
        }
        this.a.unlock();
        return v0_4;
    }

    public final void shutdown()
    {
        this.a.lock();
        try {
            this.d = 1;
            this.a.unlock();
            return;
        } catch (Throwable v0_3) {
            this.a.unlock();
            throw v0_3;
        }
    }

    public final java.util.List shutdownNow()
    {
        this.shutdown();
        return java.util.Collections.emptyList();
    }
}
