package com.a.b.b;
final class am implements java.util.Iterator {
    final synthetic com.a.b.b.al a;
    private final java.util.Iterator b;

    am(com.a.b.b.al p2)
    {
        this.a = p2;
        this.b = this.a.a.iterator();
        return;
    }

    public final boolean hasNext()
    {
        return this.b.hasNext();
    }

    public final Object next()
    {
        return this.a.b.c(this.b.next());
    }

    public final void remove()
    {
        this.b.remove();
        return;
    }
}
