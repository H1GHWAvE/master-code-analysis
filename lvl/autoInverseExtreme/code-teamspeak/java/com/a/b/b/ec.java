package com.a.b.b;
final class ec implements com.a.b.b.dz, java.io.Serializable {
    private static final long d;
    final com.a.b.b.dz a;
    transient volatile boolean b;
    transient Object c;

    ec(com.a.b.b.dz p1)
    {
        this.a = p1;
        return;
    }

    public final Object a()
    {
        Object v0_2;
        if (this.b) {
            v0_2 = this.c;
        } else {
            if (this.b) {
            } else {
                v0_2 = this.a.a();
                this.c = v0_2;
                this.b = 1;
            }
        }
        return v0_2;
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        return new StringBuilder((v0_2.length() + 19)).append("Suppliers.memoize(").append(v0_2).append(")").toString();
    }
}
