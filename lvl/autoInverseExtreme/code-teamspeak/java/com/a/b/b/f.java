package com.a.b.b;
public abstract enum class f extends java.lang.Enum {
    public static final enum com.a.b.b.f a;
    public static final enum com.a.b.b.f b;
    public static final enum com.a.b.b.f c;
    public static final enum com.a.b.b.f d;
    public static final enum com.a.b.b.f e;
    private static final synthetic com.a.b.b.f[] h;
    private final com.a.b.b.m f;
    private final String g;

    static f()
    {
        com.a.b.b.f.a = new com.a.b.b.g("LOWER_HYPHEN", com.a.b.b.m.a(45), "-");
        com.a.b.b.f.b = new com.a.b.b.h("LOWER_UNDERSCORE", com.a.b.b.m.a(95), "_");
        com.a.b.b.f.c = new com.a.b.b.i("LOWER_CAMEL", com.a.b.b.m.a(65, 90), "");
        com.a.b.b.f.d = new com.a.b.b.j("UPPER_CAMEL", com.a.b.b.m.a(65, 90), "");
        com.a.b.b.f.e = new com.a.b.b.k("UPPER_UNDERSCORE", com.a.b.b.m.a(95), "_");
        com.a.b.b.f[] v0_11 = new com.a.b.b.f[5];
        v0_11[0] = com.a.b.b.f.a;
        v0_11[1] = com.a.b.b.f.b;
        v0_11[2] = com.a.b.b.f.c;
        v0_11[3] = com.a.b.b.f.d;
        v0_11[4] = com.a.b.b.f.e;
        com.a.b.b.f.h = v0_11;
        return;
    }

    private f(String p1, int p2, com.a.b.b.m p3, String p4)
    {
        this(p1, p2);
        this.f = p3;
        this.g = p4;
        return;
    }

    synthetic f(String p1, int p2, com.a.b.b.m p3, String p4, byte p5)
    {
        this(p1, p2, p3, p4);
        return;
    }

    private com.a.b.b.ak a(com.a.b.b.f p2)
    {
        return new com.a.b.b.l(this, p2);
    }

    static synthetic String b(String p2)
    {
        if (!p2.isEmpty()) {
            p2 = new StringBuilder(p2.length()).append(com.a.b.b.e.b(p2.charAt(0))).append(com.a.b.b.e.a(p2.substring(1))).toString();
        }
        return p2;
    }

    private String c(String p2)
    {
        String v0_1;
        if (this != com.a.b.b.f.c) {
            v0_1 = this.a(p2);
        } else {
            v0_1 = com.a.b.b.e.a(p2);
        }
        return v0_1;
    }

    private static String d(String p2)
    {
        if (!p2.isEmpty()) {
            p2 = new StringBuilder(p2.length()).append(com.a.b.b.e.b(p2.charAt(0))).append(com.a.b.b.e.a(p2.substring(1))).toString();
        }
        return p2;
    }

    public static com.a.b.b.f valueOf(String p1)
    {
        return ((com.a.b.b.f) Enum.valueOf(com.a.b.b.f, p1));
    }

    public static com.a.b.b.f[] values()
    {
        return ((com.a.b.b.f[]) com.a.b.b.f.h.clone());
    }

    public final String a(com.a.b.b.f p1, String p2)
    {
        com.a.b.b.cn.a(p1);
        com.a.b.b.cn.a(p2);
        if (p1 != this) {
            p2 = this.b(p1, p2);
        }
        return p2;
    }

    abstract String a();

    String b(com.a.b.b.f p7, String p8)
    {
        int v3_0 = 0;
        String v0_1 = 0;
        String v1_1 = -1;
        while(true) {
            v1_1 = this.f.a(p8, (v1_1 + 1));
            if (v1_1 == -1) {
                break;
            }
            if (v3_0 != 0) {
                v0_1.append(p7.a(p8.substring(v3_0, v1_1)));
            } else {
                v0_1 = new StringBuilder((p8.length() + (this.g.length() * 4)));
                v0_1.append(p7.c(p8.substring(v3_0, v1_1)));
            }
            v0_1.append(p7.g);
            v3_0 = (this.g.length() + v1_1);
        }
        String v0_3;
        if (v3_0 != 0) {
            v0_3 = v0_1.append(p7.a(p8.substring(v3_0))).toString();
        } else {
            v0_3 = p7.c(p8);
        }
        return v0_3;
    }
}
