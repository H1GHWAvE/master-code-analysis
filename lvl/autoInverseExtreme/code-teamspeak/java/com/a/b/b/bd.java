package com.a.b.b;
final class bd implements com.a.b.b.bf {
    private static final String a = "Could not load Finalizer in its own class loader.Loading Finalizer in the current class loader instead. As a result, you will not be ableto garbage collect this class loader. To support reclaiming this class loader, eitherresolve the underlying issue, or move Google Collections to your system class path.";

    bd()
    {
        return;
    }

    private static java.net.URLClassLoader a(java.net.URL p3)
    {
        java.net.URL[] v1_1 = new java.net.URL[1];
        v1_1[0] = p3;
        return new java.net.URLClassLoader(v1_1, 0);
    }

    private java.net.URL b()
    {
        String v0_3 = String.valueOf("com.google.common.base.internal.Finalizer".replace(46, 47)).concat(".class");
        java.io.IOException v1_4 = this.getClass().getClassLoader().getResource(v0_3);
        if (v1_4 != null) {
            java.net.URL v2_1 = v1_4.toString();
            if (v2_1.endsWith(v0_3)) {
                return new java.net.URL(v1_4, v2_1.substring(0, (v2_1.length() - v0_3.length())));
            } else {
                String v0_9;
                String v0_7 = String.valueOf(v2_1);
                if (v0_7.length() == 0) {
                    v0_9 = new String("Unsupported path style: ");
                } else {
                    v0_9 = "Unsupported path style: ".concat(v0_7);
                }
                throw new java.io.IOException(v0_9);
            }
        } else {
            throw new java.io.FileNotFoundException(v0_3);
        }
    }

    public final Class a()
    {
        Class v0 = 0;
        try {
            String v1_3 = String.valueOf("com.google.common.base.internal.Finalizer".replace(46, 47)).concat(".class");
            String v2_4 = this.getClass().getClassLoader().getResource(v1_3);
        } catch (String v1_12) {
            com.a.b.b.bc.b().log(java.util.logging.Level.WARNING, "Could not load Finalizer in its own class loader.Loading Finalizer in the current class loader instead. As a result, you will not be ableto garbage collect this class loader. To support reclaiming this class loader, eitherresolve the underlying issue, or move Google Collections to your system class path.", v1_12);
            return v0;
        }
        if (v2_4 != null) {
            int v3_1 = v2_4.toString();
            if (v3_1.endsWith(v1_3)) {
                String v2_6 = new java.net.URL[1];
                v2_6[0] = new java.net.URL(v2_4, v3_1.substring(0, (v3_1.length() - v1_3.length())));
                v0 = new java.net.URLClassLoader(v2_6, 0).loadClass("com.google.common.base.internal.Finalizer");
                return v0;
            } else {
                String v1_11;
                String v1_9 = String.valueOf(v3_1);
                if (v1_9.length() == 0) {
                    v1_11 = new String("Unsupported path style: ");
                } else {
                    v1_11 = "Unsupported path style: ".concat(v1_9);
                }
                throw new java.io.IOException(v1_11);
            }
        } else {
            throw new java.io.FileNotFoundException(v1_3);
        }
    }
}
