package com.a.b.b;
final class ed implements com.a.b.b.dz, java.io.Serializable {
    private static final long c;
    final com.a.b.b.bj a;
    final com.a.b.b.dz b;

    ed(com.a.b.b.bj p1, com.a.b.b.dz p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    public final Object a()
    {
        return this.a.e(this.b.a());
    }

    public final boolean equals(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof com.a.b.b.ed)) && ((this.a.equals(((com.a.b.b.ed) p4).a)) && (this.b.equals(((com.a.b.b.ed) p4).b)))) {
            v0 = 1;
        }
        return v0;
    }

    public final int hashCode()
    {
        int v0_1 = new Object[2];
        v0_1[0] = this.a;
        v0_1[1] = this.b;
        return java.util.Arrays.hashCode(v0_1);
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        String v1_2 = String.valueOf(String.valueOf(this.b));
        return new StringBuilder(((v0_2.length() + 21) + v1_2.length())).append("Suppliers.compose(").append(v0_2).append(", ").append(v1_2).append(")").toString();
    }
}
