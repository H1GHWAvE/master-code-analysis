package com.a.b.b;
final class dh extends com.a.b.b.ae {
    static final int s = 1023;
    private static final int w = 3432918353;
    private static final int x = 461845907;
    private static final double y = 16352;
    private final char[] t;
    private final boolean u;
    private final long v;

    dh(char[] p1, long p2, boolean p4, String p5)
    {
        this(p5);
        this.t = p1;
        this.v = p2;
        this.u = p4;
        return;
    }

    static int a(int p3)
    {
        return (461845907 * Integer.rotateLeft((-862048943 * p3), 15));
    }

    private static com.a.b.b.m a(java.util.BitSet p11, String p12)
    {
        int v0_3;
        long v2 = 0;
        char[] v1_0 = p11.cardinality();
        boolean v4 = p11.get(0);
        if (v1_0 != 1) {
            v0_3 = (Integer.highestOneBit((v1_0 - 1)) << 1);
            while ((((double) v0_3) * 0.5) < ((double) v1_0)) {
                v0_3 <<= 1;
            }
        } else {
            v0_3 = 2;
        }
        char[] v1_1 = new char[v0_3];
        double v6_2 = (v1_1.length - 1);
        int v5_1 = p11.nextSetBit(0);
        while (v5_1 != -1) {
            v2 |= (1 << v5_1);
            int v0_10 = (com.a.b.b.dh.a(v5_1) & v6_2);
            while (v1_1[v0_10] != 0) {
                v0_10 = ((v0_10 + 1) & v6_2);
            }
            v1_1[v0_10] = ((char) v5_1);
            v5_1 = p11.nextSetBit((v5_1 + 1));
        }
        return new com.a.b.b.dh(v1_1, v2, v4, p12);
    }

    private boolean b(int p5)
    {
        int v0_4;
        if (1 != ((this.v >> p5) & 1)) {
            v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    private static int c(int p6)
    {
        int v0_3;
        if (p6 != 1) {
            v0_3 = (Integer.highestOneBit((p6 - 1)) << 1);
            while ((((double) v0_3) * 0.5) < ((double) p6)) {
                v0_3 <<= 1;
            }
        } else {
            v0_3 = 2;
        }
        return v0_3;
    }

    final void a(java.util.BitSet p5)
    {
        int v0 = 0;
        if (this.u) {
            p5.set(0);
        }
        char[] v1_1 = this.t;
        int v2 = v1_1.length;
        while (v0 < v2) {
            char v3 = v1_1[v0];
            if (v3 != 0) {
                p5.set(v3);
            }
            v0++;
        }
        return;
    }

    public final boolean c(char p7)
    {
        int v0_10;
        if (p7 != 0) {
            int v0_4;
            if (1 != ((this.v >> p7) & 1)) {
                v0_4 = 0;
            } else {
                v0_4 = 1;
            }
            if (v0_4 != 0) {
                int v4_1 = (this.t.length - 1);
                int v1 = (com.a.b.b.dh.a(p7) & v4_1);
                int v0_8 = v1;
                while (this.t[v0_8] != 0) {
                    if (this.t[v0_8] != p7) {
                        v0_8 = ((v0_8 + 1) & v4_1);
                        if (v0_8 == v1) {
                            v0_10 = 0;
                        }
                    } else {
                        v0_10 = 1;
                    }
                }
                v0_10 = 0;
            } else {
                v0_10 = 0;
            }
        } else {
            v0_10 = this.u;
        }
        return v0_10;
    }
}
