package com.a.b.b;
abstract enum class da extends java.lang.Enum implements com.a.b.b.co {
    public static final enum com.a.b.b.da a;
    public static final enum com.a.b.b.da b;
    public static final enum com.a.b.b.da c;
    public static final enum com.a.b.b.da d;
    private static final synthetic com.a.b.b.da[] e;

    static da()
    {
        com.a.b.b.da.a = new com.a.b.b.db("ALWAYS_TRUE");
        com.a.b.b.da.b = new com.a.b.b.dc("ALWAYS_FALSE");
        com.a.b.b.da.c = new com.a.b.b.dd("IS_NULL");
        com.a.b.b.da.d = new com.a.b.b.de("NOT_NULL");
        com.a.b.b.da[] v0_9 = new com.a.b.b.da[4];
        v0_9[0] = com.a.b.b.da.a;
        v0_9[1] = com.a.b.b.da.b;
        v0_9[2] = com.a.b.b.da.c;
        v0_9[3] = com.a.b.b.da.d;
        com.a.b.b.da.e = v0_9;
        return;
    }

    private da(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    synthetic da(String p1, int p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    private com.a.b.b.co a()
    {
        return this;
    }

    public static com.a.b.b.da valueOf(String p1)
    {
        return ((com.a.b.b.da) Enum.valueOf(com.a.b.b.da, p1));
    }

    public static com.a.b.b.da[] values()
    {
        return ((com.a.b.b.da[]) com.a.b.b.da.e.clone());
    }
}
