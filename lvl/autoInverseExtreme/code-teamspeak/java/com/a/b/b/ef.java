package com.a.b.b;
final enum class ef extends java.lang.Enum implements com.a.b.b.ee {
    public static final enum com.a.b.b.ef a;
    private static final synthetic com.a.b.b.ef[] b;

    static ef()
    {
        com.a.b.b.ef.a = new com.a.b.b.ef("INSTANCE");
        com.a.b.b.ef[] v0_3 = new com.a.b.b.ef[1];
        v0_3[0] = com.a.b.b.ef.a;
        com.a.b.b.ef.b = v0_3;
        return;
    }

    private ef(String p2)
    {
        this(p2, 0);
        return;
    }

    private static Object a(com.a.b.b.dz p1)
    {
        return p1.a();
    }

    public static com.a.b.b.ef valueOf(String p1)
    {
        return ((com.a.b.b.ef) Enum.valueOf(com.a.b.b.ef, p1));
    }

    public static com.a.b.b.ef[] values()
    {
        return ((com.a.b.b.ef[]) com.a.b.b.ef.b.clone());
    }

    public final synthetic Object e(Object p2)
    {
        return ((com.a.b.b.dz) p2).a();
    }

    public final String toString()
    {
        return "Suppliers.supplierFunction()";
    }
}
