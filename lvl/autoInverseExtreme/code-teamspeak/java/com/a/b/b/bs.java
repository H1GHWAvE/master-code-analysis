package com.a.b.b;
final class bs implements com.a.b.b.bj, java.io.Serializable {
    private static final long b;
    private final com.a.b.b.co a;

    private bs(com.a.b.b.co p2)
    {
        this.a = ((com.a.b.b.co) com.a.b.b.cn.a(p2));
        return;
    }

    synthetic bs(com.a.b.b.co p1, byte p2)
    {
        this(p1);
        return;
    }

    private Boolean a(Object p2)
    {
        return Boolean.valueOf(this.a.a(p2));
    }

    public final synthetic Object e(Object p2)
    {
        return Boolean.valueOf(this.a.a(p2));
    }

    public final boolean equals(Object p3)
    {
        int v0_1;
        if (!(p3 instanceof com.a.b.b.bs)) {
            v0_1 = 0;
        } else {
            v0_1 = this.a.equals(((com.a.b.b.bs) p3).a);
        }
        return v0_1;
    }

    public final int hashCode()
    {
        return this.a.hashCode();
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        return new StringBuilder((v0_2.length() + 14)).append("forPredicate(").append(v0_2).append(")").toString();
    }
}
