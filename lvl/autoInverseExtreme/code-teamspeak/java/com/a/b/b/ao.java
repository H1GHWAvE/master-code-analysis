package com.a.b.b;
final class ao extends com.a.b.b.ak implements java.io.Serializable {
    private final com.a.b.b.bj a;
    private final com.a.b.b.bj b;

    private ao(com.a.b.b.bj p2, com.a.b.b.bj p3)
    {
        this.a = ((com.a.b.b.bj) com.a.b.b.cn.a(p2));
        this.b = ((com.a.b.b.bj) com.a.b.b.cn.a(p3));
        return;
    }

    synthetic ao(com.a.b.b.bj p1, com.a.b.b.bj p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    protected final Object a(Object p2)
    {
        return this.b.e(p2);
    }

    protected final Object b(Object p2)
    {
        return this.a.e(p2);
    }

    public final boolean equals(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof com.a.b.b.ao)) && ((this.a.equals(((com.a.b.b.ao) p4).a)) && (this.b.equals(((com.a.b.b.ao) p4).b)))) {
            v0 = 1;
        }
        return v0;
    }

    public final int hashCode()
    {
        return ((this.a.hashCode() * 31) + this.b.hashCode());
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        String v1_2 = String.valueOf(String.valueOf(this.b));
        return new StringBuilder(((v0_2.length() + 18) + v1_2.length())).append("Converter.from(").append(v0_2).append(", ").append(v1_2).append(")").toString();
    }
}
