package com.a.b.b;
public final enum class dv extends java.lang.Enum {
    public static final enum com.a.b.b.dv A;
    public static final enum com.a.b.b.dv B;
    private static final synthetic com.a.b.b.dv[] D;
    public static final enum com.a.b.b.dv a;
    public static final enum com.a.b.b.dv b;
    public static final enum com.a.b.b.dv c;
    public static final enum com.a.b.b.dv d;
    public static final enum com.a.b.b.dv e;
    public static final enum com.a.b.b.dv f;
    public static final enum com.a.b.b.dv g;
    public static final enum com.a.b.b.dv h;
    public static final enum com.a.b.b.dv i;
    public static final enum com.a.b.b.dv j;
    public static final enum com.a.b.b.dv k;
    public static final enum com.a.b.b.dv l;
    public static final enum com.a.b.b.dv m;
    public static final enum com.a.b.b.dv n;
    public static final enum com.a.b.b.dv o;
    public static final enum com.a.b.b.dv p;
    public static final enum com.a.b.b.dv q;
    public static final enum com.a.b.b.dv r;
    public static final enum com.a.b.b.dv s;
    public static final enum com.a.b.b.dv t;
    public static final enum com.a.b.b.dv u;
    public static final enum com.a.b.b.dv v;
    public static final enum com.a.b.b.dv w;
    public static final enum com.a.b.b.dv x;
    public static final enum com.a.b.b.dv y;
    public static final enum com.a.b.b.dv z;
    private final String C;

    static dv()
    {
        com.a.b.b.dv.a = new com.a.b.b.dv("JAVA_VERSION", 0, "java.version");
        com.a.b.b.dv.b = new com.a.b.b.dv("JAVA_VENDOR", 1, "java.vendor");
        com.a.b.b.dv.c = new com.a.b.b.dv("JAVA_VENDOR_URL", 2, "java.vendor.url");
        com.a.b.b.dv.d = new com.a.b.b.dv("JAVA_HOME", 3, "java.home");
        com.a.b.b.dv.e = new com.a.b.b.dv("JAVA_VM_SPECIFICATION_VERSION", 4, "java.vm.specification.version");
        com.a.b.b.dv.f = new com.a.b.b.dv("JAVA_VM_SPECIFICATION_VENDOR", 5, "java.vm.specification.vendor");
        com.a.b.b.dv.g = new com.a.b.b.dv("JAVA_VM_SPECIFICATION_NAME", 6, "java.vm.specification.name");
        com.a.b.b.dv.h = new com.a.b.b.dv("JAVA_VM_VERSION", 7, "java.vm.version");
        com.a.b.b.dv.i = new com.a.b.b.dv("JAVA_VM_VENDOR", 8, "java.vm.vendor");
        com.a.b.b.dv.j = new com.a.b.b.dv("JAVA_VM_NAME", 9, "java.vm.name");
        com.a.b.b.dv.k = new com.a.b.b.dv("JAVA_SPECIFICATION_VERSION", 10, "java.specification.version");
        com.a.b.b.dv.l = new com.a.b.b.dv("JAVA_SPECIFICATION_VENDOR", 11, "java.specification.vendor");
        com.a.b.b.dv.m = new com.a.b.b.dv("JAVA_SPECIFICATION_NAME", 12, "java.specification.name");
        com.a.b.b.dv.n = new com.a.b.b.dv("JAVA_CLASS_VERSION", 13, "java.class.version");
        com.a.b.b.dv.o = new com.a.b.b.dv("JAVA_CLASS_PATH", 14, "java.class.path");
        com.a.b.b.dv.p = new com.a.b.b.dv("JAVA_LIBRARY_PATH", 15, "java.library.path");
        com.a.b.b.dv.q = new com.a.b.b.dv("JAVA_IO_TMPDIR", 16, "java.io.tmpdir");
        com.a.b.b.dv.r = new com.a.b.b.dv("JAVA_COMPILER", 17, "java.compiler");
        com.a.b.b.dv.s = new com.a.b.b.dv("JAVA_EXT_DIRS", 18, "java.ext.dirs");
        com.a.b.b.dv.t = new com.a.b.b.dv("OS_NAME", 19, "os.name");
        com.a.b.b.dv.u = new com.a.b.b.dv("OS_ARCH", 20, "os.arch");
        com.a.b.b.dv.v = new com.a.b.b.dv("OS_VERSION", 21, "os.version");
        com.a.b.b.dv.w = new com.a.b.b.dv("FILE_SEPARATOR", 22, "file.separator");
        com.a.b.b.dv.x = new com.a.b.b.dv("PATH_SEPARATOR", 23, "path.separator");
        com.a.b.b.dv.y = new com.a.b.b.dv("LINE_SEPARATOR", 24, "line.separator");
        com.a.b.b.dv.z = new com.a.b.b.dv("USER_NAME", 25, "user.name");
        com.a.b.b.dv.A = new com.a.b.b.dv("USER_HOME", 26, "user.home");
        com.a.b.b.dv.B = new com.a.b.b.dv("USER_DIR", 27, "user.dir");
        com.a.b.b.dv[] v0_57 = new com.a.b.b.dv[28];
        v0_57[0] = com.a.b.b.dv.a;
        v0_57[1] = com.a.b.b.dv.b;
        v0_57[2] = com.a.b.b.dv.c;
        v0_57[3] = com.a.b.b.dv.d;
        v0_57[4] = com.a.b.b.dv.e;
        v0_57[5] = com.a.b.b.dv.f;
        v0_57[6] = com.a.b.b.dv.g;
        v0_57[7] = com.a.b.b.dv.h;
        v0_57[8] = com.a.b.b.dv.i;
        v0_57[9] = com.a.b.b.dv.j;
        v0_57[10] = com.a.b.b.dv.k;
        v0_57[11] = com.a.b.b.dv.l;
        v0_57[12] = com.a.b.b.dv.m;
        v0_57[13] = com.a.b.b.dv.n;
        v0_57[14] = com.a.b.b.dv.o;
        v0_57[15] = com.a.b.b.dv.p;
        v0_57[16] = com.a.b.b.dv.q;
        v0_57[17] = com.a.b.b.dv.r;
        v0_57[18] = com.a.b.b.dv.s;
        v0_57[19] = com.a.b.b.dv.t;
        v0_57[20] = com.a.b.b.dv.u;
        v0_57[21] = com.a.b.b.dv.v;
        v0_57[22] = com.a.b.b.dv.w;
        v0_57[23] = com.a.b.b.dv.x;
        v0_57[24] = com.a.b.b.dv.y;
        v0_57[25] = com.a.b.b.dv.z;
        v0_57[26] = com.a.b.b.dv.A;
        v0_57[27] = com.a.b.b.dv.B;
        com.a.b.b.dv.D = v0_57;
        return;
    }

    private dv(String p1, int p2, String p3)
    {
        this(p1, p2);
        this.C = p3;
        return;
    }

    private String a()
    {
        return this.C;
    }

    private String b()
    {
        return System.getProperty(this.C);
    }

    public static com.a.b.b.dv valueOf(String p1)
    {
        return ((com.a.b.b.dv) Enum.valueOf(com.a.b.b.dv, p1));
    }

    public static com.a.b.b.dv[] values()
    {
        return ((com.a.b.b.dv[]) com.a.b.b.dv.D.clone());
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.C));
        String v1_3 = String.valueOf(String.valueOf(System.getProperty(this.C)));
        return new StringBuilder(((v0_2.length() + 1) + v1_3.length())).append(v0_2).append("=").append(v1_3).toString();
    }
}
