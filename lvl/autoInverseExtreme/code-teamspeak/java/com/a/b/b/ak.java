package com.a.b.b;
public abstract class ak implements com.a.b.b.bj {
    private final boolean a;
    private transient com.a.b.b.ak b;

    public ak()
    {
        this(0);
        return;
    }

    private ak(byte p2)
    {
        this.a = 1;
        return;
    }

    private static com.a.b.b.ak a(com.a.b.b.bj p2, com.a.b.b.bj p3)
    {
        return new com.a.b.b.ao(p2, p3, 0);
    }

    private Iterable a(Iterable p2)
    {
        com.a.b.b.cn.a(p2, "fromIterable");
        return new com.a.b.b.al(this, p2);
    }

    private static com.a.b.b.ak b()
    {
        return com.a.b.b.ap.a;
    }

    private com.a.b.b.ak b(com.a.b.b.ak p2)
    {
        return this.a(p2);
    }

    private Object f(Object p2)
    {
        return this.c(p2);
    }

    public com.a.b.b.ak a()
    {
        com.a.b.b.aq v0_0 = this.b;
        if (v0_0 == null) {
            v0_0 = new com.a.b.b.aq(this);
            this.b = v0_0;
        }
        return v0_0;
    }

    com.a.b.b.ak a(com.a.b.b.ak p3)
    {
        return new com.a.b.b.an(this, ((com.a.b.b.ak) com.a.b.b.cn.a(p3)));
    }

    public abstract Object a();

    public abstract Object b();

    Object c(Object p2)
    {
        Object v0_1;
        if (!this.a) {
            v0_1 = this.b(p2);
        } else {
            if (p2 != null) {
                v0_1 = com.a.b.b.cn.a(this.b(p2));
            } else {
                v0_1 = 0;
            }
        }
        return v0_1;
    }

    Object d(Object p2)
    {
        Object v0_1;
        if (!this.a) {
            v0_1 = this.a(p2);
        } else {
            if (p2 != null) {
                v0_1 = com.a.b.b.cn.a(this.a(p2));
            } else {
                v0_1 = 0;
            }
        }
        return v0_1;
    }

    public final Object e(Object p2)
    {
        return this.c(p2);
    }

    public boolean equals(Object p2)
    {
        return super.equals(p2);
    }
}
