package com.a.b.b;
final class bp implements com.a.b.b.bj, java.io.Serializable {
    private static final long c;
    private final com.a.b.b.bj a;
    private final com.a.b.b.bj b;

    public bp(com.a.b.b.bj p2, com.a.b.b.bj p3)
    {
        this.a = ((com.a.b.b.bj) com.a.b.b.cn.a(p2));
        this.b = ((com.a.b.b.bj) com.a.b.b.cn.a(p3));
        return;
    }

    public final Object e(Object p3)
    {
        return this.a.e(this.b.e(p3));
    }

    public final boolean equals(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof com.a.b.b.bp)) && ((this.b.equals(((com.a.b.b.bp) p4).b)) && (this.a.equals(((com.a.b.b.bp) p4).a)))) {
            v0 = 1;
        }
        return v0;
    }

    public final int hashCode()
    {
        return (this.b.hashCode() ^ this.a.hashCode());
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        String v1_2 = String.valueOf(String.valueOf(this.b));
        return new StringBuilder(((v0_2.length() + 2) + v1_2.length())).append(v0_2).append("(").append(v1_2).append(")").toString();
    }
}
