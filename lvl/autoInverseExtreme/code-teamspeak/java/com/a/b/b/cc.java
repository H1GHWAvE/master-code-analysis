package com.a.b.b;
public final class cc {
    private final String a;
    private com.a.b.b.cd b;
    private com.a.b.b.cd c;
    private boolean d;

    private cc(String p3)
    {
        this.b = new com.a.b.b.cd(0);
        this.c = this.b;
        this.d = 0;
        this.a = ((String) com.a.b.b.cn.a(p3));
        return;
    }

    public synthetic cc(String p1, byte p2)
    {
        this(p1);
        return;
    }

    private com.a.b.b.cc a()
    {
        this.d = 1;
        return this;
    }

    private com.a.b.b.cc a(char p2)
    {
        return this.a(String.valueOf(p2));
    }

    private com.a.b.b.cc a(double p2)
    {
        return this.a(String.valueOf(p2));
    }

    private com.a.b.b.cc a(float p2)
    {
        return this.a(String.valueOf(p2));
    }

    private com.a.b.b.cc a(int p2)
    {
        return this.a(String.valueOf(p2));
    }

    private com.a.b.b.cc a(long p2)
    {
        return this.a(String.valueOf(p2));
    }

    private com.a.b.b.cc a(String p2, char p3)
    {
        return this.a(p2, String.valueOf(p3));
    }

    private com.a.b.b.cc a(String p3, double p4)
    {
        return this.a(p3, String.valueOf(p4));
    }

    private com.a.b.b.cc a(String p2, float p3)
    {
        return this.a(p2, String.valueOf(p3));
    }

    private com.a.b.b.cc a(String p2, boolean p3)
    {
        return this.a(p2, String.valueOf(p3));
    }

    private com.a.b.b.cc a(boolean p2)
    {
        return this.a(String.valueOf(p2));
    }

    private com.a.b.b.cc b(Object p2)
    {
        return this.a(p2);
    }

    private com.a.b.b.cc b(String p2, Object p3)
    {
        return this.a(p2, p3);
    }

    private com.a.b.b.cd b()
    {
        com.a.b.b.cd v0_1 = new com.a.b.b.cd(0);
        this.c.c = v0_1;
        this.c = v0_1;
        return v0_1;
    }

    public final com.a.b.b.cc a(Object p2)
    {
        this.b().b = p2;
        return this;
    }

    public final com.a.b.b.cc a(String p2, int p3)
    {
        return this.a(p2, String.valueOf(p3));
    }

    public final com.a.b.b.cc a(String p3, long p4)
    {
        return this.a(p3, String.valueOf(p4));
    }

    public final com.a.b.b.cc a(String p3, Object p4)
    {
        com.a.b.b.cd v1 = this.b();
        v1.b = p4;
        v1.a = ((String) com.a.b.b.cn.a(p3));
        return this;
    }

    public final String toString()
    {
        boolean v2 = this.d;
        StringBuilder v3_3 = new StringBuilder(32).append(this.a).append(123);
        String v0_5 = "";
        com.a.b.b.cd v1_1 = this.b.c;
        while (v1_1 != null) {
            if ((!v2) || (v1_1.b != null)) {
                v3_3.append(v0_5);
                v0_5 = ", ";
                if (v1_1.a != null) {
                    v3_3.append(v1_1.a).append(61);
                }
                v3_3.append(v1_1.b);
            }
            v1_1 = v1_1.c;
        }
        return v3_3.append(125).toString();
    }
}
