package com.a.b.b;
public final class el {

    private el()
    {
        return;
    }

    private static int a(CharSequence p8)
    {
        IllegalArgumentException v0_0 = 0;
        String v3_0 = p8.length();
        int v1_0 = 0;
        while ((v1_0 < v3_0) && (p8.charAt(v1_0) < 128)) {
            v1_0++;
        }
        StringBuilder v2_1 = v3_0;
        while (v1_0 < v3_0) {
            String v4_1 = p8.charAt(v1_0);
            if (v4_1 >= 2048) {
                String v4_2 = p8.length();
                while (v1_0 < v4_2) {
                    int v5_0 = p8.charAt(v1_0);
                    if (v5_0 >= 2048) {
                        v0_0 += 2;
                        if ((55296 <= v5_0) && (v5_0 <= 57343)) {
                            if (Character.codePointAt(p8, v1_0) >= 65536) {
                                v1_0++;
                            } else {
                                throw new IllegalArgumentException(new StringBuilder(39).append("Unpaired surrogate at index ").append(v1_0).toString());
                            }
                        }
                    } else {
                        v0_0 += ((127 - v5_0) >> 31);
                    }
                    v1_0++;
                }
                IllegalArgumentException v0_1 = (v0_0 + v2_1);
            } else {
                v2_1 += ((127 - v4_1) >> 31);
                v1_0++;
            }
            if (v0_1 >= v3_0) {
                return v0_1;
            } else {
                throw new IllegalArgumentException(new StringBuilder(54).append("UTF-8 length does not fit in int: ").append((((long) v0_1) + 2.121995791e-314)).toString());
            }
        }
        v0_1 = v2_1;
    }

    private static int a(CharSequence p5, int p6)
    {
        StringBuilder v2_0 = p5.length();
        IllegalArgumentException v0_0 = 0;
        int v1_0 = p6;
        while (v1_0 < v2_0) {
            String v3_0 = p5.charAt(v1_0);
            if (v3_0 >= 2048) {
                v0_0 += 2;
                if ((55296 <= v3_0) && (v3_0 <= 57343)) {
                    if (Character.codePointAt(p5, v1_0) >= 65536) {
                        v1_0++;
                    } else {
                        throw new IllegalArgumentException(new StringBuilder(39).append("Unpaired surrogate at index ").append(v1_0).toString());
                    }
                }
            } else {
                v0_0 += ((127 - v3_0) >> 31);
            }
            v1_0++;
        }
        return v0_0;
    }

    private static boolean a(byte[] p9)
    {
        int v3 = (p9.length + 0);
        com.a.b.b.cn.a(0, v3, p9.length);
        int v0_2 = 0;
        while (v0_2 < v3) {
            if (p9[v0_2] >= 0) {
                v0_2++;
            }
            while (v0_2 < v3) {
                byte v2_1 = (v0_2 + 1);
                int v0_4 = p9[v0_2];
                if (v0_4 >= 0) {
                    v0_2 = v2_1;
                } else {
                    if (v0_4 >= -32) {
                        if (v0_4 >= -16) {
                            if ((v2_1 + 2) < v3) {
                                int v4_2 = (v2_1 + 1);
                                byte v2_2 = p9[v2_1];
                                if ((v2_2 <= -65) && ((((v0_4 << 28) + (v2_2 + 112)) >> 30) == 0)) {
                                    byte v2_4 = (v4_2 + 1);
                                    if (p9[v4_2] <= -65) {
                                        v0_2 = (v2_4 + 1);
                                        if (p9[v2_4] > -65) {
                                        }
                                    }
                                }
                                int v0_3 = 0;
                            } else {
                                v0_3 = 0;
                            }
                        } else {
                            if ((v2_1 + 1) < v3) {
                                int v4_4 = (v2_1 + 1);
                                byte v2_6 = p9[v2_1];
                                if (((v2_6 <= -65) && ((v0_4 != -32) || (v2_6 >= -96))) && ((v0_4 != -19) || (-96 > v2_6))) {
                                    v0_2 = (v4_4 + 1);
                                    if (p9[v4_4] > -65) {
                                    }
                                }
                                v0_3 = 0;
                            } else {
                                v0_3 = 0;
                            }
                        }
                    } else {
                        if (v2_1 != v3) {
                            if (v0_4 >= -62) {
                                v0_2 = (v2_1 + 1);
                                if (p9[v2_1] > -65) {
                                }
                            }
                            v0_3 = 0;
                        } else {
                            v0_3 = 0;
                        }
                    }
                }
                return v0_3;
            }
        }
        v0_3 = 1;
        return v0_3;
    }

    private static boolean a(byte[] p10, int p11)
    {
        int v4 = (p11 + 0);
        com.a.b.b.cn.a(0, v4, p10.length);
        int v0_1 = 0;
        while (v0_1 < v4) {
            if (p10[v0_1] >= 0) {
                v0_1++;
            }
            while (v0_1 < v4) {
                byte v3_1 = (v0_1 + 1);
                int v0_3 = p10[v0_1];
                if (v0_3 >= 0) {
                    v0_1 = v3_1;
                } else {
                    if (v0_3 >= -32) {
                        if (v0_3 >= -16) {
                            if ((v3_1 + 2) < v4) {
                                int v5_2 = (v3_1 + 1);
                                byte v3_2 = p10[v3_1];
                                if ((v3_2 <= -65) && ((((v0_3 << 28) + (v3_2 + 112)) >> 30) == 0)) {
                                    byte v3_4 = (v5_2 + 1);
                                    if (p10[v5_2] <= -65) {
                                        v0_1 = (v3_4 + 1);
                                        if (p10[v3_4] > -65) {
                                        }
                                    }
                                }
                                int v0_2 = 0;
                            } else {
                                v0_2 = 0;
                            }
                        } else {
                            if ((v3_1 + 1) < v4) {
                                int v5_4 = (v3_1 + 1);
                                byte v3_6 = p10[v3_1];
                                if (((v3_6 <= -65) && ((v0_3 != -32) || (v3_6 >= -96))) && ((v0_3 != -19) || (-96 > v3_6))) {
                                    v0_1 = (v5_4 + 1);
                                    if (p10[v5_4] > -65) {
                                    }
                                }
                                v0_2 = 0;
                            } else {
                                v0_2 = 0;
                            }
                        }
                    } else {
                        if (v3_1 != v4) {
                            if (v0_3 >= -62) {
                                v0_1 = (v3_1 + 1);
                                if (p10[v3_1] > -65) {
                                }
                            }
                            v0_2 = 0;
                        } else {
                            v0_2 = 0;
                        }
                    }
                }
                return v0_2;
            }
            v0_2 = 1;
            return v0_2;
        }
        v0_2 = 1;
        return v0_2;
    }

    private static boolean a(byte[] p8, int p9, int p10)
    {
        while (p9 < p10) {
            byte v2_0 = (p9 + 1);
            int v0_0 = p8[p9];
            if (v0_0 >= 0) {
                p9 = v2_0;
            } else {
                int v0_5;
                if (v0_0 >= -32) {
                    if (v0_0 >= -16) {
                        if ((v2_0 + 2) < p10) {
                            int v3_2 = (v2_0 + 1);
                            byte v2_1 = p8[v2_0];
                            if ((v2_1 <= -65) && ((((v0_0 << 28) + (v2_1 + 112)) >> 30) == 0)) {
                                byte v2_3 = (v3_2 + 1);
                                if (p8[v3_2] <= -65) {
                                    v0_5 = (v2_3 + 1);
                                    if (p8[v2_3] <= -65) {
                                        p9 = v0_5;
                                    }
                                }
                            }
                            int v0_6 = 0;
                            return v0_6;
                        } else {
                            v0_6 = 0;
                            return v0_6;
                        }
                    } else {
                        if ((v2_0 + 1) < p10) {
                            int v3_4 = (v2_0 + 1);
                            byte v2_5 = p8[v2_0];
                            if (((v2_5 <= -65) && ((v0_0 != -32) || (v2_5 >= -96))) && ((v0_0 != -19) || (-96 > v2_5))) {
                                p9 = (v3_4 + 1);
                                if (p8[v3_4] > -65) {
                                }
                            }
                            v0_6 = 0;
                            return v0_6;
                        } else {
                            v0_6 = 0;
                            return v0_6;
                        }
                    }
                } else {
                    if (v2_0 != p10) {
                        if (v0_0 >= -62) {
                            v0_5 = (v2_0 + 1);
                            if (p8[v2_0] <= -65) {
                            }
                        }
                        v0_6 = 0;
                        return v0_6;
                    } else {
                        v0_6 = 0;
                        return v0_6;
                    }
                }
            }
            return v0_6;
        }
        v0_6 = 1;
        return v0_6;
    }
}
