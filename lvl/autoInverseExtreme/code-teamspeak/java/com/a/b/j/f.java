package com.a.b.j;
final class f {
    static final long a = 4503599627370495;
    static final long b = 9218868437227405312;
    static final long c = 9223372036854775808;
    static final int d = 52;
    static final int e = 1023;
    static final long f = 4503599627370496;
    private static final long g;

    static f()
    {
        com.a.b.j.f.g = Double.doubleToRawLongBits(1.0);
        return;
    }

    private f()
    {
        return;
    }

    static double a(java.math.BigInteger p14)
    {
        double v0_9;
        long v2_0 = 1;
        int v3_0 = p14.abs();
        double v4_0 = (v3_0.bitLength() - 1);
        if (v4_0 >= 63) {
            if (v4_0 <= 1023) {
                int v5 = ((v4_0 - 52) - 1);
                long v6_0 = v3_0.shiftRight(v5).longValue();
                double v0_6 = ((v6_0 >> 1) & 2.225073858507201e-308);
                if (((v6_0 & 1) == 0) || (((v0_6 & 1) == 0) && (v3_0.getLowestSetBit() >= v5))) {
                    v2_0 = 0;
                }
                if (v2_0 != 0) {
                    v0_6++;
                }
                v0_9 = Double.longBitsToDouble(((v0_6 + (((long) (v4_0 + 1023)) << 52)) | (((long) p14.signum()) & -0.0)));
            } else {
                v0_9 = (((double) p14.signum()) * inf);
            }
        } else {
            v0_9 = ((double) p14.longValue());
        }
        return v0_9;
    }

    static long a(double p6)
    {
        long v0_3;
        com.a.b.b.cn.a(com.a.b.j.f.b(p6), "not a normal value");
        long v0_1 = Math.getExponent(p6);
        long v2_1 = (Double.doubleToRawLongBits(p6) & 2.225073858507201e-308);
        if (v0_1 != -1023) {
            v0_3 = (2.2250738585072014e-308 | v2_1);
        } else {
            v0_3 = (v2_1 << 1);
        }
        return v0_3;
    }

    static boolean b(double p2)
    {
        int v0_1;
        if (Math.getExponent(p2) > 1023) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    static boolean c(double p2)
    {
        int v0_1;
        if (Math.getExponent(p2) < -1022) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    static double d(double p4)
    {
        return Double.longBitsToDouble(((Double.doubleToRawLongBits(p4) & 2.225073858507201e-308) | com.a.b.j.f.g));
    }

    private static double e(double p2)
    {
        return (- Math.nextUp((- p2)));
    }

    private static double f(double p4)
    {
        double v0_1;
        if (Double.isNaN(p4)) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.a(v0_1);
        if (p4 <= 0) {
            p4 = 0;
        }
        return p4;
    }
}
