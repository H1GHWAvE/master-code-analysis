package com.a.b.j;
public final class a {
    static final int a = 256;
    static final java.math.BigInteger b;
    private static final double c;
    private static final double d;

    static a()
    {
        com.a.b.j.a.b = new java.math.BigInteger("16a09e667f3bcc908b2fb1366ea957d3e3adec17512775099da2f590b0667322a", 16);
        com.a.b.j.a.c = Math.log(10.0);
        com.a.b.j.a.d = Math.log(2.0);
        return;
    }

    private a()
    {
        return;
    }

    private static int a(java.math.BigInteger p3, java.math.RoundingMode p4)
    {
        com.a.b.j.k.a("x", ((java.math.BigInteger) com.a.b.b.cn.a(p3)));
        int v0_3 = (p3.bitLength() - 1);
        switch (com.a.b.j.b.a[p4.ordinal()]) {
            case 1:
                com.a.b.j.k.a(com.a.b.j.a.a(p3));
            case 2:
            case 3:
                break;
            case 2:
            case 3:
                break;
            case 4:
            case 5:
                if (com.a.b.j.a.a(p3)) {
                } else {
                    v0_3++;
                }
                break;
            case 6:
            case 7:
            case 8:
                if (v0_3 >= 256) {
                    if ((p3.pow(2).bitLength() - 1) < ((v0_3 * 2) + 1)) {
                    } else {
                        v0_3++;
                    }
                } else {
                    if (p3.compareTo(com.a.b.j.a.b.shiftRight((256 - v0_3))) <= 0) {
                    } else {
                        v0_3++;
                    }
                }
                break;
            default:
                throw new AssertionError();
        }
        return v0_3;
    }

    private static java.math.BigInteger a(int p18)
    {
        java.math.BigInteger v2_16;
        com.a.b.j.k.b("n", p18);
        if (p18 >= com.a.b.j.i.f.length) {
            java.util.ArrayList v12_1 = new java.util.ArrayList(com.a.b.j.g.a((com.a.b.j.g.a(p18, java.math.RoundingMode.CEILING) * p18), 64, java.math.RoundingMode.CEILING));
            java.math.BigInteger v2_8 = com.a.b.j.i.f.length;
            long v4_2 = com.a.b.j.i.f[(v2_8 - 1)];
            int v7 = Long.numberOfTrailingZeros(v4_2);
            int v8_0 = (v4_2 >> v7);
            long v6_0 = (com.a.b.j.i.a(v8_0, java.math.RoundingMode.FLOOR) + 1);
            int v5 = (com.a.b.j.i.a(((long) v2_8), java.math.RoundingMode.FLOOR) + 1);
            java.math.BigInteger v2_10 = (1 << (v5 - 1));
            int v3_7 = v5;
            long v4_6 = v8_0;
            int v8_1 = v6_0;
            int v9 = v7;
            long v6_1 = ((long) v2_8);
            while (v6_1 <= ((long) p18)) {
                if ((((long) v2_10) & v6_1) != 0) {
                    v2_10 <<= 1;
                    v3_7++;
                }
                long v10_5 = Long.numberOfTrailingZeros(v6_1);
                long v14_1 = (v6_1 >> v10_5);
                v9 += v10_5;
                if ((v8_1 + (v3_7 - v10_5)) >= 64) {
                    v12_1.add(java.math.BigInteger.valueOf(v4_6));
                    v4_6 = 1;
                }
                long v10_8 = (v4_6 * v14_1);
                v8_1 = (com.a.b.j.i.a(v10_8, java.math.RoundingMode.FLOOR) + 1);
                v6_1 = (1 + v6_1);
                v4_6 = v10_8;
            }
            if (v4_6 > 1) {
                v12_1.add(java.math.BigInteger.valueOf(v4_6));
            }
            v2_16 = com.a.b.j.a.a(v12_1, 0, v12_1.size()).shiftLeft(v9);
        } else {
            v2_16 = java.math.BigInteger.valueOf(com.a.b.j.i.f[p18]);
        }
        return v2_16;
    }

    private static java.math.BigInteger a(int p14, int p15)
    {
        int v0_2;
        com.a.b.j.k.b("n", p14);
        com.a.b.j.k.b("k", p15);
        if (p15 > p14) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        long v4_1 = new Object[2];
        v4_1[0] = Integer.valueOf(p15);
        v4_1[1] = Integer.valueOf(p14);
        com.a.b.b.cn.a(v0_2, "k (%s) > n (%s)", v4_1);
        if (p15 > (p14 >> 1)) {
            p15 = (p14 - p15);
        }
        if ((p15 >= com.a.b.j.i.g.length) || (p14 > com.a.b.j.i.g[p15])) {
            long v6 = ((long) p14);
            long v4_2 = 1;
            int v2_3 = com.a.b.j.i.a(((long) p14), java.math.RoundingMode.CEILING);
            int v8 = 1;
            java.math.BigInteger v1_1 = java.math.BigInteger.ONE;
            int v0_9 = v2_3;
            while (v8 < p15) {
                int v9 = (p14 - v8);
                long v10_0 = (v8 + 1);
                if ((v0_9 + v2_3) < 63) {
                    v6 *= ((long) v9);
                    v4_2 *= ((long) v10_0);
                    v0_9 += v2_3;
                } else {
                    int v3_2 = v1_1.multiply(java.math.BigInteger.valueOf(v6)).divide(java.math.BigInteger.valueOf(v4_2));
                    v6 = ((long) v9);
                    v4_2 = ((long) v10_0);
                    v0_9 = v2_3;
                    v1_1 = v3_2;
                }
                v8++;
            }
            int v0_12 = v1_1.multiply(java.math.BigInteger.valueOf(v6)).divide(java.math.BigInteger.valueOf(v4_2));
        } else {
            v0_12 = java.math.BigInteger.valueOf(com.a.b.j.i.a(p14, p15));
        }
        return v0_12;
    }

    private static java.math.BigInteger a(java.math.BigInteger p3, java.math.BigInteger p4, java.math.RoundingMode p5)
    {
        return new java.math.BigDecimal(p3).divide(new java.math.BigDecimal(p4), 0, p5).toBigIntegerExact();
    }

    private static java.math.BigInteger a(java.util.List p2)
    {
        return com.a.b.j.a.a(p2, 0, p2.size());
    }

    private static java.math.BigInteger a(java.util.List p2, int p3, int p4)
    {
        java.math.BigInteger v0_6;
        switch ((p4 - p3)) {
            case 0:
                v0_6 = java.math.BigInteger.ONE;
                break;
            case 1:
                v0_6 = ((java.math.BigInteger) p2.get(p3));
                break;
            case 2:
                v0_6 = ((java.math.BigInteger) p2.get(p3)).multiply(((java.math.BigInteger) p2.get((p3 + 1))));
                break;
            case 3:
                v0_6 = ((java.math.BigInteger) p2.get(p3)).multiply(((java.math.BigInteger) p2.get((p3 + 1)))).multiply(((java.math.BigInteger) p2.get((p3 + 2))));
                break;
            default:
                java.math.BigInteger v0_11 = ((p4 + p3) >> 1);
                v0_6 = com.a.b.j.a.a(p2, p3, v0_11).multiply(com.a.b.j.a.a(p2, v0_11, p4));
        }
        return v0_6;
    }

    private static boolean a(java.math.BigInteger p2)
    {
        int v0_2;
        com.a.b.b.cn.a(p2);
        if ((p2.signum() <= 0) || (p2.getLowestSetBit() != (p2.bitLength() - 1))) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private static int b(java.math.BigInteger p7, java.math.RoundingMode p8)
    {
        int v1_0;
        com.a.b.j.k.a("x", p7);
        if (!com.a.b.j.a.d(p7)) {
            int v0_9;
            java.math.BigInteger v2_3;
            v1_0 = ((int) ((((double) com.a.b.j.a.a(p7, java.math.RoundingMode.FLOOR)) * com.a.b.j.a.d) / com.a.b.j.a.c));
            int v0_8 = java.math.BigInteger.TEN.pow(v1_0);
            java.math.BigInteger v2_2 = v0_8.compareTo(p7);
            if (v2_2 <= null) {
                int v4_0 = v1_0;
                v0_9 = v2_2;
                v2_3 = v0_8;
                while(true) {
                    java.math.BigInteger v3_0 = java.math.BigInteger.TEN.multiply(v2_3);
                    int v1_2 = v3_0.compareTo(p7);
                    if (v1_2 > 0) {
                        break;
                    }
                    v2_3 = v3_0;
                    v4_0++;
                    v0_9 = v1_2;
                }
                v1_0 = v4_0;
            } else {
                do {
                    v1_0--;
                    v0_8 = v0_8.divide(java.math.BigInteger.TEN);
                    java.math.BigInteger v2_5 = v0_8.compareTo(p7);
                } while(v2_5 > null);
                v2_3 = v0_8;
                v0_9 = v2_5;
            }
            switch (com.a.b.j.b.a[p8.ordinal()]) {
                case 1:
                    int v0_14;
                    if (v0_9 != 0) {
                        v0_14 = 0;
                    } else {
                        v0_14 = 1;
                    }
                    com.a.b.j.k.a(v0_14);
                case 2:
                case 3:
                    break;
                case 2:
                case 3:
                    break;
                case 4:
                case 5:
                    if (!v2_3.equals(p7)) {
                        v1_0++;
                    }
                    break;
                case 6:
                case 7:
                case 8:
                    if (p7.pow(2).compareTo(v2_3.pow(2).multiply(java.math.BigInteger.TEN)) > 0) {
                        v1_0++;
                    }
                    break;
                default:
                    throw new AssertionError();
            }
        } else {
            v1_0 = com.a.b.j.i.b(p7.longValue(), p8);
        }
        return v1_0;
    }

    private static java.math.BigInteger b(java.math.BigInteger p4)
    {
        java.math.BigInteger v0_5;
        java.math.BigInteger v0_1 = com.a.b.j.a.a(p4, java.math.RoundingMode.FLOOR);
        if (v0_1 >= 1023) {
            java.math.BigInteger v0_3 = ((v0_1 - 52) & -2);
            v0_5 = com.a.b.j.a.c(p4.shiftRight(v0_3)).shiftLeft((v0_3 >> 1));
        } else {
            v0_5 = com.a.b.j.a.c(p4);
        }
        java.math.BigInteger v1_5 = v0_5.add(p4.divide(v0_5)).shiftRight(1);
        if (!v0_5.equals(v1_5)) {
            do {
                v0_5 = v1_5;
                v1_5 = v0_5.add(p4.divide(v0_5)).shiftRight(1);
            } while(v1_5.compareTo(v0_5) < 0);
        }
        return v0_5;
    }

    private static java.math.BigInteger c(java.math.BigInteger p3)
    {
        return com.a.b.j.c.a(Math.sqrt(com.a.b.j.f.a(p3)), java.math.RoundingMode.HALF_EVEN);
    }

    private static java.math.BigInteger c(java.math.BigInteger p6, java.math.RoundingMode p7)
    {
        if (p6.signum() >= 0) {
            java.math.BigInteger v0_2;
            if (!com.a.b.j.a.d(p6)) {
                v0_2 = com.a.b.j.a.b(p6);
                switch (com.a.b.j.b.a[p7.ordinal()]) {
                    case 1:
                        com.a.b.j.k.a(v0_2.pow(2).equals(p6));
                    case 2:
                    case 3:
                        break;
                    case 2:
                    case 3:
                        break;
                    case 4:
                    case 5:
                        if (((v0_2.intValue() * v0_2.intValue()) != p6.intValue()) || (!v0_2.pow(2).equals(p6))) {
                            java.math.BigInteger v1_11 = 0;
                        } else {
                            v1_11 = 1;
                        }
                        if (v1_11 == null) {
                            v0_2 = v0_2.add(java.math.BigInteger.ONE);
                        }
                        break;
                    case 6:
                    case 7:
                    case 8:
                        if (v0_2.pow(2).add(v0_2).compareTo(p6) < 0) {
                            v0_2 = v0_2.add(java.math.BigInteger.ONE);
                        }
                        break;
                    default:
                        throw new AssertionError();
                }
            } else {
                v0_2 = java.math.BigInteger.valueOf(com.a.b.j.i.c(p6.longValue(), p7));
            }
            return v0_2;
        } else {
            java.math.BigInteger v0_8 = String.valueOf(String.valueOf("x"));
            int v2_3 = String.valueOf(String.valueOf(p6));
            throw new IllegalArgumentException(new StringBuilder(((v0_8.length() + 16) + v2_3.length())).append(v0_8).append(" (").append(v2_3).append(") must be >= 0").toString());
        }
    }

    private static boolean d(java.math.BigInteger p2)
    {
        int v0_1;
        if (p2.bitLength() > 63) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }
}
