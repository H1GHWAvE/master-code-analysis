package com.a.b.d;
public abstract class gh extends com.a.b.d.hg implements java.util.Collection {

    protected gh()
    {
        return;
    }

    private Object[] a(Object[] p2)
    {
        return com.a.b.d.yc.a(this, p2);
    }

    private boolean c()
    {
        int v0_2;
        if (this.iterator().hasNext()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private boolean d(java.util.Collection p2)
    {
        return com.a.b.d.cm.a(this, p2);
    }

    protected boolean a(java.util.Collection p2)
    {
        return com.a.b.d.nj.a(this, p2.iterator());
    }

    public boolean add(Object p2)
    {
        return this.b().add(p2);
    }

    public boolean addAll(java.util.Collection p2)
    {
        return this.b().addAll(p2);
    }

    public abstract java.util.Collection b();

    protected boolean b(Object p2)
    {
        return com.a.b.d.nj.a(this.iterator(), p2);
    }

    protected boolean b(java.util.Collection p2)
    {
        return com.a.b.d.nj.a(this.iterator(), p2);
    }

    protected boolean c(Object p3)
    {
        int v0_0 = this.iterator();
        while (v0_0.hasNext()) {
            if (com.a.b.b.ce.a(v0_0.next(), p3)) {
                v0_0.remove();
                int v0_1 = 1;
            }
            return v0_1;
        }
        v0_1 = 0;
        return v0_1;
    }

    protected boolean c(java.util.Collection p2)
    {
        return com.a.b.d.nj.b(this.iterator(), p2);
    }

    public void clear()
    {
        this.b().clear();
        return;
    }

    public boolean contains(Object p2)
    {
        return this.b().contains(p2);
    }

    public boolean containsAll(java.util.Collection p2)
    {
        return this.b().containsAll(p2);
    }

    public boolean isEmpty()
    {
        return this.b().isEmpty();
    }

    public java.util.Iterator iterator()
    {
        return this.b().iterator();
    }

    public synthetic Object k_()
    {
        return this.b();
    }

    protected void l()
    {
        com.a.b.d.nj.i(this.iterator());
        return;
    }

    protected String o()
    {
        return com.a.b.d.cm.a(this);
    }

    public final Object[] p()
    {
        Object[] v0_1 = new Object[this.size()];
        return this.toArray(v0_1);
    }

    public boolean remove(Object p2)
    {
        return this.b().remove(p2);
    }

    public boolean removeAll(java.util.Collection p2)
    {
        return this.b().removeAll(p2);
    }

    public boolean retainAll(java.util.Collection p2)
    {
        return this.b().retainAll(p2);
    }

    public int size()
    {
        return this.b().size();
    }

    public Object[] toArray()
    {
        return this.b().toArray();
    }

    public Object[] toArray(Object[] p2)
    {
        return this.b().toArray(p2);
    }
}
