package com.a.b.d;
final class pg implements java.util.ListIterator {
    boolean a;
    final synthetic java.util.ListIterator b;
    final synthetic com.a.b.d.pf c;

    pg(com.a.b.d.pf p1, java.util.ListIterator p2)
    {
        this.c = p1;
        this.b = p2;
        return;
    }

    public final void add(Object p2)
    {
        this.b.add(p2);
        this.b.previous();
        this.a = 0;
        return;
    }

    public final boolean hasNext()
    {
        return this.b.hasPrevious();
    }

    public final boolean hasPrevious()
    {
        return this.b.hasNext();
    }

    public final Object next()
    {
        if (this.hasNext()) {
            this.a = 1;
            return this.b.previous();
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public final int nextIndex()
    {
        return com.a.b.d.pf.a(this.c, this.b.nextIndex());
    }

    public final Object previous()
    {
        if (this.hasPrevious()) {
            this.a = 1;
            return this.b.next();
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public final int previousIndex()
    {
        return (this.nextIndex() - 1);
    }

    public final void remove()
    {
        com.a.b.b.cn.b(this.a, "no calls to next() since the last call to remove()");
        this.b.remove();
        this.a = 0;
        return;
    }

    public final void set(Object p2)
    {
        com.a.b.b.cn.b(this.a);
        this.b.set(p2);
        return;
    }
}
