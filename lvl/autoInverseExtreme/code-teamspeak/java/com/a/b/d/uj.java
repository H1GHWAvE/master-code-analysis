package com.a.b.d;
abstract class uj extends java.util.AbstractMap {
    private transient java.util.Set a;
    private transient java.util.Set b;
    private transient java.util.Collection c;

    uj()
    {
        return;
    }

    abstract java.util.Set a();

    java.util.Collection c_()
    {
        return new com.a.b.d.vb(this);
    }

    java.util.Set e()
    {
        return new com.a.b.d.uk(this);
    }

    public java.util.Set entrySet()
    {
        java.util.Set v0 = this.a;
        if (v0 == null) {
            v0 = this.a();
            this.a = v0;
        }
        return v0;
    }

    public java.util.Set keySet()
    {
        java.util.Set v0 = this.b;
        if (v0 == null) {
            v0 = this.e();
            this.b = v0;
        }
        return v0;
    }

    public java.util.Collection values()
    {
        java.util.Collection v0 = this.c;
        if (v0 == null) {
            v0 = this.c_();
            this.c = v0;
        }
        return v0;
    }
}
