package com.a.b.d;
public final class fg extends com.a.b.d.hh implements java.io.Serializable {
    private static final long c;
    final int a;
    private final java.util.Queue b;

    private fg(int p6)
    {
        java.util.ArrayDeque v0_0;
        if (p6 < 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        Object[] v1_1 = new Object[1];
        v1_1[0] = Integer.valueOf(p6);
        com.a.b.b.cn.a(v0_0, "maxSize (%s) must >= 0", v1_1);
        this.b = new java.util.ArrayDeque(p6);
        this.a = p6;
        return;
    }

    private static com.a.b.d.fg a(int p1)
    {
        return new com.a.b.d.fg(p1);
    }

    private int c()
    {
        return (this.a - this.size());
    }

    protected final java.util.Queue a()
    {
        return this.b;
    }

    public final boolean add(Object p4)
    {
        com.a.b.b.cn.a(p4);
        if (this.a != 0) {
            if (this.size() == this.a) {
                this.b.remove();
            }
            this.b.add(p4);
        }
        return 1;
    }

    public final boolean addAll(java.util.Collection p2)
    {
        return this.a(p2);
    }

    protected final bridge synthetic java.util.Collection b()
    {
        return this.b;
    }

    public final boolean contains(Object p3)
    {
        return this.b.contains(com.a.b.b.cn.a(p3));
    }

    protected final bridge synthetic Object k_()
    {
        return this.b;
    }

    public final boolean offer(Object p2)
    {
        return this.add(p2);
    }

    public final boolean remove(Object p3)
    {
        return this.b.remove(com.a.b.b.cn.a(p3));
    }
}
