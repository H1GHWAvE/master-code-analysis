package com.a.b.d;
 class abx extends com.a.b.d.bf implements java.io.Serializable {
    private static final long f;
    final java.util.Map a;
    final com.a.b.b.dz b;
    private transient java.util.Set c;
    private transient java.util.Map d;
    private transient com.a.b.d.aci e;

    abx(java.util.Map p1, com.a.b.b.dz p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    static synthetic java.util.Map a(com.a.b.d.abx p5, Object p6)
    {
        java.util.LinkedHashMap v2_1 = new java.util.LinkedHashMap();
        java.util.Iterator v3 = p5.a.entrySet().iterator();
        while (v3.hasNext()) {
            boolean v0_4 = ((java.util.Map$Entry) v3.next());
            Object v1_2 = ((java.util.Map) v0_4.getValue()).remove(p6);
            if (v1_2 != null) {
                v2_1.put(v0_4.getKey(), v1_2);
                if (((java.util.Map) v0_4.getValue()).isEmpty()) {
                    v3.remove();
                }
            }
        }
        return v2_1;
    }

    static synthetic boolean a(com.a.b.d.abx p1, Object p2, Object p3, Object p4)
    {
        return p1.b(p2, p3, p4);
    }

    static synthetic boolean b(com.a.b.d.abx p1, Object p2, Object p3, Object p4)
    {
        int v0_1;
        if (!p1.b(p2, p3, p4)) {
            v0_1 = 0;
        } else {
            p1.c(p2, p3);
            v0_1 = 1;
        }
        return v0_1;
    }

    private boolean b(Object p2, Object p3, Object p4)
    {
        if ((p4 == null) || (!p4.equals(this.b(p2, p3)))) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private boolean c(Object p2, Object p3, Object p4)
    {
        int v0_1;
        if (!this.b(p2, p3, p4)) {
            v0_1 = 0;
        } else {
            this.c(p2, p3);
            v0_1 = 1;
        }
        return v0_1;
    }

    private java.util.Map f(Object p3)
    {
        java.util.Map v0_2 = ((java.util.Map) this.a.get(p3));
        if (v0_2 == null) {
            v0_2 = ((java.util.Map) this.b.a());
            this.a.put(p3, v0_2);
        }
        return v0_2;
    }

    private java.util.Map g(Object p6)
    {
        java.util.LinkedHashMap v2_1 = new java.util.LinkedHashMap();
        java.util.Iterator v3 = this.a.entrySet().iterator();
        while (v3.hasNext()) {
            boolean v0_4 = ((java.util.Map$Entry) v3.next());
            Object v1_2 = ((java.util.Map) v0_4.getValue()).remove(p6);
            if (v1_2 != null) {
                v2_1.put(v0_4.getKey(), v1_2);
                if (((java.util.Map) v0_4.getValue()).isEmpty()) {
                    v3.remove();
                }
            }
        }
        return v2_1;
    }

    public Object a(Object p3, Object p4, Object p5)
    {
        com.a.b.b.cn.a(p3);
        com.a.b.b.cn.a(p4);
        com.a.b.b.cn.a(p5);
        Object v0_2 = ((java.util.Map) this.a.get(p3));
        if (v0_2 == null) {
            v0_2 = ((java.util.Map) this.b.a());
            this.a.put(p3, v0_2);
        }
        return v0_2.put(p4, p5);
    }

    public java.util.Set a()
    {
        return this.m().keySet();
    }

    public boolean a(Object p2)
    {
        if ((p2 == null) || (!com.a.b.d.sz.b(this.a, p2))) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public boolean a(Object p2, Object p3)
    {
        if ((p2 == null) || ((p3 == null) || (!super.a(p2, p3)))) {
            int v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public Object b(Object p2, Object p3)
    {
        if ((p2 != null) && (p3 != null)) {
            Object v0 = super.b(p2, p3);
        } else {
            v0 = 0;
        }
        return v0;
    }

    public java.util.Set b()
    {
        com.a.b.d.ach v0_0 = this.c;
        if (v0_0 == null) {
            v0_0 = new com.a.b.d.ach(this, 0);
            this.c = v0_0;
        }
        return v0_0;
    }

    public boolean b(Object p4)
    {
        int v0_3;
        if (p4 != null) {
            java.util.Iterator v2 = this.a.values().iterator();
            while (v2.hasNext()) {
                if (com.a.b.d.sz.b(((java.util.Map) v2.next()), p4)) {
                    v0_3 = 1;
                }
            }
            v0_3 = 0;
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public Object c(Object p3, Object p4)
    {
        if ((p3 != null) && (p4 != null)) {
            Object v0_2 = ((java.util.Map) com.a.b.d.sz.a(this.a, p3));
            if (v0_2 != null) {
                Object v1_1 = v0_2.remove(p4);
                if (v0_2.isEmpty()) {
                    this.a.remove(p3);
                }
                Object v0_5 = v1_1;
            } else {
                v0_5 = 0;
            }
        } else {
            v0_5 = 0;
        }
        return v0_5;
    }

    public boolean c()
    {
        return this.a.isEmpty();
    }

    public boolean c(Object p2)
    {
        if ((p2 == null) || (!super.c(p2))) {
            int v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public java.util.Map d(Object p2)
    {
        return new com.a.b.d.aca(this, p2);
    }

    public void d()
    {
        this.a.clear();
        return;
    }

    public java.util.Map e(Object p2)
    {
        return new com.a.b.d.acm(this, p2);
    }

    public java.util.Set e()
    {
        return super.e();
    }

    final java.util.Iterator g()
    {
        return new com.a.b.d.abz(this, 0);
    }

    public java.util.Collection h()
    {
        return super.h();
    }

    public int k()
    {
        java.util.Iterator v2 = this.a.values().iterator();
        int v1_2 = 0;
        while (v2.hasNext()) {
            v1_2 = (((java.util.Map) v2.next()).size() + v1_2);
        }
        return v1_2;
    }

    public java.util.Map l()
    {
        com.a.b.d.aci v0_0 = this.e;
        if (v0_0 == null) {
            v0_0 = new com.a.b.d.aci(this, 0);
            this.e = v0_0;
        }
        return v0_0;
    }

    public java.util.Map m()
    {
        java.util.Map v0 = this.d;
        if (v0 == null) {
            v0 = this.n();
            this.d = v0;
        }
        return v0;
    }

    java.util.Map n()
    {
        return new com.a.b.d.acq(this);
    }

    java.util.Iterator o()
    {
        return new com.a.b.d.acg(this, 0);
    }
}
