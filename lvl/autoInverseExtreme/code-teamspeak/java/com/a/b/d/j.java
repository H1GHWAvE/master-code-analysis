package com.a.b.d;
public abstract class j extends com.a.b.d.agi {
    private int a;
    private Object b;

    public j()
    {
        this.a = com.a.b.d.l.b;
        return;
    }

    private boolean c()
    {
        int v0_3;
        this.a = com.a.b.d.l.d;
        this.b = this.a();
        if (this.a == com.a.b.d.l.c) {
            v0_3 = 0;
        } else {
            this.a = com.a.b.d.l.a;
            v0_3 = 1;
        }
        return v0_3;
    }

    private Object d()
    {
        if (this.hasNext()) {
            return this.b;
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public abstract Object a();

    public final Object b()
    {
        this.a = com.a.b.d.l.c;
        return 0;
    }

    public final boolean hasNext()
    {
        int v0_1;
        int v2 = 0;
        if (this.a == com.a.b.d.l.d) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.b(v0_1);
        switch (com.a.b.d.k.a[(this.a - 1)]) {
            case 1:
                break;
            case 2:
                v2 = 1;
                break;
            default:
                this.a = com.a.b.d.l.d;
                this.b = this.a();
                if (this.a == com.a.b.d.l.c) {
                } else {
                    this.a = com.a.b.d.l.a;
                    v2 = 1;
                }
        }
        return v2;
    }

    public final Object next()
    {
        if (this.hasNext()) {
            this.a = com.a.b.d.l.b;
            Object v0_2 = this.b;
            this.b = 0;
            return v0_2;
        } else {
            throw new java.util.NoSuchElementException();
        }
    }
}
