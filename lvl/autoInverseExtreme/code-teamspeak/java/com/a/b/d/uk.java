package com.a.b.d;
 class uk extends com.a.b.d.aan {
    final java.util.Map d;

    uk(java.util.Map p2)
    {
        this.d = ((java.util.Map) com.a.b.b.cn.a(p2));
        return;
    }

    java.util.Map b()
    {
        return this.d;
    }

    public void clear()
    {
        this.b().clear();
        return;
    }

    public boolean contains(Object p2)
    {
        return this.b().containsKey(p2);
    }

    public boolean isEmpty()
    {
        return this.b().isEmpty();
    }

    public java.util.Iterator iterator()
    {
        return com.a.b.d.sz.a(this.b().entrySet().iterator());
    }

    public boolean remove(Object p2)
    {
        int v0_1;
        if (!this.contains(p2)) {
            v0_1 = 0;
        } else {
            this.b().remove(p2);
            v0_1 = 1;
        }
        return v0_1;
    }

    public int size()
    {
        return this.b().size();
    }
}
