package com.a.b.d;
 class wy extends com.a.b.d.gx implements java.io.Serializable {
    private static final long g;
    final com.a.b.d.vi a;
    transient java.util.Collection b;
    transient com.a.b.d.xc c;
    transient java.util.Set d;
    transient java.util.Collection e;
    transient java.util.Map f;

    wy(com.a.b.d.vi p2)
    {
        this.a = ((com.a.b.d.vi) com.a.b.b.cn.a(p2));
        return;
    }

    public final boolean a(com.a.b.d.vi p2)
    {
        throw new UnsupportedOperationException();
    }

    public final boolean a(Object p2, Object p3)
    {
        throw new UnsupportedOperationException();
    }

    public java.util.Collection b(Object p2, Iterable p3)
    {
        throw new UnsupportedOperationException();
    }

    public final java.util.Map b()
    {
        java.util.Map v0_0 = this.f;
        if (v0_0 == null) {
            v0_0 = java.util.Collections.unmodifiableMap(com.a.b.d.sz.a(this.a.b(), new com.a.b.d.wz(this)));
            this.f = v0_0;
        }
        return v0_0;
    }

    protected com.a.b.d.vi c()
    {
        return this.a;
    }

    public java.util.Collection c(Object p2)
    {
        return com.a.b.d.we.a(this.a.c(p2));
    }

    public final boolean c(Object p2, Iterable p3)
    {
        throw new UnsupportedOperationException();
    }

    public final boolean c(Object p2, Object p3)
    {
        throw new UnsupportedOperationException();
    }

    public java.util.Collection d(Object p2)
    {
        throw new UnsupportedOperationException();
    }

    public final void g()
    {
        throw new UnsupportedOperationException();
    }

    public final java.util.Collection i()
    {
        java.util.Collection v0_0 = this.e;
        if (v0_0 == null) {
            v0_0 = java.util.Collections.unmodifiableCollection(this.a.i());
            this.e = v0_0;
        }
        return v0_0;
    }

    public java.util.Collection k()
    {
        com.a.b.d.uw v0_0 = this.b;
        if (v0_0 == null) {
            com.a.b.d.uw v0_2 = this.a.k();
            if (!(v0_2 instanceof java.util.Set)) {
                v0_0 = new com.a.b.d.uw(java.util.Collections.unmodifiableCollection(v0_2));
            } else {
                v0_0 = com.a.b.d.sz.a(((java.util.Set) v0_2));
            }
            this.b = v0_0;
        }
        return v0_0;
    }

    protected synthetic Object k_()
    {
        return this.c();
    }

    public final java.util.Set p()
    {
        java.util.Set v0_0 = this.d;
        if (v0_0 == null) {
            v0_0 = java.util.Collections.unmodifiableSet(this.a.p());
            this.d = v0_0;
        }
        return v0_0;
    }

    public final com.a.b.d.xc q()
    {
        com.a.b.d.xc v0_0 = this.c;
        if (v0_0 == null) {
            v0_0 = com.a.b.d.xe.a(this.a.q());
            this.c = v0_0;
        }
        return v0_0;
    }
}
