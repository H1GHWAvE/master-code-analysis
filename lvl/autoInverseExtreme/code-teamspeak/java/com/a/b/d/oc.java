package com.a.b.d;
public final class oc extends com.a.b.d.ba {
    static final double a = 16368;
    private static final int c = 16;
    private static final int d = 2;
    private static final long f = 1;
    transient int b;
    private transient com.a.b.d.oe e;

    private oc(int p4, int p5)
    {
        this(new java.util.LinkedHashMap(p4));
        this.b = 2;
        com.a.b.d.cl.a(p5, "expectedValuesPerKey");
        this.b = p5;
        this.e = new com.a.b.d.oe(0, 0, 0, 0);
        com.a.b.d.oc.b(this.e, this.e);
        return;
    }

    private static com.a.b.d.oc a(int p3)
    {
        return new com.a.b.d.oc(com.a.b.d.sz.b(p3), com.a.b.d.sz.b(2));
    }

    static synthetic com.a.b.d.oe a(com.a.b.d.oc p1)
    {
        return p1.e;
    }

    static synthetic void a(com.a.b.d.oe p2)
    {
        com.a.b.d.oc.b(p2.g, p2.h);
        return;
    }

    static synthetic void a(com.a.b.d.oe p0, com.a.b.d.oe p1)
    {
        com.a.b.d.oc.b(p0, p1);
        return;
    }

    static synthetic void a(com.a.b.d.oh p2)
    {
        com.a.b.d.oc.b(p2.a(), p2.b());
        return;
    }

    static synthetic void a(com.a.b.d.oh p0, com.a.b.d.oh p1)
    {
        com.a.b.d.oc.b(p0, p1);
        return;
    }

    private void a(java.io.ObjectInputStream p7)
    {
        p7.defaultReadObject();
        this.e = new com.a.b.d.oe(0, 0, 0, 0);
        com.a.b.d.oc.b(this.e, this.e);
        this.b = p7.readInt();
        int v2_2 = p7.readInt();
        java.util.LinkedHashMap v3_1 = new java.util.LinkedHashMap(com.a.b.d.sz.b(v2_2));
        int v1_5 = 0;
        while (v1_5 < v2_2) {
            Object v4_1 = p7.readObject();
            v3_1.put(v4_1, this.e(v4_1));
            v1_5++;
        }
        int v2_3 = p7.readInt();
        int v1_6 = 0;
        while (v1_6 < v2_3) {
            ((java.util.Collection) v3_1.get(p7.readObject())).add(p7.readObject());
            v1_6++;
        }
        this.a(v3_1);
        return;
    }

    private void a(java.io.ObjectOutputStream p4)
    {
        p4.defaultWriteObject();
        p4.writeInt(this.b);
        p4.writeInt(this.p().size());
        Object v0_4 = this.p().iterator();
        while (v0_4.hasNext()) {
            p4.writeObject(v0_4.next());
        }
        p4.writeInt(this.f());
        java.util.Iterator v1_1 = this.u().iterator();
        while (v1_1.hasNext()) {
            Object v0_9 = ((java.util.Map$Entry) v1_1.next());
            p4.writeObject(v0_9.getKey());
            p4.writeObject(v0_9.getValue());
        }
        return;
    }

    private static com.a.b.d.oc b(com.a.b.d.vi p3)
    {
        com.a.b.d.oc v1_1 = new com.a.b.d.oc(com.a.b.d.sz.b(p3.p().size()), com.a.b.d.sz.b(2));
        v1_1.a(p3);
        return v1_1;
    }

    private static void b(com.a.b.d.oe p2)
    {
        com.a.b.d.oc.b(p2.g, p2.h);
        return;
    }

    private static void b(com.a.b.d.oe p0, com.a.b.d.oe p1)
    {
        p0.h = p1;
        p1.g = p0;
        return;
    }

    private static void b(com.a.b.d.oh p2)
    {
        com.a.b.d.oc.b(p2.a(), p2.b());
        return;
    }

    private static void b(com.a.b.d.oh p0, com.a.b.d.oh p1)
    {
        p0.b(p1);
        p1.a(p0);
        return;
    }

    private static com.a.b.d.oc v()
    {
        return new com.a.b.d.oc(16, 2);
    }

    final java.util.Set a()
    {
        return new java.util.LinkedHashSet(this.b);
    }

    public final bridge synthetic java.util.Set a(Object p2)
    {
        return super.a(p2);
    }

    public final java.util.Set a(Object p2, Iterable p3)
    {
        return super.a(p2, p3);
    }

    public final bridge synthetic boolean a(com.a.b.d.vi p2)
    {
        return super.a(p2);
    }

    public final bridge synthetic boolean a(Object p2, Object p3)
    {
        return super.a(p2, p3);
    }

    public final synthetic java.util.Collection b(Object p2, Iterable p3)
    {
        return this.a(p2, p3);
    }

    public final bridge synthetic java.util.Map b()
    {
        return super.b();
    }

    public final bridge synthetic java.util.Set b(Object p2)
    {
        return super.b(p2);
    }

    public final bridge synthetic boolean b(Object p2, Object p3)
    {
        return super.b(p2, p3);
    }

    final synthetic java.util.Collection c()
    {
        return this.a();
    }

    public final bridge synthetic boolean c(Object p2, Iterable p3)
    {
        return super.c(p2, p3);
    }

    public final bridge synthetic boolean c(Object p2, Object p3)
    {
        return super.c(p2, p3);
    }

    final java.util.Collection e(Object p3)
    {
        return new com.a.b.d.of(this, p3, this.b);
    }

    public final bridge synthetic boolean equals(Object p2)
    {
        return super.equals(p2);
    }

    public final bridge synthetic int f()
    {
        return super.f();
    }

    public final bridge synthetic boolean f(Object p2)
    {
        return super.f(p2);
    }

    public final void g()
    {
        super.g();
        com.a.b.d.oc.b(this.e, this.e);
        return;
    }

    public final bridge synthetic boolean g(Object p2)
    {
        return super.g(p2);
    }

    public final bridge synthetic int hashCode()
    {
        return super.hashCode();
    }

    public final java.util.Collection i()
    {
        return super.i();
    }

    final java.util.Iterator j()
    {
        return com.a.b.d.sz.b(this.l());
    }

    public final synthetic java.util.Collection k()
    {
        return this.u();
    }

    final java.util.Iterator l()
    {
        return new com.a.b.d.od(this);
    }

    public final bridge synthetic boolean n()
    {
        return super.n();
    }

    public final bridge synthetic java.util.Set p()
    {
        return super.p();
    }

    public final bridge synthetic com.a.b.d.xc q()
    {
        return super.q();
    }

    public final bridge synthetic String toString()
    {
        return super.toString();
    }

    public final java.util.Set u()
    {
        return super.u();
    }
}
