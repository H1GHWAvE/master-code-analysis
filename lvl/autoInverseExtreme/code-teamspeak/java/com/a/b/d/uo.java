package com.a.b.d;
final class uo extends com.a.b.d.tm implements java.util.SortedMap {

    uo(java.util.SortedSet p1, com.a.b.b.bj p2)
    {
        this(p1, p2);
        return;
    }

    private java.util.SortedSet d()
    {
        return ((java.util.SortedSet) super.c());
    }

    final bridge synthetic java.util.Set c()
    {
        return ((java.util.SortedSet) super.c());
    }

    public final java.util.Comparator comparator()
    {
        return ((java.util.SortedSet) super.c()).comparator();
    }

    public final Object firstKey()
    {
        return ((java.util.SortedSet) super.c()).first();
    }

    public final java.util.SortedMap headMap(Object p3)
    {
        return com.a.b.d.sz.a(((java.util.SortedSet) super.c()).headSet(p3), this.a);
    }

    public final java.util.Set keySet()
    {
        return com.a.b.d.sz.a(((java.util.SortedSet) super.c()));
    }

    public final Object lastKey()
    {
        return ((java.util.SortedSet) super.c()).last();
    }

    public final java.util.SortedMap subMap(Object p3, Object p4)
    {
        return com.a.b.d.sz.a(((java.util.SortedSet) super.c()).subSet(p3, p4), this.a);
    }

    public final java.util.SortedMap tailMap(Object p3)
    {
        return com.a.b.d.sz.a(((java.util.SortedSet) super.c()).tailSet(p3), this.a);
    }
}
