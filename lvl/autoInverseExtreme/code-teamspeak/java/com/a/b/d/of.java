package com.a.b.d;
final class of extends com.a.b.d.aan implements com.a.b.d.oh {
    com.a.b.d.oe[] a;
    final synthetic com.a.b.d.oc b;
    private final Object c;
    private int d;
    private int e;
    private com.a.b.d.oh f;
    private com.a.b.d.oh g;

    of(com.a.b.d.oc p3, Object p4, int p5)
    {
        this.b = p3;
        this.d = 0;
        this.e = 0;
        this.c = p4;
        this.f = this;
        this.g = this;
        com.a.b.d.oe[] v0_3 = new com.a.b.d.oe[com.a.b.d.iq.a(p5, 1.0)];
        this.a = v0_3;
        return;
    }

    static synthetic com.a.b.d.oh a(com.a.b.d.of p1)
    {
        return p1.f;
    }

    static synthetic int b(com.a.b.d.of p1)
    {
        return p1.e;
    }

    private int c()
    {
        return (this.a.length - 1);
    }

    private void d()
    {
        if (com.a.b.d.iq.a(this.d, this.a.length)) {
            com.a.b.d.oe[] v2 = new com.a.b.d.oe[(this.a.length * 2)];
            this.a = v2;
            com.a.b.d.oh v1_2 = this.f;
            while (v1_2 != this) {
                com.a.b.d.oe v0_7 = ((com.a.b.d.oe) v1_2);
                int v4_1 = (v0_7.a & (v2.length - 1));
                v0_7.b = v2[v4_1];
                v2[v4_1] = v0_7;
                v1_2 = v1_2.b();
            }
        }
        return;
    }

    public final com.a.b.d.oh a()
    {
        return this.g;
    }

    public final void a(com.a.b.d.oh p1)
    {
        this.g = p1;
        return;
    }

    public final boolean add(Object p7)
    {
        com.a.b.d.oe[] v2_0 = com.a.b.d.iq.a(p7);
        int v3_0 = (v2_0 & (this.a.length - 1));
        com.a.b.d.oh v1_0 = this.a[v3_0];
        com.a.b.d.oe v0_4 = v1_0;
        while (v0_4 != null) {
            if (!v0_4.a(p7, v2_0)) {
                v0_4 = v0_4.b;
            } else {
                com.a.b.d.oe v0_17 = 0;
            }
            return v0_17;
        }
        com.a.b.d.oe v0_6 = new com.a.b.d.oe(this.c, p7, v2_0, v1_0);
        com.a.b.d.oc.a(this.g, v0_6);
        com.a.b.d.oc.a(v0_6, this);
        com.a.b.d.oc.a(com.a.b.d.oc.a(this.b).g, v0_6);
        com.a.b.d.oc.a(v0_6, com.a.b.d.oc.a(this.b));
        this.a[v3_0] = v0_6;
        this.d = (this.d + 1);
        this.e = (this.e + 1);
        if (com.a.b.d.iq.a(this.d, this.a.length)) {
            com.a.b.d.oe[] v2_1 = new com.a.b.d.oe[(this.a.length * 2)];
            this.a = v2_1;
            com.a.b.d.oh v1_10 = this.f;
            while (v1_10 != this) {
                com.a.b.d.oe v0_19 = ((com.a.b.d.oe) v1_10);
                int v4_2 = (v0_19.a & (v2_1.length - 1));
                v0_19.b = v2_1[v4_2];
                v2_1[v4_2] = v0_19;
                v1_10 = v1_10.b();
            }
        }
        v0_17 = 1;
        return v0_17;
    }

    public final com.a.b.d.oh b()
    {
        return this.f;
    }

    public final void b(com.a.b.d.oh p1)
    {
        this.f = p1;
        return;
    }

    public final void clear()
    {
        java.util.Arrays.fill(this.a, 0);
        this.d = 0;
        com.a.b.d.oh v1_1 = this.f;
        while (v1_1 != this) {
            com.a.b.d.oc.a(((com.a.b.d.oe) v1_1));
            v1_1 = v1_1.b();
        }
        com.a.b.d.oc.a(this, this);
        this.e = (this.e + 1);
        return;
    }

    public final boolean contains(Object p4)
    {
        int v1 = com.a.b.d.iq.a(p4);
        com.a.b.d.oe v0_1 = this.a[((this.a.length - 1) & v1)];
        while (v0_1 != null) {
            if (!v0_1.a(p4, v1)) {
                v0_1 = v0_1.b;
            } else {
                com.a.b.d.oe v0_2 = 1;
            }
            return v0_2;
        }
        v0_2 = 0;
        return v0_2;
    }

    public final java.util.Iterator iterator()
    {
        return new com.a.b.d.og(this);
    }

    public final boolean remove(Object p7)
    {
        com.a.b.d.oe v2_0 = com.a.b.d.iq.a(p7);
        int v3 = (v2_0 & (this.a.length - 1));
        com.a.b.d.oe[] v1_0 = 0;
        int v0_4 = this.a[v3];
        while (v0_4 != 0) {
            if (!v0_4.a(p7, v2_0)) {
                v1_0 = v0_4;
                v0_4 = v0_4.b;
            } else {
                if (v1_0 != null) {
                    v1_0.b = v0_4.b;
                } else {
                    this.a[v3] = v0_4.b;
                }
                com.a.b.d.oc.a(v0_4);
                com.a.b.d.oc.a(v0_4);
                this.d = (this.d - 1);
                this.e = (this.e + 1);
                int v0_5 = 1;
            }
            return v0_5;
        }
        v0_5 = 0;
        return v0_5;
    }

    public final int size()
    {
        return this.d;
    }
}
