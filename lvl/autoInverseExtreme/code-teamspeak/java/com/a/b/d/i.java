package com.a.b.d;
abstract class i extends com.a.b.d.agj {
    private final int a;
    private int b;

    protected i(int p2)
    {
        this(p2, 0);
        return;
    }

    protected i(int p1, int p2)
    {
        com.a.b.b.cn.b(p2, p1);
        this.a = p1;
        this.b = p2;
        return;
    }

    protected abstract Object a();

    public final boolean hasNext()
    {
        int v0_1;
        if (this.b >= this.a) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final boolean hasPrevious()
    {
        int v0_1;
        if (this.b <= 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final Object next()
    {
        if (this.hasNext()) {
            Object v0_1 = this.b;
            this.b = (v0_1 + 1);
            return this.a(v0_1);
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public final int nextIndex()
    {
        return this.b;
    }

    public final Object previous()
    {
        if (this.hasPrevious()) {
            Object v0_2 = (this.b - 1);
            this.b = v0_2;
            return this.a(v0_2);
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public final int previousIndex()
    {
        return (this.b - 1);
    }
}
