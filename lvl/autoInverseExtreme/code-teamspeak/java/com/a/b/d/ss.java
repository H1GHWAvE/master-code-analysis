package com.a.b.d;
final class ss extends java.util.AbstractCollection {
    final synthetic com.a.b.d.qy a;

    ss(com.a.b.d.qy p1)
    {
        this.a = p1;
        return;
    }

    public final void clear()
    {
        this.a.clear();
        return;
    }

    public final boolean contains(Object p2)
    {
        return this.a.containsValue(p2);
    }

    public final boolean isEmpty()
    {
        return this.a.isEmpty();
    }

    public final java.util.Iterator iterator()
    {
        return new com.a.b.d.sq(this.a);
    }

    public final int size()
    {
        return this.a.size();
    }
}
