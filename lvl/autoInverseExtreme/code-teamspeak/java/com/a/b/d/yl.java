package com.a.b.d;
public final class yl implements com.a.b.b.co, java.io.Serializable {
    static final com.a.b.d.yd a;
    private static final com.a.b.b.bj d;
    private static final com.a.b.b.bj e;
    private static final com.a.b.d.yl f;
    private static final long g;
    final com.a.b.d.dw b;
    final com.a.b.d.dw c;

    static yl()
    {
        com.a.b.d.yl.d = new com.a.b.d.ym();
        com.a.b.d.yl.e = new com.a.b.d.yn();
        com.a.b.d.yl.a = new com.a.b.d.yo();
        com.a.b.d.yl.f = new com.a.b.d.yl(com.a.b.d.dw.d(), com.a.b.d.dw.e());
        return;
    }

    private yl(com.a.b.d.dw p5, com.a.b.d.dw p6)
    {
        if ((p5.a(p6) <= 0) && ((p5 != com.a.b.d.dw.e()) && (p6 != com.a.b.d.dw.d()))) {
            this.b = ((com.a.b.d.dw) com.a.b.b.cn.a(p5));
            this.c = ((com.a.b.d.dw) com.a.b.b.cn.a(p6));
            return;
        } else {
            com.a.b.d.dw v0_10;
            com.a.b.d.dw v0_8 = String.valueOf(com.a.b.d.yl.b(p5, p6));
            if (v0_8.length() == 0) {
                v0_10 = new String("Invalid range: ");
            } else {
                v0_10 = "Invalid range: ".concat(v0_8);
            }
            throw new IllegalArgumentException(v0_10);
        }
    }

    static com.a.b.b.bj a()
    {
        return com.a.b.d.yl.d;
    }

    static com.a.b.d.yl a(com.a.b.d.dw p1, com.a.b.d.dw p2)
    {
        return new com.a.b.d.yl(p1, p2);
    }

    private com.a.b.d.yl a(com.a.b.d.ep p4)
    {
        com.a.b.b.cn.a(p4);
        com.a.b.d.dw v0_1 = this.b.c(p4);
        com.a.b.d.dw v1_1 = this.c.c(p4);
        if ((v0_1 != this.b) || (v1_1 != this.c)) {
            this = com.a.b.d.yl.a(v0_1, v1_1);
        }
        return this;
    }

    public static com.a.b.d.yl a(Comparable p2)
    {
        return com.a.b.d.yl.a(com.a.b.d.dw.d(), com.a.b.d.dw.c(p2));
    }

    public static com.a.b.d.yl a(Comparable p2, com.a.b.d.ce p3)
    {
        com.a.b.d.yl v0_2;
        switch (com.a.b.d.yp.a[p3.ordinal()]) {
            case 1:
                v0_2 = com.a.b.d.yl.a(com.a.b.d.dw.d(), com.a.b.d.dw.b(p2));
                break;
            case 2:
                v0_2 = com.a.b.d.yl.a(p2);
                break;
            default:
                throw new AssertionError();
        }
        return v0_2;
    }

    public static com.a.b.d.yl a(Comparable p2, com.a.b.d.ce p3, Comparable p4, com.a.b.d.ce p5)
    {
        com.a.b.d.yl v0_1;
        com.a.b.b.cn.a(p3);
        com.a.b.b.cn.a(p5);
        if (p3 != com.a.b.d.ce.a) {
            v0_1 = com.a.b.d.dw.b(p2);
        } else {
            v0_1 = com.a.b.d.dw.c(p2);
        }
        com.a.b.d.dw v1_1;
        if (p5 != com.a.b.d.ce.a) {
            v1_1 = com.a.b.d.dw.c(p4);
        } else {
            v1_1 = com.a.b.d.dw.b(p4);
        }
        return com.a.b.d.yl.a(v0_1, v1_1);
    }

    public static com.a.b.d.yl a(Comparable p2, Comparable p3)
    {
        return com.a.b.d.yl.a(com.a.b.d.dw.b(p2), com.a.b.d.dw.c(p3));
    }

    private static com.a.b.d.yl a(Iterable p5)
    {
        com.a.b.d.yl v0_5;
        com.a.b.b.cn.a(p5);
        if (!(p5 instanceof com.a.b.d.du)) {
            java.util.Iterator v3 = p5.iterator();
            com.a.b.d.yl v0_3 = ((Comparable) com.a.b.b.cn.a(v3.next()));
            Comparable v2 = v0_3;
            Comparable v1_0 = v0_3;
            while (v3.hasNext()) {
                com.a.b.d.yl v0_8 = ((Comparable) com.a.b.b.cn.a(v3.next()));
                v1_0 = ((Comparable) com.a.b.d.yd.d().a(v1_0, v0_8));
                v2 = ((Comparable) com.a.b.d.yd.d().b(v2, v0_8));
            }
            v0_5 = com.a.b.d.yl.a(v1_0, v2);
        } else {
            v0_5 = ((com.a.b.d.du) p5).f_();
        }
        return v0_5;
    }

    static int b(Comparable p1, Comparable p2)
    {
        return p1.compareTo(p2);
    }

    static com.a.b.b.bj b()
    {
        return com.a.b.d.yl.e;
    }

    public static com.a.b.d.yl b(Comparable p2)
    {
        return com.a.b.d.yl.a(com.a.b.d.dw.b(p2), com.a.b.d.dw.e());
    }

    public static com.a.b.d.yl b(Comparable p2, com.a.b.d.ce p3)
    {
        com.a.b.d.yl v0_2;
        switch (com.a.b.d.yp.a[p3.ordinal()]) {
            case 1:
                v0_2 = com.a.b.d.yl.a(com.a.b.d.dw.c(p2), com.a.b.d.dw.e());
                break;
            case 2:
                v0_2 = com.a.b.d.yl.b(p2);
                break;
            default:
                throw new AssertionError();
        }
        return v0_2;
    }

    private static String b(com.a.b.d.dw p2, com.a.b.d.dw p3)
    {
        String v0_1 = new StringBuilder(16);
        p2.a(v0_1);
        v0_1.append(8229);
        p3.b(v0_1);
        return v0_1.toString();
    }

    private boolean b(Iterable p6)
    {
        int v0_3;
        if (!(p6 instanceof java.util.Collection)) {
            if (p6.iterator().hasNext()) {
                v0_3 = 0;
            } else {
                v0_3 = 1;
            }
        } else {
            v0_3 = ((java.util.Collection) p6).isEmpty();
        }
        int v0_10;
        if (v0_3 == 0) {
            if ((p6 instanceof java.util.SortedSet)) {
                boolean v1_0 = ((java.util.SortedSet) p6).comparator();
                if ((com.a.b.d.yd.d().equals(v1_0)) || (!v1_0)) {
                    if ((!this.c(((Comparable) ((java.util.SortedSet) p6).first()))) || (!this.c(((Comparable) ((java.util.SortedSet) p6).last())))) {
                        v0_10 = 0;
                        return v0_10;
                    } else {
                        v0_10 = 1;
                        return v0_10;
                    }
                }
            }
            boolean v1_1 = p6.iterator();
            while (v1_1.hasNext()) {
                if (!this.c(((Comparable) v1_1.next()))) {
                    v0_10 = 0;
                }
            }
            v0_10 = 1;
        } else {
            v0_10 = 1;
        }
        return v0_10;
    }

    public static com.a.b.d.yl c()
    {
        return com.a.b.d.yl.f;
    }

    private static com.a.b.d.yl c(Comparable p2, Comparable p3)
    {
        return com.a.b.d.yl.a(com.a.b.d.dw.c(p2), com.a.b.d.dw.b(p3));
    }

    private static java.util.SortedSet c(Iterable p0)
    {
        return ((java.util.SortedSet) p0);
    }

    private com.a.b.d.yl d(com.a.b.d.yl p4)
    {
        com.a.b.d.dw v0_1 = this.b.a(p4.b);
        int v2_1 = this.c.a(p4.c);
        if ((v0_1 > null) || (v2_1 < 0)) {
            if ((v0_1 < null) || (v2_1 > 0)) {
                com.a.b.d.dw v1_2;
                if (v0_1 > null) {
                    v1_2 = p4.b;
                } else {
                    v1_2 = this.b;
                }
                com.a.b.d.dw v0_4;
                if (v2_1 < 0) {
                    v0_4 = p4.c;
                } else {
                    v0_4 = this.c;
                }
                this = com.a.b.d.yl.a(v1_2, v0_4);
            } else {
                this = p4;
            }
        }
        return this;
    }

    private static com.a.b.d.yl d(Comparable p2)
    {
        return com.a.b.d.yl.a(com.a.b.d.dw.d(), com.a.b.d.dw.b(p2));
    }

    private static com.a.b.d.yl d(Comparable p2, Comparable p3)
    {
        return com.a.b.d.yl.a(com.a.b.d.dw.b(p2), com.a.b.d.dw.b(p3));
    }

    private static com.a.b.d.yl e(Comparable p2)
    {
        return com.a.b.d.yl.a(com.a.b.d.dw.c(p2), com.a.b.d.dw.e());
    }

    private static com.a.b.d.yl e(Comparable p2, Comparable p3)
    {
        return com.a.b.d.yl.a(com.a.b.d.dw.c(p2), com.a.b.d.dw.c(p3));
    }

    private static com.a.b.d.yl f(Comparable p1)
    {
        return com.a.b.d.yl.a(p1, p1);
    }

    private Comparable g()
    {
        return this.b.c();
    }

    private boolean g(Comparable p2)
    {
        return this.c(p2);
    }

    private com.a.b.d.ce h()
    {
        return this.b.a();
    }

    private Comparable i()
    {
        return this.c.c();
    }

    private com.a.b.d.ce j()
    {
        return this.c.b();
    }

    private Object k()
    {
        if (this.equals(com.a.b.d.yl.f)) {
            this = com.a.b.d.yl.f;
        }
        return this;
    }

    public final boolean a(com.a.b.d.yl p3)
    {
        if ((this.b.a(p3.b) > 0) || (this.c.a(p3.c) < 0)) {
            int v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    public final synthetic boolean a(Object p2)
    {
        return this.c(((Comparable) p2));
    }

    public final boolean b(com.a.b.d.yl p3)
    {
        if ((this.b.a(p3.c) > 0) || (p3.b.a(this.c) > 0)) {
            int v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    public final com.a.b.d.yl c(com.a.b.d.yl p4)
    {
        com.a.b.d.dw v0_1 = this.b.a(p4.b);
        int v2_1 = this.c.a(p4.c);
        if ((v0_1 < null) || (v2_1 > 0)) {
            if ((v0_1 > null) || (v2_1 < 0)) {
                com.a.b.d.dw v1_2;
                if (v0_1 < null) {
                    v1_2 = p4.b;
                } else {
                    v1_2 = this.b;
                }
                com.a.b.d.dw v0_4;
                if (v2_1 > 0) {
                    v0_4 = p4.c;
                } else {
                    v0_4 = this.c;
                }
                this = com.a.b.d.yl.a(v1_2, v0_4);
            } else {
                this = p4;
            }
        }
        return this;
    }

    public final boolean c(Comparable p2)
    {
        int v0_4;
        com.a.b.b.cn.a(p2);
        if ((!this.b.a(p2)) || (this.c.a(p2))) {
            v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    public final boolean d()
    {
        int v0_1;
        if (this.b == com.a.b.d.dw.d()) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final boolean e()
    {
        int v0_1;
        if (this.c == com.a.b.d.dw.e()) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final boolean equals(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof com.a.b.d.yl)) && ((this.b.equals(((com.a.b.d.yl) p4).b)) && (this.c.equals(((com.a.b.d.yl) p4).c)))) {
            v0 = 1;
        }
        return v0;
    }

    public final boolean f()
    {
        return this.b.equals(this.c);
    }

    public final int hashCode()
    {
        return ((this.b.hashCode() * 31) + this.c.hashCode());
    }

    public final String toString()
    {
        return com.a.b.d.yl.b(this.b, this.c);
    }
}
