package com.a.b.d;
abstract class tu extends com.a.b.d.aan {

    tu()
    {
        return;
    }

    abstract java.util.Map a();

    public void clear()
    {
        this.a().clear();
        return;
    }

    public boolean contains(Object p5)
    {
        int v0 = 0;
        if ((p5 instanceof java.util.Map$Entry)) {
            boolean v1_1 = ((java.util.Map$Entry) p5).getKey();
            java.util.Map v2_1 = com.a.b.d.sz.a(this.a(), v1_1);
            if ((com.a.b.b.ce.a(v2_1, ((java.util.Map$Entry) p5).getValue())) && ((v2_1 != null) || (this.a().containsKey(v1_1)))) {
                v0 = 1;
            }
        }
        return v0;
    }

    public boolean isEmpty()
    {
        return this.a().isEmpty();
    }

    public boolean remove(Object p3)
    {
        int v0_1;
        if (!this.contains(p3)) {
            v0_1 = 0;
        } else {
            v0_1 = this.a().keySet().remove(((java.util.Map$Entry) p3).getKey());
        }
        return v0_1;
    }

    public boolean removeAll(java.util.Collection p2)
    {
        try {
            boolean v0_2 = super.removeAll(((java.util.Collection) com.a.b.b.cn.a(p2)));
        } catch (boolean v0) {
            v0_2 = com.a.b.d.aad.a(this, p2.iterator());
        }
        return v0_2;
    }

    public boolean retainAll(java.util.Collection p5)
    {
        try {
            Object v0_2 = super.retainAll(((java.util.Collection) com.a.b.b.cn.a(p5)));
        } catch (Object v0) {
            java.util.HashSet v1 = com.a.b.d.aad.a(p5.size());
            java.util.Iterator v2 = p5.iterator();
        }
        return v0_2;
    }

    public int size()
    {
        return this.a().size();
    }
}
