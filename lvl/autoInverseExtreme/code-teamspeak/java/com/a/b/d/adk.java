package com.a.b.d;
final class adk extends com.a.b.d.add implements com.a.b.d.xc {
    private static final long c;
    transient java.util.Set a;
    transient java.util.Set b;

    adk(com.a.b.d.xc p2, Object p3)
    {
        this(p2, p3, 0);
        return;
    }

    private com.a.b.d.xc c()
    {
        return ((com.a.b.d.xc) super.b());
    }

    public final int a(Object p3)
    {
        try {
            return ((com.a.b.d.xc) super.b()).a(p3);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final int a(Object p3, int p4)
    {
        try {
            return ((com.a.b.d.xc) super.b()).a(p3, p4);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final java.util.Set a()
    {
        try {
            if (this.b == null) {
                this.b = com.a.b.d.acu.b(((com.a.b.d.xc) super.b()).a(), this.h);
            }
        } catch (java.util.Set v0_6) {
            throw v0_6;
        }
        return this.b;
    }

    public final boolean a(Object p3, int p4, int p5)
    {
        try {
            return ((com.a.b.d.xc) super.b()).a(p3, p4, p5);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final int b(Object p3, int p4)
    {
        try {
            return ((com.a.b.d.xc) super.b()).b(p3, p4);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    final bridge synthetic java.util.Collection b()
    {
        return ((com.a.b.d.xc) super.b());
    }

    public final int c(Object p3, int p4)
    {
        try {
            return ((com.a.b.d.xc) super.b()).c(p3, p4);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    final synthetic Object d()
    {
        return ((com.a.b.d.xc) super.b());
    }

    public final boolean equals(Object p3)
    {
        Throwable v0_2;
        if (p3 != this) {
            try {
                v0_2 = ((com.a.b.d.xc) super.b()).equals(p3);
            } catch (Throwable v0_3) {
                throw v0_3;
            }
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final int hashCode()
    {
        try {
            return ((com.a.b.d.xc) super.b()).hashCode();
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public final java.util.Set n_()
    {
        try {
            if (this.a == null) {
                this.a = com.a.b.d.acu.b(((com.a.b.d.xc) super.b()).n_(), this.h);
            }
        } catch (java.util.Set v0_6) {
            throw v0_6;
        }
        return this.a;
    }
}
