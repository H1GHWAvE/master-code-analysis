package com.a.b.d;
 class ah extends com.a.b.d.ab implements java.util.SortedSet {
    final synthetic com.a.b.d.n g;

    ah(com.a.b.d.n p1, Object p2, java.util.SortedSet p3, com.a.b.d.ab p4)
    {
        this.g = p1;
        this(p1, p2, p3, p4);
        return;
    }

    public java.util.Comparator comparator()
    {
        return this.d().comparator();
    }

    java.util.SortedSet d()
    {
        return ((java.util.SortedSet) this.c);
    }

    public Object first()
    {
        this.a();
        return this.d().first();
    }

    public java.util.SortedSet headSet(Object p6)
    {
        this.a();
        com.a.b.d.n v1 = this.g;
        Object v2 = this.b;
        java.util.SortedSet v3_1 = this.d().headSet(p6);
        if (this.d != null) {
            this = this.d;
        }
        return new com.a.b.d.ah(v1, v2, v3_1, this);
    }

    public Object last()
    {
        this.a();
        return this.d().last();
    }

    public java.util.SortedSet subSet(Object p6, Object p7)
    {
        this.a();
        com.a.b.d.n v1 = this.g;
        Object v2 = this.b;
        java.util.SortedSet v3_1 = this.d().subSet(p6, p7);
        if (this.d != null) {
            this = this.d;
        }
        return new com.a.b.d.ah(v1, v2, v3_1, this);
    }

    public java.util.SortedSet tailSet(Object p6)
    {
        this.a();
        com.a.b.d.n v1 = this.g;
        Object v2 = this.b;
        java.util.SortedSet v3_1 = this.d().tailSet(p6);
        if (this.d != null) {
            this = this.d;
        }
        return new com.a.b.d.ah(v1, v2, v3_1, this);
    }
}
