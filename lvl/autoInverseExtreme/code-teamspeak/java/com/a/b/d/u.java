package com.a.b.d;
 class u extends com.a.b.d.uk {
    final synthetic com.a.b.d.n a;

    u(com.a.b.d.n p1, java.util.Map p2)
    {
        this.a = p1;
        this(p2);
        return;
    }

    public void clear()
    {
        com.a.b.d.nj.i(this.iterator());
        return;
    }

    public boolean containsAll(java.util.Collection p2)
    {
        return this.b().keySet().containsAll(p2);
    }

    public boolean equals(Object p2)
    {
        if ((this != p2) && (!this.b().keySet().equals(p2))) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public int hashCode()
    {
        return this.b().keySet().hashCode();
    }

    public java.util.Iterator iterator()
    {
        return new com.a.b.d.v(this, this.b().entrySet().iterator());
    }

    public boolean remove(Object p4)
    {
        int v0_3;
        int v0_2 = ((java.util.Collection) this.b().remove(p4));
        if (v0_2 == 0) {
            v0_3 = 0;
        } else {
            int v2 = v0_2.size();
            v0_2.clear();
            com.a.b.d.n.b(this.a, v2);
            v0_3 = v2;
        }
        int v0_5;
        if (v0_3 <= 0) {
            v0_5 = 0;
        } else {
            v0_5 = 1;
        }
        return v0_5;
    }
}
