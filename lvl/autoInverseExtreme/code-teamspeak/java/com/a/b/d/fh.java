package com.a.b.d;
final class fh extends com.a.b.d.yd implements java.io.Serializable {
    private static final long b;
    final com.a.b.d.jt a;

    private fh(com.a.b.d.jt p1)
    {
        this.a = p1;
        return;
    }

    fh(java.util.List p2)
    {
        this(com.a.b.d.fh.a(p2));
        return;
    }

    private int a(Object p2)
    {
        int v0_2 = ((Integer) this.a.get(p2));
        if (v0_2 != 0) {
            return v0_2.intValue();
        } else {
            throw new com.a.b.d.yh(p2);
        }
    }

    private static com.a.b.d.jt a(java.util.List p5)
    {
        com.a.b.d.ju v2 = com.a.b.d.jt.l();
        com.a.b.d.jt v0_0 = 0;
        java.util.Iterator v3 = p5.iterator();
        while (v3.hasNext()) {
            int v1_1 = (v0_0 + 1);
            v2.a(v3.next(), Integer.valueOf(v0_0));
            v0_0 = v1_1;
        }
        return v2.a();
    }

    public final int compare(Object p3, Object p4)
    {
        return (this.a(p3) - this.a(p4));
    }

    public final boolean equals(Object p3)
    {
        int v0_1;
        if (!(p3 instanceof com.a.b.d.fh)) {
            v0_1 = 0;
        } else {
            v0_1 = this.a.equals(((com.a.b.d.fh) p3).a);
        }
        return v0_1;
    }

    public final int hashCode()
    {
        return this.a.hashCode();
    }

    public final String toString()
    {
        String v0_3 = String.valueOf(String.valueOf(this.a.g()));
        return new StringBuilder((v0_3.length() + 19)).append("Ordering.explicit(").append(v0_3).append(")").toString();
    }
}
