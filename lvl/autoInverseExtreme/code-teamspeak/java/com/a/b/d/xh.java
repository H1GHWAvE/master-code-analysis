package com.a.b.d;
final class xh extends com.a.b.d.as {
    final synthetic com.a.b.d.xc a;
    final synthetic com.a.b.d.xc b;

    xh(com.a.b.d.xc p1, com.a.b.d.xc p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    public final int a(Object p3)
    {
        int v0_2;
        int v0_1 = this.a.a(p3);
        if (v0_1 != 0) {
            v0_2 = Math.min(v0_1, this.b.a(p3));
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    final java.util.Iterator b()
    {
        return new com.a.b.d.xi(this, this.a.a().iterator());
    }

    final int c()
    {
        return this.n_().size();
    }

    final java.util.Set e()
    {
        return com.a.b.d.aad.b(this.a.n_(), this.b.n_());
    }
}
