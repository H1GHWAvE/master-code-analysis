package com.a.b.d;
final class ph extends com.a.b.d.jl {
    private final String a;

    ph(String p1)
    {
        this.a = p1;
        return;
    }

    private Character b(int p2)
    {
        com.a.b.b.cn.a(p2, this.size());
        return Character.valueOf(this.a.charAt(p2));
    }

    public final com.a.b.d.jl a(int p3, int p4)
    {
        com.a.b.b.cn.a(p3, p4, this.size());
        return new com.a.b.d.ph(((String) com.a.b.b.cn.a(this.a.substring(p3, p4))));
    }

    public final synthetic Object get(int p2)
    {
        com.a.b.b.cn.a(p2, this.size());
        return Character.valueOf(this.a.charAt(p2));
    }

    final boolean h_()
    {
        return 0;
    }

    public final int indexOf(Object p3)
    {
        int v0_1;
        if (!(p3 instanceof Character)) {
            v0_1 = -1;
        } else {
            v0_1 = this.a.indexOf(((Character) p3).charValue());
        }
        return v0_1;
    }

    public final int lastIndexOf(Object p3)
    {
        int v0_1;
        if (!(p3 instanceof Character)) {
            v0_1 = -1;
        } else {
            v0_1 = this.a.lastIndexOf(((Character) p3).charValue());
        }
        return v0_1;
    }

    public final int size()
    {
        return this.a.length();
    }

    public final synthetic java.util.List subList(int p2, int p3)
    {
        return this.a(p2, p3);
    }
}
