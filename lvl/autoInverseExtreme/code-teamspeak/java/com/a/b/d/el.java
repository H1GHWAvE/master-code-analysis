package com.a.b.d;
final class el extends com.a.b.d.ma {
    private final transient com.a.b.d.ma b;

    el(com.a.b.d.ma p1)
    {
        this.b = p1;
        return;
    }

    public final int a(Object p2)
    {
        return this.b.a(p2);
    }

    public final com.a.b.d.ma a(Object p2, com.a.b.d.ce p3)
    {
        return this.b.b(p2, p3).e();
    }

    final com.a.b.d.xd a(int p2)
    {
        return ((com.a.b.d.xd) this.b.o().f().e().get(p2));
    }

    public final com.a.b.d.ma b(Object p2, com.a.b.d.ce p3)
    {
        return this.b.a(p2, p3).e();
    }

    public final com.a.b.d.me b()
    {
        return this.b.b().b();
    }

    public final synthetic com.a.b.d.abn c(Object p2, com.a.b.d.ce p3)
    {
        return this.b(p2, p3);
    }

    public final synthetic com.a.b.d.abn d(Object p2, com.a.b.d.ce p3)
    {
        return this.a(p2, p3);
    }

    public final com.a.b.d.ma e()
    {
        return this.b;
    }

    public final synthetic java.util.NavigableSet e_()
    {
        return this.b();
    }

    public final com.a.b.d.xd h()
    {
        return this.b.i();
    }

    final boolean h_()
    {
        return this.b.h_();
    }

    public final com.a.b.d.xd i()
    {
        return this.b.h();
    }

    public final bridge synthetic com.a.b.d.abn m()
    {
        return this.b;
    }

    public final synthetic java.util.SortedSet n()
    {
        return this.b();
    }

    public final synthetic java.util.Set n_()
    {
        return this.b();
    }

    public final int size()
    {
        return this.b.size();
    }
}
