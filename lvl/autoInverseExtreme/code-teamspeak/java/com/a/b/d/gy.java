package com.a.b.d;
public abstract class gy extends com.a.b.d.gh implements com.a.b.d.xc {

    protected gy()
    {
        return;
    }

    private boolean b(Object p2, int p3, int p4)
    {
        return com.a.b.d.xe.a(this, p2, p3, p4);
    }

    private java.util.Iterator c()
    {
        return com.a.b.d.xe.b(this);
    }

    private int d(Object p4)
    {
        java.util.Iterator v1 = this.a().iterator();
        while (v1.hasNext()) {
            int v0_4 = ((com.a.b.d.xd) v1.next());
            if (com.a.b.b.ce.a(v0_4.a(), p4)) {
                int v0_2 = v0_4.b();
            }
            return v0_2;
        }
        v0_2 = 0;
        return v0_2;
    }

    private int d(Object p2, int p3)
    {
        return com.a.b.d.xe.a(this, p2, p3);
    }

    private int e()
    {
        return com.a.b.d.xe.c(this);
    }

    private boolean e(Object p2)
    {
        this.a(p2, 1);
        return 1;
    }

    private boolean f(Object p2)
    {
        return com.a.b.d.xe.a(this, p2);
    }

    private int q()
    {
        return this.a().hashCode();
    }

    public final int a(Object p2)
    {
        return this.f().a(p2);
    }

    public int a(Object p2, int p3)
    {
        return this.f().a(p2, p3);
    }

    public java.util.Set a()
    {
        return this.f().a();
    }

    public boolean a(Object p2, int p3, int p4)
    {
        return this.f().a(p2, p3, p4);
    }

    protected final boolean a(java.util.Collection p2)
    {
        return com.a.b.d.xe.a(this, p2);
    }

    public int b(Object p2, int p3)
    {
        return this.f().b(p2, p3);
    }

    protected synthetic java.util.Collection b()
    {
        return this.f();
    }

    protected final boolean b(Object p2)
    {
        int v0_1;
        if (this.a(p2) <= 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    protected final boolean b(java.util.Collection p2)
    {
        return com.a.b.d.xe.b(this, p2);
    }

    public int c(Object p2, int p3)
    {
        return this.f().c(p2, p3);
    }

    protected final boolean c(Object p3)
    {
        int v0 = 1;
        if (this.b(p3, 1) <= 0) {
            v0 = 0;
        }
        return v0;
    }

    protected final boolean c(java.util.Collection p2)
    {
        return com.a.b.d.xe.c(this, p2);
    }

    public boolean equals(Object p2)
    {
        if ((p2 != this) && (!this.f().equals(p2))) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    protected abstract com.a.b.d.xc f();

    public int hashCode()
    {
        return this.f().hashCode();
    }

    protected synthetic Object k_()
    {
        return this.f();
    }

    protected final void l()
    {
        com.a.b.d.nj.i(this.a().iterator());
        return;
    }

    public java.util.Set n_()
    {
        return this.f().n_();
    }

    protected final String o()
    {
        return this.a().toString();
    }
}
