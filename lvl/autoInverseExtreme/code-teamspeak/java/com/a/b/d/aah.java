package com.a.b.d;
final class aah extends com.a.b.d.gh implements java.util.Set {
    private final transient com.a.b.d.jl a;
    private final transient com.a.b.d.ci b;

    private aah(com.a.b.d.jl p1, com.a.b.d.ci p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    static java.util.Set a(java.util.List p4)
    {
        com.a.b.d.jl v1_1 = new com.a.b.d.jn(p4.size());
        com.a.b.d.aai v2_0 = p4.iterator();
        while (v2_0.hasNext()) {
            com.a.b.d.lo v0_6 = com.a.b.d.lo.a(((java.util.Set) v2_0.next()));
            if (!v0_6.isEmpty()) {
                v1_1.c(v0_6);
            } else {
                com.a.b.d.lo v0_3 = com.a.b.d.lo.h();
            }
            return v0_3;
        }
        com.a.b.d.jl v1_2 = v1_1.b();
        v0_3 = new com.a.b.d.aah(v1_2, new com.a.b.d.ci(new com.a.b.d.aai(v1_2)));
        return v0_3;
    }

    protected final java.util.Collection b()
    {
        return this.b;
    }

    public final boolean equals(Object p3)
    {
        boolean v0_1;
        if (!(p3 instanceof com.a.b.d.aah)) {
            v0_1 = super.equals(p3);
        } else {
            v0_1 = this.a.equals(((com.a.b.d.aah) p3).a);
        }
        return v0_1;
    }

    public final int hashCode()
    {
        int v1_0 = (this.size() - 1);
        int v0_1 = 0;
        while (v0_1 < this.a.size()) {
            v1_0 = (((v1_0 * 31) ^ -1) ^ -1);
            v0_1++;
        }
        java.util.Iterator v3 = this.a.iterator();
        int v2_3 = 1;
        while (v3.hasNext()) {
            int v0_8 = ((java.util.Set) v3.next());
            v2_3 = ((((v0_8.hashCode() * (this.size() / v0_8.size())) + (v2_3 * 31)) ^ -1) ^ -1);
        }
        return (((v2_3 + v1_0) ^ -1) ^ -1);
    }

    protected final bridge synthetic Object k_()
    {
        return this.b;
    }
}
