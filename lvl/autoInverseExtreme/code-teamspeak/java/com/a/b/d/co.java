package com.a.b.d;
 class co extends java.util.AbstractCollection {
    final java.util.Collection a;
    final com.a.b.b.co b;

    co(java.util.Collection p1, com.a.b.b.co p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    private com.a.b.d.co a(com.a.b.b.co p4)
    {
        return new com.a.b.d.co(this.a, com.a.b.b.cp.a(this.b, p4));
    }

    public boolean add(Object p2)
    {
        com.a.b.b.cn.a(this.b.a(p2));
        return this.a.add(p2);
    }

    public boolean addAll(java.util.Collection p4)
    {
        boolean v0_0 = p4.iterator();
        while (v0_0.hasNext()) {
            com.a.b.b.cn.a(this.b.a(v0_0.next()));
        }
        return this.a.addAll(p4);
    }

    public void clear()
    {
        com.a.b.d.mq.a(this.a, this.b);
        return;
    }

    public boolean contains(Object p2)
    {
        int v0_2;
        if (!com.a.b.d.cm.a(this.a, p2)) {
            v0_2 = 0;
        } else {
            v0_2 = this.b.a(p2);
        }
        return v0_2;
    }

    public boolean containsAll(java.util.Collection p2)
    {
        return com.a.b.d.cm.a(this, p2);
    }

    public boolean isEmpty()
    {
        int v0_2;
        if (com.a.b.d.mq.d(this.a, this.b)) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public java.util.Iterator iterator()
    {
        return com.a.b.d.nj.b(this.a.iterator(), this.b);
    }

    public boolean remove(Object p2)
    {
        if ((!this.contains(p2)) || (!this.a.remove(p2))) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public boolean removeAll(java.util.Collection p4)
    {
        return com.a.b.d.mq.a(this.a, com.a.b.b.cp.a(this.b, com.a.b.b.cp.a(p4)));
    }

    public boolean retainAll(java.util.Collection p4)
    {
        return com.a.b.d.mq.a(this.a, com.a.b.b.cp.a(this.b, com.a.b.b.cp.a(com.a.b.b.cp.a(p4))));
    }

    public int size()
    {
        return com.a.b.d.nj.b(this.iterator());
    }

    public Object[] toArray()
    {
        return com.a.b.d.ov.a(this.iterator()).toArray();
    }

    public Object[] toArray(Object[] p2)
    {
        return com.a.b.d.ov.a(this.iterator()).toArray(p2);
    }
}
