package com.a.b.d;
public abstract class jt implements java.io.Serializable, java.util.Map {
    private static final java.util.Map$Entry[] a;
    private transient com.a.b.d.lo b;
    private transient com.a.b.d.lo c;
    private transient com.a.b.d.iz d;
    private transient com.a.b.d.lr e;

    static jt()
    {
        java.util.Map$Entry[] v0_1 = new java.util.Map$Entry[0];
        com.a.b.d.jt.a = v0_1;
        return;
    }

    jt()
    {
        return;
    }

    private static com.a.b.d.jt a(Object p4, Object p5, Object p6, Object p7)
    {
        com.a.b.d.kb[] v1_1 = new com.a.b.d.kb[2];
        v1_1[0] = com.a.b.d.jt.d(p4, p5);
        v1_1[1] = com.a.b.d.jt.d(p6, p7);
        return new com.a.b.d.zf(v1_1);
    }

    private static com.a.b.d.jt a(Object p4, Object p5, Object p6, Object p7, Object p8, Object p9)
    {
        com.a.b.d.kb[] v1_1 = new com.a.b.d.kb[3];
        v1_1[0] = com.a.b.d.jt.d(p4, p5);
        v1_1[1] = com.a.b.d.jt.d(p6, p7);
        v1_1[2] = com.a.b.d.jt.d(p8, p9);
        return new com.a.b.d.zf(v1_1);
    }

    private static com.a.b.d.jt a(Object p4, Object p5, Object p6, Object p7, Object p8, Object p9, Object p10, Object p11)
    {
        com.a.b.d.kb[] v1_1 = new com.a.b.d.kb[4];
        v1_1[0] = com.a.b.d.jt.d(p4, p5);
        v1_1[1] = com.a.b.d.jt.d(p6, p7);
        v1_1[2] = com.a.b.d.jt.d(p8, p9);
        v1_1[3] = com.a.b.d.jt.d(p10, p11);
        return new com.a.b.d.zf(v1_1);
    }

    private static com.a.b.d.jt a(Object p4, Object p5, Object p6, Object p7, Object p8, Object p9, Object p10, Object p11, Object p12, Object p13)
    {
        com.a.b.d.kb[] v1_1 = new com.a.b.d.kb[5];
        v1_1[0] = com.a.b.d.jt.d(p4, p5);
        v1_1[1] = com.a.b.d.jt.d(p6, p7);
        v1_1[2] = com.a.b.d.jt.d(p8, p9);
        v1_1[3] = com.a.b.d.jt.d(p10, p11);
        v1_1[4] = com.a.b.d.jt.d(p12, p13);
        return new com.a.b.d.zf(v1_1);
    }

    public static com.a.b.d.jt a(java.util.Map p4)
    {
        if ((!(p4 instanceof com.a.b.d.jt)) || ((p4 instanceof com.a.b.d.lw))) {
            if (!(p4 instanceof java.util.EnumMap)) {
                com.a.b.d.jt v0_12 = ((java.util.Map$Entry[]) p4.entrySet().toArray(com.a.b.d.jt.a));
                switch (v0_12.length) {
                    case 0:
                        com.a.b.d.jt v0_5 = com.a.b.d.it.i();
                        break;
                    case 1:
                        com.a.b.d.jt v0_13 = v0_12[0];
                        v0_5 = com.a.b.d.it.b(v0_13.getKey(), v0_13.getValue());
                        break;
                    default:
                        v0_5 = new com.a.b.d.zf(v0_12);
                }
            } else {
                Object v1_1 = new java.util.EnumMap(((java.util.EnumMap) p4));
                java.util.Iterator v2 = v1_1.entrySet().iterator();
                while (v2.hasNext()) {
                    com.a.b.d.jt v0_7 = ((java.util.Map$Entry) v2.next());
                    com.a.b.d.cl.a(v0_7.getKey(), v0_7.getValue());
                }
                v0_5 = com.a.b.d.jd.a(v1_1);
            }
        } else {
            v0_5 = ((com.a.b.d.jt) p4);
            if (((com.a.b.d.jt) p4).i_()) {
            }
        }
        return v0_5;
    }

    private com.a.b.d.lr a()
    {
        com.a.b.d.jv v0_1 = new com.a.b.d.jv(this);
        return new com.a.b.d.lr(v0_1, v0_1.size(), 0);
    }

    static void a(boolean p7, String p8, java.util.Map$Entry p9, java.util.Map$Entry p10)
    {
        if (p7) {
            return;
        } else {
            String v1_1 = String.valueOf(String.valueOf(p8));
            String v2_1 = String.valueOf(String.valueOf(p9));
            String v3_1 = String.valueOf(String.valueOf(p10));
            throw new IllegalArgumentException(new StringBuilder((((v1_1.length() + 34) + v2_1.length()) + v3_1.length())).append("Multiple entries with same ").append(v1_1).append(": ").append(v2_1).append(" and ").append(v3_1).toString());
        }
    }

    private static com.a.b.d.jt b(java.util.Map p4)
    {
        java.util.EnumMap v1_1 = new java.util.EnumMap(((java.util.EnumMap) p4));
        java.util.Iterator v2 = v1_1.entrySet().iterator();
        while (v2.hasNext()) {
            com.a.b.d.jt v0_4 = ((java.util.Map$Entry) v2.next());
            com.a.b.d.cl.a(v0_4.getKey(), v0_4.getValue());
        }
        return com.a.b.d.jd.a(v1_1);
    }

    public static com.a.b.d.jt c(Object p1, Object p2)
    {
        return com.a.b.d.it.b(p1, p2);
    }

    private static com.a.b.d.jt c(java.util.Map p4)
    {
        java.util.EnumMap v1_1 = new java.util.EnumMap(p4);
        java.util.Iterator v2 = v1_1.entrySet().iterator();
        while (v2.hasNext()) {
            com.a.b.d.jt v0_4 = ((java.util.Map$Entry) v2.next());
            com.a.b.d.cl.a(v0_4.getKey(), v0_4.getValue());
        }
        return com.a.b.d.jd.a(v1_1);
    }

    static com.a.b.d.kb d(Object p1, Object p2)
    {
        com.a.b.d.cl.a(p1, p2);
        return new com.a.b.d.kb(p1, p2);
    }

    private com.a.b.d.jt i()
    {
        return new com.a.b.d.jv(this);
    }

    public static com.a.b.d.jt k()
    {
        return com.a.b.d.it.i();
    }

    public static com.a.b.d.ju l()
    {
        return new com.a.b.d.ju();
    }

    com.a.b.d.lo c()
    {
        return new com.a.b.d.ke(this);
    }

    public final void clear()
    {
        throw new UnsupportedOperationException();
    }

    public boolean containsKey(Object p2)
    {
        int v0_1;
        if (this.get(p2) == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public boolean containsValue(Object p2)
    {
        return this.h().contains(p2);
    }

    abstract com.a.b.d.lo d();

    public com.a.b.d.lo e()
    {
        com.a.b.d.lo v0 = this.b;
        if (v0 == null) {
            v0 = this.d();
            this.b = v0;
        }
        return v0;
    }

    public synthetic java.util.Set entrySet()
    {
        return this.e();
    }

    public boolean equals(Object p2)
    {
        return com.a.b.d.sz.f(this, p2);
    }

    public com.a.b.d.lr f()
    {
        com.a.b.d.lr v0_0 = this.e;
        if (v0_0 == null) {
            com.a.b.d.jv v1_1 = new com.a.b.d.jv(this);
            v0_0 = new com.a.b.d.lr(v1_1, v1_1.size(), 0);
            this.e = v0_0;
        }
        return v0_0;
    }

    public com.a.b.d.lo g()
    {
        com.a.b.d.lo v0 = this.c;
        if (v0 == null) {
            v0 = this.c();
            this.c = v0;
        }
        return v0;
    }

    public abstract Object get();

    public com.a.b.d.iz h()
    {
        com.a.b.d.kh v0_0 = this.d;
        if (v0_0 == null) {
            v0_0 = new com.a.b.d.kh(this);
            this.d = v0_0;
        }
        return v0_0;
    }

    public int hashCode()
    {
        return this.e().hashCode();
    }

    abstract boolean i_();

    public boolean isEmpty()
    {
        int v0_1;
        if (this.size() != 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    Object j()
    {
        return new com.a.b.d.jz(this);
    }

    public synthetic java.util.Set keySet()
    {
        return this.g();
    }

    public final Object put(Object p2, Object p3)
    {
        throw new UnsupportedOperationException();
    }

    public final void putAll(java.util.Map p2)
    {
        throw new UnsupportedOperationException();
    }

    public final Object remove(Object p2)
    {
        throw new UnsupportedOperationException();
    }

    public String toString()
    {
        return com.a.b.d.sz.a(this);
    }

    public synthetic java.util.Collection values()
    {
        return this.h();
    }
}
