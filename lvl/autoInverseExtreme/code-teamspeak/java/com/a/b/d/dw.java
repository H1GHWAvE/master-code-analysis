package com.a.b.d;
abstract class dw implements java.io.Serializable, java.lang.Comparable {
    private static final long b;
    final Comparable a;

    dw(Comparable p1)
    {
        this.a = p1;
        return;
    }

    static com.a.b.d.dw b(Comparable p1)
    {
        return new com.a.b.d.eb(p1);
    }

    static com.a.b.d.dw c(Comparable p1)
    {
        return new com.a.b.d.dz(p1);
    }

    static com.a.b.d.dw d()
    {
        return com.a.b.d.ea.f();
    }

    static com.a.b.d.dw e()
    {
        return com.a.b.d.dy.f();
    }

    public int a(com.a.b.d.dw p3)
    {
        int v0_3;
        if (p3 != com.a.b.d.ea.f()) {
            if (p3 != com.a.b.d.dy.f()) {
                v0_3 = com.a.b.d.yl.b(this.a, p3.a);
                if (v0_3 == 0) {
                    v0_3 = com.a.b.l.a.a((this instanceof com.a.b.d.dz), (p3 instanceof com.a.b.d.dz));
                }
            } else {
                v0_3 = -1;
            }
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    abstract com.a.b.d.ce a();

    abstract com.a.b.d.dw a();

    abstract Comparable a();

    abstract void a();

    abstract boolean a();

    abstract com.a.b.d.ce b();

    abstract com.a.b.d.dw b();

    abstract Comparable b();

    abstract void b();

    com.a.b.d.dw c(com.a.b.d.ep p1)
    {
        return this;
    }

    Comparable c()
    {
        return this.a;
    }

    public synthetic int compareTo(Object p2)
    {
        return this.a(((com.a.b.d.dw) p2));
    }

    public boolean equals(Object p3)
    {
        int v0 = 0;
        try {
            if (((p3 instanceof com.a.b.d.dw)) && (this.a(((com.a.b.d.dw) p3)) == 0)) {
                v0 = 1;
            }
        } catch (ClassCastException v1) {
        }
        return v0;
    }
}
