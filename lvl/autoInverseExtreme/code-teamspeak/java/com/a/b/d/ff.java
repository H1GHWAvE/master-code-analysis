package com.a.b.d;
public final class ff extends com.a.b.d.ai {
    private static final long c;
    private transient Class b;

    private ff(Class p2)
    {
        this(com.a.b.d.agm.a(new java.util.EnumMap(p2)));
        this.b = p2;
        return;
    }

    private static com.a.b.d.ff a(Class p1)
    {
        return new com.a.b.d.ff(p1);
    }

    private static com.a.b.d.ff a(Iterable p3)
    {
        Class v0_0 = p3.iterator();
        com.a.b.b.cn.a(v0_0.hasNext(), "EnumMultiset constructor passed empty Iterable");
        com.a.b.d.ff v1_2 = new com.a.b.d.ff(((Enum) v0_0.next()).getDeclaringClass());
        com.a.b.d.mq.a(v1_2, p3);
        return v1_2;
    }

    private static com.a.b.d.ff a(Iterable p1, Class p2)
    {
        com.a.b.d.ff v0_1 = new com.a.b.d.ff(p2);
        com.a.b.d.mq.a(v0_1, p1);
        return v0_1;
    }

    private void a(java.io.ObjectInputStream p3)
    {
        p3.defaultReadObject();
        this.b = ((Class) p3.readObject());
        this.a = com.a.b.d.agm.a(new java.util.EnumMap(this.b));
        com.a.b.d.zz.a(this, p3);
        return;
    }

    private void a(java.io.ObjectOutputStream p2)
    {
        p2.defaultWriteObject();
        p2.writeObject(this.b);
        com.a.b.d.zz.a(this, p2);
        return;
    }

    public final bridge synthetic int a(Object p2)
    {
        return super.a(p2);
    }

    public final bridge synthetic java.util.Set a()
    {
        return super.a();
    }

    public final bridge synthetic boolean addAll(java.util.Collection p2)
    {
        return super.addAll(p2);
    }

    public final bridge synthetic int b(Object p2, int p3)
    {
        return super.b(p2, p3);
    }

    public final bridge synthetic void clear()
    {
        super.clear();
        return;
    }

    public final bridge synthetic boolean contains(Object p2)
    {
        return super.contains(p2);
    }

    public final bridge synthetic boolean equals(Object p2)
    {
        return super.equals(p2);
    }

    public final bridge synthetic int hashCode()
    {
        return super.hashCode();
    }

    public final bridge synthetic boolean isEmpty()
    {
        return super.isEmpty();
    }

    public final bridge synthetic java.util.Iterator iterator()
    {
        return super.iterator();
    }

    public final bridge synthetic java.util.Set n_()
    {
        return super.n_();
    }

    public final bridge synthetic boolean remove(Object p2)
    {
        return super.remove(p2);
    }

    public final bridge synthetic boolean removeAll(java.util.Collection p2)
    {
        return super.removeAll(p2);
    }

    public final bridge synthetic boolean retainAll(java.util.Collection p2)
    {
        return super.retainAll(p2);
    }

    public final bridge synthetic int size()
    {
        return super.size();
    }

    public final bridge synthetic String toString()
    {
        return super.toString();
    }
}
