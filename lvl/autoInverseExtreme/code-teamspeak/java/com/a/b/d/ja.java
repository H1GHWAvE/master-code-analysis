package com.a.b.d;
abstract class ja extends com.a.b.d.jb {
    Object[] a;
    int b;

    ja(int p2)
    {
        com.a.b.d.cl.a(p2, "initialCapacity");
        int v0_1 = new Object[p2];
        this.a = v0_1;
        this.b = 0;
        return;
    }

    private void a(int p3)
    {
        if (this.a.length < p3) {
            this.a = com.a.b.d.yc.b(this.a, com.a.b.d.ja.a(this.a.length, p3));
        }
        return;
    }

    public com.a.b.d.ja a(Object p4)
    {
        com.a.b.b.cn.a(p4);
        this.a((this.b + 1));
        Object[] v0_2 = this.a;
        int v1 = this.b;
        this.b = (v1 + 1);
        v0_2[v1] = p4;
        return this;
    }

    public com.a.b.d.jb a(Iterable p3)
    {
        if ((p3 instanceof java.util.Collection)) {
            this.a((((java.util.Collection) p3).size() + this.b));
        }
        super.a(p3);
        return this;
    }

    public varargs com.a.b.d.jb a(Object[] p5)
    {
        com.a.b.d.yc.a(p5);
        this.a((this.b + p5.length));
        System.arraycopy(p5, 0, this.a, this.b, p5.length);
        this.b = (this.b + p5.length);
        return this;
    }

    public synthetic com.a.b.d.jb b(Object p2)
    {
        return this.a(p2);
    }
}
