package com.a.b.d;
 class yw extends com.a.b.d.ir {
    private final com.a.b.d.iz a;
    private final com.a.b.d.jl b;

    yw(com.a.b.d.iz p1, com.a.b.d.jl p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    yw(com.a.b.d.iz p2, Object[] p3)
    {
        this(p2, com.a.b.d.jl.b(p3, p3.length));
        return;
    }

    private com.a.b.d.jl i()
    {
        return this.b;
    }

    final int a(Object[] p2, int p3)
    {
        return this.b.a(p2, p3);
    }

    public final com.a.b.d.agj a(int p2)
    {
        return this.b.a(p2);
    }

    com.a.b.d.iz b()
    {
        return this.a;
    }

    public Object get(int p2)
    {
        return this.b.get(p2);
    }

    public synthetic java.util.ListIterator listIterator(int p2)
    {
        return this.a(p2);
    }
}
