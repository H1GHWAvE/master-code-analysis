package com.a.b.d;
 class vb extends java.util.AbstractCollection {
    final java.util.Map c;

    vb(java.util.Map p2)
    {
        this.c = ((java.util.Map) com.a.b.b.cn.a(p2));
        return;
    }

    private java.util.Map a()
    {
        return this.c;
    }

    public void clear()
    {
        this.c.clear();
        return;
    }

    public boolean contains(Object p2)
    {
        return this.c.containsValue(p2);
    }

    public boolean isEmpty()
    {
        return this.c.isEmpty();
    }

    public java.util.Iterator iterator()
    {
        return com.a.b.d.sz.b(this.c.entrySet().iterator());
    }

    public boolean remove(Object p4)
    {
        try {
            int v0_0 = super.remove(p4);
        } catch (int v0) {
            java.util.Map v1_0 = this.c.entrySet().iterator();
        }
        return v0_0;
    }

    public boolean removeAll(java.util.Collection p5)
    {
        try {
            Object v0_2 = super.removeAll(((java.util.Collection) com.a.b.b.cn.a(p5)));
        } catch (Object v0) {
            java.util.HashSet v1_1 = new java.util.HashSet();
            java.util.Iterator v2 = this.c.entrySet().iterator();
        }
        return v0_2;
    }

    public boolean retainAll(java.util.Collection p5)
    {
        try {
            Object v0_2 = super.retainAll(((java.util.Collection) com.a.b.b.cn.a(p5)));
        } catch (Object v0) {
            java.util.HashSet v1_1 = new java.util.HashSet();
            java.util.Iterator v2 = this.c.entrySet().iterator();
        }
        return v0_2;
    }

    public int size()
    {
        return this.c.size();
    }
}
