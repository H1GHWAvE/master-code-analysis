package com.a.b.d;
final class dt extends com.a.b.d.hp {
    final java.util.SortedSet a;
    final com.a.b.d.dm b;

    dt(java.util.SortedSet p2, com.a.b.d.dm p3)
    {
        this.a = ((java.util.SortedSet) com.a.b.b.cn.a(p2));
        this.b = ((com.a.b.d.dm) com.a.b.b.cn.a(p3));
        return;
    }

    protected final bridge synthetic java.util.Set a()
    {
        return this.a;
    }

    public final boolean add(Object p2)
    {
        this.b.a(p2);
        return this.a.add(p2);
    }

    public final boolean addAll(java.util.Collection p3)
    {
        return this.a.addAll(com.a.b.d.dn.b(p3, this.b));
    }

    protected final bridge synthetic java.util.Collection b()
    {
        return this.a;
    }

    protected final java.util.SortedSet c()
    {
        return this.a;
    }

    public final java.util.SortedSet headSet(Object p3)
    {
        return com.a.b.d.dn.a(this.a.headSet(p3), this.b);
    }

    protected final bridge synthetic Object k_()
    {
        return this.a;
    }

    public final java.util.SortedSet subSet(Object p3, Object p4)
    {
        return com.a.b.d.dn.a(this.a.subSet(p3, p4), this.b);
    }

    public final java.util.SortedSet tailSet(Object p3)
    {
        return com.a.b.d.dn.a(this.a.tailSet(p3), this.b);
    }
}
