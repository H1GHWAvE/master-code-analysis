package com.a.b.d;
public final class mq {

    private mq()
    {
        return;
    }

    private static com.a.b.b.bj a()
    {
        return new com.a.b.d.my();
    }

    private static Iterable a(com.a.b.d.iz p1)
    {
        return ((Iterable) com.a.b.b.cn.a(p1));
    }

    private static Iterable a(Iterable p1, int p2)
    {
        com.a.b.d.nb v0_0;
        com.a.b.b.cn.a(p1);
        if (p2 <= 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        com.a.b.b.cn.a(v0_0);
        return new com.a.b.d.nb(p1, p2);
    }

    public static Iterable a(Iterable p1, com.a.b.b.bj p2)
    {
        com.a.b.b.cn.a(p1);
        com.a.b.b.cn.a(p2);
        return new com.a.b.d.nf(p1, p2);
    }

    public static Iterable a(Iterable p1, Iterable p2)
    {
        return com.a.b.d.mq.e(com.a.b.d.jl.a(p1, p2));
    }

    private static Iterable a(Iterable p1, Iterable p2, Iterable p3)
    {
        return com.a.b.d.mq.e(com.a.b.d.jl.a(p1, p2, p3));
    }

    private static Iterable a(Iterable p1, Iterable p2, Iterable p3, Iterable p4)
    {
        return com.a.b.d.mq.e(com.a.b.d.jl.a(p1, p2, p3, p4));
    }

    private static Iterable a(Iterable p3, java.util.Comparator p4)
    {
        com.a.b.b.cn.a(p3, "iterables");
        com.a.b.b.cn.a(p4, "comparator");
        return new com.a.b.d.ni(new com.a.b.d.mx(p3, p4), 0);
    }

    private static varargs Iterable a(Iterable[] p1)
    {
        return com.a.b.d.mq.e(com.a.b.d.jl.a(p1));
    }

    private static varargs Iterable a(Object[] p1)
    {
        return com.a.b.d.mq.d(com.a.b.d.ov.a(p1));
    }

    private static Object a(Iterable p1, int p2, Object p3)
    {
        com.a.b.b.cn.a(p1);
        com.a.b.d.nj.a(p2);
        if (!(p1 instanceof java.util.List)) {
            java.util.Iterator v0_1 = p1.iterator();
            com.a.b.d.nj.d(v0_1, p2);
            p3 = com.a.b.d.nj.d(v0_1, p3);
        } else {
            if (p2 < ((java.util.List) p1).size()) {
                p3 = ((java.util.List) p1).get(p2);
            }
        }
        return p3;
    }

    private static Object a(Iterable p1, com.a.b.b.co p2, Object p3)
    {
        return com.a.b.d.nj.a(p1.iterator(), p2, p3);
    }

    static Object a(java.util.List p1)
    {
        return p1.get((p1.size() - 1));
    }

    public static String a(Iterable p1)
    {
        return com.a.b.d.nj.c(p1.iterator());
    }

    private static void a(java.util.List p2, com.a.b.b.co p3, int p4, int p5)
    {
        int v0_1 = (p2.size() - 1);
        while (v0_1 > p5) {
            if (p3.a(p2.get(v0_1))) {
                p2.remove(v0_1);
            }
            v0_1--;
        }
        int v0_2 = (p5 - 1);
        while (v0_2 >= p4) {
            p2.remove(v0_2);
            v0_2--;
        }
        return;
    }

    public static boolean a(Iterable p1, com.a.b.b.co p2)
    {
        if ((!(p1 instanceof java.util.RandomAccess)) || (!(p1 instanceof java.util.List))) {
            boolean v0_3 = com.a.b.d.nj.a(p1.iterator(), p2);
        } else {
            v0_3 = com.a.b.d.mq.a(((java.util.List) p1), ((com.a.b.b.co) com.a.b.b.cn.a(p2)));
        }
        return v0_3;
    }

    private static boolean a(Iterable p1, Object p2)
    {
        boolean v0_2;
        if (!(p1 instanceof java.util.Collection)) {
            v0_2 = com.a.b.d.nj.a(p1.iterator(), p2);
        } else {
            v0_2 = com.a.b.d.cm.a(((java.util.Collection) p1), p2);
        }
        return v0_2;
    }

    private static boolean a(Iterable p1, java.util.Collection p2)
    {
        boolean v0_2;
        if (!(p1 instanceof java.util.Collection)) {
            v0_2 = com.a.b.d.nj.a(p1.iterator(), p2);
        } else {
            v0_2 = ((java.util.Collection) p1).removeAll(((java.util.Collection) com.a.b.b.cn.a(p2)));
        }
        return v0_2;
    }

    public static boolean a(java.util.Collection p1, Iterable p2)
    {
        boolean v0_4;
        if (!(p2 instanceof java.util.Collection)) {
            v0_4 = com.a.b.d.nj.a(p1, ((Iterable) com.a.b.b.cn.a(p2)).iterator());
        } else {
            v0_4 = p1.addAll(com.a.b.d.cm.a(p2));
        }
        return v0_4;
    }

    private static boolean a(java.util.List p6, com.a.b.b.co p7)
    {
        int v1_0 = 0;
        int v0 = 0;
        int v2 = 0;
        while (v2 < p6.size()) {
            boolean v4_3 = p6.get(v2);
            if (!p7.a(v4_3)) {
                if (v2 > v0) {
                    try {
                        p6.set(v0, v4_3);
                    } catch (int v1) {
                        int v1_2 = (p6.size() - 1);
                    }
                }
                v0++;
            }
            v2++;
        }
        p6.subList(v0, p6.size()).clear();
        if (v2 != v0) {
            v1_0 = 1;
        }
        return v1_0;
    }

    public static Object[] a(Iterable p2, Class p3)
    {
        Object[] v0_0 = com.a.b.d.mq.i(p2);
        return v0_0.toArray(com.a.b.d.yc.a(p3, v0_0.size()));
    }

    private static synthetic com.a.b.b.bj b()
    {
        return new com.a.b.d.my();
    }

    private static Iterable b(Iterable p1, int p2)
    {
        com.a.b.d.nc v0_0;
        com.a.b.b.cn.a(p1);
        if (p2 <= 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        com.a.b.b.cn.a(v0_0);
        return new com.a.b.d.nc(p1, p2);
    }

    private static Iterable b(Iterable p1, Class p2)
    {
        com.a.b.b.cn.a(p1);
        com.a.b.b.cn.a(p2);
        return new com.a.b.d.ne(p1, p2);
    }

    public static Object b(Iterable p1)
    {
        return com.a.b.d.nj.d(p1.iterator());
    }

    static Object b(Iterable p3, com.a.b.b.co p4)
    {
        com.a.b.b.cn.a(p4);
        java.util.Iterator v1 = p3.iterator();
        while (v1.hasNext()) {
            int v0_1 = v1.next();
            if (p4.a(v0_1)) {
                v1.remove();
            }
            return v0_1;
        }
        v0_1 = 0;
        return v0_1;
    }

    private static Object b(Iterable p1, Object p2)
    {
        return com.a.b.d.nj.b(p1.iterator(), p2);
    }

    private static boolean b(Iterable p2, Iterable p3)
    {
        if ((!(p2 instanceof java.util.Collection)) || ((!(p3 instanceof java.util.Collection)) || (((java.util.Collection) p2).size() == ((java.util.Collection) p3).size()))) {
            int v0_6 = com.a.b.d.nj.a(p2.iterator(), p3.iterator());
        } else {
            v0_6 = 0;
        }
        return v0_6;
    }

    private static boolean b(Iterable p1, java.util.Collection p2)
    {
        boolean v0_2;
        if (!(p1 instanceof java.util.Collection)) {
            v0_2 = com.a.b.d.nj.b(p1.iterator(), p2);
        } else {
            v0_2 = ((java.util.Collection) p1).retainAll(((java.util.Collection) com.a.b.b.cn.a(p2)));
        }
        return v0_2;
    }

    private static int c(Iterable p1, Object p2)
    {
        int v0_3;
        if (!(p1 instanceof com.a.b.d.xc)) {
            if (!(p1 instanceof java.util.Set)) {
                v0_3 = com.a.b.d.nj.c(p1.iterator(), p2);
            } else {
                if (!((java.util.Set) p1).contains(p2)) {
                    v0_3 = 0;
                } else {
                    v0_3 = 1;
                }
            }
        } else {
            v0_3 = ((com.a.b.d.xc) p1).a(p2);
        }
        return v0_3;
    }

    public static Iterable c(Iterable p1, com.a.b.b.co p2)
    {
        com.a.b.b.cn.a(p1);
        com.a.b.b.cn.a(p2);
        return new com.a.b.d.nd(p1, p2);
    }

    private static Object c(Iterable p1, int p2)
    {
        Object v0_2;
        com.a.b.b.cn.a(p1);
        if (!(p1 instanceof java.util.List)) {
            v0_2 = com.a.b.d.nj.c(p1.iterator(), p2);
        } else {
            v0_2 = ((java.util.List) p1).get(p2);
        }
        return v0_2;
    }

    static Object[] c(Iterable p1)
    {
        return com.a.b.d.mq.i(p1).toArray();
    }

    public static Iterable d(Iterable p1)
    {
        com.a.b.b.cn.a(p1);
        return new com.a.b.d.mr(p1);
    }

    private static Iterable d(Iterable p2, int p3)
    {
        com.a.b.d.ms v0_0;
        com.a.b.b.cn.a(p2);
        if (p3 < 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        com.a.b.d.ms v0_3;
        com.a.b.b.cn.a(v0_0, "number to skip cannot be negative");
        if (!(p2 instanceof java.util.List)) {
            v0_3 = new com.a.b.d.ms(p2, p3);
        } else {
            v0_3 = new com.a.b.d.ng(((java.util.List) p2), p3);
        }
        return v0_3;
    }

    private static Object d(Iterable p1, Object p2)
    {
        if (!(p1 instanceof java.util.Collection)) {
            p2 = com.a.b.d.nj.e(p1.iterator(), p2);
        } else {
            if (!com.a.b.d.cm.a(p1).isEmpty()) {
                if (!(p1 instanceof java.util.List)) {
                } else {
                    p2 = com.a.b.d.mq.a(((java.util.List) p1));
                }
            }
        }
        return p2;
    }

    public static boolean d(Iterable p1, com.a.b.b.co p2)
    {
        return com.a.b.d.nj.c(p1.iterator(), p2);
    }

    public static Iterable e(Iterable p1)
    {
        com.a.b.b.cn.a(p1);
        return new com.a.b.d.mz(p1);
    }

    private static Iterable e(Iterable p2, int p3)
    {
        com.a.b.d.mu v0_0;
        com.a.b.b.cn.a(p2);
        if (p3 < 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        com.a.b.b.cn.a(v0_0, "limit is negative");
        return new com.a.b.d.mu(p2, p3);
    }

    public static boolean e(Iterable p1, com.a.b.b.co p2)
    {
        return com.a.b.d.nj.d(p1.iterator(), p2);
    }

    public static Object f(Iterable p2)
    {
        return com.a.b.d.nj.d(p2.iterator(), 0);
    }

    private static Object f(Iterable p1, com.a.b.b.co p2)
    {
        return com.a.b.d.nj.e(p1.iterator(), p2);
    }

    private static com.a.b.b.ci g(Iterable p1, com.a.b.b.co p2)
    {
        return com.a.b.d.nj.f(p1.iterator(), p2);
    }

    private static Iterable g(Iterable p2)
    {
        com.a.b.b.cn.a(p2);
        if ((!(p2 instanceof com.a.b.d.ni)) && (!(p2 instanceof com.a.b.d.iz))) {
            p2 = new com.a.b.d.ni(p2, 0);
        }
        return p2;
    }

    private static int h(Iterable p1)
    {
        int v0_2;
        if (!(p1 instanceof java.util.Collection)) {
            v0_2 = com.a.b.d.nj.b(p1.iterator());
        } else {
            v0_2 = ((java.util.Collection) p1).size();
        }
        return v0_2;
    }

    private static int h(Iterable p1, com.a.b.b.co p2)
    {
        return com.a.b.d.nj.g(p1.iterator(), p2);
    }

    private static java.util.Collection i(Iterable p1)
    {
        java.util.ArrayList v1_1;
        if (!(p1 instanceof java.util.Collection)) {
            v1_1 = com.a.b.d.ov.a(p1.iterator());
        } else {
            v1_1 = ((java.util.Collection) p1);
        }
        return v1_1;
    }

    private static java.util.Iterator j(Iterable p2)
    {
        return new com.a.b.d.na(p2.iterator());
    }

    private static Object k(Iterable p1)
    {
        Object v0_2;
        if (!(p1 instanceof java.util.List)) {
            v0_2 = com.a.b.d.nj.f(p1.iterator());
        } else {
            if (!((java.util.List) p1).isEmpty()) {
                v0_2 = com.a.b.d.mq.a(((java.util.List) p1));
            } else {
                throw new java.util.NoSuchElementException();
            }
        }
        return v0_2;
    }

    private static Iterable l(Iterable p1)
    {
        com.a.b.d.mw v0_2;
        if (!(p1 instanceof java.util.Queue)) {
            com.a.b.b.cn.a(p1);
            v0_2 = new com.a.b.d.mw(p1);
        } else {
            v0_2 = new com.a.b.d.mv(p1);
        }
        return v0_2;
    }

    private static boolean m(Iterable p1)
    {
        int v0_3;
        if (!(p1 instanceof java.util.Collection)) {
            if (p1.iterator().hasNext()) {
                v0_3 = 0;
            } else {
                v0_3 = 1;
            }
        } else {
            v0_3 = ((java.util.Collection) p1).isEmpty();
        }
        return v0_3;
    }

    private static synthetic java.util.Iterator n(Iterable p2)
    {
        return new com.a.b.d.na(p2.iterator());
    }
}
