package com.a.b.d;
public final class ve {
    private static final int d = 255;
    final java.util.Comparator a;
    int b;
    int c;

    private ve(java.util.Comparator p2)
    {
        this.b = -1;
        this.c = 2147483647;
        this.a = ((java.util.Comparator) com.a.b.b.cn.a(p2));
        return;
    }

    synthetic ve(java.util.Comparator p1, byte p2)
    {
        this(p1);
        return;
    }

    private com.a.b.d.vc a()
    {
        return this.a(java.util.Collections.emptySet());
    }

    private com.a.b.d.ve a(int p2)
    {
        int v0;
        if (p2 < 0) {
            v0 = 0;
        } else {
            v0 = 1;
        }
        com.a.b.b.cn.a(v0);
        this.b = p2;
        return this;
    }

    private static synthetic com.a.b.d.yd a(com.a.b.d.ve p1)
    {
        return com.a.b.d.yd.a(p1.a);
    }

    private static synthetic int b(com.a.b.d.ve p1)
    {
        return p1.c;
    }

    private com.a.b.d.ve b(int p2)
    {
        int v0;
        if (p2 <= 0) {
            v0 = 0;
        } else {
            v0 = 1;
        }
        com.a.b.b.cn.a(v0);
        this.c = p2;
        return this;
    }

    private com.a.b.d.yd b()
    {
        return com.a.b.d.yd.a(this.a);
    }

    public final com.a.b.d.vc a(Iterable p4)
    {
        com.a.b.d.vc v0_1 = new com.a.b.d.vc(this, com.a.b.d.vc.a(this.b, this.c, p4), 0);
        java.util.Iterator v1_2 = p4.iterator();
        while (v1_2.hasNext()) {
            v0_1.offer(v1_2.next());
        }
        return v0_1;
    }
}
