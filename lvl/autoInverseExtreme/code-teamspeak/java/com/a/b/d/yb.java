package com.a.b.d;
final class yb extends com.a.b.d.yd implements java.io.Serializable {
    private static final long b;
    final com.a.b.d.yd a;

    yb(com.a.b.d.yd p1)
    {
        this.a = p1;
        return;
    }

    public final com.a.b.d.yd a()
    {
        return this.a.a().b();
    }

    public final com.a.b.d.yd b()
    {
        return this.a.b();
    }

    public final com.a.b.d.yd c()
    {
        return this;
    }

    public final int compare(Object p2, Object p3)
    {
        int v0_1;
        if (p2 != p3) {
            if (p2 != null) {
                if (p3 != null) {
                    v0_1 = this.a.compare(p2, p3);
                } else {
                    v0_1 = -1;
                }
            } else {
                v0_1 = 1;
            }
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    public final boolean equals(Object p3)
    {
        int v0_1;
        if (p3 != this) {
            if (!(p3 instanceof com.a.b.d.yb)) {
                v0_1 = 0;
            } else {
                v0_1 = this.a.equals(((com.a.b.d.yb) p3).a);
            }
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final int hashCode()
    {
        return (this.a.hashCode() ^ -921210296);
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        return new StringBuilder((v0_2.length() + 12)).append(v0_2).append(".nullsLast()").toString();
    }
}
