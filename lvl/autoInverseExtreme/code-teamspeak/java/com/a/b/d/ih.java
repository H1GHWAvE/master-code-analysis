package com.a.b.d;
final class ih extends com.a.b.d.am {
    com.a.b.d.ia a;
    final synthetic com.a.b.d.ig b;

    ih(com.a.b.d.ig p1, com.a.b.d.ia p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final Object getKey()
    {
        return this.a.f;
    }

    public final Object getValue()
    {
        return this.a.e;
    }

    public final Object setValue(Object p7)
    {
        Object v3 = this.a.e;
        int v4 = com.a.b.d.hy.b(p7);
        if ((v4 != this.a.a) || (!com.a.b.b.ce.a(p7, v3))) {
            com.a.b.d.ig v0_9;
            if (com.a.b.d.hy.a(this.b.a.a.a, p7, v4) != null) {
                v0_9 = 0;
            } else {
                v0_9 = 1;
            }
            int v1_1 = new Object[1];
            v1_1[0] = p7;
            com.a.b.b.cn.a(v0_9, "value already present: %s", v1_1);
            com.a.b.d.hy.a(this.b.a.a.a, this.a);
            com.a.b.d.hy.b(this.b.a.a.a, new com.a.b.d.ia(p7, v4, this.a.f, this.a.b));
            this.b.e = com.a.b.d.hy.a(this.b.a.a.a);
            p7 = v3;
        }
        return p7;
    }
}
