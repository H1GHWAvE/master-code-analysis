package com.a.b.d;
abstract class adz implements com.a.b.d.adw {

    adz()
    {
        return;
    }

    public boolean equals(Object p5)
    {
        int v0 = 1;
        if (p5 != this) {
            if (!(p5 instanceof com.a.b.d.adw)) {
                v0 = 0;
            } else {
                if ((!com.a.b.b.ce.a(this.a(), ((com.a.b.d.adw) p5).a())) || ((!com.a.b.b.ce.a(this.b(), ((com.a.b.d.adw) p5).b())) || (!com.a.b.b.ce.a(this.c(), ((com.a.b.d.adw) p5).c())))) {
                    v0 = 0;
                }
            }
        }
        return v0;
    }

    public int hashCode()
    {
        int v0_1 = new Object[3];
        v0_1[0] = this.a();
        v0_1[1] = this.b();
        v0_1[2] = this.c();
        return java.util.Arrays.hashCode(v0_1);
    }

    public String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a()));
        String v1_2 = String.valueOf(String.valueOf(this.b()));
        String v2_2 = String.valueOf(String.valueOf(this.c()));
        return new StringBuilder((((v0_2.length() + 4) + v1_2.length()) + v2_2.length())).append("(").append(v0_2).append(",").append(v1_2).append(")=").append(v2_2).toString();
    }
}
