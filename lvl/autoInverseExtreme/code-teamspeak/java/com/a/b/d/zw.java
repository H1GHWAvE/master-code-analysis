package com.a.b.d;
final class zw extends com.a.b.d.yd implements java.io.Serializable {
    static final com.a.b.d.zw a;
    private static final long b;

    static zw()
    {
        com.a.b.d.zw.a = new com.a.b.d.zw();
        return;
    }

    private zw()
    {
        return;
    }

    private static int a(Comparable p1, Comparable p2)
    {
        int v0;
        com.a.b.b.cn.a(p1);
        if (p1 != p2) {
            v0 = p2.compareTo(p1);
        } else {
            v0 = 0;
        }
        return v0;
    }

    private static varargs Comparable a(Comparable p1, Comparable p2, Comparable p3, Comparable[] p4)
    {
        return ((Comparable) com.a.b.d.xz.a.b(p1, p2, p3, p4));
    }

    private static Comparable b(Comparable p1, Comparable p2)
    {
        return ((Comparable) com.a.b.d.xz.a.b(p1, p2));
    }

    private static varargs Comparable b(Comparable p1, Comparable p2, Comparable p3, Comparable[] p4)
    {
        return ((Comparable) com.a.b.d.xz.a.a(p1, p2, p3, p4));
    }

    private static Comparable c(Comparable p1, Comparable p2)
    {
        return ((Comparable) com.a.b.d.xz.a.a(p1, p2));
    }

    private static Comparable c(java.util.Iterator p1)
    {
        return ((Comparable) com.a.b.d.xz.a.b(p1));
    }

    private static Comparable d(java.util.Iterator p1)
    {
        return ((Comparable) com.a.b.d.xz.a.a(p1));
    }

    private static Comparable e(Iterable p1)
    {
        return ((Comparable) com.a.b.d.xz.a.d(p1));
    }

    private static Comparable f(Iterable p1)
    {
        return ((Comparable) com.a.b.d.xz.a.c(p1));
    }

    private static Object f()
    {
        return com.a.b.d.zw.a;
    }

    public final com.a.b.d.yd a()
    {
        return com.a.b.d.xz.a;
    }

    public final synthetic Object a(Object p2, Object p3)
    {
        return ((Comparable) com.a.b.d.xz.a.b(((Comparable) p2), ((Comparable) p3)));
    }

    public final synthetic Object a(Object p2, Object p3, Object p4, Object[] p5)
    {
        return ((Comparable) com.a.b.d.xz.a.b(((Comparable) p2), ((Comparable) p3), ((Comparable) p4), ((Comparable[]) p5)));
    }

    public final synthetic Object a(java.util.Iterator p2)
    {
        return ((Comparable) com.a.b.d.xz.a.b(p2));
    }

    public final synthetic Object b(Object p2, Object p3)
    {
        return ((Comparable) com.a.b.d.xz.a.a(((Comparable) p2), ((Comparable) p3)));
    }

    public final synthetic Object b(Object p2, Object p3, Object p4, Object[] p5)
    {
        return ((Comparable) com.a.b.d.xz.a.a(((Comparable) p2), ((Comparable) p3), ((Comparable) p4), ((Comparable[]) p5)));
    }

    public final synthetic Object b(java.util.Iterator p2)
    {
        return ((Comparable) com.a.b.d.xz.a.a(p2));
    }

    public final synthetic Object c(Iterable p2)
    {
        return ((Comparable) com.a.b.d.xz.a.d(p2));
    }

    public final synthetic int compare(Object p2, Object p3)
    {
        int v0;
        com.a.b.b.cn.a(((Comparable) p2));
        if (((Comparable) p2) != ((Comparable) p3)) {
            v0 = ((Comparable) p3).compareTo(((Comparable) p2));
        } else {
            v0 = 0;
        }
        return v0;
    }

    public final synthetic Object d(Iterable p2)
    {
        return ((Comparable) com.a.b.d.xz.a.c(p2));
    }

    public final String toString()
    {
        return "Ordering.natural().reverse()";
    }
}
