package com.a.b.d;
 class st extends java.lang.ref.WeakReference implements com.a.b.d.rz {
    final int a;
    final com.a.b.d.rz b;
    volatile com.a.b.d.sr c;

    st(ref.ReferenceQueue p2, Object p3, int p4, com.a.b.d.rz p5)
    {
        this(p3, p2);
        this.c = com.a.b.d.qy.g();
        this.a = p4;
        this.b = p5;
        return;
    }

    public final com.a.b.d.sr a()
    {
        return this.c;
    }

    public void a(long p2)
    {
        throw new UnsupportedOperationException();
    }

    public void a(com.a.b.d.rz p2)
    {
        throw new UnsupportedOperationException();
    }

    public final void a(com.a.b.d.sr p2)
    {
        com.a.b.d.sr v0 = this.c;
        this.c = p2;
        v0.a(p2);
        return;
    }

    public final com.a.b.d.rz b()
    {
        return this.b;
    }

    public void b(com.a.b.d.rz p2)
    {
        throw new UnsupportedOperationException();
    }

    public final int c()
    {
        return this.a;
    }

    public void c(com.a.b.d.rz p2)
    {
        throw new UnsupportedOperationException();
    }

    public final Object d()
    {
        return this.get();
    }

    public void d(com.a.b.d.rz p2)
    {
        throw new UnsupportedOperationException();
    }

    public long e()
    {
        throw new UnsupportedOperationException();
    }

    public com.a.b.d.rz f()
    {
        throw new UnsupportedOperationException();
    }

    public com.a.b.d.rz g()
    {
        throw new UnsupportedOperationException();
    }

    public com.a.b.d.rz h()
    {
        throw new UnsupportedOperationException();
    }

    public com.a.b.d.rz i()
    {
        throw new UnsupportedOperationException();
    }
}
