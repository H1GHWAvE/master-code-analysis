package com.a.b.d;
final class aeu implements java.util.Iterator {
    com.a.b.d.aez a;
    com.a.b.d.xd b;
    final synthetic com.a.b.d.aer c;

    aeu(com.a.b.d.aer p2)
    {
        this.c = p2;
        this.a = com.a.b.d.aer.d(this.c);
        this.b = 0;
        return;
    }

    private com.a.b.d.xd a()
    {
        if (this.hasNext()) {
            com.a.b.d.xd v0_2 = com.a.b.d.aer.a(this.c, this.a);
            this.b = v0_2;
            if (this.a.g != com.a.b.d.aer.c(this.c)) {
                this.a = this.a.g;
            } else {
                this.a = 0;
            }
            return v0_2;
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public final boolean hasNext()
    {
        int v0 = 0;
        if (this.a != null) {
            if (!com.a.b.d.aer.b(this.c).a(this.a.a)) {
                v0 = 1;
            } else {
                this.a = 0;
            }
        }
        return v0;
    }

    public final synthetic Object next()
    {
        if (this.hasNext()) {
            com.a.b.d.xd v0_2 = com.a.b.d.aer.a(this.c, this.a);
            this.b = v0_2;
            if (this.a.g != com.a.b.d.aer.c(this.c)) {
                this.a = this.a.g;
            } else {
                this.a = 0;
            }
            return v0_2;
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public final void remove()
    {
        int v0_1;
        if (this.b == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.b(v0_1, "no calls to next() since the last call to remove()");
        this.c.c(this.b.a(), 0);
        this.b = 0;
        return;
    }
}
