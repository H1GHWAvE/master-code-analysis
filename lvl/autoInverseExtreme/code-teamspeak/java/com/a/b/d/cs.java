package com.a.b.d;
final class cs extends com.a.b.d.j {
    final java.util.List a;
    final int[] b;
    final int[] c;
    int d;

    cs(java.util.List p3)
    {
        this.a = new java.util.ArrayList(p3);
        int v0_2 = p3.size();
        int v1_0 = new int[v0_2];
        this.b = v1_0;
        int v0_3 = new int[v0_2];
        this.c = v0_3;
        java.util.Arrays.fill(this.b, 0);
        java.util.Arrays.fill(this.c, 1);
        this.d = 2147483647;
        return;
    }

    private java.util.List c()
    {
        int v0_8;
        if (this.d > 0) {
            com.a.b.d.jl v1 = com.a.b.d.jl.a(this.a);
            this.d = (this.a.size() - 1);
            int v0_5 = 0;
            if (this.d != -1) {
                while(true) {
                    int v2_3 = (this.b[this.d] + this.c[this.d]);
                    if (v2_3 >= 0) {
                        if (v2_3 != (this.d + 1)) {
                            break;
                        }
                        if (this.d != 0) {
                            v0_5++;
                            this.e();
                        }
                    } else {
                        this.e();
                    }
                }
                java.util.Collections.swap(this.a, ((this.d - this.b[this.d]) + v0_5), (v0_5 + (this.d - v2_3)));
                this.b[this.d] = v2_3;
            }
            v0_8 = v1;
        } else {
            this.b();
            v0_8 = 0;
        }
        return v0_8;
    }

    private void d()
    {
        this.d = (this.a.size() - 1);
        int v0_3 = 0;
        if (this.d != -1) {
            while(true) {
                int v1_3 = (this.b[this.d] + this.c[this.d]);
                if (v1_3 >= 0) {
                    if (v1_3 != (this.d + 1)) {
                        break;
                    }
                    if (this.d != 0) {
                        v0_3++;
                        this.e();
                    }
                } else {
                    this.e();
                }
            }
            java.util.Collections.swap(this.a, ((this.d - this.b[this.d]) + v0_3), (v0_3 + (this.d - v1_3)));
            this.b[this.d] = v1_3;
        }
        return;
    }

    private void e()
    {
        this.c[this.d] = (- this.c[this.d]);
        this.d = (this.d - 1);
        return;
    }

    protected final synthetic Object a()
    {
        int v0_8;
        if (this.d > 0) {
            com.a.b.d.jl v1 = com.a.b.d.jl.a(this.a);
            this.d = (this.a.size() - 1);
            int v0_5 = 0;
            if (this.d != -1) {
                while(true) {
                    int v2_3 = (this.b[this.d] + this.c[this.d]);
                    if (v2_3 >= 0) {
                        if (v2_3 != (this.d + 1)) {
                            break;
                        }
                        if (this.d != 0) {
                            v0_5++;
                            this.e();
                        }
                    } else {
                        this.e();
                    }
                }
                java.util.Collections.swap(this.a, ((this.d - this.b[this.d]) + v0_5), (v0_5 + (this.d - v2_3)));
                this.b[this.d] = v2_3;
            }
            v0_8 = v1;
        } else {
            this.b();
            v0_8 = 0;
        }
        return v0_8;
    }
}
