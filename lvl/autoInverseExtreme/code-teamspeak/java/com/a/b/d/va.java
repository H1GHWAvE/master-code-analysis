package com.a.b.d;
final class va implements com.a.b.d.qk {
    private final Object a;
    private final Object b;

    va(Object p1, Object p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    private static com.a.b.d.qk a(Object p1, Object p2)
    {
        return new com.a.b.d.va(p1, p2);
    }

    public final Object a()
    {
        return this.a;
    }

    public final Object b()
    {
        return this.b;
    }

    public final boolean equals(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof com.a.b.d.qk)) && ((com.a.b.b.ce.a(this.a, ((com.a.b.d.qk) p4).a())) && (com.a.b.b.ce.a(this.b, ((com.a.b.d.qk) p4).b())))) {
            v0 = 1;
        }
        return v0;
    }

    public final int hashCode()
    {
        int v0_1 = new Object[2];
        v0_1[0] = this.a;
        v0_1[1] = this.b;
        return java.util.Arrays.hashCode(v0_1);
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        String v1_2 = String.valueOf(String.valueOf(this.b));
        return new StringBuilder(((v0_2.length() + 4) + v1_2.length())).append("(").append(v0_2).append(", ").append(v1_2).append(")").toString();
    }
}
