package com.a.b.d;
final class ag extends com.a.b.d.ab implements java.util.Set {
    final synthetic com.a.b.d.n a;

    ag(com.a.b.d.n p2, Object p3, java.util.Set p4)
    {
        this.a = p2;
        this(p2, p3, p4, 0);
        return;
    }

    public final boolean removeAll(java.util.Collection p5)
    {
        boolean v0_3;
        if (!p5.isEmpty()) {
            int v1_0 = this.size();
            v0_3 = com.a.b.d.aad.a(((java.util.Set) this.c), p5);
            if (v0_3) {
                com.a.b.d.n.a(this.a, (this.c.size() - v1_0));
                this.b();
            }
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }
}
