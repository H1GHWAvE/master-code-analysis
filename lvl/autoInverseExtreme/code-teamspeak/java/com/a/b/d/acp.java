package com.a.b.d;
final class acp extends com.a.b.d.gw {
    final synthetic java.util.Map$Entry a;
    final synthetic com.a.b.d.aco b;

    acp(com.a.b.d.aco p1, java.util.Map$Entry p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    protected final java.util.Map$Entry a()
    {
        return this.a;
    }

    public final boolean equals(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof java.util.Map$Entry)) && ((com.a.b.b.ce.a(this.getKey(), ((java.util.Map$Entry) p4).getKey())) && (com.a.b.b.ce.a(this.getValue(), ((java.util.Map$Entry) p4).getValue())))) {
            v0 = 1;
        }
        return v0;
    }

    protected final bridge synthetic Object k_()
    {
        return this.a;
    }

    public final Object setValue(Object p2)
    {
        return super.setValue(com.a.b.b.cn.a(p2));
    }
}
