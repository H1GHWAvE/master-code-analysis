package com.a.b.d;
final class afq extends com.a.b.d.av {
    private final java.util.NavigableMap a;
    private final java.util.NavigableMap b;
    private final com.a.b.d.yl c;

    afq(java.util.NavigableMap p2)
    {
        this(p2, com.a.b.d.yl.c());
        return;
    }

    private afq(java.util.NavigableMap p2, com.a.b.d.yl p3)
    {
        this.a = p2;
        this.b = new com.a.b.d.aft(p2);
        this.c = p3;
        return;
    }

    static synthetic com.a.b.d.yl a(com.a.b.d.afq p1)
    {
        return p1.c;
    }

    private com.a.b.d.yl a(Object p4)
    {
        com.a.b.d.yl v0_6;
        if (!(p4 instanceof com.a.b.d.dw)) {
            v0_6 = 0;
        } else {
            try {
                java.util.Map$Entry v2 = this.b(((com.a.b.d.dw) p4), 1).firstEntry();
            } catch (com.a.b.d.yl v0) {
                v0_6 = 0;
            }
            if ((v2 == null) || (!((com.a.b.d.dw) v2.getKey()).equals(((com.a.b.d.dw) p4)))) {
            } else {
                v0_6 = ((com.a.b.d.yl) v2.getValue());
            }
        }
        return v0_6;
    }

    private java.util.NavigableMap a(com.a.b.d.dw p2, boolean p3)
    {
        return this.a(com.a.b.d.yl.a(p2, com.a.b.d.ce.a(p3)));
    }

    private java.util.NavigableMap a(com.a.b.d.dw p3, boolean p4, com.a.b.d.dw p5, boolean p6)
    {
        return this.a(com.a.b.d.yl.a(p3, com.a.b.d.ce.a(p4), p5, com.a.b.d.ce.a(p6)));
    }

    private java.util.NavigableMap a(com.a.b.d.yl p4)
    {
        com.a.b.d.afq v0_4;
        if (this.c.b(p4)) {
            v0_4 = new com.a.b.d.afq(this.a, p4.c(this.c));
        } else {
            v0_4 = com.a.b.d.lw.m();
        }
        return v0_4;
    }

    private java.util.NavigableMap b(com.a.b.d.dw p2, boolean p3)
    {
        return this.a(com.a.b.d.yl.b(p2, com.a.b.d.ce.a(p3)));
    }

    final java.util.Iterator a()
    {
        com.a.b.d.afr v0_3;
        if (!this.c.d()) {
            v0_3 = this.b.values();
        } else {
            com.a.b.d.afr v0_9;
            com.a.b.d.afr v1_0 = this.b;
            com.a.b.d.yi v2_0 = this.c.b.c();
            if (this.c.b.a() != com.a.b.d.ce.b) {
                v0_9 = 0;
            } else {
                v0_9 = 1;
            }
            v0_3 = v1_0.tailMap(v2_0, v0_9).values();
        }
        com.a.b.d.afr v0_19;
        com.a.b.d.afr v0_22;
        com.a.b.d.yi v2_1 = com.a.b.d.nj.j(v0_3.iterator());
        if ((!this.c.c(com.a.b.d.dw.d())) || ((v2_1.hasNext()) && (((com.a.b.d.yl) v2_1.a()).b == com.a.b.d.dw.d()))) {
            if (!v2_1.hasNext()) {
                v0_19 = com.a.b.d.nj.a();
            } else {
                v0_22 = ((com.a.b.d.yl) v2_1.next()).c;
                v0_19 = new com.a.b.d.afr(this, v0_22, v2_1);
            }
        } else {
            v0_22 = com.a.b.d.dw.d();
        }
        return v0_19;
    }

    final java.util.Iterator b()
    {
        com.a.b.d.dw v0_2;
        if (!this.c.e()) {
            v0_2 = com.a.b.d.dw.e();
        } else {
            v0_2 = ((com.a.b.d.dw) this.c.c.c());
        }
        if ((!this.c.e()) || (this.c.c.b() != com.a.b.d.ce.b)) {
            com.a.b.d.dw v1_5 = 0;
        } else {
            v1_5 = 1;
        }
        com.a.b.d.dw v0_18;
        com.a.b.d.dw v0_17;
        com.a.b.d.yi v2_2 = com.a.b.d.nj.j(this.b.headMap(v0_2, v1_5).descendingMap().values().iterator());
        if (!v2_2.hasNext()) {
            if ((this.c.c(com.a.b.d.dw.d())) && (!this.a.containsKey(com.a.b.d.dw.d()))) {
                v0_17 = ((com.a.b.d.dw) this.a.higherKey(com.a.b.d.dw.d()));
                v0_18 = new com.a.b.d.afs(this, ((com.a.b.d.dw) com.a.b.b.ca.a(v0_17, com.a.b.d.dw.e())), v2_2);
            } else {
                v0_18 = com.a.b.d.nj.a();
            }
        } else {
            if (((com.a.b.d.yl) v2_2.a()).c != com.a.b.d.dw.e()) {
                v0_17 = ((com.a.b.d.dw) this.a.higherKey(((com.a.b.d.yl) v2_2.a()).c));
            } else {
                v0_17 = ((com.a.b.d.yl) v2_2.next()).b;
            }
        }
        return v0_18;
    }

    public final java.util.Comparator comparator()
    {
        return com.a.b.d.yd.d();
    }

    public final boolean containsKey(Object p2)
    {
        int v0_1;
        if (this.a(p2) == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final synthetic Object get(Object p2)
    {
        return this.a(p2);
    }

    public final synthetic java.util.NavigableMap headMap(Object p2, boolean p3)
    {
        return this.a(com.a.b.d.yl.a(((com.a.b.d.dw) p2), com.a.b.d.ce.a(p3)));
    }

    public final int size()
    {
        return com.a.b.d.nj.b(this.a());
    }

    public final synthetic java.util.NavigableMap subMap(Object p3, boolean p4, Object p5, boolean p6)
    {
        return this.a(com.a.b.d.yl.a(((com.a.b.d.dw) p3), com.a.b.d.ce.a(p4), ((com.a.b.d.dw) p5), com.a.b.d.ce.a(p6)));
    }

    public final synthetic java.util.NavigableMap tailMap(Object p2, boolean p3)
    {
        return this.b(((com.a.b.d.dw) p2), p3);
    }
}
