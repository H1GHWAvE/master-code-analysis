package com.a.b.d;
final class vh implements java.util.Iterator {
    final synthetic com.a.b.d.vc a;
    private int b;
    private int c;
    private java.util.Queue d;
    private java.util.List e;
    private Object f;
    private boolean g;

    private vh(com.a.b.d.vc p2)
    {
        this.a = p2;
        this.b = -1;
        this.c = com.a.b.d.vc.c(this.a);
        return;
    }

    synthetic vh(com.a.b.d.vc p1, byte p2)
    {
        this(p1);
        return;
    }

    private int a(int p4)
    {
        if (this.e != null) {
            while (p4 < this.a.size()) {
                Object v1_2 = this.a.b[p4];
                int v0_4 = this.e.iterator();
                while (v0_4.hasNext()) {
                    if (v0_4.next() == v1_2) {
                        int v0_5 = 1;
                    }
                    if (v0_5 != 0) {
                        p4++;
                    }
                }
                v0_5 = 0;
            }
        }
        return p4;
    }

    private void a()
    {
        if (com.a.b.d.vc.c(this.a) == this.c) {
            return;
        } else {
            throw new java.util.ConcurrentModificationException();
        }
    }

    private static boolean a(Iterable p2, Object p3)
    {
        int v0_0 = p2.iterator();
        while (v0_0.hasNext()) {
            if (v0_0.next() == p3) {
                int v0_1 = 1;
            }
            return v0_1;
        }
        v0_1 = 0;
        return v0_1;
    }

    private boolean a(Object p4)
    {
        int v1_0 = 0;
        int v0 = 0;
        while (v0 < com.a.b.d.vc.b(this.a)) {
            if (com.a.b.d.vc.a(this.a)[v0] != p4) {
                v0++;
            } else {
                this.a.a(v0);
                v1_0 = 1;
                break;
            }
        }
        return v1_0;
    }

    public final boolean hasNext()
    {
        int v0_6;
        this.a();
        if ((this.a((this.b + 1)) >= this.a.size()) && ((this.d == null) || (this.d.isEmpty()))) {
            v0_6 = 0;
        } else {
            v0_6 = 1;
        }
        return v0_6;
    }

    public final Object next()
    {
        Object v0_11;
        this.a();
        Object v0_2 = this.a((this.b + 1));
        if (v0_2 >= this.a.size()) {
            if (this.d != null) {
                this.b = this.a.size();
                this.f = this.d.poll();
                if (this.f != null) {
                    this.g = 1;
                    v0_11 = this.f;
                    return v0_11;
                }
            }
            throw new java.util.NoSuchElementException("iterator moved past last element in queue.");
        } else {
            this.b = v0_2;
            this.g = 1;
            v0_11 = this.a.b[this.b];
        }
        return v0_11;
    }

    public final void remove()
    {
        com.a.b.b.cn.b(this.g, "no calls to next() since the last call to remove()");
        this.a();
        this.g = 0;
        this.c = (this.c + 1);
        if (this.b >= this.a.size()) {
            com.a.b.b.cn.b(this.a(this.f));
            this.f = 0;
        } else {
            Object v0_9 = this.a.a(this.b);
            if (v0_9 != null) {
                if (this.d == null) {
                    this.d = new java.util.ArrayDeque();
                    this.e = new java.util.ArrayList(3);
                }
                this.d.add(v0_9.a);
                this.e.add(v0_9.b);
            }
            this.b = (this.b - 1);
        }
        return;
    }
}
