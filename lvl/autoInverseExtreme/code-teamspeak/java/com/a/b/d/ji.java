package com.a.b.d;
final class ji extends com.a.b.d.lo {
    private final transient java.util.EnumSet a;
    private transient int c;

    private ji(java.util.EnumSet p1)
    {
        this.a = p1;
        return;
    }

    synthetic ji(java.util.EnumSet p1, byte p2)
    {
        this(p1);
        return;
    }

    static com.a.b.d.lo a(java.util.EnumSet p1)
    {
        com.a.b.d.lo v0_2;
        switch (p1.size()) {
            case 0:
                v0_2 = com.a.b.d.ey.a;
                break;
            case 1:
                v0_2 = com.a.b.d.lo.d(com.a.b.d.mq.b(p1));
                break;
            default:
                v0_2 = new com.a.b.d.ji(p1);
        }
        return v0_2;
    }

    public final com.a.b.d.agi c()
    {
        return com.a.b.d.nj.a(this.a.iterator());
    }

    public final boolean contains(Object p2)
    {
        return this.a.contains(p2);
    }

    public final boolean containsAll(java.util.Collection p2)
    {
        return this.a.containsAll(p2);
    }

    public final boolean equals(Object p2)
    {
        if ((p2 != this) && (!this.a.equals(p2))) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    final Object g()
    {
        return new com.a.b.d.jk(this.a);
    }

    final boolean h_()
    {
        return 0;
    }

    public final int hashCode()
    {
        int v0_0 = this.c;
        if (v0_0 == 0) {
            v0_0 = this.a.hashCode();
            this.c = v0_0;
        }
        return v0_0;
    }

    public final boolean isEmpty()
    {
        return this.a.isEmpty();
    }

    public final synthetic java.util.Iterator iterator()
    {
        return this.c();
    }

    public final int size()
    {
        return this.a.size();
    }

    public final String toString()
    {
        return this.a.toString();
    }
}
