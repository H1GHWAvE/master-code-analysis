package com.a.b.d;
abstract class ba extends com.a.b.d.n implements com.a.b.d.aac {
    private static final long a = 7431625294878419160;

    protected ba(java.util.Map p1)
    {
        this(p1);
        return;
    }

    abstract java.util.Set a();

    public java.util.Set a(Object p2)
    {
        return ((java.util.Set) super.c(p2));
    }

    public java.util.Set a(Object p2, Iterable p3)
    {
        return ((java.util.Set) super.b(p2, p3));
    }

    public boolean a(Object p2, Object p3)
    {
        return super.a(p2, p3);
    }

    public synthetic java.util.Collection b(Object p2, Iterable p3)
    {
        return this.a(p2, p3);
    }

    public java.util.Map b()
    {
        return super.b();
    }

    public java.util.Set b(Object p2)
    {
        return ((java.util.Set) super.d(p2));
    }

    synthetic java.util.Collection c()
    {
        return this.a();
    }

    public synthetic java.util.Collection c(Object p2)
    {
        return this.a(p2);
    }

    synthetic java.util.Collection d()
    {
        return this.t();
    }

    public synthetic java.util.Collection d(Object p2)
    {
        return this.b(p2);
    }

    public boolean equals(Object p2)
    {
        return super.equals(p2);
    }

    public synthetic java.util.Collection k()
    {
        return this.u();
    }

    java.util.Set t()
    {
        return com.a.b.d.lo.h();
    }

    public java.util.Set u()
    {
        return ((java.util.Set) super.k());
    }
}
