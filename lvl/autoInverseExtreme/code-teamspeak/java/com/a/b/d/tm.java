package com.a.b.d;
 class tm extends com.a.b.d.uj {
    final com.a.b.b.bj a;
    private final java.util.Set b;

    tm(java.util.Set p2, com.a.b.b.bj p3)
    {
        this.b = ((java.util.Set) com.a.b.b.cn.a(p2));
        this.a = ((com.a.b.b.bj) com.a.b.b.cn.a(p3));
        return;
    }

    protected final java.util.Set a()
    {
        return new com.a.b.d.tn(this);
    }

    java.util.Set c()
    {
        return this.b;
    }

    final java.util.Collection c_()
    {
        return com.a.b.d.cm.a(this.b, this.a);
    }

    public void clear()
    {
        this.c().clear();
        return;
    }

    public boolean containsKey(Object p2)
    {
        return this.c().contains(p2);
    }

    public final java.util.Set e()
    {
        return com.a.b.d.sz.b(this.c());
    }

    public Object get(Object p2)
    {
        int v0_2;
        if (!com.a.b.d.cm.a(this.c(), p2)) {
            v0_2 = 0;
        } else {
            v0_2 = this.a.e(p2);
        }
        return v0_2;
    }

    public Object remove(Object p2)
    {
        int v0_2;
        if (!this.c().remove(p2)) {
            v0_2 = 0;
        } else {
            v0_2 = this.a.e(p2);
        }
        return v0_2;
    }

    public int size()
    {
        return this.c().size();
    }
}
