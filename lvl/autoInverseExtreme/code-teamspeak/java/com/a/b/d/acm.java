package com.a.b.d;
 class acm extends com.a.b.d.uj {
    final Object a;
    java.util.Map b;
    final synthetic com.a.b.d.abx c;

    acm(com.a.b.d.abx p2, Object p3)
    {
        this.c = p2;
        this.a = com.a.b.b.cn.a(p3);
        return;
    }

    protected final java.util.Set a()
    {
        return new com.a.b.d.acn(this, 0);
    }

    java.util.Map c()
    {
        if ((this.b != null) && ((!this.b.isEmpty()) || (!this.c.a.containsKey(this.a)))) {
            java.util.Map v0_6 = this.b;
        } else {
            v0_6 = this.d();
            this.b = v0_6;
        }
        return v0_6;
    }

    public void clear()
    {
        java.util.Map v0 = this.c();
        if (v0 != null) {
            v0.clear();
        }
        this.f();
        return;
    }

    public boolean containsKey(Object p2)
    {
        int v0_2;
        int v0_0 = this.c();
        if ((p2 == null) || ((v0_0 == 0) || (!com.a.b.d.sz.b(v0_0, p2)))) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    java.util.Map d()
    {
        return ((java.util.Map) this.c.a.get(this.a));
    }

    void f()
    {
        if ((this.c() != null) && (this.b.isEmpty())) {
            this.c.a.remove(this.a);
            this.b = 0;
        }
        return;
    }

    public Object get(Object p2)
    {
        Object v0_1;
        Object v0_0 = this.c();
        if ((p2 == null) || (v0_0 == null)) {
            v0_1 = 0;
        } else {
            v0_1 = com.a.b.d.sz.a(v0_0, p2);
        }
        return v0_1;
    }

    public Object put(Object p3, Object p4)
    {
        Object v0_4;
        com.a.b.b.cn.a(p3);
        com.a.b.b.cn.a(p4);
        if ((this.b == null) || (this.b.isEmpty())) {
            v0_4 = this.c.a(this.a, p3, p4);
        } else {
            v0_4 = this.b.put(p3, p4);
        }
        return v0_4;
    }

    public Object remove(Object p2)
    {
        Object v0_1;
        Object v0_0 = this.c();
        if (v0_0 != null) {
            v0_1 = com.a.b.d.sz.c(v0_0, p2);
            this.f();
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }
}
