package com.a.b.d;
final class pu extends com.a.b.d.gh {
    final java.util.Collection a;
    final java.util.Set b;

    pu(java.util.Collection p1, java.util.Set p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    protected final java.util.Collection b()
    {
        return this.a;
    }

    public final boolean contains(Object p2)
    {
        return this.b(p2);
    }

    public final boolean containsAll(java.util.Collection p2)
    {
        return com.a.b.d.cm.a(this, p2);
    }

    public final java.util.Iterator iterator()
    {
        return new com.a.b.d.pv(this, this.b.iterator());
    }

    protected final bridge synthetic Object k_()
    {
        return this.a;
    }

    public final boolean remove(Object p2)
    {
        return this.c(p2);
    }

    public final boolean removeAll(java.util.Collection p2)
    {
        return this.b(p2);
    }

    public final boolean retainAll(java.util.Collection p2)
    {
        return this.c(p2);
    }

    public final Object[] toArray()
    {
        return this.p();
    }

    public final Object[] toArray(Object[] p2)
    {
        return com.a.b.d.yc.a(this, p2);
    }
}
