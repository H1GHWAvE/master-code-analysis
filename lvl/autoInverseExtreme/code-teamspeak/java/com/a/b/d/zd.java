package com.a.b.d;
final class zd extends com.a.b.d.ka {
    private final com.a.b.d.ka a;
    private final com.a.b.d.ka b;

    zd(com.a.b.d.ka p1, com.a.b.d.ka p2, com.a.b.d.ka p3)
    {
        this(p1);
        this.a = p2;
        this.b = p3;
        return;
    }

    zd(Object p1, Object p2, com.a.b.d.ka p3, com.a.b.d.ka p4)
    {
        this(p1, p2);
        this.a = p3;
        this.b = p4;
        return;
    }

    final com.a.b.d.ka a()
    {
        return this.a;
    }

    final com.a.b.d.ka b()
    {
        return this.b;
    }
}
