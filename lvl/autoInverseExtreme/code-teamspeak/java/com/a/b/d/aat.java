package com.a.b.d;
final class aat extends com.a.b.d.hp implements java.io.Serializable, java.util.NavigableSet {
    private static final long c;
    private final java.util.NavigableSet a;
    private transient com.a.b.d.aat b;

    aat(java.util.NavigableSet p2)
    {
        this.a = ((java.util.NavigableSet) com.a.b.b.cn.a(p2));
        return;
    }

    protected final synthetic java.util.Set a()
    {
        return java.util.Collections.unmodifiableSortedSet(this.a);
    }

    protected final synthetic java.util.Collection b()
    {
        return java.util.Collections.unmodifiableSortedSet(this.a);
    }

    protected final java.util.SortedSet c()
    {
        return java.util.Collections.unmodifiableSortedSet(this.a);
    }

    public final Object ceiling(Object p2)
    {
        return this.a.ceiling(p2);
    }

    public final java.util.Iterator descendingIterator()
    {
        return com.a.b.d.nj.a(this.a.descendingIterator());
    }

    public final java.util.NavigableSet descendingSet()
    {
        com.a.b.d.aat v0_0 = this.b;
        if (v0_0 == null) {
            v0_0 = new com.a.b.d.aat(this.a.descendingSet());
            this.b = v0_0;
            v0_0.b = this;
        }
        return v0_0;
    }

    public final Object floor(Object p2)
    {
        return this.a.floor(p2);
    }

    public final java.util.NavigableSet headSet(Object p2, boolean p3)
    {
        return com.a.b.d.aad.a(this.a.headSet(p2, p3));
    }

    public final Object higher(Object p2)
    {
        return this.a.higher(p2);
    }

    protected final synthetic Object k_()
    {
        return java.util.Collections.unmodifiableSortedSet(this.a);
    }

    public final Object lower(Object p2)
    {
        return this.a.lower(p2);
    }

    public final Object pollFirst()
    {
        throw new UnsupportedOperationException();
    }

    public final Object pollLast()
    {
        throw new UnsupportedOperationException();
    }

    public final java.util.NavigableSet subSet(Object p2, boolean p3, Object p4, boolean p5)
    {
        return com.a.b.d.aad.a(this.a.subSet(p2, p3, p4, p5));
    }

    public final java.util.NavigableSet tailSet(Object p2, boolean p3)
    {
        return com.a.b.d.aad.a(this.a.tailSet(p2, p3));
    }
}
