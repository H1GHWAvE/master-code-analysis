package com.a.b.d;
public abstract class kk extends com.a.b.d.an implements java.io.Serializable {
    private static final long a;
    public final transient com.a.b.d.jt b;
    final transient int c;

    kk(com.a.b.d.jt p1, int p2)
    {
        this.b = p1;
        this.c = p2;
        return;
    }

    private com.a.b.d.jt A()
    {
        return this.b;
    }

    private com.a.b.d.iz B()
    {
        return new com.a.b.d.kp(this);
    }

    private com.a.b.d.ku C()
    {
        return ((com.a.b.d.ku) super.q());
    }

    private com.a.b.d.ku D()
    {
        return new com.a.b.d.ks(this);
    }

    private com.a.b.d.iz E()
    {
        return new com.a.b.d.kt(this);
    }

    private static com.a.b.d.kk a()
    {
        return com.a.b.d.ex.a;
    }

    private static com.a.b.d.kk a(Object p1, Object p2, Object p3, Object p4)
    {
        com.a.b.d.jr v0_1 = new com.a.b.d.js();
        v0_1.a(p1, p2);
        v0_1.a(p3, p4);
        return v0_1.a();
    }

    private static com.a.b.d.kk a(Object p1, Object p2, Object p3, Object p4, Object p5, Object p6)
    {
        com.a.b.d.jr v0_1 = new com.a.b.d.js();
        v0_1.a(p1, p2);
        v0_1.a(p3, p4);
        v0_1.a(p5, p6);
        return v0_1.a();
    }

    private static com.a.b.d.kk a(Object p1, Object p2, Object p3, Object p4, Object p5, Object p6, Object p7, Object p8)
    {
        com.a.b.d.jr v0_1 = new com.a.b.d.js();
        v0_1.a(p1, p2);
        v0_1.a(p3, p4);
        v0_1.a(p5, p6);
        v0_1.a(p7, p8);
        return v0_1.a();
    }

    private static com.a.b.d.kk a(Object p1, Object p2, Object p3, Object p4, Object p5, Object p6, Object p7, Object p8, Object p9, Object p10)
    {
        com.a.b.d.jr v0_1 = new com.a.b.d.js();
        v0_1.a(p1, p2);
        v0_1.a(p3, p4);
        v0_1.a(p5, p6);
        v0_1.a(p7, p8);
        v0_1.a(p9, p10);
        return v0_1.a();
    }

    public static com.a.b.d.kk b(com.a.b.d.vi p6)
    {
        int v0_2;
        if (!(p6 instanceof com.a.b.d.kk)) {
            if (!p6.n()) {
                if ((p6 instanceof com.a.b.d.jr)) {
                    v0_2 = ((com.a.b.d.jr) p6);
                    if (!((com.a.b.d.jr) p6).b.i_()) {
                        return v0_2;
                    }
                }
                com.a.b.d.ju v3 = com.a.b.d.jt.l();
                java.util.Iterator v4 = p6.b().entrySet().iterator();
                int v2 = 0;
                while (v4.hasNext()) {
                    int v0_11;
                    int v0_10 = ((java.util.Map$Entry) v4.next());
                    com.a.b.d.jt v1_9 = com.a.b.d.jl.a(((java.util.Collection) v0_10.getValue()));
                    if (v1_9.isEmpty()) {
                        v0_11 = v2;
                    } else {
                        v3.a(v0_10.getKey(), v1_9);
                        v0_11 = (v1_9.size() + v2);
                    }
                    v2 = v0_11;
                }
                v0_2 = new com.a.b.d.jr(v3.a(), v2);
            } else {
                v0_2 = com.a.b.d.ex.a;
            }
        } else {
            v0_2 = ((com.a.b.d.kk) p6);
            if (((com.a.b.d.kk) p6).b.i_()) {
            }
        }
        return v0_2;
    }

    private static com.a.b.d.kn c()
    {
        return new com.a.b.d.kn();
    }

    private static com.a.b.d.kk d(Object p1, Object p2)
    {
        return com.a.b.d.jr.d(p1, p2);
    }

    private boolean u()
    {
        return this.b.i_();
    }

    private com.a.b.d.lo z()
    {
        return this.b.g();
    }

    public final boolean a(com.a.b.d.vi p2)
    {
        throw new UnsupportedOperationException();
    }

    public final boolean a(Object p2, Object p3)
    {
        throw new UnsupportedOperationException();
    }

    public synthetic java.util.Collection b(Object p2, Iterable p3)
    {
        return this.e();
    }

    public final bridge synthetic java.util.Map b()
    {
        return this.b;
    }

    public final bridge synthetic boolean b(Object p2, Object p3)
    {
        return super.b(p2, p3);
    }

    public synthetic java.util.Collection c(Object p2)
    {
        return this.h(p2);
    }

    public final boolean c(Object p2, Iterable p3)
    {
        throw new UnsupportedOperationException();
    }

    public final boolean c(Object p2, Object p3)
    {
        throw new UnsupportedOperationException();
    }

    public abstract com.a.b.d.kk d();

    public synthetic java.util.Collection d(Object p2)
    {
        return this.t();
    }

    public com.a.b.d.iz e()
    {
        throw new UnsupportedOperationException();
    }

    public bridge synthetic boolean equals(Object p2)
    {
        return super.equals(p2);
    }

    public final int f()
    {
        return this.c;
    }

    public final boolean f(Object p2)
    {
        return this.b.containsKey(p2);
    }

    public final void g()
    {
        throw new UnsupportedOperationException();
    }

    public final boolean g(Object p2)
    {
        if ((p2 == null) || (!super.g(p2))) {
            int v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public abstract com.a.b.d.iz h();

    public bridge synthetic int hashCode()
    {
        return super.hashCode();
    }

    public final bridge synthetic java.util.Collection i()
    {
        return ((com.a.b.d.iz) super.i());
    }

    final synthetic java.util.Iterator j()
    {
        return this.y();
    }

    public synthetic java.util.Collection k()
    {
        return this.v();
    }

    final synthetic java.util.Iterator l()
    {
        return this.w();
    }

    final java.util.Map m()
    {
        throw new AssertionError("should never be called");
    }

    public final bridge synthetic boolean n()
    {
        return super.n();
    }

    final synthetic java.util.Collection o()
    {
        return new com.a.b.d.kp(this);
    }

    public final synthetic java.util.Set p()
    {
        return this.b.g();
    }

    public final bridge synthetic com.a.b.d.xc q()
    {
        return ((com.a.b.d.ku) super.q());
    }

    final synthetic com.a.b.d.xc r()
    {
        return new com.a.b.d.ks(this);
    }

    final synthetic java.util.Collection s()
    {
        return new com.a.b.d.kt(this);
    }

    public com.a.b.d.iz t()
    {
        throw new UnsupportedOperationException();
    }

    public bridge synthetic String toString()
    {
        return super.toString();
    }

    public com.a.b.d.iz v()
    {
        return ((com.a.b.d.iz) super.k());
    }

    final com.a.b.d.agi w()
    {
        return new com.a.b.d.kl(this);
    }

    public final com.a.b.d.iz x()
    {
        return ((com.a.b.d.iz) super.i());
    }

    final com.a.b.d.agi y()
    {
        return new com.a.b.d.km(this);
    }
}
