package com.a.b.d;
public final class xe {
    private static final com.a.b.d.yd a;

    static xe()
    {
        com.a.b.d.xe.a = new com.a.b.d.xn();
        return;
    }

    private xe()
    {
        return;
    }

    static int a(com.a.b.d.xc p2, Object p3, int p4)
    {
        com.a.b.d.cl.a(p4, "count");
        int v0_1 = p2.a(p3);
        int v1_0 = (p4 - v0_1);
        if (v1_0 <= 0) {
            if (v1_0 < 0) {
                p2.b(p3, (- v1_0));
            }
        } else {
            p2.a(p3, v1_0);
        }
        return v0_1;
    }

    static int a(Iterable p1)
    {
        int v0_1;
        if (!(p1 instanceof com.a.b.d.xc)) {
            v0_1 = 11;
        } else {
            v0_1 = ((com.a.b.d.xc) p1).n_().size();
        }
        return v0_1;
    }

    public static com.a.b.d.abn a(com.a.b.d.abn p2)
    {
        return new com.a.b.d.agk(((com.a.b.d.abn) com.a.b.b.cn.a(p2)));
    }

    private static com.a.b.d.xc a(com.a.b.d.ku p1)
    {
        return ((com.a.b.d.xc) com.a.b.b.cn.a(p1));
    }

    public static com.a.b.d.xc a(com.a.b.d.xc p2)
    {
        if ((!(p2 instanceof com.a.b.d.xw)) && (!(p2 instanceof com.a.b.d.ku))) {
            p2 = new com.a.b.d.xw(((com.a.b.d.xc) com.a.b.b.cn.a(p2)));
        }
        return p2;
    }

    public static com.a.b.d.xc a(com.a.b.d.xc p3, com.a.b.b.co p4)
    {
        com.a.b.d.xs v0_2;
        if (!(p3 instanceof com.a.b.d.xs)) {
            v0_2 = new com.a.b.d.xs(p3, p4);
        } else {
            v0_2 = new com.a.b.d.xs(((com.a.b.d.xs) p3).a, com.a.b.b.cp.a(((com.a.b.d.xs) p3).b, p4));
        }
        return v0_2;
    }

    private static com.a.b.d.xc a(com.a.b.d.xc p1, com.a.b.d.xc p2)
    {
        com.a.b.b.cn.a(p1);
        com.a.b.b.cn.a(p2);
        return new com.a.b.d.xf(p1, p2);
    }

    public static com.a.b.d.xd a(Object p1, int p2)
    {
        return new com.a.b.d.xu(p1, p2);
    }

    private static boolean a(com.a.b.d.xc p6, Iterable p7)
    {
        boolean v1_2;
        boolean v0_0 = 0;
        if (!(p7 instanceof com.a.b.d.xc)) {
            com.a.b.b.cn.a(p6);
            com.a.b.b.cn.a(p7);
            boolean v1_1 = p7.iterator();
            while (v1_1.hasNext()) {
                v0_0 |= p6.remove(v1_1.next());
            }
            v1_2 = v0_0;
        } else {
            com.a.b.b.cn.a(p6);
            com.a.b.b.cn.a(((com.a.b.d.xc) p7));
            java.util.Iterator v3 = p6.a().iterator();
            v1_2 = 0;
            while (v3.hasNext()) {
                boolean v0_3 = ((com.a.b.d.xd) v3.next());
                int v4_1 = ((com.a.b.d.xc) p7).a(v0_3.a());
                if (v4_1 < v0_3.b()) {
                    boolean v0_4;
                    if (v4_1 <= 0) {
                        v0_4 = v1_2;
                    } else {
                        p6.b(v0_3.a(), v4_1);
                        v0_4 = 1;
                    }
                    v1_2 = v0_4;
                } else {
                    v3.remove();
                    v1_2 = 1;
                }
            }
        }
        return v1_2;
    }

    static boolean a(com.a.b.d.xc p5, Object p6)
    {
        int v0_1;
        if (p6 != p5) {
            if (!(p6 instanceof com.a.b.d.xc)) {
                v0_1 = 0;
            } else {
                if ((p5.size() == ((com.a.b.d.xc) p6).size()) && (p5.a().size() == ((com.a.b.d.xc) p6).a().size())) {
                    java.util.Iterator v3_3 = ((com.a.b.d.xc) p6).a().iterator();
                    while (v3_3.hasNext()) {
                        int v0_8 = ((com.a.b.d.xd) v3_3.next());
                        if (p5.a(v0_8.a()) != v0_8.b()) {
                            v0_1 = 0;
                        }
                    }
                    v0_1 = 1;
                } else {
                    v0_1 = 0;
                }
            }
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    static boolean a(com.a.b.d.xc p1, Object p2, int p3, int p4)
    {
        int v0_3;
        com.a.b.d.cl.a(p3, "oldCount");
        com.a.b.d.cl.a(p4, "newCount");
        if (p1.a(p2) != p3) {
            v0_3 = 0;
        } else {
            p1.c(p2, p4);
            v0_3 = 1;
        }
        return v0_3;
    }

    static boolean a(com.a.b.d.xc p3, java.util.Collection p4)
    {
        int v0_5;
        if (!p4.isEmpty()) {
            if (!(p4 instanceof com.a.b.d.xc)) {
                com.a.b.d.nj.a(p3, p4.iterator());
            } else {
                java.util.Iterator v1 = ((com.a.b.d.xc) p4).a().iterator();
                while (v1.hasNext()) {
                    int v0_7 = ((com.a.b.d.xd) v1.next());
                    p3.a(v0_7.a(), v0_7.b());
                }
            }
            v0_5 = 1;
        } else {
            v0_5 = 0;
        }
        return v0_5;
    }

    private static com.a.b.d.xc b(com.a.b.d.xc p1, com.a.b.d.xc p2)
    {
        com.a.b.b.cn.a(p1);
        com.a.b.b.cn.a(p2);
        return new com.a.b.d.xh(p1, p2);
    }

    static com.a.b.d.xc b(Iterable p0)
    {
        return ((com.a.b.d.xc) p0);
    }

    static java.util.Iterator b(com.a.b.d.xc p2)
    {
        return new com.a.b.d.xv(p2, p2.a().iterator());
    }

    private static boolean b(com.a.b.d.xc p3, Iterable p4)
    {
        com.a.b.b.cn.a(p3);
        com.a.b.b.cn.a(p4);
        int v0 = 0;
        java.util.Iterator v1 = p4.iterator();
        while (v1.hasNext()) {
            v0 |= p3.remove(v1.next());
        }
        return v0;
    }

    static boolean b(com.a.b.d.xc p1, java.util.Collection p2)
    {
        if ((p2 instanceof com.a.b.d.xc)) {
            p2 = ((com.a.b.d.xc) p2).n_();
        }
        return p1.n_().removeAll(p2);
    }

    static int c(com.a.b.d.xc p5)
    {
        java.util.Iterator v4 = p5.a().iterator();
        long v2_1 = 0;
        while (v4.hasNext()) {
            v2_1 = (((long) ((com.a.b.d.xd) v4.next()).b()) + v2_1);
        }
        return com.a.b.l.q.b(v2_1);
    }

    private static com.a.b.d.xc c(com.a.b.d.xc p1, com.a.b.d.xc p2)
    {
        com.a.b.b.cn.a(p1);
        com.a.b.b.cn.a(p2);
        return new com.a.b.d.xj(p1, p2);
    }

    static boolean c(com.a.b.d.xc p1, java.util.Collection p2)
    {
        com.a.b.b.cn.a(p2);
        if ((p2 instanceof com.a.b.d.xc)) {
            p2 = ((com.a.b.d.xc) p2).n_();
        }
        return p1.n_().retainAll(p2);
    }

    private static com.a.b.d.ku d(com.a.b.d.xc p2)
    {
        return com.a.b.d.ku.a(com.a.b.d.xe.a.b(p2.a()));
    }

    private static com.a.b.d.xc d(com.a.b.d.xc p1, com.a.b.d.xc p2)
    {
        com.a.b.b.cn.a(p1);
        com.a.b.b.cn.a(p2);
        return new com.a.b.d.xl(p1, p2);
    }

    private static boolean e(com.a.b.d.xc p3, com.a.b.d.xc p4)
    {
        com.a.b.b.cn.a(p3);
        com.a.b.b.cn.a(p4);
        java.util.Iterator v1 = p4.a().iterator();
        while (v1.hasNext()) {
            int v0_4 = ((com.a.b.d.xd) v1.next());
            if (p3.a(v0_4.a()) < v0_4.b()) {
                int v0_2 = 0;
            }
            return v0_2;
        }
        v0_2 = 1;
        return v0_2;
    }

    private static boolean f(com.a.b.d.xc p6, com.a.b.d.xc p7)
    {
        com.a.b.b.cn.a(p6);
        com.a.b.b.cn.a(p7);
        java.util.Iterator v3 = p6.a().iterator();
        int v1 = 0;
        while (v3.hasNext()) {
            int v0_4 = ((com.a.b.d.xd) v3.next());
            int v4_1 = p7.a(v0_4.a());
            if (v4_1 != 0) {
                int v0_5;
                if (v4_1 >= v0_4.b()) {
                    v0_5 = v1;
                } else {
                    p6.c(v0_4.a(), v4_1);
                    v0_5 = 1;
                }
                v1 = v0_5;
            } else {
                v3.remove();
                v1 = 1;
            }
        }
        return v1;
    }

    private static boolean g(com.a.b.d.xc p6, com.a.b.d.xc p7)
    {
        com.a.b.b.cn.a(p6);
        com.a.b.b.cn.a(p7);
        java.util.Iterator v3 = p6.a().iterator();
        int v1 = 0;
        while (v3.hasNext()) {
            int v0_4 = ((com.a.b.d.xd) v3.next());
            int v4_1 = p7.a(v0_4.a());
            if (v4_1 != 0) {
                int v0_5;
                if (v4_1 >= v0_4.b()) {
                    v0_5 = v1;
                } else {
                    p6.c(v0_4.a(), v4_1);
                    v0_5 = 1;
                }
                v1 = v0_5;
            } else {
                v3.remove();
                v1 = 1;
            }
        }
        return v1;
    }

    private static boolean h(com.a.b.d.xc p6, com.a.b.d.xc p7)
    {
        com.a.b.b.cn.a(p6);
        com.a.b.b.cn.a(p7);
        java.util.Iterator v3 = p6.a().iterator();
        int v1_1 = 0;
        while (v3.hasNext()) {
            int v0_3 = ((com.a.b.d.xd) v3.next());
            int v4_1 = p7.a(v0_3.a());
            if (v4_1 < v0_3.b()) {
                int v0_4;
                if (v4_1 <= 0) {
                    v0_4 = v1_1;
                } else {
                    p6.b(v0_3.a(), v4_1);
                    v0_4 = 1;
                }
                v1_1 = v0_4;
            } else {
                v3.remove();
                v1_1 = 1;
            }
        }
        return v1_1;
    }
}
