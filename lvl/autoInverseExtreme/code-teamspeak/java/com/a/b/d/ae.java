package com.a.b.d;
final class ae extends com.a.b.d.ac implements java.util.ListIterator {
    final synthetic com.a.b.d.ad d;

    ae(com.a.b.d.ad p1)
    {
        this.d = p1;
        this(p1);
        return;
    }

    public ae(com.a.b.d.ad p2, int p3)
    {
        this.d = p2;
        this(p2, ((java.util.List) p2.c).listIterator(p3));
        return;
    }

    private java.util.ListIterator b()
    {
        this.a();
        return ((java.util.ListIterator) this.a);
    }

    public final void add(Object p3)
    {
        com.a.b.d.ad v0_1 = this.d.isEmpty();
        this.b().add(p3);
        com.a.b.d.n.c(this.d.g);
        if (v0_1 != null) {
            this.d.c();
        }
        return;
    }

    public final boolean hasPrevious()
    {
        return this.b().hasPrevious();
    }

    public final int nextIndex()
    {
        return this.b().nextIndex();
    }

    public final Object previous()
    {
        return this.b().previous();
    }

    public final int previousIndex()
    {
        return this.b().previousIndex();
    }

    public final void set(Object p2)
    {
        this.b().set(p2);
        return;
    }
}
