package com.a.b.d;
final class zq extends com.a.b.d.me {
    private final transient com.a.b.d.jl a;

    zq(com.a.b.d.jl p2, java.util.Comparator p3)
    {
        int v0_1;
        this(p3);
        this.a = p2;
        if (p2.isEmpty()) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.a(v0_1);
        return;
    }

    private int e(Object p3)
    {
        return java.util.Collections.binarySearch(this.a, p3, this.d);
    }

    private java.util.Comparator k()
    {
        return this.d;
    }

    final int a(Object[] p2, int p3)
    {
        return this.a.a(p2, p3);
    }

    final com.a.b.d.me a(int p4, int p5)
    {
        if ((p4 != 0) || (p5 != this.size())) {
            if (p4 >= p5) {
                this = com.a.b.d.zq.a(this.d);
            } else {
                this = new com.a.b.d.zq(this.a.a(p4, p5), this.d);
            }
        }
        return this;
    }

    final com.a.b.d.me a(Object p3, boolean p4)
    {
        return this.a(this.f(p3, p4), this.size());
    }

    final com.a.b.d.me a(Object p2, boolean p3, Object p4, boolean p5)
    {
        return this.a(p2, p3).b(p4, p5);
    }

    final com.a.b.d.me b(Object p3, boolean p4)
    {
        return this.a(0, this.e(p3, p4));
    }

    final int c(Object p6)
    {
        ClassCastException v0 = -1;
        if (p6 != null) {
            try {
                ClassCastException v1_1 = com.a.b.d.aba.a(this.a, p6, this.d, com.a.b.d.abg.a, com.a.b.d.abc.c);
            } catch (ClassCastException v1) {
            }
            if (v1_1 >= null) {
                v0 = v1_1;
            }
        }
        return v0;
    }

    public final com.a.b.d.agi c()
    {
        return this.a.c();
    }

    public final Object ceiling(Object p3)
    {
        Object v0_2;
        Object v0_1 = this.f(p3, 1);
        if (v0_1 != this.size()) {
            v0_2 = this.a.get(v0_1);
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public final boolean contains(Object p4)
    {
        int v0 = 0;
        try {
            if ((p4 != null) && (java.util.Collections.binarySearch(this.a, p4, this.d) >= 0)) {
                v0 = 1;
            }
        } catch (ClassCastException v1) {
        }
        return v0;
    }

    public final boolean containsAll(java.util.Collection p7)
    {
        int v0 = 1;
        if ((p7 instanceof com.a.b.d.xc)) {
            p7 = ((com.a.b.d.xc) p7).n_();
        }
        if ((com.a.b.d.aaz.a(this.comparator(), p7)) && (p7.size() > 1)) {
            com.a.b.d.yi v3 = com.a.b.d.nj.j(this.a.c());
            java.util.Iterator v4 = p7.iterator();
            Object v2_6 = v4.next();
            try {
                while (v3.hasNext()) {
                    int v5_2 = this.c(v3.a(), v2_6);
                    if (v5_2 >= 0) {
                        if (v5_2 != 0) {
                            if (v5_2 > 0) {
                                v0 = 0;
                            }
                        } else {
                            if (v4.hasNext()) {
                                v2_6 = v4.next();
                            }
                        }
                    } else {
                        v3.next();
                    }
                }
            } catch (int v0) {
                v0 = 0;
            }
            v0 = 0;
        } else {
            v0 = super.containsAll(p7);
        }
        return v0;
    }

    public final com.a.b.d.agi d()
    {
        return this.a.e().c();
    }

    public final synthetic java.util.Iterator descendingIterator()
    {
        return this.d();
    }

    final int e(Object p6, boolean p7)
    {
        int v0_0;
        com.a.b.d.jl v1 = this.a;
        Object v2 = com.a.b.b.cn.a(p6);
        java.util.Comparator v3 = this.comparator();
        if (!p7) {
            v0_0 = com.a.b.d.abg.c;
        } else {
            v0_0 = com.a.b.d.abg.d;
        }
        return com.a.b.d.aba.a(v1, v2, v3, v0_0, com.a.b.d.abc.b);
    }

    final com.a.b.d.me e()
    {
        return new com.a.b.d.zq(this.a.e(), com.a.b.d.yd.a(this.d).a());
    }

    public final boolean equals(Object p7)
    {
        int v0 = 1;
        if (p7 != this) {
            if ((p7 instanceof java.util.Set)) {
                if (this.size() == ((java.util.Set) p7).size()) {
                    if (!com.a.b.d.aaz.a(this.d, ((java.util.Set) p7))) {
                        v0 = this.containsAll(((java.util.Set) p7));
                    } else {
                        java.util.Iterator v2_4 = ((java.util.Set) p7).iterator();
                        try {
                            com.a.b.d.agi v3_2 = this.a.c();
                        } catch (int v0) {
                            v0 = 0;
                        } catch (int v0) {
                            v0 = 0;
                        }
                        while (v3_2.hasNext()) {
                            int v4_1 = v3_2.next();
                            Object v5 = v2_4.next();
                            if ((v5 == null) || (this.c(v4_1, v5) != 0)) {
                                v0 = 0;
                                break;
                            }
                        }
                    }
                } else {
                    v0 = 0;
                }
            } else {
                v0 = 0;
            }
        }
        return v0;
    }

    final int f(Object p6, boolean p7)
    {
        int v0_0;
        com.a.b.d.jl v1 = this.a;
        Object v2 = com.a.b.b.cn.a(p6);
        java.util.Comparator v3 = this.comparator();
        if (!p7) {
            v0_0 = com.a.b.d.abg.d;
        } else {
            v0_0 = com.a.b.d.abg.c;
        }
        return com.a.b.d.aba.a(v1, v2, v3, v0_0, com.a.b.d.abc.b);
    }

    public final Object first()
    {
        return this.a.get(0);
    }

    public final Object floor(Object p3)
    {
        Object v0_3;
        Object v0_2 = (this.e(p3, 1) - 1);
        if (v0_2 != -1) {
            v0_3 = this.a.get(v0_2);
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    final boolean h_()
    {
        return this.a.h_();
    }

    public final Object higher(Object p3)
    {
        Object v0_2;
        Object v0_1 = this.f(p3, 0);
        if (v0_1 != this.size()) {
            v0_2 = this.a.get(v0_1);
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public final boolean isEmpty()
    {
        return 0;
    }

    public final synthetic java.util.Iterator iterator()
    {
        return this.a.c();
    }

    final com.a.b.d.jl l()
    {
        return new com.a.b.d.lv(this, this.a);
    }

    public final Object last()
    {
        return this.a.get((this.size() - 1));
    }

    public final Object lower(Object p3)
    {
        Object v0_3;
        Object v0_2 = (this.e(p3, 0) - 1);
        if (v0_2 != -1) {
            v0_3 = this.a.get(v0_2);
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public final int size()
    {
        return this.a.size();
    }
}
