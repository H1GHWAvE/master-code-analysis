package com.a.b.d;
abstract enum class aew extends java.lang.Enum {
    public static final enum com.a.b.d.aew a;
    public static final enum com.a.b.d.aew b;
    private static final synthetic com.a.b.d.aew[] c;

    static aew()
    {
        com.a.b.d.aew.a = new com.a.b.d.aex("SIZE");
        com.a.b.d.aew.b = new com.a.b.d.aey("DISTINCT");
        com.a.b.d.aew[] v0_5 = new com.a.b.d.aew[2];
        v0_5[0] = com.a.b.d.aew.a;
        v0_5[1] = com.a.b.d.aew.b;
        com.a.b.d.aew.c = v0_5;
        return;
    }

    private aew(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    synthetic aew(String p1, int p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    public static com.a.b.d.aew valueOf(String p1)
    {
        return ((com.a.b.d.aew) Enum.valueOf(com.a.b.d.aew, p1));
    }

    public static com.a.b.d.aew[] values()
    {
        return ((com.a.b.d.aew[]) com.a.b.d.aew.c.clone());
    }

    abstract int a();

    abstract long b();
}
