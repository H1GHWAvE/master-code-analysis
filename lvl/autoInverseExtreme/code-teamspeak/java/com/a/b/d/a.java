package com.a.b.d;
abstract class a extends com.a.b.d.gs implements com.a.b.d.bw, java.io.Serializable {
    private static final long f;
    transient com.a.b.d.a a;
    private transient java.util.Map b;
    private transient java.util.Set c;
    private transient java.util.Set d;
    private transient java.util.Set e;

    private a(java.util.Map p1, com.a.b.d.a p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    synthetic a(java.util.Map p1, com.a.b.d.a p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    a(java.util.Map p1, java.util.Map p2)
    {
        this.a(p1, p2);
        return;
    }

    static synthetic Object a(com.a.b.d.a p1, Object p2)
    {
        return p1.d(p2);
    }

    private Object a(Object p6, Object p7, boolean p8)
    {
        this.a(p6);
        this.b(p7);
        boolean v3 = this.containsKey(p6);
        if ((!v3) || (!com.a.b.b.ce.a(p7, this.get(p6)))) {
            if (!p8) {
                int v0_3;
                if (this.containsValue(p7)) {
                    v0_3 = 0;
                } else {
                    v0_3 = 1;
                }
                Object[] v1_1 = new Object[1];
                v1_1[0] = p7;
                com.a.b.b.cn.a(v0_3, "value already present: %s", v1_1);
            } else {
                this.b().remove(p7);
            }
            int v0_6 = this.b.put(p6, p7);
            this.a(p6, v3, v0_6, p7);
            p7 = v0_6;
        }
        return p7;
    }

    static synthetic java.util.Map a(com.a.b.d.a p1)
    {
        return p1.b;
    }

    static synthetic void a(com.a.b.d.a p1, Object p2, Object p3, Object p4)
    {
        p1.a(p2, 1, p3, p4);
        return;
    }

    private void a(Object p2, boolean p3, Object p4, Object p5)
    {
        if (p3) {
            this.e(p4);
        }
        this.a.b.put(p5, p2);
        return;
    }

    private void b(com.a.b.d.a p1)
    {
        this.a = p1;
        return;
    }

    static synthetic void b(com.a.b.d.a p0, Object p1)
    {
        p0.e(p1);
        return;
    }

    private Object d(Object p2)
    {
        Object v0_1 = this.b.remove(p2);
        this.e(v0_1);
        return v0_1;
    }

    private void e(Object p2)
    {
        this.a.b.remove(p2);
        return;
    }

    Object a(Object p1)
    {
        return p1;
    }

    public Object a(Object p2, Object p3)
    {
        return this.a(p2, p3, 1);
    }

    protected final java.util.Map a()
    {
        return this.b;
    }

    final void a(java.util.Map p4, java.util.Map p5)
    {
        com.a.b.d.f v0_1;
        int v1 = 1;
        if (this.b != null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.d.f v0_3;
        com.a.b.b.cn.b(v0_1);
        if (this.a != null) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        com.a.b.b.cn.b(v0_3);
        com.a.b.b.cn.a(p4.isEmpty());
        com.a.b.b.cn.a(p5.isEmpty());
        if (p4 == p5) {
            v1 = 0;
        }
        com.a.b.b.cn.a(v1);
        this.b = p4;
        this.a = new com.a.b.d.f(p5, this, 0);
        return;
    }

    public com.a.b.d.bw b()
    {
        return this.a;
    }

    Object b(Object p1)
    {
        return p1;
    }

    public void clear()
    {
        this.b.clear();
        this.a.b.clear();
        return;
    }

    public boolean containsValue(Object p2)
    {
        return this.a.containsKey(p2);
    }

    public java.util.Set entrySet()
    {
        com.a.b.d.c v0_0 = this.e;
        if (v0_0 == null) {
            v0_0 = new com.a.b.d.c(this, 0);
            this.e = v0_0;
        }
        return v0_0;
    }

    public java.util.Set j_()
    {
        com.a.b.d.h v0_0 = this.d;
        if (v0_0 == null) {
            v0_0 = new com.a.b.d.h(this, 0);
            this.d = v0_0;
        }
        return v0_0;
    }

    protected bridge synthetic Object k_()
    {
        return this.b;
    }

    public java.util.Set keySet()
    {
        com.a.b.d.g v0_0 = this.c;
        if (v0_0 == null) {
            v0_0 = new com.a.b.d.g(this, 0);
            this.c = v0_0;
        }
        return v0_0;
    }

    public Object put(Object p2, Object p3)
    {
        return this.a(p2, p3, 0);
    }

    public void putAll(java.util.Map p4)
    {
        java.util.Iterator v1 = p4.entrySet().iterator();
        while (v1.hasNext()) {
            Object v0_3 = ((java.util.Map$Entry) v1.next());
            this.put(v0_3.getKey(), v0_3.getValue());
        }
        return;
    }

    public Object remove(Object p2)
    {
        int v0_1;
        if (!this.containsKey(p2)) {
            v0_1 = 0;
        } else {
            v0_1 = this.d(p2);
        }
        return v0_1;
    }

    public synthetic java.util.Collection values()
    {
        return this.j_();
    }
}
