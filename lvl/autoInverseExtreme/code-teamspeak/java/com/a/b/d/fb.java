package com.a.b.d;
final class fb extends com.a.b.d.ma {
    private final com.a.b.d.me b;

    fb(java.util.Comparator p2)
    {
        this.b = com.a.b.d.me.a(p2);
        return;
    }

    public final int a(Object p2)
    {
        return 0;
    }

    final int a(Object[] p1, int p2)
    {
        return p2;
    }

    public final com.a.b.d.ma a(Object p1, com.a.b.d.ce p2)
    {
        com.a.b.b.cn.a(p1);
        com.a.b.b.cn.a(p2);
        return this;
    }

    final com.a.b.d.xd a(int p3)
    {
        throw new AssertionError("should never be called");
    }

    public final com.a.b.d.ma b(Object p1, com.a.b.d.ce p2)
    {
        com.a.b.b.cn.a(p1);
        com.a.b.b.cn.a(p2);
        return this;
    }

    public final com.a.b.d.me b()
    {
        return this.b;
    }

    public final synthetic com.a.b.d.abn c(Object p2, com.a.b.d.ce p3)
    {
        return this.b(p2, p3);
    }

    public final com.a.b.d.agi c()
    {
        return com.a.b.d.nj.a();
    }

    public final boolean containsAll(java.util.Collection p2)
    {
        return p2.isEmpty();
    }

    public final synthetic com.a.b.d.abn d(Object p2, com.a.b.d.ce p3)
    {
        return this.a(p2, p3);
    }

    public final bridge synthetic java.util.NavigableSet e_()
    {
        return this.b;
    }

    public final boolean equals(Object p2)
    {
        int v0_1;
        if (!(p2 instanceof com.a.b.d.xc)) {
            v0_1 = 0;
        } else {
            v0_1 = ((com.a.b.d.xc) p2).isEmpty();
        }
        return v0_1;
    }

    public final com.a.b.d.jl f()
    {
        return com.a.b.d.jl.d();
    }

    public final com.a.b.d.xd h()
    {
        return 0;
    }

    final boolean h_()
    {
        return 0;
    }

    public final com.a.b.d.xd i()
    {
        return 0;
    }

    public final synthetic java.util.Iterator iterator()
    {
        return com.a.b.d.nj.a();
    }

    public final bridge synthetic java.util.SortedSet n()
    {
        return this.b;
    }

    public final bridge synthetic java.util.Set n_()
    {
        return this.b;
    }

    public final int size()
    {
        return 0;
    }
}
