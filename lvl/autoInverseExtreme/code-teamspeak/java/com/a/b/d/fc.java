package com.a.b.d;
final class fc extends com.a.b.d.me {

    fc(java.util.Comparator p1)
    {
        this(p1);
        return;
    }

    final int a(Object[] p1, int p2)
    {
        return p2;
    }

    final com.a.b.d.me a(Object p1, boolean p2)
    {
        return this;
    }

    final com.a.b.d.me a(Object p1, boolean p2, Object p3, boolean p4)
    {
        return this;
    }

    final com.a.b.d.me b(Object p1, boolean p2)
    {
        return this;
    }

    final int c(Object p2)
    {
        return -1;
    }

    public final com.a.b.d.agi c()
    {
        return com.a.b.d.nj.a();
    }

    public final boolean contains(Object p2)
    {
        return 0;
    }

    public final boolean containsAll(java.util.Collection p2)
    {
        return p2.isEmpty();
    }

    public final com.a.b.d.agi d()
    {
        return com.a.b.d.nj.a();
    }

    public final synthetic java.util.Iterator descendingIterator()
    {
        return com.a.b.d.nj.a();
    }

    final com.a.b.d.me e()
    {
        return new com.a.b.d.fc(com.a.b.d.yd.a(this.d).a());
    }

    public final boolean equals(Object p2)
    {
        int v0_1;
        if (!(p2 instanceof java.util.Set)) {
            v0_1 = 0;
        } else {
            v0_1 = ((java.util.Set) p2).isEmpty();
        }
        return v0_1;
    }

    public final com.a.b.d.jl f()
    {
        return com.a.b.d.jl.d();
    }

    public final Object first()
    {
        throw new java.util.NoSuchElementException();
    }

    final boolean h_()
    {
        return 0;
    }

    public final int hashCode()
    {
        return 0;
    }

    public final boolean isEmpty()
    {
        return 1;
    }

    public final synthetic java.util.Iterator iterator()
    {
        return com.a.b.d.nj.a();
    }

    public final Object last()
    {
        throw new java.util.NoSuchElementException();
    }

    public final int size()
    {
        return 0;
    }

    public final String toString()
    {
        return "[]";
    }
}
