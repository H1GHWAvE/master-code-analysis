package com.a.b.d;
final class x extends com.a.b.d.aa implements java.util.NavigableSet {
    final synthetic com.a.b.d.n b;

    x(com.a.b.d.n p1, java.util.NavigableMap p2)
    {
        this.b = p1;
        this(p1, p2);
        return;
    }

    private java.util.NavigableSet a(Object p2)
    {
        return this.headSet(p2, 0);
    }

    private java.util.NavigableSet a(Object p3, Object p4)
    {
        return this.subSet(p3, 1, p4, 0);
    }

    private java.util.NavigableSet b(Object p2)
    {
        return this.tailSet(p2, 1);
    }

    private java.util.NavigableMap c()
    {
        return ((java.util.NavigableMap) super.a());
    }

    final bridge synthetic java.util.SortedMap a()
    {
        return ((java.util.NavigableMap) super.a());
    }

    public final Object ceiling(Object p2)
    {
        return ((java.util.NavigableMap) super.a()).ceilingKey(p2);
    }

    public final java.util.Iterator descendingIterator()
    {
        return this.descendingSet().iterator();
    }

    public final java.util.NavigableSet descendingSet()
    {
        return new com.a.b.d.x(this.b, ((java.util.NavigableMap) super.a()).descendingMap());
    }

    public final Object floor(Object p2)
    {
        return ((java.util.NavigableMap) super.a()).floorKey(p2);
    }

    public final java.util.NavigableSet headSet(Object p4, boolean p5)
    {
        return new com.a.b.d.x(this.b, ((java.util.NavigableMap) super.a()).headMap(p4, p5));
    }

    public final bridge synthetic java.util.SortedSet headSet(Object p2)
    {
        return this.headSet(p2, 0);
    }

    public final Object higher(Object p2)
    {
        return ((java.util.NavigableMap) super.a()).higherKey(p2);
    }

    public final Object lower(Object p2)
    {
        return ((java.util.NavigableMap) super.a()).lowerKey(p2);
    }

    public final Object pollFirst()
    {
        return com.a.b.d.nj.h(this.iterator());
    }

    public final Object pollLast()
    {
        return com.a.b.d.nj.h(this.descendingIterator());
    }

    public final java.util.NavigableSet subSet(Object p4, boolean p5, Object p6, boolean p7)
    {
        return new com.a.b.d.x(this.b, ((java.util.NavigableMap) super.a()).subMap(p4, p5, p6, p7));
    }

    public final bridge synthetic java.util.SortedSet subSet(Object p3, Object p4)
    {
        return this.subSet(p3, 1, p4, 0);
    }

    public final java.util.NavigableSet tailSet(Object p4, boolean p5)
    {
        return new com.a.b.d.x(this.b, ((java.util.NavigableMap) super.a()).tailMap(p4, p5));
    }

    public final bridge synthetic java.util.SortedSet tailSet(Object p2)
    {
        return this.tailSet(p2, 1);
    }
}
