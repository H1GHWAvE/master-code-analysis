package com.a.b.d;
public final class oi extends com.a.b.d.ai {
    private static final long b;

    oi()
    {
        this(new java.util.LinkedHashMap());
        return;
    }

    private oi(int p3)
    {
        this(new java.util.LinkedHashMap(com.a.b.d.sz.b(p3)));
        return;
    }

    public static com.a.b.d.oi a(int p1)
    {
        return new com.a.b.d.oi(p1);
    }

    private static com.a.b.d.oi a(Iterable p1)
    {
        com.a.b.d.oi v0_1 = com.a.b.d.oi.a(com.a.b.d.xe.a(p1));
        com.a.b.d.mq.a(v0_1, p1);
        return v0_1;
    }

    private void a(java.io.ObjectInputStream p4)
    {
        p4.defaultReadObject();
        int v0 = p4.readInt();
        this.a = new java.util.LinkedHashMap(com.a.b.d.sz.b(v0));
        com.a.b.d.zz.a(this, p4, v0);
        return;
    }

    private void a(java.io.ObjectOutputStream p1)
    {
        p1.defaultWriteObject();
        com.a.b.d.zz.a(this, p1);
        return;
    }

    public static com.a.b.d.oi g()
    {
        return new com.a.b.d.oi();
    }

    public final bridge synthetic int a(Object p2)
    {
        return super.a(p2);
    }

    public final bridge synthetic int a(Object p2, int p3)
    {
        return super.a(p2, p3);
    }

    public final bridge synthetic java.util.Set a()
    {
        return super.a();
    }

    public final bridge synthetic boolean a(Object p2, int p3, int p4)
    {
        return super.a(p2, p3, p4);
    }

    public final bridge synthetic boolean add(Object p2)
    {
        return super.add(p2);
    }

    public final bridge synthetic boolean addAll(java.util.Collection p2)
    {
        return super.addAll(p2);
    }

    public final bridge synthetic int b(Object p2, int p3)
    {
        return super.b(p2, p3);
    }

    public final bridge synthetic int c(Object p2, int p3)
    {
        return super.c(p2, p3);
    }

    public final bridge synthetic void clear()
    {
        super.clear();
        return;
    }

    public final bridge synthetic boolean contains(Object p2)
    {
        return super.contains(p2);
    }

    public final bridge synthetic boolean equals(Object p2)
    {
        return super.equals(p2);
    }

    public final bridge synthetic int hashCode()
    {
        return super.hashCode();
    }

    public final bridge synthetic boolean isEmpty()
    {
        return super.isEmpty();
    }

    public final bridge synthetic java.util.Iterator iterator()
    {
        return super.iterator();
    }

    public final bridge synthetic java.util.Set n_()
    {
        return super.n_();
    }

    public final bridge synthetic boolean remove(Object p2)
    {
        return super.remove(p2);
    }

    public final bridge synthetic boolean removeAll(java.util.Collection p2)
    {
        return super.removeAll(p2);
    }

    public final bridge synthetic boolean retainAll(java.util.Collection p2)
    {
        return super.retainAll(p2);
    }

    public final bridge synthetic int size()
    {
        return super.size();
    }

    public final bridge synthetic String toString()
    {
        return super.toString();
    }
}
