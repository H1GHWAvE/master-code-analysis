package com.a.b.d;
 class abr extends com.a.b.d.abq implements java.util.NavigableSet {

    abr(com.a.b.d.abn p1)
    {
        this(p1);
        return;
    }

    public Object ceiling(Object p3)
    {
        return com.a.b.d.abp.b(this.b.c(p3, com.a.b.d.ce.b).h());
    }

    public java.util.Iterator descendingIterator()
    {
        return this.descendingSet().iterator();
    }

    public java.util.NavigableSet descendingSet()
    {
        return new com.a.b.d.abr(this.b.m());
    }

    public Object floor(Object p3)
    {
        return com.a.b.d.abp.b(this.b.d(p3, com.a.b.d.ce.b).i());
    }

    public java.util.NavigableSet headSet(Object p4, boolean p5)
    {
        return new com.a.b.d.abr(this.b.d(p4, com.a.b.d.ce.a(p5)));
    }

    public Object higher(Object p3)
    {
        return com.a.b.d.abp.b(this.b.c(p3, com.a.b.d.ce.a).h());
    }

    public Object lower(Object p3)
    {
        return com.a.b.d.abp.b(this.b.d(p3, com.a.b.d.ce.a).i());
    }

    public Object pollFirst()
    {
        return com.a.b.d.abp.b(this.b.j());
    }

    public Object pollLast()
    {
        return com.a.b.d.abp.b(this.b.k());
    }

    public java.util.NavigableSet subSet(Object p5, boolean p6, Object p7, boolean p8)
    {
        return new com.a.b.d.abr(this.b.a(p5, com.a.b.d.ce.a(p6), p7, com.a.b.d.ce.a(p8)));
    }

    public java.util.NavigableSet tailSet(Object p4, boolean p5)
    {
        return new com.a.b.d.abr(this.b.c(p4, com.a.b.d.ce.a(p5)));
    }
}
