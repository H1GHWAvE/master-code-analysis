package com.a.b.d;
 class wn extends com.a.b.d.as {
    final com.a.b.d.vi b;

    wn(com.a.b.d.vi p1)
    {
        this.b = p1;
        return;
    }

    public final int a(Object p2)
    {
        int v0_4;
        int v0_3 = ((java.util.Collection) com.a.b.d.sz.a(this.b.b(), p2));
        if (v0_3 != 0) {
            v0_4 = v0_3.size();
        } else {
            v0_4 = 0;
        }
        return v0_4;
    }

    public int b(Object p4, int p5)
    {
        int v0_6;
        int v1 = 0;
        com.a.b.d.cl.a(p5, "occurrences");
        if (p5 != 0) {
            int v0_4 = ((java.util.Collection) com.a.b.d.sz.a(this.b.b(), p4));
            if (v0_4 != 0) {
                int v2 = v0_4.size();
                if (p5 < v2) {
                    int v0_5 = v0_4.iterator();
                    while (v1 < p5) {
                        v0_5.next();
                        v0_5.remove();
                        v1++;
                    }
                } else {
                    v0_4.clear();
                }
                v0_6 = v2;
            } else {
                v0_6 = 0;
            }
        } else {
            v0_6 = this.a(p4);
        }
        return v0_6;
    }

    final java.util.Iterator b()
    {
        return new com.a.b.d.wo(this, this.b.b().entrySet().iterator());
    }

    final int c()
    {
        return this.b.b().size();
    }

    public void clear()
    {
        this.b.g();
        return;
    }

    public boolean contains(Object p2)
    {
        return this.b.f(p2);
    }

    final java.util.Set f()
    {
        return new com.a.b.d.wq(this);
    }

    public java.util.Iterator iterator()
    {
        return com.a.b.d.sz.a(this.b.k().iterator());
    }

    public final java.util.Set n_()
    {
        return this.b.p();
    }
}
