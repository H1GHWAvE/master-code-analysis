package com.a.b.d;
final class zx extends com.a.b.d.yd implements java.io.Serializable {
    private static final long b;
    final com.a.b.d.yd a;

    zx(com.a.b.d.yd p2)
    {
        this.a = ((com.a.b.d.yd) com.a.b.b.cn.a(p2));
        return;
    }

    public final com.a.b.d.yd a()
    {
        return this.a;
    }

    public final Object a(Object p2, Object p3)
    {
        return this.a.b(p2, p3);
    }

    public final varargs Object a(Object p2, Object p3, Object p4, Object[] p5)
    {
        return this.a.b(p2, p3, p4, p5);
    }

    public final Object a(java.util.Iterator p2)
    {
        return this.a.b(p2);
    }

    public final Object b(Object p2, Object p3)
    {
        return this.a.a(p2, p3);
    }

    public final varargs Object b(Object p2, Object p3, Object p4, Object[] p5)
    {
        return this.a.a(p2, p3, p4, p5);
    }

    public final Object b(java.util.Iterator p2)
    {
        return this.a.a(p2);
    }

    public final Object c(Iterable p2)
    {
        return this.a.d(p2);
    }

    public final int compare(Object p2, Object p3)
    {
        return this.a.compare(p3, p2);
    }

    public final Object d(Iterable p2)
    {
        return this.a.c(p2);
    }

    public final boolean equals(Object p3)
    {
        int v0_1;
        if (p3 != this) {
            if (!(p3 instanceof com.a.b.d.zx)) {
                v0_1 = 0;
            } else {
                v0_1 = this.a.equals(((com.a.b.d.zx) p3).a);
            }
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final int hashCode()
    {
        return (- this.a.hashCode());
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        return new StringBuilder((v0_2.length() + 10)).append(v0_2).append(".reverse()").toString();
    }
}
