package com.a.b.d;
 class dp extends com.a.b.d.gp {
    final java.util.List a;
    final com.a.b.d.dm b;

    dp(java.util.List p2, com.a.b.d.dm p3)
    {
        this.a = ((java.util.List) com.a.b.b.cn.a(p2));
        this.b = ((com.a.b.d.dm) com.a.b.b.cn.a(p3));
        return;
    }

    protected final java.util.List a()
    {
        return this.a;
    }

    public void add(int p2, Object p3)
    {
        this.b.a(p3);
        this.a.add(p2, p3);
        return;
    }

    public boolean add(Object p2)
    {
        this.b.a(p2);
        return this.a.add(p2);
    }

    public boolean addAll(int p3, java.util.Collection p4)
    {
        return this.a.addAll(p3, com.a.b.d.dn.b(p4, this.b));
    }

    public boolean addAll(java.util.Collection p3)
    {
        return this.a.addAll(com.a.b.d.dn.b(p3, this.b));
    }

    protected final bridge synthetic java.util.Collection b()
    {
        return this.a;
    }

    protected final bridge synthetic Object k_()
    {
        return this.a;
    }

    public java.util.ListIterator listIterator()
    {
        return com.a.b.d.dn.a(this.a.listIterator(), this.b);
    }

    public java.util.ListIterator listIterator(int p3)
    {
        return com.a.b.d.dn.a(this.a.listIterator(p3), this.b);
    }

    public Object set(int p2, Object p3)
    {
        this.b.a(p3);
        return this.a.set(p2, p3);
    }

    public java.util.List subList(int p3, int p4)
    {
        return com.a.b.d.dn.a(this.a.subList(p3, p4), this.b);
    }
}
