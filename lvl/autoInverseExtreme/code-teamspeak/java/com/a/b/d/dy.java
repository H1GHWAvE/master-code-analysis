package com.a.b.d;
final class dy extends com.a.b.d.dw {
    private static final com.a.b.d.dy b;
    private static final long c;

    static dy()
    {
        com.a.b.d.dy.b = new com.a.b.d.dy();
        return;
    }

    private dy()
    {
        this(0);
        return;
    }

    static synthetic com.a.b.d.dy f()
    {
        return com.a.b.d.dy.b;
    }

    private static Object g()
    {
        return com.a.b.d.dy.b;
    }

    public final int a(com.a.b.d.dw p2)
    {
        int v0;
        if (p2 != this) {
            v0 = 1;
        } else {
            v0 = 0;
        }
        return v0;
    }

    final com.a.b.d.ce a()
    {
        throw new AssertionError("this statement should be unreachable");
    }

    final com.a.b.d.dw a(com.a.b.d.ce p3, com.a.b.d.ep p4)
    {
        throw new AssertionError("this statement should be unreachable");
    }

    final Comparable a(com.a.b.d.ep p2)
    {
        throw new AssertionError();
    }

    final void a(StringBuilder p2)
    {
        throw new AssertionError();
    }

    final boolean a(Comparable p2)
    {
        return 0;
    }

    final com.a.b.d.ce b()
    {
        throw new IllegalStateException();
    }

    final com.a.b.d.dw b(com.a.b.d.ce p2, com.a.b.d.ep p3)
    {
        throw new IllegalStateException();
    }

    final Comparable b(com.a.b.d.ep p2)
    {
        return p2.b();
    }

    final void b(StringBuilder p2)
    {
        p2.append("+\u221e)");
        return;
    }

    final Comparable c()
    {
        throw new IllegalStateException("range unbounded on this side");
    }

    public final synthetic int compareTo(Object p2)
    {
        return this.a(((com.a.b.d.dw) p2));
    }

    public final String toString()
    {
        return "+\u221e";
    }
}
