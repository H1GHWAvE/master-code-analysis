package com.a.b.d;
final class acw extends com.a.b.d.adi {
    private static final long f;
    transient java.util.Set a;
    transient java.util.Collection b;

    acw(java.util.Map p1, Object p2)
    {
        this(p1, p2);
        return;
    }

    private java.util.Collection a(Object p4)
    {
        try {
            java.util.Collection v0_2;
            java.util.Collection v0_1 = ((java.util.Collection) super.get(p4));
        } catch (java.util.Collection v0_3) {
            throw v0_3;
        }
        if (v0_1 != null) {
            v0_2 = com.a.b.d.acu.b(v0_1, this.h);
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public final boolean containsValue(Object p2)
    {
        return this.values().contains(p2);
    }

    public final java.util.Set entrySet()
    {
        try {
            if (this.a == null) {
                this.a = new com.a.b.d.acx(this.a().entrySet(), this.h);
            }
        } catch (java.util.Set v0_4) {
            throw v0_4;
        }
        return this.a;
    }

    public final synthetic Object get(Object p2)
    {
        return this.a(p2);
    }

    public final java.util.Collection values()
    {
        try {
            if (this.b == null) {
                this.b = new com.a.b.d.ada(this.a().values(), this.h);
            }
        } catch (java.util.Collection v0_4) {
            throw v0_4;
        }
        return this.b;
    }
}
