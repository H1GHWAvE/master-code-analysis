package com.a.b.d;
 class adg extends com.a.b.d.add implements java.util.List {
    private static final long a;

    adg(java.util.List p2, Object p3)
    {
        this(p2, p3, 0);
        return;
    }

    private java.util.List a()
    {
        return ((java.util.List) super.b());
    }

    public void add(int p3, Object p4)
    {
        try {
            ((java.util.List) super.b()).add(p3, p4);
            return;
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public boolean addAll(int p3, java.util.Collection p4)
    {
        try {
            return ((java.util.List) super.b()).addAll(p3, p4);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    final bridge synthetic java.util.Collection b()
    {
        return ((java.util.List) super.b());
    }

    final synthetic Object d()
    {
        return ((java.util.List) super.b());
    }

    public boolean equals(Object p3)
    {
        Throwable v0_2;
        if (p3 != this) {
            try {
                v0_2 = ((java.util.List) super.b()).equals(p3);
            } catch (Throwable v0_3) {
                throw v0_3;
            }
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public Object get(int p3)
    {
        try {
            return ((java.util.List) super.b()).get(p3);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public int hashCode()
    {
        try {
            return ((java.util.List) super.b()).hashCode();
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public int indexOf(Object p3)
    {
        try {
            return ((java.util.List) super.b()).indexOf(p3);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public int lastIndexOf(Object p3)
    {
        try {
            return ((java.util.List) super.b()).lastIndexOf(p3);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public java.util.ListIterator listIterator()
    {
        return ((java.util.List) super.b()).listIterator();
    }

    public java.util.ListIterator listIterator(int p2)
    {
        return ((java.util.List) super.b()).listIterator(p2);
    }

    public Object remove(int p3)
    {
        try {
            return ((java.util.List) super.b()).remove(p3);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public Object set(int p3, Object p4)
    {
        try {
            return ((java.util.List) super.b()).set(p3, p4);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public java.util.List subList(int p4, int p5)
    {
        try {
            return com.a.b.d.acu.a(((java.util.List) super.b()).subList(p4, p5), this.h);
        } catch (Throwable v0_4) {
            throw v0_4;
        }
    }
}
