package com.a.b.d;
abstract class bo extends com.a.b.d.uj {
    final com.a.b.d.jt a;

    private bo(com.a.b.d.jt p1)
    {
        this.a = p1;
        return;
    }

    synthetic bo(com.a.b.d.jt p1, byte p2)
    {
        this(p1);
        return;
    }

    private Object b(int p2)
    {
        return this.a.g().f().get(p2);
    }

    abstract Object a();

    abstract Object a();

    protected final java.util.Set a()
    {
        return new com.a.b.d.bp(this);
    }

    abstract String b();

    public void clear()
    {
        throw new UnsupportedOperationException();
    }

    public boolean containsKey(Object p2)
    {
        return this.a.containsKey(p2);
    }

    public Object get(Object p2)
    {
        Object v0_4;
        Object v0_2 = ((Integer) this.a.get(p2));
        if (v0_2 != null) {
            v0_4 = this.a(v0_2.intValue());
        } else {
            v0_4 = 0;
        }
        return v0_4;
    }

    public boolean isEmpty()
    {
        return this.a.isEmpty();
    }

    public java.util.Set keySet()
    {
        return this.a.g();
    }

    public Object put(Object p8, Object p9)
    {
        Object v0_2 = ((Integer) this.a.get(p8));
        if (v0_2 != null) {
            return this.a(v0_2.intValue(), p9);
        } else {
            String v1_2 = String.valueOf(String.valueOf(this.b()));
            String v2_1 = String.valueOf(String.valueOf(p8));
            String v3_3 = String.valueOf(String.valueOf(this.a.g()));
            throw new IllegalArgumentException(new StringBuilder((((v1_2.length() + 9) + v2_1.length()) + v3_3.length())).append(v1_2).append(" ").append(v2_1).append(" not in ").append(v3_3).toString());
        }
    }

    public Object remove(Object p2)
    {
        throw new UnsupportedOperationException();
    }

    public int size()
    {
        return this.a.size();
    }
}
