package com.a.b.d;
public final class le {
    private final com.a.b.d.yr a;
    private final com.a.b.d.yq b;

    public le()
    {
        this.a = com.a.b.d.afm.c();
        this.b = com.a.b.d.afb.c();
        return;
    }

    private com.a.b.d.lb a()
    {
        com.a.b.d.lb v0_1 = this.b.d();
        com.a.b.d.jl v1_1 = new com.a.b.d.jn(v0_1.size());
        com.a.b.d.jl v2_2 = new com.a.b.d.jn(v0_1.size());
        java.util.Iterator v3_1 = v0_1.entrySet().iterator();
        while (v3_1.hasNext()) {
            com.a.b.d.lb v0_7 = ((java.util.Map$Entry) v3_1.next());
            v1_1.c(v0_7.getKey());
            v2_2.c(v0_7.getValue());
        }
        return new com.a.b.d.lb(v1_1.b(), v2_2.b());
    }

    private com.a.b.d.le a(com.a.b.d.yl p7, Object p8)
    {
        String v0_1;
        com.a.b.b.cn.a(p7);
        com.a.b.b.cn.a(p8);
        if (p7.f()) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        IllegalArgumentException v1_1 = new Object[1];
        v1_1[0] = p7;
        com.a.b.b.cn.a(v0_1, "Range must not be empty, but was %s", v1_1);
        if (!this.a.f().c(p7)) {
            StringBuilder v2_1 = this.b.d().entrySet().iterator();
            while (v2_1.hasNext()) {
                String v0_12 = ((java.util.Map$Entry) v2_1.next());
                IllegalArgumentException v1_3 = ((com.a.b.d.yl) v0_12.getKey());
                if ((v1_3.b(p7)) && (!v1_3.c(p7).f())) {
                    StringBuilder v2_3 = String.valueOf(String.valueOf(p7));
                    String v0_14 = String.valueOf(String.valueOf(v0_12));
                    throw new IllegalArgumentException(new StringBuilder(((v2_3.length() + 47) + v0_14.length())).append("Overlapping ranges: range ").append(v2_3).append(" overlaps with entry ").append(v0_14).toString());
                }
            }
        }
        this.a.a(p7);
        this.b.a(p7, p8);
        return this;
    }

    private com.a.b.d.le a(com.a.b.d.yq p10)
    {
        int v5_0 = p10.d().entrySet().iterator();
        while (v5_0.hasNext()) {
            String v0_6;
            String v0_4 = ((java.util.Map$Entry) v5_0.next());
            StringBuilder v1_1 = ((com.a.b.d.yl) v0_4.getKey());
            Object v6 = v0_4.getValue();
            com.a.b.b.cn.a(v1_1);
            com.a.b.b.cn.a(v6);
            if (v1_1.f()) {
                v0_6 = 0;
            } else {
                v0_6 = 1;
            }
            java.util.Iterator v7_0 = new Object[1];
            v7_0[0] = v1_1;
            com.a.b.b.cn.a(v0_6, "Range must not be empty, but was %s", v7_0);
            if (!this.a.f().c(v1_1)) {
                java.util.Iterator v7_1 = this.b.d().entrySet().iterator();
                while (v7_1.hasNext()) {
                    String v0_17 = ((java.util.Map$Entry) v7_1.next());
                    IllegalArgumentException v2_2 = ((com.a.b.d.yl) v0_17.getKey());
                    if ((v2_2.b(v1_1)) && (!v2_2.c(v1_1).f())) {
                        StringBuilder v1_3 = String.valueOf(String.valueOf(v1_1));
                        String v0_19 = String.valueOf(String.valueOf(v0_17));
                        throw new IllegalArgumentException(new StringBuilder(((v1_3.length() + 47) + v0_19.length())).append("Overlapping ranges: range ").append(v1_3).append(" overlaps with entry ").append(v0_19).toString());
                    }
                }
            }
            this.a.a(v1_1);
            this.b.a(v1_1, v6);
        }
        return this;
    }
}
