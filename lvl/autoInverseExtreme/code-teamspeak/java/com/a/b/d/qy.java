package com.a.b.d;
 class qy extends java.util.AbstractMap implements java.io.Serializable, java.util.concurrent.ConcurrentMap {
    private static final long a = 5;
    static final int b = 1073741824;
    static final int c = 65536;
    static final int d = 3;
    static final int e = 63;
    static final int f = 16;
    static final long g = 60;
    static final java.util.logging.Logger h;
    static final com.a.b.d.sr x;
    static final java.util.Queue y;
    transient java.util.Collection A;
    transient java.util.Set B;
    final transient int i;
    final transient int j;
    final transient com.a.b.d.sa[] k;
    final int l;
    final com.a.b.b.au m;
    final com.a.b.b.au n;
    final com.a.b.d.sh o;
    final com.a.b.d.sh p;
    final int q;
    final long r;
    final long s;
    final java.util.Queue t;
    final com.a.b.d.qw u;
    final transient com.a.b.d.re v;
    final com.a.b.b.ej w;
    transient java.util.Set z;

    static qy()
    {
        com.a.b.d.qy.h = java.util.logging.Logger.getLogger(com.a.b.d.qy.getName());
        com.a.b.d.qy.x = new com.a.b.d.qz();
        com.a.b.d.qy.y = new com.a.b.d.ra();
        return;
    }

    qy(com.a.b.d.ql p9)
    {
        int v0_14;
        com.a.b.d.sa[] v2_0 = 0;
        int v4 = 1;
        int v5 = 0;
        this.l = Math.min(p9.h(), 65536);
        this.o = p9.i();
        this.p = ((com.a.b.d.sh) com.a.b.b.ca.a(p9.h, com.a.b.d.sh.a));
        this.m = ((com.a.b.b.au) com.a.b.b.ca.a(p9.l, p9.i().a()));
        this.n = this.p.a();
        this.q = p9.f;
        if (p9.j != -1) {
            v0_14 = p9.j;
        } else {
            v0_14 = 0;
        }
        this.r = v0_14;
        if (p9.i != -1) {
            v2_0 = p9.i;
        }
        int v0_25;
        this.s = v2_0;
        this.v = com.a.b.d.re.a(this.o, this.c(), this.b());
        this.w = ((com.a.b.b.ej) com.a.b.b.ca.a(p9.m, com.a.b.b.ej.b()));
        this.u = p9.d();
        if (this.u != com.a.b.d.hu.a) {
            v0_25 = new java.util.concurrent.ConcurrentLinkedQueue();
        } else {
            v0_25 = com.a.b.d.qy.y;
        }
        this.t = v0_25;
        int v0_27 = Math.min(p9.g(), 1073741824);
        if (this.b()) {
            v0_27 = Math.min(v0_27, this.q);
        }
        com.a.b.d.sa v1_10 = 1;
        com.a.b.d.sa[] v2_2 = 0;
        while ((v1_10 < this.l) && ((!this.b()) || ((v1_10 * 2) <= this.q))) {
            v2_2++;
            v1_10 <<= 1;
        }
        int v0_28;
        this.j = (32 - v2_2);
        this.i = (v1_10 - 1);
        com.a.b.d.sa[] v2_5 = new com.a.b.d.sa[v1_10];
        this.k = v2_5;
        com.a.b.d.sa[] v2_6 = (v0_27 / v1_10);
        if ((v2_6 * v1_10) >= v0_27) {
            v0_28 = v2_6;
        } else {
            v0_28 = (v2_6 + 1);
        }
        while (v4 < v0_28) {
            v4 <<= 1;
        }
        if (!this.b()) {
            while (v5 < this.k.length) {
                this.k[v5] = this.a(v4, -1);
                v5++;
            }
        } else {
            int v0_35 = ((this.q / v1_10) + 1);
            com.a.b.d.sa v1_13 = (this.q % v1_10);
            while (v5 < this.k.length) {
                if (v5 == v1_13) {
                    v0_35--;
                }
                this.k[v5] = this.a(v4, v0_35);
                v5++;
            }
        }
        return;
    }

    private com.a.b.d.rz a(Object p3)
    {
        com.a.b.d.rz v0_1;
        if (p3 != null) {
            com.a.b.d.rz v0_0 = this.b(p3);
            v0_1 = this.a(v0_0).a(p3, v0_0);
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    private com.a.b.d.rz a(Object p2, int p3, com.a.b.d.rz p4)
    {
        return this.a(p3).a(p2, p3, p4);
    }

    private com.a.b.d.sr a(com.a.b.d.rz p3, Object p4)
    {
        return this.p.a(this.a(p3.c()), p3, p4);
    }

    static void a(com.a.b.d.rz p0, com.a.b.d.rz p1)
    {
        p0.a(p1);
        p1.b(p0);
        return;
    }

    private void a(com.a.b.d.sr p4)
    {
        Object v0_0 = p4.a();
        int v1 = v0_0.c();
        this.a(v1).a(v0_0.d(), v1, p4);
        return;
    }

    static boolean a(com.a.b.d.rz p5, long p6)
    {
        int v0_3;
        if ((p6 - p5.e()) <= 0) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    private static int b(int p3)
    {
        int v0_2 = (((p3 << 15) ^ -12931) + p3);
        int v0_3 = (v0_2 ^ (v0_2 >> 10));
        int v0_4 = (v0_3 + (v0_3 << 3));
        int v0_5 = (v0_4 ^ (v0_4 >> 6));
        int v0_6 = (v0_5 + ((v0_5 << 2) + (v0_5 << 14)));
        return (v0_6 ^ (v0_6 >> 16));
    }

    static void b(com.a.b.d.rz p1)
    {
        com.a.b.d.ry v0 = com.a.b.d.ry.a;
        p1.a(v0);
        p1.b(v0);
        return;
    }

    static void b(com.a.b.d.rz p0, com.a.b.d.rz p1)
    {
        p0.c(p1);
        p1.d(p0);
        return;
    }

    private com.a.b.d.rz c(com.a.b.d.rz p2, com.a.b.d.rz p3)
    {
        return this.a(p2.c()).a(p2, p3);
    }

    static void c(com.a.b.d.rz p1)
    {
        com.a.b.d.ry v0 = com.a.b.d.ry.a;
        p1.c(v0);
        p1.d(v0);
        return;
    }

    private static com.a.b.d.sa[] c(int p1)
    {
        com.a.b.d.sa[] v0 = new com.a.b.d.sa[p1];
        return v0;
    }

    private void d(com.a.b.d.rz p3)
    {
        int v0 = p3.c();
        this.a(v0).a(p3, v0);
        return;
    }

    private boolean e(com.a.b.d.rz p2)
    {
        int v0_3;
        if (this.a(p2.c()).c(p2) == null) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    private Object f(com.a.b.d.rz p4)
    {
        Object v0 = 0;
        if (p4.d() != null) {
            Object v1_2 = p4.a().get();
            if ((v1_2 != null) && ((!this.c()) || (!this.a(p4)))) {
                v0 = v1_2;
            }
        }
        return v0;
    }

    static com.a.b.d.sr g()
    {
        return com.a.b.d.qy.x;
    }

    static com.a.b.d.rz h()
    {
        return com.a.b.d.ry.a;
    }

    static java.util.Queue i()
    {
        return com.a.b.d.qy.y;
    }

    private boolean j()
    {
        int v0_2;
        if (this.s <= 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private void k()
    {
        while (((com.a.b.d.qx) this.t.poll()) != null) {
        }
        return;
    }

    com.a.b.d.sa a(int p4)
    {
        return this.k[((p4 >> this.j) & this.i)];
    }

    com.a.b.d.sa a(int p2, int p3)
    {
        return new com.a.b.d.sa(this, p2, p3);
    }

    Object a()
    {
        return new com.a.b.d.sb(this.o, this.p, this.m, this.n, this.s, this.r, this.q, this.l, this.u, this);
    }

    final boolean a(com.a.b.d.rz p3)
    {
        return com.a.b.d.qy.a(p3, this.w.a());
    }

    final int b(Object p4)
    {
        int v0_1 = this.m.a(p4);
        int v0_2 = (v0_1 + ((v0_1 << 15) ^ -12931));
        int v0_3 = (v0_2 ^ (v0_2 >> 10));
        int v0_4 = (v0_3 + (v0_3 << 3));
        int v0_5 = (v0_4 ^ (v0_4 >> 6));
        int v0_6 = (v0_5 + ((v0_5 << 2) + (v0_5 << 14)));
        return (v0_6 ^ (v0_6 >> 16));
    }

    final boolean b()
    {
        int v0_1;
        if (this.q == -1) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    final boolean c()
    {
        boolean v2_2;
        int v0 = 0;
        if (this.s <= 0) {
            v2_2 = 0;
        } else {
            v2_2 = 1;
        }
        if ((v2_2) || (this.d())) {
            v0 = 1;
        }
        return v0;
    }

    public void clear()
    {
        com.a.b.d.sa[] v4 = this.k;
        int v5 = v4.length;
        int v3 = 0;
        while (v3 < v5) {
            com.a.b.d.sa v6 = v4[v3];
            if (v6.b != 0) {
                v6.lock();
                try {
                    java.util.concurrent.atomic.AtomicReferenceArray v7 = v6.e;
                } catch (com.a.b.d.rz v0_23) {
                    v6.unlock();
                    v6.d();
                    throw v0_23;
                }
                if (v6.a.t != com.a.b.d.qy.y) {
                    int v2_1 = 0;
                    while (v2_1 < v7.length()) {
                        com.a.b.d.rz v0_21 = ((com.a.b.d.rz) v7.get(v2_1));
                        while (v0_21 != null) {
                            if (!v0_21.a().b()) {
                                v6.a(v0_21, com.a.b.d.qq.a);
                            }
                            v0_21 = v0_21.b();
                        }
                        v2_1++;
                    }
                }
                com.a.b.d.rz v0_4 = 0;
                while (v0_4 < v7.length()) {
                    v7.set(v0_4, 0);
                    v0_4++;
                }
                if (v6.a.e()) {
                    while (v6.g.poll() != null) {
                    }
                }
                if (v6.a.f()) {
                    while (v6.h.poll() != null) {
                    }
                }
                v6.k.clear();
                v6.l.clear();
                v6.j.set(0);
                v6.c = (v6.c + 1);
                v6.b = 0;
                v6.unlock();
                v6.d();
            }
            v3++;
        }
        return;
    }

    public boolean containsKey(Object p3)
    {
        boolean v0_1;
        if (p3 != null) {
            boolean v0_0 = this.b(p3);
            v0_1 = this.a(v0_0).c(p3, v0_0);
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    public boolean containsValue(Object p15)
    {
        int v0_4;
        if (p15 != null) {
            com.a.b.d.sa[] v7 = this.k;
            int v6 = 0;
            long v8 = -1;
            while (v6 < 3) {
                int v10 = v7.length;
                long v4_1 = 0;
                int v2_1 = 0;
                while (v2_1 < v10) {
                    com.a.b.d.sa v3 = v7[v2_1];
                    java.util.concurrent.atomic.AtomicReferenceArray v11 = v3.e;
                    int v1 = 0;
                    while (v1 < v11.length()) {
                        int v0_12 = ((com.a.b.d.rz) v11.get(v1));
                        while (v0_12 != 0) {
                            boolean v12_0 = v3.c(v0_12);
                            if ((!v12_0) || (!this.n.a(p15, v12_0))) {
                                v0_12 = v0_12.b();
                            } else {
                                v0_4 = 1;
                            }
                        }
                        v1++;
                    }
                    v4_1 += ((long) v3.c);
                    v2_1++;
                }
                if (v4_1 == v8) {
                    break;
                }
                v6++;
                v8 = v4_1;
            }
            v0_4 = 0;
        } else {
            v0_4 = 0;
        }
        return v0_4;
    }

    final boolean d()
    {
        int v0_2;
        if (this.r <= 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    final boolean e()
    {
        int v0_1;
        if (this.o == com.a.b.d.sh.a) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public java.util.Set entrySet()
    {
        com.a.b.d.ro v0_0 = this.B;
        if (v0_0 == null) {
            v0_0 = new com.a.b.d.ro(this);
            this.B = v0_0;
        }
        return v0_0;
    }

    final boolean f()
    {
        int v0_1;
        if (this.p == com.a.b.d.sh.a) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public Object get(Object p3)
    {
        Object v0_1;
        if (p3 != null) {
            Object v0_0 = this.b(p3);
            v0_1 = this.a(v0_0).b(p3, v0_0);
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    public boolean isEmpty()
    {
        int v1 = 0;
        com.a.b.d.sa[] v6 = this.k;
        int v0_0 = 0;
        long v2 = 0;
        while (v0_0 < v6.length) {
            if (v6[v0_0].b == 0) {
                v2 += ((long) v6[v0_0].c);
                v0_0++;
            }
            return v1;
        }
        if (v2 != 0) {
            int v0_2 = 0;
            while (v0_2 < v6.length) {
                if (v6[v0_2].b == 0) {
                    v2 -= ((long) v6[v0_2].c);
                    v0_2++;
                }
                return v1;
            }
            if (v2 != 0) {
                return v1;
            }
        }
        v1 = 1;
        return v1;
    }

    public java.util.Set keySet()
    {
        com.a.b.d.rx v0_0 = this.z;
        if (v0_0 == null) {
            v0_0 = new com.a.b.d.rx(this);
            this.z = v0_0;
        }
        return v0_0;
    }

    public Object put(Object p4, Object p5)
    {
        com.a.b.b.cn.a(p4);
        com.a.b.b.cn.a(p5);
        Object v0_0 = this.b(p4);
        return this.a(v0_0).a(p4, v0_0, p5, 0);
    }

    public void putAll(java.util.Map p4)
    {
        java.util.Iterator v1 = p4.entrySet().iterator();
        while (v1.hasNext()) {
            Object v0_3 = ((java.util.Map$Entry) v1.next());
            this.put(v0_3.getKey(), v0_3.getValue());
        }
        return;
    }

    public Object putIfAbsent(Object p4, Object p5)
    {
        com.a.b.b.cn.a(p4);
        com.a.b.b.cn.a(p5);
        Object v0_0 = this.b(p4);
        return this.a(v0_0).a(p4, v0_0, p5, 1);
    }

    public Object remove(Object p3)
    {
        Object v0_1;
        if (p3 != null) {
            Object v0_0 = this.b(p3);
            v0_1 = this.a(v0_0).d(p3, v0_0);
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    public boolean remove(Object p3, Object p4)
    {
        if ((p3 != null) && (p4 != null)) {
            boolean v0_0 = this.b(p3);
            boolean v0_1 = this.a(v0_0).b(p3, v0_0, p4);
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    public Object replace(Object p3, Object p4)
    {
        com.a.b.b.cn.a(p3);
        com.a.b.b.cn.a(p4);
        Object v0_0 = this.b(p3);
        return this.a(v0_0).a(p3, v0_0, p4);
    }

    public boolean replace(Object p3, Object p4, Object p5)
    {
        boolean v0_1;
        com.a.b.b.cn.a(p3);
        com.a.b.b.cn.a(p5);
        if (p4 != null) {
            boolean v0_0 = this.b(p3);
            v0_1 = this.a(v0_0).a(p3, v0_0, p4, p5);
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    public int size()
    {
        com.a.b.d.sa[] v1 = this.k;
        long v2 = 0;
        int v0_0 = 0;
        while (v0_0 < v1.length) {
            v2 += ((long) v1[v0_0].b);
            v0_0++;
        }
        return com.a.b.l.q.b(v2);
    }

    public java.util.Collection values()
    {
        com.a.b.d.ss v0_0 = this.A;
        if (v0_0 == null) {
            v0_0 = new com.a.b.d.ss(this);
            this.A = v0_0;
        }
        return v0_0;
    }
}
