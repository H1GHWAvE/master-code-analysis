package com.a.b.d;
final class cq extends com.a.b.d.j {
    java.util.List a;
    final java.util.Comparator b;

    cq(java.util.List p2, java.util.Comparator p3)
    {
        this.a = com.a.b.d.ov.a(p2);
        this.b = p3;
        return;
    }

    private int a(int p5)
    {
        String v1_0 = this.a.get(p5);
        int v0_3 = (this.a.size() - 1);
        while (v0_3 > p5) {
            if (this.b.compare(v1_0, this.a.get(v0_3)) >= 0) {
                v0_3--;
            } else {
                return v0_3;
            }
        }
        throw new AssertionError("this statement should be unreachable");
    }

    private java.util.List c()
    {
        int v0_0 = 0;
        if (this.a != null) {
            String v1_2 = com.a.b.d.jl.a(this.a);
            int v2_2 = (this.a.size() - 2);
            while (v2_2 >= 0) {
                if (this.b.compare(this.a.get(v2_2), this.a.get((v2_2 + 1))) >= 0) {
                    v2_2--;
                }
                if (v2_2 != -1) {
                    java.util.List v3_1 = this.a.get(v2_2);
                    int v0_4 = (this.a.size() - 1);
                    while (v0_4 > v2_2) {
                        if (this.b.compare(v3_1, this.a.get(v0_4)) >= 0) {
                            v0_4--;
                        } else {
                            java.util.Collections.swap(this.a, v2_2, v0_4);
                            java.util.Collections.reverse(this.a.subList((v2_2 + 1), this.a.size()));
                        }
                    }
                    throw new AssertionError("this statement should be unreachable");
                } else {
                    this.a = 0;
                }
                v0_0 = v1_2;
            }
            v2_2 = -1;
        } else {
            this.b();
        }
        return v0_0;
    }

    private void d()
    {
        int v0_2 = (this.a.size() - 2);
        while (v0_2 >= 0) {
            if (this.b.compare(this.a.get(v0_2), this.a.get((v0_2 + 1))) >= 0) {
                v0_2--;
            } else {
                int v1_0 = v0_2;
            }
            if (v1_0 != -1) {
                java.util.List v2_1 = this.a.get(v1_0);
                int v0_6 = (this.a.size() - 1);
                while (v0_6 > v1_0) {
                    if (this.b.compare(v2_1, this.a.get(v0_6)) >= 0) {
                        v0_6--;
                    } else {
                        java.util.Collections.swap(this.a, v1_0, v0_6);
                        java.util.Collections.reverse(this.a.subList((v1_0 + 1), this.a.size()));
                    }
                }
                throw new AssertionError("this statement should be unreachable");
            } else {
                this.a = 0;
            }
            return;
        }
        v1_0 = -1;
    }

    private int e()
    {
        int v0_2 = (this.a.size() - 2);
        while (v0_2 >= 0) {
            if (this.b.compare(this.a.get(v0_2), this.a.get((v0_2 + 1))) >= 0) {
                v0_2--;
            }
            return v0_2;
        }
        v0_2 = -1;
        return v0_2;
    }

    protected final synthetic Object a()
    {
        int v0_0 = 0;
        if (this.a != null) {
            String v1_2 = com.a.b.d.jl.a(this.a);
            int v2_2 = (this.a.size() - 2);
            while (v2_2 >= 0) {
                if (this.b.compare(this.a.get(v2_2), this.a.get((v2_2 + 1))) >= 0) {
                    v2_2--;
                }
                if (v2_2 != -1) {
                    java.util.List v3_1 = this.a.get(v2_2);
                    int v0_4 = (this.a.size() - 1);
                    while (v0_4 > v2_2) {
                        if (this.b.compare(v3_1, this.a.get(v0_4)) >= 0) {
                            v0_4--;
                        } else {
                            java.util.Collections.swap(this.a, v2_2, v0_4);
                            java.util.Collections.reverse(this.a.subList((v2_2 + 1), this.a.size()));
                        }
                    }
                    throw new AssertionError("this statement should be unreachable");
                } else {
                    this.a = 0;
                }
                v0_0 = v1_2;
            }
            v2_2 = -1;
        } else {
            this.b();
        }
        return v0_0;
    }
}
