package com.a.b.d;
public final class oj extends com.a.b.d.an implements com.a.b.d.ou, java.io.Serializable {
    private static final long f;
    private transient com.a.b.d.or a;
    private transient com.a.b.d.or b;
    private transient java.util.Map c;
    private transient int d;
    private transient int e;

    oj()
    {
        this.c = com.a.b.d.sz.c();
        return;
    }

    private oj(int p2)
    {
        this.c = new java.util.HashMap(p2);
        return;
    }

    private oj(com.a.b.d.vi p2)
    {
        this(p2.p().size());
        this.a(p2);
        return;
    }

    static synthetic int a(com.a.b.d.oj p1)
    {
        return p1.e;
    }

    private static com.a.b.d.oj a()
    {
        return new com.a.b.d.oj();
    }

    private static com.a.b.d.oj a(int p1)
    {
        return new com.a.b.d.oj(p1);
    }

    static synthetic com.a.b.d.or a(com.a.b.d.oj p1, Object p2, Object p3, com.a.b.d.or p4)
    {
        return p1.a(p2, p3, p4);
    }

    private com.a.b.d.or a(Object p4, Object p5, com.a.b.d.or p6)
    {
        com.a.b.d.or v1_1 = new com.a.b.d.or(p4, p5);
        if (this.a != null) {
            if (p6 != null) {
                com.a.b.d.or v0_3 = ((com.a.b.d.oq) this.c.get(p4));
                v0_3.c = (v0_3.c + 1);
                v1_1.d = p6.d;
                v1_1.f = p6.f;
                v1_1.c = p6;
                v1_1.e = p6;
                if (p6.f != null) {
                    p6.f.e = v1_1;
                } else {
                    ((com.a.b.d.oq) this.c.get(p4)).a = v1_1;
                }
                if (p6.d != null) {
                    p6.d.c = v1_1;
                } else {
                    this.a = v1_1;
                }
                p6.d = v1_1;
                p6.f = v1_1;
            } else {
                this.b.c = v1_1;
                v1_1.d = this.b;
                this.b = v1_1;
                com.a.b.d.or v0_17 = ((com.a.b.d.oq) this.c.get(p4));
                if (v0_17 != null) {
                    v0_17.c = (v0_17.c + 1);
                    com.a.b.d.or v2_4 = v0_17.b;
                    v2_4.e = v1_1;
                    v1_1.f = v2_4;
                    v0_17.b = v1_1;
                } else {
                    this.c.put(p4, new com.a.b.d.oq(v1_1));
                    this.e = (this.e + 1);
                }
            }
        } else {
            this.b = v1_1;
            this.a = v1_1;
            this.c.put(p4, new com.a.b.d.oq(v1_1));
            this.e = (this.e + 1);
        }
        this.d = (this.d + 1);
        return v1_1;
    }

    static synthetic void a(com.a.b.d.oj p3, com.a.b.d.or p4)
    {
        if (p4.d == null) {
            p3.a = p4.c;
        } else {
            p4.d.c = p4.c;
        }
        if (p4.c == null) {
            p3.b = p4.d;
        } else {
            p4.c.d = p4.d;
        }
        if ((p4.f != null) || (p4.e != null)) {
            com.a.b.d.or v0_10 = ((com.a.b.d.oq) p3.c.get(p4.a));
            v0_10.c = (v0_10.c - 1);
            if (p4.f != null) {
                p4.f.e = p4.e;
            } else {
                v0_10.a = p4.e;
            }
            if (p4.e != null) {
                p4.e.f = p4.f;
            } else {
                v0_10.b = p4.f;
            }
        } else {
            ((com.a.b.d.oq) p3.c.remove(p4.a)).c = 0;
            p3.e = (p3.e + 1);
        }
        p3.d = (p3.d - 1);
        return;
    }

    static synthetic void a(com.a.b.d.oj p0, Object p1)
    {
        p0.h(p1);
        return;
    }

    private void a(com.a.b.d.or p4)
    {
        if (p4.d == null) {
            this.a = p4.c;
        } else {
            p4.d.c = p4.c;
        }
        if (p4.c == null) {
            this.b = p4.d;
        } else {
            p4.c.d = p4.d;
        }
        if ((p4.f != null) || (p4.e != null)) {
            com.a.b.d.or v0_10 = ((com.a.b.d.oq) this.c.get(p4.a));
            v0_10.c = (v0_10.c - 1);
            if (p4.f != null) {
                p4.f.e = p4.e;
            } else {
                v0_10.a = p4.e;
            }
            if (p4.e != null) {
                p4.e.f = p4.f;
            } else {
                v0_10.b = p4.f;
            }
        } else {
            ((com.a.b.d.oq) this.c.remove(p4.a)).c = 0;
            this.e = (this.e + 1);
        }
        this.d = (this.d - 1);
        return;
    }

    private void a(java.io.ObjectInputStream p5)
    {
        p5.defaultReadObject();
        this.c = com.a.b.d.sz.d();
        int v1 = p5.readInt();
        int v0_1 = 0;
        while (v0_1 < v1) {
            this.a(p5.readObject(), p5.readObject());
            v0_1++;
        }
        return;
    }

    private void a(java.io.ObjectOutputStream p4)
    {
        p4.defaultWriteObject();
        p4.writeInt(this.f());
        java.util.Iterator v1 = ((java.util.List) super.k()).iterator();
        while (v1.hasNext()) {
            Object v0_5 = ((java.util.Map$Entry) v1.next());
            p4.writeObject(v0_5.getKey());
            p4.writeObject(v0_5.getValue());
        }
        return;
    }

    private static com.a.b.d.oj b(com.a.b.d.vi p1)
    {
        return new com.a.b.d.oj(p1);
    }

    static synthetic com.a.b.d.or b(com.a.b.d.oj p1)
    {
        return p1.b;
    }

    static synthetic com.a.b.d.or c(com.a.b.d.oj p1)
    {
        return p1.a;
    }

    private java.util.List c()
    {
        return ((java.util.List) super.i());
    }

    private java.util.List d()
    {
        return new com.a.b.d.om(this);
    }

    static synthetic java.util.Map d(com.a.b.d.oj p1)
    {
        return p1.c;
    }

    static synthetic int e(com.a.b.d.oj p1)
    {
        return p1.d;
    }

    private java.util.List e()
    {
        return ((java.util.List) super.k());
    }

    static synthetic void e(Object p1)
    {
        if (p1 != null) {
            return;
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    private void h(Object p2)
    {
        com.a.b.d.nj.i(new com.a.b.d.ot(this, p2));
        return;
    }

    private static void i(Object p1)
    {
        if (p1 != null) {
            return;
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    private java.util.List j(Object p2)
    {
        return java.util.Collections.unmodifiableList(com.a.b.d.ov.a(new com.a.b.d.ot(this, p2)));
    }

    private java.util.List t()
    {
        return new com.a.b.d.oo(this);
    }

    public final java.util.List a(Object p2)
    {
        return new com.a.b.d.ok(this, p2);
    }

    public final java.util.List a(Object p5, Iterable p6)
    {
        java.util.List v0 = this.j(p5);
        com.a.b.d.ot v1_1 = new com.a.b.d.ot(this, p5);
        java.util.Iterator v2 = p6.iterator();
        while ((v1_1.hasNext()) && (v2.hasNext())) {
            v1_1.next();
            v1_1.set(v2.next());
        }
        while (v1_1.hasNext()) {
            v1_1.next();
            v1_1.remove();
        }
        while (v2.hasNext()) {
            v1_1.add(v2.next());
        }
        return v0;
    }

    public final bridge synthetic boolean a(com.a.b.d.vi p2)
    {
        return super.a(p2);
    }

    public final boolean a(Object p2, Object p3)
    {
        this.a(p2, p3, 0);
        return 1;
    }

    public final synthetic java.util.Collection b(Object p2, Iterable p3)
    {
        return this.a(p2, p3);
    }

    public final java.util.List b(Object p2)
    {
        java.util.List v0 = this.j(p2);
        this.h(p2);
        return v0;
    }

    public final bridge synthetic java.util.Map b()
    {
        return super.b();
    }

    public final bridge synthetic boolean b(Object p2, Object p3)
    {
        return super.b(p2, p3);
    }

    public final synthetic java.util.Collection c(Object p2)
    {
        return this.a(p2);
    }

    public final bridge synthetic boolean c(Object p2, Iterable p3)
    {
        return super.c(p2, p3);
    }

    public final bridge synthetic boolean c(Object p2, Object p3)
    {
        return super.c(p2, p3);
    }

    public final synthetic java.util.Collection d(Object p2)
    {
        return this.b(p2);
    }

    public final bridge synthetic boolean equals(Object p2)
    {
        return super.equals(p2);
    }

    public final int f()
    {
        return this.d;
    }

    public final boolean f(Object p2)
    {
        return this.c.containsKey(p2);
    }

    public final void g()
    {
        this.a = 0;
        this.b = 0;
        this.c.clear();
        this.d = 0;
        this.e = (this.e + 1);
        return;
    }

    public final boolean g(Object p2)
    {
        return ((java.util.List) super.i()).contains(p2);
    }

    final java.util.Set h()
    {
        return new com.a.b.d.ol(this);
    }

    public final bridge synthetic int hashCode()
    {
        return super.hashCode();
    }

    public final bridge synthetic java.util.Collection i()
    {
        return ((java.util.List) super.i());
    }

    public final bridge synthetic java.util.Collection k()
    {
        return ((java.util.List) super.k());
    }

    final java.util.Iterator l()
    {
        throw new AssertionError("should never be called");
    }

    final java.util.Map m()
    {
        return new com.a.b.d.wf(this);
    }

    public final boolean n()
    {
        int v0_1;
        if (this.a != null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    final synthetic java.util.Collection o()
    {
        return new com.a.b.d.oo(this);
    }

    public final bridge synthetic java.util.Set p()
    {
        return super.p();
    }

    public final bridge synthetic com.a.b.d.xc q()
    {
        return super.q();
    }

    final synthetic java.util.Collection s()
    {
        return new com.a.b.d.om(this);
    }

    public final bridge synthetic String toString()
    {
        return super.toString();
    }
}
