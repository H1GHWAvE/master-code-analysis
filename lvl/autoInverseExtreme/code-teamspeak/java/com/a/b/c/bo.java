package com.a.b.c;
 class bo implements com.a.b.c.e, java.io.Serializable {
    private static final long b = 1;
    final com.a.b.c.ao a;

    private bo(com.a.b.c.ao p1)
    {
        this.a = p1;
        return;
    }

    synthetic bo(com.a.b.c.ao p1, byte p2)
    {
        this(p1);
        return;
    }

    bo(com.a.b.c.f p3)
    {
        this(new com.a.b.c.ao(p3, 0));
        return;
    }

    public final com.a.b.d.jt a(Iterable p8)
    {
        int v0_0 = 0;
        com.a.b.c.ao v2 = this.a;
        java.util.LinkedHashMap v3 = com.a.b.d.sz.d();
        com.a.b.c.c v4_0 = p8.iterator();
        int v1_0 = 0;
        while (v4_0.hasNext()) {
            Object v5_1 = v4_0.next();
            Object v6 = v2.get(v5_1);
            if (v6 != null) {
                v3.put(v5_1, v6);
                v1_0++;
            } else {
                v0_0++;
            }
        }
        v2.x.a(v1_0);
        v2.x.b(v0_0);
        return com.a.b.d.jt.a(v3);
    }

    public final Object a(Object p3, java.util.concurrent.Callable p4)
    {
        com.a.b.b.cn.a(p4);
        return this.a.a(p3, new com.a.b.c.bp(this, p4));
    }

    public final void a()
    {
        com.a.b.c.bt[] v1 = this.a.i;
        int v2 = v1.length;
        int v0_1 = 0;
        while (v0_1 < v2) {
            v1[v0_1].b();
            v0_1++;
        }
        return;
    }

    public final void a(Object p2)
    {
        com.a.b.b.cn.a(p2);
        this.a.remove(p2);
        return;
    }

    public final void a(Object p2, Object p3)
    {
        this.a.put(p2, p3);
        return;
    }

    public final void a(java.util.Map p2)
    {
        this.a.putAll(p2);
        return;
    }

    public final long b()
    {
        return this.a.m();
    }

    public final void b(Iterable p4)
    {
        com.a.b.c.ao v0 = this.a;
        java.util.Iterator v1 = p4.iterator();
        while (v1.hasNext()) {
            v0.remove(v1.next());
        }
        return;
    }

    public final void c()
    {
        this.a.clear();
        return;
    }

    public final com.a.b.c.ai d()
    {
        com.a.b.c.b v1_1 = new com.a.b.c.b();
        v1_1.a(this.a.x);
        com.a.b.c.bt[] v2 = this.a.i;
        int v3 = v2.length;
        com.a.b.c.ai v0_3 = 0;
        while (v0_3 < v3) {
            v1_1.a(v2[v0_3].n);
            v0_3++;
        }
        return v1_1.b();
    }

    public final Object d(Object p5)
    {
        com.a.b.c.c v0_0 = this.a;
        Object v1_1 = v0_0.a(com.a.b.b.cn.a(p5));
        Object v1_2 = v0_0.a(v1_1).b(p5, v1_1);
        if (v1_2 != null) {
            v0_0.x.a(1);
        } else {
            v0_0.x.b(1);
        }
        return v1_2;
    }

    public final java.util.concurrent.ConcurrentMap e()
    {
        return this.a;
    }

    Object f()
    {
        return new com.a.b.c.bq(this.a);
    }
}
