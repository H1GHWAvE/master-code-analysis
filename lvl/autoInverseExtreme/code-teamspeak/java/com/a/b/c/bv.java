package com.a.b.c;
 class bv extends java.lang.ref.SoftReference implements com.a.b.c.cg {
    final com.a.b.c.bs a;

    bv(ref.ReferenceQueue p1, Object p2, com.a.b.c.bs p3)
    {
        this(p2, p1);
        this.a = p3;
        return;
    }

    public int a()
    {
        return 1;
    }

    public com.a.b.c.cg a(ref.ReferenceQueue p2, Object p3, com.a.b.c.bs p4)
    {
        return new com.a.b.c.bv(p2, p3, p4);
    }

    public final void a(Object p1)
    {
        return;
    }

    public final com.a.b.c.bs b()
    {
        return this.a;
    }

    public final boolean c()
    {
        return 0;
    }

    public final boolean d()
    {
        return 1;
    }

    public final Object e()
    {
        return this.get();
    }
}
