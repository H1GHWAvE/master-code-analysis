package com.a.b.c;
final class bj extends com.a.b.c.ar {
    final synthetic com.a.b.c.ao c;

    bj(com.a.b.c.ao p1, java.util.concurrent.ConcurrentMap p2)
    {
        this.c = p1;
        this(p1, p2);
        return;
    }

    public final boolean contains(Object p2)
    {
        return this.a.containsKey(p2);
    }

    public final java.util.Iterator iterator()
    {
        return new com.a.b.c.bi(this.c);
    }

    public final boolean remove(Object p2)
    {
        int v0_2;
        if (this.a.remove(p2) == null) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }
}
