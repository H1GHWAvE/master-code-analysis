package com.a.b.c;
abstract class ar extends java.util.AbstractSet {
    final java.util.concurrent.ConcurrentMap a;
    final synthetic com.a.b.c.ao b;

    ar(com.a.b.c.ao p1, java.util.concurrent.ConcurrentMap p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public void clear()
    {
        this.a.clear();
        return;
    }

    public boolean isEmpty()
    {
        return this.a.isEmpty();
    }

    public int size()
    {
        return this.a.size();
    }
}
