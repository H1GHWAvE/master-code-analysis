package com.a.b.m;
public final class t {

    private t()
    {
        return;
    }

    private static Object a(Class p5, reflect.InvocationHandler p6)
    {
        com.a.b.b.cn.a(p6);
        Object v0_0 = p5.isInterface();
        Object[] v2 = new Object[1];
        v2[0] = p5;
        com.a.b.b.cn.a(v0_0, "%s is not an interface", v2);
        Object v0_1 = p5.getClassLoader();
        Class[] v1_1 = new Class[1];
        v1_1[0] = p5;
        return p5.cast(reflect.Proxy.newProxyInstance(v0_1, v1_1, p6));
    }

    private static String a(Class p1)
    {
        return com.a.b.m.t.a(p1.getName());
    }

    public static String a(String p2)
    {
        String v0_2;
        String v0_1 = p2.lastIndexOf(46);
        if (v0_1 >= null) {
            v0_2 = p2.substring(0, v0_1);
        } else {
            v0_2 = "";
        }
        return v0_2;
    }

    private static varargs void a(Class[] p5)
    {
        AssertionError v1_0 = p5.length;
        ClassNotFoundException v0_0 = 0;
        while (v0_0 < v1_0) {
            ClassLoader v2_0 = p5[v0_0];
            try {
                Class.forName(v2_0.getName(), 1, v2_0.getClassLoader());
                v0_0++;
            } catch (ClassNotFoundException v0_1) {
                throw new AssertionError(v0_1);
            }
        }
        return;
    }
}
