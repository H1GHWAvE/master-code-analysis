package com.a.b.m;
public abstract class ae extends com.a.b.m.u implements java.io.Serializable {
    final reflect.Type a;
    private transient com.a.b.m.w b;

    protected ae()
    {
        int v0_3;
        this.a = this.a();
        if ((this.a instanceof reflect.TypeVariable)) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        Object[] v1_1 = new Object[1];
        v1_1[0] = this.a;
        com.a.b.b.cn.b(v0_3, "Cannot construct a TypeToken for a type variable.\nYou probably meant to call new TypeToken<%s>(getClass()) that can resolve the type variable for you.\nIf you do need to create a TypeToken of a type variable, please use TypeToken.of() instead.", v1_1);
        return;
    }

    private ae(Class p3)
    {
        reflect.Type v0_0 = super.a();
        if (!(v0_0 instanceof Class)) {
            this.a = com.a.b.m.ae.a(p3).b(v0_0).a;
        } else {
            this.a = v0_0;
        }
        return;
    }

    private ae(reflect.Type p2)
    {
        this.a = ((reflect.Type) com.a.b.b.cn.a(p2));
        return;
    }

    synthetic ae(reflect.Type p1, byte p2)
    {
        this(p1);
        return;
    }

    static com.a.b.d.jl a(reflect.Type[] p5)
    {
        com.a.b.d.jn v1 = com.a.b.d.jl.h();
        int v2 = p5.length;
        int v0_0 = 0;
        while (v0_0 < v2) {
            com.a.b.m.ae v3_1 = com.a.b.m.ae.a(p5[v0_0]);
            if (com.a.b.m.ae.e(v3_1.a).isInterface()) {
                v1.c(v3_1);
            }
            v0_0++;
        }
        return v1.b();
    }

    private com.a.b.m.ae a(com.a.b.m.v p4, com.a.b.m.ae p5)
    {
        return new com.a.b.m.am(new com.a.b.m.w().a(com.a.b.d.jt.c(new com.a.b.m.ab(p4.a), p5.a)).a(this.a));
    }

    private com.a.b.m.ae a(com.a.b.m.v p5, Class p6)
    {
        return new com.a.b.m.am(new com.a.b.m.w().a(com.a.b.d.jt.c(new com.a.b.m.ab(p5.a), com.a.b.m.ae.a(p6).a)).a(this.a));
    }

    public static com.a.b.m.ae a(Class p1)
    {
        return new com.a.b.m.am(p1);
    }

    private com.a.b.m.ae a(Class p7, reflect.Type[] p8)
    {
        String v1_0 = p8.length;
        int v0_0 = 0;
        while (v0_0 < v1_0) {
            String v2_3 = com.a.b.m.ae.a(p8[v0_0]);
            if (!com.a.b.m.ae.a(p7).a(v2_3)) {
                v0_0++;
            } else {
                return v2_3.b(p7);
            }
        }
        String v1_2 = String.valueOf(String.valueOf(p7));
        String v2_1 = String.valueOf(String.valueOf(this));
        throw new IllegalArgumentException(new StringBuilder(((v1_2.length() + 23) + v2_1.length())).append(v1_2).append(" isn\'t a super type of ").append(v2_1).toString());
    }

    public static com.a.b.m.ae a(reflect.Type p1)
    {
        return new com.a.b.m.am(p1);
    }

    private com.a.b.m.k a(reflect.Constructor p6)
    {
        com.a.b.m.ag v0_1;
        if (p6.getDeclaringClass() != com.a.b.m.ae.e(this.a)) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        Object[] v4_1 = new Object[2];
        v4_1[0] = p6;
        v4_1[1] = com.a.b.m.ae.e(this.a);
        com.a.b.b.cn.a(v0_1, "%s not declared by %s", v4_1);
        return new com.a.b.m.ag(this, p6);
    }

    private com.a.b.m.k a(reflect.Method p5)
    {
        com.a.b.m.af v0_2 = com.a.b.m.ae.a(p5.getDeclaringClass()).a(this);
        Object[] v2_1 = new Object[2];
        v2_1[0] = p5;
        v2_1[1] = this;
        com.a.b.b.cn.a(v0_2, "%s not declared by %s", v2_1);
        return new com.a.b.m.af(this, p5);
    }

    private static reflect.Type a(reflect.WildcardType p4)
    {
        while(true) {
            AssertionError v0_4;
            AssertionError v0_0 = p4.getUpperBounds();
            if (v0_0.length != 1) {
                break;
            }
            v0_4 = v0_0[0];
            if ((v0_4 instanceof reflect.WildcardType)) {
                p4 = ((reflect.WildcardType) v0_4);
            }
            return v0_4;
        }
        if (v0_0.length != 0) {
            String v1_2 = String.valueOf(String.valueOf(p4));
            throw new AssertionError(new StringBuilder((v1_2.length() + 59)).append("There should be at most one upper bound for wildcard type: ").append(v1_2).toString());
        } else {
            v0_4 = Object;
        }
        return v0_4;
    }

    private static boolean a(reflect.GenericArrayType p2, reflect.Type p3)
    {
        int v0_0 = 0;
        if (!(p3 instanceof Class)) {
            if ((p3 instanceof reflect.GenericArrayType)) {
                v0_0 = com.a.b.m.ae.a(p2.getGenericComponentType(), ((reflect.GenericArrayType) p3).getGenericComponentType());
            }
        } else {
            if (((Class) p3).isArray()) {
                v0_0 = com.a.b.m.ae.a(p2.getGenericComponentType(), ((Class) p3).getComponentType());
            } else {
                if (((Class) p3) == Object) {
                    v0_0 = 1;
                }
            }
        }
        return v0_0;
    }

    private static boolean a(reflect.Type p1, Class p2)
    {
        return p2.isAssignableFrom(com.a.b.m.ae.e(p1));
    }

    private static boolean a(reflect.Type p2, reflect.GenericArrayType p3)
    {
        boolean v0_0 = 0;
        if (!(p2 instanceof Class)) {
            if ((p2 instanceof reflect.GenericArrayType)) {
                v0_0 = com.a.b.m.ae.a(((reflect.GenericArrayType) p2).getGenericComponentType(), p3.getGenericComponentType());
            }
        } else {
            if (((Class) p2).isArray()) {
                v0_0 = com.a.b.m.ae.a(((Class) p2).getComponentType(), p3.getGenericComponentType());
            }
        }
        return v0_0;
    }

    private static boolean a(reflect.Type p9, reflect.ParameterizedType p10)
    {
        int v2 = 0;
        int v0_0 = com.a.b.m.ae.e(p10);
        if (v0_0.isAssignableFrom(com.a.b.m.ae.e(p9))) {
            reflect.TypeVariable[] v4 = v0_0.getTypeParameters();
            reflect.Type[] v5 = p10.getActualTypeArguments();
            com.a.b.m.ae v6 = com.a.b.m.ae.a(p9);
            int v1_2 = 0;
            while (v1_2 < v4.length) {
                int v0_5;
                reflect.Type v7 = v6.b(v4[v1_2]).a;
                int v0_4 = v5[v1_2];
                if (!v7.equals(v0_4)) {
                    if (!(v0_4 instanceof reflect.WildcardType)) {
                        v0_5 = 0;
                    } else {
                        v0_5 = com.a.b.m.ae.a(v7, ((reflect.WildcardType) v0_4));
                    }
                } else {
                    v0_5 = 1;
                }
                if (v0_5 != 0) {
                    v1_2++;
                }
            }
            v2 = 1;
        }
        return v2;
    }

    private static boolean a(reflect.Type p9, reflect.Type p10)
    {
        int v2 = 0;
        int v0_0 = p10;
        int v1_0 = p9;
        while (!v0_0.equals(v1_0)) {
            if (!(v0_0 instanceof reflect.WildcardType)) {
                if (!(v1_0 instanceof reflect.TypeVariable)) {
                    if (!(v1_0 instanceof reflect.WildcardType)) {
                        if (!(v1_0 instanceof reflect.GenericArrayType)) {
                            if (!(v0_0 instanceof Class)) {
                                if (!(v0_0 instanceof reflect.ParameterizedType)) {
                                    if ((v0_0 instanceof reflect.GenericArrayType)) {
                                        int v0_1 = ((reflect.GenericArrayType) v0_0);
                                        if (!(v1_0 instanceof Class)) {
                                            if ((v1_0 instanceof reflect.GenericArrayType)) {
                                                v1_0 = ((reflect.GenericArrayType) v1_0).getGenericComponentType();
                                                v0_0 = v0_1.getGenericComponentType();
                                            }
                                        } else {
                                            int v1_2 = ((Class) v1_0);
                                            if (v1_2.isArray()) {
                                                v1_0 = v1_2.getComponentType();
                                                v0_0 = v0_1.getGenericComponentType();
                                            }
                                        }
                                    }
                                } else {
                                    int v0_2 = ((reflect.ParameterizedType) v0_0);
                                    boolean v4_11 = com.a.b.m.ae.e(v0_2);
                                    if (v4_11.isAssignableFrom(com.a.b.m.ae.e(v1_0))) {
                                        boolean v4_12 = v4_11.getTypeParameters();
                                        reflect.Type[] v5_2 = v0_2.getActualTypeArguments();
                                        com.a.b.m.ae v6 = com.a.b.m.ae.a(v1_0);
                                        int v1_3 = 0;
                                        while (v1_3 < v4_12.length) {
                                            int v0_7;
                                            reflect.Type v7 = v6.b(v4_12[v1_3]).a;
                                            int v0_6 = v5_2[v1_3];
                                            if (!v7.equals(v0_6)) {
                                                if (!(v0_6 instanceof reflect.WildcardType)) {
                                                    v0_7 = 0;
                                                } else {
                                                    v0_7 = com.a.b.m.ae.a(v7, ((reflect.WildcardType) v0_6));
                                                }
                                            } else {
                                                v0_7 = 1;
                                            }
                                            if (v0_7 != 0) {
                                                v1_3++;
                                            }
                                        }
                                        v2 = 1;
                                    }
                                }
                            } else {
                                v2 = ((Class) v0_0).isAssignableFrom(com.a.b.m.ae.e(v1_0));
                            }
                        } else {
                            int v1_5 = ((reflect.GenericArrayType) v1_0);
                            if (!(v0_0 instanceof Class)) {
                                if ((v0_0 instanceof reflect.GenericArrayType)) {
                                    int v0_11 = ((reflect.GenericArrayType) v0_0);
                                    v1_0 = v1_5.getGenericComponentType();
                                    v0_0 = v0_11.getGenericComponentType();
                                }
                            } else {
                                int v0_12 = ((Class) v0_0);
                                if (v0_12.isArray()) {
                                    v1_0 = v1_5.getGenericComponentType();
                                    v0_0 = v0_12.getComponentType();
                                } else {
                                    if (v0_12 == Object) {
                                        v2 = 1;
                                    }
                                }
                            }
                        }
                    } else {
                        v2 = com.a.b.m.ae.a(((reflect.WildcardType) v1_0).getUpperBounds(), v0_0);
                    }
                } else {
                    v2 = com.a.b.m.ae.a(((reflect.TypeVariable) v1_0).getBounds(), v0_0);
                }
            } else {
                v2 = com.a.b.m.ae.a(v1_0, ((reflect.WildcardType) v0_0));
            }
            return v2;
        }
        v2 = 1;
        return v2;
    }

    private static boolean a(reflect.Type p4, reflect.WildcardType p5)
    {
        int v0 = 1;
        if (!com.a.b.m.ae.a(p4, com.a.b.m.ae.a(p5))) {
            v0 = 0;
        } else {
            boolean v2_3;
            boolean v2_2 = com.a.b.m.ae.b(p5);
            if (v2_2) {
                reflect.Type v3 = com.a.b.m.ae.i(p4);
                if (v3 != null) {
                    v2_3 = com.a.b.m.ae.a(v2_2, v3);
                } else {
                    v2_3 = 0;
                }
            } else {
                v2_3 = 1;
            }
            if (!v2_3) {
            }
        }
        return v0;
    }

    private static boolean a(reflect.Type[] p4, reflect.Type p5)
    {
        int v0 = 0;
        int v1 = 0;
        while (v1 < p4.length) {
            if (!com.a.b.m.ae.a(p4[v1], p5)) {
                v1++;
            } else {
                v0 = 1;
                break;
            }
        }
        return v0;
    }

    static synthetic reflect.Type[] a(com.a.b.m.ae p2, reflect.Type[] p3)
    {
        int v0 = 0;
        while (v0 < p3.length) {
            p3[v0] = p2.b(p3[v0]).a;
            v0++;
        }
        return p3;
    }

    private com.a.b.m.ae b(Class p7)
    {
        com.a.b.m.ae v0_10;
        com.a.b.m.ae v0_2 = p7.isAssignableFrom(com.a.b.m.ae.e(this.a));
        Object[] v2_0 = new Object[2];
        v2_0[0] = p7;
        v2_0[1] = this;
        com.a.b.b.cn.a(v0_2, "%s is not a super class of %s", v2_0);
        if (!(this.a instanceof reflect.TypeVariable)) {
            if (!(this.a instanceof reflect.WildcardType)) {
                if (!p7.isArray()) {
                    v0_10 = this.c(com.a.b.m.ae.d(p7).a);
                } else {
                    com.a.b.m.ae v0_11 = this.n();
                    Object[] v2_1 = new Object[2];
                    v2_1[0] = p7;
                    v2_1[1] = this;
                    v0_10 = com.a.b.m.ae.a(com.a.b.m.bh.b.a(((com.a.b.m.ae) com.a.b.b.cn.a(v0_11, "%s isn\'t a super type of %s", v2_1)).b(p7.getComponentType()).a));
                }
            } else {
                v0_10 = this.a(p7, ((reflect.WildcardType) this.a).getUpperBounds());
            }
        } else {
            v0_10 = this.a(p7, ((reflect.TypeVariable) this.a).getBounds());
        }
        return v0_10;
    }

    private com.a.b.m.ae b(Class p7, reflect.Type[] p8)
    {
        if (p8.length <= 0) {
            String v1_1 = String.valueOf(String.valueOf(p7));
            String v2_1 = String.valueOf(String.valueOf(this));
            throw new IllegalArgumentException(new StringBuilder(((v1_1.length() + 21) + v2_1.length())).append(v1_1).append(" isn\'t a subclass of ").append(v2_1).toString());
        } else {
            return com.a.b.m.ae.a(p8[0]).c(p7);
        }
    }

    static synthetic reflect.Type b(com.a.b.m.ae p1)
    {
        return p1.a;
    }

    private static reflect.Type b(reflect.WildcardType p4)
    {
        AssertionError v0_4;
        AssertionError v0_0 = p4.getLowerBounds();
        if (v0_0.length != 1) {
            if (v0_0.length != 0) {
                String v1_2 = String.valueOf(String.valueOf(p4));
                throw new AssertionError(new StringBuilder((v1_2.length() + 46)).append("Wildcard should have at most one lower bound: ").append(v1_2).toString());
            } else {
                v0_4 = 0;
            }
        } else {
            v0_4 = com.a.b.m.ae.i(v0_0[0]);
        }
        return v0_4;
    }

    private static boolean b(reflect.Type p1, reflect.Type p2)
    {
        int v0_2;
        if (!p1.equals(p2)) {
            if (!(p2 instanceof reflect.WildcardType)) {
                v0_2 = 0;
            } else {
                v0_2 = com.a.b.m.ae.a(p1, ((reflect.WildcardType) p2));
            }
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private static boolean b(reflect.Type p2, reflect.WildcardType p3)
    {
        boolean v0_1;
        boolean v0_0 = com.a.b.m.ae.b(p3);
        if (v0_0) {
            reflect.Type v1 = com.a.b.m.ae.i(p2);
            if (v1 != null) {
                v0_1 = com.a.b.m.ae.a(v0_0, v1);
            } else {
                v0_1 = 0;
            }
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    private reflect.Type[] b(reflect.Type[] p3)
    {
        int v0 = 0;
        while (v0 < p3.length) {
            p3[v0] = this.b(p3[v0]).a;
            v0++;
        }
        return p3;
    }

    private static synthetic com.a.b.d.lo c(com.a.b.m.ae p1)
    {
        return com.a.b.m.ae.f(p1.a);
    }

    private com.a.b.m.ae c(Class p7)
    {
        reflect.Type v1_0 = 1;
        while(true) {
            com.a.b.m.w v0_2;
            if ((this.a instanceof reflect.TypeVariable)) {
                v0_2 = 0;
            } else {
                v0_2 = 1;
            }
            java.util.HashMap v4_0 = new Object[1];
            v4_0[0] = this;
            com.a.b.b.cn.a(v0_2, "Cannot get subtype of type variable <%s>", v4_0);
            if (!(this.a instanceof reflect.WildcardType)) {
                break;
            }
            com.a.b.m.w v0_25 = ((reflect.WildcardType) this.a).getLowerBounds();
            if (v0_25.length <= 0) {
                reflect.Type v1_8 = String.valueOf(String.valueOf(p7));
                com.a.b.m.ae v2_3 = String.valueOf(String.valueOf(this));
                throw new IllegalArgumentException(new StringBuilder(((v1_8.length() + 21) + v2_3.length())).append(v1_8).append(" isn\'t a subclass of ").append(v2_3).toString());
            } else {
                this = com.a.b.m.ae.a(v0_25[0]);
            }
        }
        com.a.b.m.w v0_7 = com.a.b.m.ae.e(this.a).isAssignableFrom(p7);
        java.util.HashMap v4_2 = new Object[2];
        v4_2[0] = p7;
        v4_2[1] = this;
        com.a.b.b.cn.a(v0_7, "%s isn\'t a subclass of %s", v4_2);
        if (this.n() == null) {
            v1_0 = 0;
        }
        com.a.b.m.w v0_18;
        if (v1_0 == null) {
            if (!(this.a instanceof Class)) {
                com.a.b.m.ae v2_1 = com.a.b.m.ae.d(p7);
                com.a.b.m.w v0_14 = v2_1.b(com.a.b.m.ae.e(this.a)).a;
                com.a.b.m.w v3_3 = new com.a.b.m.w();
                reflect.Type v1_1 = this.a;
                java.util.HashMap v4_3 = com.a.b.d.sz.c();
                com.a.b.m.w.a(v4_3, ((reflect.Type) com.a.b.b.cn.a(v0_14)), ((reflect.Type) com.a.b.b.cn.a(v1_1)));
                p7 = v3_3.a(v4_3).a(v2_1.a);
            }
            v0_18 = com.a.b.m.ae.a(p7);
        } else {
            v0_18 = com.a.b.m.ae.a(com.a.b.m.bh.b.a(this.n().c(p7.getComponentType()).a));
        }
        return v0_18;
    }

    private static com.a.b.m.ae d(Class p2)
    {
        com.a.b.m.ae v0_2;
        if (!p2.isArray()) {
            com.a.b.m.ae v0_1 = p2.getTypeParameters();
            if (v0_1.length <= 0) {
                v0_2 = com.a.b.m.ae.a(p2);
            } else {
                v0_2 = com.a.b.m.ae.a(com.a.b.m.ay.a(p2, v0_1));
            }
        } else {
            v0_2 = com.a.b.m.ae.a(com.a.b.m.ay.a(com.a.b.m.ae.d(p2.getComponentType()).a));
        }
        return v0_2;
    }

    static com.a.b.m.ae d(reflect.Type p2)
    {
        int v0 = com.a.b.m.ae.a(p2);
        if (com.a.b.m.ae.e(v0.a).isInterface()) {
            v0 = 0;
        }
        return v0;
    }

    private Class d()
    {
        return com.a.b.m.ae.e(this.a);
    }

    private com.a.b.d.lo e()
    {
        return com.a.b.m.ae.f(this.a);
    }

    private com.a.b.m.ae e(Class p5)
    {
        com.a.b.m.ae v0_0 = this.n();
        Object[] v2_1 = new Object[2];
        v2_1[0] = p5;
        v2_1[1] = this;
        return com.a.b.m.ae.a(com.a.b.m.bh.b.a(((com.a.b.m.ae) com.a.b.b.cn.a(v0_0, "%s isn\'t a super type of %s", v2_1)).b(p5.getComponentType()).a));
    }

    static Class e(reflect.Type p1)
    {
        return ((Class) com.a.b.m.ae.f(p1).c().next());
    }

    static com.a.b.d.lo f(reflect.Type p4)
    {
        com.a.b.b.cn.a(p4);
        com.a.b.d.lo v0_0 = com.a.b.d.lo.i();
        com.a.b.m.ai v1_1 = new com.a.b.m.ai(v0_0);
        reflect.Type[] v2_1 = new reflect.Type[1];
        v2_1[0] = p4;
        v1_1.a(v2_1);
        return v0_0.b();
    }

    private com.a.b.m.ae f(Class p3)
    {
        return com.a.b.m.ae.a(com.a.b.m.bh.b.a(this.n().c(p3.getComponentType()).a));
    }

    private reflect.Type f()
    {
        return this.a;
    }

    private com.a.b.m.ae g()
    {
        com.a.b.m.ae v0_7;
        if (!(this.a instanceof reflect.TypeVariable)) {
            if (!(this.a instanceof reflect.WildcardType)) {
                com.a.b.m.ae v0_6 = com.a.b.m.ae.e(this.a).getGenericSuperclass();
                if (v0_6 != null) {
                    v0_7 = this.c(v0_6);
                } else {
                    v0_7 = 0;
                }
            } else {
                v0_7 = com.a.b.m.ae.d(((reflect.WildcardType) this.a).getUpperBounds()[0]);
            }
        } else {
            v0_7 = com.a.b.m.ae.d(((reflect.TypeVariable) this.a).getBounds()[0]);
        }
        return v0_7;
    }

    private reflect.Type g(Class p6)
    {
        if (!(this.a instanceof Class)) {
            com.a.b.m.ae v2 = com.a.b.m.ae.d(p6);
            com.a.b.m.w v0_5 = v2.b(com.a.b.m.ae.e(this.a)).a;
            com.a.b.m.w v3_1 = new com.a.b.m.w();
            reflect.Type v1_0 = this.a;
            java.util.HashMap v4 = com.a.b.d.sz.c();
            com.a.b.m.w.a(v4, ((reflect.Type) com.a.b.b.cn.a(v0_5)), ((reflect.Type) com.a.b.b.cn.a(v1_0)));
            p6 = v3_1.a(v4).a(v2.a);
        }
        return p6;
    }

    private boolean g(reflect.Type p3)
    {
        return com.a.b.m.ae.a(((reflect.Type) com.a.b.b.cn.a(p3)), this.a);
    }

    private com.a.b.d.jl h()
    {
        com.a.b.d.jl v0_7;
        if (!(this.a instanceof reflect.TypeVariable)) {
            if (!(this.a instanceof reflect.WildcardType)) {
                com.a.b.d.jn v1 = com.a.b.d.jl.h();
                reflect.Type[] v2 = com.a.b.m.ae.e(this.a).getGenericInterfaces();
                int v3 = v2.length;
                com.a.b.d.jl v0_6 = 0;
                while (v0_6 < v3) {
                    v1.c(this.c(v2[v0_6]));
                    v0_6++;
                }
                v0_7 = v1.b();
            } else {
                v0_7 = com.a.b.m.ae.a(((reflect.WildcardType) this.a).getUpperBounds());
            }
        } else {
            v0_7 = com.a.b.m.ae.a(((reflect.TypeVariable) this.a).getBounds());
        }
        return v0_7;
    }

    private static reflect.Type h(reflect.Type p1)
    {
        if ((p1 instanceof reflect.WildcardType)) {
            p1 = com.a.b.m.ae.a(((reflect.WildcardType) p1));
        }
        return p1;
    }

    private static reflect.Type i(reflect.Type p1)
    {
        if ((p1 instanceof reflect.WildcardType)) {
            p1 = com.a.b.m.ae.b(((reflect.WildcardType) p1));
        }
        return p1;
    }

    private boolean i()
    {
        int v0_1;
        if (this.n() == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    private static reflect.Type j(reflect.Type p1)
    {
        return com.a.b.m.bh.b.a(p1);
    }

    private boolean j()
    {
        if ((!(this.a instanceof Class)) || (!((Class) this.a).isPrimitive())) {
            int v0_5 = 0;
        } else {
            v0_5 = 1;
        }
        return v0_5;
    }

    private com.a.b.m.ae k()
    {
        if ((!(this.a instanceof Class)) || (!((Class) this.a).isPrimitive())) {
            Class v0_5 = 0;
        } else {
            v0_5 = 1;
        }
        if (v0_5 != null) {
            this = com.a.b.m.ae.a(com.a.b.l.z.a(((Class) this.a)));
        }
        return this;
    }

    private boolean l()
    {
        return com.a.b.l.z.a().contains(this.a);
    }

    private com.a.b.m.ae m()
    {
        if (com.a.b.l.z.a().contains(this.a)) {
            this = com.a.b.m.ae.a(com.a.b.l.z.b(((Class) this.a)));
        }
        return this;
    }

    private com.a.b.m.ae n()
    {
        com.a.b.m.ae v0_2;
        com.a.b.m.ae v0_1 = com.a.b.m.ay.c(this.a);
        if (v0_1 != null) {
            v0_2 = com.a.b.m.ae.a(v0_1);
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    private Object o()
    {
        return com.a.b.m.ae.a(new com.a.b.m.w().a(this.a));
    }

    public final boolean a(com.a.b.m.ae p3)
    {
        return com.a.b.m.ae.a(((reflect.Type) com.a.b.b.cn.a(p3.a)), this.a);
    }

    public final com.a.b.m.ae b(reflect.Type p3)
    {
        com.a.b.b.cn.a(p3);
        com.a.b.m.ae v0_0 = this.b;
        if (v0_0 == null) {
            v0_0 = new com.a.b.m.w().a(com.a.b.m.y.a(this.a));
            this.b = v0_0;
        }
        return com.a.b.m.ae.a(v0_0.a(p3));
    }

    public final com.a.b.m.aw b()
    {
        return new com.a.b.m.aw(this);
    }

    final com.a.b.m.ae c()
    {
        com.a.b.m.ah v0_1 = new com.a.b.m.ah(this);
        reflect.Type[] v1_1 = new reflect.Type[1];
        v1_1[0] = this.a;
        v0_1.a(v1_1);
        return this;
    }

    final com.a.b.m.ae c(reflect.Type p3)
    {
        com.a.b.m.ae v0 = this.b(p3);
        v0.b = this.b;
        return v0;
    }

    public boolean equals(Object p3)
    {
        int v0_1;
        if (!(p3 instanceof com.a.b.m.ae)) {
            v0_1 = 0;
        } else {
            v0_1 = this.a.equals(((com.a.b.m.ae) p3).a);
        }
        return v0_1;
    }

    public int hashCode()
    {
        return this.a.hashCode();
    }

    public String toString()
    {
        return com.a.b.m.ay.b(this.a);
    }
}
