package com.a.b.m;
final class ba extends com.a.b.m.ax {
    final synthetic java.util.concurrent.atomic.AtomicReference a;

    ba(java.util.concurrent.atomic.AtomicReference p1)
    {
        this.a = p1;
        return;
    }

    final void a(Class p3)
    {
        this.a.set(p3.getComponentType());
        return;
    }

    final void a(reflect.GenericArrayType p3)
    {
        this.a.set(p3.getGenericComponentType());
        return;
    }

    final void a(reflect.TypeVariable p3)
    {
        this.a.set(com.a.b.m.ay.a(p3.getBounds()));
        return;
    }

    final void a(reflect.WildcardType p3)
    {
        this.a.set(com.a.b.m.ay.a(p3.getUpperBounds()));
        return;
    }
}
