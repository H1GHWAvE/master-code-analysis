package com.a.b.m;
 class z {
    private final com.a.b.d.jt a;

    z()
    {
        this.a = com.a.b.d.jt.k();
        return;
    }

    private z(com.a.b.d.jt p1)
    {
        this.a = p1;
        return;
    }

    private reflect.Type a(reflect.TypeVariable p2)
    {
        return this.a(p2, new com.a.b.m.aa(this, p2, this));
    }

    final com.a.b.m.z a(java.util.Map p10)
    {
        com.a.b.d.ju v5 = com.a.b.d.jt.l();
        v5.a(this.a);
        java.util.Iterator v6 = p10.entrySet().iterator();
        while (v6.hasNext()) {
            int v2_1;
            com.a.b.m.z v0_6 = ((java.util.Map$Entry) v6.next());
            com.a.b.d.jt v1_2 = ((com.a.b.m.ab) v0_6.getKey());
            com.a.b.m.z v0_8 = ((reflect.Type) v0_6.getValue());
            if (v1_2.b(v0_8)) {
                v2_1 = 0;
            } else {
                v2_1 = 1;
            }
            Object[] v8 = new Object[1];
            v8[0] = v1_2;
            com.a.b.b.cn.a(v2_1, "Type variable %s bound to itself", v8);
            v5.a(v1_2, v0_8);
        }
        return new com.a.b.m.z(v5.a());
    }

    reflect.Type a(reflect.TypeVariable p4, com.a.b.m.z p5)
    {
        reflect.GenericDeclaration v0_2 = ((reflect.Type) this.a.get(new com.a.b.m.ab(p4)));
        if (v0_2 != null) {
            p4 = new com.a.b.m.w(p5, 0).a(v0_2);
        } else {
            reflect.GenericDeclaration v0_3 = p4.getBounds();
            if (v0_3.length != 0) {
                reflect.Type[] v1_7 = new com.a.b.m.w(p5, 0).a(v0_3);
                if ((!com.a.b.m.bm.a) || (!java.util.Arrays.equals(v0_3, v1_7))) {
                    p4 = com.a.b.m.ay.a(p4.getGenericDeclaration(), p4.getName(), v1_7);
                }
            }
        }
        return p4;
    }
}
