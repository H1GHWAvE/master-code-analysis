package com.a.b.m;
abstract enum class bb extends java.lang.Enum {
    public static final enum com.a.b.m.bb a;
    public static final enum com.a.b.m.bb b;
    static final com.a.b.m.bb c;
    private static final synthetic com.a.b.m.bb[] d;

    static bb()
    {
        int v1 = 0;
        com.a.b.m.bb.a = new com.a.b.m.bc("OWNED_BY_ENCLOSING_CLASS");
        com.a.b.m.bb.b = new com.a.b.m.be("LOCAL_CLASS_HAS_NO_OWNER");
        AssertionError v0_5 = new com.a.b.m.bb[2];
        v0_5[0] = com.a.b.m.bb.a;
        v0_5[1] = com.a.b.m.bb.b;
        com.a.b.m.bb.d = v0_5;
        com.a.b.m.bb[] v2_4 = com.a.b.m.bb.values();
        while (v1 < v2_4.length) {
            com.a.b.m.bb v4 = v2_4[v1];
            if (v4.a(com.a.b.m.bd) != ((reflect.ParameterizedType) new com.a.b.m.bf().getClass().getGenericSuperclass()).getOwnerType()) {
                v1++;
            } else {
                com.a.b.m.bb.c = v4;
                return;
            }
        }
        throw new AssertionError();
    }

    private bb(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    synthetic bb(String p1, int p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    private static com.a.b.m.bb a()
    {
        com.a.b.m.bb[] v2 = com.a.b.m.bb.values();
        int v1 = 0;
        while (v1 < v2.length) {
            com.a.b.m.bb v4 = v2[v1];
            if (v4.a(com.a.b.m.bd) != ((reflect.ParameterizedType) new com.a.b.m.bf().getClass().getGenericSuperclass()).getOwnerType()) {
                v1++;
            } else {
                return v4;
            }
        }
        throw new AssertionError();
    }

    public static com.a.b.m.bb valueOf(String p1)
    {
        return ((com.a.b.m.bb) Enum.valueOf(com.a.b.m.bb, p1));
    }

    public static com.a.b.m.bb[] values()
    {
        return ((com.a.b.m.bb[]) com.a.b.m.bb.d.clone());
    }

    abstract Class a();
}
