package com.a.b.m;
final class y extends com.a.b.m.ax {
    private static final com.a.b.m.ac a;
    private final java.util.Map b;

    static y()
    {
        com.a.b.m.y.a = new com.a.b.m.ac(0);
        return;
    }

    private y()
    {
        this.b = com.a.b.d.sz.c();
        return;
    }

    static com.a.b.d.jt a(reflect.Type p4)
    {
        com.a.b.d.jt v0_1 = new com.a.b.m.y();
        reflect.Type[] v1_1 = new reflect.Type[1];
        v1_1[0] = com.a.b.m.y.a.a(p4);
        v0_1.a(v1_1);
        return com.a.b.d.jt.a(v0_1.b);
    }

    private void a(com.a.b.m.ab p3, reflect.Type p4)
    {
        if (!this.b.containsKey(p3)) {
            reflect.Type v0_2 = p4;
            while (v0_2 != null) {
                if (!p3.b(v0_2)) {
                    v0_2 = ((reflect.Type) this.b.get(com.a.b.m.ab.a(v0_2)));
                }
                while (p4 != null) {
                    p4 = ((reflect.Type) this.b.remove(com.a.b.m.ab.a(p4)));
                }
            }
            this.b.put(p3, p4);
        }
        return;
    }

    final void a(Class p4)
    {
        reflect.Type[] v0_1 = new reflect.Type[1];
        v0_1[0] = p4.getGenericSuperclass();
        this.a(v0_1);
        this.a(p4.getGenericInterfaces());
        return;
    }

    final void a(reflect.ParameterizedType p11)
    {
        reflect.Type v1_1;
        reflect.Type[] v0_1 = ((Class) p11.getRawType());
        reflect.TypeVariable[] v6 = v0_1.getTypeParameters();
        reflect.Type[] v7 = p11.getActualTypeArguments();
        if (v6.length != v7.length) {
            v1_1 = 0;
        } else {
            v1_1 = 1;
        }
        com.a.b.b.cn.b(v1_1);
        int v4_1 = 0;
        while (v4_1 < v6.length) {
            com.a.b.m.ab v8_1 = new com.a.b.m.ab(v6[v4_1]);
            java.util.Map v5_0 = v7[v4_1];
            if (!this.b.containsKey(v8_1)) {
                reflect.Type v1_8 = v5_0;
                while (v1_8 != null) {
                    if (!v8_1.b(v1_8)) {
                        v1_8 = ((reflect.Type) this.b.get(com.a.b.m.ab.a(v1_8)));
                    } else {
                        reflect.Type v1_12 = v5_0;
                        while (v1_12 != null) {
                            v1_12 = ((reflect.Type) this.b.remove(com.a.b.m.ab.a(v1_12)));
                        }
                    }
                }
                this.b.put(v8_1, v5_0);
            }
            v4_1++;
        }
        reflect.Type v1_3 = new reflect.Type[1];
        v1_3[0] = v0_1;
        this.a(v1_3);
        reflect.Type[] v0_2 = new reflect.Type[1];
        v0_2[0] = p11.getOwnerType();
        this.a(v0_2);
        return;
    }

    final void a(reflect.TypeVariable p2)
    {
        this.a(p2.getBounds());
        return;
    }

    final void a(reflect.WildcardType p2)
    {
        this.a(p2.getUpperBounds());
        return;
    }
}
