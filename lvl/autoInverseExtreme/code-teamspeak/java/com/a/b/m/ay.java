package com.a.b.m;
final class ay {
    private static final com.a.b.b.bj a;
    private static final com.a.b.b.bv b;

    static ay()
    {
        com.a.b.m.ay.a = new com.a.b.m.az();
        com.a.b.m.ay.b = com.a.b.b.bv.a(", ").b("null");
        return;
    }

    private ay()
    {
        return;
    }

    static synthetic com.a.b.b.bj a()
    {
        return com.a.b.m.ay.a;
    }

    static Class a(Class p1)
    {
        return reflect.Array.newInstance(p1, 0).getClass();
    }

    static synthetic Iterable a(Iterable p1)
    {
        return com.a.b.d.mq.c(p1, com.a.b.b.cp.a(com.a.b.b.cp.a(Object)));
    }

    static varargs reflect.ParameterizedType a(Class p2, reflect.Type[] p3)
    {
        return new com.a.b.m.bn(com.a.b.m.bb.c.a(p2), p2, p3);
    }

    static varargs reflect.ParameterizedType a(reflect.Type p4, Class p5, reflect.Type[] p6)
    {
        com.a.b.m.bn v0_3;
        if (p4 != null) {
            com.a.b.m.bn v0_1;
            com.a.b.b.cn.a(p6);
            if (p5.getEnclosingClass() == null) {
                v0_1 = 0;
            } else {
                v0_1 = 1;
            }
            Object[] v1_1 = new Object[1];
            v1_1[0] = p5;
            com.a.b.b.cn.a(v0_1, "Owner type for unenclosed %s", v1_1);
            v0_3 = new com.a.b.m.bn(p4, p5, p6);
        } else {
            v0_3 = com.a.b.m.ay.a(p5, p6);
        }
        return v0_3;
    }

    static reflect.Type a(reflect.Type p5)
    {
        reflect.WildcardType v0_2;
        int v1_0 = 1;
        if (!(p5 instanceof reflect.WildcardType)) {
            v0_2 = com.a.b.m.bh.d.a(p5);
        } else {
            reflect.WildcardType v0_4;
            String v3_0 = ((reflect.WildcardType) p5).getLowerBounds();
            if (v3_0.length > 1) {
                v0_4 = 0;
            } else {
                v0_4 = 1;
            }
            com.a.b.b.cn.a(v0_4, "Wildcard cannot have more than one lower bounds.");
            if (v3_0.length != 1) {
                reflect.WildcardType v0_6 = ((reflect.WildcardType) p5).getUpperBounds();
                if (v0_6.length != 1) {
                    v1_0 = 0;
                }
                com.a.b.b.cn.a(v1_0, "Wildcard should have only one upper bound.");
                v0_2 = com.a.b.m.ay.d(com.a.b.m.ay.a(v0_6[0]));
            } else {
                reflect.Type[] v4_1 = new reflect.Type[1];
                v4_1[0] = com.a.b.m.ay.a(v3_0[0]);
                int v1_1 = new reflect.Type[1];
                v1_1[0] = Object;
                v0_2 = new com.a.b.m.bp(v4_1, v1_1);
            }
        }
        return v0_2;
    }

    static synthetic reflect.Type a(reflect.Type[] p3)
    {
        boolean v2_0 = p3.length;
        reflect.WildcardType v0_0 = 0;
        while (v0_0 < v2_0) {
            reflect.Type v1_1 = com.a.b.m.ay.c(p3[v0_0]);
            if (v1_1 == null) {
                v0_0++;
            } else {
                if ((v1_1 instanceof Class)) {
                    reflect.WildcardType v0_1 = ((Class) v1_1);
                    if (v0_1.isPrimitive()) {
                        return v0_1;
                    }
                }
                v0_1 = com.a.b.m.ay.d(v1_1);
            }
            return v0_1;
        }
        v0_1 = 0;
        return v0_1;
    }

    static varargs reflect.TypeVariable a(reflect.GenericDeclaration p3, String p4, reflect.Type[] p5)
    {
        if (p5.length == 0) {
            p5 = new reflect.Type[1];
            p5[0] = Object;
        }
        return new com.a.b.m.bo(p3, p4, p5);
    }

    static synthetic void a(reflect.Type[] p8, String p9)
    {
        int v5 = p8.length;
        int v4 = 0;
        while (v4 < v5) {
            int v0_0 = p8[v4];
            if ((v0_0 instanceof Class)) {
                int v1_2;
                int v0_1 = ((Class) v0_0);
                if (v0_1.isPrimitive()) {
                    v1_2 = 0;
                } else {
                    v1_2 = 1;
                }
                Object[] v7_1 = new Object[2];
                v7_1[0] = v0_1;
                v7_1[1] = p9;
                com.a.b.b.cn.a(v1_2, "Primitive type \'%s\' used as %s", v7_1);
            }
            v4++;
        }
        return;
    }

    static synthetic reflect.Type[] a(java.util.Collection p1)
    {
        reflect.Type[] v0_1 = new reflect.Type[p1.size()];
        return ((reflect.Type[]) p1.toArray(v0_1));
    }

    static synthetic com.a.b.b.bv b()
    {
        return com.a.b.m.ay.b;
    }

    private static Iterable b(Iterable p1)
    {
        return com.a.b.d.mq.c(p1, com.a.b.b.cp.a(com.a.b.b.cp.a(Object)));
    }

    static String b(reflect.Type p1)
    {
        String v0_1;
        if (!(p1 instanceof Class)) {
            v0_1 = p1.toString();
        } else {
            v0_1 = ((Class) p1).getName();
        }
        return v0_1;
    }

    private static reflect.Type b(reflect.Type[] p3)
    {
        boolean v2_0 = p3.length;
        reflect.WildcardType v0_0 = 0;
        while (v0_0 < v2_0) {
            reflect.Type v1_1 = com.a.b.m.ay.c(p3[v0_0]);
            if (v1_1 == null) {
                v0_0++;
            } else {
                if ((v1_1 instanceof Class)) {
                    reflect.WildcardType v0_1 = ((Class) v1_1);
                    if (v0_1.isPrimitive()) {
                        return v0_1;
                    }
                }
                v0_1 = com.a.b.m.ay.d(v1_1);
            }
            return v0_1;
        }
        v0_1 = 0;
        return v0_1;
    }

    private static void b(reflect.Type[] p8, String p9)
    {
        int v5 = p8.length;
        int v4 = 0;
        while (v4 < v5) {
            int v0_0 = p8[v4];
            if ((v0_0 instanceof Class)) {
                int v1_2;
                int v0_1 = ((Class) v0_0);
                if (v0_1.isPrimitive()) {
                    v1_2 = 0;
                } else {
                    v1_2 = 1;
                }
                Object[] v7_1 = new Object[2];
                v7_1[0] = v0_1;
                v7_1[1] = p9;
                com.a.b.b.cn.a(v1_2, "Primitive type \'%s\' used as %s", v7_1);
            }
            v4++;
        }
        return;
    }

    private static reflect.Type[] b(java.util.Collection p1)
    {
        reflect.Type[] v0_1 = new reflect.Type[p1.size()];
        return ((reflect.Type[]) p1.toArray(v0_1));
    }

    static reflect.Type c(reflect.Type p4)
    {
        com.a.b.b.cn.a(p4);
        reflect.Type v0_1 = new java.util.concurrent.atomic.AtomicReference();
        com.a.b.m.ba v1_1 = new com.a.b.m.ba(v0_1);
        reflect.Type[] v2_1 = new reflect.Type[1];
        v2_1[0] = p4;
        v1_1.a(v2_1);
        return ((reflect.Type) v0_1.get());
    }

    private static reflect.WildcardType d(reflect.Type p4)
    {
        reflect.Type[] v1 = new reflect.Type[0];
        reflect.Type[] v2_1 = new reflect.Type[1];
        v2_1[0] = p4;
        return new com.a.b.m.bp(v1, v2_1);
    }

    private static reflect.WildcardType e(reflect.Type p5)
    {
        reflect.Type[] v1 = new reflect.Type[1];
        v1[0] = p5;
        reflect.Type[] v2_1 = new reflect.Type[1];
        v2_1[0] = Object;
        return new com.a.b.m.bp(v1, v2_1);
    }
}
