package com.a.b.m;
public class e {
    private final String a;
    final ClassLoader b;

    e(String p2, ClassLoader p3)
    {
        this.a = ((String) com.a.b.b.cn.a(p2));
        this.b = ((ClassLoader) com.a.b.b.cn.a(p3));
        return;
    }

    static com.a.b.m.e a(String p1, ClassLoader p2)
    {
        com.a.b.m.e v0_3;
        if (!p1.endsWith(".class")) {
            v0_3 = new com.a.b.m.e(p1, p2);
        } else {
            v0_3 = new com.a.b.m.d(p1, p2);
        }
        return v0_3;
    }

    private java.net.URL a()
    {
        java.net.URL v0_1 = this.b.getResource(this.a);
        Object[] v2_1 = new Object[1];
        v2_1[0] = this.a;
        return ((java.net.URL) com.a.b.b.cn.a(v0_1, "Failed to load resource: %s", v2_1));
    }

    private String b()
    {
        return this.a;
    }

    public boolean equals(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof com.a.b.m.e)) && ((this.a.equals(((com.a.b.m.e) p4).a)) && (this.b == ((com.a.b.m.e) p4).b))) {
            v0 = 1;
        }
        return v0;
    }

    public int hashCode()
    {
        return this.a.hashCode();
    }

    public String toString()
    {
        return this.a;
    }
}
