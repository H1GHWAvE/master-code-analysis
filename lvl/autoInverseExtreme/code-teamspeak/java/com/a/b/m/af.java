package com.a.b.m;
final class af extends com.a.b.m.m {
    final synthetic com.a.b.m.ae b;

    af(com.a.b.m.ae p1, reflect.Method p2)
    {
        this.b = p1;
        this(p2);
        return;
    }

    public final com.a.b.m.ae a()
    {
        return this.b;
    }

    final reflect.Type[] d()
    {
        return com.a.b.m.ae.a(this.b, super.d());
    }

    final reflect.Type[] e()
    {
        return com.a.b.m.ae.a(this.b, super.e());
    }

    final reflect.Type g()
    {
        return this.b.b(super.g()).a;
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.b));
        String v1_2 = String.valueOf(String.valueOf(super.toString()));
        return new StringBuilder(((v0_2.length() + 1) + v1_2.length())).append(v0_2).append(".").append(v1_2).toString();
    }
}
