package com.a.b.m;
final class ao extends com.a.b.m.an {

    ao()
    {
        this(0);
        return;
    }

    private static Class a(com.a.b.m.ae p1)
    {
        return com.a.b.m.ae.e(p1.a);
    }

    private static Iterable b(com.a.b.m.ae p5)
    {
        com.a.b.d.jl v0_7;
        if (!(p5.a instanceof reflect.TypeVariable)) {
            if (!(p5.a instanceof reflect.WildcardType)) {
                com.a.b.d.jn v1 = com.a.b.d.jl.h();
                reflect.Type[] v2 = com.a.b.m.ae.e(p5.a).getGenericInterfaces();
                int v3 = v2.length;
                com.a.b.d.jl v0_6 = 0;
                while (v0_6 < v3) {
                    v1.c(p5.c(v2[v0_6]));
                    v0_6++;
                }
                v0_7 = v1.b();
            } else {
                v0_7 = com.a.b.m.ae.a(((reflect.WildcardType) p5.a).getUpperBounds());
            }
        } else {
            v0_7 = com.a.b.m.ae.a(((reflect.TypeVariable) p5.a).getBounds());
        }
        return v0_7;
    }

    private static com.a.b.m.ae c(com.a.b.m.ae p2)
    {
        com.a.b.m.ae v0_7;
        if (!(p2.a instanceof reflect.TypeVariable)) {
            if (!(p2.a instanceof reflect.WildcardType)) {
                com.a.b.m.ae v0_6 = com.a.b.m.ae.e(p2.a).getGenericSuperclass();
                if (v0_6 != null) {
                    v0_7 = p2.c(v0_6);
                } else {
                    v0_7 = 0;
                }
            } else {
                v0_7 = com.a.b.m.ae.d(((reflect.WildcardType) p2.a).getUpperBounds()[0]);
            }
        } else {
            v0_7 = com.a.b.m.ae.d(((reflect.TypeVariable) p2.a).getBounds()[0]);
        }
        return v0_7;
    }

    final synthetic Class b(Object p2)
    {
        return com.a.b.m.ae.e(((com.a.b.m.ae) p2).a);
    }

    final synthetic Iterable c(Object p6)
    {
        com.a.b.d.jl v0_7;
        if (!(((com.a.b.m.ae) p6).a instanceof reflect.TypeVariable)) {
            if (!(((com.a.b.m.ae) p6).a instanceof reflect.WildcardType)) {
                com.a.b.d.jn v1 = com.a.b.d.jl.h();
                reflect.Type[] v2 = com.a.b.m.ae.e(((com.a.b.m.ae) p6).a).getGenericInterfaces();
                int v3 = v2.length;
                com.a.b.d.jl v0_6 = 0;
                while (v0_6 < v3) {
                    v1.c(((com.a.b.m.ae) p6).c(v2[v0_6]));
                    v0_6++;
                }
                v0_7 = v1.b();
            } else {
                v0_7 = com.a.b.m.ae.a(((reflect.WildcardType) ((com.a.b.m.ae) p6).a).getUpperBounds());
            }
        } else {
            v0_7 = com.a.b.m.ae.a(((reflect.TypeVariable) ((com.a.b.m.ae) p6).a).getBounds());
        }
        return v0_7;
    }

    final synthetic Object d(Object p3)
    {
        com.a.b.m.ae v0_7;
        if (!(((com.a.b.m.ae) p3).a instanceof reflect.TypeVariable)) {
            if (!(((com.a.b.m.ae) p3).a instanceof reflect.WildcardType)) {
                com.a.b.m.ae v0_6 = com.a.b.m.ae.e(((com.a.b.m.ae) p3).a).getGenericSuperclass();
                if (v0_6 != null) {
                    v0_7 = ((com.a.b.m.ae) p3).c(v0_6);
                } else {
                    v0_7 = 0;
                }
            } else {
                v0_7 = com.a.b.m.ae.d(((reflect.WildcardType) ((com.a.b.m.ae) p3).a).getUpperBounds()[0]);
            }
        } else {
            v0_7 = com.a.b.m.ae.d(((reflect.TypeVariable) ((com.a.b.m.ae) p3).a).getBounds()[0]);
        }
        return v0_7;
    }
}
