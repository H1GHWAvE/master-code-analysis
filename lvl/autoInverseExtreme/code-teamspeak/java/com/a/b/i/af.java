package com.a.b.i;
final class af extends java.io.Reader {
    private CharSequence a;
    private int b;
    private int c;

    public af(CharSequence p2)
    {
        this.a = ((CharSequence) com.a.b.b.cn.a(p2));
        return;
    }

    private void a()
    {
        if (this.a != null) {
            return;
        } else {
            throw new java.io.IOException("reader closed");
        }
    }

    private boolean b()
    {
        int v0_1;
        if (this.c() <= 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    private int c()
    {
        return (this.a.length() - this.b);
    }

    public final declared_synchronized void close()
    {
        try {
            this.a = 0;
            return;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    public final declared_synchronized void mark(int p6)
    {
        Throwable v0_0 = 1;
        if (p6 < 0) {
            v0_0 = 0;
        }
        try {
            Object[] v2_1 = new Object[1];
            v2_1[0] = Integer.valueOf(p6);
            com.a.b.b.cn.a(v0_0, "readAheadLimit (%s) may not be negative", v2_1);
            this.a();
            this.c = this.b;
            return;
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public final boolean markSupported()
    {
        return 1;
    }

    public final declared_synchronized int read()
    {
        try {
            int v0_2;
            this.a();
        } catch (int v0_1) {
            throw v0_1;
        }
        if (!this.b()) {
            v0_2 = -1;
        } else {
            int v0_3 = this.a;
            int v1 = this.b;
            this.b = (v1 + 1);
            v0_2 = v0_3.charAt(v1);
        }
        return v0_2;
    }

    public final declared_synchronized int read(java.nio.CharBuffer p6)
    {
        try {
            int v0_2;
            com.a.b.b.cn.a(p6);
            this.a();
        } catch (int v0_3) {
            throw v0_3;
        }
        if (this.b()) {
            v0_2 = Math.min(p6.remaining(), this.c());
            int v1_1 = 0;
            while (v1_1 < v0_2) {
                char v2_0 = this.a;
                int v3 = this.b;
                this.b = (v3 + 1);
                p6.put(v2_0.charAt(v3));
                v1_1++;
            }
        } else {
            v0_2 = -1;
        }
        return v0_2;
    }

    public final declared_synchronized int read(char[] p7, int p8, int p9)
    {
        try {
            int v0_3;
            com.a.b.b.cn.a(p8, (p8 + p9), p7.length);
            this.a();
        } catch (int v0_4) {
            throw v0_4;
        }
        if (this.b()) {
            v0_3 = Math.min(p9, this.c());
            int v1_1 = 0;
            while (v1_1 < v0_3) {
                int v2 = (p8 + v1_1);
                char v3_0 = this.a;
                int v4 = this.b;
                this.b = (v4 + 1);
                p7[v2] = v3_0.charAt(v4);
                v1_1++;
            }
        } else {
            v0_3 = -1;
        }
        return v0_3;
    }

    public final declared_synchronized boolean ready()
    {
        try {
            this.a();
            return 1;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    public final declared_synchronized void reset()
    {
        try {
            this.a();
            this.b = this.c;
            return;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    public final declared_synchronized long skip(long p6)
    {
        Throwable v0_0 = 1;
        if (p6 < 0) {
            v0_0 = 0;
        }
        try {
            Object[] v2_3 = new Object[1];
            v2_3[0] = Long.valueOf(p6);
            com.a.b.b.cn.a(v0_0, "n (%s) may not be negative", v2_3);
            this.a();
            Throwable v0_4 = ((int) Math.min(((long) this.c()), p6));
            this.b = (this.b + v0_4);
            return ((long) v0_4);
        } catch (Throwable v0_6) {
            throw v0_6;
        }
    }
}
