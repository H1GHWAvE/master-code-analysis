package com.a.b.i;
public abstract class p {

    protected p()
    {
        return;
    }

    private long a(java.io.InputStream p5)
    {
        com.a.b.b.cn.a(p5);
        com.a.b.i.ar v1 = com.a.b.i.ar.a();
        try {
            RuntimeException v0_2 = ((java.io.OutputStream) v1.a(this.a()));
            long v2 = com.a.b.i.z.a(p5, v0_2);
            v0_2.flush();
            v1.close();
            return v2;
        } catch (RuntimeException v0_5) {
            v1.close();
            throw v0_5;
        } catch (RuntimeException v0_3) {
            throw v1.a(v0_3);
        }
    }

    private com.a.b.i.ag a(java.nio.charset.Charset p3)
    {
        return new com.a.b.i.r(this, p3, 0);
    }

    private void a(byte[] p3)
    {
        com.a.b.b.cn.a(p3);
        com.a.b.i.ar v1 = com.a.b.i.ar.a();
        try {
            RuntimeException v0_2 = ((java.io.OutputStream) v1.a(this.a()));
            v0_2.write(p3);
            v0_2.flush();
            v1.close();
            return;
        } catch (RuntimeException v0_5) {
            v1.close();
            throw v0_5;
        } catch (RuntimeException v0_3) {
            throw v1.a(v0_3);
        }
    }

    private java.io.OutputStream b()
    {
        java.io.BufferedOutputStream v0_1;
        java.io.BufferedOutputStream v0_0 = this.a();
        if (!(v0_0 instanceof java.io.BufferedOutputStream)) {
            v0_1 = new java.io.BufferedOutputStream(v0_0);
        } else {
            v0_1 = ((java.io.BufferedOutputStream) v0_0);
        }
        return v0_1;
    }

    public abstract java.io.OutputStream a();
}
