package com.a.b.i;
final class ab implements com.a.b.i.m {
    final java.io.DataInput a;

    ab(java.io.ByteArrayInputStream p2)
    {
        this.a = new java.io.DataInputStream(p2);
        return;
    }

    public final boolean readBoolean()
    {
        try {
            return this.a.readBoolean();
        } catch (java.io.IOException v0_2) {
            throw new IllegalStateException(v0_2);
        }
    }

    public final byte readByte()
    {
        try {
            return this.a.readByte();
        } catch (java.io.IOException v0_3) {
            throw new IllegalStateException(v0_3);
        } catch (java.io.IOException v0_2) {
            throw new AssertionError(v0_2);
        }
    }

    public final char readChar()
    {
        try {
            return this.a.readChar();
        } catch (java.io.IOException v0_2) {
            throw new IllegalStateException(v0_2);
        }
    }

    public final double readDouble()
    {
        try {
            return this.a.readDouble();
        } catch (java.io.IOException v0_2) {
            throw new IllegalStateException(v0_2);
        }
    }

    public final float readFloat()
    {
        try {
            return this.a.readFloat();
        } catch (java.io.IOException v0_2) {
            throw new IllegalStateException(v0_2);
        }
    }

    public final void readFully(byte[] p3)
    {
        try {
            this.a.readFully(p3);
            return;
        } catch (java.io.IOException v0_1) {
            throw new IllegalStateException(v0_1);
        }
    }

    public final void readFully(byte[] p3, int p4, int p5)
    {
        try {
            this.a.readFully(p3, p4, p5);
            return;
        } catch (java.io.IOException v0_1) {
            throw new IllegalStateException(v0_1);
        }
    }

    public final int readInt()
    {
        try {
            return this.a.readInt();
        } catch (java.io.IOException v0_2) {
            throw new IllegalStateException(v0_2);
        }
    }

    public final String readLine()
    {
        try {
            return this.a.readLine();
        } catch (java.io.IOException v0_2) {
            throw new IllegalStateException(v0_2);
        }
    }

    public final long readLong()
    {
        try {
            return this.a.readLong();
        } catch (java.io.IOException v0_2) {
            throw new IllegalStateException(v0_2);
        }
    }

    public final short readShort()
    {
        try {
            return this.a.readShort();
        } catch (java.io.IOException v0_2) {
            throw new IllegalStateException(v0_2);
        }
    }

    public final String readUTF()
    {
        try {
            return this.a.readUTF();
        } catch (java.io.IOException v0_2) {
            throw new IllegalStateException(v0_2);
        }
    }

    public final int readUnsignedByte()
    {
        try {
            return this.a.readUnsignedByte();
        } catch (java.io.IOException v0_2) {
            throw new IllegalStateException(v0_2);
        }
    }

    public final int readUnsignedShort()
    {
        try {
            return this.a.readUnsignedShort();
        } catch (java.io.IOException v0_2) {
            throw new IllegalStateException(v0_2);
        }
    }

    public final int skipBytes(int p3)
    {
        try {
            return this.a.skipBytes(p3);
        } catch (java.io.IOException v0_2) {
            throw new IllegalStateException(v0_2);
        }
    }
}
