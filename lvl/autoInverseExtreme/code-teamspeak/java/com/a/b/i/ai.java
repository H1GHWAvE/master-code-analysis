package com.a.b.i;
 class ai extends com.a.b.i.ah {
    private static final com.a.b.b.di a;
    private final CharSequence b;

    static ai()
    {
        com.a.b.i.ai.a = com.a.b.b.di.a(java.util.regex.Pattern.compile("\r\n|\n|\r"));
        return;
    }

    protected ai(CharSequence p2)
    {
        this.b = ((CharSequence) com.a.b.b.cn.a(p2));
        return;
    }

    static synthetic CharSequence a(com.a.b.i.ai p1)
    {
        return p1.b;
    }

    static synthetic com.a.b.b.di f()
    {
        return com.a.b.i.ai.a;
    }

    private Iterable g()
    {
        return new com.a.b.i.aj(this);
    }

    public final java.io.Reader a()
    {
        return new com.a.b.i.af(this.b);
    }

    public final Object a(com.a.b.i.by p3)
    {
        java.util.Iterator v1 = this.g().iterator();
        while (v1.hasNext()) {
            p3.a(((String) v1.next()));
        }
        return p3.a();
    }

    public final String b()
    {
        return this.b.toString();
    }

    public final String c()
    {
        int v0_2;
        int v0_1 = this.g().iterator();
        if (!v0_1.hasNext()) {
            v0_2 = 0;
        } else {
            v0_2 = ((String) v0_1.next());
        }
        return v0_2;
    }

    public final com.a.b.d.jl d()
    {
        return com.a.b.d.jl.a(this.g());
    }

    public final boolean e()
    {
        int v0_2;
        if (this.b.length() != 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public String toString()
    {
        String v0_3 = String.valueOf(String.valueOf(com.a.b.b.e.a(this.b, "...")));
        return new StringBuilder((v0_3.length() + 17)).append("CharSource.wrap(").append(v0_3).append(")").toString();
    }
}
