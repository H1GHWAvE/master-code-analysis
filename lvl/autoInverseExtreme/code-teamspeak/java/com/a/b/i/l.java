package com.a.b.i;
final class l implements com.a.b.i.bs {
    int a;
    int b;
    int c;
    boolean d;
    final com.a.b.b.m e;
    final synthetic com.a.b.i.bu f;
    final synthetic com.a.b.i.j g;

    l(com.a.b.i.j p2, com.a.b.i.bu p3)
    {
        this.g = p2;
        this.f = p3;
        this.a = 0;
        this.b = 0;
        this.c = 0;
        this.d = 0;
        this.e = this.g.a();
        return;
    }

    public final int a()
    {
        int v0_0 = -1;
        do {
            int v1_1 = this.f.a();
            if (v1_1 != -1) {
                this.c = (this.c + 1);
                int v1_2 = ((char) v1_1);
                if (!this.e.c(v1_2)) {
                    if (!this.d) {
                        this.a = (this.a << com.a.b.i.j.a(this.g).v);
                        int v2_7 = this.a;
                        byte[] v3_4 = com.a.b.i.j.a(this.g);
                        if ((v1_2 <= 127) && (v3_4.y[v1_2] != -1)) {
                            this.a = (v3_4.y[v1_2] | v2_7);
                            this.b = (this.b + com.a.b.i.j.a(this.g).v);
                        } else {
                            throw new com.a.b.i.h(new StringBuilder(25).append("Unrecognized character: ").append(v1_2).toString());
                        }
                    } else {
                        throw new com.a.b.i.h(new StringBuilder(61).append("Expected padding character but found \'").append(v1_2).append("\' at index ").append(this.c).toString());
                    }
                } else {
                    if ((this.d) || ((this.c != 1) && (com.a.b.i.j.a(this.g).a((this.c - 1))))) {
                        this.d = 1;
                    } else {
                        throw new com.a.b.i.h(new StringBuilder(41).append("Padding cannot start at index ").append(this.c).toString());
                    }
                }
            } else {
                if ((!this.d) && (!com.a.b.i.j.a(this.g).a(this.c))) {
                    throw new com.a.b.i.h(new StringBuilder(32).append("Invalid input length ").append(this.c).toString());
                }
            }
            return v0_0;
        } while(this.b < 8);
        this.b = (this.b - 8);
        v0_0 = ((this.a >> this.b) & 255);
        return v0_0;
    }

    public final void b()
    {
        this.f.b();
        return;
    }
}
