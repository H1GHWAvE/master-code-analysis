package com.a.b.i;
final class ap extends java.io.Writer {
    private static final com.a.b.i.ap a;

    static ap()
    {
        com.a.b.i.ap.a = new com.a.b.i.ap();
        return;
    }

    private ap()
    {
        return;
    }

    static synthetic com.a.b.i.ap a()
    {
        return com.a.b.i.ap.a;
    }

    public final java.io.Writer append(char p1)
    {
        return this;
    }

    public final java.io.Writer append(CharSequence p1)
    {
        com.a.b.b.cn.a(p1);
        return this;
    }

    public final java.io.Writer append(CharSequence p2, int p3, int p4)
    {
        com.a.b.b.cn.a(p3, p4, p2.length());
        return this;
    }

    public final bridge synthetic Appendable append(char p2)
    {
        return this.append(p2);
    }

    public final bridge synthetic Appendable append(CharSequence p2)
    {
        return this.append(p2);
    }

    public final bridge synthetic Appendable append(CharSequence p2, int p3, int p4)
    {
        return this.append(p2, p3, p4);
    }

    public final void close()
    {
        return;
    }

    public final void flush()
    {
        return;
    }

    public final String toString()
    {
        return "CharStreams.nullWriter()";
    }

    public final void write(int p1)
    {
        return;
    }

    public final void write(String p1)
    {
        com.a.b.b.cn.a(p1);
        return;
    }

    public final void write(String p3, int p4, int p5)
    {
        com.a.b.b.cn.a(p4, (p4 + p5), p3.length());
        return;
    }

    public final void write(char[] p1)
    {
        com.a.b.b.cn.a(p1);
        return;
    }

    public final void write(char[] p3, int p4, int p5)
    {
        com.a.b.b.cn.a(p4, (p4 + p5), p3.length);
        return;
    }
}
