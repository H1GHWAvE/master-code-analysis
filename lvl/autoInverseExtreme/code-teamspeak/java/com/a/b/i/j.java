package com.a.b.i;
final class j extends com.a.b.i.b {
    private final com.a.b.i.g a;
    private final Character b;
    private transient com.a.b.i.b c;
    private transient com.a.b.i.b d;

    private j(com.a.b.i.g p5, Character p6)
    {
        int v0_4;
        this.a = ((com.a.b.i.g) com.a.b.b.cn.a(p5));
        if ((p6 != null) && (p5.c(p6.charValue()))) {
            v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        Object[] v2_1 = new Object[1];
        v2_1[0] = p6;
        com.a.b.b.cn.a(v0_4, "Padding character %s was already in alphabet", v2_1);
        this.b = p6;
        return;
    }

    j(String p3, String p4, Character p5)
    {
        this(new com.a.b.i.g(p3, p4.toCharArray()), p5);
        return;
    }

    static synthetic com.a.b.i.g a(com.a.b.i.j p1)
    {
        return p1.a;
    }

    static synthetic Character b(com.a.b.i.j p1)
    {
        return p1.b;
    }

    final int a(int p4)
    {
        return (this.a.w * com.a.b.j.g.a(p4, this.a.x, java.math.RoundingMode.CEILING));
    }

    final com.a.b.b.m a()
    {
        com.a.b.b.m v0_3;
        if (this.b != null) {
            v0_3 = com.a.b.b.m.a(this.b.charValue());
        } else {
            v0_3 = com.a.b.b.m.m;
        }
        return v0_3;
    }

    public final com.a.b.i.b a(char p4)
    {
        if (((8 % this.a.v) != 0) && ((this.b == null) || (this.b.charValue() != p4))) {
            this = new com.a.b.i.j(this.a, Character.valueOf(p4));
        }
        return this;
    }

    public final com.a.b.i.b a(String p3, int p4)
    {
        com.a.b.b.cn.a(p3);
        com.a.b.b.cn.a(this.a().b(this.a).d(p3), "Separator cannot contain alphabet or padding characters");
        return new com.a.b.i.i(this, p3, p4);
    }

    final com.a.b.i.bs a(com.a.b.i.bu p2)
    {
        com.a.b.b.cn.a(p2);
        return new com.a.b.i.l(this, p2);
    }

    final com.a.b.i.bt a(com.a.b.i.bv p2)
    {
        com.a.b.b.cn.a(p2);
        return new com.a.b.i.k(this, p2);
    }

    final int b(int p5)
    {
        return ((int) (((((long) this.a.v) * ((long) p5)) + 7) / 8));
    }

    public final com.a.b.i.b b()
    {
        if (this.b != null) {
            this = new com.a.b.i.j(this.a, 0);
        }
        return this;
    }

    public final com.a.b.i.b c()
    {
        String v1_0 = 0;
        com.a.b.i.g v0_0 = this.c;
        if (v0_0 == null) {
            com.a.b.i.g v0_9;
            String v2_0 = this.a;
            if (v2_0.c()) {
                com.a.b.i.g v0_3;
                if (v2_0.d()) {
                    v0_3 = 0;
                } else {
                    v0_3 = 1;
                }
                com.a.b.b.cn.b(v0_3, "Cannot call upperCase() on a mixed-case alphabet");
                char[] v3_1 = new char[v2_0.t.length];
                while (v1_0 < v2_0.t.length) {
                    v3_1[v1_0] = com.a.b.b.e.b(v2_0.t[v1_0]);
                    v1_0++;
                }
                v0_9 = new com.a.b.i.g(String.valueOf(v2_0.s).concat(".upperCase()"), v3_1);
            } else {
                v0_9 = v2_0;
            }
            if (v0_9 != this.a) {
                v0_0 = new com.a.b.i.j(v0_9, this.b);
            } else {
                v0_0 = this;
            }
            this.c = v0_0;
        }
        return v0_0;
    }

    public final com.a.b.i.b d()
    {
        String v1_0 = 0;
        com.a.b.i.g v0_0 = this.d;
        if (v0_0 == null) {
            com.a.b.i.g v0_9;
            String v2_0 = this.a;
            if (v2_0.d()) {
                com.a.b.i.g v0_3;
                if (v2_0.c()) {
                    v0_3 = 0;
                } else {
                    v0_3 = 1;
                }
                com.a.b.b.cn.b(v0_3, "Cannot call lowerCase() on a mixed-case alphabet");
                char[] v3_1 = new char[v2_0.t.length];
                while (v1_0 < v2_0.t.length) {
                    v3_1[v1_0] = com.a.b.b.e.a(v2_0.t[v1_0]);
                    v1_0++;
                }
                v0_9 = new com.a.b.i.g(String.valueOf(v2_0.s).concat(".lowerCase()"), v3_1);
            } else {
                v0_9 = v2_0;
            }
            if (v0_9 != this.a) {
                v0_0 = new com.a.b.i.j(v0_9, this.b);
            } else {
                v0_0 = this;
            }
            this.d = v0_0;
        }
        return v0_0;
    }

    public final String toString()
    {
        String v0_1 = new StringBuilder("BaseEncoding.");
        v0_1.append(this.a.toString());
        if ((8 % this.a.v) != 0) {
            if (this.b != null) {
                v0_1.append(".withPadChar(").append(this.b).append(41);
            } else {
                v0_1.append(".omitPadding()");
            }
        }
        return v0_1.toString();
    }
}
