package com.a.b.i;
public final class cc extends java.io.FilterOutputStream implements java.io.DataOutput {

    private cc(java.io.OutputStream p3)
    {
        this(new java.io.DataOutputStream(((java.io.OutputStream) com.a.b.b.cn.a(p3))));
        return;
    }

    public final void close()
    {
        this.out.close();
        return;
    }

    public final void write(byte[] p2, int p3, int p4)
    {
        this.out.write(p2, p3, p4);
        return;
    }

    public final void writeBoolean(boolean p2)
    {
        ((java.io.DataOutputStream) this.out).writeBoolean(p2);
        return;
    }

    public final void writeByte(int p2)
    {
        ((java.io.DataOutputStream) this.out).writeByte(p2);
        return;
    }

    public final void writeBytes(String p2)
    {
        ((java.io.DataOutputStream) this.out).writeBytes(p2);
        return;
    }

    public final void writeChar(int p1)
    {
        this.writeShort(p1);
        return;
    }

    public final void writeChars(String p3)
    {
        int v0 = 0;
        while (v0 < p3.length()) {
            this.writeChar(p3.charAt(v0));
            v0++;
        }
        return;
    }

    public final void writeDouble(double p4)
    {
        this.writeLong(Double.doubleToLongBits(p4));
        return;
    }

    public final void writeFloat(float p2)
    {
        this.writeInt(Float.floatToIntBits(p2));
        return;
    }

    public final void writeInt(int p3)
    {
        this.out.write((p3 & 255));
        this.out.write(((p3 >> 8) & 255));
        this.out.write(((p3 >> 16) & 255));
        this.out.write(((p3 >> 24) & 255));
        return;
    }

    public final void writeLong(long p8)
    {
        int v0_0 = Long.reverseBytes(p8);
        byte[] v3 = new byte[8];
        int v2 = 7;
        while (v2 >= 0) {
            v3[v2] = ((byte) ((int) (255 & v0_0)));
            v0_0 >>= 8;
            v2--;
        }
        this.write(v3, 0, 8);
        return;
    }

    public final void writeShort(int p3)
    {
        this.out.write((p3 & 255));
        this.out.write(((p3 >> 8) & 255));
        return;
    }

    public final void writeUTF(String p2)
    {
        ((java.io.DataOutputStream) this.out).writeUTF(p2);
        return;
    }
}
