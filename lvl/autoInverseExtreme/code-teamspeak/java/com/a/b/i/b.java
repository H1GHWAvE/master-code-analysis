package com.a.b.i;
public abstract class b {
    private static final com.a.b.i.b a;
    private static final com.a.b.i.b b;
    private static final com.a.b.i.b c;
    private static final com.a.b.i.b d;
    private static final com.a.b.i.b e;

    static b()
    {
        com.a.b.i.b.a = new com.a.b.i.j("base64()", "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", Character.valueOf(61));
        com.a.b.i.b.b = new com.a.b.i.j("base64Url()", "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_", Character.valueOf(61));
        com.a.b.i.b.c = new com.a.b.i.j("base32()", "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567", Character.valueOf(61));
        com.a.b.i.b.d = new com.a.b.i.j("base32Hex()", "0123456789ABCDEFGHIJKLMNOPQRSTUV", Character.valueOf(61));
        com.a.b.i.b.e = new com.a.b.i.j("base16()", "0123456789ABCDEF", 0);
        return;
    }

    b()
    {
        return;
    }

    private static com.a.b.i.bu a(com.a.b.i.bu p1, com.a.b.b.m p2)
    {
        com.a.b.b.cn.a(p1);
        com.a.b.b.cn.a(p2);
        return new com.a.b.i.e(p1, p2);
    }

    private static com.a.b.i.bv a(com.a.b.i.bv p1, String p2, int p3)
    {
        com.a.b.i.f v0_0;
        com.a.b.b.cn.a(p1);
        com.a.b.b.cn.a(p2);
        if (p3 <= 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        com.a.b.b.cn.a(v0_0);
        return new com.a.b.i.f(p3, p2, p1);
    }

    private com.a.b.i.p a(com.a.b.i.ag p2)
    {
        com.a.b.b.cn.a(p2);
        return new com.a.b.i.c(this, p2);
    }

    private com.a.b.i.s a(com.a.b.i.ah p2)
    {
        com.a.b.b.cn.a(p2);
        return new com.a.b.i.d(this, p2);
    }

    private java.io.InputStream a(java.io.Reader p3)
    {
        com.a.b.b.cn.a(p3);
        com.a.b.i.bs v0_2 = this.a(new com.a.b.i.bm(p3));
        com.a.b.b.cn.a(v0_2);
        return new com.a.b.i.bo(v0_2);
    }

    private java.io.OutputStream a(java.io.Writer p3)
    {
        com.a.b.b.cn.a(p3);
        com.a.b.i.bt v0_2 = this.a(new com.a.b.i.bq(p3));
        com.a.b.b.cn.a(v0_2);
        return new com.a.b.i.bp(v0_2);
    }

    private String a(byte[] p3)
    {
        return this.a(((byte[]) com.a.b.b.cn.a(p3)), p3.length);
    }

    private byte[] a(CharSequence p3)
    {
        try {
            return this.b(p3);
        } catch (com.a.b.i.h v0_1) {
            throw new IllegalArgumentException(v0_1);
        }
    }

    private byte[] b(CharSequence p7)
    {
        byte[] v0_1 = this.a().k(p7);
        com.a.b.b.cn.a(v0_1);
        com.a.b.i.bs v5 = this.a(new com.a.b.i.bn(v0_1));
        byte[] v0_4 = new byte[this.b(v0_1.length())];
        try {
            byte[] v1_2 = v5.a();
            int v2 = 0;
        } catch (byte[] v0_6) {
            throw v0_6;
        } catch (byte[] v0_5) {
            throw new AssertionError(v0_5);
        }
        while (v1_2 != -1) {
            int v4_1 = (v2 + 1);
            v0_4[v2] = ((byte) v1_2);
            v1_2 = v5.a();
            v2 = v4_1;
        }
        if (v2 != v0_4.length) {
            byte[] v1_6 = new byte[v2];
            System.arraycopy(v0_4, 0, v1_6, 0, v2);
            v0_4 = v1_6;
        }
        return v0_4;
    }

    private static byte[] b(byte[] p2, int p3)
    {
        if (p3 != p2.length) {
            byte[] v0_1 = new byte[p3];
            System.arraycopy(p2, 0, v0_1, 0, p3);
            p2 = v0_1;
        }
        return p2;
    }

    public static com.a.b.i.b e()
    {
        return com.a.b.i.b.e;
    }

    private static com.a.b.i.b f()
    {
        return com.a.b.i.b.a;
    }

    private static com.a.b.i.b g()
    {
        return com.a.b.i.b.b;
    }

    private static com.a.b.i.b h()
    {
        return com.a.b.i.b.c;
    }

    private static com.a.b.i.b i()
    {
        return com.a.b.i.b.d;
    }

    abstract int a();

    abstract com.a.b.b.m a();

    public abstract com.a.b.i.b a();

    public abstract com.a.b.i.b a();

    abstract com.a.b.i.bs a();

    abstract com.a.b.i.bt a();

    public final String a(byte[] p5, int p6)
    {
        AssertionError v0_0 = 0;
        com.a.b.b.cn.a(p5);
        com.a.b.b.cn.a(0, (p6 + 0), p5.length);
        String v1_3 = new com.a.b.i.br(new StringBuilder(this.a(p6)));
        com.a.b.i.bt v2_3 = this.a(v1_3);
        while (v0_0 < p6) {
            try {
                v2_3.a(p5[(v0_0 + 0)]);
                v0_0++;
            } catch (AssertionError v0) {
                throw new AssertionError("impossible");
            }
        }
        v2_3.b();
        return v1_3.toString();
    }

    abstract int b();

    public abstract com.a.b.i.b b();

    public abstract com.a.b.i.b c();

    public abstract com.a.b.i.b d();
}
