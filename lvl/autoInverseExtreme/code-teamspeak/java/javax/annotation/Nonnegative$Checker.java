package javax.annotation;
public final class Nonnegative$Checker implements javax.annotation.meta.TypeQualifierValidator {

    public Nonnegative$Checker()
    {
        return;
    }

    private static javax.annotation.meta.When forConstantValue$4ec69d5d(Object p6)
    {
        int v0_1;
        int v0_0 = 1;
        if ((p6 instanceof Number)) {
            if (!(((Number) p6) instanceof Long)) {
                if (!(((Number) p6) instanceof Double)) {
                    if (!(((Number) p6) instanceof Float)) {
                        if (((Number) p6).intValue() >= 0) {
                            v0_0 = 0;
                        }
                    } else {
                        if (((Number) p6).floatValue() >= 0) {
                            v0_0 = 0;
                        }
                    }
                } else {
                    if (((Number) p6).doubleValue() >= 0) {
                        v0_0 = 0;
                    }
                }
            } else {
                if (((Number) p6).longValue() >= 0) {
                    v0_0 = 0;
                }
            }
            if (v0_0 == 0) {
                v0_1 = javax.annotation.meta.When.ALWAYS;
            } else {
                v0_1 = javax.annotation.meta.When.NEVER;
            }
        } else {
            v0_1 = javax.annotation.meta.When.NEVER;
        }
        return v0_1;
    }

    public final synthetic javax.annotation.meta.When forConstantValue(otation.Annotation p7, Object p8)
    {
        int v0_1;
        int v0_0 = 1;
        if ((p8 instanceof Number)) {
            if (!(((Number) p8) instanceof Long)) {
                if (!(((Number) p8) instanceof Double)) {
                    if (!(((Number) p8) instanceof Float)) {
                        if (((Number) p8).intValue() >= 0) {
                            v0_0 = 0;
                        }
                    } else {
                        if (((Number) p8).floatValue() >= 0) {
                            v0_0 = 0;
                        }
                    }
                } else {
                    if (((Number) p8).doubleValue() >= 0) {
                        v0_0 = 0;
                    }
                }
            } else {
                if (((Number) p8).longValue() >= 0) {
                    v0_0 = 0;
                }
            }
            if (v0_0 == 0) {
                v0_1 = javax.annotation.meta.When.ALWAYS;
            } else {
                v0_1 = javax.annotation.meta.When.NEVER;
            }
        } else {
            v0_1 = javax.annotation.meta.When.NEVER;
        }
        return v0_1;
    }
}
