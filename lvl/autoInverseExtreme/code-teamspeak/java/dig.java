public class Ldig {
    static org.xbill.DNS.Name a;
    static int b;
    static int c;

    static dig()
    {
        dig.a = 0;
        dig.b = 1;
        dig.c = 1;
        return;
    }

    public dig()
    {
        return;
    }

    private static void a()
    {
        System.out.println("Usage: dig [@server] name [<type>] [<class>] [options]");
        System.exit(0);
        return;
    }

    private static void a(org.xbill.DNS.Message p5)
    {
        System.out.println(new StringBuffer("; java dig 0.0 <> ").append(dig.a).append(" axfr").toString());
        if (p5.isSigned()) {
            System.out.print(";; TSIG ");
            if (!p5.isVerified()) {
                System.out.println("failed");
            } else {
                System.out.println("ok");
            }
        }
        if (p5.getRcode() == 0) {
            String v1_8 = p5.getSectionArray(1);
            java.io.PrintStream v0_7 = 0;
            while (v0_7 < v1_8.length) {
                System.out.println(v1_8[v0_7]);
                v0_7++;
            }
            System.out.print(";; done (");
            System.out.print(p5.getHeader().getCount(1));
            System.out.print(" records, ");
            System.out.print(p5.getHeader().getCount(3));
            System.out.println(" additional)");
        } else {
            System.out.println(p5);
        }
        return;
    }

    private static void a(org.xbill.DNS.Message p3, long p4)
    {
        System.out.println("; java dig 0.0");
        System.out.println(p3);
        System.out.println(new StringBuffer(";; Query time: ").append(p4).append(" ms").toString());
        return;
    }

    public static void main(String[] p10)
    {
        java.io.PrintStream v0_0 = 0;
        if (p10.length <= 0) {
            dig.a();
        }
        try {
            int v5_0;
            java.io.PrintStream v0_1;
            int v4_1;
            if (!p10[0].startsWith("@")) {
                v5_0 = 0;
                v4_1 = 0;
                int v3_8;
                if (v4_1 == 0) {
                    v3_8 = new org.xbill.DNS.SimpleResolver();
                } else {
                    v3_8 = new org.xbill.DNS.SimpleResolver(v4_1);
                }
                int v4_3 = (v5_0 + 1);
                String v2_5;
                String v2_1 = p10[v5_0];
                if (!v2_1.equals("-x")) {
                    dig.a = org.xbill.DNS.Name.fromString(v2_1, org.xbill.DNS.Name.root);
                    String v2_4 = org.xbill.DNS.Type.value(p10[v4_3]);
                    dig.b = v2_4;
                    if (v2_4 >= null) {
                        v2_5 = (v4_3 + 1);
                    } else {
                        dig.b = 1;
                        v2_5 = v4_3;
                    }
                    int v4_5 = org.xbill.DNS.DClass.value(p10[v2_5]);
                    dig.c = v4_5;
                    if (v4_5 >= 0) {
                        v2_5++;
                    } else {
                        dig.c = 1;
                    }
                } else {
                    v2_5 = (v4_3 + 1);
                    dig.a = org.xbill.DNS.ReverseMap.fromAddress(p10[v4_3]);
                    dig.b = 12;
                    dig.c = 1;
                }
                while ((p10[v2_5].startsWith("-")) && (p10[v2_5].length() > 1)) {
                    switch (p10[v2_5].charAt(1)) {
                        case 98:
                            int v4_37;
                            if (p10[v2_5].length() <= 2) {
                                v2_5++;
                                v4_37 = p10[v2_5];
                            } else {
                                v4_37 = p10[v2_5].substring(2);
                            }
                            v3_8.setLocalAddress(java.net.InetAddress.getByName(v4_37));
                            break;
                        case 99:
                        case 102:
                        case 103:
                        case 104:
                        case 106:
                        case 108:
                        case 109:
                        case 110:
                        case 111:
                        case 114:
                        case 115:
                        default:
                            System.out.print("Invalid option: ");
                            System.out.println(p10[v2_5]);
                            break;
                        case 100:
                            v3_8.setEDNS(0, 0, 32768, 0);
                            break;
                        case 101:
                            int v4_31;
                            if (p10[v2_5].length() <= 2) {
                                v2_5++;
                                v4_31 = p10[v2_5];
                            } else {
                                v4_31 = p10[v2_5].substring(2);
                            }
                            int v4_33 = Integer.parseInt(v4_31);
                            if ((v4_33 >= 0) && (v4_33 <= 1)) {
                                v3_8.setEDNS(v4_33);
                            } else {
                                System.out.println(new StringBuffer("Unsupported EDNS level: ").append(v4_33).toString());
                            }
                            break;
                        case 105:
                            v3_8.setIgnoreTruncation(1);
                            break;
                        case 107:
                            int v4_25;
                            if (p10[v2_5].length() <= 2) {
                                v2_5++;
                                v4_25 = p10[v2_5];
                            } else {
                                v4_25 = p10[v2_5].substring(2);
                            }
                            v3_8.setTSIGKey(org.xbill.DNS.TSIG.fromString(v4_25));
                            break;
                        case 112:
                            int v4_20;
                            if (p10[v2_5].length() <= 2) {
                                v2_5++;
                                v4_20 = p10[v2_5];
                            } else {
                                v4_20 = p10[v2_5].substring(2);
                            }
                            int v4_22 = Integer.parseInt(v4_20);
                            if ((v4_22 >= 0) && (v4_22 <= 65536)) {
                                v3_8.setPort(v4_22);
                            } else {
                                System.out.println("Invalid port");
                            }
                            break;
                        case 113:
                            v0_0 = 1;
                            break;
                        case 116:
                            v3_8.setTCP(1);
                            break;
                    }
                    v2_5++;
                }
                java.io.PrintStream v1_2 = v0_0;
                v0_1 = v3_8;
                if (v0_1 == null) {
                    v0_1 = new org.xbill.DNS.SimpleResolver();
                }
                String v2_16 = org.xbill.DNS.Message.newQuery(org.xbill.DNS.Record.newRecord(dig.a, dig.b, dig.c));
                if (v1_2 != null) {
                    System.out.println(v2_16);
                }
                int v4_43 = System.currentTimeMillis();
                java.io.PrintStream v0_3 = v0_1.send(v2_16);
                String v2_17 = System.currentTimeMillis();
                if (dig.b != 252) {
                    String v2_18 = (v2_17 - v4_43);
                    System.out.println("; java dig 0.0");
                    System.out.println(v0_3);
                    System.out.println(new StringBuffer(";; Query time: ").append(v2_18).append(" ms").toString());
                } else {
                    dig.a(v0_3);
                }
            } else {
                v5_0 = 1;
                v4_1 = p10[0].substring(1);
            }
        } catch (java.io.PrintStream v1) {
            java.io.PrintStream v1_1 = 0;
            if (dig.a != null) {
                v0_1 = v1_1;
                v1_2 = v0_0;
            } else {
                v0_1 = v1_1;
                v1_2 = v0_0;
                dig.a();
            }
        }
        return;
    }
}
