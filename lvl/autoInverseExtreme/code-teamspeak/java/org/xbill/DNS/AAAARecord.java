package org.xbill.DNS;
public class AAAARecord extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 13858142561639803566;
    private byte[] address;

    AAAARecord()
    {
        return;
    }

    public AAAARecord(org.xbill.DNS.Name p8, int p9, long p10, java.net.InetAddress p12)
    {
        this(p8, 28, p9, p10);
        if (org.xbill.DNS.Address.familyOf(p12) == 2) {
            this.address = p12.getAddress();
            return;
        } else {
            throw new IllegalArgumentException("invalid IPv6 address");
        }
    }

    public java.net.InetAddress getAddress()
    {
        try {
            java.net.InetAddress v0_3;
            if (this.name != null) {
                v0_3 = java.net.InetAddress.getByAddress(this.name.toString(), this.address);
            } else {
                v0_3 = java.net.InetAddress.getByAddress(this.address);
            }
        } catch (java.net.InetAddress v0) {
            v0_3 = 0;
        }
        return v0_3;
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.AAAARecord();
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p2, org.xbill.DNS.Name p3)
    {
        this.address = p2.getAddressBytes(2);
        return;
    }

    void rrFromWire(org.xbill.DNS.DNSInput p2)
    {
        this.address = p2.readByteArray(16);
        return;
    }

    String rrToString()
    {
        try {
            String v0_0 = java.net.InetAddress.getByAddress(0, this.address);
        } catch (String v1) {
            return v0_0;
        }
        if (v0_0.getAddress().length != 4) {
            v0_0 = v0_0.getHostAddress();
            return v0_0;
        } else {
            String v0_2 = new StringBuffer("0:0:0:0:0:ffff:");
            int v2_10 = (((this.address[14] & 255) << 8) + (this.address[15] & 255));
            v0_2.append(Integer.toHexString((((this.address[12] & 255) << 8) + (this.address[13] & 255))));
            v0_2.append(58);
            v0_2.append(Integer.toHexString(v2_10));
            v0_0 = v0_2.toString();
            return v0_0;
        }
    }

    void rrToWire(org.xbill.DNS.DNSOutput p2, org.xbill.DNS.Compression p3, boolean p4)
    {
        p2.writeByteArray(this.address);
        return;
    }
}
