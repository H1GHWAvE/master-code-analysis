package org.xbill.DNS;
public class Generator {
    private long current;
    public final int dclass;
    public long end;
    public final String namePattern;
    public final org.xbill.DNS.Name origin;
    public final String rdataPattern;
    public long start;
    public long step;
    public final long ttl;
    public final int type;

    public Generator(long p6, long p8, long p10, String p12, int p13, int p14, long p15, String p17, org.xbill.DNS.Name p18)
    {
        if ((p6 >= 0) && ((p8 >= 0) && ((p6 <= p8) && (p10 > 0)))) {
            if (org.xbill.DNS.Generator.supportedType(p13)) {
                org.xbill.DNS.DClass.check(p14);
                this.start = p6;
                this.end = p8;
                this.step = p10;
                this.namePattern = p12;
                this.type = p13;
                this.dclass = p14;
                this.ttl = p15;
                this.rdataPattern = p17;
                this.origin = p18;
                this.current = p6;
                return;
            } else {
                throw new IllegalArgumentException("unsupported type");
            }
        } else {
            throw new IllegalArgumentException("invalid range specification");
        }
    }

    private String substitute(String p19, long p20)
    {
        String v1_0 = 0;
        byte[] v14 = p19.getBytes();
        StringBuffer v15_1 = new StringBuffer();
        int v0_0 = 0;
        while (v0_0 < v14.length) {
            int v5_0 = ((char) (v14[v0_0] & 255));
            if (v1_0 == null) {
                if (v5_0 != 92) {
                    if (v5_0 != 36) {
                        v15_1.append(v5_0);
                    } else {
                        int v2_5 = 0;
                        long v8_0 = 0;
                        int v10 = 10;
                        int v7 = 0;
                        if (((v0_0 + 1) >= v14.length) || (v14[(v0_0 + 1)] != 36)) {
                            if (((v0_0 + 1) < v14.length) && (v14[(v0_0 + 1)] == 123)) {
                                int v0_5;
                                int v5_1;
                                int v4_1;
                                int v6_6 = (v0_0 + 1);
                                if (((v6_6 + 1) >= v14.length) || (v14[(v6_6 + 1)] != 45)) {
                                    v0_5 = 0;
                                    v4_1 = v5_0;
                                    v5_1 = v6_6;
                                } else {
                                    v0_5 = 1;
                                    v5_1 = (v6_6 + 1);
                                    v4_1 = v5_0;
                                }
                                while ((v5_1 + 1) < v14.length) {
                                    v5_1++;
                                    v4_1 = ((char) (v14[v5_1] & 255));
                                    if ((v4_1 == 44) || (v4_1 == 125)) {
                                        break;
                                    }
                                    if ((v4_1 >= 48) && (v4_1 <= 57)) {
                                        v4_1 = ((char) (v4_1 - 48));
                                        v2_5 = ((v2_5 * 10) + ((long) v4_1));
                                    } else {
                                        throw new org.xbill.DNS.TextParseException("invalid offset");
                                    }
                                }
                                long v12_7;
                                if (v0_5 == 0) {
                                    v12_7 = v2_5;
                                } else {
                                    v12_7 = (- v2_5);
                                }
                                int v0_7;
                                if (v4_1 != 44) {
                                    v0_7 = v5_1;
                                } else {
                                    int v2_6 = 0;
                                    int v0_8 = v4_1;
                                    int v4_5 = v5_1;
                                    while ((v4_5 + 1) < v14.length) {
                                        v4_5++;
                                        v0_8 = ((char) (v14[v4_5] & 255));
                                        if ((v0_8 == 44) || (v0_8 == 125)) {
                                            break;
                                        }
                                        if ((v0_8 >= 48) && (v0_8 <= 57)) {
                                            v0_8 = ((char) (v0_8 - 48));
                                            v2_6 = ((v2_6 * 10) + ((long) v0_8));
                                        } else {
                                            throw new org.xbill.DNS.TextParseException("invalid width");
                                        }
                                    }
                                    v8_0 = v2_6;
                                    v0_7 = v4_5;
                                    v4_1 = v0_8;
                                }
                                int v4_6;
                                int v2_8;
                                int v0_11;
                                if (v4_1 != 44) {
                                    v2_8 = 10;
                                    v4_6 = v0_7;
                                    v0_11 = 0;
                                } else {
                                    if ((v0_7 + 1) != v14.length) {
                                        v4_6 = (v0_7 + 1);
                                        int v0_14 = ((char) (v14[v4_6] & 255));
                                        if (v0_14 != 111) {
                                            if (v0_14 != 120) {
                                                if (v0_14 != 88) {
                                                    if (v0_14 == 100) {
                                                        v2_8 = 10;
                                                        v0_11 = 0;
                                                    } else {
                                                        throw new org.xbill.DNS.TextParseException("invalid base");
                                                    }
                                                } else {
                                                    v2_8 = 16;
                                                    v0_11 = 1;
                                                }
                                            } else {
                                                v2_8 = 16;
                                                v0_11 = 0;
                                            }
                                        } else {
                                            v2_8 = 8;
                                            v0_11 = 0;
                                        }
                                    } else {
                                        throw new org.xbill.DNS.TextParseException("invalid base");
                                    }
                                }
                                if (((v4_6 + 1) != v14.length) && (v14[(v4_6 + 1)] == 125)) {
                                    v10 = v2_8;
                                    v7 = v0_11;
                                    v2_5 = v12_7;
                                    v0_0 = (v4_6 + 1);
                                } else {
                                    throw new org.xbill.DNS.TextParseException("invalid modifiers");
                                }
                            }
                            int v2_14 = (v2_5 + p20);
                            if (v2_14 >= 0) {
                                int v2_15;
                                if (v10 != 8) {
                                    if (v10 != 16) {
                                        v2_15 = Long.toString(v2_14);
                                    } else {
                                        v2_15 = Long.toHexString(v2_14);
                                    }
                                } else {
                                    v2_15 = Long.toOctalString(v2_14);
                                }
                                int v4_14;
                                if (v7 == 0) {
                                    v4_14 = v2_15;
                                } else {
                                    v4_14 = v2_15.toUpperCase();
                                }
                                if ((v8_0 != 0) && (v8_0 > ((long) v4_14.length()))) {
                                    int v2_23 = (((int) v8_0) - v4_14.length());
                                    while(true) {
                                        int v3_2 = (v2_23 - 1);
                                        if (v2_23 <= 0) {
                                            break;
                                        }
                                        v15_1.append(48);
                                        v2_23 = v3_2;
                                    }
                                }
                                v15_1.append(v4_14);
                            } else {
                                throw new org.xbill.DNS.TextParseException("invalid offset expansion");
                            }
                        } else {
                            v0_0++;
                            v15_1.append(((char) (v14[v0_0] & 255)));
                        }
                    }
                } else {
                    if ((v0_0 + 1) != v14.length) {
                        v1_0 = 1;
                    } else {
                        throw new org.xbill.DNS.TextParseException("invalid escape character");
                    }
                }
            } else {
                v15_1.append(v5_0);
                v1_0 = 0;
            }
            v0_0++;
        }
        return v15_1.toString();
    }

    public static boolean supportedType(int p2)
    {
        int v0 = 1;
        org.xbill.DNS.Type.check(p2);
        if ((p2 != 12) && ((p2 != 5) && ((p2 != 39) && ((p2 != 1) && ((p2 != 28) && (p2 != 2)))))) {
            v0 = 0;
        }
        return v0;
    }

    public org.xbill.DNS.Record[] expand()
    {
        java.util.ArrayList v10_1 = new java.util.ArrayList();
        long v8 = this.start;
        while (v8 < this.end) {
            v10_1.add(org.xbill.DNS.Record.fromString(org.xbill.DNS.Name.fromString(this.substitute(this.namePattern, this.current), this.origin), this.type, this.dclass, this.ttl, this.substitute(this.rdataPattern, this.current), this.origin));
            v8 = (this.step + v8);
        }
        org.xbill.DNS.Record[] v0_4 = new org.xbill.DNS.Record[v10_1.size()];
        return ((org.xbill.DNS.Record[]) ((org.xbill.DNS.Record[]) v10_1.toArray(v0_4)));
    }

    public org.xbill.DNS.Record nextRecord()
    {
        org.xbill.DNS.Record v0_5;
        if (this.current <= this.end) {
            org.xbill.DNS.Name v1_1 = org.xbill.DNS.Name.fromString(this.substitute(this.namePattern, this.current), this.origin);
            String v6 = this.substitute(this.rdataPattern, this.current);
            this.current = (this.current + this.step);
            v0_5 = org.xbill.DNS.Record.fromString(v1_1, this.type, this.dclass, this.ttl, v6, this.origin);
        } else {
            v0_5 = 0;
        }
        return v0_5;
    }

    public String toString()
    {
        String v0_1 = new StringBuffer();
        v0_1.append("$GENERATE ");
        v0_1.append(new StringBuffer().append(this.start).append("-").append(this.end).toString());
        if (this.step > 1) {
            v0_1.append(new StringBuffer("/").append(this.step).toString());
        }
        v0_1.append(" ");
        v0_1.append(new StringBuffer().append(this.namePattern).append(" ").toString());
        v0_1.append(new StringBuffer().append(this.ttl).append(" ").toString());
        if ((this.dclass != 1) || (!org.xbill.DNS.Options.check("noPrintIN"))) {
            v0_1.append(new StringBuffer().append(org.xbill.DNS.DClass.string(this.dclass)).append(" ").toString());
        }
        v0_1.append(new StringBuffer().append(org.xbill.DNS.Type.string(this.type)).append(" ").toString());
        v0_1.append(new StringBuffer().append(this.rdataPattern).append(" ").toString());
        return v0_1.toString();
    }
}
