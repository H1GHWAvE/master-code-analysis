package org.xbill.DNS;
public final class Serial {
    private static final long MAX32 = 4294967295;

    private Serial()
    {
        return;
    }

    public static int compare(long p8, long p10)
    {
        if ((p8 >= 0) && (p8 <= 2.1219957905e-314)) {
            if ((p10 >= 0) && (p10 <= 2.1219957905e-314)) {
                long v0_4 = (p8 - p10);
                if (v0_4 < 2.1219957905e-314) {
                    if (v0_4 < nan) {
                        v0_4 += 2.121995791e-314;
                    }
                } else {
                    v0_4 -= 2.121995791e-314;
                }
                return ((int) v0_4);
            } else {
                throw new IllegalArgumentException(new StringBuffer().append(p10).append(" out of range").toString());
            }
        } else {
            throw new IllegalArgumentException(new StringBuffer().append(p8).append(" out of range").toString());
        }
    }

    public static long increment(long p6)
    {
        long v0_0 = 0;
        if ((p6 >= 0) && (p6 <= 2.1219957905e-314)) {
            if (p6 != 2.1219957905e-314) {
                v0_0 = (1 + p6);
            }
            return v0_0;
        } else {
            throw new IllegalArgumentException(new StringBuffer().append(p6).append(" out of range").toString());
        }
    }
}
