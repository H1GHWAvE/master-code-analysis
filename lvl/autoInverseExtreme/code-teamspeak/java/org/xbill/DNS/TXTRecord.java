package org.xbill.DNS;
public class TXTRecord extends org.xbill.DNS.TXTBase {
    private static final long serialVersionUID = 12665958309425330274;

    TXTRecord()
    {
        return;
    }

    public TXTRecord(org.xbill.DNS.Name p8, int p9, long p10, String p12)
    {
        this(p8, 16, p9, p10, p12);
        return;
    }

    public TXTRecord(org.xbill.DNS.Name p8, int p9, long p10, java.util.List p12)
    {
        this(p8, 16, p9, p10, p12);
        return;
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.TXTRecord();
    }
}
