package org.xbill.DNS;
public class SIGRecord extends org.xbill.DNS.SIGBase {
    private static final long serialVersionUID = 4963556060953589058;

    SIGRecord()
    {
        return;
    }

    public SIGRecord(org.xbill.DNS.Name p16, int p17, long p18, int p20, int p21, long p22, java.util.Date p24, java.util.Date p25, int p26, org.xbill.DNS.Name p27, byte[] p28)
    {
        this(p16, 24, p17, p18, p20, p21, p22, p24, p25, p26, p27, p28);
        return;
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.SIGRecord();
    }
}
