package org.xbill.DNS;
public final class Flags {
    public static final byte AA = 0x5;
    public static final byte AD = 0xa;
    public static final byte CD = 0xb;
    public static final int DO = 32768;
    public static final byte QR = 0x0;
    public static final byte RA = 0x8;
    public static final byte RD = 0x7;
    public static final byte TC = 0x6;
    private static org.xbill.DNS.Mnemonic flags;

    static Flags()
    {
        org.xbill.DNS.Mnemonic v0_1 = new org.xbill.DNS.Mnemonic("DNS Header Flag", 3);
        org.xbill.DNS.Flags.flags = v0_1;
        v0_1.setMaximum(15);
        org.xbill.DNS.Flags.flags.setPrefix("FLAG");
        org.xbill.DNS.Flags.flags.setNumericAllowed(1);
        org.xbill.DNS.Flags.flags.add(0, "qr");
        org.xbill.DNS.Flags.flags.add(5, "aa");
        org.xbill.DNS.Flags.flags.add(6, "tc");
        org.xbill.DNS.Flags.flags.add(7, "rd");
        org.xbill.DNS.Flags.flags.add(8, "ra");
        org.xbill.DNS.Flags.flags.add(10, "ad");
        org.xbill.DNS.Flags.flags.add(11, "cd");
        return;
    }

    private Flags()
    {
        return;
    }

    public static boolean isFlag(int p1)
    {
        int v0_3;
        org.xbill.DNS.Flags.flags.check(p1);
        if (((p1 <= 0) || (p1 > 4)) && (p1 < 12)) {
            v0_3 = 1;
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public static String string(int p1)
    {
        return org.xbill.DNS.Flags.flags.getText(p1);
    }

    public static int value(String p1)
    {
        return org.xbill.DNS.Flags.flags.getValue(p1);
    }
}
