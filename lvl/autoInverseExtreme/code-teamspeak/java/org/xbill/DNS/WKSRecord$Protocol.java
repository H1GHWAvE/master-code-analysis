package org.xbill.DNS;
public class WKSRecord$Protocol {
    public static final int ARGUS = 13;
    public static final int BBN_RCC_MON = 10;
    public static final int BR_SAT_MON = 76;
    public static final int CFTP = 62;
    public static final int CHAOS = 16;
    public static final int DCN_MEAS = 19;
    public static final int EGP = 8;
    public static final int EMCON = 14;
    public static final int GGP = 3;
    public static final int HMP = 20;
    public static final int ICMP = 1;
    public static final int IGMP = 2;
    public static final int IGP = 9;
    public static final int IPCV = 71;
    public static final int IPPC = 67;
    public static final int IRTP = 28;
    public static final int ISO_TP4 = 29;
    public static final int LEAF_1 = 25;
    public static final int LEAF_2 = 26;
    public static final int MERIT_INP = 32;
    public static final int MFE_NSP = 31;
    public static final int MIT_SUBNET = 65;
    public static final int MUX = 18;
    public static final int NETBLT = 30;
    public static final int NVP_II = 11;
    public static final int PRM = 21;
    public static final int PUP = 12;
    public static final int RDP = 27;
    public static final int RVD = 66;
    public static final int SAT_EXPAK = 64;
    public static final int SAT_MON = 69;
    public static final int SEP = 33;
    public static final int ST = 5;
    public static final int TCP = 6;
    public static final int TRUNK_1 = 23;
    public static final int TRUNK_2 = 24;
    public static final int UCL = 7;
    public static final int UDP = 17;
    public static final int WB_EXPAK = 79;
    public static final int WB_MON = 78;
    public static final int XNET = 15;
    public static final int XNS_IDP = 22;
    private static org.xbill.DNS.Mnemonic protocols;

    static WKSRecord$Protocol()
    {
        org.xbill.DNS.Mnemonic v0_1 = new org.xbill.DNS.Mnemonic("IP protocol", 3);
        org.xbill.DNS.WKSRecord$Protocol.protocols = v0_1;
        v0_1.setMaximum(255);
        org.xbill.DNS.WKSRecord$Protocol.protocols.setNumericAllowed(1);
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(1, "icmp");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(2, "igmp");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(3, "ggp");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(5, "st");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(6, "tcp");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(7, "ucl");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(8, "egp");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(9, "igp");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(10, "bbn-rcc-mon");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(11, "nvp-ii");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(12, "pup");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(13, "argus");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(14, "emcon");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(15, "xnet");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(16, "chaos");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(17, "udp");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(18, "mux");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(19, "dcn-meas");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(20, "hmp");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(21, "prm");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(22, "xns-idp");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(23, "trunk-1");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(24, "trunk-2");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(25, "leaf-1");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(26, "leaf-2");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(27, "rdp");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(28, "irtp");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(29, "iso-tp4");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(30, "netblt");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(31, "mfe-nsp");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(32, "merit-inp");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(33, "sep");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(62, "cftp");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(64, "sat-expak");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(65, "mit-subnet");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(66, "rvd");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(67, "ippc");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(69, "sat-mon");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(71, "ipcv");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(76, "br-sat-mon");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(78, "wb-mon");
        org.xbill.DNS.WKSRecord$Protocol.protocols.add(79, "wb-expak");
        return;
    }

    private WKSRecord$Protocol()
    {
        return;
    }

    public static String string(int p1)
    {
        return org.xbill.DNS.WKSRecord$Protocol.protocols.getText(p1);
    }

    public static int value(String p1)
    {
        return org.xbill.DNS.WKSRecord$Protocol.protocols.getValue(p1);
    }
}
