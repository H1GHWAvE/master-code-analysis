package org.xbill.DNS;
public class Master {
    private int currentDClass;
    private long currentTTL;
    private int currentType;
    private long defaultTTL;
    private java.io.File file;
    private org.xbill.DNS.Generator generator;
    private java.util.List generators;
    private org.xbill.DNS.Master included;
    private org.xbill.DNS.Record last;
    private boolean needSOATTL;
    private boolean noExpandGenerate;
    private org.xbill.DNS.Name origin;
    private org.xbill.DNS.Tokenizer st;

    Master(java.io.File p2, org.xbill.DNS.Name p3, long p4)
    {
        this.last = 0;
        this.included = 0;
        if ((p3 == null) || (p3.isAbsolute())) {
            this.file = p2;
            this.st = new org.xbill.DNS.Tokenizer(p2);
            this.origin = p3;
            this.defaultTTL = p4;
            return;
        } else {
            throw new org.xbill.DNS.RelativeNameException(p3);
        }
    }

    public Master(java.io.InputStream p5)
    {
        this(p5, 0, -1);
        return;
    }

    public Master(java.io.InputStream p3, org.xbill.DNS.Name p4)
    {
        this(p3, p4, -1);
        return;
    }

    public Master(java.io.InputStream p2, org.xbill.DNS.Name p3, long p4)
    {
        this.last = 0;
        this.included = 0;
        if ((p3 == null) || (p3.isAbsolute())) {
            this.st = new org.xbill.DNS.Tokenizer(p2);
            this.origin = p3;
            this.defaultTTL = p4;
            return;
        } else {
            throw new org.xbill.DNS.RelativeNameException(p3);
        }
    }

    public Master(String p5)
    {
        this(new java.io.File(p5), 0, -1);
        return;
    }

    public Master(String p5, org.xbill.DNS.Name p6)
    {
        this(new java.io.File(p5), p6, -1);
        return;
    }

    public Master(String p2, org.xbill.DNS.Name p3, long p4)
    {
        this(new java.io.File(p2), p3, p4);
        return;
    }

    private void endGenerate()
    {
        this.st.getEOL();
        this.generator = 0;
        return;
    }

    private org.xbill.DNS.Record nextGenerated()
    {
        try {
            return this.generator.nextRecord();
        } catch (org.xbill.DNS.TextParseException v0_7) {
            throw this.st.exception(new StringBuffer("Parsing $GENERATE: ").append(v0_7.getBaseMessage()).toString());
        } catch (org.xbill.DNS.TextParseException v0_2) {
            throw this.st.exception(new StringBuffer("Parsing $GENERATE: ").append(v0_2.getMessage()).toString());
        }
    }

    private org.xbill.DNS.Name parseName(String p3, org.xbill.DNS.Name p4)
    {
        try {
            return org.xbill.DNS.Name.fromString(p3, p4);
        } catch (org.xbill.DNS.TextParseException v0_1) {
            throw this.st.exception(v0_1.getMessage());
        }
    }

    private void parseTTLClassAndType()
    {
        org.xbill.DNS.TextParseException v0_0 = 0;
        int v2_1 = this.st.getString();
        String v3_0 = org.xbill.DNS.DClass.value(v2_1);
        this.currentDClass = v3_0;
        if (v3_0 >= null) {
            v2_1 = this.st.getString();
            v0_0 = 1;
        }
        this.currentTTL = -1;
        try {
            this.currentTTL = org.xbill.DNS.TTL.parseTTL(v2_1);
            int v2_2 = this.st.getString();
        } catch (String v3) {
            if (this.defaultTTL < 0) {
                if (this.last == null) {
                } else {
                    this.currentTTL = this.last.getTTL();
                }
            } else {
                this.currentTTL = this.defaultTTL;
            }
        }
        org.xbill.DNS.TextParseException v0_4;
        if (v0_0 != null) {
            v0_4 = v2_2;
        } else {
            org.xbill.DNS.TextParseException v0_3 = org.xbill.DNS.DClass.value(v2_2);
            this.currentDClass = v0_3;
            if (v0_3 < null) {
                this.currentDClass = 1;
            } else {
                v0_4 = this.st.getString();
            }
        }
        int v2_3 = org.xbill.DNS.Type.value(v0_4);
        this.currentType = v2_3;
        if (v2_3 >= 0) {
            if (this.currentTTL < 0) {
                if (this.currentType == 6) {
                    this.needSOATTL = 1;
                    this.currentTTL = 0;
                } else {
                    throw this.st.exception("missing TTL");
                }
            }
            return;
        } else {
            throw this.st.exception(new StringBuffer("Invalid type \'").append(v0_4).append("\'").toString());
        }
    }

    private long parseUInt32(String p7)
    {
        NumberFormatException v0 = -1;
        if (Character.isDigit(p7.charAt(0))) {
            try {
                NumberFormatException v2_3 = Long.parseLong(p7);
            } catch (NumberFormatException v2) {
            }
            if ((v2_3 >= 0) && (v2_3 <= 2.1219957905e-314)) {
                v0 = v2_3;
            }
        }
        return v0;
    }

    private void startGenerate()
    {
        String v8_0 = this.st.getIdentifier();
        java.util.List v0_2 = v8_0.indexOf("-");
        if (v0_2 >= null) {
            String v2_0 = v8_0.substring(0, v0_2);
            org.xbill.DNS.Generator v1_0 = v8_0.substring((v0_2 + 1));
            java.util.List v0_4 = 0;
            int v3_1 = v1_0.indexOf("/");
            if (v3_1 >= 0) {
                v0_4 = v1_0.substring((v3_1 + 1));
                v1_0 = v1_0.substring(0, v3_1);
            }
            long v6;
            String v2_1 = this.parseUInt32(v2_0);
            long v4_1 = this.parseUInt32(v1_0);
            if (v0_4 == null) {
                v6 = 1;
            } else {
                v6 = this.parseUInt32(v0_4);
            }
            if ((v2_1 >= 0) && ((v4_1 >= 0) && ((v2_1 <= v4_1) && (v6 > 0)))) {
                String v8_1 = this.st.getIdentifier();
                this.parseTTLClassAndType();
                if (org.xbill.DNS.Generator.supportedType(this.currentType)) {
                    String v13 = this.st.getIdentifier();
                    this.st.getEOL();
                    this.st.unget();
                    this.generator = new org.xbill.DNS.Generator(v2_1, v4_1, v6, v8_1, this.currentType, this.currentDClass, this.currentTTL, v13, this.origin);
                    if (this.generators == null) {
                        this.generators = new java.util.ArrayList(1);
                    }
                    this.generators.add(this.generator);
                    return;
                } else {
                    throw this.st.exception(new StringBuffer("$GENERATE does not support ").append(org.xbill.DNS.Type.string(this.currentType)).append(" records").toString());
                }
            } else {
                throw this.st.exception(new StringBuffer("Invalid $GENERATE range specifier: ").append(v8_0).toString());
            }
        } else {
            throw this.st.exception(new StringBuffer("Invalid $GENERATE range specifier: ").append(v8_0).toString());
        }
    }

    public org.xbill.DNS.Record _nextRecord()
    {
        org.xbill.DNS.Record v0_2;
        if (this.included == null) {
            if (this.generator != null) {
                v0_2 = this.nextGenerated();
                if (v0_2 != null) {
                    return v0_2;
                } else {
                    this.endGenerate();
                }
            }
            do {
                org.xbill.DNS.Name v1_2;
                org.xbill.DNS.Record v0_5 = this.st.get(1, 0);
                if (v0_5.type != 2) {
                    if (v0_5.type != 1) {
                        if (v0_5.type != 0) {
                            if (v0_5.value.charAt(0) != 36) {
                                v1_2 = this.parseName(v0_5.value, this.origin);
                                if ((this.last != null) && (v1_2.equals(this.last.getName()))) {
                                    v1_2 = this.last.getName();
                                }
                            } else {
                                org.xbill.DNS.Record v0_12 = v0_5.value;
                                if (!v0_12.equalsIgnoreCase("$ORIGIN")) {
                                    if (!v0_12.equalsIgnoreCase("$TTL")) {
                                        if (!v0_12.equalsIgnoreCase("$INCLUDE")) {
                                            if (!v0_12.equalsIgnoreCase("$GENERATE")) {
                                                throw this.st.exception(new StringBuffer("Invalid directive: ").append(v0_12).toString());
                                            } else {
                                                if (this.generator == null) {
                                                    this.startGenerate();
                                                    if (!this.noExpandGenerate) {
                                                        v0_2 = this.nextGenerated();
                                                    }
                                                } else {
                                                    throw new IllegalStateException("cannot nest $GENERATE");
                                                }
                                            }
                                        } else {
                                            org.xbill.DNS.Record v0_23;
                                            org.xbill.DNS.Name v1_5 = this.st.getString();
                                            if (this.file == null) {
                                                v0_23 = new java.io.File(v1_5);
                                            } else {
                                                v0_23 = new java.io.File(this.file.getParent(), v1_5);
                                            }
                                            org.xbill.DNS.Name v1_6 = this.origin;
                                            org.xbill.DNS.Master v2_17 = this.st.get();
                                            if (v2_17.isString()) {
                                                v1_6 = this.parseName(v2_17.value, org.xbill.DNS.Name.root);
                                                this.st.getEOL();
                                            }
                                            this.included = new org.xbill.DNS.Master(v0_23, v1_6, this.defaultTTL);
                                            v0_2 = this.nextRecord();
                                        }
                                    } else {
                                        this.defaultTTL = this.st.getTTL();
                                        this.st.getEOL();
                                    }
                                } else {
                                    this.origin = this.st.getName(org.xbill.DNS.Name.root);
                                    this.st.getEOL();
                                }
                                return v0_2;
                            }
                        } else {
                            v0_2 = 0;
                            return v0_2;
                        }
                    }
                } else {
                    org.xbill.DNS.Record v0_32 = this.st.get();
                }
                this.parseTTLClassAndType();
                this.last = org.xbill.DNS.Record.fromString(v1_2, this.currentType, this.currentDClass, this.currentTTL, this.st, this.origin);
                if (this.needSOATTL) {
                    org.xbill.DNS.Record v0_41 = ((org.xbill.DNS.SOARecord) this.last).getMinimum();
                    this.last.setTTL(v0_41);
                    this.defaultTTL = v0_41;
                    this.needSOATTL = 0;
                }
                v0_2 = this.last;
            } while(v0_32.type == 1);
            if (v0_32.type != 0) {
                this.st.unget();
                if (this.last != null) {
                    v1_2 = this.last.getName();
                } else {
                    throw this.st.exception("no owner");
                }
            } else {
                v0_2 = 0;
                return v0_2;
            }
        } else {
            v0_2 = this.included.nextRecord();
            if (v0_2 == null) {
                this.included = 0;
            }
        }
        return v0_2;
    }

    public void expandGenerate(boolean p2)
    {
        int v0;
        if (p2) {
            v0 = 0;
        } else {
            v0 = 1;
        }
        this.noExpandGenerate = v0;
        return;
    }

    protected void finalize()
    {
        if (this.st != null) {
            this.st.close();
        }
        return;
    }

    public java.util.Iterator generators()
    {
        java.util.Iterator v0_2;
        if (this.generators == null) {
            v0_2 = java.util.Collections.EMPTY_LIST.iterator();
        } else {
            v0_2 = java.util.Collections.unmodifiableList(this.generators).iterator();
        }
        return v0_2;
    }

    public org.xbill.DNS.Record nextRecord()
    {
        try {
            Throwable v0_0 = this._nextRecord();
        } catch (Throwable v0_1) {
            this.st.close();
            throw v0_1;
        }
        if (v0_0 == null) {
            this.st.close();
        }
        return v0_0;
    }
}
