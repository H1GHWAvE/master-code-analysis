package org.xbill.DNS;
public class TSIG$StreamVerifier {
    private org.xbill.DNS.TSIG key;
    private org.xbill.DNS.TSIGRecord lastTSIG;
    private int lastsigned;
    private int nresponses;
    private org.xbill.DNS.utils.HMAC verifier;

    public TSIG$StreamVerifier(org.xbill.DNS.TSIG p5, org.xbill.DNS.TSIGRecord p6)
    {
        this.key = p5;
        this.verifier = new org.xbill.DNS.utils.HMAC(org.xbill.DNS.TSIG.access$000(this.key), org.xbill.DNS.TSIG.access$100(this.key), org.xbill.DNS.TSIG.access$200(this.key));
        this.nresponses = 0;
        this.lastTSIG = p6;
        return;
    }

    public int verify(org.xbill.DNS.Message p12, byte[] p13)
    {
        int v0_14;
        byte[] v3_0 = p12.getTSIG();
        this.nresponses = (this.nresponses + 1);
        if (this.nresponses != 1) {
            if (v3_0 != null) {
                p12.getHeader().decCount(3);
            }
            org.xbill.DNS.utils.HMAC v4_0 = p12.getHeader().toWire();
            if (v3_0 != null) {
                p12.getHeader().incCount(3);
            }
            int v0_8;
            this.verifier.update(v4_0);
            if (v3_0 != null) {
                v0_8 = (p12.tsigstart - v4_0.length);
            } else {
                v0_8 = (p13.length - v4_0.length);
            }
            this.verifier.update(p13, v4_0.length, v0_8);
            if (v3_0 == null) {
                int v0_12;
                if ((this.nresponses - this.lastsigned) < 100) {
                    v0_12 = 0;
                } else {
                    v0_12 = 1;
                }
                if (v0_12 == 0) {
                    p12.tsigState = 2;
                    v0_14 = 0;
                } else {
                    p12.tsigState = 4;
                    v0_14 = 1;
                }
            } else {
                this.lastsigned = this.nresponses;
                this.lastTSIG = v3_0;
                if ((v3_0.getName().equals(org.xbill.DNS.TSIG.access$300(this.key))) && (v3_0.getAlgorithm().equals(org.xbill.DNS.TSIG.access$400(this.key)))) {
                    int v0_21 = new org.xbill.DNS.DNSOutput();
                    org.xbill.DNS.utils.HMAC v4_8 = (v3_0.getTimeSigned().getTime() / 1000);
                    org.xbill.DNS.utils.HMAC v4_9 = (v4_8 & 2.1219957905e-314);
                    v0_21.writeU16(((int) (v4_8 >> 32)));
                    v0_21.writeU32(v4_9);
                    v0_21.writeU16(v3_0.getFudge());
                    this.verifier.update(v0_21.toByteArray());
                    if (this.verifier.verify(v3_0.getSignature())) {
                        this.verifier.clear();
                        int v0_27 = new org.xbill.DNS.DNSOutput();
                        v0_27.writeU16(v3_0.getSignature().length);
                        this.verifier.update(v0_27.toByteArray());
                        this.verifier.update(v3_0.getSignature());
                        p12.tsigState = 1;
                        v0_14 = 0;
                    } else {
                        if (org.xbill.DNS.Options.check("verbose")) {
                            System.err.println("BADSIG failure");
                        }
                        p12.tsigState = 4;
                        v0_14 = 16;
                    }
                } else {
                    if (org.xbill.DNS.Options.check("verbose")) {
                        System.err.println("BADKEY failure");
                    }
                    p12.tsigState = 4;
                    v0_14 = 17;
                }
            }
        } else {
            v0_14 = this.key.verify(p12, p13, this.lastTSIG);
            if (v0_14 == 0) {
                String v1_4 = v3_0.getSignature();
                org.xbill.DNS.utils.HMAC v2_2 = new org.xbill.DNS.DNSOutput();
                v2_2.writeU16(v1_4.length);
                this.verifier.update(v2_2.toByteArray());
                this.verifier.update(v1_4);
            }
            this.lastTSIG = v3_0;
        }
        return v0_14;
    }
}
