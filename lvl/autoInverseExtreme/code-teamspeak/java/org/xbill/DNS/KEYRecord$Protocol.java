package org.xbill.DNS;
public class KEYRecord$Protocol {
    public static final int ANY = 255;
    public static final int DNSSEC = 3;
    public static final int EMAIL = 2;
    public static final int IPSEC = 4;
    public static final int NONE = 0;
    public static final int TLS = 1;
    private static org.xbill.DNS.Mnemonic protocols;

    static KEYRecord$Protocol()
    {
        org.xbill.DNS.Mnemonic v0_1 = new org.xbill.DNS.Mnemonic("KEY protocol", 2);
        org.xbill.DNS.KEYRecord$Protocol.protocols = v0_1;
        v0_1.setMaximum(255);
        org.xbill.DNS.KEYRecord$Protocol.protocols.setNumericAllowed(1);
        org.xbill.DNS.KEYRecord$Protocol.protocols.add(0, "NONE");
        org.xbill.DNS.KEYRecord$Protocol.protocols.add(1, "TLS");
        org.xbill.DNS.KEYRecord$Protocol.protocols.add(2, "EMAIL");
        org.xbill.DNS.KEYRecord$Protocol.protocols.add(3, "DNSSEC");
        org.xbill.DNS.KEYRecord$Protocol.protocols.add(4, "IPSEC");
        org.xbill.DNS.KEYRecord$Protocol.protocols.add(255, "ANY");
        return;
    }

    private KEYRecord$Protocol()
    {
        return;
    }

    public static String string(int p1)
    {
        return org.xbill.DNS.KEYRecord$Protocol.protocols.getText(p1);
    }

    public static int value(String p1)
    {
        return org.xbill.DNS.KEYRecord$Protocol.protocols.getValue(p1);
    }
}
