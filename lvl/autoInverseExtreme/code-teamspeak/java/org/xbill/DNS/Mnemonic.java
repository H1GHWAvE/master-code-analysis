package org.xbill.DNS;
 class Mnemonic {
    static final int CASE_LOWER = 3;
    static final int CASE_SENSITIVE = 1;
    static final int CASE_UPPER = 2;
    private static Integer[] cachedInts;
    private String description;
    private int max;
    private boolean numericok;
    private String prefix;
    private java.util.HashMap strings;
    private java.util.HashMap values;
    private int wordcase;

    static Mnemonic()
    {
        int v0_1 = new Integer[64];
        org.xbill.DNS.Mnemonic.cachedInts = v0_1;
        int v0_2 = 0;
        while (v0_2 < org.xbill.DNS.Mnemonic.cachedInts.length) {
            org.xbill.DNS.Mnemonic.cachedInts[v0_2] = new Integer(v0_2);
            v0_2++;
        }
        return;
    }

    public Mnemonic(String p2, int p3)
    {
        this.description = p2;
        this.wordcase = p3;
        this.strings = new java.util.HashMap();
        this.values = new java.util.HashMap();
        this.max = 2147483647;
        return;
    }

    private int parseNumeric(String p3)
    {
        try {
            int v0 = Integer.parseInt(p3);
        } catch (int v0) {
            v0 = -1;
            return v0;
        }
        if ((v0 < 0) || (v0 > this.max)) {
        } else {
            return v0;
        }
    }

    private String sanitize(String p3)
    {
        if (this.wordcase != 2) {
            if (this.wordcase == 3) {
                p3 = p3.toLowerCase();
            }
        } else {
            p3 = p3.toUpperCase();
        }
        return p3;
    }

    public static Integer toInteger(int p1)
    {
        if ((p1 < 0) || (p1 >= org.xbill.DNS.Mnemonic.cachedInts.length)) {
            Integer v0_3 = new Integer(p1);
        } else {
            v0_3 = org.xbill.DNS.Mnemonic.cachedInts[p1];
        }
        return v0_3;
    }

    public void add(int p4, String p5)
    {
        this.check(p4);
        Integer v0 = org.xbill.DNS.Mnemonic.toInteger(p4);
        String v1 = this.sanitize(p5);
        this.strings.put(v1, v0);
        this.values.put(v0, v1);
        return;
    }

    public void addAlias(int p4, String p5)
    {
        this.check(p4);
        this.strings.put(this.sanitize(p5), org.xbill.DNS.Mnemonic.toInteger(p4));
        return;
    }

    public void addAll(org.xbill.DNS.Mnemonic p4)
    {
        if (this.wordcase == p4.wordcase) {
            this.strings.putAll(p4.strings);
            this.values.putAll(p4.values);
            return;
        } else {
            throw new IllegalArgumentException(new StringBuffer().append(p4.description).append(": wordcases do not match").toString());
        }
    }

    public void check(int p4)
    {
        if ((p4 >= 0) && (p4 <= this.max)) {
            return;
        } else {
            throw new IllegalArgumentException(new StringBuffer().append(this.description).append(" ").append(p4).append("is out of range").toString());
        }
    }

    public String getText(int p4)
    {
        this.check(p4);
        String v0_2 = ((String) this.values.get(org.xbill.DNS.Mnemonic.toInteger(p4)));
        if (v0_2 == null) {
            v0_2 = Integer.toString(p4);
            if (this.prefix != null) {
                v0_2 = new StringBuffer().append(this.prefix).append(v0_2).toString();
            }
        }
        return v0_2;
    }

    public int getValue(String p3)
    {
        int v0_9;
        String v1 = this.sanitize(p3);
        int v0_2 = ((Integer) this.strings.get(v1));
        if (v0_2 == 0) {
            if ((this.prefix != null) && (v1.startsWith(this.prefix))) {
                v0_9 = this.parseNumeric(v1.substring(this.prefix.length()));
                if (v0_9 >= 0) {
                    return v0_9;
                }
            }
            if (!this.numericok) {
                v0_9 = -1;
            } else {
                v0_9 = this.parseNumeric(v1);
            }
        } else {
            v0_9 = v0_2.intValue();
        }
        return v0_9;
    }

    public void setMaximum(int p1)
    {
        this.max = p1;
        return;
    }

    public void setNumericAllowed(boolean p1)
    {
        this.numericok = p1;
        return;
    }

    public void setPrefix(String p2)
    {
        this.prefix = this.sanitize(p2);
        return;
    }
}
