package org.xbill.DNS;
 class Zone$ZoneIterator implements java.util.Iterator {
    private int count;
    private org.xbill.DNS.RRset[] current;
    private final org.xbill.DNS.Zone this$0;
    private boolean wantLastSOA;
    private java.util.Iterator zentries;

    Zone$ZoneIterator(org.xbill.DNS.Zone p9, boolean p10)
    {
        this.this$0 = p9;
        this.zentries = org.xbill.DNS.Zone.access$000(p9).entrySet().iterator();
        this.wantLastSOA = p10;
        org.xbill.DNS.RRset[] v5 = org.xbill.DNS.Zone.access$200(p9, org.xbill.DNS.Zone.access$100(p9));
        int v0_5 = new org.xbill.DNS.RRset[v5.length];
        this.current = v0_5;
        int v0_6 = 2;
        int v2 = 0;
        while (v2 < v5.length) {
            int v4_2 = v5[v2].getType();
            if (v4_2 != 6) {
                if (v4_2 != 2) {
                    int v4_3 = (v0_6 + 1);
                    this.current[v0_6] = v5[v2];
                    v0_6 = v4_3;
                } else {
                    this.current[1] = v5[v2];
                }
            } else {
                this.current[0] = v5[v2];
            }
            v2++;
        }
        return;
    }

    public boolean hasNext()
    {
        if ((this.current == null) && (!this.wantLastSOA)) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public Object next()
    {
        if (this.hasNext()) {
            org.xbill.DNS.RRset[] v0_12;
            if (this.current != null) {
                org.xbill.DNS.RRset[] v0_2 = this.current;
                org.xbill.DNS.RRset v1_0 = this.count;
                this.count = (v1_0 + 1);
                org.xbill.DNS.RRset v1_1 = v0_2[v1_0];
                if (this.count == this.current.length) {
                    this.current = 0;
                    while (this.zentries.hasNext()) {
                        org.xbill.DNS.RRset[] v0_9 = ((java.util.Map$Entry) this.zentries.next());
                        if (!v0_9.getKey().equals(org.xbill.DNS.Zone.access$400(this.this$0))) {
                            org.xbill.DNS.RRset[] v0_11 = org.xbill.DNS.Zone.access$200(this.this$0, v0_9.getValue());
                            if (v0_11.length != 0) {
                                this.current = v0_11;
                                this.count = 0;
                                break;
                            }
                        }
                    }
                }
                v0_12 = v1_1;
            } else {
                this.wantLastSOA = 0;
                v0_12 = org.xbill.DNS.Zone.access$300(this.this$0, org.xbill.DNS.Zone.access$100(this.this$0), 6);
            }
            return v0_12;
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public void remove()
    {
        throw new UnsupportedOperationException();
    }
}
