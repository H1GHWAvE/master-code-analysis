package org.xbill.DNS;
public class NSEC3PARAMRecord extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 9757705474933235083;
    private int flags;
    private int hashAlg;
    private int iterations;
    private byte[] salt;

    NSEC3PARAMRecord()
    {
        return;
    }

    public NSEC3PARAMRecord(org.xbill.DNS.Name p8, int p9, long p10, int p12, int p13, int p14, byte[] p15)
    {
        this(p8, 51, p9, p10);
        this.hashAlg = org.xbill.DNS.NSEC3PARAMRecord.checkU8("hashAlg", p12);
        this.flags = org.xbill.DNS.NSEC3PARAMRecord.checkU8("flags", p13);
        this.iterations = org.xbill.DNS.NSEC3PARAMRecord.checkU16("iterations", p14);
        if (p15 != null) {
            if (p15.length <= 255) {
                if (p15.length > 0) {
                    byte[] v0_10 = new byte[p15.length];
                    this.salt = v0_10;
                    System.arraycopy(p15, 0, this.salt, 0, p15.length);
                }
            } else {
                throw new IllegalArgumentException("Invalid salt length");
            }
        }
        return;
    }

    public int getFlags()
    {
        return this.flags;
    }

    public int getHashAlgorithm()
    {
        return this.hashAlg;
    }

    public int getIterations()
    {
        return this.iterations;
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.NSEC3PARAMRecord();
    }

    public byte[] getSalt()
    {
        return this.salt;
    }

    public byte[] hashName(org.xbill.DNS.Name p4)
    {
        return org.xbill.DNS.NSEC3Record.hashName(p4, this.hashAlg, this.iterations, this.salt);
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p3, org.xbill.DNS.Name p4)
    {
        this.hashAlg = p3.getUInt8();
        this.flags = p3.getUInt8();
        this.iterations = p3.getUInt16();
        if (!p3.getString().equals("-")) {
            p3.unget();
            this.salt = p3.getHexString();
            if (this.salt.length > 255) {
                throw p3.exception("salt value too long");
            }
        } else {
            this.salt = 0;
        }
        return;
    }

    void rrFromWire(org.xbill.DNS.DNSInput p2)
    {
        this.hashAlg = p2.readU8();
        this.flags = p2.readU8();
        this.iterations = p2.readU16();
        int v0_3 = p2.readU8();
        if (v0_3 <= 0) {
            this.salt = 0;
        } else {
            this.salt = p2.readByteArray(v0_3);
        }
        return;
    }

    String rrToString()
    {
        String v0_1 = new StringBuffer();
        v0_1.append(this.hashAlg);
        v0_1.append(32);
        v0_1.append(this.flags);
        v0_1.append(32);
        v0_1.append(this.iterations);
        v0_1.append(32);
        if (this.salt != null) {
            v0_1.append(org.xbill.DNS.utils.base16.toString(this.salt));
        } else {
            v0_1.append(45);
        }
        return v0_1.toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p2, org.xbill.DNS.Compression p3, boolean p4)
    {
        p2.writeU8(this.hashAlg);
        p2.writeU8(this.flags);
        p2.writeU16(this.iterations);
        if (this.salt == null) {
            p2.writeU8(0);
        } else {
            p2.writeU8(this.salt.length);
            p2.writeByteArray(this.salt);
        }
        return;
    }
}
