package org.xbill.DNS;
public class DHCIDRecord extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 10231923872900553909;
    private byte[] data;

    DHCIDRecord()
    {
        return;
    }

    public DHCIDRecord(org.xbill.DNS.Name p8, int p9, long p10, byte[] p12)
    {
        this(p8, 49, p9, p10);
        this.data = p12;
        return;
    }

    public byte[] getData()
    {
        return this.data;
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.DHCIDRecord();
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p2, org.xbill.DNS.Name p3)
    {
        this.data = p2.getBase64();
        return;
    }

    void rrFromWire(org.xbill.DNS.DNSInput p2)
    {
        this.data = p2.readByteArray();
        return;
    }

    String rrToString()
    {
        return org.xbill.DNS.utils.base64.toString(this.data);
    }

    void rrToWire(org.xbill.DNS.DNSOutput p2, org.xbill.DNS.Compression p3, boolean p4)
    {
        p2.writeByteArray(this.data);
        return;
    }
}
