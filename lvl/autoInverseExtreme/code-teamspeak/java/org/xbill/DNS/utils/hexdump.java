package org.xbill.DNS.utils;
public class hexdump {
    private static final char[] hex;

    static hexdump()
    {
        org.xbill.DNS.utils.hexdump.hex = "0123456789ABCDEF".toCharArray();
        return;
    }

    public hexdump()
    {
        return;
    }

    public static String dump(String p2, byte[] p3)
    {
        return org.xbill.DNS.utils.hexdump.dump(p2, p3, 0, p3.length);
    }

    public static String dump(String p10, byte[] p11, int p12, int p13)
    {
        StringBuffer v3_1 = new StringBuffer();
        v3_1.append(new StringBuffer().append(p13).append("b").toString());
        if (p10 != null) {
            v3_1.append(new StringBuffer(" (").append(p10).append(")").toString());
        }
        v3_1.append(58);
        int v4 = ((v3_1.toString().length() + 8) & -8);
        v3_1.append(9);
        int v5 = ((80 - v4) / 3);
        int v2_3 = 0;
        while (v2_3 < p13) {
            if ((v2_3 != 0) && ((v2_3 % v5) == 0)) {
                v3_1.append(10);
                int v0_17 = 0;
                while (v0_17 < (v4 / 8)) {
                    v3_1.append(9);
                    v0_17++;
                }
            }
            int v0_20 = (p11[(v2_3 + p12)] & 255);
            v3_1.append(org.xbill.DNS.utils.hexdump.hex[(v0_20 >> 4)]);
            v3_1.append(org.xbill.DNS.utils.hexdump.hex[(v0_20 & 15)]);
            v3_1.append(32);
            v2_3++;
        }
        v3_1.append(10);
        return v3_1.toString();
    }
}
