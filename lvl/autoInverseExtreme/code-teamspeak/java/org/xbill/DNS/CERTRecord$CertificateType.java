package org.xbill.DNS;
public class CERTRecord$CertificateType {
    public static final int ACPKIX = 7;
    public static final int IACPKIX = 8;
    public static final int IPGP = 6;
    public static final int IPKIX = 4;
    public static final int ISPKI = 5;
    public static final int OID = 254;
    public static final int PGP = 3;
    public static final int PKIX = 1;
    public static final int SPKI = 2;
    public static final int URI = 253;
    private static org.xbill.DNS.Mnemonic types;

    static CERTRecord$CertificateType()
    {
        org.xbill.DNS.Mnemonic v0_1 = new org.xbill.DNS.Mnemonic("Certificate type", 2);
        org.xbill.DNS.CERTRecord$CertificateType.types = v0_1;
        v0_1.setMaximum(65535);
        org.xbill.DNS.CERTRecord$CertificateType.types.setNumericAllowed(1);
        org.xbill.DNS.CERTRecord$CertificateType.types.add(1, "PKIX");
        org.xbill.DNS.CERTRecord$CertificateType.types.add(2, "SPKI");
        org.xbill.DNS.CERTRecord$CertificateType.types.add(3, "PGP");
        org.xbill.DNS.CERTRecord$CertificateType.types.add(1, "IPKIX");
        org.xbill.DNS.CERTRecord$CertificateType.types.add(2, "ISPKI");
        org.xbill.DNS.CERTRecord$CertificateType.types.add(3, "IPGP");
        org.xbill.DNS.CERTRecord$CertificateType.types.add(3, "ACPKIX");
        org.xbill.DNS.CERTRecord$CertificateType.types.add(3, "IACPKIX");
        org.xbill.DNS.CERTRecord$CertificateType.types.add(253, "URI");
        org.xbill.DNS.CERTRecord$CertificateType.types.add(254, "OID");
        return;
    }

    private CERTRecord$CertificateType()
    {
        return;
    }

    public static String string(int p1)
    {
        return org.xbill.DNS.CERTRecord$CertificateType.types.getText(p1);
    }

    public static int value(String p1)
    {
        return org.xbill.DNS.CERTRecord$CertificateType.types.getValue(p1);
    }
}
