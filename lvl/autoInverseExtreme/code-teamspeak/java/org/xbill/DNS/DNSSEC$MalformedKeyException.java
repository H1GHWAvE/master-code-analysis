package org.xbill.DNS;
public class DNSSEC$MalformedKeyException extends org.xbill.DNS.DNSSEC$DNSSECException {

    DNSSEC$MalformedKeyException(org.xbill.DNS.KEYBase p3)
    {
        this(new StringBuffer("Invalid key data: ").append(p3.rdataToString()).toString());
        return;
    }
}
