package org.xbill.DNS;
public class KXRecord extends org.xbill.DNS.U16NameBase {
    private static final long serialVersionUID = 7448568832769757809;

    KXRecord()
    {
        return;
    }

    public KXRecord(org.xbill.DNS.Name p12, int p13, long p14, int p16, org.xbill.DNS.Name p17)
    {
        this(p12, 36, p13, p14, p16, "preference", p17, "target");
        return;
    }

    public org.xbill.DNS.Name getAdditionalName()
    {
        return this.getNameField();
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.KXRecord();
    }

    public int getPreference()
    {
        return this.getU16Field();
    }

    public org.xbill.DNS.Name getTarget()
    {
        return this.getNameField();
    }
}
