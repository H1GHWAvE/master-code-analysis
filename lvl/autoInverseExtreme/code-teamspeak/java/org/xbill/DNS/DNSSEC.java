package org.xbill.DNS;
public class DNSSEC {
    private static final int ASN1_INT = 2;
    private static final int ASN1_SEQ = 48;
    private static final int DSA_LEN = 20;
    private static final org.xbill.DNS.DNSSEC$ECKeyInfo ECDSA_P256;
    private static final org.xbill.DNS.DNSSEC$ECKeyInfo ECDSA_P384;
    private static final org.xbill.DNS.DNSSEC$ECKeyInfo GOST;

    static DNSSEC()
    {
        org.xbill.DNS.DNSSEC.GOST = new org.xbill.DNS.DNSSEC$ECKeyInfo(32, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD97", "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD94", "A6", "1", "8D91E471E0989CDA27DF505A453F2B7635294F2DDF23E3B122ACC99C9E9F1E14", "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6C611070995AD10045841B09B761B893");
        org.xbill.DNS.DNSSEC.ECDSA_P256 = new org.xbill.DNS.DNSSEC$ECKeyInfo(32, "FFFFFFFF00000001000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF", "FFFFFFFF00000001000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFC", "5AC635D8AA3A93E7B3EBBD55769886BC651D06B0CC53B0F63BCE3C3E27D2604B", "6B17D1F2E12C4247F8BCE6E563A440F277037D812DEB33A0F4A13945D898C296", "4FE342E2FE1A7F9B8EE7EB4A7C0F9E162BCE33576B315ECECBB6406837BF51F5", "FFFFFFFF00000000FFFFFFFFFFFFFFFFBCE6FAADA7179E84F3B9CAC2FC632551");
        org.xbill.DNS.DNSSEC.ECDSA_P384 = new org.xbill.DNS.DNSSEC$ECKeyInfo(48, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFF0000000000000000FFFFFFFF", "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFF0000000000000000FFFFFFFC", "B3312FA7E23EE7E4988E056BE3F82D19181D9C6EFE8141120314088F5013875AC656398D8A2ED19D2A85C8EDD3EC2AEF", "AA87CA22BE8B05378EB1C71EF320AD746E1D3B628BA79B9859F741E082542A385502F25DBF55296C3A545E3872760AB7", "3617DE4A96262C6F5D9E98BF9292DC29F8F41DBD289A147CE9DA3113B5F0B8C00A60B1CE1D7E819D7A431D7C90EA0E5F", "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC7634D81F4372DDF581A0DB248B0A77AECEC196ACCC52973");
        return;
    }

    private DNSSEC()
    {
        return;
    }

    private static int BigIntegerLength(java.math.BigInteger p1)
    {
        return ((p1.bitLength() + 7) / 8);
    }

    private static byte[] DSASignaturefromDNS(byte[] p9)
    {
        int v1 = 21;
        if (p9.length == 41) {
            byte[] v0_2;
            byte[] v3_2 = new org.xbill.DNS.DNSInput(p9);
            org.xbill.DNS.DNSOutput v4_1 = new org.xbill.DNS.DNSOutput();
            v3_2.readU8();
            byte[] v5 = v3_2.readByteArray(20);
            if (v5[0] >= 0) {
                v0_2 = 20;
            } else {
                v0_2 = 21;
            }
            byte[] v3_3 = v3_2.readByteArray(20);
            if (v3_3[0] >= 0) {
                v1 = 20;
            }
            v4_1.writeU8(48);
            v4_1.writeU8(((v0_2 + v1) + 4));
            v4_1.writeU8(2);
            v4_1.writeU8(v0_2);
            if (v0_2 > 20) {
                v4_1.writeU8(0);
            }
            v4_1.writeByteArray(v5);
            v4_1.writeU8(2);
            v4_1.writeU8(v1);
            if (v1 > 20) {
                v4_1.writeU8(0);
            }
            v4_1.writeByteArray(v3_3);
            return v4_1.toByteArray();
        } else {
            throw new org.xbill.DNS.DNSSEC$SignatureVerificationException();
        }
    }

    private static byte[] DSASignaturetoDNS(byte[] p7, int p8)
    {
        java.io.IOException v0_1 = new org.xbill.DNS.DNSInput(p7);
        org.xbill.DNS.DNSOutput v1_1 = new org.xbill.DNS.DNSOutput();
        v1_1.writeU8(p8);
        if (v0_1.readU8() == 48) {
            v0_1.readU8();
            if (v0_1.readU8() == 2) {
                int v2_2 = v0_1.readU8();
                if (v2_2 != 21) {
                    if (v2_2 != 20) {
                        throw new java.io.IOException();
                    }
                } else {
                    if (v0_1.readU8() != 0) {
                        throw new java.io.IOException();
                    }
                }
                v1_1.writeByteArray(v0_1.readByteArray(20));
                if (v0_1.readU8() == 2) {
                    int v2_6 = v0_1.readU8();
                    if (v2_6 != 21) {
                        if (v2_6 != 20) {
                            throw new java.io.IOException();
                        }
                    } else {
                        if (v0_1.readU8() != 0) {
                            throw new java.io.IOException();
                        }
                    }
                    v1_1.writeByteArray(v0_1.readByteArray(20));
                    return v1_1.toByteArray();
                } else {
                    throw new java.io.IOException();
                }
            } else {
                throw new java.io.IOException();
            }
        } else {
            throw new java.io.IOException();
        }
    }

    private static byte[] ECDSASignaturefromDNS(byte[] p8, org.xbill.DNS.DNSSEC$ECKeyInfo p9)
    {
        if (p8.length == (p9.length * 2)) {
            int v1_3 = new org.xbill.DNS.DNSInput(p8);
            org.xbill.DNS.DNSOutput v2_1 = new org.xbill.DNS.DNSOutput();
            byte[] v3 = v1_3.readByteArray(p9.length);
            byte[] v0_2 = p9.length;
            if (v3[0] < 0) {
                v0_2++;
            }
            byte[] v4_2 = v1_3.readByteArray(p9.length);
            int v1_4 = p9.length;
            if (v4_2[0] < 0) {
                v1_4++;
            }
            v2_1.writeU8(48);
            v2_1.writeU8(((v0_2 + v1_4) + 4));
            v2_1.writeU8(2);
            v2_1.writeU8(v0_2);
            if (v0_2 > p9.length) {
                v2_1.writeU8(0);
            }
            v2_1.writeByteArray(v3);
            v2_1.writeU8(2);
            v2_1.writeU8(v1_4);
            if (v1_4 > p9.length) {
                v2_1.writeU8(0);
            }
            v2_1.writeByteArray(v4_2);
            return v2_1.toByteArray();
        } else {
            throw new org.xbill.DNS.DNSSEC$SignatureVerificationException();
        }
    }

    private static byte[] ECDSASignaturetoDNS(byte[] p5, org.xbill.DNS.DNSSEC$ECKeyInfo p6)
    {
        java.io.IOException v0_1 = new org.xbill.DNS.DNSInput(p5);
        org.xbill.DNS.DNSOutput v1_1 = new org.xbill.DNS.DNSOutput();
        if (v0_1.readU8() == 48) {
            v0_1.readU8();
            if (v0_1.readU8() == 2) {
                int v2_2 = v0_1.readU8();
                if (v2_2 != (p6.length + 1)) {
                    if (v2_2 != p6.length) {
                        throw new java.io.IOException();
                    }
                } else {
                    if (v0_1.readU8() != 0) {
                        throw new java.io.IOException();
                    }
                }
                v1_1.writeByteArray(v0_1.readByteArray(p6.length));
                if (v0_1.readU8() == 2) {
                    int v2_7 = v0_1.readU8();
                    if (v2_7 != (p6.length + 1)) {
                        if (v2_7 != p6.length) {
                            throw new java.io.IOException();
                        }
                    } else {
                        if (v0_1.readU8() != 0) {
                            throw new java.io.IOException();
                        }
                    }
                    v1_1.writeByteArray(v0_1.readByteArray(p6.length));
                    return v1_1.toByteArray();
                } else {
                    throw new java.io.IOException();
                }
            } else {
                throw new java.io.IOException();
            }
        } else {
            throw new java.io.IOException();
        }
    }

    private static byte[] ECGOSTSignaturefromDNS(byte[] p2, org.xbill.DNS.DNSSEC$ECKeyInfo p3)
    {
        if (p2.length == (p3.length * 2)) {
            return p2;
        } else {
            throw new org.xbill.DNS.DNSSEC$SignatureVerificationException();
        }
    }

    public static String algString(int p1)
    {
        String v0_0;
        switch (p1) {
            case 1:
                v0_0 = "MD5withRSA";
                break;
            case 2:
            case 4:
            case 9:
            case 11:
            default:
                throw new org.xbill.DNS.DNSSEC$UnsupportedAlgorithmException(p1);
                break;
            case 3:
            case 6:
                v0_0 = "SHA1withDSA";
                break;
            case 5:
            case 7:
                v0_0 = "SHA1withRSA";
                break;
            case 8:
                v0_0 = "SHA256withRSA";
                break;
            case 10:
                v0_0 = "SHA512withRSA";
                break;
            case 12:
                v0_0 = "GOST3411withECGOST3410";
                break;
            case 13:
                v0_0 = "SHA256withECDSA";
                break;
            case 14:
                v0_0 = "SHA384withECDSA";
                break;
        }
        return v0_0;
    }

    static void checkAlgorithm(java.security.PrivateKey p1, int p2)
    {
        switch (p2) {
            case 1:
            case 5:
            case 7:
            case 8:
            case 10:
                if ((p1 instanceof java.security.interfaces.RSAPrivateKey)) {
                } else {
                    throw new org.xbill.DNS.DNSSEC$IncompatibleKeyException();
                }
                break;
            case 2:
            case 4:
            case 9:
            case 11:
            default:
                throw new org.xbill.DNS.DNSSEC$UnsupportedAlgorithmException(p2);
                break;
            case 3:
            case 6:
                if ((p1 instanceof java.security.interfaces.DSAPrivateKey)) {
                } else {
                    throw new org.xbill.DNS.DNSSEC$IncompatibleKeyException();
                }
                break;
            case 12:
            case 13:
            case 14:
                if ((p1 instanceof java.security.interfaces.ECPrivateKey)) {
                } else {
                    throw new org.xbill.DNS.DNSSEC$IncompatibleKeyException();
                }
                break;
        }
        return;
    }

    public static byte[] digestMessage(org.xbill.DNS.SIGRecord p1, org.xbill.DNS.Message p2, byte[] p3)
    {
        byte[] v0_1 = new org.xbill.DNS.DNSOutput();
        org.xbill.DNS.DNSSEC.digestSIG(v0_1, p1);
        if (p3 != null) {
            v0_1.writeByteArray(p3);
        }
        p2.toWire(v0_1);
        return v0_1.toByteArray();
    }

    public static byte[] digestRRset(org.xbill.DNS.RRSIGRecord p10, org.xbill.DNS.RRset p11)
    {
        byte[] v0_1;
        int v1_1;
        org.xbill.DNS.DNSOutput v4_1 = new org.xbill.DNS.DNSOutput();
        org.xbill.DNS.DNSSEC.digestSIG(v4_1, p10);
        int v1_0 = p11.size();
        org.xbill.DNS.Record[] v5 = new org.xbill.DNS.Record[v1_0];
        int v6_0 = p11.rrs();
        org.xbill.DNS.Name v7 = p11.getName();
        org.xbill.DNS.DNSOutput v2_1 = (p10.getLabels() + 1);
        if (v7.labels() <= v2_1) {
            v0_1 = v1_0;
            v1_1 = 0;
        } else {
            v0_1 = v1_0;
            v1_1 = v7.wild((v7.labels() - v2_1));
        }
        while (v6_0.hasNext()) {
            org.xbill.DNS.DNSOutput v2_5 = (v0_1 - 1);
            v5[v2_5] = ((org.xbill.DNS.Record) v6_0.next());
            v0_1 = v2_5;
        }
        java.util.Arrays.sort(v5);
        org.xbill.DNS.DNSOutput v2_4 = new org.xbill.DNS.DNSOutput();
        if (v1_1 == 0) {
            v7.toWireCanonical(v2_4);
        } else {
            v1_1.toWireCanonical(v2_4);
        }
        v2_4.writeU16(p11.getType());
        v2_4.writeU16(p11.getDClass());
        v2_4.writeU32(p10.getOrigTTL());
        byte[] v0_8 = 0;
        while (v0_8 < v5.length) {
            v4_1.writeByteArray(v2_4.toByteArray());
            int v1_4 = v4_1.current();
            v4_1.writeU16(0);
            v4_1.writeByteArray(v5[v0_8].rdataToWireCanonical());
            int v6_5 = ((v4_1.current() - v1_4) - 2);
            v4_1.save();
            v4_1.jump(v1_4);
            v4_1.writeU16(v6_5);
            v4_1.restore();
            v0_8++;
        }
        return v4_1.toByteArray();
    }

    private static void digestSIG(org.xbill.DNS.DNSOutput p4, org.xbill.DNS.SIGBase p5)
    {
        p4.writeU16(p5.getTypeCovered());
        p4.writeU8(p5.getAlgorithm());
        p4.writeU8(p5.getLabels());
        p4.writeU32(p5.getOrigTTL());
        p4.writeU32((p5.getExpire().getTime() / 1000));
        p4.writeU32((p5.getTimeSigned().getTime() / 1000));
        p4.writeU16(p5.getFootprint());
        p5.getSigner().toWireCanonical(p4);
        return;
    }

    private static byte[] fromDSAPublicKey(java.security.interfaces.DSAPublicKey p6)
    {
        byte[] v0_1 = new org.xbill.DNS.DNSOutput();
        int v1_1 = p6.getParams().getQ();
        java.math.BigInteger v2_1 = p6.getParams().getP();
        java.math.BigInteger v3_1 = p6.getParams().getG();
        java.math.BigInteger v4 = p6.getY();
        int v5_3 = ((v2_1.toByteArray().length - 64) / 8);
        v0_1.writeU8(v5_3);
        org.xbill.DNS.DNSSEC.writeBigInteger(v0_1, v1_1);
        org.xbill.DNS.DNSSEC.writeBigInteger(v0_1, v2_1);
        org.xbill.DNS.DNSSEC.writePaddedBigInteger(v0_1, v3_1, ((v5_3 * 8) + 64));
        org.xbill.DNS.DNSSEC.writePaddedBigInteger(v0_1, v4, ((v5_3 * 8) + 64));
        return v0_1.toByteArray();
    }

    private static byte[] fromECDSAPublicKey(java.security.interfaces.ECPublicKey p4, org.xbill.DNS.DNSSEC$ECKeyInfo p5)
    {
        byte[] v0_1 = new org.xbill.DNS.DNSOutput();
        int v1_1 = p4.getW().getAffineX();
        java.math.BigInteger v2_1 = p4.getW().getAffineY();
        org.xbill.DNS.DNSSEC.writePaddedBigInteger(v0_1, v1_1, p5.length);
        org.xbill.DNS.DNSSEC.writePaddedBigInteger(v0_1, v2_1, p5.length);
        return v0_1.toByteArray();
    }

    private static byte[] fromECGOSTPublicKey(java.security.interfaces.ECPublicKey p4, org.xbill.DNS.DNSSEC$ECKeyInfo p5)
    {
        byte[] v0_1 = new org.xbill.DNS.DNSOutput();
        int v1_1 = p4.getW().getAffineX();
        java.math.BigInteger v2_1 = p4.getW().getAffineY();
        org.xbill.DNS.DNSSEC.writePaddedBigIntegerLittleEndian(v0_1, v1_1, p5.length);
        org.xbill.DNS.DNSSEC.writePaddedBigIntegerLittleEndian(v0_1, v2_1, p5.length);
        return v0_1.toByteArray();
    }

    static byte[] fromPublicKey(java.security.PublicKey p1, int p2)
    {
        byte[] v0_2;
        switch (p2) {
            case 1:
            case 5:
            case 7:
            case 8:
            case 10:
                if ((p1 instanceof java.security.interfaces.RSAPublicKey)) {
                    v0_2 = org.xbill.DNS.DNSSEC.fromRSAPublicKey(((java.security.interfaces.RSAPublicKey) p1));
                } else {
                    throw new org.xbill.DNS.DNSSEC$IncompatibleKeyException();
                }
                break;
            case 2:
            case 4:
            case 9:
            case 11:
            default:
                throw new org.xbill.DNS.DNSSEC$UnsupportedAlgorithmException(p2);
                break;
            case 3:
            case 6:
                if ((p1 instanceof java.security.interfaces.DSAPublicKey)) {
                    v0_2 = org.xbill.DNS.DNSSEC.fromDSAPublicKey(((java.security.interfaces.DSAPublicKey) p1));
                } else {
                    throw new org.xbill.DNS.DNSSEC$IncompatibleKeyException();
                }
                break;
            case 12:
                if ((p1 instanceof java.security.interfaces.ECPublicKey)) {
                    v0_2 = org.xbill.DNS.DNSSEC.fromECGOSTPublicKey(((java.security.interfaces.ECPublicKey) p1), org.xbill.DNS.DNSSEC.GOST);
                } else {
                    throw new org.xbill.DNS.DNSSEC$IncompatibleKeyException();
                }
                break;
            case 13:
                if ((p1 instanceof java.security.interfaces.ECPublicKey)) {
                    v0_2 = org.xbill.DNS.DNSSEC.fromECDSAPublicKey(((java.security.interfaces.ECPublicKey) p1), org.xbill.DNS.DNSSEC.ECDSA_P256);
                } else {
                    throw new org.xbill.DNS.DNSSEC$IncompatibleKeyException();
                }
                break;
            case 14:
                if ((p1 instanceof java.security.interfaces.ECPublicKey)) {
                    v0_2 = org.xbill.DNS.DNSSEC.fromECDSAPublicKey(((java.security.interfaces.ECPublicKey) p1), org.xbill.DNS.DNSSEC.ECDSA_P384);
                } else {
                    throw new org.xbill.DNS.DNSSEC$IncompatibleKeyException();
                }
                break;
        }
        return v0_2;
    }

    private static byte[] fromRSAPublicKey(java.security.interfaces.RSAPublicKey p5)
    {
        byte[] v0_1 = new org.xbill.DNS.DNSOutput();
        java.math.BigInteger v1 = p5.getPublicExponent();
        java.math.BigInteger v2 = p5.getModulus();
        int v3 = org.xbill.DNS.DNSSEC.BigIntegerLength(v1);
        if (v3 >= 256) {
            v0_1.writeU8(0);
            v0_1.writeU16(v3);
        } else {
            v0_1.writeU8(v3);
        }
        org.xbill.DNS.DNSSEC.writeBigInteger(v0_1, v1);
        org.xbill.DNS.DNSSEC.writeBigInteger(v0_1, v2);
        return v0_1.toByteArray();
    }

    static byte[] generateDSDigest(org.xbill.DNS.DNSKEYRecord p3, int p4)
    {
        try {
            byte[] v0_1;
            switch (p4) {
                case 1:
                    v0_1 = java.security.MessageDigest.getInstance("sha-1");
                    v0_1.update(p3.getName().toWireCanonical());
                    v0_1.update(p3.rdataToWireCanonical());
                    return v0_1.digest();
                case 2:
                    v0_1 = java.security.MessageDigest.getInstance("sha-256");
                    v0_1.update(p3.getName().toWireCanonical());
                    v0_1.update(p3.rdataToWireCanonical());
                    return v0_1.digest();
                case 3:
                    v0_1 = java.security.MessageDigest.getInstance("GOST3411");
                    v0_1.update(p3.getName().toWireCanonical());
                    v0_1.update(p3.rdataToWireCanonical());
                    return v0_1.digest();
                case 4:
                    v0_1 = java.security.MessageDigest.getInstance("sha-384");
                    v0_1.update(p3.getName().toWireCanonical());
                    v0_1.update(p3.rdataToWireCanonical());
                    return v0_1.digest();
                default:
                    throw new IllegalArgumentException(new StringBuffer("unknown DS digest type ").append(p4).toString());
            }
        } catch (byte[] v0) {
        }
        throw new IllegalStateException("no message digest support");
    }

    private static boolean matches(org.xbill.DNS.SIGBase p2, org.xbill.DNS.KEYBase p3)
    {
        if ((p3.getAlgorithm() != p2.getAlgorithm()) || ((p3.getFootprint() != p2.getFootprint()) || (!p3.getName().equals(p2.getSigner())))) {
            int v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    private static java.math.BigInteger readBigInteger(org.xbill.DNS.DNSInput p3)
    {
        return new java.math.BigInteger(1, p3.readByteArray());
    }

    private static java.math.BigInteger readBigInteger(org.xbill.DNS.DNSInput p3, int p4)
    {
        return new java.math.BigInteger(1, p3.readByteArray(p4));
    }

    private static java.math.BigInteger readBigIntegerLittleEndian(org.xbill.DNS.DNSInput p3, int p4)
    {
        byte[] v0 = p3.readByteArray(p4);
        org.xbill.DNS.DNSSEC.reverseByteArray(v0);
        return new java.math.BigInteger(1, v0);
    }

    private static void reverseByteArray(byte[] p4)
    {
        int v0 = 0;
        while (v0 < (p4.length / 2)) {
            int v1_4 = ((p4.length - v0) - 1);
            byte v2 = p4[v0];
            p4[v0] = p4[v1_4];
            p4[v1_4] = v2;
            v0++;
        }
        return;
    }

    public static org.xbill.DNS.RRSIGRecord sign(org.xbill.DNS.RRset p6, org.xbill.DNS.DNSKEYRecord p7, java.security.PrivateKey p8, java.util.Date p9, java.util.Date p10)
    {
        return org.xbill.DNS.DNSSEC.sign(p6, p7, p8, p9, p10, 0);
    }

    public static org.xbill.DNS.RRSIGRecord sign(org.xbill.DNS.RRset p17, org.xbill.DNS.DNSKEYRecord p18, java.security.PrivateKey p19, java.util.Date p20, java.util.Date p21, String p22)
    {
        int v9 = p18.getAlgorithm();
        org.xbill.DNS.DNSSEC.checkAlgorithm(p19, v9);
        org.xbill.DNS.RRSIGRecord v3_1 = new org.xbill.DNS.RRSIGRecord(p17.getName(), p17.getDClass(), p17.getTTL(), p17.getType(), v9, p17.getTTL(), p21, p20, p18.getFootprint(), p18.getName(), 0);
        v3_1.setSignature(org.xbill.DNS.DNSSEC.sign(p19, p18.getPublicKey(), v9, org.xbill.DNS.DNSSEC.digestRRset(v3_1, p17), p22));
        return v3_1;
    }

    private static byte[] sign(java.security.PrivateKey p2, java.security.PublicKey p3, int p4, byte[] p5, String p6)
    {
        try {
            IllegalStateException v0_1;
            if (p6 == null) {
                v0_1 = java.security.Signature.getInstance(org.xbill.DNS.DNSSEC.algString(p4));
            } else {
                v0_1 = java.security.Signature.getInstance(org.xbill.DNS.DNSSEC.algString(p4), p6);
            }
        } catch (IllegalStateException v0_3) {
            throw new org.xbill.DNS.DNSSEC$DNSSECException(v0_3.toString());
        }
        v0_1.initSign(p2);
        v0_1.update(p5);
        IllegalStateException v0_5 = v0_1.sign();
        if (!(p3 instanceof java.security.interfaces.DSAPublicKey)) {
            if ((p3 instanceof java.security.interfaces.ECPublicKey)) {
                try {
                    switch (p4) {
                        case 12:
                            break;
                        case 13:
                            v0_5 = org.xbill.DNS.DNSSEC.ECDSASignaturetoDNS(v0_5, org.xbill.DNS.DNSSEC.ECDSA_P256);
                            break;
                        case 14:
                            v0_5 = org.xbill.DNS.DNSSEC.ECDSASignaturetoDNS(v0_5, org.xbill.DNS.DNSSEC.ECDSA_P384);
                            break;
                        default:
                            throw new org.xbill.DNS.DNSSEC$UnsupportedAlgorithmException(p4);
                    }
                } catch (IllegalStateException v0) {
                    throw new IllegalStateException();
                }
            }
        } else {
            try {
                v0_5 = org.xbill.DNS.DNSSEC.DSASignaturetoDNS(v0_5, ((org.xbill.DNS.DNSSEC.BigIntegerLength(((java.security.interfaces.DSAPublicKey) p3).getParams().getP()) - 64) / 8));
            } catch (IllegalStateException v0) {
                throw new IllegalStateException();
            }
        }
        return v0_5;
    }

    static org.xbill.DNS.SIGRecord signMessage(org.xbill.DNS.Message p17, org.xbill.DNS.SIGRecord p18, org.xbill.DNS.KEYRecord p19, java.security.PrivateKey p20, java.util.Date p21, java.util.Date p22)
    {
        int v9 = p19.getAlgorithm();
        org.xbill.DNS.DNSSEC.checkAlgorithm(p20, v9);
        org.xbill.DNS.SIGRecord v3_1 = new org.xbill.DNS.SIGRecord(org.xbill.DNS.Name.root, 255, 0, 0, v9, 0, p22, p21, p19.getFootprint(), p19.getName(), 0);
        byte[] v2_1 = new org.xbill.DNS.DNSOutput();
        org.xbill.DNS.DNSSEC.digestSIG(v2_1, v3_1);
        if (p18 != null) {
            v2_1.writeByteArray(p18.getSignature());
        }
        v2_1.writeByteArray(p17.toWire());
        v3_1.setSignature(org.xbill.DNS.DNSSEC.sign(p20, p19.getPublicKey(), v9, v2_1.toByteArray(), 0));
        return v3_1;
    }

    private static java.security.PublicKey toDSAPublicKey(org.xbill.DNS.KEYBase p6)
    {
        java.security.PublicKey v0_1 = new org.xbill.DNS.DNSInput(p6.getKey());
        java.security.KeyFactory v1_1 = v0_1.readU8();
        if (v1_1 <= 8) {
            return java.security.KeyFactory.getInstance("DSA").generatePublic(new java.security.spec.DSAPublicKeySpec(org.xbill.DNS.DNSSEC.readBigInteger(v0_1, ((v1_1 * 8) + 64)), org.xbill.DNS.DNSSEC.readBigInteger(v0_1, ((v1_1 * 8) + 64)), org.xbill.DNS.DNSSEC.readBigInteger(v0_1, 20), org.xbill.DNS.DNSSEC.readBigInteger(v0_1, ((v1_1 * 8) + 64))));
        } else {
            throw new org.xbill.DNS.DNSSEC$MalformedKeyException(p6);
        }
    }

    private static java.security.PublicKey toECDSAPublicKey(org.xbill.DNS.KEYBase p4, org.xbill.DNS.DNSSEC$ECKeyInfo p5)
    {
        java.security.PublicKey v0_1 = new org.xbill.DNS.DNSInput(p4.getKey());
        return java.security.KeyFactory.getInstance("EC").generatePublic(new java.security.spec.ECPublicKeySpec(new java.security.spec.ECPoint(org.xbill.DNS.DNSSEC.readBigInteger(v0_1, p5.length), org.xbill.DNS.DNSSEC.readBigInteger(v0_1, p5.length)), p5.spec));
    }

    private static java.security.PublicKey toECGOSTPublicKey(org.xbill.DNS.KEYBase p4, org.xbill.DNS.DNSSEC$ECKeyInfo p5)
    {
        java.security.PublicKey v0_1 = new org.xbill.DNS.DNSInput(p4.getKey());
        return java.security.KeyFactory.getInstance("ECGOST3410").generatePublic(new java.security.spec.ECPublicKeySpec(new java.security.spec.ECPoint(org.xbill.DNS.DNSSEC.readBigIntegerLittleEndian(v0_1, p5.length), org.xbill.DNS.DNSSEC.readBigIntegerLittleEndian(v0_1, p5.length)), p5.spec));
    }

    static java.security.PublicKey toPublicKey(org.xbill.DNS.KEYBase p2)
    {
        try {
            String v0_2;
            String v0_0 = p2.getAlgorithm();
            switch (v0_0) {
                case 1:
                case 5:
                case 7:
                case 8:
                case 10:
                    v0_2 = org.xbill.DNS.DNSSEC.toRSAPublicKey(p2);
                    return v0_2;
                case 2:
                case 4:
                case 9:
                case 11:
                default:
                    throw new org.xbill.DNS.DNSSEC$UnsupportedAlgorithmException(v0_0);
                    break;
                case 3:
                case 6:
                    v0_2 = org.xbill.DNS.DNSSEC.toDSAPublicKey(p2);
                    return v0_2;
                case 12:
                    v0_2 = org.xbill.DNS.DNSSEC.toECGOSTPublicKey(p2, org.xbill.DNS.DNSSEC.GOST);
                    return v0_2;
                case 13:
                    v0_2 = org.xbill.DNS.DNSSEC.toECDSAPublicKey(p2, org.xbill.DNS.DNSSEC.ECDSA_P256);
                    return v0_2;
                case 14:
                    v0_2 = org.xbill.DNS.DNSSEC.toECDSAPublicKey(p2, org.xbill.DNS.DNSSEC.ECDSA_P384);
                    return v0_2;
            }
        } catch (String v0_5) {
            throw new org.xbill.DNS.DNSSEC$DNSSECException(v0_5.toString());
        } catch (String v0) {
        }
        throw new org.xbill.DNS.DNSSEC$MalformedKeyException(p2);
    }

    private static java.security.PublicKey toRSAPublicKey(org.xbill.DNS.KEYBase p4)
    {
        java.math.BigInteger v1_1 = new org.xbill.DNS.DNSInput(p4.getKey());
        java.security.PublicKey v0_1 = v1_1.readU8();
        if (v0_1 == null) {
            v0_1 = v1_1.readU16();
        }
        return java.security.KeyFactory.getInstance("RSA").generatePublic(new java.security.spec.RSAPublicKeySpec(org.xbill.DNS.DNSSEC.readBigInteger(v1_1), org.xbill.DNS.DNSSEC.readBigInteger(v1_1, v0_1)));
    }

    private static byte[] trimByteArray(byte[] p4)
    {
        if (p4[0] == 0) {
            byte[] v0_3 = new byte[(p4.length - 1)];
            System.arraycopy(p4, 1, v0_3, 0, (p4.length - 1));
            p4 = v0_3;
        }
        return p4;
    }

    private static void verify(java.security.PublicKey p2, int p3, byte[] p4, byte[] p5)
    {
        if (!(p2 instanceof java.security.interfaces.DSAPublicKey)) {
            if ((p2 instanceof java.security.interfaces.ECPublicKey)) {
                try {
                    switch (p3) {
                        case 12:
                            p5 = org.xbill.DNS.DNSSEC.ECGOSTSignaturefromDNS(p5, org.xbill.DNS.DNSSEC.GOST);
                            break;
                        case 13:
                            p5 = org.xbill.DNS.DNSSEC.ECDSASignaturefromDNS(p5, org.xbill.DNS.DNSSEC.ECDSA_P256);
                            break;
                        case 14:
                            p5 = org.xbill.DNS.DNSSEC.ECDSASignaturefromDNS(p5, org.xbill.DNS.DNSSEC.ECDSA_P384);
                            break;
                        default:
                            throw new org.xbill.DNS.DNSSEC$UnsupportedAlgorithmException(p3);
                    }
                } catch (IllegalStateException v0) {
                }
                throw new IllegalStateException();
            }
        } else {
            try {
                p5 = org.xbill.DNS.DNSSEC.DSASignaturefromDNS(p5);
            } catch (IllegalStateException v0) {
                throw new IllegalStateException();
            }
        }
        try {
            IllegalStateException v0_12 = java.security.Signature.getInstance(org.xbill.DNS.DNSSEC.algString(p3));
            v0_12.initVerify(p2);
            v0_12.update(p4);
        } catch (IllegalStateException v0_16) {
            throw new org.xbill.DNS.DNSSEC$DNSSECException(v0_16.toString());
        }
        if (v0_12.verify(p5)) {
            return;
        } else {
            throw new org.xbill.DNS.DNSSEC$SignatureVerificationException();
        }
    }

    public static void verify(org.xbill.DNS.RRset p4, org.xbill.DNS.RRSIGRecord p5, org.xbill.DNS.DNSKEYRecord p6)
    {
        if (org.xbill.DNS.DNSSEC.matches(p5, p6)) {
            java.security.PublicKey v0_2 = new java.util.Date();
            if (v0_2.compareTo(p5.getExpire()) <= 0) {
                if (v0_2.compareTo(p5.getTimeSigned()) >= 0) {
                    org.xbill.DNS.DNSSEC.verify(p6.getPublicKey(), p5.getAlgorithm(), org.xbill.DNS.DNSSEC.digestRRset(p5, p4), p5.getSignature());
                    return;
                } else {
                    throw new org.xbill.DNS.DNSSEC$SignatureNotYetValidException(p5.getTimeSigned(), v0_2);
                }
            } else {
                throw new org.xbill.DNS.DNSSEC$SignatureExpiredException(p5.getExpire(), v0_2);
            }
        } else {
            throw new org.xbill.DNS.DNSSEC$KeyMismatchException(p6, p5);
        }
    }

    static void verifyMessage(org.xbill.DNS.Message p4, byte[] p5, org.xbill.DNS.SIGRecord p6, org.xbill.DNS.SIGRecord p7, org.xbill.DNS.KEYRecord p8)
    {
        if (p4.sig0start != 0) {
            if (org.xbill.DNS.DNSSEC.matches(p6, p8)) {
                java.security.PublicKey v0_3 = new java.util.Date();
                if (v0_3.compareTo(p6.getExpire()) <= 0) {
                    if (v0_3.compareTo(p6.getTimeSigned()) >= 0) {
                        byte[] v1_5 = new org.xbill.DNS.DNSOutput();
                        org.xbill.DNS.DNSSEC.digestSIG(v1_5, p6);
                        if (p7 != null) {
                            v1_5.writeByteArray(p7.getSignature());
                        }
                        java.security.PublicKey v0_7 = ((org.xbill.DNS.Header) p4.getHeader().clone());
                        v0_7.decCount(3);
                        v1_5.writeByteArray(v0_7.toWire());
                        v1_5.writeByteArray(p5, 12, (p4.sig0start - 12));
                        org.xbill.DNS.DNSSEC.verify(p8.getPublicKey(), p6.getAlgorithm(), v1_5.toByteArray(), p6.getSignature());
                        return;
                    } else {
                        throw new org.xbill.DNS.DNSSEC$SignatureNotYetValidException(p6.getTimeSigned(), v0_3);
                    }
                } else {
                    throw new org.xbill.DNS.DNSSEC$SignatureExpiredException(p6.getExpire(), v0_3);
                }
            } else {
                throw new org.xbill.DNS.DNSSEC$KeyMismatchException(p8, p6);
            }
        } else {
            throw new org.xbill.DNS.DNSSEC$NoSignatureException();
        }
    }

    private static void writeBigInteger(org.xbill.DNS.DNSOutput p1, java.math.BigInteger p2)
    {
        p1.writeByteArray(org.xbill.DNS.DNSSEC.trimByteArray(p2.toByteArray()));
        return;
    }

    private static void writePaddedBigInteger(org.xbill.DNS.DNSOutput p2, java.math.BigInteger p3, int p4)
    {
        IllegalArgumentException v0_1 = org.xbill.DNS.DNSSEC.trimByteArray(p3.toByteArray());
        if (v0_1.length <= p4) {
            if (v0_1.length < p4) {
                byte[] v1_4 = new byte[(p4 - v0_1.length)];
                p2.writeByteArray(v1_4);
            }
            p2.writeByteArray(v0_1);
            return;
        } else {
            throw new IllegalArgumentException();
        }
    }

    private static void writePaddedBigIntegerLittleEndian(org.xbill.DNS.DNSOutput p2, java.math.BigInteger p3, int p4)
    {
        byte[] v0_1 = org.xbill.DNS.DNSSEC.trimByteArray(p3.toByteArray());
        if (v0_1.length <= p4) {
            org.xbill.DNS.DNSSEC.reverseByteArray(v0_1);
            p2.writeByteArray(v0_1);
            if (v0_1.length < p4) {
                byte[] v0_4 = new byte[(p4 - v0_1.length)];
                p2.writeByteArray(v0_4);
            }
            return;
        } else {
            throw new IllegalArgumentException();
        }
    }
}
