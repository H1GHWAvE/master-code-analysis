package org.xbill.DNS;
public class APLRecord$Element {
    public final Object address;
    public final int family;
    public final boolean negative;
    public final int prefixLength;

    private APLRecord$Element(int p3, boolean p4, Object p5, int p6)
    {
        this.family = p3;
        this.negative = p4;
        this.address = p5;
        this.prefixLength = p6;
        if (org.xbill.DNS.APLRecord.access$000(p3, p6)) {
            return;
        } else {
            throw new IllegalArgumentException("invalid prefix length");
        }
    }

    APLRecord$Element(int p1, boolean p2, Object p3, int p4, org.xbill.DNS.APLRecord$1 p5)
    {
        this(p1, p2, p3, p4);
        return;
    }

    public APLRecord$Element(boolean p2, java.net.InetAddress p3, int p4)
    {
        this(org.xbill.DNS.Address.familyOf(p3), p2, p3, p4);
        return;
    }

    public boolean equals(Object p4)
    {
        int v0 = 0;
        if ((p4 != null) && (((p4 instanceof org.xbill.DNS.APLRecord$Element)) && ((this.family == ((org.xbill.DNS.APLRecord$Element) p4).family) && ((this.negative == ((org.xbill.DNS.APLRecord$Element) p4).negative) && ((this.prefixLength == ((org.xbill.DNS.APLRecord$Element) p4).prefixLength) && (this.address.equals(((org.xbill.DNS.APLRecord$Element) p4).address))))))) {
            v0 = 1;
        }
        return v0;
    }

    public int hashCode()
    {
        int v0_3;
        int v1_1 = (this.prefixLength + this.address.hashCode());
        if (!this.negative) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return (v0_3 + v1_1);
    }

    public String toString()
    {
        StringBuffer v1_1 = new StringBuffer();
        if (this.negative) {
            v1_1.append("!");
        }
        v1_1.append(this.family);
        v1_1.append(":");
        if ((this.family != 1) && (this.family != 2)) {
            v1_1.append(org.xbill.DNS.utils.base16.toString(((byte[]) ((byte[]) this.address))));
        } else {
            v1_1.append(((java.net.InetAddress) this.address).getHostAddress());
        }
        v1_1.append("/");
        v1_1.append(this.prefixLength);
        return v1_1.toString();
    }
}
