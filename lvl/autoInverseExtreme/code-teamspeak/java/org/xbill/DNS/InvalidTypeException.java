package org.xbill.DNS;
public class InvalidTypeException extends java.lang.IllegalArgumentException {

    public InvalidTypeException(int p3)
    {
        this(new StringBuffer("Invalid DNS type: ").append(p3).toString());
        return;
    }
}
