package org.xbill.DNS;
public class TLSARecord$MatchingType {
    public static final int EXACT = 0;
    public static final int SHA256 = 1;
    public static final int SHA512 = 2;

    private TLSARecord$MatchingType()
    {
        return;
    }
}
