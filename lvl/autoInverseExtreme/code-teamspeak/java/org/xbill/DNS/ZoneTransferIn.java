package org.xbill.DNS;
public class ZoneTransferIn {
    private static final int AXFR = 6;
    private static final int END = 7;
    private static final int FIRSTDATA = 1;
    private static final int INITIALSOA = 0;
    private static final int IXFR_ADD = 5;
    private static final int IXFR_ADDSOA = 4;
    private static final int IXFR_DEL = 3;
    private static final int IXFR_DELSOA = 2;
    private java.net.SocketAddress address;
    private org.xbill.DNS.TCPClient client;
    private long current_serial;
    private int dclass;
    private long end_serial;
    private org.xbill.DNS.ZoneTransferIn$ZoneTransferHandler handler;
    private org.xbill.DNS.Record initialsoa;
    private long ixfr_serial;
    private java.net.SocketAddress localAddress;
    private int qtype;
    private int rtype;
    private int state;
    private long timeout;
    private org.xbill.DNS.TSIG tsig;
    private org.xbill.DNS.TSIG$StreamVerifier verifier;
    private boolean want_fallback;
    private org.xbill.DNS.Name zname;

    private ZoneTransferIn()
    {
        this.timeout = 900000;
        return;
    }

    private ZoneTransferIn(org.xbill.DNS.Name p4, int p5, long p6, boolean p8, java.net.SocketAddress p9, org.xbill.DNS.TSIG p10)
    {
        this.timeout = 900000;
        this.address = p9;
        this.tsig = p10;
        if (!p4.isAbsolute()) {
            try {
                this.zname = org.xbill.DNS.Name.concatenate(p4, org.xbill.DNS.Name.root);
            } catch (IllegalArgumentException v0) {
                throw new IllegalArgumentException("ZoneTransferIn: name too long");
            }
        } else {
            this.zname = p4;
        }
        this.qtype = p5;
        this.dclass = 1;
        this.ixfr_serial = p6;
        this.want_fallback = p8;
        this.state = 0;
        return;
    }

    static long access$100(org.xbill.DNS.Record p2)
    {
        return org.xbill.DNS.ZoneTransferIn.getSOASerial(p2);
    }

    private void closeConnection()
    {
        try {
            if (this.client != null) {
                this.client.cleanup();
            }
        } catch (org.xbill.DNS.TCPClient v0) {
        }
        return;
    }

    private void doxfr()
    {
        while(true) {
            this.sendQuery();
            if (this.state == 7) {
                return;
            } else {
                String v0_2 = this.client.recv();
                org.xbill.DNS.Message v1 = this.parseMessage(v0_2);
                if ((v1.getHeader().getRcode() == 0) && (this.verifier != null)) {
                    v1.getTSIG();
                    if (this.verifier.verify(v1, v0_2) != 0) {
                        this.fail("TSIG failure");
                    }
                }
                org.xbill.DNS.TSIG$StreamVerifier v2_4 = v1.getSectionArray(1);
                if (this.state == 0) {
                    String v0_7 = v1.getRcode();
                    if (v0_7 != null) {
                        if ((this.qtype == 251) && (v0_7 == 4)) {
                            break;
                        }
                        this.fail(org.xbill.DNS.Rcode.string(v0_7));
                    }
                    String v0_9 = v1.getQuestion();
                    if ((v0_9 != null) && (v0_9.getType() != this.qtype)) {
                        this.fail("invalid question section");
                    }
                    if ((v2_4.length == 0) && (this.qtype == 251)) {
                        this.fallback();
                    }
                }
                String v0_14 = 0;
                while (v0_14 < v2_4.length) {
                    this.parseRR(v2_4[v0_14]);
                    v0_14++;
                }
                if ((this.state == 7) && ((this.verifier != null) && (!v1.isVerified()))) {
                    this.fail("last message must be signed");
                    while (this.state != 7) {
                    }
                }
                return;
            }
        }
        this.fallback();
    }

    private void fail(String p2)
    {
        throw new org.xbill.DNS.ZoneTransferException(p2);
    }

    private void fallback()
    {
        if (!this.want_fallback) {
            this.fail("server doesn\'t support IXFR");
        }
        this.logxfr("falling back to AXFR");
        this.qtype = 252;
        this.state = 0;
        return;
    }

    private org.xbill.DNS.ZoneTransferIn$BasicHandler getBasicHandler()
    {
        if (!(this.handler instanceof org.xbill.DNS.ZoneTransferIn$BasicHandler)) {
            throw new IllegalArgumentException("ZoneTransferIn used callback interface");
        } else {
            return ((org.xbill.DNS.ZoneTransferIn$BasicHandler) this.handler);
        }
    }

    private static long getSOASerial(org.xbill.DNS.Record p2)
    {
        return ((org.xbill.DNS.SOARecord) p2).getSerial();
    }

    private void logxfr(String p4)
    {
        if (org.xbill.DNS.Options.check("verbose")) {
            System.out.println(new StringBuffer().append(this.zname).append(": ").append(p4).toString());
        }
        return;
    }

    public static org.xbill.DNS.ZoneTransferIn newAXFR(org.xbill.DNS.Name p1, String p2, int p3, org.xbill.DNS.TSIG p4)
    {
        if (p3 == 0) {
            p3 = 53;
        }
        return org.xbill.DNS.ZoneTransferIn.newAXFR(p1, new java.net.InetSocketAddress(p2, p3), p4);
    }

    public static org.xbill.DNS.ZoneTransferIn newAXFR(org.xbill.DNS.Name p1, String p2, org.xbill.DNS.TSIG p3)
    {
        return org.xbill.DNS.ZoneTransferIn.newAXFR(p1, p2, 0, p3);
    }

    public static org.xbill.DNS.ZoneTransferIn newAXFR(org.xbill.DNS.Name p9, java.net.SocketAddress p10, org.xbill.DNS.TSIG p11)
    {
        return new org.xbill.DNS.ZoneTransferIn(p9, 252, 0, 0, p10, p11);
    }

    public static org.xbill.DNS.ZoneTransferIn newIXFR(org.xbill.DNS.Name p7, long p8, boolean p10, String p11, int p12, org.xbill.DNS.TSIG p13)
    {
        if (p12 == 0) {
            p12 = 53;
        }
        return org.xbill.DNS.ZoneTransferIn.newIXFR(p7, p8, p10, new java.net.InetSocketAddress(p11, p12), p13);
    }

    public static org.xbill.DNS.ZoneTransferIn newIXFR(org.xbill.DNS.Name p9, long p10, boolean p12, String p13, org.xbill.DNS.TSIG p14)
    {
        return org.xbill.DNS.ZoneTransferIn.newIXFR(p9, p10, p12, p13, 0, p14);
    }

    public static org.xbill.DNS.ZoneTransferIn newIXFR(org.xbill.DNS.Name p9, long p10, boolean p12, java.net.SocketAddress p13, org.xbill.DNS.TSIG p14)
    {
        return new org.xbill.DNS.ZoneTransferIn(p9, 251, p10, p12, p13, p14);
    }

    private void openConnection()
    {
        this.client = new org.xbill.DNS.TCPClient((System.currentTimeMillis() + this.timeout));
        if (this.localAddress != null) {
            this.client.bind(this.localAddress);
        }
        this.client.connect(this.address);
        return;
    }

    private org.xbill.DNS.Message parseMessage(byte[] p3)
    {
        try {
            return new org.xbill.DNS.Message(p3);
        } catch (org.xbill.DNS.WireParseException v0_2) {
            if (!(v0_2 instanceof org.xbill.DNS.WireParseException)) {
                throw new org.xbill.DNS.WireParseException("Error parsing message");
            } else {
                throw ((org.xbill.DNS.WireParseException) v0_2);
            }
        }
    }

    private void parseRR(org.xbill.DNS.Record p10)
    {
        while(true) {
            String v0_0 = p10.getType();
            switch (this.state) {
                case 0:
                    if (v0_0 != 6) {
                        this.fail("missing initial SOA");
                    }
                    this.initialsoa = p10;
                    this.end_serial = org.xbill.DNS.ZoneTransferIn.getSOASerial(p10);
                    if ((this.qtype != 251) || (org.xbill.DNS.Serial.compare(this.end_serial, this.ixfr_serial) > 0)) {
                        this.state = 1;
                    } else {
                        this.logxfr("up to date");
                        this.state = 7;
                    }
                    break;
                case 1:
                    if ((this.qtype != 251) || ((v0_0 != 6) || (org.xbill.DNS.ZoneTransferIn.getSOASerial(p10) != this.ixfr_serial))) {
                        this.rtype = 252;
                        this.handler.startAXFR();
                        this.handler.handleRecord(this.initialsoa);
                        this.logxfr("got nonincremental response");
                        this.state = 6;
                    } else {
                        this.rtype = 251;
                        this.handler.startIXFR();
                        this.logxfr("got incremental response");
                        this.state = 2;
                    }
                    break;
                case 2:
                    this.handler.startIXFRDeletes(p10);
                    this.state = 3;
                    break;
                case 3:
                    if (v0_0 != 6) {
                        this.handler.handleRecord(p10);
                    } else {
                        this.current_serial = org.xbill.DNS.ZoneTransferIn.getSOASerial(p10);
                        this.state = 4;
                    }
                    break;
                case 4:
                    this.handler.startIXFRAdds(p10);
                    this.state = 5;
                    break;
                case 5:
                    if (v0_0 == 6) {
                        String v0_2 = org.xbill.DNS.ZoneTransferIn.getSOASerial(p10);
                        if (v0_2 != this.end_serial) {
                            if (v0_2 != this.current_serial) {
                                break;
                            }
                            this.state = 2;
                        } else {
                            this.state = 7;
                        }
                    }
                    this.handler.handleRecord(p10);
                    break;
                case 6:
                    if ((v0_0 == 1) && (p10.getDClass() != this.dclass)) {
                    } else {
                        this.handler.handleRecord(p10);
                        if (v0_0 != 6) {
                        } else {
                            this.state = 7;
                        }
                    }
                    break;
                case 7:
                    this.fail("extra data");
                    break;
                default:
                    this.fail("invalid state");
            }
            return;
        }
        this.fail(new StringBuffer("IXFR out of sync: expected serial ").append(this.current_serial).append(" , got ").append(v0_2).toString());
    }

    private void sendQuery()
    {
        byte[] v2_1 = org.xbill.DNS.Record.newRecord(this.zname, this.qtype, this.dclass);
        org.xbill.DNS.Message v20_1 = new org.xbill.DNS.Message();
        v20_1.getHeader().setOpcode(0);
        v20_1.addRecord(v2_1, 0);
        if (this.qtype == 251) {
            v20_1.addRecord(new org.xbill.DNS.SOARecord(this.zname, this.dclass, 0, org.xbill.DNS.Name.root, org.xbill.DNS.Name.root, this.ixfr_serial, 0, 0, 0, 0), 2);
        }
        if (this.tsig != null) {
            this.tsig.apply(v20_1, 0);
            this.verifier = new org.xbill.DNS.TSIG$StreamVerifier(this.tsig, v20_1.getTSIG());
        }
        this.client.send(v20_1.toWire(65535));
        return;
    }

    public java.util.List getAXFR()
    {
        return org.xbill.DNS.ZoneTransferIn$BasicHandler.access$300(this.getBasicHandler());
    }

    public java.util.List getIXFR()
    {
        return org.xbill.DNS.ZoneTransferIn$BasicHandler.access$400(this.getBasicHandler());
    }

    public org.xbill.DNS.Name getName()
    {
        return this.zname;
    }

    public int getType()
    {
        return this.qtype;
    }

    public boolean isAXFR()
    {
        int v0_1;
        if (this.rtype != 252) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public boolean isCurrent()
    {
        int v0_2;
        int v0_0 = this.getBasicHandler();
        if ((org.xbill.DNS.ZoneTransferIn$BasicHandler.access$300(v0_0) != null) || (org.xbill.DNS.ZoneTransferIn$BasicHandler.access$400(v0_0) != null)) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public boolean isIXFR()
    {
        int v0_1;
        if (this.rtype != 251) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public java.util.List run()
    {
        java.util.List v0_2;
        java.util.List v0_1 = new org.xbill.DNS.ZoneTransferIn$BasicHandler(0);
        this.run(v0_1);
        if (org.xbill.DNS.ZoneTransferIn$BasicHandler.access$300(v0_1) == null) {
            v0_2 = org.xbill.DNS.ZoneTransferIn$BasicHandler.access$400(v0_1);
        } else {
            v0_2 = org.xbill.DNS.ZoneTransferIn$BasicHandler.access$300(v0_1);
        }
        return v0_2;
    }

    public void run(org.xbill.DNS.ZoneTransferIn$ZoneTransferHandler p2)
    {
        this.handler = p2;
        try {
            this.openConnection();
            this.doxfr();
            this.closeConnection();
            return;
        } catch (Throwable v0) {
            this.closeConnection();
            throw v0;
        }
    }

    public void setDClass(int p1)
    {
        org.xbill.DNS.DClass.check(p1);
        this.dclass = p1;
        return;
    }

    public void setLocalAddress(java.net.SocketAddress p1)
    {
        this.localAddress = p1;
        return;
    }

    public void setTimeout(int p5)
    {
        if (p5 >= 0) {
            this.timeout = (1000 * ((long) p5));
            return;
        } else {
            throw new IllegalArgumentException("timeout cannot be negative");
        }
    }
}
