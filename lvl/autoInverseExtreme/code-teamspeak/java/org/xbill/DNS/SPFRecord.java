package org.xbill.DNS;
public class SPFRecord extends org.xbill.DNS.TXTBase {
    private static final long serialVersionUID = 16345989720907892894;

    SPFRecord()
    {
        return;
    }

    public SPFRecord(org.xbill.DNS.Name p8, int p9, long p10, String p12)
    {
        this(p8, 99, p9, p10, p12);
        return;
    }

    public SPFRecord(org.xbill.DNS.Name p8, int p9, long p10, java.util.List p12)
    {
        this(p8, 99, p9, p10, p12);
        return;
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.SPFRecord();
    }
}
