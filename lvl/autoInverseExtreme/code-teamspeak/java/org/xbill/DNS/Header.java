package org.xbill.DNS;
public class Header implements java.lang.Cloneable {
    public static final int LENGTH = 12;
    private static java.util.Random random;
    private int[] counts;
    private int flags;
    private int id;

    static Header()
    {
        org.xbill.DNS.Header.random = new java.util.Random();
        return;
    }

    public Header()
    {
        this.init();
        return;
    }

    public Header(int p1)
    {
        this.init();
        this.setID(p1);
        return;
    }

    Header(org.xbill.DNS.DNSInput p4)
    {
        this(p4.readU16());
        this.flags = p4.readU16();
        int v0_2 = 0;
        while (v0_2 < this.counts.length) {
            this.counts[v0_2] = p4.readU16();
            v0_2++;
        }
        return;
    }

    public Header(byte[] p2)
    {
        this(new org.xbill.DNS.DNSInput(p2));
        return;
    }

    private static void checkFlag(int p3)
    {
        if (org.xbill.DNS.Header.validFlag(p3)) {
            return;
        } else {
            throw new IllegalArgumentException(new StringBuffer("invalid flag bit ").append(p3).toString());
        }
    }

    private void init()
    {
        int v0_1 = new int[4];
        this.counts = v0_1;
        this.flags = 0;
        this.id = -1;
        return;
    }

    static int setFlag(int p2, int p3, boolean p4)
    {
        int v0_3;
        org.xbill.DNS.Header.checkFlag(p3);
        if (!p4) {
            v0_3 = (((1 << (15 - p3)) ^ -1) & p2);
        } else {
            v0_3 = ((1 << (15 - p3)) | p2);
        }
        return v0_3;
    }

    private static boolean validFlag(int p1)
    {
        if ((p1 < 0) || ((p1 > 15) || (!org.xbill.DNS.Flags.isFlag(p1)))) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public Object clone()
    {
        org.xbill.DNS.Header v0_1 = new org.xbill.DNS.Header();
        v0_1.id = this.id;
        v0_1.flags = this.flags;
        System.arraycopy(this.counts, 0, v0_1.counts, 0, this.counts.length);
        return v0_1;
    }

    void decCount(int p3)
    {
        if (this.counts[p3] != 0) {
            int[] v0_2 = this.counts;
            v0_2[p3] = (v0_2[p3] - 1);
            return;
        } else {
            throw new IllegalStateException("DNS section count cannot be decremented");
        }
    }

    public int getCount(int p2)
    {
        return this.counts[p2];
    }

    public boolean getFlag(int p4)
    {
        int v0 = 1;
        org.xbill.DNS.Header.checkFlag(p4);
        if ((this.flags & (1 << (15 - p4))) == 0) {
            v0 = 0;
        }
        return v0;
    }

    boolean[] getFlags()
    {
        boolean[] v1 = new boolean[16];
        int v0 = 0;
        while (v0 < 16) {
            if (org.xbill.DNS.Header.validFlag(v0)) {
                v1[v0] = this.getFlag(v0);
            }
            v0++;
        }
        return v1;
    }

    int getFlagsByte()
    {
        return this.flags;
    }

    public int getID()
    {
        int v0_4;
        if (this.id < 0) {
            try {
                if (this.id < 0) {
                    this.id = org.xbill.DNS.Header.random.nextInt(65535);
                }
            } catch (int v0_5) {
                throw v0_5;
            }
            v0_4 = this.id;
        } else {
            v0_4 = this.id;
        }
        return v0_4;
    }

    public int getOpcode()
    {
        return ((this.flags >> 11) & 15);
    }

    public int getRcode()
    {
        return (this.flags & 15);
    }

    void incCount(int p3)
    {
        if (this.counts[p3] != 65535) {
            int[] v0_2 = this.counts;
            v0_2[p3] = (v0_2[p3] + 1);
            return;
        } else {
            throw new IllegalStateException("DNS section count cannot be incremented");
        }
    }

    public String printFlags()
    {
        StringBuffer v1_1 = new StringBuffer();
        int v0_0 = 0;
        while (v0_0 < 16) {
            if ((org.xbill.DNS.Header.validFlag(v0_0)) && (this.getFlag(v0_0))) {
                v1_1.append(org.xbill.DNS.Flags.string(v0_0));
                v1_1.append(" ");
            }
            v0_0++;
        }
        return v1_1.toString();
    }

    void setCount(int p4, int p5)
    {
        if ((p5 >= 0) && (p5 <= 65535)) {
            this.counts[p4] = p5;
            return;
        } else {
            throw new IllegalArgumentException(new StringBuffer("DNS section count ").append(p5).append(" is out of range").toString());
        }
    }

    public void setFlag(int p3)
    {
        org.xbill.DNS.Header.checkFlag(p3);
        this.flags = org.xbill.DNS.Header.setFlag(this.flags, p3, 1);
        return;
    }

    public void setID(int p4)
    {
        if ((p4 >= 0) && (p4 <= 65535)) {
            this.id = p4;
            return;
        } else {
            throw new IllegalArgumentException(new StringBuffer("DNS message ID ").append(p4).append(" is out of range").toString());
        }
    }

    public void setOpcode(int p4)
    {
        if ((p4 >= 0) && (p4 <= 15)) {
            this.flags = (this.flags & 34815);
            this.flags = (this.flags | (p4 << 11));
            return;
        } else {
            throw new IllegalArgumentException(new StringBuffer("DNS Opcode ").append(p4).append("is out of range").toString());
        }
    }

    public void setRcode(int p4)
    {
        if ((p4 >= 0) && (p4 <= 15)) {
            this.flags = (this.flags & -16);
            this.flags = (this.flags | p4);
            return;
        } else {
            throw new IllegalArgumentException(new StringBuffer("DNS Rcode ").append(p4).append(" is out of range").toString());
        }
    }

    public String toString()
    {
        return this.toStringWithRcode(this.getRcode());
    }

    String toStringWithRcode(int p5)
    {
        StringBuffer v1_1 = new StringBuffer();
        v1_1.append(";; ->>HEADER<<- ");
        v1_1.append(new StringBuffer("opcode: ").append(org.xbill.DNS.Opcode.string(this.getOpcode())).toString());
        v1_1.append(new StringBuffer(", status: ").append(org.xbill.DNS.Rcode.string(p5)).toString());
        v1_1.append(new StringBuffer(", id: ").append(this.getID()).toString());
        v1_1.append("\n");
        v1_1.append(new StringBuffer(";; flags: ").append(this.printFlags()).toString());
        v1_1.append("; ");
        String v0_19 = 0;
        while (v0_19 < 4) {
            v1_1.append(new StringBuffer().append(org.xbill.DNS.Section.string(v0_19)).append(": ").append(this.getCount(v0_19)).append(" ").toString());
            v0_19++;
        }
        return v1_1.toString();
    }

    void toWire(org.xbill.DNS.DNSOutput p3)
    {
        p3.writeU16(this.getID());
        p3.writeU16(this.flags);
        int v0_2 = 0;
        while (v0_2 < this.counts.length) {
            p3.writeU16(this.counts[v0_2]);
            v0_2++;
        }
        return;
    }

    public byte[] toWire()
    {
        byte[] v0_1 = new org.xbill.DNS.DNSOutput();
        this.toWire(v0_1);
        return v0_1.toByteArray();
    }

    public void unsetFlag(int p3)
    {
        org.xbill.DNS.Header.checkFlag(p3);
        this.flags = org.xbill.DNS.Header.setFlag(this.flags, p3, 0);
        return;
    }
}
