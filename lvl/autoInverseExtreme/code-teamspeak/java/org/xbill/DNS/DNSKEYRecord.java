package org.xbill.DNS;
public class DNSKEYRecord extends org.xbill.DNS.KEYBase {
    private static final long serialVersionUID = 9766944033282876614;

    DNSKEYRecord()
    {
        return;
    }

    public DNSKEYRecord(org.xbill.DNS.Name p14, int p15, long p16, int p18, int p19, int p20, java.security.PublicKey p21)
    {
        this(p14, 48, p15, p16, p18, p19, p20, org.xbill.DNS.DNSSEC.fromPublicKey(p21, p20));
        this.publicKey = p21;
        return;
    }

    public DNSKEYRecord(org.xbill.DNS.Name p12, int p13, long p14, int p16, int p17, int p18, byte[] p19)
    {
        this(p12, 48, p13, p14, p16, p17, p18, p19);
        return;
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.DNSKEYRecord();
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p4, org.xbill.DNS.Name p5)
    {
        this.flags = p4.getUInt16();
        this.proto = p4.getUInt8();
        byte[] v0_2 = p4.getString();
        this.alg = org.xbill.DNS.DNSSEC$Algorithm.value(v0_2);
        if (this.alg >= 0) {
            this.key = p4.getBase64();
            return;
        } else {
            throw p4.exception(new StringBuffer("Invalid algorithm: ").append(v0_2).toString());
        }
    }
}
