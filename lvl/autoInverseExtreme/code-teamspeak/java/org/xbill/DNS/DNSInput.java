package org.xbill.DNS;
public class DNSInput {
    private byte[] array;
    private int end;
    private int pos;
    private int saved_end;
    private int saved_pos;

    public DNSInput(byte[] p3)
    {
        this.array = p3;
        this.pos = 0;
        this.end = this.array.length;
        this.saved_pos = -1;
        this.saved_end = -1;
        return;
    }

    private void require(int p3)
    {
        if (p3 <= this.remaining()) {
            return;
        } else {
            throw new org.xbill.DNS.WireParseException("end of input");
        }
    }

    public void clearActive()
    {
        this.end = this.array.length;
        return;
    }

    public int current()
    {
        return this.pos;
    }

    public void jump(int p3)
    {
        if (p3 < this.array.length) {
            this.pos = p3;
            this.end = this.array.length;
            return;
        } else {
            throw new IllegalArgumentException("cannot jump past end of input");
        }
    }

    public void readByteArray(byte[] p3, int p4, int p5)
    {
        this.require(p5);
        System.arraycopy(this.array, this.pos, p3, p4, p5);
        this.pos = (this.pos + p5);
        return;
    }

    public byte[] readByteArray()
    {
        int v0_0 = this.remaining();
        byte[] v1 = new byte[v0_0];
        System.arraycopy(this.array, this.pos, v1, 0, v0_0);
        this.pos = (v0_0 + this.pos);
        return v1;
    }

    public byte[] readByteArray(int p5)
    {
        this.require(p5);
        byte[] v0 = new byte[p5];
        System.arraycopy(this.array, this.pos, v0, 0, p5);
        this.pos = (this.pos + p5);
        return v0;
    }

    public byte[] readCountedString()
    {
        this.require(1);
        byte[] v0_1 = this.array;
        int v1 = this.pos;
        this.pos = (v1 + 1);
        return this.readByteArray((v0_1[v1] & 255));
    }

    public int readU16()
    {
        this.require(2);
        int v0_1 = this.array;
        int v1_0 = this.pos;
        this.pos = (v1_0 + 1);
        int v0_3 = (v0_1[v1_0] & 255);
        int v1_1 = this.array;
        int v2_1 = this.pos;
        this.pos = (v2_1 + 1);
        return ((v0_3 << 8) + (v1_1[v2_1] & 255));
    }

    public long readU32()
    {
        this.require(4);
        long v0_1 = this.array;
        int v1_0 = this.pos;
        this.pos = (v1_0 + 1);
        long v0_3 = (v0_1[v1_0] & 255);
        int v1_1 = this.array;
        long v2_1 = this.pos;
        this.pos = (v2_1 + 1);
        int v1_3 = (v1_1[v2_1] & 255);
        long v2_2 = this.array;
        int v3_1 = this.pos;
        this.pos = (v3_1 + 1);
        long v2_4 = (v2_2[v3_1] & 255);
        int v3_2 = this.array;
        long v4_1 = this.pos;
        this.pos = (v4_1 + 1);
        return (((((long) (v1_3 << 16)) + (((long) v0_3) << 24)) + ((long) (v2_4 << 8))) + ((long) (v3_2[v4_1] & 255)));
    }

    public int readU8()
    {
        this.require(1);
        int v0_1 = this.array;
        int v1 = this.pos;
        this.pos = (v1 + 1);
        return (v0_1[v1] & 255);
    }

    public int remaining()
    {
        return (this.end - this.pos);
    }

    public void restore()
    {
        if (this.saved_pos >= 0) {
            this.pos = this.saved_pos;
            this.end = this.saved_end;
            this.saved_pos = -1;
            this.saved_end = -1;
            return;
        } else {
            throw new IllegalStateException("no previous state");
        }
    }

    public void restoreActive(int p3)
    {
        if (p3 <= this.array.length) {
            this.end = p3;
            return;
        } else {
            throw new IllegalArgumentException("cannot set active region past end of input");
        }
    }

    public void save()
    {
        this.saved_pos = this.pos;
        this.saved_end = this.end;
        return;
    }

    public int saveActive()
    {
        return this.end;
    }

    public void setActive(int p3)
    {
        if (p3 <= (this.array.length - this.pos)) {
            this.end = (this.pos + p3);
            return;
        } else {
            throw new IllegalArgumentException("cannot set active region past end of input");
        }
    }
}
