package org.xbill.DNS;
interface Cache$Element {

    public abstract int compareCredibility();

    public abstract boolean expired();

    public abstract int getType();
}
