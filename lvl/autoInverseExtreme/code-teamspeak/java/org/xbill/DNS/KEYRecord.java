package org.xbill.DNS;
public class KEYRecord extends org.xbill.DNS.KEYBase {
    public static final int FLAG_NOAUTH = 32768;
    public static final int FLAG_NOCONF = 16384;
    public static final int FLAG_NOKEY = 49152;
    public static final int OWNER_HOST = 512;
    public static final int OWNER_USER = 0;
    public static final int OWNER_ZONE = 256;
    public static final int PROTOCOL_ANY = 255;
    public static final int PROTOCOL_DNSSEC = 3;
    public static final int PROTOCOL_EMAIL = 2;
    public static final int PROTOCOL_IPSEC = 4;
    public static final int PROTOCOL_TLS = 1;
    private static final long serialVersionUID = 6385613447571488906;

    KEYRecord()
    {
        return;
    }

    public KEYRecord(org.xbill.DNS.Name p14, int p15, long p16, int p18, int p19, int p20, java.security.PublicKey p21)
    {
        this(p14, 25, p15, p16, p18, p19, p20, org.xbill.DNS.DNSSEC.fromPublicKey(p21, p20));
        this.publicKey = p21;
        return;
    }

    public KEYRecord(org.xbill.DNS.Name p12, int p13, long p14, int p16, int p17, int p18, byte[] p19)
    {
        this(p12, 25, p13, p14, p16, p17, p18, p19);
        return;
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.KEYRecord();
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p4, org.xbill.DNS.Name p5)
    {
        byte[] v0_0 = p4.getIdentifier();
        this.flags = org.xbill.DNS.KEYRecord$Flags.value(v0_0);
        if (this.flags >= 0) {
            byte[] v0_1 = p4.getIdentifier();
            this.proto = org.xbill.DNS.KEYRecord$Protocol.value(v0_1);
            if (this.proto >= 0) {
                byte[] v0_2 = p4.getIdentifier();
                this.alg = org.xbill.DNS.DNSSEC$Algorithm.value(v0_2);
                if (this.alg >= 0) {
                    if ((this.flags & 49152) != 49152) {
                        this.key = p4.getBase64();
                    } else {
                        this.key = 0;
                    }
                    return;
                } else {
                    throw p4.exception(new StringBuffer("Invalid algorithm: ").append(v0_2).toString());
                }
            } else {
                throw p4.exception(new StringBuffer("Invalid protocol: ").append(v0_1).toString());
            }
        } else {
            throw p4.exception(new StringBuffer("Invalid flags: ").append(v0_0).toString());
        }
    }
}
