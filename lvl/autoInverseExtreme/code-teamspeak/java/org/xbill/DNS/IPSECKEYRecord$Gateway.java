package org.xbill.DNS;
public class IPSECKEYRecord$Gateway {
    public static final int IPv4 = 1;
    public static final int IPv6 = 2;
    public static final int Name = 3;
    public static final int None;

    private IPSECKEYRecord$Gateway()
    {
        return;
    }
}
