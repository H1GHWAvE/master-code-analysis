package org.xbill.DNS;
 class ResolveThread extends java.lang.Thread {
    private Object id;
    private org.xbill.DNS.ResolverListener listener;
    private org.xbill.DNS.Message query;
    private org.xbill.DNS.Resolver res;

    public ResolveThread(org.xbill.DNS.Resolver p1, org.xbill.DNS.Message p2, Object p3, org.xbill.DNS.ResolverListener p4)
    {
        this.res = p1;
        this.query = p2;
        this.id = p3;
        this.listener = p4;
        return;
    }

    public void run()
    {
        try {
            this.listener.receiveMessage(this.id, this.res.send(this.query));
        } catch (Exception v0_2) {
            this.listener.handleException(this.id, v0_2);
        }
        return;
    }
}
