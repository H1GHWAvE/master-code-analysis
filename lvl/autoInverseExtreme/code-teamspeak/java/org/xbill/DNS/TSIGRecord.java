package org.xbill.DNS;
public class TSIGRecord extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 18357923164692902310;
    private org.xbill.DNS.Name alg;
    private int error;
    private int fudge;
    private int originalID;
    private byte[] other;
    private byte[] signature;
    private java.util.Date timeSigned;

    TSIGRecord()
    {
        return;
    }

    public TSIGRecord(org.xbill.DNS.Name p10, int p11, long p12, org.xbill.DNS.Name p14, java.util.Date p15, int p16, byte[] p17, int p18, int p19, byte[] p20)
    {
        this(p10, 250, p11, p12);
        this.alg = org.xbill.DNS.TSIGRecord.checkName("alg", p14);
        this.timeSigned = p15;
        this.fudge = org.xbill.DNS.TSIGRecord.checkU16("fudge", p16);
        this.signature = p17;
        this.originalID = org.xbill.DNS.TSIGRecord.checkU16("originalID", p18);
        this.error = org.xbill.DNS.TSIGRecord.checkU16("error", p19);
        this.other = p20;
        return;
    }

    public org.xbill.DNS.Name getAlgorithm()
    {
        return this.alg;
    }

    public int getError()
    {
        return this.error;
    }

    public int getFudge()
    {
        return this.fudge;
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.TSIGRecord();
    }

    public int getOriginalID()
    {
        return this.originalID;
    }

    public byte[] getOther()
    {
        return this.other;
    }

    public byte[] getSignature()
    {
        return this.signature;
    }

    public java.util.Date getTimeSigned()
    {
        return this.timeSigned;
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p2, org.xbill.DNS.Name p3)
    {
        throw p2.exception("no text format defined for TSIG");
    }

    void rrFromWire(org.xbill.DNS.DNSInput p7)
    {
        this.alg = new org.xbill.DNS.Name(p7);
        this.timeSigned = new java.util.Date((((((long) p7.readU16()) << 32) + p7.readU32()) * 1000));
        this.fudge = p7.readU16();
        this.signature = p7.readByteArray(p7.readU16());
        this.originalID = p7.readU16();
        this.error = p7.readU16();
        int v0_12 = p7.readU16();
        if (v0_12 <= 0) {
            this.other = 0;
        } else {
            this.other = p7.readByteArray(v0_12);
        }
        return;
    }

    String rrToString()
    {
        String v0_1 = new StringBuffer();
        v0_1.append(this.alg);
        v0_1.append(" ");
        if (org.xbill.DNS.Options.check("multiline")) {
            v0_1.append("(\n\t");
        }
        v0_1.append((this.timeSigned.getTime() / 1000));
        v0_1.append(" ");
        v0_1.append(this.fudge);
        v0_1.append(" ");
        v0_1.append(this.signature.length);
        if (!org.xbill.DNS.Options.check("multiline")) {
            v0_1.append(" ");
            v0_1.append(org.xbill.DNS.utils.base64.toString(this.signature));
        } else {
            v0_1.append("\n");
            v0_1.append(org.xbill.DNS.utils.base64.formatString(this.signature, 64, "\t", 0));
        }
        v0_1.append(" ");
        v0_1.append(org.xbill.DNS.Rcode.TSIGstring(this.error));
        v0_1.append(" ");
        if (this.other != null) {
            v0_1.append(this.other.length);
            if (!org.xbill.DNS.Options.check("multiline")) {
                v0_1.append(" ");
            } else {
                v0_1.append("\n\n\n\t");
            }
            if (this.error != 18) {
                v0_1.append("<");
                v0_1.append(org.xbill.DNS.utils.base64.toString(this.other));
                v0_1.append(">");
            } else {
                if (this.other.length == 6) {
                    long v2_11 = ((((((((long) (this.other[0] & 255)) << 40) + (((long) (this.other[1] & 255)) << 32)) + ((long) ((this.other[2] & 255) << 24))) + ((long) ((this.other[3] & 255) << 16))) + ((long) ((this.other[4] & 255) << 8))) + ((long) (this.other[5] & 255)));
                    v0_1.append("<server time: ");
                    v0_1.append(new java.util.Date((v2_11 * 1000)));
                    v0_1.append(">");
                } else {
                    v0_1.append("<invalid BADTIME other data>");
                }
            }
        } else {
            v0_1.append(0);
        }
        if (org.xbill.DNS.Options.check("multiline")) {
            v0_1.append(" )");
        }
        return v0_1.toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p7, org.xbill.DNS.Compression p8, boolean p9)
    {
        this.alg.toWire(p7, 0, p9);
        int v0_3 = (this.timeSigned.getTime() / 1000);
        int v0_4 = (v0_3 & 2.1219957905e-314);
        p7.writeU16(((int) (v0_3 >> 32)));
        p7.writeU32(v0_4);
        p7.writeU16(this.fudge);
        p7.writeU16(this.signature.length);
        p7.writeByteArray(this.signature);
        p7.writeU16(this.originalID);
        p7.writeU16(this.error);
        if (this.other == null) {
            p7.writeU16(0);
        } else {
            p7.writeU16(this.other.length);
            p7.writeByteArray(this.other);
        }
        return;
    }
}
