package org.xbill.DNS;
 class ExtendedResolver$Resolution implements org.xbill.DNS.ResolverListener {
    boolean done;
    Object[] inprogress;
    org.xbill.DNS.ResolverListener listener;
    int outstanding;
    org.xbill.DNS.Message query;
    org.xbill.DNS.Resolver[] resolvers;
    org.xbill.DNS.Message response;
    int retries;
    int[] sent;
    Throwable thrown;

    public ExtendedResolver$Resolution(org.xbill.DNS.ExtendedResolver p7, org.xbill.DNS.Message p8)
    {
        int v0_0 = org.xbill.DNS.ExtendedResolver.access$000(p7);
        int v1_1 = new org.xbill.DNS.Resolver[v0_0.size()];
        this.resolvers = ((org.xbill.DNS.Resolver[]) ((org.xbill.DNS.Resolver[]) v0_0.toArray(v1_1)));
        if (org.xbill.DNS.ExtendedResolver.access$100(p7)) {
            int v1_2 = this.resolvers.length;
            int v2 = (org.xbill.DNS.ExtendedResolver.access$208(p7) % v1_2);
            if (org.xbill.DNS.ExtendedResolver.access$200(p7) > v1_2) {
                org.xbill.DNS.ExtendedResolver.access$244(p7, v1_2);
            }
            if (v2 > 0) {
                org.xbill.DNS.Resolver[] v3 = new org.xbill.DNS.Resolver[v1_2];
                int v0_8 = 0;
                while (v0_8 < v1_2) {
                    v3[v0_8] = this.resolvers[((v0_8 + v2) % v1_2)];
                    v0_8++;
                }
                this.resolvers = v3;
            }
        }
        int v0_11 = new int[this.resolvers.length];
        this.sent = v0_11;
        int v0_14 = new Object[this.resolvers.length];
        this.inprogress = v0_14;
        this.retries = org.xbill.DNS.ExtendedResolver.access$300(p7);
        this.query = p8;
        return;
    }

    public void handleException(Object p6, Exception p7)
    {
        Exception v0_0 = 1;
        if (org.xbill.DNS.Options.check("verbose")) {
            System.err.println(new StringBuffer("ExtendedResolver: got ").append(p7).toString());
        }
        try {
            this.outstanding = (this.outstanding - 1);
        } catch (Exception v0_13) {
            throw v0_13;
        }
        if (!this.done) {
            int v2_6 = 0;
            while ((v2_6 < this.inprogress.length) && (this.inprogress[v2_6] != p6)) {
                v2_6++;
            }
            if (v2_6 != this.inprogress.length) {
                if ((this.sent[v2_6] != 1) || (v2_6 >= (this.resolvers.length - 1))) {
                    v0_0 = 0;
                }
                if (!(p7 instanceof java.io.InterruptedIOException)) {
                    if (!(p7 instanceof java.net.SocketException)) {
                        this.thrown = p7;
                    } else {
                        if ((this.thrown == null) || ((this.thrown instanceof java.io.InterruptedIOException))) {
                            this.thrown = p7;
                        }
                    }
                } else {
                    if (this.sent[v2_6] < this.retries) {
                        this.send(v2_6);
                    }
                    if (this.thrown == null) {
                    }
                }
                if (!this.done) {
                    if (v0_0 != null) {
                        this.send((v2_6 + 1));
                    }
                    if (!this.done) {
                        if (this.outstanding == 0) {
                            this.done = 1;
                            if (this.listener == null) {
                                this.notifyAll();
                                return;
                            }
                        }
                        if (this.done) {
                            if (!(this.thrown instanceof Exception)) {
                                this.thrown = new RuntimeException(this.thrown.getMessage());
                            }
                            this.listener.handleException(this, ((Exception) this.thrown));
                        } else {
                        }
                    } else {
                    }
                } else {
                }
            } else {
            }
        } else {
        }
        return;
    }

    public void receiveMessage(Object p3, org.xbill.DNS.Message p4)
    {
        if (org.xbill.DNS.Options.check("verbose")) {
            System.err.println("ExtendedResolver: received message");
        }
        try {
            if (!this.done) {
                this.response = p4;
                this.done = 1;
                if (this.listener != null) {
                    this.listener.receiveMessage(this, this.response);
                } else {
                    this.notifyAll();
                }
            } else {
            }
        } catch (org.xbill.DNS.ResolverListener v0_7) {
            throw v0_7;
        }
        return;
    }

    public void send(int p4)
    {
        Throwable v0_0 = this.sent;
        v0_0[p4] = (v0_0[p4] + 1);
        this.outstanding = (this.outstanding + 1);
        try {
            this.inprogress[p4] = this.resolvers[p4].sendAsync(this.query, this);
        } catch (Throwable v0_4) {
            try {
                this.thrown = v0_4;
                this.done = 1;
            } catch (Throwable v0_7) {
                throw v0_7;
            }
            if (this.listener != null) {
            } else {
                this.notifyAll();
            }
        }
        return;
    }

    public org.xbill.DNS.Message start()
    {
        try {
            IllegalStateException v0_0 = this.sent;
            v0_0[0] = (v0_0[0] + 1);
            this.outstanding = (this.outstanding + 1);
            this.inprogress[0] = new Object();
            IllegalStateException v0_6 = this.resolvers[0].send(this.query);
        } catch (IllegalStateException v0_7) {
            this.handleException(this.inprogress[0], v0_7);
        }
        return v0_6;
    }

    public void startAsync(org.xbill.DNS.ResolverListener p2)
    {
        this.listener = p2;
        this.send(0);
        return;
    }
}
