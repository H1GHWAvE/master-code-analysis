package org.xbill.DNS;
public class EDNSOption$Code {
    public static final int CLIENT_SUBNET = 8;
    public static final int NSID = 3;
    private static org.xbill.DNS.Mnemonic codes;

    static EDNSOption$Code()
    {
        org.xbill.DNS.Mnemonic v0_1 = new org.xbill.DNS.Mnemonic("EDNS Option Codes", 2);
        org.xbill.DNS.EDNSOption$Code.codes = v0_1;
        v0_1.setMaximum(65535);
        org.xbill.DNS.EDNSOption$Code.codes.setPrefix("CODE");
        org.xbill.DNS.EDNSOption$Code.codes.setNumericAllowed(1);
        org.xbill.DNS.EDNSOption$Code.codes.add(3, "NSID");
        org.xbill.DNS.EDNSOption$Code.codes.add(8, "CLIENT_SUBNET");
        return;
    }

    private EDNSOption$Code()
    {
        return;
    }

    public static String string(int p1)
    {
        return org.xbill.DNS.EDNSOption$Code.codes.getText(p1);
    }

    public static int value(String p1)
    {
        return org.xbill.DNS.EDNSOption$Code.codes.getValue(p1);
    }
}
