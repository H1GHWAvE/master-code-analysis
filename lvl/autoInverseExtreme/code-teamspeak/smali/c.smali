.class final Lc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Ljava/net/InetAddress;

.field private final b:I

.field private final c:Ljnamed;


# direct methods
.method constructor <init>(Ljnamed;Ljava/net/InetAddress;I)V
    .registers 4

    .prologue
    .line 613
    iput-object p1, p0, Lc;->c:Ljnamed;

    iput-object p2, p0, Lc;->a:Ljava/net/InetAddress;

    iput p3, p0, Lc;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 12

    .prologue
    const/4 v0, 0x0

    .line 614
    iget-object v2, p0, Lc;->c:Ljnamed;

    iget-object v3, p0, Lc;->a:Ljava/net/InetAddress;

    iget v4, p0, Lc;->b:I

    .line 1556
    :try_start_7
    new-instance v5, Ljava/net/DatagramSocket;

    invoke-direct {v5, v4, v3}, Ljava/net/DatagramSocket;-><init>(ILjava/net/InetAddress;)V

    .line 1558
    const/16 v1, 0x200

    new-array v6, v1, [B

    .line 1559
    new-instance v7, Ljava/net/DatagramPacket;

    const/16 v1, 0x200

    invoke-direct {v7, v6, v1}, Ljava/net/DatagramPacket;-><init>([BI)V

    move-object v1, v0

    .line 1562
    :cond_18
    :goto_18
    const/16 v0, 0x200

    invoke-virtual {v7, v0}, Ljava/net/DatagramPacket;->setLength(I)V
    :try_end_1d
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_1d} :catch_63

    .line 1564
    :try_start_1d
    invoke-virtual {v5, v7}, Ljava/net/DatagramSocket;->receive(Ljava/net/DatagramPacket;)V
    :try_end_20
    .catch Ljava/io/InterruptedIOException; {:try_start_1d .. :try_end_20} :catch_87
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_20} :catch_63

    .line 1572
    :try_start_20
    new-instance v0, Lorg/xbill/DNS/Message;

    invoke-direct {v0, v6}, Lorg/xbill/DNS/Message;-><init>([B)V

    .line 1573
    invoke-virtual {v7}, Ljava/net/DatagramPacket;->getLength()I

    move-result v8

    const/4 v9, 0x0

    invoke-virtual {v2, v0, v6, v8, v9}, Ljnamed;->a(Lorg/xbill/DNS/Message;[BILjava/net/Socket;)[B
    :try_end_2d
    .catch Ljava/io/IOException; {:try_start_20 .. :try_end_2d} :catch_46

    move-result-object v0

    .line 1576
    if-eqz v0, :cond_18

    .line 1582
    :goto_30
    if-nez v1, :cond_4c

    .line 1583
    :try_start_32
    new-instance v1, Ljava/net/DatagramPacket;

    array-length v8, v0

    invoke-virtual {v7}, Ljava/net/DatagramPacket;->getAddress()Ljava/net/InetAddress;

    move-result-object v9

    invoke-virtual {v7}, Ljava/net/DatagramPacket;->getPort()I

    move-result v10

    invoke-direct {v1, v0, v8, v9, v10}, Ljava/net/DatagramPacket;-><init>([BILjava/net/InetAddress;I)V

    move-object v0, v1

    .line 1593
    :goto_41
    invoke-virtual {v5, v0}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V

    move-object v1, v0

    .line 1594
    goto :goto_18

    .line 1580
    :catch_46
    move-exception v0

    invoke-static {v6}, Ljnamed;->a([B)[B

    move-result-object v0

    goto :goto_30

    .line 1588
    :cond_4c
    invoke-virtual {v1, v0}, Ljava/net/DatagramPacket;->setData([B)V

    .line 1589
    array-length v0, v0

    invoke-virtual {v1, v0}, Ljava/net/DatagramPacket;->setLength(I)V

    .line 1590
    invoke-virtual {v7}, Ljava/net/DatagramPacket;->getAddress()Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/net/DatagramPacket;->setAddress(Ljava/net/InetAddress;)V

    .line 1591
    invoke-virtual {v7}, Ljava/net/DatagramPacket;->getPort()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/net/DatagramPacket;->setPort(I)V
    :try_end_61
    .catch Ljava/io/IOException; {:try_start_32 .. :try_end_61} :catch_63

    move-object v0, v1

    goto :goto_41

    .line 1596
    :catch_63
    move-exception v0

    .line 1597
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuffer;

    const-string v5, "serveUDP("

    invoke-direct {v2, v5}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-static {v3, v4}, Ljnamed;->a(Ljava/net/InetAddress;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 614
    return-void

    .line 1567
    :catch_87
    move-exception v0

    goto :goto_18
.end method
