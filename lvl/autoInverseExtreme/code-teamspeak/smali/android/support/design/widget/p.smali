.class public final Landroid/support/design/widget/p;
.super Landroid/widget/FrameLayout$LayoutParams;
.source "SourceFile"


# static fields
.field public static final a:I = 0x0

.field public static final b:I = 0x1

.field public static final c:I = 0x2

.field private static final f:F = 0.5f


# instance fields
.field d:I

.field e:F


# direct methods
.method private constructor <init>(II)V
    .registers 4

    .prologue
    .line 789
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 771
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/design/widget/p;->d:I

    .line 772
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Landroid/support/design/widget/p;->e:F

    .line 790
    return-void
.end method

.method private constructor <init>(III)V
    .registers 5

    .prologue
    .line 793
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 771
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/design/widget/p;->d:I

    .line 772
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Landroid/support/design/widget/p;->e:F

    .line 794
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 7

    .prologue
    const/4 v3, 0x0

    const/high16 v2, 0x3f000000    # 0.5f

    .line 775
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 771
    iput v3, p0, Landroid/support/design/widget/p;->d:I

    .line 772
    iput v2, p0, Landroid/support/design/widget/p;->e:F

    .line 777
    sget-object v0, Landroid/support/design/n;->CollapsingAppBarLayout_LayoutParams:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 779
    sget v1, Landroid/support/design/n;->CollapsingAppBarLayout_LayoutParams_layout_collapseMode:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Landroid/support/design/widget/p;->d:I

    .line 782
    sget v1, Landroid/support/design/n;->CollapsingAppBarLayout_LayoutParams_layout_collapseParallaxMultiplier:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    .line 1839
    iput v1, p0, Landroid/support/design/widget/p;->e:F

    .line 785
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 786
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .registers 3

    .prologue
    .line 797
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 771
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/design/widget/p;->d:I

    .line 772
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Landroid/support/design/widget/p;->e:F

    .line 798
    return-void
.end method

.method private constructor <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
    .registers 3

    .prologue
    .line 801
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 771
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/design/widget/p;->d:I

    .line 772
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Landroid/support/design/widget/p;->e:F

    .line 802
    return-void
.end method

.method public constructor <init>(Landroid/widget/FrameLayout$LayoutParams;)V
    .registers 3

    .prologue
    .line 805
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/widget/FrameLayout$LayoutParams;)V

    .line 771
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/design/widget/p;->d:I

    .line 772
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Landroid/support/design/widget/p;->e:F

    .line 806
    return-void
.end method

.method private a()I
    .registers 2

    .prologue
    .line 826
    iget v0, p0, Landroid/support/design/widget/p;->d:I

    return v0
.end method

.method private a(F)V
    .registers 2

    .prologue
    .line 839
    iput p1, p0, Landroid/support/design/widget/p;->e:F

    .line 840
    return-void
.end method

.method private a(I)V
    .registers 2

    .prologue
    .line 815
    iput p1, p0, Landroid/support/design/widget/p;->d:I

    .line 816
    return-void
.end method

.method private b()F
    .registers 2

    .prologue
    .line 849
    iget v0, p0, Landroid/support/design/widget/p;->e:F

    return v0
.end method
