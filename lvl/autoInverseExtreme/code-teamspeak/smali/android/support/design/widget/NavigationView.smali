.class public final Landroid/support/design/widget/NavigationView;
.super Landroid/support/design/internal/f;
.source "SourceFile"


# static fields
.field private static final a:[I

.field private static final b:[I

.field private static final c:I = 0x1


# instance fields
.field private final d:Landroid/support/design/internal/a;

.field private final e:Landroid/support/design/internal/b;

.field private f:Landroid/support/design/widget/ap;

.field private g:I

.field private h:Landroid/view/MenuInflater;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 74
    new-array v0, v3, [I

    const v1, 0x10100a0

    aput v1, v0, v2

    sput-object v0, Landroid/support/design/widget/NavigationView;->a:[I

    .line 75
    new-array v0, v3, [I

    const v1, -0x101009e

    aput v1, v0, v2

    sput-object v0, Landroid/support/design/widget/NavigationView;->b:[I

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 88
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/design/widget/NavigationView;-><init>(Landroid/content/Context;B)V

    .line 89
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .registers 4

    .prologue
    .line 92
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/design/widget/NavigationView;-><init>(Landroid/content/Context;C)V

    .line 93
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;C)V
    .registers 13

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 96
    invoke-direct {p0, p1, v2}, Landroid/support/design/internal/f;-><init>(Landroid/content/Context;C)V

    .line 80
    new-instance v0, Landroid/support/design/internal/b;

    invoke-direct {v0}, Landroid/support/design/internal/b;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/NavigationView;->e:Landroid/support/design/internal/b;

    .line 99
    new-instance v0, Landroid/support/design/internal/a;

    invoke-direct {v0, p1}, Landroid/support/design/internal/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/design/widget/NavigationView;->d:Landroid/support/design/internal/a;

    .line 102
    sget-object v0, Landroid/support/design/n;->NavigationView:[I

    sget v1, Landroid/support/design/m;->Widget_Design_NavigationView:I

    invoke-virtual {p1, v5, v0, v2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v6

    .line 107
    sget v0, Landroid/support/design/n;->NavigationView_android_background:I

    invoke-virtual {v6, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/design/widget/NavigationView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 108
    sget v0, Landroid/support/design/n;->NavigationView_elevation:I

    invoke-virtual {v6, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 109
    sget v0, Landroid/support/design/n;->NavigationView_elevation:I

    invoke-virtual {v6, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    int-to-float v0, v0

    invoke-static {p0, v0}, Landroid/support/v4/view/cx;->f(Landroid/view/View;F)V

    .line 112
    :cond_37
    sget v0, Landroid/support/design/n;->NavigationView_android_fitsSystemWindows:I

    invoke-virtual {v6, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    invoke-static {p0, v0}, Landroid/support/v4/view/cx;->a(Landroid/view/View;Z)V

    .line 115
    sget v0, Landroid/support/design/n;->NavigationView_android_maxWidth:I

    invoke-virtual {v6, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Landroid/support/design/widget/NavigationView;->g:I

    .line 118
    sget v0, Landroid/support/design/n;->NavigationView_itemIconTint:I

    invoke-virtual {v6, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_105

    .line 119
    sget v0, Landroid/support/design/n;->NavigationView_itemIconTint:I

    invoke-virtual {v6, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 126
    :goto_56
    sget v1, Landroid/support/design/n;->NavigationView_itemTextAppearance:I

    invoke-virtual {v6, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_10e

    .line 127
    sget v1, Landroid/support/design/n;->NavigationView_itemTextAppearance:I

    invoke-virtual {v6, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    move v3, v4

    .line 132
    :goto_65
    sget v7, Landroid/support/design/n;->NavigationView_itemTextColor:I

    invoke-virtual {v6, v7}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v7

    if-eqz v7, :cond_73

    .line 133
    sget v5, Landroid/support/design/n;->NavigationView_itemTextColor:I

    invoke-virtual {v6, v5}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v5

    .line 136
    :cond_73
    if-nez v3, :cond_7e

    if-nez v5, :cond_7e

    .line 138
    const v5, 0x1010036

    invoke-direct {p0, v5}, Landroid/support/design/widget/NavigationView;->c(I)Landroid/content/res/ColorStateList;

    move-result-object v5

    .line 141
    :cond_7e
    sget v7, Landroid/support/design/n;->NavigationView_itemBackground:I

    invoke-virtual {v6, v7}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 143
    iget-object v8, p0, Landroid/support/design/widget/NavigationView;->d:Landroid/support/design/internal/a;

    new-instance v9, Landroid/support/design/widget/ao;

    invoke-direct {v9, p0}, Landroid/support/design/widget/ao;-><init>(Landroid/support/design/widget/NavigationView;)V

    invoke-virtual {v8, v9}, Landroid/support/design/internal/a;->a(Landroid/support/v7/internal/view/menu/j;)V

    .line 152
    iget-object v8, p0, Landroid/support/design/widget/NavigationView;->e:Landroid/support/design/internal/b;

    .line 1157
    iput v4, v8, Landroid/support/design/internal/b;->c:I

    .line 153
    iget-object v8, p0, Landroid/support/design/widget/NavigationView;->e:Landroid/support/design/internal/b;

    iget-object v9, p0, Landroid/support/design/widget/NavigationView;->d:Landroid/support/design/internal/a;

    invoke-virtual {v8, p1, v9}, Landroid/support/design/internal/b;->a(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;)V

    .line 154
    iget-object v8, p0, Landroid/support/design/widget/NavigationView;->e:Landroid/support/design/internal/b;

    invoke-virtual {v8, v0}, Landroid/support/design/internal/b;->a(Landroid/content/res/ColorStateList;)V

    .line 155
    if-eqz v3, :cond_a5

    .line 156
    iget-object v0, p0, Landroid/support/design/widget/NavigationView;->e:Landroid/support/design/internal/b;

    invoke-virtual {v0, v1}, Landroid/support/design/internal/b;->a(I)V

    .line 158
    :cond_a5
    iget-object v0, p0, Landroid/support/design/widget/NavigationView;->e:Landroid/support/design/internal/b;

    invoke-virtual {v0, v5}, Landroid/support/design/internal/b;->b(Landroid/content/res/ColorStateList;)V

    .line 159
    iget-object v0, p0, Landroid/support/design/widget/NavigationView;->e:Landroid/support/design/internal/b;

    .line 1256
    iput-object v7, v0, Landroid/support/design/internal/b;->h:Landroid/graphics/drawable/Drawable;

    .line 160
    iget-object v0, p0, Landroid/support/design/widget/NavigationView;->d:Landroid/support/design/internal/a;

    iget-object v1, p0, Landroid/support/design/widget/NavigationView;->e:Landroid/support/design/internal/b;

    invoke-virtual {v0, v1}, Landroid/support/design/internal/a;->a(Landroid/support/v7/internal/view/menu/x;)V

    .line 161
    iget-object v0, p0, Landroid/support/design/widget/NavigationView;->e:Landroid/support/design/internal/b;

    invoke-virtual {v0, p0}, Landroid/support/design/internal/b;->a(Landroid/view/ViewGroup;)Landroid/support/v7/internal/view/menu/z;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Landroid/support/design/widget/NavigationView;->addView(Landroid/view/View;)V

    .line 163
    sget v0, Landroid/support/design/n;->NavigationView_menu:I

    invoke-virtual {v6, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_e6

    .line 164
    sget v0, Landroid/support/design/n;->NavigationView_menu:I

    invoke-virtual {v6, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 2226
    iget-object v1, p0, Landroid/support/design/widget/NavigationView;->e:Landroid/support/design/internal/b;

    invoke-virtual {v1, v4}, Landroid/support/design/internal/b;->b(Z)V

    .line 2227
    invoke-direct {p0}, Landroid/support/design/widget/NavigationView;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    iget-object v3, p0, Landroid/support/design/widget/NavigationView;->d:Landroid/support/design/internal/a;

    invoke-virtual {v1, v0, v3}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2228
    iget-object v0, p0, Landroid/support/design/widget/NavigationView;->e:Landroid/support/design/internal/b;

    invoke-virtual {v0, v2}, Landroid/support/design/internal/b;->b(Z)V

    .line 2229
    iget-object v0, p0, Landroid/support/design/widget/NavigationView;->e:Landroid/support/design/internal/b;

    invoke-virtual {v0, v2}, Landroid/support/design/internal/b;->a(Z)V

    .line 167
    :cond_e6
    sget v0, Landroid/support/design/n;->NavigationView_headerLayout:I

    invoke-virtual {v6, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_101

    .line 168
    sget v0, Landroid/support/design/n;->NavigationView_headerLayout:I

    invoke-virtual {v6, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 2246
    iget-object v1, p0, Landroid/support/design/widget/NavigationView;->e:Landroid/support/design/internal/b;

    .line 3207
    iget-object v3, v1, Landroid/support/design/internal/b;->e:Landroid/view/LayoutInflater;

    iget-object v4, v1, Landroid/support/design/internal/b;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0, v4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 3208
    invoke-virtual {v1, v0}, Landroid/support/design/internal/b;->a(Landroid/view/View;)V

    .line 171
    :cond_101
    invoke-virtual {v6}, Landroid/content/res/TypedArray;->recycle()V

    .line 172
    return-void

    .line 121
    :cond_105
    const v0, 0x1010038

    invoke-direct {p0, v0}, Landroid/support/design/widget/NavigationView;->c(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    goto/16 :goto_56

    :cond_10e
    move v1, v2

    move v3, v2

    goto/16 :goto_65
.end method

.method static synthetic a(Landroid/support/design/widget/NavigationView;)Landroid/support/design/widget/ap;
    .registers 2

    .prologue
    .line 72
    iget-object v0, p0, Landroid/support/design/widget/NavigationView;->f:Landroid/support/design/widget/ap;

    return-object v0
.end method

.method private a(I)V
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 226
    iget-object v0, p0, Landroid/support/design/widget/NavigationView;->e:Landroid/support/design/internal/b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/design/internal/b;->b(Z)V

    .line 227
    invoke-direct {p0}, Landroid/support/design/widget/NavigationView;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    iget-object v1, p0, Landroid/support/design/widget/NavigationView;->d:Landroid/support/design/internal/a;

    invoke-virtual {v0, p1, v1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 228
    iget-object v0, p0, Landroid/support/design/widget/NavigationView;->e:Landroid/support/design/internal/b;

    invoke-virtual {v0, v2}, Landroid/support/design/internal/b;->b(Z)V

    .line 229
    iget-object v0, p0, Landroid/support/design/widget/NavigationView;->e:Landroid/support/design/internal/b;

    invoke-virtual {v0, v2}, Landroid/support/design/internal/b;->a(Z)V

    .line 230
    return-void
.end method

.method private a(Landroid/view/View;)V
    .registers 3
    .param p1    # Landroid/view/View;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    .line 255
    iget-object v0, p0, Landroid/support/design/widget/NavigationView;->e:Landroid/support/design/internal/b;

    invoke-virtual {v0, p1}, Landroid/support/design/internal/b;->a(Landroid/view/View;)V

    .line 256
    return-void
.end method

.method private b(I)Landroid/view/View;
    .registers 6
    .param p1    # I
        .annotation build Landroid/support/a/v;
        .end annotation
    .end param

    .prologue
    .line 246
    iget-object v0, p0, Landroid/support/design/widget/NavigationView;->e:Landroid/support/design/internal/b;

    .line 4207
    iget-object v1, v0, Landroid/support/design/internal/b;->e:Landroid/view/LayoutInflater;

    iget-object v2, v0, Landroid/support/design/internal/b;->b:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 4208
    invoke-virtual {v0, v1}, Landroid/support/design/internal/b;->a(Landroid/view/View;)V

    .line 246
    return-object v1
.end method

.method private b(Landroid/view/View;)V
    .registers 6
    .param p1    # Landroid/view/View;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 264
    iget-object v0, p0, Landroid/support/design/widget/NavigationView;->e:Landroid/support/design/internal/b;

    .line 4219
    iget-object v1, v0, Landroid/support/design/internal/b;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 4220
    iget-object v1, v0, Landroid/support/design/internal/b;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-nez v1, :cond_1d

    .line 4221
    iget-object v1, v0, Landroid/support/design/internal/b;->a:Landroid/support/design/internal/NavigationMenuView;

    iget v2, v0, Landroid/support/design/internal/b;->i:I

    iget-object v0, v0, Landroid/support/design/internal/b;->a:Landroid/support/design/internal/NavigationMenuView;

    invoke-virtual {v0}, Landroid/support/design/internal/NavigationMenuView;->getPaddingBottom()I

    move-result v0

    invoke-virtual {v1, v3, v2, v3, v0}, Landroid/support/design/internal/NavigationMenuView;->setPadding(IIII)V

    .line 265
    :cond_1d
    return-void
.end method

.method private c(I)Landroid/content/res/ColorStateList;
    .registers 12

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 374
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 375
    invoke-virtual {p0}, Landroid/support/design/widget/NavigationView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    invoke-virtual {v2, p1, v1, v7}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v2

    if-nez v2, :cond_19

    .line 384
    :cond_18
    :goto_18
    return-object v0

    .line 378
    :cond_19
    invoke-virtual {p0}, Landroid/support/design/widget/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    .line 379
    invoke-virtual {p0}, Landroid/support/design/widget/NavigationView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    sget v4, Landroid/support/design/d;->colorPrimary:I

    invoke-virtual {v3, v4, v1, v7}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 382
    iget v1, v1, Landroid/util/TypedValue;->data:I

    .line 383
    invoke-virtual {v2}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v3

    .line 384
    new-instance v0, Landroid/content/res/ColorStateList;

    new-array v4, v6, [[I

    sget-object v5, Landroid/support/design/widget/NavigationView;->b:[I

    aput-object v5, v4, v8

    sget-object v5, Landroid/support/design/widget/NavigationView;->a:[I

    aput-object v5, v4, v7

    sget-object v5, Landroid/support/design/widget/NavigationView;->EMPTY_STATE_SET:[I

    aput-object v5, v4, v9

    new-array v5, v6, [I

    sget-object v6, Landroid/support/design/widget/NavigationView;->b:[I

    invoke-virtual {v2, v6, v3}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v2

    aput v2, v5, v8

    aput v1, v5, v7

    aput v3, v5, v9

    invoke-direct {v0, v4, v5}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    goto :goto_18
.end method

.method private getMenuInflater()Landroid/view/MenuInflater;
    .registers 3

    .prologue
    .line 367
    iget-object v0, p0, Landroid/support/design/widget/NavigationView;->h:Landroid/view/MenuInflater;

    if-nez v0, :cond_f

    .line 368
    new-instance v0, Landroid/support/v7/internal/view/f;

    invoke-virtual {p0}, Landroid/support/design/widget/NavigationView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/internal/view/f;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/design/widget/NavigationView;->h:Landroid/view/MenuInflater;

    .line 370
    :cond_f
    iget-object v0, p0, Landroid/support/design/widget/NavigationView;->h:Landroid/view/MenuInflater;

    return-object v0
.end method


# virtual methods
.method public final getItemBackground()Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 321
    iget-object v0, p0, Landroid/support/design/widget/NavigationView;->e:Landroid/support/design/internal/b;

    .line 4252
    iget-object v0, v0, Landroid/support/design/internal/b;->h:Landroid/graphics/drawable/Drawable;

    .line 321
    return-object v0
.end method

.method public final getItemIconTintList()Landroid/content/res/ColorStateList;
    .registers 2
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 276
    iget-object v0, p0, Landroid/support/design/widget/NavigationView;->e:Landroid/support/design/internal/b;

    .line 4227
    iget-object v0, v0, Landroid/support/design/internal/b;->g:Landroid/content/res/ColorStateList;

    .line 276
    return-object v0
.end method

.method public final getItemTextColor()Landroid/content/res/ColorStateList;
    .registers 2
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 299
    iget-object v0, p0, Landroid/support/design/widget/NavigationView;->e:Landroid/support/design/internal/b;

    .line 4237
    iget-object v0, v0, Landroid/support/design/internal/b;->f:Landroid/content/res/ColorStateList;

    .line 299
    return-object v0
.end method

.method public final getMenu()Landroid/view/Menu;
    .registers 2

    .prologue
    .line 236
    iget-object v0, p0, Landroid/support/design/widget/NavigationView;->d:Landroid/support/design/internal/a;

    return-object v0
.end method

.method protected final onMeasure(II)V
    .registers 6

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 201
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_24

    .line 214
    :goto_9
    :sswitch_9
    invoke-super {p0, p1, p2}, Landroid/support/design/internal/f;->onMeasure(II)V

    .line 215
    return-void

    .line 206
    :sswitch_d
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iget v1, p0, Landroid/support/design/widget/NavigationView;->g:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    goto :goto_9

    .line 210
    :sswitch_1c
    iget v0, p0, Landroid/support/design/widget/NavigationView;->g:I

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    goto :goto_9

    .line 201
    nop

    :sswitch_data_24
    .sparse-switch
        -0x80000000 -> :sswitch_d
        0x0 -> :sswitch_1c
        0x40000000 -> :sswitch_9
    .end sparse-switch
.end method

.method protected final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 4

    .prologue
    .line 185
    check-cast p1, Landroid/support/design/widget/NavigationView$SavedState;

    .line 186
    invoke-virtual {p1}, Landroid/support/design/widget/NavigationView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/support/design/internal/f;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 187
    iget-object v0, p0, Landroid/support/design/widget/NavigationView;->d:Landroid/support/design/internal/a;

    iget-object v1, p1, Landroid/support/design/widget/NavigationView$SavedState;->a:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/support/design/internal/a;->b(Landroid/os/Bundle;)V

    .line 188
    return-void
.end method

.method protected final onSaveInstanceState()Landroid/os/Parcelable;
    .registers 4

    .prologue
    .line 176
    invoke-super {p0}, Landroid/support/design/internal/f;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 177
    new-instance v1, Landroid/support/design/widget/NavigationView$SavedState;

    invoke-direct {v1, v0}, Landroid/support/design/widget/NavigationView$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 178
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, v1, Landroid/support/design/widget/NavigationView$SavedState;->a:Landroid/os/Bundle;

    .line 179
    iget-object v0, p0, Landroid/support/design/widget/NavigationView;->d:Landroid/support/design/internal/a;

    iget-object v2, v1, Landroid/support/design/widget/NavigationView$SavedState;->a:Landroid/os/Bundle;

    invoke-virtual {v0, v2}, Landroid/support/design/internal/a;->a(Landroid/os/Bundle;)V

    .line 180
    return-object v1
.end method

.method public final setCheckedItem(I)V
    .registers 4
    .param p1    # I
        .annotation build Landroid/support/a/p;
        .end annotation
    .end param

    .prologue
    .line 351
    iget-object v0, p0, Landroid/support/design/widget/NavigationView;->d:Landroid/support/design/internal/a;

    invoke-virtual {v0, p1}, Landroid/support/design/internal/a;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 352
    if-eqz v0, :cond_11

    .line 353
    iget-object v1, p0, Landroid/support/design/widget/NavigationView;->e:Landroid/support/design/internal/b;

    check-cast v0, Landroid/support/v7/internal/view/menu/m;

    .line 5203
    iget-object v1, v1, Landroid/support/design/internal/b;->d:Landroid/support/design/internal/c;

    invoke-virtual {v1, v0}, Landroid/support/design/internal/c;->a(Landroid/support/v7/internal/view/menu/m;)V

    .line 355
    :cond_11
    return-void
.end method

.method public final setItemBackground(Landroid/graphics/drawable/Drawable;)V
    .registers 3

    .prologue
    .line 342
    iget-object v0, p0, Landroid/support/design/widget/NavigationView;->e:Landroid/support/design/internal/b;

    .line 4256
    iput-object p1, v0, Landroid/support/design/internal/b;->h:Landroid/graphics/drawable/Drawable;

    .line 343
    return-void
.end method

.method public final setItemBackgroundResource(I)V
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/m;
        .end annotation
    .end param

    .prologue
    .line 332
    invoke-virtual {p0}, Landroid/support/design/widget/NavigationView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/support/v4/c/h;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/design/widget/NavigationView;->setItemBackground(Landroid/graphics/drawable/Drawable;)V

    .line 333
    return-void
.end method

.method public final setItemIconTintList(Landroid/content/res/ColorStateList;)V
    .registers 3
    .param p1    # Landroid/content/res/ColorStateList;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 287
    iget-object v0, p0, Landroid/support/design/widget/NavigationView;->e:Landroid/support/design/internal/b;

    invoke-virtual {v0, p1}, Landroid/support/design/internal/b;->a(Landroid/content/res/ColorStateList;)V

    .line 288
    return-void
.end method

.method public final setItemTextAppearance(I)V
    .registers 3
    .param p1    # I
        .annotation build Landroid/support/a/ai;
        .end annotation
    .end param

    .prologue
    .line 363
    iget-object v0, p0, Landroid/support/design/widget/NavigationView;->e:Landroid/support/design/internal/b;

    invoke-virtual {v0, p1}, Landroid/support/design/internal/b;->a(I)V

    .line 364
    return-void
.end method

.method public final setItemTextColor(Landroid/content/res/ColorStateList;)V
    .registers 3
    .param p1    # Landroid/content/res/ColorStateList;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 310
    iget-object v0, p0, Landroid/support/design/widget/NavigationView;->e:Landroid/support/design/internal/b;

    invoke-virtual {v0, p1}, Landroid/support/design/internal/b;->b(Landroid/content/res/ColorStateList;)V

    .line 311
    return-void
.end method

.method public final setNavigationItemSelectedListener(Landroid/support/design/widget/ap;)V
    .registers 2

    .prologue
    .line 196
    iput-object p1, p0, Landroid/support/design/widget/NavigationView;->f:Landroid/support/design/widget/ap;

    .line 197
    return-void
.end method
