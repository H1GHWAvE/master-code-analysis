.class public Landroid/support/design/widget/SwipeDismissBehavior;
.super Landroid/support/design/widget/t;
.source "SourceFile"


# static fields
.field private static final a:F = 0.5f

.field public static final b:I = 0x0

.field public static final c:I = 0x1

.field public static final d:I = 0x2

.field public static final e:I = 0x0

.field public static final f:I = 0x1

.field public static final g:I = 0x2

.field private static final m:F = 0.0f

.field private static final n:F = 0.5f


# instance fields
.field h:Landroid/support/v4/widget/eg;

.field i:Landroid/support/design/widget/bp;

.field j:I

.field k:F

.field l:F

.field private o:Z

.field private p:F

.field private q:Z

.field private r:F

.field private final s:Landroid/support/v4/widget/ej;


# direct methods
.method public constructor <init>()V
    .registers 4

    .prologue
    const/high16 v2, 0x3f000000    # 0.5f

    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Landroid/support/design/widget/t;-><init>()V

    .line 83
    iput v1, p0, Landroid/support/design/widget/SwipeDismissBehavior;->p:F

    .line 86
    const/4 v0, 0x2

    iput v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->j:I

    .line 87
    iput v2, p0, Landroid/support/design/widget/SwipeDismissBehavior;->r:F

    .line 88
    iput v1, p0, Landroid/support/design/widget/SwipeDismissBehavior;->k:F

    .line 89
    iput v2, p0, Landroid/support/design/widget/SwipeDismissBehavior;->l:F

    .line 201
    new-instance v0, Landroid/support/design/widget/bo;

    invoke-direct {v0, p0}, Landroid/support/design/widget/bo;-><init>(Landroid/support/design/widget/SwipeDismissBehavior;)V

    iput-object v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->s:Landroid/support/v4/widget/ej;

    .line 333
    return-void
.end method

.method static a(F)F
    .registers 3

    .prologue
    .line 355
    const/4 v0, 0x0

    invoke-static {v0, p0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method static a(FFF)F
    .registers 5

    .prologue
    .line 376
    sub-float v0, p2, p0

    sub-float v1, p1, p0

    div-float/2addr v0, v1

    return v0
.end method

.method static synthetic a(III)I
    .registers 4

    .prologue
    .line 34
    .line 2359
    invoke-static {p0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 34
    return v0
.end method

.method static synthetic a(Landroid/support/design/widget/SwipeDismissBehavior;)Landroid/support/design/widget/bp;
    .registers 2

    .prologue
    .line 34
    iget-object v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->i:Landroid/support/design/widget/bp;

    return-object v0
.end method

.method private a()V
    .registers 2

    .prologue
    .line 125
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->j:I

    .line 126
    return-void
.end method

.method private a(Landroid/support/design/widget/bp;)V
    .registers 2

    .prologue
    .line 115
    iput-object p1, p0, Landroid/support/design/widget/SwipeDismissBehavior;->i:Landroid/support/design/widget/bp;

    .line 116
    return-void
.end method

.method private a(Landroid/view/ViewGroup;)V
    .registers 4

    .prologue
    .line 326
    iget-object v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->h:Landroid/support/v4/widget/eg;

    if-nez v0, :cond_12

    .line 327
    iget-boolean v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->q:Z

    if-eqz v0, :cond_13

    iget v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->p:F

    iget-object v1, p0, Landroid/support/design/widget/SwipeDismissBehavior;->s:Landroid/support/v4/widget/ej;

    invoke-static {p1, v0, v1}, Landroid/support/v4/widget/eg;->a(Landroid/view/ViewGroup;FLandroid/support/v4/widget/ej;)Landroid/support/v4/widget/eg;

    move-result-object v0

    :goto_10
    iput-object v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->h:Landroid/support/v4/widget/eg;

    .line 331
    :cond_12
    return-void

    .line 327
    :cond_13
    iget-object v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->s:Landroid/support/v4/widget/ej;

    invoke-static {p1, v0}, Landroid/support/v4/widget/eg;->a(Landroid/view/ViewGroup;Landroid/support/v4/widget/ej;)Landroid/support/v4/widget/eg;

    move-result-object v0

    goto :goto_10
.end method

.method static synthetic b(F)F
    .registers 2

    .prologue
    .line 34
    invoke-static {p0}, Landroid/support/design/widget/SwipeDismissBehavior;->a(F)F

    move-result v0

    return v0
.end method

.method private static b(III)I
    .registers 4

    .prologue
    .line 359
    invoke-static {p0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method static synthetic b(Landroid/support/design/widget/SwipeDismissBehavior;)Landroid/support/v4/widget/eg;
    .registers 2

    .prologue
    .line 34
    iget-object v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->h:Landroid/support/v4/widget/eg;

    return-object v0
.end method

.method private b()V
    .registers 2

    .prologue
    .line 143
    const v0, 0x3dcccccd    # 0.1f

    invoke-static {v0}, Landroid/support/design/widget/SwipeDismissBehavior;->a(F)F

    move-result v0

    iput v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->k:F

    .line 144
    return-void
.end method

.method static synthetic c(Landroid/support/design/widget/SwipeDismissBehavior;)I
    .registers 2

    .prologue
    .line 34
    iget v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->j:I

    return v0
.end method

.method private c()V
    .registers 2

    .prologue
    .line 152
    const v0, 0x3f19999a    # 0.6f

    invoke-static {v0}, Landroid/support/design/widget/SwipeDismissBehavior;->a(F)F

    move-result v0

    iput v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->l:F

    .line 153
    return-void
.end method

.method private c(F)V
    .registers 3

    .prologue
    .line 134
    invoke-static {p1}, Landroid/support/design/widget/SwipeDismissBehavior;->a(F)F

    move-result v0

    iput v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->r:F

    .line 135
    return-void
.end method

.method static synthetic d(Landroid/support/design/widget/SwipeDismissBehavior;)F
    .registers 2

    .prologue
    .line 34
    iget v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->r:F

    return v0
.end method

.method private d()I
    .registers 2

    .prologue
    .line 369
    iget-object v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->h:Landroid/support/v4/widget/eg;

    if-eqz v0, :cond_9

    iget-object v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->h:Landroid/support/v4/widget/eg;

    .line 1421
    iget v0, v0, Landroid/support/v4/widget/eg;->m:I

    .line 369
    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method private d(F)V
    .registers 3

    .prologue
    .line 163
    iput p1, p0, Landroid/support/design/widget/SwipeDismissBehavior;->p:F

    .line 164
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->q:Z

    .line 165
    return-void
.end method

.method static synthetic e(Landroid/support/design/widget/SwipeDismissBehavior;)F
    .registers 2

    .prologue
    .line 34
    iget v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->k:F

    return v0
.end method

.method static synthetic f(Landroid/support/design/widget/SwipeDismissBehavior;)F
    .registers 2

    .prologue
    .line 34
    iget v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->l:F

    return v0
.end method


# virtual methods
.method public final a(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 5

    .prologue
    .line 194
    iget-object v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->h:Landroid/support/v4/widget/eg;

    if-eqz v0, :cond_b

    .line 195
    iget-object v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->h:Landroid/support/v4/widget/eg;

    invoke-virtual {v0, p3}, Landroid/support/v4/widget/eg;->b(Landroid/view/MotionEvent;)V

    .line 196
    const/4 v0, 0x1

    .line 198
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public b(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 169
    invoke-static {p3}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;)I

    move-result v0

    packed-switch v0, :pswitch_data_4a

    .line 179
    :pswitch_8
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1, p2, v0, v2}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;II)Z

    move-result v0

    if-nez v0, :cond_27

    const/4 v0, 0x1

    :goto_19
    iput-boolean v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->o:Z

    .line 184
    :cond_1b
    iget-boolean v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->o:Z

    if-eqz v0, :cond_29

    .line 189
    :goto_1f
    return v1

    .line 173
    :pswitch_20
    iget-boolean v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->o:Z

    if-eqz v0, :cond_1b

    .line 174
    iput-boolean v1, p0, Landroid/support/design/widget/SwipeDismissBehavior;->o:Z

    goto :goto_1f

    :cond_27
    move v0, v1

    .line 179
    goto :goto_19

    .line 1326
    :cond_29
    iget-object v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->h:Landroid/support/v4/widget/eg;

    if-nez v0, :cond_3b

    .line 1327
    iget-boolean v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->q:Z

    if-eqz v0, :cond_42

    iget v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->p:F

    iget-object v1, p0, Landroid/support/design/widget/SwipeDismissBehavior;->s:Landroid/support/v4/widget/ej;

    invoke-static {p1, v0, v1}, Landroid/support/v4/widget/eg;->a(Landroid/view/ViewGroup;FLandroid/support/v4/widget/ej;)Landroid/support/v4/widget/eg;

    move-result-object v0

    :goto_39
    iput-object v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->h:Landroid/support/v4/widget/eg;

    .line 189
    :cond_3b
    iget-object v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->h:Landroid/support/v4/widget/eg;

    invoke-virtual {v0, p3}, Landroid/support/v4/widget/eg;->a(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_1f

    .line 1327
    :cond_42
    iget-object v0, p0, Landroid/support/design/widget/SwipeDismissBehavior;->s:Landroid/support/v4/widget/ej;

    invoke-static {p1, v0}, Landroid/support/v4/widget/eg;->a(Landroid/view/ViewGroup;Landroid/support/v4/widget/ej;)Landroid/support/v4/widget/eg;

    move-result-object v0

    goto :goto_39

    .line 169
    nop

    :pswitch_data_4a
    .packed-switch 0x1
        :pswitch_20
        :pswitch_8
        :pswitch_20
    .end packed-switch
.end method
