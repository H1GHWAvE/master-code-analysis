.class final Landroid/support/design/widget/cu;
.super Landroid/support/design/widget/cr;
.source "SourceFile"


# static fields
.field private static final a:I = 0xa

.field private static final b:I = 0xc8

.field private static final c:Landroid/os/Handler;


# instance fields
.field private d:J

.field private e:Z

.field private final f:[I

.field private final g:[F

.field private h:I

.field private i:Landroid/view/animation/Interpolator;

.field private j:Landroid/support/design/widget/cs;

.field private k:Landroid/support/design/widget/ct;

.field private l:F

.field private final m:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 33
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Landroid/support/design/widget/cu;->c:Landroid/os/Handler;

    return-void
.end method

.method constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x2

    .line 28
    invoke-direct {p0}, Landroid/support/design/widget/cr;-><init>()V

    .line 38
    new-array v0, v1, [I

    iput-object v0, p0, Landroid/support/design/widget/cu;->f:[I

    .line 39
    new-array v0, v1, [F

    iput-object v0, p0, Landroid/support/design/widget/cu;->g:[F

    .line 41
    const/16 v0, 0xc8

    iput v0, p0, Landroid/support/design/widget/cu;->h:I

    .line 180
    new-instance v0, Landroid/support/design/widget/cv;

    invoke-direct {v0, p0}, Landroid/support/design/widget/cv;-><init>(Landroid/support/design/widget/cu;)V

    iput-object v0, p0, Landroid/support/design/widget/cu;->m:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Landroid/support/design/widget/cu;)V
    .registers 7

    .prologue
    .line 28
    .line 2151
    iget-boolean v0, p0, Landroid/support/design/widget/cu;->e:Z

    if-eqz v0, :cond_3f

    .line 2153
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Landroid/support/design/widget/cu;->d:J

    sub-long/2addr v0, v2

    .line 2154
    long-to-float v0, v0

    iget v1, p0, Landroid/support/design/widget/cu;->h:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 2155
    iget-object v1, p0, Landroid/support/design/widget/cu;->i:Landroid/view/animation/Interpolator;

    if-eqz v1, :cond_1a

    iget-object v1, p0, Landroid/support/design/widget/cu;->i:Landroid/view/animation/Interpolator;

    invoke-interface {v1, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    :cond_1a
    iput v0, p0, Landroid/support/design/widget/cu;->l:F

    .line 2160
    iget-object v0, p0, Landroid/support/design/widget/cu;->k:Landroid/support/design/widget/ct;

    if-eqz v0, :cond_25

    .line 2161
    iget-object v0, p0, Landroid/support/design/widget/cu;->k:Landroid/support/design/widget/ct;

    invoke-interface {v0}, Landroid/support/design/widget/ct;->a()V

    .line 2165
    :cond_25
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Landroid/support/design/widget/cu;->d:J

    iget v4, p0, Landroid/support/design/widget/cu;->h:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-ltz v0, :cond_3f

    .line 2166
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/design/widget/cu;->e:Z

    .line 2168
    iget-object v0, p0, Landroid/support/design/widget/cu;->j:Landroid/support/design/widget/cs;

    if-eqz v0, :cond_3f

    .line 2169
    iget-object v0, p0, Landroid/support/design/widget/cu;->j:Landroid/support/design/widget/cs;

    invoke-interface {v0}, Landroid/support/design/widget/cs;->b()V

    .line 2174
    :cond_3f
    iget-boolean v0, p0, Landroid/support/design/widget/cu;->e:Z

    if-eqz v0, :cond_4c

    .line 2176
    sget-object v0, Landroid/support/design/widget/cu;->c:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/design/widget/cu;->m:Ljava/lang/Runnable;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 28
    :cond_4c
    return-void
.end method

.method private h()V
    .registers 7

    .prologue
    .line 151
    iget-boolean v0, p0, Landroid/support/design/widget/cu;->e:Z

    if-eqz v0, :cond_3f

    .line 153
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Landroid/support/design/widget/cu;->d:J

    sub-long/2addr v0, v2

    .line 154
    long-to-float v0, v0

    iget v1, p0, Landroid/support/design/widget/cu;->h:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 155
    iget-object v1, p0, Landroid/support/design/widget/cu;->i:Landroid/view/animation/Interpolator;

    if-eqz v1, :cond_1a

    iget-object v1, p0, Landroid/support/design/widget/cu;->i:Landroid/view/animation/Interpolator;

    invoke-interface {v1, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    :cond_1a
    iput v0, p0, Landroid/support/design/widget/cu;->l:F

    .line 160
    iget-object v0, p0, Landroid/support/design/widget/cu;->k:Landroid/support/design/widget/ct;

    if-eqz v0, :cond_25

    .line 161
    iget-object v0, p0, Landroid/support/design/widget/cu;->k:Landroid/support/design/widget/ct;

    invoke-interface {v0}, Landroid/support/design/widget/ct;->a()V

    .line 165
    :cond_25
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Landroid/support/design/widget/cu;->d:J

    iget v4, p0, Landroid/support/design/widget/cu;->h:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-ltz v0, :cond_3f

    .line 166
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/design/widget/cu;->e:Z

    .line 168
    iget-object v0, p0, Landroid/support/design/widget/cu;->j:Landroid/support/design/widget/cs;

    if-eqz v0, :cond_3f

    .line 169
    iget-object v0, p0, Landroid/support/design/widget/cu;->j:Landroid/support/design/widget/cs;

    invoke-interface {v0}, Landroid/support/design/widget/cs;->b()V

    .line 174
    :cond_3f
    iget-boolean v0, p0, Landroid/support/design/widget/cu;->e:Z

    if-eqz v0, :cond_4c

    .line 176
    sget-object v0, Landroid/support/design/widget/cu;->c:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/design/widget/cu;->m:Ljava/lang/Runnable;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 178
    :cond_4c
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 5

    .prologue
    .line 50
    iget-boolean v0, p0, Landroid/support/design/widget/cu;->e:Z

    if-eqz v0, :cond_5

    .line 67
    :goto_4
    return-void

    .line 55
    :cond_5
    iget-object v0, p0, Landroid/support/design/widget/cu;->i:Landroid/view/animation/Interpolator;

    if-nez v0, :cond_10

    .line 56
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    iput-object v0, p0, Landroid/support/design/widget/cu;->i:Landroid/view/animation/Interpolator;

    .line 59
    :cond_10
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/support/design/widget/cu;->d:J

    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/design/widget/cu;->e:Z

    .line 66
    sget-object v0, Landroid/support/design/widget/cu;->c:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/design/widget/cu;->m:Ljava/lang/Runnable;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_4
.end method

.method public final a(FF)V
    .registers 5

    .prologue
    .line 102
    iget-object v0, p0, Landroid/support/design/widget/cu;->g:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 103
    iget-object v0, p0, Landroid/support/design/widget/cu;->g:[F

    const/4 v1, 0x1

    aput p2, v0, v1

    .line 104
    return-void
.end method

.method public final a(I)V
    .registers 2

    .prologue
    .line 113
    iput p1, p0, Landroid/support/design/widget/cu;->h:I

    .line 114
    return-void
.end method

.method public final a(II)V
    .registers 5

    .prologue
    .line 91
    iget-object v0, p0, Landroid/support/design/widget/cu;->f:[I

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 92
    iget-object v0, p0, Landroid/support/design/widget/cu;->f:[I

    const/4 v1, 0x1

    aput p2, v0, v1

    .line 93
    return-void
.end method

.method public final a(Landroid/support/design/widget/cs;)V
    .registers 2

    .prologue
    .line 81
    iput-object p1, p0, Landroid/support/design/widget/cu;->j:Landroid/support/design/widget/cs;

    .line 82
    return-void
.end method

.method public final a(Landroid/support/design/widget/ct;)V
    .registers 2

    .prologue
    .line 86
    iput-object p1, p0, Landroid/support/design/widget/cu;->k:Landroid/support/design/widget/ct;

    .line 87
    return-void
.end method

.method public final a(Landroid/view/animation/Interpolator;)V
    .registers 2

    .prologue
    .line 76
    iput-object p1, p0, Landroid/support/design/widget/cu;->i:Landroid/view/animation/Interpolator;

    .line 77
    return-void
.end method

.method public final b()Z
    .registers 2

    .prologue
    .line 71
    iget-boolean v0, p0, Landroid/support/design/widget/cu;->e:Z

    return v0
.end method

.method public final c()I
    .registers 4

    .prologue
    .line 97
    iget-object v0, p0, Landroid/support/design/widget/cu;->f:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    iget-object v1, p0, Landroid/support/design/widget/cu;->f:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    .line 1128
    iget v2, p0, Landroid/support/design/widget/cu;->l:F

    .line 97
    invoke-static {v0, v1, v2}, Landroid/support/design/widget/a;->a(IIF)I

    move-result v0

    return v0
.end method

.method public final d()F
    .registers 4

    .prologue
    .line 108
    iget-object v0, p0, Landroid/support/design/widget/cu;->g:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    iget-object v1, p0, Landroid/support/design/widget/cu;->g:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    .line 2128
    iget v2, p0, Landroid/support/design/widget/cu;->l:F

    .line 108
    invoke-static {v0, v1, v2}, Landroid/support/design/widget/a;->a(FFF)F

    move-result v0

    return v0
.end method

.method public final e()V
    .registers 3

    .prologue
    .line 118
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/design/widget/cu;->e:Z

    .line 119
    sget-object v0, Landroid/support/design/widget/cu;->c:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/design/widget/cu;->m:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 121
    iget-object v0, p0, Landroid/support/design/widget/cu;->j:Landroid/support/design/widget/cs;

    if-eqz v0, :cond_13

    .line 122
    iget-object v0, p0, Landroid/support/design/widget/cu;->j:Landroid/support/design/widget/cs;

    invoke-interface {v0}, Landroid/support/design/widget/cs;->c()V

    .line 124
    :cond_13
    return-void
.end method

.method public final f()F
    .registers 2

    .prologue
    .line 128
    iget v0, p0, Landroid/support/design/widget/cu;->l:F

    return v0
.end method

.method public final g()V
    .registers 3

    .prologue
    .line 133
    iget-boolean v0, p0, Landroid/support/design/widget/cu;->e:Z

    if-eqz v0, :cond_24

    .line 134
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/design/widget/cu;->e:Z

    .line 135
    sget-object v0, Landroid/support/design/widget/cu;->c:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/design/widget/cu;->m:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 138
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Landroid/support/design/widget/cu;->l:F

    .line 140
    iget-object v0, p0, Landroid/support/design/widget/cu;->k:Landroid/support/design/widget/ct;

    if-eqz v0, :cond_1b

    .line 141
    iget-object v0, p0, Landroid/support/design/widget/cu;->k:Landroid/support/design/widget/ct;

    invoke-interface {v0}, Landroid/support/design/widget/ct;->a()V

    .line 144
    :cond_1b
    iget-object v0, p0, Landroid/support/design/widget/cu;->j:Landroid/support/design/widget/cs;

    if-eqz v0, :cond_24

    .line 145
    iget-object v0, p0, Landroid/support/design/widget/cu;->j:Landroid/support/design/widget/cs;

    invoke-interface {v0}, Landroid/support/design/widget/cs;->b()V

    .line 148
    :cond_24
    return-void
.end method
