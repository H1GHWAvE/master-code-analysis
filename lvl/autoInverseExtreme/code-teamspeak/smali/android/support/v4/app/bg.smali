.class public final Landroid/support/v4/app/bg;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/support/v4/app/bh;


# direct methods
.method constructor <init>(Landroid/support/v4/app/bh;)V
    .registers 2

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    .line 52
    return-void
.end method

.method private static a(Landroid/support/v4/app/bh;)Landroid/support/v4/app/bg;
    .registers 2

    .prologue
    .line 47
    new-instance v0, Landroid/support/v4/app/bg;

    invoke-direct {v0, p0}, Landroid/support/v4/app/bg;-><init>(Landroid/support/v4/app/bh;)V

    return-object v0
.end method

.method private a(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .registers 6

    .prologue
    .line 111
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/support/v4/app/bl;->a(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/res/Configuration;)V
    .registers 3

    .prologue
    .line 246
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/bl;->a(Landroid/content/res/Configuration;)V

    .line 247
    return-void
.end method

.method private a(Landroid/os/Parcelable;Ljava/util/List;)V
    .registers 4

    .prologue
    .line 135
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/app/bl;->a(Landroid/os/Parcelable;Ljava/util/List;)V

    .line 136
    return-void
.end method

.method private a(Landroid/support/v4/n/v;)V
    .registers 3

    .prologue
    .line 387
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    .line 6309
    iput-object p1, v0, Landroid/support/v4/app/bh;->g:Landroid/support/v4/n/v;

    .line 388
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 8

    .prologue
    .line 394
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    .line 6313
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v1, "mLoadersStarted="

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 6314
    iget-boolean v1, v0, Landroid/support/v4/app/bh;->j:Z

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Z)V

    .line 6315
    iget-object v1, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    if-eqz v1, :cond_45

    .line 6316
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v1, "Loader Manager "

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 6317
    iget-object v1, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    invoke-static {v1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 6318
    const-string v1, ":"

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 6319
    iget-object v0, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, Landroid/support/v4/app/ct;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 395
    :cond_45
    return-void
.end method

.method private a(Z)V
    .registers 4

    .prologue
    .line 348
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    .line 4220
    iget-object v1, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    if-eqz v1, :cond_14

    .line 4224
    iget-boolean v1, v0, Landroid/support/v4/app/bh;->j:Z

    if-eqz v1, :cond_14

    .line 4227
    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/support/v4/app/bh;->j:Z

    .line 4229
    if-eqz p1, :cond_15

    .line 4230
    iget-object v0, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    invoke-virtual {v0}, Landroid/support/v4/app/ct;->d()V

    :cond_14
    :goto_14
    return-void

    .line 4232
    :cond_15
    iget-object v0, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    invoke-virtual {v0}, Landroid/support/v4/app/ct;->c()V

    goto :goto_14
.end method

.method private a(Landroid/view/Menu;)Z
    .registers 3

    .prologue
    .line 282
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/bl;->a(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method private a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
    .registers 4

    .prologue
    .line 270
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/app/bl;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    move-result v0

    return v0
.end method

.method private a(Landroid/view/MenuItem;)Z
    .registers 3

    .prologue
    .line 295
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/bl;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method private b(Landroid/view/Menu;)V
    .registers 3

    .prologue
    .line 319
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/bl;->b(Landroid/view/Menu;)V

    .line 320
    return-void
.end method

.method private b(Landroid/view/MenuItem;)Z
    .registers 3

    .prologue
    .line 308
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/bl;->b(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method private d()Landroid/support/v4/app/bi;
    .registers 2

    .prologue
    .line 58
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    .line 2174
    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    .line 58
    return-object v0
.end method

.method private e()Landroid/support/v4/app/cr;
    .registers 5

    .prologue
    const/4 v3, 0x1

    .line 65
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    .line 2178
    iget-object v1, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    if-eqz v1, :cond_a

    .line 2179
    iget-object v0, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    :goto_9
    return-object v0

    .line 2181
    :cond_a
    iput-boolean v3, v0, Landroid/support/v4/app/bh;->i:Z

    .line 2182
    const-string v1, "(root)"

    iget-boolean v2, v0, Landroid/support/v4/app/bh;->j:Z

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/bh;->a(Ljava/lang/String;ZZ)Landroid/support/v4/app/ct;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    .line 2183
    iget-object v0, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    goto :goto_9
.end method

.method private f()V
    .registers 5

    .prologue
    .line 95
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    iget-object v1, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v2, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/bl;->a(Landroid/support/v4/app/bh;Landroid/support/v4/app/bf;Landroid/support/v4/app/Fragment;)V

    .line 97
    return-void
.end method

.method private g()Landroid/os/Parcelable;
    .registers 2

    .prologue
    .line 125
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->m()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method private h()Ljava/util/List;
    .registers 8

    .prologue
    .line 143
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v4, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    .line 3686
    const/4 v1, 0x0

    .line 3687
    iget-object v0, v4, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    if-eqz v0, :cond_56

    .line 3688
    const/4 v0, 0x0

    move v3, v0

    :goto_b
    iget-object v0, v4, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_56

    .line 3689
    iget-object v0, v4, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 3690
    if-eqz v0, :cond_50

    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->V:Z

    if-eqz v2, :cond_50

    .line 3691
    if-nez v1, :cond_28

    .line 3692
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3694
    :cond_28
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3695
    const/4 v2, 0x1

    iput-boolean v2, v0, Landroid/support/v4/app/Fragment;->W:Z

    .line 3696
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->C:Landroid/support/v4/app/Fragment;

    if-eqz v2, :cond_54

    iget-object v2, v0, Landroid/support/v4/app/Fragment;->C:Landroid/support/v4/app/Fragment;

    iget v2, v2, Landroid/support/v4/app/Fragment;->z:I

    :goto_36
    iput v2, v0, Landroid/support/v4/app/Fragment;->D:I

    .line 3697
    sget-boolean v2, Landroid/support/v4/app/bl;->b:Z

    if-eqz v2, :cond_50

    const-string v2, "FragmentManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "retainNonConfig: keeping retained "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3688
    :cond_50
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_b

    .line 3696
    :cond_54
    const/4 v2, -0x1

    goto :goto_36

    .line 143
    :cond_56
    return-object v1
.end method

.method private i()V
    .registers 2

    .prologue
    .line 154
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->n()V

    .line 155
    return-void
.end method

.method private j()V
    .registers 2

    .prologue
    .line 165
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->o()V

    .line 166
    return-void
.end method

.method private k()V
    .registers 2

    .prologue
    .line 176
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->p()V

    .line 177
    return-void
.end method

.method private l()V
    .registers 2

    .prologue
    .line 187
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->q()V

    .line 188
    return-void
.end method

.method private m()V
    .registers 3

    .prologue
    .line 198
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    .line 4015
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bl;->c(I)V

    .line 199
    return-void
.end method

.method private n()V
    .registers 2

    .prologue
    .line 209
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->r()V

    .line 210
    return-void
.end method

.method private o()V
    .registers 3

    .prologue
    .line 213
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    .line 4028
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bl;->c(I)V

    .line 214
    return-void
.end method

.method private p()V
    .registers 3

    .prologue
    .line 224
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    .line 4032
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bl;->c(I)V

    .line 225
    return-void
.end method

.method private q()V
    .registers 2

    .prologue
    .line 235
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->s()V

    .line 236
    return-void
.end method

.method private r()V
    .registers 2

    .prologue
    .line 258
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->t()V

    .line 259
    return-void
.end method

.method private s()V
    .registers 6

    .prologue
    const/4 v4, 0x1

    .line 337
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    .line 4201
    iget-boolean v1, v0, Landroid/support/v4/app/bh;->j:Z

    if-nez v1, :cond_14

    .line 4204
    iput-boolean v4, v0, Landroid/support/v4/app/bh;->j:Z

    .line 4206
    iget-object v1, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    if-eqz v1, :cond_15

    .line 4207
    iget-object v1, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    invoke-virtual {v1}, Landroid/support/v4/app/ct;->b()V

    .line 4215
    :cond_12
    :goto_12
    iput-boolean v4, v0, Landroid/support/v4/app/bh;->i:Z

    .line 338
    :cond_14
    return-void

    .line 4208
    :cond_15
    iget-boolean v1, v0, Landroid/support/v4/app/bh;->i:Z

    if-nez v1, :cond_12

    .line 4209
    const-string v1, "(root)"

    iget-boolean v2, v0, Landroid/support/v4/app/bh;->j:Z

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/bh;->a(Ljava/lang/String;ZZ)Landroid/support/v4/app/ct;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    .line 4211
    iget-object v1, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    if-eqz v1, :cond_12

    iget-object v1, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    iget-boolean v1, v1, Landroid/support/v4/app/ct;->f:Z

    if-nez v1, :cond_12

    .line 4212
    iget-object v1, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    invoke-virtual {v1}, Landroid/support/v4/app/ct;->b()V

    goto :goto_12
.end method

.method private t()V
    .registers 3

    .prologue
    .line 355
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    .line 4237
    iget-object v1, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    if-eqz v1, :cond_b

    .line 4240
    iget-object v0, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    invoke-virtual {v0}, Landroid/support/v4/app/ct;->d()V

    .line 356
    :cond_b
    return-void
.end method

.method private u()V
    .registers 3

    .prologue
    .line 362
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    .line 4244
    iget-object v1, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    if-eqz v1, :cond_b

    .line 4247
    iget-object v0, v0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    invoke-virtual {v0}, Landroid/support/v4/app/ct;->g()V

    .line 363
    :cond_b
    return-void
.end method

.method private v()V
    .registers 11

    .prologue
    const/4 v2, 0x0

    .line 369
    iget-object v3, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    .line 4251
    iget-object v0, v3, Landroid/support/v4/app/bh;->g:Landroid/support/v4/n/v;

    if-eqz v0, :cond_a1

    .line 4252
    iget-object v0, v3, Landroid/support/v4/app/bh;->g:Landroid/support/v4/n/v;

    invoke-virtual {v0}, Landroid/support/v4/n/v;->size()I

    move-result v4

    .line 4253
    new-array v5, v4, [Landroid/support/v4/app/ct;

    .line 4254
    add-int/lit8 v0, v4, -0x1

    move v1, v0

    :goto_12
    if-ltz v1, :cond_22

    .line 4255
    iget-object v0, v3, Landroid/support/v4/app/bh;->g:Landroid/support/v4/n/v;

    invoke-virtual {v0, v1}, Landroid/support/v4/n/v;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/ct;

    aput-object v0, v5, v1

    .line 4254
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_12

    :cond_22
    move v1, v2

    .line 4257
    :goto_23
    if-ge v1, v4, :cond_a1

    .line 4258
    aget-object v6, v5, v1

    .line 4801
    iget-boolean v0, v6, Landroid/support/v4/app/ct;->g:Z

    if-eqz v0, :cond_9a

    .line 4802
    sget-boolean v0, Landroid/support/v4/app/ct;->b:Z

    if-eqz v0, :cond_43

    const-string v0, "LoaderManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "Finished Retaining in "

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 4804
    :cond_43
    iput-boolean v2, v6, Landroid/support/v4/app/ct;->g:Z

    .line 4805
    iget-object v0, v6, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v0}, Landroid/support/v4/n/w;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_4e
    if-ltz v3, :cond_9a

    .line 4806
    iget-object v0, v6, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v0, v3}, Landroid/support/v4/n/w;->d(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/cu;

    .line 5286
    iget-boolean v7, v0, Landroid/support/v4/app/cu;->i:Z

    if-eqz v7, :cond_83

    .line 5287
    sget-boolean v7, Landroid/support/v4/app/ct;->b:Z

    if-eqz v7, :cond_74

    const-string v7, "LoaderManager"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "  Finished Retaining: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 5288
    :cond_74
    iput-boolean v2, v0, Landroid/support/v4/app/cu;->i:Z

    .line 5289
    iget-boolean v7, v0, Landroid/support/v4/app/cu;->h:Z

    iget-boolean v8, v0, Landroid/support/v4/app/cu;->j:Z

    if-eq v7, v8, :cond_83

    .line 5290
    iget-boolean v7, v0, Landroid/support/v4/app/cu;->h:Z

    if-nez v7, :cond_83

    .line 5294
    invoke-virtual {v0}, Landroid/support/v4/app/cu;->b()V

    .line 5299
    :cond_83
    iget-boolean v7, v0, Landroid/support/v4/app/cu;->h:Z

    if-eqz v7, :cond_96

    iget-boolean v7, v0, Landroid/support/v4/app/cu;->e:Z

    if-eqz v7, :cond_96

    iget-boolean v7, v0, Landroid/support/v4/app/cu;->k:Z

    if-nez v7, :cond_96

    .line 5306
    iget-object v7, v0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    iget-object v8, v0, Landroid/support/v4/app/cu;->g:Ljava/lang/Object;

    invoke-virtual {v0, v7, v8}, Landroid/support/v4/app/cu;->b(Landroid/support/v4/c/aa;Ljava/lang/Object;)V

    .line 4805
    :cond_96
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_4e

    .line 4260
    :cond_9a
    invoke-virtual {v6}, Landroid/support/v4/app/ct;->f()V

    .line 4257
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_23

    .line 370
    :cond_a1
    return-void
.end method

.method private w()Landroid/support/v4/n/v;
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 377
    iget-object v3, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    .line 6283
    iget-object v0, v3, Landroid/support/v4/app/bh;->g:Landroid/support/v4/n/v;

    if-eqz v0, :cond_3a

    .line 6286
    iget-object v0, v3, Landroid/support/v4/app/bh;->g:Landroid/support/v4/n/v;

    invoke-virtual {v0}, Landroid/support/v4/n/v;->size()I

    move-result v4

    .line 6287
    new-array v5, v4, [Landroid/support/v4/app/ct;

    .line 6288
    add-int/lit8 v0, v4, -0x1

    move v2, v0

    :goto_12
    if-ltz v2, :cond_22

    .line 6289
    iget-object v0, v3, Landroid/support/v4/app/bh;->g:Landroid/support/v4/n/v;

    invoke-virtual {v0, v2}, Landroid/support/v4/n/v;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/ct;

    aput-object v0, v5, v2

    .line 6288
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_12

    :cond_22
    move v0, v1

    .line 6291
    :goto_23
    if-ge v1, v4, :cond_3b

    .line 6292
    aget-object v2, v5, v1

    .line 6293
    iget-boolean v6, v2, Landroid/support/v4/app/ct;->g:Z

    if-eqz v6, :cond_2f

    .line 6294
    const/4 v0, 0x1

    .line 6291
    :goto_2c
    add-int/lit8 v1, v1, 0x1

    goto :goto_23

    .line 6296
    :cond_2f
    invoke-virtual {v2}, Landroid/support/v4/app/ct;->g()V

    .line 6297
    iget-object v6, v3, Landroid/support/v4/app/bh;->g:Landroid/support/v4/n/v;

    iget-object v2, v2, Landroid/support/v4/app/ct;->e:Ljava/lang/String;

    invoke-virtual {v6, v2}, Landroid/support/v4/n/v;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2c

    :cond_3a
    move v0, v1

    .line 6302
    :cond_3b
    if-eqz v0, :cond_40

    .line 6303
    iget-object v0, v3, Landroid/support/v4/app/bh;->g:Landroid/support/v4/n/v;

    :goto_3f
    return-object v0

    .line 6305
    :cond_40
    const/4 v0, 0x0

    .line 377
    goto :goto_3f
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 72
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    iget-object v0, v0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    .line 73
    if-nez v0, :cond_a

    const/4 v0, 0x0

    :goto_9
    return v0

    :cond_a
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_9
.end method

.method public final a(Ljava/util/List;)Ljava/util/List;
    .registers 3

    .prologue
    .line 80
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    iget-object v0, v0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    if-nez v0, :cond_a

    .line 81
    const/4 p1, 0x0

    .line 87
    :goto_9
    return-object p1

    .line 86
    :cond_a
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    iget-object v0, v0, Landroid/support/v4/app/bl;->l:Ljava/util/ArrayList;

    invoke-interface {p1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_9
.end method

.method public final b()V
    .registers 3

    .prologue
    .line 118
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    .line 2991
    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/support/v4/app/bl;->z:Z

    .line 119
    return-void
.end method

.method public final c()Z
    .registers 2

    .prologue
    .line 330
    iget-object v0, p0, Landroid/support/v4/app/bg;->a:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->k()Z

    move-result v0

    return v0
.end method
