.class public abstract Landroid/support/v4/app/bh;
.super Landroid/support/v4/app/bf;
.source "SourceFile"


# instance fields
.field final b:Landroid/app/Activity;

.field final c:Landroid/content/Context;

.field final d:Landroid/os/Handler;

.field final e:I

.field final f:Landroid/support/v4/app/bl;

.field g:Landroid/support/v4/n/v;

.field h:Landroid/support/v4/app/ct;

.field i:Z

.field j:Z


# direct methods
.method private constructor <init>(Landroid/app/Activity;Landroid/content/Context;Landroid/os/Handler;I)V
    .registers 6

    .prologue
    .line 59
    invoke-direct {p0}, Landroid/support/v4/app/bf;-><init>()V

    .line 44
    new-instance v0, Landroid/support/v4/app/bl;

    invoke-direct {v0}, Landroid/support/v4/app/bl;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    .line 60
    iput-object p1, p0, Landroid/support/v4/app/bh;->b:Landroid/app/Activity;

    .line 61
    iput-object p2, p0, Landroid/support/v4/app/bh;->c:Landroid/content/Context;

    .line 62
    iput-object p3, p0, Landroid/support/v4/app/bh;->d:Landroid/os/Handler;

    .line 63
    iput p4, p0, Landroid/support/v4/app/bh;->e:I

    .line 64
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/os/Handler;I)V
    .registers 5

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2, p3}, Landroid/support/v4/app/bh;-><init>(Landroid/app/Activity;Landroid/content/Context;Landroid/os/Handler;I)V

    .line 52
    return-void
.end method

.method constructor <init>(Landroid/support/v4/app/bb;)V
    .registers 4

    .prologue
    .line 55
    iget-object v0, p1, Landroid/support/v4/app/bb;->d:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p1, v0, v1}, Landroid/support/v4/app/bh;-><init>(Landroid/app/Activity;Landroid/content/Context;Landroid/os/Handler;I)V

    .line 56
    return-void
.end method

.method private a(Landroid/support/v4/n/v;)V
    .registers 2

    .prologue
    .line 309
    iput-object p1, p0, Landroid/support/v4/app/bh;->g:Landroid/support/v4/n/v;

    .line 310
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 8

    .prologue
    .line 313
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mLoadersStarted="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 314
    iget-boolean v0, p0, Landroid/support/v4/app/bh;->j:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 315
    iget-object v0, p0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    if-eqz v0, :cond_43

    .line 316
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Loader Manager "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 317
    iget-object v0, p0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    invoke-static {v0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 318
    const-string v0, ":"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 319
    iget-object v0, p0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, Landroid/support/v4/app/ct;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 321
    :cond_43
    return-void
.end method

.method private a(Z)V
    .registers 3

    .prologue
    .line 220
    iget-object v0, p0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    if-nez v0, :cond_5

    .line 234
    :cond_4
    :goto_4
    return-void

    .line 224
    :cond_5
    iget-boolean v0, p0, Landroid/support/v4/app/bh;->j:Z

    if-eqz v0, :cond_4

    .line 227
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/bh;->j:Z

    .line 229
    if-eqz p1, :cond_14

    .line 230
    iget-object v0, p0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    invoke-virtual {v0}, Landroid/support/v4/app/ct;->d()V

    goto :goto_4

    .line 232
    :cond_14
    iget-object v0, p0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    invoke-virtual {v0}, Landroid/support/v4/app/ct;->c()V

    goto :goto_4
.end method

.method private j()Landroid/app/Activity;
    .registers 2

    .prologue
    .line 162
    iget-object v0, p0, Landroid/support/v4/app/bh;->b:Landroid/app/Activity;

    return-object v0
.end method

.method private k()Landroid/content/Context;
    .registers 2

    .prologue
    .line 166
    iget-object v0, p0, Landroid/support/v4/app/bh;->c:Landroid/content/Context;

    return-object v0
.end method

.method private l()Landroid/os/Handler;
    .registers 2

    .prologue
    .line 170
    iget-object v0, p0, Landroid/support/v4/app/bh;->d:Landroid/os/Handler;

    return-object v0
.end method

.method private m()Landroid/support/v4/app/bl;
    .registers 2

    .prologue
    .line 174
    iget-object v0, p0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    return-object v0
.end method

.method private n()Landroid/support/v4/app/ct;
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 178
    iget-object v0, p0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    if-eqz v0, :cond_8

    .line 179
    iget-object v0, p0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    .line 183
    :goto_7
    return-object v0

    .line 181
    :cond_8
    iput-boolean v2, p0, Landroid/support/v4/app/bh;->i:Z

    .line 182
    const-string v0, "(root)"

    iget-boolean v1, p0, Landroid/support/v4/app/bh;->j:Z

    invoke-virtual {p0, v0, v1, v2}, Landroid/support/v4/app/bh;->a(Ljava/lang/String;ZZ)Landroid/support/v4/app/ct;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    .line 183
    iget-object v0, p0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    goto :goto_7
.end method

.method private o()V
    .registers 5

    .prologue
    const/4 v3, 0x1

    .line 201
    iget-boolean v0, p0, Landroid/support/v4/app/bh;->j:Z

    if-eqz v0, :cond_6

    .line 216
    :goto_5
    return-void

    .line 204
    :cond_6
    iput-boolean v3, p0, Landroid/support/v4/app/bh;->j:Z

    .line 206
    iget-object v0, p0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    if-eqz v0, :cond_14

    .line 207
    iget-object v0, p0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    invoke-virtual {v0}, Landroid/support/v4/app/ct;->b()V

    .line 215
    :cond_11
    :goto_11
    iput-boolean v3, p0, Landroid/support/v4/app/bh;->i:Z

    goto :goto_5

    .line 208
    :cond_14
    iget-boolean v0, p0, Landroid/support/v4/app/bh;->i:Z

    if-nez v0, :cond_11

    .line 209
    const-string v0, "(root)"

    iget-boolean v1, p0, Landroid/support/v4/app/bh;->j:Z

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Landroid/support/v4/app/bh;->a(Ljava/lang/String;ZZ)Landroid/support/v4/app/ct;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    .line 211
    iget-object v0, p0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    if-eqz v0, :cond_11

    iget-object v0, p0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    iget-boolean v0, v0, Landroid/support/v4/app/ct;->f:Z

    if-nez v0, :cond_11

    .line 212
    iget-object v0, p0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    invoke-virtual {v0}, Landroid/support/v4/app/ct;->b()V

    goto :goto_11
.end method

.method private p()V
    .registers 2

    .prologue
    .line 237
    iget-object v0, p0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    if-nez v0, :cond_5

    .line 241
    :goto_4
    return-void

    .line 240
    :cond_5
    iget-object v0, p0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    invoke-virtual {v0}, Landroid/support/v4/app/ct;->d()V

    goto :goto_4
.end method

.method private q()V
    .registers 2

    .prologue
    .line 244
    iget-object v0, p0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    if-nez v0, :cond_5

    .line 248
    :goto_4
    return-void

    .line 247
    :cond_5
    iget-object v0, p0, Landroid/support/v4/app/bh;->h:Landroid/support/v4/app/ct;

    invoke-virtual {v0}, Landroid/support/v4/app/ct;->g()V

    goto :goto_4
.end method

.method private r()V
    .registers 11

    .prologue
    const/4 v3, 0x0

    .line 251
    iget-object v0, p0, Landroid/support/v4/app/bh;->g:Landroid/support/v4/n/v;

    if-eqz v0, :cond_9f

    .line 252
    iget-object v0, p0, Landroid/support/v4/app/bh;->g:Landroid/support/v4/n/v;

    invoke-virtual {v0}, Landroid/support/v4/n/v;->size()I

    move-result v4

    .line 253
    new-array v5, v4, [Landroid/support/v4/app/ct;

    .line 254
    add-int/lit8 v0, v4, -0x1

    move v1, v0

    :goto_10
    if-ltz v1, :cond_20

    .line 255
    iget-object v0, p0, Landroid/support/v4/app/bh;->g:Landroid/support/v4/n/v;

    invoke-virtual {v0, v1}, Landroid/support/v4/n/v;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/ct;

    aput-object v0, v5, v1

    .line 254
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_10

    :cond_20
    move v2, v3

    .line 257
    :goto_21
    if-ge v2, v4, :cond_9f

    .line 258
    aget-object v6, v5, v2

    .line 1801
    iget-boolean v0, v6, Landroid/support/v4/app/ct;->g:Z

    if-eqz v0, :cond_98

    .line 1802
    sget-boolean v0, Landroid/support/v4/app/ct;->b:Z

    if-eqz v0, :cond_41

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v7, "Finished Retaining in "

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1804
    :cond_41
    iput-boolean v3, v6, Landroid/support/v4/app/ct;->g:Z

    .line 1805
    iget-object v0, v6, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v0}, Landroid/support/v4/n/w;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_4c
    if-ltz v1, :cond_98

    .line 1806
    iget-object v0, v6, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v0, v1}, Landroid/support/v4/n/w;->d(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/cu;

    .line 2286
    iget-boolean v7, v0, Landroid/support/v4/app/cu;->i:Z

    if-eqz v7, :cond_81

    .line 2287
    sget-boolean v7, Landroid/support/v4/app/ct;->b:Z

    if-eqz v7, :cond_72

    const-string v7, "LoaderManager"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "  Finished Retaining: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2288
    :cond_72
    iput-boolean v3, v0, Landroid/support/v4/app/cu;->i:Z

    .line 2289
    iget-boolean v7, v0, Landroid/support/v4/app/cu;->h:Z

    iget-boolean v8, v0, Landroid/support/v4/app/cu;->j:Z

    if-eq v7, v8, :cond_81

    .line 2290
    iget-boolean v7, v0, Landroid/support/v4/app/cu;->h:Z

    if-nez v7, :cond_81

    .line 2294
    invoke-virtual {v0}, Landroid/support/v4/app/cu;->b()V

    .line 2299
    :cond_81
    iget-boolean v7, v0, Landroid/support/v4/app/cu;->h:Z

    if-eqz v7, :cond_94

    iget-boolean v7, v0, Landroid/support/v4/app/cu;->e:Z

    if-eqz v7, :cond_94

    iget-boolean v7, v0, Landroid/support/v4/app/cu;->k:Z

    if-nez v7, :cond_94

    .line 2306
    iget-object v7, v0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    iget-object v8, v0, Landroid/support/v4/app/cu;->g:Ljava/lang/Object;

    invoke-virtual {v0, v7, v8}, Landroid/support/v4/app/cu;->b(Landroid/support/v4/c/aa;Ljava/lang/Object;)V

    .line 1805
    :cond_94
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_4c

    .line 260
    :cond_98
    invoke-virtual {v6}, Landroid/support/v4/app/ct;->f()V

    .line 257
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_21

    .line 263
    :cond_9f
    return-void
.end method


# virtual methods
.method final a(Ljava/lang/String;ZZ)Landroid/support/v4/app/ct;
    .registers 6

    .prologue
    .line 266
    iget-object v0, p0, Landroid/support/v4/app/bh;->g:Landroid/support/v4/n/v;

    if-nez v0, :cond_b

    .line 267
    new-instance v0, Landroid/support/v4/n/v;

    invoke-direct {v0}, Landroid/support/v4/n/v;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/bh;->g:Landroid/support/v4/n/v;

    .line 269
    :cond_b
    iget-object v0, p0, Landroid/support/v4/app/bh;->g:Landroid/support/v4/n/v;

    invoke-virtual {v0, p1}, Landroid/support/v4/n/v;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/ct;

    .line 270
    if-nez v0, :cond_22

    .line 271
    if-eqz p3, :cond_21

    .line 272
    new-instance v0, Landroid/support/v4/app/ct;

    invoke-direct {v0, p1, p0, p2}, Landroid/support/v4/app/ct;-><init>(Ljava/lang/String;Landroid/support/v4/app/bh;Z)V

    .line 273
    iget-object v1, p0, Landroid/support/v4/app/bh;->g:Landroid/support/v4/n/v;

    invoke-virtual {v1, p1, v0}, Landroid/support/v4/n/v;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    :cond_21
    :goto_21
    return-object v0

    .line 2533
    :cond_22
    iput-object p0, v0, Landroid/support/v4/app/ct;->j:Landroid/support/v4/app/bh;

    goto :goto_21
.end method

.method public a(I)Landroid/view/View;
    .registers 3
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 153
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V
    .registers 6

    .prologue
    .line 113
    const/4 v0, -0x1

    if-eq p3, v0, :cond_b

    .line 114
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Starting activity with a requestCode requires a FragmentActivity host"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 117
    :cond_b
    iget-object v0, p0, Landroid/support/v4/app/bh;->c:Landroid/content/Context;

    invoke-virtual {v0, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 118
    return-void
.end method

.method public a(Landroid/support/v4/app/Fragment;[Ljava/lang/String;I)V
    .registers 4
    .param p1    # Landroid/support/v4/app/Fragment;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param
    .param p2    # [Ljava/lang/String;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    .line 126
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 4

    .prologue
    .line 76
    return-void
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 158
    const/4 v0, 0x1

    return v0
.end method

.method public a(Ljava/lang/String;)Z
    .registers 3
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    .line 133
    const/4 v0, 0x0

    return v0
.end method

.method final b(Ljava/lang/String;)V
    .registers 4

    .prologue
    .line 188
    iget-object v0, p0, Landroid/support/v4/app/bh;->g:Landroid/support/v4/n/v;

    if-eqz v0, :cond_1a

    .line 189
    iget-object v0, p0, Landroid/support/v4/app/bh;->g:Landroid/support/v4/n/v;

    invoke-virtual {v0, p1}, Landroid/support/v4/n/v;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/ct;

    .line 190
    if-eqz v0, :cond_1a

    iget-boolean v1, v0, Landroid/support/v4/app/ct;->g:Z

    if-nez v1, :cond_1a

    .line 191
    invoke-virtual {v0}, Landroid/support/v4/app/ct;->g()V

    .line 192
    iget-object v0, p0, Landroid/support/v4/app/bh;->g:Landroid/support/v4/n/v;

    invoke-virtual {v0, p1}, Landroid/support/v4/n/v;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    :cond_1a
    return-void
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 82
    const/4 v0, 0x1

    return v0
.end method

.method public c()Landroid/view/LayoutInflater;
    .registers 3

    .prologue
    .line 90
    iget-object v0, p0, Landroid/support/v4/app/bh;->c:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    return-object v0
.end method

.method public d()V
    .registers 1

    .prologue
    .line 106
    return-void
.end method

.method public e()Z
    .registers 2

    .prologue
    .line 140
    const/4 v0, 0x1

    return v0
.end method

.method public f()I
    .registers 2

    .prologue
    .line 147
    iget v0, p0, Landroid/support/v4/app/bh;->e:I

    return v0
.end method

.method g()V
    .registers 1

    .prologue
    .line 198
    return-void
.end method

.method public abstract h()Ljava/lang/Object;
    .annotation build Landroid/support/a/z;
    .end annotation
.end method

.method final i()Landroid/support/v4/n/v;
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 282
    .line 283
    iget-object v0, p0, Landroid/support/v4/app/bh;->g:Landroid/support/v4/n/v;

    if-eqz v0, :cond_38

    .line 286
    iget-object v0, p0, Landroid/support/v4/app/bh;->g:Landroid/support/v4/n/v;

    invoke-virtual {v0}, Landroid/support/v4/n/v;->size()I

    move-result v3

    .line 287
    new-array v4, v3, [Landroid/support/v4/app/ct;

    .line 288
    add-int/lit8 v0, v3, -0x1

    move v2, v0

    :goto_10
    if-ltz v2, :cond_20

    .line 289
    iget-object v0, p0, Landroid/support/v4/app/bh;->g:Landroid/support/v4/n/v;

    invoke-virtual {v0, v2}, Landroid/support/v4/n/v;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/ct;

    aput-object v0, v4, v2

    .line 288
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_10

    :cond_20
    move v0, v1

    .line 291
    :goto_21
    if-ge v1, v3, :cond_39

    .line 292
    aget-object v2, v4, v1

    .line 293
    iget-boolean v5, v2, Landroid/support/v4/app/ct;->g:Z

    if-eqz v5, :cond_2d

    .line 294
    const/4 v0, 0x1

    .line 291
    :goto_2a
    add-int/lit8 v1, v1, 0x1

    goto :goto_21

    .line 296
    :cond_2d
    invoke-virtual {v2}, Landroid/support/v4/app/ct;->g()V

    .line 297
    iget-object v5, p0, Landroid/support/v4/app/bh;->g:Landroid/support/v4/n/v;

    iget-object v2, v2, Landroid/support/v4/app/ct;->e:Ljava/lang/String;

    invoke-virtual {v5, v2}, Landroid/support/v4/n/v;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2a

    :cond_38
    move v0, v1

    .line 302
    :cond_39
    if-eqz v0, :cond_3e

    .line 303
    iget-object v0, p0, Landroid/support/v4/app/bh;->g:Landroid/support/v4/n/v;

    .line 305
    :goto_3d
    return-object v0

    :cond_3e
    const/4 v0, 0x0

    goto :goto_3d
.end method
