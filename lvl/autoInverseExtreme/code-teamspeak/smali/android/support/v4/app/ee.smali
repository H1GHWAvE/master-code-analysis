.class public final Landroid/support/v4/app/ee;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/app/ds;


# static fields
.field private static final A:I = 0x10

.field private static final B:I = 0x1

.field private static final C:I = 0x800005

.field private static final D:I = 0x50

.field public static final a:I = -0x1

.field public static final b:I = 0x0

.field public static final c:I = 0x1

.field public static final d:I = 0x2

.field public static final e:I = 0x3

.field public static final f:I = 0x4

.field public static final g:I = 0x5

.field public static final h:I = 0x0

.field public static final i:I = -0x1

.field private static final j:Ljava/lang/String; = "android.wearable.EXTENSIONS"

.field private static final k:Ljava/lang/String; = "actions"

.field private static final l:Ljava/lang/String; = "flags"

.field private static final m:Ljava/lang/String; = "displayIntent"

.field private static final n:Ljava/lang/String; = "pages"

.field private static final o:Ljava/lang/String; = "background"

.field private static final p:Ljava/lang/String; = "contentIcon"

.field private static final q:Ljava/lang/String; = "contentIconGravity"

.field private static final r:Ljava/lang/String; = "contentActionIndex"

.field private static final s:Ljava/lang/String; = "customSizePreset"

.field private static final t:Ljava/lang/String; = "customContentHeight"

.field private static final u:Ljava/lang/String; = "gravity"

.field private static final v:Ljava/lang/String; = "hintScreenTimeout"

.field private static final w:I = 0x1

.field private static final x:I = 0x2

.field private static final y:I = 0x4

.field private static final z:I = 0x8


# instance fields
.field private E:Ljava/util/ArrayList;

.field private F:I

.field private G:Landroid/app/PendingIntent;

.field private H:Ljava/util/ArrayList;

.field private I:Landroid/graphics/Bitmap;

.field private J:I

.field private K:I

.field private L:I

.field private M:I

.field private N:I

.field private O:I

.field private P:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 2355
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2338
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/ee;->E:Ljava/util/ArrayList;

    .line 2339
    const/4 v0, 0x1

    iput v0, p0, Landroid/support/v4/app/ee;->F:I

    .line 2341
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/ee;->H:Ljava/util/ArrayList;

    .line 2344
    const v0, 0x800005

    iput v0, p0, Landroid/support/v4/app/ee;->K:I

    .line 2345
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v4/app/ee;->L:I

    .line 2346
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v4/app/ee;->M:I

    .line 2348
    const/16 v0, 0x50

    iput v0, p0, Landroid/support/v4/app/ee;->O:I

    .line 2356
    return-void
.end method

.method private constructor <init>(Landroid/app/Notification;)V
    .registers 10

    .prologue
    const v7, 0x800005

    const/16 v6, 0x50

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 2358
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2338
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/ee;->E:Ljava/util/ArrayList;

    .line 2339
    iput v5, p0, Landroid/support/v4/app/ee;->F:I

    .line 2341
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/ee;->H:Ljava/util/ArrayList;

    .line 2344
    iput v7, p0, Landroid/support/v4/app/ee;->K:I

    .line 2345
    iput v3, p0, Landroid/support/v4/app/ee;->L:I

    .line 2346
    iput v4, p0, Landroid/support/v4/app/ee;->M:I

    .line 2348
    iput v6, p0, Landroid/support/v4/app/ee;->O:I

    .line 2359
    invoke-static {p1}, Landroid/support/v4/app/dd;->a(Landroid/app/Notification;)Landroid/os/Bundle;

    move-result-object v0

    .line 2360
    if-eqz v0, :cond_a9

    const-string v1, "android.wearable.EXTENSIONS"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    move-object v1, v0

    .line 2362
    :goto_30
    if-eqz v1, :cond_a8

    .line 2363
    invoke-static {}, Landroid/support/v4/app/dd;->a()Landroid/support/v4/app/du;

    move-result-object v0

    const-string v2, "actions"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {v0, v2}, Landroid/support/v4/app/du;->a(Ljava/util/ArrayList;)[Landroid/support/v4/app/df;

    move-result-object v0

    .line 2365
    if-eqz v0, :cond_47

    .line 2366
    iget-object v2, p0, Landroid/support/v4/app/ee;->E:Ljava/util/ArrayList;

    invoke-static {v2, v0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 2369
    :cond_47
    const-string v0, "flags"

    invoke-virtual {v1, v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/ee;->F:I

    .line 2370
    const-string v0, "displayIntent"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    iput-object v0, p0, Landroid/support/v4/app/ee;->G:Landroid/app/PendingIntent;

    .line 2372
    const-string v0, "pages"

    invoke-static {v1, v0}, Landroid/support/v4/app/dd;->a(Landroid/os/Bundle;Ljava/lang/String;)[Landroid/app/Notification;

    move-result-object v0

    .line 2374
    if-eqz v0, :cond_66

    .line 2375
    iget-object v2, p0, Landroid/support/v4/app/ee;->H:Ljava/util/ArrayList;

    invoke-static {v2, v0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 2378
    :cond_66
    const-string v0, "background"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Landroid/support/v4/app/ee;->I:Landroid/graphics/Bitmap;

    .line 2379
    const-string v0, "contentIcon"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/ee;->J:I

    .line 2380
    const-string v0, "contentIconGravity"

    invoke-virtual {v1, v0, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/ee;->K:I

    .line 2382
    const-string v0, "contentActionIndex"

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/ee;->L:I

    .line 2384
    const-string v0, "customSizePreset"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/ee;->M:I

    .line 2386
    const-string v0, "customContentHeight"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/ee;->N:I

    .line 2387
    const-string v0, "gravity"

    invoke-virtual {v1, v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/ee;->O:I

    .line 2388
    const-string v0, "hintScreenTimeout"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/ee;->P:I

    .line 2390
    :cond_a8
    return-void

    .line 2360
    :cond_a9
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_30
.end method

.method private a()Landroid/support/v4/app/ee;
    .registers 4

    .prologue
    .line 2448
    new-instance v0, Landroid/support/v4/app/ee;

    invoke-direct {v0}, Landroid/support/v4/app/ee;-><init>()V

    .line 2449
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Landroid/support/v4/app/ee;->E:Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Landroid/support/v4/app/ee;->E:Ljava/util/ArrayList;

    .line 2450
    iget v1, p0, Landroid/support/v4/app/ee;->F:I

    iput v1, v0, Landroid/support/v4/app/ee;->F:I

    .line 2451
    iget-object v1, p0, Landroid/support/v4/app/ee;->G:Landroid/app/PendingIntent;

    iput-object v1, v0, Landroid/support/v4/app/ee;->G:Landroid/app/PendingIntent;

    .line 2452
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Landroid/support/v4/app/ee;->H:Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Landroid/support/v4/app/ee;->H:Ljava/util/ArrayList;

    .line 2453
    iget-object v1, p0, Landroid/support/v4/app/ee;->I:Landroid/graphics/Bitmap;

    iput-object v1, v0, Landroid/support/v4/app/ee;->I:Landroid/graphics/Bitmap;

    .line 2454
    iget v1, p0, Landroid/support/v4/app/ee;->J:I

    iput v1, v0, Landroid/support/v4/app/ee;->J:I

    .line 2455
    iget v1, p0, Landroid/support/v4/app/ee;->K:I

    iput v1, v0, Landroid/support/v4/app/ee;->K:I

    .line 2456
    iget v1, p0, Landroid/support/v4/app/ee;->L:I

    iput v1, v0, Landroid/support/v4/app/ee;->L:I

    .line 2457
    iget v1, p0, Landroid/support/v4/app/ee;->M:I

    iput v1, v0, Landroid/support/v4/app/ee;->M:I

    .line 2458
    iget v1, p0, Landroid/support/v4/app/ee;->N:I

    iput v1, v0, Landroid/support/v4/app/ee;->N:I

    .line 2459
    iget v1, p0, Landroid/support/v4/app/ee;->O:I

    iput v1, v0, Landroid/support/v4/app/ee;->O:I

    .line 2460
    iget v1, p0, Landroid/support/v4/app/ee;->P:I

    iput v1, v0, Landroid/support/v4/app/ee;->P:I

    .line 2461
    return-object v0
.end method

.method private a(I)Landroid/support/v4/app/ee;
    .registers 2

    .prologue
    .line 2639
    iput p1, p0, Landroid/support/v4/app/ee;->J:I

    .line 2640
    return-object p0
.end method

.method private a(Landroid/app/Notification;)Landroid/support/v4/app/ee;
    .registers 3

    .prologue
    .line 2569
    iget-object v0, p0, Landroid/support/v4/app/ee;->H:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2570
    return-object p0
.end method

.method private a(Landroid/app/PendingIntent;)Landroid/support/v4/app/ee;
    .registers 2

    .prologue
    .line 2546
    iput-object p1, p0, Landroid/support/v4/app/ee;->G:Landroid/app/PendingIntent;

    .line 2547
    return-object p0
.end method

.method private a(Landroid/graphics/Bitmap;)Landroid/support/v4/app/ee;
    .registers 2

    .prologue
    .line 2619
    iput-object p1, p0, Landroid/support/v4/app/ee;->I:Landroid/graphics/Bitmap;

    .line 2620
    return-object p0
.end method

.method private a(Landroid/support/v4/app/df;)Landroid/support/v4/app/ee;
    .registers 3

    .prologue
    .line 2477
    iget-object v0, p0, Landroid/support/v4/app/ee;->E:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2478
    return-object p0
.end method

.method private a(Ljava/util/List;)Landroid/support/v4/app/ee;
    .registers 3

    .prologue
    .line 2494
    iget-object v0, p0, Landroid/support/v4/app/ee;->E:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 2495
    return-object p0
.end method

.method private a(Z)Landroid/support/v4/app/ee;
    .registers 3

    .prologue
    .line 2783
    const/16 v0, 0x8

    invoke-direct {p0, v0, p1}, Landroid/support/v4/app/ee;->a(IZ)V

    .line 2784
    return-object p0
.end method

.method private a(IZ)V
    .registers 5

    .prologue
    .line 2903
    if-eqz p2, :cond_8

    .line 2904
    iget v0, p0, Landroid/support/v4/app/ee;->F:I

    or-int/2addr v0, p1

    iput v0, p0, Landroid/support/v4/app/ee;->F:I

    .line 2908
    :goto_7
    return-void

    .line 2906
    :cond_8
    iget v0, p0, Landroid/support/v4/app/ee;->F:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Landroid/support/v4/app/ee;->F:I

    goto :goto_7
.end method

.method private b()Landroid/support/v4/app/ee;
    .registers 2

    .prologue
    .line 2504
    iget-object v0, p0, Landroid/support/v4/app/ee;->E:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2505
    return-object p0
.end method

.method private b(I)Landroid/support/v4/app/ee;
    .registers 2

    .prologue
    .line 2657
    iput p1, p0, Landroid/support/v4/app/ee;->K:I

    .line 2658
    return-object p0
.end method

.method private b(Ljava/util/List;)Landroid/support/v4/app/ee;
    .registers 3

    .prologue
    .line 2584
    iget-object v0, p0, Landroid/support/v4/app/ee;->H:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 2585
    return-object p0
.end method

.method private b(Z)Landroid/support/v4/app/ee;
    .registers 3

    .prologue
    .line 2804
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Landroid/support/v4/app/ee;->a(IZ)V

    .line 2805
    return-object p0
.end method

.method private c(I)Landroid/support/v4/app/ee;
    .registers 2

    .prologue
    .line 2686
    iput p1, p0, Landroid/support/v4/app/ee;->L:I

    .line 2687
    return-object p0
.end method

.method private c(Z)Landroid/support/v4/app/ee;
    .registers 3

    .prologue
    .line 2824
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Landroid/support/v4/app/ee;->a(IZ)V

    .line 2825
    return-object p0
.end method

.method private c()Ljava/util/List;
    .registers 2

    .prologue
    .line 2512
    iget-object v0, p0, Landroid/support/v4/app/ee;->E:Ljava/util/ArrayList;

    return-object v0
.end method

.method private d()Landroid/app/PendingIntent;
    .registers 2

    .prologue
    .line 2555
    iget-object v0, p0, Landroid/support/v4/app/ee;->G:Landroid/app/PendingIntent;

    return-object v0
.end method

.method private d(I)Landroid/support/v4/app/ee;
    .registers 2

    .prologue
    .line 2715
    iput p1, p0, Landroid/support/v4/app/ee;->O:I

    .line 2716
    return-object p0
.end method

.method private d(Z)Landroid/support/v4/app/ee;
    .registers 3

    .prologue
    .line 2843
    const/4 v0, 0x4

    invoke-direct {p0, v0, p1}, Landroid/support/v4/app/ee;->a(IZ)V

    .line 2844
    return-object p0
.end method

.method private e()Landroid/support/v4/app/ee;
    .registers 2

    .prologue
    .line 2594
    iget-object v0, p0, Landroid/support/v4/app/ee;->H:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2595
    return-object p0
.end method

.method private e(I)Landroid/support/v4/app/ee;
    .registers 2

    .prologue
    .line 2739
    iput p1, p0, Landroid/support/v4/app/ee;->M:I

    .line 2740
    return-object p0
.end method

.method private e(Z)Landroid/support/v4/app/ee;
    .registers 3

    .prologue
    .line 2865
    const/16 v0, 0x10

    invoke-direct {p0, v0, p1}, Landroid/support/v4/app/ee;->a(IZ)V

    .line 2866
    return-object p0
.end method

.method private f(I)Landroid/support/v4/app/ee;
    .registers 2

    .prologue
    .line 2763
    iput p1, p0, Landroid/support/v4/app/ee;->N:I

    .line 2764
    return-object p0
.end method

.method private f()Ljava/util/List;
    .registers 2

    .prologue
    .line 2606
    iget-object v0, p0, Landroid/support/v4/app/ee;->H:Ljava/util/ArrayList;

    return-object v0
.end method

.method private g()Landroid/graphics/Bitmap;
    .registers 2

    .prologue
    .line 2632
    iget-object v0, p0, Landroid/support/v4/app/ee;->I:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private g(I)Landroid/support/v4/app/ee;
    .registers 2

    .prologue
    .line 2888
    iput p1, p0, Landroid/support/v4/app/ee;->P:I

    .line 2889
    return-object p0
.end method

.method private h()I
    .registers 2

    .prologue
    .line 2647
    iget v0, p0, Landroid/support/v4/app/ee;->J:I

    return v0
.end method

.method private i()I
    .registers 2

    .prologue
    .line 2668
    iget v0, p0, Landroid/support/v4/app/ee;->K:I

    return v0
.end method

.method private j()I
    .registers 2

    .prologue
    .line 2705
    iget v0, p0, Landroid/support/v4/app/ee;->L:I

    return v0
.end method

.method private k()I
    .registers 2

    .prologue
    .line 2726
    iget v0, p0, Landroid/support/v4/app/ee;->O:I

    return v0
.end method

.method private l()I
    .registers 2

    .prologue
    .line 2752
    iget v0, p0, Landroid/support/v4/app/ee;->M:I

    return v0
.end method

.method private m()I
    .registers 2

    .prologue
    .line 2774
    iget v0, p0, Landroid/support/v4/app/ee;->N:I

    return v0
.end method

.method private n()Z
    .registers 2

    .prologue
    .line 2793
    iget v0, p0, Landroid/support/v4/app/ee;->F:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private o()Z
    .registers 2

    .prologue
    .line 2815
    iget v0, p0, Landroid/support/v4/app/ee;->F:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private p()Z
    .registers 2

    .prologue
    .line 2834
    iget v0, p0, Landroid/support/v4/app/ee;->F:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private q()Z
    .registers 2

    .prologue
    .line 2853
    iget v0, p0, Landroid/support/v4/app/ee;->F:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private r()Z
    .registers 2

    .prologue
    .line 2877
    iget v0, p0, Landroid/support/v4/app/ee;->F:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private s()I
    .registers 2

    .prologue
    .line 2899
    iget v0, p0, Landroid/support/v4/app/ee;->P:I

    return v0
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/dm;)Landroid/support/v4/app/dm;
    .registers 7

    .prologue
    .line 2399
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2401
    iget-object v0, p0, Landroid/support/v4/app/ee;->E:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2a

    .line 2402
    const-string v2, "actions"

    invoke-static {}, Landroid/support/v4/app/dd;->a()Landroid/support/v4/app/du;

    move-result-object v3

    iget-object v0, p0, Landroid/support/v4/app/ee;->E:Ljava/util/ArrayList;

    iget-object v4, p0, Landroid/support/v4/app/ee;->E:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Landroid/support/v4/app/df;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/support/v4/app/df;

    invoke-interface {v3, v0}, Landroid/support/v4/app/du;->a([Landroid/support/v4/app/df;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2406
    :cond_2a
    iget v0, p0, Landroid/support/v4/app/ee;->F:I

    const/4 v2, 0x1

    if-eq v0, v2, :cond_36

    .line 2407
    const-string v0, "flags"

    iget v2, p0, Landroid/support/v4/app/ee;->F:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2409
    :cond_36
    iget-object v0, p0, Landroid/support/v4/app/ee;->G:Landroid/app/PendingIntent;

    if-eqz v0, :cond_41

    .line 2410
    const-string v0, "displayIntent"

    iget-object v2, p0, Landroid/support/v4/app/ee;->G:Landroid/app/PendingIntent;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2412
    :cond_41
    iget-object v0, p0, Landroid/support/v4/app/ee;->H:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5e

    .line 2413
    const-string v2, "pages"

    iget-object v0, p0, Landroid/support/v4/app/ee;->H:Ljava/util/ArrayList;

    iget-object v3, p0, Landroid/support/v4/app/ee;->H:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Landroid/app/Notification;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 2416
    :cond_5e
    iget-object v0, p0, Landroid/support/v4/app/ee;->I:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_69

    .line 2417
    const-string v0, "background"

    iget-object v2, p0, Landroid/support/v4/app/ee;->I:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2419
    :cond_69
    iget v0, p0, Landroid/support/v4/app/ee;->J:I

    if-eqz v0, :cond_74

    .line 2420
    const-string v0, "contentIcon"

    iget v2, p0, Landroid/support/v4/app/ee;->J:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2422
    :cond_74
    iget v0, p0, Landroid/support/v4/app/ee;->K:I

    const v2, 0x800005

    if-eq v0, v2, :cond_82

    .line 2423
    const-string v0, "contentIconGravity"

    iget v2, p0, Landroid/support/v4/app/ee;->K:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2425
    :cond_82
    iget v0, p0, Landroid/support/v4/app/ee;->L:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_8e

    .line 2426
    const-string v0, "contentActionIndex"

    iget v2, p0, Landroid/support/v4/app/ee;->L:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2429
    :cond_8e
    iget v0, p0, Landroid/support/v4/app/ee;->M:I

    if-eqz v0, :cond_99

    .line 2430
    const-string v0, "customSizePreset"

    iget v2, p0, Landroid/support/v4/app/ee;->M:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2432
    :cond_99
    iget v0, p0, Landroid/support/v4/app/ee;->N:I

    if-eqz v0, :cond_a4

    .line 2433
    const-string v0, "customContentHeight"

    iget v2, p0, Landroid/support/v4/app/ee;->N:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2435
    :cond_a4
    iget v0, p0, Landroid/support/v4/app/ee;->O:I

    const/16 v2, 0x50

    if-eq v0, v2, :cond_b1

    .line 2436
    const-string v0, "gravity"

    iget v2, p0, Landroid/support/v4/app/ee;->O:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2438
    :cond_b1
    iget v0, p0, Landroid/support/v4/app/ee;->P:I

    if-eqz v0, :cond_bc

    .line 2439
    const-string v0, "hintScreenTimeout"

    iget v2, p0, Landroid/support/v4/app/ee;->P:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2442
    :cond_bc
    invoke-virtual {p1}, Landroid/support/v4/app/dm;->b()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "android.wearable.EXTENSIONS"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2443
    return-object p1
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .registers 4

    .prologue
    .line 2240
    .line 3448
    new-instance v0, Landroid/support/v4/app/ee;

    invoke-direct {v0}, Landroid/support/v4/app/ee;-><init>()V

    .line 3449
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Landroid/support/v4/app/ee;->E:Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Landroid/support/v4/app/ee;->E:Ljava/util/ArrayList;

    .line 3450
    iget v1, p0, Landroid/support/v4/app/ee;->F:I

    iput v1, v0, Landroid/support/v4/app/ee;->F:I

    .line 3451
    iget-object v1, p0, Landroid/support/v4/app/ee;->G:Landroid/app/PendingIntent;

    iput-object v1, v0, Landroid/support/v4/app/ee;->G:Landroid/app/PendingIntent;

    .line 3452
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Landroid/support/v4/app/ee;->H:Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Landroid/support/v4/app/ee;->H:Ljava/util/ArrayList;

    .line 3453
    iget-object v1, p0, Landroid/support/v4/app/ee;->I:Landroid/graphics/Bitmap;

    iput-object v1, v0, Landroid/support/v4/app/ee;->I:Landroid/graphics/Bitmap;

    .line 3454
    iget v1, p0, Landroid/support/v4/app/ee;->J:I

    iput v1, v0, Landroid/support/v4/app/ee;->J:I

    .line 3455
    iget v1, p0, Landroid/support/v4/app/ee;->K:I

    iput v1, v0, Landroid/support/v4/app/ee;->K:I

    .line 3456
    iget v1, p0, Landroid/support/v4/app/ee;->L:I

    iput v1, v0, Landroid/support/v4/app/ee;->L:I

    .line 3457
    iget v1, p0, Landroid/support/v4/app/ee;->M:I

    iput v1, v0, Landroid/support/v4/app/ee;->M:I

    .line 3458
    iget v1, p0, Landroid/support/v4/app/ee;->N:I

    iput v1, v0, Landroid/support/v4/app/ee;->N:I

    .line 3459
    iget v1, p0, Landroid/support/v4/app/ee;->O:I

    iput v1, v0, Landroid/support/v4/app/ee;->O:I

    .line 3460
    iget v1, p0, Landroid/support/v4/app/ee;->P:I

    iput v1, v0, Landroid/support/v4/app/ee;->P:I

    .line 2240
    return-object v0
.end method
