.class public final Landroid/support/v4/app/m;
.super Landroid/support/v4/c/h;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/support/v4/c/h;-><init>()V

    .line 377
    return-void
.end method

.method static a(Landroid/support/v4/app/gj;)Landroid/support/v4/app/r;
    .registers 2

    .prologue
    .line 370
    const/4 v0, 0x0

    .line 371
    if-eqz p0, :cond_8

    .line 372
    new-instance v0, Landroid/support/v4/app/p;

    invoke-direct {v0, p0}, Landroid/support/v4/app/p;-><init>(Landroid/support/v4/app/gj;)V

    .line 374
    :cond_8
    return-object v0
.end method

.method private static a(Landroid/app/Activity;Landroid/content/Intent;ILandroid/os/Bundle;)V
    .registers 6
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 161
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_a

    .line 2030
    invoke-virtual {p0, p1, p2, p3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 166
    :goto_9
    return-void

    .line 164
    :cond_a
    invoke-virtual {p0, p1, p2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_9
.end method

.method private static a(Landroid/app/Activity;Landroid/content/Intent;Landroid/os/Bundle;)V
    .registers 5
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 132
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_a

    .line 2026
    invoke-virtual {p0, p1, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 137
    :goto_9
    return-void

    .line 135
    :cond_a
    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_9
.end method

.method private static a(Landroid/app/Activity;Landroid/support/v4/app/gj;)V
    .registers 4

    .prologue
    .line 231
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_11

    .line 232
    invoke-static {p1}, Landroid/support/v4/app/m;->a(Landroid/support/v4/app/gj;)Landroid/support/v4/app/r;

    move-result-object v0

    .line 4040
    invoke-static {v0}, Landroid/support/v4/app/q;->a(Landroid/support/v4/app/r;)Landroid/app/SharedElementCallback;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setEnterSharedElementCallback(Landroid/app/SharedElementCallback;)V

    .line 234
    :cond_11
    return-void
.end method

.method public static a(Landroid/app/Activity;[Ljava/lang/String;I)V
    .registers 5
    .param p0    # Landroid/app/Activity;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param
    .param p1    # [Ljava/lang/String;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    .line 315
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_14

    .line 5028
    instance-of v0, p0, Landroid/support/v4/app/v;

    if-eqz v0, :cond_10

    move-object v0, p0

    .line 5029
    check-cast v0, Landroid/support/v4/app/v;

    invoke-interface {v0, p2}, Landroid/support/v4/app/v;->a(I)V

    .line 5032
    :cond_10
    invoke-virtual {p0, p1, p2}, Landroid/app/Activity;->requestPermissions([Ljava/lang/String;I)V

    .line 338
    :cond_13
    :goto_13
    return-void

    .line 317
    :cond_14
    instance-of v0, p0, Landroid/support/v4/app/o;

    if-eqz v0, :cond_13

    .line 318
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 319
    new-instance v1, Landroid/support/v4/app/n;

    invoke-direct {v1, p1, p0, p2}, Landroid/support/v4/app/n;-><init>([Ljava/lang/String;Landroid/app/Activity;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_13
.end method

.method private static a(Landroid/app/Activity;)Z
    .registers 3

    .prologue
    .line 107
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_b

    .line 1029
    invoke-virtual {p0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 109
    const/4 v0, 0x1

    .line 111
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method private static a(Landroid/app/Activity;Ljava/lang/String;)Z
    .registers 4
    .param p0    # Landroid/app/Activity;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param

    .prologue
    .line 362
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_b

    .line 5037
    invoke-virtual {p0, p1}, Landroid/app/Activity;->shouldShowRequestPermissionRationale(Ljava/lang/String;)Z

    move-result v0

    .line 365
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method private static b(Landroid/app/Activity;)V
    .registers 3

    .prologue
    .line 176
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_a

    .line 2034
    invoke-virtual {p0}, Landroid/app/Activity;->finishAffinity()V

    .line 181
    :goto_9
    return-void

    .line 179
    :cond_a
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_9
.end method

.method private static b(Landroid/app/Activity;Landroid/support/v4/app/gj;)V
    .registers 4

    .prologue
    .line 247
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_11

    .line 248
    invoke-static {p1}, Landroid/support/v4/app/m;->a(Landroid/support/v4/app/gj;)Landroid/support/v4/app/r;

    move-result-object v0

    .line 4045
    invoke-static {v0}, Landroid/support/v4/app/q;->a(Landroid/support/v4/app/r;)Landroid/app/SharedElementCallback;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setExitSharedElementCallback(Landroid/app/SharedElementCallback;)V

    .line 250
    :cond_11
    return-void
.end method

.method private static c(Landroid/app/Activity;)V
    .registers 3

    .prologue
    .line 193
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_a

    .line 3035
    invoke-virtual {p0}, Landroid/app/Activity;->finishAfterTransition()V

    .line 198
    :goto_9
    return-void

    .line 196
    :cond_a
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_9
.end method

.method private static d(Landroid/app/Activity;)Landroid/net/Uri;
    .registers 3

    .prologue
    .line 206
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x16

    if-lt v0, v1, :cond_b

    .line 4024
    invoke-virtual {p0}, Landroid/app/Activity;->getReferrer()Landroid/net/Uri;

    move-result-object v0

    .line 218
    :cond_a
    :goto_a
    return-object v0

    .line 209
    :cond_b
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 210
    const-string v0, "android.intent.extra.REFERRER"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 211
    if-nez v0, :cond_a

    .line 214
    const-string v0, "android.intent.extra.REFERRER_NAME"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 215
    if-eqz v0, :cond_26

    .line 216
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_a

    .line 218
    :cond_26
    const/4 v0, 0x0

    goto :goto_a
.end method

.method private static e(Landroid/app/Activity;)V
    .registers 3

    .prologue
    .line 253
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_9

    .line 4049
    invoke-virtual {p0}, Landroid/app/Activity;->postponeEnterTransition()V

    .line 256
    :cond_9
    return-void
.end method

.method private static f(Landroid/app/Activity;)V
    .registers 3

    .prologue
    .line 259
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_9

    .line 4053
    invoke-virtual {p0}, Landroid/app/Activity;->startPostponedEnterTransition()V

    .line 262
    :cond_9
    return-void
.end method
