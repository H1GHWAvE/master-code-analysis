.class final Landroid/support/v4/app/et;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String; = "NotificationCompat"

.field static final b:Ljava/lang/String; = "android.support.localOnly"

.field static final c:Ljava/lang/String; = "android.support.actionExtras"

.field static final d:Ljava/lang/String; = "android.support.remoteInputs"

.field static final e:Ljava/lang/String; = "android.support.groupKey"

.field static final f:Ljava/lang/String; = "android.support.isGroupSummary"

.field static final g:Ljava/lang/String; = "android.support.sortKey"

.field static final h:Ljava/lang/String; = "android.support.useSideChannel"

.field private static final i:Ljava/lang/String; = "icon"

.field private static final j:Ljava/lang/String; = "title"

.field private static final k:Ljava/lang/String; = "actionIntent"

.field private static final l:Ljava/lang/String; = "extras"

.field private static final m:Ljava/lang/String; = "remoteInputs"

.field private static final n:Ljava/lang/Object;

.field private static o:Ljava/lang/reflect/Field;

.field private static p:Z

.field private static final q:Ljava/lang/Object;

.field private static r:Ljava/lang/Class;

.field private static s:Ljava/lang/reflect/Field;

.field private static t:Ljava/lang/reflect/Field;

.field private static u:Ljava/lang/reflect/Field;

.field private static v:Ljava/lang/reflect/Field;

.field private static w:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 52
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Landroid/support/v4/app/et;->n:Ljava/lang/Object;

    .line 56
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Landroid/support/v4/app/et;->q:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>()V
    .registers 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    return-void
.end method

.method public static a(Landroid/app/Notification$Builder;Landroid/support/v4/app/ek;)Landroid/os/Bundle;
    .registers 5

    .prologue
    .line 258
    invoke-virtual {p1}, Landroid/support/v4/app/ek;->a()I

    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/support/v4/app/ek;->b()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1}, Landroid/support/v4/app/ek;->c()Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 259
    new-instance v0, Landroid/os/Bundle;

    invoke-virtual {p1}, Landroid/support/v4/app/ek;->d()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 260
    invoke-virtual {p1}, Landroid/support/v4/app/ek;->e()[Landroid/support/v4/app/fw;

    move-result-object v1

    if-eqz v1, :cond_2b

    .line 261
    const-string v1, "android.support.remoteInputs"

    invoke-virtual {p1}, Landroid/support/v4/app/ek;->e()[Landroid/support/v4/app/fw;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v4/app/fy;->a([Landroid/support/v4/app/fw;)[Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 264
    :cond_2b
    return-object v0
.end method

.method public static a(Landroid/app/Notification;)Landroid/os/Bundle;
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 212
    sget-object v2, Landroid/support/v4/app/et;->n:Ljava/lang/Object;

    monitor-enter v2

    .line 213
    :try_start_4
    sget-boolean v0, Landroid/support/v4/app/et;->p:Z

    if-eqz v0, :cond_b

    .line 214
    monitor-exit v2
    :try_end_9
    .catchall {:try_start_4 .. :try_end_9} :catchall_4c

    move-object v0, v1

    .line 239
    :goto_a
    return-object v0

    .line 217
    :cond_b
    :try_start_b
    sget-object v0, Landroid/support/v4/app/et;->o:Ljava/lang/reflect/Field;

    if-nez v0, :cond_36

    .line 218
    const-class v0, Landroid/app/Notification;

    const-string v3, "extras"

    invoke-virtual {v0, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 219
    const-class v3, Landroid/os/Bundle;

    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-nez v3, :cond_30

    .line 220
    const-string v0, "NotificationCompat"

    const-string v3, "Notification.extras field is not of type Bundle"

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    const/4 v0, 0x1

    sput-boolean v0, Landroid/support/v4/app/et;->p:Z
    :try_end_2d
    .catch Ljava/lang/IllegalAccessException; {:try_start_b .. :try_end_2d} :catch_4f
    .catch Ljava/lang/NoSuchFieldException; {:try_start_b .. :try_end_2d} :catch_5d
    .catchall {:try_start_b .. :try_end_2d} :catchall_4c

    .line 222
    :try_start_2d
    monitor-exit v2
    :try_end_2e
    .catchall {:try_start_2d .. :try_end_2e} :catchall_4c

    move-object v0, v1

    goto :goto_a

    .line 224
    :cond_30
    const/4 v3, 0x1

    :try_start_31
    invoke-virtual {v0, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 225
    sput-object v0, Landroid/support/v4/app/et;->o:Ljava/lang/reflect/Field;

    .line 227
    :cond_36
    sget-object v0, Landroid/support/v4/app/et;->o:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 228
    if-nez v0, :cond_4a

    .line 229
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 230
    sget-object v3, Landroid/support/v4/app/et;->o:Ljava/lang/reflect/Field;

    invoke-virtual {v3, p0, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_4a
    .catch Ljava/lang/IllegalAccessException; {:try_start_31 .. :try_end_4a} :catch_4f
    .catch Ljava/lang/NoSuchFieldException; {:try_start_31 .. :try_end_4a} :catch_5d
    .catchall {:try_start_31 .. :try_end_4a} :catchall_4c

    .line 232
    :cond_4a
    :try_start_4a
    monitor-exit v2

    goto :goto_a

    .line 240
    :catchall_4c
    move-exception v0

    monitor-exit v2
    :try_end_4e
    .catchall {:try_start_4a .. :try_end_4e} :catchall_4c

    throw v0

    .line 233
    :catch_4f
    move-exception v0

    .line 234
    :try_start_50
    const-string v3, "NotificationCompat"

    const-string v4, "Unable to access notification extras"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 238
    :goto_57
    const/4 v0, 0x1

    sput-boolean v0, Landroid/support/v4/app/et;->p:Z

    .line 239
    monitor-exit v2

    move-object v0, v1

    goto :goto_a

    .line 235
    :catch_5d
    move-exception v0

    .line 236
    const-string v3, "NotificationCompat"

    const-string v4, "Unable to access notification extras"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_65
    .catchall {:try_start_50 .. :try_end_65} :catchall_4c

    goto :goto_57
.end method

.method private static a(Landroid/support/v4/app/ek;)Landroid/os/Bundle;
    .registers 4

    .prologue
    .line 381
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 382
    const-string v1, "icon"

    invoke-virtual {p0}, Landroid/support/v4/app/ek;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 383
    const-string v1, "title"

    invoke-virtual {p0}, Landroid/support/v4/app/ek;->b()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 384
    const-string v1, "actionIntent"

    invoke-virtual {p0}, Landroid/support/v4/app/ek;->c()Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 385
    const-string v1, "extras"

    invoke-virtual {p0}, Landroid/support/v4/app/ek;->d()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 386
    const-string v1, "remoteInputs"

    invoke-virtual {p0}, Landroid/support/v4/app/ek;->e()[Landroid/support/v4/app/fw;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v4/app/fy;->a([Landroid/support/v4/app/fw;)[Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 388
    return-object v0
.end method

.method public static a(Landroid/app/Notification;ILandroid/support/v4/app/el;Landroid/support/v4/app/fx;)Landroid/support/v4/app/ek;
    .registers 12

    .prologue
    const/4 v6, 0x0

    .line 277
    sget-object v7, Landroid/support/v4/app/et;->q:Ljava/lang/Object;

    monitor-enter v7

    .line 279
    :try_start_4
    invoke-static {p0}, Landroid/support/v4/app/et;->g(Landroid/app/Notification;)[Ljava/lang/Object;

    move-result-object v0

    aget-object v1, v0, p1

    .line 281
    invoke-static {p0}, Landroid/support/v4/app/et;->a(Landroid/app/Notification;)Landroid/os/Bundle;

    move-result-object v0

    .line 282
    if-eqz v0, :cond_4e

    .line 283
    const-string v2, "android.support.actionExtras"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v0

    .line 285
    if-eqz v0, :cond_4e

    .line 286
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    move-object v5, v0

    .line 289
    :goto_1f
    sget-object v0, Landroid/support/v4/app/et;->t:Ljava/lang/reflect/Field;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v2

    sget-object v0, Landroid/support/v4/app/et;->u:Ljava/lang/reflect/Field;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    sget-object v0, Landroid/support/v4/app/et;->v:Ljava/lang/reflect/Field;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/PendingIntent;

    move-object v0, p2

    move-object v1, p3

    invoke-static/range {v0 .. v5}, Landroid/support/v4/app/et;->a(Landroid/support/v4/app/el;Landroid/support/v4/app/fx;ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;)Landroid/support/v4/app/ek;
    :try_end_3a
    .catch Ljava/lang/IllegalAccessException; {:try_start_4 .. :try_end_3a} :catch_3d
    .catchall {:try_start_4 .. :try_end_3a} :catchall_4b

    move-result-object v0

    :try_start_3b
    monitor-exit v7

    .line 299
    :goto_3c
    return-object v0

    .line 294
    :catch_3d
    move-exception v0

    .line 295
    const-string v1, "NotificationCompat"

    const-string v2, "Unable to access notification actions"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 296
    const/4 v0, 0x1

    sput-boolean v0, Landroid/support/v4/app/et;->w:Z

    .line 298
    monitor-exit v7

    move-object v0, v6

    .line 299
    goto :goto_3c

    .line 298
    :catchall_4b
    move-exception v0

    monitor-exit v7
    :try_end_4d
    .catchall {:try_start_3b .. :try_end_4d} :catchall_4b

    throw v0

    :cond_4e
    move-object v5, v6

    goto :goto_1f
.end method

.method private static a(Landroid/os/Bundle;Landroid/support/v4/app/el;Landroid/support/v4/app/fx;)Landroid/support/v4/app/ek;
    .registers 9

    .prologue
    .line 358
    const-string v0, "icon"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const-string v0, "title"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    const-string v0, "actionIntent"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/app/PendingIntent;

    const-string v0, "extras"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    const-string v0, "remoteInputs"

    invoke-static {p0, v0}, Landroid/support/v4/app/aw;->a(Landroid/os/Bundle;Ljava/lang/String;)[Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0, p2}, Landroid/support/v4/app/fy;->a([Landroid/os/Bundle;Landroid/support/v4/app/fx;)[Landroid/support/v4/app/fw;

    move-result-object v5

    move-object v0, p1

    invoke-interface/range {v0 .. v5}, Landroid/support/v4/app/el;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[Landroid/support/v4/app/fw;)Landroid/support/v4/app/ek;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/support/v4/app/el;Landroid/support/v4/app/fx;ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;)Landroid/support/v4/app/ek;
    .registers 12

    .prologue
    .line 247
    const/4 v5, 0x0

    .line 248
    if-eqz p5, :cond_d

    .line 249
    const-string v0, "android.support.remoteInputs"

    invoke-static {p5, v0}, Landroid/support/v4/app/aw;->a(Landroid/os/Bundle;Ljava/lang/String;)[Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/support/v4/app/fy;->a([Landroid/os/Bundle;Landroid/support/v4/app/fx;)[Landroid/support/v4/app/fw;

    move-result-object v5

    :cond_d
    move-object v0, p0

    move v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    .line 253
    invoke-interface/range {v0 .. v5}, Landroid/support/v4/app/el;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[Landroid/support/v4/app/fw;)Landroid/support/v4/app/ek;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/List;)Landroid/util/SparseArray;
    .registers 5

    .prologue
    .line 194
    const/4 v1, 0x0

    .line 195
    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    move v2, v0

    :goto_7
    if-ge v2, v3, :cond_1f

    .line 196
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 197
    if-eqz v0, :cond_1b

    .line 198
    if-nez v1, :cond_18

    .line 199
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    .line 201
    :cond_18
    invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 195
    :cond_1b
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_7

    .line 204
    :cond_1f
    return-object v1
.end method

.method public static a([Landroid/support/v4/app/ek;)Ljava/util/ArrayList;
    .registers 8

    .prologue
    .line 370
    if-nez p0, :cond_4

    .line 371
    const/4 v0, 0x0

    .line 377
    :cond_3
    return-object v0

    .line 373
    :cond_4
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 374
    array-length v2, p0

    const/4 v1, 0x0

    :goto_c
    if-ge v1, v2, :cond_3

    aget-object v3, p0, v1

    .line 1381
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 1382
    const-string v5, "icon"

    invoke-virtual {v3}, Landroid/support/v4/app/ek;->a()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1383
    const-string v5, "title"

    invoke-virtual {v3}, Landroid/support/v4/app/ek;->b()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 1384
    const-string v5, "actionIntent"

    invoke-virtual {v3}, Landroid/support/v4/app/ek;->c()Landroid/app/PendingIntent;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1385
    const-string v5, "extras"

    invoke-virtual {v3}, Landroid/support/v4/app/ek;->d()Landroid/os/Bundle;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1386
    const-string v5, "remoteInputs"

    invoke-virtual {v3}, Landroid/support/v4/app/ek;->e()[Landroid/support/v4/app/fw;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v4/app/fy;->a([Landroid/support/v4/app/fw;)[Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 375
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 374
    add-int/lit8 v1, v1, 0x1

    goto :goto_c
.end method

.method public static a(Landroid/support/v4/app/dc;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Z)V
    .registers 9

    .prologue
    .line 168
    new-instance v0, Landroid/app/Notification$BigPictureStyle;

    invoke-interface {p0}, Landroid/support/v4/app/dc;->a()Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/Notification$BigPictureStyle;-><init>(Landroid/app/Notification$Builder;)V

    invoke-virtual {v0, p1}, Landroid/app/Notification$BigPictureStyle;->setBigContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$BigPictureStyle;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/app/Notification$BigPictureStyle;->bigPicture(Landroid/graphics/Bitmap;)Landroid/app/Notification$BigPictureStyle;

    move-result-object v0

    .line 171
    if-eqz p6, :cond_16

    .line 172
    invoke-virtual {v0, p5}, Landroid/app/Notification$BigPictureStyle;->bigLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$BigPictureStyle;

    .line 174
    :cond_16
    if-eqz p2, :cond_1b

    .line 175
    invoke-virtual {v0, p3}, Landroid/app/Notification$BigPictureStyle;->setSummaryText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigPictureStyle;

    .line 177
    :cond_1b
    return-void
.end method

.method public static a(Landroid/support/v4/app/dc;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .registers 7

    .prologue
    .line 156
    new-instance v0, Landroid/app/Notification$BigTextStyle;

    invoke-interface {p0}, Landroid/support/v4/app/dc;->a()Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/Notification$BigTextStyle;-><init>(Landroid/app/Notification$Builder;)V

    invoke-virtual {v0, p1}, Landroid/app/Notification$BigTextStyle;->setBigContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v0

    .line 159
    if-eqz p2, :cond_16

    .line 160
    invoke-virtual {v0, p3}, Landroid/app/Notification$BigTextStyle;->setSummaryText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    .line 162
    :cond_16
    return-void
.end method

.method public static a(Landroid/support/v4/app/dc;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/util/ArrayList;)V
    .registers 8

    .prologue
    .line 182
    new-instance v0, Landroid/app/Notification$InboxStyle;

    invoke-interface {p0}, Landroid/support/v4/app/dc;->a()Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/Notification$InboxStyle;-><init>(Landroid/app/Notification$Builder;)V

    invoke-virtual {v0, p1}, Landroid/app/Notification$InboxStyle;->setBigContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;

    move-result-object v1

    .line 184
    if-eqz p2, :cond_12

    .line 185
    invoke-virtual {v1, p3}, Landroid/app/Notification$InboxStyle;->setSummaryText(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;

    .line 187
    :cond_12
    invoke-virtual {p4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_16
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 188
    invoke-virtual {v1, v0}, Landroid/app/Notification$InboxStyle;->addLine(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;

    goto :goto_16

    .line 190
    :cond_26
    return-void
.end method

.method private static a()Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 318
    sget-boolean v2, Landroid/support/v4/app/et;->w:Z

    if-eqz v2, :cond_7

    .line 337
    :cond_6
    :goto_6
    return v0

    .line 322
    :cond_7
    :try_start_7
    sget-object v2, Landroid/support/v4/app/et;->s:Ljava/lang/reflect/Field;

    if-nez v2, :cond_3d

    .line 323
    const-string v2, "android.app.Notification$Action"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 324
    sput-object v2, Landroid/support/v4/app/et;->r:Ljava/lang/Class;

    const-string v3, "icon"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    sput-object v2, Landroid/support/v4/app/et;->t:Ljava/lang/reflect/Field;

    .line 325
    sget-object v2, Landroid/support/v4/app/et;->r:Ljava/lang/Class;

    const-string v3, "title"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    sput-object v2, Landroid/support/v4/app/et;->u:Ljava/lang/reflect/Field;

    .line 326
    sget-object v2, Landroid/support/v4/app/et;->r:Ljava/lang/Class;

    const-string v3, "actionIntent"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    sput-object v2, Landroid/support/v4/app/et;->v:Ljava/lang/reflect/Field;

    .line 327
    const-class v2, Landroid/app/Notification;

    const-string v3, "actions"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 328
    sput-object v2, Landroid/support/v4/app/et;->s:Ljava/lang/reflect/Field;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_3d
    .catch Ljava/lang/ClassNotFoundException; {:try_start_7 .. :try_end_3d} :catch_43
    .catch Ljava/lang/NoSuchFieldException; {:try_start_7 .. :try_end_3d} :catch_4e

    .line 337
    :cond_3d
    :goto_3d
    sget-boolean v2, Landroid/support/v4/app/et;->w:Z

    if-nez v2, :cond_6

    move v0, v1

    goto :goto_6

    .line 330
    :catch_43
    move-exception v2

    .line 331
    const-string v3, "NotificationCompat"

    const-string v4, "Unable to access notification actions"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 332
    sput-boolean v1, Landroid/support/v4/app/et;->w:Z

    goto :goto_3d

    .line 333
    :catch_4e
    move-exception v2

    .line 334
    const-string v3, "NotificationCompat"

    const-string v4, "Unable to access notification actions"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 335
    sput-boolean v1, Landroid/support/v4/app/et;->w:Z

    goto :goto_3d
.end method

.method public static a(Ljava/util/ArrayList;Landroid/support/v4/app/el;Landroid/support/v4/app/fx;)[Landroid/support/v4/app/ek;
    .registers 11

    .prologue
    .line 344
    if-nez p0, :cond_4

    .line 345
    const/4 v0, 0x0

    .line 352
    :goto_3
    return-object v0

    .line 347
    :cond_4
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-interface {p1, v0}, Landroid/support/v4/app/el;->a(I)[Landroid/support/v4/app/ek;

    move-result-object v7

    .line 348
    const/4 v0, 0x0

    move v6, v0

    :goto_e
    array-length v0, v7

    if-ge v6, v0, :cond_46

    .line 349
    invoke-virtual {p0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 1358
    const-string v1, "icon"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const-string v2, "title"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    const-string v3, "actionIntent"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/app/PendingIntent;

    const-string v4, "extras"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "remoteInputs"

    invoke-static {v0, v5}, Landroid/support/v4/app/aw;->a(Landroid/os/Bundle;Ljava/lang/String;)[Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0, p2}, Landroid/support/v4/app/fy;->a([Landroid/os/Bundle;Landroid/support/v4/app/fx;)[Landroid/support/v4/app/fw;

    move-result-object v5

    move-object v0, p1

    invoke-interface/range {v0 .. v5}, Landroid/support/v4/app/el;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[Landroid/support/v4/app/fw;)Landroid/support/v4/app/ek;

    move-result-object v0

    .line 349
    aput-object v0, v7, v6

    .line 348
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_e

    :cond_46
    move-object v0, v7

    .line 352
    goto :goto_3
.end method

.method public static b(Landroid/app/Notification;)I
    .registers 3

    .prologue
    .line 268
    sget-object v1, Landroid/support/v4/app/et;->q:Ljava/lang/Object;

    monitor-enter v1

    .line 269
    :try_start_3
    invoke-static {p0}, Landroid/support/v4/app/et;->g(Landroid/app/Notification;)[Ljava/lang/Object;

    move-result-object v0

    .line 270
    if-eqz v0, :cond_c

    array-length v0, v0

    :goto_a
    monitor-exit v1

    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_a

    .line 271
    :catchall_e
    move-exception v0

    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_e

    throw v0
.end method

.method public static c(Landroid/app/Notification;)Z
    .registers 3

    .prologue
    .line 392
    invoke-static {p0}, Landroid/support/v4/app/et;->a(Landroid/app/Notification;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "android.support.localOnly"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static d(Landroid/app/Notification;)Ljava/lang/String;
    .registers 3

    .prologue
    .line 396
    invoke-static {p0}, Landroid/support/v4/app/et;->a(Landroid/app/Notification;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "android.support.groupKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static e(Landroid/app/Notification;)Z
    .registers 3

    .prologue
    .line 400
    invoke-static {p0}, Landroid/support/v4/app/et;->a(Landroid/app/Notification;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "android.support.isGroupSummary"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static f(Landroid/app/Notification;)Ljava/lang/String;
    .registers 3

    .prologue
    .line 404
    invoke-static {p0}, Landroid/support/v4/app/et;->a(Landroid/app/Notification;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "android.support.sortKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static g(Landroid/app/Notification;)[Ljava/lang/Object;
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 303
    sget-object v2, Landroid/support/v4/app/et;->q:Ljava/lang/Object;

    monitor-enter v2

    .line 304
    :try_start_4
    invoke-static {}, Landroid/support/v4/app/et;->a()Z

    move-result v0

    if-nez v0, :cond_d

    .line 305
    monitor-exit v2
    :try_end_b
    .catchall {:try_start_4 .. :try_end_b} :catchall_19

    move-object v0, v1

    .line 312
    :goto_c
    return-object v0

    .line 308
    :cond_d
    :try_start_d
    sget-object v0, Landroid/support/v4/app/et;->s:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;
    :try_end_17
    .catch Ljava/lang/IllegalAccessException; {:try_start_d .. :try_end_17} :catch_1c
    .catchall {:try_start_d .. :try_end_17} :catchall_19

    :try_start_17
    monitor-exit v2

    goto :goto_c

    .line 314
    :catchall_19
    move-exception v0

    monitor-exit v2
    :try_end_1b
    .catchall {:try_start_17 .. :try_end_1b} :catchall_19

    throw v0

    .line 309
    :catch_1c
    move-exception v0

    .line 310
    :try_start_1d
    const-string v3, "NotificationCompat"

    const-string v4, "Unable to access notification actions"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 311
    const/4 v0, 0x1

    sput-boolean v0, Landroid/support/v4/app/et;->w:Z

    .line 312
    monitor-exit v2
    :try_end_28
    .catchall {:try_start_1d .. :try_end_28} :catchall_19

    move-object v0, v1

    goto :goto_c
.end method
