.class final Landroid/support/v4/app/ct;
.super Landroid/support/v4/app/cr;
.source "SourceFile"


# static fields
.field static final a:Ljava/lang/String; = "LoaderManager"

.field static b:Z


# instance fields
.field final c:Landroid/support/v4/n/w;

.field final d:Landroid/support/v4/n/w;

.field final e:Ljava/lang/String;

.field f:Z

.field g:Z

.field h:Z

.field i:Z

.field j:Landroid/support/v4/app/bh;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 192
    const/4 v0, 0x0

    sput-boolean v0, Landroid/support/v4/app/ct;->b:Z

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Landroid/support/v4/app/bh;Z)V
    .registers 5

    .prologue
    .line 526
    invoke-direct {p0}, Landroid/support/v4/app/cr;-><init>()V

    .line 197
    new-instance v0, Landroid/support/v4/n/w;

    invoke-direct {v0}, Landroid/support/v4/n/w;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    .line 203
    new-instance v0, Landroid/support/v4/n/w;

    invoke-direct {v0}, Landroid/support/v4/n/w;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/ct;->d:Landroid/support/v4/n/w;

    .line 527
    iput-object p1, p0, Landroid/support/v4/app/ct;->e:Ljava/lang/String;

    .line 528
    iput-object p2, p0, Landroid/support/v4/app/ct;->j:Landroid/support/v4/app/bh;

    .line 529
    iput-boolean p3, p0, Landroid/support/v4/app/ct;->f:Z

    .line 530
    return-void
.end method

.method static synthetic a(Landroid/support/v4/app/ct;)Landroid/support/v4/app/bh;
    .registers 2

    .prologue
    .line 190
    iget-object v0, p0, Landroid/support/v4/app/ct;->j:Landroid/support/v4/app/bh;

    return-object v0
.end method

.method private a(Landroid/support/v4/app/bh;)V
    .registers 2

    .prologue
    .line 533
    iput-object p1, p0, Landroid/support/v4/app/ct;->j:Landroid/support/v4/app/bh;

    .line 534
    return-void
.end method

.method private c(ILandroid/os/Bundle;Landroid/support/v4/app/cs;)Landroid/support/v4/app/cu;
    .registers 6

    .prologue
    .line 538
    new-instance v0, Landroid/support/v4/app/cu;

    invoke-direct {v0, p0, p1, p2, p3}, Landroid/support/v4/app/cu;-><init>(Landroid/support/v4/app/ct;ILandroid/os/Bundle;Landroid/support/v4/app/cs;)V

    .line 539
    invoke-interface {p3}, Landroid/support/v4/app/cs;->a()Landroid/support/v4/c/aa;

    move-result-object v1

    .line 540
    iput-object v1, v0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    .line 541
    return-object v0
.end method

.method private d(ILandroid/os/Bundle;Landroid/support/v4/app/cs;)Landroid/support/v4/app/cu;
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 547
    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Landroid/support/v4/app/ct;->i:Z

    .line 548
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/app/ct;->c(ILandroid/os/Bundle;Landroid/support/v4/app/cs;)Landroid/support/v4/app/cu;

    move-result-object v0

    .line 549
    invoke-virtual {p0, v0}, Landroid/support/v4/app/ct;->a(Landroid/support/v4/app/cu;)V
    :try_end_b
    .catchall {:try_start_2 .. :try_end_b} :catchall_e

    .line 552
    iput-boolean v1, p0, Landroid/support/v4/app/ct;->i:Z

    return-object v0

    :catchall_e
    move-exception v0

    iput-boolean v1, p0, Landroid/support/v4/app/ct;->i:Z

    throw v0
.end method

.method private h()V
    .registers 7

    .prologue
    const/4 v5, 0x0

    .line 801
    iget-boolean v0, p0, Landroid/support/v4/app/ct;->g:Z

    if-eqz v0, :cond_74

    .line 802
    sget-boolean v0, Landroid/support/v4/app/ct;->b:Z

    if-eqz v0, :cond_1d

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Finished Retaining in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 804
    :cond_1d
    iput-boolean v5, p0, Landroid/support/v4/app/ct;->g:Z

    .line 805
    iget-object v0, p0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v0}, Landroid/support/v4/n/w;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_28
    if-ltz v1, :cond_74

    .line 806
    iget-object v0, p0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v0, v1}, Landroid/support/v4/n/w;->d(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/cu;

    .line 3286
    iget-boolean v2, v0, Landroid/support/v4/app/cu;->i:Z

    if-eqz v2, :cond_5d

    .line 3287
    sget-boolean v2, Landroid/support/v4/app/ct;->b:Z

    if-eqz v2, :cond_4e

    const-string v2, "LoaderManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "  Finished Retaining: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3288
    :cond_4e
    iput-boolean v5, v0, Landroid/support/v4/app/cu;->i:Z

    .line 3289
    iget-boolean v2, v0, Landroid/support/v4/app/cu;->h:Z

    iget-boolean v3, v0, Landroid/support/v4/app/cu;->j:Z

    if-eq v2, v3, :cond_5d

    .line 3290
    iget-boolean v2, v0, Landroid/support/v4/app/cu;->h:Z

    if-nez v2, :cond_5d

    .line 3294
    invoke-virtual {v0}, Landroid/support/v4/app/cu;->b()V

    .line 3299
    :cond_5d
    iget-boolean v2, v0, Landroid/support/v4/app/cu;->h:Z

    if-eqz v2, :cond_70

    iget-boolean v2, v0, Landroid/support/v4/app/cu;->e:Z

    if-eqz v2, :cond_70

    iget-boolean v2, v0, Landroid/support/v4/app/cu;->k:Z

    if-nez v2, :cond_70

    .line 3306
    iget-object v2, v0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    iget-object v3, v0, Landroid/support/v4/app/cu;->g:Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/app/cu;->b(Landroid/support/v4/c/aa;Ljava/lang/Object;)V

    .line 805
    :cond_70
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_28

    .line 809
    :cond_74
    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;Landroid/support/v4/app/cs;)Landroid/support/v4/c/aa;
    .registers 8

    .prologue
    .line 592
    iget-boolean v0, p0, Landroid/support/v4/app/ct;->i:Z

    if-eqz v0, :cond_c

    .line 593
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Called while creating a loader"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 596
    :cond_c
    iget-object v0, p0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v0, p1}, Landroid/support/v4/n/w;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/cu;

    .line 598
    sget-boolean v1, Landroid/support/v4/app/ct;->b:Z

    if-eqz v1, :cond_36

    const-string v1, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "initLoader in "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": args="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    :cond_36
    if-nez v0, :cond_66

    .line 602
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/app/ct;->d(ILandroid/os/Bundle;Landroid/support/v4/app/cs;)Landroid/support/v4/app/cu;

    move-result-object v0

    .line 603
    sget-boolean v1, Landroid/support/v4/app/ct;->b:Z

    if-eqz v1, :cond_54

    const-string v1, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "  Created new loader "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 609
    :cond_54
    :goto_54
    iget-boolean v1, v0, Landroid/support/v4/app/cu;->e:Z

    if-eqz v1, :cond_63

    iget-boolean v1, p0, Landroid/support/v4/app/ct;->f:Z

    if-eqz v1, :cond_63

    .line 611
    iget-object v1, v0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    iget-object v2, v0, Landroid/support/v4/app/cu;->g:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/cu;->b(Landroid/support/v4/c/aa;Ljava/lang/Object;)V

    .line 614
    :cond_63
    iget-object v0, v0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    return-object v0

    .line 605
    :cond_66
    sget-boolean v1, Landroid/support/v4/app/ct;->b:Z

    if-eqz v1, :cond_7e

    const-string v1, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "  Re-using existing loader "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 606
    :cond_7e
    iput-object p3, v0, Landroid/support/v4/app/cu;->c:Landroid/support/v4/app/cs;

    goto :goto_54
.end method

.method public final a(I)V
    .registers 5

    .prologue
    .line 709
    iget-boolean v0, p0, Landroid/support/v4/app/ct;->i:Z

    if-eqz v0, :cond_c

    .line 710
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Called while creating a loader"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 713
    :cond_c
    sget-boolean v0, Landroid/support/v4/app/ct;->b:Z

    if-eqz v0, :cond_2e

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "destroyLoader in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 714
    :cond_2e
    iget-object v0, p0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v0, p1}, Landroid/support/v4/n/w;->e(I)I

    move-result v1

    .line 715
    if-ltz v1, :cond_46

    .line 716
    iget-object v0, p0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v0, v1}, Landroid/support/v4/n/w;->d(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/cu;

    .line 717
    iget-object v2, p0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v2, v1}, Landroid/support/v4/n/w;->b(I)V

    .line 718
    invoke-virtual {v0}, Landroid/support/v4/app/cu;->c()V

    .line 720
    :cond_46
    iget-object v0, p0, Landroid/support/v4/app/ct;->d:Landroid/support/v4/n/w;

    invoke-virtual {v0, p1}, Landroid/support/v4/n/w;->e(I)I

    move-result v1

    .line 721
    if-ltz v1, :cond_5e

    .line 722
    iget-object v0, p0, Landroid/support/v4/app/ct;->d:Landroid/support/v4/n/w;

    invoke-virtual {v0, v1}, Landroid/support/v4/n/w;->d(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/cu;

    .line 723
    iget-object v2, p0, Landroid/support/v4/app/ct;->d:Landroid/support/v4/n/w;

    invoke-virtual {v2, v1}, Landroid/support/v4/n/w;->b(I)V

    .line 724
    invoke-virtual {v0}, Landroid/support/v4/app/cu;->c()V

    .line 726
    :cond_5e
    iget-object v0, p0, Landroid/support/v4/app/ct;->j:Landroid/support/v4/app/bh;

    if-eqz v0, :cond_6f

    invoke-virtual {p0}, Landroid/support/v4/app/ct;->a()Z

    move-result v0

    if-nez v0, :cond_6f

    .line 727
    iget-object v0, p0, Landroid/support/v4/app/ct;->j:Landroid/support/v4/app/bh;

    iget-object v0, v0, Landroid/support/v4/app/bh;->f:Landroid/support/v4/app/bl;

    invoke-virtual {v0}, Landroid/support/v4/app/bl;->j()V

    .line 729
    :cond_6f
    return-void
.end method

.method final a(Landroid/support/v4/app/cu;)V
    .registers 4

    .prologue
    .line 557
    iget-object v0, p0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    iget v1, p1, Landroid/support/v4/app/cu;->a:I

    invoke-virtual {v0, v1, p1}, Landroid/support/v4/n/w;->a(ILjava/lang/Object;)V

    .line 558
    iget-boolean v0, p0, Landroid/support/v4/app/ct;->f:Z

    if-eqz v0, :cond_e

    .line 562
    invoke-virtual {p1}, Landroid/support/v4/app/cu;->a()V

    .line 564
    :cond_e
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 10

    .prologue
    const/4 v2, 0x0

    .line 852
    iget-object v0, p0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v0}, Landroid/support/v4/n/w;->a()I

    move-result v0

    if-lez v0, :cond_59

    .line 853
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Active Loaders:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 854
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "    "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move v1, v2

    .line 855
    :goto_25
    iget-object v0, p0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v0}, Landroid/support/v4/n/w;->a()I

    move-result v0

    if-ge v1, v0, :cond_59

    .line 856
    iget-object v0, p0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v0, v1}, Landroid/support/v4/n/w;->d(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/cu;

    .line 857
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v4, "  #"

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v4, p0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v4, v1}, Landroid/support/v4/n/w;->c(I)I

    move-result v4

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(I)V

    .line 858
    const-string v4, ": "

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/support/v4/app/cu;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 859
    invoke-virtual {v0, v3, p2, p3, p4}, Landroid/support/v4/app/cu;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 855
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_25

    .line 862
    :cond_59
    iget-object v0, p0, Landroid/support/v4/app/ct;->d:Landroid/support/v4/n/w;

    invoke-virtual {v0}, Landroid/support/v4/n/w;->a()I

    move-result v0

    if-lez v0, :cond_af

    .line 863
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Inactive Loaders:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 864
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "    "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 865
    :goto_7c
    iget-object v0, p0, Landroid/support/v4/app/ct;->d:Landroid/support/v4/n/w;

    invoke-virtual {v0}, Landroid/support/v4/n/w;->a()I

    move-result v0

    if-ge v2, v0, :cond_af

    .line 866
    iget-object v0, p0, Landroid/support/v4/app/ct;->d:Landroid/support/v4/n/w;

    invoke-virtual {v0, v2}, Landroid/support/v4/n/w;->d(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/cu;

    .line 867
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v3, "  #"

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v3, p0, Landroid/support/v4/app/ct;->d:Landroid/support/v4/n/w;

    invoke-virtual {v3, v2}, Landroid/support/v4/n/w;->c(I)I

    move-result v3

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(I)V

    .line 868
    const-string v3, ": "

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/support/v4/app/cu;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 869
    invoke-virtual {v0, v1, p2, p3, p4}, Landroid/support/v4/app/cu;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 865
    add-int/lit8 v2, v2, 0x1

    goto :goto_7c

    .line 872
    :cond_af
    return-void
.end method

.method public final a()Z
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 876
    .line 877
    iget-object v0, p0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v0}, Landroid/support/v4/n/w;->a()I

    move-result v4

    move v2, v1

    move v3, v1

    .line 878
    :goto_9
    if-ge v2, v4, :cond_23

    .line 879
    iget-object v0, p0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v0, v2}, Landroid/support/v4/n/w;->d(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/cu;

    .line 880
    iget-boolean v5, v0, Landroid/support/v4/app/cu;->h:Z

    if-eqz v5, :cond_21

    iget-boolean v0, v0, Landroid/support/v4/app/cu;->f:Z

    if-nez v0, :cond_21

    const/4 v0, 0x1

    :goto_1c
    or-int/2addr v3, v0

    .line 878
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_9

    :cond_21
    move v0, v1

    .line 880
    goto :goto_1c

    .line 882
    :cond_23
    return v3
.end method

.method public final b(I)Landroid/support/v4/c/aa;
    .registers 4

    .prologue
    .line 737
    iget-boolean v0, p0, Landroid/support/v4/app/ct;->i:Z

    if-eqz v0, :cond_c

    .line 738
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Called while creating a loader"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 741
    :cond_c
    iget-object v0, p0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v0, p1}, Landroid/support/v4/n/w;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/cu;

    .line 742
    if-eqz v0, :cond_22

    .line 743
    iget-object v1, v0, Landroid/support/v4/app/cu;->n:Landroid/support/v4/app/cu;

    if-eqz v1, :cond_1f

    .line 744
    iget-object v0, v0, Landroid/support/v4/app/cu;->n:Landroid/support/v4/app/cu;

    iget-object v0, v0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    .line 748
    :goto_1e
    return-object v0

    .line 746
    :cond_1f
    iget-object v0, v0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    goto :goto_1e

    .line 748
    :cond_22
    const/4 v0, 0x0

    goto :goto_1e
.end method

.method public final b(ILandroid/os/Bundle;Landroid/support/v4/app/cs;)Landroid/support/v4/c/aa;
    .registers 9

    .prologue
    const/4 v4, 0x0

    .line 642
    iget-boolean v0, p0, Landroid/support/v4/app/ct;->i:Z

    if-eqz v0, :cond_d

    .line 643
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Called while creating a loader"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 646
    :cond_d
    iget-object v0, p0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v0, p1}, Landroid/support/v4/n/w;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/cu;

    .line 647
    sget-boolean v1, Landroid/support/v4/app/ct;->b:Z

    if-eqz v1, :cond_37

    const-string v1, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "restartLoader in "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": args="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 648
    :cond_37
    if-eqz v0, :cond_6f

    .line 649
    iget-object v1, p0, Landroid/support/v4/app/ct;->d:Landroid/support/v4/n/w;

    invoke-virtual {v1, p1}, Landroid/support/v4/n/w;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/cu;

    .line 650
    if-eqz v1, :cond_104

    .line 651
    iget-boolean v2, v0, Landroid/support/v4/app/cu;->e:Z

    if-eqz v2, :cond_76

    .line 656
    sget-boolean v2, Landroid/support/v4/app/ct;->b:Z

    if-eqz v2, :cond_5f

    const-string v2, "LoaderManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "  Removing last inactive loader: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 657
    :cond_5f
    const/4 v2, 0x0

    iput-boolean v2, v1, Landroid/support/v4/app/cu;->f:Z

    .line 658
    invoke-virtual {v1}, Landroid/support/v4/app/cu;->c()V

    .line 692
    :cond_65
    :goto_65
    iget-object v1, v0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    .line 2385
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/support/v4/c/aa;->u:Z

    .line 693
    iget-object v1, p0, Landroid/support/v4/app/ct;->d:Landroid/support/v4/n/w;

    invoke-virtual {v1, p1, v0}, Landroid/support/v4/n/w;->a(ILjava/lang/Object;)V

    .line 697
    :cond_6f
    :goto_6f
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/app/ct;->d(ILandroid/os/Bundle;Landroid/support/v4/app/cs;)Landroid/support/v4/app/cu;

    move-result-object v0

    .line 698
    iget-object v0, v0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    :goto_75
    return-object v0

    .line 664
    :cond_76
    iget-boolean v1, v0, Landroid/support/v4/app/cu;->h:Z

    if-nez v1, :cond_8e

    .line 668
    sget-boolean v1, Landroid/support/v4/app/ct;->b:Z

    if-eqz v1, :cond_85

    const-string v1, "LoaderManager"

    const-string v2, "  Current loader is stopped; replacing"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 669
    :cond_85
    iget-object v1, p0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v1, p1, v4}, Landroid/support/v4/n/w;->a(ILjava/lang/Object;)V

    .line 670
    invoke-virtual {v0}, Landroid/support/v4/app/cu;->c()V

    goto :goto_6f

    .line 675
    :cond_8e
    sget-boolean v1, Landroid/support/v4/app/ct;->b:Z

    if-eqz v1, :cond_99

    const-string v1, "LoaderManager"

    const-string v2, "  Current loader is running; attempting to cancel"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2336
    :cond_99
    sget-boolean v1, Landroid/support/v4/app/ct;->b:Z

    if-eqz v1, :cond_b1

    const-string v1, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "  Canceling: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2337
    :cond_b1
    iget-boolean v1, v0, Landroid/support/v4/app/cu;->h:Z

    if-eqz v1, :cond_c8

    iget-object v1, v0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    if-eqz v1, :cond_c8

    iget-boolean v1, v0, Landroid/support/v4/app/cu;->m:Z

    if-eqz v1, :cond_c8

    .line 2338
    iget-object v1, v0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    invoke-virtual {v1}, Landroid/support/v4/c/aa;->j()Z

    move-result v1

    if-nez v1, :cond_c8

    .line 2339
    invoke-virtual {v0}, Landroid/support/v4/app/cu;->d()V

    .line 677
    :cond_c8
    iget-object v1, v0, Landroid/support/v4/app/cu;->n:Landroid/support/v4/app/cu;

    if-eqz v1, :cond_ed

    .line 678
    sget-boolean v1, Landroid/support/v4/app/ct;->b:Z

    if-eqz v1, :cond_e6

    const-string v1, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "  Removing pending loader: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Landroid/support/v4/app/cu;->n:Landroid/support/v4/app/cu;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 679
    :cond_e6
    iget-object v1, v0, Landroid/support/v4/app/cu;->n:Landroid/support/v4/app/cu;

    invoke-virtual {v1}, Landroid/support/v4/app/cu;->c()V

    .line 680
    iput-object v4, v0, Landroid/support/v4/app/cu;->n:Landroid/support/v4/app/cu;

    .line 682
    :cond_ed
    sget-boolean v1, Landroid/support/v4/app/ct;->b:Z

    if-eqz v1, :cond_f8

    const-string v1, "LoaderManager"

    const-string v2, "  Enqueuing as new pending loader"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 683
    :cond_f8
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/app/ct;->c(ILandroid/os/Bundle;Landroid/support/v4/app/cs;)Landroid/support/v4/app/cu;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v4/app/cu;->n:Landroid/support/v4/app/cu;

    .line 685
    iget-object v0, v0, Landroid/support/v4/app/cu;->n:Landroid/support/v4/app/cu;

    iget-object v0, v0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    goto/16 :goto_75

    .line 691
    :cond_104
    sget-boolean v1, Landroid/support/v4/app/ct;->b:Z

    if-eqz v1, :cond_65

    const-string v1, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "  Making last loader inactive: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_65
.end method

.method final b()V
    .registers 5

    .prologue
    .line 752
    sget-boolean v0, Landroid/support/v4/app/ct;->b:Z

    if-eqz v0, :cond_18

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Starting in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 753
    :cond_18
    iget-boolean v0, p0, Landroid/support/v4/app/ct;->f:Z

    if-eqz v0, :cond_3b

    .line 754
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "here"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 755
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->fillInStackTrace()Ljava/lang/Throwable;

    .line 756
    const-string v1, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Called doStart when already started: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 767
    :cond_3a
    return-void

    .line 760
    :cond_3b
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/ct;->f:Z

    .line 764
    iget-object v0, p0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v0}, Landroid/support/v4/n/w;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_47
    if-ltz v1, :cond_3a

    .line 765
    iget-object v0, p0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v0, v1}, Landroid/support/v4/n/w;->d(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/cu;

    invoke-virtual {v0}, Landroid/support/v4/app/cu;->a()V

    .line 764
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_47
.end method

.method final c()V
    .registers 5

    .prologue
    .line 770
    sget-boolean v0, Landroid/support/v4/app/ct;->b:Z

    if-eqz v0, :cond_18

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Stopping in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 771
    :cond_18
    iget-boolean v0, p0, Landroid/support/v4/app/ct;->f:Z

    if-nez v0, :cond_3b

    .line 772
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "here"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 773
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->fillInStackTrace()Ljava/lang/Throwable;

    .line 774
    const-string v1, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Called doStop when not started: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 782
    :goto_3a
    return-void

    .line 778
    :cond_3b
    iget-object v0, p0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v0}, Landroid/support/v4/n/w;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_44
    if-ltz v1, :cond_55

    .line 779
    iget-object v0, p0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v0, v1}, Landroid/support/v4/n/w;->d(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/cu;

    invoke-virtual {v0}, Landroid/support/v4/app/cu;->b()V

    .line 778
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_44

    .line 781
    :cond_55
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/ct;->f:Z

    goto :goto_3a
.end method

.method final d()V
    .registers 8

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 785
    sget-boolean v0, Landroid/support/v4/app/ct;->b:Z

    if-eqz v0, :cond_1a

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Retaining in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 786
    :cond_1a
    iget-boolean v0, p0, Landroid/support/v4/app/ct;->f:Z

    if-nez v0, :cond_3d

    .line 787
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "here"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 788
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->fillInStackTrace()Ljava/lang/Throwable;

    .line 789
    const-string v1, "LoaderManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Called doRetain when not started: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 798
    :cond_3c
    return-void

    .line 793
    :cond_3d
    iput-boolean v6, p0, Landroid/support/v4/app/ct;->g:Z

    .line 794
    iput-boolean v5, p0, Landroid/support/v4/app/ct;->f:Z

    .line 795
    iget-object v0, p0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v0}, Landroid/support/v4/n/w;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_4a
    if-ltz v1, :cond_3c

    .line 796
    iget-object v0, p0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v0, v1}, Landroid/support/v4/n/w;->d(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/cu;

    .line 3278
    sget-boolean v2, Landroid/support/v4/app/ct;->b:Z

    if-eqz v2, :cond_6c

    const-string v2, "LoaderManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "  Retaining: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3279
    :cond_6c
    iput-boolean v6, v0, Landroid/support/v4/app/cu;->i:Z

    .line 3280
    iget-boolean v2, v0, Landroid/support/v4/app/cu;->h:Z

    iput-boolean v2, v0, Landroid/support/v4/app/cu;->j:Z

    .line 3281
    iput-boolean v5, v0, Landroid/support/v4/app/cu;->h:Z

    .line 3282
    const/4 v2, 0x0

    iput-object v2, v0, Landroid/support/v4/app/cu;->c:Landroid/support/v4/app/cs;

    .line 795
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_4a
.end method

.method final e()V
    .registers 4

    .prologue
    .line 812
    iget-object v0, p0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v0}, Landroid/support/v4/n/w;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_9
    if-ltz v1, :cond_1a

    .line 813
    iget-object v0, p0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v0, v1}, Landroid/support/v4/n/w;->d(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/cu;

    const/4 v2, 0x1

    iput-boolean v2, v0, Landroid/support/v4/app/cu;->k:Z

    .line 812
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_9

    .line 815
    :cond_1a
    return-void
.end method

.method final f()V
    .registers 5

    .prologue
    .line 818
    iget-object v0, p0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v0}, Landroid/support/v4/n/w;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_9
    if-ltz v1, :cond_2d

    .line 819
    iget-object v0, p0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v0, v1}, Landroid/support/v4/n/w;->d(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/cu;

    .line 3311
    iget-boolean v2, v0, Landroid/support/v4/app/cu;->h:Z

    if-eqz v2, :cond_29

    .line 3312
    iget-boolean v2, v0, Landroid/support/v4/app/cu;->k:Z

    if-eqz v2, :cond_29

    .line 3313
    const/4 v2, 0x0

    iput-boolean v2, v0, Landroid/support/v4/app/cu;->k:Z

    .line 3314
    iget-boolean v2, v0, Landroid/support/v4/app/cu;->e:Z

    if-eqz v2, :cond_29

    .line 3315
    iget-object v2, v0, Landroid/support/v4/app/cu;->d:Landroid/support/v4/c/aa;

    iget-object v3, v0, Landroid/support/v4/app/cu;->g:Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/app/cu;->b(Landroid/support/v4/c/aa;Ljava/lang/Object;)V

    .line 818
    :cond_29
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_9

    .line 821
    :cond_2d
    return-void
.end method

.method final g()V
    .registers 4

    .prologue
    .line 824
    iget-boolean v0, p0, Landroid/support/v4/app/ct;->g:Z

    if-nez v0, :cond_3b

    .line 825
    sget-boolean v0, Landroid/support/v4/app/ct;->b:Z

    if-eqz v0, :cond_1c

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Destroying Active in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 826
    :cond_1c
    iget-object v0, p0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v0}, Landroid/support/v4/n/w;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_25
    if-ltz v1, :cond_36

    .line 827
    iget-object v0, p0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v0, v1}, Landroid/support/v4/n/w;->d(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/cu;

    invoke-virtual {v0}, Landroid/support/v4/app/cu;->c()V

    .line 826
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_25

    .line 829
    :cond_36
    iget-object v0, p0, Landroid/support/v4/app/ct;->c:Landroid/support/v4/n/w;

    invoke-virtual {v0}, Landroid/support/v4/n/w;->b()V

    .line 832
    :cond_3b
    sget-boolean v0, Landroid/support/v4/app/ct;->b:Z

    if-eqz v0, :cond_53

    const-string v0, "LoaderManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Destroying Inactive in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 833
    :cond_53
    iget-object v0, p0, Landroid/support/v4/app/ct;->d:Landroid/support/v4/n/w;

    invoke-virtual {v0}, Landroid/support/v4/n/w;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_5c
    if-ltz v1, :cond_6d

    .line 834
    iget-object v0, p0, Landroid/support/v4/app/ct;->d:Landroid/support/v4/n/w;

    invoke-virtual {v0, v1}, Landroid/support/v4/n/w;->d(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/cu;

    invoke-virtual {v0}, Landroid/support/v4/app/cu;->c()V

    .line 833
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_5c

    .line 836
    :cond_6d
    iget-object v0, p0, Landroid/support/v4/app/ct;->d:Landroid/support/v4/n/w;

    invoke-virtual {v0}, Landroid/support/v4/n/w;->b()V

    .line 837
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 841
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 842
    const-string v1, "LoaderManager{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 843
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 844
    const-string v1, " in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 845
    iget-object v1, p0, Landroid/support/v4/app/ct;->j:Landroid/support/v4/app/bh;

    invoke-static {v1, v0}, Landroid/support/v4/n/g;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    .line 846
    const-string v1, "}}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 847
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
