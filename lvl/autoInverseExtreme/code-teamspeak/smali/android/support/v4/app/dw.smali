.class final Landroid/support/v4/app/dw;
.super Landroid/support/v4/app/dv;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 758
    invoke-direct {p0}, Landroid/support/v4/app/dv;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/dm;Landroid/support/v4/app/dn;)Landroid/app/Notification;
    .registers 33

    .prologue
    .line 761
    new-instance v2, Landroid/support/v4/app/ei;

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/support/v4/app/dm;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/support/v4/app/dm;->b:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v6, v0, Landroid/support/v4/app/dm;->c:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v7, v0, Landroid/support/v4/app/dm;->h:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v8, v0, Landroid/support/v4/app/dm;->f:Landroid/widget/RemoteViews;

    move-object/from16 v0, p1

    iget v9, v0, Landroid/support/v4/app/dm;->i:I

    move-object/from16 v0, p1

    iget-object v10, v0, Landroid/support/v4/app/dm;->d:Landroid/app/PendingIntent;

    move-object/from16 v0, p1

    iget-object v11, v0, Landroid/support/v4/app/dm;->e:Landroid/app/PendingIntent;

    move-object/from16 v0, p1

    iget-object v12, v0, Landroid/support/v4/app/dm;->g:Landroid/graphics/Bitmap;

    move-object/from16 v0, p1

    iget v13, v0, Landroid/support/v4/app/dm;->o:I

    move-object/from16 v0, p1

    iget v14, v0, Landroid/support/v4/app/dm;->p:I

    move-object/from16 v0, p1

    iget-boolean v15, v0, Landroid/support/v4/app/dm;->q:Z

    move-object/from16 v0, p1

    iget-boolean v0, v0, Landroid/support/v4/app/dm;->k:Z

    move/from16 v16, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, Landroid/support/v4/app/dm;->l:Z

    move/from16 v17, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/support/v4/app/dm;->j:I

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v4/app/dm;->n:Ljava/lang/CharSequence;

    move-object/from16 v19, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, Landroid/support/v4/app/dm;->v:Z

    move/from16 v20, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v4/app/dm;->w:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v4/app/dm;->C:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v4/app/dm;->x:Landroid/os/Bundle;

    move-object/from16 v23, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/support/v4/app/dm;->y:I

    move/from16 v24, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/support/v4/app/dm;->z:I

    move/from16 v25, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v4/app/dm;->A:Landroid/app/Notification;

    move-object/from16 v26, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v4/app/dm;->r:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, Landroid/support/v4/app/dm;->s:Z

    move/from16 v28, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v4/app/dm;->t:Ljava/lang/String;

    move-object/from16 v29, v0

    invoke-direct/range {v2 .. v29}, Landroid/support/v4/app/ei;-><init>(Landroid/content/Context;Landroid/app/Notification;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/widget/RemoteViews;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/graphics/Bitmap;IIZZZILjava/lang/CharSequence;ZLjava/lang/String;Ljava/util/ArrayList;Landroid/os/Bundle;IILandroid/app/Notification;Ljava/lang/String;ZLjava/lang/String;)V

    .line 768
    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/support/v4/app/dm;->u:Ljava/util/ArrayList;

    invoke-static {v2, v3}, Landroid/support/v4/app/dd;->a(Landroid/support/v4/app/db;Ljava/util/ArrayList;)V

    .line 769
    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/support/v4/app/dm;->m:Landroid/support/v4/app/ed;

    invoke-static {v2, v3}, Landroid/support/v4/app/dd;->a(Landroid/support/v4/app/dc;Landroid/support/v4/app/ed;)V

    .line 770
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/dn;->a(Landroid/support/v4/app/dm;Landroid/support/v4/app/dc;)Landroid/app/Notification;

    move-result-object v2

    return-object v2
.end method

.method public final a(Landroid/support/v4/app/em;)Landroid/os/Bundle;
    .registers 9

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 780
    .line 1129
    if-nez p1, :cond_5

    .line 1130
    :goto_4
    return-object v0

    .line 1132
    :cond_5
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1134
    invoke-virtual {p1}, Landroid/support/v4/app/em;->d()[Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1e

    invoke-virtual {p1}, Landroid/support/v4/app/em;->d()[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_1e

    .line 1135
    invoke-virtual {p1}, Landroid/support/v4/app/em;->d()[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v1

    .line 1137
    :cond_1e
    invoke-virtual {p1}, Landroid/support/v4/app/em;->a()[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [Landroid/os/Parcelable;

    .line 1138
    :goto_25
    array-length v4, v3

    if-ge v1, v4, :cond_42

    .line 1139
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 1140
    const-string v5, "text"

    invoke-virtual {p1}, Landroid/support/v4/app/em;->a()[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v1

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1141
    const-string v5, "author"

    invoke-virtual {v4, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1142
    aput-object v4, v3, v1

    .line 1138
    add-int/lit8 v1, v1, 0x1

    goto :goto_25

    .line 1144
    :cond_42
    const-string v0, "messages"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 1145
    invoke-virtual {p1}, Landroid/support/v4/app/em;->g()Landroid/support/v4/app/fw;

    move-result-object v0

    .line 1146
    if-eqz v0, :cond_56

    .line 1147
    const-string v1, "remote_input"

    invoke-static {v0}, Landroid/support/v4/app/eh;->a(Landroid/support/v4/app/fw;)Landroid/app/RemoteInput;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1149
    :cond_56
    const-string v0, "on_reply"

    invoke-virtual {p1}, Landroid/support/v4/app/em;->b()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1150
    const-string v0, "on_read"

    invoke-virtual {p1}, Landroid/support/v4/app/em;->c()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1151
    const-string v0, "participants"

    invoke-virtual {p1}, Landroid/support/v4/app/em;->d()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1152
    const-string v0, "timestamp"

    invoke-virtual {p1}, Landroid/support/v4/app/em;->f()J

    move-result-wide v4

    invoke-virtual {v2, v0, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    move-object v0, v2

    .line 780
    goto :goto_4
.end method

.method public final a(Landroid/os/Bundle;Landroid/support/v4/app/en;Landroid/support/v4/app/fx;)Landroid/support/v4/app/em;
    .registers 16

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 787
    .line 1159
    if-nez p1, :cond_6

    .line 1192
    :cond_5
    :goto_5
    return-object v4

    .line 1162
    :cond_6
    const-string v0, "messages"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v6

    .line 1164
    if-eqz v6, :cond_86

    .line 1165
    array-length v0, v6

    new-array v3, v0, [Ljava/lang/String;

    move v1, v2

    .line 1167
    :goto_12
    array-length v0, v3

    if-ge v1, v0, :cond_84

    .line 1168
    aget-object v0, v6, v1

    instance-of v0, v0, Landroid/os/Bundle;

    if-nez v0, :cond_6e

    .line 1178
    :cond_1b
    :goto_1b
    if-eqz v2, :cond_5

    move-object v10, v3

    .line 1185
    :goto_1e
    const-string v0, "on_read"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/app/PendingIntent;

    .line 1186
    const-string v0, "on_reply"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Landroid/app/PendingIntent;

    .line 1188
    const-string v0, "remote_input"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/RemoteInput;

    .line 1190
    const-string v1, "participants"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 1191
    if-eqz v11, :cond_5

    array-length v1, v11

    if-ne v1, v5, :cond_5

    .line 1196
    if-eqz v0, :cond_82

    .line 1217
    invoke-virtual {v0}, Landroid/app/RemoteInput;->getResultKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/app/RemoteInput;->getLabel()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0}, Landroid/app/RemoteInput;->getChoices()[Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0}, Landroid/app/RemoteInput;->getAllowFreeFormInput()Z

    move-result v4

    invoke-virtual {v0}, Landroid/app/RemoteInput;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    move-object v0, p3

    invoke-interface/range {v0 .. v5}, Landroid/support/v4/app/fx;->a(Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/CharSequence;ZLandroid/os/Bundle;)Landroid/support/v4/app/fw;

    move-result-object v2

    .line 1196
    :goto_5e
    const-string v0, "timestamp"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    move-object v0, p2

    move-object v1, v10

    move-object v3, v9

    move-object v4, v8

    move-object v5, v11

    invoke-interface/range {v0 .. v7}, Landroid/support/v4/app/en;->a([Ljava/lang/String;Landroid/support/v4/app/fw;Landroid/app/PendingIntent;Landroid/app/PendingIntent;[Ljava/lang/String;J)Landroid/support/v4/app/em;

    move-result-object v4

    goto :goto_5

    .line 1172
    :cond_6e
    aget-object v0, v6, v1

    check-cast v0, Landroid/os/Bundle;

    const-string v7, "text"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v1

    .line 1173
    aget-object v0, v3, v1

    if-eqz v0, :cond_1b

    .line 1167
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_12

    :cond_82
    move-object v2, v4

    .line 1196
    goto :goto_5e

    :cond_84
    move v2, v5

    goto :goto_1b

    :cond_86
    move-object v10, v4

    goto :goto_1e
.end method

.method public final c(Landroid/app/Notification;)Ljava/lang/String;
    .registers 3

    .prologue
    .line 775
    .line 1125
    iget-object v0, p1, Landroid/app/Notification;->category:Ljava/lang/String;

    .line 775
    return-object v0
.end method
