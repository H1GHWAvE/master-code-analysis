.class public final Landroid/support/v4/app/at;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/IBinder;
    .registers 4

    .prologue
    .line 36
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_b

    .line 1027
    invoke-virtual {p0, p1}, Landroid/os/Bundle;->getBinder(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 39
    :goto_a
    return-object v0

    :cond_b
    invoke-static {p0, p1}, Landroid/support/v4/app/au;->a(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    goto :goto_a
.end method

.method private static a(Landroid/os/Bundle;Ljava/lang/String;Landroid/os/IBinder;)V
    .registers 9

    .prologue
    const/4 v5, 0x1

    .line 51
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_b

    .line 1031
    invoke-virtual {p0, p1, p2}, Landroid/os/Bundle;->putBinder(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 1080
    :cond_a
    :goto_a
    return-void

    .line 1062
    :cond_b
    sget-boolean v0, Landroid/support/v4/app/au;->b:Z

    if-nez v0, :cond_2c

    .line 1064
    :try_start_f
    const-class v0, Landroid/os/Bundle;

    const-string v1, "putIBinder"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-class v4, Landroid/os/IBinder;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 1066
    sput-object v0, Landroid/support/v4/app/au;->a:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V
    :try_end_2a
    .catch Ljava/lang/NoSuchMethodException; {:try_start_f .. :try_end_2a} :catch_4b

    .line 1070
    :goto_2a
    sput-boolean v5, Landroid/support/v4/app/au;->b:Z

    .line 1073
    :cond_2c
    sget-object v0, Landroid/support/v4/app/au;->a:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_a

    .line 1075
    :try_start_30
    sget-object v0, Landroid/support/v4/app/au;->a:Ljava/lang/reflect/Method;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3e
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_30 .. :try_end_3e} :catch_3f
    .catch Ljava/lang/IllegalAccessException; {:try_start_30 .. :try_end_3e} :catch_56
    .catch Ljava/lang/IllegalArgumentException; {:try_start_30 .. :try_end_3e} :catch_54

    goto :goto_a

    .line 1076
    :catch_3f
    move-exception v0

    .line 1078
    :goto_40
    const-string v1, "BundleCompatDonut"

    const-string v2, "Failed to invoke putIBinder via reflection"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1079
    const/4 v0, 0x0

    sput-object v0, Landroid/support/v4/app/au;->a:Ljava/lang/reflect/Method;

    goto :goto_a

    .line 1067
    :catch_4b
    move-exception v0

    .line 1068
    const-string v1, "BundleCompatDonut"

    const-string v2, "Failed to retrieve putIBinder method"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2a

    .line 1076
    :catch_54
    move-exception v0

    goto :goto_40

    :catch_56
    move-exception v0

    goto :goto_40
.end method
