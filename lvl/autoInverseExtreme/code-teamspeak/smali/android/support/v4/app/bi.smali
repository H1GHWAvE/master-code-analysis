.class public abstract Landroid/support/v4/app/bi;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:I = 0x1


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    return-void
.end method

.method private static a(Z)V
    .registers 1

    .prologue
    .line 361
    sput-boolean p0, Landroid/support/v4/app/bl;->b:Z

    .line 362
    return-void
.end method

.method private j()Landroid/support/v4/app/cd;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 152
    invoke-virtual {p0}, Landroid/support/v4/app/bi;->a()Landroid/support/v4/app/cd;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abstract a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/Fragment$SavedState;
.end method

.method public abstract a(I)Landroid/support/v4/app/Fragment;
    .param p1    # I
        .annotation build Landroid/support/a/p;
        .end annotation
    .end param
.end method

.method public abstract a(Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;
.end method

.method public abstract a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
.end method

.method public abstract a()Landroid/support/v4/app/cd;
.end method

.method public abstract a(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V
.end method

.method public abstract a(Landroid/support/v4/app/bk;)V
.end method

.method public abstract a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
.end method

.method public abstract a(II)Z
.end method

.method public abstract a(Ljava/lang/String;I)Z
.end method

.method public abstract b(I)V
.end method

.method public abstract b(Landroid/support/v4/app/bk;)V
.end method

.method public abstract b()Z
.end method

.method public abstract c()V
.end method

.method public abstract d()Z
.end method

.method public abstract e()V
.end method

.method public abstract f()I
.end method

.method public abstract g()Landroid/support/v4/app/bj;
.end method

.method public abstract h()Ljava/util/List;
.end method

.method public abstract i()Z
.end method
