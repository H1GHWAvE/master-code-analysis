.class public Landroid/support/v4/app/ax;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnDismissListener;


# static fields
.field public static final a:I = 0x0

.field private static final at:Ljava/lang/String; = "android:savedDialogState"

.field private static final au:Ljava/lang/String; = "android:style"

.field private static final av:Ljava/lang/String; = "android:theme"

.field private static final aw:Ljava/lang/String; = "android:cancelable"

.field private static final ax:Ljava/lang/String; = "android:showsDialog"

.field private static final ay:Ljava/lang/String; = "android:backStackId"

.field public static final b:I = 0x1

.field public static final c:I = 0x2

.field public static final d:I = 0x3


# instance fields
.field e:I

.field public f:I

.field g:Z

.field h:Z

.field i:I

.field public j:Landroid/app/Dialog;

.field k:Z

.field l:Z

.field m:Z


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 96
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 85
    iput v0, p0, Landroid/support/v4/app/ax;->e:I

    .line 86
    iput v0, p0, Landroid/support/v4/app/ax;->f:I

    .line 87
    iput-boolean v1, p0, Landroid/support/v4/app/ax;->g:Z

    .line 88
    iput-boolean v1, p0, Landroid/support/v4/app/ax;->h:Z

    .line 89
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v4/app/ax;->i:I

    .line 97
    return-void
.end method

.method private A()I
    .registers 2
    .annotation build Landroid/support/a/ai;
    .end annotation

    .prologue
    .line 212
    iget v0, p0, Landroid/support/v4/app/ax;->f:I

    return v0
.end method

.method private B()Z
    .registers 2

    .prologue
    .line 233
    iget-boolean v0, p0, Landroid/support/v4/app/ax;->g:Z

    return v0
.end method

.method private C()Z
    .registers 2

    .prologue
    .line 261
    iget-boolean v0, p0, Landroid/support/v4/app/ax;->h:Z

    return v0
.end method

.method private d(Z)V
    .registers 4

    .prologue
    const/4 v1, 0x1

    .line 181
    iget-boolean v0, p0, Landroid/support/v4/app/ax;->l:Z

    if-eqz v0, :cond_6

    .line 204
    :goto_5
    return-void

    .line 184
    :cond_6
    iput-boolean v1, p0, Landroid/support/v4/app/ax;->l:Z

    .line 185
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/ax;->m:Z

    .line 186
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    if-eqz v0, :cond_17

    .line 187
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 188
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 190
    :cond_17
    iput-boolean v1, p0, Landroid/support/v4/app/ax;->k:Z

    .line 191
    iget v0, p0, Landroid/support/v4/app/ax;->i:I

    if-ltz v0, :cond_28

    .line 1688
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    .line 192
    iget v1, p0, Landroid/support/v4/app/ax;->i:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bi;->b(I)V

    .line 194
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v4/app/ax;->i:I

    goto :goto_5

    .line 2688
    :cond_28
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    .line 196
    invoke-virtual {v0}, Landroid/support/v4/app/bi;->a()Landroid/support/v4/app/cd;

    move-result-object v0

    .line 197
    invoke-virtual {v0, p0}, Landroid/support/v4/app/cd;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;

    .line 198
    if-eqz p1, :cond_37

    .line 199
    invoke-virtual {v0}, Landroid/support/v4/app/cd;->j()I

    goto :goto_5

    .line 201
    :cond_37
    invoke-virtual {v0}, Landroid/support/v4/app/cd;->i()I

    goto :goto_5
.end method

.method private e(Z)V
    .registers 2

    .prologue
    .line 254
    iput-boolean p1, p0, Landroid/support/v4/app/ax;->h:Z

    .line 255
    return-void
.end method

.method private y()V
    .registers 2

    .prologue
    .line 177
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/support/v4/app/ax;->d(Z)V

    .line 178
    return-void
.end method

.method private z()Landroid/app/Dialog;
    .registers 2

    .prologue
    .line 207
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/cd;Ljava/lang/String;)I
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 152
    iput-boolean v1, p0, Landroid/support/v4/app/ax;->l:Z

    .line 153
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/ax;->m:Z

    .line 154
    invoke-virtual {p1, p0, p2}, Landroid/support/v4/app/cd;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;

    .line 155
    iput-boolean v1, p0, Landroid/support/v4/app/ax;->k:Z

    .line 156
    invoke-virtual {p1}, Landroid/support/v4/app/cd;->i()I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/ax;->i:I

    .line 157
    iget v0, p0, Landroid/support/v4/app/ax;->i:I

    return v0
.end method

.method public final a(Landroid/app/Activity;)V
    .registers 3

    .prologue
    .line 266
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/app/Activity;)V

    .line 267
    iget-boolean v0, p0, Landroid/support/v4/app/ax;->m:Z

    if-nez v0, :cond_a

    .line 270
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/ax;->l:Z

    .line 272
    :cond_a
    return-void
.end method

.method public a(Landroid/app/Dialog;I)V
    .registers 5

    .prologue
    .line 322
    packed-switch p2, :pswitch_data_12

    .line 332
    :goto_3
    return-void

    .line 324
    :pswitch_4
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x18

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 330
    :pswitch_d
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    goto :goto_3

    .line 322
    :pswitch_data_12
    .packed-switch 0x1
        :pswitch_d
        :pswitch_d
        :pswitch_4
    .end packed-switch
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 287
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/os/Bundle;)V

    .line 289
    iget v0, p0, Landroid/support/v4/app/ax;->R:I

    if-nez v0, :cond_3a

    move v0, v1

    :goto_a
    iput-boolean v0, p0, Landroid/support/v4/app/ax;->h:Z

    .line 291
    if-eqz p1, :cond_39

    .line 292
    const-string v0, "android:style"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/ax;->e:I

    .line 293
    const-string v0, "android:theme"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/ax;->f:I

    .line 294
    const-string v0, "android:cancelable"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/v4/app/ax;->g:Z

    .line 295
    const-string v0, "android:showsDialog"

    iget-boolean v1, p0, Landroid/support/v4/app/ax;->h:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/v4/app/ax;->h:Z

    .line 296
    const-string v0, "android:backStackId"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroid/support/v4/app/ax;->i:I

    .line 299
    :cond_39
    return-void

    :cond_3a
    move v0, v2

    .line 289
    goto :goto_a
.end method

.method public a(Landroid/support/v4/app/bi;Ljava/lang/String;)V
    .registers 4

    .prologue
    .line 135
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/ax;->l:Z

    .line 136
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/ax;->m:Z

    .line 137
    invoke-virtual {p1}, Landroid/support/v4/app/bi;->a()Landroid/support/v4/app/cd;

    move-result-object v0

    .line 138
    invoke-virtual {v0, p0, p2}, Landroid/support/v4/app/cd;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;

    .line 139
    invoke-virtual {v0}, Landroid/support/v4/app/cd;->i()I

    .line 140
    return-void
.end method

.method public final a(Z)V
    .registers 3

    .prologue
    .line 225
    iput-boolean p1, p0, Landroid/support/v4/app/ax;->g:Z

    .line 226
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    invoke-virtual {v0, p1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 227
    :cond_b
    return-void
.end method

.method public final b(Landroid/os/Bundle;)Landroid/view/LayoutInflater;
    .registers 4

    .prologue
    .line 304
    iget-boolean v0, p0, Landroid/support/v4/app/ax;->h:Z

    if-nez v0, :cond_9

    .line 305
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->b(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 316
    :goto_8
    return-object v0

    .line 308
    :cond_9
    invoke-virtual {p0}, Landroid/support/v4/app/ax;->d()Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 310
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    if-eqz v0, :cond_29

    .line 311
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    iget v1, p0, Landroid/support/v4/app/ax;->e:I

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/ax;->a(Landroid/app/Dialog;I)V

    .line 313
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    goto :goto_8

    .line 316
    :cond_29
    iget-object v0, p0, Landroid/support/v4/app/ax;->N:Landroid/support/v4/app/bh;

    .line 3166
    iget-object v0, v0, Landroid/support/v4/app/bh;->c:Landroid/content/Context;

    .line 316
    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    goto :goto_8
.end method

.method public b()V
    .registers 2

    .prologue
    .line 167
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v4/app/ax;->d(Z)V

    .line 168
    return-void
.end method

.method public final c()V
    .registers 2

    .prologue
    .line 276
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->c()V

    .line 277
    iget-boolean v0, p0, Landroid/support/v4/app/ax;->m:Z

    if-nez v0, :cond_e

    iget-boolean v0, p0, Landroid/support/v4/app/ax;->l:Z

    if-nez v0, :cond_e

    .line 281
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/ax;->l:Z

    .line 283
    :cond_e
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .registers 4

    .prologue
    .line 376
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->c(Landroid/os/Bundle;)V

    .line 378
    iget-boolean v0, p0, Landroid/support/v4/app/ax;->h:Z

    if-nez v0, :cond_8

    .line 399
    :cond_7
    :goto_7
    return-void

    .line 3237
    :cond_8
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->ac:Landroid/view/View;

    .line 383
    if-eqz v0, :cond_1f

    .line 384
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_1a

    .line 385
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "DialogFragment can not be attached to a container view"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 387
    :cond_1a
    iget-object v1, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 389
    :cond_1f
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    invoke-virtual {p0}, Landroid/support/v4/app/ax;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOwnerActivity(Landroid/app/Activity;)V

    .line 390
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    iget-boolean v1, p0, Landroid/support/v4/app/ax;->g:Z

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 391
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    invoke-virtual {v0, p0}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 392
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    invoke-virtual {v0, p0}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 393
    if-eqz p1, :cond_7

    .line 394
    const-string v0, "android:savedDialogState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 395
    if-eqz v0, :cond_7

    .line 396
    iget-object v1, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->onRestoreInstanceState(Landroid/os/Bundle;)V

    goto :goto_7
.end method

.method public d()Landroid/app/Dialog;
    .registers 4
    .annotation build Landroid/support/a/y;
    .end annotation

    .prologue
    .line 358
    new-instance v0, Landroid/app/Dialog;

    invoke-virtual {p0}, Landroid/support/v4/app/ax;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    .line 3212
    iget v2, p0, Landroid/support/v4/app/ax;->f:I

    .line 358
    invoke-direct {v0, v1, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    return-object v0
.end method

.method public final d(Landroid/os/Bundle;)V
    .registers 4

    .prologue
    .line 412
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->d(Landroid/os/Bundle;)V

    .line 413
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    if-eqz v0, :cond_14

    .line 414
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->onSaveInstanceState()Landroid/os/Bundle;

    move-result-object v0

    .line 415
    if-eqz v0, :cond_14

    .line 416
    const-string v1, "android:savedDialogState"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 419
    :cond_14
    iget v0, p0, Landroid/support/v4/app/ax;->e:I

    if-eqz v0, :cond_1f

    .line 420
    const-string v0, "android:style"

    iget v1, p0, Landroid/support/v4/app/ax;->e:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 422
    :cond_1f
    iget v0, p0, Landroid/support/v4/app/ax;->f:I

    if-eqz v0, :cond_2a

    .line 423
    const-string v0, "android:theme"

    iget v1, p0, Landroid/support/v4/app/ax;->f:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 425
    :cond_2a
    iget-boolean v0, p0, Landroid/support/v4/app/ax;->g:Z

    if-nez v0, :cond_35

    .line 426
    const-string v0, "android:cancelable"

    iget-boolean v1, p0, Landroid/support/v4/app/ax;->g:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 428
    :cond_35
    iget-boolean v0, p0, Landroid/support/v4/app/ax;->h:Z

    if-nez v0, :cond_40

    .line 429
    const-string v0, "android:showsDialog"

    iget-boolean v1, p0, Landroid/support/v4/app/ax;->h:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 431
    :cond_40
    iget v0, p0, Landroid/support/v4/app/ax;->i:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_4c

    .line 432
    const-string v0, "android:backStackId"

    iget v1, p0, Landroid/support/v4/app/ax;->i:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 434
    :cond_4c
    return-void
.end method

.method public e()V
    .registers 2

    .prologue
    .line 403
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->e()V

    .line 404
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    if-eqz v0, :cond_f

    .line 405
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/app/ax;->k:Z

    .line 406
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 408
    :cond_f
    return-void
.end method

.method public f()V
    .registers 2

    .prologue
    .line 438
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->f()V

    .line 439
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    if-eqz v0, :cond_c

    .line 440
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->hide()V

    .line 442
    :cond_c
    return-void
.end method

.method public final g()V
    .registers 2

    .prologue
    .line 449
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->g()V

    .line 450
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    if-eqz v0, :cond_12

    .line 454
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/ax;->k:Z

    .line 455
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 456
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 458
    :cond_12
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .registers 2

    .prologue
    .line 362
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .registers 3

    .prologue
    .line 365
    iget-boolean v0, p0, Landroid/support/v4/app/ax;->k:Z

    if-nez v0, :cond_8

    .line 370
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/support/v4/app/ax;->d(Z)V

    .line 372
    :cond_8
    return-void
.end method

.method public final q_()V
    .registers 3

    .prologue
    .line 114
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v4/app/ax;->e:I

    .line 115
    iget v0, p0, Landroid/support/v4/app/ax;->e:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_d

    iget v0, p0, Landroid/support/v4/app/ax;->e:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_12

    .line 116
    :cond_d
    const v0, 0x1030059

    iput v0, p0, Landroid/support/v4/app/ax;->f:I

    .line 119
    :cond_12
    const v0, 0x7f0700b5

    iput v0, p0, Landroid/support/v4/app/ax;->f:I

    .line 121
    return-void
.end method
