.class Landroid/support/v4/app/ec;
.super Landroid/support/v4/app/eb;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 653
    invoke-direct {p0}, Landroid/support/v4/app/eb;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v4/app/dm;Landroid/support/v4/app/dn;)Landroid/app/Notification;
    .registers 29

    .prologue
    .line 656
    new-instance v2, Landroid/support/v4/app/ew;

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/support/v4/app/dm;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/support/v4/app/dm;->B:Landroid/app/Notification;

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/support/v4/app/dm;->b:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v6, v0, Landroid/support/v4/app/dm;->c:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v7, v0, Landroid/support/v4/app/dm;->h:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v8, v0, Landroid/support/v4/app/dm;->f:Landroid/widget/RemoteViews;

    move-object/from16 v0, p1

    iget v9, v0, Landroid/support/v4/app/dm;->i:I

    move-object/from16 v0, p1

    iget-object v10, v0, Landroid/support/v4/app/dm;->d:Landroid/app/PendingIntent;

    move-object/from16 v0, p1

    iget-object v11, v0, Landroid/support/v4/app/dm;->e:Landroid/app/PendingIntent;

    move-object/from16 v0, p1

    iget-object v12, v0, Landroid/support/v4/app/dm;->g:Landroid/graphics/Bitmap;

    move-object/from16 v0, p1

    iget v13, v0, Landroid/support/v4/app/dm;->o:I

    move-object/from16 v0, p1

    iget v14, v0, Landroid/support/v4/app/dm;->p:I

    move-object/from16 v0, p1

    iget-boolean v15, v0, Landroid/support/v4/app/dm;->q:Z

    move-object/from16 v0, p1

    iget-boolean v0, v0, Landroid/support/v4/app/dm;->k:Z

    move/from16 v16, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, Landroid/support/v4/app/dm;->l:Z

    move/from16 v17, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/support/v4/app/dm;->j:I

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v4/app/dm;->n:Ljava/lang/CharSequence;

    move-object/from16 v19, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, Landroid/support/v4/app/dm;->v:Z

    move/from16 v20, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v4/app/dm;->C:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v4/app/dm;->x:Landroid/os/Bundle;

    move-object/from16 v22, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v4/app/dm;->r:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, p1

    iget-boolean v0, v0, Landroid/support/v4/app/dm;->s:Z

    move/from16 v24, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/support/v4/app/dm;->t:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-direct/range {v2 .. v25}, Landroid/support/v4/app/ew;-><init>(Landroid/content/Context;Landroid/app/Notification;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/widget/RemoteViews;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/graphics/Bitmap;IIZZZILjava/lang/CharSequence;ZLjava/util/ArrayList;Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/String;)V

    .line 662
    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/support/v4/app/dm;->u:Ljava/util/ArrayList;

    invoke-static {v2, v3}, Landroid/support/v4/app/dd;->a(Landroid/support/v4/app/db;Ljava/util/ArrayList;)V

    .line 663
    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/support/v4/app/dm;->m:Landroid/support/v4/app/ed;

    invoke-static {v2, v3}, Landroid/support/v4/app/dd;->a(Landroid/support/v4/app/dc;Landroid/support/v4/app/ed;)V

    .line 664
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/dn;->a(Landroid/support/v4/app/dm;Landroid/support/v4/app/dc;)Landroid/app/Notification;

    move-result-object v2

    return-object v2
.end method

.method public final a(Landroid/app/Notification;)Landroid/os/Bundle;
    .registers 3

    .prologue
    .line 669
    .line 1120
    iget-object v0, p1, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    .line 669
    return-object v0
.end method

.method public a(Landroid/app/Notification;I)Landroid/support/v4/app/df;
    .registers 9

    .prologue
    .line 679
    sget-object v0, Landroid/support/v4/app/df;->e:Landroid/support/v4/app/el;

    sget-object v1, Landroid/support/v4/app/fn;->c:Landroid/support/v4/app/fx;

    .line 1130
    iget-object v2, p1, Landroid/app/Notification;->actions:[Landroid/app/Notification$Action;

    aget-object v4, v2, p2

    .line 1131
    const/4 v5, 0x0

    .line 1132
    iget-object v2, p1, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    const-string v3, "android.support.actionExtras"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v2

    .line 1134
    if-eqz v2, :cond_1a

    .line 1135
    invoke-virtual {v2, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    move-object v5, v2

    .line 1137
    :cond_1a
    iget v2, v4, Landroid/app/Notification$Action;->icon:I

    iget-object v3, v4, Landroid/app/Notification$Action;->title:Ljava/lang/CharSequence;

    iget-object v4, v4, Landroid/app/Notification$Action;->actionIntent:Landroid/app/PendingIntent;

    invoke-static/range {v0 .. v5}, Landroid/support/v4/app/et;->a(Landroid/support/v4/app/el;Landroid/support/v4/app/fx;ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;)Landroid/support/v4/app/ek;

    move-result-object v0

    .line 679
    check-cast v0, Landroid/support/v4/app/df;

    return-object v0
.end method

.method public final b(Landroid/app/Notification;)I
    .registers 3

    .prologue
    .line 674
    .line 1124
    iget-object v0, p1, Landroid/app/Notification;->actions:[Landroid/app/Notification$Action;

    if-eqz v0, :cond_8

    iget-object v0, p1, Landroid/app/Notification;->actions:[Landroid/app/Notification$Action;

    array-length v0, v0

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    .line 674
    goto :goto_7
.end method

.method public d(Landroid/app/Notification;)Z
    .registers 4

    .prologue
    .line 685
    .line 1142
    iget-object v0, p1, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    const-string v1, "android.support.localOnly"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 685
    return v0
.end method

.method public e(Landroid/app/Notification;)Ljava/lang/String;
    .registers 4

    .prologue
    .line 690
    .line 1146
    iget-object v0, p1, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    const-string v1, "android.support.groupKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 690
    return-object v0
.end method

.method public f(Landroid/app/Notification;)Z
    .registers 4

    .prologue
    .line 695
    .line 1150
    iget-object v0, p1, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    const-string v1, "android.support.isGroupSummary"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 695
    return v0
.end method

.method public g(Landroid/app/Notification;)Ljava/lang/String;
    .registers 4

    .prologue
    .line 700
    .line 1154
    iget-object v0, p1, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    const-string v1, "android.support.sortKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 700
    return-object v0
.end method
