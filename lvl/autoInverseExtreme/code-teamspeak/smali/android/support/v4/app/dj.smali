.class public final Landroid/support/v4/app/dj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/app/di;


# static fields
.field private static final a:Ljava/lang/String; = "android.wearable.EXTENSIONS"

.field private static final b:Ljava/lang/String; = "flags"

.field private static final c:Ljava/lang/String; = "inProgressLabel"

.field private static final d:Ljava/lang/String; = "confirmLabel"

.field private static final e:Ljava/lang/String; = "cancelLabel"

.field private static final f:I = 0x1

.field private static final g:I = 0x1


# instance fields
.field private h:I

.field private i:Ljava/lang/CharSequence;

.field private j:Ljava/lang/CharSequence;

.field private k:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 2020
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2010
    const/4 v0, 0x1

    iput v0, p0, Landroid/support/v4/app/dj;->h:I

    .line 2021
    return-void
.end method

.method private constructor <init>(Landroid/support/v4/app/df;)V
    .registers 5

    .prologue
    const/4 v2, 0x1

    .line 2028
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2010
    iput v2, p0, Landroid/support/v4/app/dj;->h:I

    .line 2859
    iget-object v0, p1, Landroid/support/v4/app/df;->a:Landroid/os/Bundle;

    .line 2029
    const-string v1, "android.wearable.EXTENSIONS"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2030
    if-eqz v0, :cond_30

    .line 2031
    const-string v1, "flags"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Landroid/support/v4/app/dj;->h:I

    .line 2032
    const-string v1, "inProgressLabel"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v4/app/dj;->i:Ljava/lang/CharSequence;

    .line 2033
    const-string v1, "confirmLabel"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v4/app/dj;->j:Ljava/lang/CharSequence;

    .line 2034
    const-string v1, "cancelLabel"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/dj;->k:Ljava/lang/CharSequence;

    .line 2036
    :cond_30
    return-void
.end method

.method private a()Landroid/support/v4/app/dj;
    .registers 3

    .prologue
    .line 2066
    new-instance v0, Landroid/support/v4/app/dj;

    invoke-direct {v0}, Landroid/support/v4/app/dj;-><init>()V

    .line 2067
    iget v1, p0, Landroid/support/v4/app/dj;->h:I

    iput v1, v0, Landroid/support/v4/app/dj;->h:I

    .line 2068
    iget-object v1, p0, Landroid/support/v4/app/dj;->i:Ljava/lang/CharSequence;

    iput-object v1, v0, Landroid/support/v4/app/dj;->i:Ljava/lang/CharSequence;

    .line 2069
    iget-object v1, p0, Landroid/support/v4/app/dj;->j:Ljava/lang/CharSequence;

    iput-object v1, v0, Landroid/support/v4/app/dj;->j:Ljava/lang/CharSequence;

    .line 2070
    iget-object v1, p0, Landroid/support/v4/app/dj;->k:Ljava/lang/CharSequence;

    iput-object v1, v0, Landroid/support/v4/app/dj;->k:Ljava/lang/CharSequence;

    .line 2071
    return-object v0
.end method

.method private a(Ljava/lang/CharSequence;)Landroid/support/v4/app/dj;
    .registers 2

    .prologue
    .line 2111
    iput-object p1, p0, Landroid/support/v4/app/dj;->i:Ljava/lang/CharSequence;

    .line 2112
    return-object p0
.end method

.method private a(Z)Landroid/support/v4/app/dj;
    .registers 3

    .prologue
    .line 2081
    .line 3096
    if-eqz p1, :cond_9

    .line 3097
    iget v0, p0, Landroid/support/v4/app/dj;->h:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/v4/app/dj;->h:I

    .line 2082
    :goto_8
    return-object p0

    .line 3099
    :cond_9
    iget v0, p0, Landroid/support/v4/app/dj;->h:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Landroid/support/v4/app/dj;->h:I

    goto :goto_8
.end method

.method private b(Ljava/lang/CharSequence;)Landroid/support/v4/app/dj;
    .registers 2

    .prologue
    .line 2133
    iput-object p1, p0, Landroid/support/v4/app/dj;->j:Ljava/lang/CharSequence;

    .line 2134
    return-object p0
.end method

.method private b(Z)V
    .registers 3

    .prologue
    .line 2096
    if-eqz p1, :cond_9

    .line 2097
    iget v0, p0, Landroid/support/v4/app/dj;->h:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/v4/app/dj;->h:I

    .line 2101
    :goto_8
    return-void

    .line 2099
    :cond_9
    iget v0, p0, Landroid/support/v4/app/dj;->h:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Landroid/support/v4/app/dj;->h:I

    goto :goto_8
.end method

.method private b()Z
    .registers 2

    .prologue
    .line 2092
    iget v0, p0, Landroid/support/v4/app/dj;->h:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private c(Ljava/lang/CharSequence;)Landroid/support/v4/app/dj;
    .registers 2

    .prologue
    .line 2155
    iput-object p1, p0, Landroid/support/v4/app/dj;->k:Ljava/lang/CharSequence;

    .line 2156
    return-object p0
.end method

.method private c()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 2122
    iget-object v0, p0, Landroid/support/v4/app/dj;->i:Ljava/lang/CharSequence;

    return-object v0
.end method

.method private d()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 2144
    iget-object v0, p0, Landroid/support/v4/app/dj;->j:Ljava/lang/CharSequence;

    return-object v0
.end method

.method private e()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 2166
    iget-object v0, p0, Landroid/support/v4/app/dj;->k:Ljava/lang/CharSequence;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/dh;)Landroid/support/v4/app/dh;
    .registers 5

    .prologue
    .line 2045
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2047
    iget v1, p0, Landroid/support/v4/app/dj;->h:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_11

    .line 2048
    const-string v1, "flags"

    iget v2, p0, Landroid/support/v4/app/dj;->h:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2050
    :cond_11
    iget-object v1, p0, Landroid/support/v4/app/dj;->i:Ljava/lang/CharSequence;

    if-eqz v1, :cond_1c

    .line 2051
    const-string v1, "inProgressLabel"

    iget-object v2, p0, Landroid/support/v4/app/dj;->i:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 2053
    :cond_1c
    iget-object v1, p0, Landroid/support/v4/app/dj;->j:Ljava/lang/CharSequence;

    if-eqz v1, :cond_27

    .line 2054
    const-string v1, "confirmLabel"

    iget-object v2, p0, Landroid/support/v4/app/dj;->j:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 2056
    :cond_27
    iget-object v1, p0, Landroid/support/v4/app/dj;->k:Ljava/lang/CharSequence;

    if-eqz v1, :cond_32

    .line 2057
    const-string v1, "cancelLabel"

    iget-object v2, p0, Landroid/support/v4/app/dj;->k:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 2927
    :cond_32
    iget-object v1, p1, Landroid/support/v4/app/dh;->a:Landroid/os/Bundle;

    .line 2060
    const-string v2, "android.wearable.EXTENSIONS"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2061
    return-object p1
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1994
    .line 4066
    new-instance v0, Landroid/support/v4/app/dj;

    invoke-direct {v0}, Landroid/support/v4/app/dj;-><init>()V

    .line 4067
    iget v1, p0, Landroid/support/v4/app/dj;->h:I

    iput v1, v0, Landroid/support/v4/app/dj;->h:I

    .line 4068
    iget-object v1, p0, Landroid/support/v4/app/dj;->i:Ljava/lang/CharSequence;

    iput-object v1, v0, Landroid/support/v4/app/dj;->i:Ljava/lang/CharSequence;

    .line 4069
    iget-object v1, p0, Landroid/support/v4/app/dj;->j:Ljava/lang/CharSequence;

    iput-object v1, v0, Landroid/support/v4/app/dj;->j:Ljava/lang/CharSequence;

    .line 4070
    iget-object v1, p0, Landroid/support/v4/app/dj;->k:Ljava/lang/CharSequence;

    iput-object v1, v0, Landroid/support/v4/app/dj;->k:Ljava/lang/CharSequence;

    .line 1994
    return-object v0
.end method
