.class public final Landroid/support/v4/n/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:[I

.field private b:I

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v4/n/e;-><init>(B)V

    .line 49
    return-void
.end method

.method private constructor <init>(B)V
    .registers 5

    .prologue
    const/4 v2, 0x1

    const/16 v0, 0x8

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    invoke-static {v0}, Ljava/lang/Integer;->bitCount(I)I

    move-result v1

    if-eq v1, v2, :cond_14

    .line 64
    invoke-static {v0}, Ljava/lang/Integer;->highestOneBit(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    shl-int v0, v2, v0

    .line 66
    :cond_14
    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Landroid/support/v4/n/e;->d:I

    .line 67
    new-array v0, v0, [I

    iput-object v0, p0, Landroid/support/v4/n/e;->a:[I

    .line 68
    return-void
.end method

.method private a()V
    .registers 8

    .prologue
    const/4 v6, 0x0

    .line 29
    iget-object v0, p0, Landroid/support/v4/n/e;->a:[I

    array-length v0, v0

    .line 30
    iget v1, p0, Landroid/support/v4/n/e;->b:I

    sub-int v1, v0, v1

    .line 31
    shl-int/lit8 v2, v0, 0x1

    .line 32
    if-gez v2, :cond_14

    .line 33
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Max array capacity exceeded"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35
    :cond_14
    new-array v3, v2, [I

    .line 36
    iget-object v4, p0, Landroid/support/v4/n/e;->a:[I

    iget v5, p0, Landroid/support/v4/n/e;->b:I

    invoke-static {v4, v5, v3, v6, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 37
    iget-object v4, p0, Landroid/support/v4/n/e;->a:[I

    iget v5, p0, Landroid/support/v4/n/e;->b:I

    invoke-static {v4, v6, v3, v1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 38
    iput-object v3, p0, Landroid/support/v4/n/e;->a:[I

    .line 39
    iput v6, p0, Landroid/support/v4/n/e;->b:I

    .line 40
    iput v0, p0, Landroid/support/v4/n/e;->c:I

    .line 41
    add-int/lit8 v0, v2, -0x1

    iput v0, p0, Landroid/support/v4/n/e;->d:I

    .line 42
    return-void
.end method

.method private a(I)V
    .registers 4

    .prologue
    .line 75
    iget v0, p0, Landroid/support/v4/n/e;->b:I

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Landroid/support/v4/n/e;->d:I

    and-int/2addr v0, v1

    iput v0, p0, Landroid/support/v4/n/e;->b:I

    .line 76
    iget-object v0, p0, Landroid/support/v4/n/e;->a:[I

    iget v1, p0, Landroid/support/v4/n/e;->b:I

    aput p1, v0, v1

    .line 77
    iget v0, p0, Landroid/support/v4/n/e;->b:I

    iget v1, p0, Landroid/support/v4/n/e;->c:I

    if-ne v0, v1, :cond_18

    .line 78
    invoke-direct {p0}, Landroid/support/v4/n/e;->a()V

    .line 80
    :cond_18
    return-void
.end method

.method private b()I
    .registers 4

    .prologue
    .line 100
    iget v0, p0, Landroid/support/v4/n/e;->b:I

    iget v1, p0, Landroid/support/v4/n/e;->c:I

    if-ne v0, v1, :cond_c

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 101
    :cond_c
    iget-object v0, p0, Landroid/support/v4/n/e;->a:[I

    iget v1, p0, Landroid/support/v4/n/e;->b:I

    aget v0, v0, v1

    .line 102
    iget v1, p0, Landroid/support/v4/n/e;->b:I

    add-int/lit8 v1, v1, 0x1

    iget v2, p0, Landroid/support/v4/n/e;->d:I

    and-int/2addr v1, v2

    iput v1, p0, Landroid/support/v4/n/e;->b:I

    .line 103
    return v0
.end method

.method private b(I)V
    .registers 4

    .prologue
    .line 87
    iget-object v0, p0, Landroid/support/v4/n/e;->a:[I

    iget v1, p0, Landroid/support/v4/n/e;->c:I

    aput p1, v0, v1

    .line 88
    iget v0, p0, Landroid/support/v4/n/e;->c:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Landroid/support/v4/n/e;->d:I

    and-int/2addr v0, v1

    iput v0, p0, Landroid/support/v4/n/e;->c:I

    .line 89
    iget v0, p0, Landroid/support/v4/n/e;->c:I

    iget v1, p0, Landroid/support/v4/n/e;->b:I

    if-ne v0, v1, :cond_18

    .line 90
    invoke-direct {p0}, Landroid/support/v4/n/e;->a()V

    .line 92
    :cond_18
    return-void
.end method

.method private c()I
    .registers 3

    .prologue
    .line 112
    iget v0, p0, Landroid/support/v4/n/e;->b:I

    iget v1, p0, Landroid/support/v4/n/e;->c:I

    if-ne v0, v1, :cond_c

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 113
    :cond_c
    iget v0, p0, Landroid/support/v4/n/e;->c:I

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Landroid/support/v4/n/e;->d:I

    and-int/2addr v0, v1

    .line 114
    iget-object v1, p0, Landroid/support/v4/n/e;->a:[I

    aget v1, v1, v0

    .line 115
    iput v0, p0, Landroid/support/v4/n/e;->c:I

    .line 116
    return v1
.end method

.method private c(I)V
    .registers 4

    .prologue
    .line 134
    if-gtz p1, :cond_3

    .line 141
    :goto_2
    return-void

    .line 137
    :cond_3
    invoke-direct {p0}, Landroid/support/v4/n/e;->g()I

    move-result v0

    if-le p1, v0, :cond_f

    .line 138
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 140
    :cond_f
    iget v0, p0, Landroid/support/v4/n/e;->b:I

    add-int/2addr v0, p1

    iget v1, p0, Landroid/support/v4/n/e;->d:I

    and-int/2addr v0, v1

    iput v0, p0, Landroid/support/v4/n/e;->b:I

    goto :goto_2
.end method

.method private d()V
    .registers 2

    .prologue
    .line 123
    iget v0, p0, Landroid/support/v4/n/e;->b:I

    iput v0, p0, Landroid/support/v4/n/e;->c:I

    .line 124
    return-void
.end method

.method private d(I)V
    .registers 4

    .prologue
    .line 151
    if-gtz p1, :cond_3

    .line 158
    :goto_2
    return-void

    .line 154
    :cond_3
    invoke-direct {p0}, Landroid/support/v4/n/e;->g()I

    move-result v0

    if-le p1, v0, :cond_f

    .line 155
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 157
    :cond_f
    iget v0, p0, Landroid/support/v4/n/e;->c:I

    sub-int/2addr v0, p1

    iget v1, p0, Landroid/support/v4/n/e;->d:I

    and-int/2addr v0, v1

    iput v0, p0, Landroid/support/v4/n/e;->c:I

    goto :goto_2
.end method

.method private e()I
    .registers 3

    .prologue
    .line 166
    iget v0, p0, Landroid/support/v4/n/e;->b:I

    iget v1, p0, Landroid/support/v4/n/e;->c:I

    if-ne v0, v1, :cond_c

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 167
    :cond_c
    iget-object v0, p0, Landroid/support/v4/n/e;->a:[I

    iget v1, p0, Landroid/support/v4/n/e;->b:I

    aget v0, v0, v1

    return v0
.end method

.method private e(I)I
    .registers 5

    .prologue
    .line 187
    if-ltz p1, :cond_8

    invoke-direct {p0}, Landroid/support/v4/n/e;->g()I

    move-result v0

    if-lt p1, v0, :cond_e

    :cond_8
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 188
    :cond_e
    iget-object v0, p0, Landroid/support/v4/n/e;->a:[I

    iget v1, p0, Landroid/support/v4/n/e;->b:I

    add-int/2addr v1, p1

    iget v2, p0, Landroid/support/v4/n/e;->d:I

    and-int/2addr v1, v2

    aget v0, v0, v1

    return v0
.end method

.method private f()I
    .registers 4

    .prologue
    .line 176
    iget v0, p0, Landroid/support/v4/n/e;->b:I

    iget v1, p0, Landroid/support/v4/n/e;->c:I

    if-ne v0, v1, :cond_c

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 177
    :cond_c
    iget-object v0, p0, Landroid/support/v4/n/e;->a:[I

    iget v1, p0, Landroid/support/v4/n/e;->c:I

    add-int/lit8 v1, v1, -0x1

    iget v2, p0, Landroid/support/v4/n/e;->d:I

    and-int/2addr v1, v2

    aget v0, v0, v1

    return v0
.end method

.method private g()I
    .registers 3

    .prologue
    .line 196
    iget v0, p0, Landroid/support/v4/n/e;->c:I

    iget v1, p0, Landroid/support/v4/n/e;->b:I

    sub-int/2addr v0, v1

    iget v1, p0, Landroid/support/v4/n/e;->d:I

    and-int/2addr v0, v1

    return v0
.end method

.method private h()Z
    .registers 3

    .prologue
    .line 204
    iget v0, p0, Landroid/support/v4/n/e;->b:I

    iget v1, p0, Landroid/support/v4/n/e;->c:I

    if-ne v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method
