.class final Landroid/support/v4/n/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Set;


# instance fields
.field final synthetic a:Landroid/support/v4/n/k;


# direct methods
.method constructor <init>(Landroid/support/v4/n/k;)V
    .registers 2

    .prologue
    .line 163
    iput-object p1, p0, Landroid/support/v4/n/m;->a:Landroid/support/v4/n/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a()Z
    .registers 1

    .prologue
    .line 166
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method


# virtual methods
.method public final synthetic add(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 1166
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final addAll(Ljava/util/Collection;)Z
    .registers 7

    .prologue
    .line 171
    iget-object v0, p0, Landroid/support/v4/n/m;->a:Landroid/support/v4/n/k;

    invoke-virtual {v0}, Landroid/support/v4/n/k;->a()I

    move-result v1

    .line 172
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_24

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 173
    iget-object v3, p0, Landroid/support/v4/n/m;->a:Landroid/support/v4/n/k;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/support/v4/n/k;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_a

    .line 175
    :cond_24
    iget-object v0, p0, Landroid/support/v4/n/m;->a:Landroid/support/v4/n/k;

    invoke-virtual {v0}, Landroid/support/v4/n/k;->a()I

    move-result v0

    if-eq v1, v0, :cond_2e

    const/4 v0, 0x1

    :goto_2d
    return v0

    :cond_2e
    const/4 v0, 0x0

    goto :goto_2d
.end method

.method public final clear()V
    .registers 2

    .prologue
    .line 180
    iget-object v0, p0, Landroid/support/v4/n/m;->a:Landroid/support/v4/n/k;

    invoke-virtual {v0}, Landroid/support/v4/n/k;->c()V

    .line 181
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 185
    instance-of v1, p1, Ljava/util/Map$Entry;

    if-nez v1, :cond_6

    .line 193
    :cond_5
    :goto_5
    return v0

    .line 187
    :cond_6
    check-cast p1, Ljava/util/Map$Entry;

    .line 188
    iget-object v1, p0, Landroid/support/v4/n/m;->a:Landroid/support/v4/n/k;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/n/k;->a(Ljava/lang/Object;)I

    move-result v1

    .line 189
    if-ltz v1, :cond_5

    .line 192
    iget-object v0, p0, Landroid/support/v4/n/m;->a:Landroid/support/v4/n/k;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/n/k;->a(II)Ljava/lang/Object;

    move-result-object v0

    .line 193
    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/support/v4/n/f;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_5
.end method

.method public final containsAll(Ljava/util/Collection;)Z
    .registers 4

    .prologue
    .line 198
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 199
    :cond_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_16

    .line 200
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/support/v4/n/m;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 201
    const/4 v0, 0x0

    .line 204
    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x1

    goto :goto_15
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 249
    invoke-static {p0, p1}, Landroid/support/v4/n/k;->a(Ljava/util/Set;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final hashCode()I
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 254
    .line 255
    iget-object v0, p0, Landroid/support/v4/n/m;->a:Landroid/support/v4/n/k;

    invoke-virtual {v0}, Landroid/support/v4/n/k;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    move v4, v1

    :goto_b
    if-ltz v3, :cond_33

    .line 256
    iget-object v0, p0, Landroid/support/v4/n/m;->a:Landroid/support/v4/n/k;

    invoke-virtual {v0, v3, v1}, Landroid/support/v4/n/k;->a(II)Ljava/lang/Object;

    move-result-object v0

    .line 257
    iget-object v2, p0, Landroid/support/v4/n/m;->a:Landroid/support/v4/n/k;

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v5}, Landroid/support/v4/n/k;->a(II)Ljava/lang/Object;

    move-result-object v5

    .line 258
    if-nez v0, :cond_28

    move v2, v1

    :goto_1d
    if-nez v5, :cond_2e

    move v0, v1

    :goto_20
    xor-int/2addr v0, v2

    add-int v2, v4, v0

    .line 255
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    move v4, v2

    goto :goto_b

    .line 258
    :cond_28
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    move v2, v0

    goto :goto_1d

    :cond_2e
    invoke-virtual {v5}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_20

    .line 261
    :cond_33
    return v4
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 209
    iget-object v0, p0, Landroid/support/v4/n/m;->a:Landroid/support/v4/n/k;

    invoke-virtual {v0}, Landroid/support/v4/n/k;->a()I

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 214
    new-instance v0, Landroid/support/v4/n/o;

    iget-object v1, p0, Landroid/support/v4/n/m;->a:Landroid/support/v4/n/k;

    invoke-direct {v0, v1}, Landroid/support/v4/n/o;-><init>(Landroid/support/v4/n/k;)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 219
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 224
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 229
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 234
    iget-object v0, p0, Landroid/support/v4/n/m;->a:Landroid/support/v4/n/k;

    invoke-virtual {v0}, Landroid/support/v4/n/k;->a()I

    move-result v0

    return v0
.end method

.method public final toArray()[Ljava/lang/Object;
    .registers 2

    .prologue
    .line 239
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 3

    .prologue
    .line 244
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
