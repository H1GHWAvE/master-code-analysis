.class public final Landroid/support/v4/view/ec;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:I = 0x0

.field public static final b:I = 0x1

.field static final c:Landroid/support/v4/view/ef;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 141
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 142
    const/16 v1, 0x15

    if-lt v0, v1, :cond_e

    .line 143
    new-instance v0, Landroid/support/v4/view/eh;

    invoke-direct {v0}, Landroid/support/v4/view/eh;-><init>()V

    sput-object v0, Landroid/support/v4/view/ec;->c:Landroid/support/v4/view/ef;

    .line 153
    :goto_d
    return-void

    .line 144
    :cond_e
    const/16 v1, 0x12

    if-lt v0, v1, :cond_1a

    .line 145
    new-instance v0, Landroid/support/v4/view/eg;

    invoke-direct {v0}, Landroid/support/v4/view/eg;-><init>()V

    sput-object v0, Landroid/support/v4/view/ec;->c:Landroid/support/v4/view/ef;

    goto :goto_d

    .line 146
    :cond_1a
    const/16 v1, 0xe

    if-lt v0, v1, :cond_26

    .line 147
    new-instance v0, Landroid/support/v4/view/ee;

    invoke-direct {v0}, Landroid/support/v4/view/ee;-><init>()V

    sput-object v0, Landroid/support/v4/view/ec;->c:Landroid/support/v4/view/ef;

    goto :goto_d

    .line 148
    :cond_26
    const/16 v1, 0xb

    if-lt v0, v1, :cond_32

    .line 149
    new-instance v0, Landroid/support/v4/view/ed;

    invoke-direct {v0}, Landroid/support/v4/view/ed;-><init>()V

    sput-object v0, Landroid/support/v4/view/ec;->c:Landroid/support/v4/view/ef;

    goto :goto_d

    .line 151
    :cond_32
    new-instance v0, Landroid/support/v4/view/ei;

    invoke-direct {v0}, Landroid/support/v4/view/ei;-><init>()V

    sput-object v0, Landroid/support/v4/view/ec;->c:Landroid/support/v4/view/ef;

    goto :goto_d
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160
    return-void
.end method

.method public static a(Landroid/view/ViewGroup;)V
    .registers 2

    .prologue
    .line 199
    sget-object v0, Landroid/support/v4/view/ec;->c:Landroid/support/v4/view/ef;

    invoke-interface {v0, p0}, Landroid/support/v4/view/ef;->a(Landroid/view/ViewGroup;)V

    .line 200
    return-void
.end method

.method private static a(Landroid/view/ViewGroup;I)V
    .registers 3

    .prologue
    .line 228
    sget-object v0, Landroid/support/v4/view/ec;->c:Landroid/support/v4/view/ef;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/ef;->a(Landroid/view/ViewGroup;I)V

    .line 229
    return-void
.end method

.method private static a(Landroid/view/ViewGroup;Z)V
    .registers 3

    .prologue
    .line 240
    sget-object v0, Landroid/support/v4/view/ec;->c:Landroid/support/v4/view/ef;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/ef;->a(Landroid/view/ViewGroup;Z)V

    .line 241
    return-void
.end method

.method private static a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 4

    .prologue
    .line 179
    sget-object v0, Landroid/support/v4/view/ec;->c:Landroid/support/v4/view/ef;

    invoke-interface {v0, p0, p1, p2}, Landroid/support/v4/view/ef;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method private static b(Landroid/view/ViewGroup;)I
    .registers 2

    .prologue
    .line 215
    sget-object v0, Landroid/support/v4/view/ec;->c:Landroid/support/v4/view/ef;

    invoke-interface {v0, p0}, Landroid/support/v4/view/ef;->b(Landroid/view/ViewGroup;)I

    move-result v0

    return v0
.end method

.method private static c(Landroid/view/ViewGroup;)Z
    .registers 2

    .prologue
    .line 249
    sget-object v0, Landroid/support/v4/view/ec;->c:Landroid/support/v4/view/ef;

    invoke-interface {v0, p0}, Landroid/support/v4/view/ef;->c(Landroid/view/ViewGroup;)Z

    move-result v0

    return v0
.end method

.method private static d(Landroid/view/ViewGroup;)I
    .registers 2

    .prologue
    .line 265
    sget-object v0, Landroid/support/v4/view/ec;->c:Landroid/support/v4/view/ef;

    invoke-interface {v0, p0}, Landroid/support/v4/view/ef;->d(Landroid/view/ViewGroup;)I

    move-result v0

    return v0
.end method
