.class Landroid/support/v4/view/de;
.super Landroid/support/v4/view/dd;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 1308
    invoke-direct {p0}, Landroid/support/v4/view/dd;-><init>()V

    return-void
.end method


# virtual methods
.method public final L(Landroid/view/View;)I
    .registers 3

    .prologue
    .line 1352
    .line 2060
    invoke-virtual {p1}, Landroid/view/View;->getWindowSystemUiVisibility()I

    move-result v0

    .line 1352
    return v0
.end method

.method public final T(Landroid/view/View;)Z
    .registers 3

    .prologue
    .line 1357
    .line 2064
    invoke-virtual {p1}, Landroid/view/View;->isPaddingRelative()Z

    move-result v0

    .line 1357
    return v0
.end method

.method public final a(Landroid/view/View;Landroid/graphics/Paint;)V
    .registers 3

    .prologue
    .line 1322
    .line 2036
    invoke-virtual {p1, p2}, Landroid/view/View;->setLayerPaint(Landroid/graphics/Paint;)V

    .line 1323
    return-void
.end method

.method public final b(Landroid/view/View;IIII)V
    .registers 6

    .prologue
    .line 1347
    .line 2056
    invoke-virtual {p1, p2, p3, p4, p5}, Landroid/view/View;->setPaddingRelative(IIII)V

    .line 1348
    return-void
.end method

.method public final e(Landroid/view/View;I)V
    .registers 3

    .prologue
    .line 1317
    .line 2032
    invoke-virtual {p1, p2}, Landroid/view/View;->setLabelFor(I)V

    .line 1318
    return-void
.end method

.method public final f(Landroid/view/View;I)V
    .registers 3

    .prologue
    .line 1332
    .line 2044
    invoke-virtual {p1, p2}, Landroid/view/View;->setLayoutDirection(I)V

    .line 1333
    return-void
.end method

.method public final j(Landroid/view/View;)I
    .registers 3

    .prologue
    .line 1312
    .line 2028
    invoke-virtual {p1}, Landroid/view/View;->getLabelFor()I

    move-result v0

    .line 1312
    return v0
.end method

.method public final k(Landroid/view/View;)I
    .registers 3

    .prologue
    .line 1327
    .line 2040
    invoke-virtual {p1}, Landroid/view/View;->getLayoutDirection()I

    move-result v0

    .line 1327
    return v0
.end method

.method public final r(Landroid/view/View;)I
    .registers 3

    .prologue
    .line 1337
    .line 2048
    invoke-virtual {p1}, Landroid/view/View;->getPaddingStart()I

    move-result v0

    .line 1337
    return v0
.end method

.method public final s(Landroid/view/View;)I
    .registers 3

    .prologue
    .line 1342
    .line 2052
    invoke-virtual {p1}, Landroid/view/View;->getPaddingEnd()I

    move-result v0

    .line 1342
    return v0
.end method
