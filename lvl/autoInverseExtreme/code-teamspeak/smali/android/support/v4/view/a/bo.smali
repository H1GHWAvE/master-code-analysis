.class final Landroid/support/v4/view/a/bo;
.super Landroid/support/v4/view/a/bq;
.source "SourceFile"


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 116
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v4/view/a/bq;-><init>(B)V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .registers 2

    .prologue
    .line 116
    invoke-direct {p0}, Landroid/support/v4/view/a/bo;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1028
    invoke-static {}, Landroid/view/accessibility/AccessibilityWindowInfo;->obtain()Landroid/view/accessibility/AccessibilityWindowInfo;

    move-result-object v0

    .line 119
    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 124
    .line 1032
    check-cast p1, Landroid/view/accessibility/AccessibilityWindowInfo;

    invoke-static {p1}, Landroid/view/accessibility/AccessibilityWindowInfo;->obtain(Landroid/view/accessibility/AccessibilityWindowInfo;)Landroid/view/accessibility/AccessibilityWindowInfo;

    move-result-object v0

    .line 124
    return-object v0
.end method

.method public final a(Ljava/lang/Object;I)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 179
    .line 1077
    check-cast p1, Landroid/view/accessibility/AccessibilityWindowInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityWindowInfo;->getChild(I)Landroid/view/accessibility/AccessibilityWindowInfo;

    move-result-object v0

    .line 179
    return-object v0
.end method

.method public final a(Ljava/lang/Object;Landroid/graphics/Rect;)V
    .registers 3

    .prologue
    .line 154
    .line 1057
    check-cast p1, Landroid/view/accessibility/AccessibilityWindowInfo;

    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityWindowInfo;->getBoundsInScreen(Landroid/graphics/Rect;)V

    .line 155
    return-void
.end method

.method public final b(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 129
    .line 1037
    check-cast p1, Landroid/view/accessibility/AccessibilityWindowInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityWindowInfo;->getType()I

    move-result v0

    .line 129
    return v0
.end method

.method public final c(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 134
    .line 1041
    check-cast p1, Landroid/view/accessibility/AccessibilityWindowInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityWindowInfo;->getLayer()I

    move-result v0

    .line 134
    return v0
.end method

.method public final d(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 139
    .line 1045
    check-cast p1, Landroid/view/accessibility/AccessibilityWindowInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityWindowInfo;->getRoot()Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v0

    .line 139
    return-object v0
.end method

.method public final e(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 144
    .line 1049
    check-cast p1, Landroid/view/accessibility/AccessibilityWindowInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityWindowInfo;->getParent()Landroid/view/accessibility/AccessibilityWindowInfo;

    move-result-object v0

    .line 144
    return-object v0
.end method

.method public final f(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 149
    .line 1053
    check-cast p1, Landroid/view/accessibility/AccessibilityWindowInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityWindowInfo;->getId()I

    move-result v0

    .line 149
    return v0
.end method

.method public final g(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 159
    .line 1061
    check-cast p1, Landroid/view/accessibility/AccessibilityWindowInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityWindowInfo;->isActive()Z

    move-result v0

    .line 159
    return v0
.end method

.method public final h(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 164
    .line 1065
    check-cast p1, Landroid/view/accessibility/AccessibilityWindowInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityWindowInfo;->isFocused()Z

    move-result v0

    .line 164
    return v0
.end method

.method public final i(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 169
    .line 1069
    check-cast p1, Landroid/view/accessibility/AccessibilityWindowInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityWindowInfo;->isAccessibilityFocused()Z

    move-result v0

    .line 169
    return v0
.end method

.method public final j(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 174
    .line 1073
    check-cast p1, Landroid/view/accessibility/AccessibilityWindowInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityWindowInfo;->getChildCount()I

    move-result v0

    .line 174
    return v0
.end method

.method public final k(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 184
    .line 1081
    check-cast p1, Landroid/view/accessibility/AccessibilityWindowInfo;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityWindowInfo;->recycle()V

    .line 185
    return-void
.end method
