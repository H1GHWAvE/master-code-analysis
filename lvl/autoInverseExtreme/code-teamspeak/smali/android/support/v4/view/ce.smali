.class final Landroid/support/v4/view/ce;
.super Landroid/database/DataSetObserver;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/view/eu;
.implements Landroid/support/v4/view/ev;


# instance fields
.field final synthetic a:Landroid/support/v4/view/cc;

.field private b:I


# direct methods
.method private constructor <init>(Landroid/support/v4/view/cc;)V
    .registers 2

    .prologue
    .line 474
    iput-object p1, p0, Landroid/support/v4/view/ce;->a:Landroid/support/v4/view/cc;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v4/view/cc;B)V
    .registers 3

    .prologue
    .line 474
    invoke-direct {p0, p1}, Landroid/support/v4/view/ce;-><init>(Landroid/support/v4/view/cc;)V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .registers 2

    .prologue
    .line 500
    iput p1, p0, Landroid/support/v4/view/ce;->b:I

    .line 501
    return-void
.end method

.method public final a(IF)V
    .registers 5

    .prologue
    .line 480
    const/high16 v0, 0x3f000000    # 0.5f

    cmpl-float v0, p2, v0

    if-lez v0, :cond_8

    .line 482
    add-int/lit8 p1, p1, 0x1

    .line 484
    :cond_8
    iget-object v0, p0, Landroid/support/v4/view/ce;->a:Landroid/support/v4/view/cc;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Landroid/support/v4/view/cc;->a(IFZ)V

    .line 485
    return-void
.end method

.method public final a(Landroid/support/v4/view/by;Landroid/support/v4/view/by;)V
    .registers 4

    .prologue
    .line 505
    iget-object v0, p0, Landroid/support/v4/view/ce;->a:Landroid/support/v4/view/cc;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/view/cc;->a(Landroid/support/v4/view/by;Landroid/support/v4/view/by;)V

    .line 506
    return-void
.end method

.method public final b(I)V
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 489
    iget v1, p0, Landroid/support/v4/view/ce;->b:I

    if-nez v1, :cond_37

    .line 491
    iget-object v1, p0, Landroid/support/v4/view/ce;->a:Landroid/support/v4/view/cc;

    iget-object v2, p0, Landroid/support/v4/view/ce;->a:Landroid/support/v4/view/cc;

    iget-object v2, v2, Landroid/support/v4/view/cc;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    iget-object v3, p0, Landroid/support/v4/view/ce;->a:Landroid/support/v4/view/cc;

    iget-object v3, v3, Landroid/support/v4/view/cc;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/by;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/cc;->a(I)V

    .line 493
    iget-object v1, p0, Landroid/support/v4/view/ce;->a:Landroid/support/v4/view/cc;

    invoke-static {v1}, Landroid/support/v4/view/cc;->a(Landroid/support/v4/view/cc;)F

    move-result v1

    cmpl-float v1, v1, v0

    if-ltz v1, :cond_29

    iget-object v0, p0, Landroid/support/v4/view/ce;->a:Landroid/support/v4/view/cc;

    invoke-static {v0}, Landroid/support/v4/view/cc;->a(Landroid/support/v4/view/cc;)F

    move-result v0

    .line 494
    :cond_29
    iget-object v1, p0, Landroid/support/v4/view/ce;->a:Landroid/support/v4/view/cc;

    iget-object v2, p0, Landroid/support/v4/view/ce;->a:Landroid/support/v4/view/cc;

    iget-object v2, v2, Landroid/support/v4/view/cc;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/view/cc;->a(IFZ)V

    .line 496
    :cond_37
    return-void
.end method

.method public final onChanged()V
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 510
    iget-object v1, p0, Landroid/support/v4/view/ce;->a:Landroid/support/v4/view/cc;

    iget-object v2, p0, Landroid/support/v4/view/ce;->a:Landroid/support/v4/view/cc;

    iget-object v2, v2, Landroid/support/v4/view/cc;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    iget-object v3, p0, Landroid/support/v4/view/ce;->a:Landroid/support/v4/view/cc;

    iget-object v3, v3, Landroid/support/v4/view/cc;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/by;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/cc;->a(I)V

    .line 512
    iget-object v1, p0, Landroid/support/v4/view/ce;->a:Landroid/support/v4/view/cc;

    invoke-static {v1}, Landroid/support/v4/view/cc;->a(Landroid/support/v4/view/cc;)F

    move-result v1

    cmpl-float v1, v1, v0

    if-ltz v1, :cond_25

    iget-object v0, p0, Landroid/support/v4/view/ce;->a:Landroid/support/v4/view/cc;

    invoke-static {v0}, Landroid/support/v4/view/cc;->a(Landroid/support/v4/view/cc;)F

    move-result v0

    .line 513
    :cond_25
    iget-object v1, p0, Landroid/support/v4/view/ce;->a:Landroid/support/v4/view/cc;

    iget-object v2, p0, Landroid/support/v4/view/ce;->a:Landroid/support/v4/view/cc;

    iget-object v2, v2, Landroid/support/v4/view/cc;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/view/cc;->a(IFZ)V

    .line 514
    return-void
.end method
