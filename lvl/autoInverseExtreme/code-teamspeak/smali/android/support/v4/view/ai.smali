.class public final Landroid/support/v4/view/ai;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Landroid/support/v4/view/aj;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 55
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 56
    const/16 v1, 0x15

    if-lt v0, v1, :cond_e

    .line 57
    new-instance v0, Landroid/support/v4/view/am;

    invoke-direct {v0}, Landroid/support/v4/view/am;-><init>()V

    sput-object v0, Landroid/support/v4/view/ai;->a:Landroid/support/v4/view/aj;

    .line 63
    :goto_d
    return-void

    .line 58
    :cond_e
    const/16 v1, 0xb

    if-lt v0, v1, :cond_1a

    .line 59
    new-instance v0, Landroid/support/v4/view/al;

    invoke-direct {v0}, Landroid/support/v4/view/al;-><init>()V

    sput-object v0, Landroid/support/v4/view/ai;->a:Landroid/support/v4/view/aj;

    goto :goto_d

    .line 61
    :cond_1a
    new-instance v0, Landroid/support/v4/view/ak;

    invoke-direct {v0}, Landroid/support/v4/view/ak;-><init>()V

    sput-object v0, Landroid/support/v4/view/ai;->a:Landroid/support/v4/view/aj;

    goto :goto_d
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    return-void
.end method

.method public static a(Landroid/view/LayoutInflater;Landroid/support/v4/view/as;)V
    .registers 3

    .prologue
    .line 79
    sget-object v0, Landroid/support/v4/view/ai;->a:Landroid/support/v4/view/aj;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/view/aj;->a(Landroid/view/LayoutInflater;Landroid/support/v4/view/as;)V

    .line 80
    return-void
.end method
