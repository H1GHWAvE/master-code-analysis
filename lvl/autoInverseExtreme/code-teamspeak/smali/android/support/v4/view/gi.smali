.class final Landroid/support/v4/view/gi;
.super Landroid/support/v4/view/gh;
.source "SourceFile"


# instance fields
.field final a:Landroid/view/WindowInsets;


# direct methods
.method constructor <init>(Landroid/view/WindowInsets;)V
    .registers 2

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/support/v4/view/gh;-><init>()V

    .line 27
    iput-object p1, p0, Landroid/support/v4/view/gi;->a:Landroid/view/WindowInsets;

    .line 28
    return-void
.end method

.method private p()Landroid/view/WindowInsets;
    .registers 2

    .prologue
    .line 116
    iget-object v0, p0, Landroid/support/v4/view/gi;->a:Landroid/view/WindowInsets;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 32
    iget-object v0, p0, Landroid/support/v4/view/gi;->a:Landroid/view/WindowInsets;

    invoke-virtual {v0}, Landroid/view/WindowInsets;->getSystemWindowInsetLeft()I

    move-result v0

    return v0
.end method

.method public final a(IIII)Landroid/support/v4/view/gh;
    .registers 7

    .prologue
    .line 77
    new-instance v0, Landroid/support/v4/view/gi;

    iget-object v1, p0, Landroid/support/v4/view/gi;->a:Landroid/view/WindowInsets;

    invoke-virtual {v1, p1, p2, p3, p4}, Landroid/view/WindowInsets;->replaceSystemWindowInsets(IIII)Landroid/view/WindowInsets;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/view/gi;-><init>(Landroid/view/WindowInsets;)V

    return-object v0
.end method

.method public final a(Landroid/graphics/Rect;)Landroid/support/v4/view/gh;
    .registers 4

    .prologue
    .line 82
    new-instance v0, Landroid/support/v4/view/gi;

    iget-object v1, p0, Landroid/support/v4/view/gi;->a:Landroid/view/WindowInsets;

    invoke-virtual {v1, p1}, Landroid/view/WindowInsets;->replaceSystemWindowInsets(Landroid/graphics/Rect;)Landroid/view/WindowInsets;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/view/gi;-><init>(Landroid/view/WindowInsets;)V

    return-object v0
.end method

.method public final b()I
    .registers 2

    .prologue
    .line 37
    iget-object v0, p0, Landroid/support/v4/view/gi;->a:Landroid/view/WindowInsets;

    invoke-virtual {v0}, Landroid/view/WindowInsets;->getSystemWindowInsetTop()I

    move-result v0

    return v0
.end method

.method public final c()I
    .registers 2

    .prologue
    .line 42
    iget-object v0, p0, Landroid/support/v4/view/gi;->a:Landroid/view/WindowInsets;

    invoke-virtual {v0}, Landroid/view/WindowInsets;->getSystemWindowInsetRight()I

    move-result v0

    return v0
.end method

.method public final d()I
    .registers 2

    .prologue
    .line 47
    iget-object v0, p0, Landroid/support/v4/view/gi;->a:Landroid/view/WindowInsets;

    invoke-virtual {v0}, Landroid/view/WindowInsets;->getSystemWindowInsetBottom()I

    move-result v0

    return v0
.end method

.method public final e()Z
    .registers 2

    .prologue
    .line 52
    iget-object v0, p0, Landroid/support/v4/view/gi;->a:Landroid/view/WindowInsets;

    invoke-virtual {v0}, Landroid/view/WindowInsets;->hasSystemWindowInsets()Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .registers 2

    .prologue
    .line 57
    iget-object v0, p0, Landroid/support/v4/view/gi;->a:Landroid/view/WindowInsets;

    invoke-virtual {v0}, Landroid/view/WindowInsets;->hasInsets()Z

    move-result v0

    return v0
.end method

.method public final g()Z
    .registers 2

    .prologue
    .line 62
    iget-object v0, p0, Landroid/support/v4/view/gi;->a:Landroid/view/WindowInsets;

    invoke-virtual {v0}, Landroid/view/WindowInsets;->isConsumed()Z

    move-result v0

    return v0
.end method

.method public final h()Z
    .registers 2

    .prologue
    .line 67
    iget-object v0, p0, Landroid/support/v4/view/gi;->a:Landroid/view/WindowInsets;

    invoke-virtual {v0}, Landroid/view/WindowInsets;->isRound()Z

    move-result v0

    return v0
.end method

.method public final i()Landroid/support/v4/view/gh;
    .registers 3

    .prologue
    .line 72
    new-instance v0, Landroid/support/v4/view/gi;

    iget-object v1, p0, Landroid/support/v4/view/gi;->a:Landroid/view/WindowInsets;

    invoke-virtual {v1}, Landroid/view/WindowInsets;->consumeSystemWindowInsets()Landroid/view/WindowInsets;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/view/gi;-><init>(Landroid/view/WindowInsets;)V

    return-object v0
.end method

.method public final j()I
    .registers 2

    .prologue
    .line 87
    iget-object v0, p0, Landroid/support/v4/view/gi;->a:Landroid/view/WindowInsets;

    invoke-virtual {v0}, Landroid/view/WindowInsets;->getStableInsetTop()I

    move-result v0

    return v0
.end method

.method public final k()I
    .registers 2

    .prologue
    .line 92
    iget-object v0, p0, Landroid/support/v4/view/gi;->a:Landroid/view/WindowInsets;

    invoke-virtual {v0}, Landroid/view/WindowInsets;->getStableInsetLeft()I

    move-result v0

    return v0
.end method

.method public final l()I
    .registers 2

    .prologue
    .line 97
    iget-object v0, p0, Landroid/support/v4/view/gi;->a:Landroid/view/WindowInsets;

    invoke-virtual {v0}, Landroid/view/WindowInsets;->getStableInsetRight()I

    move-result v0

    return v0
.end method

.method public final m()I
    .registers 2

    .prologue
    .line 102
    iget-object v0, p0, Landroid/support/v4/view/gi;->a:Landroid/view/WindowInsets;

    invoke-virtual {v0}, Landroid/view/WindowInsets;->getStableInsetBottom()I

    move-result v0

    return v0
.end method

.method public final n()Z
    .registers 2

    .prologue
    .line 107
    iget-object v0, p0, Landroid/support/v4/view/gi;->a:Landroid/view/WindowInsets;

    invoke-virtual {v0}, Landroid/view/WindowInsets;->hasStableInsets()Z

    move-result v0

    return v0
.end method

.method public final o()Landroid/support/v4/view/gh;
    .registers 3

    .prologue
    .line 112
    new-instance v0, Landroid/support/v4/view/gi;

    iget-object v1, p0, Landroid/support/v4/view/gi;->a:Landroid/view/WindowInsets;

    invoke-virtual {v1}, Landroid/view/WindowInsets;->consumeStableInsets()Landroid/view/WindowInsets;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/view/gi;-><init>(Landroid/view/WindowInsets;)V

    return-object v0
.end method
