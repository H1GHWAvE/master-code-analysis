.class Landroid/support/v4/view/ad;
.super Landroid/support/v4/view/ac;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 118
    invoke-direct {p0}, Landroid/support/v4/view/ac;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 131
    .line 2025
    invoke-virtual {p1}, Landroid/view/View;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v0

    .line 131
    return-object v0
.end method

.method public final a(Landroid/view/KeyEvent;)V
    .registers 2

    .prologue
    .line 121
    .line 1034
    invoke-virtual {p1}, Landroid/view/KeyEvent;->startTracking()V

    .line 122
    return-void
.end method

.method public final a(Landroid/view/KeyEvent;Landroid/view/KeyEvent$Callback;Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 6

    .prologue
    .line 137
    .line 2030
    check-cast p3, Landroid/view/KeyEvent$DispatcherState;

    invoke-virtual {p1, p2, p3, p4}, Landroid/view/KeyEvent;->dispatch(Landroid/view/KeyEvent$Callback;Landroid/view/KeyEvent$DispatcherState;Ljava/lang/Object;)Z

    move-result v0

    .line 137
    return v0
.end method

.method public final b(Landroid/view/KeyEvent;)Z
    .registers 3

    .prologue
    .line 126
    .line 1038
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isTracking()Z

    move-result v0

    .line 126
    return v0
.end method
