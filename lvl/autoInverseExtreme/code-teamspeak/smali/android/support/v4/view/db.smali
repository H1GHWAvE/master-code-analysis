.class Landroid/support/v4/view/db;
.super Landroid/support/v4/view/da;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 991
    invoke-direct {p0}, Landroid/support/v4/view/da;-><init>()V

    return-void
.end method


# virtual methods
.method public final A(Landroid/view/View;)F
    .registers 3

    .prologue
    .line 1100
    .line 5073
    invoke-virtual {p1}, Landroid/view/View;->getRotation()F

    move-result v0

    .line 1100
    return v0
.end method

.method public final B(Landroid/view/View;)F
    .registers 3

    .prologue
    .line 1105
    .line 5077
    invoke-virtual {p1}, Landroid/view/View;->getRotationX()F

    move-result v0

    .line 1105
    return v0
.end method

.method public final C(Landroid/view/View;)F
    .registers 3

    .prologue
    .line 1110
    .line 5081
    invoke-virtual {p1}, Landroid/view/View;->getRotationY()F

    move-result v0

    .line 1110
    return v0
.end method

.method public final D(Landroid/view/View;)F
    .registers 3

    .prologue
    .line 1115
    .line 5085
    invoke-virtual {p1}, Landroid/view/View;->getScaleX()F

    move-result v0

    .line 1115
    return v0
.end method

.method public final E(Landroid/view/View;)F
    .registers 3

    .prologue
    .line 1120
    .line 5089
    invoke-virtual {p1}, Landroid/view/View;->getScaleY()F

    move-result v0

    .line 1120
    return v0
.end method

.method public final I(Landroid/view/View;)F
    .registers 3

    .prologue
    .line 1125
    .line 5141
    invoke-virtual {p1}, Landroid/view/View;->getPivotX()F

    move-result v0

    .line 1125
    return v0
.end method

.method public final J(Landroid/view/View;)F
    .registers 3

    .prologue
    .line 1129
    .line 5145
    invoke-virtual {p1}, Landroid/view/View;->getPivotY()F

    move-result v0

    .line 1129
    return v0
.end method

.method public final R(Landroid/view/View;)V
    .registers 2

    .prologue
    .line 1133
    .line 5149
    invoke-virtual {p1}, Landroid/view/View;->jumpDrawablesToCurrentState()V

    .line 1134
    return-void
.end method

.method public final S(Landroid/view/View;)V
    .registers 3

    .prologue
    .line 1138
    .line 5153
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setSaveFromParentEnabled(Z)V

    .line 1139
    return-void
.end method

.method public final a(II)I
    .registers 4

    .prologue
    .line 1148
    .line 5161
    invoke-static {p1, p2}, Landroid/view/View;->combineMeasuredStates(II)I

    move-result v0

    .line 1148
    return v0
.end method

.method public final a(III)I
    .registers 5

    .prologue
    .line 1018
    .line 4041
    invoke-static {p1, p2, p3}, Landroid/view/View;->resolveSizeAndState(III)I

    move-result v0

    .line 1018
    return v0
.end method

.method final a()J
    .registers 3

    .prologue
    .line 2025
    invoke-static {}, Landroid/animation/ValueAnimator;->getFrameDelay()J

    move-result-wide v0

    .line 994
    return-wide v0
.end method

.method public final a(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 1062
    .line 4113
    invoke-virtual {p1, p2}, Landroid/view/View;->setRotation(F)V

    .line 1063
    return-void
.end method

.method public final a(Landroid/view/View;ILandroid/graphics/Paint;)V
    .registers 4

    .prologue
    .line 1002
    .line 2033
    invoke-virtual {p1, p2, p3}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 1003
    return-void
.end method

.method public a(Landroid/view/View;Landroid/graphics/Paint;)V
    .registers 4

    .prologue
    .line 1012
    .line 3037
    invoke-virtual {p1}, Landroid/view/View;->getLayerType()I

    move-result v0

    .line 4033
    invoke-virtual {p1, v0, p2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 1014
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 1015
    return-void
.end method

.method public final b(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 1042
    .line 4093
    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationX(F)V

    .line 1043
    return-void
.end method

.method public final c(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 1046
    .line 4097
    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationY(F)V

    .line 1047
    return-void
.end method

.method public final c(Landroid/view/View;Z)V
    .registers 3

    .prologue
    .line 1143
    .line 5157
    invoke-virtual {p1, p2}, Landroid/view/View;->setActivated(Z)V

    .line 1144
    return-void
.end method

.method public final d(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 1050
    .line 4101
    invoke-virtual {p1, p2}, Landroid/view/View;->setAlpha(F)V

    .line 1051
    return-void
.end method

.method public final e(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 1066
    .line 4117
    invoke-virtual {p1, p2}, Landroid/view/View;->setRotationX(F)V

    .line 1067
    return-void
.end method

.method public final f(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 1070
    .line 4121
    invoke-virtual {p1, p2}, Landroid/view/View;->setRotationY(F)V

    .line 1071
    return-void
.end method

.method public final g(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 1074
    .line 4125
    invoke-virtual {p1, p2}, Landroid/view/View;->setScaleX(F)V

    .line 1075
    return-void
.end method

.method public final h(Landroid/view/View;)F
    .registers 3

    .prologue
    .line 998
    .line 2029
    invoke-virtual {p1}, Landroid/view/View;->getAlpha()F

    move-result v0

    .line 998
    return v0
.end method

.method public final h(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 1078
    .line 4129
    invoke-virtual {p1, p2}, Landroid/view/View;->setScaleY(F)V

    .line 1079
    return-void
.end method

.method public final i(Landroid/view/View;)I
    .registers 3

    .prologue
    .line 1006
    .line 2037
    invoke-virtual {p1}, Landroid/view/View;->getLayerType()I

    move-result v0

    .line 1006
    return v0
.end method

.method public final i(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 1054
    .line 4105
    invoke-virtual {p1, p2}, Landroid/view/View;->setX(F)V

    .line 1055
    return-void
.end method

.method public final j(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 1058
    .line 4109
    invoke-virtual {p1, p2}, Landroid/view/View;->setY(F)V

    .line 1059
    return-void
.end method

.method public final k(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 1082
    .line 4133
    invoke-virtual {p1, p2}, Landroid/view/View;->setPivotX(F)V

    .line 1083
    return-void
.end method

.method public final l(Landroid/view/View;F)V
    .registers 3

    .prologue
    .line 1086
    .line 4137
    invoke-virtual {p1, p2}, Landroid/view/View;->setPivotY(F)V

    .line 1087
    return-void
.end method

.method public final n(Landroid/view/View;)I
    .registers 3

    .prologue
    .line 1022
    .line 4045
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidthAndState()I

    move-result v0

    .line 1022
    return v0
.end method

.method public final o(Landroid/view/View;)I
    .registers 3

    .prologue
    .line 1026
    .line 4049
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeightAndState()I

    move-result v0

    .line 1026
    return v0
.end method

.method public final p(Landroid/view/View;)I
    .registers 3

    .prologue
    .line 1030
    .line 4053
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredState()I

    move-result v0

    .line 1030
    return v0
.end method

.method public final w(Landroid/view/View;)F
    .registers 3

    .prologue
    .line 1034
    .line 4057
    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v0

    .line 1034
    return v0
.end method

.method public final x(Landroid/view/View;)F
    .registers 3

    .prologue
    .line 1038
    .line 4061
    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    move-result v0

    .line 1038
    return v0
.end method

.method public final y(Landroid/view/View;)F
    .registers 3

    .prologue
    .line 1090
    .line 5065
    invoke-virtual {p1}, Landroid/view/View;->getX()F

    move-result v0

    .line 1090
    return v0
.end method

.method public final z(Landroid/view/View;)F
    .registers 3

    .prologue
    .line 1095
    .line 5069
    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v0

    .line 1095
    return v0
.end method
