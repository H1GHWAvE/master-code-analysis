.class public final Landroid/support/v4/media/session/MediaSessionCompat;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:I = 0x1

.field public static final b:I = 0x2


# instance fields
.field private final c:Landroid/support/v4/media/session/ag;

.field private final d:Landroid/support/v4/media/session/g;

.field private final e:Ljava/util/ArrayList;


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/support/v4/media/session/ag;)V
    .registers 4

    .prologue
    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/media/session/MediaSessionCompat;->e:Ljava/util/ArrayList;

    .line 144
    iput-object p2, p0, Landroid/support/v4/media/session/MediaSessionCompat;->c:Landroid/support/v4/media/session/ag;

    .line 145
    new-instance v0, Landroid/support/v4/media/session/g;

    invoke-direct {v0, p1, p0}, Landroid/support/v4/media/session/g;-><init>(Landroid/content/Context;Landroid/support/v4/media/session/MediaSessionCompat;)V

    iput-object v0, p0, Landroid/support/v4/media/session/MediaSessionCompat;->d:Landroid/support/v4/media/session/g;

    .line 146
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/content/ComponentName;Landroid/app/PendingIntent;)V
    .registers 8

    .prologue
    const/4 v2, 0x0

    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/media/session/MediaSessionCompat;->e:Ljava/util/ArrayList;

    .line 119
    if-nez p1, :cond_15

    .line 120
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "context must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 122
    :cond_15
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 123
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "tag must not be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 126
    :cond_23
    if-eqz p3, :cond_35

    if-nez p4, :cond_35

    .line 128
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MEDIA_BUTTON"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 130
    invoke-virtual {v0, p3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 131
    invoke-static {p1, v2, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object p4

    .line 134
    :cond_35
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_4f

    .line 135
    new-instance v0, Landroid/support/v4/media/session/ah;

    invoke-direct {v0, p1, p2}, Landroid/support/v4/media/session/ah;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Landroid/support/v4/media/session/MediaSessionCompat;->c:Landroid/support/v4/media/session/ag;

    .line 136
    iget-object v0, p0, Landroid/support/v4/media/session/MediaSessionCompat;->c:Landroid/support/v4/media/session/ag;

    invoke-interface {v0, p4}, Landroid/support/v4/media/session/ag;->b(Landroid/app/PendingIntent;)V

    .line 140
    :goto_47
    new-instance v0, Landroid/support/v4/media/session/g;

    invoke-direct {v0, p1, p0}, Landroid/support/v4/media/session/g;-><init>(Landroid/content/Context;Landroid/support/v4/media/session/MediaSessionCompat;)V

    iput-object v0, p0, Landroid/support/v4/media/session/MediaSessionCompat;->d:Landroid/support/v4/media/session/g;

    .line 141
    return-void

    .line 138
    :cond_4f
    new-instance v0, Landroid/support/v4/media/session/ai;

    invoke-direct {v0, p1, p2, p3, p4}, Landroid/support/v4/media/session/ai;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/content/ComponentName;Landroid/app/PendingIntent;)V

    iput-object v0, p0, Landroid/support/v4/media/session/MediaSessionCompat;->c:Landroid/support/v4/media/session/ag;

    goto :goto_47
.end method

.method private static a(Landroid/content/Context;Ljava/lang/Object;)Landroid/support/v4/media/session/MediaSessionCompat;
    .registers 4

    .prologue
    .line 463
    new-instance v0, Landroid/support/v4/media/session/MediaSessionCompat;

    new-instance v1, Landroid/support/v4/media/session/ah;

    invoke-direct {v1, p1}, Landroid/support/v4/media/session/ah;-><init>(Ljava/lang/Object;)V

    invoke-direct {v0, p0, v1}, Landroid/support/v4/media/session/MediaSessionCompat;-><init>(Landroid/content/Context;Landroid/support/v4/media/session/ag;)V

    return-object v0
.end method

.method private a(I)V
    .registers 3

    .prologue
    .line 206
    iget-object v0, p0, Landroid/support/v4/media/session/MediaSessionCompat;->c:Landroid/support/v4/media/session/ag;

    invoke-interface {v0, p1}, Landroid/support/v4/media/session/ag;->a(I)V

    .line 207
    return-void
.end method

.method private a(Landroid/app/PendingIntent;)V
    .registers 3

    .prologue
    .line 180
    iget-object v0, p0, Landroid/support/v4/media/session/MediaSessionCompat;->c:Landroid/support/v4/media/session/ag;

    invoke-interface {v0, p1}, Landroid/support/v4/media/session/ag;->a(Landroid/app/PendingIntent;)V

    .line 181
    return-void
.end method

.method private a(Landroid/os/Bundle;)V
    .registers 3

    .prologue
    .line 397
    iget-object v0, p0, Landroid/support/v4/media/session/MediaSessionCompat;->c:Landroid/support/v4/media/session/ag;

    invoke-interface {v0, p1}, Landroid/support/v4/media/session/ag;->a(Landroid/os/Bundle;)V

    .line 398
    return-void
.end method

.method private a(Landroid/support/v4/media/MediaMetadataCompat;)V
    .registers 3

    .prologue
    .line 342
    iget-object v0, p0, Landroid/support/v4/media/session/MediaSessionCompat;->c:Landroid/support/v4/media/session/ag;

    invoke-interface {v0, p1}, Landroid/support/v4/media/session/ag;->a(Landroid/support/v4/media/MediaMetadataCompat;)V

    .line 343
    return-void
.end method

.method private a(Landroid/support/v4/media/ae;)V
    .registers 4

    .prologue
    .line 239
    if-nez p1, :cond_a

    .line 240
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "volumeProvider may not be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 242
    :cond_a
    iget-object v0, p0, Landroid/support/v4/media/session/MediaSessionCompat;->c:Landroid/support/v4/media/session/ag;

    invoke-interface {v0, p1}, Landroid/support/v4/media/session/ag;->a(Landroid/support/v4/media/ae;)V

    .line 243
    return-void
.end method

.method private a(Landroid/support/v4/media/session/PlaybackStateCompat;)V
    .registers 3

    .prologue
    .line 332
    iget-object v0, p0, Landroid/support/v4/media/session/MediaSessionCompat;->c:Landroid/support/v4/media/session/ag;

    invoke-interface {v0, p1}, Landroid/support/v4/media/session/ag;->a(Landroid/support/v4/media/session/PlaybackStateCompat;)V

    .line 333
    return-void
.end method

.method private a(Landroid/support/v4/media/session/ad;)V
    .registers 4

    .prologue
    .line 156
    .line 1168
    iget-object v0, p0, Landroid/support/v4/media/session/MediaSessionCompat;->c:Landroid/support/v4/media/session/ag;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-interface {v0, p1, v1}, Landroid/support/v4/media/session/ag;->a(Landroid/support/v4/media/session/ad;Landroid/os/Handler;)V

    .line 157
    return-void
.end method

.method private a(Landroid/support/v4/media/session/ao;)V
    .registers 4

    .prologue
    .line 436
    if-nez p1, :cond_a

    .line 437
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Listener may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 439
    :cond_a
    iget-object v0, p0, Landroid/support/v4/media/session/MediaSessionCompat;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 440
    return-void
.end method

.method private a(Ljava/lang/CharSequence;)V
    .registers 3

    .prologue
    .line 368
    iget-object v0, p0, Landroid/support/v4/media/session/MediaSessionCompat;->c:Landroid/support/v4/media/session/ag;

    invoke-interface {v0, p1}, Landroid/support/v4/media/session/ag;->a(Ljava/lang/CharSequence;)V

    .line 369
    return-void
.end method

.method private a(Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 5

    .prologue
    .line 283
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 284
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "event cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 286
    :cond_e
    iget-object v0, p0, Landroid/support/v4/media/session/MediaSessionCompat;->c:Landroid/support/v4/media/session/ag;

    invoke-interface {v0, p1, p2}, Landroid/support/v4/media/session/ag;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 287
    return-void
.end method

.method private a(Ljava/util/List;)V
    .registers 3

    .prologue
    .line 357
    iget-object v0, p0, Landroid/support/v4/media/session/MediaSessionCompat;->c:Landroid/support/v4/media/session/ag;

    invoke-interface {v0, p1}, Landroid/support/v4/media/session/ag;->a(Ljava/util/List;)V

    .line 358
    return-void
.end method

.method private a(Z)V
    .registers 4

    .prologue
    .line 259
    iget-object v0, p0, Landroid/support/v4/media/session/MediaSessionCompat;->c:Landroid/support/v4/media/session/ag;

    invoke-interface {v0, p1}, Landroid/support/v4/media/session/ag;->a(Z)V

    .line 260
    iget-object v0, p0, Landroid/support/v4/media/session/MediaSessionCompat;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_15

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_b

    .line 263
    :cond_15
    return-void
.end method

.method private b(I)V
    .registers 3

    .prologue
    .line 220
    iget-object v0, p0, Landroid/support/v4/media/session/MediaSessionCompat;->c:Landroid/support/v4/media/session/ag;

    invoke-interface {v0, p1}, Landroid/support/v4/media/session/ag;->b(I)V

    .line 221
    return-void
.end method

.method private b(Landroid/app/PendingIntent;)V
    .registers 3

    .prologue
    .line 197
    iget-object v0, p0, Landroid/support/v4/media/session/MediaSessionCompat;->c:Landroid/support/v4/media/session/ag;

    invoke-interface {v0, p1}, Landroid/support/v4/media/session/ag;->b(Landroid/app/PendingIntent;)V

    .line 198
    return-void
.end method

.method private b(Landroid/support/v4/media/session/ad;)V
    .registers 4

    .prologue
    .line 168
    iget-object v0, p0, Landroid/support/v4/media/session/MediaSessionCompat;->c:Landroid/support/v4/media/session/ag;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-interface {v0, p1, v1}, Landroid/support/v4/media/session/ag;->a(Landroid/support/v4/media/session/ad;Landroid/os/Handler;)V

    .line 169
    return-void
.end method

.method private b(Landroid/support/v4/media/session/ao;)V
    .registers 4

    .prologue
    .line 449
    if-nez p1, :cond_a

    .line 450
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Listener may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 452
    :cond_a
    iget-object v0, p0, Landroid/support/v4/media/session/MediaSessionCompat;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 453
    return-void
.end method

.method private b()Z
    .registers 2

    .prologue
    .line 271
    iget-object v0, p0, Landroid/support/v4/media/session/MediaSessionCompat;->c:Landroid/support/v4/media/session/ag;

    invoke-interface {v0}, Landroid/support/v4/media/session/ag;->a()Z

    move-result v0

    return v0
.end method

.method private c()V
    .registers 2

    .prologue
    .line 295
    iget-object v0, p0, Landroid/support/v4/media/session/MediaSessionCompat;->c:Landroid/support/v4/media/session/ag;

    invoke-interface {v0}, Landroid/support/v4/media/session/ag;->b()V

    .line 296
    return-void
.end method

.method private c(I)V
    .registers 3

    .prologue
    .line 385
    iget-object v0, p0, Landroid/support/v4/media/session/MediaSessionCompat;->c:Landroid/support/v4/media/session/ag;

    invoke-interface {v0, p1}, Landroid/support/v4/media/session/ag;->c(I)V

    .line 386
    return-void
.end method

.method private d()Landroid/support/v4/media/session/g;
    .registers 2

    .prologue
    .line 323
    iget-object v0, p0, Landroid/support/v4/media/session/MediaSessionCompat;->d:Landroid/support/v4/media/session/g;

    return-object v0
.end method

.method private e()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 411
    iget-object v0, p0, Landroid/support/v4/media/session/MediaSessionCompat;->c:Landroid/support/v4/media/session/ag;

    invoke-interface {v0}, Landroid/support/v4/media/session/ag;->d()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private f()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 425
    iget-object v0, p0, Landroid/support/v4/media/session/MediaSessionCompat;->c:Landroid/support/v4/media/session/ag;

    invoke-interface {v0}, Landroid/support/v4/media/session/ag;->e()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/support/v4/media/session/MediaSessionCompat$Token;
    .registers 2

    .prologue
    .line 313
    iget-object v0, p0, Landroid/support/v4/media/session/MediaSessionCompat;->c:Landroid/support/v4/media/session/ag;

    invoke-interface {v0}, Landroid/support/v4/media/session/ag;->c()Landroid/support/v4/media/session/MediaSessionCompat$Token;

    move-result-object v0

    return-object v0
.end method
