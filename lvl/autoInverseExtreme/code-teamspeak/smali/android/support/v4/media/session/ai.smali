.class final Landroid/support/v4/media/session/ai;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/media/session/ag;


# instance fields
.field private A:Z

.field private B:Z

.field private C:Landroid/support/v4/media/ag;

.field final a:Landroid/support/v4/media/session/an;

.field final b:Ljava/lang/String;

.field final c:Ljava/lang/String;

.field final d:Landroid/media/AudioManager;

.field final e:Ljava/lang/Object;

.field final f:Landroid/os/RemoteCallbackList;

.field g:Z

.field h:Landroid/support/v4/media/session/ad;

.field i:I

.field j:Landroid/support/v4/media/MediaMetadataCompat;

.field k:Landroid/support/v4/media/session/PlaybackStateCompat;

.field l:Landroid/app/PendingIntent;

.field m:Ljava/util/List;

.field n:Ljava/lang/CharSequence;

.field o:I

.field p:Landroid/os/Bundle;

.field q:I

.field r:I

.field s:Landroid/support/v4/media/ae;

.field private final t:Landroid/content/Context;

.field private final u:Landroid/content/ComponentName;

.field private final v:Landroid/app/PendingIntent;

.field private final w:Ljava/lang/Object;

.field private final x:Landroid/support/v4/media/session/am;

.field private final y:Landroid/support/v4/media/session/MediaSessionCompat$Token;

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/content/ComponentName;Landroid/app/PendingIntent;)V
    .registers 8

    .prologue
    const/4 v2, 0x0

    .line 1014
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 975
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Landroid/support/v4/media/session/ai;->e:Ljava/lang/Object;

    .line 976
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    .line 979
    iput-boolean v2, p0, Landroid/support/v4/media/session/ai;->g:Z

    .line 980
    iput-boolean v2, p0, Landroid/support/v4/media/session/ai;->z:Z

    .line 981
    iput-boolean v2, p0, Landroid/support/v4/media/session/ai;->A:Z

    .line 982
    iput-boolean v2, p0, Landroid/support/v4/media/session/ai;->B:Z

    .line 999
    new-instance v0, Landroid/support/v4/media/session/aj;

    invoke-direct {v0, p0}, Landroid/support/v4/media/session/aj;-><init>(Landroid/support/v4/media/session/ai;)V

    iput-object v0, p0, Landroid/support/v4/media/session/ai;->C:Landroid/support/v4/media/ag;

    .line 1015
    if-nez p3, :cond_2b

    .line 1016
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "MediaButtonReceiver component may not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1019
    :cond_2b
    iput-object p1, p0, Landroid/support/v4/media/session/ai;->t:Landroid/content/Context;

    .line 1020
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/media/session/ai;->b:Ljava/lang/String;

    .line 1021
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Landroid/support/v4/media/session/ai;->d:Landroid/media/AudioManager;

    .line 1022
    iput-object p2, p0, Landroid/support/v4/media/session/ai;->c:Ljava/lang/String;

    .line 1023
    iput-object p3, p0, Landroid/support/v4/media/session/ai;->u:Landroid/content/ComponentName;

    .line 1024
    iput-object p4, p0, Landroid/support/v4/media/session/ai;->v:Landroid/app/PendingIntent;

    .line 1025
    new-instance v0, Landroid/support/v4/media/session/am;

    invoke-direct {v0, p0}, Landroid/support/v4/media/session/am;-><init>(Landroid/support/v4/media/session/ai;)V

    iput-object v0, p0, Landroid/support/v4/media/session/ai;->x:Landroid/support/v4/media/session/am;

    .line 1026
    new-instance v0, Landroid/support/v4/media/session/MediaSessionCompat$Token;

    iget-object v1, p0, Landroid/support/v4/media/session/ai;->x:Landroid/support/v4/media/session/am;

    invoke-direct {v0, v1}, Landroid/support/v4/media/session/MediaSessionCompat$Token;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Landroid/support/v4/media/session/ai;->y:Landroid/support/v4/media/session/MediaSessionCompat$Token;

    .line 1027
    new-instance v0, Landroid/support/v4/media/session/an;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Landroid/support/v4/media/session/an;-><init>(Landroid/support/v4/media/session/ai;Landroid/os/Looper;)V

    iput-object v0, p0, Landroid/support/v4/media/session/ai;->a:Landroid/support/v4/media/session/an;

    .line 1029
    iput v2, p0, Landroid/support/v4/media/session/ai;->o:I

    .line 1030
    const/4 v0, 0x1

    iput v0, p0, Landroid/support/v4/media/session/ai;->q:I

    .line 1031
    const/4 v0, 0x3

    iput v0, p0, Landroid/support/v4/media/session/ai;->r:I

    .line 1032
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_74

    .line 2074
    new-instance v0, Landroid/media/RemoteControlClient;

    invoke-direct {v0, p4}, Landroid/media/RemoteControlClient;-><init>(Landroid/app/PendingIntent;)V

    .line 1033
    iput-object v0, p0, Landroid/support/v4/media/session/ai;->w:Ljava/lang/Object;

    .line 1037
    :goto_73
    return-void

    .line 1035
    :cond_74
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/media/session/ai;->w:Ljava/lang/Object;

    goto :goto_73
.end method

.method private static synthetic a(Landroid/support/v4/media/session/ai;)Landroid/support/v4/media/ae;
    .registers 2

    .prologue
    .line 963
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->s:Landroid/support/v4/media/ae;

    return-object v0
.end method

.method private a(II)V
    .registers 5

    .prologue
    .line 1371
    iget v0, p0, Landroid/support/v4/media/session/ai;->q:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_a

    .line 1372
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->s:Landroid/support/v4/media/ae;

    if-eqz v0, :cond_9

    .line 1378
    :cond_9
    :goto_9
    return-void

    .line 1376
    :cond_a
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->d:Landroid/media/AudioManager;

    iget v1, p0, Landroid/support/v4/media/session/ai;->r:I

    invoke-virtual {v0, p1, v1, p2}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    goto :goto_9
.end method

.method static synthetic a(Landroid/support/v4/media/session/ai;II)V
    .registers 5

    .prologue
    .line 963
    .line 22371
    iget v0, p0, Landroid/support/v4/media/session/ai;->q:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_a

    .line 22372
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->s:Landroid/support/v4/media/ae;

    if-eqz v0, :cond_9

    .line 22373
    :cond_9
    :goto_9
    return-void

    .line 22376
    :cond_a
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->d:Landroid/media/AudioManager;

    iget v1, p0, Landroid/support/v4/media/session/ai;->r:I

    invoke-virtual {v0, p1, v1, p2}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    goto :goto_9
.end method

.method private static synthetic a(Landroid/support/v4/media/session/ai;Landroid/support/v4/media/session/ParcelableVolumeInfo;)V
    .registers 2

    .prologue
    .line 963
    invoke-virtual {p0, p1}, Landroid/support/v4/media/session/ai;->a(Landroid/support/v4/media/session/ParcelableVolumeInfo;)V

    return-void
.end method

.method private static synthetic b(Landroid/support/v4/media/session/ai;)I
    .registers 2

    .prologue
    .line 963
    iget v0, p0, Landroid/support/v4/media/session/ai;->q:I

    return v0
.end method

.method private b(II)V
    .registers 5

    .prologue
    .line 1381
    iget v0, p0, Landroid/support/v4/media/session/ai;->q:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_a

    .line 1382
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->s:Landroid/support/v4/media/ae;

    if-eqz v0, :cond_9

    .line 1388
    :cond_9
    :goto_9
    return-void

    .line 1386
    :cond_a
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->d:Landroid/media/AudioManager;

    iget v1, p0, Landroid/support/v4/media/session/ai;->r:I

    invoke-virtual {v0, v1, p1, p2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    goto :goto_9
.end method

.method private b(Landroid/support/v4/media/MediaMetadataCompat;)V
    .registers 4

    .prologue
    .line 1477
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    .line 1478
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_9
    if-ltz v1, :cond_1a

    .line 1479
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Landroid/support/v4/media/session/a;

    .line 1481
    :try_start_13
    invoke-interface {v0, p1}, Landroid/support/v4/media/session/a;->a(Landroid/support/v4/media/MediaMetadataCompat;)V
    :try_end_16
    .catch Landroid/os/RemoteException; {:try_start_13 .. :try_end_16} :catch_20

    .line 1478
    :goto_16
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_9

    .line 1485
    :cond_1a
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1486
    return-void

    :catch_20
    move-exception v0

    goto :goto_16
.end method

.method private b(Landroid/support/v4/media/session/PlaybackStateCompat;)V
    .registers 4

    .prologue
    .line 1465
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    .line 1466
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_9
    if-ltz v1, :cond_1a

    .line 1467
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Landroid/support/v4/media/session/a;

    .line 1469
    :try_start_13
    invoke-interface {v0, p1}, Landroid/support/v4/media/session/a;->a(Landroid/support/v4/media/session/PlaybackStateCompat;)V
    :try_end_16
    .catch Landroid/os/RemoteException; {:try_start_13 .. :try_end_16} :catch_20

    .line 1466
    :goto_16
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_9

    .line 1473
    :cond_1a
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1474
    return-void

    :catch_20
    move-exception v0

    goto :goto_16
.end method

.method static synthetic b(Landroid/support/v4/media/session/ai;II)V
    .registers 5

    .prologue
    .line 963
    .line 22381
    iget v0, p0, Landroid/support/v4/media/session/ai;->q:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_a

    .line 22382
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->s:Landroid/support/v4/media/ae;

    if-eqz v0, :cond_9

    .line 22383
    :cond_9
    :goto_9
    return-void

    .line 22386
    :cond_a
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->d:Landroid/media/AudioManager;

    iget v1, p0, Landroid/support/v4/media/session/ai;->r:I

    invoke-virtual {v0, v1, p1, p2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    goto :goto_9
.end method

.method private b(Ljava/lang/CharSequence;)V
    .registers 4

    .prologue
    .line 1501
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    .line 1502
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_9
    if-ltz v1, :cond_1a

    .line 1503
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Landroid/support/v4/media/session/a;

    .line 1505
    :try_start_13
    invoke-interface {v0, p1}, Landroid/support/v4/media/session/a;->a(Ljava/lang/CharSequence;)V
    :try_end_16
    .catch Landroid/os/RemoteException; {:try_start_13 .. :try_end_16} :catch_20

    .line 1502
    :goto_16
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_9

    .line 1509
    :cond_1a
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1510
    return-void

    :catch_20
    move-exception v0

    goto :goto_16
.end method

.method private b(Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 5

    .prologue
    .line 1453
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    .line 1454
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_9
    if-ltz v1, :cond_1a

    .line 1455
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Landroid/support/v4/media/session/a;

    .line 1457
    :try_start_13
    invoke-interface {v0, p1, p2}, Landroid/support/v4/media/session/a;->a(Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_16
    .catch Landroid/os/RemoteException; {:try_start_13 .. :try_end_16} :catch_20

    .line 1454
    :goto_16
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_9

    .line 1461
    :cond_1a
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1462
    return-void

    :catch_20
    move-exception v0

    goto :goto_16
.end method

.method private b(Ljava/util/List;)V
    .registers 4

    .prologue
    .line 1489
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    .line 1490
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_9
    if-ltz v1, :cond_1a

    .line 1491
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Landroid/support/v4/media/session/a;

    .line 1493
    :try_start_13
    invoke-interface {v0, p1}, Landroid/support/v4/media/session/a;->a(Ljava/util/List;)V
    :try_end_16
    .catch Landroid/os/RemoteException; {:try_start_13 .. :try_end_16} :catch_20

    .line 1490
    :goto_16
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_9

    .line 1497
    :cond_1a
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1498
    return-void

    :catch_20
    move-exception v0

    goto :goto_16
.end method

.method private static synthetic c(Landroid/support/v4/media/session/ai;)I
    .registers 2

    .prologue
    .line 963
    iget v0, p0, Landroid/support/v4/media/session/ai;->r:I

    return v0
.end method

.method private static synthetic d(Landroid/support/v4/media/session/ai;)Landroid/support/v4/media/session/an;
    .registers 2

    .prologue
    .line 963
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->a:Landroid/support/v4/media/session/an;

    return-object v0
.end method

.method private static synthetic e(Landroid/support/v4/media/session/ai;)I
    .registers 2

    .prologue
    .line 963
    iget v0, p0, Landroid/support/v4/media/session/ai;->i:I

    return v0
.end method

.method private static synthetic f(Landroid/support/v4/media/session/ai;)Z
    .registers 2

    .prologue
    .line 963
    iget-boolean v0, p0, Landroid/support/v4/media/session/ai;->g:Z

    return v0
.end method

.method private static synthetic g(Landroid/support/v4/media/session/ai;)Landroid/os/RemoteCallbackList;
    .registers 2

    .prologue
    .line 963
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    return-object v0
.end method

.method private g()Z
    .registers 6

    .prologue
    const/16 v4, 0x12

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1304
    .line 1305
    iget-boolean v0, p0, Landroid/support/v4/media/session/ai;->z:Z

    if-eqz v0, :cond_9c

    .line 1308
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x8

    if-lt v0, v3, :cond_2d

    .line 1309
    iget-boolean v0, p0, Landroid/support/v4/media/session/ai;->B:Z

    if-nez v0, :cond_62

    iget v0, p0, Landroid/support/v4/media/session/ai;->i:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_62

    .line 1310
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v4, :cond_52

    .line 1311
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->t:Landroid/content/Context;

    iget-object v3, p0, Landroid/support/v4/media/session/ai;->v:Landroid/app/PendingIntent;

    .line 15034
    const-string v4, "audio"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 15035
    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->registerMediaButtonEventReceiver(Landroid/app/PendingIntent;)V

    .line 1317
    :goto_2b
    iput-boolean v2, p0, Landroid/support/v4/media/session/ai;->B:Z

    .line 1331
    :cond_2d
    :goto_2d
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v0, v3, :cond_bf

    .line 1332
    iget-boolean v0, p0, Landroid/support/v4/media/session/ai;->A:Z

    if-nez v0, :cond_82

    iget v0, p0, Landroid/support/v4/media/session/ai;->i:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_82

    .line 1333
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->t:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v4/media/session/ai;->w:Ljava/lang/Object;

    .line 16094
    const-string v3, "audio"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 16095
    check-cast v1, Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->registerRemoteControlClient(Landroid/media/RemoteControlClient;)V

    .line 1334
    iput-boolean v2, p0, Landroid/support/v4/media/session/ai;->A:Z

    move v0, v2

    .line 1367
    :goto_51
    return v0

    .line 1314
    :cond_52
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->t:Landroid/content/Context;

    iget-object v3, p0, Landroid/support/v4/media/session/ai;->u:Landroid/content/ComponentName;

    .line 16024
    const-string v4, "audio"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 16025
    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->registerMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    goto :goto_2b

    .line 1318
    :cond_62
    iget-boolean v0, p0, Landroid/support/v4/media/session/ai;->B:Z

    if-eqz v0, :cond_2d

    iget v0, p0, Landroid/support/v4/media/session/ai;->i:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_2d

    .line 1319
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v4, :cond_7a

    .line 1320
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->t:Landroid/content/Context;

    iget-object v3, p0, Landroid/support/v4/media/session/ai;->v:Landroid/app/PendingIntent;

    invoke-static {v0, v3}, Landroid/support/v4/media/session/av;->a(Landroid/content/Context;Landroid/app/PendingIntent;)V

    .line 1326
    :goto_77
    iput-boolean v1, p0, Landroid/support/v4/media/session/ai;->B:Z

    goto :goto_2d

    .line 1323
    :cond_7a
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->t:Landroid/content/Context;

    iget-object v3, p0, Landroid/support/v4/media/session/ai;->u:Landroid/content/ComponentName;

    invoke-static {v0, v3}, Landroid/support/v4/media/session/bh;->a(Landroid/content/Context;Landroid/content/ComponentName;)V

    goto :goto_77

    .line 1336
    :cond_82
    iget-boolean v0, p0, Landroid/support/v4/media/session/ai;->A:Z

    if-eqz v0, :cond_bf

    iget v0, p0, Landroid/support/v4/media/session/ai;->i:I

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_bf

    .line 1341
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->w:Ljava/lang/Object;

    invoke-static {v0, v1}, Landroid/support/v4/media/session/at;->a(Ljava/lang/Object;I)V

    .line 1342
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->t:Landroid/content/Context;

    iget-object v2, p0, Landroid/support/v4/media/session/ai;->w:Ljava/lang/Object;

    invoke-static {v0, v2}, Landroid/support/v4/media/session/at;->a(Landroid/content/Context;Ljava/lang/Object;)V

    .line 1343
    iput-boolean v1, p0, Landroid/support/v4/media/session/ai;->A:Z

    move v0, v1

    goto :goto_51

    .line 1348
    :cond_9c
    iget-boolean v0, p0, Landroid/support/v4/media/session/ai;->B:Z

    if-eqz v0, :cond_ad

    .line 1349
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v4, :cond_c1

    .line 1350
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->t:Landroid/content/Context;

    iget-object v2, p0, Landroid/support/v4/media/session/ai;->v:Landroid/app/PendingIntent;

    invoke-static {v0, v2}, Landroid/support/v4/media/session/av;->a(Landroid/content/Context;Landroid/app/PendingIntent;)V

    .line 1356
    :goto_ab
    iput-boolean v1, p0, Landroid/support/v4/media/session/ai;->B:Z

    .line 1358
    :cond_ad
    iget-boolean v0, p0, Landroid/support/v4/media/session/ai;->A:Z

    if-eqz v0, :cond_bf

    .line 1362
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->w:Ljava/lang/Object;

    invoke-static {v0, v1}, Landroid/support/v4/media/session/at;->a(Ljava/lang/Object;I)V

    .line 1363
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->t:Landroid/content/Context;

    iget-object v2, p0, Landroid/support/v4/media/session/ai;->w:Ljava/lang/Object;

    invoke-static {v0, v2}, Landroid/support/v4/media/session/at;->a(Landroid/content/Context;Ljava/lang/Object;)V

    .line 1364
    iput-boolean v1, p0, Landroid/support/v4/media/session/ai;->A:Z

    :cond_bf
    move v0, v1

    goto :goto_51

    .line 1353
    :cond_c1
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->t:Landroid/content/Context;

    iget-object v2, p0, Landroid/support/v4/media/session/ai;->u:Landroid/content/ComponentName;

    invoke-static {v0, v2}, Landroid/support/v4/media/session/bh;->a(Landroid/content/Context;Landroid/content/ComponentName;)V

    goto :goto_ab
.end method

.method private static synthetic h(Landroid/support/v4/media/session/ai;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 963
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->b:Ljava/lang/String;

    return-object v0
.end method

.method private h()V
    .registers 3

    .prologue
    .line 1440
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    .line 1441
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_9
    if-ltz v1, :cond_1a

    .line 1442
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Landroid/support/v4/media/session/a;

    .line 1444
    :try_start_13
    invoke-interface {v0}, Landroid/support/v4/media/session/a;->a()V
    :try_end_16
    .catch Landroid/os/RemoteException; {:try_start_13 .. :try_end_16} :catch_25

    .line 1441
    :goto_16
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_9

    .line 1448
    :cond_1a
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1449
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->kill()V

    .line 1450
    return-void

    :catch_25
    move-exception v0

    goto :goto_16
.end method

.method private static synthetic i(Landroid/support/v4/media/session/ai;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 963
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->c:Ljava/lang/String;

    return-object v0
.end method

.method private static synthetic j(Landroid/support/v4/media/session/ai;)Ljava/lang/Object;
    .registers 2

    .prologue
    .line 963
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->e:Ljava/lang/Object;

    return-object v0
.end method

.method private static synthetic k(Landroid/support/v4/media/session/ai;)Landroid/app/PendingIntent;
    .registers 2

    .prologue
    .line 963
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->l:Landroid/app/PendingIntent;

    return-object v0
.end method

.method private static synthetic l(Landroid/support/v4/media/session/ai;)Landroid/media/AudioManager;
    .registers 2

    .prologue
    .line 963
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->d:Landroid/media/AudioManager;

    return-object v0
.end method

.method private static synthetic m(Landroid/support/v4/media/session/ai;)Landroid/support/v4/media/MediaMetadataCompat;
    .registers 2

    .prologue
    .line 963
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->j:Landroid/support/v4/media/MediaMetadataCompat;

    return-object v0
.end method

.method private static synthetic n(Landroid/support/v4/media/session/ai;)Landroid/support/v4/media/session/PlaybackStateCompat;
    .registers 2

    .prologue
    .line 963
    invoke-virtual {p0}, Landroid/support/v4/media/session/ai;->f()Landroid/support/v4/media/session/PlaybackStateCompat;

    move-result-object v0

    return-object v0
.end method

.method private static synthetic o(Landroid/support/v4/media/session/ai;)Ljava/util/List;
    .registers 2

    .prologue
    .line 963
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->m:Ljava/util/List;

    return-object v0
.end method

.method private static synthetic p(Landroid/support/v4/media/session/ai;)Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 963
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->n:Ljava/lang/CharSequence;

    return-object v0
.end method

.method private static synthetic q(Landroid/support/v4/media/session/ai;)Landroid/os/Bundle;
    .registers 2

    .prologue
    .line 963
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->p:Landroid/os/Bundle;

    return-object v0
.end method

.method private static synthetic r(Landroid/support/v4/media/session/ai;)I
    .registers 2

    .prologue
    .line 963
    iget v0, p0, Landroid/support/v4/media/session/ai;->o:I

    return v0
.end method

.method private static synthetic s(Landroid/support/v4/media/session/ai;)Landroid/support/v4/media/session/ad;
    .registers 2

    .prologue
    .line 963
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->h:Landroid/support/v4/media/session/ad;

    return-object v0
.end method

.method private static synthetic t(Landroid/support/v4/media/session/ai;)Landroid/support/v4/media/session/PlaybackStateCompat;
    .registers 2

    .prologue
    .line 963
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->k:Landroid/support/v4/media/session/PlaybackStateCompat;

    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .registers 4

    .prologue
    .line 1131
    iget-object v1, p0, Landroid/support/v4/media/session/ai;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 1132
    :try_start_3
    iput p1, p0, Landroid/support/v4/media/session/ai;->i:I

    .line 1133
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_a

    .line 1134
    invoke-direct {p0}, Landroid/support/v4/media/session/ai;->g()Z

    .line 1135
    return-void

    .line 1133
    :catchall_a
    move-exception v0

    :try_start_b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_b .. :try_end_c} :catchall_a

    throw v0
.end method

.method public final a(Landroid/app/PendingIntent;)V
    .registers 4

    .prologue
    .line 1260
    iget-object v1, p0, Landroid/support/v4/media/session/ai;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 1261
    :try_start_3
    iput-object p1, p0, Landroid/support/v4/media/session/ai;->l:Landroid/app/PendingIntent;

    .line 1262
    monitor-exit v1

    return-void

    :catchall_7
    move-exception v0

    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    throw v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .registers 2

    .prologue
    .line 1299
    iput-object p1, p0, Landroid/support/v4/media/session/ai;->p:Landroid/os/Bundle;

    .line 1300
    return-void
.end method

.method public final a(Landroid/support/v4/media/MediaMetadataCompat;)V
    .registers 13

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    const v10, 0x10000001

    const/4 v6, 0x1

    .line 1240
    iget-object v2, p0, Landroid/support/v4/media/session/ai;->e:Ljava/lang/Object;

    monitor-enter v2

    .line 1241
    :try_start_a
    iput-object p1, p0, Landroid/support/v4/media/session/ai;->j:Landroid/support/v4/media/MediaMetadataCompat;

    .line 1242
    monitor-exit v2
    :try_end_d
    .catchall {:try_start_a .. :try_end_d} :catchall_27

    .line 11477
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    .line 11478
    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_16
    if-ltz v2, :cond_2a

    .line 11479
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Landroid/support/v4/media/session/a;

    .line 11481
    :try_start_20
    invoke-interface {v0, p1}, Landroid/support/v4/media/session/a;->a(Landroid/support/v4/media/MediaMetadataCompat;)V
    :try_end_23
    .catch Landroid/os/RemoteException; {:try_start_20 .. :try_end_23} :catch_b5

    .line 11478
    :goto_23
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_16

    .line 1242
    :catchall_27
    move-exception v0

    :try_start_28
    monitor-exit v2
    :try_end_29
    .catchall {:try_start_28 .. :try_end_29} :catchall_27

    throw v0

    .line 11485
    :cond_2a
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1244
    iget-boolean v0, p0, Landroid/support/v4/media/session/ai;->z:Z

    if-nez v0, :cond_34

    .line 1256
    :cond_33
    :goto_33
    return-void

    .line 1248
    :cond_34
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v0, v2, :cond_9b

    .line 1249
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->w:Ljava/lang/Object;

    if-nez p1, :cond_93

    :goto_3e
    iget-object v2, p0, Landroid/support/v4/media/session/ai;->k:Landroid/support/v4/media/session/PlaybackStateCompat;

    if-nez v2, :cond_96

    move-wide v2, v4

    .line 13044
    :goto_43
    check-cast v0, Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v6}, Landroid/media/RemoteControlClient;->editMetadata(Z)Landroid/media/RemoteControlClient$MetadataEditor;

    move-result-object v0

    .line 13046
    invoke-static {v1, v0}, Landroid/support/v4/media/session/at;->a(Landroid/os/Bundle;Landroid/media/RemoteControlClient$MetadataEditor;)V

    .line 13069
    if-eqz v1, :cond_85

    .line 13072
    const-string v6, "android.media.metadata.YEAR"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_61

    .line 13073
    const/16 v6, 0x8

    const-string v7, "android.media.metadata.YEAR"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-virtual {v0, v6, v8, v9}, Landroid/media/RemoteControlClient$MetadataEditor;->putLong(IJ)Landroid/media/RemoteControlClient$MetadataEditor;

    .line 13076
    :cond_61
    const-string v6, "android.media.metadata.RATING"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_74

    .line 13077
    const/16 v6, 0x65

    const-string v7, "android.media.metadata.RATING"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/media/RemoteControlClient$MetadataEditor;->putObject(ILjava/lang/Object;)Landroid/media/MediaMetadataEditor;

    .line 13080
    :cond_74
    const-string v6, "android.media.metadata.USER_RATING"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_85

    .line 13081
    const-string v6, "android.media.metadata.USER_RATING"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {v0, v10, v1}, Landroid/media/RemoteControlClient$MetadataEditor;->putObject(ILjava/lang/Object;)Landroid/media/MediaMetadataEditor;

    .line 13048
    :cond_85
    const-wide/16 v6, 0x80

    and-long/2addr v2, v6

    cmp-long v1, v2, v4

    if-eqz v1, :cond_8f

    .line 13049
    invoke-virtual {v0, v10}, Landroid/media/RemoteControlClient$MetadataEditor;->addEditableKey(I)V

    .line 13051
    :cond_8f
    invoke-virtual {v0}, Landroid/media/RemoteControlClient$MetadataEditor;->apply()V

    goto :goto_33

    .line 11491
    :cond_93
    iget-object v1, p1, Landroid/support/v4/media/MediaMetadataCompat;->C:Landroid/os/Bundle;

    goto :goto_3e

    .line 1249
    :cond_96
    iget-object v2, p0, Landroid/support/v4/media/session/ai;->k:Landroid/support/v4/media/session/PlaybackStateCompat;

    .line 12405
    iget-wide v2, v2, Landroid/support/v4/media/session/PlaybackStateCompat;->F:J

    goto :goto_43

    .line 1252
    :cond_9b
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v0, v2, :cond_33

    .line 1253
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->w:Ljava/lang/Object;

    if-nez p1, :cond_b2

    .line 14087
    :goto_a5
    check-cast v0, Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v6}, Landroid/media/RemoteControlClient;->editMetadata(Z)Landroid/media/RemoteControlClient$MetadataEditor;

    move-result-object v0

    .line 14089
    invoke-static {v1, v0}, Landroid/support/v4/media/session/at;->a(Landroid/os/Bundle;Landroid/media/RemoteControlClient$MetadataEditor;)V

    .line 14090
    invoke-virtual {v0}, Landroid/media/RemoteControlClient$MetadataEditor;->apply()V

    goto :goto_33

    .line 13491
    :cond_b2
    iget-object v1, p1, Landroid/support/v4/media/MediaMetadataCompat;->C:Landroid/os/Bundle;

    goto :goto_a5

    :catch_b5
    move-exception v0

    goto/16 :goto_23
.end method

.method public final a(Landroid/support/v4/media/ae;)V
    .registers 8

    .prologue
    .line 1152
    if-nez p1, :cond_a

    .line 1153
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "volumeProvider may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1155
    :cond_a
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->s:Landroid/support/v4/media/ae;

    if-eqz v0, :cond_13

    .line 1156
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->s:Landroid/support/v4/media/ae;

    .line 4151
    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v4/media/ae;->g:Landroid/support/v4/media/ag;

    .line 1158
    :cond_13
    const/4 v0, 0x2

    iput v0, p0, Landroid/support/v4/media/session/ai;->q:I

    .line 1159
    iput-object p1, p0, Landroid/support/v4/media/session/ai;->s:Landroid/support/v4/media/ae;

    .line 1160
    new-instance v0, Landroid/support/v4/media/session/ParcelableVolumeInfo;

    iget v1, p0, Landroid/support/v4/media/session/ai;->q:I

    iget v2, p0, Landroid/support/v4/media/session/ai;->r:I

    iget-object v3, p0, Landroid/support/v4/media/session/ai;->s:Landroid/support/v4/media/ae;

    .line 5099
    iget v3, v3, Landroid/support/v4/media/ae;->d:I

    .line 1160
    iget-object v4, p0, Landroid/support/v4/media/session/ai;->s:Landroid/support/v4/media/ae;

    .line 5108
    iget v4, v4, Landroid/support/v4/media/ae;->e:I

    .line 1160
    iget-object v5, p0, Landroid/support/v4/media/session/ai;->s:Landroid/support/v4/media/ae;

    .line 6089
    iget v5, v5, Landroid/support/v4/media/ae;->f:I

    .line 1160
    invoke-direct/range {v0 .. v5}, Landroid/support/v4/media/session/ParcelableVolumeInfo;-><init>(IIIII)V

    .line 1163
    invoke-virtual {p0, v0}, Landroid/support/v4/media/session/ai;->a(Landroid/support/v4/media/session/ParcelableVolumeInfo;)V

    .line 1165
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->C:Landroid/support/v4/media/ag;

    .line 6151
    iput-object v0, p1, Landroid/support/v4/media/ae;->g:Landroid/support/v4/media/ag;

    .line 1166
    return-void
.end method

.method final a(Landroid/support/v4/media/session/ParcelableVolumeInfo;)V
    .registers 4

    .prologue
    .line 1428
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    .line 1429
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_9
    if-ltz v1, :cond_1a

    .line 1430
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Landroid/support/v4/media/session/a;

    .line 1432
    :try_start_13
    invoke-interface {v0, p1}, Landroid/support/v4/media/session/a;->a(Landroid/support/v4/media/session/ParcelableVolumeInfo;)V
    :try_end_16
    .catch Landroid/os/RemoteException; {:try_start_13 .. :try_end_16} :catch_20

    .line 1429
    :goto_16
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_9

    .line 1436
    :cond_1a
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1437
    return-void

    :catch_20
    move-exception v0

    goto :goto_16
.end method

.method public final a(Landroid/support/v4/media/session/PlaybackStateCompat;)V
    .registers 16

    .prologue
    const/16 v13, 0x12

    const/16 v12, 0xe

    const-wide/16 v4, 0x0

    .line 1205
    iget-object v1, p0, Landroid/support/v4/media/session/ai;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 1206
    :try_start_9
    iput-object p1, p0, Landroid/support/v4/media/session/ai;->k:Landroid/support/v4/media/session/PlaybackStateCompat;

    .line 1207
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_9 .. :try_end_c} :catchall_26

    .line 7465
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    .line 7466
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_15
    if-ltz v1, :cond_29

    .line 7467
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Landroid/support/v4/media/session/a;

    .line 7469
    :try_start_1f
    invoke-interface {v0, p1}, Landroid/support/v4/media/session/a;->a(Landroid/support/v4/media/session/PlaybackStateCompat;)V
    :try_end_22
    .catch Landroid/os/RemoteException; {:try_start_1f .. :try_end_22} :catch_c5

    .line 7466
    :goto_22
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_15

    .line 1207
    :catchall_26
    move-exception v0

    :try_start_27
    monitor-exit v1
    :try_end_28
    .catchall {:try_start_27 .. :try_end_28} :catchall_26

    throw v0

    .line 7473
    :cond_29
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1209
    iget-boolean v0, p0, Landroid/support/v4/media/session/ai;->z:Z

    if-nez v0, :cond_33

    .line 1236
    :cond_32
    :goto_32
    return-void

    .line 1213
    :cond_33
    if-nez p1, :cond_45

    .line 1214
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v12, :cond_32

    .line 1215
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->w:Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/support/v4/media/session/at;->a(Ljava/lang/Object;I)V

    .line 1216
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->w:Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Landroid/support/v4/media/session/at;->a(Ljava/lang/Object;J)V

    goto :goto_32

    .line 1220
    :cond_45
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v13, :cond_99

    .line 1221
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->w:Ljava/lang/Object;

    .line 8353
    iget v1, p1, Landroid/support/v4/media/session/PlaybackStateCompat;->B:I

    .line 8360
    iget-wide v6, p1, Landroid/support/v4/media/session/PlaybackStateCompat;->C:J

    .line 8380
    iget v8, p1, Landroid/support/v4/media/session/PlaybackStateCompat;->E:F

    .line 8430
    iget-wide v2, p1, Landroid/support/v4/media/session/PlaybackStateCompat;->H:J

    .line 9045
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 9046
    const/4 v9, 0x3

    if-ne v1, v9, :cond_ca

    cmp-long v9, v6, v4

    if-lez v9, :cond_ca

    .line 9048
    cmp-long v9, v2, v4

    if-lez v9, :cond_c8

    .line 9049
    sub-long v2, v10, v2

    .line 9050
    const/4 v9, 0x0

    cmpl-float v9, v8, v9

    if-lez v9, :cond_72

    const/high16 v9, 0x3f800000    # 1.0f

    cmpl-float v9, v8, v9

    if-eqz v9, :cond_72

    .line 9051
    long-to-float v2, v2

    mul-float/2addr v2, v8

    float-to-long v2, v2

    .line 9054
    :cond_72
    :goto_72
    add-long/2addr v2, v6

    .line 9056
    :goto_73
    invoke-static {v1}, Landroid/support/v4/media/session/at;->a(I)I

    move-result v1

    .line 9057
    check-cast v0, Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v1, v2, v3, v8}, Landroid/media/RemoteControlClient;->setPlaybackState(IJF)V

    .line 1228
    :cond_7c
    :goto_7c
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_a5

    .line 1229
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->w:Ljava/lang/Object;

    .line 9405
    iget-wide v2, p1, Landroid/support/v4/media/session/PlaybackStateCompat;->F:J

    .line 10035
    check-cast v0, Landroid/media/RemoteControlClient;

    .line 10060
    invoke-static {v2, v3}, Landroid/support/v4/media/session/av;->a(J)I

    move-result v1

    .line 10062
    const-wide/16 v6, 0x80

    and-long/2addr v2, v6

    cmp-long v2, v2, v4

    if-eqz v2, :cond_95

    .line 10063
    or-int/lit16 v1, v1, 0x200

    .line 10035
    :cond_95
    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient;->setTransportControlFlags(I)V

    goto :goto_32

    .line 1223
    :cond_99
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v12, :cond_7c

    .line 1224
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->w:Ljava/lang/Object;

    .line 9353
    iget v1, p1, Landroid/support/v4/media/session/PlaybackStateCompat;->B:I

    .line 1224
    invoke-static {v0, v1}, Landroid/support/v4/media/session/at;->a(Ljava/lang/Object;I)V

    goto :goto_7c

    .line 1230
    :cond_a5
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v13, :cond_b8

    .line 1231
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->w:Ljava/lang/Object;

    .line 10405
    iget-wide v2, p1, Landroid/support/v4/media/session/PlaybackStateCompat;->F:J

    .line 11061
    check-cast v0, Landroid/media/RemoteControlClient;

    invoke-static {v2, v3}, Landroid/support/v4/media/session/av;->a(J)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient;->setTransportControlFlags(I)V

    goto/16 :goto_32

    .line 1232
    :cond_b8
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v12, :cond_32

    .line 1233
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->w:Ljava/lang/Object;

    .line 11405
    iget-wide v2, p1, Landroid/support/v4/media/session/PlaybackStateCompat;->F:J

    .line 1233
    invoke-static {v0, v2, v3}, Landroid/support/v4/media/session/at;->a(Ljava/lang/Object;J)V

    goto/16 :goto_32

    :catch_c5
    move-exception v0

    goto/16 :goto_22

    :cond_c8
    move-wide v2, v4

    goto :goto_72

    :cond_ca
    move-wide v2, v6

    goto :goto_73
.end method

.method public final a(Landroid/support/v4/media/session/ad;Landroid/os/Handler;)V
    .registers 7

    .prologue
    const/4 v1, 0x0

    const/16 v3, 0x13

    const/16 v2, 0x12

    .line 1041
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->h:Landroid/support/v4/media/session/ad;

    if-ne p1, v0, :cond_a

    .line 1127
    :goto_9
    return-void

    .line 1044
    :cond_a
    if-eqz p1, :cond_10

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v2, :cond_25

    .line 1047
    :cond_10
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_19

    .line 1048
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->w:Ljava/lang/Object;

    invoke-static {v0, v1}, Landroid/support/v4/media/session/av;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1050
    :cond_19
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_22

    .line 1051
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->w:Ljava/lang/Object;

    invoke-static {v0, v1}, Landroid/support/v4/media/session/ax;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1126
    :cond_22
    :goto_22
    iput-object p1, p0, Landroid/support/v4/media/session/ai;->h:Landroid/support/v4/media/session/ad;

    goto :goto_9

    .line 1057
    :cond_25
    new-instance v0, Landroid/support/v4/media/session/ak;

    invoke-direct {v0, p0, p1}, Landroid/support/v4/media/session/ak;-><init>(Landroid/support/v4/media/session/ai;Landroid/support/v4/media/session/ad;)V

    .line 1113
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v2, :cond_38

    .line 3030
    new-instance v1, Landroid/support/v4/media/session/aw;

    invoke-direct {v1, v0}, Landroid/support/v4/media/session/aw;-><init>(Landroid/support/v4/media/session/au;)V

    .line 1116
    iget-object v2, p0, Landroid/support/v4/media/session/ai;->w:Ljava/lang/Object;

    invoke-static {v2, v1}, Landroid/support/v4/media/session/av;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1119
    :cond_38
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v3, :cond_22

    .line 3040
    new-instance v1, Landroid/support/v4/media/session/ay;

    invoke-direct {v1, v0}, Landroid/support/v4/media/session/ay;-><init>(Landroid/support/v4/media/session/au;)V

    .line 1122
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->w:Ljava/lang/Object;

    invoke-static {v0, v1}, Landroid/support/v4/media/session/ax;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_22
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .registers 4

    .prologue
    .line 1278
    iput-object p1, p0, Landroid/support/v4/media/session/ai;->n:Ljava/lang/CharSequence;

    .line 14501
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    .line 14502
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_b
    if-ltz v1, :cond_1c

    .line 14503
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Landroid/support/v4/media/session/a;

    .line 14505
    :try_start_15
    invoke-interface {v0, p1}, Landroid/support/v4/media/session/a;->a(Ljava/lang/CharSequence;)V
    :try_end_18
    .catch Landroid/os/RemoteException; {:try_start_15 .. :try_end_18} :catch_22

    .line 14502
    :goto_18
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_b

    .line 14509
    :cond_1c
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1280
    return-void

    :catch_22
    move-exception v0

    goto :goto_18
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 5

    .prologue
    .line 1187
    .line 6453
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    .line 6454
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_9
    if-ltz v1, :cond_1a

    .line 6455
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Landroid/support/v4/media/session/a;

    .line 6457
    :try_start_13
    invoke-interface {v0, p1, p2}, Landroid/support/v4/media/session/a;->a(Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_16
    .catch Landroid/os/RemoteException; {:try_start_13 .. :try_end_16} :catch_20

    .line 6454
    :goto_16
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_9

    .line 6461
    :cond_1a
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1188
    return-void

    :catch_20
    move-exception v0

    goto :goto_16
.end method

.method public final a(Ljava/util/List;)V
    .registers 4

    .prologue
    .line 1272
    iput-object p1, p0, Landroid/support/v4/media/session/ai;->m:Ljava/util/List;

    .line 14489
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    .line 14490
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_b
    if-ltz v1, :cond_1c

    .line 14491
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Landroid/support/v4/media/session/a;

    .line 14493
    :try_start_15
    invoke-interface {v0, p1}, Landroid/support/v4/media/session/a;->a(Ljava/util/List;)V
    :try_end_18
    .catch Landroid/os/RemoteException; {:try_start_15 .. :try_end_18} :catch_22

    .line 14490
    :goto_18
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_b

    .line 14497
    :cond_1c
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1274
    return-void

    :catch_22
    move-exception v0

    goto :goto_18
.end method

.method public final a(Z)V
    .registers 3

    .prologue
    .line 1170
    iget-boolean v0, p0, Landroid/support/v4/media/session/ai;->z:Z

    if-ne p1, v0, :cond_5

    .line 1178
    :cond_4
    :goto_4
    return-void

    .line 1173
    :cond_5
    iput-boolean p1, p0, Landroid/support/v4/media/session/ai;->z:Z

    .line 1174
    invoke-direct {p0}, Landroid/support/v4/media/session/ai;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1175
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->j:Landroid/support/v4/media/MediaMetadataCompat;

    invoke-virtual {p0, v0}, Landroid/support/v4/media/session/ai;->a(Landroid/support/v4/media/MediaMetadataCompat;)V

    .line 1176
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->k:Landroid/support/v4/media/session/PlaybackStateCompat;

    invoke-virtual {p0, v0}, Landroid/support/v4/media/session/ai;->a(Landroid/support/v4/media/session/PlaybackStateCompat;)V

    goto :goto_4
.end method

.method public final a()Z
    .registers 2

    .prologue
    .line 1182
    iget-boolean v0, p0, Landroid/support/v4/media/session/ai;->z:Z

    return v0
.end method

.method public final b()V
    .registers 3

    .prologue
    .line 1192
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v4/media/session/ai;->z:Z

    .line 1193
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/media/session/ai;->g:Z

    .line 1194
    invoke-direct {p0}, Landroid/support/v4/media/session/ai;->g()Z

    .line 7440
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    .line 7441
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_12
    if-ltz v1, :cond_23

    .line 7442
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Landroid/support/v4/media/session/a;

    .line 7444
    :try_start_1c
    invoke-interface {v0}, Landroid/support/v4/media/session/a;->a()V
    :try_end_1f
    .catch Landroid/os/RemoteException; {:try_start_1c .. :try_end_1f} :catch_2e

    .line 7441
    :goto_1f
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_12

    .line 7448
    :cond_23
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 7449
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->kill()V

    .line 1196
    return-void

    :catch_2e
    move-exception v0

    goto :goto_1f
.end method

.method public final b(I)V
    .registers 9

    .prologue
    .line 1139
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->s:Landroid/support/v4/media/ae;

    if-eqz v0, :cond_9

    .line 1140
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->s:Landroid/support/v4/media/ae;

    .line 3151
    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v4/media/ae;->g:Landroid/support/v4/media/ag;

    .line 1142
    :cond_9
    const/4 v0, 0x1

    iput v0, p0, Landroid/support/v4/media/session/ai;->q:I

    .line 1143
    new-instance v0, Landroid/support/v4/media/session/ParcelableVolumeInfo;

    iget v1, p0, Landroid/support/v4/media/session/ai;->q:I

    iget v2, p0, Landroid/support/v4/media/session/ai;->r:I

    const/4 v3, 0x2

    iget-object v4, p0, Landroid/support/v4/media/session/ai;->d:Landroid/media/AudioManager;

    iget v5, p0, Landroid/support/v4/media/session/ai;->r:I

    invoke-virtual {v4, v5}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v4

    iget-object v5, p0, Landroid/support/v4/media/session/ai;->d:Landroid/media/AudioManager;

    iget v6, p0, Landroid/support/v4/media/session/ai;->r:I

    invoke-virtual {v5, v6}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v5

    invoke-direct/range {v0 .. v5}, Landroid/support/v4/media/session/ParcelableVolumeInfo;-><init>(IIIII)V

    .line 1147
    invoke-virtual {p0, v0}, Landroid/support/v4/media/session/ai;->a(Landroid/support/v4/media/session/ParcelableVolumeInfo;)V

    .line 1148
    return-void
.end method

.method public final b(Landroid/app/PendingIntent;)V
    .registers 2

    .prologue
    .line 1268
    return-void
.end method

.method public final c()Landroid/support/v4/media/session/MediaSessionCompat$Token;
    .registers 2

    .prologue
    .line 1200
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->y:Landroid/support/v4/media/session/MediaSessionCompat$Token;

    return-object v0
.end method

.method public final c(I)V
    .registers 2

    .prologue
    .line 1294
    iput p1, p0, Landroid/support/v4/media/session/ai;->o:I

    .line 1295
    return-void
.end method

.method public final d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1284
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1289
    iget-object v0, p0, Landroid/support/v4/media/session/ai;->w:Ljava/lang/Object;

    return-object v0
.end method

.method final f()Landroid/support/v4/media/session/PlaybackStateCompat;
    .registers 22

    .prologue
    .line 1392
    const-wide/16 v4, -0x1

    .line 1393
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v4/media/session/ai;->e:Ljava/lang/Object;

    monitor-enter v3

    .line 1394
    :try_start_7
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v4/media/session/ai;->k:Landroid/support/v4/media/session/PlaybackStateCompat;

    move-object/from16 v19, v0

    .line 1395
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/media/session/ai;->j:Landroid/support/v4/media/MediaMetadataCompat;

    if-eqz v2, :cond_2b

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/media/session/ai;->j:Landroid/support/v4/media/MediaMetadataCompat;

    const-string v6, "android.media.metadata.DURATION"

    .line 16311
    iget-object v2, v2, Landroid/support/v4/media/MediaMetadataCompat;->C:Landroid/os/Bundle;

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    .line 1395
    if-eqz v2, :cond_2b

    .line 1397
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v4/media/session/ai;->j:Landroid/support/v4/media/MediaMetadataCompat;

    const-string v4, "android.media.metadata.DURATION"

    invoke-virtual {v2, v4}, Landroid/support/v4/media/MediaMetadataCompat;->b(Ljava/lang/String;)J

    move-result-wide v4

    .line 1399
    :cond_2b
    monitor-exit v3
    :try_end_2c
    .catchall {:try_start_7 .. :try_end_2c} :catchall_a2

    .line 1401
    const/4 v2, 0x0

    .line 1402
    if-eqz v19, :cond_9d

    .line 16353
    move-object/from16 v0, v19

    iget v3, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->B:I

    .line 1403
    const/4 v6, 0x3

    if-eq v3, v6, :cond_44

    .line 17353
    move-object/from16 v0, v19

    iget v3, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->B:I

    .line 1403
    const/4 v6, 0x4

    if-eq v3, v6, :cond_44

    .line 18353
    move-object/from16 v0, v19

    iget v3, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->B:I

    .line 1403
    const/4 v6, 0x5

    if-ne v3, v6, :cond_9d

    .line 18430
    :cond_44
    move-object/from16 v0, v19

    iget-wide v10, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->H:J

    .line 1407
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v7

    .line 1408
    const-wide/16 v12, 0x0

    cmp-long v3, v10, v12

    if-lez v3, :cond_9d

    .line 19380
    move-object/from16 v0, v19

    iget v2, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->E:F

    .line 1409
    sub-long v10, v7, v10

    long-to-float v3, v10

    mul-float/2addr v2, v3

    float-to-long v2, v2

    .line 20360
    move-object/from16 v0, v19

    iget-wide v10, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->C:J

    .line 1409
    add-long/2addr v2, v10

    .line 1411
    const-wide/16 v10, 0x0

    cmp-long v6, v4, v10

    if-ltz v6, :cond_a5

    cmp-long v6, v2, v4

    if-lez v6, :cond_a5

    .line 1416
    :goto_6a
    new-instance v2, Landroid/support/v4/media/session/bl;

    move-object/from16 v0, v19

    invoke-direct {v2, v0}, Landroid/support/v4/media/session/bl;-><init>(Landroid/support/v4/media/session/PlaybackStateCompat;)V

    .line 21353
    move-object/from16 v0, v19

    iget v3, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->B:I

    .line 21380
    move-object/from16 v0, v19

    iget v6, v0, Landroid/support/v4/media/session/PlaybackStateCompat;->E:F

    .line 1418
    invoke-virtual/range {v2 .. v8}, Landroid/support/v4/media/session/bl;->a(IJFJ)Landroid/support/v4/media/session/bl;

    .line 21998
    new-instance v20, Landroid/support/v4/media/session/PlaybackStateCompat;

    iget v3, v2, Landroid/support/v4/media/session/bl;->b:I

    iget-wide v4, v2, Landroid/support/v4/media/session/bl;->c:J

    iget-wide v6, v2, Landroid/support/v4/media/session/bl;->d:J

    iget v8, v2, Landroid/support/v4/media/session/bl;->e:F

    iget-wide v9, v2, Landroid/support/v4/media/session/bl;->f:J

    iget-object v11, v2, Landroid/support/v4/media/session/bl;->g:Ljava/lang/CharSequence;

    iget-wide v12, v2, Landroid/support/v4/media/session/bl;->h:J

    iget-object v14, v2, Landroid/support/v4/media/session/bl;->a:Ljava/util/List;

    iget-wide v15, v2, Landroid/support/v4/media/session/bl;->i:J

    iget-object v0, v2, Landroid/support/v4/media/session/bl;->j:Landroid/os/Bundle;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v2, v20

    invoke-direct/range {v2 .. v18}, Landroid/support/v4/media/session/PlaybackStateCompat;-><init>(IJJFJLjava/lang/CharSequence;JLjava/util/List;JLandroid/os/Bundle;B)V

    move-object/from16 v2, v20

    .line 1424
    :cond_9d
    if-nez v2, :cond_a1

    move-object/from16 v2, v19

    :cond_a1
    return-object v2

    .line 1399
    :catchall_a2
    move-exception v2

    :try_start_a3
    monitor-exit v3
    :try_end_a4
    .catchall {:try_start_a3 .. :try_end_a4} :catchall_a2

    throw v2

    .line 1413
    :cond_a5
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-gez v4, :cond_ae

    .line 1414
    const-wide/16 v4, 0x0

    goto :goto_6a

    :cond_ae
    move-wide v4, v2

    goto :goto_6a
.end method
