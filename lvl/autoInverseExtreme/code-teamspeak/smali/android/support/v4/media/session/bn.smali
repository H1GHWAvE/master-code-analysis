.class public final Landroid/support/v4/media/session/bn;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/CharSequence;

.field private final c:I

.field private d:Landroid/os/Bundle;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V
    .registers 6

    .prologue
    .line 712
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 713
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 714
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "You must specify an action to build a CustomAction."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 717
    :cond_11
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 718
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "You must specify a name to build a CustomAction."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 721
    :cond_1f
    if-nez p3, :cond_29

    .line 722
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "You must specify an icon resource id to build a CustomAction."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 725
    :cond_29
    iput-object p1, p0, Landroid/support/v4/media/session/bn;->a:Ljava/lang/String;

    .line 726
    iput-object p2, p0, Landroid/support/v4/media/session/bn;->b:Ljava/lang/CharSequence;

    .line 727
    iput p3, p0, Landroid/support/v4/media/session/bn;->c:I

    .line 728
    return-void
.end method

.method private a()Landroid/support/v4/media/session/PlaybackStateCompat$CustomAction;
    .registers 7

    .prologue
    .line 751
    new-instance v0, Landroid/support/v4/media/session/PlaybackStateCompat$CustomAction;

    iget-object v1, p0, Landroid/support/v4/media/session/bn;->a:Ljava/lang/String;

    iget-object v2, p0, Landroid/support/v4/media/session/bn;->b:Ljava/lang/CharSequence;

    iget v3, p0, Landroid/support/v4/media/session/bn;->c:I

    iget-object v4, p0, Landroid/support/v4/media/session/bn;->d:Landroid/os/Bundle;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Landroid/support/v4/media/session/PlaybackStateCompat$CustomAction;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;ILandroid/os/Bundle;B)V

    return-object v0
.end method

.method private a(Landroid/os/Bundle;)Landroid/support/v4/media/session/bn;
    .registers 2

    .prologue
    .line 740
    iput-object p1, p0, Landroid/support/v4/media/session/bn;->d:Landroid/os/Bundle;

    .line 741
    return-object p0
.end method
