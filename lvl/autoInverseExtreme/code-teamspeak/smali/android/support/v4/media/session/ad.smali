.class public abstract Landroid/support/v4/media/session/ad;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 473
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 474
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_17

    .line 475
    new-instance v0, Landroid/support/v4/media/session/af;

    invoke-direct {v0, p0, v2}, Landroid/support/v4/media/session/af;-><init>(Landroid/support/v4/media/session/ad;B)V

    .line 1025
    new-instance v1, Landroid/support/v4/media/session/bg;

    invoke-direct {v1, v0}, Landroid/support/v4/media/session/bg;-><init>(Landroid/support/v4/media/session/bf;)V

    .line 475
    iput-object v1, p0, Landroid/support/v4/media/session/ad;->a:Ljava/lang/Object;

    .line 481
    :goto_16
    return-void

    .line 476
    :cond_17
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_2a

    .line 477
    new-instance v0, Landroid/support/v4/media/session/ae;

    invoke-direct {v0, p0, v2}, Landroid/support/v4/media/session/ae;-><init>(Landroid/support/v4/media/session/ad;B)V

    .line 1057
    new-instance v1, Landroid/support/v4/media/session/bb;

    invoke-direct {v1, v0}, Landroid/support/v4/media/session/bb;-><init>(Landroid/support/v4/media/session/ba;)V

    .line 477
    iput-object v1, p0, Landroid/support/v4/media/session/ad;->a:Ljava/lang/Object;

    goto :goto_16

    .line 479
    :cond_2a
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/media/session/ad;->a:Ljava/lang/Object;

    goto :goto_16
.end method

.method private static a()V
    .registers 0

    .prologue
    .line 493
    return-void
.end method

.method private static b()Z
    .registers 1

    .prologue
    .line 502
    const/4 v0, 0x0

    return v0
.end method

.method private static c()V
    .registers 0

    .prologue
    .line 509
    return-void
.end method

.method private static d()V
    .registers 0

    .prologue
    .line 516
    return-void
.end method

.method private static e()V
    .registers 0

    .prologue
    .line 525
    return-void
.end method

.method private static f()V
    .registers 0

    .prologue
    .line 531
    return-void
.end method

.method private static g()V
    .registers 0

    .prologue
    .line 538
    return-void
.end method

.method private static h()V
    .registers 0

    .prologue
    .line 544
    return-void
.end method

.method private static i()V
    .registers 0

    .prologue
    .line 550
    return-void
.end method

.method private static j()V
    .registers 0

    .prologue
    .line 556
    return-void
.end method

.method private static k()V
    .registers 0

    .prologue
    .line 562
    return-void
.end method

.method private static l()V
    .registers 0

    .prologue
    .line 568
    return-void
.end method

.method private static m()V
    .registers 0

    .prologue
    .line 574
    return-void
.end method

.method private static n()V
    .registers 0

    .prologue
    .line 582
    return-void
.end method

.method private static o()V
    .registers 0

    .prologue
    .line 590
    return-void
.end method

.method private static p()V
    .registers 0

    .prologue
    .line 602
    return-void
.end method
