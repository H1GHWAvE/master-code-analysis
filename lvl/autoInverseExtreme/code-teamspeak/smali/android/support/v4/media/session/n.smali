.class Landroid/support/v4/media/session/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/media/session/m;


# instance fields
.field protected final a:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/media/session/MediaSessionCompat$Token;)V
    .registers 4

    .prologue
    .line 1149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2745
    iget-object v0, p2, Landroid/support/v4/media/session/MediaSessionCompat$Token;->a:Ljava/lang/Object;

    .line 1150
    invoke-static {p1, v0}, Landroid/support/v4/media/session/v;->a(Landroid/content/Context;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/media/session/n;->a:Ljava/lang/Object;

    .line 1152
    iget-object v0, p0, Landroid/support/v4/media/session/n;->a:Ljava/lang/Object;

    if-nez v0, :cond_15

    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0

    .line 1153
    :cond_15
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/media/session/MediaSessionCompat;)V
    .registers 4

    .prologue
    .line 1143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1144
    invoke-virtual {p2}, Landroid/support/v4/media/session/MediaSessionCompat;->a()Landroid/support/v4/media/session/MediaSessionCompat$Token;

    move-result-object v0

    .line 1745
    iget-object v0, v0, Landroid/support/v4/media/session/MediaSessionCompat$Token;->a:Ljava/lang/Object;

    .line 1144
    invoke-static {p1, v0}, Landroid/support/v4/media/session/v;->a(Landroid/content/Context;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/media/session/n;->a:Ljava/lang/Object;

    .line 1146
    return-void
.end method


# virtual methods
.method public a()Landroid/support/v4/media/session/r;
    .registers 3

    .prologue
    .line 1172
    iget-object v0, p0, Landroid/support/v4/media/session/n;->a:Ljava/lang/Object;

    .line 4056
    check-cast v0, Landroid/media/session/MediaController;

    invoke-virtual {v0}, Landroid/media/session/MediaController;->getTransportControls()Landroid/media/session/MediaController$TransportControls;

    move-result-object v1

    .line 1173
    if-eqz v1, :cond_10

    new-instance v0, Landroid/support/v4/media/session/s;

    invoke-direct {v0, v1}, Landroid/support/v4/media/session/s;-><init>(Ljava/lang/Object;)V

    :goto_f
    return-object v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public final a(II)V
    .registers 4

    .prologue
    .line 1240
    iget-object v0, p0, Landroid/support/v4/media/session/n;->a:Ljava/lang/Object;

    .line 7105
    check-cast v0, Landroid/media/session/MediaController;

    invoke-virtual {v0, p1, p2}, Landroid/media/session/MediaController;->setVolumeTo(II)V

    .line 1241
    return-void
.end method

.method public final a(Landroid/support/v4/media/session/i;)V
    .registers 4

    .prologue
    .line 1162
    iget-object v0, p0, Landroid/support/v4/media/session/n;->a:Ljava/lang/Object;

    invoke-static {p1}, Landroid/support/v4/media/session/i;->c(Landroid/support/v4/media/session/i;)Ljava/lang/Object;

    move-result-object v1

    .line 3051
    check-cast v0, Landroid/media/session/MediaController;

    check-cast v1, Landroid/media/session/MediaController$Callback;

    invoke-virtual {v0, v1}, Landroid/media/session/MediaController;->unregisterCallback(Landroid/media/session/MediaController$Callback;)V

    .line 1163
    return-void
.end method

.method public final a(Landroid/support/v4/media/session/i;Landroid/os/Handler;)V
    .registers 5

    .prologue
    .line 1157
    iget-object v0, p0, Landroid/support/v4/media/session/n;->a:Ljava/lang/Object;

    invoke-static {p1}, Landroid/support/v4/media/session/i;->c(Landroid/support/v4/media/session/i;)Ljava/lang/Object;

    move-result-object v1

    .line 3046
    check-cast v0, Landroid/media/session/MediaController;

    check-cast v1, Landroid/media/session/MediaController$Callback;

    invoke-virtual {v0, v1, p2}, Landroid/media/session/MediaController;->registerCallback(Landroid/media/session/MediaController$Callback;Landroid/os/Handler;)V

    .line 1158
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;Landroid/os/ResultReceiver;)V
    .registers 5

    .prologue
    .line 1250
    iget-object v0, p0, Landroid/support/v4/media/session/n;->a:Ljava/lang/Object;

    .line 7114
    check-cast v0, Landroid/media/session/MediaController;

    invoke-virtual {v0, p1, p2, p3}, Landroid/media/session/MediaController;->sendCommand(Ljava/lang/String;Landroid/os/Bundle;Landroid/os/ResultReceiver;)V

    .line 1251
    return-void
.end method

.method public final a(Landroid/view/KeyEvent;)Z
    .registers 3

    .prologue
    .line 1167
    iget-object v0, p0, Landroid/support/v4/media/session/n;->a:Ljava/lang/Object;

    .line 3101
    check-cast v0, Landroid/media/session/MediaController;

    invoke-virtual {v0, p1}, Landroid/media/session/MediaController;->dispatchMediaButtonEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    .line 1167
    return v0
.end method

.method public final b()Landroid/support/v4/media/session/PlaybackStateCompat;
    .registers 2

    .prologue
    .line 1178
    iget-object v0, p0, Landroid/support/v4/media/session/n;->a:Ljava/lang/Object;

    .line 4060
    check-cast v0, Landroid/media/session/MediaController;

    invoke-virtual {v0}, Landroid/media/session/MediaController;->getPlaybackState()Landroid/media/session/PlaybackState;

    move-result-object v0

    .line 1179
    if-eqz v0, :cond_f

    invoke-static {v0}, Landroid/support/v4/media/session/PlaybackStateCompat;->a(Ljava/lang/Object;)Landroid/support/v4/media/session/PlaybackStateCompat;

    move-result-object v0

    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public final b(II)V
    .registers 4

    .prologue
    .line 1245
    iget-object v0, p0, Landroid/support/v4/media/session/n;->a:Ljava/lang/Object;

    .line 7109
    check-cast v0, Landroid/media/session/MediaController;

    invoke-virtual {v0, p1, p2}, Landroid/media/session/MediaController;->adjustVolume(II)V

    .line 1246
    return-void
.end method

.method public final c()Landroid/support/v4/media/MediaMetadataCompat;
    .registers 2

    .prologue
    .line 1184
    iget-object v0, p0, Landroid/support/v4/media/session/n;->a:Ljava/lang/Object;

    .line 4064
    check-cast v0, Landroid/media/session/MediaController;

    invoke-virtual {v0}, Landroid/media/session/MediaController;->getMetadata()Landroid/media/MediaMetadata;

    move-result-object v0

    .line 1185
    if-eqz v0, :cond_f

    invoke-static {v0}, Landroid/support/v4/media/MediaMetadataCompat;->a(Ljava/lang/Object;)Landroid/support/v4/media/MediaMetadataCompat;

    move-result-object v0

    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public final d()Ljava/util/List;
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 1190
    iget-object v0, p0, Landroid/support/v4/media/session/n;->a:Ljava/lang/Object;

    .line 4068
    check-cast v0, Landroid/media/session/MediaController;

    invoke-virtual {v0}, Landroid/media/session/MediaController;->getQueue()Ljava/util/List;

    move-result-object v2

    .line 4069
    if-nez v2, :cond_10

    move-object v2, v1

    .line 1191
    :goto_c
    if-nez v2, :cond_17

    move-object v0, v1

    .line 1199
    :cond_f
    return-object v0

    .line 4072
    :cond_10
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object v2, v0

    goto :goto_c

    .line 1194
    :cond_17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1196
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_20
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 1197
    invoke-static {v2}, Landroid/support/v4/media/session/MediaSessionCompat$QueueItem;->a(Ljava/lang/Object;)Landroid/support/v4/media/session/MediaSessionCompat$QueueItem;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_20
.end method

.method public final e()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 1204
    iget-object v0, p0, Landroid/support/v4/media/session/n;->a:Ljava/lang/Object;

    .line 4077
    check-cast v0, Landroid/media/session/MediaController;

    invoke-virtual {v0}, Landroid/media/session/MediaController;->getQueueTitle()Ljava/lang/CharSequence;

    move-result-object v0

    .line 1204
    return-object v0
.end method

.method public final f()Landroid/os/Bundle;
    .registers 2

    .prologue
    .line 1209
    iget-object v0, p0, Landroid/support/v4/media/session/n;->a:Ljava/lang/Object;

    .line 4081
    check-cast v0, Landroid/media/session/MediaController;

    invoke-virtual {v0}, Landroid/media/session/MediaController;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1209
    return-object v0
.end method

.method public final g()I
    .registers 2

    .prologue
    .line 1214
    iget-object v0, p0, Landroid/support/v4/media/session/n;->a:Ljava/lang/Object;

    .line 4085
    check-cast v0, Landroid/media/session/MediaController;

    invoke-virtual {v0}, Landroid/media/session/MediaController;->getRatingType()I

    move-result v0

    .line 1214
    return v0
.end method

.method public final h()J
    .registers 3

    .prologue
    .line 1219
    iget-object v0, p0, Landroid/support/v4/media/session/n;->a:Ljava/lang/Object;

    .line 4089
    check-cast v0, Landroid/media/session/MediaController;

    invoke-virtual {v0}, Landroid/media/session/MediaController;->getFlags()J

    move-result-wide v0

    .line 1219
    return-wide v0
.end method

.method public final i()Landroid/support/v4/media/session/q;
    .registers 9

    .prologue
    const/4 v6, 0x4

    const/4 v3, 0x3

    const/4 v4, 0x1

    .line 1224
    iget-object v0, p0, Landroid/support/v4/media/session/n;->a:Ljava/lang/Object;

    .line 4093
    check-cast v0, Landroid/media/session/MediaController;

    invoke-virtual {v0}, Landroid/media/session/MediaController;->getPlaybackInfo()Landroid/media/session/MediaController$PlaybackInfo;

    move-result-object v5

    .line 1225
    if-eqz v5, :cond_60

    new-instance v0, Landroid/support/v4/media/session/q;

    move-object v1, v5

    .line 4177
    check-cast v1, Landroid/media/session/MediaController$PlaybackInfo;

    invoke-virtual {v1}, Landroid/media/session/MediaController$PlaybackInfo;->getPlaybackType()I

    move-result v1

    move-object v2, v5

    .line 5181
    check-cast v2, Landroid/media/session/MediaController$PlaybackInfo;

    invoke-virtual {v2}, Landroid/media/session/MediaController$PlaybackInfo;->getAudioAttributes()Landroid/media/AudioAttributes;

    move-result-object v2

    .line 5209
    invoke-virtual {v2}, Landroid/media/AudioAttributes;->getFlags()I

    move-result v7

    and-int/lit8 v7, v7, 0x1

    if-ne v7, v4, :cond_3e

    .line 5211
    const/4 v2, 0x7

    :goto_26
    move-object v3, v5

    .line 6190
    check-cast v3, Landroid/media/session/MediaController$PlaybackInfo;

    invoke-virtual {v3}, Landroid/media/session/MediaController$PlaybackInfo;->getVolumeControl()I

    move-result v3

    move-object v4, v5

    .line 6194
    check-cast v4, Landroid/media/session/MediaController$PlaybackInfo;

    invoke-virtual {v4}, Landroid/media/session/MediaController$PlaybackInfo;->getMaxVolume()I

    move-result v4

    .line 6198
    check-cast v5, Landroid/media/session/MediaController$PlaybackInfo;

    invoke-virtual {v5}, Landroid/media/session/MediaController$PlaybackInfo;->getCurrentVolume()I

    move-result v5

    .line 1225
    invoke-direct/range {v0 .. v5}, Landroid/support/v4/media/session/q;-><init>(IIIII)V

    :goto_3d
    return-object v0

    .line 5213
    :cond_3e
    invoke-virtual {v2}, Landroid/media/AudioAttributes;->getFlags()I

    move-result v7

    and-int/lit8 v7, v7, 0x4

    if-ne v7, v6, :cond_48

    .line 5214
    const/4 v2, 0x6

    goto :goto_26

    .line 5218
    :cond_48
    invoke-virtual {v2}, Landroid/media/AudioAttributes;->getUsage()I

    move-result v2

    packed-switch v2, :pswitch_data_62

    move v2, v3

    .line 5242
    goto :goto_26

    :pswitch_51
    move v2, v3

    .line 5223
    goto :goto_26

    :pswitch_53
    move v2, v4

    .line 5225
    goto :goto_26

    .line 5227
    :pswitch_55
    const/4 v2, 0x0

    goto :goto_26

    .line 5229
    :pswitch_57
    const/16 v2, 0x8

    goto :goto_26

    :pswitch_5a
    move v2, v6

    .line 5231
    goto :goto_26

    .line 5233
    :pswitch_5c
    const/4 v2, 0x2

    goto :goto_26

    .line 5239
    :pswitch_5e
    const/4 v2, 0x5

    goto :goto_26

    .line 1225
    :cond_60
    const/4 v0, 0x0

    goto :goto_3d

    .line 5218
    :pswitch_data_62
    .packed-switch 0x1
        :pswitch_51
        :pswitch_55
        :pswitch_57
        :pswitch_5a
        :pswitch_5e
        :pswitch_5c
        :pswitch_5e
        :pswitch_5e
        :pswitch_5e
        :pswitch_5e
        :pswitch_51
        :pswitch_51
        :pswitch_53
        :pswitch_51
    .end packed-switch
.end method

.method public final j()Landroid/app/PendingIntent;
    .registers 2

    .prologue
    .line 1235
    iget-object v0, p0, Landroid/support/v4/media/session/n;->a:Ljava/lang/Object;

    .line 7097
    check-cast v0, Landroid/media/session/MediaController;

    invoke-virtual {v0}, Landroid/media/session/MediaController;->getSessionActivity()Landroid/app/PendingIntent;

    move-result-object v0

    .line 1235
    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1255
    iget-object v0, p0, Landroid/support/v4/media/session/n;->a:Ljava/lang/Object;

    .line 7118
    check-cast v0, Landroid/media/session/MediaController;

    invoke-virtual {v0}, Landroid/media/session/MediaController;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 1255
    return-object v0
.end method

.method public final l()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1260
    iget-object v0, p0, Landroid/support/v4/media/session/n;->a:Ljava/lang/Object;

    return-object v0
.end method
