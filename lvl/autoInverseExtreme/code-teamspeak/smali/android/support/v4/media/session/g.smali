.class public final Landroid/support/v4/media/session/g;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String; = "MediaControllerCompat"


# instance fields
.field private final b:Landroid/support/v4/media/session/m;

.field private final c:Landroid/support/v4/media/session/MediaSessionCompat$Token;


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/support/v4/media/session/MediaSessionCompat$Token;)V
    .registers 5

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    if-nez p2, :cond_d

    .line 91
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "sessionToken must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 93
    :cond_d
    iput-object p2, p0, Landroid/support/v4/media/session/g;->c:Landroid/support/v4/media/session/MediaSessionCompat$Token;

    .line 95
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1d

    .line 96
    new-instance v0, Landroid/support/v4/media/session/n;

    invoke-direct {v0, p1, p2}, Landroid/support/v4/media/session/n;-><init>(Landroid/content/Context;Landroid/support/v4/media/session/MediaSessionCompat$Token;)V

    iput-object v0, p0, Landroid/support/v4/media/session/g;->b:Landroid/support/v4/media/session/m;

    .line 100
    :goto_1c
    return-void

    .line 98
    :cond_1d
    new-instance v0, Landroid/support/v4/media/session/p;

    iget-object v1, p0, Landroid/support/v4/media/session/g;->c:Landroid/support/v4/media/session/MediaSessionCompat$Token;

    invoke-direct {v0, v1}, Landroid/support/v4/media/session/p;-><init>(Landroid/support/v4/media/session/MediaSessionCompat$Token;)V

    iput-object v0, p0, Landroid/support/v4/media/session/g;->b:Landroid/support/v4/media/session/m;

    goto :goto_1c
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/media/session/MediaSessionCompat;)V
    .registers 5

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    if-nez p2, :cond_d

    .line 68
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "session must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_d
    invoke-virtual {p2}, Landroid/support/v4/media/session/MediaSessionCompat;->a()Landroid/support/v4/media/session/MediaSessionCompat$Token;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/media/session/g;->c:Landroid/support/v4/media/session/MediaSessionCompat$Token;

    .line 72
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_21

    .line 73
    new-instance v0, Landroid/support/v4/media/session/o;

    invoke-direct {v0, p1, p2}, Landroid/support/v4/media/session/o;-><init>(Landroid/content/Context;Landroid/support/v4/media/session/MediaSessionCompat;)V

    iput-object v0, p0, Landroid/support/v4/media/session/g;->b:Landroid/support/v4/media/session/m;

    .line 79
    :goto_20
    return-void

    .line 74
    :cond_21
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_2f

    .line 75
    new-instance v0, Landroid/support/v4/media/session/n;

    invoke-direct {v0, p1, p2}, Landroid/support/v4/media/session/n;-><init>(Landroid/content/Context;Landroid/support/v4/media/session/MediaSessionCompat;)V

    iput-object v0, p0, Landroid/support/v4/media/session/g;->b:Landroid/support/v4/media/session/m;

    goto :goto_20

    .line 77
    :cond_2f
    new-instance v0, Landroid/support/v4/media/session/p;

    iget-object v1, p0, Landroid/support/v4/media/session/g;->c:Landroid/support/v4/media/session/MediaSessionCompat$Token;

    invoke-direct {v0, v1}, Landroid/support/v4/media/session/p;-><init>(Landroid/support/v4/media/session/MediaSessionCompat$Token;)V

    iput-object v0, p0, Landroid/support/v4/media/session/g;->b:Landroid/support/v4/media/session/m;

    goto :goto_20
.end method

.method private a()Landroid/support/v4/media/session/r;
    .registers 2

    .prologue
    .line 108
    iget-object v0, p0, Landroid/support/v4/media/session/g;->b:Landroid/support/v4/media/session/m;

    invoke-interface {v0}, Landroid/support/v4/media/session/m;->a()Landroid/support/v4/media/session/r;

    move-result-object v0

    return-object v0
.end method

.method private a(II)V
    .registers 4

    .prologue
    .line 235
    iget-object v0, p0, Landroid/support/v4/media/session/g;->b:Landroid/support/v4/media/session/m;

    invoke-interface {v0, p1, p2}, Landroid/support/v4/media/session/m;->a(II)V

    .line 236
    return-void
.end method

.method private a(Landroid/support/v4/media/session/i;)V
    .registers 4

    .prologue
    .line 262
    .line 1274
    if-nez p1, :cond_a

    .line 1275
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "callback cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1278
    :cond_a
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 1280
    iget-object v1, p0, Landroid/support/v4/media/session/g;->b:Landroid/support/v4/media/session/m;

    invoke-interface {v1, p1, v0}, Landroid/support/v4/media/session/m;->a(Landroid/support/v4/media/session/i;Landroid/os/Handler;)V

    .line 263
    return-void
.end method

.method private a(Ljava/lang/String;Landroid/os/Bundle;Landroid/os/ResultReceiver;)V
    .registers 6

    .prologue
    .line 306
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 307
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "command cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 309
    :cond_e
    iget-object v0, p0, Landroid/support/v4/media/session/g;->b:Landroid/support/v4/media/session/m;

    invoke-interface {v0, p1, p2, p3}, Landroid/support/v4/media/session/m;->a(Ljava/lang/String;Landroid/os/Bundle;Landroid/os/ResultReceiver;)V

    .line 310
    return-void
.end method

.method private a(Landroid/view/KeyEvent;)Z
    .registers 4

    .prologue
    .line 119
    if-nez p1, :cond_a

    .line 120
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "KeyEvent may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 122
    :cond_a
    iget-object v0, p0, Landroid/support/v4/media/session/g;->b:Landroid/support/v4/media/session/m;

    invoke-interface {v0, p1}, Landroid/support/v4/media/session/m;->a(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method private b()Landroid/support/v4/media/session/PlaybackStateCompat;
    .registers 2

    .prologue
    .line 131
    iget-object v0, p0, Landroid/support/v4/media/session/g;->b:Landroid/support/v4/media/session/m;

    invoke-interface {v0}, Landroid/support/v4/media/session/m;->b()Landroid/support/v4/media/session/PlaybackStateCompat;

    move-result-object v0

    return-object v0
.end method

.method private b(II)V
    .registers 4

    .prologue
    .line 252
    iget-object v0, p0, Landroid/support/v4/media/session/g;->b:Landroid/support/v4/media/session/m;

    invoke-interface {v0, p1, p2}, Landroid/support/v4/media/session/m;->b(II)V

    .line 253
    return-void
.end method

.method private b(Landroid/support/v4/media/session/i;)V
    .registers 4

    .prologue
    .line 274
    if-nez p1, :cond_a

    .line 275
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "callback cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 278
    :cond_a
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 280
    iget-object v1, p0, Landroid/support/v4/media/session/g;->b:Landroid/support/v4/media/session/m;

    invoke-interface {v1, p1, v0}, Landroid/support/v4/media/session/m;->a(Landroid/support/v4/media/session/i;Landroid/os/Handler;)V

    .line 281
    return-void
.end method

.method private c()Landroid/support/v4/media/MediaMetadataCompat;
    .registers 2

    .prologue
    .line 140
    iget-object v0, p0, Landroid/support/v4/media/session/g;->b:Landroid/support/v4/media/session/m;

    invoke-interface {v0}, Landroid/support/v4/media/session/m;->c()Landroid/support/v4/media/MediaMetadataCompat;

    move-result-object v0

    return-object v0
.end method

.method private c(Landroid/support/v4/media/session/i;)V
    .registers 4

    .prologue
    .line 290
    if-nez p1, :cond_a

    .line 291
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "callback cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 293
    :cond_a
    iget-object v0, p0, Landroid/support/v4/media/session/g;->b:Landroid/support/v4/media/session/m;

    invoke-interface {v0, p1}, Landroid/support/v4/media/session/m;->a(Landroid/support/v4/media/session/i;)V

    .line 294
    return-void
.end method

.method private d()Ljava/util/List;
    .registers 2

    .prologue
    .line 150
    iget-object v0, p0, Landroid/support/v4/media/session/g;->b:Landroid/support/v4/media/session/m;

    invoke-interface {v0}, Landroid/support/v4/media/session/m;->d()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private e()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 157
    iget-object v0, p0, Landroid/support/v4/media/session/g;->b:Landroid/support/v4/media/session/m;

    invoke-interface {v0}, Landroid/support/v4/media/session/m;->e()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private f()Landroid/os/Bundle;
    .registers 2

    .prologue
    .line 164
    iget-object v0, p0, Landroid/support/v4/media/session/g;->b:Landroid/support/v4/media/session/m;

    invoke-interface {v0}, Landroid/support/v4/media/session/m;->f()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method private g()I
    .registers 2

    .prologue
    .line 182
    iget-object v0, p0, Landroid/support/v4/media/session/g;->b:Landroid/support/v4/media/session/m;

    invoke-interface {v0}, Landroid/support/v4/media/session/m;->g()I

    move-result v0

    return v0
.end method

.method private h()J
    .registers 3

    .prologue
    .line 192
    iget-object v0, p0, Landroid/support/v4/media/session/g;->b:Landroid/support/v4/media/session/m;

    invoke-interface {v0}, Landroid/support/v4/media/session/m;->h()J

    move-result-wide v0

    return-wide v0
.end method

.method private i()Landroid/support/v4/media/session/q;
    .registers 2

    .prologue
    .line 201
    iget-object v0, p0, Landroid/support/v4/media/session/g;->b:Landroid/support/v4/media/session/m;

    invoke-interface {v0}, Landroid/support/v4/media/session/m;->i()Landroid/support/v4/media/session/q;

    move-result-object v0

    return-object v0
.end method

.method private j()Landroid/app/PendingIntent;
    .registers 2

    .prologue
    .line 211
    iget-object v0, p0, Landroid/support/v4/media/session/g;->b:Landroid/support/v4/media/session/m;

    invoke-interface {v0}, Landroid/support/v4/media/session/m;->j()Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private k()Landroid/support/v4/media/session/MediaSessionCompat$Token;
    .registers 2

    .prologue
    .line 220
    iget-object v0, p0, Landroid/support/v4/media/session/g;->c:Landroid/support/v4/media/session/MediaSessionCompat$Token;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .registers 2

    .prologue
    .line 318
    iget-object v0, p0, Landroid/support/v4/media/session/g;->b:Landroid/support/v4/media/session/m;

    invoke-interface {v0}, Landroid/support/v4/media/session/m;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private m()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 332
    iget-object v0, p0, Landroid/support/v4/media/session/g;->b:Landroid/support/v4/media/session/m;

    invoke-interface {v0}, Landroid/support/v4/media/session/m;->l()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
