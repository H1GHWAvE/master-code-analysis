.class final Landroid/support/v4/e/a/l;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 47
    instance-of v0, p0, Landroid/support/v4/e/a/r;

    if-nez v0, :cond_a

    .line 48
    new-instance v0, Landroid/support/v4/e/a/r;

    invoke-direct {v0, p0}, Landroid/support/v4/e/a/r;-><init>(Landroid/graphics/drawable/Drawable;)V

    move-object p0, v0

    .line 50
    :cond_a
    return-object p0
.end method

.method public static a(Landroid/graphics/drawable/Drawable;I)V
    .registers 3

    .prologue
    .line 29
    instance-of v0, p0, Landroid/support/v4/e/a/q;

    if-eqz v0, :cond_9

    .line 30
    check-cast p0, Landroid/support/v4/e/a/q;

    invoke-interface {p0, p1}, Landroid/support/v4/e/a/q;->setTint(I)V

    .line 32
    :cond_9
    return-void
.end method

.method public static a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V
    .registers 3

    .prologue
    .line 35
    instance-of v0, p0, Landroid/support/v4/e/a/q;

    if-eqz v0, :cond_9

    .line 36
    check-cast p0, Landroid/support/v4/e/a/q;

    invoke-interface {p0, p1}, Landroid/support/v4/e/a/q;->setTintList(Landroid/content/res/ColorStateList;)V

    .line 38
    :cond_9
    return-void
.end method

.method public static a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V
    .registers 3

    .prologue
    .line 41
    instance-of v0, p0, Landroid/support/v4/e/a/q;

    if-eqz v0, :cond_9

    .line 42
    check-cast p0, Landroid/support/v4/e/a/q;

    invoke-interface {p0, p1}, Landroid/support/v4/e/a/q;->setTintMode(Landroid/graphics/PorterDuff$Mode;)V

    .line 44
    :cond_9
    return-void
.end method
