.class public final Landroid/support/v4/e/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Landroid/support/v4/e/c;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 81
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 82
    const/16 v1, 0x13

    if-lt v0, v1, :cond_e

    .line 83
    new-instance v0, Landroid/support/v4/e/f;

    invoke-direct {v0}, Landroid/support/v4/e/f;-><init>()V

    sput-object v0, Landroid/support/v4/e/a;->a:Landroid/support/v4/e/c;

    .line 91
    :goto_d
    return-void

    .line 84
    :cond_e
    const/16 v1, 0x12

    if-lt v0, v1, :cond_1a

    .line 85
    new-instance v0, Landroid/support/v4/e/e;

    invoke-direct {v0}, Landroid/support/v4/e/e;-><init>()V

    sput-object v0, Landroid/support/v4/e/a;->a:Landroid/support/v4/e/c;

    goto :goto_d

    .line 86
    :cond_1a
    const/16 v1, 0xc

    if-lt v0, v1, :cond_26

    .line 87
    new-instance v0, Landroid/support/v4/e/d;

    invoke-direct {v0}, Landroid/support/v4/e/d;-><init>()V

    sput-object v0, Landroid/support/v4/e/a;->a:Landroid/support/v4/e/c;

    goto :goto_d

    .line 89
    :cond_26
    new-instance v0, Landroid/support/v4/e/b;

    invoke-direct {v0}, Landroid/support/v4/e/b;-><init>()V

    sput-object v0, Landroid/support/v4/e/a;->a:Landroid/support/v4/e/c;

    goto :goto_d
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    return-void
.end method

.method public static a(Landroid/graphics/Bitmap;Z)V
    .registers 3

    .prologue
    .line 98
    sget-object v0, Landroid/support/v4/e/a;->a:Landroid/support/v4/e/c;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/e/c;->a(Landroid/graphics/Bitmap;Z)V

    .line 99
    return-void
.end method

.method public static a(Landroid/graphics/Bitmap;)Z
    .registers 2

    .prologue
    .line 94
    sget-object v0, Landroid/support/v4/e/a;->a:Landroid/support/v4/e/c;

    invoke-interface {v0, p0}, Landroid/support/v4/e/c;->a(Landroid/graphics/Bitmap;)Z

    move-result v0

    return v0
.end method

.method private static b(Landroid/graphics/Bitmap;)I
    .registers 2

    .prologue
    .line 109
    sget-object v0, Landroid/support/v4/e/a;->a:Landroid/support/v4/e/c;

    invoke-interface {v0, p0}, Landroid/support/v4/e/c;->b(Landroid/graphics/Bitmap;)I

    move-result v0

    return v0
.end method
