.class public final Landroid/support/v4/e/j;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:I = 0xa

.field private static final b:I = 0xa


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(I)D
    .registers 9

    .prologue
    .line 64
    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v0

    int-to-double v0, v0

    const-wide v2, 0x406fe00000000000L    # 255.0

    div-double/2addr v0, v2

    .line 65
    const-wide v2, 0x3fa41c8216c61523L    # 0.03928

    cmpg-double v2, v0, v2

    if-gez v2, :cond_63

    const-wide v2, 0x4029d70a3d70a3d7L    # 12.92

    div-double/2addr v0, v2

    .line 67
    :goto_1a
    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v2

    int-to-double v2, v2

    const-wide v4, 0x406fe00000000000L    # 255.0

    div-double/2addr v2, v4

    .line 68
    const-wide v4, 0x3fa41c8216c61523L    # 0.03928

    cmpg-double v4, v2, v4

    if-gez v4, :cond_79

    const-wide v4, 0x4029d70a3d70a3d7L    # 12.92

    div-double/2addr v2, v4

    .line 70
    :goto_34
    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v4

    int-to-double v4, v4

    const-wide v6, 0x406fe00000000000L    # 255.0

    div-double/2addr v4, v6

    .line 71
    const-wide v6, 0x3fa41c8216c61523L    # 0.03928

    cmpg-double v6, v4, v6

    if-gez v6, :cond_8f

    const-wide v6, 0x4029d70a3d70a3d7L    # 12.92

    div-double/2addr v4, v6

    .line 73
    :goto_4e
    const-wide v6, 0x3fcb367a0f9096bcL    # 0.2126

    mul-double/2addr v0, v6

    const-wide v6, 0x3fe6e2eb1c432ca5L    # 0.7152

    mul-double/2addr v2, v6

    add-double/2addr v0, v2

    const-wide v2, 0x3fb27bb2fec56d5dL    # 0.0722

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    return-wide v0

    .line 65
    :cond_63
    const-wide v2, 0x3fac28f5c28f5c29L    # 0.055

    add-double/2addr v0, v2

    const-wide v2, 0x3ff0e147ae147ae1L    # 1.055

    div-double/2addr v0, v2

    const-wide v2, 0x4003333333333333L    # 2.4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    goto :goto_1a

    .line 68
    :cond_79
    const-wide v4, 0x3fac28f5c28f5c29L    # 0.055

    add-double/2addr v2, v4

    const-wide v4, 0x3ff0e147ae147ae1L    # 1.055

    div-double/2addr v2, v4

    const-wide v4, 0x4003333333333333L    # 2.4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    goto :goto_34

    .line 71
    :cond_8f
    const-wide v6, 0x3fac28f5c28f5c29L    # 0.055

    add-double/2addr v4, v6

    const-wide v6, 0x3ff0e147ae147ae1L    # 1.055

    div-double/2addr v4, v6

    const-wide v6, 0x4003333333333333L    # 2.4

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    goto :goto_4e
.end method

.method private static a(FF)F
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 290
    cmpg-float v1, p0, v0

    if-gez v1, :cond_7

    move p1, v0

    :cond_6
    :goto_6
    return p1

    :cond_7
    cmpl-float v0, p0, p1

    if-gtz v0, :cond_6

    move p1, p0

    goto :goto_6
.end method

.method public static a(II)I
    .registers 9

    .prologue
    .line 35
    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    .line 36
    invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I

    move-result v1

    .line 1050
    rsub-int v2, v0, 0xff

    rsub-int v3, v1, 0xff

    mul-int/2addr v2, v3

    div-int/lit16 v2, v2, 0xff

    rsub-int v2, v2, 0xff

    .line 39
    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v3

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v4

    invoke-static {v3, v1, v4, v0, v2}, Landroid/support/v4/e/j;->a(IIIII)I

    move-result v3

    .line 41
    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v4

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v5

    invoke-static {v4, v1, v5, v0, v2}, Landroid/support/v4/e/j;->a(IIIII)I

    move-result v4

    .line 43
    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v5

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v6

    invoke-static {v5, v1, v6, v0, v2}, Landroid/support/v4/e/j;->a(IIIII)I

    move-result v0

    .line 46
    invoke-static {v2, v3, v4, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method private static a(IIF)I
    .registers 12

    .prologue
    const/16 v8, 0xa

    const/4 v2, 0x0

    const/16 v0, 0xff

    .line 111
    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v1

    if-eq v1, v0, :cond_13

    .line 112
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "background can not be translucent"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 116
    :cond_13
    invoke-static {p0, v0}, Landroid/support/v4/e/j;->b(II)I

    move-result v1

    .line 117
    invoke-static {v1, p1}, Landroid/support/v4/e/j;->d(II)D

    move-result-wide v4

    .line 118
    float-to-double v6, p2

    cmpg-double v1, v4, v6

    if-gez v1, :cond_22

    .line 120
    const/4 v0, -0x1

    .line 145
    :cond_21
    return v0

    :cond_22
    move v3, v2

    .line 128
    :goto_23
    if-gt v3, v8, :cond_21

    sub-int v1, v0, v2

    if-le v1, v8, :cond_21

    .line 130
    add-int v1, v2, v0

    div-int/lit8 v1, v1, 0x2

    .line 132
    invoke-static {p0, v1}, Landroid/support/v4/e/j;->b(II)I

    move-result v4

    .line 133
    invoke-static {v4, p1}, Landroid/support/v4/e/j;->d(II)D

    move-result-wide v4

    .line 135
    float-to-double v6, p2

    cmpg-double v4, v4, v6

    if-gez v4, :cond_3f

    .line 141
    :goto_3a
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    .line 142
    goto :goto_23

    :cond_3f
    move v0, v1

    move v1, v2

    .line 138
    goto :goto_3a
.end method

.method private static a(IIIII)I
    .registers 8

    .prologue
    .line 54
    if-nez p4, :cond_4

    const/4 v0, 0x0

    .line 55
    :goto_3
    return v0

    :cond_4
    mul-int/lit16 v0, p0, 0xff

    mul-int/2addr v0, p1

    mul-int v1, p2, p3

    rsub-int v2, p1, 0xff

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    mul-int/lit16 v1, p4, 0xff

    div-int/2addr v0, v1

    goto :goto_3
.end method

.method private static a([F)I
    .registers 9

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/4 v0, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v6, 0x437f0000    # 255.0f

    .line 226
    aget v1, p0, v0

    .line 227
    const/4 v2, 0x1

    aget v2, p0, v2

    .line 228
    const/4 v3, 0x2

    aget v3, p0, v3

    .line 230
    mul-float v4, v7, v3

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    sub-float v4, v5, v4

    mul-float/2addr v4, v2

    .line 231
    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v2, v4

    sub-float/2addr v3, v2

    .line 232
    const/high16 v2, 0x42700000    # 60.0f

    div-float v2, v1, v2

    rem-float/2addr v2, v7

    sub-float/2addr v2, v5

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    sub-float v2, v5, v2

    mul-float v5, v4, v2

    .line 234
    float-to-int v1, v1

    div-int/lit8 v1, v1, 0x3c

    .line 238
    packed-switch v1, :pswitch_data_c4

    move v1, v0

    move v2, v0

    .line 272
    :goto_33
    invoke-static {v2}, Landroid/support/v4/e/j;->b(I)I

    move-result v2

    .line 273
    invoke-static {v1}, Landroid/support/v4/e/j;->b(I)I

    move-result v1

    .line 274
    invoke-static {v0}, Landroid/support/v4/e/j;->b(I)I

    move-result v0

    .line 276
    invoke-static {v2, v1, v0}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    return v0

    .line 240
    :pswitch_44
    add-float v0, v4, v3

    mul-float/2addr v0, v6

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 241
    add-float v0, v5, v3

    mul-float/2addr v0, v6

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 242
    mul-float v0, v6, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_33

    .line 245
    :pswitch_59
    add-float v0, v5, v3

    mul-float/2addr v0, v6

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 246
    add-float v0, v4, v3

    mul-float/2addr v0, v6

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 247
    mul-float v0, v6, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_33

    .line 250
    :pswitch_6e
    mul-float v0, v6, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 251
    add-float v0, v4, v3

    mul-float/2addr v0, v6

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 252
    add-float v0, v5, v3

    mul-float/2addr v0, v6

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_33

    .line 255
    :pswitch_83
    mul-float v0, v6, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 256
    add-float v0, v5, v3

    mul-float/2addr v0, v6

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 257
    add-float v0, v4, v3

    mul-float/2addr v0, v6

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_33

    .line 260
    :pswitch_98
    add-float v0, v5, v3

    mul-float/2addr v0, v6

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 261
    mul-float v0, v6, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 262
    add-float v0, v4, v3

    mul-float/2addr v0, v6

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_33

    .line 266
    :pswitch_ad
    add-float v0, v4, v3

    mul-float/2addr v0, v6

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 267
    mul-float v0, v6, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 268
    add-float v0, v5, v3

    mul-float/2addr v0, v6

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto/16 :goto_33

    .line 238
    nop

    :pswitch_data_c4
    .packed-switch 0x0
        :pswitch_44
        :pswitch_59
        :pswitch_6e
        :pswitch_83
        :pswitch_98
        :pswitch_ad
        :pswitch_ad
    .end packed-switch
.end method

.method private static a(III[F)V
    .registers 15

    .prologue
    const/4 v2, 0x0

    const/high16 v10, 0x43b40000    # 360.0f

    const/high16 v4, 0x437f0000    # 255.0f

    const/high16 v9, 0x40000000    # 2.0f

    const/high16 v8, 0x3f800000    # 1.0f

    .line 162
    int-to-float v0, p0

    div-float/2addr v0, v4

    .line 163
    int-to-float v1, p1

    div-float/2addr v1, v4

    .line 164
    int-to-float v3, p2

    div-float/2addr v3, v4

    .line 166
    invoke-static {v1, v3}, Ljava/lang/Math;->max(FF)F

    move-result v4

    invoke-static {v0, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 167
    invoke-static {v1, v3}, Ljava/lang/Math;->min(FF)F

    move-result v5

    invoke-static {v0, v5}, Ljava/lang/Math;->min(FF)F

    move-result v5

    .line 168
    sub-float v6, v4, v5

    .line 171
    add-float v7, v4, v5

    div-float/2addr v7, v9

    .line 173
    cmpl-float v5, v4, v5

    if-nez v5, :cond_49

    move v1, v2

    move v0, v2

    .line 188
    :goto_2a
    const/high16 v3, 0x42700000    # 60.0f

    mul-float/2addr v0, v3

    rem-float/2addr v0, v10

    .line 189
    cmpg-float v2, v0, v2

    if-gez v2, :cond_33

    .line 190
    add-float/2addr v0, v10

    .line 193
    :cond_33
    const/4 v2, 0x0

    invoke-static {v0, v10}, Landroid/support/v4/e/j;->a(FF)F

    move-result v0

    aput v0, p3, v2

    .line 194
    const/4 v0, 0x1

    invoke-static {v1, v8}, Landroid/support/v4/e/j;->a(FF)F

    move-result v1

    aput v1, p3, v0

    .line 195
    const/4 v0, 0x2

    invoke-static {v7, v8}, Landroid/support/v4/e/j;->a(FF)F

    move-result v1

    aput v1, p3, v0

    .line 196
    return-void

    .line 177
    :cond_49
    cmpl-float v5, v4, v0

    if-nez v5, :cond_5f

    .line 178
    sub-float v0, v1, v3

    div-float/2addr v0, v6

    const/high16 v1, 0x40c00000    # 6.0f

    rem-float/2addr v0, v1

    .line 185
    :goto_53
    mul-float v1, v9, v7

    sub-float/2addr v1, v8

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    sub-float v1, v8, v1

    div-float v1, v6, v1

    goto :goto_2a

    .line 179
    :cond_5f
    cmpl-float v4, v4, v1

    if-nez v4, :cond_68

    .line 180
    sub-float v0, v3, v0

    div-float/2addr v0, v6

    add-float/2addr v0, v9

    goto :goto_53

    .line 182
    :cond_68
    sub-float/2addr v0, v1

    div-float/2addr v0, v6

    const/high16 v1, 0x40800000    # 4.0f

    add-float/2addr v0, v1

    goto :goto_53
.end method

.method private static a(I[F)V
    .registers 13

    .prologue
    const/4 v2, 0x0

    const/high16 v10, 0x43b40000    # 360.0f

    const/high16 v4, 0x437f0000    # 255.0f

    const/high16 v9, 0x40000000    # 2.0f

    const/high16 v8, 0x3f800000    # 1.0f

    .line 210
    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v0

    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v1

    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    .line 1162
    int-to-float v0, v0

    div-float/2addr v0, v4

    .line 1163
    int-to-float v1, v1

    div-float/2addr v1, v4

    .line 1164
    int-to-float v3, v3

    div-float/2addr v3, v4

    .line 1166
    invoke-static {v1, v3}, Ljava/lang/Math;->max(FF)F

    move-result v4

    invoke-static {v0, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 1167
    invoke-static {v1, v3}, Ljava/lang/Math;->min(FF)F

    move-result v5

    invoke-static {v0, v5}, Ljava/lang/Math;->min(FF)F

    move-result v5

    .line 1168
    sub-float v6, v4, v5

    .line 1171
    add-float v7, v4, v5

    div-float/2addr v7, v9

    .line 1173
    cmpl-float v5, v4, v5

    if-nez v5, :cond_55

    move v1, v2

    move v0, v2

    .line 1188
    :goto_36
    const/high16 v3, 0x42700000    # 60.0f

    mul-float/2addr v0, v3

    rem-float/2addr v0, v10

    .line 1189
    cmpg-float v2, v0, v2

    if-gez v2, :cond_3f

    .line 1190
    add-float/2addr v0, v10

    .line 1193
    :cond_3f
    const/4 v2, 0x0

    invoke-static {v0, v10}, Landroid/support/v4/e/j;->a(FF)F

    move-result v0

    aput v0, p1, v2

    .line 1194
    const/4 v0, 0x1

    invoke-static {v1, v8}, Landroid/support/v4/e/j;->a(FF)F

    move-result v1

    aput v1, p1, v0

    .line 1195
    const/4 v0, 0x2

    invoke-static {v7, v8}, Landroid/support/v4/e/j;->a(FF)F

    move-result v1

    aput v1, p1, v0

    .line 211
    return-void

    .line 1177
    :cond_55
    cmpl-float v5, v4, v0

    if-nez v5, :cond_6b

    .line 1178
    sub-float v0, v1, v3

    div-float/2addr v0, v6

    const/high16 v1, 0x40c00000    # 6.0f

    rem-float/2addr v0, v1

    .line 1185
    :goto_5f
    mul-float v1, v9, v7

    sub-float/2addr v1, v8

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    sub-float v1, v8, v1

    div-float v1, v6, v1

    goto :goto_36

    .line 1179
    :cond_6b
    cmpl-float v4, v4, v1

    if-nez v4, :cond_74

    .line 1180
    sub-float v0, v3, v0

    div-float/2addr v0, v6

    add-float/2addr v0, v9

    goto :goto_5f

    .line 1182
    :cond_74
    sub-float/2addr v0, v1

    div-float/2addr v0, v6

    const/high16 v1, 0x40800000    # 4.0f

    add-float/2addr v0, v1

    goto :goto_5f
.end method

.method private static b(I)I
    .registers 2

    .prologue
    const/16 v0, 0xff

    .line 294
    if-gez p0, :cond_6

    const/4 p0, 0x0

    :cond_5
    :goto_5
    return p0

    :cond_6
    if-le p0, v0, :cond_5

    move p0, v0

    goto :goto_5
.end method

.method public static b(II)I
    .registers 4

    .prologue
    .line 283
    if-ltz p1, :cond_6

    const/16 v0, 0xff

    if-le p1, v0, :cond_e

    .line 284
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "alpha must be between 0 and 255."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 286
    :cond_e
    const v0, 0xffffff

    and-int/2addr v0, p0

    shl-int/lit8 v1, p1, 0x18

    or-int/2addr v0, v1

    return v0
.end method

.method private static c(II)I
    .registers 4

    .prologue
    .line 50
    rsub-int v0, p1, 0xff

    rsub-int v1, p0, 0xff

    mul-int/2addr v0, v1

    div-int/lit16 v0, v0, 0xff

    rsub-int v0, v0, 0xff

    return v0
.end method

.method private static d(II)D
    .registers 8

    .prologue
    const/16 v1, 0xff

    const-wide v4, 0x3fa999999999999aL    # 0.05

    .line 84
    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    if-eq v0, v1, :cond_15

    .line 85
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "background can not be translucent"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87
    :cond_15
    invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    if-ge v0, v1, :cond_1f

    .line 89
    invoke-static {p0, p1}, Landroid/support/v4/e/j;->a(II)I

    move-result p0

    .line 92
    :cond_1f
    invoke-static {p0}, Landroid/support/v4/e/j;->a(I)D

    move-result-wide v0

    add-double/2addr v0, v4

    .line 93
    invoke-static {p1}, Landroid/support/v4/e/j;->a(I)D

    move-result-wide v2

    add-double/2addr v2, v4

    .line 96
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    div-double v0, v4, v0

    return-wide v0
.end method
