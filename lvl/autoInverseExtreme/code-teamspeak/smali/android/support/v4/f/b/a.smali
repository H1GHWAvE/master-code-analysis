.class public final Landroid/support/v4/f/b/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Landroid/support/v4/f/b/g;


# instance fields
.field private b:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 52
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 53
    const/16 v1, 0x17

    if-lt v0, v1, :cond_e

    .line 54
    new-instance v0, Landroid/support/v4/f/b/b;

    invoke-direct {v0}, Landroid/support/v4/f/b/b;-><init>()V

    sput-object v0, Landroid/support/v4/f/b/a;->a:Landroid/support/v4/f/b/g;

    .line 58
    :goto_d
    return-void

    .line 56
    :cond_e
    new-instance v0, Landroid/support/v4/f/b/h;

    invoke-direct {v0}, Landroid/support/v4/f/b/h;-><init>()V

    sput-object v0, Landroid/support/v4/f/b/a;->a:Landroid/support/v4/f/b/g;

    goto :goto_d
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 2

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Landroid/support/v4/f/b/a;->b:Landroid/content/Context;

    .line 48
    return-void
.end method

.method private static a(Landroid/content/Context;)Landroid/support/v4/f/b/a;
    .registers 2

    .prologue
    .line 43
    new-instance v0, Landroid/support/v4/f/b/a;

    invoke-direct {v0, p0}, Landroid/support/v4/f/b/a;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private a(Landroid/support/v4/f/b/f;ILandroid/support/v4/i/c;Landroid/support/v4/f/b/d;Landroid/os/Handler;)V
    .registers 13
    .param p1    # Landroid/support/v4/f/b/f;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param
    .param p3    # Landroid/support/v4/i/c;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param
    .param p4    # Landroid/support/v4/f/b/d;
        .annotation build Landroid/support/a/y;
        .end annotation
    .end param
    .param p5    # Landroid/os/Handler;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 95
    sget-object v0, Landroid/support/v4/f/b/a;->a:Landroid/support/v4/f/b/g;

    iget-object v1, p0, Landroid/support/v4/f/b/a;->b:Landroid/content/Context;

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-interface/range {v0 .. v6}, Landroid/support/v4/f/b/g;->a(Landroid/content/Context;Landroid/support/v4/f/b/f;ILandroid/support/v4/i/c;Landroid/support/v4/f/b/d;Landroid/os/Handler;)V

    .line 96
    return-void
.end method

.method private a()Z
    .registers 3

    .prologue
    .line 66
    sget-object v0, Landroid/support/v4/f/b/a;->a:Landroid/support/v4/f/b/g;

    iget-object v1, p0, Landroid/support/v4/f/b/a;->b:Landroid/content/Context;

    invoke-interface {v0, v1}, Landroid/support/v4/f/b/g;->a(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private b()Z
    .registers 3

    .prologue
    .line 75
    sget-object v0, Landroid/support/v4/f/b/a;->a:Landroid/support/v4/f/b/g;

    iget-object v1, p0, Landroid/support/v4/f/b/a;->b:Landroid/content/Context;

    invoke-interface {v0, v1}, Landroid/support/v4/f/b/g;->b(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
