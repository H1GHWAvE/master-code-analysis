.class final Landroid/support/v4/f/a/b;
.super Landroid/support/v4/f/a/a;
.source "SourceFile"


# instance fields
.field private final b:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 135
    invoke-direct {p0}, Landroid/support/v4/f/a/a;-><init>()V

    .line 1024
    const-string v0, "display"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 136
    iput-object v0, p0, Landroid/support/v4/f/a/b;->b:Ljava/lang/Object;

    .line 137
    return-void
.end method


# virtual methods
.method public final a(I)Landroid/view/Display;
    .registers 3

    .prologue
    .line 141
    iget-object v0, p0, Landroid/support/v4/f/a/b;->b:Ljava/lang/Object;

    .line 1028
    check-cast v0, Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0, p1}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;

    move-result-object v0

    .line 141
    return-object v0
.end method

.method public final a()[Landroid/view/Display;
    .registers 2

    .prologue
    .line 146
    iget-object v0, p0, Landroid/support/v4/f/a/b;->b:Ljava/lang/Object;

    .line 1032
    check-cast v0, Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->getDisplays()[Landroid/view/Display;

    move-result-object v0

    .line 146
    return-object v0
.end method

.method public final a(Ljava/lang/String;)[Landroid/view/Display;
    .registers 3

    .prologue
    .line 151
    iget-object v0, p0, Landroid/support/v4/f/a/b;->b:Ljava/lang/Object;

    .line 1036
    check-cast v0, Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0, p1}, Landroid/hardware/display/DisplayManager;->getDisplays(Ljava/lang/String;)[Landroid/view/Display;

    move-result-object v0

    .line 151
    return-object v0
.end method
