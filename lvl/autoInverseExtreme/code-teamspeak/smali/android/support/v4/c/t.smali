.class public final Landroid/support/v4/c/t;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String; = "android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE"

.field public static final b:Ljava/lang/String; = "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE"

.field public static final c:Ljava/lang/String; = "android.intent.extra.changed_package_list"

.field public static final d:Ljava/lang/String; = "android.intent.extra.changed_uid_list"

.field public static final e:Ljava/lang/String; = "android.intent.extra.HTML_TEXT"

.field public static final f:I = 0x4000

.field public static final g:I = 0x8000

.field private static final h:Landroid/support/v4/c/u;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 85
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 86
    const/16 v1, 0xf

    if-lt v0, v1, :cond_e

    .line 87
    new-instance v0, Landroid/support/v4/c/x;

    invoke-direct {v0}, Landroid/support/v4/c/x;-><init>()V

    sput-object v0, Landroid/support/v4/c/t;->h:Landroid/support/v4/c/u;

    .line 93
    :goto_d
    return-void

    .line 88
    :cond_e
    const/16 v1, 0xb

    if-lt v0, v1, :cond_1a

    .line 89
    new-instance v0, Landroid/support/v4/c/w;

    invoke-direct {v0}, Landroid/support/v4/c/w;-><init>()V

    sput-object v0, Landroid/support/v4/c/t;->h:Landroid/support/v4/c/u;

    goto :goto_d

    .line 91
    :cond_1a
    new-instance v0, Landroid/support/v4/c/v;

    invoke-direct {v0}, Landroid/support/v4/c/v;-><init>()V

    sput-object v0, Landroid/support/v4/c/t;->h:Landroid/support/v4/c/u;

    goto :goto_d
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    return-void
.end method

.method public static a(Landroid/content/ComponentName;)Landroid/content/Intent;
    .registers 2

    .prologue
    .line 221
    sget-object v0, Landroid/support/v4/c/t;->h:Landroid/support/v4/c/u;

    invoke-interface {v0, p0}, Landroid/support/v4/c/u;->a(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .registers 3

    .prologue
    .line 249
    sget-object v0, Landroid/support/v4/c/t;->h:Landroid/support/v4/c/u;

    invoke-interface {v0, p0, p1}, Landroid/support/v4/c/u;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/content/ComponentName;)Landroid/content/Intent;
    .registers 2

    .prologue
    .line 266
    sget-object v0, Landroid/support/v4/c/t;->h:Landroid/support/v4/c/u;

    invoke-interface {v0, p0}, Landroid/support/v4/c/u;->b(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
