.class final Landroid/support/v4/j/i;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final d:I = 0x1

.field public static final e:I = 0x2

.field public static final f:I = 0x1

.field public static final g:I = 0x2

.field public static final h:I = 0x1

.field public static final i:I = 0x2

.field private static final m:Ljava/lang/String; = "PrintHelperKitkat"

.field private static final n:I = 0xdac


# instance fields
.field final a:Landroid/content/Context;

.field b:Landroid/graphics/BitmapFactory$Options;

.field final c:Ljava/lang/Object;

.field j:I

.field k:I

.field l:I


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .registers 4

    .prologue
    const/4 v1, 0x2

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/j/i;->b:Landroid/graphics/BitmapFactory$Options;

    .line 57
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Landroid/support/v4/j/i;->c:Ljava/lang/Object;

    .line 90
    iput v1, p0, Landroid/support/v4/j/i;->j:I

    .line 92
    iput v1, p0, Landroid/support/v4/j/i;->k:I

    .line 94
    const/4 v0, 0x1

    iput v0, p0, Landroid/support/v4/j/i;->l:I

    .line 97
    iput-object p1, p0, Landroid/support/v4/j/i;->a:Landroid/content/Context;

    .line 98
    return-void
.end method

.method private a()I
    .registers 2

    .prologue
    .line 121
    iget v0, p0, Landroid/support/v4/j/i;->j:I

    return v0
.end method

.method static synthetic a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .registers 8

    .prologue
    const/4 v5, 0x0

    .line 51
    .line 1569
    const/4 v0, 0x1

    if-eq p1, v0, :cond_5

    .line 1570
    :goto_4
    return-object p0

    .line 1573
    :cond_5
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1575
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1576
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 1577
    new-instance v3, Landroid/graphics/ColorMatrix;

    invoke-direct {v3}, Landroid/graphics/ColorMatrix;-><init>()V

    .line 1578
    invoke-virtual {v3, v5}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    .line 1579
    new-instance v4, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v4, v3}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    .line 1580
    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 1581
    invoke-virtual {v1, p0, v5, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1582
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    move-object p0, v0

    .line 51
    goto :goto_4
.end method

.method private a(Landroid/net/Uri;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 550
    if-eqz p1, :cond_7

    iget-object v0, p0, Landroid/support/v4/j/i;->a:Landroid/content/Context;

    if-nez v0, :cond_f

    .line 551
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "bad argument to loadBitmap"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 555
    :cond_f
    :try_start_f
    iget-object v0, p0, Landroid/support/v4/j/i;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    .line 556
    const/4 v0, 0x0

    invoke-static {v1, v0, p2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_1d
    .catchall {:try_start_f .. :try_end_1d} :catchall_2d

    move-result-object v0

    .line 558
    if-eqz v1, :cond_23

    .line 560
    :try_start_20
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_23
    .catch Ljava/io/IOException; {:try_start_20 .. :try_end_23} :catch_24

    .line 563
    :cond_23
    :goto_23
    return-object v0

    .line 561
    :catch_24
    move-exception v1

    .line 562
    const-string v2, "PrintHelperKitkat"

    const-string v3, "close fail "

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_23

    .line 558
    :catchall_2d
    move-exception v0

    if-eqz v1, :cond_33

    .line 560
    :try_start_30
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_33
    .catch Ljava/io/IOException; {:try_start_30 .. :try_end_33} :catch_34

    .line 563
    :cond_33
    :goto_33
    throw v0

    .line 561
    :catch_34
    move-exception v1

    .line 562
    const-string v2, "PrintHelperKitkat"

    const-string v3, "close fail "

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_33
.end method

.method private static synthetic a(Landroid/support/v4/j/i;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .registers 3

    .prologue
    .line 51
    invoke-virtual {p0, p1}, Landroid/support/v4/j/i;->a(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(IILandroid/graphics/RectF;I)Landroid/graphics/Matrix;
    .registers 10

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 51
    .line 2283
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 2286
    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v0

    int-to-float v2, p0

    div-float/2addr v0, v2

    .line 2287
    const/4 v2, 0x2

    if-ne p3, v2, :cond_32

    .line 2288
    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v2

    int-to-float v3, p1

    div-float/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 2292
    :goto_1a
    invoke-virtual {v1, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 2295
    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v2

    int-to-float v3, p0

    mul-float/2addr v3, v0

    sub-float/2addr v2, v3

    div-float/2addr v2, v5

    .line 2297
    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v3

    int-to-float v4, p1

    mul-float/2addr v0, v4

    sub-float v0, v3, v0

    div-float/2addr v0, v5

    .line 2299
    invoke-virtual {v1, v2, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 51
    return-object v1

    .line 2290
    :cond_32
    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v2

    int-to-float v3, p1

    div-float/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto :goto_1a
.end method

.method private static synthetic a(Landroid/support/v4/j/i;)Ljava/lang/Object;
    .registers 2

    .prologue
    .line 51
    iget-object v0, p0, Landroid/support/v4/j/i;->c:Ljava/lang/Object;

    return-object v0
.end method

.method private a(I)V
    .registers 2

    .prologue
    .line 111
    iput p1, p0, Landroid/support/v4/j/i;->j:I

    .line 112
    return-void
.end method

.method private a(Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/support/v4/j/n;)V
    .registers 12

    .prologue
    .line 175
    if-nez p2, :cond_3

    .line 271
    :goto_2
    return-void

    .line 178
    :cond_3
    iget v4, p0, Landroid/support/v4/j/i;->j:I

    .line 179
    iget-object v0, p0, Landroid/support/v4/j/i;->a:Landroid/content/Context;

    const-string v1, "print"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/print/PrintManager;

    .line 180
    sget-object v0, Landroid/print/PrintAttributes$MediaSize;->UNKNOWN_PORTRAIT:Landroid/print/PrintAttributes$MediaSize;

    .line 181
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-le v1, v2, :cond_1e

    .line 182
    sget-object v0, Landroid/print/PrintAttributes$MediaSize;->UNKNOWN_LANDSCAPE:Landroid/print/PrintAttributes$MediaSize;

    .line 184
    :cond_1e
    new-instance v1, Landroid/print/PrintAttributes$Builder;

    invoke-direct {v1}, Landroid/print/PrintAttributes$Builder;-><init>()V

    invoke-virtual {v1, v0}, Landroid/print/PrintAttributes$Builder;->setMediaSize(Landroid/print/PrintAttributes$MediaSize;)Landroid/print/PrintAttributes$Builder;

    move-result-object v0

    iget v1, p0, Landroid/support/v4/j/i;->k:I

    invoke-virtual {v0, v1}, Landroid/print/PrintAttributes$Builder;->setColorMode(I)Landroid/print/PrintAttributes$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/print/PrintAttributes$Builder;->build()Landroid/print/PrintAttributes;

    move-result-object v7

    .line 189
    new-instance v0, Landroid/support/v4/j/j;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Landroid/support/v4/j/j;-><init>(Landroid/support/v4/j/i;Ljava/lang/String;Landroid/graphics/Bitmap;ILandroid/support/v4/j/n;)V

    invoke-virtual {v6, p1, v0, v7}, Landroid/print/PrintManager;->print(Ljava/lang/String;Landroid/print/PrintDocumentAdapter;Landroid/print/PrintAttributes;)Landroid/print/PrintJob;

    goto :goto_2
.end method

.method private a(Ljava/lang/String;Landroid/net/Uri;Landroid/support/v4/j/n;)V
    .registers 10

    .prologue
    .line 314
    iget v5, p0, Landroid/support/v4/j/i;->j:I

    .line 316
    new-instance v0, Landroid/support/v4/j/k;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Landroid/support/v4/j/k;-><init>(Landroid/support/v4/j/i;Ljava/lang/String;Landroid/net/Uri;Landroid/support/v4/j/n;I)V

    .line 477
    iget-object v1, p0, Landroid/support/v4/j/i;->a:Landroid/content/Context;

    const-string v2, "print"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/print/PrintManager;

    .line 478
    new-instance v2, Landroid/print/PrintAttributes$Builder;

    invoke-direct {v2}, Landroid/print/PrintAttributes$Builder;-><init>()V

    .line 479
    iget v3, p0, Landroid/support/v4/j/i;->k:I

    invoke-virtual {v2, v3}, Landroid/print/PrintAttributes$Builder;->setColorMode(I)Landroid/print/PrintAttributes$Builder;

    .line 481
    iget v3, p0, Landroid/support/v4/j/i;->l:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_31

    .line 482
    sget-object v3, Landroid/print/PrintAttributes$MediaSize;->UNKNOWN_LANDSCAPE:Landroid/print/PrintAttributes$MediaSize;

    invoke-virtual {v2, v3}, Landroid/print/PrintAttributes$Builder;->setMediaSize(Landroid/print/PrintAttributes$MediaSize;)Landroid/print/PrintAttributes$Builder;

    .line 486
    :cond_29
    :goto_29
    invoke-virtual {v2}, Landroid/print/PrintAttributes$Builder;->build()Landroid/print/PrintAttributes;

    move-result-object v2

    .line 488
    invoke-virtual {v1, p1, v0, v2}, Landroid/print/PrintManager;->print(Ljava/lang/String;Landroid/print/PrintDocumentAdapter;Landroid/print/PrintAttributes;)Landroid/print/PrintJob;

    .line 489
    return-void

    .line 483
    :cond_31
    iget v3, p0, Landroid/support/v4/j/i;->l:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_29

    .line 484
    sget-object v3, Landroid/print/PrintAttributes$MediaSize;->UNKNOWN_PORTRAIT:Landroid/print/PrintAttributes$MediaSize;

    invoke-virtual {v2, v3}, Landroid/print/PrintAttributes$Builder;->setMediaSize(Landroid/print/PrintAttributes$MediaSize;)Landroid/print/PrintAttributes$Builder;

    goto :goto_29
.end method

.method private b()I
    .registers 2

    .prologue
    .line 153
    iget v0, p0, Landroid/support/v4/j/i;->l:I

    return v0
.end method

.method private static b(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .registers 8

    .prologue
    const/4 v5, 0x0

    .line 569
    const/4 v0, 0x1

    if-eq p1, v0, :cond_5

    .line 584
    :goto_4
    return-object p0

    .line 573
    :cond_5
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 575
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 576
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 577
    new-instance v3, Landroid/graphics/ColorMatrix;

    invoke-direct {v3}, Landroid/graphics/ColorMatrix;-><init>()V

    .line 578
    invoke-virtual {v3, v5}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    .line 579
    new-instance v4, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v4, v3}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    .line 580
    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 581
    invoke-virtual {v1, p0, v5, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 582
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    move-object p0, v0

    .line 584
    goto :goto_4
.end method

.method private static b(IILandroid/graphics/RectF;I)Landroid/graphics/Matrix;
    .registers 10

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 283
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 286
    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v0

    int-to-float v2, p0

    div-float/2addr v0, v2

    .line 287
    const/4 v2, 0x2

    if-ne p3, v2, :cond_32

    .line 288
    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v2

    int-to-float v3, p1

    div-float/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 292
    :goto_1a
    invoke-virtual {v1, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 295
    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v2

    int-to-float v3, p0

    mul-float/2addr v3, v0

    sub-float/2addr v2, v3

    div-float/2addr v2, v5

    .line 297
    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v3

    int-to-float v4, p1

    mul-float/2addr v0, v4

    sub-float v0, v3, v0

    div-float/2addr v0, v5

    .line 299
    invoke-virtual {v1, v2, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 300
    return-object v1

    .line 290
    :cond_32
    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v2

    int-to-float v3, p1

    div-float/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto :goto_1a
.end method

.method private b(I)V
    .registers 2

    .prologue
    .line 133
    iput p1, p0, Landroid/support/v4/j/i;->k:I

    .line 134
    return-void
.end method

.method private c()I
    .registers 2

    .prologue
    .line 163
    iget v0, p0, Landroid/support/v4/j/i;->k:I

    return v0
.end method

.method private c(I)V
    .registers 2

    .prologue
    .line 143
    iput p1, p0, Landroid/support/v4/j/i;->l:I

    .line 144
    return-void
.end method


# virtual methods
.method final a(Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .registers 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 500
    if-eqz p1, :cond_8

    iget-object v2, p0, Landroid/support/v4/j/i;->a:Landroid/content/Context;

    if-nez v2, :cond_10

    .line 501
    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "bad argument to getScaledBitmap"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 504
    :cond_10
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 505
    iput-boolean v1, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 506
    invoke-direct {p0, p1, v2}, Landroid/support/v4/j/i;->a(Landroid/net/Uri;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 508
    iget v3, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 509
    iget v4, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 512
    if-lez v3, :cond_22

    if-gtz v4, :cond_23

    .line 541
    :cond_22
    :goto_22
    return-object v0

    .line 517
    :cond_23
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 520
    :goto_27
    const/16 v5, 0xdac

    if-le v2, v5, :cond_30

    .line 521
    ushr-int/lit8 v2, v2, 0x1

    .line 522
    shl-int/lit8 v1, v1, 0x1

    goto :goto_27

    .line 526
    :cond_30
    if-lez v1, :cond_22

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    div-int/2addr v2, v1

    if-lez v2, :cond_22

    .line 530
    iget-object v2, p0, Landroid/support/v4/j/i;->c:Ljava/lang/Object;

    monitor-enter v2

    .line 531
    :try_start_3c
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object v0, p0, Landroid/support/v4/j/i;->b:Landroid/graphics/BitmapFactory$Options;

    .line 532
    iget-object v0, p0, Landroid/support/v4/j/i;->b:Landroid/graphics/BitmapFactory$Options;

    const/4 v3, 0x1

    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 533
    iget-object v0, p0, Landroid/support/v4/j/i;->b:Landroid/graphics/BitmapFactory$Options;

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 534
    iget-object v0, p0, Landroid/support/v4/j/i;->b:Landroid/graphics/BitmapFactory$Options;

    .line 535
    monitor-exit v2
    :try_end_4f
    .catchall {:try_start_3c .. :try_end_4f} :catchall_5e

    .line 537
    :try_start_4f
    invoke-direct {p0, p1, v0}, Landroid/support/v4/j/i;->a(Landroid/net/Uri;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_52
    .catchall {:try_start_4f .. :try_end_52} :catchall_61

    move-result-object v0

    .line 539
    iget-object v1, p0, Landroid/support/v4/j/i;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 540
    const/4 v2, 0x0

    :try_start_57
    iput-object v2, p0, Landroid/support/v4/j/i;->b:Landroid/graphics/BitmapFactory$Options;

    .line 541
    monitor-exit v1

    goto :goto_22

    :catchall_5b
    move-exception v0

    monitor-exit v1
    :try_end_5d
    .catchall {:try_start_57 .. :try_end_5d} :catchall_5b

    throw v0

    .line 535
    :catchall_5e
    move-exception v0

    :try_start_5f
    monitor-exit v2
    :try_end_60
    .catchall {:try_start_5f .. :try_end_60} :catchall_5e

    throw v0

    .line 539
    :catchall_61
    move-exception v0

    iget-object v1, p0, Landroid/support/v4/j/i;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 540
    const/4 v2, 0x0

    :try_start_66
    iput-object v2, p0, Landroid/support/v4/j/i;->b:Landroid/graphics/BitmapFactory$Options;

    .line 541
    monitor-exit v1
    :try_end_69
    .catchall {:try_start_66 .. :try_end_69} :catchall_6a

    throw v0

    :catchall_6a
    move-exception v0

    :try_start_6b
    monitor-exit v1
    :try_end_6c
    .catchall {:try_start_6b .. :try_end_6c} :catchall_6a

    throw v0
.end method
