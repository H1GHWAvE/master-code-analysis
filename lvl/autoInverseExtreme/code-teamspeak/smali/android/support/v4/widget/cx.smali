.class public final Landroid/support/v4/widget/cx;
.super Landroid/support/v4/widget/bz;
.source "SourceFile"


# instance fields
.field protected l:[I

.field protected m:[I

.field n:[Ljava/lang/String;

.field private o:I

.field private p:Landroid/support/v4/widget/cy;

.field private q:Landroid/support/v4/widget/cz;


# direct methods
.method private constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V
    .registers 7
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/widget/bz;-><init>(Landroid/content/Context;ILandroid/database/Cursor;)V

    .line 47
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v4/widget/cx;->o:I

    .line 64
    iput-object p5, p0, Landroid/support/v4/widget/cx;->m:[I

    .line 65
    iput-object p4, p0, Landroid/support/v4/widget/cx;->n:[Ljava/lang/String;

    .line 66
    invoke-direct {p0, p4}, Landroid/support/v4/widget/cx;->a([Ljava/lang/String;)V

    .line 67
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V
    .registers 8

    .prologue
    .line 89
    invoke-direct {p0, p1, p2, p3, p6}, Landroid/support/v4/widget/bz;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 47
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v4/widget/cx;->o:I

    .line 90
    iput-object p5, p0, Landroid/support/v4/widget/cx;->m:[I

    .line 91
    iput-object p4, p0, Landroid/support/v4/widget/cx;->n:[Ljava/lang/String;

    .line 92
    invoke-direct {p0, p4}, Landroid/support/v4/widget/cx;->a([Ljava/lang/String;)V

    .line 93
    return-void
.end method

.method private a(I)V
    .registers 2

    .prologue
    .line 246
    iput p1, p0, Landroid/support/v4/widget/cx;->o:I

    .line 247
    return-void
.end method

.method private a(Landroid/database/Cursor;[Ljava/lang/String;[I)V
    .registers 5

    .prologue
    .line 344
    iput-object p2, p0, Landroid/support/v4/widget/cx;->n:[Ljava/lang/String;

    .line 345
    iput-object p3, p0, Landroid/support/v4/widget/cx;->m:[I

    .line 346
    invoke-super {p0, p1}, Landroid/support/v4/widget/bz;->a(Landroid/database/Cursor;)V

    .line 347
    iget-object v0, p0, Landroid/support/v4/widget/cx;->n:[Ljava/lang/String;

    invoke-direct {p0, v0}, Landroid/support/v4/widget/cx;->a([Ljava/lang/String;)V

    .line 348
    return-void
.end method

.method private a(Landroid/support/v4/widget/cy;)V
    .registers 2

    .prologue
    .line 278
    iput-object p1, p0, Landroid/support/v4/widget/cx;->p:Landroid/support/v4/widget/cy;

    .line 279
    return-void
.end method

.method private a(Landroid/support/v4/widget/cz;)V
    .registers 2

    .prologue
    .line 175
    iput-object p1, p0, Landroid/support/v4/widget/cx;->q:Landroid/support/v4/widget/cz;

    .line 176
    return-void
.end method

.method private static a(Landroid/widget/ImageView;Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 195
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_7
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_7} :catch_8

    .line 199
    :goto_7
    return-void

    .line 197
    :catch_8
    move-exception v0

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    goto :goto_7
.end method

.method private static a(Landroid/widget/TextView;Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 213
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    return-void
.end method

.method private a([Ljava/lang/String;)V
    .registers 7

    .prologue
    .line 310
    iget-object v0, p0, Landroid/support/v4/widget/cx;->c:Landroid/database/Cursor;

    if-eqz v0, :cond_24

    .line 312
    array-length v1, p1

    .line 313
    iget-object v0, p0, Landroid/support/v4/widget/cx;->l:[I

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v4/widget/cx;->l:[I

    array-length v0, v0

    if-eq v0, v1, :cond_12

    .line 314
    :cond_e
    new-array v0, v1, [I

    iput-object v0, p0, Landroid/support/v4/widget/cx;->l:[I

    .line 316
    :cond_12
    const/4 v0, 0x0

    :goto_13
    if-ge v0, v1, :cond_27

    .line 317
    iget-object v2, p0, Landroid/support/v4/widget/cx;->l:[I

    iget-object v3, p0, Landroid/support/v4/widget/cx;->c:Landroid/database/Cursor;

    aget-object v4, p1, v0

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    aput v3, v2, v0

    .line 316
    add-int/lit8 v0, v0, 0x1

    goto :goto_13

    .line 320
    :cond_24
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/widget/cx;->l:[I

    .line 322
    :cond_27
    return-void
.end method

.method private c()Landroid/support/v4/widget/cz;
    .registers 2

    .prologue
    .line 162
    iget-object v0, p0, Landroid/support/v4/widget/cx;->q:Landroid/support/v4/widget/cz;

    return-object v0
.end method

.method private d()I
    .registers 2

    .prologue
    .line 228
    iget v0, p0, Landroid/support/v4/widget/cx;->o:I

    return v0
.end method

.method private e()Landroid/support/v4/widget/cy;
    .registers 2

    .prologue
    .line 262
    iget-object v0, p0, Landroid/support/v4/widget/cx;->p:Landroid/support/v4/widget/cy;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/database/Cursor;)V
    .registers 12

    .prologue
    const/4 v2, 0x0

    .line 121
    iget-object v4, p0, Landroid/support/v4/widget/cx;->q:Landroid/support/v4/widget/cz;

    .line 122
    iget-object v0, p0, Landroid/support/v4/widget/cx;->m:[I

    array-length v5, v0

    .line 123
    iget-object v6, p0, Landroid/support/v4/widget/cx;->l:[I

    .line 124
    iget-object v7, p0, Landroid/support/v4/widget/cx;->m:[I

    move v3, v2

    .line 126
    :goto_b
    if-ge v3, v5, :cond_6c

    .line 127
    aget v0, v7, v3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 128
    if-eqz v0, :cond_30

    .line 130
    if-eqz v4, :cond_6d

    .line 131
    invoke-interface {v4}, Landroid/support/v4/widget/cz;->a()Z

    move-result v1

    .line 134
    :goto_1b
    if-nez v1, :cond_30

    .line 135
    aget v1, v6, v3

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 136
    if-nez v1, :cond_27

    .line 137
    const-string v1, ""

    .line 140
    :cond_27
    instance-of v8, v0, Landroid/widget/TextView;

    if-eqz v8, :cond_34

    .line 141
    check-cast v0, Landroid/widget/TextView;

    .line 1213
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    :cond_30
    :goto_30
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_b

    .line 142
    :cond_34
    instance-of v8, v0, Landroid/widget/ImageView;

    if-eqz v8, :cond_4b

    .line 143
    check-cast v0, Landroid/widget/ImageView;

    .line 2195
    :try_start_3a
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_41
    .catch Ljava/lang/NumberFormatException; {:try_start_3a .. :try_end_41} :catch_42

    goto :goto_30

    .line 2197
    :catch_42
    move-exception v8

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    goto :goto_30

    .line 145
    :cond_4b
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a  view that can be bounds by this SimpleCursorAdapter"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 151
    :cond_6c
    return-void

    :cond_6d
    move v1, v2

    goto :goto_1b
.end method

.method public final b(Landroid/database/Cursor;)Landroid/database/Cursor;
    .registers 4

    .prologue
    .line 326
    invoke-super {p0, p1}, Landroid/support/v4/widget/bz;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 328
    iget-object v1, p0, Landroid/support/v4/widget/cx;->n:[Ljava/lang/String;

    invoke-direct {p0, v1}, Landroid/support/v4/widget/cx;->a([Ljava/lang/String;)V

    .line 329
    return-object v0
.end method

.method public final c(Landroid/database/Cursor;)Ljava/lang/CharSequence;
    .registers 3

    .prologue
    .line 294
    iget-object v0, p0, Landroid/support/v4/widget/cx;->p:Landroid/support/v4/widget/cy;

    if-eqz v0, :cond_b

    .line 295
    iget-object v0, p0, Landroid/support/v4/widget/cx;->p:Landroid/support/v4/widget/cy;

    invoke-interface {v0}, Landroid/support/v4/widget/cy;->a()Ljava/lang/CharSequence;

    move-result-object v0

    .line 300
    :goto_a
    return-object v0

    .line 296
    :cond_b
    iget v0, p0, Landroid/support/v4/widget/cx;->o:I

    if-ltz v0, :cond_16

    .line 297
    iget v0, p0, Landroid/support/v4/widget/cx;->o:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_a

    .line 300
    :cond_16
    invoke-super {p0, p1}, Landroid/support/v4/widget/bz;->c(Landroid/database/Cursor;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_a
.end method
