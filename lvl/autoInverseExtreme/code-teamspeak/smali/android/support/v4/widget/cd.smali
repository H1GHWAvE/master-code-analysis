.class Landroid/support/v4/widget/cd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/widget/cb;


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/view/animation/Interpolator;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 1026
    if-eqz p2, :cond_8

    new-instance v0, Landroid/widget/OverScroller;

    invoke-direct {v0, p1, p2}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    :goto_7
    return-object v0

    :cond_8
    new-instance v0, Landroid/widget/OverScroller;

    invoke-direct {v0, p1}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;)V

    goto :goto_7
.end method

.method public final a(Ljava/lang/Object;III)V
    .registers 6

    .prologue
    .line 183
    .line 1047
    check-cast p1, Landroid/widget/OverScroller;

    const/4 v0, 0x0

    invoke-virtual {p1, p2, p3, v0, p4}, Landroid/widget/OverScroller;->startScroll(IIII)V

    .line 184
    return-void
.end method

.method public final a(Ljava/lang/Object;IIIII)V
    .registers 13

    .prologue
    .line 189
    move-object v0, p1

    .line 1052
    check-cast v0, Landroid/widget/OverScroller;

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/widget/OverScroller;->startScroll(IIIII)V

    .line 190
    return-void
.end method

.method public final a(Ljava/lang/Object;IIIIIIII)V
    .registers 19

    .prologue
    .line 195
    move-object v0, p1

    .line 1057
    check-cast v0, Landroid/widget/OverScroller;

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    move/from16 v6, p7

    move/from16 v7, p8

    move/from16 v8, p9

    invoke-virtual/range {v0 .. v8}, Landroid/widget/OverScroller;->fling(IIIIIIII)V

    .line 197
    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 158
    .line 1031
    check-cast p1, Landroid/widget/OverScroller;

    invoke-virtual {p1}, Landroid/widget/OverScroller;->isFinished()Z

    move-result v0

    .line 158
    return v0
.end method

.method public final b(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 163
    .line 1035
    check-cast p1, Landroid/widget/OverScroller;

    invoke-virtual {p1}, Landroid/widget/OverScroller;->getCurrX()I

    move-result v0

    .line 163
    return v0
.end method

.method public final b(Ljava/lang/Object;III)V
    .registers 5

    .prologue
    .line 214
    .line 1072
    check-cast p1, Landroid/widget/OverScroller;

    invoke-virtual {p1, p2, p3, p4}, Landroid/widget/OverScroller;->notifyHorizontalEdgeReached(III)V

    .line 215
    return-void
.end method

.method public final b(Ljava/lang/Object;IIIII)V
    .registers 18

    .prologue
    .line 202
    move-object v0, p1

    .line 1062
    check-cast v0, Landroid/widget/OverScroller;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    move v1, p2

    move v2, p3

    move v4, p4

    move/from16 v8, p5

    move/from16 v10, p6

    invoke-virtual/range {v0 .. v10}, Landroid/widget/OverScroller;->fling(IIIIIIIIII)V

    .line 204
    return-void
.end method

.method public final c(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 168
    .line 1039
    check-cast p1, Landroid/widget/OverScroller;

    invoke-virtual {p1}, Landroid/widget/OverScroller;->getCurrY()I

    move-result v0

    .line 168
    return v0
.end method

.method public final c(Ljava/lang/Object;III)V
    .registers 5

    .prologue
    .line 219
    .line 1076
    check-cast p1, Landroid/widget/OverScroller;

    invoke-virtual {p1, p2, p3, p4}, Landroid/widget/OverScroller;->notifyVerticalEdgeReached(III)V

    .line 220
    return-void
.end method

.method public d(Ljava/lang/Object;)F
    .registers 3

    .prologue
    .line 173
    const/4 v0, 0x0

    return v0
.end method

.method public final e(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 178
    .line 1043
    check-cast p1, Landroid/widget/OverScroller;

    invoke-virtual {p1}, Landroid/widget/OverScroller;->computeScrollOffset()Z

    move-result v0

    .line 178
    return v0
.end method

.method public final f(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 208
    .line 1067
    check-cast p1, Landroid/widget/OverScroller;

    invoke-virtual {p1}, Landroid/widget/OverScroller;->abortAnimation()V

    .line 209
    return-void
.end method

.method public final g(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 224
    .line 1080
    check-cast p1, Landroid/widget/OverScroller;

    invoke-virtual {p1}, Landroid/widget/OverScroller;->isOverScrolled()Z

    move-result v0

    .line 224
    return v0
.end method

.method public final h(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 229
    .line 1084
    check-cast p1, Landroid/widget/OverScroller;

    invoke-virtual {p1}, Landroid/widget/OverScroller;->getFinalX()I

    move-result v0

    .line 229
    return v0
.end method

.method public final i(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 234
    .line 1088
    check-cast p1, Landroid/widget/OverScroller;

    invoke-virtual {p1}, Landroid/widget/OverScroller;->getFinalY()I

    move-result v0

    .line 234
    return v0
.end method
