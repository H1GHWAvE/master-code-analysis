.class public final Landroid/support/v4/widget/ca;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final c:I = 0x10

.field private static final d:Ljava/lang/String; = "ScrollerCompat"


# instance fields
.field a:Ljava/lang/Object;

.field b:Landroid/support/v4/widget/cb;


# direct methods
.method private constructor <init>(ILandroid/content/Context;Landroid/view/animation/Interpolator;)V
    .registers 5

    .prologue
    .line 262
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 263
    const/16 v0, 0xe

    if-lt p1, v0, :cond_17

    .line 264
    new-instance v0, Landroid/support/v4/widget/ce;

    invoke-direct {v0}, Landroid/support/v4/widget/ce;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/ca;->b:Landroid/support/v4/widget/cb;

    .line 270
    :goto_e
    iget-object v0, p0, Landroid/support/v4/widget/ca;->b:Landroid/support/v4/widget/cb;

    invoke-interface {v0, p2, p3}, Landroid/support/v4/widget/cb;->a(Landroid/content/Context;Landroid/view/animation/Interpolator;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/widget/ca;->a:Ljava/lang/Object;

    .line 271
    return-void

    .line 265
    :cond_17
    const/16 v0, 0x9

    if-lt p1, v0, :cond_23

    .line 266
    new-instance v0, Landroid/support/v4/widget/cd;

    invoke-direct {v0}, Landroid/support/v4/widget/cd;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/ca;->b:Landroid/support/v4/widget/cb;

    goto :goto_e

    .line 268
    :cond_23
    new-instance v0, Landroid/support/v4/widget/cc;

    invoke-direct {v0}, Landroid/support/v4/widget/cc;-><init>()V

    iput-object v0, p0, Landroid/support/v4/widget/ca;->b:Landroid/support/v4/widget/cb;

    goto :goto_e
.end method

.method constructor <init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V
    .registers 4

    .prologue
    .line 254
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-direct {p0, v0, p1, p2}, Landroid/support/v4/widget/ca;-><init>(ILandroid/content/Context;Landroid/view/animation/Interpolator;)V

    .line 256
    return-void
.end method

.method private static a(Landroid/content/Context;)Landroid/support/v4/widget/ca;
    .registers 2

    .prologue
    .line 246
    const/4 v0, 0x0

    invoke-static {p0, v0}, Landroid/support/v4/widget/ca;->a(Landroid/content/Context;Landroid/view/animation/Interpolator;)Landroid/support/v4/widget/ca;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/view/animation/Interpolator;)Landroid/support/v4/widget/ca;
    .registers 3

    .prologue
    .line 250
    new-instance v0, Landroid/support/v4/widget/ca;

    invoke-direct {v0, p0, p1}, Landroid/support/v4/widget/ca;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    return-object v0
.end method

.method private a(III)V
    .registers 6

    .prologue
    .line 352
    iget-object v0, p0, Landroid/support/v4/widget/ca;->b:Landroid/support/v4/widget/cb;

    iget-object v1, p0, Landroid/support/v4/widget/ca;->a:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2, p3}, Landroid/support/v4/widget/cb;->a(Ljava/lang/Object;III)V

    .line 353
    return-void
.end method

.method private a(IIIII)V
    .registers 13

    .prologue
    .line 369
    iget-object v0, p0, Landroid/support/v4/widget/ca;->b:Landroid/support/v4/widget/cb;

    iget-object v1, p0, Landroid/support/v4/widget/ca;->a:Ljava/lang/Object;

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-interface/range {v0 .. v6}, Landroid/support/v4/widget/cb;->a(Ljava/lang/Object;IIIII)V

    .line 370
    return-void
.end method

.method private b(III)V
    .registers 6

    .prologue
    .line 448
    iget-object v0, p0, Landroid/support/v4/widget/ca;->b:Landroid/support/v4/widget/cb;

    iget-object v1, p0, Landroid/support/v4/widget/ca;->a:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2, p3}, Landroid/support/v4/widget/cb;->b(Ljava/lang/Object;III)V

    .line 449
    return-void
.end method

.method private b(IIIII)V
    .registers 13

    .prologue
    .line 421
    iget-object v0, p0, Landroid/support/v4/widget/ca;->b:Landroid/support/v4/widget/cb;

    iget-object v1, p0, Landroid/support/v4/widget/ca;->a:Ljava/lang/Object;

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-interface/range {v0 .. v6}, Landroid/support/v4/widget/cb;->b(Ljava/lang/Object;IIIII)V

    .line 423
    return-void
.end method

.method private c(III)V
    .registers 6

    .prologue
    .line 465
    iget-object v0, p0, Landroid/support/v4/widget/ca;->b:Landroid/support/v4/widget/cb;

    iget-object v1, p0, Landroid/support/v4/widget/ca;->a:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2, p3}, Landroid/support/v4/widget/cb;->c(Ljava/lang/Object;III)V

    .line 466
    return-void
.end method

.method private h()I
    .registers 3

    .prologue
    .line 304
    iget-object v0, p0, Landroid/support/v4/widget/ca;->b:Landroid/support/v4/widget/cb;

    iget-object v1, p0, Landroid/support/v4/widget/ca;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/widget/cb;->h(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private i()Z
    .registers 3

    .prologue
    .line 482
    iget-object v0, p0, Landroid/support/v4/widget/ca;->b:Landroid/support/v4/widget/cb;

    iget-object v1, p0, Landroid/support/v4/widget/ca;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/widget/cb;->g(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(IIIIIIII)V
    .registers 19

    .prologue
    .line 393
    iget-object v0, p0, Landroid/support/v4/widget/ca;->b:Landroid/support/v4/widget/cb;

    iget-object v1, p0, Landroid/support/v4/widget/ca;->a:Ljava/lang/Object;

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    invoke-interface/range {v0 .. v9}, Landroid/support/v4/widget/cb;->a(Ljava/lang/Object;IIIIIIII)V

    .line 394
    return-void
.end method

.method public final a()Z
    .registers 3

    .prologue
    .line 279
    iget-object v0, p0, Landroid/support/v4/widget/ca;->b:Landroid/support/v4/widget/cb;

    iget-object v1, p0, Landroid/support/v4/widget/ca;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/widget/cb;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()I
    .registers 3

    .prologue
    .line 288
    iget-object v0, p0, Landroid/support/v4/widget/ca;->b:Landroid/support/v4/widget/cb;

    iget-object v1, p0, Landroid/support/v4/widget/ca;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/widget/cb;->b(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final c()I
    .registers 3

    .prologue
    .line 297
    iget-object v0, p0, Landroid/support/v4/widget/ca;->b:Landroid/support/v4/widget/cb;

    iget-object v1, p0, Landroid/support/v4/widget/ca;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/widget/cb;->c(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final d()I
    .registers 3

    .prologue
    .line 311
    iget-object v0, p0, Landroid/support/v4/widget/ca;->b:Landroid/support/v4/widget/cb;

    iget-object v1, p0, Landroid/support/v4/widget/ca;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/widget/cb;->i(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final e()F
    .registers 3

    .prologue
    .line 325
    iget-object v0, p0, Landroid/support/v4/widget/ca;->b:Landroid/support/v4/widget/cb;

    iget-object v1, p0, Landroid/support/v4/widget/ca;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/widget/cb;->d(Ljava/lang/Object;)F

    move-result v0

    return v0
.end method

.method public final f()Z
    .registers 3

    .prologue
    .line 334
    iget-object v0, p0, Landroid/support/v4/widget/ca;->b:Landroid/support/v4/widget/cb;

    iget-object v1, p0, Landroid/support/v4/widget/ca;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/widget/cb;->e(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final g()V
    .registers 3

    .prologue
    .line 430
    iget-object v0, p0, Landroid/support/v4/widget/ca;->b:Landroid/support/v4/widget/cb;

    iget-object v1, p0, Landroid/support/v4/widget/ca;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/widget/cb;->f(Ljava/lang/Object;)V

    .line 431
    return-void
.end method
