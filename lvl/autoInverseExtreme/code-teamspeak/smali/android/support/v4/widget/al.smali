.class public final Landroid/support/v4/widget/al;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final b:Landroid/support/v4/widget/ao;


# instance fields
.field private a:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 37
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_e

    .line 38
    new-instance v0, Landroid/support/v4/widget/ap;

    invoke-direct {v0}, Landroid/support/v4/widget/ap;-><init>()V

    sput-object v0, Landroid/support/v4/widget/al;->b:Landroid/support/v4/widget/ao;

    .line 44
    :goto_d
    return-void

    .line 39
    :cond_e
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1c

    .line 40
    new-instance v0, Landroid/support/v4/widget/an;

    invoke-direct {v0}, Landroid/support/v4/widget/an;-><init>()V

    sput-object v0, Landroid/support/v4/widget/al;->b:Landroid/support/v4/widget/ao;

    goto :goto_d

    .line 42
    :cond_1c
    new-instance v0, Landroid/support/v4/widget/am;

    invoke-direct {v0}, Landroid/support/v4/widget/am;-><init>()V

    sput-object v0, Landroid/support/v4/widget/al;->b:Landroid/support/v4/widget/ao;

    goto :goto_d
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    sget-object v0, Landroid/support/v4/widget/al;->b:Landroid/support/v4/widget/ao;

    invoke-interface {v0, p1}, Landroid/support/v4/widget/ao;->a(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/widget/al;->a:Ljava/lang/Object;

    .line 151
    return-void
.end method


# virtual methods
.method public final a(II)V
    .registers 5

    .prologue
    .line 160
    sget-object v0, Landroid/support/v4/widget/al;->b:Landroid/support/v4/widget/ao;

    iget-object v1, p0, Landroid/support/v4/widget/al;->a:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, Landroid/support/v4/widget/ao;->a(Ljava/lang/Object;II)V

    .line 161
    return-void
.end method

.method public final a()Z
    .registers 3

    .prologue
    .line 171
    sget-object v0, Landroid/support/v4/widget/al;->b:Landroid/support/v4/widget/ao;

    iget-object v1, p0, Landroid/support/v4/widget/al;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/widget/ao;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final a(F)Z
    .registers 4

    .prologue
    .line 195
    sget-object v0, Landroid/support/v4/widget/al;->b:Landroid/support/v4/widget/ao;

    iget-object v1, p0, Landroid/support/v4/widget/al;->a:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/widget/ao;->a(Ljava/lang/Object;F)Z

    move-result v0

    return v0
.end method

.method public final a(FF)Z
    .registers 5

    .prologue
    .line 213
    sget-object v0, Landroid/support/v4/widget/al;->b:Landroid/support/v4/widget/ao;

    iget-object v1, p0, Landroid/support/v4/widget/al;->a:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, Landroid/support/v4/widget/ao;->a(Ljava/lang/Object;FF)Z

    move-result v0

    return v0
.end method

.method public final a(I)Z
    .registers 4

    .prologue
    .line 240
    sget-object v0, Landroid/support/v4/widget/al;->b:Landroid/support/v4/widget/ao;

    iget-object v1, p0, Landroid/support/v4/widget/al;->a:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/widget/ao;->a(Ljava/lang/Object;I)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/graphics/Canvas;)Z
    .registers 4

    .prologue
    .line 254
    sget-object v0, Landroid/support/v4/widget/al;->b:Landroid/support/v4/widget/ao;

    iget-object v1, p0, Landroid/support/v4/widget/al;->a:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Landroid/support/v4/widget/ao;->a(Ljava/lang/Object;Landroid/graphics/Canvas;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .registers 3

    .prologue
    .line 179
    sget-object v0, Landroid/support/v4/widget/al;->b:Landroid/support/v4/widget/ao;

    iget-object v1, p0, Landroid/support/v4/widget/al;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/widget/ao;->b(Ljava/lang/Object;)V

    .line 180
    return-void
.end method

.method public final c()Z
    .registers 3

    .prologue
    .line 225
    sget-object v0, Landroid/support/v4/widget/al;->b:Landroid/support/v4/widget/ao;

    iget-object v1, p0, Landroid/support/v4/widget/al;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Landroid/support/v4/widget/ao;->c(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
