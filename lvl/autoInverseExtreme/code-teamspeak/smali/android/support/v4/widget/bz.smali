.class public abstract Landroid/support/v4/widget/bz;
.super Landroid/support/v4/widget/r;
.source "SourceFile"


# instance fields
.field private l:I

.field private m:I

.field private n:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .registers 4

    .prologue
    .line 76
    invoke-direct {p0, p1}, Landroid/support/v4/widget/r;-><init>(Landroid/content/Context;)V

    .line 77
    iput p2, p0, Landroid/support/v4/widget/bz;->m:I

    iput p2, p0, Landroid/support/v4/widget/bz;->l:I

    .line 78
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Landroid/support/v4/widget/bz;->n:Landroid/view/LayoutInflater;

    .line 79
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;)V
    .registers 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0, p1, p3}, Landroid/support/v4/widget/r;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 55
    iput p2, p0, Landroid/support/v4/widget/bz;->m:I

    iput p2, p0, Landroid/support/v4/widget/bz;->l:I

    .line 56
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Landroid/support/v4/widget/bz;->n:Landroid/view/LayoutInflater;

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;I)V
    .registers 6

    .prologue
    .line 93
    invoke-direct {p0, p1, p3, p4}, Landroid/support/v4/widget/r;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 94
    iput p2, p0, Landroid/support/v4/widget/bz;->m:I

    iput p2, p0, Landroid/support/v4/widget/bz;->l:I

    .line 95
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Landroid/support/v4/widget/bz;->n:Landroid/view/LayoutInflater;

    .line 96
    return-void
.end method

.method private a(I)V
    .registers 2

    .prologue
    .line 120
    iput p1, p0, Landroid/support/v4/widget/bz;->l:I

    .line 121
    return-void
.end method

.method private b(I)V
    .registers 2

    .prologue
    .line 129
    iput p1, p0, Landroid/support/v4/widget/bz;->m:I

    .line 130
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7

    .prologue
    .line 106
    iget-object v0, p0, Landroid/support/v4/widget/bz;->n:Landroid/view/LayoutInflater;

    iget v1, p0, Landroid/support/v4/widget/bz;->l:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7

    .prologue
    .line 111
    iget-object v0, p0, Landroid/support/v4/widget/bz;->n:Landroid/view/LayoutInflater;

    iget v1, p0, Landroid/support/v4/widget/bz;->m:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
