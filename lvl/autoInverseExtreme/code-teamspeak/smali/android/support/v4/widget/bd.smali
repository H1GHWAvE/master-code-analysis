.class final Landroid/support/v4/widget/bd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field final synthetic a:Landroid/support/v4/widget/bg;

.field final synthetic b:Landroid/support/v4/widget/bb;


# direct methods
.method constructor <init>(Landroid/support/v4/widget/bb;Landroid/support/v4/widget/bg;)V
    .registers 3

    .prologue
    .line 419
    iput-object p1, p0, Landroid/support/v4/widget/bd;->b:Landroid/support/v4/widget/bb;

    iput-object p2, p0, Landroid/support/v4/widget/bd;->a:Landroid/support/v4/widget/bg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .registers 2

    .prologue
    .line 429
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 433
    iget-object v0, p0, Landroid/support/v4/widget/bd;->a:Landroid/support/v4/widget/bg;

    invoke-virtual {v0}, Landroid/support/v4/widget/bg;->b()V

    .line 434
    iget-object v0, p0, Landroid/support/v4/widget/bd;->a:Landroid/support/v4/widget/bg;

    .line 1630
    invoke-virtual {v0}, Landroid/support/v4/widget/bg;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/bg;->a(I)V

    .line 435
    iget-object v0, p0, Landroid/support/v4/widget/bd;->a:Landroid/support/v4/widget/bg;

    iget-object v1, p0, Landroid/support/v4/widget/bd;->a:Landroid/support/v4/widget/bg;

    .line 1697
    iget v1, v1, Landroid/support/v4/widget/bg;->e:F

    .line 435
    invoke-virtual {v0, v1}, Landroid/support/v4/widget/bg;->a(F)V

    .line 436
    iget-object v0, p0, Landroid/support/v4/widget/bd;->b:Landroid/support/v4/widget/bb;

    iget-boolean v0, v0, Landroid/support/v4/widget/bb;->d:Z

    if-eqz v0, :cond_2d

    .line 439
    iget-object v0, p0, Landroid/support/v4/widget/bd;->b:Landroid/support/v4/widget/bb;

    iput-boolean v2, v0, Landroid/support/v4/widget/bb;->d:Z

    .line 440
    const-wide/16 v0, 0x534

    invoke-virtual {p1, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 441
    iget-object v0, p0, Landroid/support/v4/widget/bd;->a:Landroid/support/v4/widget/bg;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/bg;->a(Z)V

    .line 445
    :goto_2c
    return-void

    .line 443
    :cond_2d
    iget-object v0, p0, Landroid/support/v4/widget/bd;->b:Landroid/support/v4/widget/bb;

    iget-object v1, p0, Landroid/support/v4/widget/bd;->b:Landroid/support/v4/widget/bb;

    invoke-static {v1}, Landroid/support/v4/widget/bb;->a(Landroid/support/v4/widget/bb;)F

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    add-float/2addr v1, v2

    const/high16 v2, 0x40a00000    # 5.0f

    rem-float/2addr v1, v2

    invoke-static {v0, v1}, Landroid/support/v4/widget/bb;->a(Landroid/support/v4/widget/bb;F)F

    goto :goto_2c
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .registers 4

    .prologue
    .line 423
    iget-object v0, p0, Landroid/support/v4/widget/bd;->b:Landroid/support/v4/widget/bb;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/support/v4/widget/bb;->a(Landroid/support/v4/widget/bb;F)F

    .line 424
    return-void
.end method
