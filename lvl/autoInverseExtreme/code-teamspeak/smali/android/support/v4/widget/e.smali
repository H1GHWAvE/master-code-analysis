.class final Landroid/support/v4/widget/e;
.super Landroid/widget/ImageView;
.source "SourceFile"


# static fields
.field private static final b:I = 0x1e000000

.field private static final c:I = 0x3d000000

.field private static final d:F = 0.0f

.field private static final e:F = 1.75f

.field private static final f:F = 3.5f

.field private static final g:I = 0x4


# instance fields
.field a:Landroid/view/animation/Animation$AnimationListener;

.field private h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 8

    .prologue
    .line 52
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 53
    invoke-virtual {p0}, Landroid/support/v4/widget/e;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    .line 54
    const/high16 v0, 0x41a00000    # 20.0f

    mul-float/2addr v0, v1

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 55
    const/high16 v2, 0x3fe00000    # 1.75f

    mul-float/2addr v2, v1

    float-to-int v2, v2

    .line 56
    const/4 v3, 0x0

    mul-float/2addr v3, v1

    float-to-int v3, v3

    .line 58
    const/high16 v4, 0x40600000    # 3.5f

    mul-float/2addr v4, v1

    float-to-int v4, v4

    iput v4, p0, Landroid/support/v4/widget/e;->h:I

    .line 61
    invoke-static {}, Landroid/support/v4/widget/e;->a()Z

    move-result v4

    if-eqz v4, :cond_49

    .line 62
    new-instance v0, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v2, Landroid/graphics/drawable/shapes/OvalShape;

    invoke-direct {v2}, Landroid/graphics/drawable/shapes/OvalShape;-><init>()V

    invoke-direct {v0, v2}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 63
    const/high16 v2, 0x40800000    # 4.0f

    mul-float/2addr v1, v2

    invoke-static {p0, v1}, Landroid/support/v4/view/cx;->f(Landroid/view/View;F)V

    .line 74
    :goto_3b
    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v1

    const v2, -0x50506

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 75
    invoke-virtual {p0, v0}, Landroid/support/v4/widget/e;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 76
    return-void

    .line 65
    :cond_49
    new-instance v1, Landroid/support/v4/widget/f;

    iget v4, p0, Landroid/support/v4/widget/e;->h:I

    invoke-direct {v1, p0, v4, v0}, Landroid/support/v4/widget/f;-><init>(Landroid/support/v4/widget/e;II)V

    .line 66
    new-instance v0, Landroid/graphics/drawable/ShapeDrawable;

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 67
    const/4 v1, 0x1

    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v4

    invoke-static {p0, v1, v4}, Landroid/support/v4/view/cx;->a(Landroid/view/View;ILandroid/graphics/Paint;)V

    .line 68
    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v1

    iget v4, p0, Landroid/support/v4/widget/e;->h:I

    int-to-float v4, v4

    int-to-float v3, v3

    int-to-float v2, v2

    const/high16 v5, 0x1e000000

    invoke-virtual {v1, v4, v3, v2, v5}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 70
    iget v1, p0, Landroid/support/v4/widget/e;->h:I

    .line 72
    invoke-virtual {p0, v1, v1, v1, v1}, Landroid/support/v4/widget/e;->setPadding(IIII)V

    goto :goto_3b
.end method

.method static synthetic a(Landroid/support/v4/widget/e;)I
    .registers 2

    .prologue
    .line 38
    iget v0, p0, Landroid/support/v4/widget/e;->h:I

    return v0
.end method

.method static synthetic a(Landroid/support/v4/widget/e;I)I
    .registers 2

    .prologue
    .line 38
    iput p1, p0, Landroid/support/v4/widget/e;->h:I

    return p1
.end method

.method private a(I)V
    .registers 3

    .prologue
    .line 117
    invoke-virtual {p0}, Landroid/support/v4/widget/e;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/e;->setBackgroundColor(I)V

    .line 118
    return-void
.end method

.method private a(Landroid/view/animation/Animation$AnimationListener;)V
    .registers 2

    .prologue
    .line 92
    iput-object p1, p0, Landroid/support/v4/widget/e;->a:Landroid/view/animation/Animation$AnimationListener;

    .line 93
    return-void
.end method

.method private static a()Z
    .registers 2

    .prologue
    .line 79
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method


# virtual methods
.method public final onAnimationEnd()V
    .registers 3

    .prologue
    .line 105
    invoke-super {p0}, Landroid/widget/ImageView;->onAnimationEnd()V

    .line 106
    iget-object v0, p0, Landroid/support/v4/widget/e;->a:Landroid/view/animation/Animation$AnimationListener;

    if-eqz v0, :cond_10

    .line 107
    iget-object v0, p0, Landroid/support/v4/widget/e;->a:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {p0}, Landroid/support/v4/widget/e;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/animation/Animation$AnimationListener;->onAnimationEnd(Landroid/view/animation/Animation;)V

    .line 109
    :cond_10
    return-void
.end method

.method public final onAnimationStart()V
    .registers 3

    .prologue
    .line 97
    invoke-super {p0}, Landroid/widget/ImageView;->onAnimationStart()V

    .line 98
    iget-object v0, p0, Landroid/support/v4/widget/e;->a:Landroid/view/animation/Animation$AnimationListener;

    if-eqz v0, :cond_10

    .line 99
    iget-object v0, p0, Landroid/support/v4/widget/e;->a:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {p0}, Landroid/support/v4/widget/e;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/animation/Animation$AnimationListener;->onAnimationStart(Landroid/view/animation/Animation;)V

    .line 101
    :cond_10
    return-void
.end method

.method protected final onMeasure(II)V
    .registers 6

    .prologue
    .line 84
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onMeasure(II)V

    .line 85
    invoke-static {}, Landroid/support/v4/widget/e;->a()Z

    move-result v0

    if-nez v0, :cond_1e

    .line 86
    invoke-virtual {p0}, Landroid/support/v4/widget/e;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Landroid/support/v4/widget/e;->h:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    invoke-virtual {p0}, Landroid/support/v4/widget/e;->getMeasuredHeight()I

    move-result v1

    iget v2, p0, Landroid/support/v4/widget/e;->h:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/widget/e;->setMeasuredDimension(II)V

    .line 89
    :cond_1e
    return-void
.end method

.method public final setBackgroundColor(I)V
    .registers 3

    .prologue
    .line 122
    invoke-virtual {p0}, Landroid/support/v4/widget/e;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    instance-of v0, v0, Landroid/graphics/drawable/ShapeDrawable;

    if-eqz v0, :cond_15

    .line 123
    invoke-virtual {p0}, Landroid/support/v4/widget/e;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 125
    :cond_15
    return-void
.end method
