.class final Landroid/support/v7/internal/widget/ai;
.super Landroid/support/v7/b/a/b;
.source "SourceFile"


# instance fields
.field a:Z


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;)V
    .registers 3

    .prologue
    .line 341
    invoke-direct {p0, p1}, Landroid/support/v7/b/a/b;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 342
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/internal/widget/ai;->a:Z

    .line 343
    return-void
.end method

.method private a(Z)V
    .registers 2

    .prologue
    .line 346
    iput-boolean p1, p0, Landroid/support/v7/internal/widget/ai;->a:Z

    .line 347
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .registers 3

    .prologue
    .line 359
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/ai;->a:Z

    if-eqz v0, :cond_7

    .line 360
    invoke-super {p0, p1}, Landroid/support/v7/b/a/b;->draw(Landroid/graphics/Canvas;)V

    .line 362
    :cond_7
    return-void
.end method

.method public final setHotspot(FF)V
    .registers 4

    .prologue
    .line 366
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/ai;->a:Z

    if-eqz v0, :cond_7

    .line 367
    invoke-super {p0, p1, p2}, Landroid/support/v7/b/a/b;->setHotspot(FF)V

    .line 369
    :cond_7
    return-void
.end method

.method public final setHotspotBounds(IIII)V
    .registers 6

    .prologue
    .line 373
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/ai;->a:Z

    if-eqz v0, :cond_7

    .line 374
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v7/b/a/b;->setHotspotBounds(IIII)V

    .line 376
    :cond_7
    return-void
.end method

.method public final setState([I)Z
    .registers 3

    .prologue
    .line 351
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/ai;->a:Z

    if-eqz v0, :cond_9

    .line 352
    invoke-super {p0, p1}, Landroid/support/v7/b/a/b;->setState([I)Z

    move-result v0

    .line 354
    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final setVisible(ZZ)Z
    .registers 4

    .prologue
    .line 380
    iget-boolean v0, p0, Landroid/support/v7/internal/widget/ai;->a:Z

    if-eqz v0, :cond_9

    .line 381
    invoke-super {p0, p1, p2}, Landroid/support/v7/b/a/b;->setVisible(ZZ)Z

    move-result v0

    .line 383
    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method
