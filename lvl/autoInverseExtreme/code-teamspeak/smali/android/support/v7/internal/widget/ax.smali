.class public final Landroid/support/v7/internal/widget/ax;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/res/TypedArray;

.field private final b:Landroid/content/Context;

.field private c:Landroid/support/v7/internal/widget/av;


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/content/res/TypedArray;)V
    .registers 3

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Landroid/support/v7/internal/widget/ax;->b:Landroid/content/Context;

    .line 55
    iput-object p2, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    .line 56
    return-void
.end method

.method private a(IF)F
    .registers 4

    .prologue
    .line 131
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1, p2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    return v0
.end method

.method private a(IIIF)F
    .registers 6

    .prologue
    .line 151
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/content/res/TypedArray;->getFraction(IIIF)F

    move-result v0

    return v0
.end method

.method private a(ILjava/lang/String;)I
    .registers 4

    .prologue
    .line 143
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1, p2}, Landroid/content/res/TypedArray;->getLayoutDimension(ILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;Landroid/util/AttributeSet;[I)Landroid/support/v7/internal/widget/ax;
    .registers 5

    .prologue
    .line 43
    invoke-virtual {p0, p1, p2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 44
    new-instance v1, Landroid/support/v7/internal/widget/ax;

    invoke-direct {v1, p0, v0}, Landroid/support/v7/internal/widget/ax;-><init>(Landroid/content/Context;Landroid/content/res/TypedArray;)V

    return-object v1
.end method

.method public static a(Landroid/content/Context;Landroid/util/AttributeSet;[II)Landroid/support/v7/internal/widget/ax;
    .registers 6

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 50
    new-instance v1, Landroid/support/v7/internal/widget/ax;

    invoke-direct {v1, p0, v0}, Landroid/support/v7/internal/widget/ax;-><init>(Landroid/content/Context;Landroid/content/res/TypedArray;)V

    return-object v1
.end method

.method private a(ILandroid/util/TypedValue;)Z
    .registers 4

    .prologue
    .line 163
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1, p2}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    move-result v0

    return v0
.end method

.method private b()I
    .registers 2

    .prologue
    .line 79
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->length()I

    move-result v0

    return v0
.end method

.method private c()I
    .registers 2

    .prologue
    .line 83
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v0

    return v0
.end method

.method private d()Landroid/content/res/Resources;
    .registers 2

    .prologue
    .line 91
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method private e()Ljava/lang/String;
    .registers 2

    .prologue
    .line 179
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->getPositionDescription()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private f(I)I
    .registers 3

    .prologue
    .line 87
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v0

    return v0
.end method

.method private f(II)I
    .registers 4

    .prologue
    .line 127
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1, p2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    return v0
.end method

.method private f()V
    .registers 2

    .prologue
    .line 183
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 184
    return-void
.end method

.method private g()I
    .registers 2

    .prologue
    .line 187
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->getChangingConfigurations()I

    move-result v0

    return v0
.end method

.method private g(I)Ljava/lang/String;
    .registers 3

    .prologue
    .line 99
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private h(I)Ljava/lang/String;
    .registers 3

    .prologue
    .line 103
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1}, Landroid/content/res/TypedArray;->getNonResourceString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private i(I)F
    .registers 4

    .prologue
    .line 115
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {v0, p1, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    return v0
.end method

.method private j(I)Landroid/content/res/ColorStateList;
    .registers 3

    .prologue
    .line 123
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    return-object v0
.end method

.method private k(I)[Ljava/lang/CharSequence;
    .registers 3

    .prologue
    .line 159
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1}, Landroid/content/res/TypedArray;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private l(I)I
    .registers 3

    .prologue
    .line 167
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1}, Landroid/content/res/TypedArray;->getType(I)I

    move-result v0

    return v0
.end method

.method private m(I)Landroid/util/TypedValue;
    .registers 3

    .prologue
    .line 175
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(II)I
    .registers 4

    .prologue
    .line 111
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1, p2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    return v0
.end method

.method public final a(I)Landroid/graphics/drawable/Drawable;
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 59
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 60
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 61
    if-eqz v0, :cond_1a

    .line 62
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ax;->a()Landroid/support/v7/internal/widget/av;

    move-result-object v1

    .line 1167
    invoke-virtual {v1, v0, v2}, Landroid/support/v7/internal/widget/av;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 65
    :goto_19
    return-object v0

    :cond_1a
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_19
.end method

.method public final a()Landroid/support/v7/internal/widget/av;
    .registers 2

    .prologue
    .line 191
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->c:Landroid/support/v7/internal/widget/av;

    if-nez v0, :cond_c

    .line 192
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v7/internal/widget/av;->a(Landroid/content/Context;)Landroid/support/v7/internal/widget/av;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/widget/ax;->c:Landroid/support/v7/internal/widget/av;

    .line 194
    :cond_c
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->c:Landroid/support/v7/internal/widget/av;

    return-object v0
.end method

.method public final a(IZ)Z
    .registers 4

    .prologue
    .line 107
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1, p2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    return v0
.end method

.method public final b(II)I
    .registers 4

    .prologue
    .line 135
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1, p2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    return v0
.end method

.method public final b(I)Landroid/graphics/drawable/Drawable;
    .registers 5

    .prologue
    .line 69
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 70
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 71
    if-eqz v0, :cond_1b

    .line 72
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/ax;->a()Landroid/support/v7/internal/widget/av;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/support/v7/internal/widget/av;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 75
    :goto_1a
    return-object v0

    :cond_1b
    const/4 v0, 0x0

    goto :goto_1a
.end method

.method public final c(II)I
    .registers 4

    .prologue
    .line 139
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1, p2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    return v0
.end method

.method public final c(I)Ljava/lang/CharSequence;
    .registers 3

    .prologue
    .line 95
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final d(I)I
    .registers 4

    .prologue
    .line 119
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    const/4 v1, -0x1

    invoke-virtual {v0, p1, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    return v0
.end method

.method public final d(II)I
    .registers 4

    .prologue
    .line 147
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1, p2}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v0

    return v0
.end method

.method public final e(II)I
    .registers 4

    .prologue
    .line 155
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1, p2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    return v0
.end method

.method public final e(I)Z
    .registers 3

    .prologue
    .line 171
    iget-object v0, p0, Landroid/support/v7/internal/widget/ax;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v0, p1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    return v0
.end method
