.class abstract Landroid/support/v7/internal/widget/a;
.super Landroid/view/ViewGroup;
.source "SourceFile"


# static fields
.field private static final g:I = 0xc8


# instance fields
.field protected final a:Landroid/support/v7/internal/widget/c;

.field protected final b:Landroid/content/Context;

.field protected c:Landroid/support/v7/widget/ActionMenuView;

.field protected d:Landroid/support/v7/widget/ActionMenuPresenter;

.field protected e:I

.field protected f:Landroid/support/v4/view/fk;

.field private h:Z

.field private i:Z


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 55
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/internal/widget/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4

    .prologue
    .line 59
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/internal/widget/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 60
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 8

    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    new-instance v0, Landroid/support/v7/internal/widget/c;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/widget/c;-><init>(Landroid/support/v7/internal/widget/a;)V

    iput-object v0, p0, Landroid/support/v7/internal/widget/a;->a:Landroid/support/v7/internal/widget/c;

    .line 65
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 66
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget v2, Landroid/support/v7/a/d;->actionBarPopupTheme:I

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v1

    if-eqz v1, :cond_2a

    iget v1, v0, Landroid/util/TypedValue;->resourceId:I

    if-eqz v1, :cond_2a

    .line 68
    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-direct {v1, p1, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Landroid/support/v7/internal/widget/a;->b:Landroid/content/Context;

    .line 72
    :goto_29
    return-void

    .line 70
    :cond_2a
    iput-object p1, p0, Landroid/support/v7/internal/widget/a;->b:Landroid/content/Context;

    goto :goto_29
.end method

.method protected static a(IIZ)I
    .registers 4

    .prologue
    .line 260
    if-eqz p2, :cond_5

    sub-int v0, p0, p1

    :goto_4
    return v0

    :cond_5
    add-int v0, p0, p1

    goto :goto_4
.end method

.method protected static a(Landroid/view/View;II)I
    .registers 5

    .prologue
    .line 250
    const/high16 v0, -0x80000000

    invoke-static {p1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p0, v0, p2}, Landroid/view/View;->measure(II)V

    .line 253
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    sub-int v0, p1, v0

    .line 254
    add-int/lit8 v0, v0, 0x0

    .line 256
    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method protected static a(Landroid/view/View;IIIZ)I
    .registers 9

    .prologue
    .line 264
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 265
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 266
    sub-int v2, p3, v1

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, p2

    .line 268
    if-eqz p4, :cond_19

    .line 269
    sub-int v3, p1, v0

    add-int/2addr v1, v2

    invoke-virtual {p0, v3, v2, p1, v1}, Landroid/view/View;->layout(IIII)V

    .line 274
    :goto_15
    if-eqz p4, :cond_18

    neg-int v0, v0

    :cond_18
    return v0

    .line 271
    :cond_19
    add-int v3, p1, v0

    add-int/2addr v1, v2

    invoke-virtual {p0, p1, v2, v3, v1}, Landroid/view/View;->layout(IIII)V

    goto :goto_15
.end method

.method static synthetic a(Landroid/support/v7/internal/widget/a;)V
    .registers 2

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-super {p0, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method static synthetic a(Landroid/support/v7/internal/widget/a;I)V
    .registers 2

    .prologue
    .line 37
    invoke-super {p0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public a(IJ)Landroid/support/v4/view/fk;
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 163
    iget-object v0, p0, Landroid/support/v7/internal/widget/a;->f:Landroid/support/v4/view/fk;

    if-eqz v0, :cond_a

    .line 164
    iget-object v0, p0, Landroid/support/v7/internal/widget/a;->f:Landroid/support/v4/view/fk;

    invoke-virtual {v0}, Landroid/support/v4/view/fk;->a()V

    .line 167
    :cond_a
    if-nez p1, :cond_2c

    .line 168
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/a;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_15

    .line 169
    invoke-static {p0, v1}, Landroid/support/v4/view/cx;->c(Landroid/view/View;F)V

    .line 171
    :cond_15
    invoke-static {p0}, Landroid/support/v4/view/cx;->p(Landroid/view/View;)Landroid/support/v4/view/fk;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(F)Landroid/support/v4/view/fk;

    move-result-object v0

    .line 172
    invoke-virtual {v0, p2, p3}, Landroid/support/v4/view/fk;->a(J)Landroid/support/v4/view/fk;

    .line 173
    iget-object v1, p0, Landroid/support/v7/internal/widget/a;->a:Landroid/support/v7/internal/widget/c;

    invoke-virtual {v1, v0, p1}, Landroid/support/v7/internal/widget/c;->a(Landroid/support/v4/view/fk;I)Landroid/support/v7/internal/widget/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;

    .line 179
    :goto_2b
    return-object v0

    .line 176
    :cond_2c
    invoke-static {p0}, Landroid/support/v4/view/cx;->p(Landroid/view/View;)Landroid/support/v4/view/fk;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(F)Landroid/support/v4/view/fk;

    move-result-object v0

    .line 177
    invoke-virtual {v0, p2, p3}, Landroid/support/v4/view/fk;->a(J)Landroid/support/v4/view/fk;

    .line 178
    iget-object v1, p0, Landroid/support/v7/internal/widget/a;->a:Landroid/support/v7/internal/widget/c;

    invoke-virtual {v1, v0, p1}, Landroid/support/v7/internal/widget/c;->a(Landroid/support/v4/view/fk;I)Landroid/support/v7/internal/widget/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;

    goto :goto_2b
.end method

.method public a(I)V
    .registers 4

    .prologue
    .line 184
    const-wide/16 v0, 0xc8

    invoke-virtual {p0, p1, v0, v1}, Landroid/support/v7/internal/widget/a;->a(IJ)Landroid/support/v4/view/fk;

    move-result-object v0

    .line 185
    invoke-virtual {v0}, Landroid/support/v4/view/fk;->b()V

    .line 186
    return-void
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 199
    iget-object v0, p0, Landroid/support/v7/internal/widget/a;->d:Landroid/support/v7/widget/ActionMenuPresenter;

    if-eqz v0, :cond_b

    .line 200
    iget-object v0, p0, Landroid/support/v7/internal/widget/a;->d:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuPresenter;->e()Z

    move-result v0

    .line 202
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public b()V
    .registers 2

    .prologue
    .line 206
    new-instance v0, Landroid/support/v7/internal/widget/b;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/widget/b;-><init>(Landroid/support/v7/internal/widget/a;)V

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/a;->post(Ljava/lang/Runnable;)Z

    .line 211
    return-void
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 214
    iget-object v0, p0, Landroid/support/v7/internal/widget/a;->d:Landroid/support/v7/widget/ActionMenuPresenter;

    if-eqz v0, :cond_b

    .line 215
    iget-object v0, p0, Landroid/support/v7/internal/widget/a;->d:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuPresenter;->f()Z

    move-result v0

    .line 217
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public d()Z
    .registers 2

    .prologue
    .line 221
    iget-object v0, p0, Landroid/support/v7/internal/widget/a;->d:Landroid/support/v7/widget/ActionMenuPresenter;

    if-eqz v0, :cond_b

    .line 222
    iget-object v0, p0, Landroid/support/v7/internal/widget/a;->d:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuPresenter;->i()Z

    move-result v0

    .line 224
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public e()Z
    .registers 2

    .prologue
    .line 228
    iget-object v0, p0, Landroid/support/v7/internal/widget/a;->d:Landroid/support/v7/widget/ActionMenuPresenter;

    if-eqz v0, :cond_b

    .line 229
    iget-object v0, p0, Landroid/support/v7/internal/widget/a;->d:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuPresenter;->j()Z

    move-result v0

    .line 231
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public f()Z
    .registers 2

    .prologue
    .line 235
    iget-object v0, p0, Landroid/support/v7/internal/widget/a;->d:Landroid/support/v7/widget/ActionMenuPresenter;

    if-eqz v0, :cond_c

    iget-object v0, p0, Landroid/support/v7/internal/widget/a;->d:Landroid/support/v7/widget/ActionMenuPresenter;

    .line 1400
    iget-boolean v0, v0, Landroid/support/v7/widget/ActionMenuPresenter;->l:Z

    .line 235
    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public g()Z
    .registers 2

    .prologue
    .line 239
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/a;->f()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/a;->getVisibility()I

    move-result v0

    if-nez v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public getAnimatedVisibility()I
    .registers 2

    .prologue
    .line 156
    iget-object v0, p0, Landroid/support/v7/internal/widget/a;->f:Landroid/support/v4/view/fk;

    if-eqz v0, :cond_9

    .line 157
    iget-object v0, p0, Landroid/support/v7/internal/widget/a;->a:Landroid/support/v7/internal/widget/c;

    iget v0, v0, Landroid/support/v7/internal/widget/c;->a:I

    .line 159
    :goto_8
    return v0

    :cond_9
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/a;->getVisibility()I

    move-result v0

    goto :goto_8
.end method

.method public getContentHeight()I
    .registers 2

    .prologue
    .line 149
    iget v0, p0, Landroid/support/v7/internal/widget/a;->e:I

    return v0
.end method

.method public h()V
    .registers 2

    .prologue
    .line 243
    iget-object v0, p0, Landroid/support/v7/internal/widget/a;->d:Landroid/support/v7/widget/ActionMenuPresenter;

    if-eqz v0, :cond_9

    .line 244
    iget-object v0, p0, Landroid/support/v7/internal/widget/a;->d:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuPresenter;->g()Z

    .line 246
    :cond_9
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 7

    .prologue
    const/4 v4, 0x0

    .line 76
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_a

    .line 77
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 82
    :cond_a
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/a;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    sget-object v2, Landroid/support/v7/a/n;->ActionBar:[I

    sget v3, Landroid/support/v7/a/d;->actionBarStyle:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 84
    sget v1, Landroid/support/v7/a/n;->ActionBar_height:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v1

    invoke-virtual {p0, v1}, Landroid/support/v7/internal/widget/a;->setContentHeight(I)V

    .line 85
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 87
    iget-object v0, p0, Landroid/support/v7/internal/widget/a;->d:Landroid/support/v7/widget/ActionMenuPresenter;

    if-eqz v0, :cond_45

    .line 88
    iget-object v0, p0, Landroid/support/v7/internal/widget/a;->d:Landroid/support/v7/widget/ActionMenuPresenter;

    .line 1137
    iget-boolean v1, v0, Landroid/support/v7/widget/ActionMenuPresenter;->n:Z

    if-nez v1, :cond_3b

    .line 1138
    iget-object v1, v0, Landroid/support/v7/widget/ActionMenuPresenter;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Landroid/support/v7/a/j;->abc_max_action_buttons:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, v0, Landroid/support/v7/widget/ActionMenuPresenter;->m:I

    .line 1141
    :cond_3b
    iget-object v1, v0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/support/v7/internal/view/menu/i;

    if-eqz v1, :cond_45

    .line 1142
    iget-object v0, v0, Landroid/support/v7/widget/ActionMenuPresenter;->c:Landroid/support/v7/internal/view/menu/i;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->c(Z)V

    .line 90
    :cond_45
    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .registers 7

    .prologue
    const/16 v4, 0x9

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 123
    invoke-static {p1}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;)I

    move-result v0

    .line 124
    if-ne v0, v4, :cond_c

    .line 125
    iput-boolean v2, p0, Landroid/support/v7/internal/widget/a;->i:Z

    .line 128
    :cond_c
    iget-boolean v1, p0, Landroid/support/v7/internal/widget/a;->i:Z

    if-nez v1, :cond_1a

    .line 129
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 130
    if-ne v0, v4, :cond_1a

    if-nez v1, :cond_1a

    .line 131
    iput-boolean v3, p0, Landroid/support/v7/internal/widget/a;->i:Z

    .line 135
    :cond_1a
    const/16 v1, 0xa

    if-eq v0, v1, :cond_21

    const/4 v1, 0x3

    if-ne v0, v1, :cond_23

    .line 137
    :cond_21
    iput-boolean v2, p0, Landroid/support/v7/internal/widget/a;->i:Z

    .line 140
    :cond_23
    return v3
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 99
    invoke-static {p1}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;)I

    move-result v0

    .line 100
    if-nez v0, :cond_a

    .line 101
    iput-boolean v3, p0, Landroid/support/v7/internal/widget/a;->h:Z

    .line 104
    :cond_a
    iget-boolean v1, p0, Landroid/support/v7/internal/widget/a;->h:Z

    if-nez v1, :cond_18

    .line 105
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 106
    if-nez v0, :cond_18

    if-nez v1, :cond_18

    .line 107
    iput-boolean v2, p0, Landroid/support/v7/internal/widget/a;->h:Z

    .line 111
    :cond_18
    if-eq v0, v2, :cond_1d

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1f

    .line 112
    :cond_1d
    iput-boolean v3, p0, Landroid/support/v7/internal/widget/a;->h:Z

    .line 115
    :cond_1f
    return v2
.end method

.method public setContentHeight(I)V
    .registers 2

    .prologue
    .line 144
    iput p1, p0, Landroid/support/v7/internal/widget/a;->e:I

    .line 145
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/a;->requestLayout()V

    .line 146
    return-void
.end method

.method public setVisibility(I)V
    .registers 3

    .prologue
    .line 190
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/a;->getVisibility()I

    move-result v0

    if-eq p1, v0, :cond_12

    .line 191
    iget-object v0, p0, Landroid/support/v7/internal/widget/a;->f:Landroid/support/v4/view/fk;

    if-eqz v0, :cond_f

    .line 192
    iget-object v0, p0, Landroid/support/v7/internal/widget/a;->f:Landroid/support/v4/view/fk;

    invoke-virtual {v0}, Landroid/support/v4/view/fk;->a()V

    .line 194
    :cond_f
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 196
    :cond_12
    return-void
.end method
