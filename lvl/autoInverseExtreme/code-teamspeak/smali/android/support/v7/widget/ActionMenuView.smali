.class public Landroid/support/v7/widget/ActionMenuView;
.super Landroid/support/v7/widget/aj;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/internal/view/menu/k;
.implements Landroid/support/v7/internal/view/menu/z;


# static fields
.field static final a:I = 0x38

.field static final b:I = 0x4

.field private static final n:Ljava/lang/String; = "ActionMenuView"


# instance fields
.field public c:Landroid/support/v7/internal/view/menu/i;

.field public d:Z

.field public e:Landroid/support/v7/widget/ActionMenuPresenter;

.field f:Landroid/support/v7/internal/view/menu/y;

.field g:Landroid/support/v7/internal/view/menu/j;

.field private o:Landroid/content/Context;

.field private p:I

.field private q:Z

.field private r:I

.field private s:I

.field private t:I

.field private u:Landroid/support/v7/widget/o;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 74
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/ActionMenuView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6

    .prologue
    const/4 v2, 0x0

    .line 78
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/aj;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 79
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/ActionMenuView;->setBaselineAligned(Z)V

    .line 80
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 81
    const/high16 v1, 0x42600000    # 56.0f

    mul-float/2addr v1, v0

    float-to-int v1, v1

    iput v1, p0, Landroid/support/v7/widget/ActionMenuView;->s:I

    .line 82
    const/high16 v1, 0x40800000    # 4.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/ActionMenuView;->t:I

    .line 83
    iput-object p1, p0, Landroid/support/v7/widget/ActionMenuView;->o:Landroid/content/Context;

    .line 84
    iput v2, p0, Landroid/support/v7/widget/ActionMenuView;->p:I

    .line 85
    return-void
.end method

.method static a(Landroid/view/View;IIII)I
    .registers 13

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 403
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/m;

    .line 405
    invoke-static {p3}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    sub-int/2addr v1, p4

    .line 407
    invoke-static {p3}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    .line 408
    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 410
    instance-of v1, p0, Landroid/support/v7/internal/view/menu/ActionMenuItemView;

    if-eqz v1, :cond_5e

    move-object v1, p0

    check-cast v1, Landroid/support/v7/internal/view/menu/ActionMenuItemView;

    .line 412
    :goto_1d
    if-eqz v1, :cond_60

    invoke-virtual {v1}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->c()Z

    move-result v1

    if-eqz v1, :cond_60

    move v5, v4

    .line 415
    :goto_26
    if-lez p2, :cond_62

    if-eqz v5, :cond_2c

    if-lt p2, v3, :cond_62

    .line 416
    :cond_2c
    mul-int v1, p1, p2

    const/high16 v7, -0x80000000

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 418
    invoke-virtual {p0, v1, v6}, Landroid/view/View;->measure(II)V

    .line 420
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    .line 421
    div-int v1, v7, p1

    .line 422
    rem-int/2addr v7, p1

    if-eqz v7, :cond_42

    add-int/lit8 v1, v1, 0x1

    .line 423
    :cond_42
    if-eqz v5, :cond_47

    if-ge v1, v3, :cond_47

    move v1, v3

    .line 426
    :cond_47
    :goto_47
    iget-boolean v3, v0, Landroid/support/v7/widget/m;->a:Z

    if-nez v3, :cond_4e

    if-eqz v5, :cond_4e

    move v2, v4

    .line 427
    :cond_4e
    iput-boolean v2, v0, Landroid/support/v7/widget/m;->d:Z

    .line 429
    iput v1, v0, Landroid/support/v7/widget/m;->b:I

    .line 430
    mul-int v0, v1, p1

    .line 431
    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p0, v0, v6}, Landroid/view/View;->measure(II)V

    .line 433
    return v1

    .line 410
    :cond_5e
    const/4 v1, 0x0

    goto :goto_1d

    :cond_60
    move v5, v2

    .line 412
    goto :goto_26

    :cond_62
    move v1, v2

    goto :goto_47
.end method

.method public static a()Landroid/support/v7/widget/m;
    .registers 2

    .prologue
    .line 612
    invoke-static {}, Landroid/support/v7/widget/ActionMenuView;->e()Landroid/support/v7/widget/m;

    move-result-object v0

    .line 613
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/m;->a:Z

    .line 614
    return-object v0
.end method

.method protected static a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/m;
    .registers 3

    .prologue
    .line 593
    if-eqz p0, :cond_1c

    .line 594
    instance-of v0, p0, Landroid/support/v7/widget/m;

    if-eqz v0, :cond_16

    new-instance v0, Landroid/support/v7/widget/m;

    check-cast p0, Landroid/support/v7/widget/m;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/m;-><init>(Landroid/support/v7/widget/m;)V

    .line 597
    :goto_d
    iget v1, v0, Landroid/support/v7/widget/m;->h:I

    if-gtz v1, :cond_15

    .line 598
    const/16 v1, 0x10

    iput v1, v0, Landroid/support/v7/widget/m;->h:I

    .line 602
    :cond_15
    :goto_15
    return-object v0

    .line 594
    :cond_16
    new-instance v0, Landroid/support/v7/widget/m;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/m;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_d

    .line 602
    :cond_1c
    invoke-static {}, Landroid/support/v7/widget/ActionMenuView;->e()Landroid/support/v7/widget/m;

    move-result-object v0

    goto :goto_15
.end method

.method static synthetic a(Landroid/support/v7/widget/ActionMenuView;)Landroid/support/v7/widget/o;
    .registers 2

    .prologue
    .line 46
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->u:Landroid/support/v7/widget/o;

    return-object v0
.end method

.method private a(II)V
    .registers 37

    .prologue
    .line 177
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v23

    .line 178
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 179
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v17

    .line 181
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingLeft()I

    move-result v7

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingRight()I

    move-result v8

    add-int/2addr v7, v8

    .line 182
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingTop()I

    move-result v8

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingBottom()I

    move-result v9

    add-int v19, v8, v9

    .line 184
    const/4 v8, -0x2

    move/from16 v0, p2

    move/from16 v1, v19

    invoke-static {v0, v1, v8}, Landroid/support/v7/widget/ActionMenuView;->getChildMeasureSpec(III)I

    move-result v24

    .line 187
    sub-int v25, v6, v7

    .line 190
    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/widget/ActionMenuView;->s:I

    div-int v9, v25, v6

    .line 191
    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/widget/ActionMenuView;->s:I

    rem-int v6, v25, v6

    .line 193
    if-nez v9, :cond_41

    .line 195
    const/4 v6, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1, v6}, Landroid/support/v7/widget/ActionMenuView;->setMeasuredDimension(II)V

    .line 386
    :goto_40
    return-void

    .line 199
    :cond_41
    move-object/from16 v0, p0

    iget v7, v0, Landroid/support/v7/widget/ActionMenuView;->s:I

    div-int/2addr v6, v9

    add-int v26, v7, v6

    .line 202
    const/16 v16, 0x0

    .line 203
    const/4 v15, 0x0

    .line 204
    const/4 v10, 0x0

    .line 205
    const/4 v7, 0x0

    .line 206
    const/4 v11, 0x0

    .line 209
    const-wide/16 v12, 0x0

    .line 211
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getChildCount()I

    move-result v27

    .line 212
    const/4 v6, 0x0

    move/from16 v18, v6

    :goto_57
    move/from16 v0, v18

    move/from16 v1, v27

    if-ge v0, v1, :cond_fc

    .line 213
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 214
    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v6

    const/16 v14, 0x8

    if-eq v6, v14, :cond_30f

    .line 216
    instance-of v0, v8, Landroid/support/v7/internal/view/menu/ActionMenuItemView;

    move/from16 v20, v0

    .line 217
    add-int/lit8 v14, v7, 0x1

    .line 219
    if-eqz v20, :cond_89

    .line 222
    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/widget/ActionMenuView;->t:I

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v7/widget/ActionMenuView;->t:I

    move/from16 v21, v0

    const/16 v22, 0x0

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v8, v6, v7, v0, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 225
    :cond_89
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/support/v7/widget/m;

    .line 226
    const/4 v7, 0x0

    iput-boolean v7, v6, Landroid/support/v7/widget/m;->f:Z

    .line 227
    const/4 v7, 0x0

    iput v7, v6, Landroid/support/v7/widget/m;->c:I

    .line 228
    const/4 v7, 0x0

    iput v7, v6, Landroid/support/v7/widget/m;->b:I

    .line 229
    const/4 v7, 0x0

    iput-boolean v7, v6, Landroid/support/v7/widget/m;->d:Z

    .line 230
    const/4 v7, 0x0

    iput v7, v6, Landroid/support/v7/widget/m;->leftMargin:I

    .line 231
    const/4 v7, 0x0

    iput v7, v6, Landroid/support/v7/widget/m;->rightMargin:I

    .line 232
    if-eqz v20, :cond_f8

    move-object v7, v8

    check-cast v7, Landroid/support/v7/internal/view/menu/ActionMenuItemView;

    invoke-virtual {v7}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->c()Z

    move-result v7

    if-eqz v7, :cond_f8

    const/4 v7, 0x1

    :goto_ad
    iput-boolean v7, v6, Landroid/support/v7/widget/m;->e:Z

    .line 235
    iget-boolean v7, v6, Landroid/support/v7/widget/m;->a:Z

    if-eqz v7, :cond_fa

    const/4 v7, 0x1

    .line 237
    :goto_b4
    move/from16 v0, v26

    move/from16 v1, v24

    move/from16 v2, v19

    invoke-static {v8, v0, v7, v1, v2}, Landroid/support/v7/widget/ActionMenuView;->a(Landroid/view/View;IIII)I

    move-result v20

    .line 240
    move/from16 v0, v20

    invoke-static {v15, v0}, Ljava/lang/Math;->max(II)I

    move-result v15

    .line 241
    iget-boolean v7, v6, Landroid/support/v7/widget/m;->d:Z

    if-eqz v7, :cond_30c

    add-int/lit8 v7, v10, 0x1

    .line 242
    :goto_ca
    iget-boolean v6, v6, Landroid/support/v7/widget/m;->a:Z

    if-eqz v6, :cond_309

    const/4 v6, 0x1

    .line 244
    :goto_cf
    sub-int v11, v9, v20

    .line 245
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    move/from16 v0, v16

    invoke-static {v0, v8}, Ljava/lang/Math;->max(II)I

    move-result v10

    .line 246
    const/4 v8, 0x1

    move/from16 v0, v20

    if-ne v0, v8, :cond_2fd

    const/4 v8, 0x1

    shl-int v8, v8, v18

    int-to-long v8, v8

    or-long/2addr v8, v12

    move v12, v10

    move v13, v11

    move v10, v7

    move v11, v6

    move-wide v6, v8

    move v9, v15

    move v8, v14

    .line 212
    :goto_ec
    add-int/lit8 v14, v18, 0x1

    move/from16 v18, v14

    move v15, v9

    move/from16 v16, v12

    move v9, v13

    move-wide v12, v6

    move v7, v8

    goto/16 :goto_57

    .line 232
    :cond_f8
    const/4 v7, 0x0

    goto :goto_ad

    :cond_fa
    move v7, v9

    .line 235
    goto :goto_b4

    .line 251
    :cond_fc
    if-eqz v11, :cond_146

    const/4 v6, 0x2

    if-ne v7, v6, :cond_146

    const/4 v6, 0x1

    move v8, v6

    .line 256
    :goto_103
    const/16 v18, 0x0

    move-wide/from16 v20, v12

    move/from16 v19, v9

    .line 257
    :goto_109
    if-lez v10, :cond_1e4

    if-lez v19, :cond_1e4

    .line 258
    const v14, 0x7fffffff

    .line 259
    const-wide/16 v12, 0x0

    .line 260
    const/4 v9, 0x0

    .line 261
    const/4 v6, 0x0

    move/from16 v22, v6

    :goto_116
    move/from16 v0, v22

    move/from16 v1, v27

    if-ge v0, v1, :cond_159

    .line 262
    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 263
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/support/v7/widget/m;

    .line 266
    iget-boolean v0, v6, Landroid/support/v7/widget/m;->d:Z

    move/from16 v28, v0

    if-eqz v28, :cond_2f9

    .line 269
    iget v0, v6, Landroid/support/v7/widget/m;->b:I

    move/from16 v28, v0

    move/from16 v0, v28

    if-ge v0, v14, :cond_149

    .line 270
    iget v9, v6, Landroid/support/v7/widget/m;->b:I

    .line 271
    const/4 v6, 0x1

    shl-int v6, v6, v22

    int-to-long v12, v6

    .line 272
    const/4 v6, 0x1

    .line 261
    :goto_13f
    add-int/lit8 v14, v22, 0x1

    move/from16 v22, v14

    move v14, v9

    move v9, v6

    goto :goto_116

    .line 251
    :cond_146
    const/4 v6, 0x0

    move v8, v6

    goto :goto_103

    .line 273
    :cond_149
    iget v6, v6, Landroid/support/v7/widget/m;->b:I

    if-ne v6, v14, :cond_2f9

    .line 274
    const/4 v6, 0x1

    shl-int v6, v6, v22

    int-to-long v0, v6

    move-wide/from16 v28, v0

    or-long v12, v12, v28

    .line 275
    add-int/lit8 v6, v9, 0x1

    move v9, v14

    goto :goto_13f

    .line 280
    :cond_159
    or-long v20, v20, v12

    .line 282
    move/from16 v0, v19

    if-gt v9, v0, :cond_1e4

    .line 285
    add-int/lit8 v22, v14, 0x1

    .line 287
    const/4 v6, 0x0

    move v14, v6

    move/from16 v9, v19

    move-wide/from16 v18, v20

    :goto_167
    move/from16 v0, v27

    if-ge v14, v0, :cond_1db

    .line 288
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v20

    .line 289
    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/support/v7/widget/m;

    .line 290
    const/16 v21, 0x1

    shl-int v21, v21, v14

    move/from16 v0, v21

    int-to-long v0, v0

    move-wide/from16 v28, v0

    and-long v28, v28, v12

    const-wide/16 v30, 0x0

    cmp-long v21, v28, v30

    if-nez v21, :cond_19b

    .line 292
    iget v6, v6, Landroid/support/v7/widget/m;->b:I

    move/from16 v0, v22

    if-ne v6, v0, :cond_2f6

    const/4 v6, 0x1

    shl-int/2addr v6, v14

    int-to-long v0, v6

    move-wide/from16 v20, v0

    or-long v18, v18, v20

    move v6, v9

    .line 287
    :goto_196
    add-int/lit8 v9, v14, 0x1

    move v14, v9

    move v9, v6

    goto :goto_167

    .line 296
    :cond_19b
    if-eqz v8, :cond_1c8

    iget-boolean v0, v6, Landroid/support/v7/widget/m;->e:Z

    move/from16 v21, v0

    if-eqz v21, :cond_1c8

    const/16 v21, 0x1

    move/from16 v0, v21

    if-ne v9, v0, :cond_1c8

    .line 298
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v7/widget/ActionMenuView;->t:I

    move/from16 v21, v0

    add-int v21, v21, v26

    const/16 v28, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v7/widget/ActionMenuView;->t:I

    move/from16 v29, v0

    const/16 v30, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v28

    move/from16 v3, v29

    move/from16 v4, v30

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 300
    :cond_1c8
    iget v0, v6, Landroid/support/v7/widget/m;->b:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, 0x1

    move/from16 v0, v20

    iput v0, v6, Landroid/support/v7/widget/m;->b:I

    .line 301
    const/16 v20, 0x1

    move/from16 v0, v20

    iput-boolean v0, v6, Landroid/support/v7/widget/m;->f:Z

    .line 302
    add-int/lit8 v6, v9, -0x1

    goto :goto_196

    .line 305
    :cond_1db
    const/4 v6, 0x1

    move-wide/from16 v20, v18

    move/from16 v18, v6

    move/from16 v19, v9

    .line 306
    goto/16 :goto_109

    :cond_1e4
    move-wide/from16 v12, v20

    .line 311
    if-nez v11, :cond_289

    const/4 v6, 0x1

    if-ne v7, v6, :cond_289

    const/4 v6, 0x1

    .line 312
    :goto_1ec
    if-lez v19, :cond_2af

    const-wide/16 v8, 0x0

    cmp-long v8, v12, v8

    if-eqz v8, :cond_2af

    add-int/lit8 v7, v7, -0x1

    move/from16 v0, v19

    if-lt v0, v7, :cond_1ff

    if-nez v6, :cond_1ff

    const/4 v7, 0x1

    if-le v15, v7, :cond_2af

    .line 314
    :cond_1ff
    invoke-static {v12, v13}, Ljava/lang/Long;->bitCount(J)I

    move-result v7

    int-to-float v7, v7

    .line 316
    if-nez v6, :cond_2f3

    .line 318
    const-wide/16 v8, 0x1

    and-long/2addr v8, v12

    const-wide/16 v10, 0x0

    cmp-long v6, v8, v10

    if-eqz v6, :cond_223

    .line 319
    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/support/v7/widget/m;

    .line 320
    iget-boolean v6, v6, Landroid/support/v7/widget/m;->e:Z

    if-nez v6, :cond_223

    const/high16 v6, 0x3f000000    # 0.5f

    sub-float/2addr v7, v6

    .line 322
    :cond_223
    const/4 v6, 0x1

    add-int/lit8 v8, v27, -0x1

    shl-int/2addr v6, v8

    int-to-long v8, v6

    and-long/2addr v8, v12

    const-wide/16 v10, 0x0

    cmp-long v6, v8, v10

    if-eqz v6, :cond_2f3

    .line 323
    add-int/lit8 v6, v27, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/support/v7/widget/m;

    .line 324
    iget-boolean v6, v6, Landroid/support/v7/widget/m;->e:Z

    if-nez v6, :cond_2f3

    const/high16 v6, 0x3f000000    # 0.5f

    sub-float v6, v7, v6

    .line 328
    :goto_245
    const/4 v7, 0x0

    cmpl-float v7, v6, v7

    if-lez v7, :cond_28c

    mul-int v7, v19, v26

    int-to-float v7, v7

    div-float v6, v7, v6

    float-to-int v6, v6

    move v7, v6

    .line 331
    :goto_251
    const/4 v6, 0x0

    move v9, v6

    move/from16 v8, v18

    :goto_255
    move/from16 v0, v27

    if-ge v9, v0, :cond_2b1

    .line 332
    const/4 v6, 0x1

    shl-int/2addr v6, v9

    int-to-long v10, v6

    and-long/2addr v10, v12

    const-wide/16 v14, 0x0

    cmp-long v6, v10, v14

    if-eqz v6, :cond_2ad

    .line 334
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    .line 335
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/support/v7/widget/m;

    .line 336
    instance-of v10, v10, Landroid/support/v7/internal/view/menu/ActionMenuItemView;

    if-eqz v10, :cond_28f

    .line 338
    iput v7, v6, Landroid/support/v7/widget/m;->c:I

    .line 339
    const/4 v8, 0x1

    iput-boolean v8, v6, Landroid/support/v7/widget/m;->f:Z

    .line 340
    if-nez v9, :cond_283

    iget-boolean v8, v6, Landroid/support/v7/widget/m;->e:Z

    if-nez v8, :cond_283

    .line 343
    neg-int v8, v7

    div-int/lit8 v8, v8, 0x2

    iput v8, v6, Landroid/support/v7/widget/m;->leftMargin:I

    .line 345
    :cond_283
    const/4 v6, 0x1

    .line 331
    :goto_284
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    move v8, v6

    goto :goto_255

    .line 311
    :cond_289
    const/4 v6, 0x0

    goto/16 :goto_1ec

    .line 328
    :cond_28c
    const/4 v6, 0x0

    move v7, v6

    goto :goto_251

    .line 346
    :cond_28f
    iget-boolean v10, v6, Landroid/support/v7/widget/m;->a:Z

    if-eqz v10, :cond_29f

    .line 347
    iput v7, v6, Landroid/support/v7/widget/m;->c:I

    .line 348
    const/4 v8, 0x1

    iput-boolean v8, v6, Landroid/support/v7/widget/m;->f:Z

    .line 349
    neg-int v8, v7

    div-int/lit8 v8, v8, 0x2

    iput v8, v6, Landroid/support/v7/widget/m;->rightMargin:I

    .line 350
    const/4 v6, 0x1

    goto :goto_284

    .line 355
    :cond_29f
    if-eqz v9, :cond_2a5

    .line 356
    div-int/lit8 v10, v7, 0x2

    iput v10, v6, Landroid/support/v7/widget/m;->leftMargin:I

    .line 358
    :cond_2a5
    add-int/lit8 v10, v27, -0x1

    if-eq v9, v10, :cond_2ad

    .line 359
    div-int/lit8 v10, v7, 0x2

    iput v10, v6, Landroid/support/v7/widget/m;->rightMargin:I

    :cond_2ad
    move v6, v8

    goto :goto_284

    :cond_2af
    move/from16 v8, v18

    .line 368
    :cond_2b1
    if-eqz v8, :cond_2df

    .line 369
    const/4 v6, 0x0

    move v7, v6

    :goto_2b5
    move/from16 v0, v27

    if-ge v7, v0, :cond_2df

    .line 370
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 371
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/support/v7/widget/m;

    .line 373
    iget-boolean v9, v6, Landroid/support/v7/widget/m;->f:Z

    if-eqz v9, :cond_2db

    .line 375
    iget v9, v6, Landroid/support/v7/widget/m;->b:I

    mul-int v9, v9, v26

    iget v6, v6, Landroid/support/v7/widget/m;->c:I

    add-int/2addr v6, v9

    .line 376
    const/high16 v9, 0x40000000    # 2.0f

    invoke-static {v6, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    move/from16 v0, v24

    invoke-virtual {v8, v6, v0}, Landroid/view/View;->measure(II)V

    .line 369
    :cond_2db
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_2b5

    .line 381
    :cond_2df
    const/high16 v6, 0x40000000    # 2.0f

    move/from16 v0, v23

    if-eq v0, v6, :cond_2f0

    .line 385
    :goto_2e5
    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/ActionMenuView;->setMeasuredDimension(II)V

    goto/16 :goto_40

    :cond_2f0
    move/from16 v16, v17

    goto :goto_2e5

    :cond_2f3
    move v6, v7

    goto/16 :goto_245

    :cond_2f6
    move v6, v9

    goto/16 :goto_196

    :cond_2f9
    move v6, v9

    move v9, v14

    goto/16 :goto_13f

    :cond_2fd
    move v8, v14

    move v9, v15

    move-wide/from16 v32, v12

    move v12, v10

    move v13, v11

    move v11, v6

    move v10, v7

    move-wide/from16 v6, v32

    goto/16 :goto_ec

    :cond_309
    move v6, v11

    goto/16 :goto_cf

    :cond_30c
    move v7, v10

    goto/16 :goto_ca

    :cond_30f
    move v8, v7

    move-wide v6, v12

    move/from16 v12, v16

    move v13, v9

    move v9, v15

    goto/16 :goto_ec
.end method

.method private a(Landroid/support/v7/internal/view/menu/y;Landroid/support/v7/internal/view/menu/j;)V
    .registers 3

    .prologue
    .line 661
    iput-object p1, p0, Landroid/support/v7/widget/ActionMenuView;->f:Landroid/support/v7/internal/view/menu/y;

    .line 662
    iput-object p2, p0, Landroid/support/v7/widget/ActionMenuView;->g:Landroid/support/v7/internal/view/menu/j;

    .line 663
    return-void
.end method

.method private a(I)Z
    .registers 6

    .prologue
    const/4 v2, 0x0

    .line 719
    if-nez p1, :cond_5

    move v0, v2

    .line 731
    :goto_4
    return v0

    .line 722
    :cond_5
    add-int/lit8 v0, p1, -0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 723
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 725
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getChildCount()I

    move-result v3

    if-ge p1, v3, :cond_21

    instance-of v3, v0, Landroid/support/v7/widget/k;

    if-eqz v3, :cond_21

    .line 726
    check-cast v0, Landroid/support/v7/widget/k;

    invoke-interface {v0}, Landroid/support/v7/widget/k;->e()Z

    move-result v0

    or-int/lit8 v2, v0, 0x0

    .line 728
    :cond_21
    if-lez p1, :cond_30

    instance-of v0, v1, Landroid/support/v7/widget/k;

    if-eqz v0, :cond_30

    move-object v0, v1

    .line 729
    check-cast v0, Landroid/support/v7/widget/k;

    invoke-interface {v0}, Landroid/support/v7/widget/k;->d()Z

    move-result v0

    or-int/2addr v0, v2

    goto :goto_4

    :cond_30
    move v0, v2

    goto :goto_4
.end method

.method static synthetic b(Landroid/support/v7/widget/ActionMenuView;)Landroid/support/v7/internal/view/menu/j;
    .registers 2

    .prologue
    .line 46
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->g:Landroid/support/v7/internal/view/menu/j;

    return-object v0
.end method

.method private b(Landroid/util/AttributeSet;)Landroid/support/v7/widget/m;
    .registers 4

    .prologue
    .line 588
    new-instance v0, Landroid/support/v7/widget/m;

    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/support/v7/widget/m;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method private d()Z
    .registers 2

    .prologue
    .line 570
    iget-boolean v0, p0, Landroid/support/v7/widget/ActionMenuView;->d:Z

    return v0
.end method

.method private static e()Landroid/support/v7/widget/m;
    .registers 2

    .prologue
    .line 580
    new-instance v0, Landroid/support/v7/widget/m;

    invoke-direct {v0}, Landroid/support/v7/widget/m;-><init>()V

    .line 582
    const/16 v1, 0x10

    iput v1, v0, Landroid/support/v7/widget/m;->h:I

    .line 583
    return-object v0
.end method

.method private f()Landroid/support/v7/internal/view/menu/i;
    .registers 2

    .prologue
    .line 670
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->c:Landroid/support/v7/internal/view/menu/i;

    return-object v0
.end method

.method private g()Z
    .registers 2

    .prologue
    .line 679
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/ActionMenuPresenter;

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuPresenter;->e()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private h()Z
    .registers 2

    .prologue
    .line 688
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/ActionMenuPresenter;

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuPresenter;->f()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private i()Z
    .registers 2

    .prologue
    .line 698
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/ActionMenuPresenter;

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuPresenter;->i()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private j()Z
    .registers 2

    .prologue
    .line 703
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/ActionMenuPresenter;

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuPresenter;->j()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method


# virtual methods
.method public final synthetic a(Landroid/util/AttributeSet;)Landroid/support/v7/widget/al;
    .registers 3

    .prologue
    .line 46
    invoke-direct {p0, p1}, Landroid/support/v7/widget/ActionMenuView;->b(Landroid/util/AttributeSet;)Landroid/support/v7/widget/m;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/support/v7/internal/view/menu/i;)V
    .registers 2

    .prologue
    .line 629
    iput-object p1, p0, Landroid/support/v7/widget/ActionMenuView;->c:Landroid/support/v7/internal/view/menu/i;

    .line 630
    return-void
.end method

.method public final a(Landroid/support/v7/internal/view/menu/m;)Z
    .registers 5

    .prologue
    .line 619
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->c:Landroid/support/v7/internal/view/menu/i;

    .line 2948
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/view/MenuItem;Landroid/support/v7/internal/view/menu/x;I)Z

    move-result v0

    .line 619
    return v0
.end method

.method protected final synthetic b(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/al;
    .registers 3

    .prologue
    .line 46
    invoke-static {p1}, Landroid/support/v7/widget/ActionMenuView;->a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/m;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 710
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/ActionMenuPresenter;

    if-eqz v0, :cond_9

    .line 711
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuPresenter;->g()Z

    .line 713
    :cond_9
    return-void
.end method

.method protected final synthetic c()Landroid/support/v7/widget/al;
    .registers 2

    .prologue
    .line 46
    invoke-static {}, Landroid/support/v7/widget/ActionMenuView;->e()Landroid/support/v7/widget/m;

    move-result-object v0

    return-object v0
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .registers 3

    .prologue
    .line 607
    if-eqz p1, :cond_8

    instance-of v0, p1, Landroid/support/v7/widget/m;

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 3

    .prologue
    .line 735
    const/4 v0, 0x0

    return v0
.end method

.method protected synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .registers 2

    .prologue
    .line 46
    invoke-static {}, Landroid/support/v7/widget/ActionMenuView;->e()Landroid/support/v7/widget/m;

    move-result-object v0

    return-object v0
.end method

.method public synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3

    .prologue
    .line 46
    invoke-direct {p0, p1}, Landroid/support/v7/widget/ActionMenuView;->b(Landroid/util/AttributeSet;)Landroid/support/v7/widget/m;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .registers 3

    .prologue
    .line 46
    invoke-static {p1}, Landroid/support/v7/widget/ActionMenuView;->a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/m;

    move-result-object v0

    return-object v0
.end method

.method public getMenu()Landroid/view/Menu;
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 641
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->c:Landroid/support/v7/internal/view/menu/i;

    if-nez v0, :cond_3e

    .line 642
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 643
    new-instance v1, Landroid/support/v7/internal/view/menu/i;

    invoke-direct {v1, v0}, Landroid/support/v7/internal/view/menu/i;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Landroid/support/v7/widget/ActionMenuView;->c:Landroid/support/v7/internal/view/menu/i;

    .line 644
    iget-object v1, p0, Landroid/support/v7/widget/ActionMenuView;->c:Landroid/support/v7/internal/view/menu/i;

    new-instance v2, Landroid/support/v7/widget/n;

    invoke-direct {v2, p0, v3}, Landroid/support/v7/widget/n;-><init>(Landroid/support/v7/widget/ActionMenuView;B)V

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/support/v7/internal/view/menu/j;)V

    .line 645
    new-instance v1, Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-direct {v1, v0}, Landroid/support/v7/widget/ActionMenuPresenter;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/ActionMenuPresenter;

    .line 646
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuPresenter;->d()V

    .line 647
    iget-object v1, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/ActionMenuPresenter;

    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->f:Landroid/support/v7/internal/view/menu/y;

    if-eqz v0, :cond_41

    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->f:Landroid/support/v7/internal/view/menu/y;

    .line 3148
    :goto_2e
    iput-object v0, v1, Landroid/support/v7/internal/view/menu/d;->f:Landroid/support/v7/internal/view/menu/y;

    .line 649
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->c:Landroid/support/v7/internal/view/menu/i;

    iget-object v1, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/ActionMenuPresenter;

    iget-object v2, p0, Landroid/support/v7/widget/ActionMenuView;->o:Landroid/content/Context;

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/support/v7/internal/view/menu/x;Landroid/content/Context;)V

    .line 650
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/ActionMenuPresenter;->a(Landroid/support/v7/widget/ActionMenuView;)V

    .line 653
    :cond_3e
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->c:Landroid/support/v7/internal/view/menu/i;

    return-object v0

    .line 647
    :cond_41
    new-instance v0, Landroid/support/v7/widget/l;

    invoke-direct {v0, p0, v3}, Landroid/support/v7/widget/l;-><init>(Landroid/support/v7/widget/ActionMenuView;B)V

    goto :goto_2e
.end method

.method public getOverflowIcon()Landroid/graphics/drawable/Drawable;
    .registers 3
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 564
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getMenu()Landroid/view/Menu;

    .line 565
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/ActionMenuPresenter;

    .line 2176
    iget-object v1, v0, Landroid/support/v7/widget/ActionMenuPresenter;->i:Landroid/support/v7/widget/e;

    if-eqz v1, :cond_10

    .line 2177
    iget-object v0, v0, Landroid/support/v7/widget/ActionMenuPresenter;->i:Landroid/support/v7/widget/e;

    invoke-virtual {v0}, Landroid/support/v7/widget/e;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2179
    :goto_f
    return-object v0

    .line 2178
    :cond_10
    iget-boolean v1, v0, Landroid/support/v7/widget/ActionMenuPresenter;->k:Z

    if-eqz v1, :cond_17

    .line 2179
    iget-object v0, v0, Landroid/support/v7/widget/ActionMenuPresenter;->j:Landroid/graphics/drawable/Drawable;

    goto :goto_f

    .line 2181
    :cond_17
    const/4 v0, 0x0

    .line 565
    goto :goto_f
.end method

.method public getPopupTheme()I
    .registers 2

    .prologue
    .line 111
    iget v0, p0, Landroid/support/v7/widget/ActionMenuView;->p:I

    return v0
.end method

.method public getWindowAnimations()I
    .registers 2

    .prologue
    .line 624
    const/4 v0, 0x0

    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 4

    .prologue
    .line 125
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_9

    .line 126
    invoke-super {p0, p1}, Landroid/support/v7/widget/aj;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 129
    :cond_9
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/ActionMenuPresenter;

    if-eqz v0, :cond_25

    .line 130
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/ActionMenuPresenter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionMenuPresenter;->a(Z)V

    .line 132
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuPresenter;->i()Z

    move-result v0

    if-eqz v0, :cond_25

    .line 133
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuPresenter;->f()Z

    .line 134
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuPresenter;->e()Z

    .line 137
    :cond_25
    return-void
.end method

.method public onDetachedFromWindow()V
    .registers 1

    .prologue
    .line 543
    invoke-super {p0}, Landroid/support/v7/widget/aj;->onDetachedFromWindow()V

    .line 544
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->b()V

    .line 545
    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 19

    .prologue
    .line 438
    iget-boolean v0, p0, Landroid/support/v7/widget/ActionMenuView;->q:Z

    if-nez v0, :cond_8

    .line 439
    invoke-super/range {p0 .. p5}, Landroid/support/v7/widget/aj;->onLayout(ZIIII)V

    .line 539
    :cond_7
    :goto_7
    return-void

    .line 443
    :cond_8
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getChildCount()I

    move-result v6

    .line 444
    sub-int v0, p5, p3

    div-int/lit8 v7, v0, 0x2

    .line 445
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getDividerWidth()I

    move-result v8

    .line 448
    const/4 v4, 0x0

    .line 449
    sub-int v0, p4, p2

    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingLeft()I

    move-result v1

    sub-int v3, v0, v1

    .line 450
    const/4 v2, 0x0

    .line 451
    invoke-static {p0}, Landroid/support/v7/internal/widget/bd;->a(Landroid/view/View;)Z

    move-result v9

    .line 452
    const/4 v0, 0x0

    move v5, v0

    :goto_29
    if-ge v5, v6, :cond_8f

    .line 453
    invoke-virtual {p0, v5}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    .line 454
    invoke-virtual {v10}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_14c

    .line 458
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/m;

    .line 459
    iget-boolean v1, v0, Landroid/support/v7/widget/m;->a:Z

    if-eqz v1, :cond_7d

    .line 460
    invoke-virtual {v10}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 461
    invoke-direct {p0, v5}, Landroid/support/v7/widget/ActionMenuView;->a(I)Z

    move-result v2

    if-eqz v2, :cond_4c

    .line 462
    add-int/2addr v1, v8

    .line 464
    :cond_4c
    invoke-virtual {v10}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    .line 467
    if-eqz v9, :cond_6e

    .line 468
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingLeft()I

    move-result v2

    iget v0, v0, Landroid/support/v7/widget/m;->leftMargin:I

    add-int/2addr v0, v2

    .line 469
    add-int v2, v0, v1

    .line 474
    :goto_5b
    div-int/lit8 v12, v11, 0x2

    sub-int v12, v7, v12

    .line 475
    add-int/2addr v11, v12

    .line 476
    invoke-virtual {v10, v0, v12, v2, v11}, Landroid/view/View;->layout(IIII)V

    .line 478
    sub-int v0, v3, v1

    .line 479
    const/4 v1, 0x1

    move v2, v1

    move v1, v4

    .line 452
    :goto_68
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move v4, v1

    move v3, v0

    goto :goto_29

    .line 471
    :cond_6e
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingRight()I

    move-result v12

    sub-int/2addr v2, v12

    iget v0, v0, Landroid/support/v7/widget/m;->rightMargin:I

    sub-int/2addr v2, v0

    .line 472
    sub-int v0, v2, v1

    goto :goto_5b

    .line 481
    :cond_7d
    invoke-virtual {v10}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget v10, v0, Landroid/support/v7/widget/m;->leftMargin:I

    add-int/2addr v1, v10

    iget v0, v0, Landroid/support/v7/widget/m;->rightMargin:I

    add-int/2addr v0, v1

    .line 483
    sub-int v0, v3, v0

    .line 484
    invoke-direct {p0, v5}, Landroid/support/v7/widget/ActionMenuView;->a(I)Z

    .line 487
    add-int/lit8 v1, v4, 0x1

    goto :goto_68

    .line 491
    :cond_8f
    const/4 v0, 0x1

    if-ne v6, v0, :cond_b3

    if-nez v2, :cond_b3

    .line 493
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 494
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 495
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 496
    sub-int v3, p4, p2

    div-int/lit8 v3, v3, 0x2

    .line 497
    div-int/lit8 v4, v1, 0x2

    sub-int/2addr v3, v4

    .line 498
    div-int/lit8 v4, v2, 0x2

    sub-int v4, v7, v4

    .line 499
    add-int/2addr v1, v3

    add-int/2addr v2, v4

    invoke-virtual {v0, v3, v4, v1, v2}, Landroid/view/View;->layout(IIII)V

    goto/16 :goto_7

    .line 503
    :cond_b3
    if-eqz v2, :cond_107

    const/4 v0, 0x0

    :goto_b6
    sub-int v0, v4, v0

    .line 504
    const/4 v1, 0x0

    if-lez v0, :cond_109

    div-int v0, v3, v0

    :goto_bd
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 506
    if-eqz v9, :cond_10b

    .line 507
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingRight()I

    move-result v1

    sub-int v1, v0, v1

    .line 508
    const/4 v0, 0x0

    move v2, v0

    :goto_cf
    if-ge v2, v6, :cond_7

    .line 509
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 510
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/m;

    .line 511
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v5

    const/16 v8, 0x8

    if-eq v5, v8, :cond_14a

    iget-boolean v5, v0, Landroid/support/v7/widget/m;->a:Z

    if-nez v5, :cond_14a

    .line 515
    iget v5, v0, Landroid/support/v7/widget/m;->rightMargin:I

    sub-int/2addr v1, v5

    .line 516
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    .line 517
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    .line 518
    div-int/lit8 v9, v8, 0x2

    sub-int v9, v7, v9

    .line 519
    sub-int v10, v1, v5

    add-int/2addr v8, v9

    invoke-virtual {v4, v10, v9, v1, v8}, Landroid/view/View;->layout(IIII)V

    .line 520
    iget v0, v0, Landroid/support/v7/widget/m;->leftMargin:I

    add-int/2addr v0, v5

    add-int/2addr v0, v3

    sub-int v0, v1, v0

    .line 508
    :goto_102
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_cf

    .line 503
    :cond_107
    const/4 v0, 0x1

    goto :goto_b6

    .line 504
    :cond_109
    const/4 v0, 0x0

    goto :goto_bd

    .line 523
    :cond_10b
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingLeft()I

    move-result v1

    .line 524
    const/4 v0, 0x0

    move v2, v0

    :goto_111
    if-ge v2, v6, :cond_7

    .line 525
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 526
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/m;

    .line 527
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v5

    const/16 v8, 0x8

    if-eq v5, v8, :cond_148

    iget-boolean v5, v0, Landroid/support/v7/widget/m;->a:Z

    if-nez v5, :cond_148

    .line 531
    iget v5, v0, Landroid/support/v7/widget/m;->leftMargin:I

    add-int/2addr v1, v5

    .line 532
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    .line 533
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    .line 534
    div-int/lit8 v9, v8, 0x2

    sub-int v9, v7, v9

    .line 535
    add-int v10, v1, v5

    add-int/2addr v8, v9

    invoke-virtual {v4, v1, v9, v10, v8}, Landroid/view/View;->layout(IIII)V

    .line 536
    iget v0, v0, Landroid/support/v7/widget/m;->rightMargin:I

    add-int/2addr v0, v5

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    .line 524
    :goto_143
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_111

    :cond_148
    move v0, v1

    goto :goto_143

    :cond_14a
    move v0, v1

    goto :goto_102

    :cond_14c
    move v0, v3

    move v1, v4

    goto/16 :goto_68
.end method

.method protected onMeasure(II)V
    .registers 37

    .prologue
    .line 146
    move-object/from16 v0, p0

    iget-boolean v7, v0, Landroid/support/v7/widget/ActionMenuView;->q:Z

    .line 147
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v6

    const/high16 v8, 0x40000000    # 2.0f

    if-ne v6, v8, :cond_8b

    const/4 v6, 0x1

    :goto_d
    move-object/from16 v0, p0

    iput-boolean v6, v0, Landroid/support/v7/widget/ActionMenuView;->q:Z

    .line 149
    move-object/from16 v0, p0

    iget-boolean v6, v0, Landroid/support/v7/widget/ActionMenuView;->q:Z

    if-eq v7, v6, :cond_1c

    .line 150
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput v6, v0, Landroid/support/v7/widget/ActionMenuView;->r:I

    .line 155
    :cond_1c
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 156
    move-object/from16 v0, p0

    iget-boolean v7, v0, Landroid/support/v7/widget/ActionMenuView;->q:Z

    if-eqz v7, :cond_3e

    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/v7/widget/ActionMenuView;->c:Landroid/support/v7/internal/view/menu/i;

    if-eqz v7, :cond_3e

    move-object/from16 v0, p0

    iget v7, v0, Landroid/support/v7/widget/ActionMenuView;->r:I

    if-eq v6, v7, :cond_3e

    .line 157
    move-object/from16 v0, p0

    iput v6, v0, Landroid/support/v7/widget/ActionMenuView;->r:I

    .line 158
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/v7/widget/ActionMenuView;->c:Landroid/support/v7/internal/view/menu/i;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/support/v7/internal/view/menu/i;->c(Z)V

    .line 161
    :cond_3e
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getChildCount()I

    move-result v8

    .line 162
    move-object/from16 v0, p0

    iget-boolean v6, v0, Landroid/support/v7/widget/ActionMenuView;->q:Z

    if-eqz v6, :cond_33c

    if-lez v8, :cond_33c

    .line 1177
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v23

    .line 1178
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 1179
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v17

    .line 1181
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingLeft()I

    move-result v7

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingRight()I

    move-result v8

    add-int/2addr v7, v8

    .line 1182
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingTop()I

    move-result v8

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getPaddingBottom()I

    move-result v9

    add-int v19, v8, v9

    .line 1184
    const/4 v8, -0x2

    move/from16 v0, p2

    move/from16 v1, v19

    invoke-static {v0, v1, v8}, Landroid/support/v7/widget/ActionMenuView;->getChildMeasureSpec(III)I

    move-result v24

    .line 1187
    sub-int v25, v6, v7

    .line 1190
    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/widget/ActionMenuView;->s:I

    div-int v9, v25, v6

    .line 1191
    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/widget/ActionMenuView;->s:I

    rem-int v6, v25, v6

    .line 1193
    if-nez v9, :cond_8d

    .line 1195
    const/4 v6, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1, v6}, Landroid/support/v7/widget/ActionMenuView;->setMeasuredDimension(II)V

    .line 1196
    :goto_8a
    return-void

    .line 147
    :cond_8b
    const/4 v6, 0x0

    goto :goto_d

    .line 1199
    :cond_8d
    move-object/from16 v0, p0

    iget v7, v0, Landroid/support/v7/widget/ActionMenuView;->s:I

    div-int/2addr v6, v9

    add-int v26, v7, v6

    .line 1202
    const/16 v16, 0x0

    .line 1203
    const/4 v15, 0x0

    .line 1204
    const/4 v10, 0x0

    .line 1205
    const/4 v7, 0x0

    .line 1206
    const/4 v11, 0x0

    .line 1209
    const-wide/16 v12, 0x0

    .line 1211
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/ActionMenuView;->getChildCount()I

    move-result v27

    .line 1212
    const/4 v6, 0x0

    move/from16 v18, v6

    :goto_a3
    move/from16 v0, v18

    move/from16 v1, v27

    if-ge v0, v1, :cond_148

    .line 1213
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 1214
    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v6

    const/16 v14, 0x8

    if-eq v6, v14, :cond_379

    .line 1216
    instance-of v0, v8, Landroid/support/v7/internal/view/menu/ActionMenuItemView;

    move/from16 v20, v0

    .line 1217
    add-int/lit8 v14, v7, 0x1

    .line 1219
    if-eqz v20, :cond_d5

    .line 1222
    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/widget/ActionMenuView;->t:I

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v7/widget/ActionMenuView;->t:I

    move/from16 v21, v0

    const/16 v22, 0x0

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v8, v6, v7, v0, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 1225
    :cond_d5
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/support/v7/widget/m;

    .line 1226
    const/4 v7, 0x0

    iput-boolean v7, v6, Landroid/support/v7/widget/m;->f:Z

    .line 1227
    const/4 v7, 0x0

    iput v7, v6, Landroid/support/v7/widget/m;->c:I

    .line 1228
    const/4 v7, 0x0

    iput v7, v6, Landroid/support/v7/widget/m;->b:I

    .line 1229
    const/4 v7, 0x0

    iput-boolean v7, v6, Landroid/support/v7/widget/m;->d:Z

    .line 1230
    const/4 v7, 0x0

    iput v7, v6, Landroid/support/v7/widget/m;->leftMargin:I

    .line 1231
    const/4 v7, 0x0

    iput v7, v6, Landroid/support/v7/widget/m;->rightMargin:I

    .line 1232
    if-eqz v20, :cond_144

    move-object v7, v8

    check-cast v7, Landroid/support/v7/internal/view/menu/ActionMenuItemView;

    invoke-virtual {v7}, Landroid/support/v7/internal/view/menu/ActionMenuItemView;->c()Z

    move-result v7

    if-eqz v7, :cond_144

    const/4 v7, 0x1

    :goto_f9
    iput-boolean v7, v6, Landroid/support/v7/widget/m;->e:Z

    .line 1235
    iget-boolean v7, v6, Landroid/support/v7/widget/m;->a:Z

    if-eqz v7, :cond_146

    const/4 v7, 0x1

    .line 1237
    :goto_100
    move/from16 v0, v26

    move/from16 v1, v24

    move/from16 v2, v19

    invoke-static {v8, v0, v7, v1, v2}, Landroid/support/v7/widget/ActionMenuView;->a(Landroid/view/View;IIII)I

    move-result v20

    .line 1240
    move/from16 v0, v20

    invoke-static {v15, v0}, Ljava/lang/Math;->max(II)I

    move-result v15

    .line 1241
    iget-boolean v7, v6, Landroid/support/v7/widget/m;->d:Z

    if-eqz v7, :cond_376

    add-int/lit8 v7, v10, 0x1

    .line 1242
    :goto_116
    iget-boolean v6, v6, Landroid/support/v7/widget/m;->a:Z

    if-eqz v6, :cond_373

    const/4 v6, 0x1

    .line 1244
    :goto_11b
    sub-int v11, v9, v20

    .line 1245
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    move/from16 v0, v16

    invoke-static {v0, v8}, Ljava/lang/Math;->max(II)I

    move-result v10

    .line 1246
    const/4 v8, 0x1

    move/from16 v0, v20

    if-ne v0, v8, :cond_367

    const/4 v8, 0x1

    shl-int v8, v8, v18

    int-to-long v8, v8

    or-long/2addr v8, v12

    move v12, v10

    move v13, v11

    move v10, v7

    move v11, v6

    move-wide v6, v8

    move v9, v15

    move v8, v14

    .line 1212
    :goto_138
    add-int/lit8 v14, v18, 0x1

    move/from16 v18, v14

    move v15, v9

    move/from16 v16, v12

    move v9, v13

    move-wide v12, v6

    move v7, v8

    goto/16 :goto_a3

    .line 1232
    :cond_144
    const/4 v7, 0x0

    goto :goto_f9

    :cond_146
    move v7, v9

    .line 1235
    goto :goto_100

    .line 1251
    :cond_148
    if-eqz v11, :cond_192

    const/4 v6, 0x2

    if-ne v7, v6, :cond_192

    const/4 v6, 0x1

    move v8, v6

    .line 1256
    :goto_14f
    const/16 v18, 0x0

    move-wide/from16 v20, v12

    move/from16 v19, v9

    .line 1257
    :goto_155
    if-lez v10, :cond_230

    if-lez v19, :cond_230

    .line 1258
    const v14, 0x7fffffff

    .line 1259
    const-wide/16 v12, 0x0

    .line 1260
    const/4 v9, 0x0

    .line 1261
    const/4 v6, 0x0

    move/from16 v22, v6

    :goto_162
    move/from16 v0, v22

    move/from16 v1, v27

    if-ge v0, v1, :cond_1a5

    .line 1262
    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 1263
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/support/v7/widget/m;

    .line 1266
    iget-boolean v0, v6, Landroid/support/v7/widget/m;->d:Z

    move/from16 v28, v0

    if-eqz v28, :cond_363

    .line 1269
    iget v0, v6, Landroid/support/v7/widget/m;->b:I

    move/from16 v28, v0

    move/from16 v0, v28

    if-ge v0, v14, :cond_195

    .line 1270
    iget v9, v6, Landroid/support/v7/widget/m;->b:I

    .line 1271
    const/4 v6, 0x1

    shl-int v6, v6, v22

    int-to-long v12, v6

    .line 1272
    const/4 v6, 0x1

    .line 1261
    :goto_18b
    add-int/lit8 v14, v22, 0x1

    move/from16 v22, v14

    move v14, v9

    move v9, v6

    goto :goto_162

    .line 1251
    :cond_192
    const/4 v6, 0x0

    move v8, v6

    goto :goto_14f

    .line 1273
    :cond_195
    iget v6, v6, Landroid/support/v7/widget/m;->b:I

    if-ne v6, v14, :cond_363

    .line 1274
    const/4 v6, 0x1

    shl-int v6, v6, v22

    int-to-long v0, v6

    move-wide/from16 v28, v0

    or-long v12, v12, v28

    .line 1275
    add-int/lit8 v6, v9, 0x1

    move v9, v14

    goto :goto_18b

    .line 1280
    :cond_1a5
    or-long v20, v20, v12

    .line 1282
    move/from16 v0, v19

    if-gt v9, v0, :cond_230

    .line 1285
    add-int/lit8 v22, v14, 0x1

    .line 1287
    const/4 v6, 0x0

    move v14, v6

    move/from16 v9, v19

    move-wide/from16 v18, v20

    :goto_1b3
    move/from16 v0, v27

    if-ge v14, v0, :cond_227

    .line 1288
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v20

    .line 1289
    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/support/v7/widget/m;

    .line 1290
    const/16 v21, 0x1

    shl-int v21, v21, v14

    move/from16 v0, v21

    int-to-long v0, v0

    move-wide/from16 v28, v0

    and-long v28, v28, v12

    const-wide/16 v30, 0x0

    cmp-long v21, v28, v30

    if-nez v21, :cond_1e7

    .line 1292
    iget v6, v6, Landroid/support/v7/widget/m;->b:I

    move/from16 v0, v22

    if-ne v6, v0, :cond_360

    const/4 v6, 0x1

    shl-int/2addr v6, v14

    int-to-long v0, v6

    move-wide/from16 v20, v0

    or-long v18, v18, v20

    move v6, v9

    .line 1287
    :goto_1e2
    add-int/lit8 v9, v14, 0x1

    move v14, v9

    move v9, v6

    goto :goto_1b3

    .line 1296
    :cond_1e7
    if-eqz v8, :cond_214

    iget-boolean v0, v6, Landroid/support/v7/widget/m;->e:Z

    move/from16 v21, v0

    if-eqz v21, :cond_214

    const/16 v21, 0x1

    move/from16 v0, v21

    if-ne v9, v0, :cond_214

    .line 1298
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v7/widget/ActionMenuView;->t:I

    move/from16 v21, v0

    add-int v21, v21, v26

    const/16 v28, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v7/widget/ActionMenuView;->t:I

    move/from16 v29, v0

    const/16 v30, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v28

    move/from16 v3, v29

    move/from16 v4, v30

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 1300
    :cond_214
    iget v0, v6, Landroid/support/v7/widget/m;->b:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, 0x1

    move/from16 v0, v20

    iput v0, v6, Landroid/support/v7/widget/m;->b:I

    .line 1301
    const/16 v20, 0x1

    move/from16 v0, v20

    iput-boolean v0, v6, Landroid/support/v7/widget/m;->f:Z

    .line 1302
    add-int/lit8 v6, v9, -0x1

    goto :goto_1e2

    .line 1305
    :cond_227
    const/4 v6, 0x1

    move-wide/from16 v20, v18

    move/from16 v18, v6

    move/from16 v19, v9

    .line 1306
    goto/16 :goto_155

    :cond_230
    move-wide/from16 v12, v20

    .line 1311
    if-nez v11, :cond_2d5

    const/4 v6, 0x1

    if-ne v7, v6, :cond_2d5

    const/4 v6, 0x1

    .line 1312
    :goto_238
    if-lez v19, :cond_2fb

    const-wide/16 v8, 0x0

    cmp-long v8, v12, v8

    if-eqz v8, :cond_2fb

    add-int/lit8 v7, v7, -0x1

    move/from16 v0, v19

    if-lt v0, v7, :cond_24b

    if-nez v6, :cond_24b

    const/4 v7, 0x1

    if-le v15, v7, :cond_2fb

    .line 1314
    :cond_24b
    invoke-static {v12, v13}, Ljava/lang/Long;->bitCount(J)I

    move-result v7

    int-to-float v7, v7

    .line 1316
    if-nez v6, :cond_35d

    .line 1318
    const-wide/16 v8, 0x1

    and-long/2addr v8, v12

    const-wide/16 v10, 0x0

    cmp-long v6, v8, v10

    if-eqz v6, :cond_26f

    .line 1319
    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/support/v7/widget/m;

    .line 1320
    iget-boolean v6, v6, Landroid/support/v7/widget/m;->e:Z

    if-nez v6, :cond_26f

    const/high16 v6, 0x3f000000    # 0.5f

    sub-float/2addr v7, v6

    .line 1322
    :cond_26f
    const/4 v6, 0x1

    add-int/lit8 v8, v27, -0x1

    shl-int/2addr v6, v8

    int-to-long v8, v6

    and-long/2addr v8, v12

    const-wide/16 v10, 0x0

    cmp-long v6, v8, v10

    if-eqz v6, :cond_35d

    .line 1323
    add-int/lit8 v6, v27, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/support/v7/widget/m;

    .line 1324
    iget-boolean v6, v6, Landroid/support/v7/widget/m;->e:Z

    if-nez v6, :cond_35d

    const/high16 v6, 0x3f000000    # 0.5f

    sub-float v6, v7, v6

    .line 1328
    :goto_291
    const/4 v7, 0x0

    cmpl-float v7, v6, v7

    if-lez v7, :cond_2d8

    mul-int v7, v19, v26

    int-to-float v7, v7

    div-float v6, v7, v6

    float-to-int v6, v6

    move v7, v6

    .line 1331
    :goto_29d
    const/4 v6, 0x0

    move v9, v6

    move/from16 v8, v18

    :goto_2a1
    move/from16 v0, v27

    if-ge v9, v0, :cond_2fd

    .line 1332
    const/4 v6, 0x1

    shl-int/2addr v6, v9

    int-to-long v10, v6

    and-long/2addr v10, v12

    const-wide/16 v14, 0x0

    cmp-long v6, v10, v14

    if-eqz v6, :cond_2f9

    .line 1334
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    .line 1335
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/support/v7/widget/m;

    .line 1336
    instance-of v10, v10, Landroid/support/v7/internal/view/menu/ActionMenuItemView;

    if-eqz v10, :cond_2db

    .line 1338
    iput v7, v6, Landroid/support/v7/widget/m;->c:I

    .line 1339
    const/4 v8, 0x1

    iput-boolean v8, v6, Landroid/support/v7/widget/m;->f:Z

    .line 1340
    if-nez v9, :cond_2cf

    iget-boolean v8, v6, Landroid/support/v7/widget/m;->e:Z

    if-nez v8, :cond_2cf

    .line 1343
    neg-int v8, v7

    div-int/lit8 v8, v8, 0x2

    iput v8, v6, Landroid/support/v7/widget/m;->leftMargin:I

    .line 1345
    :cond_2cf
    const/4 v6, 0x1

    .line 1331
    :goto_2d0
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    move v8, v6

    goto :goto_2a1

    .line 1311
    :cond_2d5
    const/4 v6, 0x0

    goto/16 :goto_238

    .line 1328
    :cond_2d8
    const/4 v6, 0x0

    move v7, v6

    goto :goto_29d

    .line 1346
    :cond_2db
    iget-boolean v10, v6, Landroid/support/v7/widget/m;->a:Z

    if-eqz v10, :cond_2eb

    .line 1347
    iput v7, v6, Landroid/support/v7/widget/m;->c:I

    .line 1348
    const/4 v8, 0x1

    iput-boolean v8, v6, Landroid/support/v7/widget/m;->f:Z

    .line 1349
    neg-int v8, v7

    div-int/lit8 v8, v8, 0x2

    iput v8, v6, Landroid/support/v7/widget/m;->rightMargin:I

    .line 1350
    const/4 v6, 0x1

    goto :goto_2d0

    .line 1355
    :cond_2eb
    if-eqz v9, :cond_2f1

    .line 1356
    div-int/lit8 v10, v7, 0x2

    iput v10, v6, Landroid/support/v7/widget/m;->leftMargin:I

    .line 1358
    :cond_2f1
    add-int/lit8 v10, v27, -0x1

    if-eq v9, v10, :cond_2f9

    .line 1359
    div-int/lit8 v10, v7, 0x2

    iput v10, v6, Landroid/support/v7/widget/m;->rightMargin:I

    :cond_2f9
    move v6, v8

    goto :goto_2d0

    :cond_2fb
    move/from16 v8, v18

    .line 1368
    :cond_2fd
    if-eqz v8, :cond_32b

    .line 1369
    const/4 v6, 0x0

    move v7, v6

    :goto_301
    move/from16 v0, v27

    if-ge v7, v0, :cond_32b

    .line 1370
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 1371
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/support/v7/widget/m;

    .line 1373
    iget-boolean v9, v6, Landroid/support/v7/widget/m;->f:Z

    if-eqz v9, :cond_327

    .line 1375
    iget v9, v6, Landroid/support/v7/widget/m;->b:I

    mul-int v9, v9, v26

    iget v6, v6, Landroid/support/v7/widget/m;->c:I

    add-int/2addr v6, v9

    .line 1376
    const/high16 v9, 0x40000000    # 2.0f

    invoke-static {v6, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    move/from16 v0, v24

    invoke-virtual {v8, v6, v0}, Landroid/view/View;->measure(II)V

    .line 1369
    :cond_327
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_301

    .line 1381
    :cond_32b
    const/high16 v6, 0x40000000    # 2.0f

    move/from16 v0, v23

    if-eq v0, v6, :cond_35a

    .line 1385
    :goto_331
    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/ActionMenuView;->setMeasuredDimension(II)V

    goto/16 :goto_8a

    .line 166
    :cond_33c
    const/4 v6, 0x0

    move v7, v6

    :goto_33e
    if-ge v7, v8, :cond_355

    .line 167
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Landroid/support/v7/widget/ActionMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 168
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/support/v7/widget/m;

    .line 169
    const/4 v9, 0x0

    iput v9, v6, Landroid/support/v7/widget/m;->rightMargin:I

    iput v9, v6, Landroid/support/v7/widget/m;->leftMargin:I

    .line 166
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_33e

    .line 171
    :cond_355
    invoke-super/range {p0 .. p2}, Landroid/support/v7/widget/aj;->onMeasure(II)V

    goto/16 :goto_8a

    :cond_35a
    move/from16 v16, v17

    goto :goto_331

    :cond_35d
    move v6, v7

    goto/16 :goto_291

    :cond_360
    move v6, v9

    goto/16 :goto_1e2

    :cond_363
    move v6, v9

    move v9, v14

    goto/16 :goto_18b

    :cond_367
    move v8, v14

    move v9, v15

    move-wide/from16 v32, v12

    move v12, v10

    move v13, v11

    move v11, v6

    move v10, v7

    move-wide/from16 v6, v32

    goto/16 :goto_138

    :cond_373
    move v6, v11

    goto/16 :goto_11b

    :cond_376
    move v7, v10

    goto/16 :goto_116

    :cond_379
    move v8, v7

    move-wide v6, v12

    move/from16 v12, v16

    move v13, v9

    move v9, v15

    goto/16 :goto_138
.end method

.method public setExpandedActionViewsExclusive(Z)V
    .registers 3

    .prologue
    .line 740
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/ActionMenuPresenter;

    .line 3163
    iput-boolean p1, v0, Landroid/support/v7/widget/ActionMenuPresenter;->o:Z

    .line 741
    return-void
.end method

.method public setOnMenuItemClickListener(Landroid/support/v7/widget/o;)V
    .registers 2

    .prologue
    .line 140
    iput-object p1, p0, Landroid/support/v7/widget/ActionMenuView;->u:Landroid/support/v7/widget/o;

    .line 141
    return-void
.end method

.method public setOverflowIcon(Landroid/graphics/drawable/Drawable;)V
    .registers 4
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 553
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getMenu()Landroid/view/Menu;

    .line 554
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/ActionMenuPresenter;

    .line 2167
    iget-object v1, v0, Landroid/support/v7/widget/ActionMenuPresenter;->i:Landroid/support/v7/widget/e;

    if-eqz v1, :cond_f

    .line 2168
    iget-object v0, v0, Landroid/support/v7/widget/ActionMenuPresenter;->i:Landroid/support/v7/widget/e;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/e;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_e
    return-void

    .line 2170
    :cond_f
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/ActionMenuPresenter;->k:Z

    .line 2171
    iput-object p1, v0, Landroid/support/v7/widget/ActionMenuPresenter;->j:Landroid/graphics/drawable/Drawable;

    goto :goto_e
.end method

.method public setOverflowReserved(Z)V
    .registers 2

    .prologue
    .line 575
    iput-boolean p1, p0, Landroid/support/v7/widget/ActionMenuView;->d:Z

    .line 576
    return-void
.end method

.method public setPopupTheme(I)V
    .registers 4
    .param p1    # I
        .annotation build Landroid/support/a/ai;
        .end annotation
    .end param

    .prologue
    .line 95
    iget v0, p0, Landroid/support/v7/widget/ActionMenuView;->p:I

    if-eq v0, p1, :cond_e

    .line 96
    iput p1, p0, Landroid/support/v7/widget/ActionMenuView;->p:I

    .line 97
    if-nez p1, :cond_f

    .line 98
    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->o:Landroid/content/Context;

    .line 103
    :cond_e
    :goto_e
    return-void

    .line 100
    :cond_f
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Landroid/support/v7/widget/ActionMenuView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->o:Landroid/content/Context;

    goto :goto_e
.end method

.method public setPresenter(Landroid/support/v7/widget/ActionMenuPresenter;)V
    .registers 3

    .prologue
    .line 119
    iput-object p1, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/ActionMenuPresenter;

    .line 120
    iget-object v0, p0, Landroid/support/v7/widget/ActionMenuView;->e:Landroid/support/v7/widget/ActionMenuPresenter;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/ActionMenuPresenter;->a(Landroid/support/v7/widget/ActionMenuView;)V

    .line 121
    return-void
.end method
