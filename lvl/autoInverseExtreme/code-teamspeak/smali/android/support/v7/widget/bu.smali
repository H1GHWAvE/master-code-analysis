.class public final Landroid/support/v7/widget/bu;
.super Landroid/support/v4/view/n;
.source "SourceFile"


# static fields
.field public static final c:Ljava/lang/String; = "share_history.xml"

.field private static final f:I = 0x4


# instance fields
.field final d:Landroid/content/Context;

.field e:Ljava/lang/String;

.field private g:I

.field private final h:Landroid/support/v7/widget/by;

.field private i:Landroid/support/v7/widget/bw;

.field private j:Landroid/support/v7/internal/widget/s;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 4

    .prologue
    .line 157
    invoke-direct {p0, p1}, Landroid/support/v4/view/n;-><init>(Landroid/content/Context;)V

    .line 124
    const/4 v0, 0x4

    iput v0, p0, Landroid/support/v7/widget/bu;->g:I

    .line 129
    new-instance v0, Landroid/support/v7/widget/by;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Landroid/support/v7/widget/by;-><init>(Landroid/support/v7/widget/bu;B)V

    iput-object v0, p0, Landroid/support/v7/widget/bu;->h:Landroid/support/v7/widget/by;

    .line 145
    const-string v0, "share_history.xml"

    iput-object v0, p0, Landroid/support/v7/widget/bu;->e:Ljava/lang/String;

    .line 158
    iput-object p1, p0, Landroid/support/v7/widget/bu;->d:Landroid/content/Context;

    .line 159
    return-void
.end method

.method private static synthetic a(Landroid/support/v7/widget/bu;)Landroid/content/Context;
    .registers 2

    .prologue
    .line 87
    iget-object v0, p0, Landroid/support/v7/widget/bu;->d:Landroid/content/Context;

    return-object v0
.end method

.method static a(Landroid/content/Intent;)V
    .registers 3

    .prologue
    .line 365
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_c

    .line 367
    const/high16 v0, 0x8080000

    invoke-virtual {p0, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 373
    :goto_b
    return-void

    .line 371
    :cond_c
    const/high16 v0, 0x80000

    invoke-virtual {p0, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_b
.end method

.method private a(Landroid/support/v7/widget/bw;)V
    .registers 2

    .prologue
    .line 172
    iput-object p1, p0, Landroid/support/v7/widget/bu;->i:Landroid/support/v7/widget/bw;

    .line 173
    invoke-direct {p0}, Landroid/support/v7/widget/bu;->g()V

    .line 174
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 281
    iput-object p1, p0, Landroid/support/v7/widget/bu;->e:Ljava/lang/String;

    .line 282
    invoke-direct {p0}, Landroid/support/v7/widget/bu;->g()V

    .line 283
    return-void
.end method

.method private static synthetic b(Landroid/support/v7/widget/bu;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 87
    iget-object v0, p0, Landroid/support/v7/widget/bu;->e:Ljava/lang/String;

    return-object v0
.end method

.method private b(Landroid/content/Intent;)V
    .registers 5

    .prologue
    .line 302
    if-eqz p1, :cond_19

    .line 303
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 304
    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_16

    const-string v1, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 305
    :cond_16
    invoke-static {p1}, Landroid/support/v7/widget/bu;->a(Landroid/content/Intent;)V

    .line 308
    :cond_19
    iget-object v0, p0, Landroid/support/v7/widget/bu;->d:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v7/widget/bu;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/support/v7/internal/widget/l;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v7/internal/widget/l;

    move-result-object v0

    .line 1369
    iget-object v1, v0, Landroid/support/v7/internal/widget/l;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 1370
    :try_start_24
    iget-object v2, v0, Landroid/support/v7/internal/widget/l;->e:Landroid/content/Intent;

    if-ne v2, p1, :cond_2a

    .line 1371
    monitor-exit v1

    .line 1376
    :goto_29
    return-void

    .line 1373
    :cond_2a
    iput-object p1, v0, Landroid/support/v7/internal/widget/l;->e:Landroid/content/Intent;

    .line 1374
    const/4 v2, 0x1

    iput-boolean v2, v0, Landroid/support/v7/internal/widget/l;->f:Z

    .line 1375
    invoke-virtual {v0}, Landroid/support/v7/internal/widget/l;->d()V

    .line 1376
    monitor-exit v1

    goto :goto_29

    :catchall_34
    move-exception v0

    monitor-exit v1
    :try_end_36
    .catchall {:try_start_24 .. :try_end_36} :catchall_34

    throw v0
.end method

.method private static synthetic c(Landroid/support/v7/widget/bu;)Landroid/support/v7/widget/bw;
    .registers 2

    .prologue
    .line 87
    iget-object v0, p0, Landroid/support/v7/widget/bu;->i:Landroid/support/v7/widget/bw;

    return-object v0
.end method

.method private static synthetic c(Landroid/content/Intent;)V
    .registers 1

    .prologue
    .line 87
    invoke-static {p0}, Landroid/support/v7/widget/bu;->a(Landroid/content/Intent;)V

    return-void
.end method

.method private g()V
    .registers 4

    .prologue
    .line 340
    iget-object v0, p0, Landroid/support/v7/widget/bu;->i:Landroid/support/v7/widget/bw;

    if-nez v0, :cond_5

    .line 1502
    :goto_4
    return-void

    .line 343
    :cond_5
    iget-object v0, p0, Landroid/support/v7/widget/bu;->j:Landroid/support/v7/internal/widget/s;

    if-nez v0, :cond_11

    .line 344
    new-instance v0, Landroid/support/v7/widget/bx;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Landroid/support/v7/widget/bx;-><init>(Landroid/support/v7/widget/bu;B)V

    iput-object v0, p0, Landroid/support/v7/widget/bu;->j:Landroid/support/v7/internal/widget/s;

    .line 346
    :cond_11
    iget-object v0, p0, Landroid/support/v7/widget/bu;->d:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v7/widget/bu;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/support/v7/internal/widget/l;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v7/internal/widget/l;

    move-result-object v0

    .line 347
    iget-object v1, p0, Landroid/support/v7/widget/bu;->j:Landroid/support/v7/internal/widget/s;

    .line 1500
    iget-object v2, v0, Landroid/support/v7/internal/widget/l;->c:Ljava/lang/Object;

    monitor-enter v2

    .line 1501
    :try_start_1e
    iput-object v1, v0, Landroid/support/v7/internal/widget/l;->g:Landroid/support/v7/internal/widget/s;

    .line 1502
    monitor-exit v2

    goto :goto_4

    :catchall_22
    move-exception v0

    monitor-exit v2
    :try_end_24
    .catchall {:try_start_1e .. :try_end_24} :catchall_22

    throw v0
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .registers 6

    .prologue
    .line 182
    new-instance v0, Landroid/support/v7/internal/widget/ActivityChooserView;

    iget-object v1, p0, Landroid/support/v7/widget/bu;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/support/v7/internal/widget/ActivityChooserView;-><init>(Landroid/content/Context;)V

    .line 183
    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActivityChooserView;->isInEditMode()Z

    move-result v1

    if-nez v1, :cond_18

    .line 184
    iget-object v1, p0, Landroid/support/v7/widget/bu;->d:Landroid/content/Context;

    iget-object v2, p0, Landroid/support/v7/widget/bu;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/support/v7/internal/widget/l;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v7/internal/widget/l;

    move-result-object v1

    .line 185
    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActivityChooserView;->setActivityChooserModel(Landroid/support/v7/internal/widget/l;)V

    .line 189
    :cond_18
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 190
    iget-object v2, p0, Landroid/support/v7/widget/bu;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    sget v3, Landroid/support/v7/a/d;->actionModeShareDrawable:I

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 191
    iget-object v2, p0, Landroid/support/v7/widget/bu;->d:Landroid/content/Context;

    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-static {v2, v1}, Landroid/support/v7/internal/widget/av;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 192
    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActivityChooserView;->setExpandActivityOverflowButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 193
    invoke-virtual {v0, p0}, Landroid/support/v7/internal/widget/ActivityChooserView;->setProvider(Landroid/support/v4/view/n;)V

    .line 196
    sget v1, Landroid/support/v7/a/l;->abc_shareactionprovider_share_with_application:I

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActivityChooserView;->setDefaultActionButtonContentDescription(I)V

    .line 198
    sget v1, Landroid/support/v7/a/l;->abc_shareactionprovider_share_with:I

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActivityChooserView;->setExpandActivityOverflowButtonContentDescription(I)V

    .line 201
    return-object v0
.end method

.method public final a(Landroid/view/SubMenu;)V
    .registers 10

    .prologue
    const/4 v1, 0x0

    .line 218
    invoke-interface {p1}, Landroid/view/SubMenu;->clear()V

    .line 220
    iget-object v0, p0, Landroid/support/v7/widget/bu;->d:Landroid/content/Context;

    iget-object v2, p0, Landroid/support/v7/widget/bu;->e:Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/support/v7/internal/widget/l;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v7/internal/widget/l;

    move-result-object v2

    .line 221
    iget-object v0, p0, Landroid/support/v7/widget/bu;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 223
    invoke-virtual {v2}, Landroid/support/v7/internal/widget/l;->a()I

    move-result v4

    .line 224
    iget v0, p0, Landroid/support/v7/widget/bu;->g:I

    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v5

    move v0, v1

    .line 227
    :goto_1d
    if-ge v0, v5, :cond_3b

    .line 228
    invoke-virtual {v2, v0}, Landroid/support/v7/internal/widget/l;->a(I)Landroid/content/pm/ResolveInfo;

    move-result-object v6

    .line 229
    invoke-virtual {v6, v3}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {p1, v1, v0, v0, v7}, Landroid/view/SubMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v7

    invoke-virtual {v6, v3}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-interface {v7, v6}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-result-object v6

    iget-object v7, p0, Landroid/support/v7/widget/bu;->h:Landroid/support/v7/widget/by;

    invoke-interface {v6, v7}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 227
    add-int/lit8 v0, v0, 0x1

    goto :goto_1d

    .line 234
    :cond_3b
    if-ge v5, v4, :cond_68

    .line 236
    iget-object v0, p0, Landroid/support/v7/widget/bu;->d:Landroid/content/Context;

    sget v6, Landroid/support/v7/a/l;->abc_activity_chooser_view_see_all:I

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v5, v5, v0}, Landroid/view/SubMenu;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v5

    move v0, v1

    .line 239
    :goto_4a
    if-ge v0, v4, :cond_68

    .line 240
    invoke-virtual {v2, v0}, Landroid/support/v7/internal/widget/l;->a(I)Landroid/content/pm/ResolveInfo;

    move-result-object v6

    .line 241
    invoke-virtual {v6, v3}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v5, v1, v0, v0, v7}, Landroid/view/SubMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v7

    invoke-virtual {v6, v3}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-interface {v7, v6}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-result-object v6

    iget-object v7, p0, Landroid/support/v7/widget/bu;->h:Landroid/support/v7/widget/by;

    invoke-interface {v6, v7}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 239
    add-int/lit8 v0, v0, 0x1

    goto :goto_4a

    .line 246
    :cond_68
    return-void
.end method

.method public final f()Z
    .registers 2

    .prologue
    .line 209
    const/4 v0, 0x1

    return v0
.end method
