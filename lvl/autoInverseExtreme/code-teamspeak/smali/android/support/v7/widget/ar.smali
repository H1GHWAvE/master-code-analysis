.class final Landroid/support/v7/widget/ar;
.super Landroid/support/v7/internal/widget/ah;
.source "SourceFile"


# instance fields
.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Landroid/support/v4/view/fk;

.field private l:Landroid/support/v4/widget/ba;


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .registers 4

    .prologue
    .line 1550
    sget v0, Landroid/support/v7/a/d;->dropDownListViewStyle:I

    invoke-direct {p0, p1, v0}, Landroid/support/v7/internal/widget/ah;-><init>(Landroid/content/Context;I)V

    .line 1551
    iput-boolean p2, p0, Landroid/support/v7/widget/ar;->i:Z

    .line 1552
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ar;->setCacheColorHint(I)V

    .line 1553
    return-void
.end method

.method private a(Landroid/view/View;I)V
    .registers 5

    .prologue
    .line 1622
    invoke-virtual {p0, p2}, Landroid/support/v7/widget/ar;->getItemIdAtPosition(I)J

    move-result-wide v0

    .line 1623
    invoke-virtual {p0, p1, p2, v0, v1}, Landroid/support/v7/widget/ar;->performItemClick(Landroid/view/View;IJ)Z

    .line 1624
    return-void
.end method

.method private a(Landroid/view/View;IFF)V
    .registers 6

    .prologue
    const/4 v0, 0x1

    .line 1639
    iput-boolean v0, p0, Landroid/support/v7/widget/ar;->j:Z

    .line 1643
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ar;->setPressed(Z)V

    .line 1644
    invoke-virtual {p0}, Landroid/support/v7/widget/ar;->layoutChildren()V

    .line 1647
    invoke-virtual {p0, p2}, Landroid/support/v7/widget/ar;->setSelection(I)V

    .line 1648
    invoke-virtual {p0, p2, p1, p3, p4}, Landroid/support/v7/widget/ar;->a(ILandroid/view/View;FF)V

    .line 1653
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ar;->setSelectorEnabled(Z)V

    .line 1657
    invoke-virtual {p0}, Landroid/support/v7/widget/ar;->refreshDrawableState()V

    .line 1658
    return-void
.end method

.method static synthetic a(Landroid/support/v7/widget/ar;Z)Z
    .registers 2

    .prologue
    .line 1498
    iput-boolean p1, p0, Landroid/support/v7/widget/ar;->h:Z

    return p1
.end method

.method private b()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 1627
    iput-boolean v0, p0, Landroid/support/v7/widget/ar;->j:Z

    .line 1628
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ar;->setPressed(Z)V

    .line 1630
    invoke-virtual {p0}, Landroid/support/v7/widget/ar;->drawableStateChanged()V

    .line 1632
    iget-object v0, p0, Landroid/support/v7/widget/ar;->k:Landroid/support/v4/view/fk;

    if-eqz v0, :cond_15

    .line 1633
    iget-object v0, p0, Landroid/support/v7/widget/ar;->k:Landroid/support/v4/view/fk;

    invoke-virtual {v0}, Landroid/support/v4/view/fk;->a()V

    .line 1634
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/ar;->k:Landroid/support/v4/view/fk;

    .line 1636
    :cond_15
    return-void
.end method


# virtual methods
.method protected final a()Z
    .registers 2

    .prologue
    .line 1662
    iget-boolean v0, p0, Landroid/support/v7/widget/ar;->j:Z

    if-nez v0, :cond_a

    invoke-super {p0}, Landroid/support/v7/internal/widget/ah;->a()Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_a
    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public final a(Landroid/view/MotionEvent;I)Z
    .registers 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1562
    .line 1565
    invoke-static {p1}, Landroid/support/v4/view/bk;->a(Landroid/view/MotionEvent;)I

    move-result v3

    .line 1566
    packed-switch v3, :pswitch_data_92

    :cond_9
    :goto_9
    move v0, v1

    move v3, v2

    .line 1599
    :goto_b
    if-eqz v3, :cond_f

    if-eqz v0, :cond_23

    .line 3627
    :cond_f
    iput-boolean v1, p0, Landroid/support/v7/widget/ar;->j:Z

    .line 3628
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/ar;->setPressed(Z)V

    .line 3630
    invoke-virtual {p0}, Landroid/support/v7/widget/ar;->drawableStateChanged()V

    .line 3632
    iget-object v0, p0, Landroid/support/v7/widget/ar;->k:Landroid/support/v4/view/fk;

    if-eqz v0, :cond_23

    .line 3633
    iget-object v0, p0, Landroid/support/v7/widget/ar;->k:Landroid/support/v4/view/fk;

    invoke-virtual {v0}, Landroid/support/v4/view/fk;->a()V

    .line 3634
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/ar;->k:Landroid/support/v4/view/fk;

    .line 1604
    :cond_23
    if-eqz v3, :cond_86

    .line 1605
    iget-object v0, p0, Landroid/support/v7/widget/ar;->l:Landroid/support/v4/widget/ba;

    if-nez v0, :cond_30

    .line 1606
    new-instance v0, Landroid/support/v4/widget/ba;

    invoke-direct {v0, p0}, Landroid/support/v4/widget/ba;-><init>(Landroid/widget/ListView;)V

    iput-object v0, p0, Landroid/support/v7/widget/ar;->l:Landroid/support/v4/widget/ba;

    .line 1608
    :cond_30
    iget-object v0, p0, Landroid/support/v7/widget/ar;->l:Landroid/support/v4/widget/ba;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/ba;->a(Z)Landroid/support/v4/widget/a;

    .line 1609
    iget-object v0, p0, Landroid/support/v7/widget/ar;->l:Landroid/support/v4/widget/ba;

    invoke-virtual {v0, p0, p1}, Landroid/support/v4/widget/ba;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 1614
    :cond_3a
    :goto_3a
    return v3

    :pswitch_3b
    move v0, v1

    move v3, v1

    .line 1569
    goto :goto_b

    :pswitch_3e
    move v0, v1

    .line 1574
    :goto_3f
    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v4

    .line 1575
    if-gez v4, :cond_48

    move v0, v1

    move v3, v1

    .line 1577
    goto :goto_b

    .line 1580
    :cond_48
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v5

    float-to-int v5, v5

    .line 1581
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    float-to-int v4, v4

    .line 1582
    invoke-virtual {p0, v5, v4}, Landroid/support/v7/widget/ar;->pointToPosition(II)I

    move-result v6

    .line 1583
    const/4 v7, -0x1

    if-ne v6, v7, :cond_5c

    move v3, v0

    move v0, v2

    .line 1585
    goto :goto_b

    .line 1588
    :cond_5c
    invoke-virtual {p0}, Landroid/support/v7/widget/ar;->getFirstVisiblePosition()I

    move-result v0

    sub-int v0, v6, v0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ar;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1589
    int-to-float v5, v5

    int-to-float v4, v4

    .line 2639
    iput-boolean v2, p0, Landroid/support/v7/widget/ar;->j:Z

    .line 2643
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/ar;->setPressed(Z)V

    .line 2644
    invoke-virtual {p0}, Landroid/support/v7/widget/ar;->layoutChildren()V

    .line 2647
    invoke-virtual {p0, v6}, Landroid/support/v7/widget/ar;->setSelection(I)V

    .line 2648
    invoke-virtual {p0, v6, v0, v5, v4}, Landroid/support/v7/widget/ar;->a(ILandroid/view/View;FF)V

    .line 2653
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/ar;->setSelectorEnabled(Z)V

    .line 2657
    invoke-virtual {p0}, Landroid/support/v7/widget/ar;->refreshDrawableState()V

    .line 1592
    if-ne v3, v2, :cond_9

    .line 3622
    invoke-virtual {p0, v6}, Landroid/support/v7/widget/ar;->getItemIdAtPosition(I)J

    move-result-wide v4

    .line 3623
    invoke-virtual {p0, v0, v6, v4, v5}, Landroid/support/v7/widget/ar;->performItemClick(Landroid/view/View;IJ)Z

    goto :goto_9

    .line 1610
    :cond_86
    iget-object v0, p0, Landroid/support/v7/widget/ar;->l:Landroid/support/v4/widget/ba;

    if-eqz v0, :cond_3a

    .line 1611
    iget-object v0, p0, Landroid/support/v7/widget/ar;->l:Landroid/support/v4/widget/ba;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/ba;->a(Z)Landroid/support/v4/widget/a;

    goto :goto_3a

    :pswitch_90
    move v0, v2

    goto :goto_3f

    .line 1566
    :pswitch_data_92
    .packed-switch 0x1
        :pswitch_3e
        :pswitch_90
        :pswitch_3b
    .end packed-switch
.end method

.method public final hasFocus()Z
    .registers 2

    .prologue
    .line 1698
    iget-boolean v0, p0, Landroid/support/v7/widget/ar;->i:Z

    if-nez v0, :cond_a

    invoke-super {p0}, Landroid/support/v7/internal/widget/ah;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_a
    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public final hasWindowFocus()Z
    .registers 2

    .prologue
    .line 1678
    iget-boolean v0, p0, Landroid/support/v7/widget/ar;->i:Z

    if-nez v0, :cond_a

    invoke-super {p0}, Landroid/support/v7/internal/widget/ah;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_a
    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public final isFocused()Z
    .registers 2

    .prologue
    .line 1688
    iget-boolean v0, p0, Landroid/support/v7/widget/ar;->i:Z

    if-nez v0, :cond_a

    invoke-super {p0}, Landroid/support/v7/internal/widget/ah;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_a
    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public final isInTouchMode()Z
    .registers 2

    .prologue
    .line 1668
    iget-boolean v0, p0, Landroid/support/v7/widget/ar;->i:Z

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Landroid/support/v7/widget/ar;->h:Z

    if-nez v0, :cond_e

    :cond_8
    invoke-super {p0}, Landroid/support/v7/internal/widget/ah;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_10

    :cond_e
    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method
