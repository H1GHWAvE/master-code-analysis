.class public final Landroid/support/v7/widget/y;
.super Landroid/widget/RadioButton;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/widget/ef;


# instance fields
.field private a:Landroid/support/v7/internal/widget/av;

.field private b:Landroid/support/v7/widget/u;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/y;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4

    .prologue
    .line 55
    sget v0, Landroid/support/v7/a/d;->radioButtonStyle:I

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/y;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 6

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RadioButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 60
    invoke-static {p1}, Landroid/support/v7/internal/widget/av;->a(Landroid/content/Context;)Landroid/support/v7/internal/widget/av;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/y;->a:Landroid/support/v7/internal/widget/av;

    .line 61
    new-instance v0, Landroid/support/v7/widget/u;

    iget-object v1, p0, Landroid/support/v7/widget/y;->a:Landroid/support/v7/internal/widget/av;

    invoke-direct {v0, p0, v1}, Landroid/support/v7/widget/u;-><init>(Landroid/widget/CompoundButton;Landroid/support/v7/internal/widget/av;)V

    iput-object v0, p0, Landroid/support/v7/widget/y;->b:Landroid/support/v7/widget/u;

    .line 62
    iget-object v0, p0, Landroid/support/v7/widget/y;->b:Landroid/support/v7/widget/u;

    invoke-virtual {v0, p2, p3}, Landroid/support/v7/widget/u;->a(Landroid/util/AttributeSet;I)V

    .line 63
    return-void
.end method


# virtual methods
.method public final getCompoundPaddingLeft()I
    .registers 3

    .prologue
    .line 82
    invoke-super {p0}, Landroid/widget/RadioButton;->getCompoundPaddingLeft()I

    move-result v0

    .line 83
    iget-object v1, p0, Landroid/support/v7/widget/y;->b:Landroid/support/v7/widget/u;

    if-eqz v1, :cond_e

    iget-object v1, p0, Landroid/support/v7/widget/y;->b:Landroid/support/v7/widget/u;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/u;->a(I)I

    move-result v0

    :cond_e
    return v0
.end method

.method public final getSupportButtonTintList()Landroid/content/res/ColorStateList;
    .registers 2
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 106
    iget-object v0, p0, Landroid/support/v7/widget/y;->b:Landroid/support/v7/widget/u;

    if-eqz v0, :cond_9

    iget-object v0, p0, Landroid/support/v7/widget/y;->b:Landroid/support/v7/widget/u;

    .line 2091
    iget-object v0, v0, Landroid/support/v7/widget/u;->a:Landroid/content/res/ColorStateList;

    .line 106
    :goto_8
    return-object v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final getSupportButtonTintMode()Landroid/graphics/PorterDuff$Mode;
    .registers 2
    .annotation build Landroid/support/a/z;
    .end annotation

    .prologue
    .line 129
    iget-object v0, p0, Landroid/support/v7/widget/y;->b:Landroid/support/v7/widget/u;

    if-eqz v0, :cond_9

    iget-object v0, p0, Landroid/support/v7/widget/y;->b:Landroid/support/v7/widget/u;

    .line 2102
    iget-object v0, v0, Landroid/support/v7/widget/u;->b:Landroid/graphics/PorterDuff$Mode;

    .line 129
    :goto_8
    return-object v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final setButtonDrawable(I)V
    .registers 4
    .param p1    # I
        .annotation build Landroid/support/a/m;
        .end annotation
    .end param

    .prologue
    .line 75
    iget-object v0, p0, Landroid/support/v7/widget/y;->a:Landroid/support/v7/internal/widget/av;

    if-eqz v0, :cond_f

    iget-object v0, p0, Landroid/support/v7/widget/y;->a:Landroid/support/v7/internal/widget/av;

    .line 1167
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/support/v7/internal/widget/av;->a(IZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 75
    :goto_b
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/y;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 78
    return-void

    .line 75
    :cond_f
    invoke-virtual {p0}, Landroid/support/v7/widget/y;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/support/v4/c/h;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_b
.end method

.method public final setButtonDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 3

    .prologue
    .line 67
    invoke-super {p0, p1}, Landroid/widget/RadioButton;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 68
    iget-object v0, p0, Landroid/support/v7/widget/y;->b:Landroid/support/v7/widget/u;

    if-eqz v0, :cond_c

    .line 69
    iget-object v0, p0, Landroid/support/v7/widget/y;->b:Landroid/support/v7/widget/u;

    invoke-virtual {v0}, Landroid/support/v7/widget/u;->a()V

    .line 71
    :cond_c
    return-void
.end method

.method public final setSupportButtonTintList(Landroid/content/res/ColorStateList;)V
    .registers 3
    .param p1    # Landroid/content/res/ColorStateList;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 94
    iget-object v0, p0, Landroid/support/v7/widget/y;->b:Landroid/support/v7/widget/u;

    if-eqz v0, :cond_9

    .line 95
    iget-object v0, p0, Landroid/support/v7/widget/y;->b:Landroid/support/v7/widget/u;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/u;->a(Landroid/content/res/ColorStateList;)V

    .line 97
    :cond_9
    return-void
.end method

.method public final setSupportButtonTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .registers 3
    .param p1    # Landroid/graphics/PorterDuff$Mode;
        .annotation build Landroid/support/a/z;
        .end annotation
    .end param

    .prologue
    .line 117
    iget-object v0, p0, Landroid/support/v7/widget/y;->b:Landroid/support/v7/widget/u;

    if-eqz v0, :cond_9

    .line 118
    iget-object v0, p0, Landroid/support/v7/widget/y;->b:Landroid/support/v7/widget/u;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/u;->a(Landroid/graphics/PorterDuff$Mode;)V

    .line 120
    :cond_9
    return-void
.end method
