.class final Landroid/support/v7/app/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/app/l;


# instance fields
.field final a:Landroid/support/v7/widget/Toolbar;

.field final b:Landroid/graphics/drawable/Drawable;

.field final c:Ljava/lang/CharSequence;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/Toolbar;)V
    .registers 3

    .prologue
    .line 616
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 617
    iput-object p1, p0, Landroid/support/v7/app/s;->a:Landroid/support/v7/widget/Toolbar;

    .line 618
    invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar;->getNavigationIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/s;->b:Landroid/graphics/drawable/Drawable;

    .line 619
    invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar;->getNavigationContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/s;->c:Ljava/lang/CharSequence;

    .line 620
    return-void
.end method


# virtual methods
.method public final a()Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 639
    iget-object v0, p0, Landroid/support/v7/app/s;->b:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final a(I)V
    .registers 4
    .param p1    # I
        .annotation build Landroid/support/a/ah;
        .end annotation
    .end param

    .prologue
    .line 630
    if-nez p1, :cond_a

    .line 631
    iget-object v0, p0, Landroid/support/v7/app/s;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Landroid/support/v7/app/s;->c:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationContentDescription(Ljava/lang/CharSequence;)V

    .line 635
    :goto_9
    return-void

    .line 633
    :cond_a
    iget-object v0, p0, Landroid/support/v7/app/s;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->setNavigationContentDescription(I)V

    goto :goto_9
.end method

.method public final a(Landroid/graphics/drawable/Drawable;I)V
    .registers 4
    .param p2    # I
        .annotation build Landroid/support/a/ah;
        .end annotation
    .end param

    .prologue
    .line 624
    iget-object v0, p0, Landroid/support/v7/app/s;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(Landroid/graphics/drawable/Drawable;)V

    .line 625
    invoke-virtual {p0, p2}, Landroid/support/v7/app/s;->a(I)V

    .line 626
    return-void
.end method

.method public final b()Landroid/content/Context;
    .registers 2

    .prologue
    .line 644
    iget-object v0, p0, Landroid/support/v7/app/s;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public final c()Z
    .registers 2

    .prologue
    .line 649
    const/4 v0, 0x1

    return v0
.end method
