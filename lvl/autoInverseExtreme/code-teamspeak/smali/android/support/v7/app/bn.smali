.class public final Landroid/support/v7/app/bn;
.super Landroid/support/v4/app/ed;
.source "SourceFile"


# instance fields
.field a:[I

.field b:Landroid/support/v4/media/session/MediaSessionCompat$Token;

.field c:Z

.field h:Landroid/app/PendingIntent;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 175
    invoke-direct {p0}, Landroid/support/v4/app/ed;-><init>()V

    .line 170
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/app/bn;->a:[I

    .line 176
    return-void
.end method

.method private constructor <init>(Landroid/support/v4/app/dm;)V
    .registers 3

    .prologue
    .line 178
    invoke-direct {p0}, Landroid/support/v4/app/ed;-><init>()V

    .line 170
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/app/bn;->a:[I

    .line 179
    invoke-virtual {p0, p1}, Landroid/support/v7/app/bn;->a(Landroid/support/v4/app/dm;)V

    .line 180
    return-void
.end method

.method private a(Landroid/app/PendingIntent;)Landroid/support/v7/app/bn;
    .registers 2

    .prologue
    .line 238
    iput-object p1, p0, Landroid/support/v7/app/bn;->h:Landroid/app/PendingIntent;

    .line 239
    return-object p0
.end method

.method private a(Landroid/support/v4/media/session/MediaSessionCompat$Token;)Landroid/support/v7/app/bn;
    .registers 2

    .prologue
    .line 198
    iput-object p1, p0, Landroid/support/v7/app/bn;->b:Landroid/support/v4/media/session/MediaSessionCompat$Token;

    .line 199
    return-object p0
.end method

.method private a(Z)Landroid/support/v7/app/bn;
    .registers 2

    .prologue
    .line 227
    iput-boolean p1, p0, Landroid/support/v7/app/bn;->c:Z

    .line 228
    return-object p0
.end method

.method private varargs a([I)Landroid/support/v7/app/bn;
    .registers 2

    .prologue
    .line 189
    iput-object p1, p0, Landroid/support/v7/app/bn;->a:[I

    .line 190
    return-object p0
.end method
