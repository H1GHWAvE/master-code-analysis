.class final Landroid/support/v7/app/ba;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/c/b;


# instance fields
.field final synthetic a:Landroid/support/v7/app/AppCompatDelegateImplV7;

.field private b:Landroid/support/v7/c/b;


# direct methods
.method public constructor <init>(Landroid/support/v7/app/AppCompatDelegateImplV7;Landroid/support/v7/c/b;)V
    .registers 3

    .prologue
    .line 1639
    iput-object p1, p0, Landroid/support/v7/app/ba;->a:Landroid/support/v7/app/AppCompatDelegateImplV7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1640
    iput-object p2, p0, Landroid/support/v7/app/ba;->b:Landroid/support/v7/c/b;

    .line 1641
    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/c/a;)V
    .registers 5

    .prologue
    .line 1656
    iget-object v0, p0, Landroid/support/v7/app/ba;->b:Landroid/support/v7/c/b;

    invoke-interface {v0, p1}, Landroid/support/v7/c/b;->a(Landroid/support/v7/c/a;)V

    .line 1657
    iget-object v0, p0, Landroid/support/v7/app/ba;->a:Landroid/support/v7/app/AppCompatDelegateImplV7;

    iget-object v0, v0, Landroid/support/v7/app/AppCompatDelegateImplV7;->v:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_1a

    .line 1658
    iget-object v0, p0, Landroid/support/v7/app/ba;->a:Landroid/support/v7/app/AppCompatDelegateImplV7;

    iget-object v0, v0, Landroid/support/v7/app/AppCompatDelegateImplV7;->f:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/app/ba;->a:Landroid/support/v7/app/AppCompatDelegateImplV7;

    iget-object v1, v1, Landroid/support/v7/app/AppCompatDelegateImplV7;->w:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1661
    :cond_1a
    iget-object v0, p0, Landroid/support/v7/app/ba;->a:Landroid/support/v7/app/AppCompatDelegateImplV7;

    iget-object v0, v0, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-eqz v0, :cond_42

    .line 1662
    iget-object v0, p0, Landroid/support/v7/app/ba;->a:Landroid/support/v7/app/AppCompatDelegateImplV7;

    .line 2090
    invoke-virtual {v0}, Landroid/support/v7/app/AppCompatDelegateImplV7;->n()V

    .line 1663
    iget-object v0, p0, Landroid/support/v7/app/ba;->a:Landroid/support/v7/app/AppCompatDelegateImplV7;

    iget-object v1, p0, Landroid/support/v7/app/ba;->a:Landroid/support/v7/app/AppCompatDelegateImplV7;

    iget-object v1, v1, Landroid/support/v7/app/AppCompatDelegateImplV7;->u:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-static {v1}, Landroid/support/v4/view/cx;->p(Landroid/view/View;)Landroid/support/v4/view/fk;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v4/view/fk;->a(F)Landroid/support/v4/view/fk;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v7/app/AppCompatDelegateImplV7;->x:Landroid/support/v4/view/fk;

    .line 1664
    iget-object v0, p0, Landroid/support/v7/app/ba;->a:Landroid/support/v7/app/AppCompatDelegateImplV7;

    iget-object v0, v0, Landroid/support/v7/app/AppCompatDelegateImplV7;->x:Landroid/support/v4/view/fk;

    new-instance v1, Landroid/support/v7/app/bb;

    invoke-direct {v1, p0}, Landroid/support/v7/app/bb;-><init>(Landroid/support/v7/app/ba;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/fk;->a(Landroid/support/v4/view/gd;)Landroid/support/v4/view/fk;

    .line 1682
    :cond_42
    iget-object v0, p0, Landroid/support/v7/app/ba;->a:Landroid/support/v7/app/AppCompatDelegateImplV7;

    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v7/app/AppCompatDelegateImplV7;->t:Landroid/support/v7/c/a;

    .line 1683
    return-void
.end method

.method public final a(Landroid/support/v7/c/a;Landroid/view/Menu;)Z
    .registers 4

    .prologue
    .line 1644
    iget-object v0, p0, Landroid/support/v7/app/ba;->b:Landroid/support/v7/c/b;

    invoke-interface {v0, p1, p2}, Landroid/support/v7/c/b;->a(Landroid/support/v7/c/a;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/support/v7/c/a;Landroid/view/MenuItem;)Z
    .registers 4

    .prologue
    .line 1652
    iget-object v0, p0, Landroid/support/v7/app/ba;->b:Landroid/support/v7/c/b;

    invoke-interface {v0, p1, p2}, Landroid/support/v7/c/b;->a(Landroid/support/v7/c/a;Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/support/v7/c/a;Landroid/view/Menu;)Z
    .registers 4

    .prologue
    .line 1648
    iget-object v0, p0, Landroid/support/v7/app/ba;->b:Landroid/support/v7/c/b;

    invoke-interface {v0, p1, p2}, Landroid/support/v7/c/b;->b(Landroid/support/v7/c/a;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method
