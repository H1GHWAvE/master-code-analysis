.class public final Landroid/support/v7/app/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/widget/ac;


# instance fields
.field a:Z

.field b:Landroid/view/View$OnClickListener;

.field private final c:Landroid/support/v7/app/l;

.field private final d:Landroid/support/v4/widget/DrawerLayout;

.field private e:Landroid/support/v7/app/o;

.field private f:Landroid/graphics/drawable/Drawable;

.field private g:Z

.field private final h:I

.field private final i:I

.field private j:Z


# direct methods
.method private constructor <init>(Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;II)V
    .registers 11
    .param p3    # I
        .annotation build Landroid/support/a/ah;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Landroid/support/a/ah;
        .end annotation
    .end param

    .prologue
    .line 150
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/app/j;-><init>(Landroid/app/Activity;Landroid/support/v7/widget/Toolbar;Landroid/support/v4/widget/DrawerLayout;II)V

    .line 152
    return-void
.end method

.method private constructor <init>(Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;Landroid/support/v7/widget/Toolbar;II)V
    .registers 12
    .param p4    # I
        .annotation build Landroid/support/a/ah;
        .end annotation
    .end param
    .param p5    # I
        .annotation build Landroid/support/a/ah;
        .end annotation
    .end param

    .prologue
    .line 181
    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p2

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/app/j;-><init>(Landroid/app/Activity;Landroid/support/v7/widget/Toolbar;Landroid/support/v4/widget/DrawerLayout;II)V

    .line 183
    return-void
.end method

.method private constructor <init>(Landroid/app/Activity;Landroid/support/v7/widget/Toolbar;Landroid/support/v4/widget/DrawerLayout;II)V
    .registers 9
    .param p4    # I
        .annotation build Landroid/support/a/ah;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/app/j;->a:Z

    .line 127
    iput-boolean v2, p0, Landroid/support/v7/app/j;->j:Z

    .line 194
    if-eqz p2, :cond_34

    .line 195
    new-instance v0, Landroid/support/v7/app/s;

    invoke-direct {v0, p2}, Landroid/support/v7/app/s;-><init>(Landroid/support/v7/widget/Toolbar;)V

    iput-object v0, p0, Landroid/support/v7/app/j;->c:Landroid/support/v7/app/l;

    .line 196
    new-instance v0, Landroid/support/v7/app/k;

    invoke-direct {v0, p0}, Landroid/support/v7/app/k;-><init>(Landroid/support/v7/app/j;)V

    invoke-virtual {p2, v0}, Landroid/support/v7/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 216
    :goto_1a
    iput-object p3, p0, Landroid/support/v7/app/j;->d:Landroid/support/v4/widget/DrawerLayout;

    .line 217
    iput p4, p0, Landroid/support/v7/app/j;->h:I

    .line 218
    iput p5, p0, Landroid/support/v7/app/j;->i:I

    .line 220
    new-instance v0, Landroid/support/v7/app/n;

    iget-object v1, p0, Landroid/support/v7/app/j;->c:Landroid/support/v7/app/l;

    invoke-interface {v1}, Landroid/support/v7/app/l;->b()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Landroid/support/v7/app/n;-><init>(Landroid/app/Activity;Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/app/j;->e:Landroid/support/v7/app/o;

    .line 226
    invoke-direct {p0}, Landroid/support/v7/app/j;->i()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/j;->f:Landroid/graphics/drawable/Drawable;

    .line 227
    return-void

    .line 206
    :cond_34
    instance-of v0, p1, Landroid/support/v7/app/m;

    if-eqz v0, :cond_42

    move-object v0, p1

    .line 207
    check-cast v0, Landroid/support/v7/app/m;

    invoke-interface {v0}, Landroid/support/v7/app/m;->d()Landroid/support/v7/app/l;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/j;->c:Landroid/support/v7/app/l;

    goto :goto_1a

    .line 208
    :cond_42
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_50

    .line 209
    new-instance v0, Landroid/support/v7/app/r;

    invoke-direct {v0, p1, v2}, Landroid/support/v7/app/r;-><init>(Landroid/app/Activity;B)V

    iput-object v0, p0, Landroid/support/v7/app/j;->c:Landroid/support/v7/app/l;

    goto :goto_1a

    .line 210
    :cond_50
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_5e

    .line 211
    new-instance v0, Landroid/support/v7/app/q;

    invoke-direct {v0, p1, v2}, Landroid/support/v7/app/q;-><init>(Landroid/app/Activity;B)V

    iput-object v0, p0, Landroid/support/v7/app/j;->c:Landroid/support/v7/app/l;

    goto :goto_1a

    .line 213
    :cond_5e
    new-instance v0, Landroid/support/v7/app/p;

    invoke-direct {v0, p1}, Landroid/support/v7/app/p;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Landroid/support/v7/app/j;->c:Landroid/support/v7/app/l;

    goto :goto_1a
.end method

.method private a(I)V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 329
    const/4 v0, 0x0

    .line 330
    if-eqz p1, :cond_e

    .line 331
    iget-object v0, p0, Landroid/support/v7/app/j;->d:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1304
    :cond_e
    if-nez v0, :cond_22

    .line 1305
    invoke-direct {p0}, Landroid/support/v7/app/j;->i()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/j;->f:Landroid/graphics/drawable/Drawable;

    .line 1306
    iput-boolean v1, p0, Landroid/support/v7/app/j;->g:Z

    .line 1312
    :goto_18
    iget-boolean v0, p0, Landroid/support/v7/app/j;->a:Z

    if-nez v0, :cond_21

    .line 1313
    iget-object v0, p0, Landroid/support/v7/app/j;->f:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v0, v1}, Landroid/support/v7/app/j;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 334
    :cond_21
    return-void

    .line 1308
    :cond_22
    iput-object v0, p0, Landroid/support/v7/app/j;->f:Landroid/graphics/drawable/Drawable;

    .line 1309
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/app/j;->g:Z

    goto :goto_18
.end method

.method private a(Landroid/graphics/drawable/Drawable;)V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 304
    if-nez p1, :cond_15

    .line 305
    invoke-direct {p0}, Landroid/support/v7/app/j;->i()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/j;->f:Landroid/graphics/drawable/Drawable;

    .line 306
    iput-boolean v1, p0, Landroid/support/v7/app/j;->g:Z

    .line 312
    :goto_b
    iget-boolean v0, p0, Landroid/support/v7/app/j;->a:Z

    if-nez v0, :cond_14

    .line 313
    iget-object v0, p0, Landroid/support/v7/app/j;->f:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v0, v1}, Landroid/support/v7/app/j;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 315
    :cond_14
    return-void

    .line 308
    :cond_15
    iput-object p1, p0, Landroid/support/v7/app/j;->f:Landroid/graphics/drawable/Drawable;

    .line 309
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/app/j;->g:Z

    goto :goto_b
.end method

.method private a(Landroid/graphics/drawable/Drawable;I)V
    .registers 5

    .prologue
    .line 450
    iget-boolean v0, p0, Landroid/support/v7/app/j;->j:Z

    if-nez v0, :cond_16

    iget-object v0, p0, Landroid/support/v7/app/j;->c:Landroid/support/v7/app/l;

    invoke-interface {v0}, Landroid/support/v7/app/l;->c()Z

    move-result v0

    if-nez v0, :cond_16

    .line 451
    const-string v0, "ActionBarDrawerToggle"

    const-string v1, "DrawerToggle may not show up because NavigationIcon is not visible. You may need to call actionbar.setDisplayHomeAsUpEnabled(true);"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/app/j;->j:Z

    .line 456
    :cond_16
    iget-object v0, p0, Landroid/support/v7/app/j;->c:Landroid/support/v7/app/l;

    invoke-interface {v0, p1, p2}, Landroid/support/v7/app/l;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 457
    return-void
.end method

.method private a(Landroid/view/View$OnClickListener;)V
    .registers 2

    .prologue
    .line 446
    iput-object p1, p0, Landroid/support/v7/app/j;->b:Landroid/view/View$OnClickListener;

    .line 447
    return-void
.end method

.method private a(Z)V
    .registers 4

    .prologue
    .line 355
    iget-boolean v0, p0, Landroid/support/v7/app/j;->a:Z

    if-eq p1, v0, :cond_19

    .line 356
    if-eqz p1, :cond_1d

    .line 357
    iget-object v0, p0, Landroid/support/v7/app/j;->e:Landroid/support/v7/app/o;

    check-cast v0, Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Landroid/support/v7/app/j;->d:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v1}, Landroid/support/v4/widget/DrawerLayout;->c()Z

    move-result v1

    if-eqz v1, :cond_1a

    iget v1, p0, Landroid/support/v7/app/j;->i:I

    :goto_14
    invoke-direct {p0, v0, v1}, Landroid/support/v7/app/j;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 363
    :goto_17
    iput-boolean p1, p0, Landroid/support/v7/app/j;->a:Z

    .line 365
    :cond_19
    return-void

    .line 357
    :cond_1a
    iget v1, p0, Landroid/support/v7/app/j;->h:I

    goto :goto_14

    .line 361
    :cond_1d
    iget-object v0, p0, Landroid/support/v7/app/j;->f:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Landroid/support/v7/app/j;->a(Landroid/graphics/drawable/Drawable;I)V

    goto :goto_17
.end method

.method private static synthetic a(Landroid/support/v7/app/j;)Z
    .registers 2

    .prologue
    .line 64
    iget-boolean v0, p0, Landroid/support/v7/app/j;->a:Z

    return v0
.end method

.method private a(Landroid/view/MenuItem;)Z
    .registers 4

    .prologue
    .line 277
    if-eqz p1, :cond_14

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_14

    iget-boolean v0, p0, Landroid/support/v7/app/j;->a:Z

    if-eqz v0, :cond_14

    .line 278
    invoke-virtual {p0}, Landroid/support/v7/app/j;->d()V

    .line 279
    const/4 v0, 0x1

    .line 281
    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method private b(I)V
    .registers 3

    .prologue
    .line 460
    iget-object v0, p0, Landroid/support/v7/app/j;->c:Landroid/support/v7/app/l;

    invoke-interface {v0, p1}, Landroid/support/v7/app/l;->a(I)V

    .line 461
    return-void
.end method

.method private static synthetic b(Landroid/support/v7/app/j;)V
    .registers 1

    .prologue
    .line 64
    invoke-virtual {p0}, Landroid/support/v7/app/j;->d()V

    return-void
.end method

.method private static synthetic c(Landroid/support/v7/app/j;)Landroid/view/View$OnClickListener;
    .registers 2

    .prologue
    .line 64
    iget-object v0, p0, Landroid/support/v7/app/j;->b:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method private e()V
    .registers 3

    .prologue
    .line 239
    iget-object v0, p0, Landroid/support/v7/app/j;->d:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->c()Z

    move-result v0

    if-eqz v0, :cond_25

    .line 240
    iget-object v0, p0, Landroid/support/v7/app/j;->e:Landroid/support/v7/app/o;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {v0, v1}, Landroid/support/v7/app/o;->a(F)V

    .line 244
    :goto_f
    iget-boolean v0, p0, Landroid/support/v7/app/j;->a:Z

    if-eqz v0, :cond_24

    .line 245
    iget-object v0, p0, Landroid/support/v7/app/j;->e:Landroid/support/v7/app/o;

    check-cast v0, Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Landroid/support/v7/app/j;->d:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v1}, Landroid/support/v4/widget/DrawerLayout;->c()Z

    move-result v1

    if-eqz v1, :cond_2c

    iget v1, p0, Landroid/support/v7/app/j;->i:I

    :goto_21
    invoke-direct {p0, v0, v1}, Landroid/support/v7/app/j;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 249
    :cond_24
    return-void

    .line 242
    :cond_25
    iget-object v0, p0, Landroid/support/v7/app/j;->e:Landroid/support/v7/app/o;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/support/v7/app/o;->a(F)V

    goto :goto_f

    .line 245
    :cond_2c
    iget v1, p0, Landroid/support/v7/app/j;->h:I

    goto :goto_21
.end method

.method private f()V
    .registers 3

    .prologue
    .line 261
    iget-boolean v0, p0, Landroid/support/v7/app/j;->g:Z

    if-nez v0, :cond_a

    .line 262
    invoke-direct {p0}, Landroid/support/v7/app/j;->i()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/j;->f:Landroid/graphics/drawable/Drawable;

    .line 1239
    :cond_a
    iget-object v0, p0, Landroid/support/v7/app/j;->d:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->c()Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 1240
    iget-object v0, p0, Landroid/support/v7/app/j;->e:Landroid/support/v7/app/o;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {v0, v1}, Landroid/support/v7/app/o;->a(F)V

    .line 1244
    :goto_19
    iget-boolean v0, p0, Landroid/support/v7/app/j;->a:Z

    if-eqz v0, :cond_2e

    .line 1245
    iget-object v0, p0, Landroid/support/v7/app/j;->e:Landroid/support/v7/app/o;

    check-cast v0, Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Landroid/support/v7/app/j;->d:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v1}, Landroid/support/v4/widget/DrawerLayout;->c()Z

    move-result v1

    if-eqz v1, :cond_36

    iget v1, p0, Landroid/support/v7/app/j;->i:I

    :goto_2b
    invoke-direct {p0, v0, v1}, Landroid/support/v7/app/j;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 265
    :cond_2e
    return-void

    .line 1242
    :cond_2f
    iget-object v0, p0, Landroid/support/v7/app/j;->e:Landroid/support/v7/app/o;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/support/v7/app/o;->a(F)V

    goto :goto_19

    .line 1245
    :cond_36
    iget v1, p0, Landroid/support/v7/app/j;->h:I

    goto :goto_2b
.end method

.method private g()Z
    .registers 2

    .prologue
    .line 341
    iget-boolean v0, p0, Landroid/support/v7/app/j;->a:Z

    return v0
.end method

.method private h()Landroid/view/View$OnClickListener;
    .registers 2

    .prologue
    .line 432
    iget-object v0, p0, Landroid/support/v7/app/j;->b:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method private i()Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 464
    iget-object v0, p0, Landroid/support/v7/app/j;->c:Landroid/support/v7/app/l;

    invoke-interface {v0}, Landroid/support/v7/app/l;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .registers 3

    .prologue
    .line 390
    iget-object v0, p0, Landroid/support/v7/app/j;->e:Landroid/support/v7/app/o;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {v0, v1}, Landroid/support/v7/app/o;->a(F)V

    .line 391
    iget-boolean v0, p0, Landroid/support/v7/app/j;->a:Z

    if-eqz v0, :cond_10

    .line 392
    iget v0, p0, Landroid/support/v7/app/j;->i:I

    invoke-direct {p0, v0}, Landroid/support/v7/app/j;->b(I)V

    .line 394
    :cond_10
    return-void
.end method

.method public final a(F)V
    .registers 5

    .prologue
    .line 378
    iget-object v0, p0, Landroid/support/v7/app/j;->e:Landroid/support/v7/app/o;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-static {v2, p1}, Ljava/lang/Math;->max(FF)F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-interface {v0, v1}, Landroid/support/v7/app/o;->a(F)V

    .line 379
    return-void
.end method

.method public final b()V
    .registers 3

    .prologue
    .line 405
    iget-object v0, p0, Landroid/support/v7/app/j;->e:Landroid/support/v7/app/o;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/support/v7/app/o;->a(F)V

    .line 406
    iget-boolean v0, p0, Landroid/support/v7/app/j;->a:Z

    if-eqz v0, :cond_f

    .line 407
    iget v0, p0, Landroid/support/v7/app/j;->h:I

    invoke-direct {p0, v0}, Landroid/support/v7/app/j;->b(I)V

    .line 409
    :cond_f
    return-void
.end method

.method public final c()V
    .registers 1

    .prologue
    .line 420
    return-void
.end method

.method final d()V
    .registers 2

    .prologue
    .line 285
    iget-object v0, p0, Landroid/support/v7/app/j;->d:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->d()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 286
    iget-object v0, p0, Landroid/support/v7/app/j;->d:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->b()V

    .line 290
    :goto_d
    return-void

    .line 288
    :cond_e
    iget-object v0, p0, Landroid/support/v7/app/j;->d:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->a()V

    goto :goto_d
.end method
