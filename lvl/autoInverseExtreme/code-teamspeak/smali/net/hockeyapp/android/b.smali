.class public final Lnet/hockeyapp/android/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Ljava/lang/String; = null

.field private static b:Ljava/lang/String; = null

.field private static c:Z = false

.field private static final d:Ljava/lang/String; = "always_send_crash_reports"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    const/4 v0, 0x0

    .line 68
    sput-object v0, Lnet/hockeyapp/android/b;->a:Ljava/lang/String;

    .line 73
    sput-object v0, Lnet/hockeyapp/android/b;->b:Ljava/lang/String;

    .line 78
    const/4 v0, 0x0

    sput-boolean v0, Lnet/hockeyapp/android/b;->c:Z

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 430
    return-void
.end method

.method private static a(Ljava/lang/ref/WeakReference;)I
    .registers 7

    .prologue
    const/4 v2, 0x0

    .line 208
    invoke-static {}, Lnet/hockeyapp/android/b;->c()[Ljava/lang/String;

    move-result-object v3

    .line 209
    const/4 v1, 0x0

    .line 211
    if-eqz v3, :cond_44

    array-length v0, v3

    if-lez v0, :cond_44

    .line 215
    :try_start_b
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 216
    if-eqz v0, :cond_42

    .line 217
    const-string v4, "HockeySDK"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 218
    const-string v4, "ConfirmedFilenames"

    const-string v5, ""

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "\\|"

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;
    :try_end_2b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_2b} :catch_40

    move-result-object v0

    :goto_2c
    move-object v1, v0

    .line 227
    :goto_2d
    if-eqz v1, :cond_3e

    .line 228
    const/4 v0, 0x2

    .line 230
    array-length v4, v3

    :goto_31
    if-ge v2, v4, :cond_3f

    aget-object v5, v3, v2

    .line 231
    invoke-interface {v1, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3e

    .line 230
    add-int/lit8 v2, v2, 0x1

    goto :goto_31

    .line 238
    :cond_3e
    const/4 v0, 0x1

    .line 242
    :cond_3f
    :goto_3f
    return v0

    :catch_40
    move-exception v0

    goto :goto_2d

    :cond_42
    move-object v0, v1

    goto :goto_2c

    :cond_44
    move v0, v2

    goto :goto_3f
.end method

.method private static a([Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 5

    .prologue
    .line 716
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 717
    const/4 v0, 0x0

    :goto_6
    array-length v2, p0

    if-ge v0, v2, :cond_19

    .line 718
    aget-object v2, p0, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 719
    array-length v2, p0

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_16

    .line 720
    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 717
    :cond_16
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 723
    :cond_19
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .registers 4

    .prologue
    .line 94
    const-string v0, "https://sdk.hockeyapp.net/"

    const/4 v1, 0x0

    invoke-static {p0, v0, p1, v1}, Lnet/hockeyapp/android/b;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/i;)V

    .line 95
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/i;)V
    .registers 12

    .prologue
    const/4 v7, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 121
    invoke-static {p0, p1, p2, p3, v2}, Lnet/hockeyapp/android/b;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/i;Z)V

    .line 1164
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 1165
    new-instance v4, Ljava/lang/ref/WeakReference;

    invoke-direct {v4, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 1167
    invoke-static {v4}, Lnet/hockeyapp/android/b;->a(Ljava/lang/ref/WeakReference;)I

    move-result v0

    .line 1168
    if-ne v0, v1, :cond_a5

    .line 1169
    instance-of v0, p0, Landroid/app/Activity;

    if-nez v0, :cond_9a

    move v0, v1

    :goto_1a
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1170
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 1171
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const-string v6, "always_send_crash_reports"

    invoke-interface {v5, v6, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    or-int/2addr v0, v5

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1173
    if-eqz p3, :cond_47

    .line 1174
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1175
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1180
    :cond_47
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_9d

    .line 1181
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 1484
    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 1487
    if-eqz v0, :cond_99

    .line 1495
    new-instance v5, Landroid/app/AlertDialog$Builder;

    invoke-direct {v5, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1496
    invoke-static {p3, v2}, Lnet/hockeyapp/android/aj;->a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1497
    invoke-static {p3, v1}, Lnet/hockeyapp/android/aj;->a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1499
    invoke-static {p3, v7}, Lnet/hockeyapp/android/aj;->a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lnet/hockeyapp/android/c;

    invoke-direct {v1, p3, v4, v3}, Lnet/hockeyapp/android/c;-><init>(Lnet/hockeyapp/android/i;Ljava/lang/ref/WeakReference;Z)V

    invoke-virtual {v5, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1505
    const/4 v0, 0x3

    invoke-static {p3, v0}, Lnet/hockeyapp/android/aj;->a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lnet/hockeyapp/android/d;

    invoke-direct {v1, p3, v4, v3}, Lnet/hockeyapp/android/d;-><init>(Lnet/hockeyapp/android/i;Ljava/lang/ref/WeakReference;Z)V

    invoke-virtual {v5, v0, v1}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1511
    const/4 v0, 0x4

    invoke-static {p3, v0}, Lnet/hockeyapp/android/aj;->a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lnet/hockeyapp/android/e;

    invoke-direct {v1, p3, v4, v3}, Lnet/hockeyapp/android/e;-><init>(Lnet/hockeyapp/android/i;Ljava/lang/ref/WeakReference;Z)V

    invoke-virtual {v5, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1518
    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 1192
    :cond_99
    :goto_99
    return-void

    :cond_9a
    move v0, v2

    .line 1169
    goto/16 :goto_1a

    .line 1184
    :cond_9d
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1526
    invoke-static {v4, p3, v0}, Lnet/hockeyapp/android/b;->c(Ljava/lang/ref/WeakReference;Lnet/hockeyapp/android/i;Z)V

    goto :goto_99

    .line 1187
    :cond_a5
    if-ne v0, v7, :cond_af

    .line 1192
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2526
    invoke-static {v4, p3, v0}, Lnet/hockeyapp/android/b;->c(Ljava/lang/ref/WeakReference;Lnet/hockeyapp/android/i;Z)V

    goto :goto_99

    .line 1195
    :cond_af
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {p3, v0}, Lnet/hockeyapp/android/b;->a(Lnet/hockeyapp/android/i;Z)V

    goto :goto_99
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/i;Z)V
    .registers 7

    .prologue
    .line 459
    if-eqz p0, :cond_28

    .line 460
    sput-object p1, Lnet/hockeyapp/android/b;->b:Ljava/lang/String;

    .line 461
    invoke-static {p2}, Lnet/hockeyapp/android/e/w;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lnet/hockeyapp/android/b;->a:Ljava/lang/String;

    .line 463
    invoke-static {p0}, Lnet/hockeyapp/android/a;->a(Landroid/content/Context;)V

    .line 465
    sget-object v0, Lnet/hockeyapp/android/b;->a:Ljava/lang/String;

    if-nez v0, :cond_15

    .line 466
    sget-object v0, Lnet/hockeyapp/android/a;->d:Ljava/lang/String;

    sput-object v0, Lnet/hockeyapp/android/b;->a:Ljava/lang/String;

    .line 469
    :cond_15
    if-eqz p4, :cond_28

    .line 470
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 471
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 472
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {p3, v0}, Lnet/hockeyapp/android/b;->a(Lnet/hockeyapp/android/i;Z)V

    .line 475
    :cond_28
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/i;)V
    .registers 4

    .prologue
    .line 107
    const-string v0, "https://sdk.hockeyapp.net/"

    invoke-static {p0, v0, p1, p2}, Lnet/hockeyapp/android/b;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/i;)V

    .line 108
    return-void
.end method

.method private static a(Landroid/content/Context;Lnet/hockeyapp/android/i;)V
    .registers 10

    .prologue
    const/4 v7, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 164
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 165
    new-instance v4, Ljava/lang/ref/WeakReference;

    invoke-direct {v4, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 167
    invoke-static {v4}, Lnet/hockeyapp/android/b;->a(Ljava/lang/ref/WeakReference;)I

    move-result v0

    .line 168
    if-ne v0, v1, :cond_a2

    .line 169
    instance-of v0, p0, Landroid/app/Activity;

    if-nez v0, :cond_97

    move v0, v1

    :goto_17
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 170
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 171
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const-string v6, "always_send_crash_reports"

    invoke-interface {v5, v6, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    or-int/2addr v0, v5

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 173
    if-eqz p1, :cond_44

    .line 174
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 175
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 180
    :cond_44
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_9a

    .line 181
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 3484
    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 3487
    if-eqz v0, :cond_96

    .line 3495
    new-instance v5, Landroid/app/AlertDialog$Builder;

    invoke-direct {v5, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 3496
    invoke-static {p1, v2}, Lnet/hockeyapp/android/aj;->a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 3497
    invoke-static {p1, v1}, Lnet/hockeyapp/android/aj;->a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 3499
    invoke-static {p1, v7}, Lnet/hockeyapp/android/aj;->a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lnet/hockeyapp/android/c;

    invoke-direct {v1, p1, v4, v3}, Lnet/hockeyapp/android/c;-><init>(Lnet/hockeyapp/android/i;Ljava/lang/ref/WeakReference;Z)V

    invoke-virtual {v5, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 3505
    const/4 v0, 0x3

    invoke-static {p1, v0}, Lnet/hockeyapp/android/aj;->a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lnet/hockeyapp/android/d;

    invoke-direct {v1, p1, v4, v3}, Lnet/hockeyapp/android/d;-><init>(Lnet/hockeyapp/android/i;Ljava/lang/ref/WeakReference;Z)V

    invoke-virtual {v5, v0, v1}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 3511
    const/4 v0, 0x4

    invoke-static {p1, v0}, Lnet/hockeyapp/android/aj;->a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lnet/hockeyapp/android/e;

    invoke-direct {v1, p1, v4, v3}, Lnet/hockeyapp/android/e;-><init>(Lnet/hockeyapp/android/i;Ljava/lang/ref/WeakReference;Z)V

    invoke-virtual {v5, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 3518
    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 197
    :cond_96
    :goto_96
    return-void

    :cond_97
    move v0, v2

    .line 169
    goto/16 :goto_17

    .line 184
    :cond_9a
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 3526
    invoke-static {v4, p1, v0}, Lnet/hockeyapp/android/b;->c(Ljava/lang/ref/WeakReference;Lnet/hockeyapp/android/i;Z)V

    goto :goto_96

    .line 187
    :cond_a2
    if-ne v0, v7, :cond_ac

    .line 192
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 4526
    invoke-static {v4, p1, v0}, Lnet/hockeyapp/android/b;->c(Ljava/lang/ref/WeakReference;Lnet/hockeyapp/android/i;Z)V

    goto :goto_96

    .line 195
    :cond_ac
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {p1, v0}, Lnet/hockeyapp/android/b;->a(Lnet/hockeyapp/android/i;Z)V

    goto :goto_96
.end method

.method private static a(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V
    .registers 5

    .prologue
    .line 615
    if-eqz p0, :cond_2a

    .line 616
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 617
    if-eqz v0, :cond_2a

    .line 618
    const-string v1, "HockeySDK"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 619
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 620
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RETRY_COUNT: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 621
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 624
    :cond_2a
    return-void
.end method

.method private static a(Ljava/lang/ref/WeakReference;Ljava/lang/String;I)V
    .registers 8

    .prologue
    const/4 v4, 0x0

    .line 585
    const/4 v0, -0x1

    if-ne p2, v0, :cond_5

    .line 607
    :cond_4
    :goto_4
    return-void

    .line 590
    :cond_5
    if-eqz p0, :cond_4

    .line 591
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 592
    if-eqz v0, :cond_4

    .line 593
    const-string v1, "HockeySDK"

    invoke-virtual {v0, v1, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 594
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 596
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RETRY_COUNT: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 597
    if-lt v0, p2, :cond_35

    .line 598
    invoke-static {p0, p1}, Lnet/hockeyapp/android/b;->b(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V

    .line 599
    invoke-static {p0, p1}, Lnet/hockeyapp/android/b;->a(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V

    goto :goto_4

    .line 602
    :cond_35
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RETRY_COUNT: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v0, v0, 0x1

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 603
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_4
.end method

.method private static a(Ljava/lang/ref/WeakReference;Lnet/hockeyapp/android/i;)V
    .registers 3

    .prologue
    .line 252
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lnet/hockeyapp/android/b;->a(Ljava/lang/ref/WeakReference;Lnet/hockeyapp/android/i;Lnet/hockeyapp/android/c/b;)V

    .line 253
    return-void
.end method

.method public static a(Ljava/lang/ref/WeakReference;Lnet/hockeyapp/android/i;Lnet/hockeyapp/android/c/b;)V
    .registers 16

    .prologue
    .line 262
    invoke-static {}, Lnet/hockeyapp/android/b;->c()[Ljava/lang/String;

    move-result-object v6

    .line 263
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 265
    if-eqz v6, :cond_192

    array-length v0, v6

    if-lez v0, :cond_192

    .line 266
    const-string v0, "HockeyApp"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Found "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v3, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " stacktrace(s)."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    const/4 v0, 0x0

    move-object v2, v1

    move v1, v0

    :goto_2c
    array-length v0, v6

    if-ge v1, v0, :cond_192

    .line 269
    const/4 v3, 0x0

    .line 272
    :try_start_30
    aget-object v7, v6, v1

    .line 273
    invoke-static {p0, v7}, Lnet/hockeyapp/android/b;->c(Ljava/lang/ref/WeakReference;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 274
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_118

    .line 277
    const-string v0, "HockeyApp"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Transmitting crash data: \n"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    const-string v0, ".stacktrace"

    const-string v4, ".user"

    invoke-virtual {v7, v0, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lnet/hockeyapp/android/b;->c(Ljava/lang/ref/WeakReference;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 281
    const-string v4, ".stacktrace"

    const-string v5, ".contact"

    invoke-virtual {v7, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Lnet/hockeyapp/android/b;->c(Ljava/lang/ref/WeakReference;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 283
    if-eqz p2, :cond_297

    .line 5029
    iget-object v5, p2, Lnet/hockeyapp/android/c/b;->c:Ljava/lang/String;

    .line 285
    if-eqz v5, :cond_294

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_294

    .line 6021
    :goto_74
    iget-object v0, p2, Lnet/hockeyapp/android/c/b;->b:Ljava/lang/String;

    .line 289
    if-eqz v0, :cond_7f

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_7f

    move-object v4, v0

    .line 295
    :cond_7f
    :goto_7f
    const-string v0, ".stacktrace"

    const-string v9, ".description"

    invoke-virtual {v7, v0, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lnet/hockeyapp/android/b;->c(Ljava/lang/ref/WeakReference;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 296
    if-eqz p2, :cond_13b

    .line 7013
    iget-object v0, p2, Lnet/hockeyapp/android/c/b;->a:Ljava/lang/String;

    .line 297
    :goto_8f
    if-eqz v7, :cond_ae

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_ae

    .line 298
    if-eqz v0, :cond_13f

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_13f

    .line 299
    const-string v9, "%s\n\nLog:\n%s"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v0, v10, v11

    const/4 v0, 0x1

    aput-object v7, v10, v0

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 305
    :cond_ae
    :goto_ae
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 307
    const-string v9, "raw"

    invoke-interface {v7, v9, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 308
    const-string v8, "userID"

    invoke-interface {v7, v8, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    const-string v5, "contact"

    invoke-interface {v7, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 310
    const-string v4, "description"

    invoke-interface {v7, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 311
    const-string v0, "sdk"

    const-string v4, "HockeySDK"

    invoke-interface {v7, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 312
    const-string v0, "sdk_version"

    const-string v4, "3.6.0"

    invoke-interface {v7, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 314
    new-instance v0, Lnet/hockeyapp/android/e/l;

    .line 7578
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lnet/hockeyapp/android/b;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "api/2/apps/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lnet/hockeyapp/android/b;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/crashes/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 314
    invoke-direct {v0, v4}, Lnet/hockeyapp/android/e/l;-><init>(Ljava/lang/String;)V

    const-string v4, "POST"

    .line 8071
    iput-object v4, v0, Lnet/hockeyapp/android/e/l;->b:Ljava/lang/String;

    .line 316
    invoke-virtual {v0, v7}, Lnet/hockeyapp/android/e/l;->a(Ljava/util/Map;)Lnet/hockeyapp/android/e/l;

    move-result-object v0

    .line 317
    invoke-virtual {v0}, Lnet/hockeyapp/android/e/l;->a()Ljava/net/HttpURLConnection;
    :try_end_106
    .catch Ljava/lang/Exception; {:try_start_30 .. :try_end_106} :catch_14f
    .catchall {:try_start_30 .. :try_end_106} :catchall_16f

    move-result-object v3

    .line 319
    :try_start_107
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    .line 321
    const/16 v4, 0xca

    if-eq v0, v4, :cond_113

    const/16 v4, 0xc9

    if-ne v0, v4, :cond_14d

    :cond_113
    const/4 v0, 0x1

    :goto_114
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_117
    .catch Ljava/lang/Exception; {:try_start_107 .. :try_end_117} :catch_14f
    .catchall {:try_start_107 .. :try_end_117} :catchall_28e

    move-result-object v2

    .line 329
    :cond_118
    if-eqz v3, :cond_11d

    .line 330
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 332
    :cond_11d
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_23f

    .line 333
    const-string v0, "HockeyApp"

    const-string v3, "Transmission succeeded"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    aget-object v0, v6, v1

    invoke-static {p0, v0}, Lnet/hockeyapp/android/b;->b(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V

    .line 336
    if-eqz p1, :cond_136

    .line 338
    aget-object v0, v6, v1

    :goto_133
    invoke-static {p0, v0}, Lnet/hockeyapp/android/b;->a(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V

    .line 268
    :cond_136
    :goto_136
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_2c

    .line 296
    :cond_13b
    :try_start_13b
    const-string v0, ""

    goto/16 :goto_8f

    .line 301
    :cond_13f
    const-string v0, "Log:\n%s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v7, v9, v10

    invoke-static {v0, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_14a
    .catch Ljava/lang/Exception; {:try_start_13b .. :try_end_14a} :catch_14f
    .catchall {:try_start_13b .. :try_end_14a} :catchall_16f

    move-result-object v0

    goto/16 :goto_ae

    .line 321
    :cond_14d
    const/4 v0, 0x0

    goto :goto_114

    .line 326
    :catch_14f
    move-exception v0

    :try_start_150
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_153
    .catchall {:try_start_150 .. :try_end_153} :catchall_28e

    .line 329
    if-eqz v3, :cond_158

    .line 330
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 332
    :cond_158
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1e8

    .line 333
    const-string v0, "HockeyApp"

    const-string v3, "Transmission succeeded"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    aget-object v0, v6, v1

    invoke-static {p0, v0}, Lnet/hockeyapp/android/b;->b(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V

    .line 336
    if-eqz p1, :cond_136

    .line 338
    aget-object v0, v6, v1

    goto :goto_133

    .line 329
    :catchall_16f
    move-exception v0

    move-object v12, v0

    move-object v0, v3

    move-object v3, v12

    :goto_173
    if-eqz v0, :cond_178

    .line 330
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 332
    :cond_178
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_193

    .line 333
    const-string v0, "HockeyApp"

    const-string v2, "Transmission succeeded"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    aget-object v0, v6, v1

    invoke-static {p0, v0}, Lnet/hockeyapp/android/b;->b(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V

    .line 336
    if-eqz p1, :cond_191

    .line 338
    aget-object v0, v6, v1

    invoke-static {p0, v0}, Lnet/hockeyapp/android/b;->a(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V

    .line 329
    :cond_191
    :goto_191
    throw v3

    .line 351
    :cond_192
    return-void

    .line 342
    :cond_193
    const-string v0, "HockeyApp"

    const-string v2, "Transmission failed, will retry on next register() call"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    if-eqz p1, :cond_191

    .line 345
    aget-object v1, v6, v1

    .line 8590
    if-eqz p0, :cond_191

    .line 8591
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 8592
    if-eqz v0, :cond_191

    .line 8593
    const-string v2, "HockeySDK"

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 8594
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 8596
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "RETRY_COUNT: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 8597
    if-lez v0, :cond_1d0

    .line 8598
    invoke-static {p0, v1}, Lnet/hockeyapp/android/b;->b(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V

    .line 8599
    invoke-static {p0, v1}, Lnet/hockeyapp/android/b;->a(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V

    goto :goto_191

    .line 8602
    :cond_1d0
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "RETRY_COUNT: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v0, v0, 0x1

    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 8603
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_191

    .line 342
    :cond_1e8
    const-string v0, "HockeyApp"

    const-string v3, "Transmission failed, will retry on next register() call"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    if-eqz p1, :cond_136

    .line 345
    aget-object v3, v6, v1

    .line 8590
    if-eqz p0, :cond_136

    .line 8591
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 8592
    if-eqz v0, :cond_136

    .line 8593
    const-string v4, "HockeySDK"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 8594
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 8596
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "RETRY_COUNT: "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    invoke-interface {v0, v5, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 8597
    if-lez v0, :cond_226

    .line 8598
    invoke-static {p0, v3}, Lnet/hockeyapp/android/b;->b(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V

    .line 8599
    :goto_221
    invoke-static {p0, v3}, Lnet/hockeyapp/android/b;->a(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V

    goto/16 :goto_136

    .line 8602
    :cond_226
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "RETRY_COUNT: "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v0, v0, 0x1

    invoke-interface {v4, v3, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 8603
    :goto_23a
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_136

    .line 342
    :cond_23f
    const-string v0, "HockeyApp"

    const-string v3, "Transmission failed, will retry on next register() call"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    if-eqz p1, :cond_136

    .line 345
    aget-object v3, v6, v1

    .line 8590
    if-eqz p0, :cond_136

    .line 8591
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 8592
    if-eqz v0, :cond_136

    .line 8593
    const-string v4, "HockeySDK"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 8594
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 8596
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "RETRY_COUNT: "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    invoke-interface {v0, v5, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 8597
    if-lez v0, :cond_279

    .line 8598
    invoke-static {p0, v3}, Lnet/hockeyapp/android/b;->b(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V

    goto :goto_221

    .line 8602
    :cond_279
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "RETRY_COUNT: "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v0, v0, 0x1

    invoke-interface {v4, v3, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_23a

    .line 329
    :catchall_28e
    move-exception v0

    move-object v12, v0

    move-object v0, v3

    move-object v3, v12

    goto/16 :goto_173

    :cond_294
    move-object v5, v0

    goto/16 :goto_74

    :cond_297
    move-object v5, v0

    goto/16 :goto_7f
.end method

.method private static a(Ljava/lang/ref/WeakReference;Lnet/hockeyapp/android/i;Z)V
    .registers 6

    .prologue
    .line 484
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 487
    if-nez v0, :cond_9

    .line 519
    :goto_8
    return-void

    .line 495
    :cond_9
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 496
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lnet/hockeyapp/android/aj;->a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 497
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lnet/hockeyapp/android/aj;->a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 499
    const/4 v0, 0x2

    invoke-static {p1, v0}, Lnet/hockeyapp/android/aj;->a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lnet/hockeyapp/android/c;

    invoke-direct {v2, p1, p0, p2}, Lnet/hockeyapp/android/c;-><init>(Lnet/hockeyapp/android/i;Ljava/lang/ref/WeakReference;Z)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 505
    const/4 v0, 0x3

    invoke-static {p1, v0}, Lnet/hockeyapp/android/aj;->a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lnet/hockeyapp/android/d;

    invoke-direct {v2, p1, p0, p2}, Lnet/hockeyapp/android/d;-><init>(Lnet/hockeyapp/android/i;Ljava/lang/ref/WeakReference;Z)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 511
    const/4 v0, 0x4

    invoke-static {p1, v0}, Lnet/hockeyapp/android/aj;->a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lnet/hockeyapp/android/e;

    invoke-direct {v2, p1, p0, p2}, Lnet/hockeyapp/android/e;-><init>(Lnet/hockeyapp/android/i;Ljava/lang/ref/WeakReference;Z)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 518
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_8
.end method

.method private static a(Lnet/hockeyapp/android/i;Z)V
    .registers 6

    .prologue
    .line 554
    sget-object v0, Lnet/hockeyapp/android/a;->b:Ljava/lang/String;

    if-eqz v0, :cond_3c

    sget-object v0, Lnet/hockeyapp/android/a;->d:Ljava/lang/String;

    if-eqz v0, :cond_3c

    .line 556
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    .line 557
    if-eqz v0, :cond_2a

    .line 558
    const-string v1, "HockeyApp"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Current handler class = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 562
    :cond_2a
    instance-of v1, v0, Lnet/hockeyapp/android/j;

    if-eqz v1, :cond_33

    .line 563
    check-cast v0, Lnet/hockeyapp/android/j;

    .line 10067
    iput-object p0, v0, Lnet/hockeyapp/android/j;->a:Lnet/hockeyapp/android/i;

    .line 572
    :goto_32
    return-void

    .line 566
    :cond_33
    new-instance v1, Lnet/hockeyapp/android/j;

    invoke-direct {v1, v0, p0, p1}, Lnet/hockeyapp/android/j;-><init>(Ljava/lang/Thread$UncaughtExceptionHandler;Lnet/hockeyapp/android/i;Z)V

    invoke-static {v1}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    goto :goto_32

    .line 570
    :cond_3c
    const-string v0, "HockeyApp"

    const-string v1, "Exception handler not set because version or package is null."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_32
.end method

.method static synthetic a()Z
    .registers 1

    .prologue
    .line 64
    const/4 v0, 0x0

    sput-boolean v0, Lnet/hockeyapp/android/b;->c:Z

    return v0
.end method

.method public static a(Lnet/hockeyapp/android/c/a;Lnet/hockeyapp/android/i;Ljava/lang/ref/WeakReference;Z)Z
    .registers 10

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 402
    sget-object v0, Lnet/hockeyapp/android/h;->a:[I

    invoke-virtual {p0}, Lnet/hockeyapp/android/c/a;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_9c

    .line 430
    :cond_d
    :goto_d
    return v1

    .line 9359
    :pswitch_e
    invoke-static {}, Lnet/hockeyapp/android/b;->c()[Ljava/lang/String;

    move-result-object v3

    .line 9361
    if-eqz v3, :cond_6e

    array-length v0, v3

    if-lez v0, :cond_6e

    .line 9362
    const-string v0, "HockeyApp"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Found "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v5, v3

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " stacktrace(s)."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 9364
    :goto_32
    array-length v0, v3

    if-ge v1, v0, :cond_6e

    .line 9367
    if-eqz p2, :cond_65

    .line 9368
    :try_start_37
    const-string v0, "HockeyApp"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Delete stacktrace "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v5, v3, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 9369
    aget-object v0, v3, v1

    invoke-static {p2, v0}, Lnet/hockeyapp/android/b;->b(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V

    .line 9371
    invoke-virtual {p2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 9372
    if-eqz v0, :cond_65

    .line 9373
    aget-object v4, v3, v1

    invoke-virtual {v0, v4}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z
    :try_end_65
    .catch Ljava/lang/Exception; {:try_start_37 .. :try_end_65} :catch_69

    .line 9364
    :cond_65
    :goto_65
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_32

    .line 9378
    :catch_69
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_65

    .line 409
    :cond_6e
    invoke-static {p1, p3}, Lnet/hockeyapp/android/b;->a(Lnet/hockeyapp/android/i;Z)V

    move v1, v2

    .line 410
    goto :goto_d

    .line 412
    :pswitch_73
    const/4 v0, 0x0

    .line 413
    if-eqz p2, :cond_7c

    .line 414
    invoke-virtual {p2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 417
    :cond_7c
    if-eqz v0, :cond_d

    .line 421
    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 422
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "always_send_crash_reports"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 424
    invoke-static {p2, p1, p3}, Lnet/hockeyapp/android/b;->c(Ljava/lang/ref/WeakReference;Lnet/hockeyapp/android/i;Z)V

    move v1, v2

    .line 425
    goto/16 :goto_d

    .line 427
    :pswitch_95
    invoke-static {p2, p1, p3}, Lnet/hockeyapp/android/b;->c(Ljava/lang/ref/WeakReference;Lnet/hockeyapp/android/i;Z)V

    move v1, v2

    .line 428
    goto/16 :goto_d

    .line 402
    nop

    :pswitch_data_9c
    .packed-switch 0x1
        :pswitch_e
        :pswitch_73
        :pswitch_95
    .end packed-switch
.end method

.method private static b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 578
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lnet/hockeyapp/android/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "api/2/apps/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lnet/hockeyapp/android/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/crashes/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/i;)V
    .registers 5

    .prologue
    .line 151
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, p3, v0}, Lnet/hockeyapp/android/b;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/i;Z)V

    .line 152
    return-void
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/i;)V
    .registers 5

    .prologue
    .line 136
    const-string v0, "https://sdk.hockeyapp.net/"

    const/4 v1, 0x1

    invoke-static {p0, v0, p1, p2, v1}, Lnet/hockeyapp/android/b;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/i;Z)V

    .line 137
    return-void
.end method

.method private static b(Ljava/lang/ref/WeakReference;)V
    .registers 6

    .prologue
    .line 359
    invoke-static {}, Lnet/hockeyapp/android/b;->c()[Ljava/lang/String;

    move-result-object v2

    .line 361
    if-eqz v2, :cond_62

    array-length v0, v2

    if-lez v0, :cond_62

    .line 362
    const-string v0, "HockeyApp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Found "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v3, v2

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " stacktrace(s)."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    const/4 v0, 0x0

    move v1, v0

    :goto_26
    array-length v0, v2

    if-ge v1, v0, :cond_62

    .line 367
    if-eqz p0, :cond_59

    .line 368
    :try_start_2b
    const-string v0, "HockeyApp"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Delete stacktrace "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v4, v2, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    aget-object v0, v2, v1

    invoke-static {p0, v0}, Lnet/hockeyapp/android/b;->b(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V

    .line 371
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 372
    if-eqz v0, :cond_59

    .line 373
    aget-object v3, v2, v1

    invoke-virtual {v0, v3}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z
    :try_end_59
    .catch Ljava/lang/Exception; {:try_start_2b .. :try_end_59} :catch_5d

    .line 364
    :cond_59
    :goto_59
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_26

    .line 378
    :catch_5d
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_59

    .line 382
    :cond_62
    return-void
.end method

.method private static b(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V
    .registers 5

    .prologue
    .line 632
    if-eqz p0, :cond_2e

    .line 633
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 634
    if-eqz v0, :cond_2e

    .line 635
    invoke-virtual {v0, p1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    .line 637
    const-string v1, ".stacktrace"

    const-string v2, ".user"

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 638
    invoke-virtual {v0, v1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    .line 640
    const-string v1, ".stacktrace"

    const-string v2, ".contact"

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 641
    invoke-virtual {v0, v1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    .line 643
    const-string v1, ".stacktrace"

    const-string v2, ".description"

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 644
    invoke-virtual {v0, v1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    .line 647
    :cond_2e
    return-void
.end method

.method private static b(Ljava/lang/ref/WeakReference;Lnet/hockeyapp/android/i;Z)V
    .registers 3

    .prologue
    .line 526
    invoke-static {p0, p1, p2}, Lnet/hockeyapp/android/b;->c(Ljava/lang/ref/WeakReference;Lnet/hockeyapp/android/i;Z)V

    .line 527
    return-void
.end method

.method private static c(Ljava/lang/ref/WeakReference;Ljava/lang/String;)Ljava/lang/String;
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 654
    if-eqz p0, :cond_52

    .line 655
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 656
    if-eqz v0, :cond_52

    .line 657
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 660
    :try_start_10
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-virtual {v0, p1}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1e
    .catch Ljava/io/FileNotFoundException; {:try_start_10 .. :try_end_1e} :catch_60
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_1e} :catch_3d
    .catchall {:try_start_10 .. :try_end_1e} :catchall_4a

    .line 662
    :goto_1e
    :try_start_1e
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_56

    .line 663
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 664
    const-string v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_30
    .catch Ljava/io/FileNotFoundException; {:try_start_1e .. :try_end_30} :catch_31
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_30} :catch_5e
    .catchall {:try_start_1e .. :try_end_30} :catchall_5c

    goto :goto_1e

    :catch_31
    move-exception v0

    move-object v1, v2

    .line 673
    :goto_33
    if-eqz v1, :cond_38

    .line 675
    :try_start_35
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_38
    .catch Ljava/io/IOException; {:try_start_35 .. :try_end_38} :catch_48

    .line 682
    :cond_38
    :goto_38
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 686
    :goto_3c
    return-object v0

    .line 670
    :catch_3d
    move-exception v0

    move-object v2, v1

    :goto_3f
    :try_start_3f
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_42
    .catchall {:try_start_3f .. :try_end_42} :catchall_5c

    .line 673
    if-eqz v2, :cond_38

    .line 675
    :try_start_44
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_47
    .catch Ljava/io/IOException; {:try_start_44 .. :try_end_47} :catch_48

    goto :goto_38

    :catch_48
    move-exception v0

    goto :goto_38

    .line 673
    :catchall_4a
    move-exception v0

    move-object v2, v1

    :goto_4c
    if-eqz v2, :cond_51

    .line 675
    :try_start_4e
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_51
    .catch Ljava/io/IOException; {:try_start_4e .. :try_end_51} :catch_54

    .line 673
    :cond_51
    :goto_51
    throw v0

    :cond_52
    move-object v0, v1

    .line 686
    goto :goto_3c

    :catch_54
    move-exception v1

    goto :goto_51

    .line 673
    :cond_56
    if-eqz v2, :cond_38

    .line 675
    :try_start_58
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_5b
    .catch Ljava/io/IOException; {:try_start_58 .. :try_end_5b} :catch_48

    goto :goto_38

    .line 673
    :catchall_5c
    move-exception v0

    goto :goto_4c

    .line 670
    :catch_5e
    move-exception v0

    goto :goto_3f

    :catch_60
    move-exception v0

    goto :goto_33
.end method

.method private static c(Ljava/lang/ref/WeakReference;)V
    .registers 3

    .prologue
    .line 443
    if-eqz p0, :cond_1b

    .line 444
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 446
    if-eqz v0, :cond_1b

    .line 447
    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 448
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "always_send_crash_reports"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 451
    :cond_1b
    return-void
.end method

.method private static c(Ljava/lang/ref/WeakReference;Lnet/hockeyapp/android/i;Z)V
    .registers 10

    .prologue
    const/4 v1, 0x0

    .line 534
    .line 9694
    if-eqz p0, :cond_41

    .line 9695
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 9696
    if-eqz v0, :cond_41

    .line 9698
    :try_start_b
    invoke-static {}, Lnet/hockeyapp/android/b;->c()[Ljava/lang/String;

    move-result-object v2

    .line 9699
    const-string v3, "HockeySDK"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 9700
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 9701
    const-string v4, "ConfirmedFilenames"

    const-string v5, "|"

    .line 9716
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    move v0, v1

    .line 9717
    :goto_24
    array-length v1, v2

    if-ge v0, v1, :cond_37

    .line 9718
    aget-object v1, v2, v0

    invoke-virtual {v6, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 9719
    array-length v1, v2

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_34

    .line 9720
    invoke-virtual {v6, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 9717
    :cond_34
    add-int/lit8 v0, v0, 0x1

    goto :goto_24

    .line 9723
    :cond_37
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 9701
    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 9702
    invoke-static {v3}, Lnet/hockeyapp/android/e/n;->a(Landroid/content/SharedPreferences$Editor;)V
    :try_end_41
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_41} :catch_54

    .line 535
    :cond_41
    :goto_41
    invoke-static {p1, p2}, Lnet/hockeyapp/android/b;->a(Lnet/hockeyapp/android/i;Z)V

    .line 537
    sget-boolean v0, Lnet/hockeyapp/android/b;->c:Z

    if-nez v0, :cond_53

    .line 538
    const/4 v0, 0x1

    sput-boolean v0, Lnet/hockeyapp/android/b;->c:Z

    .line 540
    new-instance v0, Lnet/hockeyapp/android/f;

    invoke-direct {v0, p0, p1}, Lnet/hockeyapp/android/f;-><init>(Ljava/lang/ref/WeakReference;Lnet/hockeyapp/android/i;)V

    .line 546
    invoke-virtual {v0}, Lnet/hockeyapp/android/f;->start()V

    .line 548
    :cond_53
    return-void

    :catch_54
    move-exception v0

    goto :goto_41
.end method

.method private static c()[Ljava/lang/String;
    .registers 3

    .prologue
    .line 730
    sget-object v0, Lnet/hockeyapp/android/a;->a:Ljava/lang/String;

    if-eqz v0, :cond_4e

    .line 731
    const-string v0, "HockeyApp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Looking for exceptions in: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lnet/hockeyapp/android/a;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 734
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lnet/hockeyapp/android/a;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 735
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    move-result v1

    .line 736
    if-nez v1, :cond_44

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_44

    .line 737
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    .line 750
    :goto_43
    return-object v0

    .line 741
    :cond_44
    new-instance v1, Lnet/hockeyapp/android/g;

    invoke-direct {v1}, Lnet/hockeyapp/android/g;-><init>()V

    .line 746
    invoke-virtual {v0, v1}, Ljava/io/File;->list(Ljava/io/FilenameFilter;)[Ljava/lang/String;

    move-result-object v0

    goto :goto_43

    .line 749
    :cond_4e
    const-string v0, "HockeyApp"

    const-string v1, "Can\'t search for exception as file path is null."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 750
    const/4 v0, 0x0

    goto :goto_43
.end method

.method private static d(Ljava/lang/ref/WeakReference;)V
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 694
    if-eqz p0, :cond_41

    .line 695
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 696
    if-eqz v0, :cond_41

    .line 698
    :try_start_b
    invoke-static {}, Lnet/hockeyapp/android/b;->c()[Ljava/lang/String;

    move-result-object v2

    .line 699
    const-string v3, "HockeySDK"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 700
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 701
    const-string v4, "ConfirmedFilenames"

    const-string v5, "|"

    .line 10716
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    move v0, v1

    .line 10717
    :goto_24
    array-length v1, v2

    if-ge v0, v1, :cond_37

    .line 10718
    aget-object v1, v2, v0

    invoke-virtual {v6, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 10719
    array-length v1, v2

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_34

    .line 10720
    invoke-virtual {v6, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 10717
    :cond_34
    add-int/lit8 v0, v0, 0x1

    goto :goto_24

    .line 10723
    :cond_37
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 701
    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 702
    invoke-static {v3}, Lnet/hockeyapp/android/e/n;->a(Landroid/content/SharedPreferences$Editor;)V
    :try_end_41
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_41} :catch_42

    .line 709
    :cond_41
    :goto_41
    return-void

    :catch_42
    move-exception v0

    goto :goto_41
.end method
