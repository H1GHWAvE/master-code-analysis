.class public final Lnet/hockeyapp/android/f/k;
.super Landroid/widget/ImageView;
.source "SourceFile"


# static fields
.field private static final b:F = 4.0f


# instance fields
.field public a:Ljava/util/Stack;

.field private c:Landroid/graphics/Path;

.field private d:Landroid/graphics/Paint;

.field private e:F

.field private f:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;II)V
    .registers 9

    .prologue
    const/4 v3, 0x1

    .line 151
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 153
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lnet/hockeyapp/android/f/k;->c:Landroid/graphics/Path;

    .line 154
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lnet/hockeyapp/android/f/k;->a:Ljava/util/Stack;

    .line 155
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lnet/hockeyapp/android/f/k;->d:Landroid/graphics/Paint;

    .line 156
    iget-object v0, p0, Lnet/hockeyapp/android/f/k;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 157
    iget-object v0, p0, Lnet/hockeyapp/android/f/k;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setDither(Z)V

    .line 158
    iget-object v0, p0, Lnet/hockeyapp/android/f/k;->d:Landroid/graphics/Paint;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 159
    iget-object v0, p0, Lnet/hockeyapp/android/f/k;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 160
    iget-object v0, p0, Lnet/hockeyapp/android/f/k;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 161
    iget-object v0, p0, Lnet/hockeyapp/android/f/k;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 162
    iget-object v0, p0, Lnet/hockeyapp/android/f/k;->d:Landroid/graphics/Paint;

    const/high16 v1, 0x41400000    # 12.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 164
    new-instance v0, Lnet/hockeyapp/android/f/l;

    invoke-direct {v0, p0}, Lnet/hockeyapp/android/f/l;-><init>(Lnet/hockeyapp/android/f/k;)V

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    aput-object p2, v1, v3

    const/4 v2, 0x2

    .line 193
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lnet/hockeyapp/android/f/l;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 194
    return-void
.end method

.method public static a(Landroid/content/ContentResolver;Landroid/net/Uri;)I
    .registers 6

    .prologue
    const/4 v0, 0x1

    .line 65
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 66
    iput-boolean v0, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 69
    :try_start_8
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v2

    .line 70
    const/4 v3, 0x0

    invoke-static {v2, v3, v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 73
    iget v2, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v2, v2

    iget v1, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_15} :catch_20

    int-to-float v1, v1

    div-float v1, v2, v1

    .line 74
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1f

    const/4 v0, 0x0

    .line 78
    :cond_1f
    :goto_1f
    return v0

    .line 76
    :catch_20
    move-exception v1

    .line 77
    const-string v2, "HockeyApp"

    const-string v3, "Unable to determine necessary screen orientation."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1f
.end method

.method private static a(Landroid/graphics/BitmapFactory$Options;II)I
    .registers 7

    .prologue
    .line 95
    iget v1, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 96
    iget v2, p0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 97
    const/4 v0, 0x1

    .line 99
    if-gt v1, p2, :cond_9

    if-le v2, p1, :cond_18

    .line 101
    :cond_9
    div-int/lit8 v1, v1, 0x2

    .line 102
    div-int/lit8 v2, v2, 0x2

    .line 106
    :goto_d
    div-int v3, v1, v0

    if-le v3, p2, :cond_18

    div-int v3, v2, v0

    if-le v3, p1, :cond_18

    .line 107
    mul-int/lit8 v0, v0, 0x2

    goto :goto_d

    .line 111
    :cond_18
    return v0
.end method

.method static synthetic a(Landroid/content/ContentResolver;Landroid/net/Uri;II)Landroid/graphics/Bitmap;
    .registers 7

    .prologue
    const/4 v2, 0x0

    .line 2128
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 2129
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 2131
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    .line 2132
    invoke-static {v1, v2, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 2135
    invoke-static {v0, p2, p3}, Lnet/hockeyapp/android/f/k;->a(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v1

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 2138
    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 2139
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    .line 2140
    invoke-static {v1, v2, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 52
    return-object v0
.end method

.method private a()V
    .registers 2

    .prologue
    .line 197
    iget-object v0, p0, Lnet/hockeyapp/android/f/k;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 198
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/k;->invalidate()V

    .line 199
    return-void
.end method

.method private a(FF)V
    .registers 4

    .prologue
    .line 226
    iget-object v0, p0, Lnet/hockeyapp/android/f/k;->c:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 227
    iget-object v0, p0, Lnet/hockeyapp/android/f/k;->c:Landroid/graphics/Path;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 228
    iput p1, p0, Lnet/hockeyapp/android/f/k;->e:F

    .line 229
    iput p2, p0, Lnet/hockeyapp/android/f/k;->f:F

    .line 230
    return-void
.end method

.method private static b(Landroid/content/ContentResolver;Landroid/net/Uri;II)Landroid/graphics/Bitmap;
    .registers 7

    .prologue
    const/4 v2, 0x0

    .line 128
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 129
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 131
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    .line 132
    invoke-static {v1, v2, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 135
    invoke-static {v0, p2, p3}, Lnet/hockeyapp/android/f/k;->a(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v1

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 138
    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 139
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    .line 140
    invoke-static {v1, v2, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 142
    return-object v0
.end method

.method private b()V
    .registers 2

    .prologue
    .line 202
    iget-object v0, p0, Lnet/hockeyapp/android/f/k;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v0

    if-nez v0, :cond_10

    .line 203
    iget-object v0, p0, Lnet/hockeyapp/android/f/k;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 204
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/k;->invalidate()V

    .line 206
    :cond_10
    return-void
.end method

.method private b(FF)V
    .registers 9

    .prologue
    const/high16 v2, 0x40800000    # 4.0f

    const/high16 v5, 0x40000000    # 2.0f

    .line 233
    iget v0, p0, Lnet/hockeyapp/android/f/k;->e:F

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 234
    iget v1, p0, Lnet/hockeyapp/android/f/k;->f:F

    sub-float v1, p2, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 235
    cmpl-float v0, v0, v2

    if-gez v0, :cond_1c

    cmpl-float v0, v1, v2

    if-ltz v0, :cond_31

    .line 236
    :cond_1c
    iget-object v0, p0, Lnet/hockeyapp/android/f/k;->c:Landroid/graphics/Path;

    iget v1, p0, Lnet/hockeyapp/android/f/k;->e:F

    iget v2, p0, Lnet/hockeyapp/android/f/k;->f:F

    iget v3, p0, Lnet/hockeyapp/android/f/k;->e:F

    add-float/2addr v3, p1

    div-float/2addr v3, v5

    iget v4, p0, Lnet/hockeyapp/android/f/k;->f:F

    add-float/2addr v4, p2

    div-float/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 237
    iput p1, p0, Lnet/hockeyapp/android/f/k;->e:F

    .line 238
    iput p2, p0, Lnet/hockeyapp/android/f/k;->f:F

    .line 240
    :cond_31
    return-void
.end method

.method private c()Z
    .registers 2

    .prologue
    .line 209
    iget-object v0, p0, Lnet/hockeyapp/android/f/k;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v0

    return v0
.end method

.method private d()V
    .registers 4

    .prologue
    .line 243
    iget-object v0, p0, Lnet/hockeyapp/android/f/k;->c:Landroid/graphics/Path;

    iget v1, p0, Lnet/hockeyapp/android/f/k;->e:F

    iget v2, p0, Lnet/hockeyapp/android/f/k;->f:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 244
    iget-object v0, p0, Lnet/hockeyapp/android/f/k;->a:Ljava/util/Stack;

    iget-object v1, p0, Lnet/hockeyapp/android/f/k;->c:Landroid/graphics/Path;

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lnet/hockeyapp/android/f/k;->c:Landroid/graphics/Path;

    .line 246
    return-void
.end method


# virtual methods
.method protected final onDraw(Landroid/graphics/Canvas;)V
    .registers 5

    .prologue
    .line 214
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 217
    iget-object v0, p0, Lnet/hockeyapp/android/f/k;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Path;

    .line 218
    iget-object v2, p0, Lnet/hockeyapp/android/f/k;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_9

    .line 222
    :cond_1b
    iget-object v0, p0, Lnet/hockeyapp/android/f/k;->c:Landroid/graphics/Path;

    iget-object v1, p0, Lnet/hockeyapp/android/f/k;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 223
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 10

    .prologue
    const/high16 v4, 0x40800000    # 4.0f

    const/high16 v7, 0x40000000    # 2.0f

    .line 250
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 251
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 253
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_74

    .line 267
    :goto_13
    const/4 v0, 0x1

    return v0

    .line 1226
    :pswitch_15
    iget-object v2, p0, Lnet/hockeyapp/android/f/k;->c:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    .line 1227
    iget-object v2, p0, Lnet/hockeyapp/android/f/k;->c:Landroid/graphics/Path;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1228
    iput v0, p0, Lnet/hockeyapp/android/f/k;->e:F

    .line 1229
    iput v1, p0, Lnet/hockeyapp/android/f/k;->f:F

    .line 256
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/k;->invalidate()V

    goto :goto_13

    .line 1233
    :pswitch_27
    iget v2, p0, Lnet/hockeyapp/android/f/k;->e:F

    sub-float v2, v0, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 1234
    iget v3, p0, Lnet/hockeyapp/android/f/k;->f:F

    sub-float v3, v1, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 1235
    cmpl-float v2, v2, v4

    if-gez v2, :cond_3f

    cmpl-float v2, v3, v4

    if-ltz v2, :cond_54

    .line 1236
    :cond_3f
    iget-object v2, p0, Lnet/hockeyapp/android/f/k;->c:Landroid/graphics/Path;

    iget v3, p0, Lnet/hockeyapp/android/f/k;->e:F

    iget v4, p0, Lnet/hockeyapp/android/f/k;->f:F

    iget v5, p0, Lnet/hockeyapp/android/f/k;->e:F

    add-float/2addr v5, v0

    div-float/2addr v5, v7

    iget v6, p0, Lnet/hockeyapp/android/f/k;->f:F

    add-float/2addr v6, v1

    div-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 1237
    iput v0, p0, Lnet/hockeyapp/android/f/k;->e:F

    .line 1238
    iput v1, p0, Lnet/hockeyapp/android/f/k;->f:F

    .line 260
    :cond_54
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/k;->invalidate()V

    goto :goto_13

    .line 1243
    :pswitch_58
    iget-object v0, p0, Lnet/hockeyapp/android/f/k;->c:Landroid/graphics/Path;

    iget v1, p0, Lnet/hockeyapp/android/f/k;->e:F

    iget v2, p0, Lnet/hockeyapp/android/f/k;->f:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1244
    iget-object v0, p0, Lnet/hockeyapp/android/f/k;->a:Ljava/util/Stack;

    iget-object v1, p0, Lnet/hockeyapp/android/f/k;->c:Landroid/graphics/Path;

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1245
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lnet/hockeyapp/android/f/k;->c:Landroid/graphics/Path;

    .line 264
    invoke-virtual {p0}, Lnet/hockeyapp/android/f/k;->invalidate()V

    goto :goto_13

    .line 253
    nop

    :pswitch_data_74
    .packed-switch 0x0
        :pswitch_15
        :pswitch_58
        :pswitch_27
    .end packed-switch
.end method
