.class final Lnet/hockeyapp/android/m;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lnet/hockeyapp/android/FeedbackActivity;


# direct methods
.method constructor <init>(Lnet/hockeyapp/android/FeedbackActivity;)V
    .registers 2

    .prologue
    .line 572
    iput-object p1, p0, Lnet/hockeyapp/android/m;->a:Lnet/hockeyapp/android/FeedbackActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .registers 10

    .prologue
    const/16 v7, 0x40c

    const/4 v1, 0x1

    .line 575
    const/4 v0, 0x0

    .line 576
    iget-object v2, p0, Lnet/hockeyapp/android/m;->a:Lnet/hockeyapp/android/FeedbackActivity;

    new-instance v3, Lnet/hockeyapp/android/c/c;

    invoke-direct {v3}, Lnet/hockeyapp/android/c/c;-><init>()V

    invoke-static {v2, v3}, Lnet/hockeyapp/android/FeedbackActivity;->a(Lnet/hockeyapp/android/FeedbackActivity;Lnet/hockeyapp/android/c/c;)Lnet/hockeyapp/android/c/c;

    .line 578
    if-eqz p1, :cond_90

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_90

    .line 579
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    .line 580
    const-string v3, "feedback_response"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 581
    const-string v4, "feedback_status"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 582
    const-string v5, "request_type"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 583
    const-string v5, "send"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_57

    if-eqz v3, :cond_3e

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    const/16 v6, 0xc9

    if-eq v5, v6, :cond_57

    .line 585
    :cond_3e
    iget-object v1, p0, Lnet/hockeyapp/android/m;->a:Lnet/hockeyapp/android/FeedbackActivity;

    invoke-static {v1}, Lnet/hockeyapp/android/FeedbackActivity;->a(Lnet/hockeyapp/android/FeedbackActivity;)Lnet/hockeyapp/android/c/c;

    move-result-object v1

    invoke-static {v7}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 1058
    iput-object v2, v1, Lnet/hockeyapp/android/c/c;->a:Ljava/lang/String;

    .line 604
    :goto_4a
    if-nez v0, :cond_56

    .line 605
    iget-object v0, p0, Lnet/hockeyapp/android/m;->a:Lnet/hockeyapp/android/FeedbackActivity;

    new-instance v1, Lnet/hockeyapp/android/n;

    invoke-direct {v1, p0}, Lnet/hockeyapp/android/n;-><init>(Lnet/hockeyapp/android/m;)V

    invoke-virtual {v0, v1}, Lnet/hockeyapp/android/FeedbackActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 617
    :cond_56
    return-void

    .line 587
    :cond_57
    const-string v5, "fetch"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_78

    if-eqz v4, :cond_78

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    const/16 v6, 0x194

    if-eq v5, v6, :cond_71

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/16 v5, 0x1a6

    if-ne v4, v5, :cond_78

    .line 589
    :cond_71
    iget-object v0, p0, Lnet/hockeyapp/android/m;->a:Lnet/hockeyapp/android/FeedbackActivity;

    invoke-static {v0}, Lnet/hockeyapp/android/FeedbackActivity;->b(Lnet/hockeyapp/android/FeedbackActivity;)V

    move v0, v1

    .line 590
    goto :goto_4a

    .line 592
    :cond_78
    if-eqz v3, :cond_81

    .line 593
    iget-object v0, p0, Lnet/hockeyapp/android/m;->a:Lnet/hockeyapp/android/FeedbackActivity;

    invoke-static {v0, v3, v2}, Lnet/hockeyapp/android/FeedbackActivity;->a(Lnet/hockeyapp/android/FeedbackActivity;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 594
    goto :goto_4a

    .line 597
    :cond_81
    iget-object v1, p0, Lnet/hockeyapp/android/m;->a:Lnet/hockeyapp/android/FeedbackActivity;

    invoke-static {v1}, Lnet/hockeyapp/android/FeedbackActivity;->a(Lnet/hockeyapp/android/FeedbackActivity;)Lnet/hockeyapp/android/c/c;

    move-result-object v1

    const/16 v2, 0x40d

    invoke-static {v2}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 2058
    iput-object v2, v1, Lnet/hockeyapp/android/c/c;->a:Ljava/lang/String;

    goto :goto_4a

    .line 601
    :cond_90
    iget-object v1, p0, Lnet/hockeyapp/android/m;->a:Lnet/hockeyapp/android/FeedbackActivity;

    invoke-static {v1}, Lnet/hockeyapp/android/FeedbackActivity;->a(Lnet/hockeyapp/android/FeedbackActivity;)Lnet/hockeyapp/android/c/c;

    move-result-object v1

    invoke-static {v7}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 3058
    iput-object v2, v1, Lnet/hockeyapp/android/c/c;->a:Ljava/lang/String;

    goto :goto_4a
.end method
