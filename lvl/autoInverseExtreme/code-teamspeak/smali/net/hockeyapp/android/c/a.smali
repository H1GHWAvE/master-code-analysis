.class public final enum Lnet/hockeyapp/android/c/a;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lnet/hockeyapp/android/c/a;

.field public static final enum b:Lnet/hockeyapp/android/c/a;

.field public static final enum c:Lnet/hockeyapp/android/c/a;

.field private static final synthetic e:[Lnet/hockeyapp/android/c/a;


# instance fields
.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 11
    new-instance v0, Lnet/hockeyapp/android/c/a;

    const-string v1, "CrashManagerUserInputDontSend"

    invoke-direct {v0, v1, v2, v2}, Lnet/hockeyapp/android/c/a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lnet/hockeyapp/android/c/a;->a:Lnet/hockeyapp/android/c/a;

    .line 15
    new-instance v0, Lnet/hockeyapp/android/c/a;

    const-string v1, "CrashManagerUserInputSend"

    invoke-direct {v0, v1, v3, v3}, Lnet/hockeyapp/android/c/a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lnet/hockeyapp/android/c/a;->b:Lnet/hockeyapp/android/c/a;

    .line 19
    new-instance v0, Lnet/hockeyapp/android/c/a;

    const-string v1, "CrashManagerUserInputAlwaysSend"

    invoke-direct {v0, v1, v4, v4}, Lnet/hockeyapp/android/c/a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lnet/hockeyapp/android/c/a;->c:Lnet/hockeyapp/android/c/a;

    .line 7
    const/4 v0, 0x3

    new-array v0, v0, [Lnet/hockeyapp/android/c/a;

    sget-object v1, Lnet/hockeyapp/android/c/a;->a:Lnet/hockeyapp/android/c/a;

    aput-object v1, v0, v2

    sget-object v1, Lnet/hockeyapp/android/c/a;->b:Lnet/hockeyapp/android/c/a;

    aput-object v1, v0, v3

    sget-object v1, Lnet/hockeyapp/android/c/a;->c:Lnet/hockeyapp/android/c/a;

    aput-object v1, v0, v4

    sput-object v0, Lnet/hockeyapp/android/c/a;->e:[Lnet/hockeyapp/android/c/a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 24
    iput p3, p0, Lnet/hockeyapp/android/c/a;->d:I

    .line 25
    return-void
.end method

.method private a()I
    .registers 2

    .prologue
    .line 28
    iget v0, p0, Lnet/hockeyapp/android/c/a;->d:I

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lnet/hockeyapp/android/c/a;
    .registers 2

    .prologue
    .line 7
    const-class v0, Lnet/hockeyapp/android/c/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lnet/hockeyapp/android/c/a;

    return-object v0
.end method

.method public static values()[Lnet/hockeyapp/android/c/a;
    .registers 1

    .prologue
    .line 7
    sget-object v0, Lnet/hockeyapp/android/c/a;->e:[Lnet/hockeyapp/android/c/a;

    invoke-virtual {v0}, [Lnet/hockeyapp/android/c/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lnet/hockeyapp/android/c/a;

    return-object v0
.end method
