.class public Lnet/hockeyapp/android/al;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lnet/hockeyapp/android/as;
.implements Lnet/hockeyapp/android/ax;


# instance fields
.field protected a:Lnet/hockeyapp/android/d/l;

.field protected b:Lnet/hockeyapp/android/e/y;

.field private final c:I

.field private d:Lnet/hockeyapp/android/c/c;

.field private e:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 65
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 66
    const/4 v0, 0x0

    iput v0, p0, Lnet/hockeyapp/android/al;->c:I

    return-void
.end method

.method static synthetic a(Lnet/hockeyapp/android/al;)Lnet/hockeyapp/android/c/c;
    .registers 2

    .prologue
    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/hockeyapp/android/al;->d:Lnet/hockeyapp/android/c/c;

    return-object v0
.end method

.method private a(Ljava/lang/String;)V
    .registers 4

    .prologue
    .line 227
    new-instance v0, Lnet/hockeyapp/android/ao;

    invoke-direct {v0, p0}, Lnet/hockeyapp/android/ao;-><init>(Lnet/hockeyapp/android/al;)V

    .line 3255
    new-instance v1, Lnet/hockeyapp/android/d/l;

    invoke-direct {v1, p0, p1, v0}, Lnet/hockeyapp/android/d/l;-><init>(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/b/a;)V

    iput-object v1, p0, Lnet/hockeyapp/android/al;->a:Lnet/hockeyapp/android/d/l;

    .line 251
    iget-object v0, p0, Lnet/hockeyapp/android/al;->a:Lnet/hockeyapp/android/d/l;

    invoke-static {v0}, Lnet/hockeyapp/android/e/a;->a(Landroid/os/AsyncTask;)V

    .line 252
    return-void
.end method

.method private a(Ljava/lang/String;Lnet/hockeyapp/android/b/a;)V
    .registers 4

    .prologue
    .line 255
    new-instance v0, Lnet/hockeyapp/android/d/l;

    invoke-direct {v0, p0, p1, p2}, Lnet/hockeyapp/android/d/l;-><init>(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/b/a;)V

    iput-object v0, p0, Lnet/hockeyapp/android/al;->a:Lnet/hockeyapp/android/d/l;

    .line 256
    return-void
.end method

.method private static a(Landroid/content/Context;)Z
    .registers 2

    .prologue
    .line 314
    const-string v0, "android.permission.WRITE_EXTERNAL_STORAGE"

    .line 315
    invoke-virtual {p0, v0}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    .line 317
    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method private e()V
    .registers 11

    .prologue
    const/4 v9, 0x1

    .line 110
    const/16 v0, 0x1002

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/al;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 111
    invoke-direct {p0}, Lnet/hockeyapp/android/al;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    const/16 v0, 0x1003

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/al;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 114
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Version "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lnet/hockeyapp/android/al;->b:Lnet/hockeyapp/android/e/y;

    invoke-virtual {v2}, Lnet/hockeyapp/android/e/y;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 115
    iget-object v1, p0, Lnet/hockeyapp/android/al;->b:Lnet/hockeyapp/android/e/y;

    invoke-virtual {v1}, Lnet/hockeyapp/android/e/y;->b()Ljava/lang/String;

    move-result-object v3

    .line 117
    const-string v1, "Unknown size"

    .line 118
    iget-object v4, p0, Lnet/hockeyapp/android/al;->b:Lnet/hockeyapp/android/e/y;

    invoke-virtual {v4}, Lnet/hockeyapp/android/e/y;->c()J

    move-result-wide v4

    .line 119
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-ltz v6, :cond_b5

    .line 120
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "%.2f"

    new-array v7, v9, [Ljava/lang/Object;

    const/4 v8, 0x0

    long-to-float v4, v4

    const/high16 v5, 0x49800000    # 1048576.0f

    div-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " MB"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 135
    :goto_67
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    const/16 v0, 0x1004

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/al;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 138
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 140
    const/16 v0, 0x1005

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/al;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 141
    invoke-virtual {v0, v9}, Landroid/webkit/WebView;->clearCache(Z)V

    .line 142
    invoke-virtual {v0}, Landroid/webkit/WebView;->destroyDrawingCache()V

    .line 143
    const-string v1, "https://sdk.hockeyapp.net/"

    .line 2152
    iget-object v2, p0, Lnet/hockeyapp/android/al;->b:Lnet/hockeyapp/android/e/y;

    invoke-virtual {v2}, Lnet/hockeyapp/android/e/y;->d()Ljava/lang/String;

    move-result-object v2

    .line 143
    const-string v3, "text/html"

    const-string v4, "utf-8"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    return-void

    .line 123
    :cond_b5
    new-instance v4, Lnet/hockeyapp/android/d/o;

    invoke-virtual {p0}, Lnet/hockeyapp/android/al;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "url"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lnet/hockeyapp/android/am;

    invoke-direct {v6, p0, v0, v2, v3}, Lnet/hockeyapp/android/am;-><init>(Lnet/hockeyapp/android/al;Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v4, p0, v5, v6}, Lnet/hockeyapp/android/d/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/b/a;)V

    .line 133
    invoke-static {v4}, Lnet/hockeyapp/android/e/a;->a(Landroid/os/AsyncTask;)V

    goto :goto_67
.end method

.method private f()Ljava/lang/String;
    .registers 2

    .prologue
    .line 152
    iget-object v0, p0, Lnet/hockeyapp/android/al;->b:Lnet/hockeyapp/android/e/y;

    invoke-virtual {v0}, Lnet/hockeyapp/android/e/y;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private g()Landroid/view/ViewGroup;
    .registers 2

    .prologue
    .line 289
    new-instance v0, Lnet/hockeyapp/android/f/m;

    invoke-direct {v0, p0}, Lnet/hockeyapp/android/f/m;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private h()Ljava/lang/String;
    .registers 4

    .prologue
    .line 299
    :try_start_0
    invoke-virtual {p0}, Lnet/hockeyapp/android/al;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 300
    invoke-virtual {p0}, Lnet/hockeyapp/android/al;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 301
    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
    :try_end_14
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_14} :catch_16

    move-result-object v0

    .line 304
    :goto_15
    return-object v0

    :catch_16
    move-exception v0

    const-string v0, ""

    goto :goto_15
.end method

.method private i()Z
    .registers 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 328
    :try_start_2
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_1d

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-ge v2, v3, :cond_1d

    .line 329
    invoke-virtual {p0}, Lnet/hockeyapp/android/al;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "install_non_market_apps"

    invoke-static {v2, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v2

    if-ne v2, v0, :cond_1b

    .line 335
    :cond_1a
    :goto_1a
    return v0

    :cond_1b
    move v0, v1

    .line 329
    goto :goto_1a

    .line 332
    :cond_1d
    invoke-virtual {p0}, Lnet/hockeyapp/android/al;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "install_non_market_apps"

    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_26
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_2 .. :try_end_26} :catch_2b

    move-result v2

    if-eq v2, v0, :cond_1a

    move v0, v1

    goto :goto_1a

    .line 335
    :catch_2b
    move-exception v1

    goto :goto_1a
.end method


# virtual methods
.method protected final a()V
    .registers 4

    .prologue
    .line 176
    invoke-virtual {p0}, Lnet/hockeyapp/android/al;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "url"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2227
    new-instance v1, Lnet/hockeyapp/android/ao;

    invoke-direct {v1, p0}, Lnet/hockeyapp/android/ao;-><init>(Lnet/hockeyapp/android/al;)V

    .line 2255
    new-instance v2, Lnet/hockeyapp/android/d/l;

    invoke-direct {v2, p0, v0, v1}, Lnet/hockeyapp/android/d/l;-><init>(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/b/a;)V

    iput-object v2, p0, Lnet/hockeyapp/android/al;->a:Lnet/hockeyapp/android/d/l;

    .line 2251
    iget-object v0, p0, Lnet/hockeyapp/android/al;->a:Lnet/hockeyapp/android/d/l;

    invoke-static {v0}, Lnet/hockeyapp/android/e/a;->a(Landroid/os/AsyncTask;)V

    .line 178
    return-void
.end method

.method public final b()V
    .registers 3

    .prologue
    .line 262
    const/16 v0, 0x1004

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/al;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 263
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 264
    return-void
.end method

.method protected final c()V
    .registers 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 349
    iget-object v0, p0, Lnet/hockeyapp/android/al;->e:Landroid/content/Context;

    .line 3314
    const-string v3, "android.permission.WRITE_EXTERNAL_STORAGE"

    .line 3315
    invoke-virtual {v0, v3}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    .line 3317
    if-nez v0, :cond_1f

    move v0, v1

    .line 349
    :goto_d
    if-nez v0, :cond_37

    .line 351
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x17

    if-lt v0, v3, :cond_21

    .line 353
    new-array v0, v1, [Ljava/lang/String;

    const-string v3, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v3, v0, v2

    invoke-virtual {p0, v0, v1}, Lnet/hockeyapp/android/al;->requestPermissions([Ljava/lang/String;I)V

    .line 387
    :goto_1e
    return-void

    :cond_1f
    move v0, v2

    .line 3317
    goto :goto_d

    .line 357
    :cond_21
    new-instance v0, Lnet/hockeyapp/android/c/c;

    invoke-direct {v0}, Lnet/hockeyapp/android/c/c;-><init>()V

    iput-object v0, p0, Lnet/hockeyapp/android/al;->d:Lnet/hockeyapp/android/c/c;

    .line 358
    iget-object v0, p0, Lnet/hockeyapp/android/al;->d:Lnet/hockeyapp/android/c/c;

    const-string v1, "The permission to access the external storage permission is not set. Please contact the developer."

    .line 4058
    iput-object v1, v0, Lnet/hockeyapp/android/c/c;->a:Ljava/lang/String;

    .line 360
    new-instance v0, Lnet/hockeyapp/android/ap;

    invoke-direct {v0, p0}, Lnet/hockeyapp/android/ap;-><init>(Lnet/hockeyapp/android/al;)V

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/al;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_1e

    .line 371
    :cond_37
    invoke-direct {p0}, Lnet/hockeyapp/android/al;->i()Z

    move-result v0

    if-nez v0, :cond_53

    .line 372
    new-instance v0, Lnet/hockeyapp/android/c/c;

    invoke-direct {v0}, Lnet/hockeyapp/android/c/c;-><init>()V

    iput-object v0, p0, Lnet/hockeyapp/android/al;->d:Lnet/hockeyapp/android/c/c;

    .line 373
    iget-object v0, p0, Lnet/hockeyapp/android/al;->d:Lnet/hockeyapp/android/c/c;

    const-string v1, "The installation from unknown sources is not enabled. Please check the device settings."

    .line 5058
    iput-object v1, v0, Lnet/hockeyapp/android/c/c;->a:Ljava/lang/String;

    .line 375
    new-instance v0, Lnet/hockeyapp/android/aq;

    invoke-direct {v0, p0}, Lnet/hockeyapp/android/aq;-><init>(Lnet/hockeyapp/android/al;)V

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/al;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_1e

    .line 386
    :cond_53
    invoke-virtual {p0}, Lnet/hockeyapp/android/al;->a()V

    goto :goto_1e
.end method

.method public final synthetic d()Landroid/view/View;
    .registers 2

    .prologue
    .line 65
    invoke-direct {p0}, Lnet/hockeyapp/android/al;->g()Landroid/view/ViewGroup;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentVersionCode()I
    .registers 5

    .prologue
    .line 272
    const/4 v0, -0x1

    .line 275
    :try_start_1
    invoke-virtual {p0}, Lnet/hockeyapp/android/al;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Lnet/hockeyapp/android/al;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x80

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget v0, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_11
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_11} :catch_12

    .line 280
    :goto_11
    return v0

    :catch_12
    move-exception v1

    goto :goto_11
.end method

.method public onClick(Landroid/view/View;)V
    .registers 3

    .prologue
    .line 344
    invoke-virtual {p0}, Lnet/hockeyapp/android/al;->c()V

    .line 345
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 346
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 12

    .prologue
    const/4 v9, 0x1

    .line 90
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 92
    const-string v0, "App Update"

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/al;->setTitle(Ljava/lang/CharSequence;)V

    .line 93
    invoke-direct {p0}, Lnet/hockeyapp/android/al;->g()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/al;->setContentView(Landroid/view/View;)V

    .line 95
    iput-object p0, p0, Lnet/hockeyapp/android/al;->e:Landroid/content/Context;

    .line 96
    new-instance v0, Lnet/hockeyapp/android/e/y;

    invoke-virtual {p0}, Lnet/hockeyapp/android/al;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "json"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1, p0}, Lnet/hockeyapp/android/e/y;-><init>(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/ax;)V

    iput-object v0, p0, Lnet/hockeyapp/android/al;->b:Lnet/hockeyapp/android/e/y;

    .line 1110
    const/16 v0, 0x1002

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/al;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1111
    invoke-direct {p0}, Lnet/hockeyapp/android/al;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1113
    const/16 v0, 0x1003

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/al;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1114
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Version "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lnet/hockeyapp/android/al;->b:Lnet/hockeyapp/android/e/y;

    invoke-virtual {v2}, Lnet/hockeyapp/android/e/y;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1115
    iget-object v1, p0, Lnet/hockeyapp/android/al;->b:Lnet/hockeyapp/android/e/y;

    invoke-virtual {v1}, Lnet/hockeyapp/android/e/y;->b()Ljava/lang/String;

    move-result-object v3

    .line 1117
    const-string v1, "Unknown size"

    .line 1118
    iget-object v4, p0, Lnet/hockeyapp/android/al;->b:Lnet/hockeyapp/android/e/y;

    invoke-virtual {v4}, Lnet/hockeyapp/android/e/y;->c()J

    move-result-wide v4

    .line 1119
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-ltz v6, :cond_e8

    .line 1120
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "%.2f"

    new-array v7, v9, [Ljava/lang/Object;

    const/4 v8, 0x0

    long-to-float v4, v4

    const/high16 v5, 0x49800000    # 1048576.0f

    div-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " MB"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1135
    :goto_89
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1137
    const/16 v0, 0x1004

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/al;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 1138
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1140
    const/16 v0, 0x1005

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/al;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 1141
    invoke-virtual {v0, v9}, Landroid/webkit/WebView;->clearCache(Z)V

    .line 1142
    invoke-virtual {v0}, Landroid/webkit/WebView;->destroyDrawingCache()V

    .line 1143
    const-string v1, "https://sdk.hockeyapp.net/"

    .line 1152
    iget-object v2, p0, Lnet/hockeyapp/android/al;->b:Lnet/hockeyapp/android/e/y;

    invoke-virtual {v2}, Lnet/hockeyapp/android/e/y;->d()Ljava/lang/String;

    move-result-object v2

    .line 1143
    const-string v3, "text/html"

    const-string v4, "utf-8"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    invoke-virtual {p0}, Lnet/hockeyapp/android/al;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/hockeyapp/android/d/l;

    iput-object v0, p0, Lnet/hockeyapp/android/al;->a:Lnet/hockeyapp/android/d/l;

    .line 100
    iget-object v0, p0, Lnet/hockeyapp/android/al;->a:Lnet/hockeyapp/android/d/l;

    if-eqz v0, :cond_e7

    .line 101
    iget-object v0, p0, Lnet/hockeyapp/android/al;->a:Lnet/hockeyapp/android/d/l;

    invoke-virtual {v0, p0}, Lnet/hockeyapp/android/d/l;->a(Landroid/content/Context;)V

    .line 103
    :cond_e7
    return-void

    .line 1123
    :cond_e8
    new-instance v4, Lnet/hockeyapp/android/d/o;

    invoke-virtual {p0}, Lnet/hockeyapp/android/al;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "url"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lnet/hockeyapp/android/am;

    invoke-direct {v6, p0, v0, v2, v3}, Lnet/hockeyapp/android/am;-><init>(Lnet/hockeyapp/android/al;Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v4, p0, v5, v6}, Lnet/hockeyapp/android/d/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/b/a;)V

    .line 1133
    invoke-static {v4}, Lnet/hockeyapp/android/e/a;->a(Landroid/os/AsyncTask;)V

    goto :goto_89
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .registers 3

    .prologue
    .line 391
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lnet/hockeyapp/android/al;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .registers 6

    .prologue
    .line 396
    packed-switch p1, :pswitch_data_32

    .line 411
    const/4 v0, 0x0

    :goto_4
    return-object v0

    .line 398
    :pswitch_5
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "An error has occured"

    .line 399
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 400
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Error"

    .line 401
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1080027

    .line 402
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "OK"

    new-instance v2, Lnet/hockeyapp/android/ar;

    invoke-direct {v2, p0}, Lnet/hockeyapp/android/ar;-><init>(Lnet/hockeyapp/android/al;)V

    .line 403
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 408
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_4

    .line 396
    :pswitch_data_32
    .packed-switch 0x0
        :pswitch_5
    .end packed-switch
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .registers 4

    .prologue
    .line 416
    packed-switch p1, :pswitch_data_18

    .line 430
    :goto_3
    return-void

    .line 418
    :pswitch_4
    check-cast p2, Landroid/app/AlertDialog;

    .line 419
    iget-object v0, p0, Lnet/hockeyapp/android/al;->d:Lnet/hockeyapp/android/c/c;

    if-eqz v0, :cond_12

    .line 421
    iget-object v0, p0, Lnet/hockeyapp/android/al;->d:Lnet/hockeyapp/android/c/c;

    .line 6054
    iget-object v0, v0, Lnet/hockeyapp/android/c/c;->a:Ljava/lang/String;

    .line 421
    invoke-virtual {p2, v0}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 425
    :cond_12
    const-string v0, "An unknown error has occured."

    invoke-virtual {p2, v0}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 416
    :pswitch_data_18
    .packed-switch 0x0
        :pswitch_4
    .end packed-switch
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .registers 7

    .prologue
    .line 183
    invoke-virtual {p0}, Lnet/hockeyapp/android/al;->b()V

    .line 185
    array-length v0, p2

    if-eqz v0, :cond_9

    array-length v0, p3

    if-nez v0, :cond_a

    .line 218
    :cond_9
    :goto_9
    return-void

    .line 190
    :cond_a
    const/4 v0, 0x1

    if-ne p1, v0, :cond_9

    .line 192
    const/4 v0, 0x0

    aget v0, p3, v0

    if-nez v0, :cond_16

    .line 194
    invoke-virtual {p0}, Lnet/hockeyapp/android/al;->c()V

    goto :goto_9

    .line 197
    :cond_16
    const-string v0, "HockeyApp"

    const-string v1, "User denied write permission, can\'t continue with updater task."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    invoke-static {}, Lnet/hockeyapp/android/ay;->a()Lnet/hockeyapp/android/az;

    move-result-object v0

    .line 200
    if-nez v0, :cond_9

    .line 204
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lnet/hockeyapp/android/al;->e:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/16 v1, 0x700

    .line 205
    invoke-static {v1}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/16 v1, 0x701

    .line 206
    invoke-static {v1}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/16 v1, 0x702

    .line 207
    invoke-static {v1}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/16 v1, 0x703

    .line 208
    invoke-static {v1}, Lnet/hockeyapp/android/aj;->a(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lnet/hockeyapp/android/an;

    invoke-direct {v2, p0, p0}, Lnet/hockeyapp/android/an;-><init>(Lnet/hockeyapp/android/al;Lnet/hockeyapp/android/al;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 213
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 214
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_9
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 164
    iget-object v0, p0, Lnet/hockeyapp/android/al;->a:Lnet/hockeyapp/android/d/l;

    if-eqz v0, :cond_9

    .line 165
    iget-object v0, p0, Lnet/hockeyapp/android/al;->a:Lnet/hockeyapp/android/d/l;

    invoke-virtual {v0}, Lnet/hockeyapp/android/d/l;->a()V

    .line 167
    :cond_9
    iget-object v0, p0, Lnet/hockeyapp/android/al;->a:Lnet/hockeyapp/android/d/l;

    return-object v0
.end method
