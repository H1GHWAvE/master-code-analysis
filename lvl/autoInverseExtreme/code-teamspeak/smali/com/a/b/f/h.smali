.class public Lcom/a/b/f/h;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# static fields
.field private static final a:Lcom/a/b/c/an;


# instance fields
.field private final b:Lcom/a/b/d/aac;

.field private final c:Ljava/util/concurrent/locks/ReadWriteLock;

.field private final d:Lcom/a/b/f/r;

.field private final e:Ljava/lang/ThreadLocal;

.field private final f:Ljava/lang/ThreadLocal;

.field private g:Lcom/a/b/f/q;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 121
    invoke-static {}, Lcom/a/b/c/f;->a()Lcom/a/b/c/f;

    move-result-object v0

    .line 1518
    sget-object v1, Lcom/a/b/c/bw;->c:Lcom/a/b/c/bw;

    invoke-virtual {v0, v1}, Lcom/a/b/c/f;->a(Lcom/a/b/c/bw;)Lcom/a/b/c/f;

    move-result-object v0

    .line 121
    new-instance v1, Lcom/a/b/f/i;

    invoke-direct {v1}, Lcom/a/b/f/i;-><init>()V

    invoke-virtual {v0, v1}, Lcom/a/b/c/f;->a(Lcom/a/b/c/ab;)Lcom/a/b/c/an;

    move-result-object v0

    sput-object v0, Lcom/a/b/f/h;->a:Lcom/a/b/c/an;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 171
    const-string v0, "default"

    invoke-direct {p0, v0}, Lcom/a/b/f/h;-><init>(Ljava/lang/String;)V

    .line 172
    return-void
.end method

.method public constructor <init>(Lcom/a/b/f/q;)V
    .registers 3

    .prologue
    .line 190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 138
    invoke-static {}, Lcom/a/b/d/io;->v()Lcom/a/b/d/io;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/f/h;->b:Lcom/a/b/d/aac;

    .line 140
    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>()V

    iput-object v0, p0, Lcom/a/b/f/h;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    .line 147
    new-instance v0, Lcom/a/b/f/b;

    invoke-direct {v0}, Lcom/a/b/f/b;-><init>()V

    iput-object v0, p0, Lcom/a/b/f/h;->d:Lcom/a/b/f/r;

    .line 150
    new-instance v0, Lcom/a/b/f/j;

    invoke-direct {v0, p0}, Lcom/a/b/f/j;-><init>(Lcom/a/b/f/h;)V

    iput-object v0, p0, Lcom/a/b/f/h;->e:Ljava/lang/ThreadLocal;

    .line 158
    new-instance v0, Lcom/a/b/f/k;

    invoke-direct {v0, p0}, Lcom/a/b/f/k;-><init>(Lcom/a/b/f/h;)V

    iput-object v0, p0, Lcom/a/b/f/h;->f:Ljava/lang/ThreadLocal;

    .line 191
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/f/q;

    iput-object v0, p0, Lcom/a/b/f/h;->g:Lcom/a/b/f/q;

    .line 192
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 181
    new-instance v0, Lcom/a/b/f/m;

    invoke-direct {v0, p1}, Lcom/a/b/f/m;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/a/b/f/h;-><init>(Lcom/a/b/f/q;)V

    .line 182
    return-void
.end method

.method private static a(Ljava/lang/Class;)Ljava/util/Set;
    .registers 2
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 354
    :try_start_0
    sget-object v0, Lcom/a/b/f/h;->a:Lcom/a/b/c/an;

    invoke-interface {v0, p0}, Lcom/a/b/c/an;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;
    :try_end_8
    .catch Lcom/a/b/n/a/gq; {:try_start_0 .. :try_end_8} :catch_9

    return-object v0

    .line 356
    :catch_9
    move-exception v0

    invoke-virtual {v0}, Lcom/a/b/n/a/gq;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/b/ei;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method private a(Ljava/lang/Object;)V
    .registers 4

    .prologue
    .line 203
    iget-object v0, p0, Lcom/a/b/f/h;->d:Lcom/a/b/f/r;

    invoke-interface {v0, p1}, Lcom/a/b/f/r;->a(Ljava/lang/Object;)Lcom/a/b/d/vi;

    move-result-object v0

    .line 205
    iget-object v1, p0, Lcom/a/b/f/h;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 207
    :try_start_f
    iget-object v1, p0, Lcom/a/b/f/h;->b:Lcom/a/b/d/aac;

    invoke-interface {v1, v0}, Lcom/a/b/d/aac;->a(Lcom/a/b/d/vi;)Z
    :try_end_14
    .catchall {:try_start_f .. :try_end_14} :catchall_1e

    .line 209
    iget-object v0, p0, Lcom/a/b/f/h;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 210
    return-void

    .line 209
    :catchall_1e
    move-exception v0

    iget-object v1, p0, Lcom/a/b/f/h;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method private b(Ljava/lang/Object;)V
    .registers 6

    .prologue
    .line 220
    iget-object v0, p0, Lcom/a/b/f/h;->d:Lcom/a/b/f/r;

    invoke-interface {v0, p1}, Lcom/a/b/f/r;->a(Ljava/lang/Object;)Lcom/a/b/d/vi;

    move-result-object v0

    .line 222
    invoke-interface {v0}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_12
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_84

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 223
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Class;

    .line 224
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 226
    iget-object v3, p0, Lcom/a/b/f/h;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 228
    :try_start_33
    iget-object v3, p0, Lcom/a/b/f/h;->b:Lcom/a/b/d/aac;

    invoke-interface {v3, v1}, Lcom/a/b/d/aac;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    .line 229
    invoke-interface {v1, v0}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v3

    if-nez v3, :cond_77

    .line 230
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x41

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "missing event subscriber for an annotated method. Is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " registered?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_6c
    .catchall {:try_start_33 .. :try_end_6c} :catchall_6c

    .line 235
    :catchall_6c
    move-exception v0

    iget-object v1, p0, Lcom/a/b/f/h;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 233
    :cond_77
    :try_start_77
    invoke-interface {v1, v0}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z
    :try_end_7a
    .catchall {:try_start_77 .. :try_end_7a} :catchall_6c

    .line 235
    iget-object v0, p0, Lcom/a/b/f/h;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_12

    .line 238
    :cond_84
    return-void
.end method

.method private c(Ljava/lang/Object;)V
    .registers 5

    .prologue
    .line 252
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/f/h;->a(Ljava/lang/Class;)Ljava/util/Set;

    move-result-object v0

    .line 255
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_56

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 256
    iget-object v2, p0, Lcom/a/b/f/h;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 258
    :try_start_21
    iget-object v2, p0, Lcom/a/b/f/h;->b:Lcom/a/b/d/aac;

    invoke-interface {v2, v0}, Lcom/a/b/d/aac;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    .line 260
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4c

    .line 262
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_31
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/f/n;

    .line 263
    invoke-virtual {p0, p1, v0}, Lcom/a/b/f/h;->a(Ljava/lang/Object;Lcom/a/b/f/n;)V
    :try_end_40
    .catchall {:try_start_21 .. :try_end_40} :catchall_41

    goto :goto_31

    .line 267
    :catchall_41
    move-exception v0

    iget-object v1, p0, Lcom/a/b/f/h;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_4c
    iget-object v0, p0, Lcom/a/b/f/h;->c:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_c

    .line 275
    :cond_56
    invoke-virtual {p0}, Lcom/a/b/f/h;->a()V

    .line 276
    return-void
.end method


# virtual methods
.method a()V
    .registers 4

    .prologue
    .line 295
    iget-object v0, p0, Lcom/a/b/f/h;->f:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 309
    :goto_e
    return-void

    .line 299
    :cond_f
    iget-object v0, p0, Lcom/a/b/f/h;->f:Ljava/lang/ThreadLocal;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 301
    :try_start_19
    iget-object v0, p0, Lcom/a/b/f/h;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    .line 303
    :goto_21
    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/b/f/l;

    if-eqz v1, :cond_3d

    .line 304
    iget-object v2, v1, Lcom/a/b/f/l;->a:Ljava/lang/Object;

    iget-object v1, v1, Lcom/a/b/f/l;->b:Lcom/a/b/f/n;

    invoke-virtual {p0, v2, v1}, Lcom/a/b/f/h;->b(Ljava/lang/Object;Lcom/a/b/f/n;)V
    :try_end_30
    .catchall {:try_start_19 .. :try_end_30} :catchall_31

    goto :goto_21

    .line 307
    :catchall_31
    move-exception v0

    iget-object v1, p0, Lcom/a/b/f/h;->f:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->remove()V

    .line 308
    iget-object v1, p0, Lcom/a/b/f/h;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->remove()V

    throw v0

    .line 307
    :cond_3d
    iget-object v0, p0, Lcom/a/b/f/h;->f:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->remove()V

    .line 308
    iget-object v0, p0, Lcom/a/b/f/h;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->remove()V

    goto :goto_e
.end method

.method a(Ljava/lang/Object;Lcom/a/b/f/n;)V
    .registers 5

    .prologue
    .line 284
    iget-object v0, p0, Lcom/a/b/f/h;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    new-instance v1, Lcom/a/b/f/l;

    invoke-direct {v1, p1, p2}, Lcom/a/b/f/l;-><init>(Ljava/lang/Object;Lcom/a/b/f/n;)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 285
    return-void
.end method

.method b(Ljava/lang/Object;Lcom/a/b/f/n;)V
    .registers 10

    .prologue
    .line 322
    :try_start_0
    invoke-virtual {p2, p1}, Lcom/a/b/f/n;->a(Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_3} :catch_4

    .line 341
    :goto_3
    return-void

    .line 323
    :catch_4
    move-exception v1

    .line 325
    :try_start_5
    iget-object v0, p0, Lcom/a/b/f/h;->g:Lcom/a/b/f/q;

    invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    new-instance v3, Lcom/a/b/f/p;

    .line 1109
    iget-object v4, p2, Lcom/a/b/f/n;->a:Ljava/lang/Object;

    .line 1113
    iget-object v5, p2, Lcom/a/b/f/n;->b:Ljava/lang/reflect/Method;

    .line 325
    invoke-direct {v3, p0, p1, v4, v5}, Lcom/a/b/f/p;-><init>(Lcom/a/b/f/h;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Method;)V

    invoke-interface {v0, v2, v3}, Lcom/a/b/f/q;->a(Ljava/lang/Throwable;Lcom/a/b/f/p;)V
    :try_end_17
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_17} :catch_18

    goto :goto_3

    .line 332
    :catch_18
    move-exception v0

    .line 334
    const-class v2, Lcom/a/b/f/h;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v2

    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v4, "Exception %s thrown while handling exception: %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v6, 0x1

    invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    aput-object v1, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3
.end method
