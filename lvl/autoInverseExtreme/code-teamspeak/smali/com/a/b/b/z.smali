.class final Lcom/a/b/b/z;
.super Lcom/a/b/b/ae;
.source "SourceFile"


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 253
    invoke-direct {p0, p1}, Lcom/a/b/b/ae;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;I)I
    .registers 4

    .prologue
    .line 263
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 264
    invoke-static {p2, v0}, Lcom/a/b/b/cn;->b(II)I

    .line 265
    if-ne p2, v0, :cond_a

    const/4 p2, -0x1

    :cond_a
    return p2
.end method

.method public final a()Lcom/a/b/b/m;
    .registers 2

    .prologue
    .line 323
    sget-object v0, Lcom/a/b/b/z;->m:Lcom/a/b/b/m;

    return-object v0
.end method

.method public final a(Lcom/a/b/b/m;)Lcom/a/b/b/m;
    .registers 3

    .prologue
    .line 314
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/m;

    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;C)Ljava/lang/String;
    .registers 5

    .prologue
    .line 287
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    new-array v0, v0, [C

    .line 288
    invoke-static {v0, p2}, Ljava/util/Arrays;->fill([CC)V

    .line 289
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    return-object v1
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    .registers 6

    .prologue
    .line 293
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    mul-int/2addr v0, v2

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 294
    const/4 v0, 0x0

    :goto_f
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-ge v0, v2, :cond_1b

    .line 295
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 294
    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    .line 297
    :cond_1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/a/b/b/m;)Lcom/a/b/b/m;
    .registers 2

    .prologue
    .line 318
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 319
    return-object p0
.end method

.method public final b(Ljava/lang/CharSequence;C)Ljava/lang/String;
    .registers 4

    .prologue
    .line 301
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_9

    const-string v0, ""

    :goto_8
    return-object v0

    :cond_9
    invoke-static {p2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    goto :goto_8
.end method

.method public final c(C)Z
    .registers 3

    .prologue
    .line 255
    const/4 v0, 0x1

    return v0
.end method

.method public final c(Ljava/lang/CharSequence;)Z
    .registers 3

    .prologue
    .line 273
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    const/4 v0, 0x1

    return v0
.end method

.method public final d(Ljava/lang/CharSequence;)Z
    .registers 3

    .prologue
    .line 278
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final e(Ljava/lang/CharSequence;)I
    .registers 3

    .prologue
    .line 259
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, -0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final f(Ljava/lang/CharSequence;)I
    .registers 3

    .prologue
    .line 269
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final g(Ljava/lang/CharSequence;)I
    .registers 3

    .prologue
    .line 310
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    return v0
.end method

.method public final h(Ljava/lang/CharSequence;)Ljava/lang/String;
    .registers 3

    .prologue
    .line 282
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    const-string v0, ""

    return-object v0
.end method

.method public final i(Ljava/lang/CharSequence;)Ljava/lang/String;
    .registers 3

    .prologue
    .line 305
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 306
    const-string v0, ""

    return-object v0
.end method
