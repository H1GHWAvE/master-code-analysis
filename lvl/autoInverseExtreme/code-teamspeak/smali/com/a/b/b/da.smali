.class abstract enum Lcom/a/b/b/da;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/b/co;


# static fields
.field public static final enum a:Lcom/a/b/b/da;

.field public static final enum b:Lcom/a/b/b/da;

.field public static final enum c:Lcom/a/b/b/da;

.field public static final enum d:Lcom/a/b/b/da;

.field private static final synthetic e:[Lcom/a/b/b/da;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 277
    new-instance v0, Lcom/a/b/b/db;

    const-string v1, "ALWAYS_TRUE"

    invoke-direct {v0, v1}, Lcom/a/b/b/db;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/b/da;->a:Lcom/a/b/b/da;

    .line 286
    new-instance v0, Lcom/a/b/b/dc;

    const-string v1, "ALWAYS_FALSE"

    invoke-direct {v0, v1}, Lcom/a/b/b/dc;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/b/da;->b:Lcom/a/b/b/da;

    .line 295
    new-instance v0, Lcom/a/b/b/dd;

    const-string v1, "IS_NULL"

    invoke-direct {v0, v1}, Lcom/a/b/b/dd;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/b/da;->c:Lcom/a/b/b/da;

    .line 304
    new-instance v0, Lcom/a/b/b/de;

    const-string v1, "NOT_NULL"

    invoke-direct {v0, v1}, Lcom/a/b/b/de;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/b/da;->d:Lcom/a/b/b/da;

    .line 275
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/a/b/b/da;

    const/4 v1, 0x0

    sget-object v2, Lcom/a/b/b/da;->a:Lcom/a/b/b/da;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/a/b/b/da;->b:Lcom/a/b/b/da;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/a/b/b/da;->c:Lcom/a/b/b/da;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/a/b/b/da;->d:Lcom/a/b/b/da;

    aput-object v2, v0, v1

    sput-object v0, Lcom/a/b/b/da;->e:[Lcom/a/b/b/da;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3

    .prologue
    .line 275
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .registers 4

    .prologue
    .line 275
    invoke-direct {p0, p1, p2}, Lcom/a/b/b/da;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private a()Lcom/a/b/b/co;
    .registers 1

    .prologue
    .line 315
    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/b/da;
    .registers 2

    .prologue
    .line 275
    const-class v0, Lcom/a/b/b/da;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/da;

    return-object v0
.end method

.method public static values()[Lcom/a/b/b/da;
    .registers 1

    .prologue
    .line 275
    sget-object v0, Lcom/a/b/b/da;->e:[Lcom/a/b/b/da;

    invoke-virtual {v0}, [Lcom/a/b/b/da;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/b/b/da;

    return-object v0
.end method
