.class final Lcom/a/b/b/bk;
.super Lcom/a/b/b/au;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field private static final a:J


# instance fields
.field private final b:Lcom/a/b/b/bj;

.field private final c:Lcom/a/b/b/au;


# direct methods
.method constructor <init>(Lcom/a/b/b/bj;Lcom/a/b/b/au;)V
    .registers 4

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/a/b/b/au;-><init>()V

    .line 46
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/bj;

    iput-object v0, p0, Lcom/a/b/b/bk;->b:Lcom/a/b/b/bj;

    .line 47
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/au;

    iput-object v0, p0, Lcom/a/b/b/bk;->c:Lcom/a/b/b/au;

    .line 48
    return-void
.end method


# virtual methods
.method protected final b(Ljava/lang/Object;)I
    .registers 4

    .prologue
    .line 55
    iget-object v0, p0, Lcom/a/b/b/bk;->c:Lcom/a/b/b/au;

    iget-object v1, p0, Lcom/a/b/b/bk;->b:Lcom/a/b/b/bj;

    invoke-interface {v1, p1}, Lcom/a/b/b/bj;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/b/au;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method protected final b(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 6

    .prologue
    .line 51
    iget-object v0, p0, Lcom/a/b/b/bk;->c:Lcom/a/b/b/au;

    iget-object v1, p0, Lcom/a/b/b/bk;->b:Lcom/a/b/b/bj;

    invoke-interface {v1, p1}, Lcom/a/b/b/bj;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/a/b/b/bk;->b:Lcom/a/b/b/bj;

    invoke-interface {v2, p2}, Lcom/a/b/b/bj;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/a/b/b/au;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 6
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 59
    if-ne p1, p0, :cond_5

    .line 67
    :cond_4
    :goto_4
    return v0

    .line 62
    :cond_5
    instance-of v2, p1, Lcom/a/b/b/bk;

    if-eqz v2, :cond_21

    .line 63
    check-cast p1, Lcom/a/b/b/bk;

    .line 64
    iget-object v2, p0, Lcom/a/b/b/bk;->b:Lcom/a/b/b/bj;

    iget-object v3, p1, Lcom/a/b/b/bk;->b:Lcom/a/b/b/bj;

    invoke-interface {v2, v3}, Lcom/a/b/b/bj;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1f

    iget-object v2, p0, Lcom/a/b/b/bk;->c:Lcom/a/b/b/au;

    iget-object v3, p1, Lcom/a/b/b/bk;->c:Lcom/a/b/b/au;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_1f
    move v0, v1

    goto :goto_4

    :cond_21
    move v0, v1

    .line 67
    goto :goto_4
.end method

.method public final hashCode()I
    .registers 4

    .prologue
    .line 71
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/a/b/b/bk;->b:Lcom/a/b/b/bj;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/a/b/b/bk;->c:Lcom/a/b/b/au;

    aput-object v2, v0, v1

    .line 1084
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    .line 71
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 6

    .prologue
    .line 75
    iget-object v0, p0, Lcom/a/b/b/bk;->c:Lcom/a/b/b/au;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/b/bk;->b:Lcom/a/b/b/bj;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xd

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".onResultOf("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
