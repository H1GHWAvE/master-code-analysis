.class Lcom/a/b/b/ag;
.super Lcom/a/b/b/m;
.source "SourceFile"


# instance fields
.field final s:Lcom/a/b/b/m;


# direct methods
.method constructor <init>(Lcom/a/b/b/m;)V
    .registers 5

    .prologue
    .line 635
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x9

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".negate()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/a/b/b/ag;-><init>(Ljava/lang/String;Lcom/a/b/b/m;)V

    .line 636
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lcom/a/b/b/m;)V
    .registers 3

    .prologue
    .line 630
    invoke-direct {p0, p1}, Lcom/a/b/b/m;-><init>(Ljava/lang/String;)V

    .line 631
    iput-object p2, p0, Lcom/a/b/b/ag;->s:Lcom/a/b/b/m;

    .line 632
    return-void
.end method


# virtual methods
.method public final a()Lcom/a/b/b/m;
    .registers 2

    .prologue
    .line 664
    iget-object v0, p0, Lcom/a/b/b/ag;->s:Lcom/a/b/b/m;

    return-object v0
.end method

.method a(Ljava/lang/String;)Lcom/a/b/b/m;
    .registers 4

    .prologue
    .line 669
    new-instance v0, Lcom/a/b/b/ag;

    iget-object v1, p0, Lcom/a/b/b/ag;->s:Lcom/a/b/b/m;

    invoke-direct {v0, p1, v1}, Lcom/a/b/b/ag;-><init>(Ljava/lang/String;Lcom/a/b/b/m;)V

    return-object v0
.end method

.method final a(Ljava/util/BitSet;)V
    .registers 5
    .annotation build Lcom/a/b/a/c;
        a = "java.util.BitSet"
    .end annotation

    .prologue
    .line 657
    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    .line 658
    iget-object v1, p0, Lcom/a/b/b/ag;->s:Lcom/a/b/b/m;

    invoke-virtual {v1, v0}, Lcom/a/b/b/m;->a(Ljava/util/BitSet;)V

    .line 659
    const/4 v1, 0x0

    const/high16 v2, 0x10000

    invoke-virtual {v0, v1, v2}, Ljava/util/BitSet;->flip(II)V

    .line 660
    invoke-virtual {p1, v0}, Ljava/util/BitSet;->or(Ljava/util/BitSet;)V

    .line 661
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 626
    check-cast p1, Ljava/lang/Character;

    invoke-super {p0, p1}, Lcom/a/b/b/m;->a(Ljava/lang/Character;)Z

    move-result v0

    return v0
.end method

.method public final c(C)Z
    .registers 3

    .prologue
    .line 639
    iget-object v0, p0, Lcom/a/b/b/ag;->s:Lcom/a/b/b/m;

    invoke-virtual {v0, p1}, Lcom/a/b/b/m;->c(C)Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final c(Ljava/lang/CharSequence;)Z
    .registers 3

    .prologue
    .line 643
    iget-object v0, p0, Lcom/a/b/b/ag;->s:Lcom/a/b/b/m;

    invoke-virtual {v0, p1}, Lcom/a/b/b/m;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public final d(Ljava/lang/CharSequence;)Z
    .registers 3

    .prologue
    .line 647
    iget-object v0, p0, Lcom/a/b/b/ag;->s:Lcom/a/b/b/m;

    invoke-virtual {v0, p1}, Lcom/a/b/b/m;->c(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public final g(Ljava/lang/CharSequence;)I
    .registers 4

    .prologue
    .line 651
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    iget-object v1, p0, Lcom/a/b/b/ag;->s:Lcom/a/b/b/m;

    invoke-virtual {v1, p1}, Lcom/a/b/b/m;->g(Ljava/lang/CharSequence;)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method
