.class final Lcom/a/b/b/l;
.super Lcom/a/b/b/ak;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final c:J


# instance fields
.field private final a:Lcom/a/b/b/f;

.field private final b:Lcom/a/b/b/f;


# direct methods
.method constructor <init>(Lcom/a/b/b/f;Lcom/a/b/b/f;)V
    .registers 4

    .prologue
    .line 168
    invoke-direct {p0}, Lcom/a/b/b/ak;-><init>()V

    .line 169
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/f;

    iput-object v0, p0, Lcom/a/b/b/l;->a:Lcom/a/b/b/f;

    .line 170
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/f;

    iput-object v0, p0, Lcom/a/b/b/l;->b:Lcom/a/b/b/f;

    .line 171
    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .registers 4

    .prologue
    .line 175
    if-nez p1, :cond_4

    const/4 v0, 0x0

    :goto_3
    return-object v0

    :cond_4
    iget-object v0, p0, Lcom/a/b/b/l;->a:Lcom/a/b/b/f;

    iget-object v1, p0, Lcom/a/b/b/l;->b:Lcom/a/b/b/f;

    invoke-virtual {v0, v1, p1}, Lcom/a/b/b/f;->a(Lcom/a/b/b/f;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .registers 4

    .prologue
    .line 180
    if-nez p1, :cond_4

    const/4 v0, 0x0

    :goto_3
    return-object v0

    :cond_4
    iget-object v0, p0, Lcom/a/b/b/l;->b:Lcom/a/b/b/f;

    iget-object v1, p0, Lcom/a/b/b/l;->a:Lcom/a/b/b/f;

    invoke-virtual {v0, v1, p1}, Lcom/a/b/b/f;->a(Lcom/a/b/b/f;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method


# virtual methods
.method protected final bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 162
    check-cast p1, Ljava/lang/String;

    .line 1180
    if-nez p1, :cond_6

    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, Lcom/a/b/b/l;->b:Lcom/a/b/b/f;

    iget-object v1, p0, Lcom/a/b/b/l;->a:Lcom/a/b/b/f;

    invoke-virtual {v0, v1, p1}, Lcom/a/b/b/f;->a(Lcom/a/b/b/f;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_5
.end method

.method protected final synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 162
    check-cast p1, Ljava/lang/String;

    .line 2175
    if-nez p1, :cond_6

    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, Lcom/a/b/b/l;->a:Lcom/a/b/b/f;

    iget-object v1, p0, Lcom/a/b/b/l;->b:Lcom/a/b/b/f;

    invoke-virtual {v0, v1, p1}, Lcom/a/b/b/f;->a(Lcom/a/b/b/f;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_5
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 184
    instance-of v1, p1, Lcom/a/b/b/l;

    if-eqz v1, :cond_1c

    .line 185
    check-cast p1, Lcom/a/b/b/l;

    .line 186
    iget-object v1, p0, Lcom/a/b/b/l;->a:Lcom/a/b/b/f;

    iget-object v2, p1, Lcom/a/b/b/l;->a:Lcom/a/b/b/f;

    invoke-virtual {v1, v2}, Lcom/a/b/b/f;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    iget-object v1, p0, Lcom/a/b/b/l;->b:Lcom/a/b/b/f;

    iget-object v2, p1, Lcom/a/b/b/l;->b:Lcom/a/b/b/f;

    invoke-virtual {v1, v2}, Lcom/a/b/b/f;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    const/4 v0, 0x1

    .line 189
    :cond_1c
    return v0
.end method

.method public final hashCode()I
    .registers 3

    .prologue
    .line 193
    iget-object v0, p0, Lcom/a/b/b/l;->a:Lcom/a/b/b/f;

    invoke-virtual {v0}, Lcom/a/b/b/f;->hashCode()I

    move-result v0

    iget-object v1, p0, Lcom/a/b/b/l;->b:Lcom/a/b/b/f;

    invoke-virtual {v1}, Lcom/a/b/b/f;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 6

    .prologue
    .line 197
    iget-object v0, p0, Lcom/a/b/b/l;->a:Lcom/a/b/b/f;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/b/l;->b:Lcom/a/b/b/f;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xe

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".converterTo("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
