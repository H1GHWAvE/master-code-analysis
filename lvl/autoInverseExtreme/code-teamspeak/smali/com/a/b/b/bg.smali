.class final Lcom/a/b/b/bg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/b/bf;


# static fields
.field static a:Z
    .annotation build Lcom/a/b/a/d;
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 249
    sget-boolean v1, Lcom/a/b/b/bg;->a:Z

    if-eqz v1, :cond_6

    .line 267
    :cond_5
    :goto_5
    return-object v0

    .line 254
    :cond_6
    :try_start_6
    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;
    :try_end_9
    .catch Ljava/lang/SecurityException; {:try_start_6 .. :try_end_9} :catch_13

    move-result-object v1

    .line 259
    if-eqz v1, :cond_5

    .line 261
    :try_start_c
    const-string v2, "com.google.common.base.internal.Finalizer"

    invoke-virtual {v1, v2}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_11
    .catch Ljava/lang/ClassNotFoundException; {:try_start_c .. :try_end_11} :catch_1e

    move-result-object v0

    goto :goto_5

    .line 256
    :catch_13
    move-exception v1

    invoke-static {}, Lcom/a/b/b/bc;->b()Ljava/util/logging/Logger;

    move-result-object v1

    const-string v2, "Not allowed to access system class loader."

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    goto :goto_5

    .line 264
    :catch_1e
    move-exception v1

    goto :goto_5
.end method
