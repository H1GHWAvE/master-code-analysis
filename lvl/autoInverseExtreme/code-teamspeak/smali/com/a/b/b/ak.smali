.class public abstract Lcom/a/b/b/ak;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/b/bj;


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/b;
.end annotation


# instance fields
.field private final a:Z

.field private transient b:Lcom/a/b/b/ak;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 103
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/a/b/b/ak;-><init>(B)V

    .line 104
    return-void
.end method

.method private constructor <init>(B)V
    .registers 3

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/a/b/b/ak;->a:Z

    .line 111
    return-void
.end method

.method private static a(Lcom/a/b/b/bj;Lcom/a/b/b/bj;)Lcom/a/b/b/ak;
    .registers 4

    .prologue
    .line 405
    new-instance v0, Lcom/a/b/b/ao;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/a/b/b/ao;-><init>(Lcom/a/b/b/bj;Lcom/a/b/b/bj;B)V

    return-object v0
.end method

.method private a(Ljava/lang/Iterable;)Ljava/lang/Iterable;
    .registers 3

    .prologue
    .line 179
    const-string v0, "fromIterable"

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    new-instance v0, Lcom/a/b/b/al;

    invoke-direct {v0, p0, p1}, Lcom/a/b/b/al;-><init>(Lcom/a/b/b/ak;Ljava/lang/Iterable;)V

    return-object v0
.end method

.method private static b()Lcom/a/b/b/ak;
    .registers 1

    .prologue
    .line 456
    sget-object v0, Lcom/a/b/b/ap;->a:Lcom/a/b/b/ap;

    return-object v0
.end method

.method private b(Lcom/a/b/b/ak;)Lcom/a/b/b/ak;
    .registers 3

    .prologue
    .line 288
    invoke-virtual {p0, p1}, Lcom/a/b/b/ak;->a(Lcom/a/b/b/ak;)Lcom/a/b/b/ak;

    move-result-object v0

    return-object v0
.end method

.method private f(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 147
    invoke-virtual {p0, p1}, Lcom/a/b/b/ak;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Lcom/a/b/b/ak;
    .registers 2

    .prologue
    .line 212
    iget-object v0, p0, Lcom/a/b/b/ak;->b:Lcom/a/b/b/ak;

    .line 213
    if-nez v0, :cond_b

    new-instance v0, Lcom/a/b/b/aq;

    invoke-direct {v0, p0}, Lcom/a/b/b/aq;-><init>(Lcom/a/b/b/ak;)V

    iput-object v0, p0, Lcom/a/b/b/ak;->b:Lcom/a/b/b/ak;

    :cond_b
    return-object v0
.end method

.method a(Lcom/a/b/b/ak;)Lcom/a/b/b/ak;
    .registers 4

    .prologue
    .line 295
    new-instance v1, Lcom/a/b/b/an;

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/ak;

    invoke-direct {v1, p0, v0}, Lcom/a/b/b/an;-><init>(Lcom/a/b/b/ak;Lcom/a/b/b/ak;)V

    return-object v1
.end method

.method public abstract a(Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public abstract b(Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method c(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/a/b/b/ak;->a:Z

    if-eqz v0, :cond_11

    .line 154
    if-nez p1, :cond_8

    const/4 v0, 0x0

    .line 156
    :goto_7
    return-object v0

    .line 154
    :cond_8
    invoke-virtual {p0, p1}, Lcom/a/b/b/ak;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_7

    .line 156
    :cond_11
    invoke-virtual {p0, p1}, Lcom/a/b/b/ak;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_7
.end method

.method d(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 162
    iget-boolean v0, p0, Lcom/a/b/b/ak;->a:Z

    if-eqz v0, :cond_11

    .line 164
    if-nez p1, :cond_8

    const/4 v0, 0x0

    .line 166
    :goto_7
    return-object v0

    .line 164
    :cond_8
    invoke-virtual {p0, p1}, Lcom/a/b/b/ak;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_7

    .line 166
    :cond_11
    invoke-virtual {p0, p1}, Lcom/a/b/b/ak;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_7
.end method

.method public final e(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 367
    .line 1147
    invoke-virtual {p0, p1}, Lcom/a/b/b/ak;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 367
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 383
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
