.class public final Lcom/a/b/k/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/b;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final b:I = -0x1

.field private static final e:J


# instance fields
.field final a:Ljava/lang/String;

.field private final c:I

.field private final d:Z


# direct methods
.method private constructor <init>(Ljava/lang/String;IZ)V
    .registers 4

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput-object p1, p0, Lcom/a/b/k/a;->a:Ljava/lang/String;

    .line 82
    iput p2, p0, Lcom/a/b/k/a;->c:I

    .line 83
    iput-boolean p3, p0, Lcom/a/b/k/a;->d:Z

    .line 84
    return-void
.end method

.method private a(I)I
    .registers 3

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/a/b/k/a;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    iget p1, p0, Lcom/a/b/k/a;->c:I

    :cond_8
    return p1
.end method

.method public static a(Ljava/lang/String;)Lcom/a/b/k/a;
    .registers 11

    .prologue
    const/4 v9, 0x2

    const/4 v5, -0x1

    const/16 v7, 0x3a

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 168
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    const/4 v3, 0x0

    .line 173
    const-string v0, "["

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c8

    .line 1216
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v3, 0x5b

    if-ne v0, v3, :cond_87

    move v0, v1

    :goto_1b
    const-string v3, "Bracketed host-port string must start with a bracket: %s"

    new-array v4, v1, [Ljava/lang/Object;

    aput-object p0, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1218
    invoke-virtual {p0, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 1219
    const/16 v3, 0x5d

    invoke-virtual {p0, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    .line 1220
    if-ltz v0, :cond_89

    if-le v3, v0, :cond_89

    move v0, v1

    :goto_33
    const-string v4, "Invalid bracketed host/port: %s"

    new-array v6, v1, [Ljava/lang/Object;

    aput-object p0, v6, v2

    invoke-static {v0, v4, v6}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1223
    invoke-virtual {p0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 1224
    add-int/lit8 v0, v3, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    if-ne v0, v6, :cond_8b

    .line 1225
    new-array v0, v9, [Ljava/lang/String;

    aput-object v4, v0, v2

    const-string v3, ""

    aput-object v3, v0, v1

    .line 175
    :goto_50
    aget-object v3, v0, v2

    .line 176
    aget-object v0, v0, v1

    move-object v4, v3

    move-object v3, v0

    move v0, v2

    .line 191
    :goto_57
    invoke-static {v3}, Lcom/a/b/b/dy;->a(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_10d

    .line 194
    const-string v5, "+"

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_ed

    move v5, v1

    :goto_66
    const-string v6, "Unparseable port number: %s"

    new-array v7, v1, [Ljava/lang/Object;

    aput-object p0, v7, v2

    invoke-static {v5, v6, v7}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 196
    :try_start_6f
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_72
    .catch Ljava/lang/NumberFormatException; {:try_start_6f .. :try_end_72} :catch_f0

    move-result v3

    .line 200
    invoke-static {v3}, Lcom/a/b/k/a;->c(I)Z

    move-result v5

    const-string v6, "Port number out of range: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v2

    invoke-static {v5, v6, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    move v1, v3

    .line 203
    :goto_81
    new-instance v2, Lcom/a/b/k/a;

    invoke-direct {v2, v4, v1, v0}, Lcom/a/b/k/a;-><init>(Ljava/lang/String;IZ)V

    return-object v2

    :cond_87
    move v0, v2

    .line 1216
    goto :goto_1b

    :cond_89
    move v0, v2

    .line 1220
    goto :goto_33

    .line 1227
    :cond_8b
    add-int/lit8 v0, v3, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-ne v0, v7, :cond_b9

    move v0, v1

    :goto_94
    const-string v6, "Only a colon may follow a close bracket: %s"

    new-array v7, v1, [Ljava/lang/Object;

    aput-object p0, v7, v2

    invoke-static {v0, v6, v7}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1229
    add-int/lit8 v0, v3, 0x2

    :goto_9f
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v0, v6, :cond_bb

    .line 1230
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Ljava/lang/Character;->isDigit(C)Z

    move-result v6

    const-string v7, "Port must be numeric: %s"

    new-array v8, v1, [Ljava/lang/Object;

    aput-object p0, v8, v2

    invoke-static {v6, v7, v8}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1229
    add-int/lit8 v0, v0, 0x1

    goto :goto_9f

    :cond_b9
    move v0, v2

    .line 1227
    goto :goto_94

    .line 1233
    :cond_bb
    new-array v0, v9, [Ljava/lang/String;

    aput-object v4, v0, v2

    add-int/lit8 v3, v3, 0x2

    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    goto :goto_50

    .line 178
    :cond_c8
    invoke-virtual {p0, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 179
    if-ltz v0, :cond_e5

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p0, v7, v4}, Ljava/lang/String;->indexOf(II)I

    move-result v4

    if-ne v4, v5, :cond_e5

    .line 181
    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 182
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v3

    move-object v3, v0

    move v0, v2

    goto/16 :goto_57

    .line 186
    :cond_e5
    if-ltz v0, :cond_eb

    move v0, v1

    :goto_e8
    move-object v4, p0

    goto/16 :goto_57

    :cond_eb
    move v0, v2

    goto :goto_e8

    :cond_ed
    move v5, v2

    .line 194
    goto/16 :goto_66

    .line 198
    :catch_f0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Unparseable port number: "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_107

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_103
    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_107
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_103

    :cond_10d
    move v1, v5

    goto/16 :goto_81
.end method

.method private static a(Ljava/lang/String;I)Lcom/a/b/k/a;
    .registers 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 134
    invoke-static {p1}, Lcom/a/b/k/a;->c(I)Z

    move-result v0

    const-string v3, "Port out of range: %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 135
    invoke-static {p0}, Lcom/a/b/k/a;->a(Ljava/lang/String;)Lcom/a/b/k/a;

    move-result-object v3

    .line 136
    invoke-virtual {v3}, Lcom/a/b/k/a;->a()Z

    move-result v0

    if-nez v0, :cond_31

    move v0, v1

    :goto_1e
    const-string v4, "Host has a port: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v2

    invoke-static {v0, v4, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 137
    new-instance v0, Lcom/a/b/k/a;

    iget-object v1, v3, Lcom/a/b/k/a;->a:Ljava/lang/String;

    iget-boolean v2, v3, Lcom/a/b/k/a;->d:Z

    invoke-direct {v0, v1, p1, v2}, Lcom/a/b/k/a;-><init>(Ljava/lang/String;IZ)V

    return-object v0

    :cond_31
    move v0, v2

    .line 136
    goto :goto_1e
.end method

.method private b(I)Lcom/a/b/k/a;
    .registers 5

    .prologue
    .line 248
    invoke-static {p1}, Lcom/a/b/k/a;->c(I)Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 249
    invoke-virtual {p0}, Lcom/a/b/k/a;->a()Z

    move-result v0

    if-nez v0, :cond_11

    iget v0, p0, Lcom/a/b/k/a;->c:I

    if-ne v0, p1, :cond_12

    .line 252
    :cond_11
    :goto_11
    return-object p0

    :cond_12
    new-instance v0, Lcom/a/b/k/a;

    iget-object v1, p0, Lcom/a/b/k/a;->a:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/a/b/k/a;->d:Z

    invoke-direct {v0, v1, p1, v2}, Lcom/a/b/k/a;-><init>(Ljava/lang/String;IZ)V

    move-object p0, v0

    goto :goto_11
.end method

.method private static b(Ljava/lang/String;)Lcom/a/b/k/a;
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 152
    invoke-static {p0}, Lcom/a/b/k/a;->a(Ljava/lang/String;)Lcom/a/b/k/a;

    move-result-object v3

    .line 153
    invoke-virtual {v3}, Lcom/a/b/k/a;->a()Z

    move-result v0

    if-nez v0, :cond_17

    move v0, v1

    :goto_d
    const-string v4, "Host has a port: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v2

    invoke-static {v0, v4, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 154
    return-object v3

    :cond_17
    move v0, v2

    .line 153
    goto :goto_d
.end method

.method private b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/a/b/k/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method private c()I
    .registers 2

    .prologue
    .line 110
    invoke-virtual {p0}, Lcom/a/b/k/a;->a()Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    .line 111
    iget v0, p0, Lcom/a/b/k/a;->c:I

    return v0
.end method

.method private static c(I)Z
    .registers 2

    .prologue
    .line 312
    if-ltz p0, :cond_9

    const v0, 0xffff

    if-gt p0, v0, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method private static c(Ljava/lang/String;)[Ljava/lang/String;
    .registers 10

    .prologue
    const/16 v6, 0x3a

    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 216
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v3, 0x5b

    if-ne v0, v3, :cond_44

    move v0, v1

    :goto_e
    const-string v3, "Bracketed host-port string must start with a bracket: %s"

    new-array v4, v1, [Ljava/lang/Object;

    aput-object p0, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 218
    invoke-virtual {p0, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 219
    const/16 v3, 0x5d

    invoke-virtual {p0, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    .line 220
    if-ltz v0, :cond_46

    if-le v3, v0, :cond_46

    move v0, v1

    :goto_26
    const-string v4, "Invalid bracketed host/port: %s"

    new-array v5, v1, [Ljava/lang/Object;

    aput-object p0, v5, v2

    invoke-static {v0, v4, v5}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 223
    invoke-virtual {p0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 224
    add-int/lit8 v0, v3, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-ne v0, v5, :cond_48

    .line 225
    new-array v0, v8, [Ljava/lang/String;

    aput-object v4, v0, v2

    const-string v2, ""

    aput-object v2, v0, v1

    .line 233
    :goto_43
    return-object v0

    :cond_44
    move v0, v2

    .line 216
    goto :goto_e

    :cond_46
    move v0, v2

    .line 220
    goto :goto_26

    .line 227
    :cond_48
    add-int/lit8 v0, v3, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-ne v0, v6, :cond_76

    move v0, v1

    :goto_51
    const-string v5, "Only a colon may follow a close bracket: %s"

    new-array v6, v1, [Ljava/lang/Object;

    aput-object p0, v6, v2

    invoke-static {v0, v5, v6}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 229
    add-int/lit8 v0, v3, 0x2

    :goto_5c
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v0, v5, :cond_78

    .line 230
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->isDigit(C)Z

    move-result v5

    const-string v6, "Port must be numeric: %s"

    new-array v7, v1, [Ljava/lang/Object;

    aput-object p0, v7, v2

    invoke-static {v5, v6, v7}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 229
    add-int/lit8 v0, v0, 0x1

    goto :goto_5c

    :cond_76
    move v0, v2

    .line 227
    goto :goto_51

    .line 233
    :cond_78
    new-array v0, v8, [Ljava/lang/String;

    aput-object v4, v0, v2

    add-int/lit8 v2, v3, 0x2

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    goto :goto_43
.end method

.method private d()Lcom/a/b/k/a;
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 271
    iget-boolean v0, p0, Lcom/a/b/k/a;->d:Z

    if-nez v0, :cond_13

    move v0, v1

    :goto_7
    const-string v3, "Possible bracketless IPv6 literal: %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/a/b/k/a;->a:Ljava/lang/String;

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 272
    return-object p0

    :cond_13
    move v0, v2

    .line 271
    goto :goto_7
.end method


# virtual methods
.method public final a()Z
    .registers 2

    .prologue
    .line 99
    iget v0, p0, Lcom/a/b/k/a;->c:I

    if-ltz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 6
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 277
    if-ne p0, p1, :cond_5

    .line 286
    :cond_4
    :goto_4
    return v0

    .line 280
    :cond_5
    instance-of v2, p1, Lcom/a/b/k/a;

    if-eqz v2, :cond_23

    .line 281
    check-cast p1, Lcom/a/b/k/a;

    .line 282
    iget-object v2, p0, Lcom/a/b/k/a;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/a/b/k/a;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_21

    iget v2, p0, Lcom/a/b/k/a;->c:I

    iget v3, p1, Lcom/a/b/k/a;->c:I

    if-ne v2, v3, :cond_21

    iget-boolean v2, p0, Lcom/a/b/k/a;->d:Z

    iget-boolean v3, p1, Lcom/a/b/k/a;->d:Z

    if-eq v2, v3, :cond_4

    :cond_21
    move v0, v1

    goto :goto_4

    :cond_23
    move v0, v1

    .line 286
    goto :goto_4
.end method

.method public final hashCode()I
    .registers 4

    .prologue
    .line 291
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/a/b/k/a;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/a/b/k/a;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/a/b/k/a;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 2084
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    .line 291
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 5

    .prologue
    const/16 v3, 0x3a

    .line 298
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/a/b/k/a;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 299
    iget-object v1, p0, Lcom/a/b/k/a;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-ltz v1, :cond_3c

    .line 300
    const/16 v1, 0x5b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/a/b/k/a;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x5d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 304
    :goto_28
    invoke-virtual {p0}, Lcom/a/b/k/a;->a()Z

    move-result v1

    if-eqz v1, :cond_37

    .line 305
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/a/b/k/a;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 307
    :cond_37
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 302
    :cond_3c
    iget-object v1, p0, Lcom/a/b/k/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_28
.end method
