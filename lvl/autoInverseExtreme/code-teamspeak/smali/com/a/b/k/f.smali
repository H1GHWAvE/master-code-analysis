.class public final Lcom/a/b/k/f;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field private static final a:Lcom/a/b/b/m;

.field private static final b:Lcom/a/b/b/di;

.field private static final c:Lcom/a/b/b/bv;

.field private static final d:I = -0x1

.field private static final e:Ljava/lang/String; = "\\."

.field private static final f:I = 0x7f

.field private static final g:I = 0xfd

.field private static final h:I = 0x3f

.field private static final l:Lcom/a/b/b/m;

.field private static final m:Lcom/a/b/b/m;


# instance fields
.field private final i:Ljava/lang/String;

.field private final j:Lcom/a/b/d/jl;

.field private final k:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/16 v1, 0x2e

    .line 79
    const-string v0, ".\u3002\uff0e\uff61"

    invoke-static {v0}, Lcom/a/b/b/m;->a(Ljava/lang/CharSequence;)Lcom/a/b/b/m;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/f;->a:Lcom/a/b/b/m;

    .line 81
    invoke-static {v1}, Lcom/a/b/b/di;->a(C)Lcom/a/b/b/di;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/f;->b:Lcom/a/b/b/di;

    .line 82
    invoke-static {v1}, Lcom/a/b/b/bv;->a(C)Lcom/a/b/b/bv;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/f;->c:Lcom/a/b/b/bv;

    .line 241
    const-string v0, "-_"

    invoke-static {v0}, Lcom/a/b/b/m;->a(Ljava/lang/CharSequence;)Lcom/a/b/b/m;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/f;->l:Lcom/a/b/b/m;

    .line 243
    sget-object v0, Lcom/a/b/b/m;->f:Lcom/a/b/b/m;

    sget-object v1, Lcom/a/b/k/f;->l:Lcom/a/b/b/m;

    invoke-virtual {v0, v1}, Lcom/a/b/b/m;->b(Lcom/a/b/b/m;)Lcom/a/b/b/m;

    move-result-object v0

    sput-object v0, Lcom/a/b/k/f;->m:Lcom/a/b/b/m;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .registers 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143
    sget-object v0, Lcom/a/b/k/f;->a:Lcom/a/b/b/m;

    const/16 v1, 0x2e

    invoke-virtual {v0, p1, v1}, Lcom/a/b/b/m;->a(Ljava/lang/CharSequence;C)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/b/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 145
    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_23

    .line 146
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 149
    :cond_23
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v4, 0xfd

    if-gt v1, v4, :cond_6d

    move v1, v2

    :goto_2c
    const-string v4, "Domain name too long: \'%s\':"

    new-array v5, v2, [Ljava/lang/Object;

    aput-object v0, v5, v3

    invoke-static {v1, v4, v5}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 151
    iput-object v0, p0, Lcom/a/b/k/f;->i:Ljava/lang/String;

    .line 153
    sget-object v1, Lcom/a/b/k/f;->b:Lcom/a/b/b/di;

    invoke-virtual {v1, v0}, Lcom/a/b/b/di;->a(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-static {v1}, Lcom/a/b/d/jl;->a(Ljava/lang/Iterable;)Lcom/a/b/d/jl;

    move-result-object v1

    iput-object v1, p0, Lcom/a/b/k/f;->j:Lcom/a/b/d/jl;

    .line 154
    iget-object v1, p0, Lcom/a/b/k/f;->j:Lcom/a/b/d/jl;

    invoke-virtual {v1}, Lcom/a/b/d/jl;->size()I

    move-result v1

    const/16 v4, 0x7f

    if-gt v1, v4, :cond_6f

    move v1, v2

    :goto_4e
    const-string v4, "Domain has too many parts: \'%s\'"

    new-array v5, v2, [Ljava/lang/Object;

    aput-object v0, v5, v3

    invoke-static {v1, v4, v5}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 156
    iget-object v1, p0, Lcom/a/b/k/f;->j:Lcom/a/b/d/jl;

    invoke-static {v1}, Lcom/a/b/k/f;->a(Ljava/util/List;)Z

    move-result v1

    const-string v4, "Not a valid domain name: \'%s\'"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-static {v1, v4, v2}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 158
    invoke-direct {p0}, Lcom/a/b/k/f;->b()I

    move-result v0

    iput v0, p0, Lcom/a/b/k/f;->k:I

    .line 159
    return-void

    :cond_6d
    move v1, v3

    .line 149
    goto :goto_2c

    :cond_6f
    move v1, v3

    .line 154
    goto :goto_4e
.end method

.method private a(I)Lcom/a/b/k/f;
    .registers 5

    .prologue
    .line 457
    sget-object v0, Lcom/a/b/k/f;->c:Lcom/a/b/b/bv;

    iget-object v1, p0, Lcom/a/b/k/f;->j:Lcom/a/b/d/jl;

    iget-object v2, p0, Lcom/a/b/k/f;->j:Lcom/a/b/d/jl;

    invoke-virtual {v2}, Lcom/a/b/d/jl;->size()I

    move-result v2

    invoke-virtual {v1, p1, v2}, Lcom/a/b/d/jl;->a(II)Lcom/a/b/d/jl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/b/bv;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/k/f;->a(Ljava/lang/String;)Lcom/a/b/k/f;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lcom/a/b/k/f;
    .registers 3

    .prologue
    .line 213
    new-instance v1, Lcom/a/b/k/f;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v0}, Lcom/a/b/k/f;-><init>(Ljava/lang/String;)V

    return-object v1
.end method

.method private static a(Ljava/lang/String;Z)Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 259
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_f

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x3f

    if-le v1, v2, :cond_10

    .line 298
    :cond_f
    :goto_f
    return v0

    .line 273
    :cond_10
    sget-object v1, Lcom/a/b/b/m;->b:Lcom/a/b/b/m;

    .line 2095
    invoke-virtual {v1}, Lcom/a/b/b/m;->a()Lcom/a/b/b/m;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/a/b/b/m;->h(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 275
    sget-object v2, Lcom/a/b/k/f;->m:Lcom/a/b/b/m;

    invoke-virtual {v2, v1}, Lcom/a/b/b/m;->c(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 281
    sget-object v1, Lcom/a/b/k/f;->l:Lcom/a/b/b/m;

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v1, v2}, Lcom/a/b/b/m;->c(C)Z

    move-result v1

    if-nez v1, :cond_f

    sget-object v1, Lcom/a/b/k/f;->l:Lcom/a/b/b/m;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v1, v2}, Lcom/a/b/b/m;->c(C)Z

    move-result v1

    if-nez v1, :cond_f

    .line 294
    if-eqz p1, :cond_4e

    sget-object v1, Lcom/a/b/b/m;->c:Lcom/a/b/b/m;

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v1, v2}, Lcom/a/b/b/m;->c(C)Z

    move-result v1

    if-nez v1, :cond_f

    .line 298
    :cond_4e
    const/4 v0, 0x1

    goto :goto_f
.end method

.method private static a(Ljava/util/List;)Z
    .registers 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 223
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v4, v0, -0x1

    .line 227
    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/a/b/k/f;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_16

    move v0, v1

    .line 238
    :goto_15
    return v0

    :cond_16
    move v3, v1

    .line 231
    :goto_17
    if-ge v3, v4, :cond_2b

    .line 232
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 233
    invoke-static {v0, v1}, Lcom/a/b/k/f;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_27

    move v0, v1

    .line 234
    goto :goto_15

    .line 231
    :cond_27
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_17

    :cond_2b
    move v0, v2

    .line 238
    goto :goto_15
.end method

.method private b()I
    .registers 8

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 168
    iget-object v0, p0, Lcom/a/b/k/f;->j:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->size()I

    move-result v4

    move v0, v3

    .line 170
    :goto_a
    if-ge v0, v4, :cond_47

    .line 171
    sget-object v1, Lcom/a/b/k/f;->c:Lcom/a/b/b/bv;

    iget-object v5, p0, Lcom/a/b/k/f;->j:Lcom/a/b/d/jl;

    invoke-virtual {v5, v0, v4}, Lcom/a/b/d/jl;->a(II)Lcom/a/b/d/jl;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/a/b/b/bv;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    .line 173
    sget-object v5, Lcom/a/d/a/a;->a:Lcom/a/b/d/jt;

    invoke-virtual {v5, v1}, Lcom/a/b/d/jt;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_21

    .line 189
    :cond_20
    :goto_20
    return v0

    .line 180
    :cond_21
    sget-object v5, Lcom/a/d/a/a;->c:Lcom/a/b/d/jt;

    invoke-virtual {v5, v1}, Lcom/a/b/d/jt;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2c

    .line 181
    add-int/lit8 v0, v0, 0x1

    goto :goto_20

    .line 1510
    :cond_2c
    const-string v5, "\\."

    invoke-virtual {v1, v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v1

    .line 1511
    array-length v5, v1

    if-ne v5, v6, :cond_45

    sget-object v5, Lcom/a/d/a/a;->b:Lcom/a/b/d/jt;

    aget-object v1, v1, v2

    invoke-virtual {v5, v1}, Lcom/a/b/d/jt;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_45

    move v1, v2

    .line 184
    :goto_40
    if-nez v1, :cond_20

    .line 170
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_45
    move v1, v3

    .line 1511
    goto :goto_40

    .line 189
    :cond_47
    const/4 v0, -0x1

    goto :goto_20
.end method

.method private b(Ljava/lang/String;)Lcom/a/b/k/f;
    .registers 7

    .prologue
    .line 471
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/k/f;->i:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/k/f;->a(Ljava/lang/String;)Lcom/a/b/k/f;

    move-result-object v0

    return-object v0
.end method

.method private c()Lcom/a/b/d/jl;
    .registers 2

    .prologue
    .line 307
    iget-object v0, p0, Lcom/a/b/k/f;->j:Lcom/a/b/d/jl;

    return-object v0
.end method

.method private static c(Ljava/lang/String;)Z
    .registers 2

    .prologue
    .line 498
    :try_start_0
    invoke-static {p0}, Lcom/a/b/k/f;->a(Ljava/lang/String;)Lcom/a/b/k/f;
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_3} :catch_5

    .line 499
    const/4 v0, 0x1

    .line 501
    :goto_4
    return v0

    :catch_5
    move-exception v0

    const/4 v0, 0x0

    goto :goto_4
.end method

.method private d()Z
    .registers 2

    .prologue
    .line 324
    iget v0, p0, Lcom/a/b/k/f;->k:I

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private static d(Ljava/lang/String;)Z
    .registers 5

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    .line 510
    const-string v1, "\\."

    invoke-virtual {p0, v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v1

    .line 511
    array-length v2, v1

    if-ne v2, v3, :cond_16

    sget-object v2, Lcom/a/d/a/a;->b:Lcom/a/b/d/jt;

    aget-object v1, v1, v0

    invoke-virtual {v2, v1}, Lcom/a/b/d/jt;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method private e()Lcom/a/b/k/f;
    .registers 2

    .prologue
    .line 348
    invoke-virtual {p0}, Lcom/a/b/k/f;->a()Z

    move-result v0

    if-eqz v0, :cond_d

    iget v0, p0, Lcom/a/b/k/f;->k:I

    invoke-direct {p0, v0}, Lcom/a/b/k/f;->a(I)Lcom/a/b/k/f;

    move-result-object v0

    :goto_c
    return-object v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method private f()Z
    .registers 2

    .prologue
    .line 371
    iget v0, p0, Lcom/a/b/k/f;->k:I

    if-lez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private g()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 395
    iget v1, p0, Lcom/a/b/k/f;->k:I

    if-ne v1, v0, :cond_6

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private h()Lcom/a/b/k/f;
    .registers 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 421
    .line 2395
    iget v0, p0, Lcom/a/b/k/f;->k:I

    if-ne v0, v1, :cond_a

    move v0, v1

    .line 421
    :goto_7
    if-eqz v0, :cond_c

    .line 425
    :goto_9
    return-object p0

    :cond_a
    move v0, v2

    .line 2395
    goto :goto_7

    .line 3371
    :cond_c
    iget v0, p0, Lcom/a/b/k/f;->k:I

    if-lez v0, :cond_25

    move v0, v1

    .line 424
    :goto_11
    const-string v3, "Not under a public suffix: %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/a/b/k/f;->i:Ljava/lang/String;

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 425
    iget v0, p0, Lcom/a/b/k/f;->k:I

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/a/b/k/f;->a(I)Lcom/a/b/k/f;

    move-result-object p0

    goto :goto_9

    :cond_25
    move v0, v2

    .line 3371
    goto :goto_11
.end method

.method private i()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 432
    iget-object v1, p0, Lcom/a/b/k/f;->j:Lcom/a/b/d/jl;

    invoke-virtual {v1}, Lcom/a/b/d/jl;->size()I

    move-result v1

    if-le v1, v0, :cond_a

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method private j()Lcom/a/b/k/f;
    .registers 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 444
    .line 3432
    iget-object v0, p0, Lcom/a/b/k/f;->j:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->size()I

    move-result v0

    if-le v0, v1, :cond_1b

    move v0, v1

    .line 444
    :goto_b
    const-string v3, "Domain \'%s\' has no parent"

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/a/b/k/f;->i:Ljava/lang/String;

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 445
    invoke-direct {p0, v1}, Lcom/a/b/k/f;->a(I)Lcom/a/b/k/f;

    move-result-object v0

    return-object v0

    :cond_1b
    move v0, v2

    .line 3432
    goto :goto_b
.end method


# virtual methods
.method public final a()Z
    .registers 3

    .prologue
    .line 338
    iget v0, p0, Lcom/a/b/k/f;->k:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 531
    if-ne p1, p0, :cond_4

    .line 532
    const/4 v0, 0x1

    .line 540
    :goto_3
    return v0

    .line 535
    :cond_4
    instance-of v0, p1, Lcom/a/b/k/f;

    if-eqz v0, :cond_13

    .line 536
    check-cast p1, Lcom/a/b/k/f;

    .line 537
    iget-object v0, p0, Lcom/a/b/k/f;->i:Ljava/lang/String;

    iget-object v1, p1, Lcom/a/b/k/f;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_3

    .line 540
    :cond_13
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 545
    iget-object v0, p0, Lcom/a/b/k/f;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 519
    iget-object v0, p0, Lcom/a/b/k/f;->i:Ljava/lang/String;

    return-object v0
.end method
