.class Lcom/a/b/d/abx;
.super Lcom/a/b/d/bf;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field private static final f:J


# instance fields
.field final a:Ljava/util/Map;
    .annotation runtime Lcom/a/b/d/hv;
    .end annotation
.end field

.field final b:Lcom/a/b/b/dz;
    .annotation runtime Lcom/a/b/d/hv;
    .end annotation
.end field

.field private transient c:Ljava/util/Set;

.field private transient d:Ljava/util/Map;

.field private transient e:Lcom/a/b/d/aci;


# direct methods
.method constructor <init>(Ljava/util/Map;Lcom/a/b/b/dz;)V
    .registers 3

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/a/b/d/bf;-><init>()V

    .line 73
    iput-object p1, p0, Lcom/a/b/d/abx;->a:Ljava/util/Map;

    .line 74
    iput-object p2, p0, Lcom/a/b/d/abx;->b:Lcom/a/b/b/dz;

    .line 75
    return-void
.end method

.method static synthetic a(Lcom/a/b/d/abx;Ljava/lang/Object;)Ljava/util/Map;
    .registers 7

    .prologue
    .line 2161
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    .line 2162
    iget-object v0, p0, Lcom/a/b/d/abx;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 2164
    :cond_f
    :goto_f
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 2165
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2166
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2167
    if-eqz v1, :cond_f

    .line 2168
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v2, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2169
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2170
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_f

    .line 67
    :cond_3e
    return-object v2
.end method

.method static synthetic a(Lcom/a/b/d/abx;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 5

    .prologue
    .line 67
    invoke-direct {p0, p1, p2, p3}, Lcom/a/b/d/abx;->b(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/a/b/d/abx;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 5

    .prologue
    .line 67
    .line 1184
    invoke-direct {p0, p1, p2, p3}, Lcom/a/b/d/abx;->b(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1185
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/abx;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1186
    const/4 v0, 0x1

    :goto_a
    return v0

    .line 1188
    :cond_b
    const/4 v0, 0x0

    .line 67
    goto :goto_a
.end method

.method private b(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 5

    .prologue
    .line 179
    if-eqz p3, :cond_e

    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/abx;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private c(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 5

    .prologue
    .line 184
    invoke-direct {p0, p1, p2, p3}, Lcom/a/b/d/abx;->b(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 185
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/abx;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    const/4 v0, 0x1

    .line 188
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method private f(Ljava/lang/Object;)Ljava/util/Map;
    .registers 4

    .prologue
    .line 129
    iget-object v0, p0, Lcom/a/b/d/abx;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 130
    if-nez v0, :cond_17

    .line 131
    iget-object v0, p0, Lcom/a/b/d/abx;->b:Lcom/a/b/b/dz;

    invoke-interface {v0}, Lcom/a/b/b/dz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 132
    iget-object v1, p0, Lcom/a/b/d/abx;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    :cond_17
    return-object v0
.end method

.method private g(Ljava/lang/Object;)Ljava/util/Map;
    .registers 7

    .prologue
    .line 161
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    .line 162
    iget-object v0, p0, Lcom/a/b/d/abx;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 164
    :cond_f
    :goto_f
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 165
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 166
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 167
    if-eqz v1, :cond_f

    .line 168
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v2, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 170
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_f

    .line 174
    :cond_3e
    return-object v2
.end method


# virtual methods
.method public a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 6

    .prologue
    .line 138
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    invoke-static {p3}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1129
    iget-object v0, p0, Lcom/a/b/d/abx;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 1130
    if-nez v0, :cond_20

    .line 1131
    iget-object v0, p0, Lcom/a/b/d/abx;->b:Lcom/a/b/b/dz;

    invoke-interface {v0}, Lcom/a/b/b/dz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 1132
    iget-object v1, p0, Lcom/a/b/d/abx;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    :cond_20
    invoke-interface {v0, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 562
    invoke-virtual {p0}, Lcom/a/b/d/abx;->m()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 97
    if-eqz p1, :cond_c

    iget-object v0, p0, Lcom/a/b/d/abx;->a:Ljava/util/Map;

    invoke-static {v0, p1}, Lcom/a/b/d/sz;->b(Ljava/util/Map;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 81
    if-eqz p1, :cond_c

    if-eqz p2, :cond_c

    invoke-super {p0, p1, p2}, Lcom/a/b/d/bf;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 105
    if-eqz p1, :cond_4

    if-nez p2, :cond_6

    :cond_4
    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_6
    invoke-super {p0, p1, p2}, Lcom/a/b/d/bf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_5
.end method

.method public b()Ljava/util/Set;
    .registers 3

    .prologue
    .line 578
    iget-object v0, p0, Lcom/a/b/d/abx;->c:Ljava/util/Set;

    .line 579
    if-nez v0, :cond_c

    new-instance v0, Lcom/a/b/d/ach;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/ach;-><init>(Lcom/a/b/d/abx;B)V

    iput-object v0, p0, Lcom/a/b/d/abx;->c:Ljava/util/Set;

    :cond_c
    return-object v0
.end method

.method public b(Ljava/lang/Object;)Z
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 85
    if-nez p1, :cond_5

    move v0, v1

    .line 93
    :goto_4
    return v0

    .line 88
    :cond_5
    iget-object v0, p0, Lcom/a/b/d/abx;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_23

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 89
    invoke-static {v0, p1}, Lcom/a/b/d/sz;->b(Ljava/util/Map;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 90
    const/4 v0, 0x1

    goto :goto_4

    :cond_23
    move v0, v1

    .line 93
    goto :goto_4
.end method

.method public c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 146
    if-eqz p1, :cond_5

    if-nez p2, :cond_7

    :cond_5
    move-object v0, v1

    .line 157
    :goto_6
    return-object v0

    .line 149
    :cond_7
    iget-object v0, p0, Lcom/a/b/d/abx;->a:Ljava/util/Map;

    invoke-static {v0, p1}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 150
    if-nez v0, :cond_13

    move-object v0, v1

    .line 151
    goto :goto_6

    .line 153
    :cond_13
    invoke-interface {v0, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 154
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 155
    iget-object v0, p0, Lcom/a/b/d/abx;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_22
    move-object v0, v1

    .line 157
    goto :goto_6
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/a/b/d/abx;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public c(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 101
    if-eqz p1, :cond_a

    invoke-super {p0, p1}, Lcom/a/b/d/bf;->c(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public d(Ljava/lang/Object;)Ljava/util/Map;
    .registers 3

    .prologue
    .line 393
    new-instance v0, Lcom/a/b/d/aca;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/aca;-><init>(Lcom/a/b/d/abx;Ljava/lang/Object;)V

    return-object v0
.end method

.method public d()V
    .registers 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/a/b/d/abx;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 126
    return-void
.end method

.method public e(Ljava/lang/Object;)Ljava/util/Map;
    .registers 3

    .prologue
    .line 255
    new-instance v0, Lcom/a/b/d/acm;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/acm;-><init>(Lcom/a/b/d/abx;Ljava/lang/Object;)V

    return-object v0
.end method

.method public e()Ljava/util/Set;
    .registers 2

    .prologue
    .line 218
    invoke-super {p0}, Lcom/a/b/d/bf;->e()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method final g()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 222
    new-instance v0, Lcom/a/b/d/abz;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/abz;-><init>(Lcom/a/b/d/abx;B)V

    return-object v0
.end method

.method public h()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 687
    invoke-super {p0}, Lcom/a/b/d/bf;->h()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public k()I
    .registers 4

    .prologue
    .line 115
    const/4 v0, 0x0

    .line 116
    iget-object v1, p0, Lcom/a/b/d/abx;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 117
    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 118
    goto :goto_c

    .line 119
    :cond_1f
    return v1
.end method

.method public l()Ljava/util/Map;
    .registers 3

    .prologue
    .line 759
    iget-object v0, p0, Lcom/a/b/d/abx;->e:Lcom/a/b/d/aci;

    .line 760
    if-nez v0, :cond_c

    new-instance v0, Lcom/a/b/d/aci;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/aci;-><init>(Lcom/a/b/d/abx;B)V

    iput-object v0, p0, Lcom/a/b/d/abx;->e:Lcom/a/b/d/aci;

    :cond_c
    return-object v0
.end method

.method public m()Ljava/util/Map;
    .registers 2

    .prologue
    .line 693
    iget-object v0, p0, Lcom/a/b/d/abx;->d:Ljava/util/Map;

    .line 694
    if-nez v0, :cond_a

    invoke-virtual {p0}, Lcom/a/b/d/abx;->n()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/abx;->d:Ljava/util/Map;

    :cond_a
    return-object v0
.end method

.method n()Ljava/util/Map;
    .registers 2

    .prologue
    .line 698
    new-instance v0, Lcom/a/b/d/acq;

    invoke-direct {v0, p0}, Lcom/a/b/d/acq;-><init>(Lcom/a/b/d/abx;)V

    return-object v0
.end method

.method o()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 653
    new-instance v0, Lcom/a/b/d/acg;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/acg;-><init>(Lcom/a/b/d/abx;B)V

    return-object v0
.end method
