.class Lcom/a/b/d/z;
.super Lcom/a/b/d/q;
.source "SourceFile"

# interfaces
.implements Ljava/util/SortedMap;


# instance fields
.field d:Ljava/util/SortedSet;

.field final synthetic e:Lcom/a/b/d/n;


# direct methods
.method constructor <init>(Lcom/a/b/d/n;Ljava/util/SortedMap;)V
    .registers 3

    .prologue
    .line 1365
    iput-object p1, p0, Lcom/a/b/d/z;->e:Lcom/a/b/d/n;

    .line 1366
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/q;-><init>(Lcom/a/b/d/n;Ljava/util/Map;)V

    .line 1367
    return-void
.end method


# virtual methods
.method b()Ljava/util/SortedSet;
    .registers 4

    .prologue
    .line 1414
    new-instance v0, Lcom/a/b/d/aa;

    iget-object v1, p0, Lcom/a/b/d/z;->e:Lcom/a/b/d/n;

    invoke-virtual {p0}, Lcom/a/b/d/z;->d()Ljava/util/SortedMap;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/aa;-><init>(Lcom/a/b/d/n;Ljava/util/SortedMap;)V

    return-object v0
.end method

.method public c()Ljava/util/SortedSet;
    .registers 2

    .prologue
    .line 1408
    iget-object v0, p0, Lcom/a/b/d/z;->d:Ljava/util/SortedSet;

    .line 1409
    if-nez v0, :cond_a

    invoke-virtual {p0}, Lcom/a/b/d/z;->b()Ljava/util/SortedSet;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/z;->d:Ljava/util/SortedSet;

    :cond_a
    return-object v0
.end method

.method public comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 1375
    invoke-virtual {p0}, Lcom/a/b/d/z;->d()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method d()Ljava/util/SortedMap;
    .registers 2

    .prologue
    .line 1370
    iget-object v0, p0, Lcom/a/b/d/z;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    return-object v0
.end method

.method synthetic e()Ljava/util/Set;
    .registers 2

    .prologue
    .line 1363
    invoke-virtual {p0}, Lcom/a/b/d/z;->b()Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public firstKey()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1380
    invoke-virtual {p0}, Lcom/a/b/d/z;->d()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->firstKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 5

    .prologue
    .line 1390
    new-instance v0, Lcom/a/b/d/z;

    iget-object v1, p0, Lcom/a/b/d/z;->e:Lcom/a/b/d/n;

    invoke-virtual {p0}, Lcom/a/b/d/z;->d()Ljava/util/SortedMap;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/SortedMap;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/z;-><init>(Lcom/a/b/d/n;Ljava/util/SortedMap;)V

    return-object v0
.end method

.method public synthetic keySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 1363
    invoke-virtual {p0}, Lcom/a/b/d/z;->c()Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public lastKey()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1385
    invoke-virtual {p0}, Lcom/a/b/d/z;->d()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 6

    .prologue
    .line 1395
    new-instance v0, Lcom/a/b/d/z;

    iget-object v1, p0, Lcom/a/b/d/z;->e:Lcom/a/b/d/n;

    invoke-virtual {p0}, Lcom/a/b/d/z;->d()Ljava/util/SortedMap;

    move-result-object v2

    invoke-interface {v2, p1, p2}, Ljava/util/SortedMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/z;-><init>(Lcom/a/b/d/n;Ljava/util/SortedMap;)V

    return-object v0
.end method

.method public tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 5

    .prologue
    .line 1400
    new-instance v0, Lcom/a/b/d/z;

    iget-object v1, p0, Lcom/a/b/d/z;->e:Lcom/a/b/d/n;

    invoke-virtual {p0}, Lcom/a/b/d/z;->d()Ljava/util/SortedMap;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/SortedMap;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/z;-><init>(Lcom/a/b/d/n;Ljava/util/SortedMap;)V

    return-object v0
.end method
