.class final Lcom/a/b/d/of;
.super Lcom/a/b/d/aan;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/oh;


# annotations
.annotation build Lcom/a/b/a/d;
.end annotation


# instance fields
.field a:[Lcom/a/b/d/oe;
    .annotation build Lcom/a/b/a/d;
    .end annotation
.end field

.field final synthetic b:Lcom/a/b/d/oc;

.field private final c:Ljava/lang/Object;

.field private d:I

.field private e:I

.field private f:Lcom/a/b/d/oh;

.field private g:Lcom/a/b/d/oh;


# direct methods
.method constructor <init>(Lcom/a/b/d/oc;Ljava/lang/Object;I)V
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 319
    iput-object p1, p0, Lcom/a/b/d/of;->b:Lcom/a/b/d/oc;

    invoke-direct {p0}, Lcom/a/b/d/aan;-><init>()V

    .line 311
    iput v0, p0, Lcom/a/b/d/of;->d:I

    .line 312
    iput v0, p0, Lcom/a/b/d/of;->e:I

    .line 320
    iput-object p2, p0, Lcom/a/b/d/of;->c:Ljava/lang/Object;

    .line 321
    iput-object p0, p0, Lcom/a/b/d/of;->f:Lcom/a/b/d/oh;

    .line 322
    iput-object p0, p0, Lcom/a/b/d/of;->g:Lcom/a/b/d/oh;

    .line 324
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    invoke-static {p3, v0, v1}, Lcom/a/b/d/iq;->a(ID)I

    move-result v0

    .line 327
    new-array v0, v0, [Lcom/a/b/d/oe;

    .line 328
    iput-object v0, p0, Lcom/a/b/d/of;->a:[Lcom/a/b/d/oe;

    .line 329
    return-void
.end method

.method static synthetic a(Lcom/a/b/d/of;)Lcom/a/b/d/oh;
    .registers 2

    .prologue
    .line 303
    iget-object v0, p0, Lcom/a/b/d/of;->f:Lcom/a/b/d/oh;

    return-object v0
.end method

.method static synthetic b(Lcom/a/b/d/of;)I
    .registers 2

    .prologue
    .line 303
    iget v0, p0, Lcom/a/b/d/of;->e:I

    return v0
.end method

.method private c()I
    .registers 2

    .prologue
    .line 332
    iget-object v0, p0, Lcom/a/b/d/of;->a:[Lcom/a/b/d/oe;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method private d()V
    .registers 7

    .prologue
    .line 439
    iget v0, p0, Lcom/a/b/d/of;->d:I

    iget-object v1, p0, Lcom/a/b/d/of;->a:[Lcom/a/b/d/oe;

    array-length v1, v1

    invoke-static {v0, v1}, Lcom/a/b/d/iq;->a(II)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 441
    iget-object v0, p0, Lcom/a/b/d/of;->a:[Lcom/a/b/d/oe;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x2

    new-array v2, v0, [Lcom/a/b/d/oe;

    .line 442
    iput-object v2, p0, Lcom/a/b/d/of;->a:[Lcom/a/b/d/oe;

    .line 443
    array-length v0, v2

    add-int/lit8 v3, v0, -0x1

    .line 444
    iget-object v1, p0, Lcom/a/b/d/of;->f:Lcom/a/b/d/oh;

    .line 445
    :goto_19
    if-eq v1, p0, :cond_2c

    move-object v0, v1

    .line 446
    check-cast v0, Lcom/a/b/d/oe;

    .line 447
    iget v4, v0, Lcom/a/b/d/oe;->a:I

    and-int/2addr v4, v3

    .line 448
    aget-object v5, v2, v4

    iput-object v5, v0, Lcom/a/b/d/oe;->b:Lcom/a/b/d/oe;

    .line 449
    aput-object v0, v2, v4

    .line 445
    invoke-interface {v1}, Lcom/a/b/d/oh;->b()Lcom/a/b/d/oh;

    move-result-object v1

    goto :goto_19

    .line 452
    :cond_2c
    return-void
.end method


# virtual methods
.method public final a()Lcom/a/b/d/oh;
    .registers 2

    .prologue
    .line 337
    iget-object v0, p0, Lcom/a/b/d/of;->g:Lcom/a/b/d/oh;

    return-object v0
.end method

.method public final a(Lcom/a/b/d/oh;)V
    .registers 2

    .prologue
    .line 347
    iput-object p1, p0, Lcom/a/b/d/of;->g:Lcom/a/b/d/oh;

    .line 348
    return-void
.end method

.method public final add(Ljava/lang/Object;)Z
    .registers 8
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 416
    invoke-static {p1}, Lcom/a/b/d/iq;->a(Ljava/lang/Object;)I

    move-result v2

    .line 2332
    iget-object v0, p0, Lcom/a/b/d/of;->a:[Lcom/a/b/d/oe;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    .line 417
    and-int v3, v2, v0

    .line 418
    iget-object v0, p0, Lcom/a/b/d/of;->a:[Lcom/a/b/d/oe;

    aget-object v1, v0, v3

    move-object v0, v1

    .line 419
    :goto_10
    if-eqz v0, :cond_1d

    .line 421
    invoke-virtual {v0, p1, v2}, Lcom/a/b/d/oe;->a(Ljava/lang/Object;I)Z

    move-result v4

    if-eqz v4, :cond_1a

    .line 422
    const/4 v0, 0x0

    .line 435
    :goto_19
    return v0

    .line 420
    :cond_1a
    iget-object v0, v0, Lcom/a/b/d/oe;->b:Lcom/a/b/d/oe;

    goto :goto_10

    .line 426
    :cond_1d
    new-instance v0, Lcom/a/b/d/oe;

    iget-object v4, p0, Lcom/a/b/d/of;->c:Ljava/lang/Object;

    invoke-direct {v0, v4, p1, v2, v1}, Lcom/a/b/d/oe;-><init>(Ljava/lang/Object;Ljava/lang/Object;ILcom/a/b/d/oe;)V

    .line 427
    iget-object v1, p0, Lcom/a/b/d/of;->g:Lcom/a/b/d/oh;

    invoke-static {v1, v0}, Lcom/a/b/d/oc;->a(Lcom/a/b/d/oh;Lcom/a/b/d/oh;)V

    .line 428
    invoke-static {v0, p0}, Lcom/a/b/d/oc;->a(Lcom/a/b/d/oh;Lcom/a/b/d/oh;)V

    .line 429
    iget-object v1, p0, Lcom/a/b/d/of;->b:Lcom/a/b/d/oc;

    invoke-static {v1}, Lcom/a/b/d/oc;->a(Lcom/a/b/d/oc;)Lcom/a/b/d/oe;

    move-result-object v1

    .line 3202
    iget-object v1, v1, Lcom/a/b/d/oe;->g:Lcom/a/b/d/oe;

    .line 429
    invoke-static {v1, v0}, Lcom/a/b/d/oc;->a(Lcom/a/b/d/oe;Lcom/a/b/d/oe;)V

    .line 430
    iget-object v1, p0, Lcom/a/b/d/of;->b:Lcom/a/b/d/oc;

    invoke-static {v1}, Lcom/a/b/d/oc;->a(Lcom/a/b/d/oc;)Lcom/a/b/d/oe;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/oc;->a(Lcom/a/b/d/oe;Lcom/a/b/d/oe;)V

    .line 431
    iget-object v1, p0, Lcom/a/b/d/of;->a:[Lcom/a/b/d/oe;

    aput-object v0, v1, v3

    .line 432
    iget v0, p0, Lcom/a/b/d/of;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/of;->d:I

    .line 433
    iget v0, p0, Lcom/a/b/d/of;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/of;->e:I

    .line 3439
    iget v0, p0, Lcom/a/b/d/of;->d:I

    iget-object v1, p0, Lcom/a/b/d/of;->a:[Lcom/a/b/d/oe;

    array-length v1, v1

    invoke-static {v0, v1}, Lcom/a/b/d/iq;->a(II)Z

    move-result v0

    if-eqz v0, :cond_7c

    .line 3441
    iget-object v0, p0, Lcom/a/b/d/of;->a:[Lcom/a/b/d/oe;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x2

    new-array v2, v0, [Lcom/a/b/d/oe;

    .line 3442
    iput-object v2, p0, Lcom/a/b/d/of;->a:[Lcom/a/b/d/oe;

    .line 3443
    array-length v0, v2

    add-int/lit8 v3, v0, -0x1

    .line 3444
    iget-object v1, p0, Lcom/a/b/d/of;->f:Lcom/a/b/d/oh;

    .line 3445
    :goto_69
    if-eq v1, p0, :cond_7c

    move-object v0, v1

    .line 3446
    check-cast v0, Lcom/a/b/d/oe;

    .line 3447
    iget v4, v0, Lcom/a/b/d/oe;->a:I

    and-int/2addr v4, v3

    .line 3448
    aget-object v5, v2, v4

    iput-object v5, v0, Lcom/a/b/d/oe;->b:Lcom/a/b/d/oe;

    .line 3449
    aput-object v0, v2, v4

    .line 3445
    invoke-interface {v1}, Lcom/a/b/d/oh;->b()Lcom/a/b/d/oh;

    move-result-object v1

    goto :goto_69

    .line 435
    :cond_7c
    const/4 v0, 0x1

    goto :goto_19
.end method

.method public final b()Lcom/a/b/d/oh;
    .registers 2

    .prologue
    .line 342
    iget-object v0, p0, Lcom/a/b/d/of;->f:Lcom/a/b/d/oh;

    return-object v0
.end method

.method public final b(Lcom/a/b/d/oh;)V
    .registers 2

    .prologue
    .line 352
    iput-object p1, p0, Lcom/a/b/d/of;->f:Lcom/a/b/d/oh;

    .line 353
    return-void
.end method

.method public final clear()V
    .registers 3

    .prologue
    .line 480
    iget-object v0, p0, Lcom/a/b/d/of;->a:[Lcom/a/b/d/oe;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 481
    const/4 v0, 0x0

    iput v0, p0, Lcom/a/b/d/of;->d:I

    .line 482
    iget-object v1, p0, Lcom/a/b/d/of;->f:Lcom/a/b/d/oh;

    .line 483
    :goto_b
    if-eq v1, p0, :cond_18

    move-object v0, v1

    .line 484
    check-cast v0, Lcom/a/b/d/oe;

    .line 485
    invoke-static {v0}, Lcom/a/b/d/oc;->a(Lcom/a/b/d/oe;)V

    .line 483
    invoke-interface {v1}, Lcom/a/b/d/oh;->b()Lcom/a/b/d/oh;

    move-result-object v1

    goto :goto_b

    .line 487
    :cond_18
    invoke-static {p0, p0}, Lcom/a/b/d/oc;->a(Lcom/a/b/d/oh;Lcom/a/b/d/oh;)V

    .line 488
    iget v0, p0, Lcom/a/b/d/of;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/of;->e:I

    .line 489
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 404
    invoke-static {p1}, Lcom/a/b/d/iq;->a(Ljava/lang/Object;)I

    move-result v1

    .line 405
    iget-object v0, p0, Lcom/a/b/d/of;->a:[Lcom/a/b/d/oe;

    .line 1332
    iget-object v2, p0, Lcom/a/b/d/of;->a:[Lcom/a/b/d/oe;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    .line 405
    and-int/2addr v2, v1

    aget-object v0, v0, v2

    :goto_e
    if-eqz v0, :cond_1b

    .line 407
    invoke-virtual {v0, p1, v1}, Lcom/a/b/d/oe;->a(Ljava/lang/Object;I)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 408
    const/4 v0, 0x1

    .line 411
    :goto_17
    return v0

    .line 406
    :cond_18
    iget-object v0, v0, Lcom/a/b/d/oe;->b:Lcom/a/b/d/oe;

    goto :goto_e

    .line 411
    :cond_1b
    const/4 v0, 0x0

    goto :goto_17
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 357
    new-instance v0, Lcom/a/b/d/og;

    invoke-direct {v0, p0}, Lcom/a/b/d/og;-><init>(Lcom/a/b/d/of;)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .registers 8
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 456
    invoke-static {p1}, Lcom/a/b/d/iq;->a(Ljava/lang/Object;)I

    move-result v2

    .line 4332
    iget-object v0, p0, Lcom/a/b/d/of;->a:[Lcom/a/b/d/oe;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    .line 457
    and-int v3, v2, v0

    .line 458
    const/4 v1, 0x0

    .line 459
    iget-object v0, p0, Lcom/a/b/d/of;->a:[Lcom/a/b/d/oe;

    aget-object v0, v0, v3

    :goto_10
    if-eqz v0, :cond_3f

    .line 461
    invoke-virtual {v0, p1, v2}, Lcom/a/b/d/oe;->a(Ljava/lang/Object;I)Z

    move-result v4

    if-eqz v4, :cond_39

    .line 462
    if-nez v1, :cond_34

    .line 464
    iget-object v1, p0, Lcom/a/b/d/of;->a:[Lcom/a/b/d/oe;

    iget-object v2, v0, Lcom/a/b/d/oe;->b:Lcom/a/b/d/oe;

    aput-object v2, v1, v3

    .line 468
    :goto_20
    invoke-static {v0}, Lcom/a/b/d/oc;->a(Lcom/a/b/d/oh;)V

    .line 469
    invoke-static {v0}, Lcom/a/b/d/oc;->a(Lcom/a/b/d/oe;)V

    .line 470
    iget v0, p0, Lcom/a/b/d/of;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/b/d/of;->d:I

    .line 471
    iget v0, p0, Lcom/a/b/d/of;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/of;->e:I

    .line 472
    const/4 v0, 0x1

    .line 475
    :goto_33
    return v0

    .line 466
    :cond_34
    iget-object v2, v0, Lcom/a/b/d/oe;->b:Lcom/a/b/d/oe;

    iput-object v2, v1, Lcom/a/b/d/oe;->b:Lcom/a/b/d/oe;

    goto :goto_20

    .line 460
    :cond_39
    iget-object v1, v0, Lcom/a/b/d/oe;->b:Lcom/a/b/d/oe;

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_10

    .line 475
    :cond_3f
    const/4 v0, 0x0

    goto :goto_33
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 399
    iget v0, p0, Lcom/a/b/d/of;->d:I

    return v0
.end method
