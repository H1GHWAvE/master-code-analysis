.class final Lcom/a/b/d/hs;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
.end annotation


# instance fields
.field final a:Ljava/util/Comparator;

.field final b:Z

.field final c:Ljava/lang/Object;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field final d:Lcom/a/b/d/ce;

.field final e:Z

.field final f:Ljava/lang/Object;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field final g:Lcom/a/b/d/ce;

.field private transient h:Lcom/a/b/d/hs;


# direct methods
.method constructor <init>(Ljava/util/Comparator;ZLjava/lang/Object;Lcom/a/b/d/ce;ZLjava/lang/Object;Lcom/a/b/d/ce;)V
    .registers 14
    .param p3    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    iput-object v0, p0, Lcom/a/b/d/hs;->a:Ljava/util/Comparator;

    .line 104
    iput-boolean p2, p0, Lcom/a/b/d/hs;->b:Z

    .line 105
    iput-boolean p5, p0, Lcom/a/b/d/hs;->e:Z

    .line 106
    iput-object p3, p0, Lcom/a/b/d/hs;->c:Ljava/lang/Object;

    .line 107
    invoke-static {p4}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/ce;

    iput-object v0, p0, Lcom/a/b/d/hs;->d:Lcom/a/b/d/ce;

    .line 108
    iput-object p6, p0, Lcom/a/b/d/hs;->f:Ljava/lang/Object;

    .line 109
    invoke-static {p7}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/ce;

    iput-object v0, p0, Lcom/a/b/d/hs;->g:Lcom/a/b/d/ce;

    .line 111
    if-eqz p2, :cond_2a

    .line 112
    invoke-interface {p1, p3, p3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    .line 114
    :cond_2a
    if-eqz p5, :cond_2f

    .line 115
    invoke-interface {p1, p6, p6}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    .line 117
    :cond_2f
    if-eqz p2, :cond_55

    if-eqz p5, :cond_55

    .line 118
    invoke-interface {p1, p3, p6}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    .line 120
    if-gtz v3, :cond_56

    move v0, v1

    :goto_3a
    const-string v4, "lowerEndpoint (%s) > upperEndpoint (%s)"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p3, v5, v2

    aput-object p6, v5, v1

    invoke-static {v0, v4, v5}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 122
    if-nez v3, :cond_55

    .line 123
    sget-object v0, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    if-eq p4, v0, :cond_58

    move v0, v1

    :goto_4d
    sget-object v3, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    if-eq p7, v3, :cond_5a

    :goto_51
    or-int/2addr v0, v1

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 126
    :cond_55
    return-void

    :cond_56
    move v0, v2

    .line 120
    goto :goto_3a

    :cond_58
    move v0, v2

    .line 123
    goto :goto_4d

    :cond_5a
    move v1, v2

    goto :goto_51
.end method

.method private static a(Lcom/a/b/d/yl;)Lcom/a/b/d/hs;
    .registers 9

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-virtual {p0}, Lcom/a/b/d/yl;->d()Z

    move-result v1

    if-eqz v1, :cond_43

    .line 1383
    iget-object v1, p0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v1}, Lcom/a/b/d/dw;->c()Ljava/lang/Comparable;

    move-result-object v3

    .line 47
    :goto_d
    invoke-virtual {p0}, Lcom/a/b/d/yl;->d()Z

    move-result v1

    if-eqz v1, :cond_45

    .line 1394
    iget-object v1, p0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v1}, Lcom/a/b/d/dw;->a()Lcom/a/b/d/ce;

    move-result-object v4

    .line 50
    :goto_19
    invoke-virtual {p0}, Lcom/a/b/d/yl;->e()Z

    move-result v1

    if-eqz v1, :cond_48

    .line 1411
    iget-object v0, p0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v0}, Lcom/a/b/d/dw;->c()Ljava/lang/Comparable;

    move-result-object v6

    .line 51
    :goto_25
    invoke-virtual {p0}, Lcom/a/b/d/yl;->e()Z

    move-result v0

    if-eqz v0, :cond_4a

    .line 1422
    iget-object v0, p0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v0}, Lcom/a/b/d/dw;->b()Lcom/a/b/d/ce;

    move-result-object v7

    .line 52
    :goto_31
    new-instance v0, Lcom/a/b/d/hs;

    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v1

    invoke-virtual {p0}, Lcom/a/b/d/yl;->d()Z

    move-result v2

    invoke-virtual {p0}, Lcom/a/b/d/yl;->e()Z

    move-result v5

    invoke-direct/range {v0 .. v7}, Lcom/a/b/d/hs;-><init>(Ljava/util/Comparator;ZLjava/lang/Object;Lcom/a/b/d/ce;ZLjava/lang/Object;Lcom/a/b/d/ce;)V

    return-object v0

    :cond_43
    move-object v3, v0

    .line 46
    goto :goto_d

    .line 47
    :cond_45
    sget-object v4, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    goto :goto_19

    :cond_48
    move-object v6, v0

    .line 50
    goto :goto_25

    .line 51
    :cond_4a
    sget-object v7, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    goto :goto_31
.end method

.method static a(Ljava/util/Comparator;)Lcom/a/b/d/hs;
    .registers 9

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 60
    new-instance v0, Lcom/a/b/d/hs;

    sget-object v4, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    sget-object v7, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    move-object v1, p0

    move v5, v2

    move-object v6, v3

    invoke-direct/range {v0 .. v7}, Lcom/a/b/d/hs;-><init>(Ljava/util/Comparator;ZLjava/lang/Object;Lcom/a/b/d/ce;ZLjava/lang/Object;Lcom/a/b/d/ce;)V

    return-object v0
.end method

.method private static a(Ljava/util/Comparator;Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/hs;
    .registers 11
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 69
    new-instance v0, Lcom/a/b/d/hs;

    const/4 v2, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    sget-object v7, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v7}, Lcom/a/b/d/hs;-><init>(Ljava/util/Comparator;ZLjava/lang/Object;Lcom/a/b/d/ce;ZLjava/lang/Object;Lcom/a/b/d/ce;)V

    return-object v0
.end method

.method private static a(Ljava/util/Comparator;Ljava/lang/Object;Lcom/a/b/d/ce;Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/hs;
    .registers 13
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    .line 87
    new-instance v0, Lcom/a/b/d/hs;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move v5, v2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/a/b/d/hs;-><init>(Ljava/util/Comparator;ZLjava/lang/Object;Lcom/a/b/d/ce;ZLjava/lang/Object;Lcom/a/b/d/ce;)V

    return-object v0
.end method

.method private a()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/a/b/d/hs;->a:Ljava/util/Comparator;

    return-object v0
.end method

.method private static b(Ljava/util/Comparator;Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/hs;
    .registers 11
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 78
    new-instance v0, Lcom/a/b/d/hs;

    const/4 v2, 0x0

    const/4 v3, 0x0

    sget-object v4, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    const/4 v5, 0x1

    move-object v1, p0

    move-object v6, p1

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, Lcom/a/b/d/hs;-><init>(Ljava/util/Comparator;ZLjava/lang/Object;Lcom/a/b/d/ce;ZLjava/lang/Object;Lcom/a/b/d/ce;)V

    return-object v0
.end method

.method private b()Z
    .registers 2

    .prologue
    .line 133
    iget-boolean v0, p0, Lcom/a/b/d/hs;->b:Z

    return v0
.end method

.method private c()Z
    .registers 2

    .prologue
    .line 137
    iget-boolean v0, p0, Lcom/a/b/d/hs;->e:Z

    return v0
.end method

.method private d()Z
    .registers 2

    .prologue
    .line 141
    .line 2137
    iget-boolean v0, p0, Lcom/a/b/d/hs;->e:Z

    .line 141
    if-eqz v0, :cond_c

    .line 2277
    iget-object v0, p0, Lcom/a/b/d/hs;->f:Ljava/lang/Object;

    .line 141
    invoke-virtual {p0, v0}, Lcom/a/b/d/hs;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_18

    .line 3133
    :cond_c
    iget-boolean v0, p0, Lcom/a/b/d/hs;->b:Z

    .line 141
    if-eqz v0, :cond_1a

    .line 3269
    iget-object v0, p0, Lcom/a/b/d/hs;->c:Ljava/lang/Object;

    .line 141
    invoke-virtual {p0, v0}, Lcom/a/b/d/hs;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    :cond_18
    const/4 v0, 0x1

    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_19
.end method

.method private e()Lcom/a/b/d/hs;
    .registers 9

    .prologue
    .line 244
    iget-object v0, p0, Lcom/a/b/d/hs;->h:Lcom/a/b/d/hs;

    .line 245
    if-nez v0, :cond_23

    .line 246
    new-instance v0, Lcom/a/b/d/hs;

    iget-object v1, p0, Lcom/a/b/d/hs;->a:Ljava/util/Comparator;

    invoke-static {v1}, Lcom/a/b/d/yd;->a(Ljava/util/Comparator;)Lcom/a/b/d/yd;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/b/d/yd;->a()Lcom/a/b/d/yd;

    move-result-object v1

    iget-boolean v2, p0, Lcom/a/b/d/hs;->e:Z

    .line 22277
    iget-object v3, p0, Lcom/a/b/d/hs;->f:Ljava/lang/Object;

    .line 22281
    iget-object v4, p0, Lcom/a/b/d/hs;->g:Lcom/a/b/d/ce;

    .line 246
    iget-boolean v5, p0, Lcom/a/b/d/hs;->b:Z

    .line 23269
    iget-object v6, p0, Lcom/a/b/d/hs;->c:Ljava/lang/Object;

    .line 23273
    iget-object v7, p0, Lcom/a/b/d/hs;->d:Lcom/a/b/d/ce;

    .line 246
    invoke-direct/range {v0 .. v7}, Lcom/a/b/d/hs;-><init>(Ljava/util/Comparator;ZLjava/lang/Object;Lcom/a/b/d/ce;ZLjava/lang/Object;Lcom/a/b/d/ce;)V

    .line 249
    iput-object p0, v0, Lcom/a/b/d/hs;->h:Lcom/a/b/d/hs;

    .line 250
    iput-object v0, p0, Lcom/a/b/d/hs;->h:Lcom/a/b/d/hs;

    .line 252
    :cond_23
    return-object v0
.end method

.method private f()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 269
    iget-object v0, p0, Lcom/a/b/d/hs;->c:Ljava/lang/Object;

    return-object v0
.end method

.method private g()Lcom/a/b/d/ce;
    .registers 2

    .prologue
    .line 273
    iget-object v0, p0, Lcom/a/b/d/hs;->d:Lcom/a/b/d/ce;

    return-object v0
.end method

.method private h()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 277
    iget-object v0, p0, Lcom/a/b/d/hs;->f:Ljava/lang/Object;

    return-object v0
.end method

.method private i()Lcom/a/b/d/ce;
    .registers 2

    .prologue
    .line 281
    iget-object v0, p0, Lcom/a/b/d/hs;->g:Lcom/a/b/d/ce;

    return-object v0
.end method


# virtual methods
.method final a(Lcom/a/b/d/hs;)Lcom/a/b/d/hs;
    .registers 12

    .prologue
    .line 171
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    iget-object v0, p0, Lcom/a/b/d/hs;->a:Ljava/util/Comparator;

    iget-object v1, p1, Lcom/a/b/d/hs;->a:Ljava/util/Comparator;

    invoke-interface {v0, v1}, Ljava/util/Comparator;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 174
    iget-boolean v0, p0, Lcom/a/b/d/hs;->b:Z

    .line 6269
    iget-object v2, p0, Lcom/a/b/d/hs;->c:Ljava/lang/Object;

    .line 6273
    iget-object v1, p0, Lcom/a/b/d/hs;->d:Lcom/a/b/d/ce;

    .line 7133
    iget-boolean v3, p0, Lcom/a/b/d/hs;->b:Z

    .line 178
    if-nez v3, :cond_57

    .line 179
    iget-boolean v0, p1, Lcom/a/b/d/hs;->b:Z

    .line 10269
    :cond_1a
    iget-object v2, p1, Lcom/a/b/d/hs;->c:Ljava/lang/Object;

    .line 10273
    iget-object v1, p1, Lcom/a/b/d/hs;->d:Lcom/a/b/d/ce;

    move-object v9, v1

    move-object v1, v2

    move v2, v0

    move-object v0, v9

    .line 190
    :goto_22
    iget-boolean v3, p0, Lcom/a/b/d/hs;->e:Z

    .line 10277
    iget-object v6, p0, Lcom/a/b/d/hs;->f:Ljava/lang/Object;

    .line 10281
    iget-object v7, p0, Lcom/a/b/d/hs;->g:Lcom/a/b/d/ce;

    .line 11137
    iget-boolean v4, p0, Lcom/a/b/d/hs;->e:Z

    .line 194
    if-nez v4, :cond_74

    .line 195
    iget-boolean v3, p1, Lcom/a/b/d/hs;->e:Z

    .line 14277
    :cond_2e
    iget-object v6, p1, Lcom/a/b/d/hs;->f:Ljava/lang/Object;

    .line 14281
    iget-object v7, p1, Lcom/a/b/d/hs;->g:Lcom/a/b/d/ce;

    move v5, v3

    .line 206
    :goto_33
    if-eqz v2, :cond_8e

    if-eqz v5, :cond_8e

    .line 207
    iget-object v3, p0, Lcom/a/b/d/hs;->a:Ljava/util/Comparator;

    invoke-interface {v3, v1, v6}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    .line 208
    if-gtz v3, :cond_49

    if-nez v3, :cond_8e

    sget-object v3, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    if-ne v0, v3, :cond_8e

    sget-object v3, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    if-ne v7, v3, :cond_8e

    .line 211
    :cond_49
    sget-object v0, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    .line 212
    sget-object v7, Lcom/a/b/d/ce;->b:Lcom/a/b/d/ce;

    move-object v4, v0

    move-object v3, v6

    .line 216
    :goto_4f
    new-instance v0, Lcom/a/b/d/hs;

    iget-object v1, p0, Lcom/a/b/d/hs;->a:Ljava/util/Comparator;

    invoke-direct/range {v0 .. v7}, Lcom/a/b/d/hs;-><init>(Ljava/util/Comparator;ZLjava/lang/Object;Lcom/a/b/d/ce;ZLjava/lang/Object;Lcom/a/b/d/ce;)V

    return-object v0

    .line 8133
    :cond_57
    iget-boolean v3, p1, Lcom/a/b/d/hs;->b:Z

    .line 182
    if-eqz v3, :cond_6f

    .line 183
    iget-object v3, p0, Lcom/a/b/d/hs;->a:Ljava/util/Comparator;

    .line 8269
    iget-object v4, p0, Lcom/a/b/d/hs;->c:Ljava/lang/Object;

    .line 9269
    iget-object v5, p1, Lcom/a/b/d/hs;->c:Ljava/lang/Object;

    .line 183
    invoke-interface {v3, v4, v5}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    .line 184
    if-ltz v3, :cond_1a

    if-nez v3, :cond_6f

    .line 9273
    iget-object v3, p1, Lcom/a/b/d/hs;->d:Lcom/a/b/d/ce;

    .line 184
    sget-object v4, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    if-eq v3, v4, :cond_1a

    :cond_6f
    move-object v9, v1

    move-object v1, v2

    move v2, v0

    move-object v0, v9

    goto :goto_22

    .line 12137
    :cond_74
    iget-boolean v4, p1, Lcom/a/b/d/hs;->e:Z

    .line 198
    if-eqz v4, :cond_8c

    .line 199
    iget-object v4, p0, Lcom/a/b/d/hs;->a:Ljava/util/Comparator;

    .line 12277
    iget-object v5, p0, Lcom/a/b/d/hs;->f:Ljava/lang/Object;

    .line 13277
    iget-object v8, p1, Lcom/a/b/d/hs;->f:Ljava/lang/Object;

    .line 199
    invoke-interface {v4, v5, v8}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v4

    .line 200
    if-gtz v4, :cond_2e

    if-nez v4, :cond_8c

    .line 13281
    iget-object v4, p1, Lcom/a/b/d/hs;->g:Lcom/a/b/d/ce;

    .line 200
    sget-object v5, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    if-eq v4, v5, :cond_2e

    :cond_8c
    move v5, v3

    goto :goto_33

    :cond_8e
    move-object v4, v0

    move-object v3, v1

    goto :goto_4f
.end method

.method final a(Ljava/lang/Object;)Z
    .registers 8
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 146
    .line 4133
    iget-boolean v0, p0, Lcom/a/b/d/hs;->b:Z

    .line 146
    if-nez v0, :cond_7

    .line 151
    :goto_6
    return v2

    .line 4269
    :cond_7
    iget-object v0, p0, Lcom/a/b/d/hs;->c:Ljava/lang/Object;

    .line 150
    iget-object v3, p0, Lcom/a/b/d/hs;->a:Ljava/util/Comparator;

    invoke-interface {v3, p1, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 151
    if-gez v0, :cond_1f

    move v3, v1

    :goto_12
    if-nez v0, :cond_21

    move v0, v1

    .line 4273
    :goto_15
    iget-object v4, p0, Lcom/a/b/d/hs;->d:Lcom/a/b/d/ce;

    .line 151
    sget-object v5, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    if-ne v4, v5, :cond_23

    :goto_1b
    and-int/2addr v0, v1

    or-int v2, v3, v0

    goto :goto_6

    :cond_1f
    move v3, v2

    goto :goto_12

    :cond_21
    move v0, v2

    goto :goto_15

    :cond_23
    move v1, v2

    goto :goto_1b
.end method

.method final b(Ljava/lang/Object;)Z
    .registers 8
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 155
    .line 5137
    iget-boolean v0, p0, Lcom/a/b/d/hs;->e:Z

    .line 155
    if-nez v0, :cond_7

    .line 160
    :goto_6
    return v2

    .line 5277
    :cond_7
    iget-object v0, p0, Lcom/a/b/d/hs;->f:Ljava/lang/Object;

    .line 159
    iget-object v3, p0, Lcom/a/b/d/hs;->a:Ljava/util/Comparator;

    invoke-interface {v3, p1, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 160
    if-lez v0, :cond_1f

    move v3, v1

    :goto_12
    if-nez v0, :cond_21

    move v0, v1

    .line 5281
    :goto_15
    iget-object v4, p0, Lcom/a/b/d/hs;->g:Lcom/a/b/d/ce;

    .line 160
    sget-object v5, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    if-ne v4, v5, :cond_23

    :goto_1b
    and-int/2addr v0, v1

    or-int v2, v3, v0

    goto :goto_6

    :cond_1f
    move v3, v2

    goto :goto_12

    :cond_21
    move v0, v2

    goto :goto_15

    :cond_23
    move v1, v2

    goto :goto_1b
.end method

.method final c(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 164
    invoke-virtual {p0, p1}, Lcom/a/b/d/hs;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    invoke-virtual {p0, p1}, Lcom/a/b/d/hs;->b(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 221
    instance-of v1, p1, Lcom/a/b/d/hs;

    if-eqz v1, :cond_46

    .line 222
    check-cast p1, Lcom/a/b/d/hs;

    .line 223
    iget-object v1, p0, Lcom/a/b/d/hs;->a:Ljava/util/Comparator;

    iget-object v2, p1, Lcom/a/b/d/hs;->a:Ljava/util/Comparator;

    invoke-interface {v1, v2}, Ljava/util/Comparator;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_46

    iget-boolean v1, p0, Lcom/a/b/d/hs;->b:Z

    iget-boolean v2, p1, Lcom/a/b/d/hs;->b:Z

    if-ne v1, v2, :cond_46

    iget-boolean v1, p0, Lcom/a/b/d/hs;->e:Z

    iget-boolean v2, p1, Lcom/a/b/d/hs;->e:Z

    if-ne v1, v2, :cond_46

    .line 15273
    iget-object v1, p0, Lcom/a/b/d/hs;->d:Lcom/a/b/d/ce;

    .line 16273
    iget-object v2, p1, Lcom/a/b/d/hs;->d:Lcom/a/b/d/ce;

    .line 223
    invoke-virtual {v1, v2}, Lcom/a/b/d/ce;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_46

    .line 16281
    iget-object v1, p0, Lcom/a/b/d/hs;->g:Lcom/a/b/d/ce;

    .line 17281
    iget-object v2, p1, Lcom/a/b/d/hs;->g:Lcom/a/b/d/ce;

    .line 223
    invoke-virtual {v1, v2}, Lcom/a/b/d/ce;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_46

    .line 18269
    iget-object v1, p0, Lcom/a/b/d/hs;->c:Ljava/lang/Object;

    .line 19269
    iget-object v2, p1, Lcom/a/b/d/hs;->c:Ljava/lang/Object;

    .line 223
    invoke-static {v1, v2}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_46

    .line 19277
    iget-object v1, p0, Lcom/a/b/d/hs;->f:Ljava/lang/Object;

    .line 20277
    iget-object v2, p1, Lcom/a/b/d/hs;->f:Ljava/lang/Object;

    .line 223
    invoke-static {v1, v2}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_46

    const/4 v0, 0x1

    .line 229
    :cond_46
    return v0
.end method

.method public final hashCode()I
    .registers 4

    .prologue
    .line 234
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/a/b/d/hs;->a:Ljava/util/Comparator;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 21269
    iget-object v2, p0, Lcom/a/b/d/hs;->c:Ljava/lang/Object;

    .line 234
    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 21273
    iget-object v2, p0, Lcom/a/b/d/hs;->d:Lcom/a/b/d/ce;

    .line 234
    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 21277
    iget-object v2, p0, Lcom/a/b/d/hs;->f:Ljava/lang/Object;

    .line 234
    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 21281
    iget-object v2, p0, Lcom/a/b/d/hs;->g:Lcom/a/b/d/ce;

    .line 234
    aput-object v2, v0, v1

    .line 22084
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    .line 234
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 257
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/a/b/d/hs;->a:Ljava/util/Comparator;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/a/b/d/hs;->d:Lcom/a/b/d/ce;

    sget-object v2, Lcom/a/b/d/ce;->b:Lcom/a/b/d/ce;

    if-ne v0, v2, :cond_48

    const/16 v0, 0x5b

    :goto_19
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Lcom/a/b/d/hs;->b:Z

    if-eqz v0, :cond_4b

    iget-object v0, p0, Lcom/a/b/d/hs;->c:Ljava/lang/Object;

    :goto_23
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Lcom/a/b/d/hs;->e:Z

    if-eqz v0, :cond_4e

    iget-object v0, p0, Lcom/a/b/d/hs;->f:Ljava/lang/Object;

    :goto_33
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/a/b/d/hs;->g:Lcom/a/b/d/ce;

    sget-object v2, Lcom/a/b/d/ce;->b:Lcom/a/b/d/ce;

    if-ne v0, v2, :cond_51

    const/16 v0, 0x5d

    :goto_3f
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_48
    const/16 v0, 0x28

    goto :goto_19

    :cond_4b
    const-string v0, "-\u221e"

    goto :goto_23

    :cond_4e
    const-string v0, "\u221e"

    goto :goto_33

    :cond_51
    const/16 v0, 0x29

    goto :goto_3f
.end method
