.class final Lcom/a/b/d/og;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field a:Lcom/a/b/d/oh;

.field b:Lcom/a/b/d/oe;

.field c:I

.field final synthetic d:Lcom/a/b/d/of;


# direct methods
.method constructor <init>(Lcom/a/b/d/of;)V
    .registers 3

    .prologue
    .line 357
    iput-object p1, p0, Lcom/a/b/d/og;->d:Lcom/a/b/d/of;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 358
    iget-object v0, p0, Lcom/a/b/d/og;->d:Lcom/a/b/d/of;

    invoke-static {v0}, Lcom/a/b/d/of;->a(Lcom/a/b/d/of;)Lcom/a/b/d/oh;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/og;->a:Lcom/a/b/d/oh;

    .line 360
    iget-object v0, p0, Lcom/a/b/d/og;->d:Lcom/a/b/d/of;

    invoke-static {v0}, Lcom/a/b/d/of;->b(Lcom/a/b/d/of;)I

    move-result v0

    iput v0, p0, Lcom/a/b/d/og;->c:I

    return-void
.end method

.method private a()V
    .registers 3

    .prologue
    .line 363
    iget-object v0, p0, Lcom/a/b/d/og;->d:Lcom/a/b/d/of;

    invoke-static {v0}, Lcom/a/b/d/of;->b(Lcom/a/b/d/of;)I

    move-result v0

    iget v1, p0, Lcom/a/b/d/og;->c:I

    if-eq v0, v1, :cond_10

    .line 364
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 366
    :cond_10
    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .registers 3

    .prologue
    .line 370
    invoke-direct {p0}, Lcom/a/b/d/og;->a()V

    .line 371
    iget-object v0, p0, Lcom/a/b/d/og;->a:Lcom/a/b/d/oh;

    iget-object v1, p0, Lcom/a/b/d/og;->d:Lcom/a/b/d/of;

    if-eq v0, v1, :cond_b

    const/4 v0, 0x1

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public final next()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 376
    invoke-virtual {p0}, Lcom/a/b/d/og;->hasNext()Z

    move-result v0

    if-nez v0, :cond_c

    .line 377
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 379
    :cond_c
    iget-object v0, p0, Lcom/a/b/d/og;->a:Lcom/a/b/d/oh;

    check-cast v0, Lcom/a/b/d/oe;

    .line 380
    invoke-virtual {v0}, Lcom/a/b/d/oe;->getValue()Ljava/lang/Object;

    move-result-object v1

    .line 381
    iput-object v0, p0, Lcom/a/b/d/og;->b:Lcom/a/b/d/oe;

    .line 1188
    iget-object v0, v0, Lcom/a/b/d/oe;->d:Lcom/a/b/d/oh;

    .line 382
    iput-object v0, p0, Lcom/a/b/d/og;->a:Lcom/a/b/d/oh;

    .line 383
    return-object v1
.end method

.method public final remove()V
    .registers 3

    .prologue
    .line 388
    invoke-direct {p0}, Lcom/a/b/d/og;->a()V

    .line 389
    iget-object v0, p0, Lcom/a/b/d/og;->b:Lcom/a/b/d/oe;

    if-eqz v0, :cond_24

    const/4 v0, 0x1

    .line 2049
    :goto_8
    const-string v1, "no calls to next() since the last call to remove()"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 390
    iget-object v0, p0, Lcom/a/b/d/og;->d:Lcom/a/b/d/of;

    iget-object v1, p0, Lcom/a/b/d/og;->b:Lcom/a/b/d/oe;

    invoke-virtual {v1}, Lcom/a/b/d/oe;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/d/of;->remove(Ljava/lang/Object;)Z

    .line 391
    iget-object v0, p0, Lcom/a/b/d/og;->d:Lcom/a/b/d/of;

    invoke-static {v0}, Lcom/a/b/d/of;->b(Lcom/a/b/d/of;)I

    move-result v0

    iput v0, p0, Lcom/a/b/d/og;->c:I

    .line 392
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/b/d/og;->b:Lcom/a/b/d/oe;

    .line 393
    return-void

    .line 389
    :cond_24
    const/4 v0, 0x0

    goto :goto_8
.end method
