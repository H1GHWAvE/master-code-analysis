.class public final Lcom/a/b/d/adx;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field private static final a:Lcom/a/b/b/bj;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 591
    new-instance v0, Lcom/a/b/d/ady;

    invoke-direct {v0}, Lcom/a/b/d/ady;-><init>()V

    sput-object v0, Lcom/a/b/d/adx;->a:Lcom/a/b/b/bj;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a()Lcom/a/b/b/bj;
    .registers 1

    .prologue
    .line 1588
    sget-object v0, Lcom/a/b/d/adx;->a:Lcom/a/b/b/bj;

    .line 52
    return-object v0
.end method

.method public static a(Lcom/a/b/d/adv;)Lcom/a/b/d/adv;
    .registers 2

    .prologue
    .line 140
    instance-of v0, p0, Lcom/a/b/d/aef;

    if-eqz v0, :cond_9

    check-cast p0, Lcom/a/b/d/aef;

    iget-object v0, p0, Lcom/a/b/d/aef;->a:Lcom/a/b/d/adv;

    :goto_8
    return-object v0

    :cond_9
    new-instance v0, Lcom/a/b/d/aef;

    invoke-direct {v0, p0}, Lcom/a/b/d/aef;-><init>(Lcom/a/b/d/adv;)V

    goto :goto_8
.end method

.method private static a(Lcom/a/b/d/adv;Lcom/a/b/b/bj;)Lcom/a/b/d/adv;
    .registers 3
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 334
    new-instance v0, Lcom/a/b/d/aeb;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/aeb;-><init>(Lcom/a/b/d/adv;Lcom/a/b/b/bj;)V

    return-object v0
.end method

.method private static a(Ljava/util/Map;Lcom/a/b/b/dz;)Lcom/a/b/d/adv;
    .registers 3
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 299
    invoke-interface {p0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 300
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 302
    new-instance v0, Lcom/a/b/d/abx;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/abx;-><init>(Ljava/util/Map;Lcom/a/b/b/dz;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/adw;
    .registers 4
    .param p0    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 67
    new-instance v0, Lcom/a/b/d/aea;

    invoke-direct {v0, p0, p1, p2}, Lcom/a/b/d/aea;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method private static a(Lcom/a/b/d/zy;)Lcom/a/b/d/zy;
    .registers 2
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 557
    new-instance v0, Lcom/a/b/d/aeh;

    invoke-direct {v0, p0}, Lcom/a/b/d/aeh;-><init>(Lcom/a/b/d/zy;)V

    return-object v0
.end method

.method static a(Lcom/a/b/d/adv;Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 600
    if-ne p1, p0, :cond_4

    .line 601
    const/4 v0, 0x1

    .line 606
    :goto_3
    return v0

    .line 602
    :cond_4
    instance-of v0, p1, Lcom/a/b/d/adv;

    if-eqz v0, :cond_17

    .line 603
    check-cast p1, Lcom/a/b/d/adv;

    .line 604
    invoke-interface {p0}, Lcom/a/b/d/adv;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {p1}, Lcom/a/b/d/adv;->e()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_3

    .line 606
    :cond_17
    const/4 v0, 0x0

    goto :goto_3
.end method

.method private static b()Lcom/a/b/b/bj;
    .registers 1

    .prologue
    .line 588
    sget-object v0, Lcom/a/b/d/adx;->a:Lcom/a/b/b/bj;

    return-object v0
.end method

.method private static b(Lcom/a/b/d/adv;)Lcom/a/b/d/adv;
    .registers 2

    .prologue
    .line 455
    new-instance v0, Lcom/a/b/d/aei;

    invoke-direct {v0, p0}, Lcom/a/b/d/aei;-><init>(Lcom/a/b/d/adv;)V

    return-object v0
.end method
