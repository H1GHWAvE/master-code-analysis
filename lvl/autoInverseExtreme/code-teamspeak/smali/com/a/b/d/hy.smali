.class public final Lcom/a/b/d/hy;
.super Ljava/util/AbstractMap;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/bw;
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# static fields
.field private static final b:D = 1.0

.field private static final h:J
    .annotation build Lcom/a/b/a/c;
        a = "Not needed in emulated source"
    .end annotation
.end field


# instance fields
.field transient a:I

.field private transient c:[Lcom/a/b/d/ia;

.field private transient d:[Lcom/a/b/d/ia;

.field private transient e:I

.field private transient f:I

.field private transient g:Lcom/a/b/d/bw;


# direct methods
.method private constructor <init>(I)V
    .registers 2

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 107
    invoke-direct {p0, p1}, Lcom/a/b/d/hy;->b(I)V

    .line 108
    return-void
.end method

.method static synthetic a(Lcom/a/b/d/hy;)I
    .registers 2

    .prologue
    .line 52
    iget v0, p0, Lcom/a/b/d/hy;->f:I

    return v0
.end method

.method static a(Ljava/lang/Object;)I
    .registers 2
    .param p0    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 173
    if-nez p0, :cond_8

    const/4 v0, 0x0

    :goto_3
    invoke-static {v0}, Lcom/a/b/d/iq;->a(I)I

    move-result v0

    return v0

    :cond_8
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_3
.end method

.method public static a()Lcom/a/b/d/hy;
    .registers 1

    .prologue
    .line 58
    const/16 v0, 0x10

    invoke-static {v0}, Lcom/a/b/d/hy;->a(I)Lcom/a/b/d/hy;

    move-result-object v0

    return-object v0
.end method

.method private static a(I)Lcom/a/b/d/hy;
    .registers 2

    .prologue
    .line 68
    new-instance v0, Lcom/a/b/d/hy;

    invoke-direct {v0, p0}, Lcom/a/b/d/hy;-><init>(I)V

    return-object v0
.end method

.method private static a(Ljava/util/Map;)Lcom/a/b/d/hy;
    .registers 2

    .prologue
    .line 76
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-static {v0}, Lcom/a/b/d/hy;->a(I)Lcom/a/b/d/hy;

    move-result-object v0

    .line 77
    invoke-virtual {v0, p0}, Lcom/a/b/d/hy;->putAll(Ljava/util/Map;)V

    .line 78
    return-object v0
.end method

.method static synthetic a(Lcom/a/b/d/hy;Ljava/lang/Object;I)Lcom/a/b/d/ia;
    .registers 4

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/hy;->b(Ljava/lang/Object;I)Lcom/a/b/d/ia;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/a/b/d/hy;Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;
    .registers 8

    .prologue
    .line 5253
    invoke-static {p1}, Lcom/a/b/d/hy;->a(Ljava/lang/Object;)I

    move-result v0

    .line 5254
    invoke-static {p2}, Lcom/a/b/d/hy;->a(Ljava/lang/Object;)I

    move-result v1

    .line 5256
    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/hy;->a(Ljava/lang/Object;I)Lcom/a/b/d/ia;

    move-result-object v2

    .line 5257
    if-eqz v2, :cond_1b

    iget v3, v2, Lcom/a/b/d/ia;->a:I

    if-ne v1, v3, :cond_1b

    iget-object v3, v2, Lcom/a/b/d/ia;->e:Ljava/lang/Object;

    invoke-static {p2, v3}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1b

    .line 5277
    :goto_1a
    return-object p2

    .line 5262
    :cond_1b
    invoke-direct {p0, p2, v1}, Lcom/a/b/d/hy;->b(Ljava/lang/Object;I)Lcom/a/b/d/ia;

    move-result-object v3

    .line 5263
    if-eqz v3, :cond_26

    .line 5264
    if-eqz p3, :cond_3a

    .line 5265
    invoke-virtual {p0, v3}, Lcom/a/b/d/hy;->a(Lcom/a/b/d/ia;)V

    .line 5271
    :cond_26
    if-eqz v2, :cond_2b

    .line 5272
    invoke-virtual {p0, v2}, Lcom/a/b/d/hy;->a(Lcom/a/b/d/ia;)V

    .line 5274
    :cond_2b
    new-instance v3, Lcom/a/b/d/ia;

    invoke-direct {v3, p2, v1, p1, v0}, Lcom/a/b/d/ia;-><init>(Ljava/lang/Object;ILjava/lang/Object;I)V

    .line 5275
    invoke-direct {p0, v3}, Lcom/a/b/d/hy;->b(Lcom/a/b/d/ia;)V

    .line 5276
    invoke-direct {p0}, Lcom/a/b/d/hy;->d()V

    .line 5277
    if-nez v2, :cond_61

    const/4 p2, 0x0

    goto :goto_1a

    .line 5267
    :cond_3a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x17

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "value already present: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 5277
    :cond_61
    iget-object p2, v2, Lcom/a/b/d/ia;->e:Ljava/lang/Object;

    goto :goto_1a
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;
    .registers 8
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 224
    invoke-static {p1}, Lcom/a/b/d/hy;->a(Ljava/lang/Object;)I

    move-result v0

    .line 225
    invoke-static {p2}, Lcom/a/b/d/hy;->a(Ljava/lang/Object;)I

    move-result v1

    .line 227
    invoke-direct {p0, p1, v0}, Lcom/a/b/d/hy;->b(Ljava/lang/Object;I)Lcom/a/b/d/ia;

    move-result-object v2

    .line 228
    if-eqz v2, :cond_1b

    iget v3, v2, Lcom/a/b/d/ia;->b:I

    if-ne v1, v3, :cond_1b

    iget-object v3, v2, Lcom/a/b/d/ia;->f:Ljava/lang/Object;

    invoke-static {p2, v3}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1b

    .line 248
    :goto_1a
    return-object p2

    .line 233
    :cond_1b
    invoke-virtual {p0, p2, v1}, Lcom/a/b/d/hy;->a(Ljava/lang/Object;I)Lcom/a/b/d/ia;

    move-result-object v3

    .line 234
    if-eqz v3, :cond_26

    .line 235
    if-eqz p3, :cond_3a

    .line 236
    invoke-virtual {p0, v3}, Lcom/a/b/d/hy;->a(Lcom/a/b/d/ia;)V

    .line 242
    :cond_26
    if-eqz v2, :cond_2b

    .line 243
    invoke-virtual {p0, v2}, Lcom/a/b/d/hy;->a(Lcom/a/b/d/ia;)V

    .line 245
    :cond_2b
    new-instance v3, Lcom/a/b/d/ia;

    invoke-direct {v3, p1, v0, p2, v1}, Lcom/a/b/d/ia;-><init>(Ljava/lang/Object;ILjava/lang/Object;I)V

    .line 246
    invoke-direct {p0, v3}, Lcom/a/b/d/hy;->b(Lcom/a/b/d/ia;)V

    .line 247
    invoke-direct {p0}, Lcom/a/b/d/hy;->d()V

    .line 248
    if-nez v2, :cond_61

    const/4 p2, 0x0

    goto :goto_1a

    .line 238
    :cond_3a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x17

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "value already present: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 248
    :cond_61
    iget-object p2, v2, Lcom/a/b/d/ia;->f:Ljava/lang/Object;

    goto :goto_1a
.end method

.method static synthetic a(Lcom/a/b/d/hy;Lcom/a/b/d/ia;)V
    .registers 2

    .prologue
    .line 52
    invoke-virtual {p0, p1}, Lcom/a/b/d/hy;->a(Lcom/a/b/d/ia;)V

    return-void
.end method

.method private a(Ljava/io/ObjectInputStream;)V
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectInputStream"
    .end annotation

    .prologue
    .line 659
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 5050
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    .line 661
    invoke-direct {p0, v0}, Lcom/a/b/d/hy;->b(I)V

    .line 662
    invoke-static {p0, p1, v0}, Lcom/a/b/d/zz;->a(Ljava/util/Map;Ljava/io/ObjectInputStream;I)V

    .line 663
    return-void
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectOutputStream"
    .end annotation

    .prologue
    .line 653
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 654
    invoke-static {p0, p1}, Lcom/a/b/d/zz;->a(Ljava/util/Map;Ljava/io/ObjectOutputStream;)V

    .line 655
    return-void
.end method

.method static synthetic b(Ljava/lang/Object;)I
    .registers 2

    .prologue
    .line 52
    invoke-static {p0}, Lcom/a/b/d/hy;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/a/b/d/hy;Ljava/lang/Object;I)Lcom/a/b/d/ia;
    .registers 4

    .prologue
    .line 52
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/hy;->a(Ljava/lang/Object;I)Lcom/a/b/d/ia;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/Object;I)Lcom/a/b/d/ia;
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 177
    iget-object v0, p0, Lcom/a/b/d/hy;->c:[Lcom/a/b/d/ia;

    iget v1, p0, Lcom/a/b/d/hy;->e:I

    and-int/2addr v1, p2

    aget-object v0, v0, v1

    :goto_7
    if-eqz v0, :cond_19

    .line 179
    iget v1, v0, Lcom/a/b/d/ia;->a:I

    if-ne p2, v1, :cond_16

    iget-object v1, v0, Lcom/a/b/d/ia;->e:Ljava/lang/Object;

    invoke-static {p1, v1}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 183
    :goto_15
    return-object v0

    .line 178
    :cond_16
    iget-object v0, v0, Lcom/a/b/d/ia;->c:Lcom/a/b/d/ia;

    goto :goto_7

    .line 183
    :cond_19
    const/4 v0, 0x0

    goto :goto_15
.end method

.method private b(Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;
    .registers 8
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 253
    invoke-static {p1}, Lcom/a/b/d/hy;->a(Ljava/lang/Object;)I

    move-result v0

    .line 254
    invoke-static {p2}, Lcom/a/b/d/hy;->a(Ljava/lang/Object;)I

    move-result v1

    .line 256
    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/hy;->a(Ljava/lang/Object;I)Lcom/a/b/d/ia;

    move-result-object v2

    .line 257
    if-eqz v2, :cond_1b

    iget v3, v2, Lcom/a/b/d/ia;->a:I

    if-ne v1, v3, :cond_1b

    iget-object v3, v2, Lcom/a/b/d/ia;->e:Ljava/lang/Object;

    invoke-static {p2, v3}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1b

    .line 277
    :goto_1a
    return-object p2

    .line 262
    :cond_1b
    invoke-direct {p0, p2, v1}, Lcom/a/b/d/hy;->b(Ljava/lang/Object;I)Lcom/a/b/d/ia;

    move-result-object v3

    .line 263
    if-eqz v3, :cond_26

    .line 264
    if-eqz p3, :cond_3a

    .line 265
    invoke-virtual {p0, v3}, Lcom/a/b/d/hy;->a(Lcom/a/b/d/ia;)V

    .line 271
    :cond_26
    if-eqz v2, :cond_2b

    .line 272
    invoke-virtual {p0, v2}, Lcom/a/b/d/hy;->a(Lcom/a/b/d/ia;)V

    .line 274
    :cond_2b
    new-instance v3, Lcom/a/b/d/ia;

    invoke-direct {v3, p2, v1, p1, v0}, Lcom/a/b/d/ia;-><init>(Ljava/lang/Object;ILjava/lang/Object;I)V

    .line 275
    invoke-direct {p0, v3}, Lcom/a/b/d/hy;->b(Lcom/a/b/d/ia;)V

    .line 276
    invoke-direct {p0}, Lcom/a/b/d/hy;->d()V

    .line 277
    if-nez v2, :cond_61

    const/4 p2, 0x0

    goto :goto_1a

    .line 267
    :cond_3a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x17

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "value already present: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 277
    :cond_61
    iget-object p2, v2, Lcom/a/b/d/ia;->e:Ljava/lang/Object;

    goto :goto_1a
.end method

.method private b(I)V
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 111
    const-string v0, "expectedSize"

    invoke-static {p1, v0}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 112
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    invoke-static {p1, v0, v1}, Lcom/a/b/d/iq;->a(ID)I

    move-result v0

    .line 1304
    new-array v1, v0, [Lcom/a/b/d/ia;

    .line 113
    iput-object v1, p0, Lcom/a/b/d/hy;->c:[Lcom/a/b/d/ia;

    .line 2304
    new-array v1, v0, [Lcom/a/b/d/ia;

    .line 114
    iput-object v1, p0, Lcom/a/b/d/hy;->d:[Lcom/a/b/d/ia;

    .line 115
    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/b/d/hy;->e:I

    .line 116
    iput v2, p0, Lcom/a/b/d/hy;->f:I

    .line 117
    iput v2, p0, Lcom/a/b/d/hy;->a:I

    .line 118
    return-void
.end method

.method static synthetic b(Lcom/a/b/d/hy;Lcom/a/b/d/ia;)V
    .registers 2

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/a/b/d/hy;->b(Lcom/a/b/d/ia;)V

    return-void
.end method

.method private b(Lcom/a/b/d/ia;)V
    .registers 4

    .prologue
    .line 160
    iget v0, p1, Lcom/a/b/d/ia;->a:I

    iget v1, p0, Lcom/a/b/d/hy;->e:I

    and-int/2addr v0, v1

    .line 161
    iget-object v1, p0, Lcom/a/b/d/hy;->c:[Lcom/a/b/d/ia;

    aget-object v1, v1, v0

    iput-object v1, p1, Lcom/a/b/d/ia;->c:Lcom/a/b/d/ia;

    .line 162
    iget-object v1, p0, Lcom/a/b/d/hy;->c:[Lcom/a/b/d/ia;

    aput-object p1, v1, v0

    .line 164
    iget v0, p1, Lcom/a/b/d/ia;->b:I

    iget v1, p0, Lcom/a/b/d/hy;->e:I

    and-int/2addr v0, v1

    .line 165
    iget-object v1, p0, Lcom/a/b/d/hy;->d:[Lcom/a/b/d/ia;

    aget-object v1, v1, v0

    iput-object v1, p1, Lcom/a/b/d/ia;->d:Lcom/a/b/d/ia;

    .line 166
    iget-object v1, p0, Lcom/a/b/d/hy;->d:[Lcom/a/b/d/ia;

    aput-object p1, v1, v0

    .line 168
    iget v0, p0, Lcom/a/b/d/hy;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/hy;->a:I

    .line 169
    iget v0, p0, Lcom/a/b/d/hy;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/hy;->f:I

    .line 170
    return-void
.end method

.method static synthetic b(Lcom/a/b/d/hy;)[Lcom/a/b/d/ia;
    .registers 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/a/b/d/hy;->c:[Lcom/a/b/d/ia;

    return-object v0
.end method

.method private static synthetic c(Lcom/a/b/d/hy;)I
    .registers 2

    .prologue
    .line 52
    iget v0, p0, Lcom/a/b/d/hy;->a:I

    return v0
.end method

.method private static c(I)[Lcom/a/b/d/ia;
    .registers 2

    .prologue
    .line 304
    new-array v0, p0, [Lcom/a/b/d/ia;

    return-object v0
.end method

.method private d()V
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 281
    iget-object v3, p0, Lcom/a/b/d/hy;->c:[Lcom/a/b/d/ia;

    .line 282
    iget v1, p0, Lcom/a/b/d/hy;->a:I

    array-length v2, v3

    invoke-static {v1, v2}, Lcom/a/b/d/iq;->a(II)Z

    move-result v1

    if-eqz v1, :cond_34

    .line 283
    array-length v1, v3

    mul-int/lit8 v1, v1, 0x2

    .line 3304
    new-array v2, v1, [Lcom/a/b/d/ia;

    .line 285
    iput-object v2, p0, Lcom/a/b/d/hy;->c:[Lcom/a/b/d/ia;

    .line 4304
    new-array v2, v1, [Lcom/a/b/d/ia;

    .line 286
    iput-object v2, p0, Lcom/a/b/d/hy;->d:[Lcom/a/b/d/ia;

    .line 287
    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/a/b/d/hy;->e:I

    .line 288
    iput v0, p0, Lcom/a/b/d/hy;->a:I

    .line 290
    :goto_1d
    array-length v1, v3

    if-ge v0, v1, :cond_2e

    .line 291
    aget-object v1, v3, v0

    .line 292
    :goto_22
    if-eqz v1, :cond_2b

    .line 293
    iget-object v2, v1, Lcom/a/b/d/ia;->c:Lcom/a/b/d/ia;

    .line 294
    invoke-direct {p0, v1}, Lcom/a/b/d/hy;->b(Lcom/a/b/d/ia;)V

    move-object v1, v2

    .line 296
    goto :goto_22

    .line 290
    :cond_2b
    add-int/lit8 v0, v0, 0x1

    goto :goto_1d

    .line 298
    :cond_2e
    iget v0, p0, Lcom/a/b/d/hy;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/hy;->f:I

    .line 300
    :cond_34
    return-void
.end method


# virtual methods
.method final a(Ljava/lang/Object;I)Lcom/a/b/d/ia;
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 187
    iget-object v0, p0, Lcom/a/b/d/hy;->d:[Lcom/a/b/d/ia;

    iget v1, p0, Lcom/a/b/d/hy;->e:I

    and-int/2addr v1, p2

    aget-object v0, v0, v1

    :goto_7
    if-eqz v0, :cond_19

    .line 189
    iget v1, v0, Lcom/a/b/d/ia;->b:I

    if-ne p2, v1, :cond_16

    iget-object v1, v0, Lcom/a/b/d/ia;->f:Ljava/lang/Object;

    invoke-static {p1, v1}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 193
    :goto_15
    return-object v0

    .line 188
    :cond_16
    iget-object v0, v0, Lcom/a/b/d/ia;->d:Lcom/a/b/d/ia;

    goto :goto_7

    .line 193
    :cond_19
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 220
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/a/b/d/hy;->a(Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method final a(Lcom/a/b/d/ia;)V
    .registers 7

    .prologue
    const/4 v0, 0x0

    .line 125
    iget v1, p1, Lcom/a/b/d/ia;->a:I

    iget v2, p0, Lcom/a/b/d/hy;->e:I

    and-int v3, v1, v2

    .line 127
    iget-object v1, p0, Lcom/a/b/d/hy;->c:[Lcom/a/b/d/ia;

    aget-object v1, v1, v3

    move-object v2, v0

    .line 129
    :goto_c
    if-ne v1, p1, :cond_3b

    .line 130
    if-nez v2, :cond_36

    .line 131
    iget-object v1, p0, Lcom/a/b/d/hy;->c:[Lcom/a/b/d/ia;

    iget-object v2, p1, Lcom/a/b/d/ia;->c:Lcom/a/b/d/ia;

    aput-object v2, v1, v3

    .line 140
    :goto_16
    iget v1, p1, Lcom/a/b/d/ia;->b:I

    iget v2, p0, Lcom/a/b/d/hy;->e:I

    and-int/2addr v2, v1

    .line 142
    iget-object v1, p0, Lcom/a/b/d/hy;->d:[Lcom/a/b/d/ia;

    aget-object v1, v1, v2

    .line 144
    :goto_1f
    if-ne v1, p1, :cond_46

    .line 145
    if-nez v0, :cond_41

    .line 146
    iget-object v0, p0, Lcom/a/b/d/hy;->d:[Lcom/a/b/d/ia;

    iget-object v1, p1, Lcom/a/b/d/ia;->d:Lcom/a/b/d/ia;

    aput-object v1, v0, v2

    .line 155
    :goto_29
    iget v0, p0, Lcom/a/b/d/hy;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/b/d/hy;->a:I

    .line 156
    iget v0, p0, Lcom/a/b/d/hy;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/hy;->f:I

    .line 157
    return-void

    .line 133
    :cond_36
    iget-object v1, p1, Lcom/a/b/d/ia;->c:Lcom/a/b/d/ia;

    iput-object v1, v2, Lcom/a/b/d/ia;->c:Lcom/a/b/d/ia;

    goto :goto_16

    .line 128
    :cond_3b
    iget-object v2, v1, Lcom/a/b/d/ia;->c:Lcom/a/b/d/ia;

    move-object v4, v2

    move-object v2, v1

    move-object v1, v4

    goto :goto_c

    .line 148
    :cond_41
    iget-object v1, p1, Lcom/a/b/d/ia;->d:Lcom/a/b/d/ia;

    iput-object v1, v0, Lcom/a/b/d/ia;->d:Lcom/a/b/d/ia;

    goto :goto_29

    .line 143
    :cond_46
    iget-object v0, v1, Lcom/a/b/d/ia;->d:Lcom/a/b/d/ia;

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_1f
.end method

.method public final b()Lcom/a/b/d/bw;
    .registers 3

    .prologue
    .line 483
    iget-object v0, p0, Lcom/a/b/d/hy;->g:Lcom/a/b/d/bw;

    if-nez v0, :cond_d

    new-instance v0, Lcom/a/b/d/ie;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/ie;-><init>(Lcom/a/b/d/hy;B)V

    iput-object v0, p0, Lcom/a/b/d/hy;->g:Lcom/a/b/d/bw;

    :goto_c
    return-object v0

    :cond_d
    iget-object v0, p0, Lcom/a/b/d/hy;->g:Lcom/a/b/d/bw;

    goto :goto_c
.end method

.method public final clear()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 320
    const/4 v0, 0x0

    iput v0, p0, Lcom/a/b/d/hy;->a:I

    .line 321
    iget-object v0, p0, Lcom/a/b/d/hy;->c:[Lcom/a/b/d/ia;

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 322
    iget-object v0, p0, Lcom/a/b/d/hy;->d:[Lcom/a/b/d/ia;

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 323
    iget v0, p0, Lcom/a/b/d/hy;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/hy;->f:I

    .line 324
    return-void
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 198
    invoke-static {p1}, Lcom/a/b/d/hy;->a(Ljava/lang/Object;)I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/a/b/d/hy;->b(Ljava/lang/Object;I)Lcom/a/b/d/ia;

    move-result-object v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public final containsValue(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 203
    invoke-static {p1}, Lcom/a/b/d/hy;->a(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/hy;->a(Ljava/lang/Object;I)Lcom/a/b/d/ia;

    move-result-object v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public final entrySet()Ljava/util/Set;
    .registers 3

    .prologue
    .line 423
    new-instance v0, Lcom/a/b/d/ib;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/ib;-><init>(Lcom/a/b/d/hy;B)V

    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 209
    invoke-static {p1}, Lcom/a/b/d/hy;->a(Ljava/lang/Object;)I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/a/b/d/hy;->b(Ljava/lang/Object;I)Lcom/a/b/d/ia;

    move-result-object v0

    .line 210
    if-nez v0, :cond_c

    const/4 v0, 0x0

    :goto_b
    return-object v0

    :cond_c
    iget-object v0, v0, Lcom/a/b/d/ia;->f:Ljava/lang/Object;

    goto :goto_b
.end method

.method public final j_()Ljava/util/Set;
    .registers 2

    .prologue
    .line 418
    invoke-virtual {p0}, Lcom/a/b/d/hy;->b()Lcom/a/b/d/bw;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/bw;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final keySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 386
    new-instance v0, Lcom/a/b/d/im;

    invoke-direct {v0, p0}, Lcom/a/b/d/im;-><init>(Lcom/a/b/d/hy;)V

    return-object v0
.end method

.method public final put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 215
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/a/b/d/hy;->a(Ljava/lang/Object;Ljava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 309
    invoke-static {p1}, Lcom/a/b/d/hy;->a(Ljava/lang/Object;)I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/a/b/d/hy;->b(Ljava/lang/Object;I)Lcom/a/b/d/ia;

    move-result-object v0

    .line 310
    if-nez v0, :cond_c

    .line 311
    const/4 v0, 0x0

    .line 314
    :goto_b
    return-object v0

    .line 313
    :cond_c
    invoke-virtual {p0, v0}, Lcom/a/b/d/hy;->a(Lcom/a/b/d/ia;)V

    .line 314
    iget-object v0, v0, Lcom/a/b/d/ia;->f:Ljava/lang/Object;

    goto :goto_b
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 328
    iget v0, p0, Lcom/a/b/d/hy;->a:I

    return v0
.end method

.method public final synthetic values()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/a/b/d/hy;->j_()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
