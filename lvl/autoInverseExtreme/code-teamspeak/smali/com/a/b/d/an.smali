.class abstract Lcom/a/b/d/an;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/vi;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# instance fields
.field private transient a:Ljava/util/Collection;

.field private transient b:Ljava/util/Set;

.field private transient c:Lcom/a/b/d/xc;

.field private transient d:Ljava/util/Collection;

.field private transient e:Ljava/util/Map;


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 181
    return-void
.end method


# virtual methods
.method public a(Lcom/a/b/d/vi;)Z
    .registers 6

    .prologue
    .line 88
    const/4 v0, 0x0

    .line 89
    invoke-interface {p1}, Lcom/a/b/d/vi;->k()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_25

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 90
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v3, v0}, Lcom/a/b/d/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    or-int/2addr v0, v1

    move v1, v0

    .line 91
    goto :goto_a

    .line 92
    :cond_25
    return v1
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 69
    invoke-virtual {p0, p1}, Lcom/a/b/d/an;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 97
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    invoke-virtual {p0, p1}, Lcom/a/b/d/an;->d(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    .line 99
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/an;->c(Ljava/lang/Object;Ljava/lang/Iterable;)Z

    .line 100
    return-object v0
.end method

.method public b()Ljava/util/Map;
    .registers 2

    .prologue
    .line 207
    iget-object v0, p0, Lcom/a/b/d/an;->e:Ljava/util/Map;

    .line 208
    if-nez v0, :cond_a

    invoke-virtual {p0}, Lcom/a/b/d/an;->m()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/an;->e:Ljava/util/Map;

    :cond_a
    return-object v0
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/a/b/d/an;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 58
    if-eqz v0, :cond_14

    invoke-interface {v0, p2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
    .registers 7
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 74
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    instance-of v2, p2, Ljava/util/Collection;

    if-eqz v2, :cond_1e

    .line 78
    check-cast p2, Ljava/util/Collection;

    .line 79
    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1c

    invoke-virtual {p0, p1}, Lcom/a/b/d/an;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2, p2}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 82
    :cond_1b
    :goto_1b
    return v0

    :cond_1c
    move v0, v1

    .line 79
    goto :goto_1b

    .line 81
    :cond_1e
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 82
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_32

    invoke-virtual {p0, p1}, Lcom/a/b/d/an;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/a/b/d/nj;->a(Ljava/util/Collection;Ljava/util/Iterator;)Z

    move-result v2

    if-nez v2, :cond_1b

    :cond_32
    move v0, v1

    goto :goto_1b
.end method

.method public c(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/a/b/d/an;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 64
    if-eqz v0, :cond_14

    invoke-interface {v0, p2}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 216
    .line 3048
    if-ne p1, p0, :cond_4

    .line 3049
    const/4 v0, 0x1

    .line 3053
    :goto_3
    return v0

    .line 3051
    :cond_4
    instance-of v0, p1, Lcom/a/b/d/vi;

    if-eqz v0, :cond_17

    .line 3052
    check-cast p1, Lcom/a/b/d/vi;

    .line 3053
    invoke-interface {p0}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {p1}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_3

    .line 3055
    :cond_17
    const/4 v0, 0x0

    .line 216
    goto :goto_3
.end method

.method public g(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/a/b/d/an;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 47
    invoke-interface {v0, p1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 48
    const/4 v0, 0x1

    .line 52
    :goto_1f
    return v0

    :cond_20
    const/4 v0, 0x0

    goto :goto_1f
.end method

.method h()Ljava/util/Set;
    .registers 3

    .prologue
    .line 154
    new-instance v0, Lcom/a/b/d/uk;

    invoke-virtual {p0}, Lcom/a/b/d/an;->b()Ljava/util/Map;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/uk;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 228
    invoke-virtual {p0}, Lcom/a/b/d/an;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->hashCode()I

    move-result v0

    return v0
.end method

.method public i()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/a/b/d/an;->d:Ljava/util/Collection;

    .line 174
    if-nez v0, :cond_a

    invoke-virtual {p0}, Lcom/a/b/d/an;->s()Ljava/util/Collection;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/an;->d:Ljava/util/Collection;

    :cond_a
    return-object v0
.end method

.method j()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/a/b/d/an;->k()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->b(Ljava/util/Iterator;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public k()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/a/b/d/an;->a:Ljava/util/Collection;

    .line 108
    if-nez v0, :cond_a

    invoke-virtual {p0}, Lcom/a/b/d/an;->o()Ljava/util/Collection;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/an;->a:Ljava/util/Collection;

    :cond_a
    return-object v0
.end method

.method abstract l()Ljava/util/Iterator;
.end method

.method abstract m()Ljava/util/Map;
.end method

.method public n()Z
    .registers 2

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/a/b/d/an;->f()I

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method o()Ljava/util/Collection;
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 112
    instance-of v0, p0, Lcom/a/b/d/aac;

    if-eqz v0, :cond_b

    .line 113
    new-instance v0, Lcom/a/b/d/aq;

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/aq;-><init>(Lcom/a/b/d/an;B)V

    .line 115
    :goto_a
    return-object v0

    :cond_b
    new-instance v0, Lcom/a/b/d/ap;

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/ap;-><init>(Lcom/a/b/d/an;B)V

    goto :goto_a
.end method

.method public p()Ljava/util/Set;
    .registers 2

    .prologue
    .line 149
    iget-object v0, p0, Lcom/a/b/d/an;->b:Ljava/util/Set;

    .line 150
    if-nez v0, :cond_a

    invoke-virtual {p0}, Lcom/a/b/d/an;->h()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/an;->b:Ljava/util/Set;

    :cond_a
    return-object v0
.end method

.method public q()Lcom/a/b/d/xc;
    .registers 2

    .prologue
    .line 161
    iget-object v0, p0, Lcom/a/b/d/an;->c:Lcom/a/b/d/xc;

    .line 162
    if-nez v0, :cond_a

    invoke-virtual {p0}, Lcom/a/b/d/an;->r()Lcom/a/b/d/xc;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/an;->c:Lcom/a/b/d/xc;

    :cond_a
    return-object v0
.end method

.method r()Lcom/a/b/d/xc;
    .registers 2

    .prologue
    .line 166
    new-instance v0, Lcom/a/b/d/wn;

    invoke-direct {v0, p0}, Lcom/a/b/d/wn;-><init>(Lcom/a/b/d/vi;)V

    return-object v0
.end method

.method s()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 178
    new-instance v0, Lcom/a/b/d/ar;

    invoke-direct {v0, p0}, Lcom/a/b/d/ar;-><init>(Lcom/a/b/d/an;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 239
    invoke-virtual {p0}, Lcom/a/b/d/an;->b()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
