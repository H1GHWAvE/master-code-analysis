.class Lcom/a/b/d/ad;
.super Lcom/a/b/d/ab;
.source "SourceFile"

# interfaces
.implements Ljava/util/List;


# instance fields
.field final synthetic g:Lcom/a/b/d/n;


# direct methods
.method constructor <init>(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/List;Lcom/a/b/d/ab;)V
    .registers 5
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/a/b/d/ab;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 760
    iput-object p1, p0, Lcom/a/b/d/ad;->g:Lcom/a/b/d/n;

    .line 761
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/a/b/d/ab;-><init>(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/Collection;Lcom/a/b/d/ab;)V

    .line 762
    return-void
.end method

.method private d()Ljava/util/List;
    .registers 2

    .prologue
    .line 765
    .line 1445
    iget-object v0, p0, Lcom/a/b/d/ab;->c:Ljava/util/Collection;

    .line 765
    check-cast v0, Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public add(ILjava/lang/Object;)V
    .registers 5

    .prologue
    .line 799
    invoke-virtual {p0}, Lcom/a/b/d/ad;->a()V

    .line 6445
    iget-object v0, p0, Lcom/a/b/d/ab;->c:Ljava/util/Collection;

    .line 800
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    .line 7445
    iget-object v0, p0, Lcom/a/b/d/ab;->c:Ljava/util/Collection;

    .line 6765
    check-cast v0, Ljava/util/List;

    .line 801
    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 802
    iget-object v0, p0, Lcom/a/b/d/ad;->g:Lcom/a/b/d/n;

    invoke-static {v0}, Lcom/a/b/d/n;->c(Lcom/a/b/d/n;)I

    .line 803
    if-eqz v1, :cond_1a

    .line 804
    invoke-virtual {p0}, Lcom/a/b/d/ad;->c()V

    .line 806
    :cond_1a
    return-void
.end method

.method public addAll(ILjava/util/Collection;)Z
    .registers 7

    .prologue
    .line 770
    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 771
    const/4 v0, 0x0

    .line 782
    :cond_7
    :goto_7
    return v0

    .line 773
    :cond_8
    invoke-virtual {p0}, Lcom/a/b/d/ad;->size()I

    move-result v1

    .line 2445
    iget-object v0, p0, Lcom/a/b/d/ab;->c:Ljava/util/Collection;

    .line 1765
    check-cast v0, Ljava/util/List;

    .line 774
    invoke-interface {v0, p1, p2}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    move-result v0

    .line 775
    if-eqz v0, :cond_7

    .line 3445
    iget-object v2, p0, Lcom/a/b/d/ab;->c:Ljava/util/Collection;

    .line 776
    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v2

    .line 777
    iget-object v3, p0, Lcom/a/b/d/ad;->g:Lcom/a/b/d/n;

    sub-int/2addr v2, v1

    invoke-static {v3, v2}, Lcom/a/b/d/n;->a(Lcom/a/b/d/n;I)I

    .line 778
    if-nez v1, :cond_7

    .line 779
    invoke-virtual {p0}, Lcom/a/b/d/ad;->c()V

    goto :goto_7
.end method

.method public get(I)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 787
    invoke-virtual {p0}, Lcom/a/b/d/ad;->a()V

    .line 4445
    iget-object v0, p0, Lcom/a/b/d/ab;->c:Ljava/util/Collection;

    .line 3765
    check-cast v0, Ljava/util/List;

    .line 788
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public indexOf(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 819
    invoke-virtual {p0}, Lcom/a/b/d/ad;->a()V

    .line 9445
    iget-object v0, p0, Lcom/a/b/d/ab;->c:Ljava/util/Collection;

    .line 8765
    check-cast v0, Ljava/util/List;

    .line 820
    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 825
    invoke-virtual {p0}, Lcom/a/b/d/ad;->a()V

    .line 10445
    iget-object v0, p0, Lcom/a/b/d/ab;->c:Ljava/util/Collection;

    .line 9765
    check-cast v0, Ljava/util/List;

    .line 826
    invoke-interface {v0, p1}, Ljava/util/List;->lastIndexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public listIterator()Ljava/util/ListIterator;
    .registers 2

    .prologue
    .line 831
    invoke-virtual {p0}, Lcom/a/b/d/ad;->a()V

    .line 832
    new-instance v0, Lcom/a/b/d/ae;

    invoke-direct {v0, p0}, Lcom/a/b/d/ae;-><init>(Lcom/a/b/d/ad;)V

    return-object v0
.end method

.method public listIterator(I)Ljava/util/ListIterator;
    .registers 3

    .prologue
    .line 837
    invoke-virtual {p0}, Lcom/a/b/d/ad;->a()V

    .line 838
    new-instance v0, Lcom/a/b/d/ae;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/ae;-><init>(Lcom/a/b/d/ad;I)V

    return-object v0
.end method

.method public remove(I)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 810
    invoke-virtual {p0}, Lcom/a/b/d/ad;->a()V

    .line 8445
    iget-object v0, p0, Lcom/a/b/d/ab;->c:Ljava/util/Collection;

    .line 7765
    check-cast v0, Ljava/util/List;

    .line 811
    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    .line 812
    iget-object v1, p0, Lcom/a/b/d/ad;->g:Lcom/a/b/d/n;

    invoke-static {v1}, Lcom/a/b/d/n;->b(Lcom/a/b/d/n;)I

    .line 813
    invoke-virtual {p0}, Lcom/a/b/d/ad;->b()V

    .line 814
    return-object v0
.end method

.method public set(ILjava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 793
    invoke-virtual {p0}, Lcom/a/b/d/ad;->a()V

    .line 5445
    iget-object v0, p0, Lcom/a/b/d/ab;->c:Ljava/util/Collection;

    .line 4765
    check-cast v0, Ljava/util/List;

    .line 794
    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public subList(II)Ljava/util/List;
    .registers 7

    .prologue
    .line 843
    invoke-virtual {p0}, Lcom/a/b/d/ad;->a()V

    .line 844
    iget-object v1, p0, Lcom/a/b/d/ad;->g:Lcom/a/b/d/n;

    .line 11403
    iget-object v2, p0, Lcom/a/b/d/ab;->b:Ljava/lang/Object;

    .line 12445
    iget-object v0, p0, Lcom/a/b/d/ab;->c:Ljava/util/Collection;

    .line 11765
    check-cast v0, Ljava/util/List;

    .line 844
    invoke-interface {v0, p1, p2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 12516
    iget-object v3, p0, Lcom/a/b/d/ab;->d:Lcom/a/b/d/ab;

    .line 844
    if-nez v3, :cond_18

    :goto_13
    invoke-static {v1, v2, v0, p0}, Lcom/a/b/d/n;->a(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/List;Lcom/a/b/d/ab;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 13516
    :cond_18
    iget-object p0, p0, Lcom/a/b/d/ab;->d:Lcom/a/b/d/ab;

    goto :goto_13
.end method
