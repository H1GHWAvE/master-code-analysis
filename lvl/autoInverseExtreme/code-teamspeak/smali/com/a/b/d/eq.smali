.class final Lcom/a/b/d/eq;
.super Lcom/a/b/d/ep;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final a:Lcom/a/b/d/eq;

.field private static final b:Ljava/math/BigInteger;

.field private static final c:Ljava/math/BigInteger;

.field private static final d:J


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 160
    new-instance v0, Lcom/a/b/d/eq;

    invoke-direct {v0}, Lcom/a/b/d/eq;-><init>()V

    sput-object v0, Lcom/a/b/d/eq;->a:Lcom/a/b/d/eq;

    .line 162
    const-wide/high16 v0, -0x8000000000000000L

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    sput-object v0, Lcom/a/b/d/eq;->b:Ljava/math/BigInteger;

    .line 164
    const-wide v0, 0x7fffffffffffffffL

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    sput-object v0, Lcom/a/b/d/eq;->c:Ljava/math/BigInteger;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 158
    invoke-direct {p0}, Lcom/a/b/d/ep;-><init>()V

    return-void
.end method

.method private static a(Ljava/math/BigInteger;Ljava/math/BigInteger;)J
    .registers 4

    .prologue
    .line 176
    invoke-virtual {p1, p0}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    sget-object v1, Lcom/a/b/d/eq;->b:Ljava/math/BigInteger;

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->max(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    sget-object v1, Lcom/a/b/d/eq;->c:Ljava/math/BigInteger;

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->min(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method private static a(Ljava/math/BigInteger;)Ljava/math/BigInteger;
    .registers 2

    .prologue
    .line 168
    sget-object v0, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    invoke-virtual {p0, v0}, Ljava/math/BigInteger;->add(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/math/BigInteger;)Ljava/math/BigInteger;
    .registers 2

    .prologue
    .line 172
    sget-object v0, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    invoke-virtual {p0, v0}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c()Lcom/a/b/d/eq;
    .registers 1

    .prologue
    .line 158
    sget-object v0, Lcom/a/b/d/eq;->a:Lcom/a/b/d/eq;

    return-object v0
.end method

.method private static d()Ljava/lang/Object;
    .registers 1

    .prologue
    .line 180
    sget-object v0, Lcom/a/b/d/eq;->a:Lcom/a/b/d/eq;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Comparable;Ljava/lang/Comparable;)J
    .registers 5

    .prologue
    .line 158
    check-cast p1, Ljava/math/BigInteger;

    check-cast p2, Ljava/math/BigInteger;

    .line 1176
    invoke-virtual {p2, p1}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    sget-object v1, Lcom/a/b/d/eq;->b:Ljava/math/BigInteger;

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->max(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    sget-object v1, Lcom/a/b/d/eq;->c:Ljava/math/BigInteger;

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->min(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->longValue()J

    move-result-wide v0

    .line 158
    return-wide v0
.end method

.method public final synthetic a(Ljava/lang/Comparable;)Ljava/lang/Comparable;
    .registers 3

    .prologue
    .line 158
    check-cast p1, Ljava/math/BigInteger;

    .line 3168
    sget-object v0, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    invoke-virtual {p1, v0}, Ljava/math/BigInteger;->add(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    .line 158
    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Comparable;)Ljava/lang/Comparable;
    .registers 3

    .prologue
    .line 158
    check-cast p1, Ljava/math/BigInteger;

    .line 2172
    sget-object v0, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    invoke-virtual {p1, v0}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    .line 158
    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 185
    const-string v0, "DiscreteDomain.bigIntegers()"

    return-object v0
.end method
