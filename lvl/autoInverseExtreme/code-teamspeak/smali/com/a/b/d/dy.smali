.class final Lcom/a/b/d/dy;
.super Lcom/a/b/d/dw;
.source "SourceFile"


# static fields
.field private static final b:Lcom/a/b/d/dy;

.field private static final c:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 183
    new-instance v0, Lcom/a/b/d/dy;

    invoke-direct {v0}, Lcom/a/b/d/dy;-><init>()V

    sput-object v0, Lcom/a/b/d/dy;->b:Lcom/a/b/d/dy;

    return-void
.end method

.method private constructor <init>()V
    .registers 2

    .prologue
    .line 186
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/a/b/d/dw;-><init>(Ljava/lang/Comparable;)V

    .line 187
    return-void
.end method

.method static synthetic f()Lcom/a/b/d/dy;
    .registers 1

    .prologue
    .line 182
    sget-object v0, Lcom/a/b/d/dy;->b:Lcom/a/b/d/dy;

    return-object v0
.end method

.method private static g()Ljava/lang/Object;
    .registers 1

    .prologue
    .line 229
    sget-object v0, Lcom/a/b/d/dy;->b:Lcom/a/b/d/dy;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/a/b/d/dw;)I
    .registers 3

    .prologue
    .line 223
    if-ne p1, p0, :cond_4

    const/4 v0, 0x0

    :goto_3
    return v0

    :cond_4
    const/4 v0, 0x1

    goto :goto_3
.end method

.method final a()Lcom/a/b/d/ce;
    .registers 3

    .prologue
    .line 195
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "this statement should be unreachable"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method final a(Lcom/a/b/d/ce;Lcom/a/b/d/ep;)Lcom/a/b/d/dw;
    .registers 5

    .prologue
    .line 202
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "this statement should be unreachable"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method final a(Lcom/a/b/d/ep;)Ljava/lang/Comparable;
    .registers 3

    .prologue
    .line 216
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method final a(Ljava/lang/StringBuilder;)V
    .registers 3

    .prologue
    .line 209
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method final a(Ljava/lang/Comparable;)Z
    .registers 3

    .prologue
    .line 192
    const/4 v0, 0x0

    return v0
.end method

.method final b()Lcom/a/b/d/ce;
    .registers 2

    .prologue
    .line 198
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method final b(Lcom/a/b/d/ce;Lcom/a/b/d/ep;)Lcom/a/b/d/dw;
    .registers 4

    .prologue
    .line 206
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method final b(Lcom/a/b/d/ep;)Ljava/lang/Comparable;
    .registers 3

    .prologue
    .line 220
    invoke-virtual {p1}, Lcom/a/b/d/ep;->b()Ljava/lang/Comparable;

    move-result-object v0

    return-object v0
.end method

.method final b(Ljava/lang/StringBuilder;)V
    .registers 3

    .prologue
    .line 212
    const-string v0, "+\u221e)"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    return-void
.end method

.method final c()Ljava/lang/Comparable;
    .registers 3

    .prologue
    .line 189
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "range unbounded on this side"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 182
    check-cast p1, Lcom/a/b/d/dw;

    invoke-virtual {p0, p1}, Lcom/a/b/d/dy;->a(Lcom/a/b/d/dw;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 226
    const-string v0, "+\u221e"

    return-object v0
.end method
