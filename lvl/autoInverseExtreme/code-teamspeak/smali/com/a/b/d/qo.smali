.class final Lcom/a/b/d/qo;
.super Lcom/a/b/d/qp;
.source "SourceFile"


# static fields
.field private static final b:J


# instance fields
.field final a:Lcom/a/b/b/bj;


# direct methods
.method constructor <init>(Lcom/a/b/d/ql;Lcom/a/b/b/bj;)V
    .registers 4

    .prologue
    .line 837
    invoke-direct {p0, p1}, Lcom/a/b/d/qp;-><init>(Lcom/a/b/d/ql;)V

    .line 838
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/bj;

    iput-object v0, p0, Lcom/a/b/d/qo;->a:Lcom/a/b/b/bj;

    .line 839
    return-void
.end method

.method private a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 852
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 854
    :try_start_3
    iget-object v0, p0, Lcom/a/b/d/qo;->a:Lcom/a/b/b/bj;

    invoke-interface {v0, p1}, Lcom/a/b/b/bj;->e(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_8
    .catch Lcom/a/b/d/cz; {:try_start_3 .. :try_end_8} :catch_a
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_8} :catch_c

    move-result-object v0

    return-object v0

    .line 856
    :catch_a
    move-exception v0

    throw v0

    .line 857
    :catch_c
    move-exception v0

    .line 858
    new-instance v1, Lcom/a/b/d/cz;

    invoke-direct {v1, v0}, Lcom/a/b/d/cz;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 7

    .prologue
    .line 845
    invoke-direct {p0, p1}, Lcom/a/b/d/qo;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 846
    const-string v1, "%s returned null for key %s."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/a/b/d/qo;->a:Lcom/a/b/b/bj;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 847
    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/qo;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 848
    return-object v0
.end method
