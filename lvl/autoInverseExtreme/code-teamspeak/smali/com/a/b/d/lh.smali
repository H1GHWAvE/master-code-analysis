.class final Lcom/a/b/d/lh;
.super Lcom/a/b/d/me;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/lf;

.field private final f:Lcom/a/b/d/ep;

.field private transient g:Ljava/lang/Integer;


# direct methods
.method constructor <init>(Lcom/a/b/d/lf;Lcom/a/b/d/ep;)V
    .registers 4

    .prologue
    .line 373
    iput-object p1, p0, Lcom/a/b/d/lh;->a:Lcom/a/b/d/lf;

    .line 374
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/me;-><init>(Ljava/util/Comparator;)V

    .line 375
    iput-object p2, p0, Lcom/a/b/d/lh;->f:Lcom/a/b/d/ep;

    .line 376
    return-void
.end method

.method static synthetic a(Lcom/a/b/d/lh;)Lcom/a/b/d/ep;
    .registers 2

    .prologue
    .line 370
    iget-object v0, p0, Lcom/a/b/d/lh;->f:Lcom/a/b/d/ep;

    return-object v0
.end method

.method private a(Lcom/a/b/d/yl;)Lcom/a/b/d/me;
    .registers 4

    .prologue
    .line 439
    iget-object v0, p0, Lcom/a/b/d/lh;->a:Lcom/a/b/d/lf;

    invoke-virtual {v0, p1}, Lcom/a/b/d/lf;->d(Lcom/a/b/d/yl;)Lcom/a/b/d/lf;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/lh;->f:Lcom/a/b/d/ep;

    invoke-virtual {v0, v1}, Lcom/a/b/d/lf;->a(Lcom/a/b/d/ep;)Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Comparable;Z)Lcom/a/b/d/me;
    .registers 4

    .prologue
    .line 444
    invoke-static {p2}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/a/b/d/yl;->a(Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/lh;->a(Lcom/a/b/d/yl;)Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Comparable;ZLjava/lang/Comparable;Z)Lcom/a/b/d/me;
    .registers 7

    .prologue
    .line 450
    if-nez p2, :cond_d

    if-nez p4, :cond_d

    invoke-static {p1, p3}, Lcom/a/b/d/yl;->b(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-nez v0, :cond_d

    .line 2105
    sget-object v0, Lcom/a/b/d/me;->c:Lcom/a/b/d/me;

    .line 453
    :goto_c
    return-object v0

    :cond_d
    invoke-static {p2}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v0

    invoke-static {p4}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v1

    invoke-static {p1, v0, p3, v1}, Lcom/a/b/d/yl;->a(Ljava/lang/Comparable;Lcom/a/b/d/ce;Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/lh;->a(Lcom/a/b/d/yl;)Lcom/a/b/d/me;

    move-result-object v0

    goto :goto_c
.end method

.method private b(Ljava/lang/Comparable;Z)Lcom/a/b/d/me;
    .registers 4

    .prologue
    .line 460
    invoke-static {p2}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/a/b/d/yl;->b(Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/lh;->a(Lcom/a/b/d/yl;)Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method final synthetic a(Ljava/lang/Object;Z)Lcom/a/b/d/me;
    .registers 4

    .prologue
    .line 370
    check-cast p1, Ljava/lang/Comparable;

    .line 2460
    invoke-static {p2}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/a/b/d/yl;->b(Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/lh;->a(Lcom/a/b/d/yl;)Lcom/a/b/d/me;

    move-result-object v0

    .line 370
    return-object v0
.end method

.method final synthetic a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/a/b/d/me;
    .registers 7

    .prologue
    .line 370
    check-cast p1, Ljava/lang/Comparable;

    check-cast p3, Ljava/lang/Comparable;

    .line 3450
    if-nez p2, :cond_11

    if-nez p4, :cond_11

    invoke-static {p1, p3}, Lcom/a/b/d/yl;->b(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-nez v0, :cond_11

    .line 5105
    sget-object v0, Lcom/a/b/d/me;->c:Lcom/a/b/d/me;

    .line 3451
    :goto_10
    return-object v0

    .line 3453
    :cond_11
    invoke-static {p2}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v0

    invoke-static {p4}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v1

    invoke-static {p1, v0, p3, v1}, Lcom/a/b/d/yl;->a(Ljava/lang/Comparable;Lcom/a/b/d/ce;Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/lh;->a(Lcom/a/b/d/yl;)Lcom/a/b/d/me;

    move-result-object v0

    goto :goto_10
.end method

.method final synthetic b(Ljava/lang/Object;Z)Lcom/a/b/d/me;
    .registers 4

    .prologue
    .line 370
    check-cast p1, Ljava/lang/Comparable;

    .line 5444
    invoke-static {p2}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/a/b/d/yl;->a(Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/lh;->a(Lcom/a/b/d/yl;)Lcom/a/b/d/me;

    move-result-object v0

    .line 370
    return-object v0
.end method

.method final c(Ljava/lang/Object;)I
    .registers 7

    .prologue
    .line 479
    invoke-virtual {p0, p1}, Lcom/a/b/d/lh;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4e

    .line 481
    check-cast p1, Ljava/lang/Comparable;

    .line 482
    const-wide/16 v0, 0x0

    .line 483
    iget-object v2, p0, Lcom/a/b/d/lh;->a:Lcom/a/b/d/lf;

    invoke-static {v2}, Lcom/a/b/d/lf;->a(Lcom/a/b/d/lf;)Lcom/a/b/d/jl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/a/b/d/jl;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_15
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_46

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    .line 484
    invoke-virtual {v0, p1}, Lcom/a/b/d/yl;->c(Ljava/lang/Comparable;)Z

    move-result v1

    if-eqz v1, :cond_38

    .line 485
    iget-object v1, p0, Lcom/a/b/d/lh;->f:Lcom/a/b/d/ep;

    invoke-static {v0, v1}, Lcom/a/b/d/du;->a(Lcom/a/b/d/yl;Lcom/a/b/d/ep;)Lcom/a/b/d/du;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/b/d/du;->c(Ljava/lang/Object;)I

    move-result v0

    int-to-long v0, v0

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Lcom/a/b/l/q;->b(J)I

    move-result v0

    .line 492
    :goto_37
    return v0

    .line 487
    :cond_38
    iget-object v1, p0, Lcom/a/b/d/lh;->f:Lcom/a/b/d/ep;

    invoke-static {v0, v1}, Lcom/a/b/d/du;->a(Lcom/a/b/d/yl;Lcom/a/b/d/ep;)Lcom/a/b/d/du;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/du;->size()I

    move-result v0

    int-to-long v0, v0

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 489
    goto :goto_15

    .line 490
    :cond_46
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "impossible"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 492
    :cond_4e
    const/4 v0, -0x1

    goto :goto_37
.end method

.method public final c()Lcom/a/b/d/agi;
    .registers 2

    .prologue
    .line 399
    new-instance v0, Lcom/a/b/d/li;

    invoke-direct {v0, p0}, Lcom/a/b/d/li;-><init>(Lcom/a/b/d/lh;)V

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 465
    if-nez p1, :cond_4

    .line 473
    :goto_3
    return v0

    .line 470
    :cond_4
    :try_start_4
    check-cast p1, Ljava/lang/Comparable;

    .line 471
    iget-object v1, p0, Lcom/a/b/d/lh;->a:Lcom/a/b/d/lf;

    invoke-virtual {v1, p1}, Lcom/a/b/d/lf;->a(Ljava/lang/Comparable;)Z
    :try_end_b
    .catch Ljava/lang/ClassCastException; {:try_start_4 .. :try_end_b} :catch_d

    move-result v0

    goto :goto_3

    .line 473
    :catch_d
    move-exception v1

    goto :goto_3
.end method

.method public final d()Lcom/a/b/d/agi;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 420
    new-instance v0, Lcom/a/b/d/lj;

    invoke-direct {v0, p0}, Lcom/a/b/d/lj;-><init>(Lcom/a/b/d/lh;)V

    return-object v0
.end method

.method public final synthetic descendingIterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 370
    invoke-virtual {p0}, Lcom/a/b/d/lh;->d()Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method final g()Ljava/lang/Object;
    .registers 4

    .prologue
    .line 507
    new-instance v0, Lcom/a/b/d/lk;

    iget-object v1, p0, Lcom/a/b/d/lh;->a:Lcom/a/b/d/lf;

    invoke-static {v1}, Lcom/a/b/d/lf;->a(Lcom/a/b/d/lf;)Lcom/a/b/d/jl;

    move-result-object v1

    iget-object v2, p0, Lcom/a/b/d/lh;->f:Lcom/a/b/d/ep;

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/lk;-><init>(Lcom/a/b/d/jl;Lcom/a/b/d/ep;)V

    return-object v0
.end method

.method final h_()Z
    .registers 2

    .prologue
    .line 497
    iget-object v0, p0, Lcom/a/b/d/lh;->a:Lcom/a/b/d/lf;

    invoke-static {v0}, Lcom/a/b/d/lf;->a(Lcom/a/b/d/lf;)Lcom/a/b/d/jl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/jl;->h_()Z

    move-result v0

    return v0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 370
    invoke-virtual {p0}, Lcom/a/b/d/lh;->c()Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .registers 6

    .prologue
    .line 383
    iget-object v0, p0, Lcom/a/b/d/lh;->g:Ljava/lang/Integer;

    .line 384
    if-nez v0, :cond_3a

    .line 385
    const-wide/16 v0, 0x0

    .line 386
    iget-object v2, p0, Lcom/a/b/d/lh;->a:Lcom/a/b/d/lf;

    invoke-static {v2}, Lcom/a/b/d/lf;->a(Lcom/a/b/d/lf;)Lcom/a/b/d/jl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/a/b/d/jl;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_11
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_41

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    .line 387
    iget-object v1, p0, Lcom/a/b/d/lh;->f:Lcom/a/b/d/ep;

    invoke-static {v0, v1}, Lcom/a/b/d/du;->a(Lcom/a/b/d/yl;Lcom/a/b/d/ep;)Lcom/a/b/d/du;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/du;->size()I

    move-result v0

    int-to-long v0, v0

    add-long/2addr v0, v2

    .line 388
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-ltz v2, :cond_3f

    .line 392
    :goto_30
    invoke-static {v0, v1}, Lcom/a/b/l/q;->b(J)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/lh;->g:Ljava/lang/Integer;

    .line 394
    :cond_3a
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0

    :cond_3f
    move-wide v2, v0

    goto :goto_11

    :cond_41
    move-wide v0, v2

    goto :goto_30
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 502
    iget-object v0, p0, Lcom/a/b/d/lh;->a:Lcom/a/b/d/lf;

    invoke-static {v0}, Lcom/a/b/d/lf;->a(Lcom/a/b/d/lf;)Lcom/a/b/d/jl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/jl;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
