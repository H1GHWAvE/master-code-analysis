.class final Lcom/a/b/d/zp;
.super Lcom/a/b/d/ma;
.source "SourceFile"


# instance fields
.field private final transient b:Lcom/a/b/d/zq;

.field private final transient c:[I

.field private final transient d:[J

.field private final transient e:I

.field private final transient f:I


# direct methods
.method constructor <init>(Lcom/a/b/d/zq;[I[JII)V
    .registers 6

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/a/b/d/ma;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/a/b/d/zp;->b:Lcom/a/b/d/zq;

    .line 45
    iput-object p2, p0, Lcom/a/b/d/zp;->c:[I

    .line 46
    iput-object p3, p0, Lcom/a/b/d/zp;->d:[J

    .line 47
    iput p4, p0, Lcom/a/b/d/zp;->e:I

    .line 48
    iput p5, p0, Lcom/a/b/d/zp;->f:I

    .line 49
    return-void
.end method

.method private a(II)Lcom/a/b/d/ma;
    .registers 9

    .prologue
    .line 97
    iget v0, p0, Lcom/a/b/d/zp;->f:I

    invoke-static {p1, p2, v0}, Lcom/a/b/b/cn;->a(III)V

    .line 98
    if-ne p1, p2, :cond_10

    .line 99
    invoke-virtual {p0}, Lcom/a/b/d/zp;->comparator()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/zp;->a(Ljava/util/Comparator;)Lcom/a/b/d/ma;

    move-result-object p0

    .line 105
    :cond_f
    :goto_f
    return-object p0

    .line 100
    :cond_10
    if-nez p1, :cond_16

    iget v0, p0, Lcom/a/b/d/zp;->f:I

    if-eq p2, v0, :cond_f

    .line 103
    :cond_16
    iget-object v0, p0, Lcom/a/b/d/zp;->b:Lcom/a/b/d/zq;

    invoke-virtual {v0, p1, p2}, Lcom/a/b/d/zq;->a(II)Lcom/a/b/d/me;

    move-result-object v1

    check-cast v1, Lcom/a/b/d/zq;

    .line 105
    new-instance v0, Lcom/a/b/d/zp;

    iget-object v2, p0, Lcom/a/b/d/zp;->c:[I

    iget-object v3, p0, Lcom/a/b/d/zp;->d:[J

    iget v4, p0, Lcom/a/b/d/zp;->e:I

    add-int/2addr v4, p1

    sub-int v5, p2, p1

    invoke-direct/range {v0 .. v5}, Lcom/a/b/d/zp;-><init>(Lcom/a/b/d/zq;[I[JII)V

    move-object p0, v0

    goto :goto_f
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 70
    iget-object v0, p0, Lcom/a/b/d/zp;->b:Lcom/a/b/d/zq;

    invoke-virtual {v0, p1}, Lcom/a/b/d/zq;->c(Ljava/lang/Object;)I

    move-result v0

    .line 71
    const/4 v1, -0x1

    if-ne v0, v1, :cond_b

    const/4 v0, 0x0

    :goto_a
    return v0

    :cond_b
    iget-object v1, p0, Lcom/a/b/d/zp;->c:[I

    iget v2, p0, Lcom/a/b/d/zp;->e:I

    add-int/2addr v0, v2

    aget v0, v1, v0

    goto :goto_a
.end method

.method public final a(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 87
    iget-object v2, p0, Lcom/a/b/d/zp;->b:Lcom/a/b/d/zq;

    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    sget-object v3, Lcom/a/b/d/ce;->b:Lcom/a/b/d/ce;

    if-ne v0, v3, :cond_15

    const/4 v0, 0x1

    :goto_c
    invoke-virtual {v2, p1, v0}, Lcom/a/b/d/zq;->e(Ljava/lang/Object;Z)I

    move-result v0

    invoke-direct {p0, v1, v0}, Lcom/a/b/d/zp;->a(II)Lcom/a/b/d/ma;

    move-result-object v0

    return-object v0

    :cond_15
    move v0, v1

    goto :goto_c
.end method

.method final a(I)Lcom/a/b/d/xd;
    .registers 5

    .prologue
    .line 53
    iget-object v0, p0, Lcom/a/b/d/zp;->b:Lcom/a/b/d/zq;

    invoke-virtual {v0}, Lcom/a/b/d/zq;->f()Lcom/a/b/d/jl;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/zp;->c:[I

    iget v2, p0, Lcom/a/b/d/zp;->e:I

    add-int/2addr v2, p1

    aget v1, v1, v2

    invoke-static {v0, v1}, Lcom/a/b/d/xe;->a(Ljava/lang/Object;I)Lcom/a/b/d/xd;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;
    .registers 6

    .prologue
    .line 92
    iget-object v1, p0, Lcom/a/b/d/zp;->b:Lcom/a/b/d/zq;

    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/a/b/d/ce;->b:Lcom/a/b/d/ce;

    if-ne v0, v2, :cond_16

    const/4 v0, 0x1

    :goto_b
    invoke-virtual {v1, p1, v0}, Lcom/a/b/d/zq;->f(Ljava/lang/Object;Z)I

    move-result v0

    iget v1, p0, Lcom/a/b/d/zp;->f:I

    invoke-direct {p0, v0, v1}, Lcom/a/b/d/zp;->a(II)Lcom/a/b/d/ma;

    move-result-object v0

    return-object v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public final b()Lcom/a/b/d/me;
    .registers 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/a/b/d/zp;->b:Lcom/a/b/d/zq;

    return-object v0
.end method

.method public final synthetic c(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;
    .registers 4

    .prologue
    .line 30
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/zp;->b(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;
    .registers 4

    .prologue
    .line 30
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/zp;->a(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic e_()Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 30
    .line 1082
    iget-object v0, p0, Lcom/a/b/d/zp;->b:Lcom/a/b/d/zq;

    .line 30
    return-object v0
.end method

.method public final h()Lcom/a/b/d/xd;
    .registers 2

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/a/b/d/zp;->a(I)Lcom/a/b/d/xd;

    move-result-object v0

    return-object v0
.end method

.method final h_()Z
    .registers 3

    .prologue
    .line 112
    iget v0, p0, Lcom/a/b/d/zp;->e:I

    if-gtz v0, :cond_b

    iget v0, p0, Lcom/a/b/d/zp;->f:I

    iget-object v1, p0, Lcom/a/b/d/zp;->c:[I

    array-length v1, v1

    if-ge v0, v1, :cond_d

    :cond_b
    const/4 v0, 0x1

    :goto_c
    return v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public final i()Lcom/a/b/d/xd;
    .registers 2

    .prologue
    .line 65
    iget v0, p0, Lcom/a/b/d/zp;->f:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/a/b/d/zp;->a(I)Lcom/a/b/d/xd;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic n()Ljava/util/SortedSet;
    .registers 2

    .prologue
    .line 30
    .line 2082
    iget-object v0, p0, Lcom/a/b/d/zp;->b:Lcom/a/b/d/zq;

    .line 30
    return-object v0
.end method

.method public final bridge synthetic n_()Ljava/util/Set;
    .registers 2

    .prologue
    .line 30
    .line 3082
    iget-object v0, p0, Lcom/a/b/d/zp;->b:Lcom/a/b/d/zq;

    .line 30
    return-object v0
.end method

.method public final size()I
    .registers 5

    .prologue
    .line 76
    iget-object v0, p0, Lcom/a/b/d/zp;->d:[J

    iget v1, p0, Lcom/a/b/d/zp;->e:I

    iget v2, p0, Lcom/a/b/d/zp;->f:I

    add-int/2addr v1, v2

    aget-wide v0, v0, v1

    iget-object v2, p0, Lcom/a/b/d/zp;->d:[J

    iget v3, p0, Lcom/a/b/d/zp;->e:I

    aget-wide v2, v2, v3

    sub-long/2addr v0, v2

    .line 77
    invoke-static {v0, v1}, Lcom/a/b/l/q;->b(J)I

    move-result v0

    return v0
.end method
