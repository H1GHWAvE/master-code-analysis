.class public Lcom/a/b/d/lr;
.super Lcom/a/b/d/kk;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/aac;


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
    b = true
.end annotation


# static fields
.field private static final f:J
    .annotation build Lcom/a/b/a/c;
        a = "not needed in emulated source."
    .end annotation
.end field


# instance fields
.field private final transient a:Lcom/a/b/d/lo;

.field private transient d:Lcom/a/b/d/lr;

.field private transient e:Lcom/a/b/d/lo;


# direct methods
.method constructor <init>(Lcom/a/b/d/jt;ILjava/util/Comparator;)V
    .registers 5
    .param p3    # Ljava/util/Comparator;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 345
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/kk;-><init>(Lcom/a/b/d/jt;I)V

    .line 346
    invoke-static {p3}, Lcom/a/b/d/lr;->a(Ljava/util/Comparator;)Lcom/a/b/d/lo;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/lr;->a:Lcom/a/b/d/lo;

    .line 347
    return-void
.end method

.method private A()Lcom/a/b/d/lr;
    .registers 5

    .prologue
    .line 9148
    new-instance v1, Lcom/a/b/d/ls;

    invoke-direct {v1}, Lcom/a/b/d/ls;-><init>()V

    .line 382
    invoke-direct {p0}, Lcom/a/b/d/lr;->D()Lcom/a/b/d/lo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lo;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_25

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 383
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/a/b/d/ls;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ls;

    goto :goto_d

    .line 385
    :cond_25
    invoke-virtual {v1}, Lcom/a/b/d/ls;->a()Lcom/a/b/d/lr;

    move-result-object v0

    .line 386
    iput-object p0, v0, Lcom/a/b/d/lr;->d:Lcom/a/b/d/lr;

    .line 387
    return-object v0
.end method

.method private static B()Lcom/a/b/d/lo;
    .registers 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 397
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method private static C()Lcom/a/b/d/lo;
    .registers 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 408
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method private D()Lcom/a/b/d/lo;
    .registers 2

    .prologue
    .line 419
    iget-object v0, p0, Lcom/a/b/d/lr;->e:Lcom/a/b/d/lo;

    .line 420
    if-nez v0, :cond_b

    new-instance v0, Lcom/a/b/d/lu;

    invoke-direct {v0, p0}, Lcom/a/b/d/lu;-><init>(Lcom/a/b/d/lr;)V

    iput-object v0, p0, Lcom/a/b/d/lr;->e:Lcom/a/b/d/lo;

    :cond_b
    return-object v0
.end method

.method private E()Ljava/util/Comparator;
    .registers 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 484
    iget-object v0, p0, Lcom/a/b/d/lr;->a:Lcom/a/b/d/lo;

    instance-of v0, v0, Lcom/a/b/d/me;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/a/b/d/lr;->a:Lcom/a/b/d/lo;

    check-cast v0, Lcom/a/b/d/me;

    invoke-virtual {v0}, Lcom/a/b/d/me;->comparator()Ljava/util/Comparator;

    move-result-object v0

    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method private static a(Ljava/util/Comparator;)Lcom/a/b/d/lo;
    .registers 2
    .param p0    # Ljava/util/Comparator;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 467
    if-nez p0, :cond_7

    invoke-static {}, Lcom/a/b/d/lo;->h()Lcom/a/b/d/lo;

    move-result-object v0

    :goto_6
    return-object v0

    :cond_7
    invoke-static {p0}, Lcom/a/b/d/me;->a(Ljava/util/Comparator;)Lcom/a/b/d/me;

    move-result-object v0

    goto :goto_6
.end method

.method private static a(Ljava/util/Comparator;Ljava/util/Collection;)Lcom/a/b/d/lo;
    .registers 3
    .param p0    # Ljava/util/Comparator;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 460
    if-nez p0, :cond_7

    invoke-static {p1}, Lcom/a/b/d/lo;->a(Ljava/util/Collection;)Lcom/a/b/d/lo;

    move-result-object v0

    :goto_6
    return-object v0

    :cond_7
    invoke-static {p0, p1}, Lcom/a/b/d/me;->a(Ljava/util/Comparator;Ljava/util/Collection;)Lcom/a/b/d/me;

    move-result-object v0

    goto :goto_6
.end method

.method public static a()Lcom/a/b/d/lr;
    .registers 1

    .prologue
    .line 73
    sget-object v0, Lcom/a/b/d/ez;->a:Lcom/a/b/d/ez;

    return-object v0
.end method

.method static synthetic a(Lcom/a/b/d/vi;Ljava/util/Comparator;)Lcom/a/b/d/lr;
    .registers 3

    .prologue
    .line 65
    invoke-static {p0, p1}, Lcom/a/b/d/lr;->b(Lcom/a/b/d/vi;Ljava/util/Comparator;)Lcom/a/b/d/lr;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/lr;
    .registers 5

    .prologue
    .line 2148
    new-instance v0, Lcom/a/b/d/ls;

    invoke-direct {v0}, Lcom/a/b/d/ls;-><init>()V

    .line 92
    invoke-virtual {v0, p0, p1}, Lcom/a/b/d/ls;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ls;

    .line 93
    invoke-virtual {v0, p2, p3}, Lcom/a/b/d/ls;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ls;

    .line 94
    invoke-virtual {v0}, Lcom/a/b/d/ls;->a()Lcom/a/b/d/lr;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/lr;
    .registers 7

    .prologue
    .line 3148
    new-instance v0, Lcom/a/b/d/ls;

    invoke-direct {v0}, Lcom/a/b/d/ls;-><init>()V

    .line 105
    invoke-virtual {v0, p0, p1}, Lcom/a/b/d/ls;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ls;

    .line 106
    invoke-virtual {v0, p2, p3}, Lcom/a/b/d/ls;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ls;

    .line 107
    invoke-virtual {v0, p4, p5}, Lcom/a/b/d/ls;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ls;

    .line 108
    invoke-virtual {v0}, Lcom/a/b/d/ls;->a()Lcom/a/b/d/lr;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/lr;
    .registers 9

    .prologue
    .line 4148
    new-instance v0, Lcom/a/b/d/ls;

    invoke-direct {v0}, Lcom/a/b/d/ls;-><init>()V

    .line 119
    invoke-virtual {v0, p0, p1}, Lcom/a/b/d/ls;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ls;

    .line 120
    invoke-virtual {v0, p2, p3}, Lcom/a/b/d/ls;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ls;

    .line 121
    invoke-virtual {v0, p4, p5}, Lcom/a/b/d/ls;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ls;

    .line 122
    invoke-virtual {v0, p6, p7}, Lcom/a/b/d/ls;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ls;

    .line 123
    invoke-virtual {v0}, Lcom/a/b/d/ls;->a()Lcom/a/b/d/lr;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/lr;
    .registers 11

    .prologue
    .line 5148
    new-instance v0, Lcom/a/b/d/ls;

    invoke-direct {v0}, Lcom/a/b/d/ls;-><init>()V

    .line 134
    invoke-virtual {v0, p0, p1}, Lcom/a/b/d/ls;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ls;

    .line 135
    invoke-virtual {v0, p2, p3}, Lcom/a/b/d/ls;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ls;

    .line 136
    invoke-virtual {v0, p4, p5}, Lcom/a/b/d/ls;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ls;

    .line 137
    invoke-virtual {v0, p6, p7}, Lcom/a/b/d/ls;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ls;

    .line 138
    invoke-virtual {v0, p8, p9}, Lcom/a/b/d/ls;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ls;

    .line 139
    invoke-virtual {v0}, Lcom/a/b/d/ls;->a()Lcom/a/b/d/lr;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/io/ObjectInputStream;)V
    .registers 13
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectInputStream"
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 494
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 495
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    .line 497
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v5

    .line 498
    if-gez v5, :cond_2b

    .line 499
    new-instance v0, Ljava/io/InvalidObjectException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x1d

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Invalid key count "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 501
    :cond_2b
    invoke-static {}, Lcom/a/b/d/jt;->l()Lcom/a/b/d/ju;

    move-result-object v6

    move v3, v2

    move v4, v2

    .line 505
    :goto_31
    if-ge v3, v5, :cond_a4

    .line 506
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v7

    .line 507
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v8

    .line 508
    if-gtz v8, :cond_58

    .line 509
    new-instance v0, Ljava/io/InvalidObjectException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x1f

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Invalid value count "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 512
    :cond_58
    new-array v9, v8, [Ljava/lang/Object;

    move v1, v2

    .line 513
    :goto_5b
    if-ge v1, v8, :cond_66

    .line 514
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v10

    aput-object v10, v9, v1

    .line 513
    add-int/lit8 v1, v1, 0x1

    goto :goto_5b

    .line 516
    :cond_66
    invoke-static {v9}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/lr;->a(Ljava/util/Comparator;Ljava/util/Collection;)Lcom/a/b/d/lo;

    move-result-object v1

    .line 517
    invoke-virtual {v1}, Lcom/a/b/d/lo;->size()I

    move-result v10

    array-length v9, v9

    if-eq v10, v9, :cond_9c

    .line 518
    new-instance v0, Ljava/io/InvalidObjectException;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x28

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Duplicate key-value pairs exist for key "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 521
    :cond_9c
    invoke-virtual {v6, v7, v1}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    .line 522
    add-int/2addr v4, v8

    .line 505
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_31

    .line 527
    :cond_a4
    :try_start_a4
    invoke-virtual {v6}, Lcom/a/b/d/ju;->a()Lcom/a/b/d/jt;
    :try_end_a7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_a4 .. :try_end_a7} :catch_bc

    move-result-object v1

    .line 533
    sget-object v2, Lcom/a/b/d/kq;->a:Lcom/a/b/d/aab;

    invoke-virtual {v2, p0, v1}, Lcom/a/b/d/aab;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 534
    sget-object v1, Lcom/a/b/d/kq;->b:Lcom/a/b/d/aab;

    invoke-virtual {v1, p0, v4}, Lcom/a/b/d/aab;->a(Ljava/lang/Object;I)V

    .line 535
    sget-object v1, Lcom/a/b/d/kq;->c:Lcom/a/b/d/aab;

    invoke-static {v0}, Lcom/a/b/d/lr;->a(Ljava/util/Comparator;)Lcom/a/b/d/lo;

    move-result-object v0

    invoke-virtual {v1, p0, v0}, Lcom/a/b/d/aab;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 537
    return-void

    .line 528
    :catch_bc
    move-exception v0

    .line 529
    new-instance v1, Ljava/io/InvalidObjectException;

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/io/InvalidObjectException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Ljava/io/InvalidObjectException;

    throw v0
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectOutputStream"
    .end annotation

    .prologue
    .line 478
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 9484
    iget-object v0, p0, Lcom/a/b/d/lr;->a:Lcom/a/b/d/lo;

    instance-of v0, v0, Lcom/a/b/d/me;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/a/b/d/lr;->a:Lcom/a/b/d/lo;

    check-cast v0, Lcom/a/b/d/me;

    invoke-virtual {v0}, Lcom/a/b/d/me;->comparator()Ljava/util/Comparator;

    move-result-object v0

    .line 479
    :goto_11
    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 480
    invoke-static {p0, p1}, Lcom/a/b/d/zz;->a(Lcom/a/b/d/vi;Ljava/io/ObjectOutputStream;)V

    .line 481
    return-void

    .line 9484
    :cond_18
    const/4 v0, 0x0

    goto :goto_11
.end method

.method private static b(Lcom/a/b/d/vi;Ljava/util/Comparator;)Lcom/a/b/d/lr;
    .registers 8

    .prologue
    .line 305
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 306
    invoke-interface {p0}, Lcom/a/b/d/vi;->n()Z

    move-result v0

    if-eqz v0, :cond_e

    if-nez p1, :cond_e

    .line 6073
    sget-object v0, Lcom/a/b/d/ez;->a:Lcom/a/b/d/ez;

    .line 333
    :cond_d
    :goto_d
    return-object v0

    .line 310
    :cond_e
    instance-of v0, p0, Lcom/a/b/d/lr;

    if-eqz v0, :cond_1d

    move-object v0, p0

    .line 312
    check-cast v0, Lcom/a/b/d/lr;

    .line 6438
    iget-object v1, v0, Lcom/a/b/d/kk;->b:Lcom/a/b/d/jt;

    invoke-virtual {v1}, Lcom/a/b/d/jt;->i_()Z

    move-result v1

    .line 314
    if-eqz v1, :cond_d

    .line 319
    :cond_1d
    invoke-static {}, Lcom/a/b/d/jt;->l()Lcom/a/b/d/ju;

    move-result-object v2

    .line 320
    const/4 v0, 0x0

    .line 323
    invoke-interface {p0}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_2f
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_59

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 324
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    .line 325
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 326
    invoke-static {p1, v0}, Lcom/a/b/d/lr;->a(Ljava/util/Comparator;Ljava/util/Collection;)Lcom/a/b/d/lo;

    move-result-object v0

    .line 327
    invoke-virtual {v0}, Lcom/a/b/d/lo;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_63

    .line 328
    invoke-virtual {v2, v4, v0}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    .line 329
    invoke-virtual {v0}, Lcom/a/b/d/lo;->size()I

    move-result v0

    add-int/2addr v0, v1

    :goto_57
    move v1, v0

    .line 331
    goto :goto_2f

    .line 333
    :cond_59
    new-instance v0, Lcom/a/b/d/lr;

    invoke-virtual {v2}, Lcom/a/b/d/ju;->a()Lcom/a/b/d/jt;

    move-result-object v2

    invoke-direct {v0, v2, v1, p1}, Lcom/a/b/d/lr;-><init>(Lcom/a/b/d/jt;ILjava/util/Comparator;)V

    goto :goto_d

    :cond_63
    move v0, v1

    goto :goto_57
.end method

.method private static c(Lcom/a/b/d/vi;)Lcom/a/b/d/lr;
    .registers 2

    .prologue
    .line 299
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/a/b/d/lr;->b(Lcom/a/b/d/vi;Ljava/util/Comparator;)Lcom/a/b/d/lr;

    move-result-object v0

    return-object v0
.end method

.method public static c()Lcom/a/b/d/ls;
    .registers 1

    .prologue
    .line 148
    new-instance v0, Lcom/a/b/d/ls;

    invoke-direct {v0}, Lcom/a/b/d/ls;-><init>()V

    return-object v0
.end method

.method private static d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/lr;
    .registers 3

    .prologue
    .line 1148
    new-instance v0, Lcom/a/b/d/ls;

    invoke-direct {v0}, Lcom/a/b/d/ls;-><init>()V

    .line 81
    invoke-virtual {v0, p0, p1}, Lcom/a/b/d/ls;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ls;

    .line 82
    invoke-virtual {v0}, Lcom/a/b/d/ls;->a()Lcom/a/b/d/lr;

    move-result-object v0

    return-object v0
.end method

.method private e(Ljava/lang/Object;)Lcom/a/b/d/lo;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 359
    iget-object v0, p0, Lcom/a/b/d/lr;->b:Lcom/a/b/d/jt;

    invoke-virtual {v0, p1}, Lcom/a/b/d/jt;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/lo;

    .line 360
    iget-object v1, p0, Lcom/a/b/d/lr;->a:Lcom/a/b/d/lo;

    invoke-static {v0, v1}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/lo;

    return-object v0
.end method

.method private z()Lcom/a/b/d/lr;
    .registers 5

    .prologue
    .line 376
    iget-object v0, p0, Lcom/a/b/d/lr;->d:Lcom/a/b/d/lr;

    .line 377
    if-nez v0, :cond_31

    .line 8148
    new-instance v1, Lcom/a/b/d/ls;

    invoke-direct {v1}, Lcom/a/b/d/ls;-><init>()V

    .line 7382
    invoke-direct {p0}, Lcom/a/b/d/lr;->D()Lcom/a/b/d/lo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lo;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_11
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_29

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 7383
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/a/b/d/ls;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ls;

    goto :goto_11

    .line 7385
    :cond_29
    invoke-virtual {v1}, Lcom/a/b/d/ls;->a()Lcom/a/b/d/lr;

    move-result-object v0

    .line 7386
    iput-object p0, v0, Lcom/a/b/d/lr;->d:Lcom/a/b/d/lr;

    .line 377
    iput-object v0, p0, Lcom/a/b/d/lr;->d:Lcom/a/b/d/lr;

    :cond_31
    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Ljava/util/Set;
    .registers 3

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/a/b/d/lr;->e(Ljava/lang/Object;)Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;
    .registers 4

    .prologue
    .line 13408
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 4

    .prologue
    .line 12408
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic b(Ljava/lang/Object;)Ljava/util/Set;
    .registers 3

    .prologue
    .line 14397
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/a/b/d/lr;->e(Ljava/lang/Object;)Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/a/b/d/kk;
    .registers 5

    .prologue
    .line 64
    .line 10376
    iget-object v0, p0, Lcom/a/b/d/lr;->d:Lcom/a/b/d/lr;

    .line 10377
    if-nez v0, :cond_31

    .line 11148
    new-instance v1, Lcom/a/b/d/ls;

    invoke-direct {v1}, Lcom/a/b/d/ls;-><init>()V

    .line 10382
    invoke-direct {p0}, Lcom/a/b/d/lr;->D()Lcom/a/b/d/lo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lo;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_11
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_29

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 10383
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/a/b/d/ls;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ls;

    goto :goto_11

    .line 10385
    :cond_29
    invoke-virtual {v1}, Lcom/a/b/d/ls;->a()Lcom/a/b/d/lr;

    move-result-object v0

    .line 10386
    iput-object p0, v0, Lcom/a/b/d/lr;->d:Lcom/a/b/d/lr;

    .line 10377
    iput-object v0, p0, Lcom/a/b/d/lr;->d:Lcom/a/b/d/lr;

    :cond_31
    return-object v0
.end method

.method public final synthetic d(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 13397
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic e()Lcom/a/b/d/iz;
    .registers 2

    .prologue
    .line 11408
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic h(Ljava/lang/Object;)Lcom/a/b/d/iz;
    .registers 3

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/a/b/d/lr;->e(Ljava/lang/Object;)Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/a/b/d/lr;->D()Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic t()Lcom/a/b/d/iz;
    .registers 2

    .prologue
    .line 12397
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic u()Ljava/util/Set;
    .registers 2

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/a/b/d/lr;->D()Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic v()Lcom/a/b/d/iz;
    .registers 2

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/a/b/d/lr;->D()Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method
