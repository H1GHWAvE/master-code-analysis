.class Lcom/a/b/d/acq;
.super Lcom/a/b/d/uj;
.source "SourceFile"


# instance fields
.field final synthetic b:Lcom/a/b/d/abx;


# direct methods
.method constructor <init>(Lcom/a/b/d/abx;)V
    .registers 2

    .prologue
    .line 701
    iput-object p1, p0, Lcom/a/b/d/acq;->b:Lcom/a/b/d/abx;

    invoke-direct {p0}, Lcom/a/b/d/uj;-><init>()V

    .line 720
    return-void
.end method

.method private a(Ljava/lang/Object;)Ljava/util/Map;
    .registers 3

    .prologue
    .line 709
    iget-object v0, p0, Lcom/a/b/d/acq;->b:Lcom/a/b/d/abx;

    invoke-virtual {v0, p1}, Lcom/a/b/d/abx;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/a/b/d/acq;->b:Lcom/a/b/d/abx;

    invoke-virtual {v0, p1}, Lcom/a/b/d/abx;->e(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method private b(Ljava/lang/Object;)Ljava/util/Map;
    .registers 3

    .prologue
    .line 713
    if-nez p1, :cond_4

    const/4 v0, 0x0

    :goto_3
    return-object v0

    :cond_4
    iget-object v0, p0, Lcom/a/b/d/acq;->b:Lcom/a/b/d/abx;

    iget-object v0, v0, Lcom/a/b/d/abx;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    goto :goto_3
.end method


# virtual methods
.method protected final a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 717
    new-instance v0, Lcom/a/b/d/acr;

    invoke-direct {v0, p0}, Lcom/a/b/d/acr;-><init>(Lcom/a/b/d/acq;)V

    return-object v0
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 703
    iget-object v0, p0, Lcom/a/b/d/acq;->b:Lcom/a/b/d/abx;

    invoke-virtual {v0, p1}, Lcom/a/b/d/abx;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 701
    .line 2709
    iget-object v0, p0, Lcom/a/b/d/acq;->b:Lcom/a/b/d/abx;

    invoke-virtual {v0, p1}, Lcom/a/b/d/abx;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/a/b/d/acq;->b:Lcom/a/b/d/abx;

    invoke-virtual {v0, p1}, Lcom/a/b/d/abx;->e(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    .line 701
    goto :goto_e
.end method

.method public bridge synthetic remove(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 701
    .line 1713
    if-nez p1, :cond_4

    const/4 v0, 0x0

    :goto_3
    return-object v0

    :cond_4
    iget-object v0, p0, Lcom/a/b/d/acq;->b:Lcom/a/b/d/abx;

    iget-object v0, v0, Lcom/a/b/d/abx;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    goto :goto_3
.end method
