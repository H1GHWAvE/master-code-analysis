.class public final Lcom/a/b/d/oj;
.super Lcom/a/b/d/an;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/ou;
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
    b = true
.end annotation


# static fields
.field private static final f:J
    .annotation build Lcom/a/b/a/c;
        a = "java serialization not supported"
    .end annotation
.end field


# instance fields
.field private transient a:Lcom/a/b/d/or;

.field private transient b:Lcom/a/b/d/or;

.field private transient c:Ljava/util/Map;

.field private transient d:I

.field private transient e:I


# direct methods
.method constructor <init>()V
    .registers 2

    .prologue
    .line 199
    invoke-direct {p0}, Lcom/a/b/d/an;-><init>()V

    .line 200
    invoke-static {}, Lcom/a/b/d/sz;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/oj;->c:Ljava/util/Map;

    .line 201
    return-void
.end method

.method private constructor <init>(I)V
    .registers 3

    .prologue
    .line 203
    invoke-direct {p0}, Lcom/a/b/d/an;-><init>()V

    .line 204
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/a/b/d/oj;->c:Ljava/util/Map;

    .line 205
    return-void
.end method

.method private constructor <init>(Lcom/a/b/d/vi;)V
    .registers 3

    .prologue
    .line 208
    invoke-interface {p1}, Lcom/a/b/d/vi;->p()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/a/b/d/oj;-><init>(I)V

    .line 209
    invoke-virtual {p0, p1}, Lcom/a/b/d/oj;->a(Lcom/a/b/d/vi;)Z

    .line 210
    return-void
.end method

.method static synthetic a(Lcom/a/b/d/oj;)I
    .registers 2

    .prologue
    .line 102
    iget v0, p0, Lcom/a/b/d/oj;->e:I

    return v0
.end method

.method private static a()Lcom/a/b/d/oj;
    .registers 1

    .prologue
    .line 173
    new-instance v0, Lcom/a/b/d/oj;

    invoke-direct {v0}, Lcom/a/b/d/oj;-><init>()V

    return-object v0
.end method

.method private static a(I)Lcom/a/b/d/oj;
    .registers 2

    .prologue
    .line 184
    new-instance v0, Lcom/a/b/d/oj;

    invoke-direct {v0, p0}, Lcom/a/b/d/oj;-><init>(I)V

    return-object v0
.end method

.method static synthetic a(Lcom/a/b/d/oj;Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/or;)Lcom/a/b/d/or;
    .registers 5

    .prologue
    .line 102
    invoke-direct {p0, p1, p2, p3}, Lcom/a/b/d/oj;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/or;)Lcom/a/b/d/or;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/or;)Lcom/a/b/d/or;
    .registers 7
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/a/b/d/or;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 220
    new-instance v1, Lcom/a/b/d/or;

    invoke-direct {v1, p1, p2}, Lcom/a/b/d/or;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 221
    iget-object v0, p0, Lcom/a/b/d/oj;->a:Lcom/a/b/d/or;

    if-nez v0, :cond_24

    .line 222
    iput-object v1, p0, Lcom/a/b/d/oj;->b:Lcom/a/b/d/or;

    iput-object v1, p0, Lcom/a/b/d/oj;->a:Lcom/a/b/d/or;

    .line 223
    iget-object v0, p0, Lcom/a/b/d/oj;->c:Ljava/util/Map;

    new-instance v2, Lcom/a/b/d/oq;

    invoke-direct {v2, v1}, Lcom/a/b/d/oq;-><init>(Lcom/a/b/d/or;)V

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    iget v0, p0, Lcom/a/b/d/oj;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/oj;->e:I

    .line 260
    :goto_1d
    iget v0, p0, Lcom/a/b/d/oj;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/oj;->d:I

    .line 261
    return-object v1

    .line 225
    :cond_24
    if-nez p3, :cond_5a

    .line 226
    iget-object v0, p0, Lcom/a/b/d/oj;->b:Lcom/a/b/d/or;

    iput-object v1, v0, Lcom/a/b/d/or;->c:Lcom/a/b/d/or;

    .line 227
    iget-object v0, p0, Lcom/a/b/d/oj;->b:Lcom/a/b/d/or;

    iput-object v0, v1, Lcom/a/b/d/or;->d:Lcom/a/b/d/or;

    .line 228
    iput-object v1, p0, Lcom/a/b/d/oj;->b:Lcom/a/b/d/or;

    .line 229
    iget-object v0, p0, Lcom/a/b/d/oj;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/oq;

    .line 230
    if-nez v0, :cond_4b

    .line 231
    iget-object v0, p0, Lcom/a/b/d/oj;->c:Ljava/util/Map;

    new-instance v2, Lcom/a/b/d/oq;

    invoke-direct {v2, v1}, Lcom/a/b/d/oq;-><init>(Lcom/a/b/d/or;)V

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    iget v0, p0, Lcom/a/b/d/oj;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/oj;->e:I

    goto :goto_1d

    .line 234
    :cond_4b
    iget v2, v0, Lcom/a/b/d/oq;->c:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/a/b/d/oq;->c:I

    .line 235
    iget-object v2, v0, Lcom/a/b/d/oq;->b:Lcom/a/b/d/or;

    .line 236
    iput-object v1, v2, Lcom/a/b/d/or;->e:Lcom/a/b/d/or;

    .line 237
    iput-object v2, v1, Lcom/a/b/d/or;->f:Lcom/a/b/d/or;

    .line 238
    iput-object v1, v0, Lcom/a/b/d/oq;->b:Lcom/a/b/d/or;

    goto :goto_1d

    .line 241
    :cond_5a
    iget-object v0, p0, Lcom/a/b/d/oj;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/oq;

    .line 242
    iget v2, v0, Lcom/a/b/d/oq;->c:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/a/b/d/oq;->c:I

    .line 243
    iget-object v0, p3, Lcom/a/b/d/or;->d:Lcom/a/b/d/or;

    iput-object v0, v1, Lcom/a/b/d/or;->d:Lcom/a/b/d/or;

    .line 244
    iget-object v0, p3, Lcom/a/b/d/or;->f:Lcom/a/b/d/or;

    iput-object v0, v1, Lcom/a/b/d/or;->f:Lcom/a/b/d/or;

    .line 245
    iput-object p3, v1, Lcom/a/b/d/or;->c:Lcom/a/b/d/or;

    .line 246
    iput-object p3, v1, Lcom/a/b/d/or;->e:Lcom/a/b/d/or;

    .line 247
    iget-object v0, p3, Lcom/a/b/d/or;->f:Lcom/a/b/d/or;

    if-nez v0, :cond_8d

    .line 248
    iget-object v0, p0, Lcom/a/b/d/oj;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/oq;

    iput-object v1, v0, Lcom/a/b/d/oq;->a:Lcom/a/b/d/or;

    .line 252
    :goto_82
    iget-object v0, p3, Lcom/a/b/d/or;->d:Lcom/a/b/d/or;

    if-nez v0, :cond_92

    .line 253
    iput-object v1, p0, Lcom/a/b/d/oj;->a:Lcom/a/b/d/or;

    .line 257
    :goto_88
    iput-object v1, p3, Lcom/a/b/d/or;->d:Lcom/a/b/d/or;

    .line 258
    iput-object v1, p3, Lcom/a/b/d/or;->f:Lcom/a/b/d/or;

    goto :goto_1d

    .line 250
    :cond_8d
    iget-object v0, p3, Lcom/a/b/d/or;->f:Lcom/a/b/d/or;

    iput-object v1, v0, Lcom/a/b/d/or;->e:Lcom/a/b/d/or;

    goto :goto_82

    .line 255
    :cond_92
    iget-object v0, p3, Lcom/a/b/d/or;->d:Lcom/a/b/d/or;

    iput-object v1, v0, Lcom/a/b/d/or;->c:Lcom/a/b/d/or;

    goto :goto_88
.end method

.method static synthetic a(Lcom/a/b/d/oj;Lcom/a/b/d/or;)V
    .registers 5

    .prologue
    .line 6270
    iget-object v0, p1, Lcom/a/b/d/or;->d:Lcom/a/b/d/or;

    if-eqz v0, :cond_36

    .line 6271
    iget-object v0, p1, Lcom/a/b/d/or;->d:Lcom/a/b/d/or;

    iget-object v1, p1, Lcom/a/b/d/or;->c:Lcom/a/b/d/or;

    iput-object v1, v0, Lcom/a/b/d/or;->c:Lcom/a/b/d/or;

    .line 6275
    :goto_a
    iget-object v0, p1, Lcom/a/b/d/or;->c:Lcom/a/b/d/or;

    if-eqz v0, :cond_3b

    .line 6276
    iget-object v0, p1, Lcom/a/b/d/or;->c:Lcom/a/b/d/or;

    iget-object v1, p1, Lcom/a/b/d/or;->d:Lcom/a/b/d/or;

    iput-object v1, v0, Lcom/a/b/d/or;->d:Lcom/a/b/d/or;

    .line 6280
    :goto_14
    iget-object v0, p1, Lcom/a/b/d/or;->f:Lcom/a/b/d/or;

    if-nez v0, :cond_40

    iget-object v0, p1, Lcom/a/b/d/or;->e:Lcom/a/b/d/or;

    if-nez v0, :cond_40

    .line 6281
    iget-object v0, p0, Lcom/a/b/d/oj;->c:Ljava/util/Map;

    iget-object v1, p1, Lcom/a/b/d/or;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/oq;

    .line 6282
    const/4 v1, 0x0

    iput v1, v0, Lcom/a/b/d/oq;->c:I

    .line 6283
    iget v0, p0, Lcom/a/b/d/oj;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/oj;->e:I

    .line 6300
    :goto_2f
    iget v0, p0, Lcom/a/b/d/oj;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/b/d/oj;->d:I

    .line 102
    return-void

    .line 6273
    :cond_36
    iget-object v0, p1, Lcom/a/b/d/or;->c:Lcom/a/b/d/or;

    iput-object v0, p0, Lcom/a/b/d/oj;->a:Lcom/a/b/d/or;

    goto :goto_a

    .line 6278
    :cond_3b
    iget-object v0, p1, Lcom/a/b/d/or;->d:Lcom/a/b/d/or;

    iput-object v0, p0, Lcom/a/b/d/oj;->b:Lcom/a/b/d/or;

    goto :goto_14

    .line 6285
    :cond_40
    iget-object v0, p0, Lcom/a/b/d/oj;->c:Ljava/util/Map;

    iget-object v1, p1, Lcom/a/b/d/or;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/oq;

    .line 6286
    iget v1, v0, Lcom/a/b/d/oq;->c:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/a/b/d/oq;->c:I

    .line 6288
    iget-object v1, p1, Lcom/a/b/d/or;->f:Lcom/a/b/d/or;

    if-nez v1, :cond_61

    .line 6289
    iget-object v1, p1, Lcom/a/b/d/or;->e:Lcom/a/b/d/or;

    iput-object v1, v0, Lcom/a/b/d/oq;->a:Lcom/a/b/d/or;

    .line 6294
    :goto_58
    iget-object v1, p1, Lcom/a/b/d/or;->e:Lcom/a/b/d/or;

    if-nez v1, :cond_68

    .line 6295
    iget-object v1, p1, Lcom/a/b/d/or;->f:Lcom/a/b/d/or;

    iput-object v1, v0, Lcom/a/b/d/oq;->b:Lcom/a/b/d/or;

    goto :goto_2f

    .line 6291
    :cond_61
    iget-object v1, p1, Lcom/a/b/d/or;->f:Lcom/a/b/d/or;

    iget-object v2, p1, Lcom/a/b/d/or;->e:Lcom/a/b/d/or;

    iput-object v2, v1, Lcom/a/b/d/or;->e:Lcom/a/b/d/or;

    goto :goto_58

    .line 6297
    :cond_68
    iget-object v0, p1, Lcom/a/b/d/or;->e:Lcom/a/b/d/or;

    iget-object v1, p1, Lcom/a/b/d/or;->f:Lcom/a/b/d/or;

    iput-object v1, v0, Lcom/a/b/d/or;->f:Lcom/a/b/d/or;

    goto :goto_2f
.end method

.method static synthetic a(Lcom/a/b/d/oj;Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 102
    invoke-direct {p0, p1}, Lcom/a/b/d/oj;->h(Ljava/lang/Object;)V

    return-void
.end method

.method private a(Lcom/a/b/d/or;)V
    .registers 5

    .prologue
    .line 270
    iget-object v0, p1, Lcom/a/b/d/or;->d:Lcom/a/b/d/or;

    if-eqz v0, :cond_36

    .line 271
    iget-object v0, p1, Lcom/a/b/d/or;->d:Lcom/a/b/d/or;

    iget-object v1, p1, Lcom/a/b/d/or;->c:Lcom/a/b/d/or;

    iput-object v1, v0, Lcom/a/b/d/or;->c:Lcom/a/b/d/or;

    .line 275
    :goto_a
    iget-object v0, p1, Lcom/a/b/d/or;->c:Lcom/a/b/d/or;

    if-eqz v0, :cond_3b

    .line 276
    iget-object v0, p1, Lcom/a/b/d/or;->c:Lcom/a/b/d/or;

    iget-object v1, p1, Lcom/a/b/d/or;->d:Lcom/a/b/d/or;

    iput-object v1, v0, Lcom/a/b/d/or;->d:Lcom/a/b/d/or;

    .line 280
    :goto_14
    iget-object v0, p1, Lcom/a/b/d/or;->f:Lcom/a/b/d/or;

    if-nez v0, :cond_40

    iget-object v0, p1, Lcom/a/b/d/or;->e:Lcom/a/b/d/or;

    if-nez v0, :cond_40

    .line 281
    iget-object v0, p0, Lcom/a/b/d/oj;->c:Ljava/util/Map;

    iget-object v1, p1, Lcom/a/b/d/or;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/oq;

    .line 282
    const/4 v1, 0x0

    iput v1, v0, Lcom/a/b/d/oq;->c:I

    .line 283
    iget v0, p0, Lcom/a/b/d/oj;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/oj;->e:I

    .line 300
    :goto_2f
    iget v0, p0, Lcom/a/b/d/oj;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/b/d/oj;->d:I

    .line 301
    return-void

    .line 273
    :cond_36
    iget-object v0, p1, Lcom/a/b/d/or;->c:Lcom/a/b/d/or;

    iput-object v0, p0, Lcom/a/b/d/oj;->a:Lcom/a/b/d/or;

    goto :goto_a

    .line 278
    :cond_3b
    iget-object v0, p1, Lcom/a/b/d/or;->d:Lcom/a/b/d/or;

    iput-object v0, p0, Lcom/a/b/d/oj;->b:Lcom/a/b/d/or;

    goto :goto_14

    .line 285
    :cond_40
    iget-object v0, p0, Lcom/a/b/d/oj;->c:Ljava/util/Map;

    iget-object v1, p1, Lcom/a/b/d/or;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/oq;

    .line 286
    iget v1, v0, Lcom/a/b/d/oq;->c:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/a/b/d/oq;->c:I

    .line 288
    iget-object v1, p1, Lcom/a/b/d/or;->f:Lcom/a/b/d/or;

    if-nez v1, :cond_61

    .line 289
    iget-object v1, p1, Lcom/a/b/d/or;->e:Lcom/a/b/d/or;

    iput-object v1, v0, Lcom/a/b/d/oq;->a:Lcom/a/b/d/or;

    .line 294
    :goto_58
    iget-object v1, p1, Lcom/a/b/d/or;->e:Lcom/a/b/d/or;

    if-nez v1, :cond_68

    .line 295
    iget-object v1, p1, Lcom/a/b/d/or;->f:Lcom/a/b/d/or;

    iput-object v1, v0, Lcom/a/b/d/oq;->b:Lcom/a/b/d/or;

    goto :goto_2f

    .line 291
    :cond_61
    iget-object v1, p1, Lcom/a/b/d/or;->f:Lcom/a/b/d/or;

    iget-object v2, p1, Lcom/a/b/d/or;->e:Lcom/a/b/d/or;

    iput-object v2, v1, Lcom/a/b/d/or;->e:Lcom/a/b/d/or;

    goto :goto_58

    .line 297
    :cond_68
    iget-object v0, p1, Lcom/a/b/d/or;->e:Lcom/a/b/d/or;

    iget-object v1, p1, Lcom/a/b/d/or;->f:Lcom/a/b/d/or;

    iput-object v1, v0, Lcom/a/b/d/or;->f:Lcom/a/b/d/or;

    goto :goto_2f
.end method

.method private a(Ljava/io/ObjectInputStream;)V
    .registers 6
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectInputStream"
    .end annotation

    .prologue
    .line 803
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 804
    invoke-static {}, Lcom/a/b/d/sz;->d()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/oj;->c:Ljava/util/Map;

    .line 805
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v1

    .line 806
    const/4 v0, 0x0

    :goto_e
    if-ge v0, v1, :cond_1e

    .line 808
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v2

    .line 810
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v3

    .line 811
    invoke-virtual {p0, v2, v3}, Lcom/a/b/d/oj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 806
    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    .line 813
    :cond_1e
    return-void
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
    .registers 5
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectOutputStream"
    .end annotation

    .prologue
    .line 792
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 793
    invoke-virtual {p0}, Lcom/a/b/d/oj;->f()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 1759
    invoke-super {p0}, Lcom/a/b/d/an;->k()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 794
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_14
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 795
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 796
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    goto :goto_14

    .line 798
    :cond_2f
    return-void
.end method

.method private static b(Lcom/a/b/d/vi;)Lcom/a/b/d/oj;
    .registers 2

    .prologue
    .line 196
    new-instance v0, Lcom/a/b/d/oj;

    invoke-direct {v0, p0}, Lcom/a/b/d/oj;-><init>(Lcom/a/b/d/vi;)V

    return-object v0
.end method

.method static synthetic b(Lcom/a/b/d/oj;)Lcom/a/b/d/or;
    .registers 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/a/b/d/oj;->b:Lcom/a/b/d/or;

    return-object v0
.end method

.method static synthetic c(Lcom/a/b/d/oj;)Lcom/a/b/d/or;
    .registers 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/a/b/d/oj;->a:Lcom/a/b/d/or;

    return-object v0
.end method

.method private c()Ljava/util/List;
    .registers 2

    .prologue
    .line 712
    invoke-super {p0}, Lcom/a/b/d/an;->i()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private d()Ljava/util/List;
    .registers 2

    .prologue
    .line 717
    new-instance v0, Lcom/a/b/d/om;

    invoke-direct {v0, p0}, Lcom/a/b/d/om;-><init>(Lcom/a/b/d/oj;)V

    return-object v0
.end method

.method static synthetic d(Lcom/a/b/d/oj;)Ljava/util/Map;
    .registers 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/a/b/d/oj;->c:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic e(Lcom/a/b/d/oj;)I
    .registers 2

    .prologue
    .line 102
    iget v0, p0, Lcom/a/b/d/oj;->d:I

    return v0
.end method

.method private e()Ljava/util/List;
    .registers 2

    .prologue
    .line 759
    invoke-super {p0}, Lcom/a/b/d/an;->k()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method static synthetic e(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 102
    .line 5310
    if-nez p0, :cond_8

    .line 5311
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 102
    :cond_8
    return-void
.end method

.method private h(Ljava/lang/Object;)V
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 305
    new-instance v0, Lcom/a/b/d/ot;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/ot;-><init>(Lcom/a/b/d/oj;Ljava/lang/Object;)V

    invoke-static {v0}, Lcom/a/b/d/nj;->i(Ljava/util/Iterator;)V

    .line 306
    return-void
.end method

.method private static i(Ljava/lang/Object;)V
    .registers 2
    .param p0    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 310
    if-nez p0, :cond_8

    .line 311
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 313
    :cond_8
    return-void
.end method

.method private j(Ljava/lang/Object;)Ljava/util/List;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 633
    new-instance v0, Lcom/a/b/d/ot;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/ot;-><init>(Lcom/a/b/d/oj;Ljava/lang/Object;)V

    invoke-static {v0}, Lcom/a/b/d/ov;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private t()Ljava/util/List;
    .registers 2

    .prologue
    .line 764
    new-instance v0, Lcom/a/b/d/oo;

    invoke-direct {v0, p0}, Lcom/a/b/d/oo;-><init>(Lcom/a/b/d/oj;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/util/List;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 671
    new-instance v0, Lcom/a/b/d/ok;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/ok;-><init>(Lcom/a/b/d/oj;Ljava/lang/Object;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/List;
    .registers 7
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 608
    invoke-direct {p0, p1}, Lcom/a/b/d/oj;->j(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 609
    new-instance v1, Lcom/a/b/d/ot;

    invoke-direct {v1, p0, p1}, Lcom/a/b/d/ot;-><init>(Lcom/a/b/d/oj;Ljava/lang/Object;)V

    .line 610
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 613
    :goto_d
    invoke-interface {v1}, Ljava/util/ListIterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_24

    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_24

    .line 614
    invoke-interface {v1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    .line 615
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    goto :goto_d

    .line 619
    :cond_24
    :goto_24
    invoke-interface {v1}, Ljava/util/ListIterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_31

    .line 620
    invoke-interface {v1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    .line 621
    invoke-interface {v1}, Ljava/util/ListIterator;->remove()V

    goto :goto_24

    .line 625
    :cond_31
    :goto_31
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3f

    .line 626
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    goto :goto_31

    .line 629
    :cond_3f
    return-object v0
.end method

.method public final bridge synthetic a(Lcom/a/b/d/vi;)Z
    .registers 3

    .prologue
    .line 101
    invoke-super {p0, p1}, Lcom/a/b/d/an;->a(Lcom/a/b/d/vi;)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 590
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/a/b/d/oj;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/or;)Lcom/a/b/d/or;

    .line 591
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 4

    .prologue
    .line 101
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/oj;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)Ljava/util/List;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 644
    invoke-direct {p0, p1}, Lcom/a/b/d/oj;->j(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 645
    invoke-direct {p0, p1}, Lcom/a/b/d/oj;->h(Ljava/lang/Object;)V

    .line 646
    return-object v0
.end method

.method public final bridge synthetic b()Ljava/util/Map;
    .registers 2

    .prologue
    .line 101
    invoke-super {p0}, Lcom/a/b/d/an;->b()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 101
    invoke-super {p0, p1, p2}, Lcom/a/b/d/an;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 101
    invoke-virtual {p0, p1}, Lcom/a/b/d/oj;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
    .registers 4

    .prologue
    .line 101
    invoke-super {p0, p1, p2}, Lcom/a/b/d/an;->c(Ljava/lang/Object;Ljava/lang/Iterable;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic c(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 101
    invoke-super {p0, p1, p2}, Lcom/a/b/d/an;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final synthetic d(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 101
    invoke-virtual {p0, p1}, Lcom/a/b/d/oj;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic equals(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 101
    invoke-super {p0, p1}, Lcom/a/b/d/an;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final f()I
    .registers 2

    .prologue
    .line 561
    iget v0, p0, Lcom/a/b/d/oj;->d:I

    return v0
.end method

.method public final f(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 571
    iget-object v0, p0, Lcom/a/b/d/oj;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final g()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 651
    iput-object v0, p0, Lcom/a/b/d/oj;->a:Lcom/a/b/d/or;

    .line 652
    iput-object v0, p0, Lcom/a/b/d/oj;->b:Lcom/a/b/d/or;

    .line 653
    iget-object v0, p0, Lcom/a/b/d/oj;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 654
    const/4 v0, 0x0

    iput v0, p0, Lcom/a/b/d/oj;->d:I

    .line 655
    iget v0, p0, Lcom/a/b/d/oj;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/oj;->e:I

    .line 656
    return-void
.end method

.method public final g(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 576
    .line 1712
    invoke-super {p0}, Lcom/a/b/d/an;->i()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 576
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final h()Ljava/util/Set;
    .registers 2

    .prologue
    .line 684
    new-instance v0, Lcom/a/b/d/ol;

    invoke-direct {v0, p0}, Lcom/a/b/d/ol;-><init>(Lcom/a/b/d/oj;)V

    return-object v0
.end method

.method public final bridge synthetic hashCode()I
    .registers 2

    .prologue
    .line 101
    invoke-super {p0}, Lcom/a/b/d/an;->hashCode()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic i()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 101
    .line 3712
    invoke-super {p0}, Lcom/a/b/d/an;->i()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 101
    return-object v0
.end method

.method public final bridge synthetic k()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 101
    .line 4759
    invoke-super {p0}, Lcom/a/b/d/an;->k()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 101
    return-object v0
.end method

.method final l()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 777
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "should never be called"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method final m()Ljava/util/Map;
    .registers 2

    .prologue
    .line 782
    new-instance v0, Lcom/a/b/d/wf;

    invoke-direct {v0, p0}, Lcom/a/b/d/wf;-><init>(Lcom/a/b/d/vi;)V

    return-object v0
.end method

.method public final n()Z
    .registers 2

    .prologue
    .line 566
    iget-object v0, p0, Lcom/a/b/d/oj;->a:Lcom/a/b/d/or;

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method final synthetic o()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 101
    .line 3764
    new-instance v0, Lcom/a/b/d/oo;

    invoke-direct {v0, p0}, Lcom/a/b/d/oo;-><init>(Lcom/a/b/d/oj;)V

    .line 101
    return-object v0
.end method

.method public final bridge synthetic p()Ljava/util/Set;
    .registers 2

    .prologue
    .line 101
    invoke-super {p0}, Lcom/a/b/d/an;->p()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic q()Lcom/a/b/d/xc;
    .registers 2

    .prologue
    .line 101
    invoke-super {p0}, Lcom/a/b/d/an;->q()Lcom/a/b/d/xc;

    move-result-object v0

    return-object v0
.end method

.method final synthetic s()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 101
    .line 2717
    new-instance v0, Lcom/a/b/d/om;

    invoke-direct {v0, p0}, Lcom/a/b/d/om;-><init>(Lcom/a/b/d/oj;)V

    .line 101
    return-object v0
.end method

.method public final bridge synthetic toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 101
    invoke-super {p0}, Lcom/a/b/d/an;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
