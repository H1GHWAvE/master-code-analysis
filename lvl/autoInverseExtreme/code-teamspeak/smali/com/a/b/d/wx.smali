.class final Lcom/a/b/d/wx;
.super Lcom/a/b/d/wy;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/ou;


# static fields
.field private static final g:J


# direct methods
.method constructor <init>(Lcom/a/b/d/ou;)V
    .registers 2

    .prologue
    .line 594
    invoke-direct {p0, p1}, Lcom/a/b/d/wy;-><init>(Lcom/a/b/d/vi;)V

    .line 595
    return-void
.end method

.method private a()Lcom/a/b/d/ou;
    .registers 2

    .prologue
    .line 597
    invoke-super {p0}, Lcom/a/b/d/wy;->c()Lcom/a/b/d/vi;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/ou;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/util/List;
    .registers 3

    .prologue
    .line 600
    .line 1597
    invoke-super {p0}, Lcom/a/b/d/wy;->c()Lcom/a/b/d/vi;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/ou;

    .line 600
    invoke-interface {v0, p1}, Lcom/a/b/d/ou;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/List;
    .registers 4

    .prologue
    .line 607
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 4

    .prologue
    .line 591
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/wx;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)Ljava/util/List;
    .registers 3

    .prologue
    .line 603
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final bridge synthetic c()Lcom/a/b/d/vi;
    .registers 2

    .prologue
    .line 591
    .line 2597
    invoke-super {p0}, Lcom/a/b/d/wy;->c()Lcom/a/b/d/vi;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/ou;

    .line 591
    return-object v0
.end method

.method public final synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 591
    invoke-virtual {p0, p1}, Lcom/a/b/d/wx;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 591
    invoke-virtual {p0, p1}, Lcom/a/b/d/wx;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 591
    .line 3597
    invoke-super {p0}, Lcom/a/b/d/wy;->c()Lcom/a/b/d/vi;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/ou;

    .line 591
    return-object v0
.end method
