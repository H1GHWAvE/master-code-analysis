.class public Lcom/a/b/d/lb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/yq;


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/c;
    a = "NavigableMap"
.end annotation


# static fields
.field static final a:Lcom/a/b/d/lb;


# instance fields
.field private final b:Lcom/a/b/d/jl;

.field private final c:Lcom/a/b/d/jl;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 44
    new-instance v0, Lcom/a/b/d/lb;

    invoke-static {}, Lcom/a/b/d/jl;->d()Lcom/a/b/d/jl;

    move-result-object v1

    invoke-static {}, Lcom/a/b/d/jl;->d()Lcom/a/b/d/jl;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/lb;-><init>(Lcom/a/b/d/jl;Lcom/a/b/d/jl;)V

    sput-object v0, Lcom/a/b/d/lb;->a:Lcom/a/b/d/lb;

    return-void
.end method

.method constructor <init>(Lcom/a/b/d/jl;Lcom/a/b/d/jl;)V
    .registers 3

    .prologue
    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    iput-object p1, p0, Lcom/a/b/d/lb;->b:Lcom/a/b/d/jl;

    .line 159
    iput-object p2, p0, Lcom/a/b/d/lb;->c:Lcom/a/b/d/jl;

    .line 160
    return-void
.end method

.method static synthetic a(Lcom/a/b/d/lb;)Lcom/a/b/d/jl;
    .registers 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/a/b/d/lb;->b:Lcom/a/b/d/jl;

    return-object v0
.end method

.method private static b(Lcom/a/b/d/yl;Ljava/lang/Object;)Lcom/a/b/d/lb;
    .registers 5

    .prologue
    .line 61
    new-instance v0, Lcom/a/b/d/lb;

    invoke-static {p0}, Lcom/a/b/d/jl;->a(Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v1

    invoke-static {p1}, Lcom/a/b/d/jl;->a(Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/lb;-><init>(Lcom/a/b/d/jl;Lcom/a/b/d/jl;)V

    return-object v0
.end method

.method private static b(Lcom/a/b/d/yq;)Lcom/a/b/d/lb;
    .registers 6

    .prologue
    .line 67
    instance-of v0, p0, Lcom/a/b/d/lb;

    if-eqz v0, :cond_7

    .line 68
    check-cast p0, Lcom/a/b/d/lb;

    .line 77
    :goto_6
    return-object p0

    .line 70
    :cond_7
    invoke-interface {p0}, Lcom/a/b/d/yq;->d()Ljava/util/Map;

    move-result-object v0

    .line 71
    new-instance v1, Lcom/a/b/d/jn;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {v1, v2}, Lcom/a/b/d/jn;-><init>(I)V

    .line 72
    new-instance v2, Lcom/a/b/d/jn;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v3

    invoke-direct {v2, v3}, Lcom/a/b/d/jn;-><init>(I)V

    .line 73
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_25
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_40

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 74
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/a/b/d/jn;->c(Ljava/lang/Object;)Lcom/a/b/d/jn;

    .line 75
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/a/b/d/jn;->c(Ljava/lang/Object;)Lcom/a/b/d/jn;

    goto :goto_25

    .line 77
    :cond_40
    new-instance p0, Lcom/a/b/d/lb;

    invoke-virtual {v1}, Lcom/a/b/d/jn;->b()Lcom/a/b/d/jl;

    move-result-object v0

    invoke-virtual {v2}, Lcom/a/b/d/jn;->b()Lcom/a/b/d/jl;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/a/b/d/lb;-><init>(Lcom/a/b/d/jl;Lcom/a/b/d/jl;)V

    goto :goto_6
.end method

.method private static e()Lcom/a/b/d/lb;
    .registers 1

    .prologue
    .line 53
    sget-object v0, Lcom/a/b/d/lb;->a:Lcom/a/b/d/lb;

    return-object v0
.end method

.method private static f()Lcom/a/b/d/le;
    .registers 1

    .prologue
    .line 84
    new-instance v0, Lcom/a/b/d/le;

    invoke-direct {v0}, Lcom/a/b/d/le;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/a/b/d/yl;
    .registers 4

    .prologue
    .line 190
    iget-object v0, p0, Lcom/a/b/d/lb;->b:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 191
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 193
    :cond_e
    iget-object v0, p0, Lcom/a/b/d/lb;->b:Lcom/a/b/d/jl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    .line 194
    iget-object v1, p0, Lcom/a/b/d/lb;->b:Lcom/a/b/d/jl;

    iget-object v2, p0, Lcom/a/b/d/lb;->b:Lcom/a/b/d/jl;

    invoke-virtual {v2}, Lcom/a/b/d/jl;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/b/d/yl;

    .line 195
    iget-object v0, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    iget-object v1, v1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-static {v0, v1}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Comparable;)Ljava/lang/Object;
    .registers 8
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 165
    iget-object v0, p0, Lcom/a/b/d/lb;->b:Lcom/a/b/d/jl;

    invoke-static {}, Lcom/a/b/d/yl;->a()Lcom/a/b/b/bj;

    move-result-object v2

    invoke-static {p1}, Lcom/a/b/d/dw;->b(Ljava/lang/Comparable;)Lcom/a/b/d/dw;

    move-result-object v3

    sget-object v4, Lcom/a/b/d/abg;->a:Lcom/a/b/d/abg;

    sget-object v5, Lcom/a/b/d/abc;->a:Lcom/a/b/d/abc;

    invoke-static {v0, v2, v3, v4, v5}, Lcom/a/b/d/aba;->a(Ljava/util/List;Lcom/a/b/b/bj;Ljava/lang/Comparable;Lcom/a/b/d/abg;Lcom/a/b/d/abc;)I

    move-result v2

    .line 167
    const/4 v0, -0x1

    if-ne v2, v0, :cond_18

    move-object v0, v1

    .line 171
    :goto_17
    return-object v0

    .line 170
    :cond_18
    iget-object v0, p0, Lcom/a/b/d/lb;->b:Lcom/a/b/d/jl;

    invoke-virtual {v0, v2}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    .line 171
    invoke-virtual {v0, p1}, Lcom/a/b/d/yl;->c(Ljava/lang/Comparable;)Z

    move-result v0

    if-eqz v0, :cond_2d

    iget-object v0, p0, Lcom/a/b/d/lb;->c:Lcom/a/b/d/jl;

    invoke-virtual {v0, v2}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_17

    :cond_2d
    move-object v0, v1

    goto :goto_17
.end method

.method public final a(Lcom/a/b/d/yl;)V
    .registers 3

    .prologue
    .line 215
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Lcom/a/b/d/yl;Ljava/lang/Object;)V
    .registers 4

    .prologue
    .line 200
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Lcom/a/b/d/yq;)V
    .registers 3

    .prologue
    .line 205
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b(Lcom/a/b/d/yl;)Lcom/a/b/d/lb;
    .registers 8

    .prologue
    .line 230
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    invoke-virtual {v0}, Lcom/a/b/d/yl;->f()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1053
    sget-object v0, Lcom/a/b/d/lb;->a:Lcom/a/b/d/lb;

    .line 268
    :goto_e
    return-object v0

    .line 232
    :cond_f
    iget-object v0, p0, Lcom/a/b/d/lb;->b:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_21

    invoke-virtual {p0}, Lcom/a/b/d/lb;->a()Lcom/a/b/d/yl;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/yl;)Z

    move-result v0

    if-eqz v0, :cond_23

    :cond_21
    move-object v0, p0

    .line 233
    goto :goto_e

    .line 235
    :cond_23
    iget-object v0, p0, Lcom/a/b/d/lb;->b:Lcom/a/b/d/jl;

    invoke-static {}, Lcom/a/b/d/yl;->b()Lcom/a/b/b/bj;

    move-result-object v1

    iget-object v2, p1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    sget-object v3, Lcom/a/b/d/abg;->d:Lcom/a/b/d/abg;

    sget-object v4, Lcom/a/b/d/abc;->b:Lcom/a/b/d/abc;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/a/b/d/aba;->a(Ljava/util/List;Lcom/a/b/b/bj;Ljava/lang/Comparable;Lcom/a/b/d/abg;Lcom/a/b/d/abc;)I

    move-result v1

    .line 238
    iget-object v0, p0, Lcom/a/b/d/lb;->b:Lcom/a/b/d/jl;

    invoke-static {}, Lcom/a/b/d/yl;->a()Lcom/a/b/b/bj;

    move-result-object v2

    iget-object v3, p1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    sget-object v4, Lcom/a/b/d/abg;->a:Lcom/a/b/d/abg;

    sget-object v5, Lcom/a/b/d/abc;->b:Lcom/a/b/d/abc;

    invoke-static {v0, v2, v3, v4, v5}, Lcom/a/b/d/aba;->a(Ljava/util/List;Lcom/a/b/b/bj;Ljava/lang/Comparable;Lcom/a/b/d/abg;Lcom/a/b/d/abc;)I

    move-result v3

    .line 241
    if-lt v1, v3, :cond_48

    .line 2053
    sget-object v0, Lcom/a/b/d/lb;->a:Lcom/a/b/d/lb;

    goto :goto_e

    .line 245
    :cond_48
    sub-int v0, v3, v1

    .line 246
    new-instance v2, Lcom/a/b/d/lc;

    invoke-direct {v2, p0, v0, v1, p1}, Lcom/a/b/d/lc;-><init>(Lcom/a/b/d/lb;IILcom/a/b/d/yl;)V

    .line 268
    new-instance v0, Lcom/a/b/d/ld;

    iget-object v4, p0, Lcom/a/b/d/lb;->c:Lcom/a/b/d/jl;

    invoke-virtual {v4, v1, v3}, Lcom/a/b/d/jl;->a(II)Lcom/a/b/d/jl;

    move-result-object v3

    move-object v1, p0

    move-object v4, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/a/b/d/ld;-><init>(Lcom/a/b/d/lb;Lcom/a/b/d/jl;Lcom/a/b/d/jl;Lcom/a/b/d/yl;Lcom/a/b/d/lb;)V

    goto :goto_e
.end method

.method public final b(Ljava/lang/Comparable;)Ljava/util/Map$Entry;
    .registers 8
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 178
    iget-object v0, p0, Lcom/a/b/d/lb;->b:Lcom/a/b/d/jl;

    invoke-static {}, Lcom/a/b/d/yl;->a()Lcom/a/b/b/bj;

    move-result-object v2

    invoke-static {p1}, Lcom/a/b/d/dw;->b(Ljava/lang/Comparable;)Lcom/a/b/d/dw;

    move-result-object v3

    sget-object v4, Lcom/a/b/d/abg;->a:Lcom/a/b/d/abg;

    sget-object v5, Lcom/a/b/d/abc;->a:Lcom/a/b/d/abc;

    invoke-static {v0, v2, v3, v4, v5}, Lcom/a/b/d/aba;->a(Ljava/util/List;Lcom/a/b/b/bj;Ljava/lang/Comparable;Lcom/a/b/d/abg;Lcom/a/b/d/abc;)I

    move-result v2

    .line 180
    const/4 v0, -0x1

    if-ne v2, v0, :cond_18

    move-object v0, v1

    .line 184
    :goto_17
    return-object v0

    .line 183
    :cond_18
    iget-object v0, p0, Lcom/a/b/d/lb;->b:Lcom/a/b/d/jl;

    invoke-virtual {v0, v2}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    .line 184
    invoke-virtual {v0, p1}, Lcom/a/b/d/yl;->c(Ljava/lang/Comparable;)Z

    move-result v3

    if-eqz v3, :cond_31

    iget-object v1, p0, Lcom/a/b/d/lb;->c:Lcom/a/b/d/jl;

    invoke-virtual {v1, v2}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    goto :goto_17

    :cond_31
    move-object v0, v1

    goto :goto_17
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 210
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final c()Lcom/a/b/d/jt;
    .registers 4

    .prologue
    .line 220
    iget-object v0, p0, Lcom/a/b/d/lb;->b:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 221
    invoke-static {}, Lcom/a/b/d/jt;->k()Lcom/a/b/d/jt;

    move-result-object v0

    .line 225
    :goto_c
    return-object v0

    .line 223
    :cond_d
    new-instance v1, Lcom/a/b/d/zq;

    iget-object v0, p0, Lcom/a/b/d/lb;->b:Lcom/a/b/d/jl;

    sget-object v2, Lcom/a/b/d/yl;->a:Lcom/a/b/d/yd;

    invoke-direct {v1, v0, v2}, Lcom/a/b/d/zq;-><init>(Lcom/a/b/d/jl;Ljava/util/Comparator;)V

    .line 225
    new-instance v0, Lcom/a/b/d/zl;

    iget-object v2, p0, Lcom/a/b/d/lb;->c:Lcom/a/b/d/jl;

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/zl;-><init>(Lcom/a/b/d/zq;Lcom/a/b/d/jl;)V

    goto :goto_c
.end method

.method public synthetic c(Lcom/a/b/d/yl;)Lcom/a/b/d/yq;
    .registers 3

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/a/b/d/lb;->b(Lcom/a/b/d/yl;)Lcom/a/b/d/lb;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d()Ljava/util/Map;
    .registers 2

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/a/b/d/lb;->c()Lcom/a/b/d/jt;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 288
    instance-of v0, p1, Lcom/a/b/d/yq;

    if-eqz v0, :cond_13

    .line 289
    check-cast p1, Lcom/a/b/d/yq;

    .line 290
    invoke-virtual {p0}, Lcom/a/b/d/lb;->c()Lcom/a/b/d/jt;

    move-result-object v0

    invoke-interface {p1}, Lcom/a/b/d/yq;->d()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/d/jt;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 292
    :goto_12
    return v0

    :cond_13
    const/4 v0, 0x0

    goto :goto_12
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 283
    invoke-virtual {p0}, Lcom/a/b/d/lb;->c()Lcom/a/b/d/jt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/jt;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 297
    invoke-virtual {p0}, Lcom/a/b/d/lb;->c()Lcom/a/b/d/jt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/jt;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
