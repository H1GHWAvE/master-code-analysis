.class final Lcom/a/b/d/aaf;
.super Lcom/a/b/d/aaq;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/util/Set;

.field final synthetic b:Lcom/a/b/b/co;

.field final synthetic c:Ljava/util/Set;


# direct methods
.method constructor <init>(Ljava/util/Set;Lcom/a/b/b/co;Ljava/util/Set;)V
    .registers 5

    .prologue
    .line 644
    iput-object p1, p0, Lcom/a/b/d/aaf;->a:Ljava/util/Set;

    iput-object p2, p0, Lcom/a/b/d/aaf;->b:Lcom/a/b/b/co;

    iput-object p3, p0, Lcom/a/b/d/aaf;->c:Ljava/util/Set;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/a/b/d/aaq;-><init>(B)V

    return-void
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 655
    iget-object v0, p0, Lcom/a/b/d/aaf;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/a/b/d/aaf;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public final containsAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 658
    iget-object v0, p0, Lcom/a/b/d/aaf;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/a/b/d/aaf;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 652
    invoke-virtual {p0}, Lcom/a/b/d/aaf;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 646
    iget-object v0, p0, Lcom/a/b/d/aaf;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/aaf;->b:Lcom/a/b/b/co;

    invoke-static {v0, v1}, Lcom/a/b/d/nj;->b(Ljava/util/Iterator;Lcom/a/b/b/co;)Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 649
    invoke-virtual {p0}, Lcom/a/b/d/aaf;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->b(Ljava/util/Iterator;)I

    move-result v0

    return v0
.end method
