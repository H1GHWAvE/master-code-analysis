.class public abstract Lcom/a/b/d/hr;
.super Lcom/a/b/d/hg;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/adv;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# direct methods
.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/a/b/d/hg;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/a/b/d/hr;->f()Lcom/a/b/d/adv;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/a/b/d/adv;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/a/b/d/hr;->f()Lcom/a/b/d/adv;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/adv;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/a/b/d/adv;)V
    .registers 3

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/a/b/d/hr;->f()Lcom/a/b/d/adv;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/adv;->a(Lcom/a/b/d/adv;)V

    .line 105
    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/a/b/d/hr;->f()Lcom/a/b/d/adv;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/adv;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/a/b/d/hr;->f()Lcom/a/b/d/adv;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/d/adv;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/a/b/d/hr;->f()Lcom/a/b/d/adv;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/d/adv;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/util/Set;
    .registers 2

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/a/b/d/hr;->f()Lcom/a/b/d/adv;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/adv;->b()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/a/b/d/hr;->f()Lcom/a/b/d/adv;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/adv;->b(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/a/b/d/hr;->f()Lcom/a/b/d/adv;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/d/adv;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final c()Z
    .registers 2

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/a/b/d/hr;->f()Lcom/a/b/d/adv;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/adv;->c()Z

    move-result v0

    return v0
.end method

.method public final c(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/a/b/d/hr;->f()Lcom/a/b/d/adv;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/adv;->c(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public d(Ljava/lang/Object;)Ljava/util/Map;
    .registers 3

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/a/b/d/hr;->f()Lcom/a/b/d/adv;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/adv;->d(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .registers 2

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/a/b/d/hr;->f()Lcom/a/b/d/adv;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/adv;->d()V

    .line 50
    return-void
.end method

.method public e(Ljava/lang/Object;)Ljava/util/Map;
    .registers 3

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/a/b/d/hr;->f()Lcom/a/b/d/adv;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/adv;->e(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/util/Set;
    .registers 2

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/a/b/d/hr;->f()Lcom/a/b/d/adv;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/adv;->e()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 138
    if-eq p1, p0, :cond_c

    invoke-virtual {p0}, Lcom/a/b/d/hr;->f()Lcom/a/b/d/adv;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/adv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method protected abstract f()Lcom/a/b/d/adv;
.end method

.method public h()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 134
    invoke-virtual {p0}, Lcom/a/b/d/hr;->f()Lcom/a/b/d/adv;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/adv;->h()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/a/b/d/hr;->f()Lcom/a/b/d/adv;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/adv;->hashCode()I

    move-result v0

    return v0
.end method

.method public final k()I
    .registers 2

    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/a/b/d/hr;->f()Lcom/a/b/d/adv;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/adv;->k()I

    move-result v0

    return v0
.end method

.method protected synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/a/b/d/hr;->f()Lcom/a/b/d/adv;

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/util/Map;
    .registers 2

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/a/b/d/hr;->f()Lcom/a/b/d/adv;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/adv;->l()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public m()Ljava/util/Map;
    .registers 2

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/a/b/d/hr;->f()Lcom/a/b/d/adv;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/adv;->m()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
