.class final Lcom/a/b/d/uo;
.super Lcom/a/b/d/tm;
.source "SourceFile"

# interfaces
.implements Ljava/util/SortedMap;


# direct methods
.method constructor <init>(Ljava/util/SortedSet;Lcom/a/b/b/bj;)V
    .registers 3

    .prologue
    .line 845
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/tm;-><init>(Ljava/util/Set;Lcom/a/b/b/bj;)V

    .line 846
    return-void
.end method

.method private d()Ljava/util/SortedSet;
    .registers 2

    .prologue
    .line 850
    invoke-super {p0}, Lcom/a/b/d/tm;->c()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    return-object v0
.end method


# virtual methods
.method final bridge synthetic c()Ljava/util/Set;
    .registers 2

    .prologue
    .line 841
    .line 8850
    invoke-super {p0}, Lcom/a/b/d/tm;->c()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    .line 841
    return-object v0
.end method

.method public final comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 855
    .line 1850
    invoke-super {p0}, Lcom/a/b/d/tm;->c()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    .line 855
    invoke-interface {v0}, Ljava/util/SortedSet;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public final firstKey()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 880
    .line 6850
    invoke-super {p0}, Lcom/a/b/d/tm;->c()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    .line 880
    invoke-interface {v0}, Ljava/util/SortedSet;->first()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 4

    .prologue
    .line 870
    .line 4850
    invoke-super {p0}, Lcom/a/b/d/tm;->c()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    .line 870
    invoke-interface {v0, p1}, Ljava/util/SortedSet;->headSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/uo;->a:Lcom/a/b/b/bj;

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/util/SortedSet;Lcom/a/b/b/bj;)Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method

.method public final keySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 860
    .line 2850
    invoke-super {p0}, Lcom/a/b/d/tm;->c()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    .line 860
    invoke-static {v0}, Lcom/a/b/d/sz;->a(Ljava/util/SortedSet;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final lastKey()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 885
    .line 7850
    invoke-super {p0}, Lcom/a/b/d/tm;->c()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    .line 885
    invoke-interface {v0}, Ljava/util/SortedSet;->last()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 5

    .prologue
    .line 865
    .line 3850
    invoke-super {p0}, Lcom/a/b/d/tm;->c()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    .line 865
    invoke-interface {v0, p1, p2}, Ljava/util/SortedSet;->subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/uo;->a:Lcom/a/b/b/bj;

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/util/SortedSet;Lcom/a/b/b/bj;)Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method

.method public final tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 4

    .prologue
    .line 875
    .line 5850
    invoke-super {p0}, Lcom/a/b/d/tm;->c()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    .line 875
    invoke-interface {v0, p1}, Ljava/util/SortedSet;->tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/uo;->a:Lcom/a/b/b/bj;

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/util/SortedSet;Lcom/a/b/b/bj;)Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method
