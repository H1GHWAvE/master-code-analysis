.class final Lcom/a/b/d/ys;
.super Lcom/a/b/d/du;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# static fields
.field private static final g:J


# instance fields
.field private final f:Lcom/a/b/d/yl;


# direct methods
.method constructor <init>(Lcom/a/b/d/yl;Lcom/a/b/d/ep;)V
    .registers 3

    .prologue
    .line 40
    invoke-direct {p0, p2}, Lcom/a/b/d/du;-><init>(Lcom/a/b/d/ep;)V

    .line 41
    iput-object p1, p0, Lcom/a/b/d/ys;->f:Lcom/a/b/d/yl;

    .line 42
    return-void
.end method

.method private a(Lcom/a/b/d/yl;)Lcom/a/b/d/du;
    .registers 4

    .prologue
    .line 45
    iget-object v0, p0, Lcom/a/b/d/ys;->f:Lcom/a/b/d/yl;

    invoke-virtual {v0, p1}, Lcom/a/b/d/yl;->b(Lcom/a/b/d/yl;)Z

    move-result v0

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/a/b/d/ys;->f:Lcom/a/b/d/yl;

    invoke-virtual {v0, p1}, Lcom/a/b/d/yl;->c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/ys;->a:Lcom/a/b/d/ep;

    invoke-static {v0, v1}, Lcom/a/b/d/du;->a(Lcom/a/b/d/yl;Lcom/a/b/d/ep;)Lcom/a/b/d/du;

    move-result-object v0

    :goto_14
    return-object v0

    :cond_15
    new-instance v0, Lcom/a/b/d/et;

    iget-object v1, p0, Lcom/a/b/d/ys;->a:Lcom/a/b/d/ep;

    invoke-direct {v0, v1}, Lcom/a/b/d/et;-><init>(Lcom/a/b/d/ep;)V

    goto :goto_14
.end method

.method static synthetic a(Ljava/lang/Comparable;Ljava/lang/Comparable;)Z
    .registers 3

    .prologue
    .line 2098
    if-eqz p1, :cond_a

    invoke-static {p0, p1}, Lcom/a/b/d/yl;->b(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    .line 36
    goto :goto_9
.end method

.method private static b(Ljava/lang/Comparable;Ljava/lang/Comparable;)Z
    .registers 3
    .param p1    # Ljava/lang/Comparable;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 98
    if-eqz p1, :cond_a

    invoke-static {p0, p1}, Lcom/a/b/d/yl;->b(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method


# virtual methods
.method public final a(Lcom/a/b/d/du;)Lcom/a/b/d/du;
    .registers 6

    .prologue
    .line 138
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    iget-object v0, p0, Lcom/a/b/d/ys;->a:Lcom/a/b/d/ep;

    iget-object v1, p1, Lcom/a/b/d/du;->a:Lcom/a/b/d/ep;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 140
    invoke-virtual {p1}, Lcom/a/b/d/du;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 145
    :goto_14
    return-object p1

    .line 143
    :cond_15
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    invoke-virtual {p0}, Lcom/a/b/d/ys;->k()Ljava/lang/Comparable;

    move-result-object v1

    invoke-virtual {p1}, Lcom/a/b/d/du;->first()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/a/b/d/yd;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    .line 144
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v1

    invoke-virtual {p0}, Lcom/a/b/d/ys;->m()Ljava/lang/Comparable;

    move-result-object v2

    invoke-virtual {p1}, Lcom/a/b/d/du;->last()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/a/b/d/yd;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Comparable;

    .line 145
    invoke-interface {v0, v1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v2

    if-gez v2, :cond_4a

    invoke-static {v0, v1}, Lcom/a/b/d/yl;->a(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/a/b/d/yl;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/ys;->a:Lcom/a/b/d/ep;

    invoke-static {v0, v1}, Lcom/a/b/d/du;->a(Lcom/a/b/d/yl;Lcom/a/b/d/ep;)Lcom/a/b/d/du;

    move-result-object p1

    goto :goto_14

    :cond_4a
    new-instance p1, Lcom/a/b/d/et;

    iget-object v0, p0, Lcom/a/b/d/ys;->a:Lcom/a/b/d/ep;

    invoke-direct {p1, v0}, Lcom/a/b/d/et;-><init>(Lcom/a/b/d/ep;)V

    goto :goto_14
.end method

.method final a(Ljava/lang/Comparable;Z)Lcom/a/b/d/du;
    .registers 4

    .prologue
    .line 51
    invoke-static {p2}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/a/b/d/yl;->a(Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/ys;->a(Lcom/a/b/d/yl;)Lcom/a/b/d/du;

    move-result-object v0

    return-object v0
.end method

.method final a(Ljava/lang/Comparable;ZLjava/lang/Comparable;Z)Lcom/a/b/d/du;
    .registers 7

    .prologue
    .line 56
    invoke-interface {p1, p3}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    if-nez v0, :cond_12

    if-nez p2, :cond_12

    if-nez p4, :cond_12

    .line 58
    new-instance v0, Lcom/a/b/d/et;

    iget-object v1, p0, Lcom/a/b/d/ys;->a:Lcom/a/b/d/ep;

    invoke-direct {v0, v1}, Lcom/a/b/d/et;-><init>(Lcom/a/b/d/ep;)V

    .line 60
    :goto_11
    return-object v0

    :cond_12
    invoke-static {p2}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v0

    invoke-static {p4}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v1

    invoke-static {p1, v0, p3, v1}, Lcom/a/b/d/yl;->a(Ljava/lang/Comparable;Lcom/a/b/d/ce;Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/ys;->a(Lcom/a/b/d/yl;)Lcom/a/b/d/du;

    move-result-object v0

    goto :goto_11
.end method

.method final synthetic a(Ljava/lang/Object;Z)Lcom/a/b/d/me;
    .registers 4

    .prologue
    .line 34
    check-cast p1, Ljava/lang/Comparable;

    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/ys;->b(Ljava/lang/Comparable;Z)Lcom/a/b/d/du;

    move-result-object v0

    return-object v0
.end method

.method final bridge synthetic a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/a/b/d/me;
    .registers 6

    .prologue
    .line 34
    check-cast p1, Ljava/lang/Comparable;

    check-cast p3, Ljava/lang/Comparable;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/a/b/d/ys;->a(Ljava/lang/Comparable;ZLjava/lang/Comparable;Z)Lcom/a/b/d/du;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/a/b/d/ce;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;
    .registers 6

    .prologue
    .line 156
    iget-object v0, p0, Lcom/a/b/d/ys;->f:Lcom/a/b/d/yl;

    iget-object v0, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    iget-object v1, p0, Lcom/a/b/d/ys;->a:Lcom/a/b/d/ep;

    invoke-virtual {v0, p1, v1}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/ce;Lcom/a/b/d/ep;)Lcom/a/b/d/dw;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/ys;->f:Lcom/a/b/d/yl;

    iget-object v1, v1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    iget-object v2, p0, Lcom/a/b/d/ys;->a:Lcom/a/b/d/ep;

    invoke-virtual {v1, p2, v2}, Lcom/a/b/d/dw;->b(Lcom/a/b/d/ce;Lcom/a/b/d/ep;)Lcom/a/b/d/dw;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v0

    return-object v0
.end method

.method final b(Ljava/lang/Comparable;Z)Lcom/a/b/d/du;
    .registers 4

    .prologue
    .line 66
    invoke-static {p2}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/a/b/d/yl;->b(Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/ys;->a(Lcom/a/b/d/yl;)Lcom/a/b/d/du;

    move-result-object v0

    return-object v0
.end method

.method final synthetic b(Ljava/lang/Object;Z)Lcom/a/b/d/me;
    .registers 4

    .prologue
    .line 34
    check-cast p1, Ljava/lang/Comparable;

    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/ys;->a(Ljava/lang/Comparable;Z)Lcom/a/b/d/du;

    move-result-object v0

    return-object v0
.end method

.method final c(Ljava/lang/Object;)I
    .registers 4
    .annotation build Lcom/a/b/a/c;
        a = "not used by GWT emulation"
    .end annotation

    .prologue
    .line 71
    invoke-virtual {p0, p1}, Lcom/a/b/d/ys;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/a/b/d/ys;->a:Lcom/a/b/d/ep;

    invoke-virtual {p0}, Lcom/a/b/d/ys;->k()Ljava/lang/Comparable;

    move-result-object v1

    check-cast p1, Ljava/lang/Comparable;

    invoke-virtual {v0, v1, p1}, Lcom/a/b/d/ep;->a(Ljava/lang/Comparable;Ljava/lang/Comparable;)J

    move-result-wide v0

    long-to-int v0, v0

    :goto_13
    return v0

    :cond_14
    const/4 v0, -0x1

    goto :goto_13
.end method

.method public final c()Lcom/a/b/d/agi;
    .registers 3

    .prologue
    .line 75
    new-instance v0, Lcom/a/b/d/yt;

    invoke-virtual {p0}, Lcom/a/b/d/ys;->k()Ljava/lang/Comparable;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/yt;-><init>(Lcom/a/b/d/ys;Ljava/lang/Comparable;)V

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 119
    if-nez p1, :cond_4

    .line 125
    :goto_3
    return v0

    .line 123
    :cond_4
    :try_start_4
    iget-object v1, p0, Lcom/a/b/d/ys;->f:Lcom/a/b/d/yl;

    check-cast p1, Ljava/lang/Comparable;

    invoke-virtual {v1, p1}, Lcom/a/b/d/yl;->c(Ljava/lang/Comparable;)Z
    :try_end_b
    .catch Ljava/lang/ClassCastException; {:try_start_4 .. :try_end_b} :catch_d

    move-result v0

    goto :goto_3

    .line 125
    :catch_d
    move-exception v1

    goto :goto_3
.end method

.method public final containsAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 130
    invoke-static {p0, p1}, Lcom/a/b/d/cm;->a(Ljava/util/Collection;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final d()Lcom/a/b/d/agi;
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 87
    new-instance v0, Lcom/a/b/d/yu;

    invoke-virtual {p0}, Lcom/a/b/d/ys;->m()Ljava/lang/Comparable;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/yu;-><init>(Lcom/a/b/d/ys;Ljava/lang/Comparable;)V

    return-object v0
.end method

.method public final synthetic descendingIterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/a/b/d/ys;->d()Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 6
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 161
    if-ne p1, p0, :cond_5

    move v0, v1

    .line 170
    :goto_4
    return v0

    .line 163
    :cond_5
    instance-of v0, p1, Lcom/a/b/d/ys;

    if-eqz v0, :cond_36

    move-object v0, p1

    .line 164
    check-cast v0, Lcom/a/b/d/ys;

    .line 165
    iget-object v2, p0, Lcom/a/b/d/ys;->a:Lcom/a/b/d/ep;

    iget-object v3, v0, Lcom/a/b/d/ys;->a:Lcom/a/b/d/ep;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_36

    .line 166
    invoke-virtual {p0}, Lcom/a/b/d/ys;->k()Ljava/lang/Comparable;

    move-result-object v2

    invoke-virtual {v0}, Lcom/a/b/d/ys;->k()Ljava/lang/Comparable;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_34

    invoke-virtual {p0}, Lcom/a/b/d/ys;->m()Ljava/lang/Comparable;

    move-result-object v2

    invoke-virtual {v0}, Lcom/a/b/d/ys;->m()Ljava/lang/Comparable;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_34

    move v0, v1

    goto :goto_4

    :cond_34
    const/4 v0, 0x0

    goto :goto_4

    .line 170
    :cond_36
    invoke-super {p0, p1}, Lcom/a/b/d/du;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_4
.end method

.method public final f_()Lcom/a/b/d/yl;
    .registers 5

    .prologue
    .line 152
    sget-object v0, Lcom/a/b/d/ce;->b:Lcom/a/b/d/ce;

    sget-object v1, Lcom/a/b/d/ce;->b:Lcom/a/b/d/ce;

    .line 1156
    iget-object v2, p0, Lcom/a/b/d/ys;->f:Lcom/a/b/d/yl;

    iget-object v2, v2, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    iget-object v3, p0, Lcom/a/b/d/ys;->a:Lcom/a/b/d/ep;

    invoke-virtual {v2, v0, v3}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/ce;Lcom/a/b/d/ep;)Lcom/a/b/d/dw;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/ys;->f:Lcom/a/b/d/yl;

    iget-object v2, v2, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    iget-object v3, p0, Lcom/a/b/d/ys;->a:Lcom/a/b/d/ep;

    invoke-virtual {v2, v1, v3}, Lcom/a/b/d/dw;->b(Lcom/a/b/d/ce;Lcom/a/b/d/ep;)Lcom/a/b/d/dw;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/yl;->a(Lcom/a/b/d/dw;Lcom/a/b/d/dw;)Lcom/a/b/d/yl;

    move-result-object v0

    .line 152
    return-object v0
.end method

.method public final synthetic first()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/a/b/d/ys;->k()Ljava/lang/Comparable;

    move-result-object v0

    return-object v0
.end method

.method final g()Ljava/lang/Object;
    .registers 5
    .annotation build Lcom/a/b/a/c;
        a = "serialization"
    .end annotation

    .prologue
    .line 195
    new-instance v0, Lcom/a/b/d/yv;

    iget-object v1, p0, Lcom/a/b/d/ys;->f:Lcom/a/b/d/yl;

    iget-object v2, p0, Lcom/a/b/d/ys;->a:Lcom/a/b/d/ep;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/a/b/d/yv;-><init>(Lcom/a/b/d/yl;Lcom/a/b/d/ep;B)V

    return-object v0
.end method

.method final h_()Z
    .registers 2

    .prologue
    .line 102
    const/4 v0, 0x0

    return v0
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 175
    invoke-static {p0}, Lcom/a/b/d/aad;->a(Ljava/util/Set;)I

    move-result v0

    return v0
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 134
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/a/b/d/ys;->c()Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/lang/Comparable;
    .registers 3

    .prologue
    .line 106
    iget-object v0, p0, Lcom/a/b/d/ys;->f:Lcom/a/b/d/yl;

    iget-object v0, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    iget-object v1, p0, Lcom/a/b/d/ys;->a:Lcom/a/b/d/ep;

    invoke-virtual {v0, v1}, Lcom/a/b/d/dw;->a(Lcom/a/b/d/ep;)Ljava/lang/Comparable;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic last()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/a/b/d/ys;->m()Ljava/lang/Comparable;

    move-result-object v0

    return-object v0
.end method

.method public final m()Ljava/lang/Comparable;
    .registers 3

    .prologue
    .line 110
    iget-object v0, p0, Lcom/a/b/d/ys;->f:Lcom/a/b/d/yl;

    iget-object v0, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    iget-object v1, p0, Lcom/a/b/d/ys;->a:Lcom/a/b/d/ep;

    invoke-virtual {v0, v1}, Lcom/a/b/d/dw;->b(Lcom/a/b/d/ep;)Ljava/lang/Comparable;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .registers 5

    .prologue
    .line 114
    iget-object v0, p0, Lcom/a/b/d/ys;->a:Lcom/a/b/d/ep;

    invoke-virtual {p0}, Lcom/a/b/d/ys;->k()Ljava/lang/Comparable;

    move-result-object v1

    invoke-virtual {p0}, Lcom/a/b/d/ys;->m()Ljava/lang/Comparable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/a/b/d/ep;->a(Ljava/lang/Comparable;Ljava/lang/Comparable;)J

    move-result-wide v0

    .line 115
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-ltz v2, :cond_19

    const v0, 0x7fffffff

    :goto_18
    return v0

    :cond_19
    long-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_18
.end method
