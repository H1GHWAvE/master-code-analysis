.class final Lcom/a/b/d/af;
.super Lcom/a/b/d/ah;
.source "SourceFile"

# interfaces
.implements Ljava/util/NavigableSet;


# annotations
.annotation build Lcom/a/b/a/c;
    a = "NavigableSet"
.end annotation


# instance fields
.field final synthetic a:Lcom/a/b/d/n;


# direct methods
.method constructor <init>(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/NavigableSet;Lcom/a/b/d/ab;)V
    .registers 5
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/a/b/d/ab;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 685
    iput-object p1, p0, Lcom/a/b/d/af;->a:Lcom/a/b/d/n;

    .line 686
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/a/b/d/ah;-><init>(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/SortedSet;Lcom/a/b/d/ab;)V

    .line 687
    return-void
.end method

.method private a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;
    .registers 6

    .prologue
    .line 725
    new-instance v0, Lcom/a/b/d/af;

    iget-object v1, p0, Lcom/a/b/d/af;->a:Lcom/a/b/d/n;

    iget-object v2, p0, Lcom/a/b/d/af;->b:Ljava/lang/Object;

    .line 5516
    iget-object v3, p0, Lcom/a/b/d/ab;->d:Lcom/a/b/d/ab;

    .line 725
    if-nez v3, :cond_e

    :goto_a
    invoke-direct {v0, v1, v2, p1, p0}, Lcom/a/b/d/af;-><init>(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/NavigableSet;Lcom/a/b/d/ab;)V

    return-object v0

    .line 6516
    :cond_e
    iget-object p0, p0, Lcom/a/b/d/ab;->d:Lcom/a/b/d/ab;

    goto :goto_a
.end method

.method private e()Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 691
    invoke-super {p0}, Lcom/a/b/d/ah;->d()Ljava/util/SortedSet;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    return-object v0
.end method


# virtual methods
.method public final ceiling(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 706
    .line 3691
    invoke-super {p0}, Lcom/a/b/d/ah;->d()Ljava/util/SortedSet;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    .line 706
    invoke-interface {v0, p1}, Ljava/util/NavigableSet;->ceiling(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method final bridge synthetic d()Ljava/util/SortedSet;
    .registers 2

    .prologue
    .line 682
    .line 11691
    invoke-super {p0}, Lcom/a/b/d/ah;->d()Ljava/util/SortedSet;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    .line 682
    return-object v0
.end method

.method public final descendingIterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 736
    new-instance v1, Lcom/a/b/d/ac;

    .line 7691
    invoke-super {p0}, Lcom/a/b/d/ah;->d()Ljava/util/SortedSet;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    .line 736
    invoke-interface {v0}, Ljava/util/NavigableSet;->descendingIterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/ac;-><init>(Lcom/a/b/d/ab;Ljava/util/Iterator;)V

    return-object v1
.end method

.method public final descendingSet()Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 731
    .line 6691
    invoke-super {p0}, Lcom/a/b/d/ah;->d()Ljava/util/SortedSet;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    .line 731
    invoke-interface {v0}, Ljava/util/NavigableSet;->descendingSet()Ljava/util/NavigableSet;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/af;->a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public final floor(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 701
    .line 2691
    invoke-super {p0}, Lcom/a/b/d/ah;->d()Ljava/util/SortedSet;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    .line 701
    invoke-interface {v0, p1}, Ljava/util/NavigableSet;->floor(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
    .registers 4

    .prologue
    .line 748
    .line 9691
    invoke-super {p0}, Lcom/a/b/d/ah;->d()Ljava/util/SortedSet;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    .line 748
    invoke-interface {v0, p1, p2}, Ljava/util/NavigableSet;->headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/af;->a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public final higher(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 711
    .line 4691
    invoke-super {p0}, Lcom/a/b/d/ah;->d()Ljava/util/SortedSet;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    .line 711
    invoke-interface {v0, p1}, Ljava/util/NavigableSet;->higher(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final lower(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 696
    .line 1691
    invoke-super {p0}, Lcom/a/b/d/ah;->d()Ljava/util/SortedSet;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    .line 696
    invoke-interface {v0, p1}, Ljava/util/NavigableSet;->lower(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final pollFirst()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 716
    invoke-virtual {p0}, Lcom/a/b/d/af;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->h(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final pollLast()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 721
    invoke-virtual {p0}, Lcom/a/b/d/af;->descendingIterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->h(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;
    .registers 6

    .prologue
    .line 742
    .line 8691
    invoke-super {p0}, Lcom/a/b/d/ah;->d()Ljava/util/SortedSet;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    .line 742
    invoke-interface {v0, p1, p2, p3, p4}, Ljava/util/NavigableSet;->subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/af;->a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public final tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
    .registers 4

    .prologue
    .line 753
    .line 10691
    invoke-super {p0}, Lcom/a/b/d/ah;->d()Ljava/util/SortedSet;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableSet;

    .line 753
    invoke-interface {v0, p1, p2}, Ljava/util/NavigableSet;->tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/af;->a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method
