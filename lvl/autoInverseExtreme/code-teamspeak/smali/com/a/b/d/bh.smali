.class final Lcom/a/b/d/bh;
.super Ljava/util/AbstractSet;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/bf;


# direct methods
.method constructor <init>(Lcom/a/b/d/bf;)V
    .registers 2

    .prologue
    .line 120
    iput-object p1, p0, Lcom/a/b/d/bh;->a:Lcom/a/b/d/bf;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method


# virtual methods
.method public final clear()V
    .registers 2

    .prologue
    .line 145
    iget-object v0, p0, Lcom/a/b/d/bh;->a:Lcom/a/b/d/bf;

    invoke-virtual {v0}, Lcom/a/b/d/bf;->d()V

    .line 146
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 123
    instance-of v0, p1, Lcom/a/b/d/adw;

    if-eqz v0, :cond_33

    .line 124
    check-cast p1, Lcom/a/b/d/adw;

    .line 125
    iget-object v0, p0, Lcom/a/b/d/bh;->a:Lcom/a/b/d/bf;

    invoke-virtual {v0}, Lcom/a/b/d/bf;->m()Ljava/util/Map;

    move-result-object v0

    invoke-interface {p1}, Lcom/a/b/d/adw;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 126
    if-eqz v0, :cond_31

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {p1}, Lcom/a/b/d/adw;->b()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1}, Lcom/a/b/d/adw;->c()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/a/b/d/sz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/a/b/d/cm;->a(Ljava/util/Collection;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_31

    const/4 v0, 0x1

    .line 129
    :goto_30
    return v0

    :cond_31
    move v0, v1

    .line 126
    goto :goto_30

    :cond_33
    move v0, v1

    .line 129
    goto :goto_30
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 150
    iget-object v0, p0, Lcom/a/b/d/bh;->a:Lcom/a/b/d/bf;

    invoke-virtual {v0}, Lcom/a/b/d/bf;->g()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .registers 6
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 134
    instance-of v0, p1, Lcom/a/b/d/adw;

    if-eqz v0, :cond_33

    .line 135
    check-cast p1, Lcom/a/b/d/adw;

    .line 136
    iget-object v0, p0, Lcom/a/b/d/bh;->a:Lcom/a/b/d/bf;

    invoke-virtual {v0}, Lcom/a/b/d/bf;->m()Ljava/util/Map;

    move-result-object v0

    invoke-interface {p1}, Lcom/a/b/d/adw;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 137
    if-eqz v0, :cond_31

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {p1}, Lcom/a/b/d/adw;->b()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1}, Lcom/a/b/d/adw;->c()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/a/b/d/sz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/a/b/d/cm;->b(Ljava/util/Collection;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_31

    const/4 v0, 0x1

    .line 140
    :goto_30
    return v0

    :cond_31
    move v0, v1

    .line 137
    goto :goto_30

    :cond_33
    move v0, v1

    .line 140
    goto :goto_30
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 155
    iget-object v0, p0, Lcom/a/b/d/bh;->a:Lcom/a/b/d/bf;

    invoke-virtual {v0}, Lcom/a/b/d/bf;->k()I

    move-result v0

    return v0
.end method
