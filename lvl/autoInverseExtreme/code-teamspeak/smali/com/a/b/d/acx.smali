.class final Lcom/a/b/d/acx;
.super Lcom/a/b/d/adq;
.source "SourceFile"


# static fields
.field private static final a:J


# direct methods
.method constructor <init>(Ljava/util/Set;Ljava/lang/Object;)V
    .registers 3
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 843
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/adq;-><init>(Ljava/util/Set;Ljava/lang/Object;)V

    .line 844
    return-void
.end method


# virtual methods
.method public final contains(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 881
    iget-object v1, p0, Lcom/a/b/d/acx;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 882
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/acx;->c()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/sz;->a(Ljava/util/Collection;Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 883
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public final containsAll(Ljava/util/Collection;)Z
    .registers 4

    .prologue
    .line 886
    iget-object v1, p0, Lcom/a/b/d/acx;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 887
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/acx;->c()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/cm;->a(Ljava/util/Collection;Ljava/util/Collection;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 888
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 891
    if-ne p1, p0, :cond_4

    .line 892
    const/4 v0, 0x1

    .line 895
    :goto_3
    return v0

    .line 894
    :cond_4
    iget-object v1, p0, Lcom/a/b/d/acx;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 895
    :try_start_7
    invoke-virtual {p0}, Lcom/a/b/d/acx;->c()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/aad;->a(Ljava/util/Set;Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    goto :goto_3

    .line 896
    :catchall_11
    move-exception v0

    monitor-exit v1
    :try_end_13
    .catchall {:try_start_7 .. :try_end_13} :catchall_11

    throw v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 848
    invoke-super {p0}, Lcom/a/b/d/adq;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 849
    new-instance v1, Lcom/a/b/d/acy;

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/acy;-><init>(Lcom/a/b/d/acx;Ljava/util/Iterator;)V

    return-object v1
.end method

.method public final remove(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 899
    iget-object v1, p0, Lcom/a/b/d/acx;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 900
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/acx;->c()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/sz;->b(Ljava/util/Collection;Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 901
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .registers 4

    .prologue
    .line 904
    iget-object v1, p0, Lcom/a/b/d/acx;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 905
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/acx;->c()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/nj;->a(Ljava/util/Iterator;Ljava/util/Collection;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 906
    :catchall_11
    move-exception v0

    monitor-exit v1
    :try_end_13
    .catchall {:try_start_3 .. :try_end_13} :catchall_11

    throw v0
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .registers 4

    .prologue
    .line 909
    iget-object v1, p0, Lcom/a/b/d/acx;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 910
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/acx;->c()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/nj;->b(Ljava/util/Iterator;Ljava/util/Collection;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 911
    :catchall_11
    move-exception v0

    monitor-exit v1
    :try_end_13
    .catchall {:try_start_3 .. :try_end_13} :catchall_11

    throw v0
.end method

.method public final toArray()[Ljava/lang/Object;
    .registers 3

    .prologue
    .line 871
    iget-object v1, p0, Lcom/a/b/d/acx;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 872
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/acx;->c()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/yc;->a(Ljava/util/Collection;)[Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 873
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 4

    .prologue
    .line 876
    iget-object v1, p0, Lcom/a/b/d/acx;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 877
    :try_start_3
    invoke-virtual {p0}, Lcom/a/b/d/acx;->c()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/yc;->a(Ljava/util/Collection;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 878
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0
.end method
