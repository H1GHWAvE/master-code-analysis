.class public final Lcom/a/b/d/ls;
.super Lcom/a/b/d/kn;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 189
    invoke-direct {p0}, Lcom/a/b/d/kn;-><init>()V

    .line 190
    new-instance v0, Lcom/a/b/d/lt;

    invoke-direct {v0}, Lcom/a/b/d/lt;-><init>()V

    iput-object v0, p0, Lcom/a/b/d/ls;->a:Lcom/a/b/d/vi;

    .line 191
    return-void
.end method

.method private b(Lcom/a/b/d/vi;)Lcom/a/b/d/ls;
    .registers 5

    .prologue
    .line 228
    invoke-interface {p1}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 229
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-direct {p0, v2, v0}, Lcom/a/b/d/ls;->b(Ljava/lang/Object;Ljava/lang/Iterable;)Lcom/a/b/d/ls;

    goto :goto_c

    .line 231
    :cond_26
    return-object p0
.end method

.method private b(Ljava/lang/Object;Ljava/lang/Iterable;)Lcom/a/b/d/ls;
    .registers 6

    .prologue
    .line 214
    iget-object v0, p0, Lcom/a/b/d/ls;->a:Lcom/a/b/d/vi;

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/a/b/d/vi;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    .line 215
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_20

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 216
    invoke-static {v2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_e

    .line 218
    :cond_20
    return-object p0
.end method

.method private varargs b(Ljava/lang/Object;[Ljava/lang/Object;)Lcom/a/b/d/ls;
    .registers 4

    .prologue
    .line 222
    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/a/b/d/ls;->b(Ljava/lang/Object;Ljava/lang/Iterable;)Lcom/a/b/d/ls;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/util/Map$Entry;)Lcom/a/b/d/ls;
    .registers 5

    .prologue
    .line 208
    iget-object v0, p0, Lcom/a/b/d/ls;->a:Lcom/a/b/d/vi;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/a/b/d/vi;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 210
    return-object p0
.end method

.method private c(Ljava/util/Comparator;)Lcom/a/b/d/ls;
    .registers 3

    .prologue
    .line 241
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    iput-object v0, p0, Lcom/a/b/d/ls;->b:Ljava/util/Comparator;

    .line 242
    return-object p0
.end method

.method private d(Ljava/util/Comparator;)Lcom/a/b/d/ls;
    .registers 2

    .prologue
    .line 259
    invoke-super {p0, p1}, Lcom/a/b/d/kn;->a(Ljava/util/Comparator;)Lcom/a/b/d/kn;

    .line 260
    return-object p0
.end method


# virtual methods
.method public final synthetic a(Lcom/a/b/d/vi;)Lcom/a/b/d/kn;
    .registers 5

    .prologue
    .line 183
    .line 4228
    invoke-interface {p1}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 4229
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-direct {p0, v2, v0}, Lcom/a/b/d/ls;->b(Ljava/lang/Object;Ljava/lang/Iterable;)Lcom/a/b/d/ls;

    goto :goto_c

    .line 183
    :cond_26
    return-object p0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Iterable;)Lcom/a/b/d/kn;
    .registers 4

    .prologue
    .line 183
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/ls;->b(Ljava/lang/Object;Ljava/lang/Iterable;)Lcom/a/b/d/ls;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;[Ljava/lang/Object;)Lcom/a/b/d/kn;
    .registers 4

    .prologue
    .line 183
    .line 5222
    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/a/b/d/ls;->b(Ljava/lang/Object;Ljava/lang/Iterable;)Lcom/a/b/d/ls;

    move-result-object v0

    .line 183
    return-object v0
.end method

.method public final bridge synthetic a(Ljava/util/Comparator;)Lcom/a/b/d/kn;
    .registers 2

    .prologue
    .line 183
    .line 2259
    invoke-super {p0, p1}, Lcom/a/b/d/kn;->a(Ljava/util/Comparator;)Lcom/a/b/d/kn;

    .line 183
    return-object p0
.end method

.method public final synthetic a(Ljava/util/Map$Entry;)Lcom/a/b/d/kn;
    .registers 5

    .prologue
    .line 183
    .line 6208
    iget-object v0, p0, Lcom/a/b/d/ls;->a:Lcom/a/b/d/vi;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/a/b/d/vi;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 183
    return-object p0
.end method

.method public final a()Lcom/a/b/d/lr;
    .registers 5

    .prologue
    .line 267
    iget-object v0, p0, Lcom/a/b/d/ls;->b:Ljava/util/Comparator;

    if-eqz v0, :cond_48

    .line 268
    new-instance v1, Lcom/a/b/d/lt;

    invoke-direct {v1}, Lcom/a/b/d/lt;-><init>()V

    .line 269
    iget-object v0, p0, Lcom/a/b/d/ls;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/ov;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 271
    iget-object v2, p0, Lcom/a/b/d/ls;->b:Ljava/util/Comparator;

    invoke-static {v2}, Lcom/a/b/d/yd;->a(Ljava/util/Comparator;)Lcom/a/b/d/yd;

    move-result-object v2

    .line 1373
    invoke-static {}, Lcom/a/b/d/sz;->a()Lcom/a/b/b/bj;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/a/b/d/yd;->a(Lcom/a/b/b/bj;)Lcom/a/b/d/yd;

    move-result-object v2

    .line 271
    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 274
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_46

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 275
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-interface {v1, v3, v0}, Lcom/a/b/d/vi;->c(Ljava/lang/Object;Ljava/lang/Iterable;)Z

    goto :goto_2c

    .line 277
    :cond_46
    iput-object v1, p0, Lcom/a/b/d/ls;->a:Lcom/a/b/d/vi;

    .line 279
    :cond_48
    iget-object v0, p0, Lcom/a/b/d/ls;->a:Lcom/a/b/d/vi;

    iget-object v1, p0, Lcom/a/b/d/ls;->c:Ljava/util/Comparator;

    invoke-static {v0, v1}, Lcom/a/b/d/lr;->a(Lcom/a/b/d/vi;Ljava/util/Comparator;)Lcom/a/b/d/lr;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ls;
    .registers 6

    .prologue
    .line 198
    iget-object v0, p0, Lcom/a/b/d/ls;->a:Lcom/a/b/d/vi;

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/a/b/d/vi;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 199
    return-object p0
.end method

.method public final synthetic b()Lcom/a/b/d/kk;
    .registers 2

    .prologue
    .line 183
    invoke-virtual {p0}, Lcom/a/b/d/ls;->a()Lcom/a/b/d/lr;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kn;
    .registers 4

    .prologue
    .line 183
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/ls;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ls;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/util/Comparator;)Lcom/a/b/d/kn;
    .registers 3

    .prologue
    .line 183
    .line 3241
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    iput-object v0, p0, Lcom/a/b/d/ls;->b:Ljava/util/Comparator;

    .line 183
    return-object p0
.end method
