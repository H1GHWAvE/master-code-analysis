.class public final Lcom/a/b/d/cm;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field static final a:Lcom/a/b/b/bv;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 323
    const-string v0, ", "

    invoke-static {v0}, Lcom/a/b/b/bv;->a(Ljava/lang/String;)Lcom/a/b/b/bv;

    move-result-object v0

    const-string v1, "null"

    invoke-virtual {v0, v1}, Lcom/a/b/b/bv;->b(Ljava/lang/String;)Lcom/a/b/b/bv;

    move-result-object v0

    sput-object v0, Lcom/a/b/d/cm;->a:Lcom/a/b/b/bv;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Ljava/util/Collection;)Ljava/lang/String;
    .registers 4

    .prologue
    .line 297
    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-static {v0}, Lcom/a/b/d/cm;->a(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 299
    sget-object v1, Lcom/a/b/d/cm;->a:Lcom/a/b/b/bv;

    new-instance v2, Lcom/a/b/d/cn;

    invoke-direct {v2, p0}, Lcom/a/b/d/cn;-><init>(Ljava/util/Collection;)V

    invoke-static {p0, v2}, Lcom/a/b/d/mq;->a(Ljava/lang/Iterable;Lcom/a/b/b/bj;)Ljava/lang/Iterable;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/a/b/b/bv;->a(Ljava/lang/StringBuilder;Ljava/lang/Iterable;)Ljava/lang/StringBuilder;

    .line 305
    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static a(I)Ljava/lang/StringBuilder;
    .registers 7

    .prologue
    .line 312
    const-string v0, "size"

    invoke-static {p0, v0}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 313
    new-instance v0, Ljava/lang/StringBuilder;

    int-to-long v2, p0

    const-wide/16 v4, 0x8

    mul-long/2addr v2, v4

    const-wide/32 v4, 0x40000000

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v1, v2

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    return-object v0
.end method

.method static a(Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 1

    .prologue
    .line 320
    check-cast p0, Ljava/util/Collection;

    return-object p0
.end method

.method private static a(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/Collection;
    .registers 3
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 406
    new-instance v0, Lcom/a/b/d/cp;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/cp;-><init>(Ljava/lang/Iterable;Ljava/util/Comparator;)V

    return-object v0
.end method

.method public static a(Ljava/util/Collection;Lcom/a/b/b/bj;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 247
    new-instance v0, Lcom/a/b/d/ct;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/ct;-><init>(Ljava/util/Collection;Lcom/a/b/b/bj;)V

    return-object v0
.end method

.method public static a(Ljava/util/Collection;Lcom/a/b/b/co;)Ljava/util/Collection;
    .registers 5

    .prologue
    .line 91
    instance-of v0, p0, Lcom/a/b/d/co;

    if-eqz v0, :cond_14

    .line 94
    check-cast p0, Lcom/a/b/d/co;

    .line 1145
    new-instance v0, Lcom/a/b/d/co;

    iget-object v1, p0, Lcom/a/b/d/co;->a:Ljava/util/Collection;

    iget-object v2, p0, Lcom/a/b/d/co;->b:Lcom/a/b/b/co;

    invoke-static {v2, p1}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/co;-><init>(Ljava/util/Collection;Lcom/a/b/b/co;)V

    .line 97
    :goto_13
    return-object v0

    :cond_14
    new-instance v2, Lcom/a/b/d/co;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/b/b/co;

    invoke-direct {v2, v0, v1}, Lcom/a/b/d/co;-><init>(Ljava/util/Collection;Lcom/a/b/b/co;)V

    move-object v0, v2

    goto :goto_13
.end method

.method static synthetic a(J)Z
    .registers 4

    .prologue
    .line 56
    .line 1672
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-ltz v0, :cond_f

    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p0, v0

    if-gtz v0, :cond_f

    const/4 v0, 0x1

    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    .line 56
    goto :goto_e
.end method

.method static a(Ljava/util/Collection;Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 108
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    :try_start_4
    invoke-interface {p0, p1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z
    :try_end_7
    .catch Ljava/lang/ClassCastException; {:try_start_4 .. :try_end_7} :catch_9
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_7} :catch_b

    move-result v0

    .line 114
    :goto_8
    return v0

    .line 112
    :catch_9
    move-exception v1

    goto :goto_8

    .line 114
    :catch_b
    move-exception v1

    goto :goto_8
.end method

.method static a(Ljava/util/Collection;Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 290
    invoke-static {p0}, Lcom/a/b/b/cp;->a(Ljava/util/Collection;)Lcom/a/b/b/co;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/a/b/d/mq;->e(Ljava/lang/Iterable;Lcom/a/b/b/co;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Ljava/util/List;Ljava/util/List;)Z
    .registers 4

    .prologue
    .line 56
    .line 2663
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_c

    .line 2664
    const/4 v0, 0x0

    :goto_b
    return v0

    .line 2666
    :cond_c
    invoke-static {p0}, Lcom/a/b/d/ip;->a(Ljava/lang/Iterable;)Lcom/a/b/d/ip;

    move-result-object v0

    .line 2667
    invoke-static {p1}, Lcom/a/b/d/ip;->a(Ljava/lang/Iterable;)Lcom/a/b/d/ip;

    move-result-object v1

    .line 2668
    invoke-interface {v0, v1}, Lcom/a/b/d/xc;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_b
.end method

.method private static b(Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 3
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 354
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    .line 1406
    new-instance v1, Lcom/a/b/d/cp;

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/cp;-><init>(Ljava/lang/Iterable;Ljava/util/Comparator;)V

    .line 354
    return-object v1
.end method

.method private static b(Ljava/util/Collection;)Ljava/util/Collection;
    .registers 3
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 559
    new-instance v0, Lcom/a/b/d/cr;

    invoke-static {p0}, Lcom/a/b/d/jl;->a(Ljava/util/Collection;)Lcom/a/b/d/jl;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/cr;-><init>(Lcom/a/b/d/jl;)V

    return-object v0
.end method

.method private static b(J)Z
    .registers 4

    .prologue
    .line 672
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-ltz v0, :cond_f

    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p0, v0

    if-gtz v0, :cond_f

    const/4 v0, 0x1

    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method static b(Ljava/util/Collection;Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 124
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    :try_start_4
    invoke-interface {p0, p1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z
    :try_end_7
    .catch Ljava/lang/ClassCastException; {:try_start_4 .. :try_end_7} :catch_9
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_7} :catch_b

    move-result v0

    .line 130
    :goto_8
    return v0

    .line 128
    :catch_9
    move-exception v1

    goto :goto_8

    .line 130
    :catch_b
    move-exception v1

    goto :goto_8
.end method

.method private static b(Ljava/util/List;Ljava/util/List;)Z
    .registers 4

    .prologue
    .line 663
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_c

    .line 664
    const/4 v0, 0x0

    .line 668
    :goto_b
    return v0

    .line 666
    :cond_c
    invoke-static {p0}, Lcom/a/b/d/ip;->a(Ljava/lang/Iterable;)Lcom/a/b/d/ip;

    move-result-object v0

    .line 667
    invoke-static {p1}, Lcom/a/b/d/ip;->a(Ljava/lang/Iterable;)Lcom/a/b/d/ip;

    move-result-object v1

    .line 668
    invoke-interface {v0, v1}, Lcom/a/b/d/xc;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_b
.end method
