.class abstract Lcom/a/b/d/ja;
.super Lcom/a/b/d/jb;
.source "SourceFile"


# instance fields
.field a:[Ljava/lang/Object;

.field b:I


# direct methods
.method constructor <init>(I)V
    .registers 3

    .prologue
    .line 320
    invoke-direct {p0}, Lcom/a/b/d/jb;-><init>()V

    .line 321
    const-string v0, "initialCapacity"

    invoke-static {p1, v0}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 322
    new-array v0, p1, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/a/b/d/ja;->a:[Ljava/lang/Object;

    .line 323
    const/4 v0, 0x0

    iput v0, p0, Lcom/a/b/d/ja;->b:I

    .line 324
    return-void
.end method

.method private a(I)V
    .registers 4

    .prologue
    .line 331
    iget-object v0, p0, Lcom/a/b/d/ja;->a:[Ljava/lang/Object;

    array-length v0, v0

    if-ge v0, p1, :cond_14

    .line 332
    iget-object v0, p0, Lcom/a/b/d/ja;->a:[Ljava/lang/Object;

    iget-object v1, p0, Lcom/a/b/d/ja;->a:[Ljava/lang/Object;

    array-length v1, v1

    invoke-static {v1, p1}, Lcom/a/b/d/ja;->a(II)I

    move-result v1

    invoke-static {v0, v1}, Lcom/a/b/d/yc;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/ja;->a:[Ljava/lang/Object;

    .line 335
    :cond_14
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Lcom/a/b/d/ja;
    .registers 5

    .prologue
    .line 339
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    iget v0, p0, Lcom/a/b/d/ja;->b:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/a/b/d/ja;->a(I)V

    .line 341
    iget-object v0, p0, Lcom/a/b/d/ja;->a:[Ljava/lang/Object;

    iget v1, p0, Lcom/a/b/d/ja;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/a/b/d/ja;->b:I

    aput-object p1, v0, v1

    .line 342
    return-object p0
.end method

.method public a(Ljava/lang/Iterable;)Lcom/a/b/d/jb;
    .registers 4

    .prologue
    .line 356
    instance-of v0, p1, Ljava/util/Collection;

    if-eqz v0, :cond_11

    move-object v0, p1

    .line 357
    check-cast v0, Ljava/util/Collection;

    .line 358
    iget v1, p0, Lcom/a/b/d/ja;->b:I

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    add-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/a/b/d/ja;->a(I)V

    .line 360
    :cond_11
    invoke-super {p0, p1}, Lcom/a/b/d/jb;->a(Ljava/lang/Iterable;)Lcom/a/b/d/jb;

    .line 361
    return-object p0
.end method

.method public varargs a([Ljava/lang/Object;)Lcom/a/b/d/jb;
    .registers 6

    .prologue
    .line 347
    invoke-static {p1}, Lcom/a/b/d/yc;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 348
    iget v0, p0, Lcom/a/b/d/ja;->b:I

    array-length v1, p1

    add-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/a/b/d/ja;->a(I)V

    .line 349
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/a/b/d/ja;->a:[Ljava/lang/Object;

    iget v2, p0, Lcom/a/b/d/ja;->b:I

    array-length v3, p1

    invoke-static {p1, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 350
    iget v0, p0, Lcom/a/b/d/ja;->b:I

    array-length v1, p1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/a/b/d/ja;->b:I

    .line 351
    return-object p0
.end method

.method public synthetic b(Ljava/lang/Object;)Lcom/a/b/d/jb;
    .registers 3

    .prologue
    .line 316
    invoke-virtual {p0, p1}, Lcom/a/b/d/ja;->a(Ljava/lang/Object;)Lcom/a/b/d/ja;

    move-result-object v0

    return-object v0
.end method
