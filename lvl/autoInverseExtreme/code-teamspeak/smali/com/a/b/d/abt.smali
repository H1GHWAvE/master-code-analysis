.class final Lcom/a/b/d/abt;
.super Lcom/a/b/d/zr;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field private final a:Lcom/a/b/d/jt;

.field private final b:Lcom/a/b/d/jt;

.field private final c:[I

.field private final d:[I


# direct methods
.method constructor <init>(Lcom/a/b/d/jl;Lcom/a/b/d/lo;Lcom/a/b/d/lo;)V
    .registers 15

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/a/b/d/zr;-><init>()V

    .line 39
    invoke-static {}, Lcom/a/b/d/sz;->c()Ljava/util/HashMap;

    move-result-object v2

    .line 40
    invoke-static {}, Lcom/a/b/d/sz;->d()Ljava/util/LinkedHashMap;

    move-result-object v3

    .line 41
    invoke-virtual {p2}, Lcom/a/b/d/lo;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_f
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2d

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 42
    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    new-instance v4, Ljava/util/LinkedHashMap;

    invoke-direct {v4}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-interface {v3, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_f

    .line 45
    :cond_2d
    invoke-static {}, Lcom/a/b/d/sz;->d()Ljava/util/LinkedHashMap;

    move-result-object v4

    .line 46
    invoke-virtual {p3}, Lcom/a/b/d/lo;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_35
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_48

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 47
    new-instance v5, Ljava/util/LinkedHashMap;

    invoke-direct {v5}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-interface {v4, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_35

    .line 49
    :cond_48
    invoke-virtual {p1}, Lcom/a/b/d/jl;->size()I

    move-result v0

    new-array v5, v0, [I

    .line 50
    invoke-virtual {p1}, Lcom/a/b/d/jl;->size()I

    move-result v0

    new-array v6, v0, [I

    .line 51
    const/4 v0, 0x0

    move v1, v0

    :goto_56
    invoke-virtual {p1}, Lcom/a/b/d/jl;->size()I

    move-result v0

    if-ge v1, v0, :cond_106

    .line 52
    invoke-virtual {p1, v1}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/adw;

    .line 53
    invoke-interface {v0}, Lcom/a/b/d/adw;->a()Ljava/lang/Object;

    move-result-object v7

    .line 54
    invoke-interface {v0}, Lcom/a/b/d/adw;->b()Ljava/lang/Object;

    move-result-object v8

    .line 55
    invoke-interface {v0}, Lcom/a/b/d/adw;->c()Ljava/lang/Object;

    move-result-object v9

    .line 57
    invoke-interface {v2, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v5, v1

    .line 58
    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 59
    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v10

    aput v10, v6, v1

    .line 60
    invoke-interface {v0, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 61
    if-eqz v0, :cond_f8

    .line 62
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x25

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Duplicate value for row="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", column="

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 65
    :cond_f8
    invoke-interface {v4, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0, v7, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_56

    .line 67
    :cond_106
    iput-object v5, p0, Lcom/a/b/d/abt;->c:[I

    .line 68
    iput-object v6, p0, Lcom/a/b/d/abt;->d:[I

    .line 69
    invoke-static {}, Lcom/a/b/d/jt;->l()Lcom/a/b/d/ju;

    move-result-object v1

    .line 70
    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_116
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_134

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 71
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-static {v0}, Lcom/a/b/d/jt;->a(Ljava/util/Map;)Lcom/a/b/d/jt;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    goto :goto_116

    .line 73
    :cond_134
    invoke-virtual {v1}, Lcom/a/b/d/ju;->a()Lcom/a/b/d/jt;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/abt;->a:Lcom/a/b/d/jt;

    .line 75
    invoke-static {}, Lcom/a/b/d/jt;->l()Lcom/a/b/d/ju;

    move-result-object v1

    .line 76
    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_146
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_164

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 77
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-static {v0}, Lcom/a/b/d/jt;->a(Ljava/util/Map;)Lcom/a/b/d/jt;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    goto :goto_146

    .line 79
    :cond_164
    invoke-virtual {v1}, Lcom/a/b/d/ju;->a()Lcom/a/b/d/jt;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/abt;->b:Lcom/a/b/d/jt;

    .line 80
    return-void
.end method


# virtual methods
.method final a(I)Lcom/a/b/d/adw;
    .registers 5

    .prologue
    .line 97
    iget-object v0, p0, Lcom/a/b/d/abt;->c:[I

    aget v0, v0, p1

    .line 98
    iget-object v1, p0, Lcom/a/b/d/abt;->a:Lcom/a/b/d/jt;

    invoke-virtual {v1}, Lcom/a/b/d/jt;->e()Lcom/a/b/d/lo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/b/d/lo;->f()Lcom/a/b/d/jl;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 99
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/b/d/jt;

    .line 100
    iget-object v2, p0, Lcom/a/b/d/abt;->d:[I

    aget v2, v2, p1

    .line 101
    invoke-virtual {v1}, Lcom/a/b/d/jt;->e()Lcom/a/b/d/lo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/b/d/lo;->f()Lcom/a/b/d/jl;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 102
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v2, v1}, Lcom/a/b/d/abt;->b(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/adw;

    move-result-object v0

    return-object v0
.end method

.method final b(I)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 107
    iget-object v0, p0, Lcom/a/b/d/abt;->c:[I

    aget v0, v0, p1

    .line 108
    iget-object v1, p0, Lcom/a/b/d/abt;->a:Lcom/a/b/d/jt;

    invoke-virtual {v1}, Lcom/a/b/d/jt;->h()Lcom/a/b/d/iz;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/b/d/iz;->f()Lcom/a/b/d/jl;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/jt;

    .line 109
    iget-object v1, p0, Lcom/a/b/d/abt;->d:[I

    aget v1, v1, p1

    .line 110
    invoke-virtual {v0}, Lcom/a/b/d/jt;->h()Lcom/a/b/d/iz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/iz;->f()Lcom/a/b/d/jl;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final k()I
    .registers 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/a/b/d/abt;->c:[I

    array-length v0, v0

    return v0
.end method

.method public final bridge synthetic l()Ljava/util/Map;
    .registers 2

    .prologue
    .line 27
    .line 1083
    iget-object v0, p0, Lcom/a/b/d/abt;->b:Lcom/a/b/d/jt;

    .line 27
    return-object v0
.end method

.method public final bridge synthetic m()Ljava/util/Map;
    .registers 2

    .prologue
    .line 27
    .line 1087
    iget-object v0, p0, Lcom/a/b/d/abt;->a:Lcom/a/b/d/jt;

    .line 27
    return-object v0
.end method

.method public final n()Lcom/a/b/d/jt;
    .registers 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/a/b/d/abt;->b:Lcom/a/b/d/jt;

    return-object v0
.end method

.method public final o()Lcom/a/b/d/jt;
    .registers 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/a/b/d/abt;->a:Lcom/a/b/d/jt;

    return-object v0
.end method
