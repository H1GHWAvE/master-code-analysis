.class final Lcom/a/b/d/aez;
.super Lcom/a/b/d/xo;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/Object;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field b:I

.field c:I

.field d:J

.field e:Lcom/a/b/d/aez;

.field f:Lcom/a/b/d/aez;

.field g:Lcom/a/b/d/aez;

.field h:Lcom/a/b/d/aez;

.field private i:I


# direct methods
.method constructor <init>(Ljava/lang/Object;I)V
    .registers 8
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 533
    invoke-direct {p0}, Lcom/a/b/d/xo;-><init>()V

    .line 534
    if-lez p2, :cond_1b

    move v0, v1

    :goto_8
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 535
    iput-object p1, p0, Lcom/a/b/d/aez;->a:Ljava/lang/Object;

    .line 536
    iput p2, p0, Lcom/a/b/d/aez;->b:I

    .line 537
    int-to-long v2, p2

    iput-wide v2, p0, Lcom/a/b/d/aez;->d:J

    .line 538
    iput v1, p0, Lcom/a/b/d/aez;->c:I

    .line 539
    iput v1, p0, Lcom/a/b/d/aez;->i:I

    .line 540
    iput-object v4, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    .line 541
    iput-object v4, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    .line 542
    return-void

    .line 534
    :cond_1b
    const/4 v0, 0x0

    goto :goto_8
.end method

.method private a(Lcom/a/b/d/aez;)Lcom/a/b/d/aez;
    .registers 6

    .prologue
    .line 808
    iget-object v0, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    if-nez v0, :cond_7

    .line 809
    iget-object v0, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    .line 814
    :goto_6
    return-object v0

    .line 811
    :cond_7
    iget-object v0, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    invoke-direct {v0, p1}, Lcom/a/b/d/aez;->a(Lcom/a/b/d/aez;)Lcom/a/b/d/aez;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    .line 812
    iget v0, p0, Lcom/a/b/d/aez;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/b/d/aez;->c:I

    .line 813
    iget-wide v0, p0, Lcom/a/b/d/aez;->d:J

    iget v2, p1, Lcom/a/b/d/aez;->b:I

    int-to-long v2, v2

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/a/b/d/aez;->d:J

    .line 814
    invoke-direct {p0}, Lcom/a/b/d/aez;->g()Lcom/a/b/d/aez;

    move-result-object v0

    goto :goto_6
.end method

.method private static synthetic a(Lcom/a/b/d/aez;Lcom/a/b/d/aez;)Lcom/a/b/d/aez;
    .registers 2

    .prologue
    .line 519
    iput-object p1, p0, Lcom/a/b/d/aez;->h:Lcom/a/b/d/aez;

    return-object p1
.end method

.method private static synthetic a(Lcom/a/b/d/aez;Ljava/util/Comparator;Ljava/lang/Object;)Lcom/a/b/d/aez;
    .registers 4

    .prologue
    .line 519
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/aez;->a(Ljava/util/Comparator;Ljava/lang/Object;)Lcom/a/b/d/aez;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Object;I)Lcom/a/b/d/aez;
    .registers 7

    .prologue
    .line 556
    new-instance v0, Lcom/a/b/d/aez;

    invoke-direct {v0, p1, p2}, Lcom/a/b/d/aez;-><init>(Ljava/lang/Object;I)V

    iput-object v0, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    .line 557
    iget-object v0, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    iget-object v1, p0, Lcom/a/b/d/aez;->h:Lcom/a/b/d/aez;

    invoke-static {p0, v0, v1}, Lcom/a/b/d/aer;->a(Lcom/a/b/d/aez;Lcom/a/b/d/aez;Lcom/a/b/d/aez;)V

    .line 558
    const/4 v0, 0x2

    iget v1, p0, Lcom/a/b/d/aez;->i:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/a/b/d/aez;->i:I

    .line 559
    iget v0, p0, Lcom/a/b/d/aez;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/aez;->c:I

    .line 560
    iget-wide v0, p0, Lcom/a/b/d/aez;->d:J

    int-to-long v2, p2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/a/b/d/aez;->d:J

    .line 561
    return-object p0
.end method

.method private b(Lcom/a/b/d/aez;)Lcom/a/b/d/aez;
    .registers 6

    .prologue
    .line 820
    iget-object v0, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    if-nez v0, :cond_7

    .line 821
    iget-object v0, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    .line 826
    :goto_6
    return-object v0

    .line 823
    :cond_7
    iget-object v0, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    invoke-direct {v0, p1}, Lcom/a/b/d/aez;->b(Lcom/a/b/d/aez;)Lcom/a/b/d/aez;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    .line 824
    iget v0, p0, Lcom/a/b/d/aez;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/b/d/aez;->c:I

    .line 825
    iget-wide v0, p0, Lcom/a/b/d/aez;->d:J

    iget v2, p1, Lcom/a/b/d/aez;->b:I

    int-to-long v2, v2

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/a/b/d/aez;->d:J

    .line 826
    invoke-direct {p0}, Lcom/a/b/d/aez;->g()Lcom/a/b/d/aez;

    move-result-object v0

    goto :goto_6
.end method

.method private static synthetic b(Lcom/a/b/d/aez;Lcom/a/b/d/aez;)Lcom/a/b/d/aez;
    .registers 2

    .prologue
    .line 519
    iput-object p1, p0, Lcom/a/b/d/aez;->g:Lcom/a/b/d/aez;

    return-object p1
.end method

.method private static synthetic b(Lcom/a/b/d/aez;Ljava/util/Comparator;Ljava/lang/Object;)Lcom/a/b/d/aez;
    .registers 4

    .prologue
    .line 519
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/aez;->b(Ljava/util/Comparator;Ljava/lang/Object;)Lcom/a/b/d/aez;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/Object;I)Lcom/a/b/d/aez;
    .registers 7

    .prologue
    .line 565
    new-instance v0, Lcom/a/b/d/aez;

    invoke-direct {v0, p1, p2}, Lcom/a/b/d/aez;-><init>(Ljava/lang/Object;I)V

    iput-object v0, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    .line 566
    iget-object v0, p0, Lcom/a/b/d/aez;->g:Lcom/a/b/d/aez;

    iget-object v1, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    invoke-static {v0, v1, p0}, Lcom/a/b/d/aer;->a(Lcom/a/b/d/aez;Lcom/a/b/d/aez;Lcom/a/b/d/aez;)V

    .line 567
    const/4 v0, 0x2

    iget v1, p0, Lcom/a/b/d/aez;->i:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/a/b/d/aez;->i:I

    .line 568
    iget v0, p0, Lcom/a/b/d/aez;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/aez;->c:I

    .line 569
    iget-wide v0, p0, Lcom/a/b/d/aez;->d:J

    int-to-long v2, p2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/a/b/d/aez;->d:J

    .line 570
    return-object p0
.end method

.method private c(Ljava/util/Comparator;Ljava/lang/Object;)I
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 545
    :goto_1
    iget-object v1, p0, Lcom/a/b/d/aez;->a:Ljava/lang/Object;

    invoke-interface {p1, p2, v1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    .line 546
    if-gez v1, :cond_11

    .line 547
    iget-object v1, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    if-nez v1, :cond_e

    .line 551
    :cond_d
    :goto_d
    return v0

    .line 547
    :cond_e
    iget-object p0, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    goto :goto_1

    .line 548
    :cond_11
    if-lez v1, :cond_1a

    .line 549
    iget-object v1, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    if-eqz v1, :cond_d

    iget-object p0, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    goto :goto_1

    .line 551
    :cond_1a
    iget v0, p0, Lcom/a/b/d/aez;->b:I

    goto :goto_d
.end method

.method private static c(Lcom/a/b/d/aez;)J
    .registers 3
    .param p0    # Lcom/a/b/d/aez;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 892
    if-nez p0, :cond_5

    const-wide/16 v0, 0x0

    :goto_4
    return-wide v0

    :cond_5
    iget-wide v0, p0, Lcom/a/b/d/aez;->d:J

    goto :goto_4
.end method

.method private c()Lcom/a/b/d/aez;
    .registers 7

    .prologue
    .line 781
    iget v0, p0, Lcom/a/b/d/aez;->b:I

    .line 782
    const/4 v1, 0x0

    iput v1, p0, Lcom/a/b/d/aez;->b:I

    .line 783
    iget-object v1, p0, Lcom/a/b/d/aez;->g:Lcom/a/b/d/aez;

    iget-object v2, p0, Lcom/a/b/d/aez;->h:Lcom/a/b/d/aez;

    invoke-static {v1, v2}, Lcom/a/b/d/aer;->a(Lcom/a/b/d/aez;Lcom/a/b/d/aez;)V

    .line 784
    iget-object v1, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    if-nez v1, :cond_13

    .line 785
    iget-object v0, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    .line 802
    :goto_12
    return-object v0

    .line 786
    :cond_13
    iget-object v1, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    if-nez v1, :cond_1a

    .line 787
    iget-object v0, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    goto :goto_12

    .line 788
    :cond_1a
    iget-object v1, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    iget v1, v1, Lcom/a/b/d/aez;->i:I

    iget-object v2, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    iget v2, v2, Lcom/a/b/d/aez;->i:I

    if-lt v1, v2, :cond_43

    .line 789
    iget-object v1, p0, Lcom/a/b/d/aez;->g:Lcom/a/b/d/aez;

    .line 791
    iget-object v2, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    invoke-direct {v2, v1}, Lcom/a/b/d/aez;->b(Lcom/a/b/d/aez;)Lcom/a/b/d/aez;

    move-result-object v2

    iput-object v2, v1, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    .line 792
    iget-object v2, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    iput-object v2, v1, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    .line 793
    iget v2, p0, Lcom/a/b/d/aez;->c:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, Lcom/a/b/d/aez;->c:I

    .line 794
    iget-wide v2, p0, Lcom/a/b/d/aez;->d:J

    int-to-long v4, v0

    sub-long/2addr v2, v4

    iput-wide v2, v1, Lcom/a/b/d/aez;->d:J

    .line 795
    invoke-direct {v1}, Lcom/a/b/d/aez;->g()Lcom/a/b/d/aez;

    move-result-object v0

    goto :goto_12

    .line 797
    :cond_43
    iget-object v1, p0, Lcom/a/b/d/aez;->h:Lcom/a/b/d/aez;

    .line 798
    iget-object v2, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    invoke-direct {v2, v1}, Lcom/a/b/d/aez;->a(Lcom/a/b/d/aez;)Lcom/a/b/d/aez;

    move-result-object v2

    iput-object v2, v1, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    .line 799
    iget-object v2, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    iput-object v2, v1, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    .line 800
    iget v2, p0, Lcom/a/b/d/aez;->c:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, Lcom/a/b/d/aez;->c:I

    .line 801
    iget-wide v2, p0, Lcom/a/b/d/aez;->d:J

    int-to-long v4, v0

    sub-long/2addr v2, v4

    iput-wide v2, v1, Lcom/a/b/d/aez;->d:J

    .line 802
    invoke-direct {v1}, Lcom/a/b/d/aez;->g()Lcom/a/b/d/aez;

    move-result-object v0

    goto :goto_12
.end method

.method private static d(Lcom/a/b/d/aez;)I
    .registers 2
    .param p0    # Lcom/a/b/d/aez;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 896
    if-nez p0, :cond_4

    const/4 v0, 0x0

    :goto_3
    return v0

    :cond_4
    iget v0, p0, Lcom/a/b/d/aez;->i:I

    goto :goto_3
.end method

.method private d()V
    .registers 5

    .prologue
    .line 831
    iget-object v0, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    invoke-static {v0}, Lcom/a/b/d/aer;->a(Lcom/a/b/d/aez;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    invoke-static {v1}, Lcom/a/b/d/aer;->a(Lcom/a/b/d/aez;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/a/b/d/aez;->c:I

    .line 833
    iget v0, p0, Lcom/a/b/d/aez;->b:I

    int-to-long v0, v0

    iget-object v2, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    invoke-static {v2}, Lcom/a/b/d/aez;->c(Lcom/a/b/d/aez;)J

    move-result-wide v2

    add-long/2addr v0, v2

    iget-object v2, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    invoke-static {v2}, Lcom/a/b/d/aez;->c(Lcom/a/b/d/aez;)J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/a/b/d/aez;->d:J

    .line 834
    return-void
.end method

.method private static synthetic e(Lcom/a/b/d/aez;)I
    .registers 2

    .prologue
    .line 519
    iget v0, p0, Lcom/a/b/d/aez;->b:I

    return v0
.end method

.method private e()V
    .registers 3

    .prologue
    .line 837
    iget-object v0, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    invoke-static {v0}, Lcom/a/b/d/aez;->d(Lcom/a/b/d/aez;)I

    move-result v0

    iget-object v1, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    invoke-static {v1}, Lcom/a/b/d/aez;->d(Lcom/a/b/d/aez;)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/aez;->i:I

    .line 838
    return-void
.end method

.method private static synthetic f(Lcom/a/b/d/aez;)J
    .registers 3

    .prologue
    .line 519
    iget-wide v0, p0, Lcom/a/b/d/aez;->d:J

    return-wide v0
.end method

.method private f()V
    .registers 5

    .prologue
    .line 841
    .line 1831
    iget-object v0, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    invoke-static {v0}, Lcom/a/b/d/aer;->a(Lcom/a/b/d/aez;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    invoke-static {v1}, Lcom/a/b/d/aer;->a(Lcom/a/b/d/aez;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/a/b/d/aez;->c:I

    .line 1833
    iget v0, p0, Lcom/a/b/d/aez;->b:I

    int-to-long v0, v0

    iget-object v2, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    invoke-static {v2}, Lcom/a/b/d/aez;->c(Lcom/a/b/d/aez;)J

    move-result-wide v2

    add-long/2addr v0, v2

    iget-object v2, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    invoke-static {v2}, Lcom/a/b/d/aez;->c(Lcom/a/b/d/aez;)J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/a/b/d/aez;->d:J

    .line 842
    invoke-direct {p0}, Lcom/a/b/d/aez;->e()V

    .line 843
    return-void
.end method

.method private static synthetic g(Lcom/a/b/d/aez;)I
    .registers 2

    .prologue
    .line 519
    iget v0, p0, Lcom/a/b/d/aez;->c:I

    return v0
.end method

.method private g()Lcom/a/b/d/aez;
    .registers 2

    .prologue
    .line 846
    invoke-direct {p0}, Lcom/a/b/d/aez;->h()I

    move-result v0

    sparse-switch v0, :sswitch_data_36

    .line 858
    invoke-direct {p0}, Lcom/a/b/d/aez;->e()V

    .line 859
    :goto_a
    return-object p0

    .line 848
    :sswitch_b
    iget-object v0, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    invoke-direct {v0}, Lcom/a/b/d/aez;->h()I

    move-result v0

    if-lez v0, :cond_1b

    .line 849
    iget-object v0, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    invoke-direct {v0}, Lcom/a/b/d/aez;->j()Lcom/a/b/d/aez;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    .line 851
    :cond_1b
    invoke-direct {p0}, Lcom/a/b/d/aez;->i()Lcom/a/b/d/aez;

    move-result-object p0

    goto :goto_a

    .line 853
    :sswitch_20
    iget-object v0, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    invoke-direct {v0}, Lcom/a/b/d/aez;->h()I

    move-result v0

    if-gez v0, :cond_30

    .line 854
    iget-object v0, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    invoke-direct {v0}, Lcom/a/b/d/aez;->i()Lcom/a/b/d/aez;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    .line 856
    :cond_30
    invoke-direct {p0}, Lcom/a/b/d/aez;->j()Lcom/a/b/d/aez;

    move-result-object p0

    goto :goto_a

    .line 846
    nop

    :sswitch_data_36
    .sparse-switch
        -0x2 -> :sswitch_b
        0x2 -> :sswitch_20
    .end sparse-switch
.end method

.method private h()I
    .registers 3

    .prologue
    .line 864
    iget-object v0, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    invoke-static {v0}, Lcom/a/b/d/aez;->d(Lcom/a/b/d/aez;)I

    move-result v0

    iget-object v1, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    invoke-static {v1}, Lcom/a/b/d/aez;->d(Lcom/a/b/d/aez;)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method private static synthetic h(Lcom/a/b/d/aez;)Ljava/lang/Object;
    .registers 2

    .prologue
    .line 519
    iget-object v0, p0, Lcom/a/b/d/aez;->a:Ljava/lang/Object;

    return-object v0
.end method

.method private i()Lcom/a/b/d/aez;
    .registers 5

    .prologue
    .line 868
    iget-object v0, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    if-eqz v0, :cond_1f

    const/4 v0, 0x1

    :goto_5
    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    .line 869
    iget-object v0, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    .line 870
    iget-object v1, v0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    iput-object v1, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    .line 871
    iput-object p0, v0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    .line 872
    iget-wide v2, p0, Lcom/a/b/d/aez;->d:J

    iput-wide v2, v0, Lcom/a/b/d/aez;->d:J

    .line 873
    iget v1, p0, Lcom/a/b/d/aez;->c:I

    iput v1, v0, Lcom/a/b/d/aez;->c:I

    .line 874
    invoke-direct {p0}, Lcom/a/b/d/aez;->f()V

    .line 875
    invoke-direct {v0}, Lcom/a/b/d/aez;->e()V

    .line 876
    return-object v0

    .line 868
    :cond_1f
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private static synthetic i(Lcom/a/b/d/aez;)Lcom/a/b/d/aez;
    .registers 2

    .prologue
    .line 519
    iget-object v0, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    return-object v0
.end method

.method private j()Lcom/a/b/d/aez;
    .registers 5

    .prologue
    .line 880
    iget-object v0, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    if-eqz v0, :cond_1f

    const/4 v0, 0x1

    :goto_5
    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    .line 881
    iget-object v0, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    .line 882
    iget-object v1, v0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    iput-object v1, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    .line 883
    iput-object p0, v0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    .line 884
    iget-wide v2, p0, Lcom/a/b/d/aez;->d:J

    iput-wide v2, v0, Lcom/a/b/d/aez;->d:J

    .line 885
    iget v1, p0, Lcom/a/b/d/aez;->c:I

    iput v1, v0, Lcom/a/b/d/aez;->c:I

    .line 886
    invoke-direct {p0}, Lcom/a/b/d/aez;->f()V

    .line 887
    invoke-direct {v0}, Lcom/a/b/d/aez;->e()V

    .line 888
    return-object v0

    .line 880
    :cond_1f
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private static synthetic j(Lcom/a/b/d/aez;)Lcom/a/b/d/aez;
    .registers 2

    .prologue
    .line 519
    iget-object v0, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    return-object v0
.end method

.method private static synthetic k(Lcom/a/b/d/aez;)Lcom/a/b/d/aez;
    .registers 2

    .prologue
    .line 519
    iget-object v0, p0, Lcom/a/b/d/aez;->h:Lcom/a/b/d/aez;

    return-object v0
.end method

.method private static synthetic l(Lcom/a/b/d/aez;)Lcom/a/b/d/aez;
    .registers 2

    .prologue
    .line 519
    iget-object v0, p0, Lcom/a/b/d/aez;->g:Lcom/a/b/d/aez;

    return-object v0
.end method


# virtual methods
.method final a(Ljava/util/Comparator;Ljava/lang/Object;)Lcom/a/b/d/aez;
    .registers 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 900
    :goto_0
    iget-object v0, p0, Lcom/a/b/d/aez;->a:Ljava/lang/Object;

    invoke-interface {p1, p2, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 901
    if-gez v0, :cond_1b

    .line 902
    iget-object v0, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    if-nez v0, :cond_d

    .line 906
    :cond_c
    :goto_c
    return-object p0

    .line 902
    :cond_d
    iget-object v0, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    invoke-virtual {v0, p1, p2}, Lcom/a/b/d/aez;->a(Ljava/util/Comparator;Ljava/lang/Object;)Lcom/a/b/d/aez;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/aez;

    move-object p0, v0

    goto :goto_c

    .line 903
    :cond_1b
    if-eqz v0, :cond_c

    .line 906
    iget-object v0, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    if-nez v0, :cond_23

    const/4 p0, 0x0

    goto :goto_c

    :cond_23
    iget-object p0, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    goto :goto_0
.end method

.method final a(Ljava/util/Comparator;Ljava/lang/Object;II[I)Lcom/a/b/d/aez;
    .registers 13
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x0

    .line 723
    iget-object v0, p0, Lcom/a/b/d/aez;->a:Ljava/lang/Object;

    invoke-interface {p1, p2, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 724
    if-gez v0, :cond_4f

    .line 725
    iget-object v0, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    .line 726
    if-nez v0, :cond_18

    .line 727
    aput v6, p5, v6

    .line 728
    if-nez p3, :cond_17

    if-lez p4, :cond_17

    .line 729
    invoke-direct {p0, p2, p4}, Lcom/a/b/d/aez;->b(Ljava/lang/Object;I)Lcom/a/b/d/aez;

    move-result-object p0

    .line 777
    :cond_17
    :goto_17
    return-object p0

    :cond_18
    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    .line 734
    invoke-virtual/range {v0 .. v5}, Lcom/a/b/d/aez;->a(Ljava/util/Comparator;Ljava/lang/Object;II[I)Lcom/a/b/d/aez;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    .line 736
    aget v0, p5, v6

    if-ne v0, p3, :cond_3d

    .line 737
    if-nez p4, :cond_42

    aget v0, p5, v6

    if-eqz v0, :cond_42

    .line 738
    iget v0, p0, Lcom/a/b/d/aez;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/b/d/aez;->c:I

    .line 742
    :cond_33
    :goto_33
    iget-wide v0, p0, Lcom/a/b/d/aez;->d:J

    aget v2, p5, v6

    sub-int v2, p4, v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/a/b/d/aez;->d:J

    .line 744
    :cond_3d
    invoke-direct {p0}, Lcom/a/b/d/aez;->g()Lcom/a/b/d/aez;

    move-result-object p0

    goto :goto_17

    .line 739
    :cond_42
    if-lez p4, :cond_33

    aget v0, p5, v6

    if-nez v0, :cond_33

    .line 740
    iget v0, p0, Lcom/a/b/d/aez;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/aez;->c:I

    goto :goto_33

    .line 745
    :cond_4f
    if-lez v0, :cond_97

    .line 746
    iget-object v0, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    .line 747
    if-nez v0, :cond_60

    .line 748
    aput v6, p5, v6

    .line 749
    if-nez p3, :cond_17

    if-lez p4, :cond_17

    .line 750
    invoke-direct {p0, p2, p4}, Lcom/a/b/d/aez;->a(Ljava/lang/Object;I)Lcom/a/b/d/aez;

    move-result-object p0

    goto :goto_17

    :cond_60
    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    .line 755
    invoke-virtual/range {v0 .. v5}, Lcom/a/b/d/aez;->a(Ljava/util/Comparator;Ljava/lang/Object;II[I)Lcom/a/b/d/aez;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    .line 757
    aget v0, p5, v6

    if-ne v0, p3, :cond_85

    .line 758
    if-nez p4, :cond_8a

    aget v0, p5, v6

    if-eqz v0, :cond_8a

    .line 759
    iget v0, p0, Lcom/a/b/d/aez;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/b/d/aez;->c:I

    .line 763
    :cond_7b
    :goto_7b
    iget-wide v0, p0, Lcom/a/b/d/aez;->d:J

    aget v2, p5, v6

    sub-int v2, p4, v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/a/b/d/aez;->d:J

    .line 765
    :cond_85
    invoke-direct {p0}, Lcom/a/b/d/aez;->g()Lcom/a/b/d/aez;

    move-result-object p0

    goto :goto_17

    .line 760
    :cond_8a
    if-lez p4, :cond_7b

    aget v0, p5, v6

    if-nez v0, :cond_7b

    .line 761
    iget v0, p0, Lcom/a/b/d/aez;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/aez;->c:I

    goto :goto_7b

    .line 769
    :cond_97
    iget v0, p0, Lcom/a/b/d/aez;->b:I

    aput v0, p5, v6

    .line 770
    iget v0, p0, Lcom/a/b/d/aez;->b:I

    if-ne p3, v0, :cond_17

    .line 771
    if-nez p4, :cond_a7

    .line 772
    invoke-direct {p0}, Lcom/a/b/d/aez;->c()Lcom/a/b/d/aez;

    move-result-object p0

    goto/16 :goto_17

    .line 774
    :cond_a7
    iget-wide v0, p0, Lcom/a/b/d/aez;->d:J

    iget v2, p0, Lcom/a/b/d/aez;->b:I

    sub-int v2, p4, v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/a/b/d/aez;->d:J

    .line 775
    iput p4, p0, Lcom/a/b/d/aez;->b:I

    goto/16 :goto_17
.end method

.method final a(Ljava/util/Comparator;Ljava/lang/Object;I[I)Lcom/a/b/d/aez;
    .registers 11
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 578
    iget-object v1, p0, Lcom/a/b/d/aez;->a:Ljava/lang/Object;

    invoke-interface {p1, p2, v1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    .line 579
    if-gez v1, :cond_37

    .line 580
    iget-object v1, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    .line 581
    if-nez v1, :cond_14

    .line 582
    aput v0, p4, v0

    .line 583
    invoke-direct {p0, p2, p3}, Lcom/a/b/d/aez;->b(Ljava/lang/Object;I)Lcom/a/b/d/aez;

    move-result-object p0

    .line 615
    :cond_13
    :goto_13
    return-object p0

    .line 585
    :cond_14
    iget v2, v1, Lcom/a/b/d/aez;->i:I

    .line 587
    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/a/b/d/aez;->a(Ljava/util/Comparator;Ljava/lang/Object;I[I)Lcom/a/b/d/aez;

    move-result-object v1

    iput-object v1, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    .line 588
    aget v0, p4, v0

    if-nez v0, :cond_26

    .line 589
    iget v0, p0, Lcom/a/b/d/aez;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/aez;->c:I

    .line 591
    :cond_26
    iget-wide v0, p0, Lcom/a/b/d/aez;->d:J

    int-to-long v4, p3

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/a/b/d/aez;->d:J

    .line 592
    iget-object v0, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    iget v0, v0, Lcom/a/b/d/aez;->i:I

    if-eq v0, v2, :cond_13

    invoke-direct {p0}, Lcom/a/b/d/aez;->g()Lcom/a/b/d/aez;

    move-result-object p0

    goto :goto_13

    .line 593
    :cond_37
    if-lez v1, :cond_67

    .line 594
    iget-object v1, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    .line 595
    if-nez v1, :cond_44

    .line 596
    aput v0, p4, v0

    .line 597
    invoke-direct {p0, p2, p3}, Lcom/a/b/d/aez;->a(Ljava/lang/Object;I)Lcom/a/b/d/aez;

    move-result-object p0

    goto :goto_13

    .line 599
    :cond_44
    iget v2, v1, Lcom/a/b/d/aez;->i:I

    .line 601
    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/a/b/d/aez;->a(Ljava/util/Comparator;Ljava/lang/Object;I[I)Lcom/a/b/d/aez;

    move-result-object v1

    iput-object v1, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    .line 602
    aget v0, p4, v0

    if-nez v0, :cond_56

    .line 603
    iget v0, p0, Lcom/a/b/d/aez;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/aez;->c:I

    .line 605
    :cond_56
    iget-wide v0, p0, Lcom/a/b/d/aez;->d:J

    int-to-long v4, p3

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/a/b/d/aez;->d:J

    .line 606
    iget-object v0, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    iget v0, v0, Lcom/a/b/d/aez;->i:I

    if-eq v0, v2, :cond_13

    invoke-direct {p0}, Lcom/a/b/d/aez;->g()Lcom/a/b/d/aez;

    move-result-object p0

    goto :goto_13

    .line 610
    :cond_67
    iget v1, p0, Lcom/a/b/d/aez;->b:I

    aput v1, p4, v0

    .line 611
    iget v1, p0, Lcom/a/b/d/aez;->b:I

    int-to-long v2, v1

    int-to-long v4, p3

    add-long/2addr v2, v4

    .line 612
    const-wide/32 v4, 0x7fffffff

    cmp-long v1, v2, v4

    if-gtz v1, :cond_78

    const/4 v0, 0x1

    :cond_78
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 613
    iget v0, p0, Lcom/a/b/d/aez;->b:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/a/b/d/aez;->b:I

    .line 614
    iget-wide v0, p0, Lcom/a/b/d/aez;->d:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/a/b/d/aez;->d:J

    goto :goto_13
.end method

.method public final a()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 923
    iget-object v0, p0, Lcom/a/b/d/aez;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public final b()I
    .registers 2

    .prologue
    .line 928
    iget v0, p0, Lcom/a/b/d/aez;->b:I

    return v0
.end method

.method final b(Ljava/util/Comparator;Ljava/lang/Object;)Lcom/a/b/d/aez;
    .registers 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 911
    :goto_0
    iget-object v0, p0, Lcom/a/b/d/aez;->a:Ljava/lang/Object;

    invoke-interface {p1, p2, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 912
    if-lez v0, :cond_1b

    .line 913
    iget-object v0, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    if-nez v0, :cond_d

    .line 917
    :cond_c
    :goto_c
    return-object p0

    .line 913
    :cond_d
    iget-object v0, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    invoke-virtual {v0, p1, p2}, Lcom/a/b/d/aez;->b(Ljava/util/Comparator;Ljava/lang/Object;)Lcom/a/b/d/aez;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/aez;

    move-object p0, v0

    goto :goto_c

    .line 914
    :cond_1b
    if-eqz v0, :cond_c

    .line 917
    iget-object v0, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    if-nez v0, :cond_23

    const/4 p0, 0x0

    goto :goto_c

    :cond_23
    iget-object p0, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    goto :goto_0
.end method

.method final b(Ljava/util/Comparator;Ljava/lang/Object;I[I)Lcom/a/b/d/aez;
    .registers 10
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 619
    iget-object v0, p0, Lcom/a/b/d/aez;->a:Ljava/lang/Object;

    invoke-interface {p1, p2, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 620
    if-gez v0, :cond_3c

    .line 621
    iget-object v0, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    .line 622
    if-nez v0, :cond_10

    .line 623
    aput v4, p4, v4

    .line 665
    :cond_f
    :goto_f
    return-object p0

    .line 627
    :cond_10
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/a/b/d/aez;->b(Ljava/util/Comparator;Ljava/lang/Object;I[I)Lcom/a/b/d/aez;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    .line 629
    aget v0, p4, v4

    if-lez v0, :cond_2c

    .line 630
    aget v0, p4, v4

    if-lt p3, v0, :cond_35

    .line 631
    iget v0, p0, Lcom/a/b/d/aez;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/b/d/aez;->c:I

    .line 632
    iget-wide v0, p0, Lcom/a/b/d/aez;->d:J

    aget v2, p4, v4

    int-to-long v2, v2

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/a/b/d/aez;->d:J

    .line 637
    :cond_2c
    :goto_2c
    aget v0, p4, v4

    if-eqz v0, :cond_f

    invoke-direct {p0}, Lcom/a/b/d/aez;->g()Lcom/a/b/d/aez;

    move-result-object p0

    goto :goto_f

    .line 634
    :cond_35
    iget-wide v0, p0, Lcom/a/b/d/aez;->d:J

    int-to-long v2, p3

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/a/b/d/aez;->d:J

    goto :goto_2c

    .line 638
    :cond_3c
    if-lez v0, :cond_6d

    .line 639
    iget-object v0, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    .line 640
    if-nez v0, :cond_45

    .line 641
    aput v4, p4, v4

    goto :goto_f

    .line 645
    :cond_45
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/a/b/d/aez;->b(Ljava/util/Comparator;Ljava/lang/Object;I[I)Lcom/a/b/d/aez;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    .line 647
    aget v0, p4, v4

    if-lez v0, :cond_61

    .line 648
    aget v0, p4, v4

    if-lt p3, v0, :cond_66

    .line 649
    iget v0, p0, Lcom/a/b/d/aez;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/b/d/aez;->c:I

    .line 650
    iget-wide v0, p0, Lcom/a/b/d/aez;->d:J

    aget v2, p4, v4

    int-to-long v2, v2

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/a/b/d/aez;->d:J

    .line 655
    :cond_61
    :goto_61
    invoke-direct {p0}, Lcom/a/b/d/aez;->g()Lcom/a/b/d/aez;

    move-result-object p0

    goto :goto_f

    .line 652
    :cond_66
    iget-wide v0, p0, Lcom/a/b/d/aez;->d:J

    int-to-long v2, p3

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/a/b/d/aez;->d:J

    goto :goto_61

    .line 659
    :cond_6d
    iget v0, p0, Lcom/a/b/d/aez;->b:I

    aput v0, p4, v4

    .line 660
    iget v0, p0, Lcom/a/b/d/aez;->b:I

    if-lt p3, v0, :cond_7a

    .line 661
    invoke-direct {p0}, Lcom/a/b/d/aez;->c()Lcom/a/b/d/aez;

    move-result-object p0

    goto :goto_f

    .line 663
    :cond_7a
    iget v0, p0, Lcom/a/b/d/aez;->b:I

    sub-int/2addr v0, p3

    iput v0, p0, Lcom/a/b/d/aez;->b:I

    .line 664
    iget-wide v0, p0, Lcom/a/b/d/aez;->d:J

    int-to-long v2, p3

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/a/b/d/aez;->d:J

    goto :goto_f
.end method

.method final c(Ljava/util/Comparator;Ljava/lang/Object;I[I)Lcom/a/b/d/aez;
    .registers 9
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 670
    iget-object v0, p0, Lcom/a/b/d/aez;->a:Ljava/lang/Object;

    invoke-interface {p1, p2, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 671
    if-gez v0, :cond_44

    .line 672
    iget-object v0, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    .line 673
    if-nez v0, :cond_16

    .line 674
    aput v2, p4, v2

    .line 675
    if-lez p3, :cond_15

    invoke-direct {p0, p2, p3}, Lcom/a/b/d/aez;->b(Ljava/lang/Object;I)Lcom/a/b/d/aez;

    move-result-object p0

    .line 714
    :cond_15
    :goto_15
    return-object p0

    .line 678
    :cond_16
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/a/b/d/aez;->c(Ljava/util/Comparator;Ljava/lang/Object;I[I)Lcom/a/b/d/aez;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/aez;->e:Lcom/a/b/d/aez;

    .line 680
    if-nez p3, :cond_37

    aget v0, p4, v2

    if-eqz v0, :cond_37

    .line 681
    iget v0, p0, Lcom/a/b/d/aez;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/b/d/aez;->c:I

    .line 686
    :cond_28
    :goto_28
    iget-wide v0, p0, Lcom/a/b/d/aez;->d:J

    aget v2, p4, v2

    sub-int v2, p3, v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/a/b/d/aez;->d:J

    .line 687
    invoke-direct {p0}, Lcom/a/b/d/aez;->g()Lcom/a/b/d/aez;

    move-result-object p0

    goto :goto_15

    .line 682
    :cond_37
    if-lez p3, :cond_28

    aget v0, p4, v2

    if-nez v0, :cond_28

    .line 683
    iget v0, p0, Lcom/a/b/d/aez;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/aez;->c:I

    goto :goto_28

    .line 688
    :cond_44
    if-lez v0, :cond_81

    .line 689
    iget-object v0, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    .line 690
    if-nez v0, :cond_53

    .line 691
    aput v2, p4, v2

    .line 692
    if-lez p3, :cond_15

    invoke-direct {p0, p2, p3}, Lcom/a/b/d/aez;->a(Ljava/lang/Object;I)Lcom/a/b/d/aez;

    move-result-object p0

    goto :goto_15

    .line 695
    :cond_53
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/a/b/d/aez;->c(Ljava/util/Comparator;Ljava/lang/Object;I[I)Lcom/a/b/d/aez;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/aez;->f:Lcom/a/b/d/aez;

    .line 697
    if-nez p3, :cond_74

    aget v0, p4, v2

    if-eqz v0, :cond_74

    .line 698
    iget v0, p0, Lcom/a/b/d/aez;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/b/d/aez;->c:I

    .line 703
    :cond_65
    :goto_65
    iget-wide v0, p0, Lcom/a/b/d/aez;->d:J

    aget v2, p4, v2

    sub-int v2, p3, v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/a/b/d/aez;->d:J

    .line 704
    invoke-direct {p0}, Lcom/a/b/d/aez;->g()Lcom/a/b/d/aez;

    move-result-object p0

    goto :goto_15

    .line 699
    :cond_74
    if-lez p3, :cond_65

    aget v0, p4, v2

    if-nez v0, :cond_65

    .line 700
    iget v0, p0, Lcom/a/b/d/aez;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/aez;->c:I

    goto :goto_65

    .line 708
    :cond_81
    iget v0, p0, Lcom/a/b/d/aez;->b:I

    aput v0, p4, v2

    .line 709
    if-nez p3, :cond_8c

    .line 710
    invoke-direct {p0}, Lcom/a/b/d/aez;->c()Lcom/a/b/d/aez;

    move-result-object p0

    goto :goto_15

    .line 712
    :cond_8c
    iget-wide v0, p0, Lcom/a/b/d/aez;->d:J

    iget v2, p0, Lcom/a/b/d/aez;->b:I

    sub-int v2, p3, v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/a/b/d/aez;->d:J

    .line 713
    iput p3, p0, Lcom/a/b/d/aez;->b:I

    goto/16 :goto_15
.end method

.method public final toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 933
    .line 1923
    iget-object v0, p0, Lcom/a/b/d/aez;->a:Ljava/lang/Object;

    .line 1928
    iget v1, p0, Lcom/a/b/d/aez;->b:I

    .line 933
    invoke-static {v0, v1}, Lcom/a/b/d/xe;->a(Ljava/lang/Object;I)Lcom/a/b/d/xd;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/xd;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
