.class final Lcom/a/b/d/aft;
.super Lcom/a/b/d/av;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/d;
.end annotation


# instance fields
.field private final a:Ljava/util/NavigableMap;

.field private final b:Lcom/a/b/d/yl;


# direct methods
.method constructor <init>(Ljava/util/NavigableMap;)V
    .registers 3

    .prologue
    .line 251
    invoke-direct {p0}, Lcom/a/b/d/av;-><init>()V

    .line 252
    iput-object p1, p0, Lcom/a/b/d/aft;->a:Ljava/util/NavigableMap;

    .line 253
    invoke-static {}, Lcom/a/b/d/yl;->c()Lcom/a/b/d/yl;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/aft;->b:Lcom/a/b/d/yl;

    .line 254
    return-void
.end method

.method private constructor <init>(Ljava/util/NavigableMap;Lcom/a/b/d/yl;)V
    .registers 3

    .prologue
    .line 257
    invoke-direct {p0}, Lcom/a/b/d/av;-><init>()V

    .line 258
    iput-object p1, p0, Lcom/a/b/d/aft;->a:Ljava/util/NavigableMap;

    .line 259
    iput-object p2, p0, Lcom/a/b/d/aft;->b:Lcom/a/b/d/yl;

    .line 260
    return-void
.end method

.method static synthetic a(Lcom/a/b/d/aft;)Lcom/a/b/d/yl;
    .registers 2

    .prologue
    .line 241
    iget-object v0, p0, Lcom/a/b/d/aft;->b:Lcom/a/b/d/yl;

    return-object v0
.end method

.method private a(Ljava/lang/Object;)Lcom/a/b/d/yl;
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 300
    instance-of v0, p1, Lcom/a/b/d/dw;

    if-eqz v0, :cond_31

    .line 303
    :try_start_5
    check-cast p1, Lcom/a/b/d/dw;

    .line 304
    iget-object v0, p0, Lcom/a/b/d/aft;->b:Lcom/a/b/d/yl;

    invoke-virtual {v0, p1}, Lcom/a/b/d/yl;->c(Ljava/lang/Comparable;)Z

    move-result v0

    if-nez v0, :cond_11

    move-object v0, v1

    .line 315
    :goto_10
    return-object v0

    .line 307
    :cond_11
    iget-object v0, p0, Lcom/a/b/d/aft;->a:Ljava/util/NavigableMap;

    invoke-interface {v0, p1}, Ljava/util/NavigableMap;->lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v2

    .line 308
    if-eqz v2, :cond_31

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    iget-object v0, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v0, p1}, Lcom/a/b/d/dw;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 309
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;
    :try_end_2d
    .catch Ljava/lang/ClassCastException; {:try_start_5 .. :try_end_2d} :catch_2e

    goto :goto_10

    .line 312
    :catch_2e
    move-exception v0

    move-object v0, v1

    goto :goto_10

    :cond_31
    move-object v0, v1

    .line 315
    goto :goto_10
.end method

.method private a(Lcom/a/b/d/dw;Z)Ljava/util/NavigableMap;
    .registers 4

    .prologue
    .line 280
    invoke-static {p2}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/a/b/d/yl;->a(Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/aft;->a(Lcom/a/b/d/yl;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/a/b/d/dw;ZLcom/a/b/d/dw;Z)Ljava/util/NavigableMap;
    .registers 7

    .prologue
    .line 273
    invoke-static {p2}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v0

    invoke-static {p4}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v1

    invoke-static {p1, v0, p3, v1}, Lcom/a/b/d/yl;->a(Ljava/lang/Comparable;Lcom/a/b/d/ce;Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/aft;->a(Lcom/a/b/d/yl;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/a/b/d/yl;)Ljava/util/NavigableMap;
    .registers 5

    .prologue
    .line 263
    iget-object v0, p0, Lcom/a/b/d/aft;->b:Lcom/a/b/d/yl;

    invoke-virtual {p1, v0}, Lcom/a/b/d/yl;->b(Lcom/a/b/d/yl;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 264
    new-instance v0, Lcom/a/b/d/aft;

    iget-object v1, p0, Lcom/a/b/d/aft;->a:Ljava/util/NavigableMap;

    iget-object v2, p0, Lcom/a/b/d/aft;->b:Lcom/a/b/d/yl;

    invoke-virtual {p1, v2}, Lcom/a/b/d/yl;->c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/aft;-><init>(Ljava/util/NavigableMap;Lcom/a/b/d/yl;)V

    .line 266
    :goto_15
    return-object v0

    :cond_16
    invoke-static {}, Lcom/a/b/d/lw;->m()Lcom/a/b/d/lw;

    move-result-object v0

    goto :goto_15
.end method

.method private b(Lcom/a/b/d/dw;Z)Ljava/util/NavigableMap;
    .registers 4

    .prologue
    .line 285
    invoke-static {p2}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/a/b/d/yl;->b(Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/aft;->a(Lcom/a/b/d/yl;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method final a()Ljava/util/Iterator;
    .registers 5

    .prologue
    const/4 v3, 0x1

    .line 325
    iget-object v0, p0, Lcom/a/b/d/aft;->b:Lcom/a/b/d/yl;

    invoke-virtual {v0}, Lcom/a/b/d/yl;->d()Z

    move-result v0

    if-nez v0, :cond_19

    .line 326
    iget-object v0, p0, Lcom/a/b/d/aft;->a:Ljava/util/NavigableMap;

    invoke-interface {v0}, Ljava/util/NavigableMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 339
    :goto_13
    new-instance v1, Lcom/a/b/d/afu;

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/afu;-><init>(Lcom/a/b/d/aft;Ljava/util/Iterator;)V

    return-object v1

    .line 328
    :cond_19
    iget-object v0, p0, Lcom/a/b/d/aft;->a:Ljava/util/NavigableMap;

    iget-object v1, p0, Lcom/a/b/d/aft;->b:Lcom/a/b/d/yl;

    .line 1383
    iget-object v1, v1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v1}, Lcom/a/b/d/dw;->c()Ljava/lang/Comparable;

    move-result-object v1

    .line 328
    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v1

    .line 330
    if-nez v1, :cond_34

    .line 331
    iget-object v0, p0, Lcom/a/b/d/aft;->a:Ljava/util/NavigableMap;

    invoke-interface {v0}, Ljava/util/NavigableMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_13

    .line 332
    :cond_34
    iget-object v0, p0, Lcom/a/b/d/aft;->b:Lcom/a/b/d/yl;

    iget-object v2, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    iget-object v0, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v2, v0}, Lcom/a/b/d/dw;->a(Ljava/lang/Comparable;)Z

    move-result v0

    if-eqz v0, :cond_59

    .line 333
    iget-object v0, p0, Lcom/a/b/d/aft;->a:Ljava/util/NavigableMap;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Ljava/util/NavigableMap;->tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_13

    .line 335
    :cond_59
    iget-object v0, p0, Lcom/a/b/d/aft;->a:Ljava/util/NavigableMap;

    iget-object v1, p0, Lcom/a/b/d/aft;->b:Lcom/a/b/d/yl;

    .line 2383
    iget-object v1, v1, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v1}, Lcom/a/b/d/dw;->c()Ljava/lang/Comparable;

    move-result-object v1

    .line 335
    invoke-interface {v0, v1, v3}, Ljava/util/NavigableMap;->tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_13
.end method

.method final b()Ljava/util/Iterator;
    .registers 4

    .prologue
    .line 358
    iget-object v0, p0, Lcom/a/b/d/aft;->b:Lcom/a/b/d/yl;

    invoke-virtual {v0}, Lcom/a/b/d/yl;->e()Z

    move-result v0

    if-eqz v0, :cond_48

    .line 359
    iget-object v0, p0, Lcom/a/b/d/aft;->a:Ljava/util/NavigableMap;

    iget-object v1, p0, Lcom/a/b/d/aft;->b:Lcom/a/b/d/yl;

    .line 2411
    iget-object v1, v1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v1}, Lcom/a/b/d/dw;->c()Ljava/lang/Comparable;

    move-result-object v1

    .line 359
    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Ljava/util/NavigableMap;->headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->descendingMap()Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->values()Ljava/util/Collection;

    move-result-object v0

    .line 364
    :goto_1f
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->j(Ljava/util/Iterator;)Lcom/a/b/d/yi;

    move-result-object v1

    .line 365
    invoke-interface {v1}, Lcom/a/b/d/yi;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_42

    iget-object v0, p0, Lcom/a/b/d/aft;->b:Lcom/a/b/d/yl;

    iget-object v2, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-interface {v1}, Lcom/a/b/d/yi;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    iget-object v0, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v2, v0}, Lcom/a/b/d/dw;->a(Ljava/lang/Comparable;)Z

    move-result v0

    if-eqz v0, :cond_42

    .line 367
    invoke-interface {v1}, Lcom/a/b/d/yi;->next()Ljava/lang/Object;

    .line 369
    :cond_42
    new-instance v0, Lcom/a/b/d/afv;

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/afv;-><init>(Lcom/a/b/d/aft;Lcom/a/b/d/yi;)V

    return-object v0

    .line 362
    :cond_48
    iget-object v0, p0, Lcom/a/b/d/aft;->a:Ljava/util/NavigableMap;

    invoke-interface {v0}, Ljava/util/NavigableMap;->descendingMap()Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->values()Ljava/util/Collection;

    move-result-object v0

    goto :goto_1f
.end method

.method public final comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 290
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    return-object v0
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 295
    invoke-direct {p0, p1}, Lcom/a/b/d/aft;->a(Ljava/lang/Object;)Lcom/a/b/d/yl;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 240
    invoke-direct {p0, p1}, Lcom/a/b/d/aft;->a(Ljava/lang/Object;)Lcom/a/b/d/yl;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 4

    .prologue
    .line 240
    check-cast p1, Lcom/a/b/d/dw;

    .line 4280
    invoke-static {p2}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/a/b/d/yl;->a(Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/aft;->a(Lcom/a/b/d/yl;)Ljava/util/NavigableMap;

    move-result-object v0

    .line 240
    return-object v0
.end method

.method public final isEmpty()Z
    .registers 3

    .prologue
    .line 393
    iget-object v0, p0, Lcom/a/b/d/aft;->b:Lcom/a/b/d/yl;

    invoke-static {}, Lcom/a/b/d/yl;->c()Lcom/a/b/d/yl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/d/yl;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/a/b/d/aft;->a:Ljava/util/NavigableMap;

    invoke-interface {v0}, Ljava/util/NavigableMap;->isEmpty()Z

    move-result v0

    :goto_12
    return v0

    :cond_13
    invoke-virtual {p0}, Lcom/a/b/d/aft;->a()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1f

    const/4 v0, 0x1

    goto :goto_12

    :cond_1f
    const/4 v0, 0x0

    goto :goto_12
.end method

.method public final size()I
    .registers 3

    .prologue
    .line 385
    iget-object v0, p0, Lcom/a/b/d/aft;->b:Lcom/a/b/d/yl;

    invoke-static {}, Lcom/a/b/d/yl;->c()Lcom/a/b/d/yl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/d/yl;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 386
    iget-object v0, p0, Lcom/a/b/d/aft;->a:Ljava/util/NavigableMap;

    invoke-interface {v0}, Ljava/util/NavigableMap;->size()I

    move-result v0

    .line 388
    :goto_12
    return v0

    :cond_13
    invoke-virtual {p0}, Lcom/a/b/d/aft;->a()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->b(Ljava/util/Iterator;)I

    move-result v0

    goto :goto_12
.end method

.method public final synthetic subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 7

    .prologue
    .line 240
    check-cast p1, Lcom/a/b/d/dw;

    check-cast p3, Lcom/a/b/d/dw;

    .line 5273
    invoke-static {p2}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v0

    invoke-static {p4}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v1

    invoke-static {p1, v0, p3, v1}, Lcom/a/b/d/yl;->a(Ljava/lang/Comparable;Lcom/a/b/d/ce;Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/aft;->a(Lcom/a/b/d/yl;)Ljava/util/NavigableMap;

    move-result-object v0

    .line 240
    return-object v0
.end method

.method public final synthetic tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 4

    .prologue
    .line 240
    check-cast p1, Lcom/a/b/d/dw;

    .line 3285
    invoke-static {p2}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/a/b/d/yl;->b(Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/aft;->a(Lcom/a/b/d/yl;)Ljava/util/NavigableMap;

    move-result-object v0

    .line 240
    return-object v0
.end method
