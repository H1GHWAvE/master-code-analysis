.class final Lcom/a/b/d/uf;
.super Lcom/a/b/d/ty;
.source "SourceFile"

# interfaces
.implements Ljava/util/SortedMap;


# direct methods
.method constructor <init>(Ljava/util/SortedMap;Lcom/a/b/b/co;)V
    .registers 3

    .prologue
    .line 2786
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/ty;-><init>(Ljava/util/Map;Lcom/a/b/b/co;)V

    .line 2787
    return-void
.end method

.method private c()Ljava/util/SortedMap;
    .registers 2

    .prologue
    .line 2790
    iget-object v0, p0, Lcom/a/b/d/uf;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    return-object v0
.end method

.method private d()Ljava/util/SortedSet;
    .registers 2

    .prologue
    .line 2794
    invoke-super {p0}, Lcom/a/b/d/ty;->keySet()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    return-object v0
.end method

.method private f()Ljava/util/SortedSet;
    .registers 2

    .prologue
    .line 2799
    new-instance v0, Lcom/a/b/d/ug;

    invoke-direct {v0, p0}, Lcom/a/b/d/ug;-><init>(Lcom/a/b/d/uf;)V

    return-object v0
.end method


# virtual methods
.method public final comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 2835
    .line 3790
    iget-object v0, p0, Lcom/a/b/d/uf;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    .line 2835
    invoke-interface {v0}, Ljava/util/SortedMap;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method final synthetic e()Ljava/util/Set;
    .registers 2

    .prologue
    .line 2781
    .line 8799
    new-instance v0, Lcom/a/b/d/ug;

    invoke-direct {v0, p0}, Lcom/a/b/d/ug;-><init>(Lcom/a/b/d/uf;)V

    .line 2781
    return-object v0
.end method

.method public final firstKey()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 2840
    .line 3794
    invoke-super {p0}, Lcom/a/b/d/ty;->keySet()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    .line 2840
    invoke-interface {v0}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 5

    .prologue
    .line 2856
    new-instance v1, Lcom/a/b/d/uf;

    .line 6790
    iget-object v0, p0, Lcom/a/b/d/uf;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    .line 2856
    invoke-interface {v0, p1}, Ljava/util/SortedMap;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/uf;->b:Lcom/a/b/b/co;

    invoke-direct {v1, v0, v2}, Lcom/a/b/d/uf;-><init>(Ljava/util/SortedMap;Lcom/a/b/b/co;)V

    return-object v1
.end method

.method public final bridge synthetic keySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 2781
    .line 9794
    invoke-super {p0}, Lcom/a/b/d/ty;->keySet()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    .line 2781
    return-object v0
.end method

.method public final lastKey()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 2844
    .line 4790
    iget-object v0, p0, Lcom/a/b/d/uf;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    .line 2847
    :goto_4
    invoke-interface {v0}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v1

    .line 2848
    iget-object v0, p0, Lcom/a/b/d/uf;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/a/b/d/uf;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 2849
    return-object v1

    .line 5790
    :cond_15
    iget-object v0, p0, Lcom/a/b/d/uf;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    .line 2851
    invoke-interface {v0, v1}, Ljava/util/SortedMap;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    goto :goto_4
.end method

.method public final subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 6

    .prologue
    .line 2860
    new-instance v1, Lcom/a/b/d/uf;

    .line 7790
    iget-object v0, p0, Lcom/a/b/d/uf;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    .line 2860
    invoke-interface {v0, p1, p2}, Ljava/util/SortedMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/uf;->b:Lcom/a/b/b/co;

    invoke-direct {v1, v0, v2}, Lcom/a/b/d/uf;-><init>(Ljava/util/SortedMap;Lcom/a/b/b/co;)V

    return-object v1
.end method

.method public final tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 5

    .prologue
    .line 2865
    new-instance v1, Lcom/a/b/d/uf;

    .line 8790
    iget-object v0, p0, Lcom/a/b/d/uf;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    .line 2865
    invoke-interface {v0, p1}, Ljava/util/SortedMap;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    iget-object v2, p0, Lcom/a/b/d/uf;->b:Lcom/a/b/b/co;

    invoke-direct {v1, v0, v2}, Lcom/a/b/d/uf;-><init>(Ljava/util/SortedMap;Lcom/a/b/b/co;)V

    return-object v1
.end method
