.class public final Lcom/a/b/d/sz;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# static fields
.field static final a:Lcom/a/b/b/bz;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 3458
    sget-object v0, Lcom/a/b/d/cm;->a:Lcom/a/b/b/bv;

    const-string v1, "="

    invoke-virtual {v0, v1}, Lcom/a/b/b/bv;->c(Ljava/lang/String;)Lcom/a/b/b/bz;

    move-result-object v0

    sput-object v0, Lcom/a/b/d/sz;->a:Lcom/a/b/b/bz;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/a/b/d/bw;)Lcom/a/b/b/ak;
    .registers 2
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 1315
    new-instance v0, Lcom/a/b/d/to;

    invoke-direct {v0, p0}, Lcom/a/b/d/to;-><init>(Lcom/a/b/d/bw;)V

    return-object v0
.end method

.method static a()Lcom/a/b/b/bj;
    .registers 1

    .prologue
    .line 104
    sget-object v0, Lcom/a/b/d/tr;->a:Lcom/a/b/d/tr;

    return-object v0
.end method

.method static a(Lcom/a/b/d/tv;)Lcom/a/b/b/bj;
    .registers 2

    .prologue
    .line 1833
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1834
    new-instance v0, Lcom/a/b/d/tk;

    invoke-direct {v0, p0}, Lcom/a/b/d/tk;-><init>(Lcom/a/b/d/tv;)V

    return-object v0
.end method

.method static a(Lcom/a/b/d/tv;Ljava/lang/Object;)Lcom/a/b/b/bj;
    .registers 3

    .prologue
    .line 1819
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1820
    new-instance v0, Lcom/a/b/d/tj;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/tj;-><init>(Lcom/a/b/d/tv;Ljava/lang/Object;)V

    return-object v0
.end method

.method static a(Lcom/a/b/b/co;)Lcom/a/b/b/co;
    .registers 2

    .prologue
    .line 2083
    .line 20104
    sget-object v0, Lcom/a/b/d/tr;->a:Lcom/a/b/d/tr;

    .line 2083
    invoke-static {p0, v0}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/bj;)Lcom/a/b/b/co;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/SortedMap;Ljava/util/Map;)Lcom/a/b/d/abm;
    .registers 9

    .prologue
    .line 595
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 596
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 597
    invoke-interface {p0}, Ljava/util/SortedMap;->comparator()Ljava/util/Comparator;

    move-result-object v0

    .line 12641
    if-eqz v0, :cond_2c

    .line 598
    :goto_c
    invoke-static {v0}, Lcom/a/b/d/sz;->a(Ljava/util/Comparator;)Ljava/util/TreeMap;

    move-result-object v3

    .line 599
    invoke-static {v0}, Lcom/a/b/d/sz;->a(Ljava/util/Comparator;)Ljava/util/TreeMap;

    move-result-object v4

    .line 600
    invoke-interface {v4, p1}, Ljava/util/SortedMap;->putAll(Ljava/util/Map;)V

    .line 601
    invoke-static {v0}, Lcom/a/b/d/sz;->a(Ljava/util/Comparator;)Ljava/util/TreeMap;

    move-result-object v5

    .line 602
    invoke-static {v0}, Lcom/a/b/d/sz;->a(Ljava/util/Comparator;)Ljava/util/TreeMap;

    move-result-object v6

    .line 13306
    sget-object v2, Lcom/a/b/b/aw;->a:Lcom/a/b/b/aw;

    move-object v0, p0

    move-object v1, p1

    .line 604
    invoke-static/range {v0 .. v6}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Ljava/util/Map;Lcom/a/b/b/au;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    .line 605
    new-instance v0, Lcom/a/b/d/uq;

    invoke-direct {v0, v3, v4, v5, v6}, Lcom/a/b/d/uq;-><init>(Ljava/util/SortedMap;Ljava/util/SortedMap;Ljava/util/SortedMap;Ljava/util/SortedMap;)V

    return-object v0

    .line 12644
    :cond_2c
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    goto :goto_c
.end method

.method static a(Lcom/a/b/d/agi;)Lcom/a/b/d/agi;
    .registers 2

    .prologue
    .line 122
    new-instance v0, Lcom/a/b/d/ta;

    invoke-direct {v0, p0}, Lcom/a/b/d/ta;-><init>(Lcom/a/b/d/agi;)V

    return-object v0
.end method

.method private static a(Lcom/a/b/d/bw;Lcom/a/b/b/co;)Lcom/a/b/d/bw;
    .registers 3

    .prologue
    .line 2235
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 26104
    sget-object v0, Lcom/a/b/d/tr;->a:Lcom/a/b/d/tr;

    .line 26083
    invoke-static {p1, v0}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/bj;)Lcom/a/b/b/co;

    move-result-object v0

    .line 2236
    invoke-static {p0, v0}, Lcom/a/b/d/sz;->c(Lcom/a/b/d/bw;Lcom/a/b/b/co;)Lcom/a/b/d/bw;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/a/b/d/tw;Lcom/a/b/b/co;)Lcom/a/b/d/bw;
    .registers 5

    .prologue
    .line 3024
    iget-object v0, p0, Lcom/a/b/d/tw;->b:Lcom/a/b/b/co;

    invoke-static {v0, p1}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v1

    .line 3025
    new-instance v2, Lcom/a/b/d/tw;

    .line 36058
    iget-object v0, p0, Lcom/a/b/d/tw;->a:Ljava/util/Map;

    check-cast v0, Lcom/a/b/d/bw;

    .line 3025
    invoke-direct {v2, v0, v1}, Lcom/a/b/d/tw;-><init>(Lcom/a/b/d/bw;Lcom/a/b/b/co;)V

    return-object v2
.end method

.method public static a(Ljava/lang/Iterable;Lcom/a/b/b/bj;)Lcom/a/b/d/jt;
    .registers 6
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 1097
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 15114
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 15243
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 15117
    :goto_c
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 15118
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 15119
    invoke-interface {p1, v2}, Lcom/a/b/b/bj;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_c

    .line 15121
    :cond_1e
    invoke-static {v1}, Lcom/a/b/d/jt;->a(Ljava/util/Map;)Lcom/a/b/d/jt;

    move-result-object v0

    .line 1097
    return-object v0
.end method

.method private static a(Ljava/util/Iterator;Lcom/a/b/b/bj;)Lcom/a/b/d/jt;
    .registers 5
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 1114
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 16243
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 1117
    :goto_8
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 1118
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 1119
    invoke-interface {p1, v1}, Lcom/a/b/b/bj;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_8

    .line 1121
    :cond_1a
    invoke-static {v0}, Lcom/a/b/d/jt;->a(Ljava/util/Map;)Lcom/a/b/d/jt;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/Properties;)Lcom/a/b/d/jt;
    .registers 5
    .annotation build Lcom/a/b/a/c;
        a = "java.util.Properties"
    .end annotation

    .prologue
    .line 1185
    invoke-static {}, Lcom/a/b/d/jt;->l()Lcom/a/b/d/ju;

    move-result-object v1

    .line 1187
    invoke-virtual {p0}, Ljava/util/Properties;->propertyNames()Ljava/util/Enumeration;

    move-result-object v2

    :goto_8
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 1188
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1189
    invoke-virtual {p0, v0}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    goto :goto_8

    .line 1192
    :cond_1c
    invoke-virtual {v1}, Lcom/a/b/d/ju;->a()Lcom/a/b/d/jt;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/Map;Ljava/util/Map;Lcom/a/b/b/au;)Lcom/a/b/d/qj;
    .registers 10
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 413
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 10177
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 416
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4, p1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 11177
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 12177
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    .line 419
    invoke-static/range {v0 .. v6}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Ljava/util/Map;Lcom/a/b/b/au;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    .line 420
    new-instance v0, Lcom/a/b/d/ul;

    invoke-direct {v0, v3, v4, v5, v6}, Lcom/a/b/d/ul;-><init>(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    return-object v0
.end method

.method public static a(Lcom/a/b/b/bj;)Lcom/a/b/d/tv;
    .registers 2

    .prologue
    .line 1808
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1809
    new-instance v0, Lcom/a/b/d/ti;

    invoke-direct {v0, p0}, Lcom/a/b/d/ti;-><init>(Lcom/a/b/b/bj;)V

    return-object v0
.end method

.method static a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 3351
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3353
    :try_start_4
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_7
    .catch Ljava/lang/ClassCastException; {:try_start_4 .. :try_end_7} :catch_9
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_7} :catch_b

    move-result-object v0

    .line 3357
    :goto_8
    return-object v0

    .line 3355
    :catch_9
    move-exception v1

    goto :goto_8

    .line 3357
    :catch_b
    move-exception v1

    goto :goto_8
.end method

.method static a(Ljava/util/Map;)Ljava/lang/String;
    .registers 4

    .prologue
    .line 3465
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-static {v0}, Lcom/a/b/d/cm;->a(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 3467
    sget-object v1, Lcom/a/b/d/sz;->a:Lcom/a/b/b/bz;

    .line 39322
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/a/b/b/bz;->a(Ljava/lang/StringBuilder;Ljava/lang/Iterable;)Ljava/lang/StringBuilder;

    .line 3468
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Class;)Ljava/util/EnumMap;
    .registers 3

    .prologue
    .line 337
    new-instance v1, Ljava/util/EnumMap;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-direct {v1, v0}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    return-object v1
.end method

.method public static a(I)Ljava/util/HashMap;
    .registers 3

    .prologue
    .line 195
    new-instance v0, Ljava/util/HashMap;

    invoke-static {p0}, Lcom/a/b/d/sz;->b(I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    return-object v0
.end method

.method static a(Ljava/util/Iterator;)Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 113
    .line 4104
    sget-object v0, Lcom/a/b/d/tr;->a:Lcom/a/b/d/tr;

    .line 113
    invoke-static {p0, v0}, Lcom/a/b/d/nj;->a(Ljava/util/Iterator;Lcom/a/b/b/bj;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/util/Set;Lcom/a/b/b/bj;)Ljava/util/Iterator;
    .registers 4

    .prologue
    .line 833
    new-instance v0, Lcom/a/b/d/td;

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/a/b/d/td;-><init>(Ljava/util/Iterator;Lcom/a/b/b/bj;)V

    return-object v0
.end method

.method static a(Lcom/a/b/d/tv;Ljava/util/Map$Entry;)Ljava/util/Map$Entry;
    .registers 3

    .prologue
    .line 1847
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1848
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1849
    new-instance v0, Lcom/a/b/d/tb;

    invoke-direct {v0, p1, p0}, Lcom/a/b/d/tb;-><init>(Ljava/util/Map$Entry;Lcom/a/b/d/tv;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;
    .registers 3
    .param p0    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/a/b/a/b;
        a = true
    .end annotation

    .prologue
    .line 1207
    new-instance v0, Lcom/a/b/d/jc;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/jc;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method static a(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;
    .registers 2

    .prologue
    .line 1234
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1235
    new-instance v0, Lcom/a/b/d/th;

    invoke-direct {v0, p0}, Lcom/a/b/d/th;-><init>(Ljava/util/Map$Entry;)V

    return-object v0
.end method

.method private static a(Lcom/a/b/d/tl;Lcom/a/b/b/co;)Ljava/util/Map;
    .registers 5

    .prologue
    .line 2555
    new-instance v0, Lcom/a/b/d/ty;

    iget-object v1, p0, Lcom/a/b/d/tl;->a:Ljava/util/Map;

    iget-object v2, p0, Lcom/a/b/d/tl;->b:Lcom/a/b/b/co;

    invoke-static {v2, p1}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/ty;-><init>(Ljava/util/Map;Lcom/a/b/b/co;)V

    return-object v0
.end method

.method public static a(Ljava/util/Map;Lcom/a/b/b/bj;)Ljava/util/Map;
    .registers 3

    .prologue
    .line 1494
    invoke-static {p1}, Lcom/a/b/d/sz;->a(Lcom/a/b/b/bj;)Lcom/a/b/d/tv;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Lcom/a/b/d/tv;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/Map;Lcom/a/b/b/co;)Ljava/util/Map;
    .registers 5

    .prologue
    .line 2120
    instance-of v0, p0, Ljava/util/SortedMap;

    if-eqz v0, :cond_11

    .line 2121
    check-cast p0, Ljava/util/SortedMap;

    .line 21104
    sget-object v0, Lcom/a/b/d/tr;->a:Lcom/a/b/d/tr;

    .line 21083
    invoke-static {p1, v0}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/bj;)Lcom/a/b/b/co;

    move-result-object v0

    .line 20167
    invoke-static {p0, v0}, Lcom/a/b/d/sz;->d(Ljava/util/SortedMap;Lcom/a/b/b/co;)Ljava/util/SortedMap;

    move-result-object v0

    .line 2127
    :goto_10
    return-object v0

    .line 2122
    :cond_11
    instance-of v0, p0, Lcom/a/b/d/bw;

    if-eqz v0, :cond_25

    .line 2123
    check-cast p0, Lcom/a/b/d/bw;

    .line 21235
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 22104
    sget-object v0, Lcom/a/b/d/tr;->a:Lcom/a/b/d/tr;

    .line 22083
    invoke-static {p1, v0}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/bj;)Lcom/a/b/b/co;

    move-result-object v0

    .line 21236
    invoke-static {p0, v0}, Lcom/a/b/d/sz;->c(Lcom/a/b/d/bw;Lcom/a/b/b/co;)Lcom/a/b/d/bw;

    move-result-object v0

    goto :goto_10

    .line 2125
    :cond_25
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 23104
    sget-object v0, Lcom/a/b/d/tr;->a:Lcom/a/b/d/tr;

    .line 23083
    invoke-static {p1, v0}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/bj;)Lcom/a/b/b/co;

    move-result-object v2

    .line 2127
    instance-of v0, p0, Lcom/a/b/d/tl;

    if-eqz v0, :cond_39

    check-cast p0, Lcom/a/b/d/tl;

    invoke-static {p0, v2}, Lcom/a/b/d/sz;->a(Lcom/a/b/d/tl;Lcom/a/b/b/co;)Ljava/util/Map;

    move-result-object v0

    goto :goto_10

    :cond_39
    new-instance v1, Lcom/a/b/d/uh;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-direct {v1, v0, p1, v2}, Lcom/a/b/d/uh;-><init>(Ljava/util/Map;Lcom/a/b/b/co;Lcom/a/b/b/co;)V

    move-object v0, v1

    goto :goto_10
.end method

.method public static a(Ljava/util/Map;Lcom/a/b/d/tv;)Ljava/util/Map;
    .registers 3

    .prologue
    .line 1642
    instance-of v0, p0, Ljava/util/SortedMap;

    if-eqz v0, :cond_b

    .line 1643
    check-cast p0, Ljava/util/SortedMap;

    invoke-static {p0, p1}, Lcom/a/b/d/sz;->a(Ljava/util/SortedMap;Lcom/a/b/d/tv;)Ljava/util/SortedMap;

    move-result-object v0

    .line 1645
    :goto_a
    return-object v0

    :cond_b
    new-instance v0, Lcom/a/b/d/ur;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/ur;-><init>(Ljava/util/Map;Lcom/a/b/d/tv;)V

    goto :goto_a
.end method

.method private static a(Lcom/a/b/d/ud;Lcom/a/b/b/co;)Ljava/util/NavigableMap;
    .registers 5
    .annotation build Lcom/a/b/a/c;
        a = "NavigableMap"
    .end annotation

    .prologue
    .line 2878
    invoke-static {p0}, Lcom/a/b/d/ud;->a(Lcom/a/b/d/ud;)Lcom/a/b/b/co;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v0

    .line 2880
    new-instance v1, Lcom/a/b/d/ud;

    invoke-static {p0}, Lcom/a/b/d/ud;->b(Lcom/a/b/d/ud;)Ljava/util/NavigableMap;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/a/b/d/ud;-><init>(Ljava/util/NavigableMap;Lcom/a/b/b/co;)V

    return-object v1
.end method

.method public static a(Ljava/util/NavigableMap;)Ljava/util/NavigableMap;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "NavigableMap"
    .end annotation

    .prologue
    .line 3092
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3093
    instance-of v0, p0, Lcom/a/b/d/uz;

    if-eqz v0, :cond_8

    .line 3096
    :goto_7
    return-object p0

    :cond_8
    new-instance v0, Lcom/a/b/d/uz;

    invoke-direct {v0, p0}, Lcom/a/b/d/uz;-><init>(Ljava/util/NavigableMap;)V

    move-object p0, v0

    goto :goto_7
.end method

.method private static a(Ljava/util/NavigableMap;Lcom/a/b/b/bj;)Ljava/util/NavigableMap;
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "NavigableMap"
    .end annotation

    .prologue
    .line 1585
    invoke-static {p1}, Lcom/a/b/d/sz;->a(Lcom/a/b/b/bj;)Lcom/a/b/d/tv;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/a/b/d/sz;->a(Ljava/util/NavigableMap;Lcom/a/b/d/tv;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/NavigableMap;Lcom/a/b/b/co;)Ljava/util/NavigableMap;
    .registers 5
    .annotation build Lcom/a/b/a/c;
        a = "NavigableMap"
    .end annotation

    .prologue
    .line 2507
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2508
    instance-of v0, p0, Lcom/a/b/d/ud;

    if-eqz v0, :cond_1b

    check-cast p0, Lcom/a/b/d/ud;

    .line 34878
    invoke-static {p0}, Lcom/a/b/d/ud;->a(Lcom/a/b/d/ud;)Lcom/a/b/b/co;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v1

    .line 34880
    new-instance v0, Lcom/a/b/d/ud;

    invoke-static {p0}, Lcom/a/b/d/ud;->b(Lcom/a/b/d/ud;)Ljava/util/NavigableMap;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Lcom/a/b/d/ud;-><init>(Ljava/util/NavigableMap;Lcom/a/b/b/co;)V

    .line 2508
    :goto_1a
    return-object v0

    :cond_1b
    new-instance v1, Lcom/a/b/d/ud;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/NavigableMap;

    invoke-direct {v1, v0, p1}, Lcom/a/b/d/ud;-><init>(Ljava/util/NavigableMap;Lcom/a/b/b/co;)V

    move-object v0, v1

    goto :goto_1a
.end method

.method public static a(Ljava/util/NavigableMap;Lcom/a/b/d/tv;)Ljava/util/NavigableMap;
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "NavigableMap"
    .end annotation

    .prologue
    .line 1763
    new-instance v0, Lcom/a/b/d/ut;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/ut;-><init>(Ljava/util/NavigableMap;Lcom/a/b/d/tv;)V

    return-object v0
.end method

.method public static a(Ljava/util/NavigableSet;Lcom/a/b/b/bj;)Ljava/util/NavigableMap;
    .registers 3
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .annotation build Lcom/a/b/a/c;
        a = "NavigableMap"
    .end annotation

    .prologue
    .line 751
    new-instance v0, Lcom/a/b/d/um;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/um;-><init>(Ljava/util/NavigableSet;Lcom/a/b/b/bj;)V

    return-object v0
.end method

.method static synthetic a(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 40024
    new-instance v0, Lcom/a/b/d/tg;

    invoke-direct {v0, p0}, Lcom/a/b/d/tg;-><init>(Ljava/util/NavigableSet;)V

    .line 82
    return-object v0
.end method

.method static a(Ljava/util/Set;)Ljava/util/Set;
    .registers 3

    .prologue
    .line 1220
    new-instance v0, Lcom/a/b/d/uy;

    invoke-static {p0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/uy;-><init>(Ljava/util/Set;)V

    return-object v0
.end method

.method private static a(Lcom/a/b/d/uf;Lcom/a/b/b/co;)Ljava/util/SortedMap;
    .registers 5

    .prologue
    .line 2776
    iget-object v0, p0, Lcom/a/b/d/uf;->b:Lcom/a/b/b/co;

    invoke-static {v0, p1}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v1

    .line 2778
    new-instance v2, Lcom/a/b/d/uf;

    .line 35790
    iget-object v0, p0, Lcom/a/b/d/uf;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    .line 2778
    invoke-direct {v2, v0, v1}, Lcom/a/b/d/uf;-><init>(Ljava/util/SortedMap;Lcom/a/b/b/co;)V

    return-object v2
.end method

.method public static a(Ljava/util/SortedMap;Lcom/a/b/b/bj;)Ljava/util/SortedMap;
    .registers 3

    .prologue
    .line 1538
    invoke-static {p1}, Lcom/a/b/d/sz;->a(Lcom/a/b/b/bj;)Lcom/a/b/d/tv;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/a/b/d/sz;->a(Ljava/util/SortedMap;Lcom/a/b/d/tv;)Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/util/SortedMap;Lcom/a/b/b/co;)Ljava/util/SortedMap;
    .registers 5

    .prologue
    .line 2466
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2467
    instance-of v0, p0, Lcom/a/b/d/uf;

    if-eqz v0, :cond_1a

    check-cast p0, Lcom/a/b/d/uf;

    .line 34776
    iget-object v0, p0, Lcom/a/b/d/uf;->b:Lcom/a/b/b/co;

    invoke-static {v0, p1}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v2

    .line 34778
    new-instance v1, Lcom/a/b/d/uf;

    .line 34790
    iget-object v0, p0, Lcom/a/b/d/uf;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    .line 34778
    invoke-direct {v1, v0, v2}, Lcom/a/b/d/uf;-><init>(Ljava/util/SortedMap;Lcom/a/b/b/co;)V

    move-object v0, v1

    .line 2467
    :goto_19
    return-object v0

    :cond_1a
    new-instance v1, Lcom/a/b/d/uf;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/SortedMap;

    invoke-direct {v1, v0, p1}, Lcom/a/b/d/uf;-><init>(Ljava/util/SortedMap;Lcom/a/b/b/co;)V

    move-object v0, v1

    goto :goto_19
.end method

.method public static a(Ljava/util/SortedMap;Lcom/a/b/d/tv;)Ljava/util/SortedMap;
    .registers 3

    .prologue
    .line 1703
    .line 19074
    instance-of v0, p0, Ljava/util/NavigableMap;

    if-eqz v0, :cond_b

    check-cast p0, Ljava/util/NavigableMap;

    invoke-static {p0, p1}, Lcom/a/b/d/sz;->a(Ljava/util/NavigableMap;Lcom/a/b/d/tv;)Ljava/util/NavigableMap;

    move-result-object v0

    :goto_a
    return-object v0

    .line 19769
    :cond_b
    new-instance v0, Lcom/a/b/d/uu;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/uu;-><init>(Ljava/util/SortedMap;Lcom/a/b/d/tv;)V

    goto :goto_a
.end method

.method public static a(Ljava/util/SortedSet;Lcom/a/b/b/bj;)Ljava/util/SortedMap;
    .registers 3
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 713
    .line 14081
    instance-of v0, p0, Ljava/util/NavigableSet;

    if-eqz v0, :cond_b

    check-cast p0, Ljava/util/NavigableSet;

    invoke-static {p0, p1}, Lcom/a/b/d/sz;->a(Ljava/util/NavigableSet;Lcom/a/b/b/bj;)Ljava/util/NavigableMap;

    move-result-object v0

    :goto_a
    return-object v0

    .line 14718
    :cond_b
    new-instance v0, Lcom/a/b/d/uo;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/uo;-><init>(Ljava/util/SortedSet;Lcom/a/b/b/bj;)V

    goto :goto_a
.end method

.method static synthetic a(Ljava/util/SortedSet;)Ljava/util/SortedSet;
    .registers 2

    .prologue
    .line 39989
    new-instance v0, Lcom/a/b/d/tf;

    invoke-direct {v0, p0}, Lcom/a/b/d/tf;-><init>(Ljava/util/SortedSet;)V

    .line 82
    return-object v0
.end method

.method private static a(Ljava/util/Comparator;)Ljava/util/TreeMap;
    .registers 2
    .param p0    # Ljava/util/Comparator;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 327
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0, p0}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    return-object v0
.end method

.method private static a(Ljava/util/SortedMap;)Ljava/util/TreeMap;
    .registers 2

    .prologue
    .line 307
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0, p0}, Ljava/util/TreeMap;-><init>(Ljava/util/SortedMap;)V

    return-object v0
.end method

.method static a(Ljava/util/Map;Ljava/util/Map;)V
    .registers 5

    .prologue
    .line 3476
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 3477
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p0, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_8

    .line 3479
    :cond_20
    return-void
.end method

.method private static a(Ljava/util/Map;Ljava/util/Map;Lcom/a/b/b/au;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V
    .registers 12

    .prologue
    .line 428
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 429
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    .line 430
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 431
    invoke-interface {p1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_39

    .line 432
    invoke-interface {p4, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 433
    invoke-virtual {p2, v0, v3}, Lcom/a/b/b/au;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_30

    .line 434
    invoke-interface {p5, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_8

    .line 12537
    :cond_30
    new-instance v4, Lcom/a/b/d/va;

    invoke-direct {v4, v0, v3}, Lcom/a/b/d/va;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 436
    invoke-interface {p6, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_8

    .line 440
    :cond_39
    invoke-interface {p3, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_8

    .line 443
    :cond_3d
    return-void
.end method

.method static a(Ljava/util/Collection;Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 3419
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-nez v0, :cond_6

    .line 3420
    const/4 v0, 0x0

    .line 3422
    :goto_5
    return v0

    :cond_6
    check-cast p1, Ljava/util/Map$Entry;

    invoke-static {p1}, Lcom/a/b/d/sz;->a(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_5
.end method

.method static b(I)I
    .registers 2

    .prologue
    .line 204
    const/4 v0, 0x3

    if-ge p0, v0, :cond_b

    .line 205
    const-string v0, "expectedSize"

    invoke-static {p0, v0}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 206
    add-int/lit8 v0, p0, 0x1

    .line 211
    :goto_a
    return v0

    .line 208
    :cond_b
    const/high16 v0, 0x40000000    # 2.0f

    if-ge p0, v0, :cond_13

    .line 209
    div-int/lit8 v0, p0, 0x3

    add-int/2addr v0, p0

    goto :goto_a

    .line 211
    :cond_13
    const v0, 0x7fffffff

    goto :goto_a
.end method

.method static b()Lcom/a/b/b/bj;
    .registers 1

    .prologue
    .line 109
    sget-object v0, Lcom/a/b/d/tr;->b:Lcom/a/b/d/tr;

    return-object v0
.end method

.method static b(Lcom/a/b/d/tv;)Lcom/a/b/b/bj;
    .registers 2

    .prologue
    .line 1867
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1868
    new-instance v0, Lcom/a/b/d/tc;

    invoke-direct {v0, p0}, Lcom/a/b/d/tc;-><init>(Lcom/a/b/d/tv;)V

    return-object v0
.end method

.method static b(Lcom/a/b/b/co;)Lcom/a/b/b/co;
    .registers 2

    .prologue
    .line 2087
    .line 20109
    sget-object v0, Lcom/a/b/d/tr;->b:Lcom/a/b/d/tr;

    .line 2087
    invoke-static {p0, v0}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/bj;)Lcom/a/b/b/co;

    move-result-object v0

    return-object v0
.end method

.method private static b(Lcom/a/b/d/bw;)Lcom/a/b/d/bw;
    .registers 2

    .prologue
    .line 1393
    .line 18104
    instance-of v0, p0, Lcom/a/b/d/adc;

    if-nez v0, :cond_8

    instance-of v0, p0, Lcom/a/b/d/it;

    if-eqz v0, :cond_9

    .line 18106
    :cond_8
    :goto_8
    return-object p0

    .line 18108
    :cond_9
    new-instance v0, Lcom/a/b/d/adc;

    invoke-direct {v0, p0}, Lcom/a/b/d/adc;-><init>(Lcom/a/b/d/bw;)V

    move-object p0, v0

    .line 1393
    goto :goto_8
.end method

.method private static b(Lcom/a/b/d/bw;Lcom/a/b/b/co;)Lcom/a/b/d/bw;
    .registers 3

    .prologue
    .line 2381
    .line 32109
    sget-object v0, Lcom/a/b/d/tr;->b:Lcom/a/b/d/tr;

    .line 32087
    invoke-static {p1, v0}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/bj;)Lcom/a/b/b/co;

    move-result-object v0

    .line 2381
    invoke-static {p0, v0}, Lcom/a/b/d/sz;->c(Lcom/a/b/d/bw;Lcom/a/b/b/co;)Lcom/a/b/d/bw;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/Iterable;Lcom/a/b/b/bj;)Lcom/a/b/d/jt;
    .registers 6

    .prologue
    .line 1140
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 17160
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 17161
    invoke-static {}, Lcom/a/b/d/jt;->l()Lcom/a/b/d/ju;

    move-result-object v1

    .line 17162
    :goto_b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 17163
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 17164
    invoke-interface {p1, v2}, Lcom/a/b/b/bj;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v3, v2}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    goto :goto_b

    .line 17166
    :cond_1d
    invoke-virtual {v1}, Lcom/a/b/d/ju;->a()Lcom/a/b/d/jt;

    move-result-object v0

    .line 1140
    return-object v0
.end method

.method private static b(Ljava/util/Iterator;Lcom/a/b/b/bj;)Lcom/a/b/d/jt;
    .registers 5

    .prologue
    .line 1160
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1161
    invoke-static {}, Lcom/a/b/d/jt;->l()Lcom/a/b/d/ju;

    move-result-object v0

    .line 1162
    :goto_7
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_19

    .line 1163
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 1164
    invoke-interface {p1, v1}, Lcom/a/b/b/bj;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    goto :goto_7

    .line 1166
    :cond_19
    invoke-virtual {v0}, Lcom/a/b/d/ju;->a()Lcom/a/b/d/jt;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/util/Map;Ljava/util/Map;)Lcom/a/b/d/qj;
    .registers 9

    .prologue
    .line 382
    instance-of v0, p0, Ljava/util/SortedMap;

    if-eqz v0, :cond_37

    move-object v0, p0

    .line 383
    check-cast v0, Ljava/util/SortedMap;

    .line 4595
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4596
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4597
    invoke-interface {v0}, Ljava/util/SortedMap;->comparator()Ljava/util/Comparator;

    move-result-object v1

    .line 4641
    if-eqz v1, :cond_32

    .line 4598
    :goto_13
    invoke-static {v1}, Lcom/a/b/d/sz;->a(Ljava/util/Comparator;)Ljava/util/TreeMap;

    move-result-object v3

    .line 4599
    invoke-static {v1}, Lcom/a/b/d/sz;->a(Ljava/util/Comparator;)Ljava/util/TreeMap;

    move-result-object v4

    .line 4600
    invoke-interface {v4, p1}, Ljava/util/SortedMap;->putAll(Ljava/util/Map;)V

    .line 4601
    invoke-static {v1}, Lcom/a/b/d/sz;->a(Ljava/util/Comparator;)Ljava/util/TreeMap;

    move-result-object v5

    .line 4602
    invoke-static {v1}, Lcom/a/b/d/sz;->a(Ljava/util/Comparator;)Ljava/util/TreeMap;

    move-result-object v6

    .line 5306
    sget-object v2, Lcom/a/b/b/aw;->a:Lcom/a/b/b/aw;

    move-object v1, p1

    .line 4604
    invoke-static/range {v0 .. v6}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Ljava/util/Map;Lcom/a/b/b/au;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    .line 4605
    new-instance v0, Lcom/a/b/d/uq;

    invoke-direct {v0, v3, v4, v5, v6}, Lcom/a/b/d/uq;-><init>(Ljava/util/SortedMap;Ljava/util/SortedMap;Ljava/util/SortedMap;Ljava/util/SortedMap;)V

    .line 387
    :goto_31
    return-object v0

    .line 4644
    :cond_32
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v1

    goto :goto_13

    .line 6306
    :cond_37
    sget-object v2, Lcom/a/b/b/aw;->a:Lcom/a/b/b/aw;

    .line 6413
    invoke-static {v2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7177
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 6416
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4, p1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 8177
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 9177
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    move-object v0, p0

    move-object v1, p1

    .line 6419
    invoke-static/range {v0 .. v6}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Ljava/util/Map;Lcom/a/b/b/au;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    .line 6420
    new-instance v0, Lcom/a/b/d/ul;

    invoke-direct {v0, v3, v4, v5, v6}, Lcom/a/b/d/ul;-><init>(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    goto :goto_31
.end method

.method static b(Ljava/util/Map$Entry;)Ljava/lang/Object;
    .registers 2
    .param p0    # Ljava/util/Map$Entry;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 3523
    if-nez p0, :cond_4

    const/4 v0, 0x0

    :goto_3
    return-object v0

    :cond_4
    invoke-interface {p0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    goto :goto_3
.end method

.method private static b(Ljava/util/Comparator;)Ljava/util/Comparator;
    .registers 1
    .param p0    # Ljava/util/Comparator;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 641
    if-eqz p0, :cond_3

    .line 644
    :goto_2
    return-object p0

    :cond_3
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object p0

    goto :goto_2
.end method

.method static b(Ljava/util/Iterator;)Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 117
    .line 4109
    sget-object v0, Lcom/a/b/d/tr;->b:Lcom/a/b/d/tr;

    .line 117
    invoke-static {p0, v0}, Lcom/a/b/d/nj;->a(Ljava/util/Iterator;Lcom/a/b/b/bj;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Ljava/util/Map;)Ljava/util/Map;
    .registers 2

    .prologue
    .line 82
    .line 39446
    instance-of v0, p0, Ljava/util/SortedMap;

    if-eqz v0, :cond_b

    .line 39447
    check-cast p0, Ljava/util/SortedMap;

    invoke-static {p0}, Ljava/util/Collections;->unmodifiableSortedMap(Ljava/util/SortedMap;)Ljava/util/SortedMap;

    move-result-object v0

    :goto_a
    return-object v0

    .line 39449
    :cond_b
    invoke-static {p0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    goto :goto_a
.end method

.method private static b(Ljava/util/Map;Lcom/a/b/b/co;)Ljava/util/Map;
    .registers 5

    .prologue
    .line 2270
    instance-of v0, p0, Ljava/util/SortedMap;

    if-eqz v0, :cond_11

    .line 2271
    check-cast p0, Ljava/util/SortedMap;

    .line 27109
    sget-object v0, Lcom/a/b/d/tr;->b:Lcom/a/b/d/tr;

    .line 27087
    invoke-static {p1, v0}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/bj;)Lcom/a/b/b/co;

    move-result-object v0

    .line 26311
    invoke-static {p0, v0}, Lcom/a/b/d/sz;->d(Ljava/util/SortedMap;Lcom/a/b/b/co;)Ljava/util/SortedMap;

    move-result-object v0

    .line 29421
    :goto_10
    return-object v0

    .line 2272
    :cond_11
    instance-of v0, p0, Lcom/a/b/d/bw;

    if-eqz v0, :cond_22

    .line 2273
    check-cast p0, Lcom/a/b/d/bw;

    .line 28109
    sget-object v0, Lcom/a/b/d/tr;->b:Lcom/a/b/d/tr;

    .line 28087
    invoke-static {p1, v0}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/bj;)Lcom/a/b/b/co;

    move-result-object v0

    .line 27381
    invoke-static {p0, v0}, Lcom/a/b/d/sz;->c(Lcom/a/b/d/bw;Lcom/a/b/b/co;)Lcom/a/b/d/bw;

    move-result-object v0

    goto :goto_10

    .line 29109
    :cond_22
    sget-object v0, Lcom/a/b/d/tr;->b:Lcom/a/b/d/tr;

    .line 29087
    invoke-static {p1, v0}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/bj;)Lcom/a/b/b/co;

    move-result-object v2

    .line 29415
    instance-of v0, p0, Ljava/util/SortedMap;

    if-eqz v0, :cond_33

    .line 29416
    check-cast p0, Ljava/util/SortedMap;

    invoke-static {p0, v2}, Lcom/a/b/d/sz;->d(Ljava/util/SortedMap;Lcom/a/b/b/co;)Ljava/util/SortedMap;

    move-result-object v0

    goto :goto_10

    .line 29417
    :cond_33
    instance-of v0, p0, Lcom/a/b/d/bw;

    if-eqz v0, :cond_3e

    .line 29418
    check-cast p0, Lcom/a/b/d/bw;

    invoke-static {p0, v2}, Lcom/a/b/d/sz;->c(Lcom/a/b/d/bw;Lcom/a/b/b/co;)Lcom/a/b/d/bw;

    move-result-object v0

    goto :goto_10

    .line 29420
    :cond_3e
    invoke-static {v2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 29421
    instance-of v0, p0, Lcom/a/b/d/tl;

    if-eqz v0, :cond_4c

    check-cast p0, Lcom/a/b/d/tl;

    invoke-static {p0, v2}, Lcom/a/b/d/sz;->a(Lcom/a/b/d/tl;Lcom/a/b/b/co;)Ljava/util/Map;

    move-result-object v0

    goto :goto_10

    :cond_4c
    new-instance v1, Lcom/a/b/d/ty;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-direct {v1, v0, v2}, Lcom/a/b/d/ty;-><init>(Ljava/util/Map;Lcom/a/b/b/co;)V

    move-object v0, v1

    .line 2275
    goto :goto_10
.end method

.method private static b(Ljava/util/Set;Lcom/a/b/b/bj;)Ljava/util/Map;
    .registers 3
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 677
    instance-of v0, p0, Ljava/util/SortedSet;

    if-eqz v0, :cond_b

    .line 678
    check-cast p0, Ljava/util/SortedSet;

    invoke-static {p0, p1}, Lcom/a/b/d/sz;->a(Ljava/util/SortedSet;Lcom/a/b/b/bj;)Ljava/util/SortedMap;

    move-result-object v0

    .line 680
    :goto_a
    return-object v0

    :cond_b
    new-instance v0, Lcom/a/b/d/tm;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/tm;-><init>(Ljava/util/Set;Lcom/a/b/b/bj;)V

    goto :goto_a
.end method

.method private static b(Ljava/util/NavigableMap;)Ljava/util/NavigableMap;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "NavigableMap"
    .end annotation

    .prologue
    .line 3297
    .line 36341
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/a/b/d/acu;->a(Ljava/util/NavigableMap;Ljava/lang/Object;)Ljava/util/NavigableMap;

    move-result-object v0

    .line 3297
    return-object v0
.end method

.method private static b(Ljava/util/NavigableMap;Lcom/a/b/b/co;)Ljava/util/NavigableMap;
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "NavigableMap"
    .end annotation

    .prologue
    .line 2205
    .line 25104
    sget-object v0, Lcom/a/b/d/tr;->a:Lcom/a/b/d/tr;

    .line 25083
    invoke-static {p1, v0}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/bj;)Lcom/a/b/b/co;

    move-result-object v0

    .line 2205
    invoke-static {p0, v0}, Lcom/a/b/d/sz;->a(Ljava/util/NavigableMap;Lcom/a/b/b/co;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/util/NavigableSet;)Ljava/util/NavigableSet;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 1024
    new-instance v0, Lcom/a/b/d/tg;

    invoke-direct {v0, p0}, Lcom/a/b/d/tg;-><init>(Ljava/util/NavigableSet;)V

    return-object v0
.end method

.method static synthetic b(Ljava/util/Set;)Ljava/util/Set;
    .registers 2

    .prologue
    .line 39970
    new-instance v0, Lcom/a/b/d/te;

    invoke-direct {v0, p0}, Lcom/a/b/d/te;-><init>(Ljava/util/Set;)V

    .line 82
    return-object v0
.end method

.method private static b(Ljava/util/SortedMap;Lcom/a/b/b/co;)Ljava/util/SortedMap;
    .registers 3

    .prologue
    .line 2167
    .line 24104
    sget-object v0, Lcom/a/b/d/tr;->a:Lcom/a/b/d/tr;

    .line 24083
    invoke-static {p1, v0}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/bj;)Lcom/a/b/b/co;

    move-result-object v0

    .line 2167
    invoke-static {p0, v0}, Lcom/a/b/d/sz;->d(Ljava/util/SortedMap;Lcom/a/b/b/co;)Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method

.method static b(Ljava/util/SortedMap;Lcom/a/b/d/tv;)Ljava/util/SortedMap;
    .registers 3

    .prologue
    .line 1769
    new-instance v0, Lcom/a/b/d/uu;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/uu;-><init>(Ljava/util/SortedMap;Lcom/a/b/d/tv;)V

    return-object v0
.end method

.method static b(Ljava/util/SortedSet;Lcom/a/b/b/bj;)Ljava/util/SortedMap;
    .registers 3

    .prologue
    .line 718
    new-instance v0, Lcom/a/b/d/uo;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/uo;-><init>(Ljava/util/SortedSet;Lcom/a/b/b/bj;)V

    return-object v0
.end method

.method private static b(Ljava/util/SortedSet;)Ljava/util/SortedSet;
    .registers 2

    .prologue
    .line 989
    new-instance v0, Lcom/a/b/d/tf;

    invoke-direct {v0, p0}, Lcom/a/b/d/tf;-><init>(Ljava/util/SortedSet;)V

    return-object v0
.end method

.method static b(Ljava/util/Collection;Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 3439
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-nez v0, :cond_6

    .line 3440
    const/4 v0, 0x0

    .line 3442
    :goto_5
    return v0

    :cond_6
    check-cast p1, Ljava/util/Map$Entry;

    invoke-static {p1}, Lcom/a/b/d/sz;->a(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_5
.end method

.method static b(Ljava/util/Map;Ljava/lang/Object;)Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 3366
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3368
    :try_start_4
    invoke-interface {p0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_7
    .catch Ljava/lang/ClassCastException; {:try_start_4 .. :try_end_7} :catch_9
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_7} :catch_b

    move-result v0

    .line 3372
    :goto_8
    return v0

    .line 3370
    :catch_9
    move-exception v1

    goto :goto_8

    .line 3372
    :catch_b
    move-exception v1

    goto :goto_8
.end method

.method private static c(Lcom/a/b/d/bw;)Lcom/a/b/d/bw;
    .registers 3

    .prologue
    .line 1411
    new-instance v0, Lcom/a/b/d/uv;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/d/uv;-><init>(Lcom/a/b/d/bw;Lcom/a/b/d/bw;)V

    return-object v0
.end method

.method private static c(Lcom/a/b/d/bw;Lcom/a/b/b/co;)Lcom/a/b/d/bw;
    .registers 5

    .prologue
    .line 2542
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2543
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2544
    instance-of v0, p0, Lcom/a/b/d/tw;

    if-eqz v0, :cond_1d

    check-cast p0, Lcom/a/b/d/tw;

    .line 35024
    iget-object v0, p0, Lcom/a/b/d/tw;->b:Lcom/a/b/b/co;

    invoke-static {v0, p1}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v2

    .line 35025
    new-instance v1, Lcom/a/b/d/tw;

    .line 35058
    iget-object v0, p0, Lcom/a/b/d/tw;->a:Ljava/util/Map;

    check-cast v0, Lcom/a/b/d/bw;

    .line 35025
    invoke-direct {v1, v0, v2}, Lcom/a/b/d/tw;-><init>(Lcom/a/b/d/bw;Lcom/a/b/b/co;)V

    move-object v0, v1

    .line 2544
    :goto_1c
    return-object v0

    :cond_1d
    new-instance v0, Lcom/a/b/d/tw;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/tw;-><init>(Lcom/a/b/d/bw;Lcom/a/b/b/co;)V

    goto :goto_1c
.end method

.method private static c(Ljava/util/Map;)Lcom/a/b/d/jt;
    .registers 4
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .annotation build Lcom/a/b/a/b;
        a = true
    .end annotation

    .prologue
    .line 150
    instance-of v0, p0, Lcom/a/b/d/jd;

    if-eqz v0, :cond_7

    .line 152
    check-cast p0, Lcom/a/b/d/jd;

    .line 161
    :goto_6
    return-object p0

    .line 154
    :cond_7
    invoke-interface {p0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 155
    invoke-static {}, Lcom/a/b/d/jt;->k()Lcom/a/b/d/jt;

    move-result-object p0

    goto :goto_6

    .line 157
    :cond_12
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_35

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 158
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1a

    .line 161
    :cond_35
    new-instance v0, Ljava/util/EnumMap;

    invoke-direct {v0, p0}, Ljava/util/EnumMap;-><init>(Ljava/util/Map;)V

    invoke-static {v0}, Lcom/a/b/d/jd;->a(Ljava/util/EnumMap;)Lcom/a/b/d/jt;

    move-result-object p0

    goto :goto_6
.end method

.method static c(Ljava/util/Map$Entry;)Ljava/lang/Object;
    .registers 2
    .param p0    # Ljava/util/Map$Entry;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 3528
    if-nez p0, :cond_4

    const/4 v0, 0x0

    :goto_3
    return-object v0

    :cond_4
    invoke-interface {p0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    goto :goto_3
.end method

.method static c(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 3381
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3383
    :try_start_4
    invoke-interface {p0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_7
    .catch Ljava/lang/ClassCastException; {:try_start_4 .. :try_end_7} :catch_9
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_7} :catch_b

    move-result-object v0

    .line 3387
    :goto_8
    return-object v0

    .line 3385
    :catch_9
    move-exception v1

    goto :goto_8

    .line 3387
    :catch_b
    move-exception v1

    goto :goto_8
.end method

.method public static c()Ljava/util/HashMap;
    .registers 1

    .prologue
    .line 177
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    return-object v0
.end method

.method private static c(Ljava/util/Map;Lcom/a/b/b/co;)Ljava/util/Map;
    .registers 4

    .prologue
    .line 2415
    instance-of v0, p0, Ljava/util/SortedMap;

    if-eqz v0, :cond_b

    .line 2416
    check-cast p0, Ljava/util/SortedMap;

    invoke-static {p0, p1}, Lcom/a/b/d/sz;->d(Ljava/util/SortedMap;Lcom/a/b/b/co;)Ljava/util/SortedMap;

    move-result-object v0

    .line 2421
    :goto_a
    return-object v0

    .line 2417
    :cond_b
    instance-of v0, p0, Lcom/a/b/d/bw;

    if-eqz v0, :cond_16

    .line 2418
    check-cast p0, Lcom/a/b/d/bw;

    invoke-static {p0, p1}, Lcom/a/b/d/sz;->c(Lcom/a/b/d/bw;Lcom/a/b/b/co;)Lcom/a/b/d/bw;

    move-result-object v0

    goto :goto_a

    .line 2420
    :cond_16
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2421
    instance-of v0, p0, Lcom/a/b/d/tl;

    if-eqz v0, :cond_24

    check-cast p0, Lcom/a/b/d/tl;

    invoke-static {p0, p1}, Lcom/a/b/d/sz;->a(Lcom/a/b/d/tl;Lcom/a/b/b/co;)Ljava/util/Map;

    move-result-object v0

    goto :goto_a

    :cond_24
    new-instance v1, Lcom/a/b/d/ty;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-direct {v1, v0, p1}, Lcom/a/b/d/ty;-><init>(Ljava/util/Map;Lcom/a/b/b/co;)V

    move-object v0, v1

    goto :goto_a
.end method

.method private static c(Ljava/util/NavigableMap;Lcom/a/b/b/co;)Ljava/util/NavigableMap;
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "NavigableMap"
    .end annotation

    .prologue
    .line 2348
    .line 31109
    sget-object v0, Lcom/a/b/d/tr;->b:Lcom/a/b/d/tr;

    .line 31087
    invoke-static {p1, v0}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/bj;)Lcom/a/b/b/co;

    move-result-object v0

    .line 2348
    invoke-static {p0, v0}, Lcom/a/b/d/sz;->a(Ljava/util/NavigableMap;Lcom/a/b/b/co;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method private static c(Ljava/util/Set;)Ljava/util/Set;
    .registers 2

    .prologue
    .line 970
    new-instance v0, Lcom/a/b/d/te;

    invoke-direct {v0, p0}, Lcom/a/b/d/te;-><init>(Ljava/util/Set;)V

    return-object v0
.end method

.method private static c(Ljava/util/SortedMap;Lcom/a/b/b/co;)Ljava/util/SortedMap;
    .registers 3

    .prologue
    .line 2311
    .line 30109
    sget-object v0, Lcom/a/b/d/tr;->b:Lcom/a/b/d/tr;

    .line 30087
    invoke-static {p1, v0}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/bj;)Lcom/a/b/b/co;

    move-result-object v0

    .line 2311
    invoke-static {p0, v0}, Lcom/a/b/d/sz;->d(Ljava/util/SortedMap;Lcom/a/b/b/co;)Ljava/util/SortedMap;

    move-result-object v0

    return-object v0
.end method

.method private static d(Ljava/util/Map;)Ljava/util/HashMap;
    .registers 2

    .prologue
    .line 230
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method public static d()Ljava/util/LinkedHashMap;
    .registers 1

    .prologue
    .line 243
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    return-object v0
.end method

.method static synthetic d(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;
    .registers 2

    .prologue
    .line 82
    .line 40101
    if-nez p0, :cond_4

    const/4 v0, 0x0

    :goto_3
    return-object v0

    :cond_4
    invoke-static {p0}, Lcom/a/b/d/sz;->a(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;

    move-result-object v0

    goto :goto_3
.end method

.method private static d(Ljava/util/SortedMap;Lcom/a/b/b/co;)Ljava/util/SortedMap;
    .registers 5

    .prologue
    .line 2460
    .line 33095
    instance-of v0, p0, Ljava/util/NavigableMap;

    if-eqz v0, :cond_b

    check-cast p0, Ljava/util/NavigableMap;

    invoke-static {p0, p1}, Lcom/a/b/d/sz;->a(Ljava/util/NavigableMap;Lcom/a/b/b/co;)Ljava/util/NavigableMap;

    move-result-object v0

    .line 33467
    :goto_a
    return-object v0

    .line 33466
    :cond_b
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 33467
    instance-of v0, p0, Lcom/a/b/d/uf;

    if-eqz v0, :cond_25

    check-cast p0, Lcom/a/b/d/uf;

    .line 33776
    iget-object v0, p0, Lcom/a/b/d/uf;->b:Lcom/a/b/b/co;

    invoke-static {v0, p1}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v2

    .line 33778
    new-instance v1, Lcom/a/b/d/uf;

    .line 33790
    iget-object v0, p0, Lcom/a/b/d/uf;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    .line 33778
    invoke-direct {v1, v0, v2}, Lcom/a/b/d/uf;-><init>(Ljava/util/SortedMap;Lcom/a/b/b/co;)V

    move-object v0, v1

    .line 33467
    goto :goto_a

    :cond_25
    new-instance v1, Lcom/a/b/d/uf;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/SortedMap;

    invoke-direct {v1, v0, p1}, Lcom/a/b/d/uf;-><init>(Ljava/util/SortedMap;Lcom/a/b/b/co;)V

    move-object v0, v1

    .line 2460
    goto :goto_a
.end method

.method static d(Ljava/util/Map;Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 3395
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 38104
    sget-object v1, Lcom/a/b/d/tr;->a:Lcom/a/b/d/tr;

    .line 37113
    invoke-static {v0, v1}, Lcom/a/b/d/nj;->a(Ljava/util/Iterator;Lcom/a/b/b/bj;)Ljava/util/Iterator;

    move-result-object v0

    .line 3395
    invoke-static {v0, p1}, Lcom/a/b/d/nj;->a(Ljava/util/Iterator;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static e(Ljava/util/Map;)Ljava/util/LinkedHashMap;
    .registers 2

    .prologue
    .line 259
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0, p0}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method private static e(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;
    .registers 2
    .param p0    # Ljava/util/Map$Entry;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 3101
    if-nez p0, :cond_4

    const/4 v0, 0x0

    :goto_3
    return-object v0

    :cond_4
    invoke-static {p0}, Lcom/a/b/d/sz;->a(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;

    move-result-object v0

    goto :goto_3
.end method

.method public static e()Ljava/util/TreeMap;
    .registers 1

    .prologue
    .line 291
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    return-object v0
.end method

.method static e(Ljava/util/Map;Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 3402
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 39109
    sget-object v1, Lcom/a/b/d/tr;->b:Lcom/a/b/d/tr;

    .line 38117
    invoke-static {v0, v1}, Lcom/a/b/d/nj;->a(Ljava/util/Iterator;Lcom/a/b/b/bj;)Ljava/util/Iterator;

    move-result-object v0

    .line 3402
    invoke-static {v0, p1}, Lcom/a/b/d/nj;->a(Ljava/util/Iterator;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static f(Ljava/util/Map;)Ljava/util/EnumMap;
    .registers 2

    .prologue
    .line 351
    new-instance v0, Ljava/util/EnumMap;

    invoke-direct {v0, p0}, Ljava/util/EnumMap;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method public static f()Ljava/util/IdentityHashMap;
    .registers 1

    .prologue
    .line 360
    new-instance v0, Ljava/util/IdentityHashMap;

    invoke-direct {v0}, Ljava/util/IdentityHashMap;-><init>()V

    return-object v0
.end method

.method static f(Ljava/util/Map;Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 3449
    if-ne p0, p1, :cond_4

    .line 3450
    const/4 v0, 0x1

    .line 3455
    :goto_3
    return v0

    .line 3451
    :cond_4
    instance-of v0, p1, Ljava/util/Map;

    if-eqz v0, :cond_17

    .line 3452
    check-cast p1, Ljava/util/Map;

    .line 3453
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_3

    .line 3455
    :cond_17
    const/4 v0, 0x0

    goto :goto_3
.end method

.method private static g(Ljava/util/Map;)Ljava/util/Map;
    .registers 2

    .prologue
    .line 446
    instance-of v0, p0, Ljava/util/SortedMap;

    if-eqz v0, :cond_b

    .line 447
    check-cast p0, Ljava/util/SortedMap;

    invoke-static {p0}, Ljava/util/Collections;->unmodifiableSortedMap(Ljava/util/SortedMap;)Ljava/util/SortedMap;

    move-result-object v0

    .line 449
    :goto_a
    return-object v0

    :cond_b
    invoke-static {p0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    goto :goto_a
.end method

.method private static g()Ljava/util/concurrent/ConcurrentMap;
    .registers 1

    .prologue
    .line 278
    new-instance v0, Lcom/a/b/d/ql;

    invoke-direct {v0}, Lcom/a/b/d/ql;-><init>()V

    invoke-virtual {v0}, Lcom/a/b/d/ql;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    return-object v0
.end method
