.class public abstract Lcom/a/b/d/lo;
.super Lcom/a/b/d/iz;
.source "SourceFile"

# interfaces
.implements Ljava/util/Set;


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
    b = true
.end annotation


# static fields
.field private static final a:D = 0.7

.field static final b:I = 0x40000000

.field private static final c:I = 0x2ccccccc


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 383
    invoke-direct {p0}, Lcom/a/b/d/iz;-><init>()V

    return-void
.end method

.method private static a(I)I
    .registers 7
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    const/high16 v1, 0x40000000    # 2.0f

    .line 249
    const v0, 0x2ccccccc

    if-ge p0, v0, :cond_1e

    .line 251
    add-int/lit8 v0, p0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->highestOneBit(I)I

    move-result v0

    shl-int/lit8 v0, v0, 0x1

    .line 252
    :goto_f
    int-to-double v2, v0

    const-wide v4, 0x3fe6666666666666L    # 0.7

    mul-double/2addr v2, v4

    int-to-double v4, p0

    cmpg-double v1, v2, v4

    if-gez v1, :cond_27

    .line 253
    shl-int/lit8 v0, v0, 0x1

    goto :goto_f

    .line 259
    :cond_1e
    if-ge p0, v1, :cond_28

    const/4 v0, 0x1

    :goto_21
    const-string v2, "collection too large"

    invoke-static {v0, v2}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    move v0, v1

    .line 260
    :cond_27
    return v0

    .line 259
    :cond_28
    const/4 v0, 0x0

    goto :goto_21
.end method

.method static synthetic a(I[Ljava/lang/Object;)Lcom/a/b/d/lo;
    .registers 3

    .prologue
    .line 74
    invoke-static {p0, p1}, Lcom/a/b/d/lo;->b(I[Ljava/lang/Object;)Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Iterable;)Lcom/a/b/d/lo;
    .registers 4

    .prologue
    .line 300
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_b

    check-cast p0, Ljava/util/Collection;

    invoke-static {p0}, Lcom/a/b/d/lo;->a(Ljava/util/Collection;)Lcom/a/b/d/lo;

    move-result-object v0

    .line 2319
    :goto_a
    return-object v0

    .line 300
    :cond_b
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 2314
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_18

    .line 3084
    sget-object v0, Lcom/a/b/d/ey;->a:Lcom/a/b/d/ey;

    goto :goto_a

    .line 2317
    :cond_18
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 2318
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_27

    .line 2319
    invoke-static {v1}, Lcom/a/b/d/lo;->d(Ljava/lang/Object;)Lcom/a/b/d/lo;

    move-result-object v0

    goto :goto_a

    .line 2321
    :cond_27
    new-instance v2, Lcom/a/b/d/lp;

    invoke-direct {v2}, Lcom/a/b/d/lp;-><init>()V

    invoke-virtual {v2, v1}, Lcom/a/b/d/lp;->c(Ljava/lang/Object;)Lcom/a/b/d/lp;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/a/b/d/lp;->b(Ljava/util/Iterator;)Lcom/a/b/d/lp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lp;->b()Lcom/a/b/d/lo;

    move-result-object v0

    goto :goto_a
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/lo;
    .registers 6

    .prologue
    const/4 v2, 0x3

    .line 116
    new-array v0, v2, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    invoke-static {v2, v0}, Lcom/a/b/d/lo;->b(I[Ljava/lang/Object;)Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/lo;
    .registers 7

    .prologue
    const/4 v2, 0x4

    .line 127
    new-array v0, v2, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    invoke-static {v2, v0}, Lcom/a/b/d/lo;->b(I[Ljava/lang/Object;)Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/lo;
    .registers 8

    .prologue
    const/4 v2, 0x5

    .line 138
    new-array v0, v2, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    const/4 v1, 0x4

    aput-object p4, v0, v1

    invoke-static {v2, v0}, Lcom/a/b/d/lo;->b(I[Ljava/lang/Object;)Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method private static varargs a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Lcom/a/b/d/lo;
    .registers 11

    .prologue
    const/4 v3, 0x0

    .line 152
    array-length v0, p6

    add-int/lit8 v0, v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    .line 153
    aput-object p0, v0, v3

    .line 154
    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 155
    const/4 v1, 0x2

    aput-object p2, v0, v1

    .line 156
    const/4 v1, 0x3

    aput-object p3, v0, v1

    .line 157
    const/4 v1, 0x4

    aput-object p4, v0, v1

    .line 158
    const/4 v1, 0x5

    aput-object p5, v0, v1

    .line 159
    const/4 v1, 0x6

    array-length v2, p6

    invoke-static {p6, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 160
    array-length v1, v0

    invoke-static {v1, v0}, Lcom/a/b/d/lo;->b(I[Ljava/lang/Object;)Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/Collection;)Lcom/a/b/d/lo;
    .registers 3

    .prologue
    .line 364
    instance-of v0, p0, Lcom/a/b/d/lo;

    if-eqz v0, :cond_12

    instance-of v0, p0, Lcom/a/b/d/me;

    if-nez v0, :cond_12

    move-object v0, p0

    .line 367
    check-cast v0, Lcom/a/b/d/lo;

    .line 368
    invoke-virtual {v0}, Lcom/a/b/d/lo;->h_()Z

    move-result v1

    if-nez v1, :cond_21

    .line 375
    :goto_11
    return-object v0

    .line 371
    :cond_12
    instance-of v0, p0, Ljava/util/EnumSet;

    if-eqz v0, :cond_21

    .line 372
    check-cast p0, Ljava/util/EnumSet;

    .line 4380
    invoke-static {p0}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/ji;->a(Ljava/util/EnumSet;)Lcom/a/b/d/lo;

    move-result-object v0

    goto :goto_11

    .line 374
    :cond_21
    invoke-interface {p0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v0

    .line 375
    array-length v1, v0

    invoke-static {v1, v0}, Lcom/a/b/d/lo;->b(I[Ljava/lang/Object;)Lcom/a/b/d/lo;

    move-result-object v0

    goto :goto_11
.end method

.method private static a(Ljava/util/EnumSet;)Lcom/a/b/d/lo;
    .registers 2

    .prologue
    .line 380
    invoke-static {p0}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/ji;->a(Ljava/util/EnumSet;)Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/Iterator;)Lcom/a/b/d/lo;
    .registers 3

    .prologue
    .line 314
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_9

    .line 4084
    sget-object v0, Lcom/a/b/d/ey;->a:Lcom/a/b/d/ey;

    .line 321
    :goto_8
    return-object v0

    .line 317
    :cond_9
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 318
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_18

    .line 319
    invoke-static {v0}, Lcom/a/b/d/lo;->d(Ljava/lang/Object;)Lcom/a/b/d/lo;

    move-result-object v0

    goto :goto_8

    .line 321
    :cond_18
    new-instance v1, Lcom/a/b/d/lp;

    invoke-direct {v1}, Lcom/a/b/d/lp;-><init>()V

    invoke-virtual {v1, v0}, Lcom/a/b/d/lp;->c(Ljava/lang/Object;)Lcom/a/b/d/lp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/a/b/d/lp;->b(Ljava/util/Iterator;)Lcom/a/b/d/lp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lp;->b()Lcom/a/b/d/lo;

    move-result-object v0

    goto :goto_8
.end method

.method public static a([Ljava/lang/Object;)Lcom/a/b/d/lo;
    .registers 3

    .prologue
    .line 272
    array-length v0, p0

    packed-switch v0, :pswitch_data_1c

    .line 278
    array-length v1, p0

    invoke-virtual {p0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v1, v0}, Lcom/a/b/d/lo;->b(I[Ljava/lang/Object;)Lcom/a/b/d/lo;

    move-result-object v0

    :goto_f
    return-object v0

    .line 2084
    :pswitch_10
    sget-object v0, Lcom/a/b/d/ey;->a:Lcom/a/b/d/ey;

    goto :goto_f

    .line 276
    :pswitch_13
    const/4 v0, 0x0

    aget-object v0, p0, v0

    invoke-static {v0}, Lcom/a/b/d/lo;->d(Ljava/lang/Object;)Lcom/a/b/d/lo;

    move-result-object v0

    goto :goto_f

    .line 272
    nop

    :pswitch_data_1c
    .packed-switch 0x0
        :pswitch_10
        :pswitch_13
    .end packed-switch
.end method

.method private static varargs b(I[Ljava/lang/Object;)Lcom/a/b/d/lo;
    .registers 14

    .prologue
    const/4 v4, 0x0

    .line 179
    :goto_1
    packed-switch p0, :pswitch_data_6e

    .line 189
    invoke-static {p0}, Lcom/a/b/d/lo;->a(I)I

    move-result v5

    .line 190
    new-array v6, v5, [Ljava/lang/Object;

    .line 191
    add-int/lit8 v7, v5, -0x1

    move v3, v4

    move v1, v4

    move v2, v4

    .line 194
    :goto_f
    if-ge v3, p0, :cond_46

    .line 195
    aget-object v0, p1, v3

    invoke-static {v0, v3}, Lcom/a/b/d/yc;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v8

    .line 196
    invoke-virtual {v8}, Ljava/lang/Object;->hashCode()I

    move-result v9

    .line 197
    invoke-static {v9}, Lcom/a/b/d/iq;->a(I)I

    move-result v0

    .line 198
    :goto_1f
    and-int v10, v0, v7

    .line 199
    aget-object v11, v6, v10

    .line 200
    if-nez v11, :cond_3d

    .line 202
    add-int/lit8 v0, v1, 0x1

    aput-object v8, p1, v1

    .line 203
    aput-object v8, v6, v10

    .line 204
    add-int v1, v2, v9

    .line 194
    :goto_2d
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    move v1, v0

    goto :goto_f

    .line 1084
    :pswitch_33
    sget-object v0, Lcom/a/b/d/ey;->a:Lcom/a/b/d/ey;

    .line 225
    :goto_35
    return-object v0

    .line 184
    :pswitch_36
    aget-object v0, p1, v4

    .line 185
    invoke-static {v0}, Lcom/a/b/d/lo;->d(Ljava/lang/Object;)Lcom/a/b/d/lo;

    move-result-object v0

    goto :goto_35

    .line 206
    :cond_3d
    invoke-virtual {v11, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_6a

    .line 197
    add-int/lit8 v0, v0, 0x1

    goto :goto_1f

    .line 211
    :cond_46
    const/4 v0, 0x0

    invoke-static {p1, v1, p0, v0}, Ljava/util/Arrays;->fill([Ljava/lang/Object;IILjava/lang/Object;)V

    .line 212
    const/4 v0, 0x1

    if-ne v1, v0, :cond_55

    .line 215
    aget-object v1, p1, v4

    .line 216
    new-instance v0, Lcom/a/b/d/aaw;

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/aaw;-><init>(Ljava/lang/Object;I)V

    goto :goto_35

    .line 217
    :cond_55
    invoke-static {v1}, Lcom/a/b/d/lo;->a(I)I

    move-result v0

    if-eq v5, v0, :cond_5d

    move p0, v1

    .line 220
    goto :goto_1

    .line 222
    :cond_5d
    array-length v0, p1

    if-ge v1, v0, :cond_64

    invoke-static {p1, v1}, Lcom/a/b/d/yc;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    .line 225
    :cond_64
    new-instance v0, Lcom/a/b/d/zk;

    invoke-direct {v0, p1, v2, v6, v7}, Lcom/a/b/d/zk;-><init>([Ljava/lang/Object;I[Ljava/lang/Object;I)V

    goto :goto_35

    :cond_6a
    move v0, v1

    move v1, v2

    goto :goto_2d

    .line 179
    nop

    :pswitch_data_6e
    .packed-switch 0x0
        :pswitch_33
        :pswitch_36
    .end packed-switch
.end method

.method public static b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/lo;
    .registers 5

    .prologue
    const/4 v2, 0x2

    .line 105
    new-array v0, v2, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    invoke-static {v2, v0}, Lcom/a/b/d/lo;->b(I[Ljava/lang/Object;)Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method public static d(Ljava/lang/Object;)Lcom/a/b/d/lo;
    .registers 2

    .prologue
    .line 94
    new-instance v0, Lcom/a/b/d/aaw;

    invoke-direct {v0, p0}, Lcom/a/b/d/aaw;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static h()Lcom/a/b/d/lo;
    .registers 1

    .prologue
    .line 84
    sget-object v0, Lcom/a/b/d/ey;->a:Lcom/a/b/d/ey;

    return-object v0
.end method

.method public static i()Lcom/a/b/d/lp;
    .registers 1

    .prologue
    .line 437
    new-instance v0, Lcom/a/b/d/lp;

    invoke-direct {v0}, Lcom/a/b/d/lp;-><init>()V

    return-object v0
.end method


# virtual methods
.method public abstract c()Lcom/a/b/d/agi;
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 391
    if-ne p1, p0, :cond_4

    .line 392
    const/4 v0, 0x1

    .line 399
    :goto_3
    return v0

    .line 393
    :cond_4
    instance-of v0, p1, Lcom/a/b/d/lo;

    if-eqz v0, :cond_23

    invoke-virtual {p0}, Lcom/a/b/d/lo;->g_()Z

    move-result v0

    if-eqz v0, :cond_23

    move-object v0, p1

    check-cast v0, Lcom/a/b/d/lo;

    invoke-virtual {v0}, Lcom/a/b/d/lo;->g_()Z

    move-result v0

    if-eqz v0, :cond_23

    invoke-virtual {p0}, Lcom/a/b/d/lo;->hashCode()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    if-eq v0, v1, :cond_23

    .line 397
    const/4 v0, 0x0

    goto :goto_3

    .line 399
    :cond_23
    invoke-static {p0, p1}, Lcom/a/b/d/aad;->a(Ljava/util/Set;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_3
.end method

.method g()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 429
    new-instance v0, Lcom/a/b/d/lq;

    invoke-virtual {p0}, Lcom/a/b/d/lo;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/lq;-><init>([Ljava/lang/Object;)V

    return-object v0
.end method

.method g_()Z
    .registers 2

    .prologue
    .line 387
    const/4 v0, 0x0

    return v0
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 403
    invoke-static {p0}, Lcom/a/b/d/aad;->a(Ljava/util/Set;)I

    move-result v0

    return v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/a/b/d/lo;->c()Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method
