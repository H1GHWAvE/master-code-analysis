.class public abstract Lcom/a/b/d/j;
.super Lcom/a/b/d/agi;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/a/b/d/agi;-><init>()V

    .line 65
    sget v0, Lcom/a/b/d/l;->b:I

    iput v0, p0, Lcom/a/b/d/j;->a:I

    .line 68
    return-void
.end method

.method private c()Z
    .registers 3

    .prologue
    .line 142
    sget v0, Lcom/a/b/d/l;->d:I

    iput v0, p0, Lcom/a/b/d/j;->a:I

    .line 143
    invoke-virtual {p0}, Lcom/a/b/d/j;->a()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/j;->b:Ljava/lang/Object;

    .line 144
    iget v0, p0, Lcom/a/b/d/j;->a:I

    sget v1, Lcom/a/b/d/l;->c:I

    if-eq v0, v1, :cond_16

    .line 145
    sget v0, Lcom/a/b/d/l;->a:I

    iput v0, p0, Lcom/a/b/d/j;->a:I

    .line 146
    const/4 v0, 0x1

    .line 148
    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method private d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/a/b/d/j;->hasNext()Z

    move-result v0

    if-nez v0, :cond_c

    .line 171
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 173
    :cond_c
    iget-object v0, p0, Lcom/a/b/d/j;->b:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public abstract a()Ljava/lang/Object;
.end method

.method public final b()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 124
    sget v0, Lcom/a/b/d/l;->c:I

    iput v0, p0, Lcom/a/b/d/j;->a:I

    .line 125
    const/4 v0, 0x0

    return-object v0
.end method

.method public final hasNext()Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 130
    iget v0, p0, Lcom/a/b/d/j;->a:I

    sget v3, Lcom/a/b/d/l;->d:I

    if-eq v0, v3, :cond_2d

    move v0, v1

    :goto_9
    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    .line 131
    sget-object v0, Lcom/a/b/d/k;->a:[I

    iget v3, p0, Lcom/a/b/d/j;->a:I

    add-int/lit8 v3, v3, -0x1

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_32

    .line 1142
    sget v0, Lcom/a/b/d/l;->d:I

    iput v0, p0, Lcom/a/b/d/j;->a:I

    .line 1143
    invoke-virtual {p0}, Lcom/a/b/d/j;->a()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/j;->b:Ljava/lang/Object;

    .line 1144
    iget v0, p0, Lcom/a/b/d/j;->a:I

    sget v3, Lcom/a/b/d/l;->c:I

    if-eq v0, v3, :cond_2c

    .line 1145
    sget v0, Lcom/a/b/d/l;->a:I

    iput v0, p0, Lcom/a/b/d/j;->a:I

    move v2, v1

    .line 1146
    :cond_2c
    :goto_2c
    :pswitch_2c
    return v2

    :cond_2d
    move v0, v2

    .line 130
    goto :goto_9

    :pswitch_2f
    move v2, v1

    .line 135
    goto :goto_2c

    .line 131
    nop

    :pswitch_data_32
    .packed-switch 0x1
        :pswitch_2c
        :pswitch_2f
    .end packed-switch
.end method

.method public final next()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/a/b/d/j;->hasNext()Z

    move-result v0

    if-nez v0, :cond_c

    .line 154
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 156
    :cond_c
    sget v0, Lcom/a/b/d/l;->b:I

    iput v0, p0, Lcom/a/b/d/j;->a:I

    .line 157
    iget-object v0, p0, Lcom/a/b/d/j;->b:Ljava/lang/Object;

    .line 158
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/a/b/d/j;->b:Ljava/lang/Object;

    .line 159
    return-object v0
.end method
