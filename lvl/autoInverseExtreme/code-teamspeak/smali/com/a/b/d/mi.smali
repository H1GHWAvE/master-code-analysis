.class public abstract Lcom/a/b/d/mi;
.super Lcom/a/b/d/bf;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field private static final a:Lcom/a/b/d/mi;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    .line 49
    new-instance v0, Lcom/a/b/d/abt;

    invoke-static {}, Lcom/a/b/d/jl;->d()Lcom/a/b/d/jl;

    move-result-object v1

    invoke-static {}, Lcom/a/b/d/lo;->h()Lcom/a/b/d/lo;

    move-result-object v2

    invoke-static {}, Lcom/a/b/d/lo;->h()Lcom/a/b/d/lo;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/a/b/d/abt;-><init>(Lcom/a/b/d/jl;Lcom/a/b/d/lo;Lcom/a/b/d/lo;)V

    sput-object v0, Lcom/a/b/d/mi;->a:Lcom/a/b/d/mi;

    return-void
.end method

.method constructor <init>()V
    .registers 1

    .prologue
    .line 250
    invoke-direct {p0}, Lcom/a/b/d/bf;-><init>()V

    return-void
.end method

.method static b(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/adw;
    .registers 6

    .prologue
    .line 127
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/a/b/d/adx;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/adw;

    move-result-object v0

    return-object v0
.end method

.method private static b(Lcom/a/b/d/adv;)Lcom/a/b/d/mi;
    .registers 7

    .prologue
    const/4 v5, 0x0

    .line 82
    instance-of v0, p0, Lcom/a/b/d/mi;

    if-eqz v0, :cond_8

    .line 84
    check-cast p0, Lcom/a/b/d/mi;

    .line 109
    :goto_7
    return-object p0

    .line 88
    :cond_8
    invoke-interface {p0}, Lcom/a/b/d/adv;->k()I

    move-result v0

    .line 89
    packed-switch v0, :pswitch_data_64

    .line 98
    invoke-static {}, Lcom/a/b/d/lo;->i()Lcom/a/b/d/lp;

    move-result-object v1

    .line 101
    invoke-interface {p0}, Lcom/a/b/d/adv;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/adw;

    .line 106
    invoke-interface {v0}, Lcom/a/b/d/adw;->a()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Lcom/a/b/d/adw;->b()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0}, Lcom/a/b/d/adw;->c()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lcom/a/b/d/mi;->b(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/adw;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/a/b/d/lp;->c(Ljava/lang/Object;)Lcom/a/b/d/lp;

    goto :goto_1b

    .line 1057
    :pswitch_3b
    sget-object p0, Lcom/a/b/d/mi;->a:Lcom/a/b/d/mi;

    goto :goto_7

    .line 93
    :pswitch_3e
    invoke-interface {p0}, Lcom/a/b/d/adv;->e()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/mq;->b(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/adw;

    .line 95
    invoke-interface {v0}, Lcom/a/b/d/adw;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0}, Lcom/a/b/d/adw;->b()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Lcom/a/b/d/adw;->c()Ljava/lang/Object;

    move-result-object v0

    .line 1063
    new-instance p0, Lcom/a/b/d/aax;

    invoke-direct {p0, v1, v2, v0}, Lcom/a/b/d/aax;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_7

    .line 109
    :cond_5a
    invoke-virtual {v1}, Lcom/a/b/d/lp;->b()Lcom/a/b/d/lo;

    move-result-object v0

    .line 1141
    invoke-static {v0, v5, v5}, Lcom/a/b/d/zr;->a(Ljava/lang/Iterable;Ljava/util/Comparator;Ljava/util/Comparator;)Lcom/a/b/d/zr;

    move-result-object p0

    goto :goto_7

    .line 89
    nop

    :pswitch_data_64
    .packed-switch 0x0
        :pswitch_3b
        :pswitch_3e
    .end packed-switch
.end method

.method private static c(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/mi;
    .registers 4

    .prologue
    .line 63
    new-instance v0, Lcom/a/b/d/aax;

    invoke-direct {v0, p0, p1, p2}, Lcom/a/b/d/aax;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method private g(Ljava/lang/Object;)Lcom/a/b/d/jt;
    .registers 4

    .prologue
    .line 307
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 308
    invoke-virtual {p0}, Lcom/a/b/d/mi;->o()Lcom/a/b/d/jt;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/b/d/jt;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/jt;

    invoke-static {}, Lcom/a/b/d/jt;->k()Lcom/a/b/d/jt;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/jt;

    return-object v0
.end method

.method public static p()Lcom/a/b/d/mi;
    .registers 1

    .prologue
    .line 57
    sget-object v0, Lcom/a/b/d/mi;->a:Lcom/a/b/d/mi;

    return-object v0
.end method

.method private static s()Lcom/a/b/d/mj;
    .registers 1

    .prologue
    .line 119
    new-instance v0, Lcom/a/b/d/mj;

    invoke-direct {v0}, Lcom/a/b/d/mj;-><init>()V

    return-object v0
.end method

.method private t()Lcom/a/b/d/lo;
    .registers 2

    .prologue
    .line 253
    invoke-super {p0}, Lcom/a/b/d/bf;->e()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/lo;

    return-object v0
.end method

.method private static u()Lcom/a/b/d/agi;
    .registers 2

    .prologue
    .line 261
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "should never be called"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method private v()Lcom/a/b/d/iz;
    .registers 2

    .prologue
    .line 266
    invoke-super {p0}, Lcom/a/b/d/bf;->h()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/iz;

    return-object v0
.end method

.method private w()Lcom/a/b/d/lo;
    .registers 2

    .prologue
    .line 290
    invoke-virtual {p0}, Lcom/a/b/d/mi;->n()Lcom/a/b/d/jt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/jt;->g()Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method private x()Lcom/a/b/d/lo;
    .registers 2

    .prologue
    .line 314
    invoke-virtual {p0}, Lcom/a/b/d/mi;->o()Lcom/a/b/d/jt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/jt;->g()Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 352
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 46
    .line 4314
    invoke-virtual {p0}, Lcom/a/b/d/mi;->o()Lcom/a/b/d/jt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/jt;->g()Lcom/a/b/d/lo;

    move-result-object v0

    .line 46
    return-object v0
.end method

.method public final a(Lcom/a/b/d/adv;)V
    .registers 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 363
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/a/b/d/bf;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 327
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/mi;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public bridge synthetic b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 46
    invoke-super {p0, p1, p2}, Lcom/a/b/d/bf;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()Ljava/util/Set;
    .registers 2

    .prologue
    .line 46
    .line 4290
    invoke-virtual {p0}, Lcom/a/b/d/mi;->n()Lcom/a/b/d/jt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/jt;->g()Lcom/a/b/d/lo;

    move-result-object v0

    .line 46
    return-object v0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/a/b/d/bf;->b(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 373
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final bridge synthetic c()Z
    .registers 2

    .prologue
    .line 46
    invoke-super {p0}, Lcom/a/b/d/bf;->c()Z

    move-result v0

    return v0
.end method

.method public final c(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 332
    .line 1266
    invoke-super {p0}, Lcom/a/b/d/bf;->h()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/iz;

    .line 332
    invoke-virtual {v0, p1}, Lcom/a/b/d/iz;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public synthetic d(Ljava/lang/Object;)Ljava/util/Map;
    .registers 3

    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lcom/a/b/d/mi;->f(Ljava/lang/Object;)Lcom/a/b/d/jt;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 342
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final synthetic e(Ljava/lang/Object;)Ljava/util/Map;
    .registers 4

    .prologue
    .line 46
    .line 5307
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 5308
    invoke-virtual {p0}, Lcom/a/b/d/mi;->o()Lcom/a/b/d/jt;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/b/d/jt;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/jt;

    invoke-static {}, Lcom/a/b/d/jt;->k()Lcom/a/b/d/jt;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/jt;

    .line 46
    return-object v0
.end method

.method public final bridge synthetic e()Ljava/util/Set;
    .registers 2

    .prologue
    .line 46
    .line 4253
    invoke-super {p0}, Lcom/a/b/d/bf;->e()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/lo;

    .line 46
    return-object v0
.end method

.method public bridge synthetic equals(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/a/b/d/bf;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public f(Ljava/lang/Object;)Lcom/a/b/d/jt;
    .registers 4

    .prologue
    .line 283
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    invoke-virtual {p0}, Lcom/a/b/d/mi;->n()Lcom/a/b/d/jt;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/b/d/jt;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/jt;

    invoke-static {}, Lcom/a/b/d/jt;->k()Lcom/a/b/d/jt;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/jt;

    return-object v0
.end method

.method synthetic f()Ljava/util/Set;
    .registers 2

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/a/b/d/mi;->q()Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method final synthetic g()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 3261
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "should never be called"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public final bridge synthetic h()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 46
    .line 2266
    invoke-super {p0}, Lcom/a/b/d/bf;->h()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/iz;

    .line 46
    return-object v0
.end method

.method public bridge synthetic hashCode()I
    .registers 2

    .prologue
    .line 46
    invoke-super {p0}, Lcom/a/b/d/bf;->hashCode()I

    move-result v0

    return v0
.end method

.method synthetic i()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/a/b/d/mi;->r()Lcom/a/b/d/iz;

    move-result-object v0

    return-object v0
.end method

.method public synthetic l()Ljava/util/Map;
    .registers 2

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/a/b/d/mi;->n()Lcom/a/b/d/jt;

    move-result-object v0

    return-object v0
.end method

.method public synthetic m()Ljava/util/Map;
    .registers 2

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/a/b/d/mi;->o()Lcom/a/b/d/jt;

    move-result-object v0

    return-object v0
.end method

.method final m_()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 274
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "should never be called"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public abstract n()Lcom/a/b/d/jt;
.end method

.method public abstract o()Lcom/a/b/d/jt;
.end method

.method abstract q()Lcom/a/b/d/lo;
.end method

.method abstract r()Lcom/a/b/d/iz;
.end method

.method public bridge synthetic toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 46
    invoke-super {p0}, Lcom/a/b/d/bf;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
