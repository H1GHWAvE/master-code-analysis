.class final Lcom/a/b/d/cc;
.super Lcom/a/b/d/agi;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/bx;

.field private final b:Ljava/util/Deque;

.field private final c:Ljava/util/BitSet;


# direct methods
.method constructor <init>(Lcom/a/b/d/bx;Ljava/lang/Object;)V
    .registers 4

    .prologue
    .line 138
    iput-object p1, p0, Lcom/a/b/d/cc;->a:Lcom/a/b/d/bx;

    invoke-direct {p0}, Lcom/a/b/d/agi;-><init>()V

    .line 139
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/a/b/d/cc;->b:Ljava/util/Deque;

    .line 140
    iget-object v0, p0, Lcom/a/b/d/cc;->b:Ljava/util/Deque;

    invoke-interface {v0, p2}, Ljava/util/Deque;->addLast(Ljava/lang/Object;)V

    .line 141
    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    iput-object v0, p0, Lcom/a/b/d/cc;->c:Ljava/util/BitSet;

    .line 142
    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .registers 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/a/b/d/cc;->b:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final next()Ljava/lang/Object;
    .registers 4

    .prologue
    .line 152
    :goto_0
    iget-object v0, p0, Lcom/a/b/d/cc;->b:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->getLast()Ljava/lang/Object;

    move-result-object v0

    .line 153
    iget-object v1, p0, Lcom/a/b/d/cc;->c:Ljava/util/BitSet;

    iget-object v2, p0, Lcom/a/b/d/cc;->b:Ljava/util/Deque;

    invoke-interface {v2}, Ljava/util/Deque;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->get(I)Z

    move-result v1

    .line 154
    if-eqz v1, :cond_27

    .line 155
    iget-object v1, p0, Lcom/a/b/d/cc;->b:Ljava/util/Deque;

    invoke-interface {v1}, Ljava/util/Deque;->removeLast()Ljava/lang/Object;

    .line 156
    iget-object v1, p0, Lcom/a/b/d/cc;->c:Ljava/util/BitSet;

    iget-object v2, p0, Lcom/a/b/d/cc;->b:Ljava/util/Deque;

    invoke-interface {v2}, Ljava/util/Deque;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->clear(I)V

    .line 157
    return-object v0

    .line 159
    :cond_27
    iget-object v0, p0, Lcom/a/b/d/cc;->c:Ljava/util/BitSet;

    iget-object v1, p0, Lcom/a/b/d/cc;->b:Ljava/util/Deque;

    invoke-interface {v1}, Ljava/util/Deque;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 160
    iget-object v0, p0, Lcom/a/b/d/cc;->b:Ljava/util/Deque;

    iget-object v1, p0, Lcom/a/b/d/cc;->a:Lcom/a/b/d/bx;

    invoke-virtual {v1}, Lcom/a/b/d/bx;->b()Lcom/a/b/b/ci;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/bx;->a(Ljava/util/Deque;Lcom/a/b/b/ci;)V

    .line 161
    iget-object v0, p0, Lcom/a/b/d/cc;->b:Ljava/util/Deque;

    iget-object v1, p0, Lcom/a/b/d/cc;->a:Lcom/a/b/d/bx;

    invoke-virtual {v1}, Lcom/a/b/d/bx;->a()Lcom/a/b/b/ci;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/bx;->a(Ljava/util/Deque;Lcom/a/b/b/ci;)V

    goto :goto_0
.end method
