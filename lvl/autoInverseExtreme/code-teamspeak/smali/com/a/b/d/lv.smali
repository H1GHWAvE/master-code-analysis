.class final Lcom/a/b/d/lv;
.super Lcom/a/b/d/yw;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/aay;


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# direct methods
.method constructor <init>(Lcom/a/b/d/me;Lcom/a/b/d/jl;)V
    .registers 3

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lcom/a/b/d/yw;-><init>(Lcom/a/b/d/iz;Lcom/a/b/d/jl;)V

    .line 37
    return-void
.end method

.method private i()Lcom/a/b/d/me;
    .registers 2

    .prologue
    .line 41
    invoke-super {p0}, Lcom/a/b/d/yw;->b()Lcom/a/b/d/iz;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/me;

    return-object v0
.end method


# virtual methods
.method final bridge synthetic b()Lcom/a/b/d/iz;
    .registers 2

    .prologue
    .line 30
    .line 3041
    invoke-super {p0}, Lcom/a/b/d/yw;->b()Lcom/a/b/d/iz;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/me;

    .line 30
    return-object v0
.end method

.method final b(II)Lcom/a/b/d/jl;
    .registers 6
    .annotation build Lcom/a/b/a/c;
        a = "super.subListUnchecked does not exist; inherited subList is valid if slow"
    .end annotation

    .prologue
    .line 82
    new-instance v0, Lcom/a/b/d/zq;

    invoke-super {p0, p1, p2}, Lcom/a/b/d/yw;->b(II)Lcom/a/b/d/jl;

    move-result-object v1

    invoke-virtual {p0}, Lcom/a/b/d/lv;->comparator()Ljava/util/Comparator;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/zq;-><init>(Lcom/a/b/d/jl;Ljava/util/Comparator;)V

    invoke-virtual {v0}, Lcom/a/b/d/zq;->f()Lcom/a/b/d/jl;

    move-result-object v0

    return-object v0
.end method

.method public final comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 45
    .line 1041
    invoke-super {p0}, Lcom/a/b/d/yw;->b()Lcom/a/b/d/iz;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/me;

    .line 45
    invoke-virtual {v0}, Lcom/a/b/d/me;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 71
    invoke-virtual {p0, p1}, Lcom/a/b/d/lv;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final indexOf(Ljava/lang/Object;)I
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/a/b/a/c;
        a = "ImmutableSortedSet.indexOf"
    .end annotation

    .prologue
    .line 53
    .line 2041
    invoke-super {p0}, Lcom/a/b/d/yw;->b()Lcom/a/b/d/iz;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/me;

    .line 53
    invoke-virtual {v0, p1}, Lcom/a/b/d/me;->c(Ljava/lang/Object;)I

    move-result v0

    .line 60
    if-ltz v0, :cond_17

    invoke-virtual {p0, v0}, Lcom/a/b/d/lv;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    :goto_16
    return v0

    :cond_17
    const/4 v0, -0x1

    goto :goto_16
.end method

.method public final lastIndexOf(Ljava/lang/Object;)I
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/a/b/a/c;
        a = "ImmutableSortedSet.indexOf"
    .end annotation

    .prologue
    .line 65
    invoke-virtual {p0, p1}, Lcom/a/b/d/lv;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
