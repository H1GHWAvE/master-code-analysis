.class final Lcom/a/b/d/ct;
.super Ljava/util/AbstractCollection;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/Collection;

.field final b:Lcom/a/b/b/bj;


# direct methods
.method constructor <init>(Ljava/util/Collection;Lcom/a/b/b/bj;)V
    .registers 4

    .prologue
    .line 255
    invoke-direct {p0}, Ljava/util/AbstractCollection;-><init>()V

    .line 256
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    iput-object v0, p0, Lcom/a/b/d/ct;->a:Ljava/util/Collection;

    .line 257
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/bj;

    iput-object v0, p0, Lcom/a/b/d/ct;->b:Lcom/a/b/b/bj;

    .line 258
    return-void
.end method


# virtual methods
.method public final clear()V
    .registers 2

    .prologue
    .line 261
    iget-object v0, p0, Lcom/a/b/d/ct;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 262
    return-void
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 265
    iget-object v0, p0, Lcom/a/b/d/ct;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 269
    iget-object v0, p0, Lcom/a/b/d/ct;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/ct;->b:Lcom/a/b/b/bj;

    invoke-static {v0, v1}, Lcom/a/b/d/nj;->a(Ljava/util/Iterator;Lcom/a/b/b/bj;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 273
    iget-object v0, p0, Lcom/a/b/d/ct;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    return v0
.end method
