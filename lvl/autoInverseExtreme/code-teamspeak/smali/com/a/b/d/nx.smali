.class final Lcom/a/b/d/nx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field final synthetic a:I

.field final synthetic b:Ljava/util/Iterator;

.field private c:I


# direct methods
.method constructor <init>(ILjava/util/Iterator;)V
    .registers 3

    .prologue
    .line 930
    iput p1, p0, Lcom/a/b/d/nx;->a:I

    iput-object p2, p0, Lcom/a/b/d/nx;->b:Ljava/util/Iterator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .registers 3

    .prologue
    .line 935
    iget v0, p0, Lcom/a/b/d/nx;->c:I

    iget v1, p0, Lcom/a/b/d/nx;->a:I

    if-ge v0, v1, :cond_10

    iget-object v0, p0, Lcom/a/b/d/nx;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public final next()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 940
    invoke-virtual {p0}, Lcom/a/b/d/nx;->hasNext()Z

    move-result v0

    if-nez v0, :cond_c

    .line 941
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 943
    :cond_c
    iget v0, p0, Lcom/a/b/d/nx;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/nx;->c:I

    .line 944
    iget-object v0, p0, Lcom/a/b/d/nx;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .registers 2

    .prologue
    .line 949
    iget-object v0, p0, Lcom/a/b/d/nx;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 950
    return-void
.end method
