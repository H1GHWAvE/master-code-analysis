.class public abstract Lcom/a/b/d/hi;
.super Lcom/a/b/d/gh;
.source "SourceFile"

# interfaces
.implements Ljava/util/Set;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/a/b/d/gh;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 87
    invoke-static {p0, p1}, Lcom/a/b/d/aad;->a(Ljava/util/Set;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private c()I
    .registers 2

    .prologue
    .line 98
    invoke-static {p0}, Lcom/a/b/d/aad;->a(Ljava/util/Set;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public abstract a()Ljava/util/Set;
.end method

.method public synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/a/b/d/hi;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method protected final b(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 76
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {p0, v0}, Lcom/a/b/d/aad;->a(Ljava/util/Set;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 59
    if-eq p1, p0, :cond_c

    invoke-virtual {p0}, Lcom/a/b/d/hi;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/a/b/d/hi;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->hashCode()I

    move-result v0

    return v0
.end method

.method public synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/a/b/d/hi;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
