.class final Lcom/a/b/d/aah;
.super Lcom/a/b/d/gh;
.source "SourceFile"

# interfaces
.implements Ljava/util/Set;


# instance fields
.field private final transient a:Lcom/a/b/d/jl;

.field private final transient b:Lcom/a/b/d/ci;


# direct methods
.method private constructor <init>(Lcom/a/b/d/jl;Lcom/a/b/d/ci;)V
    .registers 3

    .prologue
    .line 1155
    invoke-direct {p0}, Lcom/a/b/d/gh;-><init>()V

    .line 1156
    iput-object p1, p0, Lcom/a/b/d/aah;->a:Lcom/a/b/d/jl;

    .line 1157
    iput-object p2, p0, Lcom/a/b/d/aah;->b:Lcom/a/b/d/ci;

    .line 1158
    return-void
.end method

.method static a(Ljava/util/List;)Ljava/util/Set;
    .registers 5

    .prologue
    .line 1124
    new-instance v1, Lcom/a/b/d/jn;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Lcom/a/b/d/jn;-><init>(I)V

    .line 1126
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 1127
    invoke-static {v0}, Lcom/a/b/d/lo;->a(Ljava/util/Collection;)Lcom/a/b/d/lo;

    move-result-object v0

    .line 1128
    invoke-virtual {v0}, Lcom/a/b/d/lo;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_28

    .line 1129
    invoke-static {}, Lcom/a/b/d/lo;->h()Lcom/a/b/d/lo;

    move-result-object v0

    .line 1151
    :goto_27
    return-object v0

    .line 1131
    :cond_28
    invoke-virtual {v1, v0}, Lcom/a/b/d/jn;->c(Ljava/lang/Object;)Lcom/a/b/d/jn;

    goto :goto_d

    .line 1133
    :cond_2c
    invoke-virtual {v1}, Lcom/a/b/d/jn;->b()Lcom/a/b/d/jl;

    move-result-object v1

    .line 1134
    new-instance v2, Lcom/a/b/d/aai;

    invoke-direct {v2, v1}, Lcom/a/b/d/aai;-><init>(Lcom/a/b/d/jl;)V

    .line 1151
    new-instance v0, Lcom/a/b/d/aah;

    new-instance v3, Lcom/a/b/d/ci;

    invoke-direct {v3, v2}, Lcom/a/b/d/ci;-><init>(Lcom/a/b/d/jl;)V

    invoke-direct {v0, v1, v3}, Lcom/a/b/d/aah;-><init>(Lcom/a/b/d/jl;Lcom/a/b/d/ci;)V

    goto :goto_27
.end method


# virtual methods
.method protected final b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 1162
    iget-object v0, p0, Lcom/a/b/d/aah;->b:Lcom/a/b/d/ci;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1168
    instance-of v0, p1, Lcom/a/b/d/aah;

    if-eqz v0, :cond_f

    .line 1169
    check-cast p1, Lcom/a/b/d/aah;

    .line 1170
    iget-object v0, p0, Lcom/a/b/d/aah;->a:Lcom/a/b/d/jl;

    iget-object v1, p1, Lcom/a/b/d/aah;->a:Lcom/a/b/d/jl;

    invoke-virtual {v0, v1}, Lcom/a/b/d/jl;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 1172
    :goto_e
    return v0

    :cond_f
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_e
.end method

.method public final hashCode()I
    .registers 7

    .prologue
    .line 1181
    invoke-virtual {p0}, Lcom/a/b/d/aah;->size()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    .line 1182
    const/4 v0, 0x0

    :goto_7
    iget-object v2, p0, Lcom/a/b/d/aah;->a:Lcom/a/b/d/jl;

    invoke-virtual {v2}, Lcom/a/b/d/jl;->size()I

    move-result v2

    if-ge v0, v2, :cond_18

    .line 1183
    mul-int/lit8 v1, v1, 0x1f

    .line 1184
    xor-int/lit8 v1, v1, -0x1

    xor-int/lit8 v1, v1, -0x1

    .line 1182
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 1187
    :cond_18
    const/4 v0, 0x1

    .line 1188
    iget-object v2, p0, Lcom/a/b/d/aah;->a:Lcom/a/b/d/jl;

    invoke-virtual {v2}, Lcom/a/b/d/jl;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_20
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_43

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 1189
    mul-int/lit8 v2, v2, 0x1f

    invoke-virtual {p0}, Lcom/a/b/d/aah;->size()I

    move-result v4

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v5

    div-int/2addr v4, v5

    invoke-interface {v0}, Ljava/util/Set;->hashCode()I

    move-result v0

    mul-int/2addr v0, v4

    add-int/2addr v0, v2

    .line 1191
    xor-int/lit8 v0, v0, -0x1

    xor-int/lit8 v0, v0, -0x1

    move v2, v0

    .line 1192
    goto :goto_20

    .line 1193
    :cond_43
    add-int v0, v2, v1

    .line 1194
    xor-int/lit8 v0, v0, -0x1

    xor-int/lit8 v0, v0, -0x1

    return v0
.end method

.method protected final bridge synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1118
    .line 2162
    iget-object v0, p0, Lcom/a/b/d/aah;->b:Lcom/a/b/d/ci;

    .line 1118
    return-object v0
.end method
