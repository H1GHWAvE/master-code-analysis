.class abstract Lcom/a/b/d/kr;
.super Lcom/a/b/d/agi;
.source "SourceFile"


# instance fields
.field final b:Ljava/util/Iterator;

.field c:Ljava/lang/Object;

.field d:Ljava/util/Iterator;

.field final synthetic e:Lcom/a/b/d/kk;


# direct methods
.method private constructor <init>(Lcom/a/b/d/kk;)V
    .registers 3

    .prologue
    .line 532
    iput-object p1, p0, Lcom/a/b/d/kr;->e:Lcom/a/b/d/kk;

    invoke-direct {p0}, Lcom/a/b/d/agi;-><init>()V

    .line 533
    iget-object v0, p0, Lcom/a/b/d/kr;->e:Lcom/a/b/d/kk;

    .line 1477
    iget-object v0, v0, Lcom/a/b/d/kk;->b:Lcom/a/b/d/jt;

    .line 533
    invoke-virtual {v0}, Lcom/a/b/d/jt;->e()Lcom/a/b/d/lo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lo;->c()Lcom/a/b/d/agi;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/kr;->b:Ljava/util/Iterator;

    .line 534
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/b/d/kr;->c:Ljava/lang/Object;

    .line 535
    invoke-static {}, Lcom/a/b/d/nj;->a()Lcom/a/b/d/agi;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/kr;->d:Ljava/util/Iterator;

    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/d/kk;B)V
    .registers 3

    .prologue
    .line 532
    invoke-direct {p0, p1}, Lcom/a/b/d/kr;-><init>(Lcom/a/b/d/kk;)V

    return-void
.end method


# virtual methods
.method abstract a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public hasNext()Z
    .registers 2

    .prologue
    .line 541
    iget-object v0, p0, Lcom/a/b/d/kr;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/a/b/d/kr;->d:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_10
    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public next()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 546
    iget-object v0, p0, Lcom/a/b/d/kr;->d:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_22

    .line 547
    iget-object v0, p0, Lcom/a/b/d/kr;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 548
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Lcom/a/b/d/kr;->c:Ljava/lang/Object;

    .line 549
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/kr;->d:Ljava/util/Iterator;

    .line 551
    :cond_22
    iget-object v0, p0, Lcom/a/b/d/kr;->c:Ljava/lang/Object;

    iget-object v1, p0, Lcom/a/b/d/kr;->d:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/a/b/d/kr;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
