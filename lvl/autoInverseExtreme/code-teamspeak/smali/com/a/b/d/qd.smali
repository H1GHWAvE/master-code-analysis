.class final Lcom/a/b/d/qd;
.super Lcom/a/b/d/gs;
.source "SourceFile"


# instance fields
.field a:Ljava/util/Set;

.field b:Ljava/util/Collection;

.field final synthetic c:Ljava/util/Map;

.field final synthetic d:Lcom/a/b/d/qc;


# direct methods
.method constructor <init>(Lcom/a/b/d/qc;Ljava/util/Map;)V
    .registers 3

    .prologue
    .line 420
    iput-object p1, p0, Lcom/a/b/d/qd;->d:Lcom/a/b/d/qc;

    iput-object p2, p0, Lcom/a/b/d/qd;->c:Ljava/util/Map;

    invoke-direct {p0}, Lcom/a/b/d/gs;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 440
    :try_start_1
    iget-object v1, p0, Lcom/a/b/d/qd;->d:Lcom/a/b/d/qc;

    invoke-virtual {v1, p1}, Lcom/a/b/d/qc;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v1

    .line 441
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z
    :try_end_a
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_a} :catch_10

    move-result v2

    if-eqz v2, :cond_e

    .line 443
    :goto_d
    return-object v0

    :cond_e
    move-object v0, v1

    .line 441
    goto :goto_d

    .line 443
    :catch_10
    move-exception v1

    goto :goto_d
.end method


# virtual methods
.method protected final a()Ljava/util/Map;
    .registers 2

    .prologue
    .line 425
    iget-object v0, p0, Lcom/a/b/d/qd;->c:Ljava/util/Map;

    return-object v0
.end method

.method public final containsValue(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 457
    invoke-virtual {p0}, Lcom/a/b/d/qd;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final entrySet()Ljava/util/Set;
    .registers 4

    .prologue
    .line 429
    iget-object v0, p0, Lcom/a/b/d/qd;->a:Ljava/util/Set;

    .line 430
    if-nez v0, :cond_15

    .line 431
    iget-object v0, p0, Lcom/a/b/d/qd;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    iget-object v0, p0, Lcom/a/b/d/qd;->d:Lcom/a/b/d/qc;

    iget-object v2, v0, Lcom/a/b/d/qc;->a:Lcom/a/b/d/pn;

    .line 1246
    new-instance v0, Lcom/a/b/d/ps;

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/ps;-><init>(Ljava/util/Set;Lcom/a/b/d/pn;)V

    .line 431
    iput-object v0, p0, Lcom/a/b/d/qd;->a:Ljava/util/Set;

    .line 434
    :cond_15
    return-object v0
.end method

.method public final synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 420
    invoke-direct {p0, p1}, Lcom/a/b/d/qd;->a(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 420
    .line 2425
    iget-object v0, p0, Lcom/a/b/d/qd;->c:Ljava/util/Map;

    .line 420
    return-object v0
.end method

.method public final values()Ljava/util/Collection;
    .registers 4

    .prologue
    .line 448
    iget-object v0, p0, Lcom/a/b/d/qd;->b:Ljava/util/Collection;

    .line 449
    if-nez v0, :cond_15

    .line 450
    new-instance v0, Lcom/a/b/d/pu;

    .line 1425
    iget-object v1, p0, Lcom/a/b/d/qd;->c:Ljava/util/Map;

    .line 450
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {p0}, Lcom/a/b/d/qd;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/pu;-><init>(Ljava/util/Collection;Ljava/util/Set;)V

    iput-object v0, p0, Lcom/a/b/d/qd;->b:Ljava/util/Collection;

    .line 453
    :cond_15
    return-object v0
.end method
