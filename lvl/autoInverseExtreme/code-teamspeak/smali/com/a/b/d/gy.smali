.class public abstract Lcom/a/b/d/gy;
.super Lcom/a/b/d/gh;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/xc;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# direct methods
.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/a/b/d/gh;-><init>()V

    return-void
.end method

.method private b(Ljava/lang/Object;II)Z
    .registers 5

    .prologue
    .line 226
    invoke-static {p0, p1, p2, p3}, Lcom/a/b/d/xe;->a(Lcom/a/b/d/xc;Ljava/lang/Object;II)Z

    move-result v0

    return v0
.end method

.method private c()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 261
    invoke-static {p0}, Lcom/a/b/d/xe;->b(Lcom/a/b/d/xc;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method private d(Ljava/lang/Object;)I
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/a/b/d/gy;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_23

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xd;

    .line 134
    invoke-interface {v0}, Lcom/a/b/d/xd;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/a/b/b/ce;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 135
    invoke-interface {v0}, Lcom/a/b/d/xd;->b()I

    move-result v0

    .line 138
    :goto_22
    return v0

    :cond_23
    const/4 v0, 0x0

    goto :goto_22
.end method

.method private d(Ljava/lang/Object;I)I
    .registers 4

    .prologue
    .line 214
    invoke-static {p0, p1, p2}, Lcom/a/b/d/xe;->a(Lcom/a/b/d/xc;Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method

.method private e()I
    .registers 2

    .prologue
    .line 272
    invoke-static {p0}, Lcom/a/b/d/xe;->c(Lcom/a/b/d/xc;)I

    move-result v0

    return v0
.end method

.method private e(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 149
    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/gy;->a(Ljava/lang/Object;I)I

    .line 150
    return v0
.end method

.method private f(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 284
    invoke-static {p0, p1}, Lcom/a/b/d/xe;->a(Lcom/a/b/d/xc;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private q()I
    .registers 2

    .prologue
    .line 295
    invoke-virtual {p0}, Lcom/a/b/d/gy;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->hashCode()I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/a/b/d/gy;->f()Lcom/a/b/d/xc;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/xc;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/Object;I)I
    .registers 4

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/a/b/d/gy;->f()Lcom/a/b/d/xc;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/d/xc;->a(Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method

.method public a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/a/b/d/gy;->f()Lcom/a/b/d/xc;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/xc;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;II)Z
    .registers 5

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/a/b/d/gy;->f()Lcom/a/b/d/xc;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/a/b/d/xc;->a(Ljava/lang/Object;II)Z

    move-result v0

    return v0
.end method

.method protected final a(Ljava/util/Collection;)Z
    .registers 3
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 163
    invoke-static {p0, p1}, Lcom/a/b/d/xe;->a(Lcom/a/b/d/xc;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/Object;I)I
    .registers 4

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/a/b/d/gy;->f()Lcom/a/b/d/xc;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/d/xc;->b(Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method

.method protected synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/a/b/d/gy;->f()Lcom/a/b/d/xc;

    move-result-object v0

    return-object v0
.end method

.method protected final b(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 111
    invoke-virtual {p0, p1}, Lcom/a/b/d/gy;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method protected final b(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 188
    invoke-static {p0, p1}, Lcom/a/b/d/xe;->b(Lcom/a/b/d/xc;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public c(Ljava/lang/Object;I)I
    .registers 4

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/a/b/d/gy;->f()Lcom/a/b/d/xc;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/d/xc;->c(Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method

.method protected final c(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    const/4 v0, 0x1

    .line 175
    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/gy;->b(Ljava/lang/Object;I)I

    move-result v1

    if-lez v1, :cond_8

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method protected final c(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 201
    invoke-static {p0, p1}, Lcom/a/b/d/xe;->c(Lcom/a/b/d/xc;Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 86
    if-eq p1, p0, :cond_c

    invoke-virtual {p0}, Lcom/a/b/d/gy;->f()Lcom/a/b/d/xc;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/xc;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method protected abstract f()Lcom/a/b/d/xc;
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/a/b/d/gy;->f()Lcom/a/b/d/xc;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/xc;->hashCode()I

    move-result v0

    return v0
.end method

.method protected synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/a/b/d/gy;->f()Lcom/a/b/d/xc;

    move-result-object v0

    return-object v0
.end method

.method protected final l()V
    .registers 2

    .prologue
    .line 122
    invoke-virtual {p0}, Lcom/a/b/d/gy;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->i(Ljava/util/Iterator;)V

    .line 123
    return-void
.end method

.method public n_()Ljava/util/Set;
    .registers 2

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/a/b/d/gy;->f()Lcom/a/b/d/xc;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/xc;->n_()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method protected final o()Ljava/lang/String;
    .registers 2

    .prologue
    .line 306
    invoke-virtual {p0}, Lcom/a/b/d/gy;->a()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
