.class Lcom/a/b/d/px;
.super Lcom/a/b/d/gh;
.source "SourceFile"


# instance fields
.field final a:Lcom/a/b/d/pn;

.field final b:Ljava/util/Collection;


# direct methods
.method constructor <init>(Ljava/util/Collection;Lcom/a/b/d/pn;)V
    .registers 3

    .prologue
    .line 574
    invoke-direct {p0}, Lcom/a/b/d/gh;-><init>()V

    .line 575
    iput-object p1, p0, Lcom/a/b/d/px;->b:Ljava/util/Collection;

    .line 576
    iput-object p2, p0, Lcom/a/b/d/px;->a:Lcom/a/b/d/pn;

    .line 577
    return-void
.end method


# virtual methods
.method protected final b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 579
    iget-object v0, p0, Lcom/a/b/d/px;->b:Ljava/util/Collection;

    return-object v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 603
    .line 1579
    iget-object v0, p0, Lcom/a/b/d/px;->b:Ljava/util/Collection;

    .line 603
    invoke-static {v0, p1}, Lcom/a/b/d/sz;->a(Ljava/util/Collection;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public containsAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 606
    .line 2141
    invoke-static {p0, p1}, Lcom/a/b/d/cm;->a(Ljava/util/Collection;Ljava/util/Collection;)Z

    move-result v0

    .line 606
    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 583
    iget-object v0, p0, Lcom/a/b/d/px;->b:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 584
    new-instance v1, Lcom/a/b/d/py;

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/py;-><init>(Lcom/a/b/d/px;Ljava/util/Iterator;)V

    return-object v1
.end method

.method protected final bridge synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 568
    .line 3579
    iget-object v0, p0, Lcom/a/b/d/px;->b:Ljava/util/Collection;

    .line 568
    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 609
    .line 2579
    iget-object v0, p0, Lcom/a/b/d/px;->b:Ljava/util/Collection;

    .line 609
    invoke-static {v0, p1}, Lcom/a/b/d/sz;->b(Ljava/util/Collection;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public removeAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 612
    invoke-virtual {p0, p1}, Lcom/a/b/d/px;->b(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public retainAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 615
    invoke-virtual {p0, p1}, Lcom/a/b/d/px;->c(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public toArray()[Ljava/lang/Object;
    .registers 2

    .prologue
    .line 597
    invoke-virtual {p0}, Lcom/a/b/d/px;->p()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 3

    .prologue
    .line 600
    .line 1253
    invoke-static {p0, p1}, Lcom/a/b/d/yc;->a(Ljava/util/Collection;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    .line 600
    return-object v0
.end method
