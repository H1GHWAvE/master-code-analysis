.class final Lcom/a/b/d/nt;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field a:Ljava/util/Iterator;

.field b:Ljava/util/Iterator;

.field final synthetic c:Ljava/util/Iterator;


# direct methods
.method constructor <init>(Ljava/util/Iterator;)V
    .registers 3

    .prologue
    .line 532
    iput-object p1, p0, Lcom/a/b/d/nt;->c:Ljava/util/Iterator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 533
    invoke-static {}, Lcom/a/b/d/nj;->a()Lcom/a/b/d/agi;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/nt;->a:Ljava/util/Iterator;

    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .registers 3

    .prologue
    .line 547
    :goto_0
    iget-object v0, p0, Lcom/a/b/d/nt;->a:Ljava/util/Iterator;

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_21

    iget-object v1, p0, Lcom/a/b/d/nt;->c:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_21

    .line 548
    iget-object v0, p0, Lcom/a/b/d/nt;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Iterator;

    iput-object v0, p0, Lcom/a/b/d/nt;->a:Ljava/util/Iterator;

    goto :goto_0

    .line 550
    :cond_21
    return v0
.end method

.method public final next()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 554
    invoke-virtual {p0}, Lcom/a/b/d/nt;->hasNext()Z

    move-result v0

    if-nez v0, :cond_c

    .line 555
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 557
    :cond_c
    iget-object v0, p0, Lcom/a/b/d/nt;->a:Ljava/util/Iterator;

    iput-object v0, p0, Lcom/a/b/d/nt;->b:Ljava/util/Iterator;

    .line 558
    iget-object v0, p0, Lcom/a/b/d/nt;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .registers 3

    .prologue
    .line 562
    iget-object v0, p0, Lcom/a/b/d/nt;->b:Ljava/util/Iterator;

    if-eqz v0, :cond_13

    const/4 v0, 0x1

    .line 1049
    :goto_5
    const-string v1, "no calls to next() since the last call to remove()"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 563
    iget-object v0, p0, Lcom/a/b/d/nt;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 564
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/b/d/nt;->b:Ljava/util/Iterator;

    .line 565
    return-void

    .line 562
    :cond_13
    const/4 v0, 0x0

    goto :goto_5
.end method
