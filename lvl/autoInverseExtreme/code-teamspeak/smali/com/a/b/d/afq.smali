.class final Lcom/a/b/d/afq;
.super Lcom/a/b/d/av;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/NavigableMap;

.field private final b:Ljava/util/NavigableMap;

.field private final c:Lcom/a/b/d/yl;


# direct methods
.method constructor <init>(Ljava/util/NavigableMap;)V
    .registers 3

    .prologue
    .line 412
    invoke-static {}, Lcom/a/b/d/yl;->c()Lcom/a/b/d/yl;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/a/b/d/afq;-><init>(Ljava/util/NavigableMap;Lcom/a/b/d/yl;)V

    .line 413
    return-void
.end method

.method private constructor <init>(Ljava/util/NavigableMap;Lcom/a/b/d/yl;)V
    .registers 4

    .prologue
    .line 416
    invoke-direct {p0}, Lcom/a/b/d/av;-><init>()V

    .line 417
    iput-object p1, p0, Lcom/a/b/d/afq;->a:Ljava/util/NavigableMap;

    .line 418
    new-instance v0, Lcom/a/b/d/aft;

    invoke-direct {v0, p1}, Lcom/a/b/d/aft;-><init>(Ljava/util/NavigableMap;)V

    iput-object v0, p0, Lcom/a/b/d/afq;->b:Ljava/util/NavigableMap;

    .line 419
    iput-object p2, p0, Lcom/a/b/d/afq;->c:Lcom/a/b/d/yl;

    .line 420
    return-void
.end method

.method static synthetic a(Lcom/a/b/d/afq;)Lcom/a/b/d/yl;
    .registers 2

    .prologue
    .line 399
    iget-object v0, p0, Lcom/a/b/d/afq;->c:Lcom/a/b/d/yl;

    return-object v0
.end method

.method private a(Ljava/lang/Object;)Lcom/a/b/d/yl;
    .registers 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 572
    instance-of v0, p1, Lcom/a/b/d/dw;

    if-eqz v0, :cond_28

    .line 575
    :try_start_5
    check-cast p1, Lcom/a/b/d/dw;

    .line 577
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/a/b/d/afq;->b(Lcom/a/b/d/dw;Z)Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v2

    .line 578
    if-eqz v2, :cond_28

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/dw;

    invoke-virtual {v0, p1}, Lcom/a/b/d/dw;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 579
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;
    :try_end_24
    .catch Ljava/lang/ClassCastException; {:try_start_5 .. :try_end_24} :catch_25

    .line 585
    :goto_24
    return-object v0

    .line 582
    :catch_25
    move-exception v0

    move-object v0, v1

    goto :goto_24

    :cond_28
    move-object v0, v1

    .line 585
    goto :goto_24
.end method

.method private a(Lcom/a/b/d/dw;Z)Ljava/util/NavigableMap;
    .registers 4

    .prologue
    .line 441
    invoke-static {p2}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/a/b/d/yl;->a(Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/afq;->a(Lcom/a/b/d/yl;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/a/b/d/dw;ZLcom/a/b/d/dw;Z)Ljava/util/NavigableMap;
    .registers 7

    .prologue
    .line 434
    invoke-static {p2}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v0

    invoke-static {p4}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v1

    invoke-static {p1, v0, p3, v1}, Lcom/a/b/d/yl;->a(Ljava/lang/Comparable;Lcom/a/b/d/ce;Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/afq;->a(Lcom/a/b/d/yl;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/a/b/d/yl;)Ljava/util/NavigableMap;
    .registers 5

    .prologue
    .line 423
    iget-object v0, p0, Lcom/a/b/d/afq;->c:Lcom/a/b/d/yl;

    invoke-virtual {v0, p1}, Lcom/a/b/d/yl;->b(Lcom/a/b/d/yl;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 424
    invoke-static {}, Lcom/a/b/d/lw;->m()Lcom/a/b/d/lw;

    move-result-object v0

    .line 427
    :goto_c
    return-object v0

    .line 426
    :cond_d
    iget-object v0, p0, Lcom/a/b/d/afq;->c:Lcom/a/b/d/yl;

    invoke-virtual {p1, v0}, Lcom/a/b/d/yl;->c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;

    move-result-object v1

    .line 427
    new-instance v0, Lcom/a/b/d/afq;

    iget-object v2, p0, Lcom/a/b/d/afq;->a:Ljava/util/NavigableMap;

    invoke-direct {v0, v2, v1}, Lcom/a/b/d/afq;-><init>(Ljava/util/NavigableMap;Lcom/a/b/d/yl;)V

    goto :goto_c
.end method

.method private b(Lcom/a/b/d/dw;Z)Ljava/util/NavigableMap;
    .registers 4

    .prologue
    .line 446
    invoke-static {p2}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/a/b/d/yl;->b(Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/afq;->a(Lcom/a/b/d/yl;)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method final a()Ljava/util/Iterator;
    .registers 5

    .prologue
    .line 466
    iget-object v0, p0, Lcom/a/b/d/afq;->c:Lcom/a/b/d/yl;

    invoke-virtual {v0}, Lcom/a/b/d/yl;->d()Z

    move-result v0

    if-eqz v0, :cond_5c

    .line 467
    iget-object v1, p0, Lcom/a/b/d/afq;->b:Ljava/util/NavigableMap;

    iget-object v0, p0, Lcom/a/b/d/afq;->c:Lcom/a/b/d/yl;

    .line 1383
    iget-object v0, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v0}, Lcom/a/b/d/dw;->c()Ljava/lang/Comparable;

    move-result-object v2

    .line 467
    iget-object v0, p0, Lcom/a/b/d/afq;->c:Lcom/a/b/d/yl;

    .line 1394
    iget-object v0, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-virtual {v0}, Lcom/a/b/d/dw;->a()Lcom/a/b/d/ce;

    move-result-object v0

    .line 467
    sget-object v3, Lcom/a/b/d/ce;->b:Lcom/a/b/d/ce;

    if-ne v0, v3, :cond_5a

    const/4 v0, 0x1

    :goto_1f
    invoke-interface {v1, v2, v0}, Ljava/util/NavigableMap;->tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->values()Ljava/util/Collection;

    move-result-object v0

    .line 473
    :goto_27
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->j(Ljava/util/Iterator;)Lcom/a/b/d/yi;

    move-result-object v2

    .line 476
    iget-object v0, p0, Lcom/a/b/d/afq;->c:Lcom/a/b/d/yl;

    invoke-static {}, Lcom/a/b/d/dw;->d()Lcom/a/b/d/dw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/d/yl;->c(Ljava/lang/Comparable;)Z

    move-result v0

    if-eqz v0, :cond_63

    invoke-interface {v2}, Lcom/a/b/d/yi;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4f

    invoke-interface {v2}, Lcom/a/b/d/yi;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    iget-object v0, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    invoke-static {}, Lcom/a/b/d/dw;->d()Lcom/a/b/d/dw;

    move-result-object v1

    if-eq v0, v1, :cond_63

    .line 478
    :cond_4f
    invoke-static {}, Lcom/a/b/d/dw;->d()Lcom/a/b/d/dw;

    move-result-object v0

    .line 484
    :goto_53
    new-instance v1, Lcom/a/b/d/afr;

    invoke-direct {v1, p0, v0, v2}, Lcom/a/b/d/afr;-><init>(Lcom/a/b/d/afq;Lcom/a/b/d/dw;Lcom/a/b/d/yi;)V

    move-object v0, v1

    :goto_59
    return-object v0

    .line 467
    :cond_5a
    const/4 v0, 0x0

    goto :goto_1f

    .line 471
    :cond_5c
    iget-object v0, p0, Lcom/a/b/d/afq;->b:Ljava/util/NavigableMap;

    invoke-interface {v0}, Ljava/util/NavigableMap;->values()Ljava/util/Collection;

    move-result-object v0

    goto :goto_27

    .line 479
    :cond_63
    invoke-interface {v2}, Lcom/a/b/d/yi;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_72

    .line 480
    invoke-interface {v2}, Lcom/a/b/d/yi;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    iget-object v0, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    goto :goto_53

    .line 482
    :cond_72
    invoke-static {}, Lcom/a/b/d/nj;->a()Lcom/a/b/d/agi;

    move-result-object v0

    goto :goto_59
.end method

.method final b()Ljava/util/Iterator;
    .registers 4

    .prologue
    .line 517
    iget-object v0, p0, Lcom/a/b/d/afq;->c:Lcom/a/b/d/yl;

    invoke-virtual {v0}, Lcom/a/b/d/yl;->e()Z

    move-result v0

    if-eqz v0, :cond_6a

    iget-object v0, p0, Lcom/a/b/d/afq;->c:Lcom/a/b/d/yl;

    .line 1411
    iget-object v0, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v0}, Lcom/a/b/d/dw;->c()Ljava/lang/Comparable;

    move-result-object v0

    .line 517
    check-cast v0, Lcom/a/b/d/dw;

    .line 520
    :goto_12
    iget-object v1, p0, Lcom/a/b/d/afq;->c:Lcom/a/b/d/yl;

    invoke-virtual {v1}, Lcom/a/b/d/yl;->e()Z

    move-result v1

    if-eqz v1, :cond_6f

    iget-object v1, p0, Lcom/a/b/d/afq;->c:Lcom/a/b/d/yl;

    .line 1422
    iget-object v1, v1, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-virtual {v1}, Lcom/a/b/d/dw;->b()Lcom/a/b/d/ce;

    move-result-object v1

    .line 520
    sget-object v2, Lcom/a/b/d/ce;->b:Lcom/a/b/d/ce;

    if-ne v1, v2, :cond_6f

    const/4 v1, 0x1

    .line 522
    :goto_27
    iget-object v2, p0, Lcom/a/b/d/afq;->b:Ljava/util/NavigableMap;

    invoke-interface {v2, v0, v1}, Ljava/util/NavigableMap;->headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->descendingMap()Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->j(Ljava/util/Iterator;)Lcom/a/b/d/yi;

    move-result-object v2

    .line 526
    invoke-interface {v2}, Lcom/a/b/d/yi;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_82

    .line 527
    invoke-interface {v2}, Lcom/a/b/d/yi;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    iget-object v0, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-static {}, Lcom/a/b/d/dw;->e()Lcom/a/b/d/dw;

    move-result-object v1

    if-ne v0, v1, :cond_71

    invoke-interface {v2}, Lcom/a/b/d/yi;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    iget-object v0, v0, Lcom/a/b/d/yl;->b:Lcom/a/b/d/dw;

    .line 536
    :goto_59
    invoke-static {}, Lcom/a/b/d/dw;->e()Lcom/a/b/d/dw;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/b/ca;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/dw;

    .line 538
    new-instance v1, Lcom/a/b/d/afs;

    invoke-direct {v1, p0, v0, v2}, Lcom/a/b/d/afs;-><init>(Lcom/a/b/d/afq;Lcom/a/b/d/dw;Lcom/a/b/d/yi;)V

    move-object v0, v1

    :goto_69
    return-object v0

    .line 517
    :cond_6a
    invoke-static {}, Lcom/a/b/d/dw;->e()Lcom/a/b/d/dw;

    move-result-object v0

    goto :goto_12

    .line 520
    :cond_6f
    const/4 v1, 0x0

    goto :goto_27

    .line 527
    :cond_71
    iget-object v1, p0, Lcom/a/b/d/afq;->a:Ljava/util/NavigableMap;

    invoke-interface {v2}, Lcom/a/b/d/yi;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/yl;

    iget-object v0, v0, Lcom/a/b/d/yl;->c:Lcom/a/b/d/dw;

    invoke-interface {v1, v0}, Ljava/util/NavigableMap;->higherKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/dw;

    goto :goto_59

    .line 530
    :cond_82
    iget-object v0, p0, Lcom/a/b/d/afq;->c:Lcom/a/b/d/yl;

    invoke-static {}, Lcom/a/b/d/dw;->d()Lcom/a/b/d/dw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/d/yl;->c(Ljava/lang/Comparable;)Z

    move-result v0

    if-eqz v0, :cond_9a

    iget-object v0, p0, Lcom/a/b/d/afq;->a:Ljava/util/NavigableMap;

    invoke-static {}, Lcom/a/b/d/dw;->d()Lcom/a/b/d/dw;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9f

    .line 532
    :cond_9a
    invoke-static {}, Lcom/a/b/d/nj;->a()Lcom/a/b/d/agi;

    move-result-object v0

    goto :goto_69

    .line 534
    :cond_9f
    iget-object v0, p0, Lcom/a/b/d/afq;->a:Ljava/util/NavigableMap;

    invoke-static {}, Lcom/a/b/d/dw;->d()Lcom/a/b/d/dw;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->higherKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/dw;

    goto :goto_59
.end method

.method public final comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 451
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    return-object v0
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 590
    invoke-direct {p0, p1}, Lcom/a/b/d/afq;->a(Ljava/lang/Object;)Lcom/a/b/d/yl;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 399
    invoke-direct {p0, p1}, Lcom/a/b/d/afq;->a(Ljava/lang/Object;)Lcom/a/b/d/yl;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 4

    .prologue
    .line 399
    check-cast p1, Lcom/a/b/d/dw;

    .line 1441
    invoke-static {p2}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/a/b/d/yl;->a(Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/afq;->a(Lcom/a/b/d/yl;)Ljava/util/NavigableMap;

    move-result-object v0

    .line 399
    return-object v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 566
    invoke-virtual {p0}, Lcom/a/b/d/afq;->a()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->b(Ljava/util/Iterator;)I

    move-result v0

    return v0
.end method

.method public final synthetic subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 7

    .prologue
    .line 399
    check-cast p1, Lcom/a/b/d/dw;

    check-cast p3, Lcom/a/b/d/dw;

    .line 2434
    invoke-static {p2}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v0

    invoke-static {p4}, Lcom/a/b/d/ce;->a(Z)Lcom/a/b/d/ce;

    move-result-object v1

    invoke-static {p1, v0, p3, v1}, Lcom/a/b/d/yl;->a(Ljava/lang/Comparable;Lcom/a/b/d/ce;Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/afq;->a(Lcom/a/b/d/yl;)Ljava/util/NavigableMap;

    move-result-object v0

    .line 399
    return-object v0
.end method

.method public final synthetic tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 4

    .prologue
    .line 399
    check-cast p1, Lcom/a/b/d/dw;

    invoke-direct {p0, p1, p2}, Lcom/a/b/d/afq;->b(Lcom/a/b/d/dw;Z)Ljava/util/NavigableMap;

    move-result-object v0

    return-object v0
.end method
