.class final Lcom/a/b/d/fp;
.super Lcom/a/b/d/xr;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/fo;


# direct methods
.method constructor <init>(Lcom/a/b/d/fo;)V
    .registers 2

    .prologue
    .line 362
    iput-object p1, p0, Lcom/a/b/d/fp;->a:Lcom/a/b/d/fo;

    invoke-direct {p0}, Lcom/a/b/d/xr;-><init>()V

    return-void
.end method

.method private a(Lcom/a/b/b/co;)Z
    .registers 4

    .prologue
    .line 380
    iget-object v0, p0, Lcom/a/b/d/fp;->a:Lcom/a/b/d/fo;

    iget-object v0, v0, Lcom/a/b/d/fo;->a:Lcom/a/b/d/fi;

    new-instance v1, Lcom/a/b/d/fq;

    invoke-direct {v1, p0, p1}, Lcom/a/b/d/fq;-><init>(Lcom/a/b/d/fp;Lcom/a/b/b/co;)V

    invoke-virtual {v0, v1}, Lcom/a/b/d/fi;->a(Lcom/a/b/b/co;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method final a()Lcom/a/b/d/xc;
    .registers 2

    .prologue
    .line 366
    iget-object v0, p0, Lcom/a/b/d/fp;->a:Lcom/a/b/d/fo;

    return-object v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 371
    iget-object v0, p0, Lcom/a/b/d/fp;->a:Lcom/a/b/d/fo;

    invoke-virtual {v0}, Lcom/a/b/d/fo;->b()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final removeAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 392
    invoke-static {p1}, Lcom/a/b/b/cp;->a(Ljava/util/Collection;)Lcom/a/b/b/co;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/fp;->a(Lcom/a/b/b/co;)Z

    move-result v0

    return v0
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 397
    invoke-static {p1}, Lcom/a/b/b/cp;->a(Ljava/util/Collection;)Lcom/a/b/b/co;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/fp;->a(Lcom/a/b/b/co;)Z

    move-result v0

    return v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 376
    iget-object v0, p0, Lcom/a/b/d/fp;->a:Lcom/a/b/d/fo;

    iget-object v0, v0, Lcom/a/b/d/fo;->a:Lcom/a/b/d/fi;

    invoke-virtual {v0}, Lcom/a/b/d/fi;->p()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method
