.class Lcom/a/b/d/sl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/rz;


# instance fields
.field final a:Ljava/lang/Object;

.field final b:I

.field final c:Lcom/a/b/d/rz;

.field volatile d:Lcom/a/b/d/sr;


# direct methods
.method constructor <init>(Ljava/lang/Object;ILcom/a/b/d/rz;)V
    .registers 5
    .param p3    # Lcom/a/b/d/rz;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 917
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 986
    invoke-static {}, Lcom/a/b/d/qy;->g()Lcom/a/b/d/sr;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/sl;->d:Lcom/a/b/d/sr;

    .line 918
    iput-object p1, p0, Lcom/a/b/d/sl;->a:Ljava/lang/Object;

    .line 919
    iput p2, p0, Lcom/a/b/d/sl;->b:I

    .line 920
    iput-object p3, p0, Lcom/a/b/d/sl;->c:Lcom/a/b/d/rz;

    .line 921
    return-void
.end method


# virtual methods
.method public final a()Lcom/a/b/d/sr;
    .registers 2

    .prologue
    .line 990
    iget-object v0, p0, Lcom/a/b/d/sl;->d:Lcom/a/b/d/sr;

    return-object v0
.end method

.method public a(J)V
    .registers 4

    .prologue
    .line 937
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(Lcom/a/b/d/rz;)V
    .registers 3

    .prologue
    .line 947
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Lcom/a/b/d/sr;)V
    .registers 3

    .prologue
    .line 995
    iget-object v0, p0, Lcom/a/b/d/sl;->d:Lcom/a/b/d/sr;

    .line 996
    iput-object p1, p0, Lcom/a/b/d/sl;->d:Lcom/a/b/d/sr;

    .line 997
    invoke-interface {v0, p1}, Lcom/a/b/d/sr;->a(Lcom/a/b/d/sr;)V

    .line 998
    return-void
.end method

.method public final b()Lcom/a/b/d/rz;
    .registers 2

    .prologue
    .line 1007
    iget-object v0, p0, Lcom/a/b/d/sl;->c:Lcom/a/b/d/rz;

    return-object v0
.end method

.method public b(Lcom/a/b/d/rz;)V
    .registers 3

    .prologue
    .line 957
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final c()I
    .registers 2

    .prologue
    .line 1002
    iget v0, p0, Lcom/a/b/d/sl;->b:I

    return v0
.end method

.method public c(Lcom/a/b/d/rz;)V
    .registers 3

    .prologue
    .line 969
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 925
    iget-object v0, p0, Lcom/a/b/d/sl;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public d(Lcom/a/b/d/rz;)V
    .registers 3

    .prologue
    .line 979
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public e()J
    .registers 2

    .prologue
    .line 932
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public f()Lcom/a/b/d/rz;
    .registers 2

    .prologue
    .line 942
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public g()Lcom/a/b/d/rz;
    .registers 2

    .prologue
    .line 952
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public h()Lcom/a/b/d/rz;
    .registers 2

    .prologue
    .line 964
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public i()Lcom/a/b/d/rz;
    .registers 2

    .prologue
    .line 974
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
