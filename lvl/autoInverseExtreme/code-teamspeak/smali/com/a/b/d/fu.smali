.class Lcom/a/b/d/fu;
.super Lcom/a/b/d/an;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/ga;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# instance fields
.field final a:Lcom/a/b/d/vi;

.field final b:Lcom/a/b/b/co;


# direct methods
.method constructor <init>(Lcom/a/b/d/vi;Lcom/a/b/b/co;)V
    .registers 4

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/a/b/d/an;-><init>()V

    .line 44
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/vi;

    iput-object v0, p0, Lcom/a/b/d/fu;->a:Lcom/a/b/d/vi;

    .line 45
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/co;

    iput-object v0, p0, Lcom/a/b/d/fu;->b:Lcom/a/b/b/co;

    .line 46
    return-void
.end method

.method private d()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/a/b/d/fu;->a:Lcom/a/b/d/vi;

    instance-of v0, v0, Lcom/a/b/d/aac;

    if-eqz v0, :cond_b

    .line 84
    invoke-static {}, Lcom/a/b/d/lo;->h()Lcom/a/b/d/lo;

    move-result-object v0

    .line 86
    :goto_a
    return-object v0

    :cond_b
    invoke-static {}, Lcom/a/b/d/jl;->d()Lcom/a/b/d/jl;

    move-result-object v0

    goto :goto_a
.end method


# virtual methods
.method public a()Lcom/a/b/d/vi;
    .registers 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/a/b/d/fu;->a:Lcom/a/b/d/vi;

    return-object v0
.end method

.method public final c()Lcom/a/b/b/co;
    .registers 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/a/b/d/fu;->b:Lcom/a/b/b/co;

    invoke-static {v0}, Lcom/a/b/d/sz;->a(Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 102
    iget-object v0, p0, Lcom/a/b/d/fu;->b:Lcom/a/b/b/co;

    invoke-interface {v0, p1}, Lcom/a/b/b/co;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 103
    iget-object v0, p0, Lcom/a/b/d/fu;->a:Lcom/a/b/d/vi;

    invoke-interface {v0, p1}, Lcom/a/b/d/vi;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    .line 107
    :goto_e
    return-object v0

    .line 104
    :cond_f
    iget-object v0, p0, Lcom/a/b/d/fu;->a:Lcom/a/b/d/vi;

    instance-of v0, v0, Lcom/a/b/d/aac;

    if-eqz v0, :cond_1b

    .line 105
    new-instance v0, Lcom/a/b/d/fw;

    invoke-direct {v0, p1}, Lcom/a/b/d/fw;-><init>(Ljava/lang/Object;)V

    goto :goto_e

    .line 107
    :cond_1b
    new-instance v0, Lcom/a/b/d/fv;

    invoke-direct {v0, p1}, Lcom/a/b/d/fv;-><init>(Ljava/lang/Object;)V

    goto :goto_e
.end method

.method public d(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 79
    invoke-virtual {p0, p1}, Lcom/a/b/d/fu;->f(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/a/b/d/fu;->a:Lcom/a/b/d/vi;

    invoke-interface {v0, p1}, Lcom/a/b/d/vi;->d(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    .line 1084
    :goto_c
    return-object v0

    .line 1083
    :cond_d
    iget-object v0, p0, Lcom/a/b/d/fu;->a:Lcom/a/b/d/vi;

    instance-of v0, v0, Lcom/a/b/d/aac;

    if-eqz v0, :cond_18

    .line 1084
    invoke-static {}, Lcom/a/b/d/lo;->h()Lcom/a/b/d/lo;

    move-result-object v0

    goto :goto_c

    .line 1086
    :cond_18
    invoke-static {}, Lcom/a/b/d/jl;->d()Lcom/a/b/d/jl;

    move-result-object v0

    goto :goto_c
.end method

.method public final f()I
    .registers 4

    .prologue
    .line 60
    const/4 v0, 0x0

    .line 61
    invoke-virtual {p0}, Lcom/a/b/d/fu;->b()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_21

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 62
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 63
    goto :goto_e

    .line 64
    :cond_21
    return v1
.end method

.method public final f(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 69
    iget-object v0, p0, Lcom/a/b/d/fu;->a:Lcom/a/b/d/vi;

    invoke-interface {v0, p1}, Lcom/a/b/d/vi;->f(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 72
    iget-object v0, p0, Lcom/a/b/d/fu;->b:Lcom/a/b/b/co;

    invoke-interface {v0, p1}, Lcom/a/b/b/co;->a(Ljava/lang/Object;)Z

    move-result v0

    .line 74
    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public final g()V
    .registers 2

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/a/b/d/fu;->p()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 93
    return-void
.end method

.method final h()Ljava/util/Set;
    .registers 3

    .prologue
    .line 97
    iget-object v0, p0, Lcom/a/b/d/fu;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->p()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/fu;->b:Lcom/a/b/b/co;

    invoke-static {v0, v1}, Lcom/a/b/d/aad;->a(Ljava/util/Set;Lcom/a/b/b/co;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method final l()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 175
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "should never be called"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method final m()Ljava/util/Map;
    .registers 3

    .prologue
    .line 211
    iget-object v0, p0, Lcom/a/b/d/fu;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/fu;->b:Lcom/a/b/b/co;

    invoke-static {v0, v1}, Lcom/a/b/d/sz;->a(Ljava/util/Map;Lcom/a/b/b/co;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method o()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 180
    new-instance v0, Lcom/a/b/d/fx;

    invoke-direct {v0, p0}, Lcom/a/b/d/fx;-><init>(Lcom/a/b/d/fu;)V

    return-object v0
.end method

.method final r()Lcom/a/b/d/xc;
    .registers 3

    .prologue
    .line 216
    iget-object v0, p0, Lcom/a/b/d/fu;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->q()Lcom/a/b/d/xc;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/fu;->b:Lcom/a/b/b/co;

    invoke-static {v0, v1}, Lcom/a/b/d/xe;->a(Lcom/a/b/d/xc;Lcom/a/b/b/co;)Lcom/a/b/d/xc;

    move-result-object v0

    return-object v0
.end method

.method final s()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 206
    new-instance v0, Lcom/a/b/d/gb;

    invoke-direct {v0, p0}, Lcom/a/b/d/gb;-><init>(Lcom/a/b/d/ga;)V

    return-object v0
.end method
