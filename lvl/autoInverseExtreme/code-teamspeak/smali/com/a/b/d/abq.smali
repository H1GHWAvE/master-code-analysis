.class Lcom/a/b/d/abq;
.super Lcom/a/b/d/xp;
.source "SourceFile"

# interfaces
.implements Ljava/util/SortedSet;


# instance fields
.field final b:Lcom/a/b/d/abn;


# direct methods
.method constructor <init>(Lcom/a/b/d/abn;)V
    .registers 2

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/a/b/d/xp;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/a/b/d/abq;->b:Lcom/a/b/d/abn;

    .line 54
    return-void
.end method

.method private b()Lcom/a/b/d/abn;
    .registers 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/a/b/d/abq;->b:Lcom/a/b/d/abn;

    return-object v0
.end method


# virtual methods
.method final bridge synthetic a()Lcom/a/b/d/xc;
    .registers 2

    .prologue
    .line 48
    .line 7057
    iget-object v0, p0, Lcom/a/b/d/abq;->b:Lcom/a/b/d/abn;

    .line 48
    return-object v0
.end method

.method public comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 61
    .line 1057
    iget-object v0, p0, Lcom/a/b/d/abq;->b:Lcom/a/b/d/abn;

    .line 61
    invoke-interface {v0}, Lcom/a/b/d/abn;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public first()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 77
    .line 5057
    iget-object v0, p0, Lcom/a/b/d/abq;->b:Lcom/a/b/d/abn;

    .line 77
    invoke-interface {v0}, Lcom/a/b/d/abn;->h()Lcom/a/b/d/xd;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/abp;->a(Lcom/a/b/d/xd;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 4

    .prologue
    .line 69
    .line 3057
    iget-object v0, p0, Lcom/a/b/d/abq;->b:Lcom/a/b/d/abn;

    .line 69
    sget-object v1, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    invoke-interface {v0, p1, v1}, Lcom/a/b/d/abn;->d(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/abn;->e_()Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public last()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 81
    .line 6057
    iget-object v0, p0, Lcom/a/b/d/abq;->b:Lcom/a/b/d/abn;

    .line 81
    invoke-interface {v0}, Lcom/a/b/d/abn;->i()Lcom/a/b/d/xd;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/abp;->a(Lcom/a/b/d/xd;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 6

    .prologue
    .line 65
    .line 2057
    iget-object v0, p0, Lcom/a/b/d/abq;->b:Lcom/a/b/d/abn;

    .line 65
    sget-object v1, Lcom/a/b/d/ce;->b:Lcom/a/b/d/ce;

    sget-object v2, Lcom/a/b/d/ce;->a:Lcom/a/b/d/ce;

    invoke-interface {v0, p1, v1, p2, v2}, Lcom/a/b/d/abn;->a(Ljava/lang/Object;Lcom/a/b/d/ce;Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/abn;->e_()Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method

.method public tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 4

    .prologue
    .line 73
    .line 4057
    iget-object v0, p0, Lcom/a/b/d/abq;->b:Lcom/a/b/d/abn;

    .line 73
    sget-object v1, Lcom/a/b/d/ce;->b:Lcom/a/b/d/ce;

    invoke-interface {v0, p1, v1}, Lcom/a/b/d/abn;->c(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/abn;->e_()Ljava/util/NavigableSet;

    move-result-object v0

    return-object v0
.end method
