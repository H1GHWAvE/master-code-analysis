.class Lcom/a/b/d/u;
.super Lcom/a/b/d/uk;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/n;


# direct methods
.method constructor <init>(Lcom/a/b/d/n;Ljava/util/Map;)V
    .registers 3

    .prologue
    .line 920
    iput-object p1, p0, Lcom/a/b/d/u;->a:Lcom/a/b/d/n;

    .line 921
    invoke-direct {p0, p2}, Lcom/a/b/d/uk;-><init>(Ljava/util/Map;)V

    .line 922
    return-void
.end method


# virtual methods
.method public clear()V
    .registers 2

    .prologue
    .line 965
    invoke-virtual {p0}, Lcom/a/b/d/u;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/nj;->i(Ljava/util/Iterator;)V

    .line 966
    return-void
.end method

.method public containsAll(Ljava/util/Collection;)Z
    .registers 3

    .prologue
    .line 969
    invoke-virtual {p0}, Lcom/a/b/d/u;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 973
    if-eq p0, p1, :cond_10

    invoke-virtual {p0}, Lcom/a/b/d/u;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_10
    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 977
    invoke-virtual {p0}, Lcom/a/b/d/u;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->hashCode()I

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 925
    invoke-virtual {p0}, Lcom/a/b/d/u;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 927
    new-instance v1, Lcom/a/b/d/v;

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/v;-><init>(Lcom/a/b/d/u;Ljava/util/Iterator;)V

    return-object v1
.end method

.method public remove(Ljava/lang/Object;)Z
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 953
    .line 954
    invoke-virtual {p0}, Lcom/a/b/d/u;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 955
    if-eqz v0, :cond_20

    .line 956
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v2

    .line 957
    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 958
    iget-object v0, p0, Lcom/a/b/d/u;->a:Lcom/a/b/d/n;

    invoke-static {v0, v2}, Lcom/a/b/d/n;->b(Lcom/a/b/d/n;I)I

    move v0, v2

    .line 960
    :goto_1a
    if-lez v0, :cond_1e

    const/4 v0, 0x1

    :goto_1d
    return v0

    :cond_1e
    move v0, v1

    goto :goto_1d

    :cond_20
    move v0, v1

    goto :goto_1a
.end method
