.class final Lcom/a/b/d/jo;
.super Lcom/a/b/d/jl;
.source "SourceFile"


# instance fields
.field private final transient a:Lcom/a/b/d/jl;


# direct methods
.method constructor <init>(Lcom/a/b/d/jl;)V
    .registers 2

    .prologue
    .line 516
    invoke-direct {p0}, Lcom/a/b/d/jl;-><init>()V

    .line 517
    iput-object p1, p0, Lcom/a/b/d/jo;->a:Lcom/a/b/d/jl;

    .line 518
    return-void
.end method

.method private b(I)I
    .registers 3

    .prologue
    .line 521
    invoke-virtual {p0}, Lcom/a/b/d/jo;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    sub-int/2addr v0, p1

    return v0
.end method

.method private c(I)I
    .registers 3

    .prologue
    .line 525
    invoke-virtual {p0}, Lcom/a/b/d/jo;->size()I

    move-result v0

    sub-int/2addr v0, p1

    return v0
.end method


# virtual methods
.method public final a(II)Lcom/a/b/d/jl;
    .registers 6

    .prologue
    .line 547
    invoke-virtual {p0}, Lcom/a/b/d/jo;->size()I

    move-result v0

    invoke-static {p1, p2, v0}, Lcom/a/b/b/cn;->a(III)V

    .line 548
    iget-object v0, p0, Lcom/a/b/d/jo;->a:Lcom/a/b/d/jl;

    .line 1525
    invoke-virtual {p0}, Lcom/a/b/d/jo;->size()I

    move-result v1

    sub-int/2addr v1, p2

    .line 2525
    invoke-virtual {p0}, Lcom/a/b/d/jo;->size()I

    move-result v2

    sub-int/2addr v2, p1

    .line 548
    invoke-virtual {v0, v1, v2}, Lcom/a/b/d/jl;->a(II)Lcom/a/b/d/jl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/jl;->e()Lcom/a/b/d/jl;

    move-result-object v0

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 533
    iget-object v0, p0, Lcom/a/b/d/jo;->a:Lcom/a/b/d/jl;

    invoke-virtual {v0, p1}, Lcom/a/b/d/jl;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final e()Lcom/a/b/d/jl;
    .registers 2

    .prologue
    .line 529
    iget-object v0, p0, Lcom/a/b/d/jo;->a:Lcom/a/b/d/jl;

    return-object v0
.end method

.method public final get(I)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 553
    invoke-virtual {p0}, Lcom/a/b/d/jo;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 554
    iget-object v0, p0, Lcom/a/b/d/jo;->a:Lcom/a/b/d/jl;

    invoke-direct {p0, p1}, Lcom/a/b/d/jo;->b(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method final h_()Z
    .registers 2

    .prologue
    .line 562
    iget-object v0, p0, Lcom/a/b/d/jo;->a:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->h_()Z

    move-result v0

    return v0
.end method

.method public final indexOf(Ljava/lang/Object;)I
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 537
    iget-object v0, p0, Lcom/a/b/d/jo;->a:Lcom/a/b/d/jl;

    invoke-virtual {v0, p1}, Lcom/a/b/d/jl;->lastIndexOf(Ljava/lang/Object;)I

    move-result v0

    .line 538
    if-ltz v0, :cond_d

    invoke-direct {p0, v0}, Lcom/a/b/d/jo;->b(I)I

    move-result v0

    :goto_c
    return v0

    :cond_d
    const/4 v0, -0x1

    goto :goto_c
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 513
    invoke-super {p0}, Lcom/a/b/d/jl;->c()Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public final lastIndexOf(Ljava/lang/Object;)I
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 542
    iget-object v0, p0, Lcom/a/b/d/jo;->a:Lcom/a/b/d/jl;

    invoke-virtual {v0, p1}, Lcom/a/b/d/jl;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 543
    if-ltz v0, :cond_d

    invoke-direct {p0, v0}, Lcom/a/b/d/jo;->b(I)I

    move-result v0

    :goto_c
    return v0

    :cond_d
    const/4 v0, -0x1

    goto :goto_c
.end method

.method public final synthetic listIterator()Ljava/util/ListIterator;
    .registers 2

    .prologue
    .line 513
    .line 3344
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/a/b/d/jl;->a(I)Lcom/a/b/d/agj;

    move-result-object v0

    .line 513
    return-object v0
.end method

.method public final synthetic listIterator(I)Ljava/util/ListIterator;
    .registers 3

    .prologue
    .line 513
    invoke-super {p0, p1}, Lcom/a/b/d/jl;->a(I)Lcom/a/b/d/agj;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 558
    iget-object v0, p0, Lcom/a/b/d/jo;->a:Lcom/a/b/d/jl;

    invoke-virtual {v0}, Lcom/a/b/d/jl;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic subList(II)Ljava/util/List;
    .registers 4

    .prologue
    .line 513
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/jo;->a(II)Lcom/a/b/d/jl;

    move-result-object v0

    return-object v0
.end method
