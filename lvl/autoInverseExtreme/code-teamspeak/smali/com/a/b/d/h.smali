.class final Lcom/a/b/d/h;
.super Lcom/a/b/d/hi;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/Set;

.field final synthetic b:Lcom/a/b/d/a;


# direct methods
.method private constructor <init>(Lcom/a/b/d/a;)V
    .registers 3

    .prologue
    .line 226
    iput-object p1, p0, Lcom/a/b/d/h;->b:Lcom/a/b/d/a;

    invoke-direct {p0}, Lcom/a/b/d/hi;-><init>()V

    .line 227
    iget-object v0, p0, Lcom/a/b/d/h;->b:Lcom/a/b/d/a;

    iget-object v0, v0, Lcom/a/b/d/a;->a:Lcom/a/b/d/a;

    invoke-virtual {v0}, Lcom/a/b/d/a;->keySet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/h;->a:Ljava/util/Set;

    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/d/a;B)V
    .registers 3

    .prologue
    .line 226
    invoke-direct {p0, p1}, Lcom/a/b/d/h;-><init>(Lcom/a/b/d/a;)V

    return-void
.end method


# virtual methods
.method protected final a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 230
    iget-object v0, p0, Lcom/a/b/d/h;->a:Ljava/util/Set;

    return-object v0
.end method

.method protected final bridge synthetic b()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 226
    .line 2230
    iget-object v0, p0, Lcom/a/b/d/h;->a:Ljava/util/Set;

    .line 226
    return-object v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 234
    iget-object v0, p0, Lcom/a/b/d/h;->b:Lcom/a/b/d/a;

    invoke-virtual {v0}, Lcom/a/b/d/a;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->b(Ljava/util/Iterator;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 226
    .line 3230
    iget-object v0, p0, Lcom/a/b/d/h;->a:Ljava/util/Set;

    .line 226
    return-object v0
.end method

.method public final toArray()[Ljava/lang/Object;
    .registers 2

    .prologue
    .line 238
    invoke-virtual {p0}, Lcom/a/b/d/h;->p()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 3

    .prologue
    .line 242
    .line 1253
    invoke-static {p0, p1}, Lcom/a/b/d/yc;->a(Ljava/util/Collection;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    .line 242
    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 246
    invoke-virtual {p0}, Lcom/a/b/d/h;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
