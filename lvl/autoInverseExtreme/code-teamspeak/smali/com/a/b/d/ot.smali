.class final Lcom/a/b/d/ot;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/ListIterator;


# instance fields
.field final a:Ljava/lang/Object;

.field b:I

.field c:Lcom/a/b/d/or;

.field d:Lcom/a/b/d/or;

.field e:Lcom/a/b/d/or;

.field final synthetic f:Lcom/a/b/d/oj;


# direct methods
.method constructor <init>(Lcom/a/b/d/oj;Ljava/lang/Object;)V
    .registers 4
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 456
    iput-object p1, p0, Lcom/a/b/d/ot;->f:Lcom/a/b/d/oj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 457
    iput-object p2, p0, Lcom/a/b/d/ot;->a:Ljava/lang/Object;

    .line 458
    invoke-static {p1}, Lcom/a/b/d/oj;->d(Lcom/a/b/d/oj;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/oq;

    .line 459
    if-nez v0, :cond_17

    const/4 v0, 0x0

    :goto_14
    iput-object v0, p0, Lcom/a/b/d/ot;->c:Lcom/a/b/d/or;

    .line 460
    return-void

    .line 459
    :cond_17
    iget-object v0, v0, Lcom/a/b/d/oq;->a:Lcom/a/b/d/or;

    goto :goto_14
.end method

.method public constructor <init>(Lcom/a/b/d/oj;Ljava/lang/Object;I)V
    .registers 8
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 471
    iput-object p1, p0, Lcom/a/b/d/ot;->f:Lcom/a/b/d/oj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 472
    invoke-static {p1}, Lcom/a/b/d/oj;->d(Lcom/a/b/d/oj;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/oq;

    .line 473
    if-nez v0, :cond_2a

    const/4 v1, 0x0

    .line 474
    :goto_13
    invoke-static {p3, v1}, Lcom/a/b/b/cn;->b(II)I

    .line 475
    div-int/lit8 v3, v1, 0x2

    if-lt p3, v3, :cond_30

    .line 476
    if-nez v0, :cond_2d

    move-object v0, v2

    :goto_1d
    iput-object v0, p0, Lcom/a/b/d/ot;->e:Lcom/a/b/d/or;

    .line 477
    iput v1, p0, Lcom/a/b/d/ot;->b:I

    .line 478
    :goto_21
    add-int/lit8 v0, p3, 0x1

    if-ge p3, v1, :cond_41

    .line 479
    invoke-virtual {p0}, Lcom/a/b/d/ot;->previous()Ljava/lang/Object;

    move p3, v0

    goto :goto_21

    .line 473
    :cond_2a
    iget v1, v0, Lcom/a/b/d/oq;->c:I

    goto :goto_13

    .line 476
    :cond_2d
    iget-object v0, v0, Lcom/a/b/d/oq;->b:Lcom/a/b/d/or;

    goto :goto_1d

    .line 482
    :cond_30
    if-nez v0, :cond_3e

    move-object v0, v2

    :goto_33
    iput-object v0, p0, Lcom/a/b/d/ot;->c:Lcom/a/b/d/or;

    .line 483
    :goto_35
    add-int/lit8 v0, p3, -0x1

    if-lez p3, :cond_41

    .line 484
    invoke-virtual {p0}, Lcom/a/b/d/ot;->next()Ljava/lang/Object;

    move p3, v0

    goto :goto_35

    .line 482
    :cond_3e
    iget-object v0, v0, Lcom/a/b/d/oq;->a:Lcom/a/b/d/or;

    goto :goto_33

    .line 487
    :cond_41
    iput-object p2, p0, Lcom/a/b/d/ot;->a:Ljava/lang/Object;

    .line 488
    iput-object v2, p0, Lcom/a/b/d/ot;->d:Lcom/a/b/d/or;

    .line 489
    return-void
.end method


# virtual methods
.method public final add(Ljava/lang/Object;)V
    .registers 5

    .prologue
    .line 551
    iget-object v0, p0, Lcom/a/b/d/ot;->f:Lcom/a/b/d/oj;

    iget-object v1, p0, Lcom/a/b/d/ot;->a:Ljava/lang/Object;

    iget-object v2, p0, Lcom/a/b/d/ot;->c:Lcom/a/b/d/or;

    invoke-static {v0, v1, p1, v2}, Lcom/a/b/d/oj;->a(Lcom/a/b/d/oj;Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/or;)Lcom/a/b/d/or;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/ot;->e:Lcom/a/b/d/or;

    .line 552
    iget v0, p0, Lcom/a/b/d/ot;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/ot;->b:I

    .line 553
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/b/d/ot;->d:Lcom/a/b/d/or;

    .line 554
    return-void
.end method

.method public final hasNext()Z
    .registers 2

    .prologue
    .line 493
    iget-object v0, p0, Lcom/a/b/d/ot;->c:Lcom/a/b/d/or;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final hasPrevious()Z
    .registers 2

    .prologue
    .line 507
    iget-object v0, p0, Lcom/a/b/d/ot;->e:Lcom/a/b/d/or;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final next()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 498
    iget-object v0, p0, Lcom/a/b/d/ot;->c:Lcom/a/b/d/or;

    invoke-static {v0}, Lcom/a/b/d/oj;->e(Ljava/lang/Object;)V

    .line 499
    iget-object v0, p0, Lcom/a/b/d/ot;->c:Lcom/a/b/d/or;

    iput-object v0, p0, Lcom/a/b/d/ot;->d:Lcom/a/b/d/or;

    iput-object v0, p0, Lcom/a/b/d/ot;->e:Lcom/a/b/d/or;

    .line 500
    iget-object v0, p0, Lcom/a/b/d/ot;->c:Lcom/a/b/d/or;

    iget-object v0, v0, Lcom/a/b/d/or;->e:Lcom/a/b/d/or;

    iput-object v0, p0, Lcom/a/b/d/ot;->c:Lcom/a/b/d/or;

    .line 501
    iget v0, p0, Lcom/a/b/d/ot;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/ot;->b:I

    .line 502
    iget-object v0, p0, Lcom/a/b/d/ot;->d:Lcom/a/b/d/or;

    iget-object v0, v0, Lcom/a/b/d/or;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public final nextIndex()I
    .registers 2

    .prologue
    .line 521
    iget v0, p0, Lcom/a/b/d/ot;->b:I

    return v0
.end method

.method public final previous()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 512
    iget-object v0, p0, Lcom/a/b/d/ot;->e:Lcom/a/b/d/or;

    invoke-static {v0}, Lcom/a/b/d/oj;->e(Ljava/lang/Object;)V

    .line 513
    iget-object v0, p0, Lcom/a/b/d/ot;->e:Lcom/a/b/d/or;

    iput-object v0, p0, Lcom/a/b/d/ot;->d:Lcom/a/b/d/or;

    iput-object v0, p0, Lcom/a/b/d/ot;->c:Lcom/a/b/d/or;

    .line 514
    iget-object v0, p0, Lcom/a/b/d/ot;->e:Lcom/a/b/d/or;

    iget-object v0, v0, Lcom/a/b/d/or;->f:Lcom/a/b/d/or;

    iput-object v0, p0, Lcom/a/b/d/ot;->e:Lcom/a/b/d/or;

    .line 515
    iget v0, p0, Lcom/a/b/d/ot;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/b/d/ot;->b:I

    .line 516
    iget-object v0, p0, Lcom/a/b/d/ot;->d:Lcom/a/b/d/or;

    iget-object v0, v0, Lcom/a/b/d/or;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public final previousIndex()I
    .registers 2

    .prologue
    .line 526
    iget v0, p0, Lcom/a/b/d/ot;->b:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final remove()V
    .registers 3

    .prologue
    .line 531
    iget-object v0, p0, Lcom/a/b/d/ot;->d:Lcom/a/b/d/or;

    if-eqz v0, :cond_27

    const/4 v0, 0x1

    .line 1049
    :goto_5
    const-string v1, "no calls to next() since the last call to remove()"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 532
    iget-object v0, p0, Lcom/a/b/d/ot;->d:Lcom/a/b/d/or;

    iget-object v1, p0, Lcom/a/b/d/ot;->c:Lcom/a/b/d/or;

    if-eq v0, v1, :cond_29

    .line 533
    iget-object v0, p0, Lcom/a/b/d/ot;->d:Lcom/a/b/d/or;

    iget-object v0, v0, Lcom/a/b/d/or;->f:Lcom/a/b/d/or;

    iput-object v0, p0, Lcom/a/b/d/ot;->e:Lcom/a/b/d/or;

    .line 534
    iget v0, p0, Lcom/a/b/d/ot;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/b/d/ot;->b:I

    .line 538
    :goto_1c
    iget-object v0, p0, Lcom/a/b/d/ot;->f:Lcom/a/b/d/oj;

    iget-object v1, p0, Lcom/a/b/d/ot;->d:Lcom/a/b/d/or;

    invoke-static {v0, v1}, Lcom/a/b/d/oj;->a(Lcom/a/b/d/oj;Lcom/a/b/d/or;)V

    .line 539
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/b/d/ot;->d:Lcom/a/b/d/or;

    .line 540
    return-void

    .line 531
    :cond_27
    const/4 v0, 0x0

    goto :goto_5

    .line 536
    :cond_29
    iget-object v0, p0, Lcom/a/b/d/ot;->d:Lcom/a/b/d/or;

    iget-object v0, v0, Lcom/a/b/d/or;->e:Lcom/a/b/d/or;

    iput-object v0, p0, Lcom/a/b/d/ot;->c:Lcom/a/b/d/or;

    goto :goto_1c
.end method

.method public final set(Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 544
    iget-object v0, p0, Lcom/a/b/d/ot;->d:Lcom/a/b/d/or;

    if-eqz v0, :cond_d

    const/4 v0, 0x1

    :goto_5
    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    .line 545
    iget-object v0, p0, Lcom/a/b/d/ot;->d:Lcom/a/b/d/or;

    iput-object p1, v0, Lcom/a/b/d/or;->b:Ljava/lang/Object;

    .line 546
    return-void

    .line 544
    :cond_d
    const/4 v0, 0x0

    goto :goto_5
.end method
