.class public Lcom/a/b/d/kn;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lcom/a/b/d/vi;

.field b:Ljava/util/Comparator;

.field c:Ljava/util/Comparator;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    new-instance v0, Lcom/a/b/d/ko;

    invoke-direct {v0}, Lcom/a/b/d/ko;-><init>()V

    iput-object v0, p0, Lcom/a/b/d/kn;->a:Lcom/a/b/d/vi;

    .line 163
    return-void
.end method


# virtual methods
.method public a(Lcom/a/b/d/vi;)Lcom/a/b/d/kn;
    .registers 5

    .prologue
    .line 224
    invoke-interface {p1}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 225
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-virtual {p0, v2, v0}, Lcom/a/b/d/kn;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Lcom/a/b/d/kn;

    goto :goto_c

    .line 227
    :cond_26
    return-object p0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Iterable;)Lcom/a/b/d/kn;
    .registers 7

    .prologue
    .line 191
    if-nez p1, :cond_22

    .line 192
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "null key in entry: null="

    invoke-static {p2}, Lcom/a/b/d/mq;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1c

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_18
    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1c
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_18

    .line 195
    :cond_22
    iget-object v0, p0, Lcom/a/b/d/kn;->a:Lcom/a/b/d/vi;

    invoke-interface {v0, p1}, Lcom/a/b/d/vi;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    .line 196
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 197
    invoke-static {p1, v2}, Lcom/a/b/d/cl;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 198
    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2c

    .line 200
    :cond_3d
    return-object p0
.end method

.method public varargs a(Ljava/lang/Object;[Ljava/lang/Object;)Lcom/a/b/d/kn;
    .registers 4

    .prologue
    .line 210
    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/kn;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Lcom/a/b/d/kn;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/Comparator;)Lcom/a/b/d/kn;
    .registers 3

    .prologue
    .line 246
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    iput-object v0, p0, Lcom/a/b/d/kn;->c:Ljava/util/Comparator;

    .line 247
    return-object p0
.end method

.method public a(Ljava/util/Map$Entry;)Lcom/a/b/d/kn;
    .registers 4

    .prologue
    .line 180
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/a/b/d/kn;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kn;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/a/b/d/kk;
    .registers 5

    .prologue
    .line 254
    iget-object v0, p0, Lcom/a/b/d/kn;->c:Ljava/util/Comparator;

    if-eqz v0, :cond_26

    .line 255
    iget-object v0, p0, Lcom/a/b/d/kn;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_12
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 256
    check-cast v0, Ljava/util/List;

    .line 257
    iget-object v2, p0, Lcom/a/b/d/kn;->c:Ljava/util/Comparator;

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_12

    .line 260
    :cond_26
    iget-object v0, p0, Lcom/a/b/d/kn;->b:Ljava/util/Comparator;

    if-eqz v0, :cond_6e

    .line 261
    new-instance v1, Lcom/a/b/d/ko;

    invoke-direct {v1}, Lcom/a/b/d/ko;-><init>()V

    .line 262
    iget-object v0, p0, Lcom/a/b/d/kn;->a:Lcom/a/b/d/vi;

    invoke-interface {v0}, Lcom/a/b/d/vi;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/ov;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 264
    iget-object v2, p0, Lcom/a/b/d/kn;->b:Ljava/util/Comparator;

    invoke-static {v2}, Lcom/a/b/d/yd;->a(Ljava/util/Comparator;)Lcom/a/b/d/yd;

    move-result-object v2

    .line 1373
    invoke-static {}, Lcom/a/b/d/sz;->a()Lcom/a/b/b/bj;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/a/b/d/yd;->a(Lcom/a/b/b/bj;)Lcom/a/b/d/yd;

    move-result-object v2

    .line 264
    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 267
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_52
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 268
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-interface {v1, v3, v0}, Lcom/a/b/d/vi;->c(Ljava/lang/Object;Ljava/lang/Iterable;)Z

    goto :goto_52

    .line 270
    :cond_6c
    iput-object v1, p0, Lcom/a/b/d/kn;->a:Lcom/a/b/d/vi;

    .line 272
    :cond_6e
    iget-object v0, p0, Lcom/a/b/d/kn;->a:Lcom/a/b/d/vi;

    invoke-static {v0}, Lcom/a/b/d/kk;->b(Lcom/a/b/d/vi;)Lcom/a/b/d/kk;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kn;
    .registers 4

    .prologue
    .line 169
    invoke-static {p1, p2}, Lcom/a/b/d/cl;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 170
    iget-object v0, p0, Lcom/a/b/d/kn;->a:Lcom/a/b/d/vi;

    invoke-interface {v0, p1, p2}, Lcom/a/b/d/vi;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 171
    return-object p0
.end method

.method public b(Ljava/util/Comparator;)Lcom/a/b/d/kn;
    .registers 3

    .prologue
    .line 236
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    iput-object v0, p0, Lcom/a/b/d/kn;->b:Ljava/util/Comparator;

    .line 237
    return-object p0
.end method
