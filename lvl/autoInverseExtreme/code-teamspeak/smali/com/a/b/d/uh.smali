.class final Lcom/a/b/d/uh;
.super Lcom/a/b/d/tl;
.source "SourceFile"


# instance fields
.field c:Lcom/a/b/b/co;


# direct methods
.method constructor <init>(Ljava/util/Map;Lcom/a/b/b/co;Lcom/a/b/b/co;)V
    .registers 4

    .prologue
    .line 2658
    invoke-direct {p0, p1, p3}, Lcom/a/b/d/tl;-><init>(Ljava/util/Map;Lcom/a/b/b/co;)V

    .line 2659
    iput-object p2, p0, Lcom/a/b/d/uh;->c:Lcom/a/b/b/co;

    .line 2660
    return-void
.end method


# virtual methods
.method protected final a()Ljava/util/Set;
    .registers 3

    .prologue
    .line 2664
    iget-object v0, p0, Lcom/a/b/d/uh;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/uh;->b:Lcom/a/b/b/co;

    invoke-static {v0, v1}, Lcom/a/b/d/aad;->a(Ljava/util/Set;Lcom/a/b/b/co;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 2677
    iget-object v0, p0, Lcom/a/b/d/uh;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/a/b/d/uh;->c:Lcom/a/b/b/co;

    invoke-interface {v0, p1}, Lcom/a/b/b/co;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method final e()Ljava/util/Set;
    .registers 3

    .prologue
    .line 2669
    iget-object v0, p0, Lcom/a/b/d/uh;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/uh;->c:Lcom/a/b/b/co;

    invoke-static {v0, v1}, Lcom/a/b/d/aad;->a(Ljava/util/Set;Lcom/a/b/b/co;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
