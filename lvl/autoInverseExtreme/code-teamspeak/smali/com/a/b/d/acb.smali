.class final Lcom/a/b/d/acb;
.super Lcom/a/b/d/aan;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/aca;


# direct methods
.method private constructor <init>(Lcom/a/b/d/aca;)V
    .registers 2

    .prologue
    .line 447
    iput-object p1, p0, Lcom/a/b/d/acb;->a:Lcom/a/b/d/aca;

    invoke-direct {p0}, Lcom/a/b/d/aan;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/d/aca;B)V
    .registers 3

    .prologue
    .line 447
    invoke-direct {p0, p1}, Lcom/a/b/d/acb;-><init>(Lcom/a/b/d/aca;)V

    return-void
.end method


# virtual methods
.method public final clear()V
    .registers 3

    .prologue
    .line 467
    iget-object v0, p0, Lcom/a/b/d/acb;->a:Lcom/a/b/d/aca;

    invoke-static {}, Lcom/a/b/b/cp;->a()Lcom/a/b/b/co;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/d/aca;->a(Lcom/a/b/b/co;)Z

    .line 468
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 6

    .prologue
    .line 471
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-eqz v0, :cond_1b

    .line 472
    check-cast p1, Ljava/util/Map$Entry;

    .line 473
    iget-object v0, p0, Lcom/a/b/d/acb;->a:Lcom/a/b/d/aca;

    iget-object v0, v0, Lcom/a/b/d/aca;->b:Lcom/a/b/d/abx;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/a/b/d/acb;->a:Lcom/a/b/d/aca;

    iget-object v2, v2, Lcom/a/b/d/aca;->a:Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/a/b/d/abx;->a(Lcom/a/b/d/abx;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 475
    :goto_1a
    return v0

    :cond_1b
    const/4 v0, 0x0

    goto :goto_1a
.end method

.method public final isEmpty()Z
    .registers 3

    .prologue
    .line 463
    iget-object v0, p0, Lcom/a/b/d/acb;->a:Lcom/a/b/d/aca;

    iget-object v0, v0, Lcom/a/b/d/aca;->b:Lcom/a/b/d/abx;

    iget-object v1, p0, Lcom/a/b/d/acb;->a:Lcom/a/b/d/aca;

    iget-object v1, v1, Lcom/a/b/d/aca;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/a/b/d/abx;->b(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 4

    .prologue
    .line 449
    new-instance v0, Lcom/a/b/d/acc;

    iget-object v1, p0, Lcom/a/b/d/acb;->a:Lcom/a/b/d/aca;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/a/b/d/acc;-><init>(Lcom/a/b/d/aca;B)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .registers 6

    .prologue
    .line 479
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-eqz v0, :cond_1b

    .line 480
    check-cast p1, Ljava/util/Map$Entry;

    .line 481
    iget-object v0, p0, Lcom/a/b/d/acb;->a:Lcom/a/b/d/aca;

    iget-object v0, v0, Lcom/a/b/d/aca;->b:Lcom/a/b/d/abx;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/a/b/d/acb;->a:Lcom/a/b/d/aca;

    iget-object v2, v2, Lcom/a/b/d/aca;->a:Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/a/b/d/abx;->b(Lcom/a/b/d/abx;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 483
    :goto_1a
    return v0

    :cond_1b
    const/4 v0, 0x0

    goto :goto_1a
.end method

.method public final retainAll(Ljava/util/Collection;)Z
    .registers 4

    .prologue
    .line 487
    iget-object v0, p0, Lcom/a/b/d/acb;->a:Lcom/a/b/d/aca;

    invoke-static {p1}, Lcom/a/b/b/cp;->a(Ljava/util/Collection;)Lcom/a/b/b/co;

    move-result-object v1

    invoke-static {v1}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/d/aca;->a(Lcom/a/b/b/co;)Z

    move-result v0

    return v0
.end method

.method public final size()I
    .registers 5

    .prologue
    .line 453
    const/4 v0, 0x0

    .line 454
    iget-object v1, p0, Lcom/a/b/d/acb;->a:Lcom/a/b/d/aca;

    iget-object v1, v1, Lcom/a/b/d/aca;->b:Lcom/a/b/d/abx;

    iget-object v1, v1, Lcom/a/b/d/abx;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_10
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 455
    iget-object v3, p0, Lcom/a/b/d/acb;->a:Lcom/a/b/d/aca;

    iget-object v3, v3, Lcom/a/b/d/aca;->a:Ljava/lang/Object;

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 456
    add-int/lit8 v0, v1, 0x1

    :goto_28
    move v1, v0

    .line 458
    goto :goto_10

    .line 459
    :cond_2a
    return v1

    :cond_2b
    move v0, v1

    goto :goto_28
.end method
