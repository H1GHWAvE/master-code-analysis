.class final Lcom/a/b/d/vh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field final synthetic a:Lcom/a/b/d/vc;

.field private b:I

.field private c:I

.field private d:Ljava/util/Queue;

.field private e:Ljava/util/List;

.field private f:Ljava/lang/Object;

.field private g:Z


# direct methods
.method private constructor <init>(Lcom/a/b/d/vc;)V
    .registers 3

    .prologue
    .line 748
    iput-object p1, p0, Lcom/a/b/d/vh;->a:Lcom/a/b/d/vc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 749
    const/4 v0, -0x1

    iput v0, p0, Lcom/a/b/d/vh;->b:I

    .line 750
    iget-object v0, p0, Lcom/a/b/d/vh;->a:Lcom/a/b/d/vc;

    invoke-static {v0}, Lcom/a/b/d/vc;->c(Lcom/a/b/d/vc;)I

    move-result v0

    iput v0, p0, Lcom/a/b/d/vh;->c:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/d/vc;B)V
    .registers 3

    .prologue
    .line 748
    invoke-direct {p0, p1}, Lcom/a/b/d/vh;-><init>(Lcom/a/b/d/vc;)V

    return-void
.end method

.method private a(I)I
    .registers 5

    .prologue
    .line 835
    iget-object v0, p0, Lcom/a/b/d/vh;->e:Ljava/util/List;

    if-eqz v0, :cond_2c

    .line 836
    :goto_4
    iget-object v0, p0, Lcom/a/b/d/vh;->a:Lcom/a/b/d/vc;

    invoke-virtual {v0}, Lcom/a/b/d/vc;->size()I

    move-result v0

    if-ge p1, v0, :cond_2c

    iget-object v0, p0, Lcom/a/b/d/vh;->e:Ljava/util/List;

    iget-object v1, p0, Lcom/a/b/d/vh;->a:Lcom/a/b/d/vc;

    .line 2290
    iget-object v1, v1, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v1, v1, p1

    .line 2805
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_18
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 2806
    if-ne v2, v1, :cond_18

    .line 2807
    const/4 v0, 0x1

    .line 836
    :goto_25
    if-eqz v0, :cond_2c

    .line 837
    add-int/lit8 p1, p1, 0x1

    goto :goto_4

    .line 2810
    :cond_2a
    const/4 v0, 0x0

    goto :goto_25

    .line 840
    :cond_2c
    return p1
.end method

.method private a()V
    .registers 3

    .prologue
    .line 825
    iget-object v0, p0, Lcom/a/b/d/vh;->a:Lcom/a/b/d/vc;

    invoke-static {v0}, Lcom/a/b/d/vc;->c(Lcom/a/b/d/vc;)I

    move-result v0

    iget v1, p0, Lcom/a/b/d/vh;->c:I

    if-eq v0, v1, :cond_10

    .line 826
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 828
    :cond_10
    return-void
.end method

.method private static a(Ljava/lang/Iterable;Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 805
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_12

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 806
    if-ne v1, p1, :cond_4

    .line 807
    const/4 v0, 0x1

    .line 810
    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method private a(Ljava/lang/Object;)Z
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 815
    move v0, v1

    :goto_2
    iget-object v2, p0, Lcom/a/b/d/vh;->a:Lcom/a/b/d/vc;

    invoke-static {v2}, Lcom/a/b/d/vc;->b(Lcom/a/b/d/vc;)I

    move-result v2

    if-ge v0, v2, :cond_1a

    .line 816
    iget-object v2, p0, Lcom/a/b/d/vh;->a:Lcom/a/b/d/vc;

    invoke-static {v2}, Lcom/a/b/d/vc;->a(Lcom/a/b/d/vc;)[Ljava/lang/Object;

    move-result-object v2

    aget-object v2, v2, v0

    if-ne v2, p1, :cond_1b

    .line 817
    iget-object v1, p0, Lcom/a/b/d/vh;->a:Lcom/a/b/d/vc;

    invoke-virtual {v1, v0}, Lcom/a/b/d/vc;->a(I)Lcom/a/b/d/vg;

    .line 818
    const/4 v1, 0x1

    .line 821
    :cond_1a
    return v1

    .line 815
    :cond_1b
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method


# virtual methods
.method public final hasNext()Z
    .registers 3

    .prologue
    .line 757
    invoke-direct {p0}, Lcom/a/b/d/vh;->a()V

    .line 758
    iget v0, p0, Lcom/a/b/d/vh;->b:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/a/b/d/vh;->a(I)I

    move-result v0

    iget-object v1, p0, Lcom/a/b/d/vh;->a:Lcom/a/b/d/vc;

    invoke-virtual {v1}, Lcom/a/b/d/vc;->size()I

    move-result v1

    if-lt v0, v1, :cond_1f

    iget-object v0, p0, Lcom/a/b/d/vh;->d:Ljava/util/Queue;

    if-eqz v0, :cond_21

    iget-object v0, p0, Lcom/a/b/d/vh;->d:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_21

    :cond_1f
    const/4 v0, 0x1

    :goto_20
    return v0

    :cond_21
    const/4 v0, 0x0

    goto :goto_20
.end method

.method public final next()Ljava/lang/Object;
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 763
    invoke-direct {p0}, Lcom/a/b/d/vh;->a()V

    .line 764
    iget v0, p0, Lcom/a/b/d/vh;->b:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/a/b/d/vh;->a(I)I

    move-result v0

    .line 765
    iget-object v1, p0, Lcom/a/b/d/vh;->a:Lcom/a/b/d/vc;

    invoke-virtual {v1}, Lcom/a/b/d/vc;->size()I

    move-result v1

    if-ge v0, v1, :cond_21

    .line 766
    iput v0, p0, Lcom/a/b/d/vh;->b:I

    .line 767
    iput-boolean v2, p0, Lcom/a/b/d/vh;->g:Z

    .line 768
    iget-object v0, p0, Lcom/a/b/d/vh;->a:Lcom/a/b/d/vc;

    iget v1, p0, Lcom/a/b/d/vh;->b:I

    .line 1290
    iget-object v0, v0, Lcom/a/b/d/vc;->b:[Ljava/lang/Object;

    aget-object v0, v0, v1

    .line 774
    :goto_20
    return-object v0

    .line 769
    :cond_21
    iget-object v0, p0, Lcom/a/b/d/vh;->d:Ljava/util/Queue;

    if-eqz v0, :cond_3e

    .line 770
    iget-object v0, p0, Lcom/a/b/d/vh;->a:Lcom/a/b/d/vc;

    invoke-virtual {v0}, Lcom/a/b/d/vc;->size()I

    move-result v0

    iput v0, p0, Lcom/a/b/d/vh;->b:I

    .line 771
    iget-object v0, p0, Lcom/a/b/d/vh;->d:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/vh;->f:Ljava/lang/Object;

    .line 772
    iget-object v0, p0, Lcom/a/b/d/vh;->f:Ljava/lang/Object;

    if-eqz v0, :cond_3e

    .line 773
    iput-boolean v2, p0, Lcom/a/b/d/vh;->g:Z

    .line 774
    iget-object v0, p0, Lcom/a/b/d/vh;->f:Ljava/lang/Object;

    goto :goto_20

    .line 777
    :cond_3e
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "iterator moved past last element in queue."

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final remove()V
    .registers 4

    .prologue
    .line 782
    iget-boolean v0, p0, Lcom/a/b/d/vh;->g:Z

    .line 2049
    const-string v1, "no calls to next() since the last call to remove()"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 783
    invoke-direct {p0}, Lcom/a/b/d/vh;->a()V

    .line 784
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/a/b/d/vh;->g:Z

    .line 785
    iget v0, p0, Lcom/a/b/d/vh;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/b/d/vh;->c:I

    .line 786
    iget v0, p0, Lcom/a/b/d/vh;->b:I

    iget-object v1, p0, Lcom/a/b/d/vh;->a:Lcom/a/b/d/vc;

    invoke-virtual {v1}, Lcom/a/b/d/vc;->size()I

    move-result v1

    if-ge v0, v1, :cond_4f

    .line 787
    iget-object v0, p0, Lcom/a/b/d/vh;->a:Lcom/a/b/d/vc;

    iget v1, p0, Lcom/a/b/d/vh;->b:I

    invoke-virtual {v0, v1}, Lcom/a/b/d/vc;->a(I)Lcom/a/b/d/vg;

    move-result-object v0

    .line 788
    if-eqz v0, :cond_48

    .line 789
    iget-object v1, p0, Lcom/a/b/d/vh;->d:Ljava/util/Queue;

    if-nez v1, :cond_3a

    .line 790
    new-instance v1, Ljava/util/ArrayDeque;

    invoke-direct {v1}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v1, p0, Lcom/a/b/d/vh;->d:Ljava/util/Queue;

    .line 791
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/a/b/d/vh;->e:Ljava/util/List;

    .line 793
    :cond_3a
    iget-object v1, p0, Lcom/a/b/d/vh;->d:Ljava/util/Queue;

    iget-object v2, v0, Lcom/a/b/d/vg;->a:Ljava/lang/Object;

    invoke-interface {v1, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 794
    iget-object v1, p0, Lcom/a/b/d/vh;->e:Ljava/util/List;

    iget-object v0, v0, Lcom/a/b/d/vg;->b:Ljava/lang/Object;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 796
    :cond_48
    iget v0, p0, Lcom/a/b/d/vh;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/b/d/vh;->b:I

    .line 801
    :goto_4e
    return-void

    .line 798
    :cond_4f
    iget-object v0, p0, Lcom/a/b/d/vh;->f:Ljava/lang/Object;

    invoke-direct {p0, v0}, Lcom/a/b/d/vh;->a(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/b/cn;->b(Z)V

    .line 799
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/b/d/vh;->f:Ljava/lang/Object;

    goto :goto_4e
.end method
