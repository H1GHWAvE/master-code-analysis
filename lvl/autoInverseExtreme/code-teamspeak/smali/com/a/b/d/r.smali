.class final Lcom/a/b/d/r;
.super Lcom/a/b/d/tu;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/d/q;


# direct methods
.method constructor <init>(Lcom/a/b/d/q;)V
    .registers 2

    .prologue
    .line 1310
    iput-object p1, p0, Lcom/a/b/d/r;->a:Lcom/a/b/d/q;

    invoke-direct {p0}, Lcom/a/b/d/tu;-><init>()V

    return-void
.end method


# virtual methods
.method final a()Ljava/util/Map;
    .registers 2

    .prologue
    .line 1313
    iget-object v0, p0, Lcom/a/b/d/r;->a:Lcom/a/b/d/q;

    return-object v0
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 1323
    iget-object v0, p0, Lcom/a/b/d/r;->a:Lcom/a/b/d/q;

    iget-object v0, v0, Lcom/a/b/d/q;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/a/b/d/cm;->a(Ljava/util/Collection;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 1317
    new-instance v0, Lcom/a/b/d/s;

    iget-object v1, p0, Lcom/a/b/d/r;->a:Lcom/a/b/d/q;

    invoke-direct {v0, v1}, Lcom/a/b/d/s;-><init>(Lcom/a/b/d/q;)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .registers 4

    .prologue
    .line 1327
    invoke-virtual {p0, p1}, Lcom/a/b/d/r;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1328
    const/4 v0, 0x0

    .line 1332
    :goto_7
    return v0

    .line 1330
    :cond_8
    check-cast p1, Ljava/util/Map$Entry;

    .line 1331
    iget-object v0, p0, Lcom/a/b/d/r;->a:Lcom/a/b/d/q;

    iget-object v0, v0, Lcom/a/b/d/q;->b:Lcom/a/b/d/n;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/n;->a(Lcom/a/b/d/n;Ljava/lang/Object;)I

    .line 1332
    const/4 v0, 0x1

    goto :goto_7
.end method
