.class final Lcom/a/b/d/em;
.super Lcom/a/b/d/me;
.source "SourceFile"


# instance fields
.field private final a:Lcom/a/b/d/me;


# direct methods
.method constructor <init>(Lcom/a/b/d/me;)V
    .registers 3

    .prologue
    .line 32
    invoke-virtual {p1}, Lcom/a/b/d/me;->comparator()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/yd;->a(Ljava/util/Comparator;)Lcom/a/b/d/yd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/yd;->a()Lcom/a/b/d/yd;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/d/me;-><init>(Ljava/util/Comparator;)V

    .line 33
    iput-object p1, p0, Lcom/a/b/d/em;->a:Lcom/a/b/d/me;

    .line 34
    return-void
.end method


# virtual methods
.method final a(Ljava/lang/Object;Z)Lcom/a/b/d/me;
    .registers 4

    .prologue
    .line 59
    iget-object v0, p0, Lcom/a/b/d/em;->a:Lcom/a/b/d/me;

    invoke-virtual {v0, p1, p2}, Lcom/a/b/d/me;->d(Ljava/lang/Object;Z)Lcom/a/b/d/me;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/me;->b()Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method final a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/a/b/d/me;
    .registers 6

    .prologue
    .line 54
    iget-object v0, p0, Lcom/a/b/d/em;->a:Lcom/a/b/d/me;

    invoke-virtual {v0, p3, p4, p1, p2}, Lcom/a/b/d/me;->b(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/a/b/d/me;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/me;->b()Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/a/b/d/me;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/a/b/d/em;->a:Lcom/a/b/d/me;

    return-object v0
.end method

.method final b(Ljava/lang/Object;Z)Lcom/a/b/d/me;
    .registers 4

    .prologue
    .line 48
    iget-object v0, p0, Lcom/a/b/d/em;->a:Lcom/a/b/d/me;

    invoke-virtual {v0, p1, p2}, Lcom/a/b/d/me;->c(Ljava/lang/Object;Z)Lcom/a/b/d/me;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/me;->b()Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method final c(Ljava/lang/Object;)I
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 102
    iget-object v0, p0, Lcom/a/b/d/em;->a:Lcom/a/b/d/me;

    invoke-virtual {v0, p1}, Lcom/a/b/d/me;->c(Ljava/lang/Object;)I

    move-result v0

    .line 103
    const/4 v1, -0x1

    if-ne v0, v1, :cond_a

    .line 106
    :goto_9
    return v0

    :cond_a
    invoke-virtual {p0}, Lcom/a/b/d/em;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    sub-int v0, v1, v0

    goto :goto_9
.end method

.method public final c()Lcom/a/b/d/agi;
    .registers 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/a/b/d/em;->a:Lcom/a/b/d/me;

    invoke-virtual {v0}, Lcom/a/b/d/me;->d()Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public final ceiling(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 92
    iget-object v0, p0, Lcom/a/b/d/em;->a:Lcom/a/b/d/me;

    invoke-virtual {v0, p1}, Lcom/a/b/d/me;->floor(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/a/b/d/agi;
    .registers 2
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lcom/a/b/d/em;->a:Lcom/a/b/d/me;

    invoke-virtual {v0}, Lcom/a/b/d/me;->c()Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic descendingIterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 28
    .line 1071
    iget-object v0, p0, Lcom/a/b/d/em;->a:Lcom/a/b/d/me;

    invoke-virtual {v0}, Lcom/a/b/d/me;->c()Lcom/a/b/d/agi;

    move-result-object v0

    .line 28
    return-object v0
.end method

.method public final bridge synthetic descendingSet()Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 28
    .line 2065
    iget-object v0, p0, Lcom/a/b/d/em;->a:Lcom/a/b/d/me;

    .line 28
    return-object v0
.end method

.method final e()Lcom/a/b/d/me;
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "NavigableSet"
    .end annotation

    .prologue
    .line 77
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "should never be called"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public final floor(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 87
    iget-object v0, p0, Lcom/a/b/d/em;->a:Lcom/a/b/d/me;

    invoke-virtual {v0, p1}, Lcom/a/b/d/me;->ceiling(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method final h_()Z
    .registers 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/a/b/d/em;->a:Lcom/a/b/d/me;

    invoke-virtual {v0}, Lcom/a/b/d/me;->h_()Z

    move-result v0

    return v0
.end method

.method public final higher(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 97
    iget-object v0, p0, Lcom/a/b/d/em;->a:Lcom/a/b/d/me;

    invoke-virtual {v0, p1}, Lcom/a/b/d/me;->lower(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 28
    .line 3043
    iget-object v0, p0, Lcom/a/b/d/em;->a:Lcom/a/b/d/me;

    invoke-virtual {v0}, Lcom/a/b/d/me;->d()Lcom/a/b/d/agi;

    move-result-object v0

    .line 28
    return-object v0
.end method

.method public final lower(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 82
    iget-object v0, p0, Lcom/a/b/d/em;->a:Lcom/a/b/d/me;

    invoke-virtual {v0, p1}, Lcom/a/b/d/me;->higher(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/a/b/d/em;->a:Lcom/a/b/d/me;

    invoke-virtual {v0}, Lcom/a/b/d/me;->size()I

    move-result v0

    return v0
.end method
