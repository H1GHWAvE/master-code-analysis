.class final Lcom/a/b/d/hc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field final synthetic a:Lcom/a/b/d/hb;

.field private b:Ljava/util/Map$Entry;

.field private c:Ljava/util/Map$Entry;


# direct methods
.method constructor <init>(Lcom/a/b/d/hb;)V
    .registers 3

    .prologue
    .line 285
    iput-object p1, p0, Lcom/a/b/d/hc;->a:Lcom/a/b/d/hb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 286
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/b/d/hc;->b:Ljava/util/Map$Entry;

    .line 287
    iget-object v0, p0, Lcom/a/b/d/hc;->a:Lcom/a/b/d/hb;

    .line 1280
    iget-object v0, v0, Lcom/a/b/d/hb;->a:Lcom/a/b/d/ha;

    .line 287
    invoke-interface {v0}, Ljava/util/NavigableMap;->lastEntry()Ljava/util/Map$Entry;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/hc;->c:Ljava/util/Map$Entry;

    return-void
.end method

.method private a()Ljava/util/Map$Entry;
    .registers 4

    .prologue
    .line 296
    invoke-virtual {p0}, Lcom/a/b/d/hc;->hasNext()Z

    move-result v0

    if-nez v0, :cond_c

    .line 297
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 300
    :cond_c
    :try_start_c
    iget-object v0, p0, Lcom/a/b/d/hc;->c:Ljava/util/Map$Entry;
    :try_end_e
    .catchall {:try_start_c .. :try_end_e} :catchall_23

    .line 302
    iget-object v1, p0, Lcom/a/b/d/hc;->c:Ljava/util/Map$Entry;

    iput-object v1, p0, Lcom/a/b/d/hc;->b:Ljava/util/Map$Entry;

    .line 303
    iget-object v1, p0, Lcom/a/b/d/hc;->a:Lcom/a/b/d/hb;

    .line 2280
    iget-object v1, v1, Lcom/a/b/d/hb;->a:Lcom/a/b/d/ha;

    .line 303
    iget-object v2, p0, Lcom/a/b/d/hc;->c:Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/NavigableMap;->lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v1

    iput-object v1, p0, Lcom/a/b/d/hc;->c:Ljava/util/Map$Entry;

    return-object v0

    .line 302
    :catchall_23
    move-exception v0

    iget-object v1, p0, Lcom/a/b/d/hc;->c:Ljava/util/Map$Entry;

    iput-object v1, p0, Lcom/a/b/d/hc;->b:Ljava/util/Map$Entry;

    .line 303
    iget-object v1, p0, Lcom/a/b/d/hc;->a:Lcom/a/b/d/hb;

    .line 3280
    iget-object v1, v1, Lcom/a/b/d/hb;->a:Lcom/a/b/d/ha;

    .line 303
    iget-object v2, p0, Lcom/a/b/d/hc;->c:Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/NavigableMap;->lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v1

    iput-object v1, p0, Lcom/a/b/d/hc;->c:Ljava/util/Map$Entry;

    throw v0
.end method


# virtual methods
.method public final hasNext()Z
    .registers 2

    .prologue
    .line 291
    iget-object v0, p0, Lcom/a/b/d/hc;->c:Ljava/util/Map$Entry;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final synthetic next()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 285
    invoke-direct {p0}, Lcom/a/b/d/hc;->a()Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .registers 3

    .prologue
    .line 309
    iget-object v0, p0, Lcom/a/b/d/hc;->b:Ljava/util/Map$Entry;

    if-eqz v0, :cond_1b

    const/4 v0, 0x1

    .line 4049
    :goto_5
    const-string v1, "no calls to next() since the last call to remove()"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 310
    iget-object v0, p0, Lcom/a/b/d/hc;->a:Lcom/a/b/d/hb;

    .line 4280
    iget-object v0, v0, Lcom/a/b/d/hb;->a:Lcom/a/b/d/ha;

    .line 310
    iget-object v1, p0, Lcom/a/b/d/hc;->b:Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/NavigableMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 311
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/b/d/hc;->b:Ljava/util/Map$Entry;

    .line 312
    return-void

    .line 309
    :cond_1b
    const/4 v0, 0x0

    goto :goto_5
.end method
