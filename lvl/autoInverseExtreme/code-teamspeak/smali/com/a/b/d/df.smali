.class final Lcom/a/b/d/df;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/sr;


# instance fields
.field final a:Lcom/a/b/b/bj;

.field volatile b:Lcom/a/b/d/sr;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ComputingValueReference.this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/a/b/b/bj;)V
    .registers 3

    .prologue
    .line 291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 288
    invoke-static {}, Lcom/a/b/d/qy;->g()Lcom/a/b/d/sr;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/df;->b:Lcom/a/b/d/sr;

    .line 292
    iput-object p1, p0, Lcom/a/b/d/df;->a:Lcom/a/b/b/bj;

    .line 293
    return-void
.end method

.method private b(Lcom/a/b/d/sr;)V
    .registers 4

    .prologue
    .line 367
    monitor-enter p0

    .line 368
    :try_start_1
    iget-object v0, p0, Lcom/a/b/d/df;->b:Lcom/a/b/d/sr;

    sget-object v1, Lcom/a/b/d/qy;->x:Lcom/a/b/d/sr;

    if-ne v0, v1, :cond_c

    .line 369
    iput-object p1, p0, Lcom/a/b/d/df;->b:Lcom/a/b/d/sr;

    .line 370
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 372
    :cond_c
    monitor-exit p0

    return-void

    :catchall_e
    move-exception v0

    monitor-exit p0
    :try_end_10
    .catchall {:try_start_1 .. :try_end_10} :catchall_e

    throw v0
.end method


# virtual methods
.method public final a()Lcom/a/b/d/rz;
    .registers 2

    .prologue
    .line 304
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;Lcom/a/b/d/rz;)Lcom/a/b/d/sr;
    .registers 4
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 310
    return-object p0
.end method

.method final a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 356
    :try_start_0
    iget-object v0, p0, Lcom/a/b/d/df;->a:Lcom/a/b/b/bj;

    invoke-interface {v0, p1}, Lcom/a/b/b/bj;->e(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_5} :catch_f

    move-result-object v0

    .line 362
    new-instance v1, Lcom/a/b/d/dc;

    invoke-direct {v1, v0}, Lcom/a/b/d/dc;-><init>(Ljava/lang/Object;)V

    invoke-direct {p0, v1}, Lcom/a/b/d/df;->b(Lcom/a/b/d/sr;)V

    .line 363
    return-object v0

    .line 357
    :catch_f
    move-exception v0

    .line 358
    new-instance v1, Lcom/a/b/d/db;

    invoke-direct {v1, v0}, Lcom/a/b/d/db;-><init>(Ljava/lang/Throwable;)V

    invoke-direct {p0, v1}, Lcom/a/b/d/df;->b(Lcom/a/b/d/sr;)V

    .line 359
    new-instance v1, Ljava/util/concurrent/ExecutionException;

    invoke-direct {v1, v0}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/a/b/d/sr;)V
    .registers 2

    .prologue
    .line 348
    invoke-direct {p0, p1}, Lcom/a/b/d/df;->b(Lcom/a/b/d/sr;)V

    .line 351
    return-void
.end method

.method public final b()Z
    .registers 2

    .prologue
    .line 315
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Ljava/lang/Object;
    .registers 4

    .prologue
    .line 323
    iget-object v0, p0, Lcom/a/b/d/df;->b:Lcom/a/b/d/sr;

    sget-object v1, Lcom/a/b/d/qy;->x:Lcom/a/b/d/sr;

    if-ne v0, v1, :cond_1f

    .line 324
    const/4 v1, 0x0

    .line 326
    :try_start_7
    monitor-enter p0
    :try_end_8
    .catchall {:try_start_7 .. :try_end_8} :catchall_29

    .line 327
    :goto_8
    :try_start_8
    iget-object v0, p0, Lcom/a/b/d/df;->b:Lcom/a/b/d/sr;

    sget-object v2, Lcom/a/b/d/qy;->x:Lcom/a/b/d/sr;
    :try_end_c
    .catchall {:try_start_8 .. :try_end_c} :catchall_26

    if-ne v0, v2, :cond_15

    .line 329
    :try_start_e
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_11
    .catch Ljava/lang/InterruptedException; {:try_start_e .. :try_end_11} :catch_12
    .catchall {:try_start_e .. :try_end_11} :catchall_26

    goto :goto_8

    .line 331
    :catch_12
    move-exception v0

    const/4 v1, 0x1

    .line 332
    goto :goto_8

    .line 334
    :cond_15
    :try_start_15
    monitor-exit p0
    :try_end_16
    .catchall {:try_start_15 .. :try_end_16} :catchall_26

    .line 336
    if-eqz v1, :cond_1f

    .line 337
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 341
    :cond_1f
    iget-object v0, p0, Lcom/a/b/d/df;->b:Lcom/a/b/d/sr;

    invoke-interface {v0}, Lcom/a/b/d/sr;->c()Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 334
    :catchall_26
    move-exception v0

    :try_start_27
    monitor-exit p0
    :try_end_28
    .catchall {:try_start_27 .. :try_end_28} :catchall_26

    :try_start_28
    throw v0
    :try_end_29
    .catchall {:try_start_28 .. :try_end_29} :catchall_29

    .line 336
    :catchall_29
    move-exception v0

    if-eqz v1, :cond_33

    .line 337
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    :cond_33
    throw v0
.end method

.method public final get()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 299
    const/4 v0, 0x0

    return-object v0
.end method
