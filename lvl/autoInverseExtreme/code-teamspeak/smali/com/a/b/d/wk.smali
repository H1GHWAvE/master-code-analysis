.class final Lcom/a/b/d/wk;
.super Lcom/a/b/d/ba;
.source "SourceFile"


# static fields
.field private static final b:J
    .annotation build Lcom/a/b/a/c;
        a = "not needed in emulated source"
    .end annotation
.end field


# instance fields
.field transient a:Lcom/a/b/b/dz;


# direct methods
.method constructor <init>(Ljava/util/Map;Lcom/a/b/b/dz;)V
    .registers 4

    .prologue
    .line 281
    invoke-direct {p0, p1}, Lcom/a/b/d/ba;-><init>(Ljava/util/Map;)V

    .line 282
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/dz;

    iput-object v0, p0, Lcom/a/b/d/wk;->a:Lcom/a/b/b/dz;

    .line 283
    return-void
.end method

.method private a(Ljava/io/ObjectInputStream;)V
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectInputStream"
    .end annotation

    .prologue
    .line 301
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 302
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/dz;

    iput-object v0, p0, Lcom/a/b/d/wk;->a:Lcom/a/b/b/dz;

    .line 303
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 304
    invoke-virtual {p0, v0}, Lcom/a/b/d/wk;->a(Ljava/util/Map;)V

    .line 305
    return-void
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
    .registers 3
    .annotation build Lcom/a/b/a/c;
        a = "java.io.ObjectOutputStream"
    .end annotation

    .prologue
    .line 292
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 293
    iget-object v0, p0, Lcom/a/b/d/wk;->a:Lcom/a/b/b/dz;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 294
    invoke-virtual {p0}, Lcom/a/b/d/wk;->e()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 295
    return-void
.end method


# virtual methods
.method protected final a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 286
    iget-object v0, p0, Lcom/a/b/d/wk;->a:Lcom/a/b/b/dz;

    invoke-interface {v0}, Lcom/a/b/b/dz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method protected final synthetic c()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 275
    invoke-virtual {p0}, Lcom/a/b/d/wk;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
