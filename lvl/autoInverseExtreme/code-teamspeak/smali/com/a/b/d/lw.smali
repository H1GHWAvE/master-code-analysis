.class public abstract Lcom/a/b/d/lw;
.super Lcom/a/b/d/lz;
.source "SourceFile"

# interfaces
.implements Ljava/util/NavigableMap;


# annotations
.annotation build Lcom/a/b/a/b;
    a = true
    b = true
.end annotation


# static fields
.field private static final a:Ljava/util/Comparator;

.field private static final b:Lcom/a/b/d/lw;

.field private static final d:J


# instance fields
.field private transient c:Lcom/a/b/d/lw;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 65
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    sput-object v0, Lcom/a/b/d/lw;->a:Ljava/util/Comparator;

    .line 67
    new-instance v0, Lcom/a/b/d/fa;

    sget-object v1, Lcom/a/b/d/lw;->a:Ljava/util/Comparator;

    invoke-direct {v0, v1}, Lcom/a/b/d/fa;-><init>(Ljava/util/Comparator;)V

    sput-object v0, Lcom/a/b/d/lw;->b:Lcom/a/b/d/lw;

    return-void
.end method

.method constructor <init>()V
    .registers 1

    .prologue
    .line 412
    invoke-direct {p0}, Lcom/a/b/d/lz;-><init>()V

    .line 413
    return-void
.end method

.method constructor <init>(Lcom/a/b/d/lw;)V
    .registers 2

    .prologue
    .line 415
    invoke-direct {p0}, Lcom/a/b/d/lz;-><init>()V

    .line 416
    iput-object p1, p0, Lcom/a/b/d/lw;->c:Lcom/a/b/d/lw;

    .line 417
    return-void
.end method

.method static a(Lcom/a/b/d/me;Lcom/a/b/d/jl;)Lcom/a/b/d/lw;
    .registers 3

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/a/b/d/me;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 102
    invoke-virtual {p0}, Lcom/a/b/d/me;->comparator()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/lw;->a(Ljava/util/Comparator;)Lcom/a/b/d/lw;

    move-result-object v0

    .line 104
    :goto_e
    return-object v0

    :cond_f
    new-instance v0, Lcom/a/b/d/zl;

    check-cast p0, Lcom/a/b/d/zq;

    invoke-direct {v0, p0, p1}, Lcom/a/b/d/zl;-><init>(Lcom/a/b/d/zq;Lcom/a/b/d/jl;)V

    goto :goto_e
.end method

.method private static a(Ljava/lang/Comparable;Ljava/lang/Object;)Lcom/a/b/d/lw;
    .registers 4

    .prologue
    .line 125
    invoke-static {p0}, Lcom/a/b/d/me;->a(Ljava/lang/Comparable;)Lcom/a/b/d/me;

    move-result-object v0

    invoke-static {p1}, Lcom/a/b/d/jl;->a(Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/a/b/d/lw;->a(Lcom/a/b/d/me;Lcom/a/b/d/jl;)Lcom/a/b/d/lw;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Comparable;Ljava/lang/Object;Ljava/lang/Comparable;Ljava/lang/Object;)Lcom/a/b/d/lw;
    .registers 10

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 138
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    new-array v1, v5, [Ljava/util/Map$Entry;

    invoke-static {p0, p1}, Lcom/a/b/d/lw;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x1

    invoke-static {p2, p3}, Lcom/a/b/d/lw;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v4, v5, v1}, Lcom/a/b/d/lw;->a(Ljava/util/Comparator;ZI[Ljava/util/Map$Entry;)Lcom/a/b/d/lw;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Comparable;Ljava/lang/Object;Ljava/lang/Comparable;Ljava/lang/Object;Ljava/lang/Comparable;Ljava/lang/Object;)Lcom/a/b/d/lw;
    .registers 12

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    .line 151
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    new-array v1, v5, [Ljava/util/Map$Entry;

    invoke-static {p0, p1}, Lcom/a/b/d/lw;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x1

    invoke-static {p2, p3}, Lcom/a/b/d/lw;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {p4, p5}, Lcom/a/b/d/lw;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v4, v5, v1}, Lcom/a/b/d/lw;->a(Ljava/util/Comparator;ZI[Ljava/util/Map$Entry;)Lcom/a/b/d/lw;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Comparable;Ljava/lang/Object;Ljava/lang/Comparable;Ljava/lang/Object;Ljava/lang/Comparable;Ljava/lang/Object;Ljava/lang/Comparable;Ljava/lang/Object;)Lcom/a/b/d/lw;
    .registers 14

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 165
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    new-array v1, v5, [Ljava/util/Map$Entry;

    invoke-static {p0, p1}, Lcom/a/b/d/lw;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x1

    invoke-static {p2, p3}, Lcom/a/b/d/lw;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {p4, p5}, Lcom/a/b/d/lw;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-static {p6, p7}, Lcom/a/b/d/lw;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v4, v5, v1}, Lcom/a/b/d/lw;->a(Ljava/util/Comparator;ZI[Ljava/util/Map$Entry;)Lcom/a/b/d/lw;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Comparable;Ljava/lang/Object;Ljava/lang/Comparable;Ljava/lang/Object;Ljava/lang/Comparable;Ljava/lang/Object;Ljava/lang/Comparable;Ljava/lang/Object;Ljava/lang/Comparable;Ljava/lang/Object;)Lcom/a/b/d/lw;
    .registers 16

    .prologue
    const/4 v5, 0x5

    const/4 v4, 0x0

    .line 179
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    new-array v1, v5, [Ljava/util/Map$Entry;

    invoke-static {p0, p1}, Lcom/a/b/d/lw;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x1

    invoke-static {p2, p3}, Lcom/a/b/d/lw;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {p4, p5}, Lcom/a/b/d/lw;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-static {p6, p7}, Lcom/a/b/d/lw;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    invoke-static {p8, p9}, Lcom/a/b/d/lw;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v4, v5, v1}, Lcom/a/b/d/lw;->a(Ljava/util/Comparator;ZI[Ljava/util/Map$Entry;)Lcom/a/b/d/lw;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Object;)Lcom/a/b/d/lw;
    .registers 3

    .prologue
    .line 484
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/lw;->a(Ljava/lang/Object;Z)Lcom/a/b/d/lw;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/lw;
    .registers 5

    .prologue
    .line 517
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, p2, v1}, Lcom/a/b/d/lw;->a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/a/b/d/lw;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/a/b/d/lw;
    .registers 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 538
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 539
    invoke-static {p3}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 540
    invoke-virtual {p0}, Lcom/a/b/d/lw;->comparator()Ljava/util/Comparator;

    move-result-object v0

    invoke-interface {v0, p1, p3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_28

    move v0, v1

    :goto_13
    const-string v3, "expected fromKey <= toKey but %s > %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v2

    aput-object p3, v4, v1

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 542
    invoke-virtual {p0, p3, p4}, Lcom/a/b/d/lw;->a(Ljava/lang/Object;Z)Lcom/a/b/d/lw;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/a/b/d/lw;->b(Ljava/lang/Object;Z)Lcom/a/b/d/lw;

    move-result-object v0

    return-object v0

    :cond_28
    move v0, v2

    .line 540
    goto :goto_13
.end method

.method static a(Ljava/util/Comparator;)Lcom/a/b/d/lw;
    .registers 2

    .prologue
    .line 71
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1117
    sget-object v0, Lcom/a/b/d/lw;->b:Lcom/a/b/d/lw;

    .line 74
    :goto_c
    return-object v0

    :cond_d
    new-instance v0, Lcom/a/b/d/fa;

    invoke-direct {v0, p0}, Lcom/a/b/d/fa;-><init>(Ljava/util/Comparator;)V

    goto :goto_c
.end method

.method private static a(Ljava/util/Comparator;I[Ljava/util/Map$Entry;)Lcom/a/b/d/lw;
    .registers 8

    .prologue
    .line 82
    if-nez p1, :cond_7

    .line 83
    invoke-static {p0}, Lcom/a/b/d/lw;->a(Ljava/util/Comparator;)Lcom/a/b/d/lw;

    move-result-object v0

    .line 94
    :goto_6
    return-object v0

    .line 86
    :cond_7
    invoke-static {}, Lcom/a/b/d/jl;->h()Lcom/a/b/d/jn;

    move-result-object v1

    .line 87
    invoke-static {}, Lcom/a/b/d/jl;->h()Lcom/a/b/d/jn;

    move-result-object v2

    .line 88
    const/4 v0, 0x0

    :goto_10
    if-ge v0, p1, :cond_25

    .line 89
    aget-object v3, p2, v0

    .line 90
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/a/b/d/jn;->c(Ljava/lang/Object;)Lcom/a/b/d/jn;

    .line 91
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/a/b/d/jn;->c(Ljava/lang/Object;)Lcom/a/b/d/jn;

    .line 88
    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    .line 94
    :cond_25
    new-instance v0, Lcom/a/b/d/zl;

    new-instance v3, Lcom/a/b/d/zq;

    invoke-virtual {v1}, Lcom/a/b/d/jn;->b()Lcom/a/b/d/jl;

    move-result-object v1

    invoke-direct {v3, v1, p0}, Lcom/a/b/d/zq;-><init>(Lcom/a/b/d/jl;Ljava/util/Comparator;)V

    invoke-virtual {v2}, Lcom/a/b/d/jn;->b()Lcom/a/b/d/jl;

    move-result-object v1

    invoke-direct {v0, v3, v1}, Lcom/a/b/d/zl;-><init>(Lcom/a/b/d/zq;Lcom/a/b/d/jl;)V

    goto :goto_6
.end method

.method static varargs a(Ljava/util/Comparator;ZI[Ljava/util/Map$Entry;)Lcom/a/b/d/lw;
    .registers 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 280
    move v0, v2

    :goto_3
    if-ge v0, p2, :cond_18

    .line 281
    aget-object v3, p3, v0

    .line 282
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/a/b/d/lw;->d(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kb;

    move-result-object v3

    aput-object v3, p3, v0

    .line 280
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 284
    :cond_18
    if-nez p1, :cond_52

    .line 1294
    invoke-static {p0}, Lcom/a/b/d/yd;->a(Ljava/util/Comparator;)Lcom/a/b/d/yd;

    move-result-object v0

    .line 1373
    invoke-static {}, Lcom/a/b/d/sz;->a()Lcom/a/b/b/bj;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/a/b/d/yd;->a(Lcom/a/b/b/bj;)Lcom/a/b/d/yd;

    move-result-object v0

    .line 1294
    invoke-static {p3, v2, p2, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;IILjava/util/Comparator;)V

    move v3, v1

    .line 2299
    :goto_2a
    if-ge v3, p2, :cond_52

    .line 2300
    add-int/lit8 v0, v3, -0x1

    aget-object v0, p3, v0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    aget-object v4, p3, v3

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {p0, v0, v4}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-eqz v0, :cond_50

    move v0, v1

    :goto_41
    const-string v4, "key"

    add-int/lit8 v5, v3, -0x1

    aget-object v5, p3, v5

    aget-object v6, p3, v3

    invoke-static {v0, v4, v5, v6}, Lcom/a/b/d/lw;->a(ZLjava/lang/String;Ljava/util/Map$Entry;Ljava/util/Map$Entry;)V

    .line 2299
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2a

    :cond_50
    move v0, v2

    .line 2300
    goto :goto_41

    .line 3082
    :cond_52
    if-nez p2, :cond_59

    .line 3083
    invoke-static {p0}, Lcom/a/b/d/lw;->a(Ljava/util/Comparator;)Lcom/a/b/d/lw;

    move-result-object v0

    :goto_58
    return-object v0

    .line 3086
    :cond_59
    invoke-static {}, Lcom/a/b/d/jl;->h()Lcom/a/b/d/jn;

    move-result-object v1

    .line 3087
    invoke-static {}, Lcom/a/b/d/jl;->h()Lcom/a/b/d/jn;

    move-result-object v3

    .line 3088
    :goto_61
    if-ge v2, p2, :cond_76

    .line 3089
    aget-object v0, p3, v2

    .line 3090
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/a/b/d/jn;->c(Ljava/lang/Object;)Lcom/a/b/d/jn;

    .line 3091
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/a/b/d/jn;->c(Ljava/lang/Object;)Lcom/a/b/d/jn;

    .line 3088
    add-int/lit8 v2, v2, 0x1

    goto :goto_61

    .line 3094
    :cond_76
    new-instance v0, Lcom/a/b/d/zl;

    new-instance v2, Lcom/a/b/d/zq;

    invoke-virtual {v1}, Lcom/a/b/d/jn;->b()Lcom/a/b/d/jl;

    move-result-object v1

    invoke-direct {v2, v1, p0}, Lcom/a/b/d/zq;-><init>(Lcom/a/b/d/jl;Ljava/util/Comparator;)V

    invoke-virtual {v3}, Lcom/a/b/d/jn;->b()Lcom/a/b/d/jl;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lcom/a/b/d/zl;-><init>(Lcom/a/b/d/zq;Lcom/a/b/d/jl;)V

    goto :goto_58
.end method

.method private static a(Ljava/util/Map;Ljava/util/Comparator;)Lcom/a/b/d/lw;
    .registers 3

    .prologue
    .line 223
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    invoke-static {p0, v0}, Lcom/a/b/d/lw;->b(Ljava/util/Map;Ljava/util/Comparator;)Lcom/a/b/d/lw;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/SortedMap;)Lcom/a/b/d/lw;
    .registers 2

    .prologue
    .line 239
    invoke-interface {p0}, Ljava/util/SortedMap;->comparator()Ljava/util/Comparator;

    move-result-object v0

    .line 240
    if-nez v0, :cond_8

    .line 243
    sget-object v0, Lcom/a/b/d/lw;->a:Ljava/util/Comparator;

    .line 245
    :cond_8
    invoke-static {p0, v0}, Lcom/a/b/d/lw;->b(Ljava/util/Map;Ljava/util/Comparator;)Lcom/a/b/d/lw;

    move-result-object v0

    return-object v0
.end method

.method private static a(I[Ljava/util/Map$Entry;Ljava/util/Comparator;)V
    .registers 9

    .prologue
    const/4 v1, 0x1

    .line 299
    move v2, v1

    :goto_2
    if-ge v2, p0, :cond_2a

    .line 300
    add-int/lit8 v0, v2, -0x1

    aget-object v0, p1, v0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    aget-object v3, p1, v2

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p2, v0, v3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-eqz v0, :cond_28

    move v0, v1

    :goto_19
    const-string v3, "key"

    add-int/lit8 v4, v2, -0x1

    aget-object v4, p1, v4

    aget-object v5, p1, v2

    invoke-static {v0, v3, v4, v5}, Lcom/a/b/d/lw;->a(ZLjava/lang/String;Ljava/util/Map$Entry;Ljava/util/Map$Entry;)V

    .line 299
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 300
    :cond_28
    const/4 v0, 0x0

    goto :goto_19

    .line 303
    :cond_2a
    return-void
.end method

.method private b(Ljava/lang/Object;)Lcom/a/b/d/lw;
    .registers 3

    .prologue
    .line 557
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/lw;->b(Ljava/lang/Object;Z)Lcom/a/b/d/lw;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/util/Map;)Lcom/a/b/d/lw;
    .registers 2

    .prologue
    .line 205
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v0

    .line 206
    invoke-static {p0, v0}, Lcom/a/b/d/lw;->b(Ljava/util/Map;Ljava/util/Comparator;)Lcom/a/b/d/lw;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/util/Map;Ljava/util/Comparator;)Lcom/a/b/d/lw;
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 250
    .line 251
    instance-of v0, p0, Ljava/util/SortedMap;

    if-eqz v0, :cond_3d

    move-object v0, p0

    .line 252
    check-cast v0, Ljava/util/SortedMap;

    .line 253
    invoke-interface {v0}, Ljava/util/SortedMap;->comparator()Ljava/util/Comparator;

    move-result-object v0

    .line 254
    if-nez v0, :cond_26

    sget-object v0, Lcom/a/b/d/lw;->a:Ljava/util/Comparator;

    if-ne p1, v0, :cond_24

    const/4 v0, 0x1

    :goto_13
    move v2, v0

    .line 259
    :goto_14
    if-eqz v2, :cond_2b

    instance-of v0, p0, Lcom/a/b/d/lw;

    if-eqz v0, :cond_2b

    move-object v0, p0

    .line 263
    check-cast v0, Lcom/a/b/d/lw;

    .line 264
    invoke-virtual {v0}, Lcom/a/b/d/lw;->i_()Z

    move-result v3

    if-nez v3, :cond_2b

    .line 275
    :goto_23
    return-object v0

    :cond_24
    move v0, v1

    .line 254
    goto :goto_13

    :cond_26
    invoke-interface {p1, v0}, Ljava/util/Comparator;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_13

    .line 273
    :cond_2b
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    new-array v1, v1, [Ljava/util/Map$Entry;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/util/Map$Entry;

    .line 275
    array-length v1, v0

    invoke-static {p1, v2, v1, v0}, Lcom/a/b/d/lw;->a(Ljava/util/Comparator;ZI[Ljava/util/Map$Entry;)Lcom/a/b/d/lw;

    move-result-object v0

    goto :goto_23

    :cond_3d
    move v2, v1

    goto :goto_14
.end method

.method private static b(Ljava/util/Comparator;)Lcom/a/b/d/lx;
    .registers 2

    .prologue
    .line 323
    new-instance v0, Lcom/a/b/d/lx;

    invoke-direct {v0, p0}, Lcom/a/b/d/lx;-><init>(Ljava/util/Comparator;)V

    return-object v0
.end method

.method private static b(Ljava/util/Comparator;I[Ljava/util/Map$Entry;)V
    .registers 6

    .prologue
    .line 294
    const/4 v0, 0x0

    invoke-static {p0}, Lcom/a/b/d/yd;->a(Ljava/util/Comparator;)Lcom/a/b/d/yd;

    move-result-object v1

    .line 3373
    invoke-static {}, Lcom/a/b/d/sz;->a()Lcom/a/b/b/bj;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/a/b/d/yd;->a(Lcom/a/b/b/bj;)Lcom/a/b/d/yd;

    move-result-object v1

    .line 294
    invoke-static {p2, v0, p1, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;IILjava/util/Comparator;)V

    .line 295
    return-void
.end method

.method public static m()Lcom/a/b/d/lw;
    .registers 1

    .prologue
    .line 117
    sget-object v0, Lcom/a/b/d/lw;->b:Lcom/a/b/d/lw;

    return-object v0
.end method

.method private static n()Lcom/a/b/d/lx;
    .registers 2

    .prologue
    .line 311
    new-instance v0, Lcom/a/b/d/lx;

    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/lx;-><init>(Ljava/util/Comparator;)V

    return-object v0
.end method

.method private static o()Lcom/a/b/d/lx;
    .registers 2

    .prologue
    .line 331
    new-instance v0, Lcom/a/b/d/lx;

    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/b/d/yd;->a()Lcom/a/b/d/yd;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/d/lx;-><init>(Ljava/util/Comparator;)V

    return-object v0
.end method

.method private p()Lcom/a/b/d/lw;
    .registers 2

    .prologue
    .line 654
    iget-object v0, p0, Lcom/a/b/d/lw;->c:Lcom/a/b/d/lw;

    .line 655
    if-nez v0, :cond_a

    .line 656
    invoke-virtual {p0}, Lcom/a/b/d/lw;->i()Lcom/a/b/d/lw;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/lw;->c:Lcom/a/b/d/lw;

    .line 658
    :cond_a
    return-object v0
.end method

.method private q()Lcom/a/b/d/me;
    .registers 2

    .prologue
    .line 665
    invoke-virtual {p0}, Lcom/a/b/d/lw;->a()Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method private r()Lcom/a/b/d/me;
    .registers 2

    .prologue
    .line 670
    invoke-virtual {p0}, Lcom/a/b/d/lw;->a()Lcom/a/b/d/me;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/me;->b()Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abstract a(Ljava/lang/Object;Z)Lcom/a/b/d/lw;
.end method

.method public abstract a()Lcom/a/b/d/me;
.end method

.method public abstract b(Ljava/lang/Object;Z)Lcom/a/b/d/lw;
.end method

.method public ceilingEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;
    .registers 3

    .prologue
    .line 598
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/lw;->b(Ljava/lang/Object;Z)Lcom/a/b/d/lw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lw;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public ceilingKey(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 603
    invoke-virtual {p0, p1}, Lcom/a/b/d/lw;->ceilingEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->b(Ljava/util/Map$Entry;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 459
    invoke-virtual {p0}, Lcom/a/b/d/lw;->a()Lcom/a/b/d/me;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/me;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 425
    invoke-virtual {p0}, Lcom/a/b/d/lw;->h()Lcom/a/b/d/iz;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/b/d/iz;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public synthetic descendingKeySet()Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 58
    .line 4670
    invoke-virtual {p0}, Lcom/a/b/d/lw;->a()Lcom/a/b/d/me;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/me;->b()Lcom/a/b/d/me;

    move-result-object v0

    .line 58
    return-object v0
.end method

.method public synthetic descendingMap()Ljava/util/NavigableMap;
    .registers 2

    .prologue
    .line 58
    .line 6654
    iget-object v0, p0, Lcom/a/b/d/lw;->c:Lcom/a/b/d/lw;

    .line 6655
    if-nez v0, :cond_a

    .line 6656
    invoke-virtual {p0}, Lcom/a/b/d/lw;->i()Lcom/a/b/d/lw;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/d/lw;->c:Lcom/a/b/d/lw;

    .line 58
    :cond_a
    return-object v0
.end method

.method public e()Lcom/a/b/d/lo;
    .registers 2

    .prologue
    .line 437
    invoke-super {p0}, Lcom/a/b/d/lz;->e()Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method public synthetic entrySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/a/b/d/lw;->e()Lcom/a/b/d/lo;

    move-result-object v0

    return-object v0
.end method

.method public firstEntry()Ljava/util/Map$Entry;
    .registers 3

    .prologue
    .line 618
    invoke-virtual {p0}, Lcom/a/b/d/lw;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x0

    :goto_7
    return-object v0

    :cond_8
    invoke-virtual {p0}, Lcom/a/b/d/lw;->e()Lcom/a/b/d/lo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lo;->f()Lcom/a/b/d/jl;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    goto :goto_7
.end method

.method public firstKey()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 464
    invoke-virtual {p0}, Lcom/a/b/d/lw;->a()Lcom/a/b/d/me;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/me;->first()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;
    .registers 3

    .prologue
    .line 588
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/lw;->a(Ljava/lang/Object;Z)Lcom/a/b/d/lw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lw;->lastEntry()Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public floorKey(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 593
    invoke-virtual {p0, p1}, Lcom/a/b/d/lw;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->b(Ljava/util/Map$Entry;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public synthetic g()Lcom/a/b/d/lo;
    .registers 2

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/a/b/d/lw;->a()Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method public abstract h()Lcom/a/b/d/iz;
.end method

.method public synthetic headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 4

    .prologue
    .line 58
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/lw;->a(Ljava/lang/Object;Z)Lcom/a/b/d/lw;

    move-result-object v0

    return-object v0
.end method

.method public synthetic headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 3

    .prologue
    .line 58
    .line 4484
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/lw;->a(Ljava/lang/Object;Z)Lcom/a/b/d/lw;

    move-result-object v0

    .line 58
    return-object v0
.end method

.method public higherEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;
    .registers 3

    .prologue
    .line 608
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/lw;->b(Ljava/lang/Object;Z)Lcom/a/b/d/lw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lw;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public higherKey(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 613
    invoke-virtual {p0, p1}, Lcom/a/b/d/lw;->higherEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->b(Ljava/util/Map$Entry;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method abstract i()Lcom/a/b/d/lw;
.end method

.method i_()Z
    .registers 2

    .prologue
    .line 429
    invoke-virtual {p0}, Lcom/a/b/d/lw;->a()Lcom/a/b/d/me;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/me;->h_()Z

    move-result v0

    if-nez v0, :cond_14

    invoke-virtual {p0}, Lcom/a/b/d/lw;->h()Lcom/a/b/d/iz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/iz;->h_()Z

    move-result v0

    if-eqz v0, :cond_16

    :cond_14
    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method final j()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 694
    new-instance v0, Lcom/a/b/d/ly;

    invoke-direct {v0, p0}, Lcom/a/b/d/ly;-><init>(Lcom/a/b/d/lw;)V

    return-object v0
.end method

.method public synthetic keySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/a/b/d/lw;->a()Lcom/a/b/d/me;

    move-result-object v0

    return-object v0
.end method

.method public lastEntry()Ljava/util/Map$Entry;
    .registers 3

    .prologue
    .line 623
    invoke-virtual {p0}, Lcom/a/b/d/lw;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x0

    :goto_7
    return-object v0

    :cond_8
    invoke-virtual {p0}, Lcom/a/b/d/lw;->e()Lcom/a/b/d/lo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lo;->f()Lcom/a/b/d/jl;

    move-result-object v0

    invoke-virtual {p0}, Lcom/a/b/d/lw;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/a/b/d/jl;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    goto :goto_7
.end method

.method public lastKey()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 469
    invoke-virtual {p0}, Lcom/a/b/d/lw;->a()Lcom/a/b/d/me;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/me;->last()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;
    .registers 3

    .prologue
    .line 578
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/lw;->a(Ljava/lang/Object;Z)Lcom/a/b/d/lw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/lw;->lastEntry()Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public lowerKey(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 583
    invoke-virtual {p0, p1}, Lcom/a/b/d/lw;->lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/d/sz;->b(Ljava/util/Map$Entry;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public synthetic navigableKeySet()Ljava/util/NavigableSet;
    .registers 2

    .prologue
    .line 58
    .line 5665
    invoke-virtual {p0}, Lcom/a/b/d/lw;->a()Lcom/a/b/d/me;

    move-result-object v0

    .line 58
    return-object v0
.end method

.method public final pollFirstEntry()Ljava/util/Map$Entry;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 635
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final pollLastEntry()Ljava/util/Map$Entry;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 647
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public size()I
    .registers 2

    .prologue
    .line 421
    invoke-virtual {p0}, Lcom/a/b/d/lw;->h()Lcom/a/b/d/iz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/d/iz;->size()I

    move-result v0

    return v0
.end method

.method public synthetic subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 6

    .prologue
    .line 58
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/a/b/d/lw;->a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/a/b/d/lw;

    move-result-object v0

    return-object v0
.end method

.method public synthetic subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 5

    .prologue
    .line 58
    .line 4517
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, p2, v1}, Lcom/a/b/d/lw;->a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/a/b/d/lw;

    move-result-object v0

    .line 58
    return-object v0
.end method

.method public synthetic tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
    .registers 4

    .prologue
    .line 58
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/lw;->b(Ljava/lang/Object;Z)Lcom/a/b/d/lw;

    move-result-object v0

    return-object v0
.end method

.method public synthetic tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 3

    .prologue
    .line 58
    .line 3557
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/a/b/d/lw;->b(Ljava/lang/Object;Z)Lcom/a/b/d/lw;

    move-result-object v0

    .line 58
    return-object v0
.end method

.method public synthetic values()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/a/b/d/lw;->h()Lcom/a/b/d/iz;

    move-result-object v0

    return-object v0
.end method
