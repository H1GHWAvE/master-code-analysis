.class final Lcom/a/b/d/aj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field a:Ljava/util/Map$Entry;

.field final synthetic b:Ljava/util/Iterator;

.field final synthetic c:Lcom/a/b/d/ai;


# direct methods
.method constructor <init>(Lcom/a/b/d/ai;Ljava/util/Iterator;)V
    .registers 3

    .prologue
    .line 89
    iput-object p1, p0, Lcom/a/b/d/aj;->c:Lcom/a/b/d/ai;

    iput-object p2, p0, Lcom/a/b/d/aj;->b:Ljava/util/Iterator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Lcom/a/b/d/xd;
    .registers 3

    .prologue
    .line 99
    iget-object v0, p0, Lcom/a/b/d/aj;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 100
    iput-object v0, p0, Lcom/a/b/d/aj;->a:Ljava/util/Map$Entry;

    .line 101
    new-instance v1, Lcom/a/b/d/ak;

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/ak;-><init>(Lcom/a/b/d/aj;Ljava/util/Map$Entry;)V

    return-object v1
.end method


# virtual methods
.method public final hasNext()Z
    .registers 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/a/b/d/aj;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public final synthetic next()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 89
    .line 1099
    iget-object v0, p0, Lcom/a/b/d/aj;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1100
    iput-object v0, p0, Lcom/a/b/d/aj;->a:Ljava/util/Map$Entry;

    .line 1101
    new-instance v1, Lcom/a/b/d/ak;

    invoke-direct {v1, p0, v0}, Lcom/a/b/d/ak;-><init>(Lcom/a/b/d/aj;Ljava/util/Map$Entry;)V

    .line 89
    return-object v1
.end method

.method public final remove()V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 122
    iget-object v0, p0, Lcom/a/b/d/aj;->a:Ljava/util/Map$Entry;

    if-eqz v0, :cond_26

    const/4 v0, 0x1

    .line 1049
    :goto_6
    const-string v2, "no calls to next() since the last call to remove()"

    invoke-static {v0, v2}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 123
    iget-object v2, p0, Lcom/a/b/d/aj;->c:Lcom/a/b/d/ai;

    iget-object v0, p0, Lcom/a/b/d/aj;->a:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/dv;

    invoke-virtual {v0, v1}, Lcom/a/b/d/dv;->b(I)I

    move-result v0

    int-to-long v0, v0

    invoke-static {v2, v0, v1}, Lcom/a/b/d/ai;->a(Lcom/a/b/d/ai;J)J

    .line 124
    iget-object v0, p0, Lcom/a/b/d/aj;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 125
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/b/d/aj;->a:Ljava/util/Map$Entry;

    .line 126
    return-void

    :cond_26
    move v0, v1

    .line 122
    goto :goto_6
.end method
