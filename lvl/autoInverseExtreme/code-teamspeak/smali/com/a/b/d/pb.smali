.class Lcom/a/b/d/pb;
.super Ljava/util/AbstractList;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/List;

.field final b:I


# direct methods
.method constructor <init>(Ljava/util/List;I)V
    .registers 3

    .prologue
    .line 663
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 664
    iput-object p1, p0, Lcom/a/b/d/pb;->a:Ljava/util/List;

    .line 665
    iput p2, p0, Lcom/a/b/d/pb;->b:I

    .line 666
    return-void
.end method

.method private a(I)Ljava/util/List;
    .registers 5

    .prologue
    .line 669
    invoke-virtual {p0}, Lcom/a/b/d/pb;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 670
    iget v0, p0, Lcom/a/b/d/pb;->b:I

    mul-int/2addr v0, p1

    .line 671
    iget v1, p0, Lcom/a/b/d/pb;->b:I

    add-int/2addr v1, v0

    iget-object v2, p0, Lcom/a/b/d/pb;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 672
    iget-object v2, p0, Lcom/a/b/d/pb;->a:Ljava/util/List;

    invoke-interface {v2, v0, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public synthetic get(I)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 659
    .line 1669
    invoke-virtual {p0}, Lcom/a/b/d/pb;->size()I

    move-result v0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 1670
    iget v0, p0, Lcom/a/b/d/pb;->b:I

    mul-int/2addr v0, p1

    .line 1671
    iget v1, p0, Lcom/a/b/d/pb;->b:I

    add-int/2addr v1, v0

    iget-object v2, p0, Lcom/a/b/d/pb;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1672
    iget-object v2, p0, Lcom/a/b/d/pb;->a:Ljava/util/List;

    invoke-interface {v2, v0, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 659
    return-object v0
.end method

.method public isEmpty()Z
    .registers 2

    .prologue
    .line 680
    iget-object v0, p0, Lcom/a/b/d/pb;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public size()I
    .registers 4

    .prologue
    .line 676
    iget-object v0, p0, Lcom/a/b/d/pb;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/a/b/d/pb;->b:I

    sget-object v2, Ljava/math/RoundingMode;->CEILING:Ljava/math/RoundingMode;

    invoke-static {v0, v1, v2}, Lcom/a/b/j/g;->a(IILjava/math/RoundingMode;)I

    move-result v0

    return v0
.end method
