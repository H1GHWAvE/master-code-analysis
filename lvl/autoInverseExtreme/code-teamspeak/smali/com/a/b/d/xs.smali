.class final Lcom/a/b/d/xs;
.super Lcom/a/b/d/as;
.source "SourceFile"


# instance fields
.field final a:Lcom/a/b/d/xc;

.field final b:Lcom/a/b/b/co;


# direct methods
.method constructor <init>(Lcom/a/b/d/xc;Lcom/a/b/b/co;)V
    .registers 4

    .prologue
    .line 282
    invoke-direct {p0}, Lcom/a/b/d/as;-><init>()V

    .line 283
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/xc;

    iput-object v0, p0, Lcom/a/b/d/xs;->a:Lcom/a/b/d/xc;

    .line 284
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/b/co;

    iput-object v0, p0, Lcom/a/b/d/xs;->b:Lcom/a/b/b/co;

    .line 285
    return-void
.end method

.method private g()Lcom/a/b/d/agi;
    .registers 3

    .prologue
    .line 289
    iget-object v0, p0, Lcom/a/b/d/xs;->a:Lcom/a/b/d/xc;

    invoke-interface {v0}, Lcom/a/b/d/xc;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/xs;->b:Lcom/a/b/b/co;

    invoke-static {v0, v1}, Lcom/a/b/d/nj;->b(Ljava/util/Iterator;Lcom/a/b/b/co;)Lcom/a/b/d/agi;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .registers 5
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 319
    iget-object v0, p0, Lcom/a/b/d/xs;->a:Lcom/a/b/d/xc;

    invoke-interface {v0, p1}, Lcom/a/b/d/xc;->a(Ljava/lang/Object;)I

    move-result v0

    .line 320
    if-lez v0, :cond_14

    .line 323
    iget-object v2, p0, Lcom/a/b/d/xs;->b:Lcom/a/b/b/co;

    invoke-interface {v2, p1}, Lcom/a/b/b/co;->a(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 325
    :goto_11
    return v0

    :cond_12
    move v0, v1

    .line 323
    goto :goto_11

    :cond_14
    move v0, v1

    .line 325
    goto :goto_11
.end method

.method public final a(Ljava/lang/Object;I)I
    .registers 8
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 330
    iget-object v0, p0, Lcom/a/b/d/xs;->b:Lcom/a/b/b/co;

    invoke-interface {v0, p1}, Lcom/a/b/b/co;->a(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "Element %s does not match predicate %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/a/b/d/xs;->b:Lcom/a/b/b/co;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 332
    iget-object v0, p0, Lcom/a/b/d/xs;->a:Lcom/a/b/d/xc;

    invoke-interface {v0, p1, p2}, Lcom/a/b/d/xc;->a(Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;I)I
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 337
    const-string v0, "occurrences"

    invoke-static {p2, v0}, Lcom/a/b/d/cl;->a(ILjava/lang/String;)I

    .line 338
    if-nez p2, :cond_c

    .line 339
    invoke-virtual {p0, p1}, Lcom/a/b/d/xs;->a(Ljava/lang/Object;)I

    move-result v0

    .line 341
    :goto_b
    return v0

    :cond_c
    invoke-virtual {p0, p1}, Lcom/a/b/d/xs;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/a/b/d/xs;->a:Lcom/a/b/d/xc;

    invoke-interface {v0, p1, p2}, Lcom/a/b/d/xc;->b(Ljava/lang/Object;I)I

    move-result v0

    goto :goto_b

    :cond_19
    const/4 v0, 0x0

    goto :goto_b
.end method

.method final b()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 309
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "should never be called"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method final c()I
    .registers 2

    .prologue
    .line 314
    invoke-virtual {p0}, Lcom/a/b/d/xs;->n_()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method

.method public final clear()V
    .registers 2

    .prologue
    .line 347
    invoke-virtual {p0}, Lcom/a/b/d/xs;->n_()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 348
    return-void
.end method

.method final e()Ljava/util/Set;
    .registers 3

    .prologue
    .line 294
    iget-object v0, p0, Lcom/a/b/d/xs;->a:Lcom/a/b/d/xc;

    invoke-interface {v0}, Lcom/a/b/d/xc;->n_()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/xs;->b:Lcom/a/b/b/co;

    invoke-static {v0, v1}, Lcom/a/b/d/aad;->a(Ljava/util/Set;Lcom/a/b/b/co;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method final f()Ljava/util/Set;
    .registers 3

    .prologue
    .line 299
    iget-object v0, p0, Lcom/a/b/d/xs;->a:Lcom/a/b/d/xc;

    invoke-interface {v0}, Lcom/a/b/d/xc;->a()Ljava/util/Set;

    move-result-object v0

    new-instance v1, Lcom/a/b/d/xt;

    invoke-direct {v1, p0}, Lcom/a/b/d/xt;-><init>(Lcom/a/b/d/xs;)V

    invoke-static {v0, v1}, Lcom/a/b/d/aad;->a(Ljava/util/Set;Lcom/a/b/b/co;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 278
    .line 1289
    iget-object v0, p0, Lcom/a/b/d/xs;->a:Lcom/a/b/d/xc;

    invoke-interface {v0}, Lcom/a/b/d/xc;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/d/xs;->b:Lcom/a/b/b/co;

    invoke-static {v0, v1}, Lcom/a/b/d/nj;->b(Ljava/util/Iterator;Lcom/a/b/b/co;)Lcom/a/b/d/agi;

    move-result-object v0

    .line 278
    return-object v0
.end method
