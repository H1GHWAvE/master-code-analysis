.class Lcom/a/b/d/oy;
.super Ljava/util/AbstractList;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/List;


# direct methods
.method constructor <init>(Ljava/util/List;)V
    .registers 3

    .prologue
    .line 1033
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 1034
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/a/b/d/oy;->a:Ljava/util/List;

    .line 1035
    return-void
.end method


# virtual methods
.method public add(ILjava/lang/Object;)V
    .registers 4

    .prologue
    .line 1038
    iget-object v0, p0, Lcom/a/b/d/oy;->a:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1039
    return-void
.end method

.method public addAll(ILjava/util/Collection;)Z
    .registers 4

    .prologue
    .line 1042
    iget-object v0, p0, Lcom/a/b/d/oy;->a:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 1058
    iget-object v0, p0, Lcom/a/b/d/oy;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public get(I)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1046
    iget-object v0, p0, Lcom/a/b/d/oy;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove(I)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 1050
    iget-object v0, p0, Lcom/a/b/d/oy;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public set(ILjava/lang/Object;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 1054
    iget-object v0, p0, Lcom/a/b/d/oy;->a:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .registers 2

    .prologue
    .line 1062
    iget-object v0, p0, Lcom/a/b/d/oy;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
