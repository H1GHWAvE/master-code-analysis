.class public abstract Lcom/a/b/d/hq;
.super Lcom/a/b/d/hj;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/d/abs;


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# direct methods
.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/a/b/d/hj;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a()Lcom/a/b/d/aac;
    .registers 2

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/a/b/d/hq;->d()Lcom/a/b/d/abs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;)Ljava/util/Set;
    .registers 3

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcom/a/b/d/hq;->h(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;
    .registers 4

    .prologue
    .line 35
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/hq;->d(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 4

    .prologue
    .line 35
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/hq;->d(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;)Ljava/util/Set;
    .registers 3

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcom/a/b/d/hq;->i(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic c()Lcom/a/b/d/vi;
    .registers 2

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/a/b/d/hq;->d()Lcom/a/b/d/abs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcom/a/b/d/hq;->h(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method protected abstract d()Lcom/a/b/d/abs;
.end method

.method public final synthetic d(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcom/a/b/d/hq;->i(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/SortedSet;
    .registers 4

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/a/b/d/hq;->d()Lcom/a/b/d/abs;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/d/abs;->d(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final d_()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/a/b/d/hq;->d()Lcom/a/b/d/abs;

    move-result-object v0

    invoke-interface {v0}, Lcom/a/b/d/abs;->d_()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public final h(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/a/b/d/hq;->d()Lcom/a/b/d/abs;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/abs;->h(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final i(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/a/b/d/hq;->d()Lcom/a/b/d/abs;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/d/abs;->i(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/a/b/d/hq;->d()Lcom/a/b/d/abs;

    move-result-object v0

    return-object v0
.end method
