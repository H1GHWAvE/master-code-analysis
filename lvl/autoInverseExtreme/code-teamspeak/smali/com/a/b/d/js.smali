.class public final Lcom/a/b/d/js;
.super Lcom/a/b/d/kn;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 164
    invoke-direct {p0}, Lcom/a/b/d/kn;-><init>()V

    return-void
.end method

.method private b(Lcom/a/b/d/vi;)Lcom/a/b/d/js;
    .registers 2

    .prologue
    .line 194
    invoke-super {p0, p1}, Lcom/a/b/d/kn;->a(Lcom/a/b/d/vi;)Lcom/a/b/d/kn;

    .line 195
    return-object p0
.end method

.method private b(Ljava/lang/Object;Ljava/lang/Iterable;)Lcom/a/b/d/js;
    .registers 3

    .prologue
    .line 183
    invoke-super {p0, p1, p2}, Lcom/a/b/d/kn;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Lcom/a/b/d/kn;

    .line 184
    return-object p0
.end method

.method private varargs b(Ljava/lang/Object;[Ljava/lang/Object;)Lcom/a/b/d/js;
    .registers 3

    .prologue
    .line 188
    invoke-super {p0, p1, p2}, Lcom/a/b/d/kn;->a(Ljava/lang/Object;[Ljava/lang/Object;)Lcom/a/b/d/kn;

    .line 189
    return-object p0
.end method

.method private b(Ljava/util/Map$Entry;)Lcom/a/b/d/js;
    .registers 2

    .prologue
    .line 178
    invoke-super {p0, p1}, Lcom/a/b/d/kn;->a(Ljava/util/Map$Entry;)Lcom/a/b/d/kn;

    .line 179
    return-object p0
.end method

.method private c(Ljava/util/Comparator;)Lcom/a/b/d/js;
    .registers 2

    .prologue
    .line 205
    invoke-super {p0, p1}, Lcom/a/b/d/kn;->b(Ljava/util/Comparator;)Lcom/a/b/d/kn;

    .line 206
    return-object p0
.end method

.method private d(Ljava/util/Comparator;)Lcom/a/b/d/js;
    .registers 2

    .prologue
    .line 216
    invoke-super {p0, p1}, Lcom/a/b/d/kn;->a(Ljava/util/Comparator;)Lcom/a/b/d/kn;

    .line 217
    return-object p0
.end method


# virtual methods
.method public final a()Lcom/a/b/d/jr;
    .registers 2

    .prologue
    .line 224
    invoke-super {p0}, Lcom/a/b/d/kn;->b()Lcom/a/b/d/kk;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/jr;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
    .registers 3

    .prologue
    .line 167
    invoke-super {p0, p1, p2}, Lcom/a/b/d/kn;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kn;

    .line 168
    return-object p0
.end method

.method public final bridge synthetic a(Lcom/a/b/d/vi;)Lcom/a/b/d/kn;
    .registers 2

    .prologue
    .line 158
    .line 4194
    invoke-super {p0, p1}, Lcom/a/b/d/kn;->a(Lcom/a/b/d/vi;)Lcom/a/b/d/kn;

    .line 158
    return-object p0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Iterable;)Lcom/a/b/d/kn;
    .registers 3

    .prologue
    .line 158
    .line 6183
    invoke-super {p0, p1, p2}, Lcom/a/b/d/kn;->a(Ljava/lang/Object;Ljava/lang/Iterable;)Lcom/a/b/d/kn;

    .line 158
    return-object p0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;[Ljava/lang/Object;)Lcom/a/b/d/kn;
    .registers 3

    .prologue
    .line 158
    .line 5188
    invoke-super {p0, p1, p2}, Lcom/a/b/d/kn;->a(Ljava/lang/Object;[Ljava/lang/Object;)Lcom/a/b/d/kn;

    .line 158
    return-object p0
.end method

.method public final bridge synthetic a(Ljava/util/Comparator;)Lcom/a/b/d/kn;
    .registers 2

    .prologue
    .line 158
    .line 2216
    invoke-super {p0, p1}, Lcom/a/b/d/kn;->a(Ljava/util/Comparator;)Lcom/a/b/d/kn;

    .line 158
    return-object p0
.end method

.method public final bridge synthetic a(Ljava/util/Map$Entry;)Lcom/a/b/d/kn;
    .registers 2

    .prologue
    .line 158
    .line 7178
    invoke-super {p0, p1}, Lcom/a/b/d/kn;->a(Ljava/util/Map$Entry;)Lcom/a/b/d/kn;

    .line 158
    return-object p0
.end method

.method public final bridge synthetic b()Lcom/a/b/d/kk;
    .registers 2

    .prologue
    .line 158
    .line 1224
    invoke-super {p0}, Lcom/a/b/d/kn;->b()Lcom/a/b/d/kk;

    move-result-object v0

    check-cast v0, Lcom/a/b/d/jr;

    .line 158
    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kn;
    .registers 4

    .prologue
    .line 158
    invoke-virtual {p0, p1, p2}, Lcom/a/b/d/js;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic b(Ljava/util/Comparator;)Lcom/a/b/d/kn;
    .registers 2

    .prologue
    .line 158
    .line 3205
    invoke-super {p0, p1}, Lcom/a/b/d/kn;->b(Ljava/util/Comparator;)Lcom/a/b/d/kn;

    .line 158
    return-object p0
.end method
