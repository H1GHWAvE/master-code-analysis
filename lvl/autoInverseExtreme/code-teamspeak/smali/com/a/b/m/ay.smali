.class final Lcom/a/b/m/ay;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/a/b/b/bj;

.field private static final b:Lcom/a/b/b/bv;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 56
    new-instance v0, Lcom/a/b/m/az;

    invoke-direct {v0}, Lcom/a/b/m/az;-><init>()V

    sput-object v0, Lcom/a/b/m/ay;->a:Lcom/a/b/b/bj;

    .line 63
    const-string v0, ", "

    invoke-static {v0}, Lcom/a/b/b/bv;->a(Ljava/lang/String;)Lcom/a/b/b/bv;

    move-result-object v0

    const-string v1, "null"

    invoke-virtual {v0, v1}, Lcom/a/b/b/bv;->b(Ljava/lang/String;)Lcom/a/b/b/bv;

    move-result-object v0

    sput-object v0, Lcom/a/b/m/ay;->b:Lcom/a/b/b/bv;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 544
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a()Lcom/a/b/b/bj;
    .registers 1

    .prologue
    .line 53
    sget-object v0, Lcom/a/b/m/ay;->a:Lcom/a/b/b/bj;

    return-object v0
.end method

.method static a(Ljava/lang/Class;)Ljava/lang/Class;
    .registers 2

    .prologue
    .line 447
    const/4 v0, 0x0

    invoke-static {p0, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ljava/lang/Iterable;)Ljava/lang/Iterable;
    .registers 2

    .prologue
    .line 53
    .line 2428
    const-class v0, Ljava/lang/Object;

    invoke-static {v0}, Lcom/a/b/b/cp;->a(Ljava/lang/Object;)Lcom/a/b/b/co;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/a/b/d/mq;->c(Ljava/lang/Iterable;Lcom/a/b/b/co;)Ljava/lang/Iterable;

    move-result-object v0

    .line 53
    return-object v0
.end method

.method static varargs a(Ljava/lang/Class;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;
    .registers 4

    .prologue
    .line 102
    new-instance v0, Lcom/a/b/m/bn;

    sget-object v1, Lcom/a/b/m/bb;->c:Lcom/a/b/m/bb;

    invoke-virtual {v1, p0}, Lcom/a/b/m/bb;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1, p0, p1}, Lcom/a/b/m/bn;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Class;[Ljava/lang/reflect/Type;)V

    return-object v0
.end method

.method static varargs a(Ljava/lang/reflect/Type;Ljava/lang/Class;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;
    .registers 7
    .param p0    # Ljava/lang/reflect/Type;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 88
    if-nez p0, :cond_9

    .line 89
    invoke-static {p1, p2}, Lcom/a/b/m/ay;->a(Ljava/lang/Class;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    .line 94
    :goto_8
    return-object v0

    .line 92
    :cond_9
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    invoke-virtual {p1}, Ljava/lang/Class;->getEnclosingClass()Ljava/lang/Class;

    move-result-object v0

    if-eqz v0, :cond_22

    move v0, v1

    :goto_13
    const-string v3, "Owner type for unenclosed %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 94
    new-instance v0, Lcom/a/b/m/bn;

    invoke-direct {v0, p0, p1, p2}, Lcom/a/b/m/bn;-><init>(Ljava/lang/reflect/Type;Ljava/lang/Class;[Ljava/lang/reflect/Type;)V

    goto :goto_8

    :cond_22
    move v0, v2

    .line 93
    goto :goto_13
.end method

.method static a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
    .registers 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 67
    instance-of v0, p0, Ljava/lang/reflect/WildcardType;

    if-eqz v0, :cond_49

    .line 68
    check-cast p0, Ljava/lang/reflect/WildcardType;

    .line 69
    invoke-interface {p0}, Ljava/lang/reflect/WildcardType;->getLowerBounds()[Ljava/lang/reflect/Type;

    move-result-object v3

    .line 70
    array-length v0, v3

    if-gt v0, v1, :cond_2e

    move v0, v1

    :goto_10
    const-string v4, "Wildcard cannot have more than one lower bounds."

    invoke-static {v0, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 71
    array-length v0, v3

    if-ne v0, v1, :cond_30

    .line 72
    aget-object v0, v3, v2

    invoke-static {v0}, Lcom/a/b/m/ay;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v3

    .line 1167
    new-instance v0, Lcom/a/b/m/bp;

    new-array v4, v1, [Ljava/lang/reflect/Type;

    aput-object v3, v4, v2

    new-array v1, v1, [Ljava/lang/reflect/Type;

    const-class v3, Ljava/lang/Object;

    aput-object v3, v1, v2

    invoke-direct {v0, v4, v1}, Lcom/a/b/m/bp;-><init>([Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)V

    .line 79
    :goto_2d
    return-object v0

    :cond_2e
    move v0, v2

    .line 70
    goto :goto_10

    .line 74
    :cond_30
    invoke-interface {p0}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    .line 75
    array-length v3, v0

    if-ne v3, v1, :cond_47

    :goto_37
    const-string v3, "Wildcard should have only one upper bound."

    invoke-static {v1, v3}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 76
    aget-object v0, v0, v2

    invoke-static {v0}, Lcom/a/b/m/ay;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/m/ay;->d(Ljava/lang/reflect/Type;)Ljava/lang/reflect/WildcardType;

    move-result-object v0

    goto :goto_2d

    :cond_47
    move v1, v2

    .line 75
    goto :goto_37

    .line 79
    :cond_49
    sget-object v0, Lcom/a/b/m/bh;->d:Lcom/a/b/m/bh;

    invoke-virtual {v0, p0}, Lcom/a/b/m/bh;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    goto :goto_2d
.end method

.method static synthetic a([Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
    .registers 4

    .prologue
    .line 53
    .line 1210
    array-length v2, p0

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v2, :cond_22

    aget-object v1, p0, v0

    .line 1211
    invoke-static {v1}, Lcom/a/b/m/ay;->c(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v1

    .line 1212
    if-eqz v1, :cond_1f

    .line 1215
    instance-of v0, v1, Ljava/lang/Class;

    if-eqz v0, :cond_1a

    move-object v0, v1

    .line 1216
    check-cast v0, Ljava/lang/Class;

    .line 1217
    invoke-virtual {v0}, Ljava/lang/Class;->isPrimitive()Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 1221
    :goto_19
    return-object v0

    :cond_1a
    invoke-static {v1}, Lcom/a/b/m/ay;->d(Ljava/lang/reflect/Type;)Ljava/lang/reflect/WildcardType;

    move-result-object v0

    goto :goto_19

    .line 1210
    :cond_1f
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1224
    :cond_22
    const/4 v0, 0x0

    .line 53
    goto :goto_19
.end method

.method static varargs a(Ljava/lang/reflect/GenericDeclaration;Ljava/lang/String;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/TypeVariable;
    .registers 6

    .prologue
    .line 152
    new-instance v0, Lcom/a/b/m/bo;

    array-length v1, p2

    if-nez v1, :cond_d

    const/4 v1, 0x1

    new-array p2, v1, [Ljava/lang/reflect/Type;

    const/4 v1, 0x0

    const-class v2, Ljava/lang/Object;

    aput-object v2, p2, v1

    :cond_d
    invoke-direct {v0, p0, p1, p2}, Lcom/a/b/m/bo;-><init>(Ljava/lang/reflect/GenericDeclaration;Ljava/lang/String;[Ljava/lang/reflect/Type;)V

    return-object v0
.end method

.method static synthetic a([Ljava/lang/reflect/Type;Ljava/lang/String;)V
    .registers 10

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 53
    .line 1433
    array-length v5, p0

    move v4, v3

    :goto_4
    if-ge v4, v5, :cond_27

    aget-object v0, p0, v4

    .line 1434
    instance-of v1, v0, Ljava/lang/Class;

    if-eqz v1, :cond_21

    .line 1435
    check-cast v0, Ljava/lang/Class;

    .line 1436
    invoke-virtual {v0}, Ljava/lang/Class;->isPrimitive()Z

    move-result v1

    if-nez v1, :cond_25

    move v1, v2

    :goto_15
    const-string v6, "Primitive type \'%s\' used as %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v0, v7, v3

    aput-object p1, v7, v2

    invoke-static {v1, v6, v7}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1433
    :cond_21
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_4

    :cond_25
    move v1, v3

    .line 1436
    goto :goto_15

    .line 53
    :cond_27
    return-void
.end method

.method static synthetic a(Ljava/util/Collection;)[Ljava/lang/reflect/Type;
    .registers 2

    .prologue
    .line 53
    .line 2424
    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/reflect/Type;

    invoke-interface {p0, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/reflect/Type;

    .line 53
    return-object v0
.end method

.method static synthetic b()Lcom/a/b/b/bv;
    .registers 1

    .prologue
    .line 53
    sget-object v0, Lcom/a/b/m/ay;->b:Lcom/a/b/b/bv;

    return-object v0
.end method

.method private static b(Ljava/lang/Iterable;)Ljava/lang/Iterable;
    .registers 2

    .prologue
    .line 428
    const-class v0, Ljava/lang/Object;

    invoke-static {v0}, Lcom/a/b/b/cp;->a(Ljava/lang/Object;)Lcom/a/b/b/co;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/b/cp;->a(Lcom/a/b/b/co;)Lcom/a/b/b/co;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/a/b/d/mq;->c(Ljava/lang/Iterable;Lcom/a/b/b/co;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method static b(Ljava/lang/reflect/Type;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 180
    instance-of v0, p0, Ljava/lang/Class;

    if-eqz v0, :cond_b

    check-cast p0, Ljava/lang/Class;

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_a
.end method

.method private static b([Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
    .registers 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 210
    array-length v2, p0

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v2, :cond_22

    aget-object v1, p0, v0

    .line 211
    invoke-static {v1}, Lcom/a/b/m/ay;->c(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v1

    .line 212
    if-eqz v1, :cond_1f

    .line 215
    instance-of v0, v1, Ljava/lang/Class;

    if-eqz v0, :cond_1a

    move-object v0, v1

    .line 216
    check-cast v0, Ljava/lang/Class;

    .line 217
    invoke-virtual {v0}, Ljava/lang/Class;->isPrimitive()Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 224
    :goto_19
    return-object v0

    .line 221
    :cond_1a
    invoke-static {v1}, Lcom/a/b/m/ay;->d(Ljava/lang/reflect/Type;)Ljava/lang/reflect/WildcardType;

    move-result-object v0

    goto :goto_19

    .line 210
    :cond_1f
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 224
    :cond_22
    const/4 v0, 0x0

    goto :goto_19
.end method

.method private static b([Ljava/lang/reflect/Type;Ljava/lang/String;)V
    .registers 10

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 433
    array-length v5, p0

    move v4, v3

    :goto_4
    if-ge v4, v5, :cond_27

    aget-object v0, p0, v4

    .line 434
    instance-of v1, v0, Ljava/lang/Class;

    if-eqz v1, :cond_21

    .line 435
    check-cast v0, Ljava/lang/Class;

    .line 436
    invoke-virtual {v0}, Ljava/lang/Class;->isPrimitive()Z

    move-result v1

    if-nez v1, :cond_25

    move v1, v2

    :goto_15
    const-string v6, "Primitive type \'%s\' used as %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v0, v7, v3

    aput-object p1, v7, v2

    invoke-static {v1, v6, v7}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 433
    :cond_21
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_4

    :cond_25
    move v1, v3

    .line 436
    goto :goto_15

    .line 440
    :cond_27
    return-void
.end method

.method private static b(Ljava/util/Collection;)[Ljava/lang/reflect/Type;
    .registers 2

    .prologue
    .line 424
    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/reflect/Type;

    invoke-interface {p0, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/reflect/Type;

    return-object v0
.end method

.method static c(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
    .registers 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 186
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 188
    new-instance v1, Lcom/a/b/m/ba;

    invoke-direct {v1, v0}, Lcom/a/b/m/ba;-><init>(Ljava/util/concurrent/atomic/AtomicReference;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/reflect/Type;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v1, v2}, Lcom/a/b/m/ba;->a([Ljava/lang/reflect/Type;)V

    .line 202
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Type;

    return-object v0
.end method

.method private static d(Ljava/lang/reflect/Type;)Ljava/lang/reflect/WildcardType;
    .registers 5
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 162
    new-instance v0, Lcom/a/b/m/bp;

    new-array v1, v3, [Ljava/lang/reflect/Type;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/reflect/Type;

    aput-object p0, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/a/b/m/bp;-><init>([Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)V

    return-object v0
.end method

.method private static e(Ljava/lang/reflect/Type;)Ljava/lang/reflect/WildcardType;
    .registers 6
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 167
    new-instance v0, Lcom/a/b/m/bp;

    new-array v1, v2, [Ljava/lang/reflect/Type;

    aput-object p0, v1, v4

    new-array v2, v2, [Ljava/lang/reflect/Type;

    const-class v3, Ljava/lang/Object;

    aput-object v3, v2, v4

    invoke-direct {v0, v1, v2}, Lcom/a/b/m/bp;-><init>([Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)V

    return-object v0
.end method
