.class abstract Lcom/a/b/m/an;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Lcom/a/b/m/an;

.field static final b:Lcom/a/b/m/an;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 1069
    new-instance v0, Lcom/a/b/m/ao;

    invoke-direct {v0}, Lcom/a/b/m/ao;-><init>()V

    sput-object v0, Lcom/a/b/m/an;->a:Lcom/a/b/m/an;

    .line 1085
    new-instance v0, Lcom/a/b/m/ap;

    invoke-direct {v0}, Lcom/a/b/m/ap;-><init>()V

    sput-object v0, Lcom/a/b/m/an;->b:Lcom/a/b/m/an;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 1067
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1172
    return-void
.end method

.method synthetic constructor <init>(B)V
    .registers 2

    .prologue
    .line 1067
    invoke-direct {p0}, Lcom/a/b/m/an;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/Object;Ljava/util/Map;)I
    .registers 6

    .prologue
    .line 1134
    invoke-interface {p2, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1135
    if-eqz v0, :cond_d

    .line 1137
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1155
    :goto_c
    return v0

    .line 1139
    :cond_d
    invoke-virtual {p0, p1}, Lcom/a/b/m/an;->b(Ljava/lang/Object;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isInterface()Z

    move-result v0

    if-eqz v0, :cond_33

    const/4 v0, 0x1

    .line 1142
    :goto_18
    invoke-virtual {p0, p1}, Lcom/a/b/m/an;->c(Ljava/lang/Object;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_20
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_35

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 1143
    invoke-direct {p0, v2, p2}, Lcom/a/b/m/an;->a(Ljava/lang/Object;Ljava/util/Map;)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_20

    .line 1139
    :cond_33
    const/4 v0, 0x0

    goto :goto_18

    .line 1145
    :cond_35
    invoke-virtual {p0, p1}, Lcom/a/b/m/an;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1146
    if-eqz v1, :cond_43

    .line 1147
    invoke-direct {p0, v1, p2}, Lcom/a/b/m/an;->a(Ljava/lang/Object;Ljava/util/Map;)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1154
    :cond_43
    add-int/lit8 v1, v0, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1155
    add-int/lit8 v0, v0, 0x1

    goto :goto_c
.end method

.method private static a(Ljava/util/Map;Ljava/util/Comparator;)Lcom/a/b/d/jl;
    .registers 4

    .prologue
    .line 1160
    new-instance v0, Lcom/a/b/m/ar;

    invoke-direct {v0, p1, p0}, Lcom/a/b/m/ar;-><init>(Ljava/util/Comparator;Ljava/util/Map;)V

    .line 1165
    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/d/yd;->b(Ljava/lang/Iterable;)Lcom/a/b/d/jl;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method a(Ljava/lang/Iterable;)Lcom/a/b/d/jl;
    .registers 5

    .prologue
    .line 1125
    invoke-static {}, Lcom/a/b/d/sz;->c()Ljava/util/HashMap;

    move-result-object v0

    .line 1126
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 1127
    invoke-direct {p0, v2, v0}, Lcom/a/b/m/an;->a(Ljava/lang/Object;Ljava/util/Map;)I

    goto :goto_8

    .line 1129
    :cond_16
    invoke-static {}, Lcom/a/b/d/yd;->d()Lcom/a/b/d/yd;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/b/d/yd;->a()Lcom/a/b/d/yd;

    move-result-object v1

    .line 2160
    new-instance v2, Lcom/a/b/m/ar;

    invoke-direct {v2, v1, v0}, Lcom/a/b/m/ar;-><init>(Ljava/util/Comparator;Ljava/util/Map;)V

    .line 2165
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/a/b/d/yd;->b(Ljava/lang/Iterable;)Lcom/a/b/d/jl;

    move-result-object v0

    .line 1129
    return-object v0
.end method

.method final a(Ljava/lang/Object;)Lcom/a/b/d/jl;
    .registers 3

    .prologue
    .line 1120
    invoke-static {p1}, Lcom/a/b/d/jl;->a(Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/b/m/an;->a(Ljava/lang/Iterable;)Lcom/a/b/d/jl;

    move-result-object v0

    return-object v0
.end method

.method final a()Lcom/a/b/m/an;
    .registers 2

    .prologue
    .line 1103
    new-instance v0, Lcom/a/b/m/aq;

    invoke-direct {v0, p0, p0}, Lcom/a/b/m/aq;-><init>(Lcom/a/b/m/an;Lcom/a/b/m/an;)V

    return-object v0
.end method

.method abstract b(Ljava/lang/Object;)Ljava/lang/Class;
.end method

.method abstract c(Ljava/lang/Object;)Ljava/lang/Iterable;
.end method

.method abstract d(Ljava/lang/Object;)Ljava/lang/Object;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end method
