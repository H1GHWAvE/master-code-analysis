.class Lcom/a/b/m/m;
.super Lcom/a/b/m/k;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/reflect/Method;


# direct methods
.method constructor <init>(Ljava/lang/reflect/Method;)V
    .registers 2

    .prologue
    .line 191
    invoke-direct {p0, p1}, Lcom/a/b/m/k;-><init>(Ljava/lang/reflect/AccessibleObject;)V

    .line 192
    iput-object p1, p0, Lcom/a/b/m/m;->a:Ljava/lang/reflect/Method;

    .line 193
    return-void
.end method


# virtual methods
.method final a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 197
    iget-object v0, p0, Lcom/a/b/m/m;->a:Ljava/lang/reflect/Method;

    invoke-virtual {v0, p1, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .registers 2

    .prologue
    .line 221
    .line 1125
    invoke-virtual {p0}, Lcom/a/b/m/g;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isFinal(I)Z

    move-result v0

    .line 221
    if-nez v0, :cond_2e

    .line 2109
    invoke-virtual {p0}, Lcom/a/b/m/g;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isPrivate(I)Z

    move-result v0

    .line 221
    if-nez v0, :cond_2e

    .line 2114
    invoke-virtual {p0}, Lcom/a/b/m/g;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v0

    .line 221
    if-nez v0, :cond_2e

    invoke-virtual {p0}, Lcom/a/b/m/m;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isFinal(I)Z

    move-result v0

    if-nez v0, :cond_2e

    const/4 v0, 0x1

    :goto_2d
    return v0

    :cond_2e
    const/4 v0, 0x0

    goto :goto_2d
.end method

.method public final c()Z
    .registers 2

    .prologue
    .line 226
    iget-object v0, p0, Lcom/a/b/m/m;->a:Ljava/lang/reflect/Method;

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->isVarArgs()Z

    move-result v0

    return v0
.end method

.method d()[Ljava/lang/reflect/Type;
    .registers 2

    .prologue
    .line 205
    iget-object v0, p0, Lcom/a/b/m/m;->a:Ljava/lang/reflect/Method;

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getGenericParameterTypes()[Ljava/lang/reflect/Type;

    move-result-object v0

    return-object v0
.end method

.method e()[Ljava/lang/reflect/Type;
    .registers 2

    .prologue
    .line 209
    iget-object v0, p0, Lcom/a/b/m/m;->a:Ljava/lang/reflect/Method;

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getGenericExceptionTypes()[Ljava/lang/reflect/Type;

    move-result-object v0

    return-object v0
.end method

.method final f()[[Ljava/lang/annotation/Annotation;
    .registers 2

    .prologue
    .line 213
    iget-object v0, p0, Lcom/a/b/m/m;->a:Ljava/lang/reflect/Method;

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getParameterAnnotations()[[Ljava/lang/annotation/Annotation;

    move-result-object v0

    return-object v0
.end method

.method g()Ljava/lang/reflect/Type;
    .registers 2

    .prologue
    .line 201
    iget-object v0, p0, Lcom/a/b/m/m;->a:Ljava/lang/reflect/Method;

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getGenericReturnType()Ljava/lang/reflect/Type;

    move-result-object v0

    return-object v0
.end method

.method public final getTypeParameters()[Ljava/lang/reflect/TypeVariable;
    .registers 2

    .prologue
    .line 217
    iget-object v0, p0, Lcom/a/b/m/m;->a:Ljava/lang/reflect/Method;

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getTypeParameters()[Ljava/lang/reflect/TypeVariable;

    move-result-object v0

    return-object v0
.end method
