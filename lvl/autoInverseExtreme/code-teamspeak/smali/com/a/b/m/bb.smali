.class abstract enum Lcom/a/b/m/bb;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/a/b/m/bb;

.field public static final enum b:Lcom/a/b/m/bb;

.field static final c:Lcom/a/b/m/bb;

.field private static final synthetic d:[Lcom/a/b/m/bb;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 109
    new-instance v0, Lcom/a/b/m/bc;

    const-string v2, "OWNED_BY_ENCLOSING_CLASS"

    invoke-direct {v0, v2}, Lcom/a/b/m/bc;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/m/bb;->a:Lcom/a/b/m/bb;

    .line 116
    new-instance v0, Lcom/a/b/m/be;

    const-string v2, "LOCAL_CLASS_HAS_NO_OWNER"

    invoke-direct {v0, v2}, Lcom/a/b/m/be;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/m/bb;->b:Lcom/a/b/m/bb;

    .line 107
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/a/b/m/bb;

    sget-object v2, Lcom/a/b/m/bb;->a:Lcom/a/b/m/bb;

    aput-object v2, v0, v1

    const/4 v2, 0x1

    sget-object v3, Lcom/a/b/m/bb;->b:Lcom/a/b/m/bb;

    aput-object v3, v0, v2

    sput-object v0, Lcom/a/b/m/bb;->d:[Lcom/a/b/m/bb;

    .line 1134
    new-instance v0, Lcom/a/b/m/bf;

    invoke-direct {v0}, Lcom/a/b/m/bf;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 1135
    invoke-virtual {v0}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    .line 1137
    invoke-static {}, Lcom/a/b/m/bb;->values()[Lcom/a/b/m/bb;

    move-result-object v2

    array-length v3, v2

    :goto_35
    if-ge v1, v3, :cond_4b

    aget-object v4, v2, v1

    .line 1138
    const-class v5, Lcom/a/b/m/bd;

    invoke-virtual {v4, v5}, Lcom/a/b/m/bb;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v5

    invoke-interface {v0}, Ljava/lang/reflect/ParameterizedType;->getOwnerType()Ljava/lang/reflect/Type;

    move-result-object v6

    if-ne v5, v6, :cond_48

    .line 130
    sput-object v4, Lcom/a/b/m/bb;->c:Lcom/a/b/m/bb;

    return-void

    .line 1137
    :cond_48
    add-int/lit8 v1, v1, 0x1

    goto :goto_35

    .line 1142
    :cond_4b
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3

    .prologue
    .line 107
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .registers 4

    .prologue
    .line 107
    invoke-direct {p0, p1, p2}, Lcom/a/b/m/bb;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private static a()Lcom/a/b/m/bb;
    .registers 7

    .prologue
    .line 134
    new-instance v0, Lcom/a/b/m/bf;

    invoke-direct {v0}, Lcom/a/b/m/bf;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 135
    invoke-virtual {v0}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    .line 137
    invoke-static {}, Lcom/a/b/m/bb;->values()[Lcom/a/b/m/bb;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_15
    if-ge v1, v3, :cond_29

    aget-object v4, v2, v1

    .line 138
    const-class v5, Lcom/a/b/m/bd;

    invoke-virtual {v4, v5}, Lcom/a/b/m/bb;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v5

    invoke-interface {v0}, Ljava/lang/reflect/ParameterizedType;->getOwnerType()Ljava/lang/reflect/Type;

    move-result-object v6

    if-ne v5, v6, :cond_26

    .line 139
    return-object v4

    .line 137
    :cond_26
    add-int/lit8 v1, v1, 0x1

    goto :goto_15

    .line 142
    :cond_29
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/m/bb;
    .registers 2

    .prologue
    .line 107
    const-class v0, Lcom/a/b/m/bb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/b/m/bb;

    return-object v0
.end method

.method public static values()[Lcom/a/b/m/bb;
    .registers 1

    .prologue
    .line 107
    sget-object v0, Lcom/a/b/m/bb;->d:[Lcom/a/b/m/bb;

    invoke-virtual {v0}, [Lcom/a/b/m/bb;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/b/m/bb;

    return-object v0
.end method


# virtual methods
.method abstract a(Ljava/lang/Class;)Ljava/lang/Class;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end method
