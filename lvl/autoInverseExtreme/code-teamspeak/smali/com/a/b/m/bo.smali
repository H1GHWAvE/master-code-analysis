.class final Lcom/a/b/m/bo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/reflect/TypeVariable;


# instance fields
.field private final a:Ljava/lang/reflect/GenericDeclaration;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/a/b/d/jl;


# direct methods
.method constructor <init>(Ljava/lang/reflect/GenericDeclaration;Ljava/lang/String;[Ljava/lang/reflect/Type;)V
    .registers 5

    .prologue
    .line 327
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 328
    const-string v0, "bound for type variable"

    invoke-static {p3, v0}, Lcom/a/b/m/ay;->a([Ljava/lang/reflect/Type;Ljava/lang/String;)V

    .line 329
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/GenericDeclaration;

    iput-object v0, p0, Lcom/a/b/m/bo;->a:Ljava/lang/reflect/GenericDeclaration;

    .line 330
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/a/b/m/bo;->b:Ljava/lang/String;

    .line 331
    invoke-static {p3}, Lcom/a/b/d/jl;->a([Ljava/lang/Object;)Lcom/a/b/d/jl;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/m/bo;->c:Lcom/a/b/d/jl;

    .line 332
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .registers 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 355
    sget-boolean v2, Lcom/a/b/m/bm;->a:Z

    if-eqz v2, :cond_33

    .line 357
    instance-of v2, p1, Lcom/a/b/m/bo;

    if-eqz v2, :cond_31

    .line 358
    check-cast p1, Lcom/a/b/m/bo;

    .line 359
    iget-object v2, p0, Lcom/a/b/m/bo;->b:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/a/b/m/bo;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2f

    iget-object v2, p0, Lcom/a/b/m/bo;->a:Ljava/lang/reflect/GenericDeclaration;

    invoke-virtual {p1}, Lcom/a/b/m/bo;->getGenericDeclaration()Ljava/lang/reflect/GenericDeclaration;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2f

    iget-object v2, p0, Lcom/a/b/m/bo;->c:Lcom/a/b/d/jl;

    iget-object v3, p1, Lcom/a/b/m/bo;->c:Lcom/a/b/d/jl;

    invoke-virtual {v2, v3}, Lcom/a/b/d/jl;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2f

    .line 371
    :cond_2e
    :goto_2e
    return v0

    :cond_2f
    move v0, v1

    .line 359
    goto :goto_2e

    :cond_31
    move v0, v1

    .line 363
    goto :goto_2e

    .line 366
    :cond_33
    instance-of v2, p1, Ljava/lang/reflect/TypeVariable;

    if-eqz v2, :cond_53

    .line 367
    check-cast p1, Ljava/lang/reflect/TypeVariable;

    .line 368
    iget-object v2, p0, Lcom/a/b/m/bo;->b:Ljava/lang/String;

    invoke-interface {p1}, Ljava/lang/reflect/TypeVariable;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_51

    iget-object v2, p0, Lcom/a/b/m/bo;->a:Ljava/lang/reflect/GenericDeclaration;

    invoke-interface {p1}, Ljava/lang/reflect/TypeVariable;->getGenericDeclaration()Ljava/lang/reflect/GenericDeclaration;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2e

    :cond_51
    move v0, v1

    goto :goto_2e

    :cond_53
    move v0, v1

    .line 371
    goto :goto_2e
.end method

.method public final getBounds()[Ljava/lang/reflect/Type;
    .registers 2

    .prologue
    .line 335
    iget-object v0, p0, Lcom/a/b/m/bo;->c:Lcom/a/b/d/jl;

    invoke-static {v0}, Lcom/a/b/m/ay;->a(Ljava/util/Collection;)[Ljava/lang/reflect/Type;

    move-result-object v0

    return-object v0
.end method

.method public final getGenericDeclaration()Ljava/lang/reflect/GenericDeclaration;
    .registers 2

    .prologue
    .line 339
    iget-object v0, p0, Lcom/a/b/m/bo;->a:Ljava/lang/reflect/GenericDeclaration;

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .registers 2

    .prologue
    .line 343
    iget-object v0, p0, Lcom/a/b/m/bo;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .registers 3

    .prologue
    .line 351
    iget-object v0, p0, Lcom/a/b/m/bo;->a:Ljava/lang/reflect/GenericDeclaration;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, Lcom/a/b/m/bo;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 347
    iget-object v0, p0, Lcom/a/b/m/bo;->b:Ljava/lang/String;

    return-object v0
.end method
