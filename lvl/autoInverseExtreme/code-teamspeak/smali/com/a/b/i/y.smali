.class final Lcom/a/b/i/y;
.super Lcom/a/b/i/s;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/i/s;

.field private final b:J

.field private final c:J


# direct methods
.method private constructor <init>(Lcom/a/b/i/s;JJ)V
    .registers 14

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 437
    iput-object p1, p0, Lcom/a/b/i/y;->a:Lcom/a/b/i/s;

    invoke-direct {p0}, Lcom/a/b/i/s;-><init>()V

    .line 438
    cmp-long v0, p2, v6

    if-ltz v0, :cond_32

    move v0, v1

    :goto_e
    const-string v3, "offset (%s) may not be negative"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 439
    cmp-long v0, p4, v6

    if-ltz v0, :cond_34

    move v0, v1

    :goto_20
    const-string v3, "length (%s) may not be negative"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 440
    iput-wide p2, p0, Lcom/a/b/i/y;->b:J

    .line 441
    iput-wide p4, p0, Lcom/a/b/i/y;->c:J

    .line 442
    return-void

    :cond_32
    move v0, v2

    .line 438
    goto :goto_e

    :cond_34
    move v0, v2

    .line 439
    goto :goto_20
.end method

.method synthetic constructor <init>(Lcom/a/b/i/s;JJB)V
    .registers 7

    .prologue
    .line 432
    invoke-direct/range {p0 .. p5}, Lcom/a/b/i/y;-><init>(Lcom/a/b/i/s;JJ)V

    return-void
.end method

.method private a(Ljava/io/InputStream;)Ljava/io/InputStream;
    .registers 6

    .prologue
    .line 455
    iget-wide v0, p0, Lcom/a/b/i/y;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_d

    .line 457
    :try_start_8
    iget-wide v0, p0, Lcom/a/b/i/y;->b:J

    invoke-static {p1, v0, v1}, Lcom/a/b/i/z;->b(Ljava/io/InputStream;J)V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_d} :catch_14

    .line 468
    :cond_d
    iget-wide v0, p0, Lcom/a/b/i/y;->c:J

    invoke-static {p1, v0, v1}, Lcom/a/b/i/z;->a(Ljava/io/InputStream;J)Ljava/io/InputStream;

    move-result-object v0

    return-object v0

    .line 458
    :catch_14
    move-exception v0

    .line 459
    invoke-static {}, Lcom/a/b/i/ar;->a()Lcom/a/b/i/ar;

    move-result-object v1

    .line 460
    invoke-virtual {v1, p1}, Lcom/a/b/i/ar;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    .line 462
    :try_start_1c
    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_21
    .catchall {:try_start_1c .. :try_end_21} :catchall_21

    .line 464
    :catchall_21
    move-exception v0

    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    throw v0
.end method


# virtual methods
.method public final a(JJ)Lcom/a/b/i/s;
    .registers 14

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 473
    cmp-long v0, p1, v6

    if-ltz v0, :cond_39

    move v0, v1

    :goto_9
    const-string v3, "offset (%s) may not be negative"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 474
    cmp-long v0, p3, v6

    if-ltz v0, :cond_3b

    move v0, v1

    :goto_1b
    const-string v3, "length (%s) may not be negative"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 475
    iget-wide v0, p0, Lcom/a/b/i/y;->c:J

    sub-long/2addr v0, p1

    .line 476
    iget-object v2, p0, Lcom/a/b/i/y;->a:Lcom/a/b/i/s;

    iget-wide v4, p0, Lcom/a/b/i/y;->b:J

    add-long/2addr v4, p1

    invoke-static {p3, p4, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    invoke-virtual {v2, v4, v5, v0, v1}, Lcom/a/b/i/s;->a(JJ)Lcom/a/b/i/s;

    move-result-object v0

    return-object v0

    :cond_39
    move v0, v2

    .line 473
    goto :goto_9

    :cond_3b
    move v0, v2

    .line 474
    goto :goto_1b
.end method

.method public final a()Ljava/io/InputStream;
    .registers 2

    .prologue
    .line 446
    iget-object v0, p0, Lcom/a/b/i/y;->a:Lcom/a/b/i/s;

    invoke-virtual {v0}, Lcom/a/b/i/s;->a()Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/i/y;->a(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/io/InputStream;
    .registers 2

    .prologue
    .line 451
    iget-object v0, p0, Lcom/a/b/i/y;->a:Lcom/a/b/i/s;

    invoke-virtual {v0}, Lcom/a/b/i/s;->b()Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/b/i/y;->a(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public final c()Z
    .registers 5

    .prologue
    .line 481
    iget-wide v0, p0, Lcom/a/b/i/y;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_e

    invoke-super {p0}, Lcom/a/b/i/s;->c()Z

    move-result v0

    if-eqz v0, :cond_10

    :cond_e
    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public final toString()Ljava/lang/String;
    .registers 8

    .prologue
    .line 486
    iget-object v0, p0, Lcom/a/b/i/y;->a:Lcom/a/b/i/s;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/a/b/i/y;->b:J

    iget-wide v4, p0, Lcom/a/b/i/y;->c:J

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x32

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".slice("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
