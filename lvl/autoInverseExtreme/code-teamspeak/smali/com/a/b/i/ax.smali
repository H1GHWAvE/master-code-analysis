.class public final Lcom/a/b/i/ax;
.super Ljava/io/OutputStream;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# instance fields
.field private final a:I

.field private final b:Z

.field private final c:Lcom/a/b/i/s;

.field private d:Ljava/io/OutputStream;

.field private e:Lcom/a/b/i/ba;

.field private f:Ljava/io/File;


# direct methods
.method private constructor <init>(I)V
    .registers 3

    .prologue
    .line 76
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/a/b/i/ax;-><init>(IB)V

    .line 77
    return-void
.end method

.method private constructor <init>(IB)V
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 90
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 91
    iput p1, p0, Lcom/a/b/i/ax;->a:I

    .line 92
    iput-boolean v1, p0, Lcom/a/b/i/ax;->b:Z

    .line 93
    new-instance v0, Lcom/a/b/i/ba;

    invoke-direct {v0, v1}, Lcom/a/b/i/ba;-><init>(B)V

    iput-object v0, p0, Lcom/a/b/i/ax;->e:Lcom/a/b/i/ba;

    .line 94
    iget-object v0, p0, Lcom/a/b/i/ax;->e:Lcom/a/b/i/ba;

    iput-object v0, p0, Lcom/a/b/i/ax;->d:Ljava/io/OutputStream;

    .line 112
    new-instance v0, Lcom/a/b/i/az;

    invoke-direct {v0, p0}, Lcom/a/b/i/az;-><init>(Lcom/a/b/i/ax;)V

    iput-object v0, p0, Lcom/a/b/i/ax;->c:Lcom/a/b/i/s;

    .line 119
    return-void
.end method

.method static synthetic a(Lcom/a/b/i/ax;)Ljava/io/InputStream;
    .registers 2

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/a/b/i/ax;->d()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method private a(I)V
    .registers 8

    .prologue
    const/4 v5, 0x0

    .line 195
    iget-object v0, p0, Lcom/a/b/i/ax;->f:Ljava/io/File;

    if-nez v0, :cond_3b

    iget-object v0, p0, Lcom/a/b/i/ax;->e:Lcom/a/b/i/ba;

    invoke-virtual {v0}, Lcom/a/b/i/ba;->b()I

    move-result v0

    add-int/2addr v0, p1

    iget v1, p0, Lcom/a/b/i/ax;->a:I

    if-le v0, v1, :cond_3b

    .line 196
    const-string v0, "FileBackedOutputStream"

    invoke-static {v0, v5}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 197
    iget-boolean v1, p0, Lcom/a/b/i/ax;->b:Z

    if-eqz v1, :cond_1d

    .line 200
    invoke-virtual {v0}, Ljava/io/File;->deleteOnExit()V

    .line 202
    :cond_1d
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 203
    iget-object v2, p0, Lcom/a/b/i/ax;->e:Lcom/a/b/i/ba;

    invoke-virtual {v2}, Lcom/a/b/i/ba;->a()[B

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/a/b/i/ax;->e:Lcom/a/b/i/ba;

    invoke-virtual {v4}, Lcom/a/b/i/ba;->b()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Ljava/io/FileOutputStream;->write([BII)V

    .line 204
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V

    .line 207
    iput-object v1, p0, Lcom/a/b/i/ax;->d:Ljava/io/OutputStream;

    .line 208
    iput-object v0, p0, Lcom/a/b/i/ax;->f:Ljava/io/File;

    .line 209
    iput-object v5, p0, Lcom/a/b/i/ax;->e:Lcom/a/b/i/ba;

    .line 211
    :cond_3b
    return-void
.end method

.method private declared-synchronized b()Ljava/io/File;
    .registers 2
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 64
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/a/b/i/ax;->f:Ljava/io/File;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return-object v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private c()Lcom/a/b/i/s;
    .registers 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/a/b/i/ax;->c:Lcom/a/b/i/s;

    return-object v0
.end method

.method private declared-synchronized d()Ljava/io/InputStream;
    .registers 5

    .prologue
    .line 132
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/a/b/i/ax;->f:Ljava/io/File;

    if-eqz v0, :cond_e

    .line 133
    new-instance v0, Ljava/io/FileInputStream;

    iget-object v1, p0, Lcom/a/b/i/ax;->f:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_21

    .line 135
    :goto_c
    monitor-exit p0

    return-object v0

    :cond_e
    :try_start_e
    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lcom/a/b/i/ax;->e:Lcom/a/b/i/ba;

    invoke-virtual {v1}, Lcom/a/b/i/ba;->a()[B

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/a/b/i/ax;->e:Lcom/a/b/i/ba;

    invoke-virtual {v3}, Lcom/a/b/i/ba;->b()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([BII)V
    :try_end_20
    .catchall {:try_start_e .. :try_end_20} :catchall_21

    goto :goto_c

    .line 132
    :catchall_21
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .registers 5

    .prologue
    .line 149
    monitor-enter p0

    :try_start_1
    invoke-virtual {p0}, Lcom/a/b/i/ax;->close()V
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_53

    .line 151
    :try_start_4
    iget-object v0, p0, Lcom/a/b/i/ax;->e:Lcom/a/b/i/ba;

    if-nez v0, :cond_4d

    .line 152
    new-instance v0, Lcom/a/b/i/ba;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/a/b/i/ba;-><init>(B)V

    iput-object v0, p0, Lcom/a/b/i/ax;->e:Lcom/a/b/i/ba;

    .line 156
    :goto_10
    iget-object v0, p0, Lcom/a/b/i/ax;->e:Lcom/a/b/i/ba;

    iput-object v0, p0, Lcom/a/b/i/ax;->d:Ljava/io/OutputStream;

    .line 157
    iget-object v0, p0, Lcom/a/b/i/ax;->f:Ljava/io/File;

    if-eqz v0, :cond_a1

    .line 158
    iget-object v0, p0, Lcom/a/b/i/ax;->f:Ljava/io/File;

    .line 159
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/a/b/i/ax;->f:Ljava/io/File;

    .line 160
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_a1

    .line 161
    new-instance v1, Ljava/io/IOException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x12

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Could not delete: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_4a
    .catchall {:try_start_4 .. :try_end_4a} :catchall_4a

    .line 149
    :catchall_4a
    move-exception v0

    monitor-exit p0

    throw v0

    .line 154
    :cond_4d
    :try_start_4d
    iget-object v0, p0, Lcom/a/b/i/ax;->e:Lcom/a/b/i/ba;

    invoke-virtual {v0}, Lcom/a/b/i/ba;->reset()V

    goto :goto_10

    .line 151
    :catchall_53
    move-exception v0

    iget-object v1, p0, Lcom/a/b/i/ax;->e:Lcom/a/b/i/ba;

    if-nez v1, :cond_9a

    .line 152
    new-instance v1, Lcom/a/b/i/ba;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/a/b/i/ba;-><init>(B)V

    iput-object v1, p0, Lcom/a/b/i/ax;->e:Lcom/a/b/i/ba;

    .line 156
    :goto_60
    iget-object v1, p0, Lcom/a/b/i/ax;->e:Lcom/a/b/i/ba;

    iput-object v1, p0, Lcom/a/b/i/ax;->d:Ljava/io/OutputStream;

    .line 157
    iget-object v1, p0, Lcom/a/b/i/ax;->f:Ljava/io/File;

    if-eqz v1, :cond_a0

    .line 158
    iget-object v1, p0, Lcom/a/b/i/ax;->f:Ljava/io/File;

    .line 159
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/a/b/i/ax;->f:Ljava/io/File;

    .line 160
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v2

    if-nez v2, :cond_a0

    .line 161
    new-instance v0, Ljava/io/IOException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x12

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Could not delete: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 154
    :cond_9a
    iget-object v1, p0, Lcom/a/b/i/ax;->e:Lcom/a/b/i/ba;

    invoke-virtual {v1}, Lcom/a/b/i/ba;->reset()V

    goto :goto_60

    .line 163
    :cond_a0
    throw v0
    :try_end_a1
    .catchall {:try_start_4d .. :try_end_a1} :catchall_4a

    .line 165
    :cond_a1
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized close()V
    .registers 2

    .prologue
    .line 183
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/a/b/i/ax;->d:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    .line 184
    monitor-exit p0

    return-void

    .line 183
    :catchall_8
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized flush()V
    .registers 2

    .prologue
    .line 187
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/a/b/i/ax;->d:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    .line 188
    monitor-exit p0

    return-void

    .line 187
    :catchall_8
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized write(I)V
    .registers 3

    .prologue
    .line 168
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_2
    invoke-direct {p0, v0}, Lcom/a/b/i/ax;->a(I)V

    .line 169
    iget-object v0, p0, Lcom/a/b/i/ax;->d:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V
    :try_end_a
    .catchall {:try_start_2 .. :try_end_a} :catchall_c

    .line 170
    monitor-exit p0

    return-void

    .line 168
    :catchall_c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized write([B)V
    .registers 4

    .prologue
    .line 173
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_2
    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/a/b/i/ax;->write([BII)V
    :try_end_6
    .catchall {:try_start_2 .. :try_end_6} :catchall_8

    .line 174
    monitor-exit p0

    return-void

    .line 173
    :catchall_8
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized write([BII)V
    .registers 5

    .prologue
    .line 178
    monitor-enter p0

    :try_start_1
    invoke-direct {p0, p3}, Lcom/a/b/i/ax;->a(I)V

    .line 179
    iget-object v0, p0, Lcom/a/b/i/ax;->d:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_b

    .line 180
    monitor-exit p0

    return-void

    .line 178
    :catchall_b
    move-exception v0

    monitor-exit p0

    throw v0
.end method
