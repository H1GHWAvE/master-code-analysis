.class public final Lcom/a/b/i/bc;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# static fields
.field private static final a:I = 0x2710

.field private static final b:Lcom/a/b/d/aga;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 807
    new-instance v0, Lcom/a/b/i/be;

    invoke-direct {v0}, Lcom/a/b/i/be;-><init>()V

    sput-object v0, Lcom/a/b/i/bc;->b:Lcom/a/b/d/aga;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/io/File;Lcom/a/b/g/ak;)Lcom/a/b/g/ag;
    .registers 3

    .prologue
    .line 594
    invoke-static {p0}, Lcom/a/b/i/bc;->a(Ljava/io/File;)Lcom/a/b/i/s;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/b/i/s;->a(Lcom/a/b/g/ak;)Lcom/a/b/g/ag;

    move-result-object v0

    return-object v0
.end method

.method private static varargs a(Ljava/io/File;Ljava/nio/charset/Charset;[Lcom/a/b/i/bb;)Lcom/a/b/i/ag;
    .registers 6

    .prologue
    .line 231
    invoke-static {p0, p2}, Lcom/a/b/i/bc;->a(Ljava/io/File;[Lcom/a/b/i/bb;)Lcom/a/b/i/p;

    move-result-object v0

    .line 1059
    new-instance v1, Lcom/a/b/i/r;

    const/4 v2, 0x0

    invoke-direct {v1, v0, p1, v2}, Lcom/a/b/i/r;-><init>(Lcom/a/b/i/p;Ljava/nio/charset/Charset;B)V

    .line 231
    return-object v1
.end method

.method private static varargs a(Ljava/io/File;[Lcom/a/b/i/bb;)Lcom/a/b/i/p;
    .registers 4

    .prologue
    .line 185
    new-instance v0, Lcom/a/b/i/bf;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/a/b/i/bf;-><init>(Ljava/io/File;[Lcom/a/b/i/bb;B)V

    return-object v0
.end method

.method private static a(Ljava/io/File;)Lcom/a/b/i/s;
    .registers 3

    .prologue
    .line 112
    new-instance v0, Lcom/a/b/i/bg;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/b/i/bg;-><init>(Ljava/io/File;B)V

    return-object v0
.end method

.method private static a(Ljava/io/File;Ljava/nio/charset/Charset;)Ljava/io/BufferedReader;
    .registers 5

    .prologue
    .line 83
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/InputStreamReader;

    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v2, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    return-object v0
.end method

.method private static a()Ljava/io/File;
    .registers 7

    .prologue
    .line 414
    new-instance v1, Ljava/io/File;

    const-string v0, "java.io.tmpdir"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 415
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v4, 0x15

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "-"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 417
    const/4 v0, 0x0

    :goto_25
    const/16 v3, 0x2710

    if-ge v0, v3, :cond_57

    .line 418
    new-instance v3, Ljava/io/File;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0xb

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 419
    invoke-virtual {v3}, Ljava/io/File;->mkdir()Z

    move-result v4

    if-eqz v4, :cond_54

    .line 420
    return-object v3

    .line 417
    :cond_54
    add-int/lit8 v0, v0, 0x1

    goto :goto_25

    .line 423
    :cond_57
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Failed to create directory within 10000 attempts (tried "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x11

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "0 to "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x270f

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(Ljava/io/File;Lcom/a/b/i/o;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 580
    invoke-static {p0}, Lcom/a/b/i/bc;->a(Ljava/io/File;)Lcom/a/b/i/s;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/b/i/s;->a(Lcom/a/b/i/o;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/io/File;Ljava/nio/charset/Charset;Lcom/a/b/i/by;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 564
    invoke-static {p0, p1}, Lcom/a/b/i/bc;->c(Ljava/io/File;Ljava/nio/charset/Charset;)Lcom/a/b/i/ah;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/a/b/i/ah;->a(Lcom/a/b/i/by;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .registers 6

    .prologue
    const/16 v4, 0x2f

    .line 719
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 720
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_e

    .line 721
    const-string v0, "."

    .line 759
    :cond_d
    :goto_d
    return-object v0

    .line 725
    :cond_e
    invoke-static {v4}, Lcom/a/b/b/di;->a(C)Lcom/a/b/b/di;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/b/di;->a()Lcom/a/b/b/di;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/a/b/b/di;->a(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    .line 727
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 730
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_23
    :goto_23
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 731
    const-string v3, "."

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_23

    .line 733
    const-string v3, ".."

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_69

    .line 734
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_63

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v3, ".."

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_63

    .line 735
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_23

    .line 737
    :cond_63
    const-string v0, ".."

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_23

    .line 740
    :cond_69
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_23

    .line 745
    :cond_6d
    invoke-static {v4}, Lcom/a/b/b/bv;->a(C)Lcom/a/b/b/bv;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/a/b/b/bv;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 746
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-ne v1, v4, :cond_8c

    .line 747
    const-string v1, "/"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_9a

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 750
    :cond_8c
    :goto_8c
    const-string v1, "/../"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a0

    .line 751
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_8c

    .line 747
    :cond_9a
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_8c

    .line 753
    :cond_a0
    const-string v1, "/.."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_ac

    .line 754
    const-string v0, "/"

    goto/16 :goto_d

    .line 755
    :cond_ac
    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 756
    const-string v0, "."

    goto/16 :goto_d
.end method

.method private static a(Ljava/io/File;Ljava/nio/channels/FileChannel$MapMode;)Ljava/nio/MappedByteBuffer;
    .registers 4

    .prologue
    .line 638
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 639
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 640
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_16

    .line 641
    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-virtual {p0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 643
    :cond_16
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v0

    invoke-static {p0, p1, v0, v1}, Lcom/a/b/i/bc;->a(Ljava/io/File;Ljava/nio/channels/FileChannel$MapMode;J)Ljava/nio/MappedByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/io/File;Ljava/nio/channels/FileChannel$MapMode;J)Ljava/nio/MappedByteBuffer;
    .registers 8

    .prologue
    .line 669
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 670
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 672
    invoke-static {}, Lcom/a/b/i/ar;->a()Lcom/a/b/i/ar;

    move-result-object v1

    .line 674
    :try_start_a
    new-instance v2, Ljava/io/RandomAccessFile;

    sget-object v0, Ljava/nio/channels/FileChannel$MapMode;->READ_ONLY:Ljava/nio/channels/FileChannel$MapMode;

    if-ne p1, v0, :cond_23

    const-string v0, "r"

    :goto_12
    invoke-direct {v2, p0, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/a/b/i/ar;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/RandomAccessFile;

    .line 676
    invoke-static {v0, p1, p2, p3}, Lcom/a/b/i/bc;->a(Ljava/io/RandomAccessFile;Ljava/nio/channels/FileChannel$MapMode;J)Ljava/nio/MappedByteBuffer;
    :try_end_1e
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_1e} :catch_26
    .catchall {:try_start_a .. :try_end_1e} :catchall_2c

    move-result-object v0

    .line 680
    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    return-object v0

    .line 674
    :cond_23
    :try_start_23
    const-string v0, "rw"
    :try_end_25
    .catch Ljava/lang/Throwable; {:try_start_23 .. :try_end_25} :catch_26
    .catchall {:try_start_23 .. :try_end_25} :catchall_2c

    goto :goto_12

    .line 677
    :catch_26
    move-exception v0

    .line 678
    :try_start_27
    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_2c
    .catchall {:try_start_27 .. :try_end_2c} :catchall_2c

    .line 680
    :catchall_2c
    move-exception v0

    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    throw v0
.end method

.method private static a(Ljava/io/RandomAccessFile;Ljava/nio/channels/FileChannel$MapMode;J)Ljava/nio/MappedByteBuffer;
    .registers 12

    .prologue
    .line 686
    invoke-static {}, Lcom/a/b/i/ar;->a()Lcom/a/b/i/ar;

    move-result-object v6

    .line 688
    :try_start_4
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/a/b/i/ar;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/nio/channels/FileChannel;

    .line 689
    const-wide/16 v2, 0x0

    move-object v1, p1

    move-wide v4, p2

    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
    :try_end_15
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_15} :catch_1a
    .catchall {:try_start_4 .. :try_end_15} :catchall_20

    move-result-object v0

    .line 693
    invoke-virtual {v6}, Lcom/a/b/i/ar;->close()V

    return-object v0

    .line 690
    :catch_1a
    move-exception v0

    .line 691
    :try_start_1b
    invoke-virtual {v6, v0}, Lcom/a/b/i/ar;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_20
    .catchall {:try_start_1b .. :try_end_20} :catchall_20

    .line 693
    :catchall_20
    move-exception v0

    invoke-virtual {v6}, Lcom/a/b/i/ar;->close()V

    throw v0
.end method

.method private static a(Ljava/io/File;Ljava/io/File;)V
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 303
    invoke-virtual {p0, p1}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_23

    move v0, v1

    :goto_9
    const-string v3, "Source %s and destination %s must be different"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p0, v4, v2

    aput-object p1, v4, v1

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 305
    invoke-static {p0}, Lcom/a/b/i/bc;->a(Ljava/io/File;)Lcom/a/b/i/s;

    move-result-object v0

    new-array v1, v2, [Lcom/a/b/i/bb;

    invoke-static {p1, v1}, Lcom/a/b/i/bc;->a(Ljava/io/File;[Lcom/a/b/i/bb;)Lcom/a/b/i/p;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/i/s;->a(Lcom/a/b/i/p;)J

    .line 306
    return-void

    :cond_23
    move v0, v2

    .line 303
    goto :goto_9
.end method

.method private static a(Ljava/io/File;Ljava/io/OutputStream;)V
    .registers 3

    .prologue
    .line 286
    invoke-static {p0}, Lcom/a/b/i/bc;->a(Ljava/io/File;)Lcom/a/b/i/s;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/b/i/s;->a(Ljava/io/OutputStream;)J

    .line 287
    return-void
.end method

.method private static a(Ljava/io/File;Ljava/nio/charset/Charset;Ljava/lang/Appendable;)V
    .registers 4

    .prologue
    .line 366
    invoke-static {p0, p1}, Lcom/a/b/i/bc;->c(Ljava/io/File;Ljava/nio/charset/Charset;)Lcom/a/b/i/ah;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/a/b/i/ah;->a(Ljava/lang/Appendable;)J

    .line 367
    return-void
.end method

.method private static a(Ljava/lang/CharSequence;Ljava/io/File;Ljava/nio/charset/Charset;)V
    .registers 4

    .prologue
    .line 320
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/a/b/i/bb;

    invoke-static {p1, p2, v0}, Lcom/a/b/i/bc;->a(Ljava/io/File;Ljava/nio/charset/Charset;[Lcom/a/b/i/bb;)Lcom/a/b/i/ag;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/a/b/i/ag;->a(Ljava/lang/CharSequence;)V

    .line 321
    return-void
.end method

.method private static a([BLjava/io/File;)V
    .registers 4

    .prologue
    .line 275
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/a/b/i/bb;

    invoke-static {p1, v0}, Lcom/a/b/i/bc;->a(Ljava/io/File;[Lcom/a/b/i/bb;)Lcom/a/b/i/p;

    move-result-object v0

    .line 1097
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1099
    invoke-static {}, Lcom/a/b/i/ar;->a()Lcom/a/b/i/ar;

    move-result-object v1

    .line 1101
    :try_start_e
    invoke-virtual {v0}, Lcom/a/b/i/p;->a()Ljava/io/OutputStream;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/OutputStream;

    .line 1102
    invoke-virtual {v0, p0}, Ljava/io/OutputStream;->write([B)V

    .line 1103
    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V
    :try_end_1e
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_1e} :catch_22
    .catchall {:try_start_e .. :try_end_1e} :catchall_28

    .line 1107
    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    .line 1108
    return-void

    .line 1104
    :catch_22
    move-exception v0

    .line 1105
    :try_start_23
    invoke-virtual {v1, v0}, Lcom/a/b/i/ar;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_28
    .catchall {:try_start_23 .. :try_end_28} :catchall_28

    .line 1107
    :catchall_28
    move-exception v0

    invoke-virtual {v1}, Lcom/a/b/i/ar;->close()V

    throw v0
.end method

.method static a(Ljava/io/InputStream;J)[B
    .registers 6

    .prologue
    .line 163
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p1, v0

    if-lez v0, :cond_28

    .line 164
    new-instance v0, Ljava/lang/OutOfMemoryError;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x44

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "file is too large to fit in a byte array: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    throw v0

    .line 170
    :cond_28
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_33

    invoke-static {p0}, Lcom/a/b/i/z;->a(Ljava/io/InputStream;)[B

    move-result-object v0

    :goto_32
    return-object v0

    :cond_33
    long-to-int v0, p1

    invoke-static {p0, v0}, Lcom/a/b/i/z;->a(Ljava/io/InputStream;I)[B

    move-result-object v0

    goto :goto_32
.end method

.method private static a(Z)[Lcom/a/b/i/bb;
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 235
    if-eqz p0, :cond_b

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/a/b/i/bb;

    sget-object v1, Lcom/a/b/i/bb;->a:Lcom/a/b/i/bb;

    aput-object v1, v0, v2

    :goto_a
    return-object v0

    :cond_b
    new-array v0, v2, [Lcom/a/b/i/bb;

    goto :goto_a
.end method

.method private static b()Lcom/a/b/d/aga;
    .registers 1

    .prologue
    .line 804
    sget-object v0, Lcom/a/b/i/bc;->b:Lcom/a/b/d/aga;

    return-object v0
.end method

.method private static b(Ljava/io/File;Ljava/nio/charset/Charset;)Ljava/io/BufferedWriter;
    .registers 5

    .prologue
    .line 100
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    new-instance v0, Ljava/io/BufferedWriter;

    new-instance v1, Ljava/io/OutputStreamWriter;

    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v2, p1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .registers 4

    .prologue
    .line 770
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 771
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 772
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 773
    const/4 v2, -0x1

    if-ne v1, v2, :cond_18

    const-string v0, ""

    :goto_17
    return-object v0

    :cond_18
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_17
.end method

.method private static b(Ljava/lang/CharSequence;Ljava/io/File;Ljava/nio/charset/Charset;)V
    .registers 6

    .prologue
    .line 1351
    .line 2235
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/a/b/i/bb;

    const/4 v1, 0x0

    sget-object v2, Lcom/a/b/i/bb;->a:Lcom/a/b/i/bb;

    aput-object v2, v0, v1

    .line 1351
    invoke-static {p1, p2, v0}, Lcom/a/b/i/bc;->a(Ljava/io/File;Ljava/nio/charset/Charset;[Lcom/a/b/i/bb;)Lcom/a/b/i/ag;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/a/b/i/ag;->a(Ljava/lang/CharSequence;)V

    .line 336
    return-void
.end method

.method private static b(Ljava/io/File;Ljava/io/File;)Z
    .registers 10

    .prologue
    const-wide/16 v6, 0x0

    .line 375
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 376
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 377
    if-eq p0, p1, :cond_10

    invoke-virtual {p0, p1}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 378
    :cond_10
    const/4 v0, 0x1

    .line 391
    :goto_11
    return v0

    .line 386
    :cond_12
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v0

    .line 387
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 388
    cmp-long v4, v0, v6

    if-eqz v4, :cond_28

    cmp-long v4, v2, v6

    if-eqz v4, :cond_28

    cmp-long v0, v0, v2

    if-eqz v0, :cond_28

    .line 389
    const/4 v0, 0x0

    goto :goto_11

    .line 391
    :cond_28
    invoke-static {p0}, Lcom/a/b/i/bc;->a(Ljava/io/File;)Lcom/a/b/i/s;

    move-result-object v0

    invoke-static {p1}, Lcom/a/b/i/bc;->a(Ljava/io/File;)Lcom/a/b/i/s;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/i/s;->a(Lcom/a/b/i/s;)Z

    move-result v0

    goto :goto_11
.end method

.method private static b(Ljava/io/File;)[B
    .registers 2

    .prologue
    .line 250
    invoke-static {p0}, Lcom/a/b/i/bc;->a(Ljava/io/File;)Lcom/a/b/i/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/i/s;->e()[B

    move-result-object v0

    return-object v0
.end method

.method private static c()Lcom/a/b/b/co;
    .registers 1

    .prologue
    .line 833
    sget-object v0, Lcom/a/b/i/bh;->a:Lcom/a/b/i/bh;

    return-object v0
.end method

.method private static c(Ljava/io/File;Ljava/nio/charset/Charset;)Lcom/a/b/i/ah;
    .registers 3

    .prologue
    .line 216
    invoke-static {p0}, Lcom/a/b/i/bc;->a(Ljava/io/File;)Lcom/a/b/i/s;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/a/b/i/s;->a(Ljava/nio/charset/Charset;)Lcom/a/b/i/ah;

    move-result-object v0

    return-object v0
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .registers 4

    .prologue
    .line 787
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 788
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 789
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 790
    const/4 v2, -0x1

    if-ne v1, v2, :cond_16

    :goto_15
    return-object v0

    :cond_16
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_15
.end method

.method private static c(Ljava/io/File;)V
    .registers 5

    .prologue
    .line 436
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 437
    invoke-virtual {p0}, Ljava/io/File;->createNewFile()Z

    move-result v0

    if-nez v0, :cond_3a

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Ljava/io/File;->setLastModified(J)Z

    move-result v0

    if-nez v0, :cond_3a

    .line 439
    new-instance v0, Ljava/io/IOException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x26

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unable to update modification time of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 441
    :cond_3a
    return-void
.end method

.method private static c(Ljava/io/File;Ljava/io/File;)V
    .registers 8

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 484
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 485
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 486
    invoke-virtual {p0, p1}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_73

    move v0, v1

    :goto_10
    const-string v3, "Source %s and destination %s must be different"

    new-array v4, v5, [Ljava/lang/Object;

    aput-object p0, v4, v2

    aput-object p1, v4, v1

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 489
    invoke-virtual {p0, p1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_9e

    .line 3303
    invoke-virtual {p0, p1}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_75

    move v0, v1

    :goto_28
    const-string v3, "Source %s and destination %s must be different"

    new-array v4, v5, [Ljava/lang/Object;

    aput-object p0, v4, v2

    aput-object p1, v4, v1

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 3305
    invoke-static {p0}, Lcom/a/b/i/bc;->a(Ljava/io/File;)Lcom/a/b/i/s;

    move-result-object v0

    new-array v1, v2, [Lcom/a/b/i/bb;

    invoke-static {p1, v1}, Lcom/a/b/i/bc;->a(Ljava/io/File;[Lcom/a/b/i/bb;)Lcom/a/b/i/p;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/i/s;->a(Lcom/a/b/i/p;)J

    .line 491
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_9e

    .line 492
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_77

    .line 493
    new-instance v0, Ljava/io/IOException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x11

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unable to delete "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_73
    move v0, v2

    .line 486
    goto :goto_10

    :cond_75
    move v0, v2

    .line 3303
    goto :goto_28

    .line 495
    :cond_77
    new-instance v0, Ljava/io/IOException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x11

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unable to delete "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 498
    :cond_9e
    return-void
.end method

.method private static c(Ljava/lang/CharSequence;Ljava/io/File;Ljava/nio/charset/Charset;)V
    .registers 6

    .prologue
    .line 351
    .line 3235
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/a/b/i/bb;

    const/4 v1, 0x0

    sget-object v2, Lcom/a/b/i/bb;->a:Lcom/a/b/i/bb;

    aput-object v2, v0, v1

    .line 351
    invoke-static {p1, p2, v0}, Lcom/a/b/i/bc;->a(Ljava/io/File;Ljava/nio/charset/Charset;[Lcom/a/b/i/bb;)Lcom/a/b/i/ag;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/a/b/i/ag;->a(Ljava/lang/CharSequence;)V

    .line 352
    return-void
.end method

.method private static d()Lcom/a/b/b/co;
    .registers 1

    .prologue
    .line 842
    sget-object v0, Lcom/a/b/i/bh;->b:Lcom/a/b/i/bh;

    return-object v0
.end method

.method private static d(Ljava/io/File;Ljava/nio/charset/Charset;)Ljava/lang/String;
    .registers 3

    .prologue
    .line 264
    invoke-static {p0, p1}, Lcom/a/b/i/bc;->c(Ljava/io/File;Ljava/nio/charset/Charset;)Lcom/a/b/i/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/i/ah;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static d(Ljava/io/File;)V
    .registers 5

    .prologue
    .line 454
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 455
    invoke-virtual {p0}, Ljava/io/File;->getCanonicalFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    .line 456
    if-nez v0, :cond_e

    .line 470
    :cond_d
    return-void

    .line 466
    :cond_e
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 467
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_d

    .line 468
    new-instance v0, Ljava/io/IOException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x27

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unable to create parent directories of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static e(Ljava/io/File;Ljava/nio/charset/Charset;)Ljava/lang/String;
    .registers 3

    .prologue
    .line 513
    invoke-static {p0, p1}, Lcom/a/b/i/bc;->c(Ljava/io/File;Ljava/nio/charset/Charset;)Lcom/a/b/i/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/i/ah;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static e(Ljava/io/File;)Ljava/nio/MappedByteBuffer;
    .registers 5

    .prologue
    .line 614
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 615
    sget-object v0, Ljava/nio/channels/FileChannel$MapMode;->READ_ONLY:Ljava/nio/channels/FileChannel$MapMode;

    .line 3638
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3639
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3640
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1b

    .line 3641
    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-virtual {p0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3643
    :cond_1b
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-static {p0, v0, v2, v3}, Lcom/a/b/i/bc;->a(Ljava/io/File;Ljava/nio/channels/FileChannel$MapMode;J)Ljava/nio/MappedByteBuffer;

    move-result-object v0

    .line 615
    return-object v0
.end method

.method private static f(Ljava/io/File;Ljava/nio/charset/Charset;)Ljava/util/List;
    .registers 4

    .prologue
    .line 535
    new-instance v0, Lcom/a/b/i/bd;

    invoke-direct {v0}, Lcom/a/b/i/bd;-><init>()V

    .line 3564
    invoke-static {p0, p1}, Lcom/a/b/i/bc;->c(Ljava/io/File;Ljava/nio/charset/Charset;)Lcom/a/b/i/ah;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/a/b/i/ah;->a(Lcom/a/b/i/by;)Ljava/lang/Object;

    move-result-object v0

    .line 535
    check-cast v0, Ljava/util/List;

    return-object v0
.end method
