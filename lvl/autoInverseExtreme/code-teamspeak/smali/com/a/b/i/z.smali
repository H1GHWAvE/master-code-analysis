.class public final Lcom/a/b/i/z;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# static fields
.field private static final a:I = 0x1000

.field private static final b:Ljava/io/OutputStream;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 501
    new-instance v0, Lcom/a/b/i/aa;

    invoke-direct {v0}, Lcom/a/b/i/aa;-><init>()V

    sput-object v0, Lcom/a/b/i/z;->b:Ljava/io/OutputStream;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    .registers 8

    .prologue
    .line 65
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    const/16 v0, 0x1000

    new-array v2, v0, [B

    .line 68
    const-wide/16 v0, 0x0

    .line 70
    :goto_c
    invoke-virtual {p0, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    .line 71
    const/4 v4, -0x1

    if-eq v3, v4, :cond_1a

    .line 74
    const/4 v4, 0x0

    invoke-virtual {p1, v2, v4, v3}, Ljava/io/OutputStream;->write([BII)V

    .line 75
    int-to-long v4, v3

    add-long/2addr v0, v4

    .line 76
    goto :goto_c

    .line 77
    :cond_1a
    return-wide v0
.end method

.method private static a(Ljava/nio/channels/ReadableByteChannel;Ljava/nio/channels/WritableByteChannel;)J
    .registers 8

    .prologue
    .line 91
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    const/16 v0, 0x1000

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 94
    const-wide/16 v0, 0x0

    .line 95
    :goto_e
    invoke-interface {p0, v2}, Ljava/nio/channels/ReadableByteChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_29

    .line 96
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 97
    :goto_18
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v3

    if-eqz v3, :cond_25

    .line 98
    invoke-interface {p1, v2}, Ljava/nio/channels/WritableByteChannel;->write(Ljava/nio/ByteBuffer;)I

    move-result v3

    int-to-long v4, v3

    add-long/2addr v0, v4

    goto :goto_18

    .line 100
    :cond_25
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    goto :goto_e

    .line 102
    :cond_29
    return-wide v0
.end method

.method private static a(Ljava/io/ByteArrayInputStream;)Lcom/a/b/i/m;
    .registers 3

    .prologue
    .line 202
    new-instance v1, Lcom/a/b/i/ab;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v0}, Lcom/a/b/i/ab;-><init>(Ljava/io/ByteArrayInputStream;)V

    return-object v1
.end method

.method public static a([B)Lcom/a/b/i/m;
    .registers 2

    .prologue
    .line 177
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-static {v0}, Lcom/a/b/i/z;->a(Ljava/io/ByteArrayInputStream;)Lcom/a/b/i/m;

    move-result-object v0

    return-object v0
.end method

.method public static a([BI)Lcom/a/b/i/m;
    .registers 4

    .prologue
    .line 188
    array-length v0, p0

    invoke-static {p1, v0}, Lcom/a/b/b/cn;->b(II)I

    .line 189
    new-instance v0, Ljava/io/ByteArrayInputStream;

    array-length v1, p0

    sub-int/2addr v1, p1

    invoke-direct {v0, p0, p1, v1}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    invoke-static {v0}, Lcom/a/b/i/z;->a(Ljava/io/ByteArrayInputStream;)Lcom/a/b/i/m;

    move-result-object v0

    return-object v0
.end method

.method private static a()Lcom/a/b/i/n;
    .registers 1

    .prologue
    .line 339
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {v0}, Lcom/a/b/i/z;->a(Ljava/io/ByteArrayOutputStream;)Lcom/a/b/i/n;

    move-result-object v0

    return-object v0
.end method

.method private static a(I)Lcom/a/b/i/n;
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 349
    if-ltz p0, :cond_1c

    move v0, v1

    :goto_5
    const-string v3, "Invalid size: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 350
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0, p0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    invoke-static {v0}, Lcom/a/b/i/z;->a(Ljava/io/ByteArrayOutputStream;)Lcom/a/b/i/n;

    move-result-object v0

    return-object v0

    :cond_1c
    move v0, v2

    .line 349
    goto :goto_5
.end method

.method private static a(Ljava/io/ByteArrayOutputStream;)Lcom/a/b/i/n;
    .registers 3

    .prologue
    .line 369
    new-instance v1, Lcom/a/b/i/ac;

    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1, v0}, Lcom/a/b/i/ac;-><init>(Ljava/io/ByteArrayOutputStream;)V

    return-object v1
.end method

.method public static a(Ljava/io/InputStream;J)Ljava/io/InputStream;
    .registers 4

    .prologue
    .line 540
    new-instance v0, Lcom/a/b/i/ae;

    invoke-direct {v0, p0, p1, p2}, Lcom/a/b/i/ae;-><init>(Ljava/io/InputStream;J)V

    return-object v0
.end method

.method public static a(Ljava/io/InputStream;Lcom/a/b/i/o;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 689
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 690
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 692
    const/16 v0, 0x1000

    new-array v0, v0, [B

    .line 695
    :cond_a
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .line 696
    const/4 v2, -0x1

    if-eq v1, v2, :cond_17

    invoke-interface {p1}, Lcom/a/b/i/o;->a()Z

    move-result v1

    if-nez v1, :cond_a

    .line 697
    :cond_17
    invoke-interface {p1}, Lcom/a/b/i/o;->b()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/io/InputStream;[B)V
    .registers 4

    .prologue
    .line 622
    const/4 v0, 0x0

    array-length v1, p1

    invoke-static {p0, p1, v0, v1}, Lcom/a/b/i/z;->a(Ljava/io/InputStream;[BII)V

    .line 623
    return-void
.end method

.method public static a(Ljava/io/InputStream;[BII)V
    .registers 8

    .prologue
    .line 641
    invoke-static {p0, p1, p2, p3}, Lcom/a/b/i/z;->b(Ljava/io/InputStream;[BII)I

    move-result v0

    .line 642
    if-eq v0, p3, :cond_31

    .line 643
    new-instance v1, Ljava/io/EOFException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x51

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "reached end of stream after reading "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " bytes; "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " bytes expected"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 646
    :cond_31
    return-void
.end method

.method public static a(Ljava/io/InputStream;)[B
    .registers 2

    .prologue
    .line 114
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 115
    invoke-static {p0, v0}, Lcom/a/b/i/z;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    .line 116
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/io/InputStream;I)[B
    .registers 8

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 127
    new-array v0, p1, [B

    move v1, p1

    .line 130
    :goto_5
    if-lez v1, :cond_16

    .line 131
    sub-int v2, p1, v1

    .line 132
    invoke-virtual {p0, v0, v2, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v3

    .line 133
    if-ne v3, v5, :cond_14

    .line 136
    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    .line 155
    :cond_13
    :goto_13
    return-object v0

    .line 138
    :cond_14
    sub-int/2addr v1, v3

    .line 139
    goto :goto_5

    .line 142
    :cond_16
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 143
    if-eq v1, v5, :cond_13

    .line 148
    new-instance v2, Lcom/a/b/i/ad;

    invoke-direct {v2, v4}, Lcom/a/b/i/ad;-><init>(B)V

    .line 149
    invoke-virtual {v2, v1}, Lcom/a/b/i/ad;->write(I)V

    .line 150
    invoke-static {p0, v2}, Lcom/a/b/i/z;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    .line 152
    array-length v1, v0

    invoke-virtual {v2}, Lcom/a/b/i/ad;->size()I

    move-result v3

    add-int/2addr v1, v3

    new-array v1, v1, [B

    .line 153
    array-length v3, v0

    invoke-static {v0, v4, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 154
    array-length v0, v0

    invoke-virtual {v2, v1, v0}, Lcom/a/b/i/ad;->a([BI)V

    move-object v0, v1

    .line 155
    goto :goto_13
.end method

.method public static b(Ljava/io/InputStream;[BII)I
    .registers 7

    .prologue
    .line 726
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 727
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 728
    if-gez p3, :cond_10

    .line 729
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "len is negative"

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 731
    :cond_10
    const/4 v0, 0x0

    .line 732
    :goto_11
    if-ge v0, p3, :cond_20

    .line 733
    add-int v1, p2, v0

    sub-int v2, p3, v0

    invoke-virtual {p0, p1, v1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 734
    const/4 v2, -0x1

    if-eq v1, v2, :cond_20

    .line 737
    add-int/2addr v0, v1

    .line 738
    goto :goto_11

    .line 739
    :cond_20
    return v0
.end method

.method private static b()Ljava/io/OutputStream;
    .registers 1

    .prologue
    .line 527
    sget-object v0, Lcom/a/b/i/z;->b:Ljava/io/OutputStream;

    return-object v0
.end method

.method public static b(Ljava/io/InputStream;J)V
    .registers 12

    .prologue
    const-wide/16 v6, 0x0

    .line 661
    move-wide v0, p1

    .line 662
    :goto_3
    cmp-long v2, v0, v6

    if-lez v2, :cond_49

    .line 663
    invoke-virtual {p0, v0, v1}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v2

    .line 664
    cmp-long v4, v2, v6

    if-nez v4, :cond_47

    .line 666
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_43

    .line 667
    sub-long v0, p1, v0

    .line 668
    new-instance v2, Ljava/io/EOFException;

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "reached end of stream after skipping "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " bytes; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " bytes expected"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 671
    :cond_43
    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    goto :goto_3

    .line 673
    :cond_47
    sub-long/2addr v0, v2

    .line 675
    goto :goto_3

    .line 676
    :cond_49
    return-void
.end method
