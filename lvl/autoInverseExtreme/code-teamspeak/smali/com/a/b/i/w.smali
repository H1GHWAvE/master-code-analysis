.class final Lcom/a/b/i/w;
.super Lcom/a/b/i/s;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/Iterable;


# direct methods
.method constructor <init>(Ljava/lang/Iterable;)V
    .registers 3

    .prologue
    .line 578
    invoke-direct {p0}, Lcom/a/b/i/s;-><init>()V

    .line 579
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    iput-object v0, p0, Lcom/a/b/i/w;->a:Ljava/lang/Iterable;

    .line 580
    return-void
.end method


# virtual methods
.method public final a()Ljava/io/InputStream;
    .registers 3

    .prologue
    .line 584
    new-instance v0, Lcom/a/b/i/cd;

    iget-object v1, p0, Lcom/a/b/i/w;->a:Ljava/lang/Iterable;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/b/i/cd;-><init>(Ljava/util/Iterator;)V

    return-object v0
.end method

.method public final c()Z
    .registers 3

    .prologue
    .line 589
    iget-object v0, p0, Lcom/a/b/i/w;->a:Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/i/s;

    .line 590
    invoke-virtual {v0}, Lcom/a/b/i/s;->c()Z

    move-result v0

    if-nez v0, :cond_6

    .line 591
    const/4 v0, 0x0

    .line 594
    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x1

    goto :goto_19
.end method

.method public final d()J
    .registers 6

    .prologue
    .line 599
    const-wide/16 v0, 0x0

    .line 600
    iget-object v2, p0, Lcom/a/b/i/w;->a:Ljava/lang/Iterable;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_9
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/i/s;

    .line 601
    invoke-virtual {v0}, Lcom/a/b/i/s;->d()J

    move-result-wide v0

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 602
    goto :goto_9

    .line 603
    :cond_1c
    return-wide v2
.end method

.method public final toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 608
    iget-object v0, p0, Lcom/a/b/i/w;->a:Ljava/lang/Iterable;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x13

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "ByteSource.concat("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
