.class final Lcom/a/b/i/i;
.super Lcom/a/b/i/b;
.source "SourceFile"


# instance fields
.field private final a:Lcom/a/b/i/b;

.field private final b:Ljava/lang/String;

.field private final c:I

.field private final d:Lcom/a/b/b/m;


# direct methods
.method constructor <init>(Lcom/a/b/i/b;Ljava/lang/String;I)V
    .registers 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 814
    invoke-direct {p0}, Lcom/a/b/i/b;-><init>()V

    .line 815
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/i/b;

    iput-object v0, p0, Lcom/a/b/i/i;->a:Lcom/a/b/i/b;

    .line 816
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/a/b/i/i;->b:Ljava/lang/String;

    .line 817
    iput p3, p0, Lcom/a/b/i/i;->c:I

    .line 818
    if-lez p3, :cond_32

    move v0, v1

    :goto_1a
    const-string v3, "Cannot add a separator after every %s chars"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 820
    invoke-static {p2}, Lcom/a/b/b/m;->a(Ljava/lang/CharSequence;)Lcom/a/b/b/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/b/b/m;->b()Lcom/a/b/b/m;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/i/i;->d:Lcom/a/b/b/m;

    .line 821
    return-void

    :cond_32
    move v0, v2

    .line 818
    goto :goto_1a
.end method


# virtual methods
.method final a(I)I
    .registers 7

    .prologue
    .line 830
    iget-object v0, p0, Lcom/a/b/i/i;->a:Lcom/a/b/i/b;

    invoke-virtual {v0, p1}, Lcom/a/b/i/b;->a(I)I

    move-result v0

    .line 831
    iget-object v1, p0, Lcom/a/b/i/i;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x0

    add-int/lit8 v3, v0, -0x1

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget v3, p0, Lcom/a/b/i/i;->c:I

    sget-object v4, Ljava/math/RoundingMode;->FLOOR:Ljava/math/RoundingMode;

    invoke-static {v2, v3, v4}, Lcom/a/b/j/g;->a(IILjava/math/RoundingMode;)I

    move-result v2

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method final a()Lcom/a/b/b/m;
    .registers 2

    .prologue
    .line 825
    iget-object v0, p0, Lcom/a/b/i/i;->a:Lcom/a/b/i/b;

    invoke-virtual {v0}, Lcom/a/b/i/b;->a()Lcom/a/b/b/m;

    move-result-object v0

    return-object v0
.end method

.method public final a(C)Lcom/a/b/i/b;
    .registers 5

    .prologue
    .line 857
    iget-object v0, p0, Lcom/a/b/i/i;->a:Lcom/a/b/i/b;

    invoke-virtual {v0, p1}, Lcom/a/b/i/b;->a(C)Lcom/a/b/i/b;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/i/i;->b:Ljava/lang/String;

    iget v2, p0, Lcom/a/b/i/i;->c:I

    invoke-virtual {v0, v1, v2}, Lcom/a/b/i/b;->a(Ljava/lang/String;I)Lcom/a/b/i/b;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;I)Lcom/a/b/i/b;
    .registers 5

    .prologue
    .line 862
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Already have a separator"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method final a(Lcom/a/b/i/bu;)Lcom/a/b/i/bs;
    .registers 5

    .prologue
    .line 847
    iget-object v0, p0, Lcom/a/b/i/i;->a:Lcom/a/b/i/b;

    iget-object v1, p0, Lcom/a/b/i/i;->d:Lcom/a/b/b/m;

    .line 2757
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2758
    invoke-static {v1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2759
    new-instance v2, Lcom/a/b/i/e;

    invoke-direct {v2, p1, v1}, Lcom/a/b/i/e;-><init>(Lcom/a/b/i/bu;Lcom/a/b/b/m;)V

    .line 847
    invoke-virtual {v0, v2}, Lcom/a/b/i/b;->a(Lcom/a/b/i/bu;)Lcom/a/b/i/bs;

    move-result-object v0

    return-object v0
.end method

.method final a(Lcom/a/b/i/bv;)Lcom/a/b/i/bt;
    .registers 6

    .prologue
    .line 837
    iget-object v1, p0, Lcom/a/b/i/i;->a:Lcom/a/b/i/b;

    iget-object v2, p0, Lcom/a/b/i/i;->b:Ljava/lang/String;

    iget v3, p0, Lcom/a/b/i/i;->c:I

    .line 1778
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1779
    invoke-static {v2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1780
    if-lez v3, :cond_1c

    const/4 v0, 0x1

    :goto_f
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 1781
    new-instance v0, Lcom/a/b/i/f;

    invoke-direct {v0, v3, v2, p1}, Lcom/a/b/i/f;-><init>(ILjava/lang/String;Lcom/a/b/i/bv;)V

    .line 837
    invoke-virtual {v1, v0}, Lcom/a/b/i/b;->a(Lcom/a/b/i/bv;)Lcom/a/b/i/bt;

    move-result-object v0

    return-object v0

    .line 1780
    :cond_1c
    const/4 v0, 0x0

    goto :goto_f
.end method

.method final b(I)I
    .registers 3

    .prologue
    .line 842
    iget-object v0, p0, Lcom/a/b/i/i;->a:Lcom/a/b/i/b;

    invoke-virtual {v0, p1}, Lcom/a/b/i/b;->b(I)I

    move-result v0

    return v0
.end method

.method public final b()Lcom/a/b/i/b;
    .registers 4

    .prologue
    .line 852
    iget-object v0, p0, Lcom/a/b/i/i;->a:Lcom/a/b/i/b;

    invoke-virtual {v0}, Lcom/a/b/i/b;->b()Lcom/a/b/i/b;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/i/i;->b:Ljava/lang/String;

    iget v2, p0, Lcom/a/b/i/i;->c:I

    invoke-virtual {v0, v1, v2}, Lcom/a/b/i/b;->a(Ljava/lang/String;I)Lcom/a/b/i/b;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/a/b/i/b;
    .registers 4

    .prologue
    .line 867
    iget-object v0, p0, Lcom/a/b/i/i;->a:Lcom/a/b/i/b;

    invoke-virtual {v0}, Lcom/a/b/i/b;->c()Lcom/a/b/i/b;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/i/i;->b:Ljava/lang/String;

    iget v2, p0, Lcom/a/b/i/i;->c:I

    invoke-virtual {v0, v1, v2}, Lcom/a/b/i/b;->a(Ljava/lang/String;I)Lcom/a/b/i/b;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/a/b/i/b;
    .registers 4

    .prologue
    .line 872
    iget-object v0, p0, Lcom/a/b/i/i;->a:Lcom/a/b/i/b;

    invoke-virtual {v0}, Lcom/a/b/i/b;->d()Lcom/a/b/i/b;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/i/i;->b:Ljava/lang/String;

    iget v2, p0, Lcom/a/b/i/i;->c:I

    invoke-virtual {v0, v1, v2}, Lcom/a/b/i/b;->a(Ljava/lang/String;I)Lcom/a/b/i/b;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 7

    .prologue
    .line 877
    iget-object v0, p0, Lcom/a/b/i/i;->a:Lcom/a/b/i/b;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/a/b/i/i;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/a/b/i/i;->c:I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1f

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ".withSeparator(\""

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
