.class Lcom/a/b/c/bo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/c/e;
.implements Ljava/io/Serializable;


# static fields
.field private static final b:J = 0x1L


# instance fields
.field final a:Lcom/a/b/c/ao;


# direct methods
.method private constructor <init>(Lcom/a/b/c/ao;)V
    .registers 2

    .prologue
    .line 4724
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4725
    iput-object p1, p0, Lcom/a/b/c/bo;->a:Lcom/a/b/c/ao;

    .line 4726
    return-void
.end method

.method synthetic constructor <init>(Lcom/a/b/c/ao;B)V
    .registers 3

    .prologue
    .line 4717
    invoke-direct {p0, p1}, Lcom/a/b/c/bo;-><init>(Lcom/a/b/c/ao;)V

    return-void
.end method

.method constructor <init>(Lcom/a/b/c/f;)V
    .registers 4

    .prologue
    .line 4721
    new-instance v0, Lcom/a/b/c/ao;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/a/b/c/ao;-><init>(Lcom/a/b/c/f;Lcom/a/b/c/ab;)V

    invoke-direct {p0, v0}, Lcom/a/b/c/bo;-><init>(Lcom/a/b/c/ao;)V

    .line 4722
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Iterable;)Lcom/a/b/d/jt;
    .registers 9

    .prologue
    const/4 v0, 0x0

    .line 4749
    iget-object v2, p0, Lcom/a/b/c/bo;->a:Lcom/a/b/c/ao;

    .line 4948
    invoke-static {}, Lcom/a/b/d/sz;->d()Ljava/util/LinkedHashMap;

    move-result-object v3

    .line 4949
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_c
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_25

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 4950
    invoke-virtual {v2, v5}, Lcom/a/b/c/ao;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 4951
    if-nez v6, :cond_1f

    .line 4952
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 4957
    :cond_1f
    invoke-interface {v3, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4958
    add-int/lit8 v1, v1, 0x1

    .line 4960
    goto :goto_c

    .line 4961
    :cond_25
    iget-object v4, v2, Lcom/a/b/c/ao;->x:Lcom/a/b/c/c;

    invoke-interface {v4, v1}, Lcom/a/b/c/c;->a(I)V

    .line 4962
    iget-object v1, v2, Lcom/a/b/c/ao;->x:Lcom/a/b/c/c;

    invoke-interface {v1, v0}, Lcom/a/b/c/c;->b(I)V

    .line 4963
    invoke-static {v3}, Lcom/a/b/d/jt;->a(Ljava/util/Map;)Lcom/a/b/d/jt;

    move-result-object v0

    .line 4749
    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    .registers 5

    .prologue
    .line 4738
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4739
    iget-object v0, p0, Lcom/a/b/c/bo;->a:Lcom/a/b/c/ao;

    new-instance v1, Lcom/a/b/c/bp;

    invoke-direct {v1, p0, p2}, Lcom/a/b/c/bp;-><init>(Lcom/a/b/c/bo;Ljava/util/concurrent/Callable;)V

    invoke-virtual {v0, p1, v1}, Lcom/a/b/c/ao;->a(Ljava/lang/Object;Lcom/a/b/c/ab;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .registers 5

    .prologue
    .line 4800
    iget-object v0, p0, Lcom/a/b/c/bo;->a:Lcom/a/b/c/ao;

    .line 5860
    iget-object v1, v0, Lcom/a/b/c/ao;->i:[Lcom/a/b/c/bt;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_6
    if-ge v0, v2, :cond_10

    aget-object v3, v1, v0

    .line 5861
    invoke-virtual {v3}, Lcom/a/b/c/bt;->b()V

    .line 5860
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 4801
    :cond_10
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 4764
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4765
    iget-object v0, p0, Lcom/a/b/c/bo;->a:Lcom/a/b/c/ao;

    invoke-virtual {v0, p1}, Lcom/a/b/c/ao;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4766
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 4

    .prologue
    .line 4754
    iget-object v0, p0, Lcom/a/b/c/bo;->a:Lcom/a/b/c/ao;

    invoke-virtual {v0, p1, p2}, Lcom/a/b/c/ao;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4755
    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .registers 3

    .prologue
    .line 4759
    iget-object v0, p0, Lcom/a/b/c/bo;->a:Lcom/a/b/c/ao;

    invoke-virtual {v0, p1}, Lcom/a/b/c/ao;->putAll(Ljava/util/Map;)V

    .line 4760
    return-void
.end method

.method public final b()J
    .registers 3

    .prologue
    .line 4780
    iget-object v0, p0, Lcom/a/b/c/bo;->a:Lcom/a/b/c/ao;

    invoke-virtual {v0}, Lcom/a/b/c/ao;->m()J

    move-result-wide v0

    return-wide v0
.end method

.method public final b(Ljava/lang/Iterable;)V
    .registers 5

    .prologue
    .line 4770
    iget-object v0, p0, Lcom/a/b/c/bo;->a:Lcom/a/b/c/ao;

    .line 5213
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_14

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 5214
    invoke-virtual {v0, v2}, Lcom/a/b/c/ao;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_6

    .line 4771
    :cond_14
    return-void
.end method

.method public final c()V
    .registers 2

    .prologue
    .line 4775
    iget-object v0, p0, Lcom/a/b/c/bo;->a:Lcom/a/b/c/ao;

    invoke-virtual {v0}, Lcom/a/b/c/ao;->clear()V

    .line 4776
    return-void
.end method

.method public final d()Lcom/a/b/c/ai;
    .registers 6

    .prologue
    .line 4790
    new-instance v1, Lcom/a/b/c/b;

    invoke-direct {v1}, Lcom/a/b/c/b;-><init>()V

    .line 4791
    iget-object v0, p0, Lcom/a/b/c/bo;->a:Lcom/a/b/c/ao;

    iget-object v0, v0, Lcom/a/b/c/ao;->x:Lcom/a/b/c/c;

    invoke-virtual {v1, v0}, Lcom/a/b/c/b;->a(Lcom/a/b/c/c;)V

    .line 4792
    iget-object v0, p0, Lcom/a/b/c/bo;->a:Lcom/a/b/c/ao;

    iget-object v2, v0, Lcom/a/b/c/ao;->i:[Lcom/a/b/c/bt;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_12
    if-ge v0, v3, :cond_1e

    aget-object v4, v2, v0

    .line 4793
    iget-object v4, v4, Lcom/a/b/c/bt;->n:Lcom/a/b/c/c;

    invoke-virtual {v1, v4}, Lcom/a/b/c/b;->a(Lcom/a/b/c/c;)V

    .line 4792
    add-int/lit8 v0, v0, 0x1

    goto :goto_12

    .line 4795
    :cond_1e
    invoke-virtual {v1}, Lcom/a/b/c/b;->b()Lcom/a/b/c/ai;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 6
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 4733
    iget-object v0, p0, Lcom/a/b/c/bo;->a:Lcom/a/b/c/ao;

    .line 4925
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/a/b/c/ao;->a(Ljava/lang/Object;)I

    move-result v1

    .line 4926
    invoke-virtual {v0, v1}, Lcom/a/b/c/ao;->a(I)Lcom/a/b/c/bt;

    move-result-object v2

    invoke-virtual {v2, p1, v1}, Lcom/a/b/c/bt;->b(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v1

    .line 4927
    if-nez v1, :cond_1b

    .line 4928
    iget-object v0, v0, Lcom/a/b/c/ao;->x:Lcom/a/b/c/c;

    invoke-interface {v0, v3}, Lcom/a/b/c/c;->b(I)V

    .line 4733
    :goto_1a
    return-object v1

    .line 4930
    :cond_1b
    iget-object v0, v0, Lcom/a/b/c/ao;->x:Lcom/a/b/c/c;

    invoke-interface {v0, v3}, Lcom/a/b/c/c;->a(I)V

    goto :goto_1a
.end method

.method public final e()Ljava/util/concurrent/ConcurrentMap;
    .registers 2

    .prologue
    .line 4785
    iget-object v0, p0, Lcom/a/b/c/bo;->a:Lcom/a/b/c/ao;

    return-object v0
.end method

.method f()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 4808
    new-instance v0, Lcom/a/b/c/bq;

    iget-object v1, p0, Lcom/a/b/c/bo;->a:Lcom/a/b/c/ao;

    invoke-direct {v0, v1}, Lcom/a/b/c/bq;-><init>(Lcom/a/b/c/ao;)V

    return-object v0
.end method
