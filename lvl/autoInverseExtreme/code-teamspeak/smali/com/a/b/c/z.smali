.class final Lcom/a/b/c/z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/c/y;


# instance fields
.field private final a:Lcom/a/b/c/bw;


# direct methods
.method public constructor <init>(Lcom/a/b/c/bw;)V
    .registers 2

    .prologue
    .line 383
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 384
    iput-object p1, p0, Lcom/a/b/c/z;->a:Lcom/a/b/c/bw;

    .line 385
    return-void
.end method


# virtual methods
.method public final a(Lcom/a/b/c/l;Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .param p3    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 389
    if-nez p3, :cond_26

    move v0, v1

    :goto_5
    const-string v3, "key %s does not take values"

    new-array v4, v1, [Ljava/lang/Object;

    aput-object p2, v4, v2

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 390
    iget-object v0, p1, Lcom/a/b/c/l;->f:Lcom/a/b/c/bw;

    if-nez v0, :cond_28

    move v0, v1

    :goto_13
    const-string v3, "%s was already set to %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p2, v4, v2

    iget-object v2, p1, Lcom/a/b/c/l;->f:Lcom/a/b/c/bw;

    aput-object v2, v4, v1

    invoke-static {v0, v3, v4}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 393
    iget-object v0, p0, Lcom/a/b/c/z;->a:Lcom/a/b/c/bw;

    iput-object v0, p1, Lcom/a/b/c/l;->f:Lcom/a/b/c/bw;

    .line 394
    return-void

    :cond_26
    move v0, v2

    .line 389
    goto :goto_5

    :cond_28
    move v0, v2

    .line 390
    goto :goto_13
.end method
