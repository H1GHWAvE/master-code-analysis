.class public final Lcom/a/b/e/l;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# instance fields
.field public a:C

.field public b:C

.field public c:Ljava/lang/String;

.field private final d:Ljava/util/Map;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/a/b/e/l;->d:Ljava/util/Map;

    .line 97
    const/4 v0, 0x0

    iput-char v0, p0, Lcom/a/b/e/l;->a:C

    .line 98
    const v0, 0xffff

    iput-char v0, p0, Lcom/a/b/e/l;->b:C

    .line 99
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/b/e/l;->c:Ljava/lang/String;

    .line 102
    return-void
.end method

.method synthetic constructor <init>(B)V
    .registers 2

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/a/b/e/l;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;)Lcom/a/b/e/l;
    .registers 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 130
    iput-object p1, p0, Lcom/a/b/e/l;->c:Ljava/lang/String;

    .line 131
    return-object p0
.end method

.method private static synthetic a(Lcom/a/b/e/l;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/a/b/e/l;->c:Ljava/lang/String;

    return-object v0
.end method

.method private b()Lcom/a/b/e/l;
    .registers 2

    .prologue
    .line 115
    const/4 v0, 0x0

    iput-char v0, p0, Lcom/a/b/e/l;->a:C

    .line 116
    const v0, 0xfffd

    iput-char v0, p0, Lcom/a/b/e/l;->b:C

    .line 117
    return-object p0
.end method


# virtual methods
.method public final a()Lcom/a/b/e/g;
    .registers 5

    .prologue
    .line 156
    new-instance v0, Lcom/a/b/e/m;

    iget-object v1, p0, Lcom/a/b/e/l;->d:Ljava/util/Map;

    iget-char v2, p0, Lcom/a/b/e/l;->a:C

    iget-char v3, p0, Lcom/a/b/e/l;->b:C

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/a/b/e/m;-><init>(Lcom/a/b/e/l;Ljava/util/Map;CC)V

    return-object v0
.end method

.method public final a(CLjava/lang/String;)Lcom/a/b/e/l;
    .registers 5

    .prologue
    .line 146
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    iget-object v0, p0, Lcom/a/b/e/l;->d:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    return-object p0
.end method
