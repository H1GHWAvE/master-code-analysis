.class final Lcom/a/b/e/k;
.super Lcom/a/b/e/p;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/b/e/d;


# direct methods
.method constructor <init>(Lcom/a/b/e/d;)V
    .registers 2

    .prologue
    .line 229
    iput-object p1, p0, Lcom/a/b/e/k;->a:Lcom/a/b/e/d;

    invoke-direct {p0}, Lcom/a/b/e/p;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a(I)[C
    .registers 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 232
    const/high16 v0, 0x10000

    if-ge p1, v0, :cond_e

    .line 233
    iget-object v0, p0, Lcom/a/b/e/k;->a:Lcom/a/b/e/d;

    int-to-char v1, p1

    invoke-virtual {v0, v1}, Lcom/a/b/e/d;->a(C)[C

    move-result-object v0

    .line 271
    :goto_d
    return-object v0

    .line 239
    :cond_e
    const/4 v0, 0x2

    new-array v5, v0, [C

    .line 240
    invoke-static {p1, v5, v2}, Ljava/lang/Character;->toChars(I[CI)I

    .line 241
    iget-object v0, p0, Lcom/a/b/e/k;->a:Lcom/a/b/e/d;

    aget-char v3, v5, v2

    invoke-virtual {v0, v3}, Lcom/a/b/e/d;->a(C)[C

    move-result-object v6

    .line 242
    iget-object v0, p0, Lcom/a/b/e/k;->a:Lcom/a/b/e/d;

    aget-char v3, v5, v1

    invoke-virtual {v0, v3}, Lcom/a/b/e/d;->a(C)[C

    move-result-object v7

    .line 248
    if-nez v6, :cond_2a

    if-nez v7, :cond_2a

    .line 250
    const/4 v0, 0x0

    goto :goto_d

    .line 253
    :cond_2a
    if-eqz v6, :cond_41

    array-length v0, v6

    move v4, v0

    .line 254
    :goto_2e
    if-eqz v7, :cond_43

    array-length v0, v7

    .line 255
    :goto_31
    add-int/2addr v0, v4

    new-array v3, v0, [C

    .line 256
    if-eqz v6, :cond_45

    move v0, v2

    .line 258
    :goto_37
    array-length v8, v6

    if-ge v0, v8, :cond_49

    .line 259
    aget-char v8, v6, v0

    aput-char v8, v3, v0

    .line 258
    add-int/lit8 v0, v0, 0x1

    goto :goto_37

    :cond_41
    move v4, v1

    .line 253
    goto :goto_2e

    :cond_43
    move v0, v1

    .line 254
    goto :goto_31

    .line 262
    :cond_45
    aget-char v0, v5, v2

    aput-char v0, v3, v2

    .line 264
    :cond_49
    if-eqz v7, :cond_57

    .line 265
    :goto_4b
    array-length v0, v7

    if-ge v2, v0, :cond_5b

    .line 266
    add-int v0, v4, v2

    aget-char v1, v7, v2

    aput-char v1, v3, v0

    .line 265
    add-int/lit8 v2, v2, 0x1

    goto :goto_4b

    .line 269
    :cond_57
    aget-char v0, v5, v1

    aput-char v0, v3, v4

    :cond_5b
    move-object v0, v3

    .line 271
    goto :goto_d
.end method
