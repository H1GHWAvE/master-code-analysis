.class public final Lcom/a/b/j/c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
    b = true
.end annotation


# static fields
.field static final a:I = 0xaa
    .annotation build Lcom/a/b/a/d;
    .end annotation
.end field

.field static final b:[D
    .annotation build Lcom/a/b/a/d;
    .end annotation
.end field

.field private static final c:D = -2.147483648E9

.field private static final d:D = 2.147483647E9

.field private static final e:D = -9.223372036854776E18

.field private static final f:D = 9.223372036854776E18

.field private static final g:D


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 220
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sput-wide v0, Lcom/a/b/j/c;->g:D

    .line 313
    const/16 v0, 0xb

    new-array v0, v0, [D

    fill-array-data v0, :array_12

    sput-object v0, Lcom/a/b/j/c;->b:[D

    return-void

    :array_12
    .array-data 8
        0x3ff0000000000000L    # 1.0
        0x42b3077775800000L    # 2.0922789888E13
        0x474956ad0aae33a4L    # 2.631308369336935E35
        0x4c9ee69a78d72cb6L    # 1.2413915592536073E61
        0x526fe478ee34844aL    # 1.2688693218588417E89
        0x589c619094edabffL    # 7.156945704626381E118
        0x5f13638dd7bd6347L    # 9.916779348709496E149
        0x65c7cac197cfe503L    # 1.974506857221074E182
        0x6cb1e5dfc140e1e5L    # 3.856204823625804E215
        0x73c8ce85fadb707eL    # 5.5502938327393044E249
        0x7b095d5f3d928edeL    # 4.7147236359920616E284
    .end array-data
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 473
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(I)D
    .registers 7

    .prologue
    .line 295
    const-string v0, "n"

    invoke-static {v0, p0}, Lcom/a/b/j/k;->b(Ljava/lang/String;I)I

    .line 296
    const/16 v0, 0xaa

    if-le p0, v0, :cond_c

    .line 297
    const-wide/high16 v0, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    .line 305
    :goto_b
    return-wide v0

    .line 301
    :cond_c
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 302
    and-int/lit8 v0, p0, -0x10

    add-int/lit8 v0, v0, 0x1

    :goto_12
    if-gt v0, p0, :cond_19

    .line 303
    int-to-double v4, v0

    mul-double/2addr v2, v4

    .line 302
    add-int/lit8 v0, v0, 0x1

    goto :goto_12

    .line 305
    :cond_19
    sget-object v0, Lcom/a/b/j/c;->b:[D

    shr-int/lit8 v1, p0, 0x4

    aget-wide v0, v0, v1

    mul-double/2addr v0, v2

    goto :goto_b
.end method

.method private static a(Ljava/lang/Iterable;)D
    .registers 7
    .annotation build Lcom/a/b/a/c;
        a = "MeanAccumulator"
    .end annotation

    .prologue
    .line 452
    new-instance v1, Lcom/a/b/j/e;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lcom/a/b/j/e;-><init>(B)V

    .line 453
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    .line 454
    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/a/b/j/e;->a(D)V

    goto :goto_a

    .line 456
    :cond_1e
    invoke-virtual {v1}, Lcom/a/b/j/e;->a()D

    move-result-wide v0

    return-wide v0
.end method

.method private static a(Ljava/util/Iterator;)D
    .registers 5
    .annotation build Lcom/a/b/a/c;
        a = "MeanAccumulator"
    .end annotation

    .prologue
    .line 466
    new-instance v1, Lcom/a/b/j/e;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lcom/a/b/j/e;-><init>(B)V

    .line 467
    :goto_6
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 468
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/a/b/j/e;->a(D)V

    goto :goto_6

    .line 470
    :cond_1a
    invoke-virtual {v1}, Lcom/a/b/j/e;->a()D

    move-result-wide v0

    return-wide v0
.end method

.method private static varargs a([D)D
    .registers 7
    .annotation build Lcom/a/b/a/c;
        a = "MeanAccumulator"
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 411
    new-instance v1, Lcom/a/b/j/e;

    invoke-direct {v1, v0}, Lcom/a/b/j/e;-><init>(B)V

    .line 412
    array-length v2, p0

    :goto_7
    if-ge v0, v2, :cond_11

    aget-wide v4, p0, v0

    .line 413
    invoke-virtual {v1, v4, v5}, Lcom/a/b/j/e;->a(D)V

    .line 412
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 415
    :cond_11
    invoke-virtual {v1}, Lcom/a/b/j/e;->a()D

    move-result-wide v0

    return-wide v0
.end method

.method private static varargs a([I)D
    .registers 7
    .annotation build Lcom/a/b/a/c;
        a = "MeanAccumulator"
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 424
    new-instance v1, Lcom/a/b/j/e;

    invoke-direct {v1, v0}, Lcom/a/b/j/e;-><init>(B)V

    .line 425
    array-length v2, p0

    :goto_7
    if-ge v0, v2, :cond_12

    aget v3, p0, v0

    .line 426
    int-to-double v4, v3

    invoke-virtual {v1, v4, v5}, Lcom/a/b/j/e;->a(D)V

    .line 425
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 428
    :cond_12
    invoke-virtual {v1}, Lcom/a/b/j/e;->a()D

    move-result-wide v0

    return-wide v0
.end method

.method private static varargs a([J)D
    .registers 7
    .annotation build Lcom/a/b/a/c;
        a = "MeanAccumulator"
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 438
    new-instance v1, Lcom/a/b/j/e;

    invoke-direct {v1, v0}, Lcom/a/b/j/e;-><init>(B)V

    .line 439
    array-length v2, p0

    :goto_7
    if-ge v0, v2, :cond_12

    aget-wide v4, p0, v0

    .line 440
    long-to-double v4, v4

    invoke-virtual {v1, v4, v5}, Lcom/a/b/j/e;->a(D)V

    .line 439
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 442
    :cond_12
    invoke-virtual {v1}, Lcom/a/b/j/e;->a()D

    move-result-wide v0

    return-wide v0
.end method

.method public static a(DLjava/math/RoundingMode;)Ljava/math/BigInteger;
    .registers 11
    .annotation build Lcom/a/b/a/c;
        a = "#roundIntermediate, java.lang.Math.getExponent, com.google.common.math.DoubleUtils"
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 182
    invoke-static {p0, p1, p2}, Lcom/a/b/j/c;->b(DLjava/math/RoundingMode;)D

    move-result-wide v4

    .line 183
    const-wide/high16 v2, -0x3c20000000000000L    # -9.223372036854776E18

    sub-double/2addr v2, v4

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    cmpg-double v2, v2, v6

    if-gez v2, :cond_1f

    move v2, v0

    :goto_10
    const-wide/high16 v6, 0x43e0000000000000L    # 9.223372036854776E18

    cmpg-double v3, v4, v6

    if-gez v3, :cond_21

    :goto_16
    and-int/2addr v0, v2

    if-eqz v0, :cond_23

    .line 184
    double-to-long v0, v4

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    .line 189
    :cond_1e
    :goto_1e
    return-object v0

    :cond_1f
    move v2, v1

    .line 183
    goto :goto_10

    :cond_21
    move v0, v1

    goto :goto_16

    .line 186
    :cond_23
    invoke-static {v4, v5}, Ljava/lang/Math;->getExponent(D)I

    move-result v0

    .line 187
    invoke-static {v4, v5}, Lcom/a/b/j/f;->a(D)J

    move-result-wide v2

    .line 188
    invoke-static {v2, v3}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v1

    add-int/lit8 v0, v0, -0x34

    invoke-virtual {v1, v0}, Ljava/math/BigInteger;->shiftLeft(I)Ljava/math/BigInteger;

    move-result-object v0

    .line 189
    const-wide/16 v2, 0x0

    cmpg-double v1, v4, v2

    if-gez v1, :cond_1e

    invoke-virtual {v0}, Ljava/math/BigInteger;->negate()Ljava/math/BigInteger;

    move-result-object v0

    goto :goto_1e
.end method

.method private static a(D)Z
    .registers 4
    .annotation build Lcom/a/b/a/c;
        a = "com.google.common.math.DoubleUtils"
    .end annotation

    .prologue
    .line 198
    const-wide/16 v0, 0x0

    cmpl-double v0, p0, v0

    if-lez v0, :cond_18

    invoke-static {p0, p1}, Lcom/a/b/j/f;->b(D)Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-static {p0, p1}, Lcom/a/b/j/f;->a(D)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/a/b/j/i;->a(J)Z

    move-result v0

    if-eqz v0, :cond_18

    const/4 v0, 0x1

    :goto_17
    return v0

    :cond_18
    const/4 v0, 0x0

    goto :goto_17
.end method

.method private static a(DDD)Z
    .registers 10

    .prologue
    .line 352
    const-string v0, "tolerance"

    .line 1073
    const-wide/16 v2, 0x0

    cmpl-double v1, p4, v2

    if-gez v1, :cond_39

    .line 1074
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x28

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4, p5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") must be >= 0"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 353
    :cond_39
    sub-double v0, p0, p2

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->copySign(DD)D

    move-result-wide v0

    cmpg-double v0, v0, p4

    if-lez v0, :cond_55

    cmpl-double v0, p0, p2

    if-eqz v0, :cond_55

    invoke-static {p0, p1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-eqz v0, :cond_57

    invoke-static {p2, p3}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-eqz v0, :cond_57

    :cond_55
    const/4 v0, 0x1

    :goto_56
    return v0

    :cond_57
    const/4 v0, 0x0

    goto :goto_56
.end method

.method private static b(D)D
    .registers 6

    .prologue
    .line 217
    invoke-static {p0, p1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sget-wide v2, Lcom/a/b/j/c;->g:D

    div-double/2addr v0, v2

    return-wide v0
.end method

.method private static b(DLjava/math/RoundingMode;)D
    .registers 11
    .annotation build Lcom/a/b/a/c;
        a = "#isMathematicalInteger, com.google.common.math.DoubleUtils"
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    .line 58
    invoke-static {p0, p1}, Lcom/a/b/j/f;->b(D)Z

    move-result v0

    if-nez v0, :cond_14

    .line 59
    new-instance v0, Ljava/lang/ArithmeticException;

    const-string v1, "input is infinite or NaN"

    invoke-direct {v0, v1}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_14
    sget-object v0, Lcom/a/b/j/d;->a:[I

    invoke-virtual {p2}, Ljava/math/RoundingMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_7c

    .line 112
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 63
    :pswitch_25
    invoke-static {p0, p1}, Lcom/a/b/j/c;->c(D)Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/j/k;->a(Z)V

    .line 107
    :cond_2c
    :goto_2c
    :pswitch_2c
    return-wide p0

    .line 67
    :pswitch_2d
    cmpl-double v0, p0, v6

    if-gez v0, :cond_2c

    invoke-static {p0, p1}, Lcom/a/b/j/c;->c(D)Z

    move-result v0

    if-nez v0, :cond_2c

    .line 70
    sub-double/2addr p0, v2

    goto :goto_2c

    .line 74
    :pswitch_39
    cmpg-double v0, p0, v6

    if-lez v0, :cond_2c

    invoke-static {p0, p1}, Lcom/a/b/j/c;->c(D)Z

    move-result v0

    if-nez v0, :cond_2c

    .line 77
    add-double/2addr p0, v2

    goto :goto_2c

    .line 84
    :pswitch_45
    invoke-static {p0, p1}, Lcom/a/b/j/c;->c(D)Z

    move-result v0

    if-nez v0, :cond_2c

    .line 87
    invoke-static {v2, v3, p0, p1}, Ljava/lang/Math;->copySign(DD)D

    move-result-wide v0

    add-double/2addr p0, v0

    goto :goto_2c

    .line 91
    :pswitch_51
    invoke-static {p0, p1}, Ljava/lang/Math;->rint(D)D

    move-result-wide p0

    goto :goto_2c

    .line 94
    :pswitch_56
    invoke-static {p0, p1}, Ljava/lang/Math;->rint(D)D

    move-result-wide v0

    .line 95
    sub-double v2, p0, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    cmpl-double v2, v2, v4

    if-nez v2, :cond_6a

    .line 96
    invoke-static {v4, v5, p0, p1}, Ljava/lang/Math;->copySign(DD)D

    move-result-wide v0

    add-double/2addr p0, v0

    goto :goto_2c

    :cond_6a
    move-wide p0, v0

    .line 98
    goto :goto_2c

    .line 103
    :pswitch_6c
    invoke-static {p0, p1}, Ljava/lang/Math;->rint(D)D

    move-result-wide v0

    .line 104
    sub-double v2, p0, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_2c

    move-wide p0, v0

    .line 107
    goto :goto_2c

    .line 61
    :pswitch_data_7c
    .packed-switch 0x1
        :pswitch_25
        :pswitch_2d
        :pswitch_39
        :pswitch_2c
        :pswitch_45
        :pswitch_51
        :pswitch_56
        :pswitch_6c
    .end packed-switch
.end method

.method private static b(DDD)I
    .registers 12

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 375
    .line 1352
    const-string v2, "tolerance"

    .line 2073
    const-wide/16 v4, 0x0

    cmpl-double v3, p4, v4

    if-gez v3, :cond_3b

    .line 2074
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x28

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4, p5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") must be >= 0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1353
    :cond_3b
    sub-double v2, p0, p2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->copySign(DD)D

    move-result-wide v2

    cmpg-double v2, v2, p4

    if-lez v2, :cond_57

    cmpl-double v2, p0, p2

    if-eqz v2, :cond_57

    invoke-static {p0, p1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-eqz v2, :cond_5b

    invoke-static {p2, p3}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-eqz v2, :cond_5b

    :cond_57
    move v2, v1

    .line 375
    :goto_58
    if-eqz v2, :cond_5d

    .line 382
    :goto_5a
    return v0

    :cond_5b
    move v2, v0

    .line 1353
    goto :goto_58

    .line 377
    :cond_5d
    cmpg-double v0, p0, p2

    if-gez v0, :cond_63

    .line 378
    const/4 v0, -0x1

    goto :goto_5a

    .line 379
    :cond_63
    cmpl-double v0, p0, p2

    if-lez v0, :cond_69

    move v0, v1

    .line 380
    goto :goto_5a

    .line 382
    :cond_69
    invoke-static {p0, p1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    invoke-static {p2, p3}, Ljava/lang/Double;->isNaN(D)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/a/b/l/a;->a(ZZ)I

    move-result v0

    goto :goto_5a
.end method

.method private static c(DLjava/math/RoundingMode;)I
    .registers 11
    .annotation build Lcom/a/b/a/c;
        a = "#roundIntermediate"
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 132
    invoke-static {p0, p1, p2}, Lcom/a/b/j/c;->b(DLjava/math/RoundingMode;)D

    move-result-wide v4

    .line 133
    const-wide v2, -0x3e1fffffffe00000L    # -2.147483649E9

    cmpl-double v2, v4, v2

    if-lez v2, :cond_1c

    move v2, v0

    :goto_10
    const-wide/high16 v6, 0x41e0000000000000L    # 2.147483648E9

    cmpg-double v3, v4, v6

    if-gez v3, :cond_1e

    :goto_16
    and-int/2addr v0, v2

    invoke-static {v0}, Lcom/a/b/j/k;->b(Z)V

    .line 134
    double-to-int v0, v4

    return v0

    :cond_1c
    move v2, v1

    .line 133
    goto :goto_10

    :cond_1e
    move v0, v1

    goto :goto_16
.end method

.method private static c(D)Z
    .registers 4
    .annotation build Lcom/a/b/a/c;
        a = "java.lang.Math.getExponent, com.google.common.math.DoubleUtils"
    .end annotation

    .prologue
    .line 280
    invoke-static {p0, p1}, Lcom/a/b/j/f;->b(D)Z

    move-result v0

    if-eqz v0, :cond_1e

    const-wide/16 v0, 0x0

    cmpl-double v0, p0, v0

    if-eqz v0, :cond_1c

    invoke-static {p0, p1}, Lcom/a/b/j/f;->a(D)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->numberOfTrailingZeros(J)I

    move-result v0

    rsub-int/lit8 v0, v0, 0x34

    invoke-static {p0, p1}, Ljava/lang/Math;->getExponent(D)I

    move-result v1

    if-gt v0, v1, :cond_1e

    :cond_1c
    const/4 v0, 0x1

    :goto_1d
    return v0

    :cond_1e
    const/4 v0, 0x0

    goto :goto_1d
.end method

.method private static d(DLjava/math/RoundingMode;)J
    .registers 11
    .annotation build Lcom/a/b/a/c;
        a = "#roundIntermediate"
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 156
    invoke-static {p0, p1, p2}, Lcom/a/b/j/c;->b(DLjava/math/RoundingMode;)D

    move-result-wide v4

    .line 157
    const-wide/high16 v2, -0x3c20000000000000L    # -9.223372036854776E18

    sub-double/2addr v2, v4

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    cmpg-double v2, v2, v6

    if-gez v2, :cond_1c

    move v2, v0

    :goto_10
    const-wide/high16 v6, 0x43e0000000000000L    # 9.223372036854776E18

    cmpg-double v3, v4, v6

    if-gez v3, :cond_1e

    :goto_16
    and-int/2addr v0, v2

    invoke-static {v0}, Lcom/a/b/j/k;->b(Z)V

    .line 158
    double-to-long v0, v4

    return-wide v0

    :cond_1c
    move v2, v1

    .line 157
    goto :goto_10

    :cond_1e
    move v0, v1

    goto :goto_16
.end method

.method private static e(DLjava/math/RoundingMode;)I
    .registers 11
    .annotation build Lcom/a/b/a/c;
        a = "java.lang.Math.getExponent, com.google.common.math.DoubleUtils"
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 234
    const-wide/16 v4, 0x0

    cmpl-double v0, p0, v4

    if-lez v0, :cond_28

    invoke-static {p0, p1}, Lcom/a/b/j/f;->b(D)Z

    move-result v0

    if-eqz v0, :cond_28

    move v0, v1

    :goto_f
    const-string v3, "x must be positive and finite"

    invoke-static {v0, v3}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 235
    invoke-static {p0, p1}, Ljava/lang/Math;->getExponent(D)I

    move-result v3

    .line 236
    invoke-static {p0, p1}, Lcom/a/b/j/f;->c(D)Z

    move-result v0

    if-nez v0, :cond_2a

    .line 237
    const-wide/high16 v0, 0x4330000000000000L    # 4.503599627370496E15

    mul-double/2addr v0, p0

    invoke-static {v0, v1, p2}, Lcom/a/b/j/c;->e(DLjava/math/RoundingMode;)I

    move-result v0

    add-int/lit8 v0, v0, -0x34

    .line 269
    :goto_27
    return v0

    :cond_28
    move v0, v2

    .line 234
    goto :goto_f

    .line 242
    :cond_2a
    sget-object v0, Lcom/a/b/j/d;->a:[I

    invoke-virtual {p2}, Ljava/math/RoundingMode;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_7e

    .line 267
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 244
    :pswitch_3b
    invoke-static {p0, p1}, Lcom/a/b/j/c;->a(D)Z

    move-result v0

    invoke-static {v0}, Lcom/a/b/j/k;->a(Z)V

    :pswitch_42
    move v1, v2

    .line 269
    :cond_43
    :goto_43
    if-eqz v1, :cond_7b

    add-int/lit8 v0, v3, 0x1

    goto :goto_27

    .line 250
    :pswitch_48
    invoke-static {p0, p1}, Lcom/a/b/j/c;->a(D)Z

    move-result v0

    if-eqz v0, :cond_43

    move v1, v2

    goto :goto_43

    .line 253
    :pswitch_50
    if-gez v3, :cond_5b

    move v0, v1

    :goto_53
    invoke-static {p0, p1}, Lcom/a/b/j/c;->a(D)Z

    move-result v4

    if-nez v4, :cond_5d

    :goto_59
    and-int/2addr v1, v0

    .line 254
    goto :goto_43

    :cond_5b
    move v0, v2

    .line 253
    goto :goto_53

    :cond_5d
    move v1, v2

    goto :goto_59

    .line 256
    :pswitch_5f
    if-ltz v3, :cond_6a

    move v0, v1

    :goto_62
    invoke-static {p0, p1}, Lcom/a/b/j/c;->a(D)Z

    move-result v4

    if-nez v4, :cond_6c

    :goto_68
    and-int/2addr v1, v0

    .line 257
    goto :goto_43

    :cond_6a
    move v0, v2

    .line 256
    goto :goto_62

    :cond_6c
    move v1, v2

    goto :goto_68

    .line 261
    :pswitch_6e
    invoke-static {p0, p1}, Lcom/a/b/j/f;->d(D)D

    move-result-wide v4

    .line 264
    mul-double/2addr v4, v4

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    cmpl-double v0, v4, v6

    if-gtz v0, :cond_43

    move v1, v2

    goto :goto_43

    :cond_7b
    move v0, v3

    .line 269
    goto :goto_27

    .line 242
    nop

    :pswitch_data_7e
    .packed-switch 0x1
        :pswitch_3b
        :pswitch_42
        :pswitch_48
        :pswitch_50
        :pswitch_5f
        :pswitch_6e
        :pswitch_6e
        :pswitch_6e
    .end packed-switch
.end method
