.class public final Lcom/a/b/n/a/bs;
.super Lcom/a/b/n/a/bd;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# instance fields
.field private final b:Ljava/util/Map;


# direct methods
.method constructor <init>(Lcom/a/b/n/a/bq;Ljava/util/Map;)V
    .registers 4
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 428
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/a/b/n/a/bd;-><init>(Lcom/a/b/n/a/bq;B)V

    .line 429
    iput-object p2, p0, Lcom/a/b/n/a/bs;->b:Ljava/util/Map;

    .line 430
    return-void
.end method

.method private a(Ljava/lang/Enum;)Ljava/util/concurrent/locks/ReentrantLock;
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 436
    .line 1449
    iget-object v0, p0, Lcom/a/b/n/a/bs;->a:Lcom/a/b/n/a/bq;

    sget-object v1, Lcom/a/b/n/a/bm;->c:Lcom/a/b/n/a/bm;

    if-ne v0, v1, :cond_d

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v2}, Ljava/util/concurrent/locks/ReentrantLock;-><init>(Z)V

    :goto_c
    return-object v0

    :cond_d
    new-instance v1, Lcom/a/b/n/a/bg;

    iget-object v0, p0, Lcom/a/b/n/a/bs;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/bl;

    invoke-direct {v1, p0, v0, v2}, Lcom/a/b/n/a/bg;-><init>(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bl;B)V

    move-object v0, v1

    .line 436
    goto :goto_c
.end method

.method private b(Ljava/lang/Enum;)Ljava/util/concurrent/locks/ReentrantLock;
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 449
    iget-object v0, p0, Lcom/a/b/n/a/bs;->a:Lcom/a/b/n/a/bq;

    sget-object v1, Lcom/a/b/n/a/bm;->c:Lcom/a/b/n/a/bm;

    if-ne v0, v1, :cond_d

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v2}, Ljava/util/concurrent/locks/ReentrantLock;-><init>(Z)V

    :goto_c
    return-object v0

    :cond_d
    new-instance v1, Lcom/a/b/n/a/bg;

    iget-object v0, p0, Lcom/a/b/n/a/bs;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/bl;

    invoke-direct {v1, p0, v0, v2}, Lcom/a/b/n/a/bg;-><init>(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bl;B)V

    move-object v0, v1

    goto :goto_c
.end method

.method private c(Ljava/lang/Enum;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 457
    .line 1471
    iget-object v0, p0, Lcom/a/b/n/a/bs;->a:Lcom/a/b/n/a/bq;

    sget-object v1, Lcom/a/b/n/a/bm;->c:Lcom/a/b/n/a/bm;

    if-ne v0, v1, :cond_d

    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-direct {v0, v2}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>(Z)V

    :goto_c
    return-object v0

    :cond_d
    new-instance v1, Lcom/a/b/n/a/bi;

    iget-object v0, p0, Lcom/a/b/n/a/bs;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/bl;

    invoke-direct {v1, p0, v0, v2}, Lcom/a/b/n/a/bi;-><init>(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bl;B)V

    move-object v0, v1

    .line 457
    goto :goto_c
.end method

.method private d(Ljava/lang/Enum;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 471
    iget-object v0, p0, Lcom/a/b/n/a/bs;->a:Lcom/a/b/n/a/bq;

    sget-object v1, Lcom/a/b/n/a/bm;->c:Lcom/a/b/n/a/bm;

    if-ne v0, v1, :cond_d

    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-direct {v0, v2}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>(Z)V

    :goto_c
    return-object v0

    :cond_d
    new-instance v1, Lcom/a/b/n/a/bi;

    iget-object v0, p0, Lcom/a/b/n/a/bs;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/bl;

    invoke-direct {v1, p0, v0, v2}, Lcom/a/b/n/a/bi;-><init>(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bl;B)V

    move-object v0, v1

    goto :goto_c
.end method
