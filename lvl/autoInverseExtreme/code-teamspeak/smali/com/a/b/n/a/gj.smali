.class final Lcom/a/b/n/a/gj;
.super Lcom/a/b/n/a/gi;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/d;
.end annotation


# instance fields
.field final a:Ljava/util/concurrent/atomic/AtomicReferenceArray;

.field final b:Lcom/a/b/b/dz;

.field final c:I

.field final e:Ljava/lang/ref/ReferenceQueue;


# direct methods
.method constructor <init>(ILcom/a/b/b/dz;)V
    .registers 5

    .prologue
    .line 348
    invoke-direct {p0, p1}, Lcom/a/b/n/a/gi;-><init>(I)V

    .line 345
    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    iput-object v0, p0, Lcom/a/b/n/a/gj;->e:Ljava/lang/ref/ReferenceQueue;

    .line 349
    iget v0, p0, Lcom/a/b/n/a/gj;->d:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_20

    const v0, 0x7fffffff

    :goto_12
    iput v0, p0, Lcom/a/b/n/a/gj;->c:I

    .line 350
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iget v1, p0, Lcom/a/b/n/a/gj;->c:I

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;-><init>(I)V

    iput-object v0, p0, Lcom/a/b/n/a/gj;->a:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 351
    iput-object p2, p0, Lcom/a/b/n/a/gj;->b:Lcom/a/b/b/dz;

    .line 352
    return-void

    .line 349
    :cond_20
    iget v0, p0, Lcom/a/b/n/a/gj;->d:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_12
.end method

.method private b()V
    .registers 5

    .prologue
    .line 382
    :goto_0
    iget-object v0, p0, Lcom/a/b/n/a/gj;->e:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 384
    check-cast v0, Lcom/a/b/n/a/gk;

    .line 387
    iget-object v1, p0, Lcom/a/b/n/a/gj;->a:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iget v2, v0, Lcom/a/b/n/a/gk;->a:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->compareAndSet(ILjava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_0

    .line 389
    :cond_13
    return-void
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 392
    iget v0, p0, Lcom/a/b/n/a/gj;->c:I

    return v0
.end method

.method public final a(I)Ljava/lang/Object;
    .registers 7

    .prologue
    const/4 v3, 0x0

    .line 355
    iget v0, p0, Lcom/a/b/n/a/gj;->c:I

    const v1, 0x7fffffff

    if-eq v0, v1, :cond_d

    .line 1392
    iget v0, p0, Lcom/a/b/n/a/gj;->c:I

    .line 356
    invoke-static {p1, v0}, Lcom/a/b/b/cn;->a(II)I

    .line 358
    :cond_d
    iget-object v0, p0, Lcom/a/b/n/a/gj;->a:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/gk;

    .line 359
    if-nez v0, :cond_1c

    move-object v1, v3

    .line 360
    :goto_18
    if-eqz v1, :cond_21

    move-object v0, v1

    .line 374
    :goto_1b
    return-object v0

    .line 359
    :cond_1c
    invoke-virtual {v0}, Lcom/a/b/n/a/gk;->get()Ljava/lang/Object;

    move-result-object v1

    goto :goto_18

    .line 363
    :cond_21
    iget-object v1, p0, Lcom/a/b/n/a/gj;->b:Lcom/a/b/b/dz;

    invoke-interface {v1}, Lcom/a/b/b/dz;->a()Ljava/lang/Object;

    move-result-object v2

    .line 364
    new-instance v4, Lcom/a/b/n/a/gk;

    iget-object v1, p0, Lcom/a/b/n/a/gj;->e:Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v4, v2, p1, v1}, Lcom/a/b/n/a/gk;-><init>(Ljava/lang/Object;ILjava/lang/ref/ReferenceQueue;)V

    .line 365
    :cond_2e
    iget-object v1, p0, Lcom/a/b/n/a/gj;->a:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v1, p1, v0, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->compareAndSet(ILjava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4a

    .line 367
    iget-object v0, p0, Lcom/a/b/n/a/gj;->a:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/gk;

    .line 368
    if-nez v0, :cond_45

    move-object v1, v3

    .line 369
    :goto_41
    if-eqz v1, :cond_2e

    move-object v0, v1

    .line 370
    goto :goto_1b

    .line 368
    :cond_45
    invoke-virtual {v0}, Lcom/a/b/n/a/gk;->get()Ljava/lang/Object;

    move-result-object v1

    goto :goto_41

    .line 2382
    :cond_4a
    :goto_4a
    iget-object v0, p0, Lcom/a/b/n/a/gj;->e:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-eqz v0, :cond_5c

    .line 2384
    check-cast v0, Lcom/a/b/n/a/gk;

    .line 2387
    iget-object v1, p0, Lcom/a/b/n/a/gj;->a:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iget v4, v0, Lcom/a/b/n/a/gk;->a:I

    invoke-virtual {v1, v4, v0, v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->compareAndSet(ILjava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_4a

    :cond_5c
    move-object v0, v2

    .line 374
    goto :goto_1b
.end method
