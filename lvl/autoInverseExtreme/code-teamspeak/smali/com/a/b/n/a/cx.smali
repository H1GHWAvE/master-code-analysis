.class final Lcom/a/b/n/a/cx;
.super Lcom/a/b/n/a/dq;
.source "SourceFile"


# instance fields
.field a:Lcom/a/b/d/jl;


# direct methods
.method private constructor <init>(Ljava/util/concurrent/Callable;Lcom/a/b/d/jl;)V
    .registers 3

    .prologue
    .line 1056
    invoke-direct {p0, p1}, Lcom/a/b/n/a/dq;-><init>(Ljava/util/concurrent/Callable;)V

    .line 1057
    iput-object p2, p0, Lcom/a/b/n/a/cx;->a:Lcom/a/b/d/jl;

    .line 1058
    return-void
.end method


# virtual methods
.method public final cancel(Z)Z
    .registers 4

    .prologue
    .line 1061
    iget-object v0, p0, Lcom/a/b/n/a/cx;->a:Lcom/a/b/d/jl;

    .line 1062
    invoke-super {p0, p1}, Lcom/a/b/n/a/dq;->cancel(Z)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 1063
    invoke-virtual {v0}, Lcom/a/b/d/jl;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/dp;

    .line 1064
    invoke-interface {v0, p1}, Lcom/a/b/n/a/dp;->cancel(Z)Z

    goto :goto_c

    .line 1066
    :cond_1c
    const/4 v0, 0x1

    .line 1068
    :goto_1d
    return v0

    :cond_1e
    const/4 v0, 0x0

    goto :goto_1d
.end method

.method protected final done()V
    .registers 2

    .prologue
    .line 1072
    invoke-super {p0}, Lcom/a/b/n/a/dq;->done()V

    .line 1073
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/b/n/a/cx;->a:Lcom/a/b/d/jl;

    .line 1074
    return-void
.end method

.method protected final setException(Ljava/lang/Throwable;)V
    .registers 2

    .prologue
    .line 1077
    invoke-super {p0, p1}, Lcom/a/b/n/a/dq;->setException(Ljava/lang/Throwable;)V

    .line 1078
    return-void
.end method
