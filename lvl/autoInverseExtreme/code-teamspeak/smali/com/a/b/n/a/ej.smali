.class final Lcom/a/b/n/a/ej;
.super Lcom/a/b/n/a/ce;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/n/a/dr;


# instance fields
.field private final a:Ljava/util/concurrent/ScheduledFuture;


# direct methods
.method public constructor <init>(Lcom/a/b/n/a/dp;Ljava/util/concurrent/ScheduledFuture;)V
    .registers 3

    .prologue
    .line 610
    invoke-direct {p0, p1}, Lcom/a/b/n/a/ce;-><init>(Lcom/a/b/n/a/dp;)V

    .line 611
    iput-object p2, p0, Lcom/a/b/n/a/ej;->a:Ljava/util/concurrent/ScheduledFuture;

    .line 612
    return-void
.end method

.method private a(Ljava/util/concurrent/Delayed;)I
    .registers 3

    .prologue
    .line 633
    iget-object v0, p0, Lcom/a/b/n/a/ej;->a:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ScheduledFuture;->compareTo(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final cancel(Z)Z
    .registers 4

    .prologue
    .line 616
    invoke-super {p0, p1}, Lcom/a/b/n/a/ce;->cancel(Z)Z

    move-result v0

    .line 617
    if-eqz v0, :cond_b

    .line 619
    iget-object v1, p0, Lcom/a/b/n/a/ej;->a:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v1, p1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 623
    :cond_b
    return v0
.end method

.method public final bridge synthetic compareTo(Ljava/lang/Object;)I
    .registers 3

    .prologue
    .line 601
    check-cast p1, Ljava/util/concurrent/Delayed;

    .line 1633
    iget-object v0, p0, Lcom/a/b/n/a/ej;->a:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ScheduledFuture;->compareTo(Ljava/lang/Object;)I

    move-result v0

    .line 601
    return v0
.end method

.method public final getDelay(Ljava/util/concurrent/TimeUnit;)J
    .registers 4

    .prologue
    .line 628
    iget-object v0, p0, Lcom/a/b/n/a/ej;->a:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ScheduledFuture;->getDelay(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    return-wide v0
.end method
