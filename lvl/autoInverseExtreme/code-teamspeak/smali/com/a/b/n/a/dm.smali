.class public final Lcom/a/b/n/a/dm;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/util/concurrent/Future;)Lcom/a/b/n/a/dp;
    .registers 2

    .prologue
    .line 60
    instance-of v0, p0, Lcom/a/b/n/a/dp;

    if-eqz v0, :cond_7

    .line 61
    check-cast p0, Lcom/a/b/n/a/dp;

    .line 63
    :goto_6
    return-object p0

    :cond_7
    new-instance v0, Lcom/a/b/n/a/dn;

    invoke-direct {v0, p0}, Lcom/a/b/n/a/dn;-><init>(Ljava/util/concurrent/Future;)V

    move-object p0, v0

    goto :goto_6
.end method

.method private static a(Ljava/util/concurrent/Future;Ljava/util/concurrent/Executor;)Lcom/a/b/n/a/dp;
    .registers 3

    .prologue
    .line 92
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    instance-of v0, p0, Lcom/a/b/n/a/dp;

    if-eqz v0, :cond_a

    .line 94
    check-cast p0, Lcom/a/b/n/a/dp;

    .line 96
    :goto_9
    return-object p0

    :cond_a
    new-instance v0, Lcom/a/b/n/a/dn;

    invoke-direct {v0, p0, p1}, Lcom/a/b/n/a/dn;-><init>(Ljava/util/concurrent/Future;Ljava/util/concurrent/Executor;)V

    move-object p0, v0

    goto :goto_9
.end method
