.class Lcom/a/b/n/a/bl;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/Map;

.field final b:Ljava/util/Map;

.field final c:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .registers 4

    .prologue
    .line 637
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 625
    new-instance v0, Lcom/a/b/d/ql;

    invoke-direct {v0}, Lcom/a/b/d/ql;-><init>()V

    .line 1265
    sget-object v1, Lcom/a/b/d/sh;->c:Lcom/a/b/d/sh;

    invoke-virtual {v0, v1}, Lcom/a/b/d/ql;->a(Lcom/a/b/d/sh;)Lcom/a/b/d/ql;

    move-result-object v0

    .line 625
    invoke-virtual {v0}, Lcom/a/b/d/ql;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/n/a/bl;->a:Ljava/util/Map;

    .line 632
    new-instance v0, Lcom/a/b/d/ql;

    invoke-direct {v0}, Lcom/a/b/d/ql;-><init>()V

    .line 2265
    sget-object v1, Lcom/a/b/d/sh;->c:Lcom/a/b/d/sh;

    invoke-virtual {v0, v1}, Lcom/a/b/d/ql;->a(Lcom/a/b/d/sh;)Lcom/a/b/d/ql;

    move-result-object v0

    .line 632
    invoke-virtual {v0}, Lcom/a/b/d/ql;->e()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, Lcom/a/b/n/a/bl;->b:Ljava/util/Map;

    .line 638
    invoke-static {p1}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/a/b/n/a/bl;->c:Ljava/lang/String;

    .line 639
    return-void
.end method

.method private a(Lcom/a/b/n/a/bl;Ljava/util/Set;)Lcom/a/b/n/a/bk;
    .registers 8
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 729
    invoke-interface {p2, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    move-object v0, v2

    .line 752
    :cond_8
    :goto_8
    return-object v0

    .line 732
    :cond_9
    iget-object v0, p0, Lcom/a/b/n/a/bl;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/bk;

    .line 733
    if-nez v0, :cond_8

    .line 738
    iget-object v0, p0, Lcom/a/b/n/a/bl;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 739
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/b/n/a/bl;

    .line 740
    invoke-direct {v1, p1, p2}, Lcom/a/b/n/a/bl;->a(Lcom/a/b/n/a/bl;Ljava/util/Set;)Lcom/a/b/n/a/bk;

    move-result-object v4

    .line 741
    if-eqz v4, :cond_1d

    .line 745
    new-instance v2, Lcom/a/b/n/a/bk;

    invoke-direct {v2, v1, p0}, Lcom/a/b/n/a/bk;-><init>(Lcom/a/b/n/a/bl;Lcom/a/b/n/a/bl;)V

    .line 747
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/bk;

    invoke-virtual {v0}, Lcom/a/b/n/a/bk;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/a/b/n/a/bk;->setStackTrace([Ljava/lang/StackTraceElement;)V

    .line 748
    invoke-virtual {v2, v4}, Lcom/a/b/n/a/bk;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-object v0, v2

    .line 749
    goto :goto_8

    :cond_4c
    move-object v0, v2

    .line 752
    goto :goto_8
.end method

.method private a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 642
    iget-object v0, p0, Lcom/a/b/n/a/bl;->c:Ljava/lang/String;

    return-object v0
.end method

.method private a(Lcom/a/b/n/a/bq;Lcom/a/b/n/a/bl;)V
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 668
    if-eq p0, p2, :cond_22

    const/4 v0, 0x1

    :goto_4
    const-string v3, "Attempted to acquire multiple locks with the same rank "

    .line 6642
    iget-object v2, p2, Lcom/a/b/n/a/bl;->c:Ljava/lang/String;

    .line 668
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_24

    invoke-virtual {v3, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_16
    invoke-static {v0, v2}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 673
    iget-object v0, p0, Lcom/a/b/n/a/bl;->a:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 715
    :goto_21
    return-void

    :cond_22
    move v0, v1

    .line 668
    goto :goto_4

    :cond_24
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_16

    .line 679
    :cond_2a
    iget-object v0, p0, Lcom/a/b/n/a/bl;->b:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/br;

    .line 681
    if-eqz v0, :cond_3f

    .line 685
    new-instance v2, Lcom/a/b/n/a/br;

    .line 7583
    iget-object v0, v0, Lcom/a/b/n/a/br;->c:Lcom/a/b/n/a/bk;

    .line 685
    invoke-direct {v2, p2, p0, v0, v1}, Lcom/a/b/n/a/br;-><init>(Lcom/a/b/n/a/bl;Lcom/a/b/n/a/bl;Lcom/a/b/n/a/bk;B)V

    .line 688
    invoke-interface {p1, v2}, Lcom/a/b/n/a/bq;->a(Lcom/a/b/n/a/br;)V

    goto :goto_21

    .line 8395
    :cond_3f
    invoke-static {}, Lcom/a/b/d/sz;->f()Ljava/util/IdentityHashMap;

    move-result-object v0

    .line 9058
    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    .line 694
    invoke-direct {p2, p0, v0}, Lcom/a/b/n/a/bl;->a(Lcom/a/b/n/a/bl;Ljava/util/Set;)Lcom/a/b/n/a/bk;

    move-result-object v0

    .line 696
    if-nez v0, :cond_58

    .line 705
    iget-object v0, p0, Lcom/a/b/n/a/bl;->a:Ljava/util/Map;

    new-instance v1, Lcom/a/b/n/a/bk;

    invoke-direct {v1, p2, p0}, Lcom/a/b/n/a/bk;-><init>(Lcom/a/b/n/a/bl;Lcom/a/b/n/a/bl;)V

    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_21

    .line 710
    :cond_58
    new-instance v2, Lcom/a/b/n/a/br;

    invoke-direct {v2, p2, p0, v0, v1}, Lcom/a/b/n/a/br;-><init>(Lcom/a/b/n/a/bl;Lcom/a/b/n/a/bl;Lcom/a/b/n/a/bk;B)V

    .line 712
    iget-object v0, p0, Lcom/a/b/n/a/bl;->b:Ljava/util/Map;

    invoke-interface {v0, p2, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 713
    invoke-interface {p1, v2}, Lcom/a/b/n/a/bq;->a(Lcom/a/b/n/a/br;)V

    goto :goto_21
.end method


# virtual methods
.method final a(Lcom/a/b/n/a/bq;Ljava/util/List;)V
    .registers 11

    .prologue
    const/4 v2, 0x0

    .line 647
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v5

    move v4, v2

    :goto_6
    if-ge v4, v5, :cond_75

    .line 648
    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/n/a/bl;

    .line 2668
    if-eq p0, v0, :cond_46

    const/4 v1, 0x1

    :goto_11
    const-string v6, "Attempted to acquire multiple locks with the same rank "

    .line 3642
    iget-object v3, v0, Lcom/a/b/n/a/bl;->c:Ljava/lang/String;

    .line 2668
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_48

    invoke-virtual {v6, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_23
    invoke-static {v1, v3}, Lcom/a/b/b/cn;->b(ZLjava/lang/Object;)V

    .line 2673
    iget-object v1, p0, Lcom/a/b/n/a/bl;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_42

    .line 2679
    iget-object v1, p0, Lcom/a/b/n/a/bl;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/a/b/n/a/br;

    .line 2681
    if-eqz v1, :cond_4e

    .line 2685
    new-instance v3, Lcom/a/b/n/a/br;

    .line 4583
    iget-object v1, v1, Lcom/a/b/n/a/br;->c:Lcom/a/b/n/a/bk;

    .line 2685
    invoke-direct {v3, v0, p0, v1, v2}, Lcom/a/b/n/a/br;-><init>(Lcom/a/b/n/a/bl;Lcom/a/b/n/a/bl;Lcom/a/b/n/a/bk;B)V

    .line 2688
    invoke-interface {p1, v3}, Lcom/a/b/n/a/bq;->a(Lcom/a/b/n/a/br;)V

    .line 647
    :cond_42
    :goto_42
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_6

    :cond_46
    move v1, v2

    .line 2668
    goto :goto_11

    :cond_48
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_23

    .line 5395
    :cond_4e
    invoke-static {}, Lcom/a/b/d/sz;->f()Ljava/util/IdentityHashMap;

    move-result-object v1

    .line 6058
    invoke-static {v1}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v1

    .line 2694
    invoke-direct {v0, p0, v1}, Lcom/a/b/n/a/bl;->a(Lcom/a/b/n/a/bl;Ljava/util/Set;)Lcom/a/b/n/a/bk;

    move-result-object v1

    .line 2696
    if-nez v1, :cond_67

    .line 2705
    iget-object v1, p0, Lcom/a/b/n/a/bl;->a:Ljava/util/Map;

    new-instance v3, Lcom/a/b/n/a/bk;

    invoke-direct {v3, v0, p0}, Lcom/a/b/n/a/bk;-><init>(Lcom/a/b/n/a/bl;Lcom/a/b/n/a/bl;)V

    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_42

    .line 2710
    :cond_67
    new-instance v3, Lcom/a/b/n/a/br;

    invoke-direct {v3, v0, p0, v1, v2}, Lcom/a/b/n/a/br;-><init>(Lcom/a/b/n/a/bl;Lcom/a/b/n/a/bl;Lcom/a/b/n/a/bk;B)V

    .line 2712
    iget-object v1, p0, Lcom/a/b/n/a/bl;->b:Ljava/util/Map;

    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2713
    invoke-interface {p1, v3}, Lcom/a/b/n/a/bq;->a(Lcom/a/b/n/a/br;)V

    goto :goto_42

    .line 650
    :cond_75
    return-void
.end method
