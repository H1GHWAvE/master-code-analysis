.class public final Lcom/a/b/n/a/dw;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# instance fields
.field final a:Ljava/util/concurrent/locks/ReentrantLock;

.field private final b:Z

.field private c:Lcom/a/b/n/a/dx;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "lock"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 339
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/a/b/n/a/dw;-><init>(B)V

    .line 340
    return-void
.end method

.method private constructor <init>(B)V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 348
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 331
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/b/n/a/dw;->c:Lcom/a/b/n/a/dx;

    .line 349
    iput-boolean v1, p0, Lcom/a/b/n/a/dw;->b:Z

    .line 350
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>(Z)V

    iput-object v0, p0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    .line 351
    return-void
.end method

.method private static synthetic a(Lcom/a/b/n/a/dw;)Ljava/util/concurrent/locks/ReentrantLock;
    .registers 2

    .prologue
    .line 202
    iget-object v0, p0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    return-object v0
.end method

.method private a(Lcom/a/b/n/a/dx;Z)V
    .registers 4
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "lock"
    .end annotation

    .prologue
    .line 985
    if-eqz p2, :cond_5

    .line 986
    invoke-direct {p0}, Lcom/a/b/n/a/dw;->k()V

    .line 988
    :cond_5
    invoke-direct {p0, p1}, Lcom/a/b/n/a/dw;->k(Lcom/a/b/n/a/dx;)V

    .line 991
    :cond_8
    :try_start_8
    iget-object v0, p1, Lcom/a/b/n/a/dx;->c:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->await()V

    .line 992
    invoke-virtual {p1}, Lcom/a/b/n/a/dx;->a()Z
    :try_end_10
    .catchall {:try_start_8 .. :try_end_10} :catchall_17

    move-result v0

    if-eqz v0, :cond_8

    .line 994
    invoke-direct {p0, p1}, Lcom/a/b/n/a/dw;->l(Lcom/a/b/n/a/dx;)V

    .line 995
    return-void

    .line 994
    :catchall_17
    move-exception v0

    invoke-direct {p0, p1}, Lcom/a/b/n/a/dw;->l(Lcom/a/b/n/a/dx;)V

    throw v0
.end method

.method private a(JLjava/util/concurrent/TimeUnit;)Z
    .registers 13

    .prologue
    const/4 v0, 0x1

    .line 373
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v2

    .line 374
    iget-object v4, p0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    .line 375
    iget-boolean v1, p0, Lcom/a/b/n/a/dw;->b:Z

    if-nez v1, :cond_12

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock()Z

    move-result v1

    if-eqz v1, :cond_12

    .line 391
    :cond_11
    :goto_11
    return v0

    .line 378
    :cond_12
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    add-long/2addr v6, v2

    .line 379
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v1

    .line 383
    :goto_1b
    :try_start_1b
    sget-object v5, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v2, v3, v5}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_20
    .catch Ljava/lang/InterruptedException; {:try_start_1b .. :try_end_20} :catch_2b
    .catchall {:try_start_1b .. :try_end_20} :catchall_34

    move-result v0

    .line 390
    if-eqz v1, :cond_11

    .line 391
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_11

    .line 385
    :catch_2b
    move-exception v1

    .line 386
    :try_start_2c
    invoke-static {}, Ljava/lang/System;->nanoTime()J
    :try_end_2f
    .catchall {:try_start_2c .. :try_end_2f} :catchall_3f

    move-result-wide v2

    sub-long v2, v6, v2

    move v1, v0

    .line 387
    goto :goto_1b

    .line 390
    :catchall_34
    move-exception v0

    :goto_35
    if-eqz v1, :cond_3e

    .line 391
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    :cond_3e
    throw v0

    .line 390
    :catchall_3f
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto :goto_35
.end method

.method private a(Lcom/a/b/n/a/dx;JZ)Z
    .registers 7
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "lock"
    .end annotation

    .prologue
    .line 1016
    if-eqz p4, :cond_5

    .line 1017
    invoke-direct {p0}, Lcom/a/b/n/a/dw;->k()V

    .line 1019
    :cond_5
    invoke-direct {p0, p1}, Lcom/a/b/n/a/dw;->k(Lcom/a/b/n/a/dx;)V

    .line 1022
    :cond_8
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-gez v0, :cond_13

    .line 1029
    invoke-direct {p0, p1}, Lcom/a/b/n/a/dw;->l(Lcom/a/b/n/a/dx;)V

    const/4 v0, 0x0

    :goto_12
    return v0

    .line 1025
    :cond_13
    :try_start_13
    iget-object v0, p1, Lcom/a/b/n/a/dx;->c:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0, p2, p3}, Ljava/util/concurrent/locks/Condition;->awaitNanos(J)J

    move-result-wide p2

    .line 1026
    invoke-virtual {p1}, Lcom/a/b/n/a/dx;->a()Z
    :try_end_1c
    .catchall {:try_start_13 .. :try_end_1c} :catchall_24

    move-result v0

    if-eqz v0, :cond_8

    .line 1029
    invoke-direct {p0, p1}, Lcom/a/b/n/a/dw;->l(Lcom/a/b/n/a/dx;)V

    const/4 v0, 0x1

    goto :goto_12

    :catchall_24
    move-exception v0

    invoke-direct {p0, p1}, Lcom/a/b/n/a/dw;->l(Lcom/a/b/n/a/dx;)V

    throw v0
.end method

.method private a(Ljava/lang/Thread;)Z
    .registers 3

    .prologue
    .line 834
    iget-object v0, p0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/locks/ReentrantLock;->hasQueuedThread(Ljava/lang/Thread;)Z

    move-result v0

    return v0
.end method

.method private b()V
    .registers 2

    .prologue
    .line 357
    iget-object v0, p0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 358
    return-void
.end method

.method private b(Lcom/a/b/n/a/dx;Z)V
    .registers 4
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "lock"
    .end annotation

    .prologue
    .line 1000
    if-eqz p2, :cond_5

    .line 1001
    invoke-direct {p0}, Lcom/a/b/n/a/dw;->k()V

    .line 1003
    :cond_5
    invoke-direct {p0, p1}, Lcom/a/b/n/a/dw;->k(Lcom/a/b/n/a/dx;)V

    .line 1006
    :cond_8
    :try_start_8
    iget-object v0, p1, Lcom/a/b/n/a/dx;->c:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->awaitUninterruptibly()V

    .line 1007
    invoke-virtual {p1}, Lcom/a/b/n/a/dx;->a()Z
    :try_end_10
    .catchall {:try_start_8 .. :try_end_10} :catchall_17

    move-result v0

    if-eqz v0, :cond_8

    .line 1009
    invoke-direct {p0, p1}, Lcom/a/b/n/a/dw;->l(Lcom/a/b/n/a/dx;)V

    .line 1010
    return-void

    .line 1009
    :catchall_17
    move-exception v0

    invoke-direct {p0, p1}, Lcom/a/b/n/a/dw;->l(Lcom/a/b/n/a/dx;)V

    throw v0
.end method

.method private b(JLjava/util/concurrent/TimeUnit;)Z
    .registers 5

    .prologue
    .line 402
    iget-object v0, p0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0, p1, p2, p3}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    return v0
.end method

.method private c()V
    .registers 2

    .prologue
    .line 364
    iget-object v0, p0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lockInterruptibly()V

    .line 365
    return-void
.end method

.method private c(Lcom/a/b/n/a/dx;)V
    .registers 4

    .prologue
    .line 420
    iget-object v0, p1, Lcom/a/b/n/a/dx;->b:Lcom/a/b/n/a/dw;

    if-eq v0, p0, :cond_a

    .line 421
    new-instance v0, Ljava/lang/IllegalMonitorStateException;

    invoke-direct {v0}, Ljava/lang/IllegalMonitorStateException;-><init>()V

    throw v0

    .line 423
    :cond_a
    iget-object v0, p0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    .line 424
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->isHeldByCurrentThread()Z

    move-result v1

    .line 425
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lockInterruptibly()V

    .line 429
    :try_start_13
    invoke-virtual {p1}, Lcom/a/b/n/a/dx;->a()Z

    move-result v0

    if-nez v0, :cond_1c

    .line 430
    invoke-direct {p0, p1, v1}, Lcom/a/b/n/a/dw;->a(Lcom/a/b/n/a/dx;Z)V
    :try_end_1c
    .catchall {:try_start_13 .. :try_end_1c} :catchall_1d

    .line 434
    :cond_1c
    return-void

    :catchall_1d
    move-exception v0

    .line 435
    invoke-virtual {p0}, Lcom/a/b/n/a/dw;->a()V

    throw v0
.end method

.method private c(Lcom/a/b/n/a/dx;JLjava/util/concurrent/TimeUnit;)Z
    .registers 13

    .prologue
    const/4 v2, 0x0

    .line 472
    invoke-virtual {p4, p2, p3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    .line 473
    iget-object v3, p1, Lcom/a/b/n/a/dx;->b:Lcom/a/b/n/a/dw;

    if-eq v3, p0, :cond_f

    .line 474
    new-instance v0, Ljava/lang/IllegalMonitorStateException;

    invoke-direct {v0}, Ljava/lang/IllegalMonitorStateException;-><init>()V

    throw v0

    .line 476
    :cond_f
    iget-object v3, p0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    .line 477
    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->isHeldByCurrentThread()Z

    move-result v4

    .line 478
    iget-boolean v5, p0, Lcom/a/b/n/a/dw;->b:Z

    if-nez v5, :cond_1f

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock()Z

    move-result v5

    if-nez v5, :cond_30

    .line 479
    :cond_1f
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    add-long/2addr v0, v6

    .line 480
    invoke-virtual {v3, p2, p3, p4}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock(JLjava/util/concurrent/TimeUnit;)Z

    move-result v5

    if-nez v5, :cond_2b

    .line 500
    :goto_2a
    return v2

    .line 483
    :cond_2b
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    sub-long/2addr v0, v6

    .line 489
    :cond_30
    :try_start_30
    invoke-virtual {p1}, Lcom/a/b/n/a/dx;->a()Z

    move-result v5

    if-nez v5, :cond_3c

    invoke-direct {p0, p1, v0, v1, v4}, Lcom/a/b/n/a/dw;->a(Lcom/a/b/n/a/dx;JZ)Z
    :try_end_39
    .catchall {:try_start_30 .. :try_end_39} :catchall_46

    move-result v0

    if-eqz v0, :cond_44

    :cond_3c
    const/4 v0, 0x1

    .line 493
    :goto_3d
    if-nez v0, :cond_42

    .line 500
    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    :cond_42
    move v2, v0

    goto :goto_2a

    :cond_44
    move v0, v2

    .line 489
    goto :goto_3d

    .line 493
    :catchall_46
    move-exception v0

    .line 496
    if-nez v4, :cond_4c

    .line 497
    :try_start_49
    invoke-direct {p0}, Lcom/a/b/n/a/dw;->k()V
    :try_end_4c
    .catchall {:try_start_49 .. :try_end_4c} :catchall_50

    .line 500
    :cond_4c
    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    :catchall_50
    move-exception v0

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private d()Z
    .registers 2

    .prologue
    .line 413
    iget-object v0, p0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock()Z

    move-result v0

    return v0
.end method

.method private d(Lcom/a/b/n/a/dx;)Z
    .registers 4

    .prologue
    .line 591
    iget-object v0, p1, Lcom/a/b/n/a/dx;->b:Lcom/a/b/n/a/dw;

    if-eq v0, p0, :cond_a

    .line 592
    new-instance v0, Ljava/lang/IllegalMonitorStateException;

    invoke-direct {v0}, Ljava/lang/IllegalMonitorStateException;-><init>()V

    throw v0

    .line 594
    :cond_a
    iget-object v0, p0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    .line 595
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lockInterruptibly()V

    .line 599
    :try_start_f
    invoke-virtual {p1}, Lcom/a/b/n/a/dx;->a()Z
    :try_end_12
    .catchall {:try_start_f .. :try_end_12} :catchall_19

    move-result v1

    .line 601
    if-nez v1, :cond_18

    .line 602
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    :cond_18
    return v1

    .line 601
    :catchall_19
    move-exception v1

    .line 602
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v1
.end method

.method private d(Lcom/a/b/n/a/dx;JLjava/util/concurrent/TimeUnit;)Z
    .registers 7

    .prologue
    .line 614
    iget-object v0, p1, Lcom/a/b/n/a/dx;->b:Lcom/a/b/n/a/dw;

    if-eq v0, p0, :cond_a

    .line 615
    new-instance v0, Ljava/lang/IllegalMonitorStateException;

    invoke-direct {v0}, Ljava/lang/IllegalMonitorStateException;-><init>()V

    throw v0

    .line 617
    :cond_a
    invoke-direct {p0, p2, p3, p4}, Lcom/a/b/n/a/dw;->a(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 618
    const/4 v0, 0x0

    .line 626
    :cond_11
    :goto_11
    return v0

    .line 623
    :cond_12
    :try_start_12
    invoke-virtual {p1}, Lcom/a/b/n/a/dx;->a()Z
    :try_end_15
    .catchall {:try_start_12 .. :try_end_15} :catchall_1e

    move-result v0

    .line 625
    if-nez v0, :cond_11

    .line 626
    iget-object v1, p0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_11

    .line 625
    :catchall_1e
    move-exception v0

    .line 626
    iget-object v1, p0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private e()Z
    .registers 2

    .prologue
    .line 780
    iget-boolean v0, p0, Lcom/a/b/n/a/dw;->b:Z

    return v0
.end method

.method private e(Lcom/a/b/n/a/dx;)Z
    .registers 4

    .prologue
    .line 666
    iget-object v0, p1, Lcom/a/b/n/a/dx;->b:Lcom/a/b/n/a/dw;

    if-eq v0, p0, :cond_a

    .line 667
    new-instance v0, Ljava/lang/IllegalMonitorStateException;

    invoke-direct {v0}, Ljava/lang/IllegalMonitorStateException;-><init>()V

    throw v0

    .line 669
    :cond_a
    iget-object v1, p0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    .line 670
    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock()Z

    move-result v0

    if-nez v0, :cond_14

    .line 671
    const/4 v0, 0x0

    .line 679
    :cond_13
    :goto_13
    return v0

    .line 676
    :cond_14
    :try_start_14
    invoke-virtual {p1}, Lcom/a/b/n/a/dx;->a()Z
    :try_end_17
    .catchall {:try_start_14 .. :try_end_17} :catchall_1e

    move-result v0

    .line 678
    if-nez v0, :cond_13

    .line 679
    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_13

    .line 678
    :catchall_1e
    move-exception v0

    .line 679
    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private e(Lcom/a/b/n/a/dx;JLjava/util/concurrent/TimeUnit;)Z
    .registers 7

    .prologue
    .line 639
    iget-object v0, p1, Lcom/a/b/n/a/dx;->b:Lcom/a/b/n/a/dw;

    if-eq v0, p0, :cond_a

    .line 640
    new-instance v0, Ljava/lang/IllegalMonitorStateException;

    invoke-direct {v0}, Ljava/lang/IllegalMonitorStateException;-><init>()V

    throw v0

    .line 642
    :cond_a
    iget-object v1, p0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    .line 643
    invoke-virtual {v1, p2, p3, p4}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_14

    .line 644
    const/4 v0, 0x0

    .line 652
    :cond_13
    :goto_13
    return v0

    .line 649
    :cond_14
    :try_start_14
    invoke-virtual {p1}, Lcom/a/b/n/a/dx;->a()Z
    :try_end_17
    .catchall {:try_start_14 .. :try_end_17} :catchall_1e

    move-result v0

    .line 651
    if-nez v0, :cond_13

    .line 652
    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_13

    .line 651
    :catchall_1e
    move-exception v0

    .line 652
    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private f(Lcom/a/b/n/a/dx;)V
    .registers 5

    .prologue
    const/4 v1, 0x1

    .line 689
    iget-object v0, p1, Lcom/a/b/n/a/dx;->b:Lcom/a/b/n/a/dw;

    if-ne v0, p0, :cond_15

    move v0, v1

    :goto_6
    iget-object v2, p0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->isHeldByCurrentThread()Z

    move-result v2

    and-int/2addr v0, v2

    if-nez v0, :cond_17

    .line 690
    new-instance v0, Ljava/lang/IllegalMonitorStateException;

    invoke-direct {v0}, Ljava/lang/IllegalMonitorStateException;-><init>()V

    throw v0

    .line 689
    :cond_15
    const/4 v0, 0x0

    goto :goto_6

    .line 692
    :cond_17
    invoke-virtual {p1}, Lcom/a/b/n/a/dx;->a()Z

    move-result v0

    if-nez v0, :cond_20

    .line 693
    invoke-direct {p0, p1, v1}, Lcom/a/b/n/a/dw;->a(Lcom/a/b/n/a/dx;Z)V

    .line 695
    :cond_20
    return-void
.end method

.method private f()Z
    .registers 2

    .prologue
    .line 788
    iget-object v0, p0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->isLocked()Z

    move-result v0

    return v0
.end method

.method private f(Lcom/a/b/n/a/dx;JLjava/util/concurrent/TimeUnit;)Z
    .registers 11

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 717
    invoke-virtual {p4, p2, p3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v4

    .line 718
    iget-object v0, p1, Lcom/a/b/n/a/dx;->b:Lcom/a/b/n/a/dw;

    if-ne v0, p0, :cond_1a

    move v0, v1

    :goto_b
    iget-object v3, p0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->isHeldByCurrentThread()Z

    move-result v3

    and-int/2addr v0, v3

    if-nez v0, :cond_1c

    .line 719
    new-instance v0, Ljava/lang/IllegalMonitorStateException;

    invoke-direct {v0}, Ljava/lang/IllegalMonitorStateException;-><init>()V

    throw v0

    :cond_1a
    move v0, v2

    .line 718
    goto :goto_b

    .line 721
    :cond_1c
    invoke-virtual {p1}, Lcom/a/b/n/a/dx;->a()Z

    move-result v0

    if-nez v0, :cond_28

    invoke-direct {p0, p1, v4, v5, v1}, Lcom/a/b/n/a/dw;->a(Lcom/a/b/n/a/dx;JZ)Z

    move-result v0

    if-eqz v0, :cond_29

    :cond_28
    move v2, v1

    :cond_29
    return v2
.end method

.method private g(Lcom/a/b/n/a/dx;)V
    .registers 5

    .prologue
    const/4 v1, 0x1

    .line 702
    iget-object v0, p1, Lcom/a/b/n/a/dx;->b:Lcom/a/b/n/a/dw;

    if-ne v0, p0, :cond_15

    move v0, v1

    :goto_6
    iget-object v2, p0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->isHeldByCurrentThread()Z

    move-result v2

    and-int/2addr v0, v2

    if-nez v0, :cond_17

    .line 703
    new-instance v0, Ljava/lang/IllegalMonitorStateException;

    invoke-direct {v0}, Ljava/lang/IllegalMonitorStateException;-><init>()V

    throw v0

    .line 702
    :cond_15
    const/4 v0, 0x0

    goto :goto_6

    .line 705
    :cond_17
    invoke-virtual {p1}, Lcom/a/b/n/a/dx;->a()Z

    move-result v0

    if-nez v0, :cond_20

    .line 706
    invoke-direct {p0, p1, v1}, Lcom/a/b/n/a/dw;->b(Lcom/a/b/n/a/dx;Z)V

    .line 708
    :cond_20
    return-void
.end method

.method private g()Z
    .registers 2

    .prologue
    .line 796
    iget-object v0, p0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->isHeldByCurrentThread()Z

    move-result v0

    return v0
.end method

.method private h()I
    .registers 2

    .prologue
    .line 804
    iget-object v0, p0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->getHoldCount()I

    move-result v0

    return v0
.end method

.method private h(Lcom/a/b/n/a/dx;)Z
    .registers 3

    .prologue
    .line 844
    invoke-direct {p0, p1}, Lcom/a/b/n/a/dw;->i(Lcom/a/b/n/a/dx;)I

    move-result v0

    if-lez v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private i()I
    .registers 2

    .prologue
    .line 814
    iget-object v0, p0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->getQueueLength()I

    move-result v0

    return v0
.end method

.method private i(Lcom/a/b/n/a/dx;)I
    .registers 4

    .prologue
    .line 854
    iget-object v0, p1, Lcom/a/b/n/a/dx;->b:Lcom/a/b/n/a/dw;

    if-eq v0, p0, :cond_a

    .line 855
    new-instance v0, Ljava/lang/IllegalMonitorStateException;

    invoke-direct {v0}, Ljava/lang/IllegalMonitorStateException;-><init>()V

    throw v0

    .line 857
    :cond_a
    iget-object v0, p0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 859
    :try_start_f
    iget v0, p1, Lcom/a/b/n/a/dx;->d:I
    :try_end_11
    .catchall {:try_start_f .. :try_end_11} :catchall_17

    .line 861
    iget-object v1, p0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return v0

    :catchall_17
    move-exception v0

    iget-object v1, p0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private j()Z
    .registers 2

    .prologue
    .line 824
    iget-object v0, p0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->hasQueuedThreads()Z

    move-result v0

    return v0
.end method

.method private j(Lcom/a/b/n/a/dx;)Z
    .registers 5
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "lock"
    .end annotation

    .prologue
    .line 924
    :try_start_0
    invoke-virtual {p1}, Lcom/a/b/n/a/dx;->a()Z
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_3} :catch_5

    move-result v0

    return v0

    .line 925
    :catch_5
    move-exception v1

    .line 1936
    iget-object v0, p0, Lcom/a/b/n/a/dw;->c:Lcom/a/b/n/a/dx;

    :goto_8
    if-eqz v0, :cond_12

    .line 1937
    iget-object v2, v0, Lcom/a/b/n/a/dx;->c:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    .line 1936
    iget-object v0, v0, Lcom/a/b/n/a/dx;->e:Lcom/a/b/n/a/dx;

    goto :goto_8

    .line 927
    :cond_12
    invoke-static {v1}, Lcom/a/b/b/ei;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method private k()V
    .registers 3
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "lock"
    .end annotation

    .prologue
    .line 891
    iget-object v0, p0, Lcom/a/b/n/a/dw;->c:Lcom/a/b/n/a/dx;

    :goto_2
    if-eqz v0, :cond_f

    .line 892
    invoke-direct {p0, v0}, Lcom/a/b/n/a/dw;->j(Lcom/a/b/n/a/dx;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 893
    iget-object v0, v0, Lcom/a/b/n/a/dx;->c:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signal()V

    .line 897
    :cond_f
    return-void

    .line 891
    :cond_10
    iget-object v0, v0, Lcom/a/b/n/a/dx;->e:Lcom/a/b/n/a/dx;

    goto :goto_2
.end method

.method private k(Lcom/a/b/n/a/dx;)V
    .registers 4
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "lock"
    .end annotation

    .prologue
    .line 946
    iget v0, p1, Lcom/a/b/n/a/dx;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p1, Lcom/a/b/n/a/dx;->d:I

    .line 947
    if-nez v0, :cond_e

    .line 949
    iget-object v0, p0, Lcom/a/b/n/a/dw;->c:Lcom/a/b/n/a/dx;

    iput-object v0, p1, Lcom/a/b/n/a/dx;->e:Lcom/a/b/n/a/dx;

    .line 950
    iput-object p1, p0, Lcom/a/b/n/a/dw;->c:Lcom/a/b/n/a/dx;

    .line 952
    :cond_e
    return-void
.end method

.method private l()V
    .registers 3
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "lock"
    .end annotation

    .prologue
    .line 936
    iget-object v0, p0, Lcom/a/b/n/a/dw;->c:Lcom/a/b/n/a/dx;

    :goto_2
    if-eqz v0, :cond_c

    .line 937
    iget-object v1, v0, Lcom/a/b/n/a/dx;->c:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    .line 936
    iget-object v0, v0, Lcom/a/b/n/a/dx;->e:Lcom/a/b/n/a/dx;

    goto :goto_2

    .line 939
    :cond_c
    return-void
.end method

.method private l(Lcom/a/b/n/a/dx;)V
    .registers 7
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "lock"
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 959
    iget v0, p1, Lcom/a/b/n/a/dx;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p1, Lcom/a/b/n/a/dx;->d:I

    .line 960
    if-nez v0, :cond_16

    .line 962
    iget-object v2, p0, Lcom/a/b/n/a/dw;->c:Lcom/a/b/n/a/dx;

    move-object v0, v1

    .line 963
    :goto_c
    if-ne v2, p1, :cond_1c

    .line 964
    if-nez v0, :cond_17

    .line 965
    iget-object v0, v2, Lcom/a/b/n/a/dx;->e:Lcom/a/b/n/a/dx;

    iput-object v0, p0, Lcom/a/b/n/a/dw;->c:Lcom/a/b/n/a/dx;

    .line 969
    :goto_14
    iput-object v1, v2, Lcom/a/b/n/a/dx;->e:Lcom/a/b/n/a/dx;

    .line 974
    :cond_16
    return-void

    .line 967
    :cond_17
    iget-object v3, v2, Lcom/a/b/n/a/dx;->e:Lcom/a/b/n/a/dx;

    iput-object v3, v0, Lcom/a/b/n/a/dx;->e:Lcom/a/b/n/a/dx;

    goto :goto_14

    .line 962
    :cond_1c
    iget-object v0, v2, Lcom/a/b/n/a/dx;->e:Lcom/a/b/n/a/dx;

    move-object v4, v2

    move-object v2, v0

    move-object v0, v4

    goto :goto_c
.end method


# virtual methods
.method public final a()V
    .registers 4

    .prologue
    .line 765
    iget-object v1, p0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    .line 768
    :try_start_2
    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->getHoldCount()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_c

    .line 769
    invoke-direct {p0}, Lcom/a/b/n/a/dw;->k()V
    :try_end_c
    .catchall {:try_start_2 .. :try_end_c} :catchall_10

    .line 772
    :cond_c
    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 773
    return-void

    .line 772
    :catchall_10
    move-exception v0

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final a(Lcom/a/b/n/a/dx;)V
    .registers 4

    .prologue
    .line 444
    iget-object v0, p1, Lcom/a/b/n/a/dx;->b:Lcom/a/b/n/a/dw;

    if-eq v0, p0, :cond_a

    .line 445
    new-instance v0, Ljava/lang/IllegalMonitorStateException;

    invoke-direct {v0}, Ljava/lang/IllegalMonitorStateException;-><init>()V

    throw v0

    .line 447
    :cond_a
    iget-object v0, p0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    .line 448
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->isHeldByCurrentThread()Z

    move-result v1

    .line 449
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 453
    :try_start_13
    invoke-virtual {p1}, Lcom/a/b/n/a/dx;->a()Z

    move-result v0

    if-nez v0, :cond_1c

    .line 454
    invoke-direct {p0, p1, v1}, Lcom/a/b/n/a/dw;->b(Lcom/a/b/n/a/dx;Z)V
    :try_end_1c
    .catchall {:try_start_13 .. :try_end_1c} :catchall_1d

    .line 458
    :cond_1c
    return-void

    :catchall_1d
    move-exception v0

    .line 459
    invoke-virtual {p0}, Lcom/a/b/n/a/dw;->a()V

    throw v0
.end method

.method public final a(Lcom/a/b/n/a/dx;JLjava/util/concurrent/TimeUnit;)Z
    .registers 21

    .prologue
    .line 513
    move-object/from16 v0, p4

    move-wide/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v4

    .line 514
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/a/b/n/a/dx;->b:Lcom/a/b/n/a/dw;

    move-object/from16 v0, p0

    if-eq v6, v0, :cond_16

    .line 515
    new-instance v4, Ljava/lang/IllegalMonitorStateException;

    invoke-direct {v4}, Ljava/lang/IllegalMonitorStateException;-><init>()V

    throw v4

    .line 517
    :cond_16
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    .line 518
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    add-long v12, v6, v4

    .line 519
    invoke-virtual {v10}, Ljava/util/concurrent/locks/ReentrantLock;->isHeldByCurrentThread()Z

    move-result v7

    .line 520
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v6

    .line 522
    :try_start_28
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/a/b/n/a/dw;->b:Z

    if-nez v8, :cond_34

    invoke-virtual {v10}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock()Z
    :try_end_31
    .catchall {:try_start_28 .. :try_end_31} :catchall_99

    move-result v8

    if-nez v8, :cond_54

    .line 523
    :cond_34
    const/4 v8, 0x0

    move v14, v8

    move-wide v8, v4

    move v4, v14

    .line 526
    :goto_38
    :try_start_38
    sget-object v5, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v10, v8, v9, v5}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_3d
    .catch Ljava/lang/InterruptedException; {:try_start_38 .. :try_end_3d} :catch_78
    .catchall {:try_start_38 .. :try_end_3d} :catchall_99

    move-result v4

    .line 527
    if-nez v4, :cond_4b

    .line 555
    if-eqz v6, :cond_49

    .line 556
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->interrupt()V

    :cond_49
    const/4 v4, 0x0

    :cond_4a
    :goto_4a
    return v4

    :cond_4b
    move v8, v4

    .line 533
    :goto_4c
    :try_start_4c
    invoke-static {}, Ljava/lang/System;->nanoTime()J
    :try_end_4f
    .catchall {:try_start_4c .. :try_end_4f} :catchall_99

    move-result-wide v4

    sub-long v4, v12, v4

    .line 534
    if-eqz v8, :cond_9b

    :cond_54
    move-wide v14, v4

    move v5, v6

    move v4, v7

    move-wide v6, v14

    .line 541
    :goto_58
    :try_start_58
    invoke-virtual/range {p1 .. p1}, Lcom/a/b/n/a/dx;->a()Z

    move-result v8

    if-nez v8, :cond_68

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6, v7, v4}, Lcom/a/b/n/a/dw;->a(Lcom/a/b/n/a/dx;JZ)Z
    :try_end_65
    .catch Ljava/lang/InterruptedException; {:try_start_58 .. :try_end_65} :catch_7e
    .catchall {:try_start_58 .. :try_end_65} :catchall_88

    move-result v4

    if-eqz v4, :cond_7c

    :cond_68
    const/4 v4, 0x1

    .line 550
    :goto_69
    if-nez v4, :cond_6e

    .line 551
    :try_start_6b
    invoke-virtual {v10}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_6e
    .catchall {:try_start_6b .. :try_end_6e} :catchall_8d

    .line 555
    :cond_6e
    if-eqz v5, :cond_4a

    .line 556
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->interrupt()V

    goto :goto_4a

    .line 531
    :catch_78
    move-exception v5

    const/4 v6, 0x1

    move v8, v4

    goto :goto_4c

    .line 541
    :cond_7c
    const/4 v4, 0x0

    goto :goto_69

    .line 544
    :catch_7e
    move-exception v4

    const/4 v5, 0x1

    .line 545
    const/4 v4, 0x0

    .line 546
    :try_start_81
    invoke-static {}, Ljava/lang/System;->nanoTime()J
    :try_end_84
    .catchall {:try_start_81 .. :try_end_84} :catchall_88

    move-result-wide v6

    sub-long v6, v12, v6

    .line 547
    goto :goto_58

    .line 550
    :catchall_88
    move-exception v4

    .line 551
    :try_start_89
    invoke-virtual {v10}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v4
    :try_end_8d
    .catchall {:try_start_89 .. :try_end_8d} :catchall_8d

    .line 555
    :catchall_8d
    move-exception v4

    move v6, v5

    :goto_8f
    if-eqz v6, :cond_98

    .line 556
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->interrupt()V

    :cond_98
    throw v4

    .line 555
    :catchall_99
    move-exception v4

    goto :goto_8f

    :cond_9b
    move v14, v8

    move-wide v8, v4

    move v4, v14

    goto :goto_38
.end method

.method public final b(Lcom/a/b/n/a/dx;)Z
    .registers 4

    .prologue
    .line 568
    iget-object v0, p1, Lcom/a/b/n/a/dx;->b:Lcom/a/b/n/a/dw;

    if-eq v0, p0, :cond_a

    .line 569
    new-instance v0, Ljava/lang/IllegalMonitorStateException;

    invoke-direct {v0}, Ljava/lang/IllegalMonitorStateException;-><init>()V

    throw v0

    .line 571
    :cond_a
    iget-object v0, p0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    .line 572
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 576
    :try_start_f
    invoke-virtual {p1}, Lcom/a/b/n/a/dx;->a()Z
    :try_end_12
    .catchall {:try_start_f .. :try_end_12} :catchall_19

    move-result v1

    .line 578
    if-nez v1, :cond_18

    .line 579
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    :cond_18
    return v1

    .line 578
    :catchall_19
    move-exception v1

    .line 579
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v1
.end method

.method public final b(Lcom/a/b/n/a/dx;JLjava/util/concurrent/TimeUnit;)Z
    .registers 13

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 731
    invoke-virtual {p4, p2, p3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v4

    .line 732
    iget-object v0, p1, Lcom/a/b/n/a/dx;->b:Lcom/a/b/n/a/dw;

    if-ne v0, p0, :cond_1a

    move v0, v1

    :goto_b
    iget-object v3, p0, Lcom/a/b/n/a/dw;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->isHeldByCurrentThread()Z

    move-result v3

    and-int/2addr v0, v3

    if-nez v0, :cond_1c

    .line 733
    new-instance v0, Ljava/lang/IllegalMonitorStateException;

    invoke-direct {v0}, Ljava/lang/IllegalMonitorStateException;-><init>()V

    throw v0

    :cond_1a
    move v0, v2

    .line 732
    goto :goto_b

    .line 735
    :cond_1c
    invoke-virtual {p1}, Lcom/a/b/n/a/dx;->a()Z

    move-result v0

    if-eqz v0, :cond_23

    .line 756
    :cond_22
    :goto_22
    return v1

    .line 739
    :cond_23
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    add-long/2addr v6, v4

    .line 740
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v3

    move v0, v1

    .line 744
    :goto_2d
    :try_start_2d
    invoke-direct {p0, p1, v4, v5, v0}, Lcom/a/b/n/a/dw;->a(Lcom/a/b/n/a/dx;JZ)Z
    :try_end_30
    .catch Ljava/lang/InterruptedException; {:try_start_2d .. :try_end_30} :catch_3b
    .catchall {:try_start_2d .. :try_end_30} :catchall_53

    move-result v1

    .line 755
    if-eqz v3, :cond_22

    .line 756
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_22

    .line 746
    :catch_3b
    move-exception v0

    .line 747
    :try_start_3c
    invoke-virtual {p1}, Lcom/a/b/n/a/dx;->a()Z
    :try_end_3f
    .catchall {:try_start_3c .. :try_end_3f} :catchall_5f

    move-result v0

    if-eqz v0, :cond_4a

    .line 756
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_22

    .line 751
    :cond_4a
    :try_start_4a
    invoke-static {}, Ljava/lang/System;->nanoTime()J
    :try_end_4d
    .catchall {:try_start_4a .. :try_end_4d} :catchall_5f

    move-result-wide v4

    sub-long v4, v6, v4

    move v3, v1

    move v0, v2

    .line 752
    goto :goto_2d

    .line 755
    :catchall_53
    move-exception v0

    move v1, v3

    :goto_55
    if-eqz v1, :cond_5e

    .line 756
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    :cond_5e
    throw v0

    .line 755
    :catchall_5f
    move-exception v0

    goto :goto_55
.end method
