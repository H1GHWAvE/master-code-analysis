.class final Lcom/a/b/n/a/ge;
.super Lcom/a/b/n/a/gi;
.source "SourceFile"


# instance fields
.field private final a:[Ljava/lang/Object;


# direct methods
.method private constructor <init>(ILcom/a/b/b/dz;)V
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 317
    invoke-direct {p0, p1}, Lcom/a/b/n/a/gi;-><init>(I)V

    .line 318
    const/high16 v0, 0x40000000    # 2.0f

    if-gt p1, v0, :cond_26

    const/4 v0, 0x1

    :goto_9
    const-string v2, "Stripes must be <= 2^30)"

    invoke-static {v0, v2}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 320
    iget v0, p0, Lcom/a/b/n/a/ge;->d:I

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/a/b/n/a/ge;->a:[Ljava/lang/Object;

    .line 321
    :goto_16
    iget-object v0, p0, Lcom/a/b/n/a/ge;->a:[Ljava/lang/Object;

    array-length v0, v0

    if-ge v1, v0, :cond_28

    .line 322
    iget-object v0, p0, Lcom/a/b/n/a/ge;->a:[Ljava/lang/Object;

    invoke-interface {p2}, Lcom/a/b/b/dz;->a()Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v0, v1

    .line 321
    add-int/lit8 v1, v1, 0x1

    goto :goto_16

    :cond_26
    move v0, v1

    .line 318
    goto :goto_9

    .line 324
    :cond_28
    return-void
.end method

.method synthetic constructor <init>(ILcom/a/b/b/dz;B)V
    .registers 4

    .prologue
    .line 312
    invoke-direct {p0, p1, p2}, Lcom/a/b/n/a/ge;-><init>(ILcom/a/b/b/dz;)V

    return-void
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 332
    iget-object v0, p0, Lcom/a/b/n/a/ge;->a:[Ljava/lang/Object;

    array-length v0, v0

    return v0
.end method

.method public final a(I)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 328
    iget-object v0, p0, Lcom/a/b/n/a/ge;->a:[Ljava/lang/Object;

    aget-object v0, v0, p1

    return-object v0
.end method
