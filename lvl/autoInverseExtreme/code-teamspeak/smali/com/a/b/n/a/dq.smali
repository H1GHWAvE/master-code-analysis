.class public Lcom/a/b/n/a/dq;
.super Ljava/util/concurrent/FutureTask;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/n/a/dp;


# instance fields
.field private final a:Lcom/a/b/n/a/bu;


# direct methods
.method private constructor <init>(Ljava/lang/Runnable;Ljava/lang/Object;)V
    .registers 4
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;)V

    .line 43
    new-instance v0, Lcom/a/b/n/a/bu;

    invoke-direct {v0}, Lcom/a/b/n/a/bu;-><init>()V

    iput-object v0, p0, Lcom/a/b/n/a/dq;->a:Lcom/a/b/n/a/bu;

    .line 79
    return-void
.end method

.method constructor <init>(Ljava/util/concurrent/Callable;)V
    .registers 3

    .prologue
    .line 74
    invoke-direct {p0, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 43
    new-instance v0, Lcom/a/b/n/a/bu;

    invoke-direct {v0}, Lcom/a/b/n/a/bu;-><init>()V

    iput-object v0, p0, Lcom/a/b/n/a/dq;->a:Lcom/a/b/n/a/bu;

    .line 75
    return-void
.end method

.method public static a(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/a/b/n/a/dq;
    .registers 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 70
    new-instance v0, Lcom/a/b/n/a/dq;

    invoke-direct {v0, p0, p1}, Lcom/a/b/n/a/dq;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/Callable;)Lcom/a/b/n/a/dq;
    .registers 2

    .prologue
    .line 53
    new-instance v0, Lcom/a/b/n/a/dq;

    invoke-direct {v0, p0}, Lcom/a/b/n/a/dq;-><init>(Ljava/util/concurrent/Callable;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .registers 4

    .prologue
    .line 83
    iget-object v0, p0, Lcom/a/b/n/a/dq;->a:Lcom/a/b/n/a/bu;

    invoke-virtual {v0, p1, p2}, Lcom/a/b/n/a/bu;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 84
    return-void
.end method

.method protected done()V
    .registers 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/a/b/n/a/dq;->a:Lcom/a/b/n/a/bu;

    invoke-virtual {v0}, Lcom/a/b/n/a/bu;->a()V

    .line 92
    return-void
.end method
