.class public abstract Lcom/a/b/n/a/cf;
.super Lcom/a/b/n/a/ca;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/n/a/du;


# direct methods
.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/a/b/n/a/ca;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Runnable;)Lcom/a/b/n/a/dp;
    .registers 3

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/a/b/n/a/cf;->b()Lcom/a/b/n/a/du;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/n/a/du;->a(Ljava/lang/Runnable;)Lcom/a/b/n/a/dp;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/a/b/n/a/dp;
    .registers 4

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/a/b/n/a/cf;->b()Lcom/a/b/n/a/du;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/a/b/n/a/du;->a(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/a/b/n/a/dp;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/concurrent/Callable;)Lcom/a/b/n/a/dp;
    .registers 3

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/a/b/n/a/cf;->b()Lcom/a/b/n/a/du;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/a/b/n/a/du;->a(Ljava/util/concurrent/Callable;)Lcom/a/b/n/a/dp;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a()Ljava/util/concurrent/ExecutorService;
    .registers 2

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/a/b/n/a/cf;->b()Lcom/a/b/n/a/du;

    move-result-object v0

    return-object v0
.end method

.method protected abstract b()Lcom/a/b/n/a/du;
.end method

.method protected final synthetic k_()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/a/b/n/a/cf;->b()Lcom/a/b/n/a/du;

    move-result-object v0

    return-object v0
.end method

.method public synthetic submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    .registers 3

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lcom/a/b/n/a/cf;->a(Ljava/lang/Runnable;)Lcom/a/b/n/a/dp;

    move-result-object v0

    return-object v0
.end method

.method public synthetic submit(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/Future;
    .registers 4

    .prologue
    .line 30
    invoke-virtual {p0, p1, p2}, Lcom/a/b/n/a/cf;->a(Ljava/lang/Runnable;Ljava/lang/Object;)Lcom/a/b/n/a/dp;

    move-result-object v0

    return-object v0
.end method

.method public synthetic submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    .registers 3

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lcom/a/b/n/a/cf;->a(Ljava/util/concurrent/Callable;)Lcom/a/b/n/a/dp;

    move-result-object v0

    return-object v0
.end method
