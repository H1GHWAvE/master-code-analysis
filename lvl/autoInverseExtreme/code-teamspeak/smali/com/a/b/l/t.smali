.class final enum Lcom/a/b/l/t;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# static fields
.field public static final enum a:Lcom/a/b/l/t;

.field private static final synthetic b:[Lcom/a/b/l/t;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 450
    new-instance v0, Lcom/a/b/l/t;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1}, Lcom/a/b/l/t;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/l/t;->a:Lcom/a/b/l/t;

    .line 449
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/a/b/l/t;

    const/4 v1, 0x0

    sget-object v2, Lcom/a/b/l/t;->a:Lcom/a/b/l/t;

    aput-object v2, v0, v1

    sput-object v0, Lcom/a/b/l/t;->b:[Lcom/a/b/l/t;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 449
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private static a([I[I)I
    .registers 6

    .prologue
    .line 454
    array-length v0, p0

    array-length v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 455
    const/4 v0, 0x0

    move v1, v0

    :goto_8
    if-ge v1, v2, :cond_19

    .line 456
    aget v0, p0, v1

    aget v3, p1, v1

    invoke-static {v0, v3}, Lcom/a/b/l/q;->a(II)I

    move-result v0

    .line 457
    if-eqz v0, :cond_15

    .line 461
    :goto_14
    return v0

    .line 455
    :cond_15
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    .line 461
    :cond_19
    array-length v0, p0

    array-length v1, p1

    sub-int/2addr v0, v1

    goto :goto_14
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/l/t;
    .registers 2

    .prologue
    .line 449
    const-class v0, Lcom/a/b/l/t;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/b/l/t;

    return-object v0
.end method

.method public static values()[Lcom/a/b/l/t;
    .registers 1

    .prologue
    .line 449
    sget-object v0, Lcom/a/b/l/t;->b:[Lcom/a/b/l/t;

    invoke-virtual {v0}, [Lcom/a/b/l/t;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/b/l/t;

    return-object v0
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 7

    .prologue
    .line 449
    check-cast p1, [I

    check-cast p2, [I

    .line 1454
    array-length v0, p1

    array-length v1, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1455
    const/4 v0, 0x0

    move v1, v0

    :goto_c
    if-ge v1, v2, :cond_1d

    .line 1456
    aget v0, p1, v1

    aget v3, p2, v1

    invoke-static {v0, v3}, Lcom/a/b/l/q;->a(II)I

    move-result v0

    .line 1457
    if-eqz v0, :cond_19

    .line 1458
    :goto_18
    return v0

    .line 1455
    :cond_19
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_c

    .line 1461
    :cond_1d
    array-length v0, p1

    array-length v1, p2

    sub-int/2addr v0, v1

    .line 449
    goto :goto_18
.end method
