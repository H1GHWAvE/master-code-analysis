.class final enum Lcom/a/b/l/aq;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# static fields
.field public static final enum a:Lcom/a/b/l/aq;

.field private static final synthetic b:[Lcom/a/b/l/aq;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 160
    new-instance v0, Lcom/a/b/l/aq;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1}, Lcom/a/b/l/aq;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/l/aq;->a:Lcom/a/b/l/aq;

    .line 159
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/a/b/l/aq;

    const/4 v1, 0x0

    sget-object v2, Lcom/a/b/l/aq;->a:Lcom/a/b/l/aq;

    aput-object v2, v0, v1

    sput-object v0, Lcom/a/b/l/aq;->b:[Lcom/a/b/l/aq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 159
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private static a([J[J)I
    .registers 8

    .prologue
    .line 164
    array-length v0, p0

    array-length v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 165
    const/4 v0, 0x0

    :goto_7
    if-ge v0, v1, :cond_1d

    .line 166
    aget-wide v2, p0, v0

    aget-wide v4, p1, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1a

    .line 167
    aget-wide v2, p0, v0

    aget-wide v0, p1, v0

    invoke-static {v2, v3, v0, v1}, Lcom/a/b/l/ap;->a(JJ)I

    move-result v0

    .line 170
    :goto_19
    return v0

    .line 165
    :cond_1a
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 170
    :cond_1d
    array-length v0, p0

    array-length v1, p1

    sub-int/2addr v0, v1

    goto :goto_19
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/l/aq;
    .registers 2

    .prologue
    .line 159
    const-class v0, Lcom/a/b/l/aq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/b/l/aq;

    return-object v0
.end method

.method public static values()[Lcom/a/b/l/aq;
    .registers 1

    .prologue
    .line 159
    sget-object v0, Lcom/a/b/l/aq;->b:[Lcom/a/b/l/aq;

    invoke-virtual {v0}, [Lcom/a/b/l/aq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/b/l/aq;

    return-object v0
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 9

    .prologue
    .line 159
    check-cast p1, [J

    check-cast p2, [J

    .line 1164
    array-length v0, p1

    array-length v1, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1165
    const/4 v0, 0x0

    :goto_b
    if-ge v0, v1, :cond_21

    .line 1166
    aget-wide v2, p1, v0

    aget-wide v4, p2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1e

    .line 1167
    aget-wide v2, p1, v0

    aget-wide v0, p2, v0

    invoke-static {v2, v3, v0, v1}, Lcom/a/b/l/ap;->a(JJ)I

    move-result v0

    :goto_1d
    return v0

    .line 1165
    :cond_1e
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 1170
    :cond_21
    array-length v0, p1

    array-length v1, p2

    sub-int/2addr v0, v1

    .line 159
    goto :goto_1d
.end method
