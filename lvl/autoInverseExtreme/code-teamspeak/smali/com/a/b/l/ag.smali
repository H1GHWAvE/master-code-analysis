.class public final Lcom/a/b/l/ag;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:B = -0x80t

.field public static final b:B = -0x1t

.field private static final c:I = 0xff


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(J)B
    .registers 6

    .prologue
    .line 89
    const/16 v0, 0x8

    shr-long v0, p0, v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_25

    .line 91
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x22

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Out of range: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 93
    :cond_25
    long-to-int v0, p0

    int-to-byte v0, v0

    return v0
.end method

.method private static a(Ljava/lang/String;)B
    .registers 5
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 208
    .line 9225
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/16 v1, 0xa

    invoke-static {v0, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    .line 9227
    shr-int/lit8 v1, v0, 0x8

    if-nez v1, :cond_12

    .line 9228
    int-to-byte v0, v0

    return v0

    .line 9230
    :cond_12
    new-instance v1, Ljava/lang/NumberFormatException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x19

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "out of range: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static varargs a([B)B
    .registers 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 138
    array-length v0, p0

    if-lez v0, :cond_1a

    move v0, v1

    :goto_6
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 139
    aget-byte v0, p0, v2

    .line 4075
    and-int/lit16 v0, v0, 0xff

    .line 140
    :goto_d
    array-length v2, p0

    if-ge v1, v2, :cond_1c

    .line 141
    aget-byte v2, p0, v1

    .line 5075
    and-int/lit16 v2, v2, 0xff

    .line 142
    if-ge v2, v0, :cond_17

    move v0, v2

    .line 140
    :cond_17
    add-int/lit8 v1, v1, 0x1

    goto :goto_d

    :cond_1a
    move v0, v2

    .line 138
    goto :goto_6

    .line 146
    :cond_1c
    int-to-byte v0, v0

    return v0
.end method

.method private static a(B)I
    .registers 2

    .prologue
    .line 75
    and-int/lit16 v0, p0, 0xff

    return v0
.end method

.method public static a(BB)I
    .registers 4

    .prologue
    .line 126
    .line 2075
    and-int/lit16 v0, p0, 0xff

    .line 3075
    and-int/lit16 v1, p1, 0xff

    .line 126
    sub-int/2addr v0, v1

    return v0
.end method

.method private static varargs a(Ljava/lang/String;[B)Ljava/lang/String;
    .registers 12

    .prologue
    const/16 v9, 0xa

    const/4 v8, 0x0

    const/4 v1, 0x1

    .line 244
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    array-length v0, p1

    if-nez v0, :cond_d

    .line 246
    const-string v0, ""

    .line 255
    :goto_c
    return-object v0

    .line 250
    :cond_d
    new-instance v2, Ljava/lang/StringBuilder;

    array-length v0, p1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x3

    mul-int/2addr v0, v3

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 251
    aget-byte v0, p1, v8

    .line 10075
    and-int/lit16 v0, v0, 0xff

    .line 251
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move v0, v1

    .line 252
    :goto_22
    array-length v3, p1

    if-ge v0, v3, :cond_44

    .line 253
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-byte v4, p1, v0

    .line 10191
    const-string v5, "radix (%s) must be between Character.MIN_RADIX and Character.MAX_RADIX"

    new-array v6, v1, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v1, v5, v6}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 11075
    and-int/lit16 v4, v4, 0xff

    .line 10194
    invoke-static {v4, v9}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v4

    .line 253
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    add-int/lit8 v0, v0, 0x1

    goto :goto_22

    .line 255
    :cond_44
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_c
.end method

.method private static a()Ljava/util/Comparator;
    .registers 1

    .prologue
    .line 275
    sget-object v0, Lcom/a/b/l/ah;->b:Ljava/util/Comparator;

    return-object v0
.end method

.method private static b(J)B
    .registers 4

    .prologue
    .line 105
    const-wide/16 v0, 0xff

    cmp-long v0, p0, v0

    if-lez v0, :cond_8

    .line 106
    const/4 v0, -0x1

    .line 111
    :goto_7
    return v0

    .line 108
    :cond_8
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-gez v0, :cond_10

    .line 109
    const/4 v0, 0x0

    goto :goto_7

    .line 111
    :cond_10
    long-to-int v0, p0

    int-to-byte v0, v0

    goto :goto_7
.end method

.method private static b(Ljava/lang/String;)B
    .registers 5
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    .line 225
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/16 v1, 0xa

    invoke-static {v0, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    .line 227
    shr-int/lit8 v1, v0, 0x8

    if-nez v1, :cond_12

    .line 228
    int-to-byte v0, v0

    return v0

    .line 230
    :cond_12
    new-instance v1, Ljava/lang/NumberFormatException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x19

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "out of range: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static varargs b([B)B
    .registers 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 158
    array-length v0, p0

    if-lez v0, :cond_1a

    move v0, v1

    :goto_6
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 159
    aget-byte v0, p0, v2

    .line 6075
    and-int/lit16 v0, v0, 0xff

    .line 160
    :goto_d
    array-length v2, p0

    if-ge v1, v2, :cond_1c

    .line 161
    aget-byte v2, p0, v1

    .line 7075
    and-int/lit16 v2, v2, 0xff

    .line 162
    if-le v2, v0, :cond_17

    move v0, v2

    .line 160
    :cond_17
    add-int/lit8 v1, v1, 0x1

    goto :goto_d

    :cond_1a
    move v0, v2

    .line 158
    goto :goto_6

    .line 166
    :cond_1c
    int-to-byte v0, v0

    return v0
.end method

.method private static b(B)Ljava/lang/String;
    .registers 7
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    const/16 v5, 0xa

    const/4 v4, 0x1

    .line 7191
    const-string v0, "radix (%s) must be between Character.MIN_RADIX and Character.MAX_RADIX"

    new-array v1, v4, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v4, v0, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 8075
    and-int/lit16 v0, p0, 0xff

    .line 7194
    invoke-static {v0, v5}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v0

    .line 176
    return-object v0
.end method

.method private static b()Ljava/util/Comparator;
    .registers 1
    .annotation build Lcom/a/b/a/d;
    .end annotation

    .prologue
    .line 280
    sget-object v0, Lcom/a/b/l/ai;->a:Lcom/a/b/l/ai;

    return-object v0
.end method

.method private static c(B)Ljava/lang/String;
    .registers 7
    .annotation build Lcom/a/b/a/a;
    .end annotation

    .prologue
    const/16 v5, 0xa

    const/4 v4, 0x1

    .line 191
    const-string v0, "radix (%s) must be between Character.MIN_RADIX and Character.MAX_RADIX"

    new-array v1, v4, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v4, v0, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 9075
    and-int/lit16 v0, p0, 0xff

    .line 194
    invoke-static {v0, v5}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
