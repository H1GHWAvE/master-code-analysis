.class final enum Lcom/a/b/l/v;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# static fields
.field public static final enum a:Lcom/a/b/l/v;

.field private static final synthetic b:[Lcom/a/b/l/v;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 483
    new-instance v0, Lcom/a/b/l/v;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1}, Lcom/a/b/l/v;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/l/v;->a:Lcom/a/b/l/v;

    .line 482
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/a/b/l/v;

    const/4 v1, 0x0

    sget-object v2, Lcom/a/b/l/v;->a:Lcom/a/b/l/v;

    aput-object v2, v0, v1

    sput-object v0, Lcom/a/b/l/v;->b:[Lcom/a/b/l/v;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 482
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private static a([J[J)I
    .registers 10

    .prologue
    .line 487
    array-length v0, p0

    array-length v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 488
    const/4 v0, 0x0

    move v1, v0

    :goto_8
    if-ge v1, v2, :cond_19

    .line 489
    aget-wide v4, p0, v1

    aget-wide v6, p1, v1

    invoke-static {v4, v5, v6, v7}, Lcom/a/b/l/u;->a(JJ)I

    move-result v0

    .line 490
    if-eqz v0, :cond_15

    .line 494
    :goto_14
    return v0

    .line 488
    :cond_15
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    .line 494
    :cond_19
    array-length v0, p0

    array-length v1, p1

    sub-int/2addr v0, v1

    goto :goto_14
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/l/v;
    .registers 2

    .prologue
    .line 482
    const-class v0, Lcom/a/b/l/v;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/b/l/v;

    return-object v0
.end method

.method public static values()[Lcom/a/b/l/v;
    .registers 1

    .prologue
    .line 482
    sget-object v0, Lcom/a/b/l/v;->b:[Lcom/a/b/l/v;

    invoke-virtual {v0}, [Lcom/a/b/l/v;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/b/l/v;

    return-object v0
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 11

    .prologue
    .line 482
    check-cast p1, [J

    check-cast p2, [J

    .line 1487
    array-length v0, p1

    array-length v1, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1488
    const/4 v0, 0x0

    move v1, v0

    :goto_c
    if-ge v1, v2, :cond_1d

    .line 1489
    aget-wide v4, p1, v1

    aget-wide v6, p2, v1

    invoke-static {v4, v5, v6, v7}, Lcom/a/b/l/u;->a(JJ)I

    move-result v0

    .line 1490
    if-eqz v0, :cond_19

    .line 1491
    :goto_18
    return v0

    .line 1488
    :cond_19
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_c

    .line 1494
    :cond_1d
    array-length v0, p1

    array-length v1, p2

    sub-int/2addr v0, v1

    .line 482
    goto :goto_18
.end method
