.class public final Lcom/a/b/l/am;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation

.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field static final a:J = 0xffffffffL


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(II)I
    .registers 4

    .prologue
    const/high16 v1, -0x80000000

    .line 68
    .line 1055
    xor-int v0, p0, v1

    .line 2055
    xor-int/2addr v1, p1

    .line 68
    invoke-static {v0, v1}, Lcom/a/b/l/q;->a(II)I

    move-result v0

    return v0
.end method

.method private static a(Ljava/lang/String;)I
    .registers 6

    .prologue
    .line 212
    invoke-static {p0}, Lcom/a/b/l/y;->a(Ljava/lang/String;)Lcom/a/b/l/y;

    move-result-object v0

    .line 215
    :try_start_4
    iget-object v1, v0, Lcom/a/b/l/y;->a:Ljava/lang/String;

    iget v0, v0, Lcom/a/b/l/y;->b:I

    invoke-static {v1, v0}, Lcom/a/b/l/am;->a(Ljava/lang/String;I)I
    :try_end_b
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_b} :catch_d

    move-result v0

    return v0

    .line 216
    :catch_d
    move-exception v0

    move-object v1, v0

    .line 217
    new-instance v2, Ljava/lang/NumberFormatException;

    const-string v3, "Error parsing value: "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_28

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_21
    invoke-direct {v2, v0}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    .line 219
    invoke-virtual {v2, v1}, Ljava/lang/NumberFormatException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 220
    throw v2

    .line 217
    :cond_28
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_21
.end method

.method public static a(Ljava/lang/String;I)I
    .registers 6

    .prologue
    .line 247
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    invoke-static {p0, p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v0

    .line 249
    const-wide v2, 0xffffffffL

    and-long/2addr v2, v0

    cmp-long v2, v2, v0

    if-eqz v2, :cond_48

    .line 250
    new-instance v0, Ljava/lang/NumberFormatException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x45

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Input "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in base "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not in the range of an unsigned integer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 253
    :cond_48
    long-to-int v0, v0

    return v0
.end method

.method private static varargs a([I)I
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/high16 v3, -0x80000000

    .line 87
    array-length v0, p0

    if-lez v0, :cond_1a

    move v0, v1

    :goto_8
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 88
    aget v0, p0, v2

    .line 3055
    xor-int/2addr v0, v3

    .line 89
    :goto_e
    array-length v2, p0

    if-ge v1, v2, :cond_1c

    .line 90
    aget v2, p0, v1

    .line 4055
    xor-int/2addr v2, v3

    .line 91
    if-ge v2, v0, :cond_17

    move v0, v2

    .line 89
    :cond_17
    add-int/lit8 v1, v1, 0x1

    goto :goto_e

    :cond_1a
    move v0, v2

    .line 87
    goto :goto_8

    .line 5055
    :cond_1c
    xor-int/2addr v0, v3

    .line 95
    return v0
.end method

.method public static a(I)Ljava/lang/String;
    .registers 5

    .prologue
    .line 273
    int-to-long v0, p0

    const-wide v2, 0xffffffffL

    and-long/2addr v0, v2

    .line 274
    const/16 v2, 0xa

    invoke-static {v0, v1, v2}, Ljava/lang/Long;->toString(JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static varargs a(Ljava/lang/String;[I)Ljava/lang/String;
    .registers 6

    .prologue
    .line 127
    invoke-static {p0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    array-length v0, p1

    if-nez v0, :cond_9

    .line 129
    const-string v0, ""

    .line 138
    :goto_8
    return-object v0

    .line 133
    :cond_9
    new-instance v1, Ljava/lang/StringBuilder;

    array-length v0, p1

    mul-int/lit8 v0, v0, 0x5

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 134
    const/4 v0, 0x0

    aget v0, p1, v0

    .line 8260
    invoke-static {v0}, Lcom/a/b/l/am;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 134
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    const/4 v0, 0x1

    :goto_1c
    array-length v2, p1

    if-ge v0, v2, :cond_2f

    .line 136
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, p1, v0

    .line 9260
    invoke-static {v3}, Lcom/a/b/l/am;->a(I)Ljava/lang/String;

    move-result-object v3

    .line 136
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    add-int/lit8 v0, v0, 0x1

    goto :goto_1c

    .line 138
    :cond_2f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_8
.end method

.method private static a()Ljava/util/Comparator;
    .registers 1

    .prologue
    .line 154
    sget-object v0, Lcom/a/b/l/an;->a:Lcom/a/b/l/an;

    return-object v0
.end method

.method private static b(I)I
    .registers 2

    .prologue
    .line 55
    const/high16 v0, -0x80000000

    xor-int/2addr v0, p0

    return v0
.end method

.method private static b(II)I
    .registers 8

    .prologue
    const-wide v4, 0xffffffffL

    .line 181
    .line 10075
    int-to-long v0, p0

    and-long/2addr v0, v4

    .line 11075
    int-to-long v2, p1

    and-long/2addr v2, v4

    .line 181
    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method private static b(Ljava/lang/String;)I
    .registers 2

    .prologue
    .line 232
    const/16 v0, 0xa

    invoke-static {p0, v0}, Lcom/a/b/l/am;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private static varargs b([I)I
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/high16 v3, -0x80000000

    .line 107
    array-length v0, p0

    if-lez v0, :cond_1a

    move v0, v1

    :goto_8
    invoke-static {v0}, Lcom/a/b/b/cn;->a(Z)V

    .line 108
    aget v0, p0, v2

    .line 6055
    xor-int/2addr v0, v3

    .line 109
    :goto_e
    array-length v2, p0

    if-ge v1, v2, :cond_1c

    .line 110
    aget v2, p0, v1

    .line 7055
    xor-int/2addr v2, v3

    .line 111
    if-le v2, v0, :cond_17

    move v0, v2

    .line 109
    :cond_17
    add-int/lit8 v1, v1, 0x1

    goto :goto_e

    :cond_1a
    move v0, v2

    .line 107
    goto :goto_8

    .line 8055
    :cond_1c
    xor-int/2addr v0, v3

    .line 115
    return v0
.end method

.method private static c(II)I
    .registers 8

    .prologue
    const-wide v4, 0xffffffffL

    .line 193
    .line 12075
    int-to-long v0, p0

    and-long/2addr v0, v4

    .line 13075
    int-to-long v2, p1

    and-long/2addr v2, v4

    .line 193
    rem-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method private static c(I)J
    .registers 5

    .prologue
    .line 75
    int-to-long v0, p0

    const-wide v2, 0xffffffffL

    and-long/2addr v0, v2

    return-wide v0
.end method

.method private static d(I)Ljava/lang/String;
    .registers 2

    .prologue
    .line 260
    invoke-static {p0}, Lcom/a/b/l/am;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
