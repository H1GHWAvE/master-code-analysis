.class public final Lcom/a/b/g/am;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# static fields
.field private static final a:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 86
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/a/b/g/am;->a:I

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 486
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(JI)I
    .registers 13

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 337
    if-lez p2, :cond_3b

    move v0, v1

    :goto_5
    const-string v3, "buckets must be positive: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 338
    new-instance v1, Lcom/a/b/g/av;

    invoke-direct {v1, p0, p1}, Lcom/a/b/g/av;-><init>(J)V

    .line 344
    :goto_17
    add-int/lit8 v0, v2, 0x1

    int-to-double v4, v0

    .line 3481
    const-wide v6, 0x27bb2ee687b0b0fdL    # 2.694898184339827E-117

    iget-wide v8, v1, Lcom/a/b/g/av;->a:J

    mul-long/2addr v6, v8

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    iput-wide v6, v1, Lcom/a/b/g/av;->a:J

    .line 3482
    iget-wide v6, v1, Lcom/a/b/g/av;->a:J

    const/16 v0, 0x21

    ushr-long/2addr v6, v0

    long-to-int v0, v6

    add-int/lit8 v0, v0, 0x1

    int-to-double v6, v0

    const-wide/high16 v8, 0x41e0000000000000L    # 2.147483648E9

    div-double/2addr v6, v8

    .line 344
    div-double/2addr v4, v6

    double-to-int v0, v4

    .line 345
    if-ltz v0, :cond_3d

    if-ge v0, p2, :cond_3d

    move v2, v0

    .line 346
    goto :goto_17

    :cond_3b
    move v0, v2

    .line 337
    goto :goto_5

    .line 348
    :cond_3d
    return v2
.end method

.method private static a(Lcom/a/b/g/ag;I)I
    .registers 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 320
    invoke-virtual {p0}, Lcom/a/b/g/ag;->d()J

    move-result-wide v4

    .line 2337
    if-lez p1, :cond_3f

    move v0, v1

    :goto_9
    const-string v3, "buckets must be positive: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v1, v2

    invoke-static {v0, v3, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 2338
    new-instance v1, Lcom/a/b/g/av;

    invoke-direct {v1, v4, v5}, Lcom/a/b/g/av;-><init>(J)V

    .line 2344
    :goto_1b
    add-int/lit8 v0, v2, 0x1

    int-to-double v4, v0

    .line 2481
    const-wide v6, 0x27bb2ee687b0b0fdL    # 2.694898184339827E-117

    iget-wide v8, v1, Lcom/a/b/g/av;->a:J

    mul-long/2addr v6, v8

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    iput-wide v6, v1, Lcom/a/b/g/av;->a:J

    .line 2482
    iget-wide v6, v1, Lcom/a/b/g/av;->a:J

    const/16 v0, 0x21

    ushr-long/2addr v6, v0

    long-to-int v0, v6

    add-int/lit8 v0, v0, 0x1

    int-to-double v6, v0

    const-wide/high16 v8, 0x41e0000000000000L    # 2.147483648E9

    div-double/2addr v6, v8

    .line 2344
    div-double/2addr v4, v6

    double-to-int v0, v4

    .line 2345
    if-ltz v0, :cond_41

    if-ge v0, p1, :cond_41

    move v2, v0

    .line 2346
    goto :goto_1b

    :cond_3f
    move v0, v2

    .line 2337
    goto :goto_9

    .line 320
    :cond_41
    return v2
.end method

.method private static a(Ljava/lang/Iterable;)Lcom/a/b/g/ag;
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 364
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 365
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const-string v3, "Must be at least 1 hash code to combine."

    invoke-static {v2, v3}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 366
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/g/ag;

    invoke-virtual {v0}, Lcom/a/b/g/ag;->a()I

    move-result v0

    .line 367
    div-int/lit8 v0, v0, 0x8

    new-array v2, v0, [B

    .line 368
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_20
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/g/ag;

    .line 369
    invoke-virtual {v0}, Lcom/a/b/g/ag;->e()[B

    move-result-object v4

    .line 370
    array-length v0, v4

    array-length v5, v2

    if-ne v0, v5, :cond_4b

    const/4 v0, 0x1

    :goto_35
    const-string v5, "All hashcodes must have the same bit length."

    invoke-static {v0, v5}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    move v0, v1

    .line 372
    :goto_3b
    array-length v5, v4

    if-ge v0, v5, :cond_20

    .line 373
    aget-byte v5, v2, v0

    mul-int/lit8 v5, v5, 0x25

    aget-byte v6, v4, v0

    xor-int/2addr v5, v6

    int-to-byte v5, v5

    aput-byte v5, v2, v0

    .line 372
    add-int/lit8 v0, v0, 0x1

    goto :goto_3b

    :cond_4b
    move v0, v1

    .line 370
    goto :goto_35

    .line 376
    :cond_4d
    invoke-static {v2}, Lcom/a/b/g/ag;->a([B)Lcom/a/b/g/ag;

    move-result-object v0

    return-object v0
.end method

.method public static a()Lcom/a/b/g/ak;
    .registers 1

    .prologue
    .line 109
    sget-object v0, Lcom/a/b/g/ay;->a:Lcom/a/b/g/ak;

    return-object v0
.end method

.method public static a(I)Lcom/a/b/g/ak;
    .registers 2

    .prologue
    .line 97
    new-instance v0, Lcom/a/b/g/bl;

    invoke-direct {v0, p0}, Lcom/a/b/g/bl;-><init>(I)V

    return-object v0
.end method

.method private static a(JJ)Lcom/a/b/g/ak;
    .registers 6

    .prologue
    .line 174
    new-instance v0, Lcom/a/b/g/bo;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/a/b/g/bo;-><init>(JJ)V

    return-object v0
.end method

.method static synthetic a(Lcom/a/b/g/ap;Ljava/lang/String;)Lcom/a/b/g/ak;
    .registers 4

    .prologue
    .line 4279
    new-instance v0, Lcom/a/b/g/r;

    invoke-static {p0}, Lcom/a/b/g/ap;->a(Lcom/a/b/g/ap;)I

    move-result v1

    invoke-direct {v0, p0, v1, p1}, Lcom/a/b/g/r;-><init>(Lcom/a/b/b/dz;ILjava/lang/String;)V

    .line 44
    return-object v0
.end method

.method private static b(Ljava/lang/Iterable;)Lcom/a/b/g/ag;
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 390
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 391
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const-string v3, "Must be at least 1 hash code to combine."

    invoke-static {v2, v3}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 392
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/g/ag;

    invoke-virtual {v0}, Lcom/a/b/g/ag;->a()I

    move-result v0

    div-int/lit8 v0, v0, 0x8

    new-array v2, v0, [B

    .line 393
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_20
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/g/ag;

    .line 394
    invoke-virtual {v0}, Lcom/a/b/g/ag;->e()[B

    move-result-object v4

    .line 395
    array-length v0, v4

    array-length v5, v2

    if-ne v0, v5, :cond_49

    const/4 v0, 0x1

    :goto_35
    const-string v5, "All hashcodes must have the same bit length."

    invoke-static {v0, v5}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    move v0, v1

    .line 397
    :goto_3b
    array-length v5, v4

    if-ge v0, v5, :cond_20

    .line 398
    aget-byte v5, v2, v0

    aget-byte v6, v4, v0

    add-int/2addr v5, v6

    int-to-byte v5, v5

    aput-byte v5, v2, v0

    .line 397
    add-int/lit8 v0, v0, 0x1

    goto :goto_3b

    :cond_49
    move v0, v1

    .line 395
    goto :goto_35

    .line 401
    :cond_4b
    invoke-static {v2}, Lcom/a/b/g/ag;->a([B)Lcom/a/b/g/ag;

    move-result-object v0

    return-object v0
.end method

.method public static b()Lcom/a/b/g/ak;
    .registers 1

    .prologue
    .line 140
    sget-object v0, Lcom/a/b/g/ax;->a:Lcom/a/b/g/ak;

    return-object v0
.end method

.method public static b(I)Lcom/a/b/g/ak;
    .registers 2

    .prologue
    .line 128
    new-instance v0, Lcom/a/b/g/bj;

    invoke-direct {v0, p0}, Lcom/a/b/g/bj;-><init>(I)V

    return-object v0
.end method

.method private static b(Lcom/a/b/g/ap;Ljava/lang/String;)Lcom/a/b/g/ak;
    .registers 4

    .prologue
    .line 279
    new-instance v0, Lcom/a/b/g/r;

    invoke-static {p0}, Lcom/a/b/g/ap;->a(Lcom/a/b/g/ap;)I

    move-result v1

    invoke-direct {v0, p0, v1, p1}, Lcom/a/b/g/r;-><init>(Lcom/a/b/b/dz;ILjava/lang/String;)V

    return-object v0
.end method

.method static synthetic c()I
    .registers 1

    .prologue
    .line 44
    sget v0, Lcom/a/b/g/am;->a:I

    return v0
.end method

.method private static c(I)Lcom/a/b/g/ak;
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 61
    .line 1408
    if-lez p0, :cond_15

    move v0, v1

    :goto_5
    const-string v3, "Number of bits must be positive"

    invoke-static {v0, v3}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 1409
    add-int/lit8 v0, p0, 0x1f

    and-int/lit8 v0, v0, -0x20

    .line 63
    const/16 v3, 0x20

    if-ne v0, v3, :cond_17

    .line 64
    sget-object v0, Lcom/a/b/g/ay;->b:Lcom/a/b/g/ak;

    .line 79
    :goto_14
    return-object v0

    :cond_15
    move v0, v2

    .line 1408
    goto :goto_5

    .line 66
    :cond_17
    const/16 v3, 0x80

    if-gt v0, v3, :cond_1e

    .line 67
    sget-object v0, Lcom/a/b/g/ax;->b:Lcom/a/b/g/ak;

    goto :goto_14

    .line 71
    :cond_1e
    add-int/lit8 v0, v0, 0x7f

    div-int/lit16 v3, v0, 0x80

    .line 72
    new-array v4, v3, [Lcom/a/b/g/ak;

    .line 73
    sget-object v0, Lcom/a/b/g/ax;->b:Lcom/a/b/g/ak;

    aput-object v0, v4, v2

    .line 74
    sget v0, Lcom/a/b/g/am;->a:I

    .line 75
    :goto_2a
    if-ge v1, v3, :cond_39

    .line 76
    const v2, 0x596f0ddf

    add-int/2addr v0, v2

    .line 77
    invoke-static {v0}, Lcom/a/b/g/am;->b(I)Lcom/a/b/g/ak;

    move-result-object v2

    aput-object v2, v4, v1

    .line 75
    add-int/lit8 v1, v1, 0x1

    goto :goto_2a

    .line 79
    :cond_39
    new-instance v0, Lcom/a/b/g/as;

    invoke-direct {v0, v4}, Lcom/a/b/g/as;-><init>([Lcom/a/b/g/ak;)V

    goto :goto_14
.end method

.method private static d(I)I
    .registers 3

    .prologue
    .line 408
    if-lez p0, :cond_d

    const/4 v0, 0x1

    :goto_3
    const-string v1, "Number of bits must be positive"

    invoke-static {v0, v1}, Lcom/a/b/b/cn;->a(ZLjava/lang/Object;)V

    .line 409
    add-int/lit8 v0, p0, 0x1f

    and-int/lit8 v0, v0, -0x20

    return v0

    .line 408
    :cond_d
    const/4 v0, 0x0

    goto :goto_3
.end method

.method private static d()Lcom/a/b/g/ak;
    .registers 1

    .prologue
    .line 158
    sget-object v0, Lcom/a/b/g/bc;->a:Lcom/a/b/g/ak;

    return-object v0
.end method

.method private static e()Lcom/a/b/g/ak;
    .registers 1

    .prologue
    .line 182
    sget-object v0, Lcom/a/b/g/aw;->a:Lcom/a/b/g/ak;

    return-object v0
.end method

.method private static f()Lcom/a/b/g/ak;
    .registers 1

    .prologue
    .line 194
    sget-object v0, Lcom/a/b/g/az;->a:Lcom/a/b/g/ak;

    return-object v0
.end method

.method private static g()Lcom/a/b/g/ak;
    .registers 1

    .prologue
    .line 207
    sget-object v0, Lcom/a/b/g/ba;->a:Lcom/a/b/g/ak;

    return-object v0
.end method

.method private static h()Lcom/a/b/g/ak;
    .registers 1

    .prologue
    .line 220
    sget-object v0, Lcom/a/b/g/bb;->a:Lcom/a/b/g/ak;

    return-object v0
.end method

.method private static i()Lcom/a/b/g/ak;
    .registers 1

    .prologue
    .line 235
    sget-object v0, Lcom/a/b/g/au;->a:Lcom/a/b/g/ak;

    return-object v0
.end method

.method private static j()Lcom/a/b/g/ak;
    .registers 1

    .prologue
    .line 252
    sget-object v0, Lcom/a/b/g/at;->a:Lcom/a/b/g/ak;

    return-object v0
.end method

.method private static k()Lcom/a/b/g/ak;
    .registers 1

    .prologue
    .line 270
    sget-object v0, Lcom/a/b/g/ao;->a:Lcom/a/b/g/ak;

    return-object v0
.end method
