.class public final Lcom/a/b/g/be;
.super Ljava/io/FilterOutputStream;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/a;
.end annotation


# instance fields
.field private final a:Lcom/a/b/g/al;


# direct methods
.method private constructor <init>(Lcom/a/b/g/ak;Ljava/io/OutputStream;)V
    .registers 4

    .prologue
    .line 46
    invoke-static {p2}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/OutputStream;

    invoke-direct {p0, v0}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 47
    invoke-interface {p1}, Lcom/a/b/g/ak;->a()Lcom/a/b/g/al;

    move-result-object v0

    invoke-static {v0}, Lcom/a/b/b/cn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/b/g/al;

    iput-object v0, p0, Lcom/a/b/g/be;->a:Lcom/a/b/g/al;

    .line 48
    return-void
.end method

.method private a()Lcom/a/b/g/ag;
    .registers 2

    .prologue
    .line 65
    iget-object v0, p0, Lcom/a/b/g/be;->a:Lcom/a/b/g/al;

    invoke-interface {v0}, Lcom/a/b/g/al;->a()Lcom/a/b/g/ag;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final close()V
    .registers 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/a/b/g/be;->out:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 73
    return-void
.end method

.method public final write(I)V
    .registers 4

    .prologue
    .line 51
    iget-object v0, p0, Lcom/a/b/g/be;->a:Lcom/a/b/g/al;

    int-to-byte v1, p1

    invoke-interface {v0, v1}, Lcom/a/b/g/al;->b(B)Lcom/a/b/g/al;

    .line 52
    iget-object v0, p0, Lcom/a/b/g/be;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    .line 53
    return-void
.end method

.method public final write([BII)V
    .registers 5

    .prologue
    .line 56
    iget-object v0, p0, Lcom/a/b/g/be;->a:Lcom/a/b/g/al;

    invoke-interface {v0, p1, p2, p3}, Lcom/a/b/g/al;->b([BII)Lcom/a/b/g/al;

    .line 57
    iget-object v0, p0, Lcom/a/b/g/be;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 58
    return-void
.end method
