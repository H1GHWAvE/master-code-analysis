.class final enum Lcom/a/b/g/z;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/a/b/g/w;


# static fields
.field public static final enum a:Lcom/a/b/g/z;

.field private static final synthetic b:[Lcom/a/b/g/z;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 144
    new-instance v0, Lcom/a/b/g/z;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1}, Lcom/a/b/g/z;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/b/g/z;->a:Lcom/a/b/g/z;

    .line 143
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/a/b/g/z;

    const/4 v1, 0x0

    sget-object v2, Lcom/a/b/g/z;->a:Lcom/a/b/g/z;

    aput-object v2, v0, v1

    sput-object v0, Lcom/a/b/g/z;->b:[Lcom/a/b/g/z;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 143
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private static a(Ljava/lang/Integer;Lcom/a/b/g/bn;)V
    .registers 3

    .prologue
    .line 147
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/a/b/g/bn;->b(I)Lcom/a/b/g/bn;

    .line 148
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/g/z;
    .registers 2

    .prologue
    .line 143
    const-class v0, Lcom/a/b/g/z;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/a/b/g/z;

    return-object v0
.end method

.method public static values()[Lcom/a/b/g/z;
    .registers 1

    .prologue
    .line 143
    sget-object v0, Lcom/a/b/g/z;->b:[Lcom/a/b/g/z;

    invoke-virtual {v0}, [Lcom/a/b/g/z;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/b/g/z;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Lcom/a/b/g/bn;)V
    .registers 4

    .prologue
    .line 143
    check-cast p1, Ljava/lang/Integer;

    .line 1147
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p2, v0}, Lcom/a/b/g/bn;->b(I)Lcom/a/b/g/bn;

    .line 143
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 151
    const-string v0, "Funnels.integerFunnel()"

    return-object v0
.end method
