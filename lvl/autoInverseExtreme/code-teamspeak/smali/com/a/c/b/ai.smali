.class final Lcom/a/c/b/ai;
.super Ljava/util/AbstractSet;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/c/b/ag;


# direct methods
.method constructor <init>(Lcom/a/c/b/ag;)V
    .registers 2

    .prologue
    .line 557
    iput-object p1, p0, Lcom/a/c/b/ai;->a:Lcom/a/c/b/ag;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method


# virtual methods
.method public final clear()V
    .registers 2

    .prologue
    .line 588
    iget-object v0, p0, Lcom/a/c/b/ai;->a:Lcom/a/c/b/ag;

    invoke-virtual {v0}, Lcom/a/c/b/ag;->clear()V

    .line 589
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .registers 3

    .prologue
    .line 571
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/a/c/b/ai;->a:Lcom/a/c/b/ag;

    check-cast p1, Ljava/util/Map$Entry;

    invoke-virtual {v0, p1}, Lcom/a/c/b/ag;->a(Ljava/util/Map$Entry;)Lcom/a/c/b/an;

    move-result-object v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 563
    new-instance v0, Lcom/a/c/b/aj;

    invoke-direct {v0, p0}, Lcom/a/c/b/aj;-><init>(Lcom/a/c/b/ai;)V

    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 575
    instance-of v2, p1, Ljava/util/Map$Entry;

    if-nez v2, :cond_7

    .line 584
    :cond_6
    :goto_6
    return v0

    .line 579
    :cond_7
    iget-object v2, p0, Lcom/a/c/b/ai;->a:Lcom/a/c/b/ag;

    check-cast p1, Ljava/util/Map$Entry;

    invoke-virtual {v2, p1}, Lcom/a/c/b/ag;->a(Ljava/util/Map$Entry;)Lcom/a/c/b/an;

    move-result-object v2

    .line 580
    if-eqz v2, :cond_6

    .line 583
    iget-object v0, p0, Lcom/a/c/b/ai;->a:Lcom/a/c/b/ag;

    invoke-virtual {v0, v2, v1}, Lcom/a/c/b/ag;->a(Lcom/a/c/b/an;Z)V

    move v0, v1

    .line 584
    goto :goto_6
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 559
    iget-object v0, p0, Lcom/a/c/b/ai;->a:Lcom/a/c/b/ag;

    iget v0, v0, Lcom/a/c/b/ag;->c:I

    return v0
.end method
