.class public final Lcom/a/c/b/a/j;
.super Lcom/a/c/d/e;
.source "SourceFile"


# static fields
.field private static final f:Ljava/io/Writer;

.field private static final g:Lcom/a/c/ac;


# instance fields
.field private final h:Ljava/util/List;

.field private i:Ljava/lang/String;

.field private j:Lcom/a/c/w;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 34
    new-instance v0, Lcom/a/c/b/a/k;

    invoke-direct {v0}, Lcom/a/c/b/a/k;-><init>()V

    sput-object v0, Lcom/a/c/b/a/j;->f:Ljava/io/Writer;

    .line 46
    new-instance v0, Lcom/a/c/ac;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Lcom/a/c/ac;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/c/b/a/j;->g:Lcom/a/c/ac;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 58
    sget-object v0, Lcom/a/c/b/a/j;->f:Ljava/io/Writer;

    invoke-direct {p0, v0}, Lcom/a/c/d/e;-><init>(Ljava/io/Writer;)V

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/a/c/b/a/j;->h:Ljava/util/List;

    .line 55
    sget-object v0, Lcom/a/c/y;->a:Lcom/a/c/y;

    iput-object v0, p0, Lcom/a/c/b/a/j;->j:Lcom/a/c/w;

    .line 59
    return-void
.end method

.method private a(Lcom/a/c/w;)V
    .registers 4

    .prologue
    .line 76
    iget-object v0, p0, Lcom/a/c/b/a/j;->i:Ljava/lang/String;

    if-eqz v0, :cond_1b

    .line 1074
    instance-of v0, p1, Lcom/a/c/y;

    .line 77
    if-eqz v0, :cond_c

    .line 1277
    iget-boolean v0, p0, Lcom/a/c/d/e;->e:Z

    .line 77
    if-eqz v0, :cond_17

    .line 78
    :cond_c
    invoke-direct {p0}, Lcom/a/c/b/a/j;->g()Lcom/a/c/w;

    move-result-object v0

    check-cast v0, Lcom/a/c/z;

    .line 79
    iget-object v1, p0, Lcom/a/c/b/a/j;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/a/c/z;->a(Ljava/lang/String;Lcom/a/c/w;)V

    .line 81
    :cond_17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/c/b/a/j;->i:Ljava/lang/String;

    .line 87
    :goto_1a
    return-void

    .line 82
    :cond_1b
    iget-object v0, p0, Lcom/a/c/b/a/j;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_26

    .line 83
    iput-object p1, p0, Lcom/a/c/b/a/j;->j:Lcom/a/c/w;

    goto :goto_1a

    .line 85
    :cond_26
    invoke-direct {p0}, Lcom/a/c/b/a/j;->g()Lcom/a/c/w;

    move-result-object v0

    .line 86
    instance-of v1, v0, Lcom/a/c/t;

    if-eqz v1, :cond_34

    .line 87
    check-cast v0, Lcom/a/c/t;

    invoke-virtual {v0, p1}, Lcom/a/c/t;->a(Lcom/a/c/w;)V

    goto :goto_1a

    .line 89
    :cond_34
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method private g()Lcom/a/c/w;
    .registers 3

    .prologue
    .line 72
    iget-object v0, p0, Lcom/a/c/b/a/j;->h:Ljava/util/List;

    iget-object v1, p0, Lcom/a/c/b/a/j;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/c/w;

    return-object v0
.end method


# virtual methods
.method public final a(D)Lcom/a/c/d/e;
    .registers 6

    .prologue
    .line 163
    .line 2242
    iget-boolean v0, p0, Lcom/a/c/d/e;->c:Z

    .line 163
    if-nez v0, :cond_25

    invoke-static {p1, p2}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_10

    invoke-static {p1, p2}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 164
    :cond_10
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "JSON forbids NaN and infinities: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 166
    :cond_25
    new-instance v0, Lcom/a/c/ac;

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/c/ac;-><init>(Ljava/lang/Number;)V

    invoke-direct {p0, v0}, Lcom/a/c/b/a/j;->a(Lcom/a/c/w;)V

    .line 167
    return-object p0
.end method

.method public final a(J)Lcom/a/c/d/e;
    .registers 6

    .prologue
    .line 171
    new-instance v0, Lcom/a/c/ac;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/c/ac;-><init>(Ljava/lang/Number;)V

    invoke-direct {p0, v0}, Lcom/a/c/b/a/j;->a(Lcom/a/c/w;)V

    .line 172
    return-object p0
.end method

.method public final a(Ljava/lang/Number;)Lcom/a/c/d/e;
    .registers 5

    .prologue
    .line 176
    if-nez p1, :cond_7

    .line 177
    invoke-virtual {p0}, Lcom/a/c/b/a/j;->f()Lcom/a/c/d/e;

    move-result-object p0

    .line 188
    :goto_6
    return-object p0

    .line 3242
    :cond_7
    iget-boolean v0, p0, Lcom/a/c/d/e;->c:Z

    .line 180
    if-nez v0, :cond_30

    .line 181
    invoke-virtual {p1}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    .line 182
    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-nez v2, :cond_1b

    invoke-static {v0, v1}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 183
    :cond_1b
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "JSON forbids NaN and infinities: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 187
    :cond_30
    new-instance v0, Lcom/a/c/ac;

    invoke-direct {v0, p1}, Lcom/a/c/ac;-><init>(Ljava/lang/Number;)V

    invoke-direct {p0, v0}, Lcom/a/c/b/a/j;->a(Lcom/a/c/w;)V

    goto :goto_6
.end method

.method public final a(Ljava/lang/String;)Lcom/a/c/d/e;
    .registers 3

    .prologue
    .line 133
    iget-object v0, p0, Lcom/a/c/b/a/j;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/a/c/b/a/j;->i:Ljava/lang/String;

    if-eqz v0, :cond_12

    .line 134
    :cond_c
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 136
    :cond_12
    invoke-direct {p0}, Lcom/a/c/b/a/j;->g()Lcom/a/c/w;

    move-result-object v0

    .line 137
    instance-of v0, v0, Lcom/a/c/z;

    if-eqz v0, :cond_1d

    .line 138
    iput-object p1, p0, Lcom/a/c/b/a/j;->i:Ljava/lang/String;

    .line 139
    return-object p0

    .line 141
    :cond_1d
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public final a(Z)Lcom/a/c/d/e;
    .registers 4

    .prologue
    .line 158
    new-instance v0, Lcom/a/c/ac;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/c/ac;-><init>(Ljava/lang/Boolean;)V

    invoke-direct {p0, v0}, Lcom/a/c/b/a/j;->a(Lcom/a/c/w;)V

    .line 159
    return-object p0
.end method

.method public final a()Lcom/a/c/w;
    .registers 4

    .prologue
    .line 65
    iget-object v0, p0, Lcom/a/c/b/a/j;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1f

    .line 66
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected one JSON element but was "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/a/c/b/a/j;->h:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 68
    :cond_1f
    iget-object v0, p0, Lcom/a/c/b/a/j;->j:Lcom/a/c/w;

    return-object v0
.end method

.method public final b()Lcom/a/c/d/e;
    .registers 3

    .prologue
    .line 95
    new-instance v0, Lcom/a/c/t;

    invoke-direct {v0}, Lcom/a/c/t;-><init>()V

    .line 96
    invoke-direct {p0, v0}, Lcom/a/c/b/a/j;->a(Lcom/a/c/w;)V

    .line 97
    iget-object v1, p0, Lcom/a/c/b/a/j;->h:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/a/c/d/e;
    .registers 3

    .prologue
    .line 145
    if-nez p1, :cond_7

    .line 146
    invoke-virtual {p0}, Lcom/a/c/b/a/j;->f()Lcom/a/c/d/e;

    move-result-object p0

    .line 149
    :goto_6
    return-object p0

    .line 148
    :cond_7
    new-instance v0, Lcom/a/c/ac;

    invoke-direct {v0, p1}, Lcom/a/c/ac;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/a/c/b/a/j;->a(Lcom/a/c/w;)V

    goto :goto_6
.end method

.method public final c()Lcom/a/c/d/e;
    .registers 3

    .prologue
    .line 102
    iget-object v0, p0, Lcom/a/c/b/a/j;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/a/c/b/a/j;->i:Ljava/lang/String;

    if-eqz v0, :cond_12

    .line 103
    :cond_c
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 105
    :cond_12
    invoke-direct {p0}, Lcom/a/c/b/a/j;->g()Lcom/a/c/w;

    move-result-object v0

    .line 106
    instance-of v0, v0, Lcom/a/c/t;

    if-eqz v0, :cond_28

    .line 107
    iget-object v0, p0, Lcom/a/c/b/a/j;->h:Ljava/util/List;

    iget-object v1, p0, Lcom/a/c/b/a/j;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 108
    return-object p0

    .line 110
    :cond_28
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public final close()V
    .registers 3

    .prologue
    .line 195
    iget-object v0, p0, Lcom/a/c/b/a/j;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_10

    .line 196
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Incomplete document"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 198
    :cond_10
    iget-object v0, p0, Lcom/a/c/b/a/j;->h:Ljava/util/List;

    sget-object v1, Lcom/a/c/b/a/j;->g:Lcom/a/c/ac;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 199
    return-void
.end method

.method public final d()Lcom/a/c/d/e;
    .registers 3

    .prologue
    .line 114
    new-instance v0, Lcom/a/c/z;

    invoke-direct {v0}, Lcom/a/c/z;-><init>()V

    .line 115
    invoke-direct {p0, v0}, Lcom/a/c/b/a/j;->a(Lcom/a/c/w;)V

    .line 116
    iget-object v1, p0, Lcom/a/c/b/a/j;->h:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 117
    return-object p0
.end method

.method public final e()Lcom/a/c/d/e;
    .registers 3

    .prologue
    .line 121
    iget-object v0, p0, Lcom/a/c/b/a/j;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/a/c/b/a/j;->i:Ljava/lang/String;

    if-eqz v0, :cond_12

    .line 122
    :cond_c
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 124
    :cond_12
    invoke-direct {p0}, Lcom/a/c/b/a/j;->g()Lcom/a/c/w;

    move-result-object v0

    .line 125
    instance-of v0, v0, Lcom/a/c/z;

    if-eqz v0, :cond_28

    .line 126
    iget-object v0, p0, Lcom/a/c/b/a/j;->h:Ljava/util/List;

    iget-object v1, p0, Lcom/a/c/b/a/j;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 127
    return-object p0

    .line 129
    :cond_28
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public final f()Lcom/a/c/d/e;
    .registers 2

    .prologue
    .line 153
    sget-object v0, Lcom/a/c/y;->a:Lcom/a/c/y;

    invoke-direct {p0, v0}, Lcom/a/c/b/a/j;->a(Lcom/a/c/w;)V

    .line 154
    return-object p0
.end method

.method public final flush()V
    .registers 1

    .prologue
    .line 192
    return-void
.end method
