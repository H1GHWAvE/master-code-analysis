.class public final Lcom/a/c/b/a/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/c/ap;


# instance fields
.field private final a:Lcom/a/c/b/f;

.field private final b:Z


# direct methods
.method public constructor <init>(Lcom/a/c/b/f;Z)V
    .registers 3

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    iput-object p1, p0, Lcom/a/c/b/a/l;->a:Lcom/a/c/b/f;

    .line 112
    iput-boolean p2, p0, Lcom/a/c/b/a/l;->b:Z

    .line 113
    return-void
.end method

.method private static a(Lcom/a/c/k;Ljava/lang/reflect/Type;)Lcom/a/c/an;
    .registers 3

    .prologue
    .line 140
    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    if-eq p1, v0, :cond_8

    const-class v0, Ljava/lang/Boolean;

    if-ne p1, v0, :cond_b

    :cond_8
    sget-object v0, Lcom/a/c/b/a/z;->f:Lcom/a/c/an;

    .line 142
    :goto_a
    return-object v0

    :cond_b
    invoke-static {p1}, Lcom/a/c/c/a;->a(Ljava/lang/reflect/Type;)Lcom/a/c/c/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/a/c/k;->a(Lcom/a/c/c/a;)Lcom/a/c/an;

    move-result-object v0

    goto :goto_a
.end method

.method static synthetic a(Lcom/a/c/b/a/l;)Z
    .registers 2

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/a/c/b/a/l;->b:Z

    return v0
.end method


# virtual methods
.method public final a(Lcom/a/c/k;Lcom/a/c/c/a;)Lcom/a/c/an;
    .registers 11

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 116
    .line 1101
    iget-object v0, p2, Lcom/a/c/c/a;->b:Ljava/lang/reflect/Type;

    .line 2094
    iget-object v1, p2, Lcom/a/c/c/a;->a:Ljava/lang/Class;

    .line 119
    const-class v2, Ljava/util/Map;

    invoke-virtual {v2, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_10

    .line 120
    const/4 v0, 0x0

    .line 133
    :goto_f
    return-object v0

    .line 123
    :cond_10
    invoke-static {v0}, Lcom/a/c/b/b;->b(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v1

    .line 124
    invoke-static {v0, v1}, Lcom/a/c/b/b;->b(Ljava/lang/reflect/Type;Ljava/lang/Class;)[Ljava/lang/reflect/Type;

    move-result-object v1

    .line 125
    aget-object v0, v1, v3

    .line 2140
    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    if-eq v0, v2, :cond_22

    const-class v2, Ljava/lang/Boolean;

    if-ne v0, v2, :cond_40

    :cond_22
    sget-object v4, Lcom/a/c/b/a/z;->f:Lcom/a/c/an;

    .line 126
    :goto_24
    aget-object v0, v1, v5

    invoke-static {v0}, Lcom/a/c/c/a;->a(Ljava/lang/reflect/Type;)Lcom/a/c/c/a;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/a/c/k;->a(Lcom/a/c/c/a;)Lcom/a/c/an;

    move-result-object v6

    .line 127
    iget-object v0, p0, Lcom/a/c/b/a/l;->a:Lcom/a/c/b/f;

    invoke-virtual {v0, p2}, Lcom/a/c/b/f;->a(Lcom/a/c/c/a;)Lcom/a/c/b/ao;

    move-result-object v7

    .line 131
    new-instance v0, Lcom/a/c/b/a/m;

    aget-object v3, v1, v3

    aget-object v5, v1, v5

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v7}, Lcom/a/c/b/a/m;-><init>(Lcom/a/c/b/a/l;Lcom/a/c/k;Ljava/lang/reflect/Type;Lcom/a/c/an;Ljava/lang/reflect/Type;Lcom/a/c/an;Lcom/a/c/b/ao;)V

    goto :goto_f

    .line 2142
    :cond_40
    invoke-static {v0}, Lcom/a/c/c/a;->a(Ljava/lang/reflect/Type;)Lcom/a/c/c/a;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/a/c/k;->a(Lcom/a/c/c/a;)Lcom/a/c/an;

    move-result-object v4

    goto :goto_24
.end method
