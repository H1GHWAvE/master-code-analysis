.class final Lcom/a/c/b/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/a/c/b/ao;


# instance fields
.field final synthetic a:Ljava/lang/Class;

.field final synthetic b:Ljava/lang/reflect/Type;

.field final synthetic c:Lcom/a/c/b/f;

.field private final d:Lcom/a/c/b/au;


# direct methods
.method constructor <init>(Lcom/a/c/b/f;Ljava/lang/Class;Ljava/lang/reflect/Type;)V
    .registers 5

    .prologue
    .line 202
    iput-object p1, p0, Lcom/a/c/b/j;->c:Lcom/a/c/b/f;

    iput-object p2, p0, Lcom/a/c/b/j;->a:Ljava/lang/Class;

    iput-object p3, p0, Lcom/a/c/b/j;->b:Ljava/lang/reflect/Type;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 203
    invoke-static {}, Lcom/a/c/b/au;->a()Lcom/a/c/b/au;

    move-result-object v0

    iput-object v0, p0, Lcom/a/c/b/j;->d:Lcom/a/c/b/au;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .registers 5

    .prologue
    .line 207
    :try_start_0
    iget-object v0, p0, Lcom/a/c/b/j;->d:Lcom/a/c/b/au;

    iget-object v1, p0, Lcom/a/c/b/j;->a:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Lcom/a/c/b/au;->a(Ljava/lang/Class;)Ljava/lang/Object;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_7} :catch_9

    move-result-object v0

    .line 208
    return-object v0

    .line 209
    :catch_9
    move-exception v0

    .line 210
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to invoke no-args constructor for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/a/c/b/j;->b:Ljava/lang/reflect/Type;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Register an InstanceCreator with Gson for this type may fix this problem."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
