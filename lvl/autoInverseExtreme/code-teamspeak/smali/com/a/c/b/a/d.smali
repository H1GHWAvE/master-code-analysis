.class final Lcom/a/c/b/a/d;
.super Lcom/a/c/an;
.source "SourceFile"


# instance fields
.field private final a:Lcom/a/c/an;

.field private final b:Lcom/a/c/b/ao;


# direct methods
.method public constructor <init>(Lcom/a/c/k;Ljava/lang/reflect/Type;Lcom/a/c/an;Lcom/a/c/b/ao;)V
    .registers 6

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/a/c/an;-><init>()V

    .line 67
    new-instance v0, Lcom/a/c/b/a/y;

    invoke-direct {v0, p1, p3, p2}, Lcom/a/c/b/a/y;-><init>(Lcom/a/c/k;Lcom/a/c/an;Ljava/lang/reflect/Type;)V

    iput-object v0, p0, Lcom/a/c/b/a/d;->a:Lcom/a/c/an;

    .line 69
    iput-object p4, p0, Lcom/a/c/b/a/d;->b:Lcom/a/c/b/ao;

    .line 70
    return-void
.end method

.method private a(Lcom/a/c/d/e;Ljava/util/Collection;)V
    .registers 6

    .prologue
    .line 89
    if-nez p2, :cond_6

    .line 90
    invoke-virtual {p1}, Lcom/a/c/d/e;->f()Lcom/a/c/d/e;

    .line 99
    :goto_5
    return-void

    .line 94
    :cond_6
    invoke-virtual {p1}, Lcom/a/c/d/e;->b()Lcom/a/c/d/e;

    .line 95
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_d
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1d

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 96
    iget-object v2, p0, Lcom/a/c/b/a/d;->a:Lcom/a/c/an;

    invoke-virtual {v2, p1, v1}, Lcom/a/c/an;->a(Lcom/a/c/d/e;Ljava/lang/Object;)V

    goto :goto_d

    .line 98
    :cond_1d
    invoke-virtual {p1}, Lcom/a/c/d/e;->c()Lcom/a/c/d/e;

    goto :goto_5
.end method

.method private b(Lcom/a/c/d/a;)Ljava/util/Collection;
    .registers 4

    .prologue
    .line 73
    invoke-virtual {p1}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v0

    sget-object v1, Lcom/a/c/d/d;->i:Lcom/a/c/d/d;

    if-ne v0, v1, :cond_d

    .line 74
    invoke-virtual {p1}, Lcom/a/c/d/a;->k()V

    .line 75
    const/4 v0, 0x0

    .line 85
    :goto_c
    return-object v0

    .line 78
    :cond_d
    iget-object v0, p0, Lcom/a/c/b/a/d;->b:Lcom/a/c/b/ao;

    invoke-interface {v0}, Lcom/a/c/b/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 79
    invoke-virtual {p1}, Lcom/a/c/d/a;->a()V

    .line 80
    :goto_18
    invoke-virtual {p1}, Lcom/a/c/d/a;->e()Z

    move-result v1

    if-eqz v1, :cond_28

    .line 81
    iget-object v1, p0, Lcom/a/c/b/a/d;->a:Lcom/a/c/an;

    invoke-virtual {v1, p1}, Lcom/a/c/an;->a(Lcom/a/c/d/a;)Ljava/lang/Object;

    move-result-object v1

    .line 82
    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_18

    .line 84
    :cond_28
    invoke-virtual {p1}, Lcom/a/c/d/a;->b()V

    goto :goto_c
.end method


# virtual methods
.method public final synthetic a(Lcom/a/c/d/a;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 60
    .line 1073
    invoke-virtual {p1}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v0

    sget-object v1, Lcom/a/c/d/d;->i:Lcom/a/c/d/d;

    if-ne v0, v1, :cond_d

    .line 1074
    invoke-virtual {p1}, Lcom/a/c/d/a;->k()V

    .line 1075
    const/4 v0, 0x0

    :goto_c
    return-object v0

    .line 1078
    :cond_d
    iget-object v0, p0, Lcom/a/c/b/a/d;->b:Lcom/a/c/b/ao;

    invoke-interface {v0}, Lcom/a/c/b/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 1079
    invoke-virtual {p1}, Lcom/a/c/d/a;->a()V

    .line 1080
    :goto_18
    invoke-virtual {p1}, Lcom/a/c/d/a;->e()Z

    move-result v1

    if-eqz v1, :cond_28

    .line 1081
    iget-object v1, p0, Lcom/a/c/b/a/d;->a:Lcom/a/c/an;

    invoke-virtual {v1, p1}, Lcom/a/c/an;->a(Lcom/a/c/d/a;)Ljava/lang/Object;

    move-result-object v1

    .line 1082
    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_18

    .line 1084
    :cond_28
    invoke-virtual {p1}, Lcom/a/c/d/a;->b()V

    goto :goto_c
.end method

.method public final synthetic a(Lcom/a/c/d/e;Ljava/lang/Object;)V
    .registers 6

    .prologue
    .line 60
    check-cast p2, Ljava/util/Collection;

    .line 1089
    if-nez p2, :cond_8

    .line 1090
    invoke-virtual {p1}, Lcom/a/c/d/e;->f()Lcom/a/c/d/e;

    .line 1091
    :goto_7
    return-void

    .line 1094
    :cond_8
    invoke-virtual {p1}, Lcom/a/c/d/e;->b()Lcom/a/c/d/e;

    .line 1095
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_f
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1f

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 1096
    iget-object v2, p0, Lcom/a/c/b/a/d;->a:Lcom/a/c/an;

    invoke-virtual {v2, p1, v1}, Lcom/a/c/an;->a(Lcom/a/c/d/e;Ljava/lang/Object;)V

    goto :goto_f

    .line 1098
    :cond_1f
    invoke-virtual {p1}, Lcom/a/c/d/e;->c()Lcom/a/c/d/e;

    goto :goto_7
.end method
