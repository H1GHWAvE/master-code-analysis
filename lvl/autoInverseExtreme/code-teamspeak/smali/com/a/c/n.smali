.class final Lcom/a/c/n;
.super Lcom/a/c/an;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/a/c/k;


# direct methods
.method constructor <init>(Lcom/a/c/k;)V
    .registers 2

    .prologue
    .line 252
    iput-object p1, p0, Lcom/a/c/n;->a:Lcom/a/c/k;

    invoke-direct {p0}, Lcom/a/c/an;-><init>()V

    return-void
.end method

.method private static a(Lcom/a/c/d/e;Ljava/lang/Number;)V
    .registers 4

    .prologue
    .line 261
    if-nez p1, :cond_6

    .line 262
    invoke-virtual {p0}, Lcom/a/c/d/e;->f()Lcom/a/c/d/e;

    .line 268
    :goto_5
    return-void

    .line 265
    :cond_6
    invoke-virtual {p1}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    .line 266
    invoke-static {v0, v1}, Lcom/a/c/k;->a(D)V

    .line 267
    invoke-virtual {p0, p1}, Lcom/a/c/d/e;->a(Ljava/lang/Number;)Lcom/a/c/d/e;

    goto :goto_5
.end method

.method private static b(Lcom/a/c/d/a;)Ljava/lang/Double;
    .registers 3

    .prologue
    .line 254
    invoke-virtual {p0}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v0

    sget-object v1, Lcom/a/c/d/d;->i:Lcom/a/c/d/d;

    if-ne v0, v1, :cond_d

    .line 255
    invoke-virtual {p0}, Lcom/a/c/d/a;->k()V

    .line 256
    const/4 v0, 0x0

    .line 258
    :goto_c
    return-object v0

    :cond_d
    invoke-virtual {p0}, Lcom/a/c/d/a;->l()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_c
.end method


# virtual methods
.method public final synthetic a(Lcom/a/c/d/a;)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 252
    .line 1254
    invoke-virtual {p1}, Lcom/a/c/d/a;->f()Lcom/a/c/d/d;

    move-result-object v0

    sget-object v1, Lcom/a/c/d/d;->i:Lcom/a/c/d/d;

    if-ne v0, v1, :cond_d

    .line 1255
    invoke-virtual {p1}, Lcom/a/c/d/a;->k()V

    .line 1256
    const/4 v0, 0x0

    :goto_c
    return-object v0

    .line 1258
    :cond_d
    invoke-virtual {p1}, Lcom/a/c/d/a;->l()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_c
.end method

.method public final synthetic a(Lcom/a/c/d/e;Ljava/lang/Object;)V
    .registers 5

    .prologue
    .line 252
    check-cast p2, Ljava/lang/Number;

    .line 1261
    if-nez p2, :cond_8

    .line 1262
    invoke-virtual {p1}, Lcom/a/c/d/e;->f()Lcom/a/c/d/e;

    .line 1263
    :goto_7
    return-void

    .line 1265
    :cond_8
    invoke-virtual {p2}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    .line 1266
    invoke-static {v0, v1}, Lcom/a/c/k;->a(D)V

    .line 1267
    invoke-virtual {p1, p2}, Lcom/a/c/d/e;->a(Ljava/lang/Number;)Lcom/a/c/d/e;

    goto :goto_7
.end method
