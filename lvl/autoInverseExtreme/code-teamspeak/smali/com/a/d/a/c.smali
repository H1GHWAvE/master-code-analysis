.class final Lcom/a/d/a/c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/a/b/a/b;
.end annotation


# static fields
.field private static final a:Lcom/a/b/b/bv;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 32
    const-string v0, ""

    invoke-static {v0}, Lcom/a/b/b/bv;->a(Ljava/lang/String;)Lcom/a/b/b/bv;

    move-result-object v0

    sput-object v0, Lcom/a/d/a/c;->a:Lcom/a/b/b/bv;

    return-void
.end method

.method constructor <init>()V
    .registers 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/util/List;Ljava/lang/CharSequence;Lcom/a/b/d/ju;)I
    .registers 14

    .prologue
    .line 65
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v4

    .line 66
    const/4 v1, 0x0

    .line 67
    const/4 v0, 0x0

    move v3, v1

    .line 70
    :goto_7
    if-ge v3, v4, :cond_25

    .line 71
    invoke-interface {p1, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 72
    const/16 v1, 0x26

    if-eq v0, v1, :cond_25

    const/16 v1, 0x3f

    if-eq v0, v1, :cond_25

    const/16 v1, 0x21

    if-eq v0, v1, :cond_25

    const/16 v1, 0x3a

    if-eq v0, v1, :cond_25

    const/16 v1, 0x2c

    if-eq v0, v1, :cond_25

    .line 70
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_7

    .line 77
    :cond_25
    const/4 v5, 0x0

    const/4 v1, 0x0

    invoke-interface {p1, v1, v3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    .line 2112
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v6

    .line 2113
    const/4 v2, 0x1

    if-gt v6, v2, :cond_85

    .line 77
    :goto_32
    invoke-interface {p0, v5, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 79
    const/16 v1, 0x21

    if-eq v0, v1, :cond_45

    const/16 v1, 0x3f

    if-eq v0, v1, :cond_45

    const/16 v1, 0x3a

    if-eq v0, v1, :cond_45

    const/16 v1, 0x2c

    if-ne v0, v1, :cond_58

    .line 84
    :cond_45
    sget-object v1, Lcom/a/d/a/c;->a:Lcom/a/b/b/bv;

    invoke-virtual {v1, p0}, Lcom/a/b/b/bv;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    .line 85
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_58

    .line 86
    invoke-static {v0}, Lcom/a/d/a/b;->a(C)Lcom/a/d/a/b;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, Lcom/a/b/d/ju;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;

    .line 89
    :cond_58
    add-int/lit8 v1, v3, 0x1

    .line 91
    const/16 v2, 0x3f

    if-eq v0, v2, :cond_bc

    const/16 v2, 0x2c

    if-eq v0, v2, :cond_bc

    move v0, v1

    .line 92
    :cond_63
    if-ge v0, v4, :cond_80

    .line 94
    invoke-interface {p1, v0, v4}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {p0, v1, p2}, Lcom/a/d/a/c;->a(Ljava/util/List;Ljava/lang/CharSequence;Lcom/a/b/d/ju;)I

    move-result v1

    add-int/2addr v0, v1

    .line 95
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    const/16 v2, 0x3f

    if-eq v1, v2, :cond_7e

    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    const/16 v2, 0x2c

    if-ne v1, v2, :cond_63

    .line 97
    :cond_7e
    add-int/lit8 v0, v0, 0x1

    .line 102
    :cond_80
    :goto_80
    const/4 v1, 0x0

    invoke-interface {p0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 103
    return v0

    .line 2117
    :cond_85
    new-array v7, v6, [C

    .line 2118
    const/4 v2, 0x0

    add-int/lit8 v8, v6, -0x1

    invoke-interface {v1, v8}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v8

    aput-char v8, v7, v2

    .line 2120
    const/4 v2, 0x1

    :goto_91
    if-ge v2, v6, :cond_b5

    .line 2121
    add-int/lit8 v8, v6, -0x1

    sub-int/2addr v8, v2

    invoke-interface {v1, v8}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v8

    aput-char v8, v7, v2

    .line 2122
    aget-char v8, v7, v2

    add-int/lit8 v9, v2, -0x1

    aget-char v9, v7, v9

    invoke-static {v8, v9}, Ljava/lang/Character;->isSurrogatePair(CC)Z

    move-result v8

    if-eqz v8, :cond_b2

    .line 2123
    add-int/lit8 v8, v2, -0x1

    .line 2131
    aget-char v9, v7, v8

    .line 2132
    aget-char v10, v7, v2

    aput-char v10, v7, v8

    .line 2133
    aput-char v9, v7, v2

    .line 2120
    :cond_b2
    add-int/lit8 v2, v2, 0x1

    goto :goto_91

    .line 2127
    :cond_b5
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v7}, Ljava/lang/String;-><init>([C)V

    goto/16 :goto_32

    :cond_bc
    move v0, v1

    goto :goto_80
.end method

.method static a(Ljava/lang/CharSequence;)Lcom/a/b/d/jt;
    .registers 6

    .prologue
    .line 39
    invoke-static {}, Lcom/a/b/d/jt;->l()Lcom/a/b/d/ju;

    move-result-object v1

    .line 40
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    .line 41
    const/4 v0, 0x0

    .line 42
    :goto_9
    if-ge v0, v2, :cond_1a

    .line 1232
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 43
    invoke-interface {p0, v0, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v3, v4, v1}, Lcom/a/d/a/c;->a(Ljava/util/List;Ljava/lang/CharSequence;Lcom/a/b/d/ju;)I

    move-result v3

    add-int/2addr v0, v3

    goto :goto_9

    .line 48
    :cond_1a
    invoke-virtual {v1}, Lcom/a/b/d/ju;->a()Lcom/a/b/d/jt;

    move-result-object v0

    return-object v0
.end method

.method private static a([CII)V
    .registers 5

    .prologue
    .line 131
    aget-char v0, p0, p1

    .line 132
    aget-char v1, p0, p2

    aput-char v1, p0, p1

    .line 133
    aput-char v0, p0, p2

    .line 134
    return-void
.end method

.method private static b(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .registers 7

    .prologue
    const/4 v0, 0x1

    .line 112
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 113
    if-gt v1, v0, :cond_8

    .line 127
    :goto_7
    return-object p0

    .line 117
    :cond_8
    new-array v2, v1, [C

    .line 118
    const/4 v3, 0x0

    add-int/lit8 v4, v1, -0x1

    invoke-interface {p0, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    aput-char v4, v2, v3

    .line 120
    :goto_13
    if-ge v0, v1, :cond_37

    .line 121
    add-int/lit8 v3, v1, -0x1

    sub-int/2addr v3, v0

    invoke-interface {p0, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    aput-char v3, v2, v0

    .line 122
    aget-char v3, v2, v0

    add-int/lit8 v4, v0, -0x1

    aget-char v4, v2, v4

    invoke-static {v3, v4}, Ljava/lang/Character;->isSurrogatePair(CC)Z

    move-result v3

    if-eqz v3, :cond_34

    .line 123
    add-int/lit8 v3, v0, -0x1

    .line 3131
    aget-char v4, v2, v3

    .line 3132
    aget-char v5, v2, v0

    aput-char v5, v2, v3

    .line 3133
    aput-char v4, v2, v0

    .line 120
    :cond_34
    add-int/lit8 v0, v0, 0x1

    goto :goto_13

    .line 127
    :cond_37
    new-instance p0, Ljava/lang/String;

    invoke-direct {p0, v2}, Ljava/lang/String;-><init>([C)V

    goto :goto_7
.end method
