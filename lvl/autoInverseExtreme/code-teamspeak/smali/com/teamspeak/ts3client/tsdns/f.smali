.class public final Lcom/teamspeak/ts3client/tsdns/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/tsdns/k;


# static fields
.field public static a:I

.field public static b:I

.field public static c:I

.field public static d:I

.field public static e:I

.field public static f:I

.field public static g:I

.field public static h:I


# instance fields
.field private i:Z

.field private j:Ljava/lang/String;

.field private k:Lcom/teamspeak/ts3client/tsdns/i;

.field private l:I

.field private m:Ljava/util/ArrayList;

.field private n:Ljava/util/BitSet;

.field private o:Ljava/util/Vector;

.field private p:Lcom/teamspeak/ts3client/tsdns/h;

.field private q:Z

.field private r:J

.field private s:Ljava/util/logging/Logger;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 16
    sput v0, Lcom/teamspeak/ts3client/tsdns/f;->a:I

    .line 17
    sput v1, Lcom/teamspeak/ts3client/tsdns/f;->b:I

    .line 18
    sput v2, Lcom/teamspeak/ts3client/tsdns/f;->c:I

    .line 19
    sput v3, Lcom/teamspeak/ts3client/tsdns/f;->d:I

    .line 21
    sput v0, Lcom/teamspeak/ts3client/tsdns/f;->e:I

    .line 22
    sput v1, Lcom/teamspeak/ts3client/tsdns/f;->f:I

    .line 23
    sput v2, Lcom/teamspeak/ts3client/tsdns/f;->g:I

    .line 24
    sput v3, Lcom/teamspeak/ts3client/tsdns/f;->h:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILcom/teamspeak/ts3client/tsdns/i;ZLjava/util/logging/Logger;)V
    .registers 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->i:Z

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->j:Ljava/lang/String;

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->m:Ljava/util/ArrayList;

    .line 32
    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->n:Ljava/util/BitSet;

    .line 33
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->o:Ljava/util/Vector;

    .line 37
    const-wide/16 v0, 0xdac

    iput-wide v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->r:J

    .line 42
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->j:Ljava/lang/String;

    .line 43
    iput p2, p0, Lcom/teamspeak/ts3client/tsdns/f;->l:I

    .line 44
    iput-object p3, p0, Lcom/teamspeak/ts3client/tsdns/f;->k:Lcom/teamspeak/ts3client/tsdns/i;

    .line 45
    iput-object p5, p0, Lcom/teamspeak/ts3client/tsdns/f;->s:Ljava/util/logging/Logger;

    .line 46
    iput-boolean p4, p0, Lcom/teamspeak/ts3client/tsdns/f;->q:Z

    .line 47
    if-eqz p4, :cond_37

    .line 48
    const-wide/16 v0, 0x1b58

    iput-wide v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->r:J

    .line 49
    :cond_37
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/tsdns/f;)J
    .registers 3

    .prologue
    .line 14
    iget-wide v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->r:J

    return-wide v0
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/tsdns/f;Lcom/teamspeak/ts3client/tsdns/h;)V
    .registers 2

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/tsdns/f;->b(Lcom/teamspeak/ts3client/tsdns/h;)V

    return-void
.end method

.method private static a(Ljava/util/ArrayList;)V
    .registers 4

    .prologue
    .line 126
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 127
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_4

    .line 128
    :cond_16
    return-void
.end method

.method private b(Lcom/teamspeak/ts3client/tsdns/h;)V
    .registers 6

    .prologue
    .line 121
    iget-object v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->s:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "PUBLISH:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1043
    iget v3, p1, Lcom/teamspeak/ts3client/tsdns/h;->d:I

    .line 121
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2019
    iget v3, p1, Lcom/teamspeak/ts3client/tsdns/h;->b:I

    .line 121
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2035
    iget-object v3, p1, Lcom/teamspeak/ts3client/tsdns/h;->a:Ljava/lang/String;

    .line 121
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 3027
    iget v3, p1, Lcom/teamspeak/ts3client/tsdns/h;->c:I

    .line 121
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->k:Lcom/teamspeak/ts3client/tsdns/i;

    invoke-interface {v0, p1}, Lcom/teamspeak/ts3client/tsdns/i;->a(Lcom/teamspeak/ts3client/tsdns/h;)V

    .line 123
    return-void
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/tsdns/f;)Z
    .registers 2

    .prologue
    .line 14
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->i:Z

    return v0
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/tsdns/f;)Z
    .registers 2

    .prologue
    .line 14
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->i:Z

    return v0
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/tsdns/f;)Lcom/teamspeak/ts3client/tsdns/h;
    .registers 2

    .prologue
    .line 14
    iget-object v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->p:Lcom/teamspeak/ts3client/tsdns/h;

    return-object v0
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/tsdns/f;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 14
    iget-object v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/tsdns/f;)I
    .registers 2

    .prologue
    .line 14
    iget v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->l:I

    return v0
.end method


# virtual methods
.method public final a()V
    .registers 15

    .prologue
    const/4 v3, -0x1

    const/4 v5, 0x0

    .line 52
    iget-object v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->j:Ljava/lang/String;

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_14

    iget-object v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->j:Ljava/lang/String;

    invoke-static {v0}, Lorg/xbill/DNS/Address;->isDottedQuad(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 53
    :cond_14
    new-instance v0, Lcom/teamspeak/ts3client/tsdns/h;

    iget-object v1, p0, Lcom/teamspeak/ts3client/tsdns/f;->j:Ljava/lang/String;

    iget v2, p0, Lcom/teamspeak/ts3client/tsdns/f;->l:I

    sget v4, Lcom/teamspeak/ts3client/tsdns/f;->a:I

    const/4 v6, 0x1

    move v5, v3

    invoke-direct/range {v0 .. v6}, Lcom/teamspeak/ts3client/tsdns/h;-><init>(Ljava/lang/String;IIIIZ)V

    invoke-direct {p0, v0}, Lcom/teamspeak/ts3client/tsdns/f;->b(Lcom/teamspeak/ts3client/tsdns/h;)V

    .line 118
    :goto_24
    return-void

    .line 57
    :cond_25
    iget-object v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->m:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/teamspeak/ts3client/tsdns/f;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 58
    new-instance v3, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->j:Ljava/lang/String;

    const-string v1, "\\."

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 59
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    sget v1, Lcom/teamspeak/ts3client/tsdns/f;->d:I

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    .line 60
    if-gez v0, :cond_147

    move v1, v5

    .line 62
    :goto_49
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_84

    .line 63
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move v2, v1

    .line 64
    :goto_57
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_77

    .line 65
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v2, v0, :cond_73

    .line 67
    const-string v0, "."

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    :cond_73
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_57

    .line 69
    :cond_77
    iget-object v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->m:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 62
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_49

    .line 75
    :cond_84
    new-instance v13, Ljava/lang/Thread;

    new-instance v0, Lcom/teamspeak/ts3client/tsdns/g;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/tsdns/g;-><init>(Lcom/teamspeak/ts3client/tsdns/f;)V

    invoke-direct {v13, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 93
    iget-object v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->n:Ljava/util/BitSet;

    invoke-virtual {v0, v5}, Ljava/util/BitSet;->set(I)V

    .line 94
    iget-object v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->n:Ljava/util/BitSet;

    sget v1, Lcom/teamspeak/ts3client/tsdns/f;->d:I

    mul-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 96
    iget-object v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->m:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 97
    iget-object v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    move v0, v5

    :goto_aa
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 98
    iget-object v2, p0, Lcom/teamspeak/ts3client/tsdns/f;->n:Ljava/util/BitSet;

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 99
    iget-object v2, p0, Lcom/teamspeak/ts3client/tsdns/f;->n:Ljava/util/BitSet;

    sget v3, Lcom/teamspeak/ts3client/tsdns/f;->d:I

    add-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 100
    add-int/lit8 v0, v0, 0x1

    .line 101
    goto :goto_aa

    .line 103
    :cond_c7
    new-instance v0, Lcom/teamspeak/ts3client/tsdns/j;

    iget-object v1, p0, Lcom/teamspeak/ts3client/tsdns/f;->j:Ljava/lang/String;

    iget-object v2, p0, Lcom/teamspeak/ts3client/tsdns/f;->j:Ljava/lang/String;

    iget v3, p0, Lcom/teamspeak/ts3client/tsdns/f;->l:I

    sget v4, Lcom/teamspeak/ts3client/tsdns/f;->h:I

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/teamspeak/ts3client/tsdns/j;-><init>(Ljava/lang/String;Ljava/lang/String;IIILcom/teamspeak/ts3client/tsdns/k;)V

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/tsdns/j;->start()V

    .line 104
    new-instance v6, Lcom/teamspeak/ts3client/tsdns/j;

    iget-object v7, p0, Lcom/teamspeak/ts3client/tsdns/f;->j:Ljava/lang/String;

    iget-object v8, p0, Lcom/teamspeak/ts3client/tsdns/f;->j:Ljava/lang/String;

    iget v9, p0, Lcom/teamspeak/ts3client/tsdns/f;->l:I

    sget v10, Lcom/teamspeak/ts3client/tsdns/f;->e:I

    sget v0, Lcom/teamspeak/ts3client/tsdns/f;->d:I

    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v11, v0, 0x3

    move-object v12, p0

    invoke-direct/range {v6 .. v12}, Lcom/teamspeak/ts3client/tsdns/j;-><init>(Ljava/lang/String;Ljava/lang/String;IIILcom/teamspeak/ts3client/tsdns/k;)V

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/tsdns/j;->start()V

    .line 107
    iget-object v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    move v0, v5

    :goto_f6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_11a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 108
    new-instance v6, Lcom/teamspeak/ts3client/tsdns/j;

    iget-object v8, p0, Lcom/teamspeak/ts3client/tsdns/f;->j:Ljava/lang/String;

    iget v9, p0, Lcom/teamspeak/ts3client/tsdns/f;->l:I

    sget v10, Lcom/teamspeak/ts3client/tsdns/f;->f:I

    sget v2, Lcom/teamspeak/ts3client/tsdns/f;->d:I

    add-int/lit8 v2, v2, 0x2

    add-int v11, v2, v0

    move-object v12, p0

    invoke-direct/range {v6 .. v12}, Lcom/teamspeak/ts3client/tsdns/j;-><init>(Ljava/lang/String;Ljava/lang/String;IIILcom/teamspeak/ts3client/tsdns/k;)V

    invoke-virtual {v6}, Lcom/teamspeak/ts3client/tsdns/j;->start()V

    .line 109
    add-int/lit8 v0, v0, 0x1

    .line 110
    goto :goto_f6

    .line 112
    :cond_11a
    iget-object v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v7, v5

    :goto_121
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_142

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 113
    new-instance v0, Lcom/teamspeak/ts3client/tsdns/j;

    iget-object v1, p0, Lcom/teamspeak/ts3client/tsdns/f;->j:Ljava/lang/String;

    iget v3, p0, Lcom/teamspeak/ts3client/tsdns/f;->l:I

    sget v4, Lcom/teamspeak/ts3client/tsdns/f;->g:I

    add-int/lit8 v5, v7, 0x1

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/teamspeak/ts3client/tsdns/j;-><init>(Ljava/lang/String;Ljava/lang/String;IIILcom/teamspeak/ts3client/tsdns/k;)V

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/tsdns/j;->start()V

    .line 114
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    .line 115
    goto :goto_121

    .line 117
    :cond_142
    invoke-virtual {v13}, Ljava/lang/Thread;->start()V

    goto/16 :goto_24

    :cond_147
    move v1, v0

    goto/16 :goto_49
.end method

.method public final declared-synchronized a(Lcom/teamspeak/ts3client/tsdns/h;)V
    .registers 9

    .prologue
    const/4 v2, -0x1

    .line 132
    monitor-enter p0

    :try_start_2
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->i:Z
    :try_end_4
    .catchall {:try_start_2 .. :try_end_4} :catchall_4c

    if-eqz v0, :cond_8

    .line 153
    :cond_6
    :goto_6
    monitor-exit p0

    return-void

    .line 134
    :cond_8
    :try_start_8
    iget-object v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->n:Ljava/util/BitSet;

    .line 4019
    iget v1, p1, Lcom/teamspeak/ts3client/tsdns/h;->b:I

    .line 134
    invoke-virtual {v0, v1}, Ljava/util/BitSet;->clear(I)V

    .line 4043
    iget v0, p1, Lcom/teamspeak/ts3client/tsdns/h;->d:I

    .line 135
    sget v1, Lcom/teamspeak/ts3client/tsdns/f;->a:I

    if-ne v0, v1, :cond_1b

    .line 136
    iget-object v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->p:Lcom/teamspeak/ts3client/tsdns/h;

    if-nez v0, :cond_4f

    .line 137
    iput-object p1, p0, Lcom/teamspeak/ts3client/tsdns/f;->p:Lcom/teamspeak/ts3client/tsdns/h;

    .line 144
    :cond_1b
    :goto_1b
    iget-object v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->n:Ljava/util/BitSet;

    .line 7019
    iget v1, p1, Lcom/teamspeak/ts3client/tsdns/h;->b:I

    .line 144
    invoke-virtual {v0, v1}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v0

    if-ne v0, v2, :cond_2b

    .line 7043
    iget v0, p1, Lcom/teamspeak/ts3client/tsdns/h;->d:I

    .line 144
    sget v1, Lcom/teamspeak/ts3client/tsdns/f;->a:I

    if-eq v0, v1, :cond_33

    :cond_2b
    iget-object v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->n:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 145
    :cond_33
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->i:Z

    .line 146
    iget-object v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->p:Lcom/teamspeak/ts3client/tsdns/h;

    if-nez v0, :cond_5a

    .line 147
    new-instance v0, Lcom/teamspeak/ts3client/tsdns/h;

    iget-object v1, p0, Lcom/teamspeak/ts3client/tsdns/f;->j:Ljava/lang/String;

    iget v2, p0, Lcom/teamspeak/ts3client/tsdns/f;->l:I

    const/4 v3, -0x1

    sget v4, Lcom/teamspeak/ts3client/tsdns/f;->c:I

    const/4 v5, -0x1

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Lcom/teamspeak/ts3client/tsdns/h;-><init>(Ljava/lang/String;IIIIZ)V

    invoke-direct {p0, v0}, Lcom/teamspeak/ts3client/tsdns/f;->b(Lcom/teamspeak/ts3client/tsdns/h;)V
    :try_end_4b
    .catchall {:try_start_8 .. :try_end_4b} :catchall_4c

    goto :goto_6

    .line 132
    :catchall_4c
    move-exception v0

    monitor-exit p0

    throw v0

    .line 138
    :cond_4f
    :try_start_4f
    iget-object v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->p:Lcom/teamspeak/ts3client/tsdns/h;

    .line 5019
    iget v0, v0, Lcom/teamspeak/ts3client/tsdns/h;->b:I

    .line 6019
    iget v1, p1, Lcom/teamspeak/ts3client/tsdns/h;->b:I

    .line 138
    if-ge v0, v1, :cond_1b

    .line 140
    iput-object p1, p0, Lcom/teamspeak/ts3client/tsdns/f;->p:Lcom/teamspeak/ts3client/tsdns/h;

    goto :goto_1b

    .line 149
    :cond_5a
    new-instance v0, Lcom/teamspeak/ts3client/tsdns/h;

    iget-object v1, p0, Lcom/teamspeak/ts3client/tsdns/f;->p:Lcom/teamspeak/ts3client/tsdns/h;

    invoke-direct {v0, v1}, Lcom/teamspeak/ts3client/tsdns/h;-><init>(Lcom/teamspeak/ts3client/tsdns/h;)V

    invoke-direct {p0, v0}, Lcom/teamspeak/ts3client/tsdns/f;->b(Lcom/teamspeak/ts3client/tsdns/h;)V
    :try_end_64
    .catchall {:try_start_4f .. :try_end_64} :catchall_4c

    goto :goto_6
.end method

.method public final a(Ljava/lang/Thread;)V
    .registers 3

    .prologue
    .line 157
    iget-object v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->o:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 158
    return-void
.end method

.method public final b()Z
    .registers 2

    .prologue
    .line 162
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->q:Z

    return v0
.end method

.method public final c()Ljava/util/logging/Logger;
    .registers 2

    .prologue
    .line 167
    iget-object v0, p0, Lcom/teamspeak/ts3client/tsdns/f;->s:Ljava/util/logging/Logger;

    return-object v0
.end method
