.class final Lcom/teamspeak/ts3client/d/ac;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/text/DecimalFormat;

.field final synthetic b:Lcom/teamspeak/ts3client/d/z;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/d/z;Ljava/text/DecimalFormat;)V
    .registers 3

    .prologue
    .line 199
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    iput-object p2, p0, Lcom/teamspeak/ts3client/d/ac;->a:Ljava/text/DecimalFormat;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 9

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 203
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/z;->d(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/z;->b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 1061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 203
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/z;->b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 2061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 203
    sget-object v4, Lcom/teamspeak/ts3client/jni/i;->b:Lcom/teamspeak/ts3client/jni/i;

    invoke-virtual {v1, v2, v3, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b(JLcom/teamspeak/ts3client/jni/i;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 204
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/z;->e(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/z;->b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 3061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3234
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 204
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/z;->b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v3

    .line 4061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 4267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 204
    sget-object v3, Lcom/teamspeak/ts3client/jni/f;->a:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v2, v4, v5, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/f;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 205
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/z;->f(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/ac;->a:Ljava/text/DecimalFormat;

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/z;->b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 5061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 5234
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 205
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/z;->b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v3

    .line 6061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 6267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 205
    sget-object v3, Lcom/teamspeak/ts3client/jni/f;->B:Lcom/teamspeak/ts3client/jni/f;

    .line 6556
    iget v3, v3, Lcom/teamspeak/ts3client/jni/f;->ap:I

    .line 6354
    invoke-virtual {v2, v4, v5, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_getServerConnectionVariableAsFloat(JI)F

    move-result v2

    .line 205
    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 207
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/z;->g(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/z;->b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 7061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 7234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 207
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/z;->b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 8061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 8267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 207
    sget-object v4, Lcom/teamspeak/ts3client/jni/f;->t:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v1, v2, v3, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/f;)J

    move-result-wide v2

    invoke-static {v2, v3, v7}, Lcom/teamspeak/ts3client/data/d/w;->a(JZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 208
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/z;->h(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/z;->b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 9061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 9234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 208
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/z;->b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 10061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 10267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 208
    sget-object v4, Lcom/teamspeak/ts3client/jni/f;->l:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v1, v2, v3, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/f;)J

    move-result-wide v2

    invoke-static {v2, v3, v7}, Lcom/teamspeak/ts3client/data/d/w;->a(JZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 210
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/z;->i(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/z;->b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 11061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 11234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 210
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/z;->b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 12061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 12267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 210
    sget-object v4, Lcom/teamspeak/ts3client/jni/f;->x:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v1, v2, v3, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/f;)J

    move-result-wide v2

    invoke-static {v2, v3, v6}, Lcom/teamspeak/ts3client/data/d/w;->a(JZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 211
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/z;->j(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/z;->b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 13061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 13234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 211
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/z;->b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 14061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 14267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 211
    sget-object v4, Lcom/teamspeak/ts3client/jni/f;->p:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v1, v2, v3, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/f;)J

    move-result-wide v2

    invoke-static {v2, v3, v6}, Lcom/teamspeak/ts3client/data/d/w;->a(JZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 213
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/z;->k(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/z;->b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 15061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 15234
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 213
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/z;->b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v3

    .line 16061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 16267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 213
    sget-object v3, Lcom/teamspeak/ts3client/jni/f;->V:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v2, v4, v5, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/f;)J

    move-result-wide v2

    invoke-static {v2, v3, v6}, Lcom/teamspeak/ts3client/data/d/w;->a(JZ)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/s"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/z;->l(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/z;->b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 17061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 17234
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 214
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/z;->b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v3

    .line 18061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 18267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 214
    sget-object v3, Lcom/teamspeak/ts3client/jni/f;->N:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v2, v4, v5, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/f;)J

    move-result-wide v2

    invoke-static {v2, v3, v6}, Lcom/teamspeak/ts3client/data/d/w;->a(JZ)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/s"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/z;->m(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/z;->b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 19061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 19234
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 216
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/z;->b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v3

    .line 20061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 20267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 216
    sget-object v3, Lcom/teamspeak/ts3client/jni/f;->Z:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v2, v4, v5, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/f;)J

    move-result-wide v2

    invoke-static {v2, v3, v6}, Lcom/teamspeak/ts3client/data/d/w;->a(JZ)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/s"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 217
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/z;->n(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/z;->b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 21061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 21234
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 217
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/z;->b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v3

    .line 22061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 22267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 217
    sget-object v3, Lcom/teamspeak/ts3client/jni/f;->R:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v2, v4, v5, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/f;)J

    move-result-wide v2

    invoke-static {v2, v3, v6}, Lcom/teamspeak/ts3client/data/d/w;->a(JZ)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/s"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 219
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/z;->o(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/z;->b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 23061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 23234
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 219
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/z;->b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v3

    .line 24061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 24267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 219
    sget-object v3, Lcom/teamspeak/ts3client/jni/f;->al:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v2, v4, v5, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/f;)J

    move-result-wide v2

    invoke-static {v2, v3, v6}, Lcom/teamspeak/ts3client/data/d/w;->a(JZ)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/s"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 220
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/z;->p(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/z;->b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 25061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 25234
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 220
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/z;->b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v3

    .line 26061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 26267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 220
    sget-object v3, Lcom/teamspeak/ts3client/jni/f;->ak:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v2, v4, v5, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/f;)J

    move-result-wide v2

    invoke-static {v2, v3, v6}, Lcom/teamspeak/ts3client/data/d/w;->a(JZ)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/s"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 222
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/z;->q(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/z;->b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 27061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 27234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 222
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/z;->b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 28061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 28267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 222
    sget-object v4, Lcom/teamspeak/ts3client/jni/f;->am:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v1, v2, v3, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/f;)J

    move-result-wide v2

    invoke-static {v2, v3, v6}, Lcom/teamspeak/ts3client/data/d/w;->a(JZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 223
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/z;->r(Lcom/teamspeak/ts3client/d/z;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/z;->b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 29061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 29234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 223
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/ac;->b:Lcom/teamspeak/ts3client/d/z;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/z;->b(Lcom/teamspeak/ts3client/d/z;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 30061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 30267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 223
    sget-object v4, Lcom/teamspeak/ts3client/jni/f;->an:Lcom/teamspeak/ts3client/jni/f;

    invoke-virtual {v1, v2, v3, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/f;)J

    move-result-wide v2

    invoke-static {v2, v3, v6}, Lcom/teamspeak/ts3client/data/d/w;->a(JZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 225
    return-void
.end method
