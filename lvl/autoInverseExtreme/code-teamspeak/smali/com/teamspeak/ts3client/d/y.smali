.class final Lcom/teamspeak/ts3client/d/y;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/d/t;


# direct methods
.method private constructor <init>(Lcom/teamspeak/ts3client/d/t;)V
    .registers 2

    .prologue
    .line 165
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/y;->a:Lcom/teamspeak/ts3client/d/t;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/teamspeak/ts3client/d/t;B)V
    .registers 3

    .prologue
    .line 165
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/d/y;-><init>(Lcom/teamspeak/ts3client/d/t;)V

    return-void
.end method

.method private static varargs a([Ljava/lang/String;)Ljava/lang/String;
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 170
    :try_start_1
    new-instance v2, Ljava/net/URL;

    const/4 v0, 0x0

    aget-object v0, p0, v0

    invoke-direct {v2, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 171
    invoke-virtual {v2}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 172
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v3

    .line 173
    invoke-virtual {v2}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getURL()Ljava/net/URL;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_26

    .line 202
    :cond_25
    :goto_25
    return-object v1

    .line 177
    :cond_26
    const-string v2, "X-TS3-SIGNATURE"

    invoke-interface {v3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_25

    .line 181
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v2

    const/16 v3, 0xc8

    if-eq v2, v3, :cond_3e

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v2

    const/16 v3, 0x130

    if-ne v2, v3, :cond_25

    .line 185
    :cond_3e
    const/16 v2, 0x3e8

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 186
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/InputStreamReader;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 187
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 189
    :goto_56
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_64

    .line 190
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_56

    .line 198
    :catch_60
    move-exception v0

    move-object v0, v1

    :goto_62
    move-object v1, v0

    .line 202
    goto :goto_25

    .line 194
    :cond_64
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 196
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_6a
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_6a} :catch_60
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_6a} :catch_6c

    move-result-object v0

    goto :goto_62

    .line 200
    :catch_6c
    move-exception v0

    move-object v0, v1

    goto :goto_62
.end method

.method private a(Ljava/lang/String;)V
    .registers 5

    .prologue
    .line 207
    if-eqz p1, :cond_21

    .line 208
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/y;->a:Lcom/teamspeak/ts3client/d/t;

    invoke-static {v0, p1}, Lcom/teamspeak/ts3client/d/t;->a(Lcom/teamspeak/ts3client/d/t;Ljava/lang/String;)V

    .line 209
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 1093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 209
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "la_url"

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/y;->a:Lcom/teamspeak/ts3client/d/t;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/t;->b(Lcom/teamspeak/ts3client/d/t;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 219
    :goto_20
    return-void

    .line 211
    :cond_21
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/y;->a:Lcom/teamspeak/ts3client/d/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/t;->e(Lcom/teamspeak/ts3client/d/t;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "en"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 212
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/y;->a:Lcom/teamspeak/ts3client/d/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/t;->f(Lcom/teamspeak/ts3client/d/t;)Lcom/teamspeak/ts3client/d/k;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/teamspeak/ts3client/d/k;->a([Ljava/lang/Object;)V

    goto :goto_20

    .line 214
    :cond_3c
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/y;->a:Lcom/teamspeak/ts3client/d/t;

    const-string v1, "en"

    invoke-static {v0, v1}, Lcom/teamspeak/ts3client/d/t;->b(Lcom/teamspeak/ts3client/d/t;Ljava/lang/String;)Ljava/lang/String;

    .line 215
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/y;->a:Lcom/teamspeak/ts3client/d/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/t;->g(Lcom/teamspeak/ts3client/d/t;)V

    goto :goto_20
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 165
    check-cast p1, [Ljava/lang/String;

    invoke-static {p1}, Lcom/teamspeak/ts3client/d/y;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 5

    .prologue
    .line 165
    check-cast p1, Ljava/lang/String;

    .line 1207
    if-eqz p1, :cond_23

    .line 1208
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/y;->a:Lcom/teamspeak/ts3client/d/t;

    invoke-static {v0, p1}, Lcom/teamspeak/ts3client/d/t;->a(Lcom/teamspeak/ts3client/d/t;Ljava/lang/String;)V

    .line 1209
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 2093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1209
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "la_url"

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/y;->a:Lcom/teamspeak/ts3client/d/t;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/t;->b(Lcom/teamspeak/ts3client/d/t;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1212
    :goto_22
    return-void

    .line 1211
    :cond_23
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/y;->a:Lcom/teamspeak/ts3client/d/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/t;->e(Lcom/teamspeak/ts3client/d/t;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "en"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 1212
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/y;->a:Lcom/teamspeak/ts3client/d/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/t;->f(Lcom/teamspeak/ts3client/d/t;)Lcom/teamspeak/ts3client/d/k;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/teamspeak/ts3client/d/k;->a([Ljava/lang/Object;)V

    goto :goto_22

    .line 1214
    :cond_3e
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/y;->a:Lcom/teamspeak/ts3client/d/t;

    const-string v1, "en"

    invoke-static {v0, v1}, Lcom/teamspeak/ts3client/d/t;->b(Lcom/teamspeak/ts3client/d/t;Ljava/lang/String;)Ljava/lang/String;

    .line 1215
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/y;->a:Lcom/teamspeak/ts3client/d/t;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/t;->g(Lcom/teamspeak/ts3client/d/t;)V

    goto :goto_22
.end method
