.class final Lcom/teamspeak/ts3client/d/ah;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/d/ae;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/d/ae;)V
    .registers 2

    .prologue
    .line 115
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/ah;->a:Lcom/teamspeak/ts3client/d/ae;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 10

    .prologue
    .line 119
    const/4 v0, 0x3

    if-ne p3, v0, :cond_43

    iget-object v0, p0, Lcom/teamspeak/ts3client/d/ah;->a:Lcom/teamspeak/ts3client/d/ae;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/ae;->l(Lcom/teamspeak/ts3client/d/ae;)Z

    move-result v0

    if-nez v0, :cond_43

    .line 120
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 121
    invoke-static {v0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/app/Dialog;)V

    .line 122
    const-string v1, "virtualservereditdialog.dialog.info"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 123
    const-string v1, "virtualservereditdialog.dialog.text"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 124
    const/4 v1, -0x1

    const-string v2, "button.ok"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/teamspeak/ts3client/d/ai;

    invoke-direct {v3, p0, v0}, Lcom/teamspeak/ts3client/d/ai;-><init>(Lcom/teamspeak/ts3client/d/ah;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 131
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 132
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 134
    :cond_43
    return-void
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .registers 2

    .prologue
    .line 140
    return-void
.end method
