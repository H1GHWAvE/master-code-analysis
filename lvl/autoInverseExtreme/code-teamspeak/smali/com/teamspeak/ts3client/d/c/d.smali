.class public final Lcom/teamspeak/ts3client/d/c/d;
.super Lcom/teamspeak/ts3client/d/c/i;
.source "SourceFile"


# instance fields
.field a:Z

.field b:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0}, Lcom/teamspeak/ts3client/d/c/i;-><init>()V

    .line 39
    iput-boolean v0, p0, Lcom/teamspeak/ts3client/d/c/d;->a:Z

    .line 40
    iput-boolean v0, p0, Lcom/teamspeak/ts3client/d/c/d;->b:Z

    return-void
.end method


# virtual methods
.method public final a()V
    .registers 1

    .prologue
    .line 122
    return-void
.end method

.method public final a(Landroid/view/Menu;Lcom/teamspeak/ts3client/Ts3Application;)V
    .registers 11

    .prologue
    const/16 v7, 0x6a

    const/16 v6, 0x67

    const/16 v5, 0x66

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 45
    const v0, 0x7f0200af

    const-string v3, "menu.settings"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x65

    invoke-static {p1, v0, v3, v4, v1}, Lcom/teamspeak/ts3client/d/c/d;->a(Landroid/view/Menu;ILjava/lang/String;II)V

    .line 1061
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 48
    if-nez v0, :cond_1b

    .line 116
    :goto_1a
    return-void

    .line 2061
    :cond_1b
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 50
    sget-object v3, Lcom/teamspeak/ts3client/jni/g;->aE:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v0, v3}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v0

    if-eqz v0, :cond_1ca

    move v0, v1

    .line 3061
    :goto_28
    iget-object v3, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3181
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 52
    sget-object v4, Lcom/teamspeak/ts3client/jni/g;->aF:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v3, v4}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v3

    if-eqz v3, :cond_35

    move v0, v1

    .line 4061
    :cond_35
    iget-object v3, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 4181
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 54
    sget-object v4, Lcom/teamspeak/ts3client/jni/g;->aG:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v3, v4}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v3

    if-eqz v3, :cond_42

    move v0, v1

    .line 5061
    :cond_42
    iget-object v3, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 5181
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 56
    sget-object v4, Lcom/teamspeak/ts3client/jni/g;->aQ:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v3, v4}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v3

    if-eqz v3, :cond_50

    .line 57
    or-int/lit8 v0, v0, 0x2

    .line 6061
    :cond_50
    iget-object v3, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 6181
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 58
    sget-object v4, Lcom/teamspeak/ts3client/jni/g;->aP:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v3, v4}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v3

    if-eqz v3, :cond_5e

    .line 59
    or-int/lit8 v0, v0, 0x2

    .line 7061
    :cond_5e
    iget-object v3, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 7181
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 60
    sget-object v4, Lcom/teamspeak/ts3client/jni/g;->aN:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v3, v4}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v3

    if-eqz v3, :cond_6c

    .line 61
    or-int/lit8 v0, v0, 0x2

    .line 8061
    :cond_6c
    iget-object v3, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 8181
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 62
    sget-object v4, Lcom/teamspeak/ts3client/jni/g;->aM:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v3, v4}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v3

    if-eqz v3, :cond_7a

    .line 63
    or-int/lit8 v0, v0, 0x2

    .line 9061
    :cond_7a
    iget-object v3, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 9181
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 64
    sget-object v4, Lcom/teamspeak/ts3client/jni/g;->aL:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v3, v4}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v3

    if-eqz v3, :cond_88

    .line 65
    or-int/lit8 v0, v0, 0x2

    .line 10061
    :cond_88
    iget-object v3, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 10181
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 66
    sget-object v4, Lcom/teamspeak/ts3client/jni/g;->aO:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v3, v4}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v3

    if-eqz v3, :cond_96

    .line 67
    or-int/lit8 v0, v0, 0x2

    .line 69
    :cond_96
    and-int/lit8 v3, v0, 0x1

    if-eqz v3, :cond_1a0

    and-int/lit8 v3, v0, 0x2

    if-eqz v3, :cond_1a0

    .line 70
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/d/c/d;->b:Z

    .line 74
    :goto_a0
    const v3, 0x7f02007b

    const-string v4, "menu.createchannel"

    invoke-static {v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v3, v4, v5, v1}, Lcom/teamspeak/ts3client/d/c/d;->a(Landroid/view/Menu;ILjava/lang/String;II)V

    .line 75
    iget-boolean v3, p0, Lcom/teamspeak/ts3client/d/c/d;->b:Z

    invoke-static {p1, v5, v3}, Lcom/teamspeak/ts3client/d/c/d;->a(Landroid/view/Menu;IZ)V

    .line 11061
    iget-object v3, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 11181
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 77
    sget-object v4, Lcom/teamspeak/ts3client/jni/g;->aD:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v3, v4}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v3

    if-eqz v3, :cond_bf

    .line 78
    or-int/lit8 v0, v0, 0x4

    .line 79
    :cond_bf
    and-int/lit8 v3, v0, 0x1

    if-eqz v3, :cond_1a4

    and-int/lit8 v3, v0, 0x2

    if-eqz v3, :cond_1a4

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1a4

    .line 80
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/d/c/d;->b:Z

    .line 84
    :goto_cd
    const v0, 0x7f02007c

    const-string v3, "menu.createsubchannel"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v0, v3, v6, v1}, Lcom/teamspeak/ts3client/d/c/d;->a(Landroid/view/Menu;ILjava/lang/String;II)V

    .line 85
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/d/c/d;->b:Z

    invoke-static {p1, v6, v0}, Lcom/teamspeak/ts3client/d/c/d;->a(Landroid/view/Menu;IZ)V

    .line 12061
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 12234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 13061
    iget-object v3, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 13267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 87
    sget-object v3, Lcom/teamspeak/ts3client/jni/d;->M:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v4, v5, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;)I

    move-result v0

    if-nez v0, :cond_1a8

    .line 88
    const v0, 0x7f02009a

    const-string v3, "menu.away.set"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x68

    invoke-static {p1, v0, v3, v4, v1}, Lcom/teamspeak/ts3client/d/c/d;->a(Landroid/view/Menu;ILjava/lang/String;II)V

    .line 14061
    :goto_fc
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 14181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 92
    sget-object v3, Lcom/teamspeak/ts3client/jni/g;->au:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v0, v3}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v0

    if-nez v0, :cond_114

    .line 15061
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 15181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 92
    sget-object v3, Lcom/teamspeak/ts3client/jni/g;->av:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v0, v3}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v0

    if-eqz v0, :cond_115

    :cond_114
    move v2, v1

    .line 94
    :cond_115
    const v0, 0x7f0200c5

    const-string v3, "menu.temporarypasswords"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v0, v3, v7, v1}, Lcom/teamspeak/ts3client/d/c/d;->a(Landroid/view/Menu;ILjava/lang/String;II)V

    .line 95
    invoke-static {p1, v7, v2}, Lcom/teamspeak/ts3client/d/c/d;->a(Landroid/view/Menu;IZ)V

    .line 97
    const v0, 0x7f020060

    const-string v2, "menu.contact"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x6b

    invoke-static {p1, v0, v2, v3, v1}, Lcom/teamspeak/ts3client/d/c/d;->a(Landroid/view/Menu;ILjava/lang/String;II)V

    .line 16061
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 99
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v0

    .line 17061
    iget-object v2, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 17214
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->g:J

    .line 99
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 18061
    :try_start_144
    iget-object v2, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 18364
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->d:Lcom/teamspeak/ts3client/data/d;

    .line 19061
    iget-object v3, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 19242
    iget v3, v3, Lcom/teamspeak/ts3client/data/e;->h:I

    .line 101
    invoke-virtual {v2, v3}, Lcom/teamspeak/ts3client/data/d;->b(I)Lcom/teamspeak/ts3client/data/c;

    move-result-object v2

    .line 20156
    iget v3, v2, Lcom/teamspeak/ts3client/data/c;->o:I

    .line 21079
    iget v0, v0, Lcom/teamspeak/ts3client/data/a;->h:I

    .line 102
    if-ge v3, v0, :cond_173

    .line 21283
    iget-boolean v0, v2, Lcom/teamspeak/ts3client/data/c;->r:Z

    .line 102
    if-nez v0, :cond_173

    .line 22164
    iget-object v0, v2, Lcom/teamspeak/ts3client/data/c;->p:Ljava/lang/String;

    .line 103
    const-string v2, "0"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b8

    .line 104
    const v0, 0x7f0200a9

    const-string v2, "menu.talkpower.request"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x6c

    const/4 v4, 0x1

    invoke-static {p1, v0, v2, v3, v4}, Lcom/teamspeak/ts3client/d/c/d;->a(Landroid/view/Menu;ILjava/lang/String;II)V
    :try_end_173
    .catch Ljava/lang/Exception; {:try_start_144 .. :try_end_173} :catch_1c8

    .line 23061
    :cond_173
    :goto_173
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 23181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 111
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->da:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v0

    .line 112
    const v2, 0x7f020046

    const-string v3, "menu.banlist"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x6e

    invoke-static {p1, v2, v3, v4, v1}, Lcom/teamspeak/ts3client/d/c/d;->a(Landroid/view/Menu;ILjava/lang/String;II)V

    .line 113
    const/16 v2, 0x6e

    invoke-static {p1, v2, v0}, Lcom/teamspeak/ts3client/d/c/d;->a(Landroid/view/Menu;IZ)V

    .line 115
    const v0, 0x7f0200c5

    const-string v2, "menu.privkey"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x6f

    invoke-static {p1, v0, v2, v3, v1}, Lcom/teamspeak/ts3client/d/c/d;->a(Landroid/view/Menu;ILjava/lang/String;II)V

    goto/16 :goto_1a

    .line 72
    :cond_1a0
    iput-boolean v2, p0, Lcom/teamspeak/ts3client/d/c/d;->b:Z

    goto/16 :goto_a0

    .line 82
    :cond_1a4
    iput-boolean v2, p0, Lcom/teamspeak/ts3client/d/c/d;->b:Z

    goto/16 :goto_cd

    .line 90
    :cond_1a8
    const v0, 0x7f02009a

    const-string v3, "menu.away.revoke"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x69

    invoke-static {p1, v0, v3, v4, v2}, Lcom/teamspeak/ts3client/d/c/d;->a(Landroid/view/Menu;ILjava/lang/String;II)V

    goto/16 :goto_fc

    .line 106
    :cond_1b8
    const v0, 0x7f0200aa

    :try_start_1bb
    const-string v2, "menu.talkpower.cancel"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x6d

    const/4 v4, 0x1

    invoke-static {p1, v0, v2, v3, v4}, Lcom/teamspeak/ts3client/d/c/d;->a(Landroid/view/Menu;ILjava/lang/String;II)V
    :try_end_1c7
    .catch Ljava/lang/Exception; {:try_start_1bb .. :try_end_1c7} :catch_1c8

    goto :goto_173

    :catch_1c8
    move-exception v0

    goto :goto_173

    :cond_1ca
    move v0, v2

    goto/16 :goto_28
.end method

.method public final a(Landroid/view/MenuItem;Lcom/teamspeak/ts3client/Ts3Application;)Z
    .registers 13

    .prologue
    .line 126
    .line 24107
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 126
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bi;->a()Landroid/support/v4/app/cd;

    move-result-object v4

    .line 127
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_3a6

    .line 331
    const/4 v0, 0x0

    :goto_12
    return v0

    .line 25107
    :pswitch_13
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 129
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v0

    const-string v1, "Settings"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bi;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_38

    .line 130
    new-instance v0, Lcom/teamspeak/ts3client/f/as;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/f/as;-><init>()V

    const-string v1, "Settings"

    invoke-virtual {v4, v0, v1}, Landroid/support/v4/app/cd;->b(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;

    .line 133
    :goto_2b
    invoke-virtual {v4}, Landroid/support/v4/app/cd;->f()Landroid/support/v4/app/cd;

    .line 134
    const/16 v0, 0x1001

    invoke-virtual {v4, v0}, Landroid/support/v4/app/cd;->a(I)Landroid/support/v4/app/cd;

    .line 135
    invoke-virtual {v4}, Landroid/support/v4/app/cd;->i()I

    .line 136
    const/4 v0, 0x1

    goto :goto_12

    .line 26107
    :cond_38
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 132
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v0

    const-string v1, "Settings"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bi;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/support/v4/app/cd;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;

    goto :goto_2b

    .line 27061
    :pswitch_48
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 27181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 138
    sget-object v1, Lcom/teamspeak/ts3client/jni/g;->aV:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v0

    .line 139
    if-eqz v0, :cond_114

    .line 140
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 28061
    iget-object v1, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 28165
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->f:Lcom/teamspeak/ts3client/data/g;

    .line 29080
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    .line 141
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 142
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 143
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6b
    :goto_6b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_87

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/a;

    .line 29087
    iget-wide v6, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 144
    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-eqz v3, :cond_6b

    .line 29095
    iget v3, v0, Lcom/teamspeak/ts3client/data/a;->f:I

    .line 146
    if-nez v3, :cond_6b

    .line 147
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6b

    .line 149
    :cond_87
    new-instance v0, Lcom/teamspeak/ts3client/data/a;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/data/a;-><init>()V

    .line 150
    const-string v2, "---"

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/data/a;->a(Ljava/lang/String;)V

    .line 30091
    const-wide/16 v2, -0x1

    iput-wide v2, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 152
    const/4 v2, 0x0

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 153
    new-instance v5, Landroid/app/Dialog;

    .line 30107
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 153
    const v2, 0x7f0700b5

    invoke-direct {v5, v0, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 154
    invoke-static {v5}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/app/Dialog;)V

    .line 155
    new-instance v6, Landroid/widget/LinearLayout;

    .line 31107
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 155
    invoke-direct {v6, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 156
    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 32107
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 158
    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/StartGUIFragment;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 159
    const v2, 0x7f030046

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 160
    const v2, 0x7f0c0190

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Spinner;

    .line 161
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v7, -0x1

    const/4 v8, -0x2

    invoke-direct {v2, v7, v8}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 163
    new-instance v2, Landroid/widget/ArrayAdapter;

    .line 33107
    iget-object v7, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 163
    const v8, 0x1090009

    invoke-direct {v2, v7, v8, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 164
    invoke-virtual {v3, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 165
    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 166
    new-instance v7, Landroid/widget/Button;

    .line 34107
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 166
    invoke-direct {v7, v0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 167
    const-string v0, "button.ok"

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 168
    new-instance v0, Lcom/teamspeak/ts3client/d/c/e;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/teamspeak/ts3client/d/c/e;-><init>(Lcom/teamspeak/ts3client/d/c/d;Landroid/widget/ArrayAdapter;Landroid/widget/Spinner;Landroid/support/v4/app/cd;Landroid/app/Dialog;)V

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 180
    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 181
    invoke-virtual {v5, v6}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 182
    const-string v0, "menu.createchannel.text"

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 183
    invoke-virtual {v5}, Landroid/app/Dialog;->show()V

    .line 191
    :goto_111
    const/4 v0, 0x1

    goto/16 :goto_12

    .line 186
    :cond_114
    new-instance v0, Lcom/teamspeak/ts3client/data/a;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/data/a;-><init>()V

    .line 34115
    const-wide/16 v2, -0x1

    iput-wide v2, v0, Lcom/teamspeak/ts3client/data/a;->g:J

    .line 188
    new-instance v1, Lcom/teamspeak/ts3client/d/a/m;

    const/4 v2, 0x1

    invoke-direct {v1, v0, v2}, Lcom/teamspeak/ts3client/d/a/m;-><init>(Lcom/teamspeak/ts3client/data/a;Z)V

    .line 189
    const-string v0, "ChannelDialog"

    invoke-virtual {v1, v4, v0}, Lcom/teamspeak/ts3client/d/a/m;->a(Landroid/support/v4/app/cd;Ljava/lang/String;)I

    goto :goto_111

    .line 193
    :pswitch_129
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 35061
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 35165
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->f:Lcom/teamspeak/ts3client/data/g;

    .line 36080
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    .line 194
    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 195
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 196
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 197
    new-instance v5, Landroid/app/Dialog;

    .line 36107
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 197
    const v2, 0x7f0700b5

    invoke-direct {v5, v0, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 198
    invoke-static {v5}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/app/Dialog;)V

    .line 199
    new-instance v6, Landroid/widget/LinearLayout;

    .line 37107
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 199
    invoke-direct {v6, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 200
    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 38107
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 202
    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/StartGUIFragment;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 203
    const v2, 0x7f030046

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 204
    const v2, 0x7f0c0190

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Spinner;

    .line 205
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v7, -0x1

    const/4 v8, -0x2

    invoke-direct {v2, v7, v8}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 207
    new-instance v2, Landroid/widget/ArrayAdapter;

    .line 39107
    iget-object v7, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 207
    const v8, 0x1090009

    invoke-direct {v2, v7, v8, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 208
    invoke-virtual {v3, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 209
    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 210
    new-instance v7, Landroid/widget/Button;

    .line 40107
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 210
    invoke-direct {v7, v0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 211
    const-string v0, "button.ok"

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 212
    new-instance v0, Lcom/teamspeak/ts3client/d/c/f;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/teamspeak/ts3client/d/c/f;-><init>(Lcom/teamspeak/ts3client/d/c/d;Landroid/widget/ArrayAdapter;Landroid/widget/Spinner;Landroid/support/v4/app/cd;Landroid/app/Dialog;)V

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 224
    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 225
    invoke-virtual {v5, v6}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 226
    const-string v0, "menu.createsubchannel.text"

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 227
    invoke-virtual {v5}, Landroid/app/Dialog;->show()V

    .line 229
    const/4 v0, 0x1

    goto/16 :goto_12

    .line 231
    :pswitch_1bf
    new-instance v0, Landroid/app/Dialog;

    .line 41107
    iget-object v1, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 231
    const v2, 0x7f0700b5

    invoke-direct {v0, v1, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 232
    invoke-static {v0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/app/Dialog;)V

    .line 233
    new-instance v1, Landroid/widget/LinearLayout;

    .line 42107
    iget-object v2, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 233
    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 234
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 235
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 236
    new-instance v2, Landroid/widget/EditText;

    .line 43107
    iget-object v3, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 236
    invoke-direct {v2, v3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 237
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setMaxLines(I)V

    .line 238
    const/4 v3, 0x1

    new-array v3, v3, [Landroid/text/InputFilter;

    .line 239
    const/4 v4, 0x0

    new-instance v5, Landroid/text/InputFilter$LengthFilter;

    const/16 v6, 0x50

    invoke-direct {v5, v6}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v5, v3, v4

    .line 240
    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 241
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 242
    new-instance v3, Landroid/widget/Button;

    .line 44107
    iget-object v4, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 242
    invoke-direct {v3, v4}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 243
    const-string v4, "button.ok"

    invoke-static {v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 244
    new-instance v4, Lcom/teamspeak/ts3client/d/c/g;

    invoke-direct {v4, p0, p2, v2, v0}, Lcom/teamspeak/ts3client/d/c/g;-><init>(Lcom/teamspeak/ts3client/d/c/d;Lcom/teamspeak/ts3client/Ts3Application;Landroid/widget/EditText;Landroid/app/Dialog;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 257
    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 258
    const-string v1, "menu.away.text"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 259
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 260
    const/4 v0, 0x1

    goto/16 :goto_12

    .line 45061
    :pswitch_222
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 45234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 46061
    iget-object v1, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 46267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 262
    sget-object v1, Lcom/teamspeak/ts3client/jni/d;->M:Lcom/teamspeak/ts3client/jni/d;

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 47061
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 47234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 48061
    iget-object v1, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 48267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 263
    sget-object v1, Lcom/teamspeak/ts3client/jni/d;->N:Lcom/teamspeak/ts3client/jni/d;

    const-string v4, ""

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;Ljava/lang/String;)I

    .line 49061
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 49234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 50061
    iget-object v1, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50062
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 264
    const-string v1, "ReturnAway"

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_flushClientSelfUpdates(JLjava/lang/String;)I

    .line 50063
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50064
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 50065
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/ab;->s:Ljava/util/BitSet;

    .line 265
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->clear(I)V

    .line 50066
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50067
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->v:Lcom/teamspeak/ts3client/data/ab;

    .line 266
    const-string v1, ""

    .line 50068
    iput-object v1, v0, Lcom/teamspeak/ts3client/data/ab;->t:Ljava/lang/String;

    .line 267
    invoke-virtual {p2}, Lcom/teamspeak/ts3client/Ts3Application;->g()Lcom/teamspeak/ts3client/a/p;

    move-result-object v0

    sget-object v1, Lcom/teamspeak/ts3client/jni/h;->aW:Lcom/teamspeak/ts3client/jni/h;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/a/p;->a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V

    .line 268
    const/4 v0, 0x1

    goto/16 :goto_12

    .line 50070
    :pswitch_26b
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 270
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v0

    const-string v1, "TempPassList"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bi;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_291

    .line 271
    new-instance v0, Lcom/teamspeak/ts3client/d/d/i;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/d/d/i;-><init>()V

    const-string v1, "TempPassList"

    invoke-virtual {v4, v0, v1}, Landroid/support/v4/app/cd;->b(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;

    .line 274
    :goto_283
    invoke-virtual {v4}, Landroid/support/v4/app/cd;->f()Landroid/support/v4/app/cd;

    .line 275
    const/16 v0, 0x1001

    invoke-virtual {v4, v0}, Landroid/support/v4/app/cd;->a(I)Landroid/support/v4/app/cd;

    .line 276
    invoke-virtual {v4}, Landroid/support/v4/app/cd;->i()I

    .line 277
    const/4 v0, 0x1

    goto/16 :goto_12

    .line 50071
    :cond_291
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 273
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v0

    const-string v1, "TempPassList"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bi;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/support/v4/app/cd;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;

    goto :goto_283

    .line 50072
    :pswitch_2a1
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 279
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v0

    const-string v1, "Contact"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bi;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_2c7

    .line 280
    new-instance v0, Lcom/teamspeak/ts3client/c/b;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/c/b;-><init>()V

    const-string v1, "Contact"

    invoke-virtual {v4, v0, v1}, Landroid/support/v4/app/cd;->b(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;

    .line 283
    :goto_2b9
    invoke-virtual {v4}, Landroid/support/v4/app/cd;->f()Landroid/support/v4/app/cd;

    .line 284
    const/16 v0, 0x1001

    invoke-virtual {v4, v0}, Landroid/support/v4/app/cd;->a(I)Landroid/support/v4/app/cd;

    .line 285
    invoke-virtual {v4}, Landroid/support/v4/app/cd;->i()I

    .line 286
    const/4 v0, 0x1

    goto/16 :goto_12

    .line 50073
    :cond_2c7
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 282
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v0

    const-string v1, "Contact"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bi;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/support/v4/app/cd;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;

    goto :goto_2b9

    .line 288
    :pswitch_2d7
    new-instance v0, Landroid/app/Dialog;

    .line 50074
    iget-object v1, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 288
    const v2, 0x7f0700b5

    invoke-direct {v0, v1, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 289
    invoke-static {v0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/app/Dialog;)V

    .line 290
    new-instance v1, Landroid/widget/LinearLayout;

    .line 50075
    iget-object v2, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 290
    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 291
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 292
    new-instance v2, Landroid/widget/TextView;

    .line 50076
    iget-object v3, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 292
    invoke-direct {v2, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 293
    const-string v3, "menu.talkpower.reason.text"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 294
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 295
    new-instance v2, Landroid/widget/EditText;

    .line 50077
    iget-object v3, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 295
    invoke-direct {v2, v3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 296
    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x2

    invoke-direct {v3, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 297
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 298
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 299
    const-string v3, "menu.talkpower.reason"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 300
    new-instance v3, Landroid/widget/Button;

    .line 50078
    iget-object v4, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 300
    invoke-direct {v3, v4}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 301
    const-string v4, "button.ok"

    invoke-static {v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 302
    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    const/4 v5, -0x1

    const/4 v6, -0x2

    invoke-direct {v4, v5, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 303
    new-instance v4, Lcom/teamspeak/ts3client/d/c/h;

    invoke-direct {v4, p0, p2, v2, v0}, Lcom/teamspeak/ts3client/d/c/h;-><init>(Lcom/teamspeak/ts3client/d/c/d;Lcom/teamspeak/ts3client/Ts3Application;Landroid/widget/EditText;Landroid/app/Dialog;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 311
    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 312
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 313
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 314
    const/4 v0, 0x1

    goto/16 :goto_12

    .line 50079
    :pswitch_351
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50080
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 50081
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50082
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 316
    const/4 v4, 0x0

    const-string v5, ""

    const-string v6, "cancel request talkpower"

    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestIsTalker(JILjava/lang/String;Ljava/lang/String;)I

    .line 317
    const/4 v0, 0x1

    goto/16 :goto_12

    .line 50083
    :pswitch_364
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 319
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v0

    const-string v1, "Banlist"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bi;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_38a

    .line 320
    new-instance v0, Lcom/teamspeak/ts3client/b/b;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/b/b;-><init>()V

    const-string v1, "Banlist"

    invoke-virtual {v4, v0, v1}, Landroid/support/v4/app/cd;->b(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/cd;

    .line 323
    :goto_37c
    invoke-virtual {v4}, Landroid/support/v4/app/cd;->f()Landroid/support/v4/app/cd;

    .line 324
    const/16 v0, 0x1001

    invoke-virtual {v4, v0}, Landroid/support/v4/app/cd;->a(I)Landroid/support/v4/app/cd;

    .line 325
    invoke-virtual {v4}, Landroid/support/v4/app/cd;->i()I

    .line 326
    const/4 v0, 0x1

    goto/16 :goto_12

    .line 50084
    :cond_38a
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 322
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/StartGUIFragment;->c()Landroid/support/v4/app/bi;

    move-result-object v0

    const-string v1, "Banlist"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bi;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/support/v4/app/cd;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/cd;

    goto :goto_37c

    .line 50085
    :pswitch_39a
    iget-object v0, p2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50086
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 328
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/t;->a(Z)V

    .line 329
    const/4 v0, 0x1

    goto/16 :goto_12

    .line 127
    nop

    :pswitch_data_3a6
    .packed-switch 0x65
        :pswitch_13
        :pswitch_48
        :pswitch_129
        :pswitch_1bf
        :pswitch_222
        :pswitch_26b
        :pswitch_2a1
        :pswitch_2d7
        :pswitch_351
        :pswitch_364
        :pswitch_39a
    .end packed-switch
.end method
