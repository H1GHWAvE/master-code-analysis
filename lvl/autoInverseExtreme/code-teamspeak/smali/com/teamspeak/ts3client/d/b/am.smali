.class public final Lcom/teamspeak/ts3client/d/b/am;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field a:Ljava/util/ArrayList;

.field b:Landroid/widget/CheckBox;

.field c:Z

.field final synthetic d:Lcom/teamspeak/ts3client/d/b/aj;


# direct methods
.method public constructor <init>(Lcom/teamspeak/ts3client/d/b/aj;Z)V
    .registers 4

    .prologue
    .line 181
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/b/am;->d:Lcom/teamspeak/ts3client/d/b/aj;

    .line 182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/b/am;->a:Ljava/util/ArrayList;

    .line 179
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/d/b/am;->c:Z

    .line 183
    iput-boolean p2, p0, Lcom/teamspeak/ts3client/d/b/am;->c:Z

    .line 184
    return-void
.end method

.method private a(Landroid/widget/CheckBox;)V
    .registers 3

    .prologue
    .line 187
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/am;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 188
    invoke-virtual {p1, p0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 189
    return-void
.end method

.method private b(Landroid/widget/CheckBox;)V
    .registers 2

    .prologue
    .line 230
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/b/am;->b:Landroid/widget/CheckBox;

    .line 231
    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 14

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 193
    move-object v0, p1

    check-cast v0, Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v2

    .line 194
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/d/b/am;->c:Z

    if-eqz v0, :cond_11a

    .line 195
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/am;->d:Lcom/teamspeak/ts3client/d/b/aj;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/b/aj;->b(Lcom/teamspeak/ts3client/d/b/aj;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 1061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 195
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/e;->c()Lcom/teamspeak/ts3client/data/b;

    move-result-object v1

    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/am;->d:Lcom/teamspeak/ts3client/d/b/aj;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/b/aj;->b(Lcom/teamspeak/ts3client/d/b/aj;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 2061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2177
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->i:Ljava/util/HashMap;

    .line 195
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/b/am;->d:Lcom/teamspeak/ts3client/d/b/aj;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/b/aj;->a(Lcom/teamspeak/ts3client/d/b/aj;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v3

    .line 2235
    iget v3, v3, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 195
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v1, v0}, Lcom/teamspeak/ts3client/data/b;->a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v6

    .line 196
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/am;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 197
    invoke-virtual {v0, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_3f

    .line 199
    :cond_4f
    if-eqz v2, :cond_b4

    move-object v0, p1

    .line 200
    check-cast v0, Landroid/widget/CompoundButton;

    invoke-virtual {v0, v7}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 201
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/am;->d:Lcom/teamspeak/ts3client/d/b/aj;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/b/aj;->b(Lcom/teamspeak/ts3client/d/b/aj;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 3061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 201
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/am;->d:Lcom/teamspeak/ts3client/d/b/aj;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/b/aj;->b(Lcom/teamspeak/ts3client/d/b/aj;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 4061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 4267
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 201
    new-array v4, v7, [J

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    aput-wide v10, v4, v8

    new-array v5, v7, [J

    .line 5087
    iget-wide v10, v6, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 201
    aput-wide v10, v5, v8

    new-array v6, v7, [J

    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/am;->d:Lcom/teamspeak/ts3client/d/b/aj;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/b/aj;->c(Lcom/teamspeak/ts3client/d/b/aj;)J

    move-result-wide v10

    aput-wide v10, v6, v8

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/teamspeak/ts3client/d/b/am;->d:Lcom/teamspeak/ts3client/d/b/aj;

    invoke-static {v8}, Lcom/teamspeak/ts3client/d/b/aj;->c(Lcom/teamspeak/ts3client/d/b/aj;)J

    move-result-wide v8

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, " set to channelgroup "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v1 .. v8}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestSetClientChannelGroup(J[J[J[JILjava/lang/String;)I

    .line 227
    :goto_b3
    return-void

    .line 203
    :cond_b4
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/am;->d:Lcom/teamspeak/ts3client/d/b/aj;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/b/aj;->b(Lcom/teamspeak/ts3client/d/b/aj;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 6061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 6234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 203
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/am;->d:Lcom/teamspeak/ts3client/d/b/aj;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/b/aj;->b(Lcom/teamspeak/ts3client/d/b/aj;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 7061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 7267
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 203
    new-array v4, v7, [J

    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/am;->b:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    aput-wide v10, v4, v8

    new-array v5, v7, [J

    .line 8087
    iget-wide v10, v6, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 203
    aput-wide v10, v5, v8

    new-array v6, v7, [J

    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/am;->d:Lcom/teamspeak/ts3client/d/b/aj;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/b/aj;->c(Lcom/teamspeak/ts3client/d/b/aj;)J

    move-result-wide v10

    aput-wide v10, v6, v8

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/teamspeak/ts3client/d/b/am;->d:Lcom/teamspeak/ts3client/d/b/aj;

    invoke-static {v8}, Lcom/teamspeak/ts3client/d/b/aj;->c(Lcom/teamspeak/ts3client/d/b/aj;)J

    move-result-wide v8

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, " set to channelgroup "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v8, p0, Lcom/teamspeak/ts3client/d/b/am;->b:Landroid/widget/CheckBox;

    invoke-virtual {v8}, Landroid/widget/CheckBox;->getTag()Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v1 .. v8}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestSetClientChannelGroup(J[J[J[JILjava/lang/String;)I

    .line 204
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/am;->b:Landroid/widget/CheckBox;

    invoke-virtual {v0, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_b3

    .line 207
    :cond_11a
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/am;->b:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_17e

    .line 208
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/am;->b:Landroid/widget/CheckBox;

    invoke-virtual {v0, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 210
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/am;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v8

    :goto_136
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_177

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 211
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_220

    .line 213
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/am;->d:Lcom/teamspeak/ts3client/d/b/aj;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/b/aj;->d(Lcom/teamspeak/ts3client/d/b/aj;)I

    move-result v1

    iget-object v4, p0, Lcom/teamspeak/ts3client/d/b/am;->d:Lcom/teamspeak/ts3client/d/b/aj;

    invoke-static {v4}, Lcom/teamspeak/ts3client/d/b/aj;->b(Lcom/teamspeak/ts3client/d/b/aj;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v4

    .line 9061
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 9271
    iget-object v4, v4, Lcom/teamspeak/ts3client/data/e;->o:Lcom/teamspeak/ts3client/data/g/b;

    .line 213
    invoke-virtual {v0}, Landroid/widget/CheckBox;->getTag()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-virtual {v4, v10, v11}, Lcom/teamspeak/ts3client/data/g/b;->b(J)Lcom/teamspeak/ts3client/data/g/a;

    move-result-object v4

    .line 10050
    iget v4, v4, Lcom/teamspeak/ts3client/data/g/a;->f:I

    .line 213
    if-ge v1, v4, :cond_171

    .line 214
    invoke-virtual {v0, v8}, Landroid/widget/CheckBox;->setEnabled(Z)V

    move v1, v7

    goto :goto_136

    .line 216
    :cond_171
    invoke-virtual {v0, v7}, Landroid/widget/CheckBox;->setEnabled(Z)V

    move v0, v7

    :goto_175
    move v1, v0

    .line 218
    goto :goto_136

    .line 219
    :cond_177
    if-nez v1, :cond_17e

    .line 220
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/am;->b:Landroid/widget/CheckBox;

    invoke-virtual {v0, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 222
    :cond_17e
    if-eqz v2, :cond_1d0

    .line 223
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/am;->d:Lcom/teamspeak/ts3client/d/b/aj;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/b/aj;->b(Lcom/teamspeak/ts3client/d/b/aj;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 10061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 10234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 223
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/am;->d:Lcom/teamspeak/ts3client/d/b/aj;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/b/aj;->b(Lcom/teamspeak/ts3client/d/b/aj;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 11061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 11267
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 223
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/am;->d:Lcom/teamspeak/ts3client/d/b/aj;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/b/aj;->c(Lcom/teamspeak/ts3client/d/b/aj;)J

    move-result-wide v6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/teamspeak/ts3client/d/b/am;->d:Lcom/teamspeak/ts3client/d/b/aj;

    invoke-static {v8}, Lcom/teamspeak/ts3client/d/b/aj;->c(Lcom/teamspeak/ts3client/d/b/aj;)J

    move-result-wide v8

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, " added in sgroup "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v1 .. v8}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestServerGroupAddClient(JJJLjava/lang/String;)I

    goto/16 :goto_b3

    .line 225
    :cond_1d0
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/am;->d:Lcom/teamspeak/ts3client/d/b/aj;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/b/aj;->b(Lcom/teamspeak/ts3client/d/b/aj;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 12061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 12234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 225
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/am;->d:Lcom/teamspeak/ts3client/d/b/aj;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/b/aj;->b(Lcom/teamspeak/ts3client/d/b/aj;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 13061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 13267
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 225
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/am;->d:Lcom/teamspeak/ts3client/d/b/aj;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/b/aj;->c(Lcom/teamspeak/ts3client/d/b/aj;)J

    move-result-wide v6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/teamspeak/ts3client/d/b/am;->d:Lcom/teamspeak/ts3client/d/b/aj;

    invoke-static {v8}, Lcom/teamspeak/ts3client/d/b/aj;->c(Lcom/teamspeak/ts3client/d/b/aj;)J

    move-result-wide v8

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, " added in sgroup "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v1 .. v8}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestServerGroupDelClient(JJJLjava/lang/String;)I

    goto/16 :goto_b3

    :cond_220
    move v0, v1

    goto/16 :goto_175
.end method
