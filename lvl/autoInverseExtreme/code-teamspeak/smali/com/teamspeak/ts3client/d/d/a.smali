.class public final Lcom/teamspeak/ts3client/d/d/a;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field a:Ljava/util/ArrayList;

.field private b:Landroid/content/Context;

.field private c:Landroid/view/LayoutInflater;

.field private d:Landroid/support/v4/app/bi;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/bi;)V
    .registers 4

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/d/a;->b:Landroid/content/Context;

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/d/a;->a:Ljava/util/ArrayList;

    .line 28
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/d/a;->c:Landroid/view/LayoutInflater;

    .line 29
    iput-object p2, p0, Lcom/teamspeak/ts3client/d/d/a;->d:Landroid/support/v4/app/bi;

    .line 30
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/d/a;)Ljava/util/ArrayList;
    .registers 2

    .prologue
    .line 17
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/a;->a:Ljava/util/ArrayList;

    return-object v0
.end method

.method private a()V
    .registers 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 39
    return-void
.end method

.method private a(Lcom/teamspeak/ts3client/d/d/c;)V
    .registers 3

    .prologue
    .line 33
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 34
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 35
    :cond_d
    return-void
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/d/a;)Landroid/support/v4/app/bi;
    .registers 2

    .prologue
    .line 17
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/a;->d:Landroid/support/v4/app/bi;

    return-object v0
.end method

.method private b(Lcom/teamspeak/ts3client/d/d/c;)V
    .registers 3

    .prologue
    .line 77
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 78
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 79
    :cond_d
    return-void
.end method


# virtual methods
.method public final getCount()I
    .registers 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 48
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .registers 4

    .prologue
    .line 53
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 18

    .prologue
    .line 58
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/a;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f030059

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 60
    const v0, 0x7f0c01e7

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 61
    const v1, 0x7f0c01e8

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 63
    const-string v4, "temppass.entry.text"

    const/4 v2, 0x1

    new-array v5, v2, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/d/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/teamspeak/ts3client/d/d/c;

    .line 1050
    iget-object v2, v2, Lcom/teamspeak/ts3client/d/d/c;->c:Ljava/lang/String;

    .line 63
    aput-object v2, v5, v6

    invoke-static {v4, v5}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    const-string v2, "temppass.entry.until"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/d/d/c;

    .line 2040
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, " "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/text/DateFormat;->getDateTimeInstance()Ljava/text/DateFormat;

    move-result-object v7

    new-instance v8, Ljava/util/Date;

    iget-wide v10, v0, Lcom/teamspeak/ts3client/d/d/c;->e:J

    const-wide/16 v12, 0x3e8

    mul-long/2addr v10, v12

    invoke-direct {v8, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v7, v8}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 64
    aput-object v0, v4, v5

    invoke-static {v2, v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    new-instance v0, Lcom/teamspeak/ts3client/d/d/b;

    invoke-direct {v0, p0, p1}, Lcom/teamspeak/ts3client/d/d/b;-><init>(Lcom/teamspeak/ts3client/d/d/a;I)V

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    return-object v3
.end method
