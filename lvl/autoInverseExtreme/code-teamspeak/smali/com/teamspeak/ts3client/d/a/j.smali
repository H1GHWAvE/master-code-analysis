.class final Lcom/teamspeak/ts3client/d/a/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/ArrayAdapter;

.field final synthetic b:Landroid/widget/Spinner;

.field final synthetic c:Landroid/app/Dialog;

.field final synthetic d:Lcom/teamspeak/ts3client/d/a/h;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/d/a/h;Landroid/widget/ArrayAdapter;Landroid/widget/Spinner;Landroid/app/Dialog;)V
    .registers 5

    .prologue
    .line 216
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/a/j;->d:Lcom/teamspeak/ts3client/d/a/h;

    iput-object p2, p0, Lcom/teamspeak/ts3client/d/a/j;->a:Landroid/widget/ArrayAdapter;

    iput-object p3, p0, Lcom/teamspeak/ts3client/d/a/j;->b:Landroid/widget/Spinner;

    iput-object p4, p0, Lcom/teamspeak/ts3client/d/a/j;->c:Landroid/app/Dialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 13

    .prologue
    .line 220
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/j;->a:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/j;->b:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/a;

    .line 1119
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/a;->e:J

    .line 221
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/j;->d:Lcom/teamspeak/ts3client/d/a/h;

    iget-object v1, v1, Lcom/teamspeak/ts3client/d/a/h;->a:Lcom/teamspeak/ts3client/d/a/a;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/a/a;->a(Lcom/teamspeak/ts3client/d/a/a;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v1

    .line 2119
    iget-wide v4, v1, Lcom/teamspeak/ts3client/data/a;->e:J

    .line 221
    cmp-long v1, v2, v4

    if-nez v1, :cond_64

    .line 222
    invoke-static {}, Lcom/teamspeak/ts3client/d/a/a;->y()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 3061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 222
    invoke-static {}, Lcom/teamspeak/ts3client/d/a/a;->y()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 4061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 4267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 222
    iget-object v4, p0, Lcom/teamspeak/ts3client/d/a/j;->d:Lcom/teamspeak/ts3client/d/a/h;

    iget-object v4, v4, Lcom/teamspeak/ts3client/d/a/h;->a:Lcom/teamspeak/ts3client/d/a/a;

    invoke-static {v4}, Lcom/teamspeak/ts3client/d/a/a;->a(Lcom/teamspeak/ts3client/d/a/a;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v4

    .line 5087
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 222
    sget-object v6, Lcom/teamspeak/ts3client/jni/c;->i:Lcom/teamspeak/ts3client/jni/c;

    .line 6087
    iget-wide v7, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 222
    invoke-virtual/range {v1 .. v8}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JJLcom/teamspeak/ts3client/jni/c;J)I

    .line 223
    invoke-static {}, Lcom/teamspeak/ts3client/d/a/a;->y()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 7061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 7234
    iget-object v1, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 223
    invoke-static {}, Lcom/teamspeak/ts3client/d/a/a;->y()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 8061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 8267
    iget-wide v2, v0, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 223
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/j;->d:Lcom/teamspeak/ts3client/d/a/h;

    iget-object v0, v0, Lcom/teamspeak/ts3client/d/a/h;->a:Lcom/teamspeak/ts3client/d/a/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/a/a;->a(Lcom/teamspeak/ts3client/d/a/a;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v0

    .line 9087
    iget-wide v4, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 223
    const-string v6, "Move channel"

    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_flushChannelUpdates(JJLjava/lang/String;)I

    .line 228
    :goto_5e
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/j;->c:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 229
    return-void

    .line 225
    :cond_64
    invoke-static {}, Lcom/teamspeak/ts3client/d/a/a;->y()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 10061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 10234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 225
    invoke-static {}, Lcom/teamspeak/ts3client/d/a/a;->y()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 11061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 11267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 225
    iget-object v4, p0, Lcom/teamspeak/ts3client/d/a/j;->d:Lcom/teamspeak/ts3client/d/a/h;

    iget-object v4, v4, Lcom/teamspeak/ts3client/d/a/h;->a:Lcom/teamspeak/ts3client/d/a/a;

    invoke-static {v4}, Lcom/teamspeak/ts3client/d/a/a;->a(Lcom/teamspeak/ts3client/d/a/a;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v4

    .line 12087
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 12119
    iget-wide v6, v0, Lcom/teamspeak/ts3client/data/a;->e:J

    .line 13087
    iget-wide v8, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 225
    const-string v10, "Move Channel"

    invoke-virtual/range {v1 .. v10}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestChannelMove(JJJJLjava/lang/String;)I

    goto :goto_5e
.end method
