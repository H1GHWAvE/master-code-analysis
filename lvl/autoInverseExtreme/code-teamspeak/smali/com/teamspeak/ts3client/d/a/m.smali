.class public final Lcom/teamspeak/ts3client/d/a/m;
.super Landroid/support/v4/app/ax;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/data/w;


# instance fields
.field private aA:Landroid/widget/Spinner;

.field private aB:Landroid/widget/CheckBox;

.field private aC:Lcom/teamspeak/ts3client/Ts3Application;

.field private aD:Lcom/teamspeak/ts3client/d/a/m;

.field private aE:Ljava/lang/String;

.field private aF:Ljava/lang/String;

.field private aG:I

.field private aH:I

.field private aI:I

.field private aJ:I

.field private aK:Ljava/lang/String;

.field private aL:Landroid/widget/Button;

.field private aM:I

.field private aN:I

.field private aO:Z

.field private aP:Z

.field private aQ:Landroid/widget/ImageView;

.field private aR:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

.field private aS:Lcom/teamspeak/ts3client/customs/a;

.field private aT:Landroid/widget/TextView;

.field private aU:I

.field private aV:I

.field private aW:Z

.field at:Ljava/lang/String;

.field au:Z

.field private av:Lcom/teamspeak/ts3client/data/a;

.field private aw:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

.field private ax:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

.field private ay:Landroid/widget/EditText;

.field private az:Landroid/widget/EditText;


# direct methods
.method public constructor <init>(Lcom/teamspeak/ts3client/data/a;)V
    .registers 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 79
    invoke-direct {p0}, Landroid/support/v4/app/ax;-><init>()V

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aE:Ljava/lang/String;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aF:Ljava/lang/String;

    .line 54
    iput v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aG:I

    .line 55
    iput v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aH:I

    .line 56
    iput v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aI:I

    .line 57
    iput v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aJ:I

    .line 58
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->at:Ljava/lang/String;

    .line 59
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aK:Ljava/lang/String;

    .line 61
    const/4 v0, 0x4

    iput v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aM:I

    .line 62
    const/4 v0, 0x6

    iput v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aN:I

    .line 63
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aO:Z

    .line 64
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aP:Z

    .line 66
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/d/a/m;->au:Z

    .line 70
    iput v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aU:I

    .line 72
    iput-boolean v2, p0, Lcom/teamspeak/ts3client/d/a/m;->aW:Z

    .line 80
    if-eqz p1, :cond_36

    .line 81
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/a/m;->av:Lcom/teamspeak/ts3client/data/a;

    .line 87
    :goto_31
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/d/a/m;->au:Z

    .line 88
    iput-object p0, p0, Lcom/teamspeak/ts3client/d/a/m;->aD:Lcom/teamspeak/ts3client/d/a/m;

    .line 89
    return-void

    .line 83
    :cond_36
    new-instance v0, Lcom/teamspeak/ts3client/data/a;

    invoke-direct {v0}, Lcom/teamspeak/ts3client/data/a;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->av:Lcom/teamspeak/ts3client/data/a;

    .line 84
    iput-boolean v2, p0, Lcom/teamspeak/ts3client/d/a/m;->aO:Z

    .line 85
    iput-boolean v2, p0, Lcom/teamspeak/ts3client/d/a/m;->aP:Z

    goto :goto_31
.end method

.method public constructor <init>(Lcom/teamspeak/ts3client/data/a;Z)V
    .registers 4

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/d/a/m;-><init>(Lcom/teamspeak/ts3client/data/a;)V

    .line 93
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aO:Z

    .line 94
    iput-boolean p2, p0, Lcom/teamspeak/ts3client/d/a/m;->aP:Z

    .line 95
    return-void
.end method

.method private A()V
    .registers 3

    .prologue
    .line 415
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/a/m;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/d/a/s;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/d/a/s;-><init>(Lcom/teamspeak/ts3client/d/a/m;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 449
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/a/m;I)I
    .registers 2

    .prologue
    .line 40
    iput p1, p0, Lcom/teamspeak/ts3client/d/a/m;->aG:I

    return p1
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/d/a/m;
    .registers 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aD:Lcom/teamspeak/ts3client/d/a/m;

    return-object v0
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/a/m;Ljava/lang/String;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 40
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/a/m;->aE:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/a/m;I)I
    .registers 2

    .prologue
    .line 40
    iput p1, p0, Lcom/teamspeak/ts3client/d/a/m;->aH:I

    return p1
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/Spinner;
    .registers 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aA:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/a/m;Ljava/lang/String;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 40
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/a/m;->aK:Ljava/lang/String;

    return-object p1
.end method

.method private b(Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 122
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/d/a/m;->au:Z

    .line 123
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/a/m;->at:Ljava/lang/String;

    .line 124
    return-void
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/d/a/m;I)I
    .registers 2

    .prologue
    .line 40
    iput p1, p0, Lcom/teamspeak/ts3client/d/a/m;->aI:I

    return p1
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/d/a/m;Ljava/lang/String;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 40
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/a/m;->aF:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/d/a/m;)Z
    .registers 2

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aW:Z

    return v0
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/d/a/m;I)I
    .registers 2

    .prologue
    .line 40
    iput p1, p0, Lcom/teamspeak/ts3client/d/a/m;->aM:I

    return p1
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aT:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/d/a/m;Ljava/lang/String;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 40
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/a/m;->at:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/d/a/m;I)I
    .registers 2

    .prologue
    .line 40
    iput p1, p0, Lcom/teamspeak/ts3client/d/a/m;->aN:I

    return p1
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/bookmark/CustomEditText;
    .registers 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->ax:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    return-object v0
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/d/a/m;I)I
    .registers 2

    .prologue
    .line 40
    iput p1, p0, Lcom/teamspeak/ts3client/d/a/m;->aJ:I

    return p1
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/CheckBox;
    .registers 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aB:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic g(Lcom/teamspeak/ts3client/d/a/m;)I
    .registers 2

    .prologue
    .line 40
    iget v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aG:I

    return v0
.end method

.method static synthetic h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;
    .registers 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    return-object v0
.end method

.method static synthetic i(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/data/a;
    .registers 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->av:Lcom/teamspeak/ts3client/data/a;

    return-object v0
.end method

.method static synthetic j(Lcom/teamspeak/ts3client/d/a/m;)I
    .registers 2

    .prologue
    .line 40
    iget v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aJ:I

    return v0
.end method

.method static synthetic k(Lcom/teamspeak/ts3client/d/a/m;)I
    .registers 2

    .prologue
    .line 40
    iget v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aU:I

    return v0
.end method

.method static synthetic l(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/EditText;
    .registers 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->ay:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic m(Lcom/teamspeak/ts3client/d/a/m;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aK:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic n(Lcom/teamspeak/ts3client/d/a/m;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aE:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic o(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/EditText;
    .registers 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->az:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic p(Lcom/teamspeak/ts3client/d/a/m;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aF:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic q(Lcom/teamspeak/ts3client/d/a/m;)Z
    .registers 2

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/d/a/m;->au:Z

    return v0
.end method

.method static synthetic r(Lcom/teamspeak/ts3client/d/a/m;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->at:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic s(Lcom/teamspeak/ts3client/d/a/m;)I
    .registers 2

    .prologue
    .line 40
    iget v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aH:I

    return v0
.end method

.method static synthetic t(Lcom/teamspeak/ts3client/d/a/m;)I
    .registers 2

    .prologue
    .line 40
    iget v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aI:I

    return v0
.end method

.method static synthetic u(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/customs/CustomCodecSettings;
    .registers 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aR:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    return-object v0
.end method

.method static synthetic v(Lcom/teamspeak/ts3client/d/a/m;)I
    .registers 2

    .prologue
    .line 40
    iget v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aM:I

    return v0
.end method

.method static synthetic w(Lcom/teamspeak/ts3client/d/a/m;)I
    .registers 2

    .prologue
    .line 40
    iget v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aN:I

    return v0
.end method

.method static synthetic x(Lcom/teamspeak/ts3client/d/a/m;)Z
    .registers 2

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aO:Z

    return v0
.end method

.method private y()Ljava/lang/String;
    .registers 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->at:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic y(Lcom/teamspeak/ts3client/d/a/m;)Z
    .registers 2

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aP:Z

    return v0
.end method

.method static synthetic z(Lcom/teamspeak/ts3client/d/a/m;)I
    .registers 2

    .prologue
    .line 40
    iget v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aV:I

    return v0
.end method

.method private z()V
    .registers 7

    .prologue
    const/4 v5, 0x2

    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 330
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aO:Z

    if-eqz v0, :cond_111

    .line 331
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 33061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 33181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 331
    sget-object v1, Lcom/teamspeak/ts3client/jni/g;->aI:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v0

    if-nez v0, :cond_1b

    .line 332
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->az:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 333
    :cond_1b
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 34061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 34181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 333
    sget-object v1, Lcom/teamspeak/ts3client/jni/g;->aK:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v0

    if-nez v0, :cond_2e

    .line 334
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->ay:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 335
    :cond_2e
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 35061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 35181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 335
    sget-object v1, Lcom/teamspeak/ts3client/jni/g;->aJ:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v0

    if-nez v0, :cond_41

    .line 336
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aQ:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 337
    :cond_41
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 36061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 36181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 337
    sget-object v1, Lcom/teamspeak/ts3client/jni/g;->aW:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v0

    if-nez v0, :cond_54

    .line 338
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aB:Landroid/widget/CheckBox;

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 339
    :cond_54
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 37061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 37181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 339
    sget-object v1, Lcom/teamspeak/ts3client/jni/g;->aE:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v0

    if-nez v0, :cond_6c

    .line 340
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aS:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v5}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 341
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aA:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 343
    :cond_6c
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 38061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 38181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 343
    sget-object v1, Lcom/teamspeak/ts3client/jni/g;->aF:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v0

    if-nez v0, :cond_84

    .line 344
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aS:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 345
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aA:Landroid/widget/Spinner;

    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setSelection(I)V

    .line 347
    :cond_84
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 39061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 39181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 347
    sget-object v1, Lcom/teamspeak/ts3client/jni/g;->aG:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v0

    if-nez v0, :cond_97

    .line 348
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aS:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v4}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 350
    :cond_97
    iput-boolean v2, p0, Lcom/teamspeak/ts3client/d/a/m;->aW:Z

    .line 374
    :cond_99
    :goto_99
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 49061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 49181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 374
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50062
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->u:Lcom/teamspeak/ts3client/data/d/v;

    .line 374
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->aZ:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/d/v;->a(Lcom/teamspeak/ts3client/jni/g;)I

    move-result v1

    .line 50063
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/f/a;->a:Ljava/util/SortedMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/SortedMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    .line 374
    if-eqz v0, :cond_d3

    .line 375
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50064
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50065
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 375
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50066
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50067
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->u:Lcom/teamspeak/ts3client/data/d/v;

    .line 375
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->aZ:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/d/v;->a(Lcom/teamspeak/ts3client/jni/g;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/f/a;->b(I)I

    move-result v0

    iput v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aU:I

    .line 376
    iget v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aU:I

    if-ge v0, v3, :cond_d5

    .line 379
    :cond_d3
    iput v3, p0, Lcom/teamspeak/ts3client/d/a/m;->aU:I

    .line 380
    :cond_d5
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50068
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50069
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 380
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50070
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 50071
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 380
    sget-object v1, Lcom/teamspeak/ts3client/jni/i;->aB:Lcom/teamspeak/ts3client/jni/i;

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/i;)I

    move-result v0

    iput v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aV:I

    .line 381
    iget v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aU:I

    if-lez v0, :cond_f7

    iget v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aV:I

    iget v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aU:I

    if-le v0, v1, :cond_f7

    .line 382
    iget v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aU:I

    iput v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aV:I

    .line 383
    :cond_f7
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aT:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/teamspeak/ts3client/d/a/m;->aV:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 384
    iget v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aU:I

    packed-switch v0, :pswitch_data_1ee

    .line 399
    :goto_110
    return-void

    .line 352
    :cond_111
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 40061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 40181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 352
    sget-object v1, Lcom/teamspeak/ts3client/jni/g;->bf:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v0

    if-nez v0, :cond_124

    .line 353
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->ax:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    invoke-virtual {v0, v4}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->setEnabled(Z)V

    .line 354
    :cond_124
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 41061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 41181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 354
    sget-object v1, Lcom/teamspeak/ts3client/jni/g;->bg:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v0

    if-nez v0, :cond_137

    .line 355
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->az:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 356
    :cond_137
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 42061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 42181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 356
    sget-object v1, Lcom/teamspeak/ts3client/jni/g;->bi:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v0

    if-nez v0, :cond_14a

    .line 357
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->ay:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 358
    :cond_14a
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 43061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 43181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 358
    sget-object v1, Lcom/teamspeak/ts3client/jni/g;->bh:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v0

    if-nez v0, :cond_15d

    .line 359
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aQ:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 360
    :cond_15d
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 44061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 44181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 360
    sget-object v1, Lcom/teamspeak/ts3client/jni/g;->bb:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v0

    if-nez v0, :cond_170

    .line 361
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aB:Landroid/widget/CheckBox;

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 362
    :cond_170
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 45061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 45181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 362
    sget-object v1, Lcom/teamspeak/ts3client/jni/g;->bc:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v0

    if-nez v0, :cond_183

    .line 363
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aS:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v5}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 364
    :cond_183
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 46061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 46181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 364
    sget-object v1, Lcom/teamspeak/ts3client/jni/g;->bd:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v0

    if-nez v0, :cond_196

    .line 365
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aS:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 366
    :cond_196
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 47061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 47181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 366
    sget-object v1, Lcom/teamspeak/ts3client/jni/g;->be:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v0

    if-nez v0, :cond_1a9

    .line 367
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aS:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v0, v4}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 368
    :cond_1a9
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 48061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 48181
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 368
    sget-object v1, Lcom/teamspeak/ts3client/jni/g;->bt:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v0

    if-nez v0, :cond_99

    .line 369
    iput-boolean v4, p0, Lcom/teamspeak/ts3client/d/a/m;->aW:Z

    .line 370
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aT:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    goto/16 :goto_99

    .line 386
    :pswitch_1c0
    iput-boolean v4, p0, Lcom/teamspeak/ts3client/d/a/m;->aW:Z

    .line 387
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aT:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 388
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aT:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/teamspeak/ts3client/d/a/m;->aV:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_110

    .line 391
    :pswitch_1dd
    iput-boolean v4, p0, Lcom/teamspeak/ts3client/d/a/m;->aW:Z

    .line 392
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aT:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 393
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aT:Landroid/widget/TextView;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_110

    .line 384
    nop

    :pswitch_data_1ee
    .packed-switch -0x1
        :pswitch_1c0
        :pswitch_1dd
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 14

    .prologue
    const/4 v10, 0x2

    const/4 v9, -0x1

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 128
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 129
    const v0, 0x7f030021

    invoke-virtual {p1, v0, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 130
    iget-boolean v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aO:Z

    if-nez v1, :cond_25b

    .line 5207
    iget-object v1, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 131
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a/m;->av:Lcom/teamspeak/ts3client/data/a;

    .line 6103
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/a;->a:Ljava/lang/String;

    .line 131
    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 134
    :goto_26
    const-string v1, "channeldialog.editname"

    const v2, 0x7f0c00af

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 135
    const-string v1, "channeldialog.editpassword"

    const v2, 0x7f0c00b2

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 136
    const-string v1, "channeldialog.edittopic"

    const v2, 0x7f0c00b5

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 137
    const-string v1, "channeldialog.editdesc"

    const v2, 0x7f0c00b8

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 138
    const-string v1, "channeldialog.edittype"

    const v2, 0x7f0c00ba

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 139
    const-string v1, "channeldialog.editdefault"

    const v2, 0x7f0c00bd

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 140
    const-string v1, "channeldialog.editdeletedelay"

    const v2, 0x7f0c00c0

    invoke-static {v1, v0, v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;Landroid/view/ViewGroup;I)V

    .line 142
    const v1, 0x7f0c00b0

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->ax:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    .line 143
    const v1, 0x7f0c00b3

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->ay:Landroid/widget/EditText;

    .line 144
    const v1, 0x7f0c00b6

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->az:Landroid/widget/EditText;

    .line 145
    const v1, 0x7f0c00b9

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aQ:Landroid/widget/ImageView;

    .line 146
    const v1, 0x7f0c00c3

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aR:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    .line 147
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aQ:Landroid/widget/ImageView;

    new-instance v2, Lcom/teamspeak/ts3client/d/a/n;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/a/n;-><init>(Lcom/teamspeak/ts3client/d/a/m;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 157
    const v1, 0x7f0c00bb

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aA:Landroid/widget/Spinner;

    .line 158
    const-string v1, "channeldialog.edittype.spinner"

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/a/m;->i()Landroid/support/v4/app/bb;

    move-result-object v2

    const/4 v3, 0x3

    invoke-static {v1, v2, v3}, Lcom/teamspeak/ts3client/data/e/a;->b(Ljava/lang/String;Landroid/content/Context;I)Lcom/teamspeak/ts3client/customs/a;

    move-result-object v1

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aS:Lcom/teamspeak/ts3client/customs/a;

    .line 159
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aA:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a/m;->aS:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 161
    const v1, 0x7f0c00be

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aB:Landroid/widget/CheckBox;

    .line 162
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aB:Landroid/widget/CheckBox;

    new-instance v2, Lcom/teamspeak/ts3client/d/a/o;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/a/o;-><init>(Lcom/teamspeak/ts3client/d/a/m;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 174
    const v1, 0x7f0c00c1

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aT:Landroid/widget/TextView;

    .line 175
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aA:Landroid/widget/Spinner;

    new-instance v2, Lcom/teamspeak/ts3client/d/a/p;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/a/p;-><init>(Lcom/teamspeak/ts3client/d/a/m;)V

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 192
    const v1, 0x7f0c00c4

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aL:Landroid/widget/Button;

    .line 193
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aL:Landroid/widget/Button;

    const-string v2, "button.save"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 194
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aL:Landroid/widget/Button;

    new-instance v2, Lcom/teamspeak/ts3client/d/a/q;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/a/q;-><init>(Lcom/teamspeak/ts3client/d/a/m;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 282
    iget-boolean v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aO:Z

    if-eqz v1, :cond_11a

    .line 283
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aR:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    new-instance v2, Lcom/teamspeak/ts3client/data/d/ab;

    iget v3, p0, Lcom/teamspeak/ts3client/d/a/m;->aM:I

    iget v4, p0, Lcom/teamspeak/ts3client/d/a/m;->aN:I

    invoke-direct {v2, v3, v4}, Lcom/teamspeak/ts3client/data/d/ab;-><init>(II)V

    invoke-virtual {v1, v2, v8}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a(Lcom/teamspeak/ts3client/data/d/ab;Z)V

    .line 284
    :cond_11a
    iget-boolean v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aO:Z

    if-nez v1, :cond_13c

    .line 285
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 7061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 7238
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 285
    invoke-virtual {v1, p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/data/w;)V

    .line 286
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 8061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 8234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 286
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 9061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 9267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 286
    iget-object v4, p0, Lcom/teamspeak/ts3client/d/a/m;->av:Lcom/teamspeak/ts3client/data/a;

    .line 10087
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 286
    const-string v6, "ChannelEdit"

    invoke-virtual/range {v1 .. v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestChannelDescription(JJLjava/lang/String;)I

    .line 10330
    :cond_13c
    iget-boolean v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aO:Z

    if-eqz v1, :cond_268

    .line 10331
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 11061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 11181
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 10331
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->aI:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v1

    if-nez v1, :cond_153

    .line 10332
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->az:Landroid/widget/EditText;

    invoke-virtual {v1, v7}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 10333
    :cond_153
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 12061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 12181
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 10333
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->aK:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v1

    if-nez v1, :cond_166

    .line 10334
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->ay:Landroid/widget/EditText;

    invoke-virtual {v1, v7}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 10335
    :cond_166
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 13061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 13181
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 10335
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->aJ:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v1

    if-nez v1, :cond_179

    .line 10336
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aQ:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 10337
    :cond_179
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 14061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 14181
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 10337
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->aW:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v1

    if-nez v1, :cond_18c

    .line 10338
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aB:Landroid/widget/CheckBox;

    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 10339
    :cond_18c
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 15061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 15181
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 10339
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->aE:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v1

    if-nez v1, :cond_1a4

    .line 10340
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aS:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v1, v10}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 10341
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aA:Landroid/widget/Spinner;

    invoke-virtual {v1, v8}, Landroid/widget/Spinner;->setSelection(I)V

    .line 10343
    :cond_1a4
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 16061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 16181
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 10343
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->aF:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v1

    if-nez v1, :cond_1bc

    .line 10344
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aS:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v1, v8}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 10345
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aA:Landroid/widget/Spinner;

    invoke-virtual {v1, v7}, Landroid/widget/Spinner;->setSelection(I)V

    .line 10347
    :cond_1bc
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 17061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 17181
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 10347
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->aG:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v1

    if-nez v1, :cond_1cf

    .line 10348
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aS:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v1, v7}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 10350
    :cond_1cf
    iput-boolean v8, p0, Lcom/teamspeak/ts3client/d/a/m;->aW:Z

    .line 10374
    :cond_1d1
    :goto_1d1
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 27061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 27181
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 10374
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 28061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 28250
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->u:Lcom/teamspeak/ts3client/data/d/v;

    .line 10374
    sget-object v3, Lcom/teamspeak/ts3client/jni/g;->aZ:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v2, v3}, Lcom/teamspeak/ts3client/data/d/v;->a(Lcom/teamspeak/ts3client/jni/g;)I

    move-result v2

    .line 29049
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/f/a;->a:Ljava/util/SortedMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/SortedMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    .line 10374
    if-eqz v1, :cond_20b

    .line 10375
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 29061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 29181
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 10375
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 30061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 30250
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->u:Lcom/teamspeak/ts3client/data/d/v;

    .line 10375
    sget-object v3, Lcom/teamspeak/ts3client/jni/g;->aZ:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v2, v3}, Lcom/teamspeak/ts3client/data/d/v;->a(Lcom/teamspeak/ts3client/jni/g;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/f/a;->b(I)I

    move-result v1

    iput v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aU:I

    .line 10376
    iget v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aU:I

    if-ge v1, v9, :cond_20d

    .line 10379
    :cond_20b
    iput v9, p0, Lcom/teamspeak/ts3client/d/a/m;->aU:I

    .line 10380
    :cond_20d
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 31061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 31234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 10380
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 32061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 32267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 10380
    sget-object v4, Lcom/teamspeak/ts3client/jni/i;->aB:Lcom/teamspeak/ts3client/jni/i;

    invoke-virtual {v1, v2, v3, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/i;)I

    move-result v1

    iput v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aV:I

    .line 10381
    iget v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aU:I

    if-lez v1, :cond_22f

    iget v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aV:I

    iget v2, p0, Lcom/teamspeak/ts3client/d/a/m;->aU:I

    if-le v1, v2, :cond_22f

    .line 10382
    iget v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aU:I

    iput v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aV:I

    .line 10383
    :cond_22f
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aT:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/teamspeak/ts3client/d/a/m;->aV:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 10384
    iget v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aU:I

    packed-switch v1, :pswitch_data_344

    .line 289
    :goto_248
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aT:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_25a

    .line 290
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aT:Landroid/widget/TextView;

    new-instance v2, Lcom/teamspeak/ts3client/d/a/r;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/a/r;-><init>(Lcom/teamspeak/ts3client/d/a/m;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 326
    :cond_25a
    return-object v0

    .line 6207
    :cond_25b
    iget-object v1, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 133
    const-string v2, "channeldialog.create.text"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_26

    .line 10352
    :cond_268
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 18061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 18181
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 10352
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->bf:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v1

    if-nez v1, :cond_27b

    .line 10353
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->ax:Lcom/teamspeak/ts3client/bookmark/CustomEditText;

    invoke-virtual {v1, v7}, Lcom/teamspeak/ts3client/bookmark/CustomEditText;->setEnabled(Z)V

    .line 10354
    :cond_27b
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 19061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 19181
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 10354
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->bg:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v1

    if-nez v1, :cond_28e

    .line 10355
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->az:Landroid/widget/EditText;

    invoke-virtual {v1, v7}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 10356
    :cond_28e
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 20061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 20181
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 10356
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->bi:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v1

    if-nez v1, :cond_2a1

    .line 10357
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->ay:Landroid/widget/EditText;

    invoke-virtual {v1, v7}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 10358
    :cond_2a1
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 21061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 21181
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 10358
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->bh:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v1

    if-nez v1, :cond_2b4

    .line 10359
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aQ:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 10360
    :cond_2b4
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 22061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 22181
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 10360
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->bb:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v1

    if-nez v1, :cond_2c7

    .line 10361
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aB:Landroid/widget/CheckBox;

    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 10362
    :cond_2c7
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 23061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 23181
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 10362
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->bc:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v1

    if-nez v1, :cond_2da

    .line 10363
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aS:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v1, v10}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 10364
    :cond_2da
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 24061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 24181
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 10364
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->bd:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v1

    if-nez v1, :cond_2ed

    .line 10365
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aS:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v1, v8}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 10366
    :cond_2ed
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 25061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 25181
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 10366
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->be:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v1

    if-nez v1, :cond_300

    .line 10367
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aS:Lcom/teamspeak/ts3client/customs/a;

    invoke-virtual {v1, v7}, Lcom/teamspeak/ts3client/customs/a;->a(I)V

    .line 10368
    :cond_300
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 26061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 26181
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 10368
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->bt:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v1

    if-nez v1, :cond_1d1

    .line 10369
    iput-boolean v7, p0, Lcom/teamspeak/ts3client/d/a/m;->aW:Z

    .line 10370
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aT:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setEnabled(Z)V

    goto/16 :goto_1d1

    .line 10386
    :pswitch_317
    iput-boolean v7, p0, Lcom/teamspeak/ts3client/d/a/m;->aW:Z

    .line 10387
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aT:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 10388
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aT:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/teamspeak/ts3client/d/a/m;->aV:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_248

    .line 10391
    :pswitch_334
    iput-boolean v7, p0, Lcom/teamspeak/ts3client/d/a/m;->aW:Z

    .line 10392
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aT:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 10393
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aT:Landroid/widget/TextView;

    const-string v2, "0"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_248

    .line 10384
    :pswitch_data_344
    .packed-switch -0x1
        :pswitch_317
        :pswitch_334
    .end packed-switch
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
    .registers 6

    .prologue
    .line 99
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/UpdateChannel;

    if-eqz v0, :cond_1d

    .line 100
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/UpdateChannel;

    .line 1023
    iget-wide v0, p1, Lcom/teamspeak/ts3client/jni/events/UpdateChannel;->a:J

    .line 101
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/a/m;->av:Lcom/teamspeak/ts3client/data/a;

    .line 1087
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 101
    cmp-long v0, v0, v2

    if-nez v0, :cond_1c

    .line 102
    invoke-direct {p0}, Lcom/teamspeak/ts3client/d/a/m;->A()V

    .line 103
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 2061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 103
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->b(Lcom/teamspeak/ts3client/data/w;)V

    .line 115
    :cond_1c
    :goto_1c
    return-void

    .line 108
    :cond_1d
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/ServerPermissionError;

    if-eqz v0, :cond_1c

    .line 109
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/rare/ServerPermissionError;

    .line 3033
    iget v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/ServerPermissionError;->a:I

    .line 109
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 3061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3250
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->u:Lcom/teamspeak/ts3client/data/d/v;

    .line 109
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->bK:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/d/v;->a(Lcom/teamspeak/ts3client/jni/g;)I

    move-result v1

    if-ne v0, v1, :cond_1c

    .line 110
    invoke-direct {p0}, Lcom/teamspeak/ts3client/d/a/m;->A()V

    .line 111
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/m;->aC:Lcom/teamspeak/ts3client/Ts3Application;

    .line 4061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 4238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 111
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->b(Lcom/teamspeak/ts3client/data/w;)V

    goto :goto_1c
.end method

.method public final e()V
    .registers 1

    .prologue
    .line 403
    invoke-super {p0}, Landroid/support/v4/app/ax;->e()V

    .line 404
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/support/v4/app/ax;)V

    .line 405
    return-void
.end method

.method public final f()V
    .registers 1

    .prologue
    .line 409
    invoke-super {p0}, Landroid/support/v4/app/ax;->f()V

    .line 410
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/d/q;->b(Landroid/support/v4/app/ax;)V

    .line 412
    return-void
.end method
