.class public final Lcom/teamspeak/ts3client/d/b/ah;
.super Landroid/support/v4/app/ax;
.source "SourceFile"


# instance fields
.field private at:Lcom/teamspeak/ts3client/data/c;

.field private au:Landroid/widget/EditText;

.field private av:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/teamspeak/ts3client/data/c;)V
    .registers 2

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/support/v4/app/ax;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/b/ah;->at:Lcom/teamspeak/ts3client/data/c;

    .line 30
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/b/ah;)Lcom/teamspeak/ts3client/data/c;
    .registers 2

    .prologue
    .line 22
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/ah;->at:Lcom/teamspeak/ts3client/data/c;

    return-object v0
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/b/ah;)Landroid/widget/EditText;
    .registers 2

    .prologue
    .line 22
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/ah;->au:Landroid/widget/EditText;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 14

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, -0x1

    const/4 v6, -0x2

    .line 34
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    .line 36
    new-instance v1, Landroid/widget/RelativeLayout;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 37
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v7, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 39
    new-instance v2, Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/teamspeak/ts3client/d/b/ah;->av:Landroid/widget/TextView;

    .line 40
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/ah;->av:Landroid/widget/TextView;

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setId(I)V

    .line 41
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/ah;->av:Landroid/widget/TextView;

    const-string v3, "clientdialog.poke.text"

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/teamspeak/ts3client/d/b/ah;->at:Lcom/teamspeak/ts3client/data/c;

    .line 1203
    iget-object v5, v5, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 41
    aput-object v5, v4, v9

    invoke-static {v3, v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 43
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v7, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 44
    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 45
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/b/ah;->av:Landroid/widget/TextView;

    invoke-virtual {v1, v3, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 47
    new-instance v2, Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/teamspeak/ts3client/d/b/ah;->au:Landroid/widget/EditText;

    .line 48
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/ah;->au:Landroid/widget/EditText;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setId(I)V

    .line 49
    new-array v2, v8, [Landroid/text/InputFilter;

    .line 50
    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v2, v9

    .line 51
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/b/ah;->au:Landroid/widget/EditText;

    invoke-virtual {v3, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 53
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v7, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 54
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/b/ah;->av:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getId()I

    move-result v3

    invoke-virtual {v2, v10, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 55
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/b/ah;->au:Landroid/widget/EditText;

    invoke-virtual {v1, v3, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 57
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v7, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 58
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/b/ah;->au:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getId()I

    move-result v3

    invoke-virtual {v2, v10, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 59
    new-instance v3, Landroid/widget/Button;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 60
    const-string v4, "clientdialog.poke.info"

    invoke-static {v4}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 61
    new-instance v4, Lcom/teamspeak/ts3client/d/b/ai;

    invoke-direct {v4, p0, v0, v1}, Lcom/teamspeak/ts3client/d/b/ai;-><init>(Lcom/teamspeak/ts3client/d/b/ah;Lcom/teamspeak/ts3client/Ts3Application;Landroid/widget/RelativeLayout;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    invoke-virtual {v1, v3, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1207
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 71
    iget-object v2, p0, Lcom/teamspeak/ts3client/d/b/ah;->at:Lcom/teamspeak/ts3client/data/c;

    .line 2203
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/c;->a:Ljava/lang/String;

    .line 71
    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 72
    return-object v1
.end method

.method public final e()V
    .registers 1

    .prologue
    .line 77
    invoke-super {p0}, Landroid/support/v4/app/ax;->e()V

    .line 78
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/support/v4/app/ax;)V

    .line 79
    return-void
.end method

.method public final f()V
    .registers 1

    .prologue
    .line 83
    invoke-super {p0}, Landroid/support/v4/app/ax;->f()V

    .line 84
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/d/q;->b(Landroid/support/v4/app/ax;)V

    .line 86
    return-void
.end method
