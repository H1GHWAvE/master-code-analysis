.class public final Lcom/teamspeak/ts3client/d/d/i;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/data/w;


# instance fields
.field private a:Lcom/teamspeak/ts3client/Ts3Application;

.field private b:Landroid/widget/ListView;

.field private c:Lcom/teamspeak/ts3client/d/d/a;

.field private d:Landroid/widget/ImageButton;

.field private e:Landroid/widget/ImageButton;

.field private f:Lcom/teamspeak/ts3client/d/d/i;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/d/i;)Lcom/teamspeak/ts3client/d/d/a;
    .registers 2

    .prologue
    .line 23
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/i;->c:Lcom/teamspeak/ts3client/d/d/a;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 6

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/d/i;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bb;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/d/i;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 85
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/i;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 6081
    iput-object p0, v0, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 86
    const v0, 0x7f030058

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 87
    const v0, 0x7f0c01e3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/d/i;->b:Landroid/widget/ListView;

    .line 88
    const v0, 0x7f0c01e5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/d/i;->d:Landroid/widget/ImageButton;

    .line 89
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/i;->d:Landroid/widget/ImageButton;

    new-instance v2, Lcom/teamspeak/ts3client/d/d/l;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/d/l;-><init>(Lcom/teamspeak/ts3client/d/d/i;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    const v0, 0x7f0c01e6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/d/i;->e:Landroid/widget/ImageButton;

    .line 99
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/i;->e:Landroid/widget/ImageButton;

    new-instance v2, Lcom/teamspeak/ts3client/d/d/m;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/d/d/m;-><init>(Lcom/teamspeak/ts3client/d/d/i;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/d/i;->n()V

    .line 107
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/i;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 6204
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->o:Landroid/support/v7/app/a;

    .line 107
    const-string v2, "menu.temporarypasswords"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v7/app/a;->b(Ljava/lang/CharSequence;)V

    .line 108
    return-object v1
.end method

.method public final a()V
    .registers 5

    .prologue
    .line 119
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/i;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 8061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 8238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 119
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/data/w;)V

    .line 120
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/i;->c:Lcom/teamspeak/ts3client/d/d/a;

    if-nez v0, :cond_37

    .line 121
    new-instance v0, Lcom/teamspeak/ts3client/d/d/a;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/d/i;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/bb;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 8688
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    .line 121
    invoke-direct {v0, v1, v2}, Lcom/teamspeak/ts3client/d/d/a;-><init>(Landroid/content/Context;Landroid/support/v4/app/bi;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/d/i;->c:Lcom/teamspeak/ts3client/d/d/a;

    .line 124
    :goto_1e
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/i;->b:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/teamspeak/ts3client/d/d/i;->c:Lcom/teamspeak/ts3client/d/d/a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 125
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/i;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 9061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 9234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 125
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/d/i;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 10061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 10267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 125
    const-string v1, "request temppasslist"

    invoke-virtual {v0, v2, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestServerTemporaryPasswordList(JLjava/lang/String;)I

    .line 126
    return-void

    .line 123
    :cond_37
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/i;->c:Lcom/teamspeak/ts3client/d/d/a;

    .line 9038
    iget-object v0, v0, Lcom/teamspeak/ts3client/d/d/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_1e
.end method

.method public final a(Landroid/app/Activity;)V
    .registers 2

    .prologue
    .line 72
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/app/Activity;)V

    .line 73
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .registers 3

    .prologue
    .line 77
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/os/Bundle;)V

    .line 78
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/d/i;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bb;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    iput-object v0, p0, Lcom/teamspeak/ts3client/d/d/i;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 79
    iput-object p0, p0, Lcom/teamspeak/ts3client/d/d/i;->f:Lcom/teamspeak/ts3client/d/d/i;

    .line 80
    return-void
.end method

.method public final a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .registers 3

    .prologue
    .line 130
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 131
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 132
    return-void
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
    .registers 19

    .prologue
    .line 35
    move-object/from16 v0, p1

    instance-of v2, v0, Lcom/teamspeak/ts3client/jni/events/ServerError;

    if-eqz v2, :cond_30

    move-object/from16 v2, p1

    .line 36
    check-cast v2, Lcom/teamspeak/ts3client/jni/events/ServerError;

    .line 1029
    iget v3, v2, Lcom/teamspeak/ts3client/jni/events/ServerError;->b:I

    .line 37
    if-nez v3, :cond_30

    .line 1041
    iget-object v3, v2, Lcom/teamspeak/ts3client/jni/events/ServerError;->c:Ljava/lang/String;

    .line 38
    const-string v4, "CreatedNewTempPw"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    .line 2041
    iget-object v2, v2, Lcom/teamspeak/ts3client/jni/events/ServerError;->c:Ljava/lang/String;

    .line 38
    const-string v3, "DeletedOldTempPw"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_30

    .line 39
    :cond_22
    invoke-virtual/range {p0 .. p0}, Lcom/teamspeak/ts3client/d/d/i;->i()Landroid/support/v4/app/bb;

    move-result-object v2

    new-instance v3, Lcom/teamspeak/ts3client/d/d/j;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/teamspeak/ts3client/d/d/j;-><init>(Lcom/teamspeak/ts3client/d/d/i;)V

    invoke-virtual {v2, v3}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 47
    :cond_30
    move-object/from16 v0, p1

    instance-of v2, v0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;

    if-eqz v2, :cond_71

    .line 48
    check-cast p1, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;

    .line 49
    new-instance v3, Lcom/teamspeak/ts3client/d/d/c;

    .line 2050
    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->a:J

    .line 3038
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->b:Ljava/lang/String;

    .line 3070
    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->c:Ljava/lang/String;

    .line 4042
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->d:Ljava/lang/String;

    .line 4046
    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->e:Ljava/lang/String;

    .line 4066
    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->f:J

    .line 5062
    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->g:J

    .line 6054
    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->h:J

    .line 6058
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList;->i:Ljava/lang/String;

    move-object/from16 v16, v0

    .line 49
    invoke-direct/range {v3 .. v16}, Lcom/teamspeak/ts3client/d/d/c;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJLjava/lang/String;)V

    .line 51
    invoke-virtual/range {p0 .. p0}, Lcom/teamspeak/ts3client/d/d/i;->i()Landroid/support/v4/app/bb;

    move-result-object v2

    new-instance v4, Lcom/teamspeak/ts3client/d/d/k;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v3}, Lcom/teamspeak/ts3client/d/d/k;-><init>(Lcom/teamspeak/ts3client/d/d/i;Lcom/teamspeak/ts3client/d/d/c;)V

    invoke-virtual {v2, v4}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 60
    :cond_71
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .registers 2

    .prologue
    .line 65
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->c(Landroid/os/Bundle;)V

    .line 66
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/d/d/i;->a()V

    .line 67
    return-void
.end method

.method public final g()V
    .registers 2

    .prologue
    .line 114
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->g()V

    .line 115
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/d/i;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 7061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 7238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 115
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->b(Lcom/teamspeak/ts3client/data/w;)V

    .line 116
    return-void
.end method
