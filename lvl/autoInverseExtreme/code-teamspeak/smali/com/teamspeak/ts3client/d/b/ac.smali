.class final Lcom/teamspeak/ts3client/d/b/ac;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/Ts3Application;

.field final synthetic b:Lcom/teamspeak/ts3client/d/b/ab;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/d/b/ab;Lcom/teamspeak/ts3client/Ts3Application;)V
    .registers 3

    .prologue
    .line 51
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/b/ac;->b:Lcom/teamspeak/ts3client/d/b/ab;

    iput-object p2, p0, Lcom/teamspeak/ts3client/d/b/ac;->a:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 11

    .prologue
    .line 56
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/ac;->b:Lcom/teamspeak/ts3client/d/b/ab;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/b/ab;->a(Lcom/teamspeak/ts3client/d/b/ab;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2d

    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/ac;->b:Lcom/teamspeak/ts3client/d/b/ab;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/b/ab;->b(Lcom/teamspeak/ts3client/d/b/ab;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2d

    .line 57
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/ac;->b:Lcom/teamspeak/ts3client/d/b/ab;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/b/ab;->a(Lcom/teamspeak/ts3client/d/b/ab;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    .line 74
    :goto_2c
    return-void

    .line 61
    :cond_2d
    const/4 v0, 0x0

    .line 63
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/ac;->b:Lcom/teamspeak/ts3client/d/b/ab;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/b/ab;->b(Lcom/teamspeak/ts3client/d/b/ab;)Landroid/widget/Spinner;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v1

    if-nez v1, :cond_4e

    .line 64
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/ac;->b:Lcom/teamspeak/ts3client/d/b/ab;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/b/ab;->a(Lcom/teamspeak/ts3client/d/b/ab;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    mul-int/lit8 v0, v0, 0x3c

    .line 66
    :cond_4e
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/ac;->b:Lcom/teamspeak/ts3client/d/b/ab;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/b/ab;->b(Lcom/teamspeak/ts3client/d/b/ab;)Landroid/widget/Spinner;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_71

    .line 67
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/ac;->b:Lcom/teamspeak/ts3client/d/b/ab;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/b/ab;->a(Lcom/teamspeak/ts3client/d/b/ab;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    mul-int/lit8 v0, v0, 0x3c

    mul-int/lit8 v0, v0, 0x3c

    .line 69
    :cond_71
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/ac;->b:Lcom/teamspeak/ts3client/d/b/ab;

    invoke-static {v1}, Lcom/teamspeak/ts3client/d/b/ab;->b(Lcom/teamspeak/ts3client/d/b/ab;)Landroid/widget/Spinner;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_db

    .line 70
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/ac;->b:Lcom/teamspeak/ts3client/d/b/ab;

    invoke-static {v0}, Lcom/teamspeak/ts3client/d/b/ab;->a(Lcom/teamspeak/ts3client/d/b/ab;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    mul-int/lit8 v0, v0, 0x3c

    mul-int/lit8 v0, v0, 0x3c

    mul-int/lit8 v0, v0, 0x18

    move v4, v0

    .line 72
    :goto_97
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/ac;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 1061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 72
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/b/ac;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 2061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2267
    iget-wide v1, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 72
    iget-object v3, p0, Lcom/teamspeak/ts3client/d/b/ac;->b:Lcom/teamspeak/ts3client/d/b/ab;

    invoke-static {v3}, Lcom/teamspeak/ts3client/d/b/ab;->c(Lcom/teamspeak/ts3client/d/b/ab;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v3

    .line 3188
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/c;->b:Ljava/lang/String;

    .line 72
    int-to-long v4, v4

    iget-object v6, p0, Lcom/teamspeak/ts3client/d/b/ac;->b:Lcom/teamspeak/ts3client/d/b/ab;

    invoke-static {v6}, Lcom/teamspeak/ts3client/d/b/ab;->d(Lcom/teamspeak/ts3client/d/b/ab;)Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Ban "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/teamspeak/ts3client/d/b/ac;->b:Lcom/teamspeak/ts3client/d/b/ab;

    invoke-static {v8}, Lcom/teamspeak/ts3client/d/b/ab;->c(Lcom/teamspeak/ts3client/d/b/ab;)Lcom/teamspeak/ts3client/data/c;

    move-result-object v8

    .line 3235
    iget v8, v8, Lcom/teamspeak/ts3client/data/c;->c:I

    .line 72
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v0 .. v7}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_banclientUID(JLjava/lang/String;JLjava/lang/String;Ljava/lang/String;)I

    .line 73
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/b/ac;->b:Lcom/teamspeak/ts3client/d/b/ab;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/d/b/ab;->b()V

    goto/16 :goto_2c

    :cond_db
    move v4, v0

    goto :goto_97
.end method
