.class final Lcom/teamspeak/ts3client/d/d/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/d/d/n;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/d/d/n;)V
    .registers 2

    .prologue
    .line 64
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/d/p;->a:Lcom/teamspeak/ts3client/d/d/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 6

    .prologue
    .line 68
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 69
    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 70
    const-string v1, "android.intent.extra.SUBJECT"

    const-string v2, "TeamSpeak 3 Temporary Password"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 71
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "You are Invited to join "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/d/p;->a:Lcom/teamspeak/ts3client/d/d/n;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/d/n;->a(Lcom/teamspeak/ts3client/d/d/n;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 1061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 71
    invoke-virtual {v2}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "!\n\nts3server://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/d/p;->a:Lcom/teamspeak/ts3client/d/d/n;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/d/n;->a(Lcom/teamspeak/ts3client/d/d/n;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 2061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2259
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->t:Ljava/lang/String;

    .line 71
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "?password="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/d/p;->a:Lcom/teamspeak/ts3client/d/d/n;

    iget-object v2, v2, Lcom/teamspeak/ts3client/d/d/n;->at:Lcom/teamspeak/ts3client/d/d/c;

    .line 3050
    iget-object v2, v2, Lcom/teamspeak/ts3client/d/d/c;->c:Ljava/lang/String;

    .line 71
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 72
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\nhttp://www.teamspeak.com/invite/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/d/p;->a:Lcom/teamspeak/ts3client/d/d/n;

    invoke-static {v2}, Lcom/teamspeak/ts3client/d/d/n;->a(Lcom/teamspeak/ts3client/d/d/n;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 3061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3259
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->t:Ljava/lang/String;

    .line 72
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/?password="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/d/d/p;->a:Lcom/teamspeak/ts3client/d/d/n;

    iget-object v2, v2, Lcom/teamspeak/ts3client/d/d/n;->at:Lcom/teamspeak/ts3client/d/d/c;

    .line 4050
    iget-object v2, v2, Lcom/teamspeak/ts3client/d/d/c;->c:Ljava/lang/String;

    .line 72
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\nYou can Download TeamSpeak 3 for free on http://www.teamspeak.com/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 73
    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 74
    iget-object v1, p0, Lcom/teamspeak/ts3client/d/d/p;->a:Lcom/teamspeak/ts3client/d/d/n;

    .line 4913
    iget-object v2, v1, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    if-nez v2, :cond_af

    .line 4914
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not attached to Activity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4916
    :cond_af
    iget-object v2, v1, Landroid/support/v4/app/Fragment;->N:Landroid/support/v4/app/bh;

    const/4 v3, -0x1

    invoke-virtual {v2, v1, v0, v3}, Landroid/support/v4/app/bh;->a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V

    .line 75
    return-void
.end method
