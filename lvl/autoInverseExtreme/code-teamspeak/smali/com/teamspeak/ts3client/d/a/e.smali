.class final Lcom/teamspeak/ts3client/d/a/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/d/a/a;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/d/a/a;)V
    .registers 2

    .prologue
    .line 114
    iput-object p1, p0, Lcom/teamspeak/ts3client/d/a/e;->a:Lcom/teamspeak/ts3client/d/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 14

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 118
    .line 122
    invoke-static {}, Lcom/teamspeak/ts3client/d/a/a;->y()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 1061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1165
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->f:Lcom/teamspeak/ts3client/data/g;

    .line 2080
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/g;->a:Ljava/util/List;

    .line 122
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    :goto_14
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_56

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/a;

    .line 2087
    iget-wide v8, v0, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 123
    iget-object v10, p0, Lcom/teamspeak/ts3client/d/a/e;->a:Lcom/teamspeak/ts3client/d/a/a;

    invoke-static {v10}, Lcom/teamspeak/ts3client/d/a/a;->a(Lcom/teamspeak/ts3client/d/a/a;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v10

    .line 3087
    iget-wide v10, v10, Lcom/teamspeak/ts3client/data/a;->b:J

    .line 123
    cmp-long v8, v8, v10

    if-nez v8, :cond_3c

    .line 3095
    iget v4, v0, Lcom/teamspeak/ts3client/data/a;->f:I

    .line 126
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/a;->j()Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    add-int/2addr v0, v3

    move v3, v0

    move v5, v6

    .line 127
    goto :goto_14

    .line 129
    :cond_3c
    if-eqz v5, :cond_c5

    .line 4095
    iget v8, v0, Lcom/teamspeak/ts3client/data/a;->f:I

    .line 130
    if-eqz v8, :cond_56

    .line 5095
    iget v8, v0, Lcom/teamspeak/ts3client/data/a;->f:I

    .line 130
    if-eq v8, v4, :cond_56

    .line 133
    add-int/lit8 v1, v1, 0x1

    .line 134
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/a;->j()Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    add-int/2addr v3, v0

    move v0, v1

    move v1, v3

    :goto_53
    move v3, v1

    move v1, v0

    .line 136
    goto :goto_14

    .line 138
    :cond_56
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/teamspeak/ts3client/d/a/e;->a:Lcom/teamspeak/ts3client/d/a/a;

    invoke-virtual {v4}, Lcom/teamspeak/ts3client/d/a/a;->i()Landroid/support/v4/app/bb;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 139
    invoke-static {v0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/app/Dialog;)V

    .line 140
    if-lez v3, :cond_af

    .line 141
    const-string v1, "dialog.channel.delete.warn1"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 147
    :goto_73
    const-string v1, "dialog.channel.delete.dialog.text"

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/teamspeak/ts3client/d/a/e;->a:Lcom/teamspeak/ts3client/d/a/a;

    invoke-static {v4}, Lcom/teamspeak/ts3client/d/a/a;->a(Lcom/teamspeak/ts3client/d/a/a;)Lcom/teamspeak/ts3client/data/a;

    move-result-object v4

    .line 5103
    iget-object v4, v4, Lcom/teamspeak/ts3client/data/a;->a:Ljava/lang/String;

    .line 147
    aput-object v4, v3, v2

    invoke-static {v1, v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 148
    const/4 v1, -0x2

    const-string v2, "button.cancel"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/teamspeak/ts3client/d/a/f;

    invoke-direct {v3, p0}, Lcom/teamspeak/ts3client/d/a/f;-><init>(Lcom/teamspeak/ts3client/d/a/e;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 155
    const/4 v1, -0x1

    const-string v2, "button.delete"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/teamspeak/ts3client/d/a/g;

    invoke-direct {v3, p0}, Lcom/teamspeak/ts3client/d/a/g;-><init>(Lcom/teamspeak/ts3client/d/a/e;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 163
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 164
    iget-object v0, p0, Lcom/teamspeak/ts3client/d/a/e;->a:Lcom/teamspeak/ts3client/d/a/a;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/d/a/a;->b()V

    .line 165
    return-void

    .line 142
    :cond_af
    if-lez v1, :cond_bb

    .line 143
    const-string v1, "dialog.channel.delete.warn2"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_73

    .line 145
    :cond_bb
    const-string v1, "dialog.channel.delete.warn3"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_73

    :cond_c5
    move v0, v1

    move v1, v3

    goto :goto_53
.end method
