.class final Lcom/teamspeak/ts3client/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# instance fields
.field private a:Lcom/teamspeak/ts3client/Ts3Application;


# direct methods
.method public constructor <init>(Lcom/teamspeak/ts3client/Ts3Application;)V
    .registers 2

    .prologue
    .line 1893
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1894
    iput-object p1, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 1895
    return-void
.end method


# virtual methods
.method public final onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .registers 11

    .prologue
    const/16 v7, 0xa

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1899
    const-string v0, "audio_ptt"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_79

    .line 1900
    iget-object v0, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 2093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1900
    const-string v1, "audio_ptt"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 1901
    iget-object v1, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 3085
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 1901
    sget-object v2, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Setting PTT: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 1902
    if-eqz v0, :cond_197

    .line 1903
    iget-object v0, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 4061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 4234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1903
    iget-object v1, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 5061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 5267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1903
    sget-object v1, Lcom/teamspeak/ts3client/jni/d;->k:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v2, v3, v1, v5}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 1904
    iget-object v0, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 6061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 6234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1904
    iget-object v1, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 7061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 7267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1904
    const-string v1, "vad"

    const-string v4, "false"

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I

    .line 1905
    iget-object v0, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 8061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 8234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1905
    iget-object v1, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 9061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 9267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1905
    const-string v1, "voiceactivation_level"

    const-string v4, "-50"

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I

    .line 1906
    iget-object v0, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 10061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 10206
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 1906
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->H()V

    .line 1914
    :goto_70
    iget-object v0, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 19061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 19198
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->l:Lcom/teamspeak/ts3client/ConnectionBackground;

    .line 1914
    invoke-static {v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->f(Lcom/teamspeak/ts3client/ConnectionBackground;)V

    .line 1916
    :cond_79
    const-string v0, "voiceactivation_level"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b8

    .line 1917
    iget-object v0, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 20093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1917
    const-string v1, "voiceactivation_level"

    invoke-interface {v0, v1, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1918
    iget-object v1, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 21085
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 1918
    sget-object v2, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Setting voiceactivation_level: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 1919
    iget-object v1, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 22061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 22234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1919
    iget-object v2, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 23061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 23267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1919
    const-string v4, "voiceactivation_level"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I

    .line 1921
    :cond_b8
    const-string v0, "audio_bt"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ed

    .line 1922
    iget-object v0, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 24093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1922
    const-string v1, "audio_bt"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 1923
    iget-object v1, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 25085
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 1923
    sget-object v2, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Setting BT:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 1924
    if-eqz v0, :cond_1e5

    .line 1925
    iget-object v0, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 26061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 26157
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->b:Lcom/teamspeak/ts3client/a/k;

    .line 1925
    invoke-virtual {v0, v5}, Lcom/teamspeak/ts3client/a/k;->b(Z)V

    .line 1929
    :cond_ed
    :goto_ed
    const-string v0, "audio_use_tts"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 1932
    const-string v0, "call_setaway"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 1935
    const-string v0, "call_awaymessage"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 1938
    const-string v0, "show_squeryclient"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10d

    .line 1939
    iget-object v0, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 28061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 28206
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 1939
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->z()V

    .line 1941
    :cond_10d
    const-string v0, "audio_handfree"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_128

    .line 1942
    iget-object v0, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 29093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1942
    const-string v1, "audio_handfree"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1f0

    .line 1943
    invoke-static {}, Lcom/teamspeak/ts3client/a/j;->a()Lcom/teamspeak/ts3client/a/j;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/teamspeak/ts3client/a/j;->a(I)V

    .line 1947
    :cond_128
    :goto_128
    const-string v0, "use_proximity"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_161

    .line 1948
    iget-object v0, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 30093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1948
    const-string v1, "use_proximity"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 1949
    iget-object v1, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 30123
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->m:Landroid/os/PowerManager$WakeLock;

    .line 1949
    if-eqz v1, :cond_161

    .line 1951
    iget-object v1, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 31107
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 1951
    invoke-virtual {v1}, Lcom/teamspeak/ts3client/StartGUIFragment;->j()V

    .line 1952
    iget-object v1, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 31123
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->m:Landroid/os/PowerManager$WakeLock;

    .line 1952
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_158

    .line 1953
    iget-object v1, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 32123
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->m:Landroid/os/PowerManager$WakeLock;

    .line 1953
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1955
    :cond_158
    if-eqz v0, :cond_161

    .line 1957
    iget-object v0, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 33107
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->f:Lcom/teamspeak/ts3client/StartGUIFragment;

    .line 1957
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/StartGUIFragment;->k()V

    .line 1961
    :cond_161
    const-string v0, "whisper"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_202

    .line 1962
    iget-object v0, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 34061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 34092
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->w:Lcom/a/b/d/bw;

    .line 1962
    invoke-interface {v0}, Lcom/a/b/d/bw;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 1963
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_177
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1f9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1964
    iget-object v2, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 35061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 35234
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1964
    iget-object v3, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 36061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 36267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1964
    invoke-virtual {v2, v4, v5, v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_removeFromAllowedWhispersFrom(JI)I

    goto :goto_177

    .line 1908
    :cond_197
    iget-object v0, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 11061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 11157
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->b:Lcom/teamspeak/ts3client/a/k;

    .line 1908
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/a/k;->d()V

    .line 1909
    iget-object v0, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 12061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 12234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1909
    iget-object v1, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 13061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 13267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1909
    sget-object v1, Lcom/teamspeak/ts3client/jni/d;->k:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v0, v2, v3, v1, v6}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 1910
    iget-object v0, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 14061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 14234
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1910
    iget-object v1, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 15061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 15267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1910
    const-string v1, "vad"

    const-string v4, "true"

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I

    .line 1911
    iget-object v0, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 16093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1911
    const-string v1, "voiceactivation_level"

    invoke-interface {v0, v1, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1912
    iget-object v1, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 17061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 17234
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1912
    iget-object v2, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 18061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 18267
    iget-wide v2, v2, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1912
    const-string v4, "voiceactivation_level"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_70

    .line 1927
    :cond_1e5
    iget-object v0, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 27061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 27157
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->b:Lcom/teamspeak/ts3client/a/k;

    .line 1927
    invoke-virtual {v0, v6}, Lcom/teamspeak/ts3client/a/k;->b(Z)V

    goto/16 :goto_ed

    .line 1945
    :cond_1f0
    invoke-static {}, Lcom/teamspeak/ts3client/a/j;->a()Lcom/teamspeak/ts3client/a/j;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/teamspeak/ts3client/a/j;->b(I)V

    goto/16 :goto_128

    .line 1966
    :cond_1f9
    iget-object v0, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 37061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 37092
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->w:Lcom/a/b/d/bw;

    .line 1966
    invoke-interface {v0}, Lcom/a/b/d/bw;->clear()V

    .line 1968
    :cond_202
    const-string v0, "talk_notification"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21e

    .line 1969
    iget-object v0, p0, Lcom/teamspeak/ts3client/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 37093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1969
    const-string v1, "talk_notification"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(Z)Z

    .line 1970
    invoke-static {}, Lcom/teamspeak/ts3client/ConnectionBackground;->e()Z

    move-result v0

    invoke-static {v0}, Lcom/teamspeak/ts3client/ConnectionBackground;->a(Z)V

    .line 1972
    :cond_21e
    return-void
.end method
