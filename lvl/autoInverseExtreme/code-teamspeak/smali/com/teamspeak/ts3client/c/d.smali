.class final Lcom/teamspeak/ts3client/c/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/teamspeak/ts3client/c/c;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/c/c;I)V
    .registers 3

    .prologue
    .line 85
    iput-object p1, p0, Lcom/teamspeak/ts3client/c/d;->b:Lcom/teamspeak/ts3client/c/c;

    iput p2, p0, Lcom/teamspeak/ts3client/c/d;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 9

    .prologue
    const/4 v6, 0x1

    .line 90
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/teamspeak/ts3client/c/d;->b:Lcom/teamspeak/ts3client/c/c;

    invoke-static {v1}, Lcom/teamspeak/ts3client/c/c;->a(Lcom/teamspeak/ts3client/c/c;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 91
    invoke-static {v1}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/app/Dialog;)V

    .line 92
    const-string v0, "contact.delete.info"

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 93
    const-string v2, "contact.delete.text"

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/teamspeak/ts3client/c/d;->b:Lcom/teamspeak/ts3client/c/c;

    invoke-static {v0}, Lcom/teamspeak/ts3client/c/c;->b(Lcom/teamspeak/ts3client/c/c;)Ljava/util/ArrayList;

    move-result-object v0

    iget v5, p0, Lcom/teamspeak/ts3client/c/d;->a:I

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/c/a;

    .line 1030
    iget-object v0, v0, Lcom/teamspeak/ts3client/c/a;->a:Ljava/lang/String;

    .line 93
    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 94
    const/4 v0, -0x1

    const-string v2, "button.delete"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/teamspeak/ts3client/c/e;

    invoke-direct {v3, p0}, Lcom/teamspeak/ts3client/c/e;-><init>(Lcom/teamspeak/ts3client/c/d;)V

    invoke-virtual {v1, v0, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 111
    const/4 v0, -0x2

    const-string v2, "button.cancel"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/teamspeak/ts3client/c/f;

    invoke-direct {v3, p0, v1}, Lcom/teamspeak/ts3client/c/f;-><init>(Lcom/teamspeak/ts3client/c/d;Landroid/app/AlertDialog;)V

    invoke-virtual {v1, v0, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 118
    invoke-virtual {v1, v6}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 119
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 120
    return-void
.end method
