.class public final Lcom/teamspeak/ts3client/c/b;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"


# instance fields
.field private a:Lcom/teamspeak/ts3client/Ts3Application;

.field private b:Landroid/widget/ListView;

.field private c:Lcom/teamspeak/ts3client/c/c;

.field private d:Lcom/teamspeak/ts3client/data/b/c;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 5

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/c/b;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bb;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    iput-object v0, p0, Lcom/teamspeak/ts3client/c/b;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 50
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/c/b;->n()V

    .line 51
    iget-object v0, p0, Lcom/teamspeak/ts3client/c/b;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 2204
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->o:Landroid/support/v7/app/a;

    .line 51
    const-string v1, "menu.contact"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->b(Ljava/lang/CharSequence;)V

    .line 52
    iget-object v0, p0, Lcom/teamspeak/ts3client/c/b;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 3081
    iput-object p0, v0, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 53
    const v0, 0x7f030032

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 54
    const v0, 0x7f0c0174

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/c/b;->b:Landroid/widget/ListView;

    .line 55
    return-object v1
.end method

.method public final a()V
    .registers 6

    .prologue
    .line 66
    new-instance v0, Lcom/teamspeak/ts3client/c/c;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/c/b;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    .line 3688
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->M:Landroid/support/v4/app/bl;

    .line 66
    invoke-direct {v0, v1, v2}, Lcom/teamspeak/ts3client/c/c;-><init>(Landroid/content/Context;Landroid/support/v4/app/bi;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/c/b;->c:Lcom/teamspeak/ts3client/c/c;

    .line 67
    iget-object v0, p0, Lcom/teamspeak/ts3client/c/b;->d:Lcom/teamspeak/ts3client/data/b/c;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/b/c;->b()Ljava/util/ArrayList;

    move-result-object v2

    .line 68
    if-eqz v2, :cond_36

    .line 69
    const/4 v0, 0x0

    move v1, v0

    :goto_17
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_36

    .line 70
    iget-object v3, p0, Lcom/teamspeak/ts3client/c/b;->c:Lcom/teamspeak/ts3client/c/c;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/c/a;

    .line 4041
    iget-object v4, v3, Lcom/teamspeak/ts3client/c/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_32

    .line 4042
    iget-object v3, v3, Lcom/teamspeak/ts3client/c/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 69
    :cond_32
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_17

    .line 72
    :cond_36
    iget-object v0, p0, Lcom/teamspeak/ts3client/c/b;->b:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/teamspeak/ts3client/c/b;->c:Lcom/teamspeak/ts3client/c/c;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 73
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .registers 3

    .prologue
    .line 43
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/os/Bundle;)V

    .line 44
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/c/b;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bb;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    iput-object v0, p0, Lcom/teamspeak/ts3client/c/b;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 45
    return-void
.end method

.method public final a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .registers 3

    .prologue
    .line 28
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 29
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 30
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .registers 3

    .prologue
    .line 35
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->c(Landroid/os/Bundle;)V

    .line 1042
    sget-object v0, Lcom/teamspeak/ts3client/data/b/c;->a:Lcom/teamspeak/ts3client/data/b/c;

    .line 36
    iput-object v0, p0, Lcom/teamspeak/ts3client/c/b;->d:Lcom/teamspeak/ts3client/data/b/c;

    .line 37
    iget-object v0, p0, Lcom/teamspeak/ts3client/c/b;->d:Lcom/teamspeak/ts3client/data/b/c;

    .line 1263
    iput-object p0, v0, Lcom/teamspeak/ts3client/data/b/c;->c:Lcom/teamspeak/ts3client/c/b;

    .line 38
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/c/b;->a()V

    .line 39
    return-void
.end method

.method public final g()V
    .registers 3

    .prologue
    .line 61
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->g()V

    .line 62
    iget-object v0, p0, Lcom/teamspeak/ts3client/c/b;->d:Lcom/teamspeak/ts3client/data/b/c;

    .line 3259
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/teamspeak/ts3client/data/b/c;->c:Lcom/teamspeak/ts3client/c/b;

    .line 63
    return-void
.end method
