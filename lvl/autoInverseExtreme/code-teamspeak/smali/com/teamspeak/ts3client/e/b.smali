.class public final Lcom/teamspeak/ts3client/e/b;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/data/b/e;


# instance fields
.field private a:Lcom/teamspeak/ts3client/Ts3Application;

.field private b:Lcom/teamspeak/ts3client/data/b/f;

.field private c:Lcom/teamspeak/ts3client/e/e;

.field private d:Landroid/widget/ListView;

.field private e:Lcom/teamspeak/ts3client/customs/FloatingButton;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 34
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/e/b;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bb;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    iput-object v0, p0, Lcom/teamspeak/ts3client/e/b;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 64
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/e/b;->n()V

    .line 65
    iget-object v0, p0, Lcom/teamspeak/ts3client/e/b;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 1204
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->o:Landroid/support/v7/app/a;

    .line 65
    const-string v1, "menu.identity"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->b(Ljava/lang/CharSequence;)V

    .line 66
    iget-object v0, p0, Lcom/teamspeak/ts3client/e/b;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 2081
    iput-object p0, v0, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 67
    const v0, 0x7f030041

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 68
    const v0, 0x7f0c0186

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/teamspeak/ts3client/e/b;->d:Landroid/widget/ListView;

    .line 69
    const v0, 0x7f0c0187

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/customs/FloatingButton;

    iput-object v0, p0, Lcom/teamspeak/ts3client/e/b;->e:Lcom/teamspeak/ts3client/customs/FloatingButton;

    .line 70
    iget-object v0, p0, Lcom/teamspeak/ts3client/e/b;->e:Lcom/teamspeak/ts3client/customs/FloatingButton;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/e/b;->j()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02007e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 71
    iget-object v0, p0, Lcom/teamspeak/ts3client/e/b;->e:Lcom/teamspeak/ts3client/customs/FloatingButton;

    new-instance v2, Lcom/teamspeak/ts3client/e/c;

    invoke-direct {v2, p0, p1}, Lcom/teamspeak/ts3client/e/c;-><init>(Lcom/teamspeak/ts3client/e/b;Landroid/view/LayoutInflater;)V

    invoke-virtual {v0, v2}, Lcom/teamspeak/ts3client/customs/FloatingButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    return-object v1
.end method

.method public final a()V
    .registers 1

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/e/b;->b()V

    .line 45
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .registers 3

    .prologue
    .line 56
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/os/Bundle;)V

    .line 57
    invoke-static {}, Lcom/teamspeak/ts3client/data/b/f;->a()Lcom/teamspeak/ts3client/data/b/f;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/e/b;->b:Lcom/teamspeak/ts3client/data/b/f;

    .line 58
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/b/f;->a(Lcom/teamspeak/ts3client/data/b/e;)V

    .line 59
    return-void
.end method

.method public final a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .registers 3

    .prologue
    .line 38
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 39
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 40
    return-void
.end method

.method public final b()V
    .registers 6

    .prologue
    .line 92
    new-instance v0, Lcom/teamspeak/ts3client/e/e;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/e/b;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/teamspeak/ts3client/e/e;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/e/b;->c:Lcom/teamspeak/ts3client/e/e;

    .line 93
    iget-object v0, p0, Lcom/teamspeak/ts3client/e/b;->b:Lcom/teamspeak/ts3client/data/b/f;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/b/f;->c()Ljava/util/ArrayList;

    move-result-object v0

    .line 94
    if-eqz v0, :cond_19

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_55

    .line 95
    :cond_19
    iget-object v0, p0, Lcom/teamspeak/ts3client/e/b;->b:Lcom/teamspeak/ts3client/data/b/f;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/b/f;->b()V

    .line 96
    iget-object v0, p0, Lcom/teamspeak/ts3client/e/b;->b:Lcom/teamspeak/ts3client/data/b/f;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/b/f;->c()Ljava/util/ArrayList;

    move-result-object v0

    move-object v2, v0

    .line 98
    :goto_25
    if-eqz v2, :cond_48

    .line 99
    const/4 v0, 0x0

    move v1, v0

    :goto_29
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_48

    .line 100
    iget-object v3, p0, Lcom/teamspeak/ts3client/e/b;->c:Lcom/teamspeak/ts3client/e/e;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/e/a;

    .line 3036
    iget-object v4, v3, Lcom/teamspeak/ts3client/e/e;->a:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_44

    .line 3037
    iget-object v3, v3, Lcom/teamspeak/ts3client/e/e;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 99
    :cond_44
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_29

    .line 103
    :cond_48
    iget-object v0, p0, Lcom/teamspeak/ts3client/e/b;->d:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/teamspeak/ts3client/e/b;->c:Lcom/teamspeak/ts3client/e/e;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 104
    iget-object v0, p0, Lcom/teamspeak/ts3client/e/b;->c:Lcom/teamspeak/ts3client/e/e;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/e/e;->notifyDataSetChanged()V

    .line 105
    return-void

    :cond_55
    move-object v2, v0

    goto :goto_25
.end method

.method public final c(Landroid/os/Bundle;)V
    .registers 2

    .prologue
    .line 50
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->c(Landroid/os/Bundle;)V

    .line 51
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/e/b;->b()V

    .line 52
    return-void
.end method
