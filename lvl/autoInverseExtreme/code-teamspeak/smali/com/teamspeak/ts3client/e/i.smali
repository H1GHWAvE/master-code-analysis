.class final Lcom/teamspeak/ts3client/e/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/teamspeak/ts3client/e/e;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/e/e;I)V
    .registers 3

    .prologue
    .line 98
    iput-object p1, p0, Lcom/teamspeak/ts3client/e/i;->b:Lcom/teamspeak/ts3client/e/e;

    iput p2, p0, Lcom/teamspeak/ts3client/e/i;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 14

    .prologue
    .line 102
    invoke-static {}, Lcom/teamspeak/ts3client/data/b/f;->a()Lcom/teamspeak/ts3client/data/b/f;

    move-result-object v9

    iget-object v0, p0, Lcom/teamspeak/ts3client/e/i;->b:Lcom/teamspeak/ts3client/e/e;

    invoke-static {v0}, Lcom/teamspeak/ts3client/e/e;->b(Lcom/teamspeak/ts3client/e/e;)Ljava/util/ArrayList;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/e/i;->a:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/e/a;

    .line 1025
    iget v10, v0, Lcom/teamspeak/ts3client/e/a;->a:I

    .line 1195
    new-instance v11, Lcom/teamspeak/ts3client/e/a;

    invoke-direct {v11}, Lcom/teamspeak/ts3client/e/a;-><init>()V

    .line 1199
    :try_start_19
    iget-object v0, v9, Lcom/teamspeak/ts3client/data/b/f;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "ident"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "ident_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "name"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "identity"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "nickname"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "defaultid"

    aput-object v4, v2, v3

    const-string v3, "defaultid=1"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1200
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_83

    .line 1201
    const-string v1, "name"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2045
    iput-object v1, v11, Lcom/teamspeak/ts3client/e/a;->b:Ljava/lang/String;

    .line 1202
    const-string v1, "identity"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 3037
    iput-object v1, v11, Lcom/teamspeak/ts3client/e/a;->c:Ljava/lang/String;

    .line 1203
    const-string v1, "nickname"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 3053
    iput-object v1, v11, Lcom/teamspeak/ts3client/e/a;->d:Ljava/lang/String;

    .line 3061
    const/4 v1, 0x0

    iput-boolean v1, v11, Lcom/teamspeak/ts3client/e/a;->e:Z

    .line 1205
    const-string v1, "ident_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 4029
    iput v1, v11, Lcom/teamspeak/ts3client/e/a;->a:I

    .line 5025
    iget v1, v11, Lcom/teamspeak/ts3client/e/a;->a:I

    .line 1206
    int-to-long v2, v1

    invoke-virtual {v9, v2, v3, v11}, Lcom/teamspeak/ts3client/data/b/f;->a(JLcom/teamspeak/ts3client/e/a;)Z

    .line 1208
    :cond_83
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1209
    new-instance v11, Lcom/teamspeak/ts3client/e/a;

    invoke-direct {v11}, Lcom/teamspeak/ts3client/e/a;-><init>()V

    .line 1210
    iget-object v0, v9, Lcom/teamspeak/ts3client/data/b/f;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "ident"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "ident_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "name"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "identity"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "nickname"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "defaultid"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ident_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1211
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_102

    .line 1212
    const-string v1, "name"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 5045
    iput-object v1, v11, Lcom/teamspeak/ts3client/e/a;->b:Ljava/lang/String;

    .line 1213
    const-string v1, "identity"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 6037
    iput-object v1, v11, Lcom/teamspeak/ts3client/e/a;->c:Ljava/lang/String;

    .line 1214
    const-string v1, "nickname"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 6053
    iput-object v1, v11, Lcom/teamspeak/ts3client/e/a;->d:Ljava/lang/String;

    .line 6061
    const/4 v1, 0x1

    iput-boolean v1, v11, Lcom/teamspeak/ts3client/e/a;->e:Z

    .line 1216
    const-string v1, "ident_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 7029
    iput v1, v11, Lcom/teamspeak/ts3client/e/a;->a:I

    .line 8025
    iget v1, v11, Lcom/teamspeak/ts3client/e/a;->a:I

    .line 1217
    int-to-long v2, v1

    invoke-virtual {v9, v2, v3, v11}, Lcom/teamspeak/ts3client/data/b/f;->a(JLcom/teamspeak/ts3client/e/a;)Z

    .line 1219
    :cond_102
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_105
    .catch Landroid/database/SQLException; {:try_start_19 .. :try_end_105} :catch_106

    .line 1221
    :goto_105
    return-void

    .line 103
    :catch_106
    move-exception v0

    goto :goto_105
.end method
