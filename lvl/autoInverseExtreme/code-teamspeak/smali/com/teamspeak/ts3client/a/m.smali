.class final Lcom/teamspeak/ts3client/a/m;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/a/k;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/a/k;)V
    .registers 2

    .prologue
    .line 255
    iput-object p1, p0, Lcom/teamspeak/ts3client/a/m;->a:Lcom/teamspeak/ts3client/a/k;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 5

    .prologue
    .line 259
    :try_start_0
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/m;->a:Lcom/teamspeak/ts3client/a/k;

    .line 1016
    iget-object v0, v0, Lcom/teamspeak/ts3client/a/k;->c:Landroid/media/AudioRecord;

    .line 259
    if-nez v0, :cond_7

    .line 271
    :cond_6
    :goto_6
    return-void

    .line 262
    :cond_7
    const/16 v0, -0x13

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 263
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/m;->a:Lcom/teamspeak/ts3client/a/k;

    .line 2016
    iget-object v0, v0, Lcom/teamspeak/ts3client/a/k;->c:Landroid/media/AudioRecord;

    .line 263
    invoke-virtual {v0}, Landroid/media/AudioRecord;->startRecording()V

    .line 264
    :goto_13
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/m;->a:Lcom/teamspeak/ts3client/a/k;

    .line 3016
    iget-boolean v0, v0, Lcom/teamspeak/ts3client/a/k;->a:Z

    .line 264
    if-eqz v0, :cond_6

    .line 265
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/m;->a:Lcom/teamspeak/ts3client/a/k;

    .line 4016
    iget-object v0, v0, Lcom/teamspeak/ts3client/a/k;->c:Landroid/media/AudioRecord;

    .line 265
    iget-object v1, p0, Lcom/teamspeak/ts3client/a/m;->a:Lcom/teamspeak/ts3client/a/k;

    .line 5016
    iget-object v1, v1, Lcom/teamspeak/ts3client/a/k;->b:[S

    .line 265
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/teamspeak/ts3client/a/m;->a:Lcom/teamspeak/ts3client/a/k;

    .line 6016
    iget v3, v3, Lcom/teamspeak/ts3client/a/k;->i:I

    .line 265
    div-int/lit8 v3, v3, 0x64

    mul-int/lit8 v3, v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioRecord;->read([SII)I

    .line 266
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/m;->a:Lcom/teamspeak/ts3client/a/k;

    .line 7016
    iget-object v0, v0, Lcom/teamspeak/ts3client/a/k;->d:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 266
    const-string v1, ""

    iget-object v2, p0, Lcom/teamspeak/ts3client/a/m;->a:Lcom/teamspeak/ts3client/a/k;

    .line 8016
    iget-object v2, v2, Lcom/teamspeak/ts3client/a/k;->b:[S

    .line 266
    iget-object v3, p0, Lcom/teamspeak/ts3client/a/m;->a:Lcom/teamspeak/ts3client/a/k;

    .line 9016
    iget v3, v3, Lcom/teamspeak/ts3client/a/k;->i:I

    .line 266
    div-int/lit8 v3, v3, 0x64

    mul-int/lit8 v3, v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_processCustomCaptureData(Ljava/lang/String;[SI)I
    :try_end_42
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_42} :catch_43

    goto :goto_13

    .line 268
    :catch_43
    move-exception v0

    .line 269
    iget-object v1, p0, Lcom/teamspeak/ts3client/a/m;->a:Lcom/teamspeak/ts3client/a/k;

    .line 10016
    iget-object v1, v1, Lcom/teamspeak/ts3client/a/k;->h:Lcom/teamspeak/ts3client/Ts3Application;

    .line 10085
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 269
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_6
.end method
