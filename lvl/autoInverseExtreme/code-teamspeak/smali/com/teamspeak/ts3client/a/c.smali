.class final Lcom/teamspeak/ts3client/a/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/a/a;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/a/a;)V
    .registers 2

    .prologue
    .line 178
    iput-object p1, p0, Lcom/teamspeak/ts3client/a/c;->a:Lcom/teamspeak/ts3client/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 6

    .prologue
    .line 182
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/c;->a:Lcom/teamspeak/ts3client/a/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/a/a;->b(Lcom/teamspeak/ts3client/a/a;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 183
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/c;->a:Lcom/teamspeak/ts3client/a/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/a/a;->c(Lcom/teamspeak/ts3client/a/a;)Z

    .line 184
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/c;->a:Lcom/teamspeak/ts3client/a/a;

    invoke-static {}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->b()Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/teamspeak/ts3client/a/a;->a(Lcom/teamspeak/ts3client/a/a;Lcom/teamspeak/ts3client/jni/Ts3Jni;)Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 185
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/c;->a:Lcom/teamspeak/ts3client/a/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/a/a;->d(Lcom/teamspeak/ts3client/a/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 1061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 185
    if-eqz v0, :cond_49

    .line 186
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/c;->a:Lcom/teamspeak/ts3client/a/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/a/a;->e(Lcom/teamspeak/ts3client/a/a;)Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/a/c;->a:Lcom/teamspeak/ts3client/a/a;

    invoke-static {v1}, Lcom/teamspeak/ts3client/a/a;->d(Lcom/teamspeak/ts3client/a/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 2061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 186
    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_closeCaptureDevice(J)I

    .line 187
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/c;->a:Lcom/teamspeak/ts3client/a/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/a/a;->e(Lcom/teamspeak/ts3client/a/a;)Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/a/c;->a:Lcom/teamspeak/ts3client/a/a;

    invoke-static {v1}, Lcom/teamspeak/ts3client/a/a;->d(Lcom/teamspeak/ts3client/a/a;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 3061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3267
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 187
    invoke-virtual {v0, v2, v3}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_closePlaybackDevice(J)I

    .line 189
    :cond_49
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/c;->a:Lcom/teamspeak/ts3client/a/a;

    iget-object v1, p0, Lcom/teamspeak/ts3client/a/c;->a:Lcom/teamspeak/ts3client/a/a;

    invoke-static {v1}, Lcom/teamspeak/ts3client/a/a;->e(Lcom/teamspeak/ts3client/a/a;)Lcom/teamspeak/ts3client/jni/Ts3Jni;

    move-result-object v1

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_spawnNewServerConnectionHandler()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/teamspeak/ts3client/a/a;->a(Lcom/teamspeak/ts3client/a/a;J)J

    .line 190
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/c;->a:Lcom/teamspeak/ts3client/a/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/a/a;->f(Lcom/teamspeak/ts3client/a/a;)Z

    .line 191
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/c;->a:Lcom/teamspeak/ts3client/a/a;

    iget-object v1, p0, Lcom/teamspeak/ts3client/a/c;->a:Lcom/teamspeak/ts3client/a/a;

    invoke-static {v1}, Lcom/teamspeak/ts3client/a/a;->g(Lcom/teamspeak/ts3client/a/a;)Landroid/widget/Spinner;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/a/c;->a:Lcom/teamspeak/ts3client/a/a;

    invoke-static {v2}, Lcom/teamspeak/ts3client/a/a;->h(Lcom/teamspeak/ts3client/a/a;)Landroid/widget/Spinner;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/teamspeak/ts3client/a/a;->a(Lcom/teamspeak/ts3client/a/a;Landroid/widget/Spinner;Landroid/widget/Spinner;)V

    .line 192
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/c;->a:Lcom/teamspeak/ts3client/a/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/a/a;->i(Lcom/teamspeak/ts3client/a/a;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 193
    iget-object v0, p0, Lcom/teamspeak/ts3client/a/c;->a:Lcom/teamspeak/ts3client/a/a;

    invoke-static {v0}, Lcom/teamspeak/ts3client/a/a;->j(Lcom/teamspeak/ts3client/a/a;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 194
    return-void
.end method
