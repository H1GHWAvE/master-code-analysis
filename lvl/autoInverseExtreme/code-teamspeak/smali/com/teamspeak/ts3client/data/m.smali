.class final Lcom/teamspeak/ts3client/data/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final a:Landroid/view/GestureDetector;

.field final synthetic b:I

.field final synthetic c:Lcom/teamspeak/ts3client/data/g;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/data/g;I)V
    .registers 6

    .prologue
    .line 635
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/m;->c:Lcom/teamspeak/ts3client/data/g;

    iput p2, p0, Lcom/teamspeak/ts3client/data/m;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 636
    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/m;->c:Lcom/teamspeak/ts3client/data/g;

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/g;->c(Lcom/teamspeak/ts3client/data/g;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    new-instance v2, Lcom/teamspeak/ts3client/data/n;

    invoke-direct {v2, p0}, Lcom/teamspeak/ts3client/data/n;-><init>(Lcom/teamspeak/ts3client/data/m;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/m;->a:Landroid/view/GestureDetector;

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 6

    .prologue
    .line 682
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/m;->a:Landroid/view/GestureDetector;

    invoke-virtual {v0, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 683
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_2a

    .line 684
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_18

    .line 685
    const v1, 0x332e64fe

    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 686
    :cond_18
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_26

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2a

    .line 687
    :cond_26
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 689
    :cond_2a
    return v0
.end method
