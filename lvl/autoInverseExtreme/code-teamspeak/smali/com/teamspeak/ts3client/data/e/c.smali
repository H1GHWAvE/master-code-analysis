.class public final enum Lcom/teamspeak/ts3client/data/e/c;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/teamspeak/ts3client/data/e/c;

.field public static final enum b:Lcom/teamspeak/ts3client/data/e/c;

.field public static final enum c:Lcom/teamspeak/ts3client/data/e/c;

.field public static final enum d:Lcom/teamspeak/ts3client/data/e/c;

.field public static final enum e:Lcom/teamspeak/ts3client/data/e/c;

.field public static final enum f:Lcom/teamspeak/ts3client/data/e/c;

.field public static final enum g:Lcom/teamspeak/ts3client/data/e/c;

.field public static final enum h:Lcom/teamspeak/ts3client/data/e/c;

.field private static final synthetic k:[Lcom/teamspeak/ts3client/data/e/c;


# instance fields
.field public i:Ljava/lang/String;

.field j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 250
    new-instance v0, Lcom/teamspeak/ts3client/data/e/c;

    const-string v1, "Espanol"

    const-string v2, "es"

    const-string v3, "lang_ES.xml"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/teamspeak/ts3client/data/e/c;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/teamspeak/ts3client/data/e/c;->a:Lcom/teamspeak/ts3client/data/e/c;

    new-instance v0, Lcom/teamspeak/ts3client/data/e/c;

    const-string v1, "French"

    const-string v2, "fr"

    const-string v3, "lang_FR.xml"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/teamspeak/ts3client/data/e/c;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/teamspeak/ts3client/data/e/c;->b:Lcom/teamspeak/ts3client/data/e/c;

    new-instance v0, Lcom/teamspeak/ts3client/data/e/c;

    const-string v1, "Polski"

    const-string v2, "pl"

    const-string v3, "lang_PL.xml"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/teamspeak/ts3client/data/e/c;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/teamspeak/ts3client/data/e/c;->c:Lcom/teamspeak/ts3client/data/e/c;

    new-instance v0, Lcom/teamspeak/ts3client/data/e/c;

    const-string v1, "Portuguese"

    const-string v2, "pt"

    const-string v3, "lang_PT-br.xml"

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/teamspeak/ts3client/data/e/c;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/teamspeak/ts3client/data/e/c;->d:Lcom/teamspeak/ts3client/data/e/c;

    new-instance v0, Lcom/teamspeak/ts3client/data/e/c;

    const-string v1, "Russian"

    const-string v2, "ru"

    const-string v3, "lang_RU.xml"

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/teamspeak/ts3client/data/e/c;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/teamspeak/ts3client/data/e/c;->e:Lcom/teamspeak/ts3client/data/e/c;

    new-instance v0, Lcom/teamspeak/ts3client/data/e/c;

    const-string v1, "Turkish"

    const/4 v2, 0x5

    const-string v3, "tr"

    const-string v4, "lang_TR.xml"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/teamspeak/ts3client/data/e/c;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/teamspeak/ts3client/data/e/c;->f:Lcom/teamspeak/ts3client/data/e/c;

    new-instance v0, Lcom/teamspeak/ts3client/data/e/c;

    const-string v1, "Deutsch"

    const/4 v2, 0x6

    const-string v3, "de"

    const-string v4, "lang_de.xml"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/teamspeak/ts3client/data/e/c;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/teamspeak/ts3client/data/e/c;->g:Lcom/teamspeak/ts3client/data/e/c;

    new-instance v0, Lcom/teamspeak/ts3client/data/e/c;

    const-string v1, "English"

    const/4 v2, 0x7

    const-string v3, "en"

    const-string v4, "lang_eng.xml"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/teamspeak/ts3client/data/e/c;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/teamspeak/ts3client/data/e/c;->h:Lcom/teamspeak/ts3client/data/e/c;

    .line 249
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/teamspeak/ts3client/data/e/c;

    sget-object v1, Lcom/teamspeak/ts3client/data/e/c;->a:Lcom/teamspeak/ts3client/data/e/c;

    aput-object v1, v0, v5

    sget-object v1, Lcom/teamspeak/ts3client/data/e/c;->b:Lcom/teamspeak/ts3client/data/e/c;

    aput-object v1, v0, v6

    sget-object v1, Lcom/teamspeak/ts3client/data/e/c;->c:Lcom/teamspeak/ts3client/data/e/c;

    aput-object v1, v0, v7

    sget-object v1, Lcom/teamspeak/ts3client/data/e/c;->d:Lcom/teamspeak/ts3client/data/e/c;

    aput-object v1, v0, v8

    sget-object v1, Lcom/teamspeak/ts3client/data/e/c;->e:Lcom/teamspeak/ts3client/data/e/c;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/teamspeak/ts3client/data/e/c;->f:Lcom/teamspeak/ts3client/data/e/c;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/teamspeak/ts3client/data/e/c;->g:Lcom/teamspeak/ts3client/data/e/c;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/teamspeak/ts3client/data/e/c;->h:Lcom/teamspeak/ts3client/data/e/c;

    aput-object v2, v0, v1

    sput-object v0, Lcom/teamspeak/ts3client/data/e/c;->k:[Lcom/teamspeak/ts3client/data/e/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .registers 5

    .prologue
    .line 254
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 255
    iput-object p3, p0, Lcom/teamspeak/ts3client/data/e/c;->i:Ljava/lang/String;

    .line 256
    iput-object p4, p0, Lcom/teamspeak/ts3client/data/e/c;->j:Ljava/lang/String;

    .line 257
    return-void
.end method

.method private a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 260
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e/c;->i:Ljava/lang/String;

    return-object v0
.end method

.method private b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 264
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/e/c;->j:Ljava/lang/String;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/teamspeak/ts3client/data/e/c;
    .registers 2

    .prologue
    .line 249
    const-class v0, Lcom/teamspeak/ts3client/data/e/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/data/e/c;

    return-object v0
.end method

.method public static values()[Lcom/teamspeak/ts3client/data/e/c;
    .registers 1

    .prologue
    .line 249
    sget-object v0, Lcom/teamspeak/ts3client/data/e/c;->k:[Lcom/teamspeak/ts3client/data/e/c;

    invoke-virtual {v0}, [Lcom/teamspeak/ts3client/data/e/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/teamspeak/ts3client/data/e/c;

    return-object v0
.end method
