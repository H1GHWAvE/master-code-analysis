.class final Lcom/teamspeak/ts3client/data/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/data/o;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/data/o;)V
    .registers 2

    .prologue
    .line 752
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/p;->a:Lcom/teamspeak/ts3client/data/o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDown(Landroid/view/MotionEvent;)Z
    .registers 3

    .prologue
    .line 756
    const/4 v0, 0x0

    return v0
.end method

.method public final onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .registers 6

    .prologue
    .line 762
    const/4 v0, 0x0

    return v0
.end method

.method public final onLongPress(Landroid/view/MotionEvent;)V
    .registers 11

    .prologue
    const/4 v8, -0x1

    const/4 v6, -0x2

    const/4 v5, 0x0

    const/4 v0, 0x1

    const/4 v7, 0x0

    .line 768
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/teamspeak/ts3client/data/p;->a:Lcom/teamspeak/ts3client/data/o;

    iget-object v2, v2, Lcom/teamspeak/ts3client/data/o;->b:Lcom/teamspeak/ts3client/data/g;

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/g;->f(Lcom/teamspeak/ts3client/data/g;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 769
    invoke-static {v1}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/app/Dialog;)V

    .line 770
    const-string v2, "dialog.virtualserver.edit.info"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 771
    const-string v2, "dialog.virtualserver.edit.text"

    new-array v3, v0, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/teamspeak/ts3client/data/p;->a:Lcom/teamspeak/ts3client/data/o;

    iget-object v4, v4, Lcom/teamspeak/ts3client/data/o;->b:Lcom/teamspeak/ts3client/data/g;

    invoke-static {v4}, Lcom/teamspeak/ts3client/data/g;->c(Lcom/teamspeak/ts3client/data/g;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v4

    .line 1061
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 771
    invoke-virtual {v4}, Lcom/teamspeak/ts3client/data/e;->m()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 773
    const-string v2, "button.serverinfo"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/teamspeak/ts3client/data/q;

    invoke-direct {v3, p0}, Lcom/teamspeak/ts3client/data/q;-><init>(Lcom/teamspeak/ts3client/data/p;)V

    invoke-virtual {v1, v6, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 783
    const/4 v2, -0x3

    const-string v3, "button.edit"

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/teamspeak/ts3client/data/r;

    invoke-direct {v4, p0}, Lcom/teamspeak/ts3client/data/r;-><init>(Lcom/teamspeak/ts3client/data/p;)V

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 793
    const-string v2, "dialog.virtualserver.edit.buttonpw"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/teamspeak/ts3client/data/s;

    invoke-direct {v3, p0}, Lcom/teamspeak/ts3client/data/s;-><init>(Lcom/teamspeak/ts3client/data/p;)V

    invoke-virtual {v1, v8, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 809
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 810
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 811
    invoke-virtual {v1, v8}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v2

    .line 812
    invoke-virtual {v1, v6}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    .line 813
    iget-object v3, p0, Lcom/teamspeak/ts3client/data/p;->a:Lcom/teamspeak/ts3client/data/o;

    iget-object v3, v3, Lcom/teamspeak/ts3client/data/o;->b:Lcom/teamspeak/ts3client/data/g;

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/g;->c(Lcom/teamspeak/ts3client/data/g;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v3

    .line 2061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2181
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 813
    sget-object v4, Lcom/teamspeak/ts3client/jni/g;->au:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v3, v4}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v3

    if-nez v3, :cond_9e

    iget-object v3, p0, Lcom/teamspeak/ts3client/data/p;->a:Lcom/teamspeak/ts3client/data/o;

    iget-object v3, v3, Lcom/teamspeak/ts3client/data/o;->b:Lcom/teamspeak/ts3client/data/g;

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/g;->c(Lcom/teamspeak/ts3client/data/g;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v3

    .line 3061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3181
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 813
    sget-object v4, Lcom/teamspeak/ts3client/jni/g;->av:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v3, v4}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v3

    if-eqz v3, :cond_d4

    .line 814
    :cond_9e
    :goto_9e
    iget-object v3, p0, Lcom/teamspeak/ts3client/data/p;->a:Lcom/teamspeak/ts3client/data/o;

    iget-object v3, v3, Lcom/teamspeak/ts3client/data/o;->b:Lcom/teamspeak/ts3client/data/g;

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/g;->c(Lcom/teamspeak/ts3client/data/g;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v3

    .line 4061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 4181
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 814
    sget-object v4, Lcom/teamspeak/ts3client/jni/g;->x:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v3, v4}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v3

    .line 816
    if-eqz v0, :cond_b4

    if-nez v2, :cond_b7

    .line 817
    :cond_b4
    invoke-virtual {v2, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 818
    :cond_b7
    if-eqz v3, :cond_bb

    if-nez v1, :cond_be

    .line 819
    :cond_bb
    invoke-virtual {v1, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 821
    :cond_be
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/p;->a:Lcom/teamspeak/ts3client/data/o;

    iget-object v0, v0, Lcom/teamspeak/ts3client/data/o;->b:Lcom/teamspeak/ts3client/data/g;

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/g;->b(Lcom/teamspeak/ts3client/data/g;)Lcom/teamspeak/ts3client/data/customExpandableListView;

    move-result-object v8

    const-wide/16 v0, 0x0

    const-wide/16 v2, 0x0

    const/4 v4, 0x3

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/teamspeak/ts3client/data/customExpandableListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 822
    return-void

    :cond_d4
    move v0, v7

    .line 813
    goto :goto_9e
.end method

.method public final onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .registers 6

    .prologue
    .line 827
    const/4 v0, 0x0

    return v0
.end method

.method public final onShowPress(Landroid/view/MotionEvent;)V
    .registers 2

    .prologue
    .line 834
    return-void
.end method

.method public final onSingleTapUp(Landroid/view/MotionEvent;)Z
    .registers 3

    .prologue
    .line 838
    const/4 v0, 0x0

    return v0
.end method
