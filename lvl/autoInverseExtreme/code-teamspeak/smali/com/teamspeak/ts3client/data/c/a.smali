.class public final Lcom/teamspeak/ts3client/data/c/a;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/teamspeak/ts3client/data/c;)V
    .registers 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1a

    .line 36
    new-instance v0, Lcom/teamspeak/ts3client/data/c/b;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/data/c/b;-><init>(Lcom/teamspeak/ts3client/data/c/a;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v2, [Lcom/teamspeak/ts3client/data/c;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/data/c/b;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 40
    :goto_19
    return-void

    .line 38
    :cond_1a
    new-instance v0, Lcom/teamspeak/ts3client/data/c/b;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/data/c/b;-><init>(Lcom/teamspeak/ts3client/data/c/a;)V

    new-array v1, v2, [Lcom/teamspeak/ts3client/data/c;

    aput-object p1, v1, v3

    invoke-virtual {v0, v1}, Lcom/teamspeak/ts3client/data/c/b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_19
.end method
