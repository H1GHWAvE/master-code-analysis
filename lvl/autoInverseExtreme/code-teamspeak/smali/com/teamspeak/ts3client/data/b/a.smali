.class public final Lcom/teamspeak/ts3client/data/b/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Lcom/teamspeak/ts3client/data/b/a; = null

.field private static final b:Ljava/lang/String; = "create table server (server_id integer primary key autoincrement, label text not null, address text not null, serverpassword blob null, nickname text not null, defaultchannel text not null, defaultchannelpassword text not null, ident int not null, subscribeall text not null,subscriptionlist int not null);"

.field private static final c:Ljava/lang/String; = "server"

.field private static final d:Ljava/lang/String; = "Teamspeak-Bookmark"

.field private static final e:I = 0x5


# instance fields
.field private f:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Lcom/teamspeak/ts3client/data/b/b;

    invoke-direct {v0, p0, p1}, Lcom/teamspeak/ts3client/data/b/b;-><init>(Lcom/teamspeak/ts3client/data/b/a;Landroid/content/Context;)V

    .line 37
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/b/b;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/b/a;->f:Landroid/database/sqlite/SQLiteDatabase;

    .line 38
    sput-object p0, Lcom/teamspeak/ts3client/data/b/a;->a:Lcom/teamspeak/ts3client/data/b/a;

    .line 39
    return-void
.end method

.method private static a([B)Ljava/lang/String;
    .registers 5

    .prologue
    .line 191
    if-nez p0, :cond_5

    .line 192
    const-string v0, ""

    .line 216
    :goto_4
    return-object v0

    .line 193
    :cond_5
    const-string v0, "mnd783bnsd03bs72"

    .line 194
    const-string v1, "abd732ns0Sg37s3S"

    .line 195
    new-instance v2, Ljavax/crypto/spec/IvParameterSpec;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-direct {v2, v0}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 196
    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    const-string v3, "AES"

    invoke-direct {v0, v1, v3}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 199
    :try_start_1d
    const-string v1, "AES/CBC/PKCS5Padding"

    invoke-static {v1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v1

    .line 200
    const/4 v3, 0x2

    invoke-virtual {v1, v3, v0, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 201
    invoke-virtual {v1, p0}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v1

    .line 202
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V
    :try_end_30
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1d .. :try_end_30} :catch_31
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_1d .. :try_end_30} :catch_44
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_1d .. :try_end_30} :catch_55
    .catch Ljavax/crypto/BadPaddingException; {:try_start_1d .. :try_end_30} :catch_66
    .catch Ljava/security/InvalidKeyException; {:try_start_1d .. :try_end_30} :catch_77
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_1d .. :try_end_30} :catch_88

    goto :goto_4

    .line 203
    :catch_31
    move-exception v0

    .line 204
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 23085
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 204
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 216
    :goto_41
    const-string v0, ""

    goto :goto_4

    .line 205
    :catch_44
    move-exception v0

    .line 206
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 24085
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 206
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljavax/crypto/NoSuchPaddingException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_41

    .line 207
    :catch_55
    move-exception v0

    .line 208
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 25085
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 208
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljavax/crypto/IllegalBlockSizeException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_41

    .line 209
    :catch_66
    move-exception v0

    .line 210
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 26085
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 210
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljavax/crypto/BadPaddingException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_41

    .line 211
    :catch_77
    move-exception v0

    .line 212
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 27085
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 212
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/security/InvalidKeyException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_41

    .line 213
    :catch_88
    move-exception v0

    .line 214
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 28085
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 214
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/security/InvalidAlgorithmParameterException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_41
.end method

.method static a(Ljava/lang/String;)[B
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 162
    const-string v1, ""

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 187
    :goto_9
    return-object v0

    .line 164
    :cond_a
    const-string v1, "mnd783bnsd03bs72"

    .line 165
    const-string v2, "abd732ns0Sg37s3S"

    .line 166
    new-instance v3, Ljavax/crypto/spec/IvParameterSpec;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-direct {v3, v1}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 167
    new-instance v1, Ljavax/crypto/spec/SecretKeySpec;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    const-string v4, "AES"

    invoke-direct {v1, v2, v4}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 170
    :try_start_22
    const-string v2, "AES/CBC/PKCS5Padding"

    invoke-static {v2}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v2

    .line 171
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v1, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 172
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v2, v1}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_33
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_22 .. :try_end_33} :catch_35
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_22 .. :try_end_33} :catch_46
    .catch Ljava/security/InvalidKeyException; {:try_start_22 .. :try_end_33} :catch_57
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_22 .. :try_end_33} :catch_68
    .catch Ljavax/crypto/BadPaddingException; {:try_start_22 .. :try_end_33} :catch_79
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_22 .. :try_end_33} :catch_8a

    move-result-object v0

    goto :goto_9

    .line 174
    :catch_35
    move-exception v1

    .line 175
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 17085
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 175
    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_9

    .line 176
    :catch_46
    move-exception v1

    .line 177
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 18085
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 177
    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v1}, Ljavax/crypto/NoSuchPaddingException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_9

    .line 178
    :catch_57
    move-exception v1

    .line 179
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 19085
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 179
    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v1}, Ljava/security/InvalidKeyException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_9

    .line 180
    :catch_68
    move-exception v1

    .line 181
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 20085
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 181
    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v1}, Ljavax/crypto/IllegalBlockSizeException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_9

    .line 182
    :catch_79
    move-exception v1

    .line 183
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 21085
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 183
    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v1}, Ljavax/crypto/BadPaddingException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_9

    .line 184
    :catch_8a
    move-exception v1

    .line 185
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 22085
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 185
    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v1}, Ljava/security/InvalidAlgorithmParameterException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto/16 :goto_9
.end method

.method private b(J)Lcom/teamspeak/ts3client/data/ab;
    .registers 16

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 113
    new-instance v9, Lcom/teamspeak/ts3client/data/ab;

    invoke-direct {v9}, Lcom/teamspeak/ts3client/data/ab;-><init>()V

    .line 118
    :try_start_8
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b/a;->f:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "server"

    const/16 v2, 0xa

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "server_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "label"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "address"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "serverpassword"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "nickname"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "defaultchannel"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "defaultchannelpassword"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "ident"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "subscribeall"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "subscriptionlist"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "server_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 119
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_f0

    .line 120
    const-string v0, "label"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 9099
    iput-object v0, v9, Lcom/teamspeak/ts3client/data/ab;->a:Ljava/lang/String;

    .line 121
    const-string v0, "address"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "(.*://)+"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/teamspeak/ts3client/data/ab;->a(Ljava/lang/String;)V

    .line 122
    new-instance v0, Ljava/lang/String;

    const-string v2, "serverpassword"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/b/a;->a([B)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 9115
    iput-object v0, v9, Lcom/teamspeak/ts3client/data/ab;->e:Ljava/lang/String;

    .line 123
    const-string v0, "nickname"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 10107
    iput-object v0, v9, Lcom/teamspeak/ts3client/data/ab;->f:Ljava/lang/String;

    .line 124
    const-string v0, "defaultchannel"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 11075
    iput-object v0, v9, Lcom/teamspeak/ts3client/data/ab;->g:Ljava/lang/String;

    .line 125
    const-string v0, "defaultchannelpassword"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 11083
    iput-object v0, v9, Lcom/teamspeak/ts3client/data/ab;->h:Ljava/lang/String;

    .line 126
    const-string v0, "server_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 12067
    iput-wide v2, v9, Lcom/teamspeak/ts3client/data/ab;->l:J

    .line 127
    const-string v0, "ident"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 12091
    iput v0, v9, Lcom/teamspeak/ts3client/data/ab;->d:I

    .line 128
    const-string v0, "subscribeall"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_f5

    move v0, v11

    .line 12163
    :goto_e1
    iput-boolean v0, v9, Lcom/teamspeak/ts3client/data/ab;->j:Z

    .line 129
    const-string v0, "subscriptionlist"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/teamspeak/ts3client/data/ab;->b(Ljava/lang/String;)V

    .line 131
    :cond_f0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_f3
    .catch Landroid/database/SQLException; {:try_start_8 .. :try_end_f3} :catch_f7

    move-object v0, v9

    .line 136
    :goto_f4
    return-object v0

    :cond_f5
    move v0, v12

    .line 128
    goto :goto_e1

    .line 133
    :catch_f7
    move-exception v0

    move-object v0, v10

    goto :goto_f4
.end method

.method private static b()Lcom/teamspeak/ts3client/data/b/a;
    .registers 1

    .prologue
    .line 42
    sget-object v0, Lcom/teamspeak/ts3client/data/b/a;->a:Lcom/teamspeak/ts3client/data/b/a;

    return-object v0
.end method

.method private static synthetic b(Ljava/lang/String;)[B
    .registers 2

    .prologue
    .line 26
    invoke-static {p0}, Lcom/teamspeak/ts3client/data/b/a;->a(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method private c()V
    .registers 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b/a;->f:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 67
    return-void
.end method


# virtual methods
.method public final a(Lcom/teamspeak/ts3client/data/ab;)J
    .registers 6

    .prologue
    .line 46
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 47
    const-string v0, "label"

    .line 1095
    iget-object v2, p1, Lcom/teamspeak/ts3client/data/ab;->a:Ljava/lang/String;

    .line 47
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2055
    iget-object v0, p1, Lcom/teamspeak/ts3client/data/ab;->c:Ljava/lang/String;

    .line 48
    const-string v2, "(.*://)+"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 49
    const-string v2, "address"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    const-string v0, "serverpassword"

    .line 2111
    iget-object v2, p1, Lcom/teamspeak/ts3client/data/ab;->e:Ljava/lang/String;

    .line 50
    invoke-static {v2}, Lcom/teamspeak/ts3client/data/b/a;->a(Ljava/lang/String;)[B

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 51
    const-string v0, "nickname"

    .line 3103
    iget-object v2, p1, Lcom/teamspeak/ts3client/data/ab;->f:Ljava/lang/String;

    .line 51
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    const-string v0, "defaultchannel"

    .line 4071
    iget-object v2, p1, Lcom/teamspeak/ts3client/data/ab;->g:Ljava/lang/String;

    .line 52
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    const-string v0, "defaultchannelpassword"

    .line 4079
    iget-object v2, p1, Lcom/teamspeak/ts3client/data/ab;->h:Ljava/lang/String;

    .line 53
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const-string v0, "ident"

    .line 4087
    iget v2, p1, Lcom/teamspeak/ts3client/data/ab;->d:I

    .line 54
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 55
    const-string v2, "subscribeall"

    .line 4159
    iget-boolean v0, p1, Lcom/teamspeak/ts3client/data/ab;->j:Z

    .line 55
    if-eqz v0, :cond_67

    const/4 v0, 0x1

    :goto_4d
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 56
    const-string v0, "subscriptionlist"

    invoke-virtual {p1}, Lcom/teamspeak/ts3client/data/ab;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    :try_start_5d
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b/a;->f:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "server"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_65
    .catch Ljava/lang/Exception; {:try_start_5d .. :try_end_65} :catch_69

    move-result-wide v0

    .line 61
    :goto_66
    return-wide v0

    .line 55
    :cond_67
    const/4 v0, 0x0

    goto :goto_4d

    .line 60
    :catch_69
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 61
    const-wide/16 v0, -0x1

    goto :goto_66
.end method

.method public final a()Ljava/util/ArrayList;
    .registers 13

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 80
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 84
    :try_start_8
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b/a;->f:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "server"

    const/16 v2, 0xa

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "server_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "label"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "address"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "serverpassword"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "nickname"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "defaultchannel"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "defaultchannelpassword"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "ident"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "subscribeall"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "subscriptionlist"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 86
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 88
    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_f2

    .line 90
    :cond_56
    new-instance v2, Lcom/teamspeak/ts3client/data/ab;

    invoke-direct {v2}, Lcom/teamspeak/ts3client/data/ab;-><init>()V

    .line 91
    const-string v0, "label"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 5099
    iput-object v0, v2, Lcom/teamspeak/ts3client/data/ab;->a:Ljava/lang/String;

    .line 92
    const-string v0, "address"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v3, "(.*://)+"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/teamspeak/ts3client/data/ab;->a(Ljava/lang/String;)V

    .line 93
    new-instance v0, Ljava/lang/String;

    const-string v3, "serverpassword"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-static {v3}, Lcom/teamspeak/ts3client/data/b/a;->a([B)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 5115
    iput-object v0, v2, Lcom/teamspeak/ts3client/data/ab;->e:Ljava/lang/String;

    .line 94
    const-string v0, "nickname"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 6107
    iput-object v0, v2, Lcom/teamspeak/ts3client/data/ab;->f:Ljava/lang/String;

    .line 95
    const-string v0, "defaultchannel"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 7075
    iput-object v0, v2, Lcom/teamspeak/ts3client/data/ab;->g:Ljava/lang/String;

    .line 96
    const-string v0, "defaultchannelpassword"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 7083
    iput-object v0, v2, Lcom/teamspeak/ts3client/data/ab;->h:Ljava/lang/String;

    .line 97
    const-string v0, "server_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 8067
    iput-wide v4, v2, Lcom/teamspeak/ts3client/data/ab;->l:J

    .line 98
    const-string v0, "ident"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 8091
    iput v0, v2, Lcom/teamspeak/ts3client/data/ab;->d:I

    .line 99
    const-string v0, "subscribeall"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_f7

    move v0, v10

    .line 8163
    :goto_da
    iput-boolean v0, v2, Lcom/teamspeak/ts3client/data/ab;->j:Z

    .line 100
    const-string v0, "subscriptionlist"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/teamspeak/ts3client/data/ab;->b(Ljava/lang/String;)V

    .line 101
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 102
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_56

    .line 104
    :cond_f2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_f5
    .catch Landroid/database/SQLException; {:try_start_8 .. :try_end_f5} :catch_f9

    move-object v0, v8

    .line 109
    :goto_f6
    return-object v0

    :cond_f7
    move v0, v11

    .line 99
    goto :goto_da

    .line 106
    :catch_f9
    move-exception v0

    invoke-virtual {v0}, Landroid/database/SQLException;->printStackTrace()V

    move-object v0, v9

    .line 107
    goto :goto_f6
.end method

.method public final a(J)Z
    .registers 8

    .prologue
    .line 71
    :try_start_0
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b/a;->f:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "server"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "server_id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_17} :catch_19

    .line 72
    const/4 v0, 0x1

    .line 75
    :goto_18
    return v0

    .line 74
    :catch_19
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 75
    const/4 v0, 0x0

    goto :goto_18
.end method

.method public final a(JLcom/teamspeak/ts3client/data/ab;)Z
    .registers 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 140
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 141
    const-string v0, "label"

    .line 13095
    iget-object v4, p3, Lcom/teamspeak/ts3client/data/ab;->a:Ljava/lang/String;

    .line 141
    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 14055
    iget-object v0, p3, Lcom/teamspeak/ts3client/data/ab;->c:Ljava/lang/String;

    .line 142
    const-string v4, "(.*://)+"

    const-string v5, ""

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 143
    const-string v4, "address"

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    const-string v0, "serverpassword"

    .line 14111
    iget-object v4, p3, Lcom/teamspeak/ts3client/data/ab;->e:Ljava/lang/String;

    .line 144
    invoke-static {v4}, Lcom/teamspeak/ts3client/data/b/a;->a(Ljava/lang/String;)[B

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 145
    const-string v0, "nickname"

    .line 15103
    iget-object v4, p3, Lcom/teamspeak/ts3client/data/ab;->f:Ljava/lang/String;

    .line 145
    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const-string v0, "defaultchannel"

    .line 16071
    iget-object v4, p3, Lcom/teamspeak/ts3client/data/ab;->g:Ljava/lang/String;

    .line 146
    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const-string v0, "defaultchannelpassword"

    .line 16079
    iget-object v4, p3, Lcom/teamspeak/ts3client/data/ab;->h:Ljava/lang/String;

    .line 147
    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    const-string v0, "ident"

    .line 16087
    iget v4, p3, Lcom/teamspeak/ts3client/data/ab;->d:I

    .line 148
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 149
    const-string v4, "subscribeall"

    .line 16159
    iget-boolean v0, p3, Lcom/teamspeak/ts3client/data/ab;->j:Z

    .line 149
    if-eqz v0, :cond_77

    move v0, v1

    :goto_4f
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 150
    const-string v0, "subscriptionlist"

    invoke-virtual {p3}, Lcom/teamspeak/ts3client/data/ab;->h()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    :try_start_5f
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/b/a;->f:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "server"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "server_id="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v0, v4, v3, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_76
    .catch Ljava/lang/Exception; {:try_start_5f .. :try_end_76} :catch_79

    .line 157
    :goto_76
    return v1

    :cond_77
    move v0, v2

    .line 149
    goto :goto_4f

    .line 156
    :catch_79
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v1, v2

    .line 157
    goto :goto_76
.end method
