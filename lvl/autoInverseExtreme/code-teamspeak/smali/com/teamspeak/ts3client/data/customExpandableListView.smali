.class public Lcom/teamspeak/ts3client/data/customExpandableListView;
.super Landroid/widget/ExpandableListView;
.source "SourceFile"


# instance fields
.field a:I

.field private b:Z

.field private c:Landroid/os/Handler;

.field private d:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Landroid/widget/ExpandableListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 13
    const/4 v0, 0x0

    iput v0, p0, Lcom/teamspeak/ts3client/data/customExpandableListView;->a:I

    .line 14
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/data/customExpandableListView;->b:Z

    .line 15
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/customExpandableListView;->c:Landroid/os/Handler;

    .line 16
    new-instance v0, Lcom/teamspeak/ts3client/data/ak;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/data/ak;-><init>(Lcom/teamspeak/ts3client/data/customExpandableListView;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/customExpandableListView;->d:Ljava/lang/Runnable;

    .line 26
    const-string v0, "Server View"

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/data/customExpandableListView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 28
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 6

    .prologue
    .line 33
    invoke-super {p0, p1}, Landroid/widget/ExpandableListView;->onDraw(Landroid/graphics/Canvas;)V

    .line 34
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/data/customExpandableListView;->getWidth()I

    move-result v0

    .line 36
    iget v1, p0, Lcom/teamspeak/ts3client/data/customExpandableListView;->a:I

    if-eq v0, v1, :cond_38

    if-lez v0, :cond_38

    .line 37
    iput v0, p0, Lcom/teamspeak/ts3client/data/customExpandableListView;->a:I

    .line 38
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/customExpandableListView;->b:Z

    if-eqz v0, :cond_1f

    .line 40
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/customExpandableListView;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/customExpandableListView;->d:Ljava/lang/Runnable;

    const-wide/16 v2, 0x320

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/teamspeak/ts3client/data/customExpandableListView;->b:Z

    .line 43
    :cond_1f
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    if-eqz v0, :cond_38

    .line 44
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 1061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 44
    if-eqz v0, :cond_38

    .line 45
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 2061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2206
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 45
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->y()V

    .line 50
    :cond_38
    return-void
.end method
