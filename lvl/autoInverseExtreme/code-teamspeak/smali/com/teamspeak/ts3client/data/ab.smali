.class public final Lcom/teamspeak/ts3client/data/ab;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:I

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Z

.field public k:Ljava/util/Vector;

.field public l:J

.field m:J

.field public n:I

.field public o:Ljava/lang/String;

.field public p:I

.field public q:J

.field public r:Ljava/lang/String;

.field public s:Ljava/util/BitSet;

.field public t:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 5

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->a:Ljava/lang/String;

    .line 9
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->b:Ljava/lang/String;

    .line 10
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->c:Ljava/lang/String;

    .line 11
    const/4 v0, 0x1

    iput v0, p0, Lcom/teamspeak/ts3client/data/ab;->d:I

    .line 12
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->e:Ljava/lang/String;

    .line 13
    const-string v0, "Android"

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->f:Ljava/lang/String;

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->g:Ljava/lang/String;

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->h:Ljava/lang/String;

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->i:Ljava/lang/String;

    .line 17
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/ab;->j:Z

    .line 18
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->k:Ljava/util/Vector;

    .line 19
    iput-wide v2, p0, Lcom/teamspeak/ts3client/data/ab;->l:J

    .line 20
    iput-wide v2, p0, Lcom/teamspeak/ts3client/data/ab;->m:J

    .line 22
    iput v1, p0, Lcom/teamspeak/ts3client/data/ab;->n:I

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->o:Ljava/lang/String;

    .line 24
    iput v1, p0, Lcom/teamspeak/ts3client/data/ab;->p:I

    .line 25
    iput-wide v2, p0, Lcom/teamspeak/ts3client/data/ab;->q:J

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->r:Ljava/lang/String;

    .line 31
    new-instance v0, Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->s:Ljava/util/BitSet;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->t:Ljava/lang/String;

    .line 36
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .registers 12

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->a:Ljava/lang/String;

    .line 9
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->b:Ljava/lang/String;

    .line 10
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->c:Ljava/lang/String;

    .line 11
    const/4 v0, 0x1

    iput v0, p0, Lcom/teamspeak/ts3client/data/ab;->d:I

    .line 12
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->e:Ljava/lang/String;

    .line 13
    const-string v0, "Android"

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->f:Ljava/lang/String;

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->g:Ljava/lang/String;

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->h:Ljava/lang/String;

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->i:Ljava/lang/String;

    .line 17
    iput-boolean v1, p0, Lcom/teamspeak/ts3client/data/ab;->j:Z

    .line 18
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->k:Ljava/util/Vector;

    .line 19
    iput-wide v2, p0, Lcom/teamspeak/ts3client/data/ab;->l:J

    .line 20
    iput-wide v2, p0, Lcom/teamspeak/ts3client/data/ab;->m:J

    .line 22
    iput v1, p0, Lcom/teamspeak/ts3client/data/ab;->n:I

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->o:Ljava/lang/String;

    .line 24
    iput v1, p0, Lcom/teamspeak/ts3client/data/ab;->p:I

    .line 25
    iput-wide v2, p0, Lcom/teamspeak/ts3client/data/ab;->q:J

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->r:Ljava/lang/String;

    .line 31
    new-instance v0, Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->s:Ljava/util/BitSet;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->t:Ljava/lang/String;

    .line 40
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/ab;->a:Ljava/lang/String;

    .line 41
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->c:Ljava/lang/String;

    .line 42
    iput-object p3, p0, Lcom/teamspeak/ts3client/data/ab;->e:Ljava/lang/String;

    .line 43
    iput-object p4, p0, Lcom/teamspeak/ts3client/data/ab;->f:Ljava/lang/String;

    .line 44
    iput-object p5, p0, Lcom/teamspeak/ts3client/data/ab;->g:Ljava/lang/String;

    .line 45
    iput-object p6, p0, Lcom/teamspeak/ts3client/data/ab;->h:Ljava/lang/String;

    .line 46
    iput p7, p0, Lcom/teamspeak/ts3client/data/ab;->d:I

    .line 47
    return-void
.end method

.method private b(I)V
    .registers 2

    .prologue
    .line 91
    iput p1, p0, Lcom/teamspeak/ts3client/data/ab;->d:I

    .line 92
    return-void
.end method

.method private c(J)V
    .registers 4

    .prologue
    .line 67
    iput-wide p1, p0, Lcom/teamspeak/ts3client/data/ab;->l:J

    .line 68
    return-void
.end method

.method private c(Ljava/lang/Long;)V
    .registers 3

    .prologue
    .line 167
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->k:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 168
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->k:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    .line 169
    :cond_d
    return-void
.end method

.method private e(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 75
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/ab;->g:Ljava/lang/String;

    .line 76
    return-void
.end method

.method private f(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 83
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/ab;->h:Ljava/lang/String;

    .line 84
    return-void
.end method

.method private g(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 99
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/ab;->a:Ljava/lang/String;

    .line 100
    return-void
.end method

.method private h(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 107
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/ab;->f:Ljava/lang/String;

    .line 108
    return-void
.end method

.method private i(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 115
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/ab;->e:Ljava/lang/String;

    .line 116
    return-void
.end method

.method private j(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 140
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/ab;->i:Ljava/lang/String;

    .line 141
    return-void
.end method

.method private k(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 195
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/ab;->b:Ljava/lang/String;

    .line 196
    return-void
.end method

.method private l(Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 215
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->o:Ljava/lang/String;

    .line 216
    return-void
.end method

.method private m()Ljava/lang/String;
    .registers 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->c:Ljava/lang/String;

    return-object v0
.end method

.method private m(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 251
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/ab;->t:Ljava/lang/String;

    .line 252
    return-void
.end method

.method private n()J
    .registers 3

    .prologue
    .line 63
    iget-wide v0, p0, Lcom/teamspeak/ts3client/data/ab;->l:J

    return-wide v0
.end method

.method private o()I
    .registers 2

    .prologue
    .line 87
    iget v0, p0, Lcom/teamspeak/ts3client/data/ab;->d:I

    return v0
.end method

.method private p()Ljava/lang/String;
    .registers 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->a:Ljava/lang/String;

    return-object v0
.end method

.method private q()Ljava/util/Vector;
    .registers 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->k:Ljava/util/Vector;

    return-object v0
.end method

.method private r()J
    .registers 3

    .prologue
    .line 183
    iget-wide v0, p0, Lcom/teamspeak/ts3client/data/ab;->m:J

    return-wide v0
.end method

.method private s()Ljava/lang/String;
    .registers 2

    .prologue
    .line 191
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->b:Ljava/lang/String;

    return-object v0
.end method

.method private t()Ljava/lang/String;
    .registers 2

    .prologue
    .line 211
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->o:Ljava/lang/String;

    return-object v0
.end method

.method private u()I
    .registers 2

    .prologue
    .line 219
    iget v0, p0, Lcom/teamspeak/ts3client/data/ab;->p:I

    return v0
.end method

.method private v()Ljava/util/BitSet;
    .registers 2

    .prologue
    .line 243
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->s:Ljava/util/BitSet;

    return-object v0
.end method

.method private w()Ljava/lang/String;
    .registers 2

    .prologue
    .line 247
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->t:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final a(I)V
    .registers 2

    .prologue
    .line 223
    iput p1, p0, Lcom/teamspeak/ts3client/data/ab;->p:I

    .line 224
    return-void
.end method

.method public final a(J)V
    .registers 6

    .prologue
    .line 187
    const-wide v0, 0xffffffffL

    and-long/2addr v0, p1

    iput-wide v0, p0, Lcom/teamspeak/ts3client/data/ab;->m:J

    .line 188
    return-void
.end method

.method public final a(Ljava/lang/Long;)V
    .registers 3

    .prologue
    .line 50
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->k:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 51
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->k:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 52
    :cond_d
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .registers 3

    .prologue
    .line 59
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->c:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public final a(Z)V
    .registers 2

    .prologue
    .line 163
    iput-boolean p1, p0, Lcom/teamspeak/ts3client/data/ab;->j:Z

    .line 164
    return-void
.end method

.method public final b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final b(J)V
    .registers 4

    .prologue
    .line 231
    iput-wide p1, p0, Lcom/teamspeak/ts3client/data/ab;->q:J

    .line 232
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .registers 10

    .prologue
    .line 144
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 152
    :cond_8
    :goto_8
    return-void

    .line 146
    :cond_9
    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 147
    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_19
    if-ge v0, v2, :cond_8

    aget-object v3, v1, v0

    .line 148
    iget-object v4, p0, Lcom/teamspeak/ts3client/data/ab;->k:Ljava/util/Vector;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 147
    add-int/lit8 v0, v0, 0x1

    goto :goto_19

    .line 150
    :cond_2d
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->k:Ljava/util/Vector;

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_8
.end method

.method public final b(Ljava/lang/Long;)Z
    .registers 3

    .prologue
    .line 155
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->k:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 207
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/ab;->o:Ljava/lang/String;

    .line 208
    return-void
.end method

.method public final d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .registers 2

    .prologue
    .line 239
    iput-object p1, p0, Lcom/teamspeak/ts3client/data/ab;->r:Ljava/lang/String;

    .line 240
    return-void
.end method

.method public final e()[J
    .registers 7

    .prologue
    .line 119
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->k:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-gtz v0, :cond_a

    .line 120
    const/4 v0, 0x0

    .line 128
    :goto_9
    return-object v0

    .line 121
    :cond_a
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->k:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    new-array v2, v0, [J

    .line 122
    const/4 v0, 0x0

    .line 123
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/ab;->k:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_32

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 124
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    aput-wide v4, v2, v1

    .line 125
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 126
    goto :goto_1c

    .line 127
    :cond_32
    const-wide/16 v4, 0x0

    aput-wide v4, v2, v1

    move-object v0, v2

    .line 128
    goto :goto_9
.end method

.method public final f()Ljava/lang/String;
    .registers 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Z
    .registers 2

    .prologue
    .line 159
    iget-boolean v0, p0, Lcom/teamspeak/ts3client/data/ab;->j:Z

    return v0
.end method

.method public final h()Ljava/lang/String;
    .registers 5

    .prologue
    .line 172
    const-string v0, ""

    .line 173
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/ab;->k:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_49

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 174
    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_30

    .line 175
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_9

    .line 177
    :cond_30
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 178
    goto :goto_9

    .line 179
    :cond_49
    return-object v1
.end method

.method public final i()I
    .registers 2

    .prologue
    .line 199
    iget v0, p0, Lcom/teamspeak/ts3client/data/ab;->n:I

    return v0
.end method

.method public final j()V
    .registers 2

    .prologue
    .line 203
    const/4 v0, 0x0

    iput v0, p0, Lcom/teamspeak/ts3client/data/ab;->n:I

    .line 204
    return-void
.end method

.method public final k()J
    .registers 3

    .prologue
    .line 227
    iget-wide v0, p0, Lcom/teamspeak/ts3client/data/ab;->q:J

    return-wide v0
.end method

.method public final l()Ljava/lang/String;
    .registers 2

    .prologue
    .line 235
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/ab;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 256
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Server [Label="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/ab;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ServerName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/ab;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Address="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/ab;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Ident="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/data/ab;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ServerPassword="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/ab;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Nickname="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/ab;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", DefaultChannel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/ab;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", DefaultChannelPassword="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/ab;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/ab;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", subscribeAll="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/teamspeak/ts3client/data/ab;->j:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", subscriptionList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/ab;->k:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", DB_ID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/data/ab;->l:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", iconID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/data/ab;->m:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", retry="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/data/ab;->n:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ip="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/ab;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", port="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/data/ab;->p:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", reconnectChannel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/data/ab;->q:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", reconnectPW="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/ab;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", reconnectSettings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/ab;->s:Ljava/util/BitSet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", reconnectAwayMsg="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/ab;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
