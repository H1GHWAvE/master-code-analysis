.class public final Lcom/teamspeak/ts3client/data/c/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/data/w;


# static fields
.field static final a:Landroid/graphics/drawable/Drawable;


# instance fields
.field private b:Ljava/io/File;

.field private c:J

.field private d:Landroid/graphics/drawable/Drawable;

.field private e:Lcom/teamspeak/ts3client/Ts3Application;

.field private f:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 20
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020069

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/teamspeak/ts3client/data/c/c;->a:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public constructor <init>(JLjava/io/File;Lcom/teamspeak/ts3client/Ts3Application;)V
    .registers 20

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/teamspeak/ts3client/data/c/c;->f:Ljava/util/ArrayList;

    .line 29
    move-wide/from16 v0, p1

    iput-wide v0, p0, Lcom/teamspeak/ts3client/data/c/c;->c:J

    .line 30
    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c/c;->b:Ljava/io/File;

    .line 31
    move-object/from16 v0, p4

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c/c;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 33
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-nez v2, :cond_1d

    .line 48
    :cond_1c
    :goto_1c
    return-void

    .line 37
    :cond_1d
    const-wide/16 v2, 0x3e7

    cmp-long v2, p1, v2

    if-lez v2, :cond_1c

    .line 41
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "icon_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".gif"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-direct {v2, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 42
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1c

    .line 1061
    move-object/from16 v0, p4

    iget-object v2, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1238
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 45
    invoke-virtual {v2, p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/data/w;)V

    .line 2085
    move-object/from16 v0, p4

    iget-object v2, v0, Lcom/teamspeak/ts3client/Ts3Application;->d:Ljava/util/logging/Logger;

    .line 46
    sget-object v3, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Loading Icon "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p1

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " from Server"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 47
    iget-object v2, p0, Lcom/teamspeak/ts3client/data/c/c;->f:Ljava/util/ArrayList;

    .line 3061
    move-object/from16 v0, p4

    iget-object v3, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3234
    iget-object v3, v3, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 4061
    move-object/from16 v0, p4

    iget-object v4, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 4267
    iget-wide v4, v4, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 47
    const-wide/16 v6, 0x0

    const-string v8, ""

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "/icon_"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p1

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/teamspeak/ts3client/data/c/c;->b:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "icon_"

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p1

    invoke-virtual {v13, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {v3 .. v13}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->ts3client_requestFile(JJLjava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1c
.end method

.method private b()J
    .registers 3

    .prologue
    .line 114
    iget-wide v0, p0, Lcom/teamspeak/ts3client/data/c/c;->c:J

    return-wide v0
.end method

.method private c()I
    .registers 3

    .prologue
    .line 118
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/c;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 119
    const/high16 v1, 0x41900000    # 18.0f

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method


# virtual methods
.method public final a()Landroid/graphics/drawable/Drawable;
    .registers 9

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x1

    const/16 v6, 0x10

    .line 68
    iget-wide v2, p0, Lcom/teamspeak/ts3client/data/c/c;->c:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_f

    .line 69
    sget-object v0, Lcom/teamspeak/ts3client/data/c/c;->a:Landroid/graphics/drawable/Drawable;

    .line 110
    :goto_e
    return-object v0

    .line 72
    :cond_f
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/c;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/c;->d:Landroid/graphics/drawable/Drawable;

    sget-object v2, Lcom/teamspeak/ts3client/data/c/c;->a:Landroid/graphics/drawable/Drawable;

    if-eq v0, v2, :cond_1c

    .line 73
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/c;->d:Landroid/graphics/drawable/Drawable;

    goto :goto_e

    .line 76
    :cond_1c
    iget-wide v2, p0, Lcom/teamspeak/ts3client/data/c/c;->c:J

    const-wide/16 v4, 0x3e7

    cmp-long v0, v2, v4

    if-gtz v0, :cond_c1

    .line 78
    iget-wide v2, p0, Lcom/teamspeak/ts3client/data/c/c;->c:J

    const-wide/16 v4, 0x64

    cmp-long v0, v2, v4

    if-nez v0, :cond_146

    .line 79
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/c;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02006d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 80
    :goto_39
    iget-wide v2, p0, Lcom/teamspeak/ts3client/data/c/c;->c:J

    const-wide/16 v4, 0xc8

    cmp-long v1, v2, v4

    if-nez v1, :cond_4e

    .line 81
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/c;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02006e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 82
    :cond_4e
    iget-wide v2, p0, Lcom/teamspeak/ts3client/data/c/c;->c:J

    const-wide/16 v4, 0x12c

    cmp-long v1, v2, v4

    if-nez v1, :cond_63

    .line 83
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/c;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02006f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 84
    :cond_63
    iget-wide v2, p0, Lcom/teamspeak/ts3client/data/c/c;->c:J

    const-wide/16 v4, 0x1f4

    cmp-long v1, v2, v4

    if-nez v1, :cond_78

    .line 85
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/c;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020070

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 86
    :cond_78
    iget-wide v2, p0, Lcom/teamspeak/ts3client/data/c/c;->c:J

    const-wide/16 v4, 0x258

    cmp-long v1, v2, v4

    if-nez v1, :cond_8d

    .line 87
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/c;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/Ts3Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020071

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 88
    :cond_8d
    if-nez v0, :cond_9e

    .line 89
    iget-object v1, p0, Lcom/teamspeak/ts3client/data/c/c;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/Ts3Application;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020069

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/teamspeak/ts3client/data/c/c;->d:Landroid/graphics/drawable/Drawable;

    .line 91
    :cond_9e
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/teamspeak/ts3client/data/c/c;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {p0}, Lcom/teamspeak/ts3client/data/c/c;->c()I

    move-result v3

    invoke-direct {p0}, Lcom/teamspeak/ts3client/data/c/c;->c()I

    move-result v4

    invoke-static {v0, v3, v4, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lcom/teamspeak/ts3client/data/c/c;->d:Landroid/graphics/drawable/Drawable;

    .line 92
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/c;->d:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_e

    .line 94
    :cond_c1
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/teamspeak/ts3client/data/c/c;->b:Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "icon_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/teamspeak/ts3client/data/c/c;->c:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".gif"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 95
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_141

    .line 96
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/drawable/Drawable;->createFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 98
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-ne v2, v6, :cond_11c

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-ne v2, v6, :cond_11c

    .line 106
    :goto_ff
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/teamspeak/ts3client/data/c/c;->e:Lcom/teamspeak/ts3client/Ts3Application;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/Ts3Application;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {p0}, Lcom/teamspeak/ts3client/data/c/c;->c()I

    move-result v3

    invoke-direct {p0}, Lcom/teamspeak/ts3client/data/c/c;->c()I

    move-result v4

    invoke-static {v0, v3, v4, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lcom/teamspeak/ts3client/data/c/c;->d:Landroid/graphics/drawable/Drawable;

    .line 110
    :goto_118
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/c;->d:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_e

    .line 101
    :cond_11c
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v6, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 102
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 103
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    rsub-int/lit8 v4, v4, 0x8

    int-to-float v4, v4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    rsub-int/lit8 v5, v5, 0x8

    int-to-float v5, v5

    invoke-virtual {v3, v0, v4, v5, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 104
    invoke-virtual {v3}, Landroid/graphics/Canvas;->save()I

    move-object v0, v2

    goto :goto_ff

    .line 108
    :cond_141
    sget-object v0, Lcom/teamspeak/ts3client/data/c/c;->a:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/teamspeak/ts3client/data/c/c;->d:Landroid/graphics/drawable/Drawable;

    goto :goto_118

    :cond_146
    move-object v0, v1

    goto/16 :goto_39
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
    .registers 8

    .prologue
    .line 54
    instance-of v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/FileTransferStatus;

    if-eqz v0, :cond_86

    move-object v0, p1

    .line 55
    check-cast v0, Lcom/teamspeak/ts3client/jni/events/rare/FileTransferStatus;

    .line 5041
    iget-wide v0, v0, Lcom/teamspeak/ts3client/jni/events/rare/FileTransferStatus;->b:J

    .line 55
    const-wide/16 v2, 0x811

    cmp-long v0, v0, v2

    if-nez v0, :cond_86

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/c/c;->f:Ljava/util/ArrayList;

    move-object v0, p1

    check-cast v0, Lcom/teamspeak/ts3client/jni/events/rare/FileTransferStatus;

    .line 5049
    iget v0, v0, Lcom/teamspeak/ts3client/jni/events/rare/FileTransferStatus;->a:I

    .line 55
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_86

    .line 56
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/c/c;->b:Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "icon_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/teamspeak/ts3client/data/c/c;->c:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 57
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/teamspeak/ts3client/data/c/c;->b:Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "icon_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/teamspeak/ts3client/data/c/c;->c:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".gif"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 58
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/c;->f:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/teamspeak/ts3client/data/c/c;->f:Ljava/util/ArrayList;

    check-cast p1, Lcom/teamspeak/ts3client/jni/events/rare/FileTransferStatus;

    .line 6049
    iget v2, p1, Lcom/teamspeak/ts3client/jni/events/rare/FileTransferStatus;->a:I

    .line 58
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 59
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/c;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 6061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 6206
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->k:Lcom/teamspeak/ts3client/t;

    .line 59
    invoke-virtual {v0}, Lcom/teamspeak/ts3client/t;->z()V

    .line 60
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/c;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_86

    .line 61
    iget-object v0, p0, Lcom/teamspeak/ts3client/data/c/c;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 7061
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 7238
    iget-object v0, v0, Lcom/teamspeak/ts3client/data/e;->c:Lcom/teamspeak/ts3client/jni/l;

    .line 61
    invoke-virtual {v0, p0}, Lcom/teamspeak/ts3client/jni/l;->b(Lcom/teamspeak/ts3client/data/w;)V

    .line 65
    :cond_86
    return-void
.end method
