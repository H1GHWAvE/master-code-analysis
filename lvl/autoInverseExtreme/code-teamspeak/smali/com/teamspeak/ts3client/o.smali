.class final Lcom/teamspeak/ts3client/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/ConnectionBackground;

.field private b:I

.field private c:I

.field private d:F

.field private e:F

.field private f:J

.field private g:J


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/ConnectionBackground;)V
    .registers 2

    .prologue
    .line 1713
    iput-object p1, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 11

    .prologue
    const-wide/32 v6, 0x1dcd6500

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1724
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_1de

    move v0, v1

    .line 1773
    :cond_d
    :goto_d
    return v0

    .line 1726
    :pswitch_e
    iget-object v2, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    iget-wide v2, v2, Lcom/teamspeak/ts3client/ConnectionBackground;->a:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_5e

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    iget-object v4, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    iget-wide v4, v4, Lcom/teamspeak/ts3client/ConnectionBackground;->a:J

    sub-long/2addr v2, v4

    cmp-long v2, v2, v6

    if-gez v2, :cond_5e

    .line 1727
    iget-object v2, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    iput-boolean v0, v2, Lcom/teamspeak/ts3client/ConnectionBackground;->b:Z

    .line 1728
    iget-object v2, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v2}, Lcom/teamspeak/ts3client/ConnectionBackground;->a(Lcom/teamspeak/ts3client/ConnectionBackground;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v2, p0, Lcom/teamspeak/ts3client/o;->b:I

    .line 1729
    iget-object v2, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v2}, Lcom/teamspeak/ts3client/ConnectionBackground;->a(Lcom/teamspeak/ts3client/ConnectionBackground;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    iput v2, p0, Lcom/teamspeak/ts3client/o;->c:I

    .line 1730
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    iput v2, p0, Lcom/teamspeak/ts3client/o;->d:F

    .line 1731
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    iput v2, p0, Lcom/teamspeak/ts3client/o;->e:F

    .line 1732
    iget-object v2, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v2}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(Lcom/teamspeak/ts3client/ConnectionBackground;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->performHapticFeedback(I)Z

    .line 1733
    iget-object v1, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(Lcom/teamspeak/ts3client/ConnectionBackground;)Landroid/widget/ImageView;

    move-result-object v1

    const/16 v2, 0x7d

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setAlpha(I)V

    goto :goto_d

    .line 1735
    :cond_5e
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/teamspeak/ts3client/o;->f:J

    .line 1736
    iget-object v2, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v2}, Lcom/teamspeak/ts3client/ConnectionBackground;->d(Lcom/teamspeak/ts3client/ConnectionBackground;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 2061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2234
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1736
    iget-object v3, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v3}, Lcom/teamspeak/ts3client/ConnectionBackground;->d(Lcom/teamspeak/ts3client/ConnectionBackground;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v3

    .line 3061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1736
    sget-object v3, Lcom/teamspeak/ts3client/jni/d;->k:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v2, v4, v5, v3, v1}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 1737
    iget-object v1, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v1}, Lcom/teamspeak/ts3client/ConnectionBackground;->d(Lcom/teamspeak/ts3client/ConnectionBackground;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 4061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 4157
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->b:Lcom/teamspeak/ts3client/a/k;

    .line 1737
    invoke-virtual {v1}, Lcom/teamspeak/ts3client/a/k;->d()V

    .line 1738
    iget-object v1, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    iput-boolean v0, v1, Lcom/teamspeak/ts3client/ConnectionBackground;->c:Z

    .line 1739
    iget-object v1, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    iget-object v2, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/ConnectionBackground;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    iput v2, v1, Lcom/teamspeak/ts3client/ConnectionBackground;->d:I

    goto/16 :goto_d

    .line 1743
    :pswitch_a0
    iget-object v2, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    iget-boolean v2, v2, Lcom/teamspeak/ts3client/ConnectionBackground;->b:Z

    if-eqz v2, :cond_157

    .line 1744
    iget-object v2, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    iput-boolean v1, v2, Lcom/teamspeak/ts3client/ConnectionBackground;->b:Z

    .line 1745
    iget-object v1, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/ConnectionBackground;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_f1

    .line 1746
    iget-object v1, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v1}, Lcom/teamspeak/ts3client/ConnectionBackground;->d(Lcom/teamspeak/ts3client/ConnectionBackground;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 5093
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1746
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "overlay_last_x_l"

    iget-object v3, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v3}, Lcom/teamspeak/ts3client/ConnectionBackground;->a(Lcom/teamspeak/ts3client/ConnectionBackground;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->x:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "overlay_last_y_l"

    iget-object v3, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v3}, Lcom/teamspeak/ts3client/ConnectionBackground;->a(Lcom/teamspeak/ts3client/ConnectionBackground;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->y:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1753
    :goto_e4
    iget-object v1, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v1}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(Lcom/teamspeak/ts3client/ConnectionBackground;)Landroid/widget/ImageView;

    move-result-object v1

    const/16 v2, 0xff

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setAlpha(I)V

    goto/16 :goto_d

    .line 1747
    :cond_f1
    iget-object v1, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/ConnectionBackground;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v0, :cond_12b

    .line 1748
    iget-object v1, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v1}, Lcom/teamspeak/ts3client/ConnectionBackground;->d(Lcom/teamspeak/ts3client/ConnectionBackground;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 6093
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1748
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "overlay_last_x_p"

    iget-object v3, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v3}, Lcom/teamspeak/ts3client/ConnectionBackground;->a(Lcom/teamspeak/ts3client/ConnectionBackground;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->x:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "overlay_last_y_p"

    iget-object v3, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v3}, Lcom/teamspeak/ts3client/ConnectionBackground;->a(Lcom/teamspeak/ts3client/ConnectionBackground;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->y:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_e4

    .line 1750
    :cond_12b
    iget-object v1, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v1}, Lcom/teamspeak/ts3client/ConnectionBackground;->d(Lcom/teamspeak/ts3client/ConnectionBackground;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 7093
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1750
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "overlay_last_x_u"

    iget-object v3, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v3}, Lcom/teamspeak/ts3client/ConnectionBackground;->a(Lcom/teamspeak/ts3client/ConnectionBackground;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->x:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "overlay_last_y_u"

    iget-object v3, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v3}, Lcom/teamspeak/ts3client/ConnectionBackground;->a(Lcom/teamspeak/ts3client/ConnectionBackground;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->y:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_e4

    .line 1755
    :cond_157
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/teamspeak/ts3client/o;->g:J

    .line 1756
    iget-object v2, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v2}, Lcom/teamspeak/ts3client/ConnectionBackground;->d(Lcom/teamspeak/ts3client/ConnectionBackground;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 8061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 8234
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->a:Lcom/teamspeak/ts3client/jni/Ts3Jni;

    .line 1756
    iget-object v3, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v3}, Lcom/teamspeak/ts3client/ConnectionBackground;->d(Lcom/teamspeak/ts3client/ConnectionBackground;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v3

    .line 9061
    iget-object v3, v3, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 9267
    iget-wide v4, v3, Lcom/teamspeak/ts3client/data/e;->e:J

    .line 1756
    sget-object v3, Lcom/teamspeak/ts3client/jni/d;->k:Lcom/teamspeak/ts3client/jni/d;

    invoke-virtual {v2, v4, v5, v3, v0}, Lcom/teamspeak/ts3client/jni/Ts3Jni;->a(JLcom/teamspeak/ts3client/jni/d;I)I

    .line 1757
    iget-object v2, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v2}, Lcom/teamspeak/ts3client/ConnectionBackground;->d(Lcom/teamspeak/ts3client/ConnectionBackground;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 10061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 10157
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->b:Lcom/teamspeak/ts3client/a/k;

    .line 1757
    invoke-virtual {v2}, Lcom/teamspeak/ts3client/a/k;->f()V

    .line 1758
    iget-object v2, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    iput-boolean v1, v2, Lcom/teamspeak/ts3client/ConnectionBackground;->c:Z

    .line 1759
    iget-wide v2, p0, Lcom/teamspeak/ts3client/o;->g:J

    iget-wide v4, p0, Lcom/teamspeak/ts3client/o;->f:J

    sub-long/2addr v2, v4

    cmp-long v1, v2, v6

    if-gez v1, :cond_d

    .line 1760
    iget-object v1, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/teamspeak/ts3client/ConnectionBackground;->a:J

    goto/16 :goto_d

    .line 1766
    :pswitch_19a
    iget-object v1, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    iget-boolean v1, v1, Lcom/teamspeak/ts3client/ConnectionBackground;->b:Z

    if-eqz v1, :cond_d

    .line 1767
    iget-object v1, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v1}, Lcom/teamspeak/ts3client/ConnectionBackground;->a(Lcom/teamspeak/ts3client/ConnectionBackground;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iget v2, p0, Lcom/teamspeak/ts3client/o;->b:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    iget v4, p0, Lcom/teamspeak/ts3client/o;->d:F

    sub-float/2addr v3, v4

    float-to-int v3, v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 1768
    iget-object v1, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v1}, Lcom/teamspeak/ts3client/ConnectionBackground;->a(Lcom/teamspeak/ts3client/ConnectionBackground;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iget v2, p0, Lcom/teamspeak/ts3client/o;->c:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    iget v4, p0, Lcom/teamspeak/ts3client/o;->e:F

    sub-float/2addr v3, v4

    float-to-int v3, v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 1769
    iget-object v1, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v1}, Lcom/teamspeak/ts3client/ConnectionBackground;->c(Lcom/teamspeak/ts3client/ConnectionBackground;)Landroid/view/WindowManager;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v2}, Lcom/teamspeak/ts3client/ConnectionBackground;->b(Lcom/teamspeak/ts3client/ConnectionBackground;)Landroid/widget/ImageView;

    move-result-object v2

    iget-object v3, p0, Lcom/teamspeak/ts3client/o;->a:Lcom/teamspeak/ts3client/ConnectionBackground;

    invoke-static {v3}, Lcom/teamspeak/ts3client/ConnectionBackground;->a(Lcom/teamspeak/ts3client/ConnectionBackground;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_d

    .line 1724
    nop

    :pswitch_data_1de
    .packed-switch 0x0
        :pswitch_e
        :pswitch_a0
        :pswitch_19a
    .end packed-switch
.end method
