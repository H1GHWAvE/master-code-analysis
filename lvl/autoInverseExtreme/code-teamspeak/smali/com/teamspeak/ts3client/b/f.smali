.class public final Lcom/teamspeak/ts3client/b/f;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field a:Lcom/teamspeak/ts3client/b/j;

.field final synthetic b:Lcom/teamspeak/ts3client/b/b;

.field private c:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Lcom/teamspeak/ts3client/b/b;Landroid/content/Context;)V
    .registers 4

    .prologue
    .line 159
    iput-object p1, p0, Lcom/teamspeak/ts3client/b/f;->b:Lcom/teamspeak/ts3client/b/b;

    .line 160
    const v0, 0x1090003

    invoke-direct {p0, p2, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 161
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/b/f;->c:Landroid/view/LayoutInflater;

    .line 162
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Landroid/widget/TableRow;
    .registers 7

    .prologue
    const/4 v3, 0x0

    .line 238
    new-instance v0, Landroid/widget/TableRow;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/b/f;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 239
    new-instance v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/b/f;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 240
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 241
    const/4 v2, 0x5

    invoke-virtual {v1, v3, v3, v2, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 242
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/b/f;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f070082

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 243
    new-instance v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/b/f;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 244
    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 245
    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 246
    invoke-virtual {v0, v2}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 247
    return-object v0
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/b/f;Lcom/teamspeak/ts3client/jni/events/rare/BanList;Landroid/widget/TableLayout;)V
    .registers 3

    .prologue
    .line 155
    invoke-direct {p0, p1, p2}, Lcom/teamspeak/ts3client/b/f;->a(Lcom/teamspeak/ts3client/jni/events/rare/BanList;Landroid/widget/TableLayout;)V

    return-void
.end method

.method private a(Lcom/teamspeak/ts3client/jni/events/rare/BanList;Landroid/widget/TableLayout;)V
    .registers 5

    .prologue
    .line 226
    .line 3090
    iget-object v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->c:Ljava/lang/String;

    .line 226
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_15

    .line 227
    const-string v0, "Name"

    .line 4090
    iget-object v1, p1, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->c:Ljava/lang/String;

    .line 227
    invoke-direct {p0, v0, v1}, Lcom/teamspeak/ts3client/b/f;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/widget/TableRow;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 5086
    :cond_15
    iget-object v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->b:Ljava/lang/String;

    .line 228
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2a

    .line 229
    const-string v0, "IP"

    .line 6086
    iget-object v1, p1, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->b:Ljava/lang/String;

    .line 229
    invoke-direct {p0, v0, v1}, Lcom/teamspeak/ts3client/b/f;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/widget/TableRow;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 6094
    :cond_2a
    iget-object v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->d:Ljava/lang/String;

    .line 230
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3f

    .line 231
    const-string v0, "Uid"

    .line 7094
    iget-object v1, p1, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->d:Ljava/lang/String;

    .line 231
    invoke-direct {p0, v0, v1}, Lcom/teamspeak/ts3client/b/f;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/widget/TableRow;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 7126
    :cond_3f
    iget-object v0, p1, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->h:Ljava/lang/String;

    .line 232
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_54

    .line 233
    const-string v0, "Nick"

    .line 8126
    iget-object v1, p1, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->h:Ljava/lang/String;

    .line 233
    invoke-direct {p0, v0, v1}, Lcom/teamspeak/ts3client/b/f;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/widget/TableRow;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 234
    :cond_54
    const-string v0, "banlist.creator"

    invoke-static {v0}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 9106
    iget-object v1, p1, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->e:Ljava/lang/String;

    .line 234
    invoke-direct {p0, v0, v1}, Lcom/teamspeak/ts3client/b/f;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/widget/TableRow;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 235
    return-void
.end method


# virtual methods
.method public final getFilter()Landroid/widget/Filter;
    .registers 2

    .prologue
    .line 252
    iget-object v0, p0, Lcom/teamspeak/ts3client/b/f;->a:Lcom/teamspeak/ts3client/b/j;

    if-nez v0, :cond_b

    .line 253
    new-instance v0, Lcom/teamspeak/ts3client/b/j;

    invoke-direct {v0, p0}, Lcom/teamspeak/ts3client/b/j;-><init>(Lcom/teamspeak/ts3client/b/f;)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/b/f;->a:Lcom/teamspeak/ts3client/b/j;

    .line 254
    :cond_b
    iget-object v0, p0, Lcom/teamspeak/ts3client/b/f;->a:Lcom/teamspeak/ts3client/b/j;

    return-object v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 10

    .prologue
    .line 166
    iget-object v0, p0, Lcom/teamspeak/ts3client/b/f;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f03001b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 167
    const v0, 0x7f0c0087

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableLayout;

    .line 168
    const v1, 0x7f0c0086

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 169
    const v2, 0x7f0c0088

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 170
    invoke-virtual {p0, p1}, Lcom/teamspeak/ts3client/b/f;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/teamspeak/ts3client/jni/events/rare/BanList;

    .line 171
    invoke-direct {p0, v3, v0}, Lcom/teamspeak/ts3client/b/f;->a(Lcom/teamspeak/ts3client/jni/events/rare/BanList;Landroid/widget/TableLayout;)V

    .line 172
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "banlist.reason"

    invoke-static {v5}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1118
    iget-object v5, v3, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->g:Ljava/lang/String;

    .line 172
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 173
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/b/f;->getContext()Landroid/content/Context;

    move-result-object v0

    const v5, 0x7f070083

    invoke-virtual {v1, v0, v5}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 174
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "banlist.created"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1134
    iget-object v1, v3, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->k:Ljava/lang/String;

    .line 174
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "banlist.expires"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 2130
    iget-object v1, v3, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->j:Ljava/lang/String;

    .line 174
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/b/f;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f070081

    invoke-virtual {v2, v0, v1}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 176
    new-instance v0, Lcom/teamspeak/ts3client/b/g;

    invoke-direct {v0, p0, v3}, Lcom/teamspeak/ts3client/b/g;-><init>(Lcom/teamspeak/ts3client/b/f;Lcom/teamspeak/ts3client/jni/events/rare/BanList;)V

    invoke-virtual {v4, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 221
    return-object v4
.end method
