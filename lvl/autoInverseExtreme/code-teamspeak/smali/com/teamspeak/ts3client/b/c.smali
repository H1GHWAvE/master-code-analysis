.class final Lcom/teamspeak/ts3client/b/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/b/b;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/b/b;)V
    .registers 2

    .prologue
    .line 81
    iput-object p1, p0, Lcom/teamspeak/ts3client/b/c;->a:Lcom/teamspeak/ts3client/b/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .registers 2

    .prologue
    .line 112
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5

    .prologue
    .line 106
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 7

    .prologue
    .line 85
    if-lez p4, :cond_10

    .line 86
    iget-object v0, p0, Lcom/teamspeak/ts3client/b/c;->a:Lcom/teamspeak/ts3client/b/b;

    invoke-static {v0}, Lcom/teamspeak/ts3client/b/b;->a(Lcom/teamspeak/ts3client/b/b;)Lcom/teamspeak/ts3client/b/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/b/f;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 100
    :goto_f
    return-void

    .line 88
    :cond_10
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 1077
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 88
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/b/d;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/b/d;-><init>(Lcom/teamspeak/ts3client/b/c;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_f
.end method
