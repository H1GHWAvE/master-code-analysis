.class final Lcom/teamspeak/ts3client/b/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/jni/events/rare/BanList;

.field final synthetic b:Lcom/teamspeak/ts3client/b/f;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/b/f;Lcom/teamspeak/ts3client/jni/events/rare/BanList;)V
    .registers 3

    .prologue
    .line 176
    iput-object p1, p0, Lcom/teamspeak/ts3client/b/g;->b:Lcom/teamspeak/ts3client/b/f;

    iput-object p2, p0, Lcom/teamspeak/ts3client/b/g;->a:Lcom/teamspeak/ts3client/jni/events/rare/BanList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLongClick(Landroid/view/View;)Z
    .registers 9

    .prologue
    const/4 v4, -0x1

    const/4 v6, 0x0

    .line 180
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/teamspeak/ts3client/b/g;->b:Lcom/teamspeak/ts3client/b/f;

    invoke-virtual {v1}, Lcom/teamspeak/ts3client/b/f;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 181
    new-instance v1, Landroid/widget/TableLayout;

    iget-object v2, p0, Lcom/teamspeak/ts3client/b/g;->b:Lcom/teamspeak/ts3client/b/f;

    iget-object v2, v2, Lcom/teamspeak/ts3client/b/f;->b:Lcom/teamspeak/ts3client/b/b;

    invoke-virtual {v2}, Lcom/teamspeak/ts3client/b/b;->i()Landroid/support/v4/app/bb;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/TableLayout;-><init>(Landroid/content/Context;)V

    .line 182
    iget-object v2, p0, Lcom/teamspeak/ts3client/b/g;->b:Lcom/teamspeak/ts3client/b/f;

    iget-object v3, p0, Lcom/teamspeak/ts3client/b/g;->a:Lcom/teamspeak/ts3client/jni/events/rare/BanList;

    invoke-static {v2, v3, v1}, Lcom/teamspeak/ts3client/b/f;->a(Lcom/teamspeak/ts3client/b/f;Lcom/teamspeak/ts3client/jni/events/rare/BanList;Landroid/widget/TableLayout;)V

    .line 183
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    .line 184
    invoke-static {v0}, Lcom/teamspeak/ts3client/data/d/q;->a(Landroid/app/Dialog;)V

    .line 185
    const-string v1, "banlist.dialog.info"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 187
    const/4 v1, -0x2

    const-string v2, "button.cancel"

    invoke-static {v2}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/teamspeak/ts3client/b/h;

    invoke-direct {v3, p0, v0}, Lcom/teamspeak/ts3client/b/h;-><init>(Lcom/teamspeak/ts3client/b/g;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 195
    const-string v1, "button.delete"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/teamspeak/ts3client/b/i;

    invoke-direct {v2, p0, v0}, Lcom/teamspeak/ts3client/b/i;-><init>(Lcom/teamspeak/ts3client/b/g;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v4, v1, v2}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 204
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 205
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 206
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 207
    iget-object v1, p0, Lcom/teamspeak/ts3client/b/g;->b:Lcom/teamspeak/ts3client/b/f;

    iget-object v1, v1, Lcom/teamspeak/ts3client/b/f;->b:Lcom/teamspeak/ts3client/b/b;

    invoke-static {v1}, Lcom/teamspeak/ts3client/b/b;->c(Lcom/teamspeak/ts3client/b/b;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 1061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 1181
    iget-object v1, v1, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 207
    sget-object v2, Lcom/teamspeak/ts3client/jni/g;->dc:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v1, v2}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v1

    .line 208
    iget-object v2, p0, Lcom/teamspeak/ts3client/b/g;->b:Lcom/teamspeak/ts3client/b/f;

    iget-object v2, v2, Lcom/teamspeak/ts3client/b/f;->b:Lcom/teamspeak/ts3client/b/b;

    invoke-static {v2}, Lcom/teamspeak/ts3client/b/b;->c(Lcom/teamspeak/ts3client/b/b;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v2

    .line 2061
    iget-object v2, v2, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 2181
    iget-object v2, v2, Lcom/teamspeak/ts3client/data/e;->s:Lcom/teamspeak/ts3client/data/f/a;

    .line 208
    sget-object v3, Lcom/teamspeak/ts3client/jni/g;->dd:Lcom/teamspeak/ts3client/jni/g;

    invoke-virtual {v2, v3}, Lcom/teamspeak/ts3client/data/f/a;->a(Lcom/teamspeak/ts3client/jni/g;)Z

    move-result v2

    .line 209
    if-nez v2, :cond_87

    .line 210
    if-nez v1, :cond_88

    .line 211
    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 218
    :cond_87
    :goto_87
    return v6

    .line 213
    :cond_88
    iget-object v1, p0, Lcom/teamspeak/ts3client/b/g;->b:Lcom/teamspeak/ts3client/b/f;

    iget-object v1, v1, Lcom/teamspeak/ts3client/b/f;->b:Lcom/teamspeak/ts3client/b/b;

    invoke-static {v1}, Lcom/teamspeak/ts3client/b/b;->c(Lcom/teamspeak/ts3client/b/b;)Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v1

    .line 3061
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->a:Lcom/teamspeak/ts3client/data/e;

    .line 3383
    iget-wide v2, v1, Lcom/teamspeak/ts3client/data/e;->z:J

    .line 213
    iget-object v1, p0, Lcom/teamspeak/ts3client/b/g;->a:Lcom/teamspeak/ts3client/jni/events/rare/BanList;

    .line 4110
    iget-wide v4, v1, Lcom/teamspeak/ts3client/jni/events/rare/BanList;->f:J

    .line 213
    cmp-long v1, v2, v4

    if-eqz v1, :cond_87

    .line 214
    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_87
.end method
