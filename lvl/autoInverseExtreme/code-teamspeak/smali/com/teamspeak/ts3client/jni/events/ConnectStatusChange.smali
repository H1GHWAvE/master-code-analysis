.class public Lcom/teamspeak/ts3client/jni/events/ConnectStatusChange;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/jni/k;


# instance fields
.field private a:J

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    return-void
.end method

.method private constructor <init>(JII)V
    .registers 6

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-wide p1, p0, Lcom/teamspeak/ts3client/jni/events/ConnectStatusChange;->a:J

    .line 20
    iput p3, p0, Lcom/teamspeak/ts3client/jni/events/ConnectStatusChange;->b:I

    .line 21
    iput p4, p0, Lcom/teamspeak/ts3client/jni/events/ConnectStatusChange;->c:I

    .line 22
    invoke-static {p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/jni/k;)V

    .line 23
    return-void
.end method

.method private c()J
    .registers 3

    .prologue
    .line 34
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/ConnectStatusChange;->a:J

    return-wide v0
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 26
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/ConnectStatusChange;->c:I

    return v0
.end method

.method public final b()I
    .registers 2

    .prologue
    .line 30
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/ConnectStatusChange;->b:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 39
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ConnectStatusChange [serverConnectionHandlerID="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/ConnectStatusChange;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", newStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/ConnectStatusChange;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", errorNumber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/ConnectStatusChange;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
