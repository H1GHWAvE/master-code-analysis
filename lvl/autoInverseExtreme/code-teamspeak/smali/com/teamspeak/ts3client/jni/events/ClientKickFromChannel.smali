.class public Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/teamspeak/ts3client/jni/k;


# instance fields
.field private a:J

.field private b:I

.field private c:J

.field private d:J

.field private e:Lcom/teamspeak/ts3client/jni/j;

.field private f:I

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    return-void
.end method

.method private constructor <init>(JIJJIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 15

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-wide p1, p0, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->a:J

    .line 27
    iput p3, p0, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->b:I

    .line 28
    iput-wide p4, p0, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->c:J

    .line 29
    iput-wide p6, p0, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->d:J

    .line 30
    if-nez p8, :cond_11

    .line 31
    sget-object v0, Lcom/teamspeak/ts3client/jni/j;->a:Lcom/teamspeak/ts3client/jni/j;

    iput-object v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->e:Lcom/teamspeak/ts3client/jni/j;

    .line 32
    :cond_11
    const/4 v0, 0x1

    if-ne p8, v0, :cond_18

    .line 33
    sget-object v0, Lcom/teamspeak/ts3client/jni/j;->b:Lcom/teamspeak/ts3client/jni/j;

    iput-object v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->e:Lcom/teamspeak/ts3client/jni/j;

    .line 34
    :cond_18
    const/4 v0, 0x2

    if-ne p8, v0, :cond_1f

    .line 35
    sget-object v0, Lcom/teamspeak/ts3client/jni/j;->c:Lcom/teamspeak/ts3client/jni/j;

    iput-object v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->e:Lcom/teamspeak/ts3client/jni/j;

    .line 36
    :cond_1f
    iput p9, p0, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->f:I

    .line 37
    iput-object p10, p0, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->g:Ljava/lang/String;

    .line 38
    iput-object p11, p0, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->h:Ljava/lang/String;

    .line 39
    iput-object p12, p0, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->i:Ljava/lang/String;

    .line 40
    invoke-static {p0}, Lcom/teamspeak/ts3client/jni/l;->a(Lcom/teamspeak/ts3client/jni/k;)V

    .line 41
    return-void
.end method

.method private g()I
    .registers 2

    .prologue
    .line 48
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->f:I

    return v0
.end method

.method private h()Ljava/lang/String;
    .registers 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private i()J
    .registers 3

    .prologue
    .line 72
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->a:J

    return-wide v0
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 44
    iget v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->b:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final d()J
    .registers 3

    .prologue
    .line 64
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->d:J

    return-wide v0
.end method

.method public final e()J
    .registers 3

    .prologue
    .line 68
    iget-wide v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->c:J

    return-wide v0
.end method

.method public final f()Lcom/teamspeak/ts3client/jni/j;
    .registers 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->e:Lcom/teamspeak/ts3client/jni/j;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ClientKickFromChannel [serverConnectionHandlerID="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clientID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", oldChannelID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", newChannelID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->d:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", visibility="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->e:Lcom/teamspeak/ts3client/jni/j;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", kickerID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", kickerName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", kickerUniqueIdentifier="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", kickMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/jni/events/ClientKickFromChannel;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
