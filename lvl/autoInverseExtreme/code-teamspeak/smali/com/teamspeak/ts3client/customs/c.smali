.class final Lcom/teamspeak/ts3client/customs/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)V
    .registers 2

    .prologue
    .line 132
    iput-object p1, p0, Lcom/teamspeak/ts3client/customs/c;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 12

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 136
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/c;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 159
    :goto_b
    return-void

    .line 138
    :cond_c
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/c;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->d(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/c;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v1}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->b(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Ljava/util/BitSet;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/customs/c;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v2}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->c(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)I

    move-result v2

    invoke-static {p3, v0, v1, v2}, Lcom/teamspeak/ts3client/data/d/y;->a(IILjava/util/BitSet;I)Lcom/teamspeak/ts3client/data/d/aa;

    move-result-object v0

    .line 139
    iget-object v1, p0, Lcom/teamspeak/ts3client/customs/c;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v1, v4}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;Z)Z

    .line 140
    sget-object v1, Lcom/teamspeak/ts3client/customs/e;->a:[I

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/data/d/aa;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_7c

    .line 154
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/c;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->h(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/Spinner;->setSelection(I)V

    .line 156
    :goto_3f
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/c;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v0, v3}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->a(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;Z)Z

    .line 157
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/c;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->g(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)V

    .line 158
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/c;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->f(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Lcom/teamspeak/ts3client/data/d/ab;

    move-result-object v0

    .line 1140
    iput p3, v0, Lcom/teamspeak/ts3client/data/d/ab;->a:I

    goto :goto_b

    .line 142
    :pswitch_52
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/c;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->h(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_3f

    .line 145
    :pswitch_5c
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/c;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->h(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_3f

    .line 148
    :pswitch_66
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/c;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->h(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Landroid/widget/Spinner;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_3f

    .line 151
    :pswitch_71
    iget-object v0, p0, Lcom/teamspeak/ts3client/customs/c;->a:Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

    invoke-static {v0}, Lcom/teamspeak/ts3client/customs/CustomCodecSettings;->h(Lcom/teamspeak/ts3client/customs/CustomCodecSettings;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_3f

    .line 140
    nop

    :pswitch_data_7c
    .packed-switch 0x1
        :pswitch_52
        :pswitch_5c
        :pswitch_66
        :pswitch_71
    .end packed-switch
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .registers 2

    .prologue
    .line 165
    return-void
.end method
