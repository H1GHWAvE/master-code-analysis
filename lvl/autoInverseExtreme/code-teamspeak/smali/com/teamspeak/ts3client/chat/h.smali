.class public final Lcom/teamspeak/ts3client/chat/h;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field a:Ljava/util/ArrayList;

.field b:Lcom/teamspeak/ts3client/chat/a;

.field private c:Landroid/content/Context;

.field private d:Landroid/view/LayoutInflater;

.field private e:Lcom/teamspeak/ts3client/Ts3Application;

.field private f:Ljava/text/DateFormat;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/teamspeak/ts3client/chat/h;->c:Landroid/content/Context;

    .line 32
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/Ts3Application;

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/h;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 33
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/h;->d:Landroid/view/LayoutInflater;

    .line 34
    invoke-static {}, Ljava/text/DateFormat;->getDateTimeInstance()Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/h;->f:Ljava/text/DateFormat;

    .line 35
    return-void
.end method

.method private a(Lcom/teamspeak/ts3client/chat/a;)V
    .registers 3

    .prologue
    .line 109
    iput-object p1, p0, Lcom/teamspeak/ts3client/chat/h;->b:Lcom/teamspeak/ts3client/chat/a;

    .line 10109
    iput-object p0, p1, Lcom/teamspeak/ts3client/chat/a;->f:Lcom/teamspeak/ts3client/chat/h;

    .line 11080
    iget-object v0, p1, Lcom/teamspeak/ts3client/chat/a;->c:Ljava/util/ArrayList;

    .line 111
    iput-object v0, p0, Lcom/teamspeak/ts3client/chat/h;->a:Ljava/util/ArrayList;

    .line 112
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/chat/h;->a()V

    .line 113
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 3

    .prologue
    .line 116
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/h;->e:Lcom/teamspeak/ts3client/Ts3Application;

    .line 12077
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->c:Landroid/support/v4/app/Fragment;

    .line 116
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    new-instance v1, Lcom/teamspeak/ts3client/chat/i;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/chat/i;-><init>(Lcom/teamspeak/ts3client/chat/h;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 122
    return-void
.end method

.method public final getCount()I
    .registers 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/h;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 44
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/h;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .registers 4

    .prologue
    .line 50
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 16

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x1

    const/16 v9, 0x8

    .line 56
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/h;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f030028

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 57
    const v0, 0x7f0c00f8

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 58
    const v1, 0x7f0c00fa

    invoke-virtual {v7, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 59
    const v2, 0x7f0c00fd

    invoke-virtual {v7, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 60
    invoke-static {}, Lcom/teamspeak/ts3client/data/d/n;->a()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 61
    const v3, 0x7f0c00fe

    invoke-virtual {v7, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 62
    const v4, 0x7f0c00fc

    invoke-virtual {v7, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 63
    const v5, 0x7f0c00f9

    invoke-virtual {v7, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    .line 64
    iget-object v6, p0, Lcom/teamspeak/ts3client/chat/h;->a:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/teamspeak/ts3client/chat/y;

    .line 1061
    iget-boolean v6, v6, Lcom/teamspeak/ts3client/chat/y;->f:Z

    .line 64
    if-eqz v6, :cond_a7

    .line 65
    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 66
    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 67
    const-string v0, ""

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/h;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/chat/y;

    .line 2049
    iget-object v0, v0, Lcom/teamspeak/ts3client/chat/y;->b:Ljava/lang/String;

    .line 68
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    iget-object v1, p0, Lcom/teamspeak/ts3client/chat/h;->f:Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/h;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/chat/y;

    .line 2053
    iget-object v0, v0, Lcom/teamspeak/ts3client/chat/y;->e:Ljava/util/Date;

    .line 69
    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/h;->b:Lcom/teamspeak/ts3client/chat/a;

    .line 2123
    iget-object v0, v0, Lcom/teamspeak/ts3client/chat/a;->e:Ljava/lang/String;

    .line 70
    const-string v1, "CHANNEL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a0

    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/h;->b:Lcom/teamspeak/ts3client/chat/a;

    .line 3123
    iget-object v0, v0, Lcom/teamspeak/ts3client/chat/a;->e:Ljava/lang/String;

    .line 70
    const-string v1, "SERVER"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a0

    .line 71
    const v0, 0x7f020090

    invoke-virtual {v5, v0}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 101
    :cond_a0
    :goto_a0
    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 102
    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setFocusableInTouchMode(Z)V

    .line 105
    return-object v7

    .line 73
    :cond_a7
    iget-object v6, p0, Lcom/teamspeak/ts3client/chat/h;->b:Lcom/teamspeak/ts3client/chat/a;

    .line 4123
    iget-object v6, v6, Lcom/teamspeak/ts3client/chat/a;->e:Ljava/lang/String;

    .line 73
    const-string v8, "CHANNEL"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_fc

    .line 74
    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 75
    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 76
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "\""

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/h;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/chat/y;

    .line 5041
    iget-object v0, v0, Lcom/teamspeak/ts3client/chat/y;->a:Ljava/lang/String;

    .line 76
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    :goto_db
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/h;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/chat/y;

    .line 10037
    iget-object v0, v0, Lcom/teamspeak/ts3client/chat/y;->c:Landroid/text/Spanned;

    .line 97
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    iget-object v1, p0, Lcom/teamspeak/ts3client/chat/h;->f:Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/h;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/chat/y;

    .line 10053
    iget-object v0, v0, Lcom/teamspeak/ts3client/chat/y;->e:Ljava/util/Date;

    .line 98
    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_a0

    .line 79
    :cond_fc
    iget-object v6, p0, Lcom/teamspeak/ts3client/chat/h;->b:Lcom/teamspeak/ts3client/chat/a;

    .line 6123
    iget-object v6, v6, Lcom/teamspeak/ts3client/chat/a;->e:Ljava/lang/String;

    .line 79
    const-string v8, "SERVER"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_131

    .line 80
    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 81
    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 82
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "\""

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/h;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/teamspeak/ts3client/chat/y;

    .line 7041
    iget-object v0, v0, Lcom/teamspeak/ts3client/chat/y;->a:Ljava/lang/String;

    .line 82
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_db

    .line 86
    :cond_131
    const-string v6, ""

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v4, p0, Lcom/teamspeak/ts3client/chat/h;->a:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/teamspeak/ts3client/chat/y;

    .line 8057
    iget-boolean v4, v4, Lcom/teamspeak/ts3client/chat/y;->d:Z

    .line 87
    if-eqz v4, :cond_14c

    .line 88
    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 89
    const v0, 0x7f020091

    invoke-virtual {v5, v0}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_db

    .line 91
    :cond_14c
    iget-object v4, p0, Lcom/teamspeak/ts3client/chat/h;->b:Lcom/teamspeak/ts3client/chat/a;

    .line 8097
    iget-object v4, v4, Lcom/teamspeak/ts3client/chat/a;->g:Lcom/teamspeak/ts3client/data/c;

    .line 8118
    iget-object v4, v4, Lcom/teamspeak/ts3client/data/c;->y:Landroid/graphics/Bitmap;

    .line 91
    if-eqz v4, :cond_15d

    .line 92
    iget-object v4, p0, Lcom/teamspeak/ts3client/chat/h;->b:Lcom/teamspeak/ts3client/chat/a;

    .line 9097
    iget-object v4, v4, Lcom/teamspeak/ts3client/chat/a;->g:Lcom/teamspeak/ts3client/data/c;

    .line 9118
    iget-object v4, v4, Lcom/teamspeak/ts3client/data/c;->y:Landroid/graphics/Bitmap;

    .line 92
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 94
    :cond_15d
    invoke-virtual {v1, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 95
    const v0, 0x7f02008f

    invoke-virtual {v5, v0}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto/16 :goto_db
.end method
