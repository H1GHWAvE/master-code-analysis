.class public Lcom/teamspeak/ts3client/chat/MListView;
.super Landroid/widget/ListView;
.source "SourceFile"


# instance fields
.field private a:Lcom/teamspeak/ts3client/chat/x;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2

    .prologue
    .line 11
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 12
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    return-void
.end method


# virtual methods
.method protected onSizeChanged(IIII)V
    .registers 6

    .prologue
    .line 20
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ListView;->onSizeChanged(IIII)V

    .line 21
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/MListView;->a:Lcom/teamspeak/ts3client/chat/x;

    if-eqz v0, :cond_c

    .line 22
    iget-object v0, p0, Lcom/teamspeak/ts3client/chat/MListView;->a:Lcom/teamspeak/ts3client/chat/x;

    invoke-interface {v0}, Lcom/teamspeak/ts3client/chat/x;->a()V

    .line 24
    :cond_c
    return-void
.end method

.method setOnSizeChangedListener(Lcom/teamspeak/ts3client/chat/x;)V
    .registers 2

    .prologue
    .line 31
    iput-object p1, p0, Lcom/teamspeak/ts3client/chat/MListView;->a:Lcom/teamspeak/ts3client/chat/x;

    .line 32
    return-void
.end method
