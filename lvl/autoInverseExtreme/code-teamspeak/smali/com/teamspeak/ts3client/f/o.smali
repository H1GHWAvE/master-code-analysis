.class final Lcom/teamspeak/ts3client/f/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/Ts3Application;

.field final synthetic b:Landroid/widget/RelativeLayout;

.field final synthetic c:Lcom/teamspeak/ts3client/f/m;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/f/m;Lcom/teamspeak/ts3client/Ts3Application;Landroid/widget/RelativeLayout;)V
    .registers 4

    .prologue
    .line 134
    iput-object p1, p0, Lcom/teamspeak/ts3client/f/o;->c:Lcom/teamspeak/ts3client/f/m;

    iput-object p2, p0, Lcom/teamspeak/ts3client/f/o;->a:Lcom/teamspeak/ts3client/Ts3Application;

    iput-object p3, p0, Lcom/teamspeak/ts3client/f/o;->b:Landroid/widget/RelativeLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 5

    .prologue
    .line 137
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/o;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 1093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 137
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/o;->c:Lcom/teamspeak/ts3client/f/m;

    iget-object v1, v1, Lcom/teamspeak/ts3client/f/m;->at:Lcom/teamspeak/ts3client/f/k;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/k;->a(Lcom/teamspeak/ts3client/f/k;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/f/o;->c:Lcom/teamspeak/ts3client/f/m;

    iget-object v2, v2, Lcom/teamspeak/ts3client/f/m;->at:Lcom/teamspeak/ts3client/f/k;

    invoke-static {v2}, Lcom/teamspeak/ts3client/f/k;->b(Lcom/teamspeak/ts3client/f/k;)Landroid/widget/SeekBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    add-int/lit8 v2, v2, -0x3c

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 138
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 139
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/o;->c:Lcom/teamspeak/ts3client/f/m;

    iget-object v0, v0, Lcom/teamspeak/ts3client/f/m;->at:Lcom/teamspeak/ts3client/f/k;

    invoke-static {v0}, Lcom/teamspeak/ts3client/f/k;->d(Lcom/teamspeak/ts3client/f/k;)Lcom/teamspeak/ts3client/f/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/f/m;->b()V

    .line 140
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/o;->b:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 141
    return-void
.end method
