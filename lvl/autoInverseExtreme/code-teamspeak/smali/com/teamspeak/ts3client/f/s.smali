.class final Lcom/teamspeak/ts3client/f/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/teamspeak/ts3client/Ts3Application;

.field final synthetic b:Landroid/widget/RelativeLayout;

.field final synthetic c:Lcom/teamspeak/ts3client/f/r;


# direct methods
.method constructor <init>(Lcom/teamspeak/ts3client/f/r;Lcom/teamspeak/ts3client/Ts3Application;Landroid/widget/RelativeLayout;)V
    .registers 4

    .prologue
    .line 143
    iput-object p1, p0, Lcom/teamspeak/ts3client/f/s;->c:Lcom/teamspeak/ts3client/f/r;

    iput-object p2, p0, Lcom/teamspeak/ts3client/f/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    iput-object p3, p0, Lcom/teamspeak/ts3client/f/s;->b:Landroid/widget/RelativeLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 6

    .prologue
    .line 146
    const-string v1, ""

    .line 147
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/s;->c:Lcom/teamspeak/ts3client/f/r;

    iget-object v0, v0, Lcom/teamspeak/ts3client/f/r;->at:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v0

    move v3, v0

    move-object v0, v1

    move v1, v3

    :goto_e
    if-ltz v1, :cond_4a

    .line 148
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_32

    .line 149
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 147
    :goto_27
    iget-object v2, p0, Lcom/teamspeak/ts3client/f/s;->c:Lcom/teamspeak/ts3client/f/r;

    iget-object v2, v2, Lcom/teamspeak/ts3client/f/r;->at:Ljava/util/BitSet;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v2, v1}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v1

    goto :goto_e

    .line 151
    :cond_32
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_27

    .line 153
    :cond_4a
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 1093
    iget-object v1, v1, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 153
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/f/s;->c:Lcom/teamspeak/ts3client/f/r;

    iget-object v2, v2, Lcom/teamspeak/ts3client/f/r;->aw:Lcom/teamspeak/ts3client/f/p;

    invoke-static {v2}, Lcom/teamspeak/ts3client/f/p;->a(Lcom/teamspeak/ts3client/f/p;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 154
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/s;->a:Lcom/teamspeak/ts3client/Ts3Application;

    .line 2093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 154
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/teamspeak/ts3client/f/s;->c:Lcom/teamspeak/ts3client/f/r;

    iget-object v2, v2, Lcom/teamspeak/ts3client/f/r;->aw:Lcom/teamspeak/ts3client/f/p;

    invoke-static {v2}, Lcom/teamspeak/ts3client/f/p;->a(Lcom/teamspeak/ts3client/f/p;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_intercept"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/teamspeak/ts3client/f/s;->c:Lcom/teamspeak/ts3client/f/r;

    invoke-static {v2}, Lcom/teamspeak/ts3client/f/r;->a(Lcom/teamspeak/ts3client/f/r;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 155
    invoke-static {}, Lcom/teamspeak/ts3client/data/v;->a()Lcom/teamspeak/ts3client/data/v;

    move-result-object v0

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/s;->c:Lcom/teamspeak/ts3client/f/r;

    iget-object v1, v1, Lcom/teamspeak/ts3client/f/r;->at:Ljava/util/BitSet;

    iget-object v2, p0, Lcom/teamspeak/ts3client/f/s;->c:Lcom/teamspeak/ts3client/f/r;

    invoke-static {v2}, Lcom/teamspeak/ts3client/f/r;->a(Lcom/teamspeak/ts3client/f/r;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/data/v;->a(Ljava/util/BitSet;Z)V

    .line 156
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 157
    invoke-static {}, Lcom/teamspeak/ts3client/data/v;->a()Lcom/teamspeak/ts3client/data/v;

    move-result-object v0

    .line 3080
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/teamspeak/ts3client/data/v;->c:Lcom/teamspeak/ts3client/data/y;

    .line 158
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/s;->c:Lcom/teamspeak/ts3client/f/r;

    iget-object v0, v0, Lcom/teamspeak/ts3client/f/r;->aw:Lcom/teamspeak/ts3client/f/p;

    invoke-static {v0}, Lcom/teamspeak/ts3client/f/p;->d(Lcom/teamspeak/ts3client/f/p;)Lcom/teamspeak/ts3client/f/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/teamspeak/ts3client/f/r;->b()V

    .line 159
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/s;->b:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 160
    return-void
.end method
