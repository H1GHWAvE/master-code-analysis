.class public final Lcom/teamspeak/ts3client/f/p;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/teamspeak/ts3client/f/r;

.field private c:Landroid/support/v4/app/bi;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/support/v4/app/bi;)V
    .registers 15

    .prologue
    const/16 v6, 0x12

    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v4, -0x2

    .line 40
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 41
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/f/p;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 42
    invoke-virtual {p0, v2, v6, v2, v6}, Lcom/teamspeak/ts3client/f/p;->setPadding(IIII)V

    .line 43
    const v0, 0x7f0200b6

    invoke-virtual {p0, v0}, Lcom/teamspeak/ts3client/f/p;->setBackgroundResource(I)V

    .line 44
    iput-object p7, p0, Lcom/teamspeak/ts3client/f/p;->c:Landroid/support/v4/app/bi;

    .line 45
    iput-object p3, p0, Lcom/teamspeak/ts3client/f/p;->a:Ljava/lang/String;

    .line 46
    iput-object p4, p0, Lcom/teamspeak/ts3client/f/p;->e:Ljava/lang/String;

    .line 47
    iput-object p6, p0, Lcom/teamspeak/ts3client/f/p;->f:Ljava/lang/String;

    .line 48
    iput-object p2, p0, Lcom/teamspeak/ts3client/f/p;->d:Ljava/lang/String;

    .line 49
    iput-object p5, p0, Lcom/teamspeak/ts3client/f/p;->g:Ljava/lang/String;

    .line 50
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 51
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 52
    iget-object v2, p0, Lcom/teamspeak/ts3client/f/p;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    const/high16 v2, 0x41a00000    # 20.0f

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 54
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 55
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setId(I)V

    .line 56
    iget-object v2, p0, Lcom/teamspeak/ts3client/f/p;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    const/high16 v2, 0x41200000    # 10.0f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 58
    const/4 v2, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 59
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setId(I)V

    .line 61
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 62
    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 63
    invoke-virtual {p0, v0, v2}, Lcom/teamspeak/ts3client/f/p;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 65
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 66
    const/4 v3, 0x3

    invoke-virtual {v0}, Landroid/widget/TextView;->getId()I

    move-result v0

    invoke-virtual {v2, v3, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 67
    invoke-virtual {p0, v1, v2}, Lcom/teamspeak/ts3client/f/p;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 69
    invoke-virtual {p0, v5}, Lcom/teamspeak/ts3client/f/p;->setClickable(Z)V

    .line 70
    invoke-virtual {p0, p0}, Lcom/teamspeak/ts3client/f/p;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    return-void
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/f/p;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/p;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/f/p;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/p;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/f/p;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/p;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/f/p;)Lcom/teamspeak/ts3client/f/r;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/p;->b:Lcom/teamspeak/ts3client/f/r;

    return-object v0
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/f/p;)Ljava/lang/String;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/p;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 5

    .prologue
    .line 76
    new-instance v0, Lcom/teamspeak/ts3client/f/r;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/teamspeak/ts3client/f/r;-><init>(Lcom/teamspeak/ts3client/f/p;B)V

    iput-object v0, p0, Lcom/teamspeak/ts3client/f/p;->b:Lcom/teamspeak/ts3client/f/r;

    .line 77
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/p;->b:Lcom/teamspeak/ts3client/f/r;

    iget-object v1, p0, Lcom/teamspeak/ts3client/f/p;->c:Landroid/support/v4/app/bi;

    iget-object v2, p0, Lcom/teamspeak/ts3client/f/p;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/teamspeak/ts3client/f/r;->a(Landroid/support/v4/app/bi;Ljava/lang/String;)V

    .line 78
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 79
    return-void
.end method
