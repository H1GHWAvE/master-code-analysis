.class final Lcom/teamspeak/ts3client/f/x;
.super Landroid/support/v4/app/ax;
.source "SourceFile"


# instance fields
.field final synthetic at:Lcom/teamspeak/ts3client/f/v;


# direct methods
.method private constructor <init>(Lcom/teamspeak/ts3client/f/v;)V
    .registers 2

    .prologue
    .line 75
    iput-object p1, p0, Lcom/teamspeak/ts3client/f/x;->at:Lcom/teamspeak/ts3client/f/v;

    invoke-direct {p0}, Landroid/support/v4/app/ax;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/teamspeak/ts3client/f/v;B)V
    .registers 3

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/teamspeak/ts3client/f/x;-><init>(Lcom/teamspeak/ts3client/f/v;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;
    .registers 12

    .prologue
    const/4 v8, 0x0

    const/16 v3, 0x12

    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v6, -0x2

    .line 109
    new-instance v1, Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/f/x;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 110
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v0, v2, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 111
    const/16 v0, 0x8

    invoke-virtual {v1, v0, v3, v5, v3}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 112
    const v0, 0x7f0200b6

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 113
    new-instance v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/f/x;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 114
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/f/x;->h()Landroid/content/Context;

    move-result-object v0

    const-string v3, "layout_inflater"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 115
    const v3, 0x7f030045

    invoke-virtual {v0, v3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 116
    const v0, 0x7f0c018f

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 117
    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    const/high16 v4, 0x41a00000    # 20.0f

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextSize(F)V

    .line 119
    invoke-virtual {v2, v8, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 120
    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setId(I)V

    .line 121
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v4

    .line 3093
    iget-object v4, v4, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 121
    invoke-interface {v4, p1, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 122
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 123
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/teamspeak/ts3client/f/x;->at:Lcom/teamspeak/ts3client/f/v;

    invoke-static {v5}, Lcom/teamspeak/ts3client/f/v;->c(Lcom/teamspeak/ts3client/f/v;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Checkbox"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 124
    new-instance v4, Lcom/teamspeak/ts3client/f/z;

    invoke-direct {v4, p0, p1}, Lcom/teamspeak/ts3client/f/z;-><init>(Lcom/teamspeak/ts3client/f/x;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 132
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v4, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 134
    invoke-virtual {v1, v2, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 135
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 136
    const/16 v4, 0xb

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 137
    invoke-virtual {v1, v3, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 138
    invoke-virtual {v1, v7}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 139
    new-instance v2, Lcom/teamspeak/ts3client/f/aa;

    invoke-direct {v2, p0, v0}, Lcom/teamspeak/ts3client/f/aa;-><init>(Lcom/teamspeak/ts3client/f/x;Landroid/widget/CheckBox;)V

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 147
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 13

    .prologue
    .line 79
    new-instance v3, Landroid/widget/ScrollView;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/f/x;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 80
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/widget/ScrollView;->setScrollbarFadingEnabled(Z)V

    .line 81
    new-instance v4, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/f/x;->i()Landroid/support/v4/app/bb;

    move-result-object v0

    invoke-direct {v4, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 82
    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 83
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/f/x;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 84
    const/16 v1, 0x8

    const/16 v2, 0x12

    const/4 v5, 0x0

    const/16 v6, 0x12

    invoke-virtual {v0, v1, v2, v5, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 85
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/x;->at:Lcom/teamspeak/ts3client/f/v;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/v;->a(Lcom/teamspeak/ts3client/f/v;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 88
    iget-object v0, p0, Lcom/teamspeak/ts3client/f/x;->at:Lcom/teamspeak/ts3client/f/v;

    invoke-static {v0}, Lcom/teamspeak/ts3client/f/v;->b(Lcom/teamspeak/ts3client/f/v;)Ljava/util/TreeMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_47
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_11d

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 89
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1109
    new-instance v6, Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/f/x;->i()Landroid/support/v4/app/bb;

    move-result-object v2

    invoke-direct {v6, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1110
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v7, -0x1

    const/4 v8, -0x2

    invoke-direct {v2, v7, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v6, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1111
    const/16 v2, 0x8

    const/16 v7, 0x12

    const/4 v8, 0x0

    const/16 v9, 0x12

    invoke-virtual {v6, v2, v7, v8, v9}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 1112
    const v2, 0x7f0200b6

    invoke-virtual {v6, v2}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 1113
    new-instance v7, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/f/x;->i()Landroid/support/v4/app/bb;

    move-result-object v2

    invoke-direct {v7, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1114
    invoke-virtual {p0}, Lcom/teamspeak/ts3client/f/x;->h()Landroid/content/Context;

    move-result-object v2

    const-string v8, "layout_inflater"

    invoke-virtual {v2, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 1115
    const v8, 0x7f030045

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    .line 1116
    const v2, 0x7f0c018f

    invoke-virtual {v8, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    .line 1117
    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1118
    const/high16 v0, 0x41a00000    # 20.0f

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setTextSize(F)V

    .line 1119
    const/4 v0, 0x0

    const/4 v9, 0x1

    invoke-virtual {v7, v0, v9}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1120
    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setId(I)V

    .line 1121
    invoke-static {}, Lcom/teamspeak/ts3client/Ts3Application;->a()Lcom/teamspeak/ts3client/Ts3Application;

    move-result-object v0

    .line 2093
    iget-object v0, v0, Lcom/teamspeak/ts3client/Ts3Application;->e:Landroid/content/SharedPreferences;

    .line 1121
    const/4 v9, 0x0

    invoke-interface {v0, v1, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1122
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1123
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/teamspeak/ts3client/f/x;->at:Lcom/teamspeak/ts3client/f/v;

    invoke-static {v9}, Lcom/teamspeak/ts3client/f/v;->c(Lcom/teamspeak/ts3client/f/v;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, " Checkbox"

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1124
    new-instance v0, Lcom/teamspeak/ts3client/f/z;

    invoke-direct {v0, p0, v1}, Lcom/teamspeak/ts3client/f/z;-><init>(Lcom/teamspeak/ts3client/f/x;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1132
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    const/4 v9, -0x2

    invoke-direct {v0, v1, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1134
    invoke-virtual {v6, v7, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1135
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    const/4 v7, -0x2

    invoke-direct {v0, v1, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1136
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1137
    invoke-virtual {v6, v8, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1138
    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 1139
    new-instance v0, Lcom/teamspeak/ts3client/f/aa;

    invoke-direct {v0, p0, v2}, Lcom/teamspeak/ts3client/f/aa;-><init>(Lcom/teamspeak/ts3client/f/x;Landroid/widget/CheckBox;)V

    invoke-virtual {v6, v0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    invoke-virtual {v4, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_47

    .line 2207
    :cond_11d
    iget-object v0, p0, Landroid/support/v4/app/ax;->j:Landroid/app/Dialog;

    .line 92
    iget-object v1, p0, Lcom/teamspeak/ts3client/f/x;->at:Lcom/teamspeak/ts3client/f/v;

    invoke-static {v1}, Lcom/teamspeak/ts3client/f/v;->c(Lcom/teamspeak/ts3client/f/v;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 93
    new-instance v0, Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/teamspeak/ts3client/f/x;->i()Landroid/support/v4/app/bb;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 94
    const-string v1, "button.close"

    invoke-static {v1}, Lcom/teamspeak/ts3client/data/e/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 95
    new-instance v1, Lcom/teamspeak/ts3client/f/y;

    invoke-direct {v1, p0}, Lcom/teamspeak/ts3client/f/y;-><init>(Lcom/teamspeak/ts3client/f/x;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 104
    invoke-virtual {v3, v4}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    .line 105
    return-object v3
.end method
