.class public Lupdate;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lorg/xbill/DNS/Message;

.field b:Lorg/xbill/DNS/Message;

.field c:Lorg/xbill/DNS/Resolver;

.field d:Ljava/lang/String;

.field e:Lorg/xbill/DNS/Name;

.field f:J

.field g:I

.field h:Ljava/io/PrintStream;


# direct methods
.method private constructor <init>(Ljava/io/InputStream;)V
    .registers 14

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x1

    const/16 v9, 0xff

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object v1, p0, Lupdate;->d:Ljava/lang/String;

    .line 15
    sget-object v0, Lorg/xbill/DNS/Name;->root:Lorg/xbill/DNS/Name;

    iput-object v0, p0, Lupdate;->e:Lorg/xbill/DNS/Name;

    .line 17
    iput v8, p0, Lupdate;->g:I

    .line 18
    iput-object v1, p0, Lupdate;->h:Ljava/io/PrintStream;

    .line 36
    new-instance v10, Ljava/util/LinkedList;

    invoke-direct {v10}, Ljava/util/LinkedList;-><init>()V

    .line 37
    new-instance v11, Ljava/util/LinkedList;

    invoke-direct {v11}, Ljava/util/LinkedList;-><init>()V

    .line 39
    invoke-static {}, Lupdate;->a()Lorg/xbill/DNS/Message;

    move-result-object v0

    iput-object v0, p0, Lupdate;->a:Lorg/xbill/DNS/Message;

    .line 41
    new-instance v0, Ljava/io/InputStreamReader;

    invoke-direct {v0, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 42
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 44
    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    invoke-interface {v11, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    :cond_31
    :goto_31
    const/4 v0, 0x0

    :try_start_32
    invoke-interface {v11, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    .line 53
    const/4 v1, 0x0

    invoke-interface {v10, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/BufferedReader;

    .line 55
    sget-object v2, Ljava/lang/System;->in:Ljava/io/InputStream;

    if-ne v0, v2, :cond_4a

    .line 56
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "> "

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 58
    :cond_4a
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 59
    if-nez v0, :cond_62

    .line 60
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 61
    const/4 v1, 0x0

    invoke-interface {v10, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 62
    const/4 v1, 0x0

    invoke-interface {v11, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 63
    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_62

    .line 204
    :goto_61
    return-void

    .line 66
    :cond_62
    if-eqz v0, :cond_31

    .line 68
    iget-object v1, p0, Lupdate;->h:Ljava/io/PrintStream;

    if-eqz v1, :cond_7c

    .line 69
    iget-object v1, p0, Lupdate;->h:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuffer;

    const-string v3, "> "

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 71
    :cond_7c
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_31

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x23

    if-eq v1, v2, :cond_31

    .line 75
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x3e

    if-ne v1, v2, :cond_99

    .line 76
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 78
    :cond_99
    new-instance v6, Lorg/xbill/DNS/Tokenizer;

    invoke-direct {v6, v0}, Lorg/xbill/DNS/Tokenizer;-><init>(Ljava/lang/String;)V

    .line 79
    invoke-virtual {v6}, Lorg/xbill/DNS/Tokenizer;->get()Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v1

    .line 81
    invoke-virtual {v1}, Lorg/xbill/DNS/Tokenizer$Token;->isEOL()Z

    move-result v2

    if-nez v2, :cond_31

    .line 83
    iget-object v1, v1, Lorg/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    .line 85
    const-string v2, "server"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e4

    .line 86
    invoke-virtual {v6}, Lorg/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lupdate;->d:Ljava/lang/String;

    .line 87
    new-instance v0, Lorg/xbill/DNS/SimpleResolver;

    iget-object v1, p0, Lupdate;->d:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/xbill/DNS/SimpleResolver;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lupdate;->c:Lorg/xbill/DNS/Resolver;

    .line 88
    invoke-virtual {v6}, Lorg/xbill/DNS/Tokenizer;->get()Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, Lorg/xbill/DNS/Tokenizer$Token;->isString()Z

    move-result v1

    if-eqz v1, :cond_31

    .line 90
    iget-object v0, v0, Lorg/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    .line 91
    iget-object v1, p0, Lupdate;->c:Lorg/xbill/DNS/Resolver;

    invoke-static {v0}, Ljava/lang/Short;->parseShort(Ljava/lang/String;)S

    move-result v0

    invoke-interface {v1, v0}, Lorg/xbill/DNS/Resolver;->setPort(I)V
    :try_end_d6
    .catch Lorg/xbill/DNS/TextParseException; {:try_start_32 .. :try_end_d6} :catch_d8
    .catch Ljava/io/InterruptedIOException; {:try_start_32 .. :try_end_d6} :catch_10d
    .catch Ljava/net/SocketException; {:try_start_32 .. :try_end_d6} :catch_137
    .catch Ljava/io/IOException; {:try_start_32 .. :try_end_d6} :catch_161

    goto/16 :goto_31

    .line 229
    :catch_d8
    move-exception v0

    .line 230
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0}, Lorg/xbill/DNS/TextParseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_31

    .line 95
    :cond_e4
    :try_start_e4
    const-string v2, "key"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_117

    .line 96
    invoke-virtual {v6}, Lorg/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;

    move-result-object v0

    .line 97
    invoke-virtual {v6}, Lorg/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;

    move-result-object v1

    .line 98
    iget-object v2, p0, Lupdate;->c:Lorg/xbill/DNS/Resolver;

    if-nez v2, :cond_101

    .line 99
    new-instance v2, Lorg/xbill/DNS/SimpleResolver;

    iget-object v3, p0, Lupdate;->d:Ljava/lang/String;

    invoke-direct {v2, v3}, Lorg/xbill/DNS/SimpleResolver;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lupdate;->c:Lorg/xbill/DNS/Resolver;

    .line 100
    :cond_101
    iget-object v2, p0, Lupdate;->c:Lorg/xbill/DNS/Resolver;

    new-instance v3, Lorg/xbill/DNS/TSIG;

    invoke-direct {v3, v0, v1}, Lorg/xbill/DNS/TSIG;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Lorg/xbill/DNS/Resolver;->setTSIGKey(Lorg/xbill/DNS/TSIG;)V
    :try_end_10b
    .catch Lorg/xbill/DNS/TextParseException; {:try_start_e4 .. :try_end_10b} :catch_d8
    .catch Ljava/io/InterruptedIOException; {:try_start_e4 .. :try_end_10b} :catch_10d
    .catch Ljava/net/SocketException; {:try_start_e4 .. :try_end_10b} :catch_137
    .catch Ljava/io/IOException; {:try_start_e4 .. :try_end_10b} :catch_161

    goto/16 :goto_31

    .line 233
    :catch_10d
    move-exception v0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "Operation timed out"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_31

    .line 103
    :cond_117
    :try_start_117
    const-string v2, "edns"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_141

    .line 104
    iget-object v0, p0, Lupdate;->c:Lorg/xbill/DNS/Resolver;

    if-nez v0, :cond_12c

    .line 105
    new-instance v0, Lorg/xbill/DNS/SimpleResolver;

    iget-object v1, p0, Lupdate;->d:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/xbill/DNS/SimpleResolver;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lupdate;->c:Lorg/xbill/DNS/Resolver;

    .line 106
    :cond_12c
    iget-object v0, p0, Lupdate;->c:Lorg/xbill/DNS/Resolver;

    invoke-virtual {v6}, Lorg/xbill/DNS/Tokenizer;->getUInt16()I

    move-result v1

    invoke-interface {v0, v1}, Lorg/xbill/DNS/Resolver;->setEDNS(I)V
    :try_end_135
    .catch Lorg/xbill/DNS/TextParseException; {:try_start_117 .. :try_end_135} :catch_d8
    .catch Ljava/io/InterruptedIOException; {:try_start_117 .. :try_end_135} :catch_10d
    .catch Ljava/net/SocketException; {:try_start_117 .. :try_end_135} :catch_137
    .catch Ljava/io/IOException; {:try_start_117 .. :try_end_135} :catch_161

    goto/16 :goto_31

    .line 236
    :catch_137
    move-exception v0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "Socket error"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_31

    .line 109
    :cond_141
    :try_start_141
    const-string v2, "port"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_169

    .line 110
    iget-object v0, p0, Lupdate;->c:Lorg/xbill/DNS/Resolver;

    if-nez v0, :cond_156

    .line 111
    new-instance v0, Lorg/xbill/DNS/SimpleResolver;

    iget-object v1, p0, Lupdate;->d:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/xbill/DNS/SimpleResolver;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lupdate;->c:Lorg/xbill/DNS/Resolver;

    .line 112
    :cond_156
    iget-object v0, p0, Lupdate;->c:Lorg/xbill/DNS/Resolver;

    invoke-virtual {v6}, Lorg/xbill/DNS/Tokenizer;->getUInt16()I

    move-result v1

    invoke-interface {v0, v1}, Lorg/xbill/DNS/Resolver;->setPort(I)V
    :try_end_15f
    .catch Lorg/xbill/DNS/TextParseException; {:try_start_141 .. :try_end_15f} :catch_d8
    .catch Ljava/io/InterruptedIOException; {:try_start_141 .. :try_end_15f} :catch_10d
    .catch Ljava/net/SocketException; {:try_start_141 .. :try_end_15f} :catch_137
    .catch Ljava/io/IOException; {:try_start_141 .. :try_end_15f} :catch_161

    goto/16 :goto_31

    .line 238
    :catch_161
    move-exception v0

    .line 239
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto/16 :goto_31

    .line 115
    :cond_169
    :try_start_169
    const-string v2, "tcp"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_186

    .line 116
    iget-object v0, p0, Lupdate;->c:Lorg/xbill/DNS/Resolver;

    if-nez v0, :cond_17e

    .line 117
    new-instance v0, Lorg/xbill/DNS/SimpleResolver;

    iget-object v1, p0, Lupdate;->d:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/xbill/DNS/SimpleResolver;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lupdate;->c:Lorg/xbill/DNS/Resolver;

    .line 118
    :cond_17e
    iget-object v0, p0, Lupdate;->c:Lorg/xbill/DNS/Resolver;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lorg/xbill/DNS/Resolver;->setTCP(Z)V

    goto/16 :goto_31

    .line 121
    :cond_186
    const-string v2, "class"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1b0

    .line 122
    invoke-virtual {v6}, Lorg/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;

    move-result-object v0

    .line 123
    invoke-static {v0}, Lorg/xbill/DNS/DClass;->value(Ljava/lang/String;)I

    move-result v1

    .line 124
    if-lez v1, :cond_19c

    .line 125
    iput v1, p0, Lupdate;->g:I

    goto/16 :goto_31

    .line 127
    :cond_19c
    new-instance v1, Ljava/lang/StringBuffer;

    const-string v2, "Invalid class "

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lupdate;->a(Ljava/lang/Object;)V

    goto/16 :goto_31

    .line 130
    :cond_1b0
    const-string v2, "ttl"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1c0

    .line 131
    invoke-virtual {v6}, Lorg/xbill/DNS/Tokenizer;->getTTL()J

    move-result-wide v0

    iput-wide v0, p0, Lupdate;->f:J

    goto/16 :goto_31

    .line 133
    :cond_1c0
    const-string v2, "origin"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1d0

    const-string v2, "zone"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1da

    .line 136
    :cond_1d0
    sget-object v0, Lorg/xbill/DNS/Name;->root:Lorg/xbill/DNS/Name;

    invoke-virtual {v6, v0}, Lorg/xbill/DNS/Tokenizer;->getName(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;

    move-result-object v0

    iput-object v0, p0, Lupdate;->e:Lorg/xbill/DNS/Name;

    goto/16 :goto_31

    .line 139
    :cond_1da
    const-string v2, "require"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_247

    .line 1323
    iget-object v0, p0, Lupdate;->e:Lorg/xbill/DNS/Name;

    invoke-virtual {v6, v0}, Lorg/xbill/DNS/Tokenizer;->getName(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;

    move-result-object v1

    .line 1324
    invoke-virtual {v6}, Lorg/xbill/DNS/Tokenizer;->get()Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v0

    .line 1325
    invoke-virtual {v0}, Lorg/xbill/DNS/Tokenizer$Token;->isString()Z

    move-result v2

    if-eqz v2, :cond_23c

    .line 1326
    iget-object v2, v0, Lorg/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    invoke-static {v2}, Lorg/xbill/DNS/Type;->value(Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_211

    .line 1327
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuffer;

    const-string v3, "Invalid type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lorg/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1328
    :cond_211
    invoke-virtual {v6}, Lorg/xbill/DNS/Tokenizer;->get()Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v0

    .line 1329
    invoke-virtual {v0}, Lorg/xbill/DNS/Tokenizer$Token;->isEOL()Z

    move-result v0

    .line 1330
    invoke-virtual {v6}, Lorg/xbill/DNS/Tokenizer;->unget()V

    .line 1331
    if-nez v0, :cond_233

    .line 1332
    iget v3, p0, Lupdate;->g:I

    const-wide/16 v4, 0x0

    iget-object v7, p0, Lupdate;->e:Lorg/xbill/DNS/Name;

    invoke-static/range {v1 .. v7}, Lorg/xbill/DNS/Record;->fromString(Lorg/xbill/DNS/Name;IIJLorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Record;

    move-result-object v0

    .line 1340
    :goto_228
    iget-object v1, p0, Lupdate;->a:Lorg/xbill/DNS/Message;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lorg/xbill/DNS/Message;->addRecord(Lorg/xbill/DNS/Record;I)V

    .line 1341
    invoke-direct {p0, v0}, Lupdate;->a(Ljava/lang/Object;)V

    goto/16 :goto_31

    .line 1335
    :cond_233
    const/16 v0, 0xff

    const-wide/16 v4, 0x0

    invoke-static {v1, v2, v0, v4, v5}, Lorg/xbill/DNS/Record;->newRecord(Lorg/xbill/DNS/Name;IIJ)Lorg/xbill/DNS/Record;

    move-result-object v0

    goto :goto_228

    .line 1338
    :cond_23c
    const/16 v0, 0xff

    const/16 v2, 0xff

    const-wide/16 v4, 0x0

    invoke-static {v1, v0, v2, v4, v5}, Lorg/xbill/DNS/Record;->newRecord(Lorg/xbill/DNS/Name;IIJ)Lorg/xbill/DNS/Record;

    move-result-object v0

    goto :goto_228

    .line 142
    :cond_247
    const-string v2, "prohibit"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_292

    .line 1351
    iget-object v0, p0, Lupdate;->e:Lorg/xbill/DNS/Name;

    invoke-virtual {v6, v0}, Lorg/xbill/DNS/Tokenizer;->getName(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;

    move-result-object v1

    .line 1352
    invoke-virtual {v6}, Lorg/xbill/DNS/Tokenizer;->get()Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v2

    .line 1353
    invoke-virtual {v2}, Lorg/xbill/DNS/Tokenizer$Token;->isString()Z

    move-result v0

    if-eqz v0, :cond_27e

    .line 1354
    iget-object v0, v2, Lorg/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    invoke-static {v0}, Lorg/xbill/DNS/Type;->value(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_27f

    .line 1355
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuffer;

    const-string v3, "Invalid type: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iget-object v2, v2, Lorg/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_27e
    move v0, v9

    .line 1358
    :cond_27f
    const/16 v2, 0xfe

    const-wide/16 v4, 0x0

    invoke-static {v1, v0, v2, v4, v5}, Lorg/xbill/DNS/Record;->newRecord(Lorg/xbill/DNS/Name;IIJ)Lorg/xbill/DNS/Record;

    move-result-object v0

    .line 1359
    iget-object v1, p0, Lupdate;->a:Lorg/xbill/DNS/Message;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lorg/xbill/DNS/Message;->addRecord(Lorg/xbill/DNS/Record;I)V

    .line 1360
    invoke-direct {p0, v0}, Lupdate;->a(Ljava/lang/Object;)V

    goto/16 :goto_31

    .line 145
    :cond_292
    const-string v2, "add"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2ad

    .line 1365
    iget v0, p0, Lupdate;->g:I

    iget-wide v2, p0, Lupdate;->f:J

    invoke-direct {p0, v6, v0, v2, v3}, Lupdate;->a(Lorg/xbill/DNS/Tokenizer;IJ)Lorg/xbill/DNS/Record;

    move-result-object v0

    .line 1366
    iget-object v1, p0, Lupdate;->a:Lorg/xbill/DNS/Message;

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, Lorg/xbill/DNS/Message;->addRecord(Lorg/xbill/DNS/Record;I)V

    .line 1367
    invoke-direct {p0, v0}, Lupdate;->a(Ljava/lang/Object;)V

    goto/16 :goto_31

    .line 148
    :cond_2ad
    const-string v2, "delete"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_322

    .line 1378
    iget-object v0, p0, Lupdate;->e:Lorg/xbill/DNS/Name;

    invoke-virtual {v6, v0}, Lorg/xbill/DNS/Tokenizer;->getName(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;

    move-result-object v1

    .line 1379
    invoke-virtual {v6}, Lorg/xbill/DNS/Tokenizer;->get()Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v0

    .line 1380
    invoke-virtual {v0}, Lorg/xbill/DNS/Tokenizer$Token;->isString()Z

    move-result v2

    if-eqz v2, :cond_317

    .line 1381
    iget-object v0, v0, Lorg/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    .line 1382
    invoke-static {v0}, Lorg/xbill/DNS/DClass;->value(Ljava/lang/String;)I

    move-result v2

    if-ltz v2, :cond_2d1

    .line 1383
    invoke-virtual {v6}, Lorg/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;

    move-result-object v0

    .line 1385
    :cond_2d1
    invoke-static {v0}, Lorg/xbill/DNS/Type;->value(Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_2ec

    .line 1386
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuffer;

    const-string v3, "Invalid type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1387
    :cond_2ec
    invoke-virtual {v6}, Lorg/xbill/DNS/Tokenizer;->get()Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v0

    .line 1388
    invoke-virtual {v0}, Lorg/xbill/DNS/Tokenizer$Token;->isEOL()Z

    move-result v0

    .line 1389
    invoke-virtual {v6}, Lorg/xbill/DNS/Tokenizer;->unget()V

    .line 1390
    if-nez v0, :cond_30e

    .line 1391
    const/16 v3, 0xfe

    const-wide/16 v4, 0x0

    iget-object v7, p0, Lupdate;->e:Lorg/xbill/DNS/Name;

    invoke-static/range {v1 .. v7}, Lorg/xbill/DNS/Record;->fromString(Lorg/xbill/DNS/Name;IIJLorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Record;

    move-result-object v0

    .line 1399
    :goto_303
    iget-object v1, p0, Lupdate;->a:Lorg/xbill/DNS/Message;

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, Lorg/xbill/DNS/Message;->addRecord(Lorg/xbill/DNS/Record;I)V

    .line 1400
    invoke-direct {p0, v0}, Lupdate;->a(Ljava/lang/Object;)V

    goto/16 :goto_31

    .line 1394
    :cond_30e
    const/16 v0, 0xff

    const-wide/16 v4, 0x0

    invoke-static {v1, v2, v0, v4, v5}, Lorg/xbill/DNS/Record;->newRecord(Lorg/xbill/DNS/Name;IIJ)Lorg/xbill/DNS/Record;

    move-result-object v0

    goto :goto_303

    .line 1397
    :cond_317
    const/16 v0, 0xff

    const/16 v2, 0xff

    const-wide/16 v4, 0x0

    invoke-static {v1, v0, v2, v4, v5}, Lorg/xbill/DNS/Record;->newRecord(Lorg/xbill/DNS/Name;IIJ)Lorg/xbill/DNS/Record;

    move-result-object v0

    goto :goto_303

    .line 151
    :cond_322
    const-string v2, "glue"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_33d

    .line 1405
    iget v0, p0, Lupdate;->g:I

    iget-wide v2, p0, Lupdate;->f:J

    invoke-direct {p0, v6, v0, v2, v3}, Lupdate;->a(Lorg/xbill/DNS/Tokenizer;IJ)Lorg/xbill/DNS/Record;

    move-result-object v0

    .line 1406
    iget-object v1, p0, Lupdate;->a:Lorg/xbill/DNS/Message;

    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, Lorg/xbill/DNS/Message;->addRecord(Lorg/xbill/DNS/Record;I)V

    .line 1407
    invoke-direct {p0, v0}, Lupdate;->a(Ljava/lang/Object;)V

    goto/16 :goto_31

    .line 154
    :cond_33d
    const-string v2, "help"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_34d

    const-string v2, "?"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_364

    .line 157
    :cond_34d
    invoke-virtual {v6}, Lorg/xbill/DNS/Tokenizer;->get()Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v0

    .line 158
    invoke-virtual {v0}, Lorg/xbill/DNS/Tokenizer$Token;->isString()Z

    move-result v1

    if-eqz v1, :cond_35e

    .line 159
    iget-object v0, v0, Lorg/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    invoke-static {v0}, Lupdate;->a(Ljava/lang/String;)V

    goto/16 :goto_31

    .line 161
    :cond_35e
    const/4 v0, 0x0

    invoke-static {v0}, Lupdate;->a(Ljava/lang/String;)V

    goto/16 :goto_31

    .line 164
    :cond_364
    const-string v2, "echo"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_37a

    .line 165
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lupdate;->a(Ljava/lang/Object;)V

    goto/16 :goto_31

    .line 167
    :cond_37a
    const-string v0, "send"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_38d

    .line 168
    invoke-direct {p0}, Lupdate;->b()V

    .line 169
    invoke-static {}, Lupdate;->a()Lorg/xbill/DNS/Message;

    move-result-object v0

    iput-object v0, p0, Lupdate;->a:Lorg/xbill/DNS/Message;

    goto/16 :goto_31

    .line 172
    :cond_38d
    const-string v0, "show"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_39c

    .line 173
    iget-object v0, p0, Lupdate;->a:Lorg/xbill/DNS/Message;

    invoke-direct {p0, v0}, Lupdate;->a(Ljava/lang/Object;)V

    goto/16 :goto_31

    .line 176
    :cond_39c
    const-string v0, "clear"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3ac

    .line 177
    invoke-static {}, Lupdate;->a()Lorg/xbill/DNS/Message;

    move-result-object v0

    iput-object v0, p0, Lupdate;->a:Lorg/xbill/DNS/Message;

    goto/16 :goto_31

    .line 179
    :cond_3ac
    const-string v0, "query"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_415

    .line 1417
    iget v0, p0, Lupdate;->g:I

    .line 1419
    iget-object v1, p0, Lupdate;->e:Lorg/xbill/DNS/Name;

    invoke-virtual {v6, v1}, Lorg/xbill/DNS/Tokenizer;->getName(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;

    move-result-object v2

    .line 1420
    invoke-virtual {v6}, Lorg/xbill/DNS/Tokenizer;->get()Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v1

    .line 1421
    invoke-virtual {v1}, Lorg/xbill/DNS/Tokenizer$Token;->isString()Z

    move-result v3

    if-eqz v3, :cond_3f0

    .line 1422
    iget-object v1, v1, Lorg/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    invoke-static {v1}, Lorg/xbill/DNS/Type;->value(Ljava/lang/String;)I

    move-result v1

    .line 1423
    if-gez v1, :cond_3d6

    .line 1424
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Invalid type"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1425
    :cond_3d6
    invoke-virtual {v6}, Lorg/xbill/DNS/Tokenizer;->get()Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v3

    .line 1426
    invoke-virtual {v3}, Lorg/xbill/DNS/Tokenizer$Token;->isString()Z

    move-result v4

    if-eqz v4, :cond_3f1

    .line 1427
    iget-object v0, v3, Lorg/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    invoke-static {v0}, Lorg/xbill/DNS/DClass;->value(Ljava/lang/String;)I

    move-result v0

    .line 1428
    if-gez v0, :cond_3f1

    .line 1429
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Invalid class"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3f0
    move v1, v8

    .line 1433
    :cond_3f1
    invoke-static {v2, v1, v0}, Lorg/xbill/DNS/Record;->newRecord(Lorg/xbill/DNS/Name;II)Lorg/xbill/DNS/Record;

    move-result-object v0

    .line 1434
    invoke-static {v0}, Lorg/xbill/DNS/Message;->newQuery(Lorg/xbill/DNS/Record;)Lorg/xbill/DNS/Message;

    move-result-object v0

    .line 1435
    iget-object v1, p0, Lupdate;->c:Lorg/xbill/DNS/Resolver;

    if-nez v1, :cond_406

    .line 1436
    new-instance v1, Lorg/xbill/DNS/SimpleResolver;

    iget-object v2, p0, Lupdate;->d:Ljava/lang/String;

    invoke-direct {v1, v2}, Lorg/xbill/DNS/SimpleResolver;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lupdate;->c:Lorg/xbill/DNS/Resolver;

    .line 1437
    :cond_406
    iget-object v1, p0, Lupdate;->c:Lorg/xbill/DNS/Resolver;

    invoke-interface {v1, v0}, Lorg/xbill/DNS/Resolver;->send(Lorg/xbill/DNS/Message;)Lorg/xbill/DNS/Message;

    move-result-object v0

    iput-object v0, p0, Lupdate;->b:Lorg/xbill/DNS/Message;

    .line 1438
    iget-object v0, p0, Lupdate;->b:Lorg/xbill/DNS/Message;

    invoke-direct {p0, v0}, Lupdate;->a(Ljava/lang/Object;)V

    goto/16 :goto_31

    .line 182
    :cond_415
    const-string v0, "quit"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_425

    const-string v0, "q"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_448

    .line 185
    :cond_425
    iget-object v0, p0, Lupdate;->h:Ljava/io/PrintStream;

    if-eqz v0, :cond_42e

    .line 186
    iget-object v0, p0, Lupdate;->h:Ljava/io/PrintStream;

    invoke-virtual {v0}, Ljava/io/PrintStream;->close()V

    .line 187
    :cond_42e
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 188
    :goto_432
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_442

    .line 190
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/BufferedReader;

    .line 191
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    goto :goto_432

    .line 193
    :cond_442
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    goto/16 :goto_31

    .line 196
    :cond_448
    const-string v0, "file"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_491

    .line 1443
    invoke-virtual {v6}, Lorg/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;
    :try_end_453
    .catch Lorg/xbill/DNS/TextParseException; {:try_start_169 .. :try_end_453} :catch_d8
    .catch Ljava/io/InterruptedIOException; {:try_start_169 .. :try_end_453} :catch_10d
    .catch Ljava/net/SocketException; {:try_start_169 .. :try_end_453} :catch_137
    .catch Ljava/io/IOException; {:try_start_169 .. :try_end_453} :catch_161

    move-result-object v1

    .line 1446
    :try_start_454
    const-string v0, "-"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_48b

    .line 1447
    sget-object v0, Ljava/lang/System;->in:Ljava/io/InputStream;

    .line 1450
    :goto_45e
    const/4 v2, 0x0

    invoke-interface {v11, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1451
    const/4 v2, 0x0

    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-direct {v4, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    invoke-interface {v10, v2, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V
    :try_end_470
    .catch Ljava/io/FileNotFoundException; {:try_start_454 .. :try_end_470} :catch_472
    .catch Lorg/xbill/DNS/TextParseException; {:try_start_454 .. :try_end_470} :catch_d8
    .catch Ljava/io/InterruptedIOException; {:try_start_454 .. :try_end_470} :catch_10d
    .catch Ljava/net/SocketException; {:try_start_454 .. :try_end_470} :catch_137
    .catch Ljava/io/IOException; {:try_start_454 .. :try_end_470} :catch_161

    goto/16 :goto_31

    .line 1454
    :catch_472
    move-exception v0

    :try_start_473
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " not found"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lupdate;->a(Ljava/lang/Object;)V
    :try_end_489
    .catch Lorg/xbill/DNS/TextParseException; {:try_start_473 .. :try_end_489} :catch_d8
    .catch Ljava/io/InterruptedIOException; {:try_start_473 .. :try_end_489} :catch_10d
    .catch Ljava/net/SocketException; {:try_start_473 .. :try_end_489} :catch_137
    .catch Ljava/io/IOException; {:try_start_473 .. :try_end_489} :catch_161

    goto/16 :goto_31

    .line 1449
    :cond_48b
    :try_start_48b
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_490
    .catch Ljava/io/FileNotFoundException; {:try_start_48b .. :try_end_490} :catch_472
    .catch Lorg/xbill/DNS/TextParseException; {:try_start_48b .. :try_end_490} :catch_d8
    .catch Ljava/io/InterruptedIOException; {:try_start_48b .. :try_end_490} :catch_10d
    .catch Ljava/net/SocketException; {:try_start_48b .. :try_end_490} :catch_137
    .catch Ljava/io/IOException; {:try_start_48b .. :try_end_490} :catch_161

    goto :goto_45e

    .line 199
    :cond_491
    :try_start_491
    const-string v0, "log"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4c0

    .line 1460
    invoke-virtual {v6}, Lorg/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;
    :try_end_49c
    .catch Lorg/xbill/DNS/TextParseException; {:try_start_491 .. :try_end_49c} :catch_d8
    .catch Ljava/io/InterruptedIOException; {:try_start_491 .. :try_end_49c} :catch_10d
    .catch Ljava/net/SocketException; {:try_start_491 .. :try_end_49c} :catch_137
    .catch Ljava/io/IOException; {:try_start_491 .. :try_end_49c} :catch_161

    move-result-object v0

    .line 1462
    :try_start_49d
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 1463
    new-instance v2, Ljava/io/PrintStream;

    invoke-direct {v2, v1}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v2, p0, Lupdate;->h:Ljava/io/PrintStream;
    :try_end_4a9
    .catch Ljava/lang/Exception; {:try_start_49d .. :try_end_4a9} :catch_4ab
    .catch Lorg/xbill/DNS/TextParseException; {:try_start_49d .. :try_end_4a9} :catch_d8
    .catch Ljava/io/InterruptedIOException; {:try_start_49d .. :try_end_4a9} :catch_10d
    .catch Ljava/net/SocketException; {:try_start_49d .. :try_end_4a9} :catch_137
    .catch Ljava/io/IOException; {:try_start_49d .. :try_end_4a9} :catch_161

    goto/16 :goto_31

    .line 1466
    :catch_4ab
    move-exception v1

    :try_start_4ac
    new-instance v1, Ljava/lang/StringBuffer;

    const-string v2, "Error opening "

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lupdate;->a(Ljava/lang/Object;)V

    goto/16 :goto_31

    .line 202
    :cond_4c0
    const-string v0, "assert"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4d0

    .line 203
    invoke-direct {p0, v6}, Lupdate;->h(Lorg/xbill/DNS/Tokenizer;)Z

    move-result v0

    if-nez v0, :cond_31

    goto/16 :goto_61

    .line 207
    :cond_4d0
    const-string v0, "sleep"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4e4

    .line 208
    invoke-virtual {v6}, Lorg/xbill/DNS/Tokenizer;->getUInt32()J
    :try_end_4db
    .catch Lorg/xbill/DNS/TextParseException; {:try_start_4ac .. :try_end_4db} :catch_d8
    .catch Ljava/io/InterruptedIOException; {:try_start_4ac .. :try_end_4db} :catch_10d
    .catch Ljava/net/SocketException; {:try_start_4ac .. :try_end_4db} :catch_137
    .catch Ljava/io/IOException; {:try_start_4ac .. :try_end_4db} :catch_161

    move-result-wide v0

    .line 210
    :try_start_4dc
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4df
    .catch Ljava/lang/InterruptedException; {:try_start_4dc .. :try_end_4df} :catch_4e1
    .catch Lorg/xbill/DNS/TextParseException; {:try_start_4dc .. :try_end_4df} :catch_d8
    .catch Ljava/io/InterruptedIOException; {:try_start_4dc .. :try_end_4df} :catch_10d
    .catch Ljava/net/SocketException; {:try_start_4dc .. :try_end_4df} :catch_137
    .catch Ljava/io/IOException; {:try_start_4dc .. :try_end_4df} :catch_161

    goto/16 :goto_31

    .line 214
    :catch_4e1
    move-exception v0

    goto/16 :goto_31

    .line 216
    :cond_4e4
    :try_start_4e4
    const-string v0, "date"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_517

    .line 217
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 218
    invoke-virtual {v6}, Lorg/xbill/DNS/Tokenizer;->get()Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v1

    .line 219
    invoke-virtual {v1}, Lorg/xbill/DNS/Tokenizer$Token;->isString()Z

    move-result v2

    if-eqz v2, :cond_512

    iget-object v1, v1, Lorg/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    const-string v2, "-ms"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_512

    .line 221
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lupdate;->a(Ljava/lang/Object;)V

    goto/16 :goto_31

    .line 223
    :cond_512
    invoke-direct {p0, v0}, Lupdate;->a(Ljava/lang/Object;)V

    goto/16 :goto_31

    .line 227
    :cond_517
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v2, "invalid keyword: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lupdate;->a(Ljava/lang/Object;)V
    :try_end_529
    .catch Lorg/xbill/DNS/TextParseException; {:try_start_4e4 .. :try_end_529} :catch_d8
    .catch Ljava/io/InterruptedIOException; {:try_start_4e4 .. :try_end_529} :catch_10d
    .catch Ljava/net/SocketException; {:try_start_4e4 .. :try_end_529} :catch_137
    .catch Ljava/io/IOException; {:try_start_4e4 .. :try_end_529} :catch_161

    goto/16 :goto_31
.end method

.method private static a()Lorg/xbill/DNS/Message;
    .registers 3

    .prologue
    .line 29
    new-instance v0, Lorg/xbill/DNS/Message;

    invoke-direct {v0}, Lorg/xbill/DNS/Message;-><init>()V

    .line 30
    invoke-virtual {v0}, Lorg/xbill/DNS/Message;->getHeader()Lorg/xbill/DNS/Header;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lorg/xbill/DNS/Header;->setOpcode(I)V

    .line 31
    return-object v0
.end method

.method private a(Lorg/xbill/DNS/Tokenizer;IJ)Lorg/xbill/DNS/Record;
    .registers 14

    .prologue
    .line 286
    iget-object v0, p0, Lupdate;->e:Lorg/xbill/DNS/Name;

    invoke-virtual {p1, v0}, Lorg/xbill/DNS/Tokenizer;->getName(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;

    move-result-object v1

    .line 291
    invoke-virtual {p1}, Lorg/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;

    move-result-object v0

    .line 294
    :try_start_a
    invoke-static {v0}, Lorg/xbill/DNS/TTL;->parseTTL(Ljava/lang/String;)J

    move-result-wide v4

    .line 295
    invoke-virtual {p1}, Lorg/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;
    :try_end_11
    .catch Ljava/lang/NumberFormatException; {:try_start_a .. :try_end_11} :catch_3b

    move-result-object v0

    .line 301
    :goto_12
    invoke-static {v0}, Lorg/xbill/DNS/DClass;->value(Ljava/lang/String;)I

    move-result v2

    if-ltz v2, :cond_50

    .line 302
    invoke-static {v0}, Lorg/xbill/DNS/DClass;->value(Ljava/lang/String;)I

    move-result v3

    .line 303
    invoke-virtual {p1}, Lorg/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;

    move-result-object v0

    .line 306
    :goto_20
    invoke-static {v0}, Lorg/xbill/DNS/Type;->value(Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_3e

    .line 307
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuffer;

    const-string v3, "Invalid type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 298
    :catch_3b
    move-exception v2

    move-wide v4, p3

    goto :goto_12

    .line 309
    :cond_3e
    iget-object v7, p0, Lupdate;->e:Lorg/xbill/DNS/Name;

    move-object v6, p1

    invoke-static/range {v1 .. v7}, Lorg/xbill/DNS/Record;->fromString(Lorg/xbill/DNS/Name;IIJLorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Record;

    move-result-object v0

    .line 310
    if-eqz v0, :cond_48

    .line 311
    return-object v0

    .line 313
    :cond_48
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Parse error"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_50
    move v3, p2

    goto :goto_20
.end method

.method private a(Ljava/lang/Object;)V
    .registers 3

    .prologue
    .line 22
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 23
    iget-object v0, p0, Lupdate;->h:Ljava/io/PrintStream;

    if-eqz v0, :cond_e

    .line 24
    iget-object v0, p0, Lupdate;->h:Ljava/io/PrintStream;

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 25
    :cond_e
    return-void
.end method

.method private static a(Ljava/lang/String;)V
    .registers 5

    .prologue
    .line 540
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0}, Ljava/io/PrintStream;->println()V

    .line 541
    if-nez p0, :cond_f

    .line 542
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "The following are supported commands:\nadd      assert   class    clear    date     delete\necho     edns     file     glue     help     key\nlog      port     prohibit query    quit     require\nsend     server   show     sleep    tcp      ttl\nzone     #\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 673
    :goto_e
    return-void

    .line 550
    :cond_f
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 552
    const-string v1, "add"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_23

    .line 553
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "add <name> [ttl] [class] <type> <data>\n\nspecify a record to be added\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_e

    .line 556
    :cond_23
    const-string v1, "assert"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_33

    .line 557
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "assert <field> <value> [msg]\n\nasserts that the value of the field in the last\nresponse matches the value specified.  If not,\nthe message is printed (if present) and the\nprogram exits.  The field may be any of <rcode>,\n<serial>, <tsig>, <qu>, <an>, <au>, or <ad>.\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_e

    .line 564
    :cond_33
    const-string v1, "class"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_43

    .line 565
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "class <class>\n\nclass of the zone to be updated (default: IN)\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_e

    .line 568
    :cond_43
    const-string v1, "clear"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_53

    .line 569
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "clear\n\nclears the current update packet\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_e

    .line 572
    :cond_53
    const-string v1, "date"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_63

    .line 573
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "date [-ms]\n\nprints the current date and time in human readable\nformat or as the number of milliseconds since the\nepoch"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_e

    .line 578
    :cond_63
    const-string v1, "delete"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_73

    .line 579
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "delete <name> [ttl] [class] <type> <data> \ndelete <name> <type> \ndelete <name>\n\nspecify a record or set to be deleted, or that\nall records at a name should be deleted\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_e

    .line 585
    :cond_73
    const-string v1, "echo"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_83

    .line 586
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "echo <text>\n\nprints the text\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_e

    .line 589
    :cond_83
    const-string v1, "edns"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_94

    .line 590
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "edns <level>\n\nEDNS level specified when sending messages\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_e

    .line 593
    :cond_94
    const-string v1, "file"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a5

    .line 594
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "file <file>\n\nopens the specified file as the new input source\n(- represents stdin)\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_e

    .line 598
    :cond_a5
    const-string v1, "glue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b6

    .line 599
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "glue <name> [ttl] [class] <type> <data>\n\nspecify an additional record\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_e

    .line 602
    :cond_b6
    const-string v1, "help"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c7

    .line 603
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "help\nhelp [topic]\n\nprints a list of commands or help about a specific\ncommand\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_e

    .line 608
    :cond_c7
    const-string v1, "key"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d8

    .line 609
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "key <name> <data>\n\nTSIG key used to sign messages\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_e

    .line 612
    :cond_d8
    const-string v1, "log"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e9

    .line 613
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "log <file>\n\nopens the specified file and uses it to log output\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_e

    .line 616
    :cond_e9
    const-string v1, "port"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_fa

    .line 617
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "port <port>\n\nUDP/TCP port messages are sent to (default: 53)\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_e

    .line 620
    :cond_fa
    const-string v1, "prohibit"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10b

    .line 621
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "prohibit <name> <type> \nprohibit <name>\n\nrequire that a set or name is not present\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_e

    .line 625
    :cond_10b
    const-string v1, "query"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11c

    .line 626
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "query <name> [type [class]] \n\nissues a query\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_e

    .line 629
    :cond_11c
    const-string v1, "q"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_12c

    const-string v1, "quit"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_135

    .line 630
    :cond_12c
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "quit\n\nquits the program\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_e

    .line 633
    :cond_135
    const-string v1, "require"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_146

    .line 634
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "require <name> [ttl] [class] <type> <data> \nrequire <name> <type> \nrequire <name>\n\nrequire that a record, set, or name is present\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_e

    .line 639
    :cond_146
    const-string v1, "send"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_157

    .line 640
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "send\n\nsends and resets the current update packet\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_e

    .line 643
    :cond_157
    const-string v1, "server"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_168

    .line 644
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "server <name> [port]\n\nserver that receives send updates/queries\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_e

    .line 647
    :cond_168
    const-string v1, "show"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_179

    .line 648
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "show\n\nshows the current update packet\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_e

    .line 651
    :cond_179
    const-string v1, "sleep"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_18a

    .line 652
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "sleep <milliseconds>\n\npause for interval before next command\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_e

    .line 655
    :cond_18a
    const-string v1, "tcp"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19b

    .line 656
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "tcp\n\nTCP should be used to send all messages\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_e

    .line 659
    :cond_19b
    const-string v1, "ttl"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1ac

    .line 660
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "ttl <ttl>\n\ndefault ttl of added records (default: 0)\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_e

    .line 663
    :cond_1ac
    const-string v1, "zone"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1bc

    const-string v1, "origin"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c5

    .line 664
    :cond_1bc
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "zone <zone>\n\nzone to update (default: .\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_e

    .line 667
    :cond_1c5
    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1d6

    .line 668
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "# <text>\n\na comment\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_e

    .line 672
    :cond_1d6
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuffer;

    const-string v3, "Topic \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, "\' unrecognized\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_e
.end method

.method private a(Lorg/xbill/DNS/Tokenizer;)V
    .registers 10

    .prologue
    const-wide/16 v4, 0x0

    const/16 v3, 0xff

    .line 323
    iget-object v0, p0, Lupdate;->e:Lorg/xbill/DNS/Name;

    invoke-virtual {p1, v0}, Lorg/xbill/DNS/Tokenizer;->getName(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;

    move-result-object v1

    .line 324
    invoke-virtual {p1}, Lorg/xbill/DNS/Tokenizer;->get()Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v0

    .line 325
    invoke-virtual {v0}, Lorg/xbill/DNS/Tokenizer$Token;->isString()Z

    move-result v2

    if-eqz v2, :cond_58

    .line 326
    iget-object v2, v0, Lorg/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    invoke-static {v2}, Lorg/xbill/DNS/Type;->value(Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_33

    .line 327
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuffer;

    const-string v3, "Invalid type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lorg/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 328
    :cond_33
    invoke-virtual {p1}, Lorg/xbill/DNS/Tokenizer;->get()Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v0

    .line 329
    invoke-virtual {v0}, Lorg/xbill/DNS/Tokenizer$Token;->isEOL()Z

    move-result v0

    .line 330
    invoke-virtual {p1}, Lorg/xbill/DNS/Tokenizer;->unget()V

    .line 331
    if-nez v0, :cond_53

    .line 332
    iget v3, p0, Lupdate;->g:I

    iget-object v7, p0, Lupdate;->e:Lorg/xbill/DNS/Name;

    move-object v6, p1

    invoke-static/range {v1 .. v7}, Lorg/xbill/DNS/Record;->fromString(Lorg/xbill/DNS/Name;IIJLorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Record;

    move-result-object v0

    .line 340
    :goto_49
    iget-object v1, p0, Lupdate;->a:Lorg/xbill/DNS/Message;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lorg/xbill/DNS/Message;->addRecord(Lorg/xbill/DNS/Record;I)V

    .line 341
    invoke-direct {p0, v0}, Lupdate;->a(Ljava/lang/Object;)V

    .line 342
    return-void

    .line 335
    :cond_53
    invoke-static {v1, v2, v3, v4, v5}, Lorg/xbill/DNS/Record;->newRecord(Lorg/xbill/DNS/Name;IIJ)Lorg/xbill/DNS/Record;

    move-result-object v0

    goto :goto_49

    .line 338
    :cond_58
    invoke-static {v1, v3, v3, v4, v5}, Lorg/xbill/DNS/Record;->newRecord(Lorg/xbill/DNS/Name;IIJ)Lorg/xbill/DNS/Record;

    move-result-object v0

    goto :goto_49
.end method

.method private a(Lorg/xbill/DNS/Tokenizer;Ljava/util/List;Ljava/util/List;)V
    .registers 9

    .prologue
    .line 443
    invoke-virtual {p1}, Lorg/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;

    move-result-object v1

    .line 446
    :try_start_4
    const-string v0, "-"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 447
    sget-object v0, Ljava/lang/System;->in:Ljava/io/InputStream;

    .line 450
    :goto_e
    const/4 v2, 0x0

    invoke-interface {p3, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 451
    const/4 v2, 0x0

    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-direct {v4, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    invoke-interface {p2, v2, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 456
    :goto_20
    return-void

    .line 449
    :cond_21
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_26
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_26} :catch_27

    goto :goto_e

    .line 454
    :catch_27
    move-exception v0

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " not found"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lupdate;->a(Ljava/lang/Object;)V

    goto :goto_20
.end method

.method private b()V
    .registers 8

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    .line 246
    iget-object v0, p0, Lupdate;->a:Lorg/xbill/DNS/Message;

    invoke-virtual {v0}, Lorg/xbill/DNS/Message;->getHeader()Lorg/xbill/DNS/Header;

    move-result-object v0

    invoke-virtual {v0, v4}, Lorg/xbill/DNS/Header;->getCount(I)I

    move-result v0

    if-nez v0, :cond_14

    .line 247
    const-string v0, "Empty update message.  Ignoring."

    invoke-direct {p0, v0}, Lupdate;->a(Ljava/lang/Object;)V

    .line 276
    :goto_13
    return-void

    .line 250
    :cond_14
    iget-object v0, p0, Lupdate;->a:Lorg/xbill/DNS/Message;

    invoke-virtual {v0}, Lorg/xbill/DNS/Message;->getHeader()Lorg/xbill/DNS/Header;

    move-result-object v0

    invoke-virtual {v0, v1}, Lorg/xbill/DNS/Header;->getCount(I)I

    move-result v0

    if-nez v0, :cond_63

    .line 252
    iget-object v0, p0, Lupdate;->e:Lorg/xbill/DNS/Name;

    .line 253
    iget v3, p0, Lupdate;->g:I

    .line 254
    if-nez v0, :cond_85

    .line 255
    iget-object v2, p0, Lupdate;->a:Lorg/xbill/DNS/Message;

    invoke-virtual {v2, v4}, Lorg/xbill/DNS/Message;->getSectionArray(I)[Lorg/xbill/DNS/Record;

    move-result-object v4

    move-object v2, v0

    move v0, v1

    .line 256
    :goto_2e
    array-length v5, v4

    if-ge v0, v5, :cond_83

    .line 257
    if-nez v2, :cond_3f

    .line 258
    new-instance v2, Lorg/xbill/DNS/Name;

    aget-object v5, v4, v0

    invoke-virtual {v5}, Lorg/xbill/DNS/Record;->getName()Lorg/xbill/DNS/Name;

    move-result-object v5

    const/4 v6, 0x1

    invoke-direct {v2, v5, v6}, Lorg/xbill/DNS/Name;-><init>(Lorg/xbill/DNS/Name;I)V

    .line 260
    :cond_3f
    aget-object v5, v4, v0

    invoke-virtual {v5}, Lorg/xbill/DNS/Record;->getDClass()I

    move-result v5

    const/16 v6, 0xfe

    if-eq v5, v6, :cond_80

    aget-object v5, v4, v0

    invoke-virtual {v5}, Lorg/xbill/DNS/Record;->getDClass()I

    move-result v5

    const/16 v6, 0xff

    if-eq v5, v6, :cond_80

    .line 263
    aget-object v0, v4, v0

    invoke-virtual {v0}, Lorg/xbill/DNS/Record;->getDClass()I

    move-result v0

    .line 268
    :goto_59
    const/4 v3, 0x6

    invoke-static {v2, v3, v0}, Lorg/xbill/DNS/Record;->newRecord(Lorg/xbill/DNS/Name;II)Lorg/xbill/DNS/Record;

    move-result-object v0

    .line 269
    iget-object v2, p0, Lupdate;->a:Lorg/xbill/DNS/Message;

    invoke-virtual {v2, v0, v1}, Lorg/xbill/DNS/Message;->addRecord(Lorg/xbill/DNS/Record;I)V

    .line 272
    :cond_63
    iget-object v0, p0, Lupdate;->c:Lorg/xbill/DNS/Resolver;

    if-nez v0, :cond_70

    .line 273
    new-instance v0, Lorg/xbill/DNS/SimpleResolver;

    iget-object v1, p0, Lupdate;->d:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/xbill/DNS/SimpleResolver;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lupdate;->c:Lorg/xbill/DNS/Resolver;

    .line 274
    :cond_70
    iget-object v0, p0, Lupdate;->c:Lorg/xbill/DNS/Resolver;

    iget-object v1, p0, Lupdate;->a:Lorg/xbill/DNS/Message;

    invoke-interface {v0, v1}, Lorg/xbill/DNS/Resolver;->send(Lorg/xbill/DNS/Message;)Lorg/xbill/DNS/Message;

    move-result-object v0

    iput-object v0, p0, Lupdate;->b:Lorg/xbill/DNS/Message;

    .line 275
    iget-object v0, p0, Lupdate;->b:Lorg/xbill/DNS/Message;

    invoke-direct {p0, v0}, Lupdate;->a(Ljava/lang/Object;)V

    goto :goto_13

    .line 256
    :cond_80
    add-int/lit8 v0, v0, 0x1

    goto :goto_2e

    :cond_83
    move v0, v3

    goto :goto_59

    :cond_85
    move-object v2, v0

    move v0, v3

    goto :goto_59
.end method

.method private b(Lorg/xbill/DNS/Tokenizer;)V
    .registers 8

    .prologue
    .line 351
    iget-object v0, p0, Lupdate;->e:Lorg/xbill/DNS/Name;

    invoke-virtual {p1, v0}, Lorg/xbill/DNS/Tokenizer;->getName(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;

    move-result-object v1

    .line 352
    invoke-virtual {p1}, Lorg/xbill/DNS/Tokenizer;->get()Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v2

    .line 353
    invoke-virtual {v2}, Lorg/xbill/DNS/Tokenizer$Token;->isString()Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 354
    iget-object v0, v2, Lorg/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    invoke-static {v0}, Lorg/xbill/DNS/Type;->value(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_31

    .line 355
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuffer;

    const-string v3, "Invalid type: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iget-object v2, v2, Lorg/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 357
    :cond_2f
    const/16 v0, 0xff

    .line 358
    :cond_31
    const/16 v2, 0xfe

    const-wide/16 v4, 0x0

    invoke-static {v1, v0, v2, v4, v5}, Lorg/xbill/DNS/Record;->newRecord(Lorg/xbill/DNS/Name;IIJ)Lorg/xbill/DNS/Record;

    move-result-object v0

    .line 359
    iget-object v1, p0, Lupdate;->a:Lorg/xbill/DNS/Message;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lorg/xbill/DNS/Message;->addRecord(Lorg/xbill/DNS/Record;I)V

    .line 360
    invoke-direct {p0, v0}, Lupdate;->a(Ljava/lang/Object;)V

    .line 361
    return-void
.end method

.method private c(Lorg/xbill/DNS/Tokenizer;)V
    .registers 6

    .prologue
    .line 365
    iget v0, p0, Lupdate;->g:I

    iget-wide v2, p0, Lupdate;->f:J

    invoke-direct {p0, p1, v0, v2, v3}, Lupdate;->a(Lorg/xbill/DNS/Tokenizer;IJ)Lorg/xbill/DNS/Record;

    move-result-object v0

    .line 366
    iget-object v1, p0, Lupdate;->a:Lorg/xbill/DNS/Message;

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, Lorg/xbill/DNS/Message;->addRecord(Lorg/xbill/DNS/Record;I)V

    .line 367
    invoke-direct {p0, v0}, Lupdate;->a(Ljava/lang/Object;)V

    .line 368
    return-void
.end method

.method private d(Lorg/xbill/DNS/Tokenizer;)V
    .registers 10

    .prologue
    const-wide/16 v4, 0x0

    const/16 v3, 0xff

    .line 378
    iget-object v0, p0, Lupdate;->e:Lorg/xbill/DNS/Name;

    invoke-virtual {p1, v0}, Lorg/xbill/DNS/Tokenizer;->getName(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;

    move-result-object v1

    .line 379
    invoke-virtual {p1}, Lorg/xbill/DNS/Tokenizer;->get()Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v0

    .line 380
    invoke-virtual {v0}, Lorg/xbill/DNS/Tokenizer$Token;->isString()Z

    move-result v2

    if-eqz v2, :cond_60

    .line 381
    iget-object v0, v0, Lorg/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    .line 382
    invoke-static {v0}, Lorg/xbill/DNS/DClass;->value(Ljava/lang/String;)I

    move-result v2

    if-ltz v2, :cond_20

    .line 383
    invoke-virtual {p1}, Lorg/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;

    move-result-object v0

    .line 385
    :cond_20
    invoke-static {v0}, Lorg/xbill/DNS/Type;->value(Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_3b

    .line 386
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuffer;

    const-string v3, "Invalid type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 387
    :cond_3b
    invoke-virtual {p1}, Lorg/xbill/DNS/Tokenizer;->get()Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v0

    .line 388
    invoke-virtual {v0}, Lorg/xbill/DNS/Tokenizer$Token;->isEOL()Z

    move-result v0

    .line 389
    invoke-virtual {p1}, Lorg/xbill/DNS/Tokenizer;->unget()V

    .line 390
    if-nez v0, :cond_5b

    .line 391
    const/16 v3, 0xfe

    iget-object v7, p0, Lupdate;->e:Lorg/xbill/DNS/Name;

    move-object v6, p1

    invoke-static/range {v1 .. v7}, Lorg/xbill/DNS/Record;->fromString(Lorg/xbill/DNS/Name;IIJLorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Record;

    move-result-object v0

    .line 399
    :goto_51
    iget-object v1, p0, Lupdate;->a:Lorg/xbill/DNS/Message;

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, Lorg/xbill/DNS/Message;->addRecord(Lorg/xbill/DNS/Record;I)V

    .line 400
    invoke-direct {p0, v0}, Lupdate;->a(Ljava/lang/Object;)V

    .line 401
    return-void

    .line 394
    :cond_5b
    invoke-static {v1, v2, v3, v4, v5}, Lorg/xbill/DNS/Record;->newRecord(Lorg/xbill/DNS/Name;IIJ)Lorg/xbill/DNS/Record;

    move-result-object v0

    goto :goto_51

    .line 397
    :cond_60
    invoke-static {v1, v3, v3, v4, v5}, Lorg/xbill/DNS/Record;->newRecord(Lorg/xbill/DNS/Name;IIJ)Lorg/xbill/DNS/Record;

    move-result-object v0

    goto :goto_51
.end method

.method private e(Lorg/xbill/DNS/Tokenizer;)V
    .registers 6

    .prologue
    .line 405
    iget v0, p0, Lupdate;->g:I

    iget-wide v2, p0, Lupdate;->f:J

    invoke-direct {p0, p1, v0, v2, v3}, Lupdate;->a(Lorg/xbill/DNS/Tokenizer;IJ)Lorg/xbill/DNS/Record;

    move-result-object v0

    .line 406
    iget-object v1, p0, Lupdate;->a:Lorg/xbill/DNS/Message;

    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, Lorg/xbill/DNS/Message;->addRecord(Lorg/xbill/DNS/Record;I)V

    .line 407
    invoke-direct {p0, v0}, Lupdate;->a(Ljava/lang/Object;)V

    .line 408
    return-void
.end method

.method private f(Lorg/xbill/DNS/Tokenizer;)V
    .registers 7

    .prologue
    .line 416
    const/4 v1, 0x1

    .line 417
    iget v0, p0, Lupdate;->g:I

    .line 419
    iget-object v2, p0, Lupdate;->e:Lorg/xbill/DNS/Name;

    invoke-virtual {p1, v2}, Lorg/xbill/DNS/Tokenizer;->getName(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;

    move-result-object v2

    .line 420
    invoke-virtual {p1}, Lorg/xbill/DNS/Tokenizer;->get()Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v3

    .line 421
    invoke-virtual {v3}, Lorg/xbill/DNS/Tokenizer$Token;->isString()Z

    move-result v4

    if-eqz v4, :cond_3d

    .line 422
    iget-object v1, v3, Lorg/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    invoke-static {v1}, Lorg/xbill/DNS/Type;->value(Ljava/lang/String;)I

    move-result v1

    .line 423
    if-gez v1, :cond_23

    .line 424
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Invalid type"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 425
    :cond_23
    invoke-virtual {p1}, Lorg/xbill/DNS/Tokenizer;->get()Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v3

    .line 426
    invoke-virtual {v3}, Lorg/xbill/DNS/Tokenizer$Token;->isString()Z

    move-result v4

    if-eqz v4, :cond_3d

    .line 427
    iget-object v0, v3, Lorg/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    invoke-static {v0}, Lorg/xbill/DNS/DClass;->value(Ljava/lang/String;)I

    move-result v0

    .line 428
    if-gez v0, :cond_3d

    .line 429
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Invalid class"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 433
    :cond_3d
    invoke-static {v2, v1, v0}, Lorg/xbill/DNS/Record;->newRecord(Lorg/xbill/DNS/Name;II)Lorg/xbill/DNS/Record;

    move-result-object v0

    .line 434
    invoke-static {v0}, Lorg/xbill/DNS/Message;->newQuery(Lorg/xbill/DNS/Record;)Lorg/xbill/DNS/Message;

    move-result-object v0

    .line 435
    iget-object v1, p0, Lupdate;->c:Lorg/xbill/DNS/Resolver;

    if-nez v1, :cond_52

    .line 436
    new-instance v1, Lorg/xbill/DNS/SimpleResolver;

    iget-object v2, p0, Lupdate;->d:Ljava/lang/String;

    invoke-direct {v1, v2}, Lorg/xbill/DNS/SimpleResolver;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lupdate;->c:Lorg/xbill/DNS/Resolver;

    .line 437
    :cond_52
    iget-object v1, p0, Lupdate;->c:Lorg/xbill/DNS/Resolver;

    invoke-interface {v1, v0}, Lorg/xbill/DNS/Resolver;->send(Lorg/xbill/DNS/Message;)Lorg/xbill/DNS/Message;

    move-result-object v0

    iput-object v0, p0, Lupdate;->b:Lorg/xbill/DNS/Message;

    .line 438
    iget-object v0, p0, Lupdate;->b:Lorg/xbill/DNS/Message;

    invoke-direct {p0, v0}, Lupdate;->a(Ljava/lang/Object;)V

    .line 439
    return-void
.end method

.method private g(Lorg/xbill/DNS/Tokenizer;)V
    .registers 5

    .prologue
    .line 460
    invoke-virtual {p1}, Lorg/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;

    move-result-object v0

    .line 462
    :try_start_4
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 463
    new-instance v2, Ljava/io/PrintStream;

    invoke-direct {v2, v1}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v2, p0, Lupdate;->h:Ljava/io/PrintStream;
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_10} :catch_11

    .line 468
    :goto_10
    return-void

    .line 466
    :catch_11
    move-exception v1

    new-instance v1, Ljava/lang/StringBuffer;

    const-string v2, "Error opening "

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lupdate;->a(Ljava/lang/Object;)V

    goto :goto_10
.end method

.method private h(Lorg/xbill/DNS/Tokenizer;)Z
    .registers 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 472
    invoke-virtual {p1}, Lorg/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;

    move-result-object v4

    .line 473
    invoke-virtual {p1}, Lorg/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;

    move-result-object v5

    .line 474
    const/4 v3, 0x0

    .line 478
    iget-object v0, p0, Lupdate;->b:Lorg/xbill/DNS/Message;

    if-nez v0, :cond_15

    .line 479
    const-string v0, "No response has been received"

    invoke-direct {p0, v0}, Lupdate;->a(Ljava/lang/Object;)V

    .line 535
    :cond_14
    :goto_14
    return v2

    .line 482
    :cond_15
    const-string v0, "rcode"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6e

    .line 483
    iget-object v0, p0, Lupdate;->b:Lorg/xbill/DNS/Message;

    invoke-virtual {v0}, Lorg/xbill/DNS/Message;->getHeader()Lorg/xbill/DNS/Header;

    move-result-object v0

    invoke-virtual {v0}, Lorg/xbill/DNS/Header;->getRcode()I

    move-result v0

    .line 484
    invoke-static {v5}, Lorg/xbill/DNS/Rcode;->value(Ljava/lang/String;)I

    move-result v6

    if-eq v0, v6, :cond_10d

    .line 485
    invoke-static {v0}, Lorg/xbill/DNS/Rcode;->string(I)Ljava/lang/String;

    move-result-object v0

    move v10, v1

    move-object v1, v0

    move v0, v10

    :goto_34
    move v2, v0

    move-object v3, v1

    .line 524
    :goto_36
    if-nez v2, :cond_14

    .line 525
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "Expected "

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ", received "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lupdate;->a(Ljava/lang/Object;)V

    .line 528
    :goto_5e
    invoke-virtual {p1}, Lorg/xbill/DNS/Tokenizer;->get()Lorg/xbill/DNS/Tokenizer$Token;

    move-result-object v0

    .line 529
    invoke-virtual {v0}, Lorg/xbill/DNS/Tokenizer$Token;->isString()Z

    move-result v1

    if-eqz v1, :cond_101

    .line 531
    iget-object v0, v0, Lorg/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    invoke-direct {p0, v0}, Lupdate;->a(Ljava/lang/Object;)V

    goto :goto_5e

    .line 489
    :cond_6e
    const-string v0, "serial"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a1

    .line 490
    iget-object v0, p0, Lupdate;->b:Lorg/xbill/DNS/Message;

    invoke-virtual {v0, v2}, Lorg/xbill/DNS/Message;->getSectionArray(I)[Lorg/xbill/DNS/Record;

    move-result-object v0

    .line 491
    array-length v6, v0

    if-lez v6, :cond_85

    aget-object v6, v0, v1

    instance-of v6, v6, Lorg/xbill/DNS/SOARecord;

    if-nez v6, :cond_8b

    .line 492
    :cond_85
    const-string v0, "Invalid response (no SOA)"

    invoke-direct {p0, v0}, Lupdate;->a(Ljava/lang/Object;)V

    goto :goto_36

    .line 494
    :cond_8b
    aget-object v0, v0, v1

    check-cast v0, Lorg/xbill/DNS/SOARecord;

    .line 495
    invoke-virtual {v0}, Lorg/xbill/DNS/SOARecord;->getSerial()J

    move-result-wide v6

    .line 496
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    cmp-long v0, v6, v8

    if-eqz v0, :cond_10b

    .line 497
    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    :goto_9f
    move v2, v1

    .line 501
    goto :goto_36

    .line 502
    :cond_a1
    const-string v0, "tsig"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_cb

    .line 503
    iget-object v0, p0, Lupdate;->b:Lorg/xbill/DNS/Message;

    invoke-virtual {v0}, Lorg/xbill/DNS/Message;->isSigned()Z

    move-result v0

    if-eqz v0, :cond_c8

    .line 504
    iget-object v0, p0, Lupdate;->b:Lorg/xbill/DNS/Message;

    invoke-virtual {v0}, Lorg/xbill/DNS/Message;->isVerified()Z

    move-result v0

    if-eqz v0, :cond_c5

    .line 505
    const-string v0, "ok"

    .line 511
    :goto_bb
    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_108

    move v2, v1

    move-object v3, v0

    .line 512
    goto/16 :goto_36

    .line 507
    :cond_c5
    const-string v0, "failed"

    goto :goto_bb

    .line 510
    :cond_c8
    const-string v0, "unsigned"

    goto :goto_bb

    .line 514
    :cond_cb
    invoke-static {v4}, Lorg/xbill/DNS/Section;->value(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_ed

    .line 515
    iget-object v6, p0, Lupdate;->b:Lorg/xbill/DNS/Message;

    invoke-virtual {v6}, Lorg/xbill/DNS/Message;->getHeader()Lorg/xbill/DNS/Header;

    move-result-object v6

    invoke-virtual {v6, v0}, Lorg/xbill/DNS/Header;->getCount(I)I

    move-result v0

    .line 516
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    if-eq v0, v6, :cond_106

    .line 517
    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_ea
    move v2, v1

    .line 520
    goto/16 :goto_36

    .line 522
    :cond_ed
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "Invalid assertion keyword: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lupdate;->a(Ljava/lang/Object;)V

    goto/16 :goto_36

    .line 533
    :cond_101
    invoke-virtual {p1}, Lorg/xbill/DNS/Tokenizer;->unget()V

    goto/16 :goto_14

    :cond_106
    move v1, v2

    goto :goto_ea

    :cond_108
    move-object v3, v0

    goto/16 :goto_36

    :cond_10b
    move v1, v2

    goto :goto_9f

    :cond_10d
    move v0, v2

    move-object v1, v3

    goto/16 :goto_34
.end method

.method public static main([Ljava/lang/String;)V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 678
    const/4 v1, 0x0

    .line 679
    array-length v0, p0

    if-lez v0, :cond_34

    .line 681
    :try_start_5
    new-instance v0, Ljava/io/FileInputStream;

    const/4 v2, 0x0

    aget-object v2, p0, v2

    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_d
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_d} :catch_13

    .line 690
    :goto_d
    new-instance v1, Lupdate;

    invoke-direct {v1, v0}, Lupdate;-><init>(Ljava/io/InputStream;)V

    .line 691
    return-void

    .line 684
    :catch_13
    move-exception v0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    aget-object v3, p0, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " not found."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 685
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    move-object v0, v1

    .line 686
    goto :goto_d

    .line 689
    :cond_34
    sget-object v0, Ljava/lang/System;->in:Ljava/io/InputStream;

    goto :goto_d
.end method
