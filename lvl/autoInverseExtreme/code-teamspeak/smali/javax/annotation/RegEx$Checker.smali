.class public final Ljavax/annotation/RegEx$Checker;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljavax/annotation/meta/TypeQualifierValidator;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static forConstantValue$78dac8c8(Ljava/lang/Object;)Ljavax/annotation/meta/When;
    .registers 2

    .prologue
    .line 28
    instance-of v0, p0, Ljava/lang/String;

    if-nez v0, :cond_7

    .line 29
    sget-object v0, Ljavax/annotation/meta/When;->NEVER:Ljavax/annotation/meta/When;

    .line 36
    :goto_6
    return-object v0

    .line 32
    :cond_7
    :try_start_7
    check-cast p0, Ljava/lang/String;

    invoke-static {p0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
    :try_end_c
    .catch Ljava/util/regex/PatternSyntaxException; {:try_start_7 .. :try_end_c} :catch_f

    .line 36
    sget-object v0, Ljavax/annotation/meta/When;->ALWAYS:Ljavax/annotation/meta/When;

    goto :goto_6

    .line 34
    :catch_f
    move-exception v0

    sget-object v0, Ljavax/annotation/meta/When;->NEVER:Ljavax/annotation/meta/When;

    goto :goto_6
.end method


# virtual methods
.method public final synthetic forConstantValue(Ljava/lang/annotation/Annotation;Ljava/lang/Object;)Ljavax/annotation/meta/When;
    .registers 4

    .prologue
    .line 25
    invoke-static {p2}, Ljavax/annotation/RegEx$Checker;->forConstantValue$78dac8c8(Ljava/lang/Object;)Ljavax/annotation/meta/When;

    move-result-object v0

    return-object v0
.end method
