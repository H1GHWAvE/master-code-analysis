package javax.annotation;
public final class Nonnull$Checker implements javax.annotation.meta.TypeQualifierValidator {

    public Nonnull$Checker()
    {
        return;
    }

    private static javax.annotation.meta.When forConstantValue$4791c4f5(Object p1)
    {
        javax.annotation.meta.When v0;
        if (p1 != null) {
            v0 = javax.annotation.meta.When.ALWAYS;
        } else {
            v0 = javax.annotation.meta.When.NEVER;
        }
        return v0;
    }

    public final bridge synthetic javax.annotation.meta.When forConstantValue(otation.Annotation p2, Object p3)
    {
        javax.annotation.meta.When v0;
        if (p3 != null) {
            v0 = javax.annotation.meta.When.ALWAYS;
        } else {
            v0 = javax.annotation.meta.When.NEVER;
        }
        return v0;
    }
}
