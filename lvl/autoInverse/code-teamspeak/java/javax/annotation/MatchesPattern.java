package javax.annotation;
public interface annotation MatchesPattern implements java.lang.annotation.Annotation {

    public abstract int flags();

    public abstract String value();
}
