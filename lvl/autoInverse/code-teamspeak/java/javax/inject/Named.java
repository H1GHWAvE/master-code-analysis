package javax.inject;
public interface annotation Named implements java.lang.annotation.Annotation {

    public abstract String value();
}
