package net.hockeyapp.android;
final class o extends android.os.Handler {
    final synthetic net.hockeyapp.android.FeedbackActivity a;

    o(net.hockeyapp.android.FeedbackActivity p1)
    {
        this.a = p1;
        return;
    }

    public final void handleMessage(android.os.Message p6)
    {
        int v0_5;
        net.hockeyapp.android.FeedbackActivity.a(this.a, new net.hockeyapp.android.c.c());
        if ((p6 == null) || (p6.getData() == null)) {
            v0_5 = 0;
        } else {
            int v0_4 = ((net.hockeyapp.android.c.h) p6.getData().getSerializable("parse_feedback_response"));
            if (v0_4 == 0) {
            } else {
                if (!v0_4.a.equalsIgnoreCase("success")) {
                    v0_5 = 0;
                } else {
                    if (v0_4.c == null) {
                        v0_5 = 1;
                    } else {
                        net.hockeyapp.android.e.p.a.a(net.hockeyapp.android.FeedbackActivity.c(this.a), v0_4.c);
                        net.hockeyapp.android.FeedbackActivity.a(this.a, v0_4);
                        net.hockeyapp.android.FeedbackActivity.d(this.a);
                        v0_5 = 1;
                    }
                }
            }
        }
        if (v0_5 == 0) {
            this.a.runOnUiThread(new net.hockeyapp.android.p(this));
        }
        this.a.a(1);
        return;
    }
}
