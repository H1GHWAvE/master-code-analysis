package net.hockeyapp.android.f;
public final class i extends android.widget.LinearLayout {
    public static final int a = 8192;
    public static final int b = 8194;
    public static final int c = 8196;
    public static final int d = 8198;
    public static final int e = 8200;
    public static final int f = 8201;
    public static final int g = 8208;
    public static final int h = 8209;
    public static final int i = 131088;
    public static final int j = 131089;
    public static final int k = 131090;
    public static final int l = 131091;
    public static final int m = 131092;
    public static final int n = 131093;
    public static final int o = 131094;
    public static final int p = 131095;
    private android.widget.LinearLayout q;
    private android.widget.ScrollView r;
    private android.widget.LinearLayout s;
    private android.widget.LinearLayout t;
    private android.widget.LinearLayout u;
    private android.view.ViewGroup v;
    private android.widget.ListView w;

    public i(android.content.Context p11)
    {
        android.widget.LinearLayout v0_47;
        this(p11);
        android.widget.LinearLayout v0_1 = new android.widget.LinearLayout$LayoutParams(-1, -2);
        this.setBackgroundColor(-1);
        this.setLayoutParams(v0_1);
        this.q = new android.widget.LinearLayout(p11);
        this.q.setId(131090);
        android.widget.LinearLayout v0_6 = new android.widget.LinearLayout$LayoutParams(-1, -1);
        v0_6.gravity = 49;
        this.q.setLayoutParams(v0_6);
        this.q.setPadding(0, 0, 0, 0);
        this.q.setOrientation(1);
        this.addView(this.q);
        this.r = new android.widget.ScrollView(p11);
        this.r.setId(131095);
        android.widget.LinearLayout v0_14 = new android.widget.LinearLayout$LayoutParams(-1, -1);
        int v2_7 = ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics()));
        v0_14.gravity = 17;
        this.r.setLayoutParams(v0_14);
        this.r.setPadding(v2_7, 0, v2_7, 0);
        this.q.addView(this.r);
        this.s = new android.widget.LinearLayout(p11);
        this.s.setId(131091);
        android.widget.LinearLayout v0_21 = new android.widget.LinearLayout$LayoutParams(-1, -1);
        int v2_13 = ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics()));
        v0_21.gravity = 3;
        this.s.setLayoutParams(v0_21);
        this.s.setPadding(v2_13, v2_13, v2_13, v2_13);
        this.s.setGravity(48);
        this.s.setOrientation(1);
        this.r.addView(this.s);
        this.t = new android.widget.LinearLayout(p11);
        this.t.setId(131093);
        android.widget.LinearLayout v0_30 = new android.widget.LinearLayout$LayoutParams(-1, -1);
        int v2_20 = ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics()));
        v0_30.gravity = 17;
        this.t.setLayoutParams(v0_30);
        this.t.setPadding(v2_20, v2_20, v2_20, 0);
        this.t.setGravity(48);
        this.t.setOrientation(1);
        this.q.addView(this.t);
        int v2_24 = new android.widget.EditText(p11);
        v2_24.setId(8194);
        android.widget.LinearLayout v0_37 = new android.widget.LinearLayout$LayoutParams(-1, -2);
        android.widget.ListView v3_8 = ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics()));
        v0_37.setMargins(0, (v3_8 / 2), 0, v3_8);
        v2_24.setLayoutParams(v0_37);
        v2_24.setImeOptions(5);
        v2_24.setInputType(16385);
        v2_24.setSingleLine(1);
        v2_24.setTextColor(-7829368);
        v2_24.setTextSize(2, 1097859072);
        v2_24.setTypeface(0, 0);
        v2_24.setHint(net.hockeyapp.android.aj.a(1026));
        v2_24.setHintTextColor(-3355444);
        net.hockeyapp.android.f.i.a(p11, v2_24);
        if (net.hockeyapp.android.t.b() != net.hockeyapp.android.c.i.a) {
            v0_47 = 0;
        } else {
            v0_47 = 8;
        }
        android.widget.LinearLayout v0_61;
        v2_24.setVisibility(v0_47);
        this.s.addView(v2_24);
        int v2_26 = new android.widget.EditText(p11);
        v2_26.setId(8196);
        android.widget.LinearLayout v0_51 = new android.widget.LinearLayout$LayoutParams(-1, -2);
        v0_51.setMargins(0, 0, 0, ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())));
        v2_26.setLayoutParams(v0_51);
        v2_26.setImeOptions(5);
        v2_26.setInputType(33);
        v2_26.setSingleLine(1);
        v2_26.setTextColor(-7829368);
        v2_26.setTextSize(2, 1097859072);
        v2_26.setTypeface(0, 0);
        v2_26.setHint(net.hockeyapp.android.aj.a(1027));
        v2_26.setHintTextColor(-3355444);
        net.hockeyapp.android.f.i.a(p11, v2_26);
        if (net.hockeyapp.android.t.c() != net.hockeyapp.android.c.i.a) {
            v0_61 = 0;
        } else {
            v0_61 = 8;
        }
        v2_26.setVisibility(v0_61);
        this.s.addView(v2_26);
        android.widget.LinearLayout v0_64 = new android.widget.EditText(p11);
        v0_64.setId(8198);
        int v2_29 = new android.widget.LinearLayout$LayoutParams(-1, -2);
        v2_29.setMargins(0, 0, 0, ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())));
        v0_64.setLayoutParams(v2_29);
        v0_64.setImeOptions(5);
        v0_64.setInputType(16433);
        v0_64.setSingleLine(1);
        v0_64.setTextColor(-7829368);
        v0_64.setTextSize(2, 1097859072);
        v0_64.setTypeface(0, 0);
        v0_64.setHint(net.hockeyapp.android.aj.a(1028));
        v0_64.setHintTextColor(-3355444);
        net.hockeyapp.android.f.i.a(p11, v0_64);
        this.s.addView(v0_64);
        android.widget.LinearLayout v0_66 = new android.widget.EditText(p11);
        v0_66.setId(8200);
        int v2_41 = new android.widget.LinearLayout$LayoutParams(-1, -2);
        int v4_11 = ((int) android.util.TypedValue.applyDimension(1, 1120403456, this.getResources().getDisplayMetrics()));
        v2_41.setMargins(0, 0, 0, ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())));
        v0_66.setLayoutParams(v2_41);
        v0_66.setImeOptions(5);
        v0_66.setInputType(16385);
        v0_66.setSingleLine(0);
        v0_66.setTextColor(-7829368);
        v0_66.setTextSize(2, 1097859072);
        v0_66.setTypeface(0, 0);
        v0_66.setMinimumHeight(v4_11);
        v0_66.setHint(net.hockeyapp.android.aj.a(1029));
        v0_66.setHintTextColor(-3355444);
        net.hockeyapp.android.f.i.a(p11, v0_66);
        this.s.addView(v0_66);
        this.v = new net.hockeyapp.android.f.a(p11);
        this.v.setId(8209);
        android.widget.LinearLayout v0_71 = new android.widget.LinearLayout$LayoutParams(-1, -1);
        int v2_55 = ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics()));
        v0_71.gravity = 3;
        this.v.setLayoutParams(v0_71);
        this.v.setPadding(0, 0, 0, v2_55);
        this.s.addView(this.v);
        android.widget.LinearLayout v0_75 = new android.widget.Button(p11);
        v0_75.setId(8208);
        int v2_61 = ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics()));
        android.widget.ListView v3_28 = ((int) android.util.TypedValue.applyDimension(1, 1106247680, this.getResources().getDisplayMetrics()));
        int v4_17 = ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics()));
        android.widget.LinearLayout$LayoutParams v5_3 = new android.widget.LinearLayout$LayoutParams(-1, -2);
        v5_3.setMargins(0, 0, 0, v4_17);
        v5_3.gravity = 1;
        v0_75.setLayoutParams(v5_3);
        v0_75.setBackgroundDrawable(this.getButtonSelector());
        v0_75.setPadding(v3_28, v2_61, v3_28, v2_61);
        v0_75.setText(net.hockeyapp.android.aj.a(1031));
        v0_75.setTextColor(-1);
        v0_75.setTextSize(2, 1097859072);
        this.s.addView(v0_75);
        android.widget.LinearLayout v0_77 = new android.widget.Button(p11);
        v0_77.setId(8201);
        int v2_70 = ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics()));
        android.widget.ListView v3_32 = ((int) android.util.TypedValue.applyDimension(1, 1106247680, this.getResources().getDisplayMetrics()));
        int v4_24 = ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics()));
        android.widget.LinearLayout$LayoutParams v5_5 = new android.widget.LinearLayout$LayoutParams(-1, -2);
        v5_5.setMargins(0, 0, 0, v4_24);
        v5_5.gravity = 1;
        v0_77.setLayoutParams(v5_5);
        v0_77.setBackgroundDrawable(this.getButtonSelector());
        v0_77.setPadding(v3_32, v2_70, v3_32, v2_70);
        v0_77.setText(net.hockeyapp.android.aj.a(1032));
        v0_77.setTextColor(-1);
        v0_77.setTextSize(2, 1097859072);
        this.s.addView(v0_77);
        android.widget.LinearLayout v0_79 = new android.widget.TextView(p11);
        v0_79.setId(8192);
        int v2_77 = new android.widget.LinearLayout$LayoutParams(-2, -2);
        android.widget.ListView v3_37 = ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics()));
        v2_77.setMargins(0, 0, 0, 0);
        v0_79.setLayoutParams(v2_77);
        v0_79.setPadding(0, v3_37, 0, v3_37);
        v0_79.setEllipsize(android.text.TextUtils$TruncateAt.END);
        v0_79.setShadowLayer(1065353216, 0, 1065353216, -1);
        v0_79.setSingleLine(1);
        v0_79.setText(net.hockeyapp.android.aj.a(1030));
        v0_79.setTextColor(-7829368);
        v0_79.setTextSize(2, 1097859072);
        v0_79.setTypeface(0, 0);
        this.t.addView(v0_79);
        this.u = new android.widget.LinearLayout(p11);
        this.u.setId(131092);
        android.widget.LinearLayout v0_84 = new android.widget.LinearLayout$LayoutParams(-1, -2);
        int v2_90 = ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics()));
        v0_84.gravity = 3;
        this.u.setLayoutParams(v0_84);
        this.u.setPadding(0, v2_90, 0, v2_90);
        this.u.setGravity(48);
        this.u.setOrientation(0);
        this.t.addView(this.u);
        android.widget.LinearLayout v0_90 = new android.widget.Button(p11);
        v0_90.setId(131088);
        int v2_97 = ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics()));
        android.widget.ListView v3_45 = ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics()));
        int v4_29 = ((int) android.util.TypedValue.applyDimension(1, 1084227584, this.getResources().getDisplayMetrics()));
        android.widget.LinearLayout$LayoutParams v5_9 = new android.widget.LinearLayout$LayoutParams(-1, -2);
        v5_9.setMargins(0, 0, v4_29, v3_45);
        v5_9.gravity = 1;
        v5_9.weight = 1065353216;
        v0_90.setLayoutParams(v5_9);
        v0_90.setBackgroundDrawable(this.getButtonSelector());
        v0_90.setPadding(0, v2_97, 0, v2_97);
        v0_90.setGravity(17);
        v0_90.setText(net.hockeyapp.android.aj.a(1033));
        v0_90.setTextColor(-1);
        v0_90.setTextSize(2, 1097859072);
        this.u.addView(v0_90);
        android.widget.LinearLayout v0_92 = new android.widget.Button(p11);
        v0_92.setId(131089);
        int v2_107 = ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics()));
        android.widget.ListView v3_52 = ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics()));
        int v4_32 = ((int) android.util.TypedValue.applyDimension(1, 1084227584, this.getResources().getDisplayMetrics()));
        android.widget.LinearLayout$LayoutParams v5_13 = new android.widget.LinearLayout$LayoutParams(-1, -2);
        v5_13.setMargins(v4_32, 0, 0, v3_52);
        v5_13.gravity = 1;
        v0_92.setLayoutParams(v5_13);
        v0_92.setBackgroundDrawable(this.getButtonSelector());
        v0_92.setPadding(0, v2_107, 0, v2_107);
        v0_92.setGravity(17);
        v0_92.setText(net.hockeyapp.android.aj.a(1034));
        v0_92.setTextColor(-1);
        v0_92.setTextSize(2, 1097859072);
        v5_13.weight = 1065353216;
        this.u.addView(v0_92);
        this.w = new android.widget.ListView(p11);
        this.w.setId(131094);
        int v2_118 = ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics()));
        this.w.setLayoutParams(new android.widget.LinearLayout$LayoutParams(-1, -1));
        this.w.setPadding(0, v2_118, 0, v2_118);
        this.t.addView(this.w);
        return;
    }

    private void a()
    {
        android.widget.LinearLayout$LayoutParams v0_1 = new android.widget.LinearLayout$LayoutParams(-1, -2);
        this.setBackgroundColor(-1);
        this.setLayoutParams(v0_1);
        return;
    }

    private void a(android.content.Context p5)
    {
        this.q = new android.widget.LinearLayout(p5);
        this.q.setId(131090);
        android.widget.LinearLayout v0_4 = new android.widget.LinearLayout$LayoutParams(-1, -1);
        v0_4.gravity = 49;
        this.q.setLayoutParams(v0_4);
        this.q.setPadding(0, 0, 0, 0);
        this.q.setOrientation(1);
        this.addView(this.q);
        return;
    }

    private static void a(android.content.Context p8, android.widget.EditText p9)
    {
        if (android.os.Build$VERSION.SDK_INT < 11) {
            android.graphics.drawable.LayerDrawable v0_5 = ((int) (p8.getResources().getDisplayMetrics().density * 1092616192));
            android.graphics.drawable.ShapeDrawable v1_3 = new android.graphics.drawable.ShapeDrawable(new android.graphics.drawable.shapes.RectShape());
            int v2_2 = v1_3.getPaint();
            v2_2.setColor(-1);
            v2_2.setStyle(android.graphics.Paint$Style.FILL_AND_STROKE);
            v2_2.setStrokeWidth(1065353216);
            v1_3.setPadding(v0_5, v0_5, v0_5, v0_5);
            android.graphics.drawable.LayerDrawable v0_9 = ((int) (((double) p8.getResources().getDisplayMetrics().density) * 1.5));
            int v2_6 = new android.graphics.drawable.ShapeDrawable(new android.graphics.drawable.shapes.RectShape());
            android.graphics.drawable.Drawable[] v3_4 = v2_6.getPaint();
            v3_4.setColor(-12303292);
            v3_4.setStyle(android.graphics.Paint$Style.FILL_AND_STROKE);
            v3_4.setStrokeWidth(1065353216);
            v2_6.setPadding(0, 0, 0, v0_9);
            android.graphics.drawable.Drawable[] v3_6 = new android.graphics.drawable.Drawable[2];
            v3_6[0] = v2_6;
            v3_6[1] = v1_3;
            p9.setBackgroundDrawable(new android.graphics.drawable.LayerDrawable(v3_6));
        }
        return;
    }

    private void b(android.content.Context p6)
    {
        this.r = new android.widget.ScrollView(p6);
        this.r.setId(131095);
        android.widget.LinearLayout v0_4 = new android.widget.LinearLayout$LayoutParams(-1, -1);
        android.widget.ScrollView v1_3 = ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics()));
        v0_4.gravity = 17;
        this.r.setLayoutParams(v0_4);
        this.r.setPadding(v1_3, 0, v1_3, 0);
        this.q.addView(this.r);
        return;
    }

    private void c(android.content.Context p5)
    {
        this.s = new android.widget.LinearLayout(p5);
        this.s.setId(131091);
        android.widget.ScrollView v0_4 = new android.widget.LinearLayout$LayoutParams(-1, -1);
        android.widget.LinearLayout v1_3 = ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics()));
        v0_4.gravity = 3;
        this.s.setLayoutParams(v0_4);
        this.s.setPadding(v1_3, v1_3, v1_3, v1_3);
        this.s.setGravity(48);
        this.s.setOrientation(1);
        this.r.addView(this.s);
        return;
    }

    private void d(android.content.Context p5)
    {
        this.t = new android.widget.LinearLayout(p5);
        this.t.setId(131093);
        android.widget.LinearLayout v0_4 = new android.widget.LinearLayout$LayoutParams(-1, -1);
        android.widget.LinearLayout v1_3 = ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics()));
        v0_4.gravity = 17;
        this.t.setLayoutParams(v0_4);
        this.t.setPadding(v1_3, v1_3, v1_3, 0);
        this.t.setGravity(48);
        this.t.setOrientation(1);
        this.q.addView(this.t);
        return;
    }

    private void e(android.content.Context p6)
    {
        this.u = new android.widget.LinearLayout(p6);
        this.u.setId(131092);
        android.widget.LinearLayout v0_4 = new android.widget.LinearLayout$LayoutParams(-1, -2);
        android.widget.LinearLayout v1_4 = ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics()));
        v0_4.gravity = 3;
        this.u.setLayoutParams(v0_4);
        this.u.setPadding(0, v1_4, 0, v1_4);
        this.u.setGravity(48);
        this.u.setOrientation(0);
        this.t.addView(this.u);
        return;
    }

    private void f(android.content.Context p6)
    {
        this.w = new android.widget.ListView(p6);
        this.w.setId(131094);
        android.widget.ListView v1_3 = ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics()));
        this.w.setLayoutParams(new android.widget.LinearLayout$LayoutParams(-1, -1));
        this.w.setPadding(0, v1_3, 0, v1_3);
        this.t.addView(this.w);
        return;
    }

    private void g(android.content.Context p7)
    {
        android.widget.LinearLayout v0_0 = 0;
        android.widget.EditText v1_1 = new android.widget.EditText(p7);
        v1_1.setId(8194);
        net.hockeyapp.android.c.i v2_2 = new android.widget.LinearLayout$LayoutParams(-1, -2);
        net.hockeyapp.android.c.i v3_3 = ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics()));
        v2_2.setMargins(0, (v3_3 / 2), 0, v3_3);
        v1_1.setLayoutParams(v2_2);
        v1_1.setImeOptions(5);
        v1_1.setInputType(16385);
        v1_1.setSingleLine(1);
        v1_1.setTextColor(-7829368);
        v1_1.setTextSize(2, 1097859072);
        v1_1.setTypeface(0, 0);
        v1_1.setHint(net.hockeyapp.android.aj.a(1026));
        v1_1.setHintTextColor(-3355444);
        net.hockeyapp.android.f.i.a(p7, v1_1);
        if (net.hockeyapp.android.t.b() == net.hockeyapp.android.c.i.a) {
            v0_0 = 8;
        }
        v1_1.setVisibility(v0_0);
        this.s.addView(v1_1);
        return;
    }

    private android.graphics.drawable.Drawable getButtonSelector()
    {
        android.graphics.drawable.StateListDrawable v0_1 = new android.graphics.drawable.StateListDrawable();
        int[] v1_0 = new int[1];
        v1_0[0] = -16842919;
        v0_1.addState(v1_0, new android.graphics.drawable.ColorDrawable(-16777216));
        int[] v1_2 = new int[2];
        v1_2 = {-16842919, 16842908};
        v0_1.addState(v1_2, new android.graphics.drawable.ColorDrawable(-12303292));
        int[] v1_3 = new int[1];
        v1_3[0] = 16842919;
        v0_1.addState(v1_3, new android.graphics.drawable.ColorDrawable(-7829368));
        return v0_1;
    }

    private void h(android.content.Context p7)
    {
        android.widget.LinearLayout v0_0 = 0;
        android.widget.EditText v1_1 = new android.widget.EditText(p7);
        v1_1.setId(8196);
        net.hockeyapp.android.c.i v2_2 = new android.widget.LinearLayout$LayoutParams(-1, -2);
        v2_2.setMargins(0, 0, 0, ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())));
        v1_1.setLayoutParams(v2_2);
        v1_1.setImeOptions(5);
        v1_1.setInputType(33);
        v1_1.setSingleLine(1);
        v1_1.setTextColor(-7829368);
        v1_1.setTextSize(2, 1097859072);
        v1_1.setTypeface(0, 0);
        v1_1.setHint(net.hockeyapp.android.aj.a(1027));
        v1_1.setHintTextColor(-3355444);
        net.hockeyapp.android.f.i.a(p7, v1_1);
        if (net.hockeyapp.android.t.c() == net.hockeyapp.android.c.i.a) {
            v0_0 = 8;
        }
        v1_1.setVisibility(v0_0);
        this.s.addView(v1_1);
        return;
    }

    private void i(android.content.Context p7)
    {
        android.widget.EditText v0_1 = new android.widget.EditText(p7);
        v0_1.setId(8198);
        android.widget.LinearLayout v1_2 = new android.widget.LinearLayout$LayoutParams(-1, -2);
        v1_2.setMargins(0, 0, 0, ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())));
        v0_1.setLayoutParams(v1_2);
        v0_1.setImeOptions(5);
        v0_1.setInputType(16433);
        v0_1.setSingleLine(1);
        v0_1.setTextColor(-7829368);
        v0_1.setTextSize(2, 1097859072);
        v0_1.setTypeface(0, 0);
        v0_1.setHint(net.hockeyapp.android.aj.a(1028));
        v0_1.setHintTextColor(-3355444);
        net.hockeyapp.android.f.i.a(p7, v0_1);
        this.s.addView(v0_1);
        return;
    }

    private void j(android.content.Context p8)
    {
        android.widget.EditText v0_1 = new android.widget.EditText(p8);
        v0_1.setId(8200);
        android.widget.LinearLayout v1_2 = new android.widget.LinearLayout$LayoutParams(-1, -2);
        int v3_5 = ((int) android.util.TypedValue.applyDimension(1, 1120403456, this.getResources().getDisplayMetrics()));
        v1_2.setMargins(0, 0, 0, ((int) android.util.TypedValue.applyDimension(1, 1101004800, this.getResources().getDisplayMetrics())));
        v0_1.setLayoutParams(v1_2);
        v0_1.setImeOptions(5);
        v0_1.setInputType(16385);
        v0_1.setSingleLine(0);
        v0_1.setTextColor(-7829368);
        v0_1.setTextSize(2, 1097859072);
        v0_1.setTypeface(0, 0);
        v0_1.setMinimumHeight(v3_5);
        v0_1.setHint(net.hockeyapp.android.aj.a(1029));
        v0_1.setHintTextColor(-3355444);
        net.hockeyapp.android.f.i.a(p8, v0_1);
        this.s.addView(v0_1);
        return;
    }

    private void k(android.content.Context p8)
    {
        android.widget.TextView v0_1 = new android.widget.TextView(p8);
        v0_1.setId(8192);
        android.widget.LinearLayout v1_2 = new android.widget.LinearLayout$LayoutParams(-2, -2);
        int v2_3 = ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics()));
        v1_2.setMargins(0, 0, 0, 0);
        v0_1.setLayoutParams(v1_2);
        v0_1.setPadding(0, v2_3, 0, v2_3);
        v0_1.setEllipsize(android.text.TextUtils$TruncateAt.END);
        v0_1.setShadowLayer(1065353216, 0, 1065353216, -1);
        v0_1.setSingleLine(1);
        v0_1.setText(net.hockeyapp.android.aj.a(1030));
        v0_1.setTextColor(-7829368);
        v0_1.setTextSize(2, 1097859072);
        v0_1.setTypeface(0, 0);
        this.t.addView(v0_1);
        return;
    }

    private void l(android.content.Context p6)
    {
        this.v = new net.hockeyapp.android.f.a(p6);
        this.v.setId(8209);
        android.widget.LinearLayout v0_4 = new android.widget.LinearLayout$LayoutParams(-1, -1);
        android.view.ViewGroup v1_3 = ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics()));
        v0_4.gravity = 3;
        this.v.setLayoutParams(v0_4);
        this.v.setPadding(0, 0, 0, v1_3);
        this.s.addView(this.v);
        return;
    }

    private void m(android.content.Context p10)
    {
        android.widget.Button v0_1 = new android.widget.Button(p10);
        v0_1.setId(8208);
        android.widget.LinearLayout v1_4 = ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics()));
        int v2_2 = ((int) android.util.TypedValue.applyDimension(1, 1106247680, this.getResources().getDisplayMetrics()));
        android.graphics.drawable.Drawable v3_5 = ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics()));
        android.widget.LinearLayout$LayoutParams v4_2 = new android.widget.LinearLayout$LayoutParams(-1, -2);
        v4_2.setMargins(0, 0, 0, v3_5);
        v4_2.gravity = 1;
        v0_1.setLayoutParams(v4_2);
        v0_1.setBackgroundDrawable(this.getButtonSelector());
        v0_1.setPadding(v2_2, v1_4, v2_2, v1_4);
        v0_1.setText(net.hockeyapp.android.aj.a(1031));
        v0_1.setTextColor(-1);
        v0_1.setTextSize(2, 1097859072);
        this.s.addView(v0_1);
        return;
    }

    private void n(android.content.Context p10)
    {
        android.widget.Button v0_1 = new android.widget.Button(p10);
        v0_1.setId(8201);
        android.widget.LinearLayout v1_4 = ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics()));
        int v2_2 = ((int) android.util.TypedValue.applyDimension(1, 1106247680, this.getResources().getDisplayMetrics()));
        android.graphics.drawable.Drawable v3_5 = ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics()));
        android.widget.LinearLayout$LayoutParams v4_2 = new android.widget.LinearLayout$LayoutParams(-1, -2);
        v4_2.setMargins(0, 0, 0, v3_5);
        v4_2.gravity = 1;
        v0_1.setLayoutParams(v4_2);
        v0_1.setBackgroundDrawable(this.getButtonSelector());
        v0_1.setPadding(v2_2, v1_4, v2_2, v1_4);
        v0_1.setText(net.hockeyapp.android.aj.a(1032));
        v0_1.setTextColor(-1);
        v0_1.setTextSize(2, 1097859072);
        this.s.addView(v0_1);
        return;
    }

    private void o(android.content.Context p10)
    {
        android.widget.Button v0_1 = new android.widget.Button(p10);
        v0_1.setId(131088);
        android.widget.LinearLayout v1_4 = ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics()));
        int v2_3 = ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics()));
        int v3_3 = ((int) android.util.TypedValue.applyDimension(1, 1084227584, this.getResources().getDisplayMetrics()));
        android.widget.LinearLayout$LayoutParams v4_3 = new android.widget.LinearLayout$LayoutParams(-1, -2);
        v4_3.setMargins(0, 0, v3_3, v2_3);
        v4_3.gravity = 1;
        v4_3.weight = 1065353216;
        v0_1.setLayoutParams(v4_3);
        v0_1.setBackgroundDrawable(this.getButtonSelector());
        v0_1.setPadding(0, v1_4, 0, v1_4);
        v0_1.setGravity(17);
        v0_1.setText(net.hockeyapp.android.aj.a(1033));
        v0_1.setTextColor(-1);
        v0_1.setTextSize(2, 1097859072);
        this.u.addView(v0_1);
        return;
    }

    private void p(android.content.Context p10)
    {
        android.widget.Button v0_1 = new android.widget.Button(p10);
        v0_1.setId(131089);
        android.widget.LinearLayout v1_4 = ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics()));
        int v2_3 = ((int) android.util.TypedValue.applyDimension(1, 1092616192, this.getResources().getDisplayMetrics()));
        int v3_3 = ((int) android.util.TypedValue.applyDimension(1, 1084227584, this.getResources().getDisplayMetrics()));
        android.widget.LinearLayout$LayoutParams v4_3 = new android.widget.LinearLayout$LayoutParams(-1, -2);
        v4_3.setMargins(v3_3, 0, 0, v2_3);
        v4_3.gravity = 1;
        v0_1.setLayoutParams(v4_3);
        v0_1.setBackgroundDrawable(this.getButtonSelector());
        v0_1.setPadding(0, v1_4, 0, v1_4);
        v0_1.setGravity(17);
        v0_1.setText(net.hockeyapp.android.aj.a(1034));
        v0_1.setTextColor(-1);
        v0_1.setTextSize(2, 1097859072);
        v4_3.weight = 1065353216;
        this.u.addView(v0_1);
        return;
    }

    private static android.graphics.drawable.Drawable q(android.content.Context p8)
    {
        android.graphics.drawable.LayerDrawable v0_4 = ((int) (p8.getResources().getDisplayMetrics().density * 1092616192));
        android.graphics.drawable.ShapeDrawable v1_2 = new android.graphics.drawable.ShapeDrawable(new android.graphics.drawable.shapes.RectShape());
        int v2_2 = v1_2.getPaint();
        v2_2.setColor(-1);
        v2_2.setStyle(android.graphics.Paint$Style.FILL_AND_STROKE);
        v2_2.setStrokeWidth(1065353216);
        v1_2.setPadding(v0_4, v0_4, v0_4, v0_4);
        android.graphics.drawable.LayerDrawable v0_8 = ((int) (((double) p8.getResources().getDisplayMetrics().density) * 1.5));
        int v2_6 = new android.graphics.drawable.ShapeDrawable(new android.graphics.drawable.shapes.RectShape());
        android.graphics.drawable.Drawable[] v3_4 = v2_6.getPaint();
        v3_4.setColor(-12303292);
        v3_4.setStyle(android.graphics.Paint$Style.FILL_AND_STROKE);
        v3_4.setStrokeWidth(1065353216);
        v2_6.setPadding(0, 0, 0, v0_8);
        android.graphics.drawable.Drawable[] v3_6 = new android.graphics.drawable.Drawable[2];
        v3_6[0] = v2_6;
        v3_6[1] = v1_2;
        return new android.graphics.drawable.LayerDrawable(v3_6);
    }
}
