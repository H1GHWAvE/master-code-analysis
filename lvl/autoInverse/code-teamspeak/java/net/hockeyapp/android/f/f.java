package net.hockeyapp.android.f;
final class f implements android.view.View$OnClickListener {
    final synthetic boolean a;
    final synthetic net.hockeyapp.android.f.b b;

    f(net.hockeyapp.android.f.b p1, boolean p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final void onClick(android.view.View p4)
    {
        if (this.a) {
            android.content.Intent v0_2 = new android.content.Intent();
            v0_2.setAction("android.intent.action.VIEW");
            v0_2.setDataAndType(net.hockeyapp.android.f.b.c(this.b), "*/*");
            net.hockeyapp.android.f.b.d(this.b).startActivity(v0_2);
        }
        return;
    }
}
