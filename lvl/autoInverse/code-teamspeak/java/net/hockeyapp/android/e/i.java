package net.hockeyapp.android.e;
public final class i {

    private i()
    {
        return;
    }

    synthetic i(byte p1)
    {
        return;
    }

    public static net.hockeyapp.android.c.h a(String p33)
    {
        net.hockeyapp.android.c.h v2_0;
        if (p33 == null) {
            v2_0 = 0;
        } else {
            try {
                org.json.JSONObject v7_1 = new org.json.JSONObject(p33);
                org.json.JSONObject v8 = v7_1.getJSONObject("feedback");
                net.hockeyapp.android.c.d v9_1 = new net.hockeyapp.android.c.d();
                org.json.JSONArray v10 = v8.getJSONArray("messages");
                net.hockeyapp.android.c.h v2_3 = 0;
            } catch (net.hockeyapp.android.c.h v2_21) {
                v2_0 = 0;
                org.json.JSONException v3_5 = v2_21;
                v3_5.printStackTrace();
            }
            if (v10.length() > 0) {
                v2_3 = new java.util.ArrayList();
                int v6 = 0;
                while (v6 < v10.length()) {
                    String v11 = v10.getJSONObject(v6).getString("subject").toString();
                    String v12 = v10.getJSONObject(v6).getString("text").toString();
                    String v13 = v10.getJSONObject(v6).getString("oem").toString();
                    String v14 = v10.getJSONObject(v6).getString("model").toString();
                    String v15 = v10.getJSONObject(v6).getString("os_version").toString();
                    String v16 = v10.getJSONObject(v6).getString("created_at").toString();
                    int v17 = v10.getJSONObject(v6).getInt("id");
                    String v18 = v10.getJSONObject(v6).getString("token").toString();
                    int v19 = v10.getJSONObject(v6).getInt("via");
                    String v20 = v10.getJSONObject(v6).getString("user_string").toString();
                    String v21 = v10.getJSONObject(v6).getString("clean_text").toString();
                    String v22 = v10.getJSONObject(v6).getString("name").toString();
                    String v23 = v10.getJSONObject(v6).getString("app_id").toString();
                    org.json.JSONArray v24 = v10.getJSONObject(v6).optJSONArray("attachments");
                    int v4_28 = java.util.Collections.emptyList();
                    if (v24 != null) {
                        v4_28 = new java.util.ArrayList();
                        int v5_14 = 0;
                        while (v5_14 < v24.length()) {
                            int v25_2 = v24.getJSONObject(v5_14).getInt("id");
                            int v26_2 = v24.getJSONObject(v5_14).getInt("feedback_message_id");
                            String v27_2 = v24.getJSONObject(v5_14).getString("file_name");
                            String v28_2 = v24.getJSONObject(v5_14).getString("url");
                            String v29_2 = v24.getJSONObject(v5_14).getString("created_at");
                            String v30_2 = v24.getJSONObject(v5_14).getString("updated_at");
                            net.hockeyapp.android.c.e v31_2 = new net.hockeyapp.android.c.e();
                            v31_2.a = v25_2;
                            v31_2.b = v26_2;
                            v31_2.c = v27_2;
                            v31_2.d = v28_2;
                            v31_2.e = v29_2;
                            v31_2.f = v30_2;
                            v4_28.add(v31_2);
                            v5_14++;
                        }
                    }
                    int v5_16 = new net.hockeyapp.android.c.g();
                    v5_16.m = v23;
                    v5_16.k = v21;
                    v5_16.f = v16;
                    v5_16.g = v17;
                    v5_16.d = v14;
                    v5_16.l = v22;
                    v5_16.c = v13;
                    v5_16.e = v15;
                    v5_16.a = v11;
                    v5_16.b = v12;
                    v5_16.h = v18;
                    v5_16.j = v20;
                    v5_16.i = v19;
                    v5_16.n = v4_28;
                    v2_3.add(v5_16);
                    v6++;
                }
            }
            v9_1.e = v2_3;
            try {
                v9_1.a = v8.getString("name").toString();
            } catch (net.hockeyapp.android.c.h v2_8) {
                v2_8.printStackTrace();
            }
            try {
                v9_1.b = v8.getString("email").toString();
            } catch (net.hockeyapp.android.c.h v2_12) {
                v2_12.printStackTrace();
            }
            v9_1.c = v8.getInt("id");
            try {
                v9_1.d = v8.getString("created_at").toString();
            } catch (net.hockeyapp.android.c.h v2_19) {
                v2_19.printStackTrace();
            }
            v2_0 = new net.hockeyapp.android.c.h();
            v2_0.b = v9_1;
            try {
                v2_0.a = v7_1.getString("status").toString();
            } catch (org.json.JSONException v3_4) {
                v3_4.printStackTrace();
            }
            try {
                v2_0.c = v7_1.getString("token").toString();
            } catch (org.json.JSONException v3_9) {
                v3_9.printStackTrace();
            }
        }
        return v2_0;
    }

    private static net.hockeyapp.android.e.i a()
    {
        return net.hockeyapp.android.e.k.a;
    }
}
