package net.hockeyapp.android.e;
final class e extends net.hockeyapp.android.e.c {
    public static final int c = 19;
    static final synthetic boolean h;
    private static final byte[] i;
    private static final byte[] j;
    int d;
    public final boolean e;
    public final boolean f;
    public final boolean g;
    private final byte[] k;
    private int l;
    private final byte[] m;

    static e()
    {
        byte[] v0_2;
        if (net.hockeyapp.android.e.b.desiredAssertionStatus()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        net.hockeyapp.android.e.e.h = v0_2;
        byte[] v0_3 = new byte[64];
        v0_3 = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
        net.hockeyapp.android.e.e.i = v0_3;
        byte[] v0_4 = new byte[64];
        v0_4 = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
        net.hockeyapp.android.e.e.j = v0_4;
        return;
    }

    public e(int p4)
    {
        int v0_2;
        int v1 = 1;
        this.a = 0;
        if ((p4 & 1) != 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        int v0_4;
        this.e = v0_2;
        if ((p4 & 2) != 0) {
            v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        this.f = v0_4;
        if ((p4 & 4) == 0) {
            v1 = 0;
        }
        int v0_7;
        this.g = v1;
        if ((p4 & 8) != 0) {
            v0_7 = net.hockeyapp.android.e.e.j;
        } else {
            v0_7 = net.hockeyapp.android.e.e.i;
        }
        int v0_11;
        this.m = v0_7;
        int v0_9 = new byte[2];
        this.k = v0_9;
        this.d = 0;
        if (!this.f) {
            v0_11 = -1;
        } else {
            v0_11 = 19;
        }
        this.l = v0_11;
        return;
    }

    public final int a(int p2)
    {
        return (((p2 * 8) / 5) + 10);
    }

    public final boolean a(byte[] p11, int p12, int p13)
    {
        int v2_7;
        int v3_4;
        byte[] v6 = this.m;
        byte[] v7 = this.a;
        int v4_0 = 0;
        int v1_0 = this.l;
        int v8 = (p13 + p12);
        switch (this.d) {
            case 0:
                v2_7 = p12;
                v3_4 = -1;
                break;
            case 1:
                if ((p12 + 2) > v8) {
                    v2_7 = p12;
                    v3_4 = -1;
                } else {
                    int v2_10 = (p12 + 1);
                    int v12_1 = (v2_10 + 1);
                    int v0_12 = ((((this.k[0] & 255) << 16) | ((p11[p12] & 255) << 8)) | (p11[v2_10] & 255));
                    this.d = 0;
                    v2_7 = v12_1;
                    v3_4 = v0_12;
                }
                break;
            case 2:
                if ((p12 + 1) > v8) {
                } else {
                    v2_7 = (p12 + 1);
                    int v0_6 = ((((this.k[0] & 255) << 16) | ((this.k[1] & 255) << 8)) | (p11[p12] & 255));
                    this.d = 0;
                    v3_4 = v0_6;
                }
                break;
            default:
        }
        int v5;
        if (v3_4 == -1) {
            v5 = v1_0;
        } else {
            v7[0] = v6[((v3_4 >> 18) & 63)];
            v7[1] = v6[((v3_4 >> 12) & 63)];
            v7[2] = v6[((v3_4 >> 6) & 63)];
            int v0_17 = 4;
            v7[3] = v6[(v3_4 & 63)];
            int v1_1 = (v1_0 - 1);
            if (v1_1 != 0) {
                v5 = v1_1;
                v4_0 = 4;
            } else {
                if (this.g) {
                    v0_17 = 5;
                    v7[4] = 13;
                }
                v4_0 = (v0_17 + 1);
                v7[v0_17] = 10;
                v5 = 19;
            }
        }
        while ((v2_7 + 3) <= v8) {
            int v0_64 = ((((p11[v2_7] & 255) << 16) | ((p11[(v2_7 + 1)] & 255) << 8)) | (p11[(v2_7 + 2)] & 255));
            v7[v4_0] = v6[((v0_64 >> 18) & 63)];
            v7[(v4_0 + 1)] = v6[((v0_64 >> 12) & 63)];
            v7[(v4_0 + 2)] = v6[((v0_64 >> 6) & 63)];
            v7[(v4_0 + 3)] = v6[(v0_64 & 63)];
            v2_7 += 3;
            int v1_38 = (v4_0 + 4);
            int v0_67 = (v5 - 1);
            if (v0_67 != 0) {
                v5 = v0_67;
                v4_0 = v1_38;
            } else {
                int v0_69;
                if (!this.g) {
                    v0_69 = v1_38;
                } else {
                    v0_69 = (v1_38 + 1);
                    v7[v1_38] = 13;
                }
                v4_0 = (v0_69 + 1);
                v7[v0_69] = 10;
                v5 = 19;
            }
        }
        if ((v2_7 - this.d) != (v8 - 1)) {
            if ((v2_7 - this.d) != (v8 - 2)) {
                if ((this.f) && ((v4_0 > 0) && (v5 != 19))) {
                    int v0_27;
                    if (!this.g) {
                        v0_27 = v4_0;
                    } else {
                        v0_27 = (v4_0 + 1);
                        v7[v4_0] = 13;
                    }
                    v4_0 = (v0_27 + 1);
                    v7[v0_27] = 10;
                }
            } else {
                int v0_29;
                int v1_9 = 0;
                if (this.d <= 1) {
                    v0_29 = p11[v2_7];
                    v2_7++;
                } else {
                    v1_9 = 1;
                    v0_29 = this.k[0];
                }
                int v0_33;
                if (this.d <= 0) {
                    v0_33 = p11[v2_7];
                    v2_7++;
                } else {
                    v0_33 = this.k[v1_9];
                    v1_9++;
                }
                int v0_41;
                int v0_37 = (((v0_33 & 255) << 2) | ((v0_29 & 255) << 10));
                this.d = (this.d - v1_9);
                int v1_11 = (v4_0 + 1);
                v7[v4_0] = v6[((v0_37 >> 12) & 63)];
                int v3_20 = (v1_11 + 1);
                v7[v1_11] = v6[((v0_37 >> 6) & 63)];
                int v1_12 = (v3_20 + 1);
                v7[v3_20] = v6[(v0_37 & 63)];
                if (!this.e) {
                    v0_41 = v1_12;
                } else {
                    v0_41 = (v1_12 + 1);
                    v7[v1_12] = 61;
                }
                if (this.f) {
                    if (this.g) {
                        int v1_15 = (v0_41 + 1);
                        v7[v0_41] = 13;
                        v0_41 = v1_15;
                    }
                    int v1_16 = (v0_41 + 1);
                    v7[v0_41] = 10;
                    v0_41 = v1_16;
                }
                v4_0 = v0_41;
            }
        } else {
            int v0_43;
            int v1_17 = 0;
            if (this.d <= 0) {
                v0_43 = p11[v2_7];
                v2_7++;
            } else {
                v1_17 = 1;
                v0_43 = this.k[0];
            }
            int v3_26 = ((v0_43 & 255) << 4);
            this.d = (this.d - v1_17);
            int v1_18 = (v4_0 + 1);
            v7[v4_0] = v6[((v3_26 >> 6) & 63)];
            int v0_51 = (v1_18 + 1);
            v7[v1_18] = v6[(v3_26 & 63)];
            if (this.e) {
                int v1_20 = (v0_51 + 1);
                v7[v0_51] = 61;
                v0_51 = (v1_20 + 1);
                v7[v1_20] = 61;
            }
            if (this.f) {
                if (this.g) {
                    int v1_23 = (v0_51 + 1);
                    v7[v0_51] = 13;
                    v0_51 = v1_23;
                }
                int v1_24 = (v0_51 + 1);
                v7[v0_51] = 10;
                v0_51 = v1_24;
            }
            v4_0 = v0_51;
        }
        if ((net.hockeyapp.android.e.e.h) || (this.d == 0)) {
            if ((net.hockeyapp.android.e.e.h) || (v2_7 == v8)) {
                this.b = v4_0;
                this.l = v5;
                return 1;
            } else {
                throw new AssertionError();
            }
        } else {
            throw new AssertionError();
        }
    }
}
