package net.hockeyapp.android.e;
public final class q {
    private static final char[] c;
    java.io.ByteArrayOutputStream a;
    String b;
    private boolean d;
    private boolean e;

    static q()
    {
        net.hockeyapp.android.e.q.c = "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        return;
    }

    public q()
    {
        String v0_0 = 0;
        this.e = 0;
        this.d = 0;
        this.a = new java.io.ByteArrayOutputStream();
        StringBuffer v1_3 = new StringBuffer();
        java.util.Random v2_1 = new java.util.Random();
        while (v0_0 < 30) {
            v1_3.append(net.hockeyapp.android.e.q.c[v2_1.nextInt(net.hockeyapp.android.e.q.c.length)]);
            v0_0++;
        }
        this.b = v1_3.toString();
        return;
    }

    private void a(String p3, java.io.File p4, boolean p5)
    {
        this.a(p3, p4.getName(), new java.io.FileInputStream(p4), p5);
        return;
    }

    private void a(String p4, String p5)
    {
        this.a();
        this.a.write(new StringBuilder("Content-Disposition: form-data; name=\"").append(p4).append("\"\r\n").toString().getBytes());
        this.a.write("Content-Type: text/plain; charset=UTF-8\r\n".getBytes());
        this.a.write("Content-Transfer-Encoding: 8bit\r\n\r\n".getBytes());
        this.a.write(p5.getBytes());
        this.a.write(new StringBuilder("\r\n--").append(this.b).append("\r\n").toString().getBytes());
        return;
    }

    private void a(String p5, String p6, java.io.InputStream p7, String p8, boolean p9)
    {
        this.a();
        try {
            java.io.IOException v0_4 = new StringBuilder("Content-Type: ").append(p8).append("\r\n").toString();
            this.a.write(new StringBuilder("Content-Disposition: form-data; name=\"").append(p5).append("\"; filename=\"").append(p6).append("\"\r\n").toString().getBytes());
            this.a.write(v0_4.getBytes());
            this.a.write("Content-Transfer-Encoding: binary\r\n\r\n".getBytes());
            java.io.IOException v0_8 = new byte[4096];
        } catch (java.io.IOException v0_11) {
            try {
                p7.close();
            } catch (byte[] v1_13) {
                v1_13.printStackTrace();
            }
            throw v0_11;
        }
        while(true) {
            byte[] v1_6 = p7.read(v0_8);
            if (v1_6 == -1) {
                break;
            }
            this.a.write(v0_8, 0, v1_6);
        }
        this.a.flush();
        if (!p9) {
            this.a.write(new StringBuilder("\r\n--").append(this.b).append("\r\n").toString().getBytes());
        } else {
            this.b();
        }
        try {
            p7.close();
        } catch (java.io.IOException v0_12) {
            v0_12.printStackTrace();
        }
        return;
    }

    private String c()
    {
        return this.b;
    }

    private long d()
    {
        this.b();
        return ((long) this.a.toByteArray().length);
    }

    private String e()
    {
        return new StringBuilder("multipart/form-data; boundary=").append(this.b).toString();
    }

    private java.io.ByteArrayOutputStream f()
    {
        this.b();
        return this.a;
    }

    public final void a()
    {
        if (!this.e) {
            this.a.write(new StringBuilder("--").append(this.b).append("\r\n").toString().getBytes());
        }
        this.e = 1;
        return;
    }

    public final void a(String p5, String p6, java.io.InputStream p7, boolean p8)
    {
        this.a();
        try {
            java.io.IOException v0_3 = new StringBuilder("Content-Type: ").append("application/octet-stream").append("\r\n").toString();
            this.a.write(new StringBuilder("Content-Disposition: form-data; name=\"").append(p5).append("\"; filename=\"").append(p6).append("\"\r\n").toString().getBytes());
            this.a.write(v0_3.getBytes());
            this.a.write("Content-Transfer-Encoding: binary\r\n\r\n".getBytes());
            java.io.IOException v0_7 = new byte[4096];
        } catch (java.io.IOException v0_10) {
            try {
                p7.close();
            } catch (byte[] v1_14) {
                v1_14.printStackTrace();
            }
            throw v0_10;
        }
        while(true) {
            byte[] v1_7 = p7.read(v0_7);
            if (v1_7 == -1) {
                break;
            }
            this.a.write(v0_7, 0, v1_7);
        }
        this.a.flush();
        if (!p8) {
            this.a.write(new StringBuilder("\r\n--").append(this.b).append("\r\n").toString().getBytes());
        } else {
            this.b();
        }
        try {
            p7.close();
        } catch (java.io.IOException v0_11) {
            v0_11.printStackTrace();
        }
        return;
    }

    public final void b()
    {
        if (!this.d) {
            try {
                this.a.write(new StringBuilder("\r\n--").append(this.b).append("--\r\n").toString().getBytes());
            } catch (int v0_2) {
                v0_2.printStackTrace();
            }
            this.d = 1;
        }
        return;
    }
}
