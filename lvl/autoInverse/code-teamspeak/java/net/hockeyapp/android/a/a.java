package net.hockeyapp.android.a;
public final class a extends android.widget.BaseAdapter {
    public java.util.ArrayList a;
    private android.content.Context b;
    private java.text.SimpleDateFormat c;
    private java.text.SimpleDateFormat d;
    private java.util.Date e;
    private android.widget.TextView f;
    private android.widget.TextView g;
    private android.widget.TextView h;
    private net.hockeyapp.android.f.a i;

    public a(android.content.Context p3, java.util.ArrayList p4)
    {
        this.b = p3;
        this.a = p4;
        this.c = new java.text.SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss\'Z\'");
        this.d = new java.text.SimpleDateFormat("d MMM h:mm a");
        return;
    }

    private void a()
    {
        if (this.a != null) {
            this.a.clear();
        }
        return;
    }

    private void a(net.hockeyapp.android.c.g p2)
    {
        if ((p2 != null) && (this.a != null)) {
            this.a.add(p2);
        }
        return;
    }

    public final int getCount()
    {
        return this.a.size();
    }

    public final Object getItem(int p2)
    {
        return this.a.get(p2);
    }

    public final long getItemId(int p3)
    {
        return ((long) p3);
    }

    public final android.view.View getView(int p8, android.view.View p9, android.view.ViewGroup p10)
    {
        net.hockeyapp.android.f.h v9_1;
        net.hockeyapp.android.f.a v0_2 = ((net.hockeyapp.android.c.g) this.a.get(p8));
        if (p9 != null) {
            v9_1 = ((net.hockeyapp.android.f.h) p9);
        } else {
            v9_1 = new net.hockeyapp.android.f.h(this.b);
        }
        if (v0_2 != null) {
            this.f = ((android.widget.TextView) v9_1.findViewById(12289));
            this.g = ((android.widget.TextView) v9_1.findViewById(12290));
            this.h = ((android.widget.TextView) v9_1.findViewById(12291));
            this.i = ((net.hockeyapp.android.f.a) v9_1.findViewById(12292));
            try {
                this.e = this.c.parse(v0_2.f);
                this.g.setText(this.d.format(this.e));
            } catch (java.util.Iterator v1_16) {
                v1_16.printStackTrace();
            }
            this.f.setText(v0_2.l);
            this.h.setText(v0_2.b);
            this.i.removeAllViews();
            java.util.Iterator v1_20 = v0_2.n.iterator();
            while (v1_20.hasNext()) {
                net.hockeyapp.android.f.a v0_8 = ((net.hockeyapp.android.c.e) v1_20.next());
                net.hockeyapp.android.f.b v3_6 = new net.hockeyapp.android.f.b(this.b, this.i, v0_8);
                net.hockeyapp.android.d.a v4_2 = net.hockeyapp.android.d.d.a;
                v4_2.a.add(new net.hockeyapp.android.d.e(v0_8, v3_6, 0));
                v4_2.a();
                this.i.addView(v3_6);
            }
        }
        net.hockeyapp.android.f.a v0_6;
        if ((p8 % 2) != 0) {
            v0_6 = 1;
        } else {
            v0_6 = 0;
        }
        v9_1.setFeedbackMessageViewBgAndTextColor(v0_6);
        return v9_1;
    }
}
