package net.hockeyapp.android.d;
public final class h extends net.hockeyapp.android.d.g {
    protected boolean h;
    private android.app.Activity i;
    private android.app.AlertDialog j;

    public h(ref.WeakReference p2, String p3, String p4, net.hockeyapp.android.az p5, boolean p6)
    {
        this(p2, p3, p4, p5);
        this.i = 0;
        this.j = 0;
        this.h = 0;
        if (p2 != null) {
            this.i = ((android.app.Activity) p2.get());
        }
        this.h = p6;
        return;
    }

    static synthetic android.app.Activity a(net.hockeyapp.android.d.h p1)
    {
        return p1.i;
    }

    static synthetic void a(net.hockeyapp.android.d.h p7, org.json.JSONArray p8)
    {
        if (p7.i != null) {
            String v1_0 = p7.i.getFragmentManager().beginTransaction();
            v1_0.setTransition(4097);
            Boolean v0_6 = p7.i.getFragmentManager().findFragmentByTag("hockey_update_dialog");
            if (v0_6 != null) {
                v1_0.remove(v0_6);
            }
            v1_0.addToBackStack(0);
            Boolean v0_7 = net.hockeyapp.android.at;
            if (p7.g != null) {
                v0_7 = net.hockeyapp.android.at;
            }
            try {
                Object[] v3_2 = new Class[2];
                v3_2[0] = org.json.JSONArray;
                v3_2[1] = String;
                Boolean v0_8 = v0_7.getMethod("newInstance", v3_2);
                Object[] v3_4 = new Object[2];
                v3_4[0] = p8;
                v3_4[1] = p7.a("apk");
                ((android.app.DialogFragment) v0_8.invoke(0, v3_4)).show(v1_0, "hockey_update_dialog");
            } catch (Boolean v0_11) {
                android.util.Log.d("HockeyApp", "An exception happened while showing the update fragment:");
                v0_11.printStackTrace();
                android.util.Log.d("HockeyApp", "Showing update activity instead.");
                p7.a(p8, Boolean.valueOf(0));
            }
        }
        return;
    }

    static synthetic void a(net.hockeyapp.android.d.h p0, org.json.JSONArray p1, Boolean p2)
    {
        p0.a(p1, p2);
        return;
    }

    private void a(org.json.JSONArray p4, Boolean p5)
    {
        android.app.Activity v0_0 = 0;
        if (this.g != null) {
            v0_0 = net.hockeyapp.android.al;
        }
        if (v0_0 == null) {
            v0_0 = net.hockeyapp.android.al;
        }
        if (this.i != null) {
            android.content.Intent v1_3 = new android.content.Intent();
            v1_3.setClass(this.i, v0_0);
            v1_3.putExtra("json", p4.toString());
            v1_3.putExtra("url", this.a("apk"));
            this.i.startActivity(v1_3);
            if (p5.booleanValue()) {
                this.i.finish();
            }
        }
        this.b();
        return;
    }

    private void b(org.json.JSONArray p5)
    {
        net.hockeyapp.android.e.x.a(this.i, p5.toString());
        if ((this.i != null) && (!this.i.isFinishing())) {
            Boolean v0_5 = new android.app.AlertDialog$Builder(this.i);
            v0_5.setTitle(net.hockeyapp.android.aj.a(this.g, 513));
            if (this.f.booleanValue()) {
                android.widget.Toast.makeText(this.i, net.hockeyapp.android.aj.a(this.g, 512), 1).show();
                this.a(p5, Boolean.valueOf(1));
            } else {
                v0_5.setMessage(net.hockeyapp.android.aj.a(this.g, 514));
                v0_5.setNegativeButton(net.hockeyapp.android.aj.a(this.g, 515), new net.hockeyapp.android.d.i(this));
                v0_5.setPositiveButton(net.hockeyapp.android.aj.a(this.g, 516), new net.hockeyapp.android.d.j(this, p5));
                this.j = v0_5.create();
                this.j.show();
            }
        }
        return;
    }

    private void c(org.json.JSONArray p8)
    {
        if (this.i != null) {
            String v1_0 = this.i.getFragmentManager().beginTransaction();
            v1_0.setTransition(4097);
            Boolean v0_6 = this.i.getFragmentManager().findFragmentByTag("hockey_update_dialog");
            if (v0_6 != null) {
                v1_0.remove(v0_6);
            }
            v1_0.addToBackStack(0);
            Boolean v0_7 = net.hockeyapp.android.at;
            if (this.g != null) {
                v0_7 = net.hockeyapp.android.at;
            }
            try {
                Object[] v3_2 = new Class[2];
                v3_2[0] = org.json.JSONArray;
                v3_2[1] = String;
                Boolean v0_8 = v0_7.getMethod("newInstance", v3_2);
                Object[] v3_4 = new Object[2];
                v3_4[0] = p8;
                v3_4[1] = this.a("apk");
                ((android.app.DialogFragment) v0_8.invoke(0, v3_4)).show(v1_0, "hockey_update_dialog");
            } catch (Boolean v0_11) {
                android.util.Log.d("HockeyApp", "An exception happened while showing the update fragment:");
                v0_11.printStackTrace();
                android.util.Log.d("HockeyApp", "Showing update activity instead.");
                this.a(p8, Boolean.valueOf(0));
            }
        }
        return;
    }

    public final void a()
    {
        super.a();
        this.i = 0;
        if (this.j != null) {
            this.j.dismiss();
            this.j = 0;
        }
        return;
    }

    protected final void a(org.json.JSONArray p5)
    {
        super.a(p5);
        if ((p5 != null) && (this.h)) {
            net.hockeyapp.android.e.x.a(this.i, p5.toString());
            if ((this.i != null) && (!this.i.isFinishing())) {
                Boolean v0_6 = new android.app.AlertDialog$Builder(this.i);
                v0_6.setTitle(net.hockeyapp.android.aj.a(this.g, 513));
                if (this.f.booleanValue()) {
                    android.widget.Toast.makeText(this.i, net.hockeyapp.android.aj.a(this.g, 512), 1).show();
                    this.a(p5, Boolean.valueOf(1));
                } else {
                    v0_6.setMessage(net.hockeyapp.android.aj.a(this.g, 514));
                    v0_6.setNegativeButton(net.hockeyapp.android.aj.a(this.g, 515), new net.hockeyapp.android.d.i(this));
                    v0_6.setPositiveButton(net.hockeyapp.android.aj.a(this.g, 516), new net.hockeyapp.android.d.j(this, p5));
                    this.j = v0_6.create();
                    this.j.show();
                }
            }
        }
        return;
    }

    protected final void b()
    {
        super.b();
        this.i = 0;
        this.j = 0;
        return;
    }

    protected final synthetic void onPostExecute(Object p1)
    {
        this.a(((org.json.JSONArray) p1));
        return;
    }
}
