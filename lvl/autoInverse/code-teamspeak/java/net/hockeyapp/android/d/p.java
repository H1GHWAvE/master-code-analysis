package net.hockeyapp.android.d;
public final class p extends net.hockeyapp.android.d.k {
    public android.content.Context a;
    public android.os.Handler b;
    public android.app.ProgressDialog c;
    public boolean d;
    private final int e;
    private final String f;
    private final java.util.Map g;

    public p(android.content.Context p2, android.os.Handler p3, String p4, int p5, java.util.Map p6)
    {
        this.a = p2;
        this.b = p3;
        this.f = p4;
        this.e = p5;
        this.g = p6;
        this.d = 1;
        if (p2 != null) {
            net.hockeyapp.android.a.a(p2);
        }
        return;
    }

    private java.net.HttpURLConnection a(int p6, java.util.Map p7)
    {
        IllegalArgumentException v0_12;
        if (p6 != 1) {
            if (p6 != 2) {
                if (p6 != 3) {
                    throw new IllegalArgumentException(new StringBuilder("Login mode ").append(p6).append(" not supported.").toString());
                } else {
                    v0_12 = new net.hockeyapp.android.e.l(new StringBuilder().append(this.f).append("?").append(((String) p7.get("type"))).append("=").append(((String) p7.get("id"))).toString()).a();
                }
            } else {
                String v2_8 = new net.hockeyapp.android.e.l(this.f);
                v2_8.b = "POST";
                v2_8.a("Authorization", new StringBuilder("Basic ").append(net.hockeyapp.android.e.b.a(new StringBuilder().append(((String) p7.get("email"))).append(":").append(((String) p7.get("password"))).toString().getBytes())).toString());
                v0_12 = v2_8.a();
            }
        } else {
            IllegalArgumentException v0_27 = new net.hockeyapp.android.e.l(this.f);
            v0_27.b = "POST";
            v0_12 = v0_27.a(p7).a();
        }
        return v0_12;
    }

    private void a()
    {
        this.d = 0;
        return;
    }

    private void a(android.content.Context p1, android.os.Handler p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    private void a(Boolean p5)
    {
        if (this.c != null) {
            try {
                this.c.dismiss();
            } catch (android.os.Message v0_2) {
                v0_2.printStackTrace();
            }
        }
        if (this.b != null) {
            android.os.Message v0_5 = new android.os.Message();
            android.os.Handler v1_1 = new android.os.Bundle();
            v1_1.putBoolean("success", p5.booleanValue());
            v0_5.setData(v1_1);
            this.b.sendMessage(v0_5);
        }
        return;
    }

    private boolean a(String p8)
    {
        IllegalArgumentException v0 = 0;
        String v2_1 = this.a.getSharedPreferences("net.hockeyapp.android.login", 0);
        try {
            String v3_2 = new org.json.JSONObject(p8);
            String v4_1 = v3_2.getString("status");
        } catch (android.content.SharedPreferences$Editor v1_6) {
            v1_6.printStackTrace();
            return v0;
        }
        if (!android.text.TextUtils.isEmpty(v4_1)) {
            if (this.e != 1) {
                if (this.e != 2) {
                    if (this.e != 3) {
                        throw new IllegalArgumentException(new StringBuilder("Login mode ").append(this.e).append(" not supported.").toString());
                    } else {
                        if (!v4_1.equals("validated")) {
                            net.hockeyapp.android.e.n.a(v2_1.edit().remove("iuid").remove("auid"));
                            return v0;
                        } else {
                            v0 = 1;
                            return v0;
                        }
                    }
                } else {
                    if (!v4_1.equals("authorized")) {
                        return v0;
                    } else {
                        String v3_9 = v3_2.getString("auid");
                        if (android.text.TextUtils.isEmpty(v3_9)) {
                            return v0;
                        } else {
                            net.hockeyapp.android.e.n.a(v2_1.edit().putString("auid", v3_9));
                            v0 = 1;
                            return v0;
                        }
                    }
                }
            } else {
                if (!v4_1.equals("identified")) {
                    return v0;
                } else {
                    String v3_10 = v3_2.getString("iuid");
                    if (android.text.TextUtils.isEmpty(v3_10)) {
                        return v0;
                    } else {
                        net.hockeyapp.android.e.n.a(v2_1.edit().putString("iuid", v3_10));
                        v0 = 1;
                        return v0;
                    }
                }
            }
        } else {
            return v0;
        }
    }

    private void b()
    {
        this.a = 0;
        this.b = 0;
        this.c = 0;
        return;
    }

    private varargs Boolean c()
    {
        boolean v2_0 = 0;
        try {
            Boolean v0_33;
            IllegalArgumentException v1_7;
            Boolean v0_0 = this.e;
            IllegalArgumentException v1_0 = this.g;
        } catch (Boolean v0_31) {
            if (v2_0) {
                v2_0.disconnect();
            }
            throw v0_31;
        } catch (Boolean v0_25) {
            v1_7 = 0;
            v0_25.printStackTrace();
            if (v1_7 == null) {
                v0_33 = Boolean.valueOf(0);
                return v0_33;
            } else {
                v1_7.disconnect();
            }
            if (v1_7 == null) {
            } else {
                v1_7.disconnect();
            }
        } catch (Boolean v0_31) {
            v2_0 = v1_7;
        } catch (Boolean v0_24) {
            v0_24.printStackTrace();
            if (!v2_0) {
            } else {
                v2_0.disconnect();
            }
        }
        if (v0_0 != 1) {
            if (v0_0 != 2) {
                if (v0_0 != 3) {
                    throw new IllegalArgumentException(new StringBuilder("Login mode ").append(v0_0).append(" not supported.").toString());
                } else {
                    v1_7 = new net.hockeyapp.android.e.l(new StringBuilder().append(this.f).append("?").append(((String) v1_0.get("type"))).append("=").append(((String) v1_0.get("id"))).toString()).a();
                }
            } else {
                String v3_13 = new net.hockeyapp.android.e.l(this.f);
                v3_13.b = "POST";
                v3_13.a("Authorization", new StringBuilder("Basic ").append(net.hockeyapp.android.e.b.a(new StringBuilder().append(((String) v1_0.get("email"))).append(":").append(((String) v1_0.get("password"))).toString().getBytes())).toString());
                v1_7 = v3_13.a();
            }
        } else {
            Boolean v0_27 = new net.hockeyapp.android.e.l(this.f);
            v0_27.b = "POST";
            v1_7 = v0_27.a(v1_0).a();
        }
        try {
            v1_7.connect();
        } catch (Boolean v0_25) {
        } catch (Boolean v0_24) {
            v2_0 = v1_7;
        }
        if (v1_7.getResponseCode() == 200) {
            Boolean v0_30 = net.hockeyapp.android.d.p.a(v1_7);
            if (!android.text.TextUtils.isEmpty(v0_30)) {
                v0_33 = Boolean.valueOf(this.a(v0_30));
                if (v1_7 == null) {
                    return v0_33;
                } else {
                    v1_7.disconnect();
                    return v0_33;
                }
            }
        }
    }

    protected final synthetic Object doInBackground(Object[] p2)
    {
        return this.c();
    }

    protected final synthetic void onPostExecute(Object p5)
    {
        if (this.c != null) {
            try {
                this.c.dismiss();
            } catch (android.os.Message v0_2) {
                v0_2.printStackTrace();
            }
        }
        if (this.b != null) {
            android.os.Message v0_5 = new android.os.Message();
            android.os.Handler v1_1 = new android.os.Bundle();
            v1_1.putBoolean("success", ((Boolean) p5).booleanValue());
            v0_5.setData(v1_1);
            this.b.sendMessage(v0_5);
        }
        return;
    }

    protected final void onPreExecute()
    {
        if (((this.c == null) || (!this.c.isShowing())) && (this.d)) {
            this.c = android.app.ProgressDialog.show(this.a, "", "Please wait...", 1, 0);
        }
        return;
    }
}
