package net.hockeyapp.android.d;
public class l extends android.os.AsyncTask {
    protected static final int a = 6;
    protected android.content.Context b;
    protected net.hockeyapp.android.b.a c;
    protected String d;
    protected String e;
    protected String f;
    protected android.app.ProgressDialog g;
    private String h;

    public l(android.content.Context p3, String p4, net.hockeyapp.android.b.a p5)
    {
        this.b = p3;
        this.d = p4;
        this.e = new StringBuilder().append(java.util.UUID.randomUUID()).append(".apk").toString();
        this.f = new StringBuilder().append(android.os.Environment.getExternalStorageDirectory().getAbsolutePath()).append("/Download").toString();
        this.c = p5;
        this.h = 0;
        return;
    }

    protected static java.net.URLConnection a(java.net.URL p4, int p5)
    {
        while(true) {
            java.net.HttpURLConnection v0_1 = ((java.net.HttpURLConnection) p4.openConnection());
            v0_1.addRequestProperty("User-Agent", "HockeySDK/Android");
            v0_1.setInstanceFollowRedirects(1);
            if (android.os.Build$VERSION.SDK_INT <= 9) {
                v0_1.setRequestProperty("connection", "close");
            }
            java.net.URL v1_4 = v0_1.getResponseCode();
            if (((v1_4 != 301) && ((v1_4 != 302) && (v1_4 != 303))) || (p5 == 0)) {
                break;
            }
            java.net.URL v1_6 = new java.net.URL(v0_1.getHeaderField("Location"));
            if (p4.getProtocol().equals(v1_6.getProtocol())) {
                break;
            }
            v0_1.disconnect();
            p5--;
            p4 = v1_6;
        }
        return v0_1;
    }

    private static void a(java.net.HttpURLConnection p2)
    {
        p2.addRequestProperty("User-Agent", "HockeySDK/Android");
        p2.setInstanceFollowRedirects(1);
        if (android.os.Build$VERSION.SDK_INT <= 9) {
            p2.setRequestProperty("connection", "close");
        }
        return;
    }

    public final void a()
    {
        this.b = 0;
        this.g = 0;
        return;
    }

    public final void a(android.content.Context p1)
    {
        this.b = p1;
        return;
    }

    protected void a(Long p5)
    {
        if (this.g != null) {
            try {
                this.g.dismiss();
            } catch (android.app.AlertDialog v0) {
            }
        }
        if (p5.longValue() <= 0) {
            android.app.AlertDialog v0_8;
            android.app.AlertDialog$Builder v1_1 = new android.app.AlertDialog$Builder(this.b);
            v1_1.setTitle(net.hockeyapp.android.aj.a(this.c, 256));
            if (this.h != null) {
                v0_8 = this.h;
            } else {
                v0_8 = net.hockeyapp.android.aj.a(this.c, 257);
            }
            v1_1.setMessage(v0_8);
            v1_1.setNegativeButton(net.hockeyapp.android.aj.a(this.c, 258), new net.hockeyapp.android.d.m(this));
            v1_1.setPositiveButton(net.hockeyapp.android.aj.a(this.c, 259), new net.hockeyapp.android.d.n(this));
            v1_1.create().show();
        } else {
            this.c.a(this);
            android.app.AlertDialog v0_17 = new android.content.Intent("android.intent.action.VIEW");
            v0_17.setDataAndType(android.net.Uri.fromFile(new java.io.File(this.f, this.e)), "application/vnd.android.package-archive");
            v0_17.setFlags(268435456);
            this.b.startActivity(v0_17);
        }
        return;
    }

    protected varargs void a(Integer[] p3)
    {
        try {
            if (this.g != null) {
                this.g.setProgress(p3[0].intValue());
            } else {
                this.g = new android.app.ProgressDialog(this.b);
                this.g.setProgressStyle(1);
                this.g.setMessage("Loading...");
                this.g.setCancelable(0);
                this.g.show();
            }
        } catch (android.app.ProgressDialog v0) {
        }
        return;
    }

    protected varargs Long b()
    {
        try {
            Long v0_6;
            Long v0_2 = net.hockeyapp.android.d.l.a(new java.net.URL(this.c()), 6);
            v0_2.connect();
            StringBuilder v4_0 = v0_2.getContentLength();
            String v1_2 = v0_2.getContentType();
        } catch (Long v0_11) {
            v0_11.printStackTrace();
            v0_6 = Long.valueOf(0);
            return v0_6;
        }
        if ((v1_2 == null) || (!v1_2.contains("text"))) {
            String v1_5 = new java.io.File(this.f);
            if ((v1_5.mkdirs()) || (v1_5.exists())) {
                String v5_5 = new java.io.File(v1_5, this.e);
                java.io.BufferedInputStream v6_2 = new java.io.BufferedInputStream(v0_2.getInputStream());
                java.io.FileOutputStream v7_1 = new java.io.FileOutputStream(v5_5);
                String v5_6 = new byte[1024];
                Long v0_5 = 0;
                while(true) {
                    int v8 = v6_2.read(v5_6);
                    if (v8 == -1) {
                        break;
                    }
                    v0_5 += ((long) v8);
                    int v9_2 = new Integer[1];
                    v9_2[0] = Integer.valueOf(Math.round(((((float) v0_5) * 1120403456) / ((float) v4_0))));
                    this.publishProgress(v9_2);
                    v7_1.write(v5_6, 0, v8);
                }
                v7_1.flush();
                v7_1.close();
                v6_2.close();
                v0_6 = Long.valueOf(v0_5);
                return v0_6;
            } else {
                throw new java.io.IOException(new StringBuilder("Could not create the dir(s):").append(v1_5.getAbsolutePath()).toString());
            }
        } else {
            this.h = "The requested download does not appear to be a file.";
            v0_6 = Long.valueOf(0);
            return v0_6;
        }
    }

    protected final String c()
    {
        return new StringBuilder().append(this.d).append("&type=apk").toString();
    }

    protected synthetic Object doInBackground(Object[] p2)
    {
        return this.b();
    }

    protected synthetic void onPostExecute(Object p1)
    {
        this.a(((Long) p1));
        return;
    }

    protected synthetic void onProgressUpdate(Object[] p1)
    {
        this.a(((Integer[]) p1));
        return;
    }
}
