package android.support.v7.b.a;
public final class a {

    public a()
    {
        return;
    }

    public static android.graphics.PorterDuff$Mode a(int p3)
    {
        android.graphics.PorterDuff$Mode v0_0 = 0;
        switch (p3) {
            case 3:
                v0_0 = android.graphics.PorterDuff$Mode.SRC_OVER;
            case 4:
            case 6:
            case 7:
            case 8:
            case 10:
            case 11:
            case 12:
            case 13:
            default:
                break;
            case 5:
                v0_0 = android.graphics.PorterDuff$Mode.SRC_IN;
            case 4:
            case 6:
            case 7:
            case 8:
            case 10:
            case 11:
            case 12:
            case 13:
                break;
            case 4:
            case 6:
            case 7:
            case 8:
            case 10:
            case 11:
            case 12:
            case 13:
                break;
            case 4:
            case 6:
            case 7:
            case 8:
            case 10:
            case 11:
            case 12:
            case 13:
                break;
            case 9:
                v0_0 = android.graphics.PorterDuff$Mode.SRC_ATOP;
            case 4:
            case 6:
            case 7:
            case 8:
            case 10:
            case 11:
            case 12:
            case 13:
                break;
            case 4:
            case 6:
            case 7:
            case 8:
            case 10:
            case 11:
            case 12:
            case 13:
                break;
            case 4:
            case 6:
            case 7:
            case 8:
            case 10:
            case 11:
            case 12:
            case 13:
                break;
            case 4:
            case 6:
            case 7:
            case 8:
            case 10:
            case 11:
            case 12:
            case 13:
                break;
            case 14:
                v0_0 = android.graphics.PorterDuff$Mode.MULTIPLY;
                break;
            case 15:
                v0_0 = android.graphics.PorterDuff$Mode.SCREEN;
                break;
            case 16:
                if (android.os.Build$VERSION.SDK_INT < 11) {
                } else {
                    v0_0 = android.graphics.PorterDuff$Mode.valueOf("ADD");
                }
                break;
        }
        return v0_0;
    }
}
