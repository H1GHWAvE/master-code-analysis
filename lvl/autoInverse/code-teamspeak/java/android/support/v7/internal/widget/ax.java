package android.support.v7.internal.widget;
public final class ax {
    public final android.content.res.TypedArray a;
    private final android.content.Context b;
    private android.support.v7.internal.widget.av c;

    private ax(android.content.Context p1, android.content.res.TypedArray p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    private float a(int p2, float p3)
    {
        return this.a.getDimension(p2, p3);
    }

    private float a(int p2, int p3, int p4, float p5)
    {
        return this.a.getFraction(p2, p3, p4, p5);
    }

    private int a(int p2, String p3)
    {
        return this.a.getLayoutDimension(p2, p3);
    }

    public static android.support.v7.internal.widget.ax a(android.content.Context p2, android.util.AttributeSet p3, int[] p4)
    {
        return new android.support.v7.internal.widget.ax(p2, p2.obtainStyledAttributes(p3, p4));
    }

    public static android.support.v7.internal.widget.ax a(android.content.Context p2, android.util.AttributeSet p3, int[] p4, int p5)
    {
        return new android.support.v7.internal.widget.ax(p2, p2.obtainStyledAttributes(p3, p4, p5, 0));
    }

    private boolean a(int p2, android.util.TypedValue p3)
    {
        return this.a.getValue(p2, p3);
    }

    private int b()
    {
        return this.a.length();
    }

    private int c()
    {
        return this.a.getIndexCount();
    }

    private android.content.res.Resources d()
    {
        return this.a.getResources();
    }

    private String e()
    {
        return this.a.getPositionDescription();
    }

    private int f(int p2)
    {
        return this.a.getIndex(p2);
    }

    private int f(int p2, int p3)
    {
        return this.a.getInteger(p2, p3);
    }

    private void f()
    {
        this.a.recycle();
        return;
    }

    private int g()
    {
        return this.a.getChangingConfigurations();
    }

    private String g(int p2)
    {
        return this.a.getString(p2);
    }

    private String h(int p2)
    {
        return this.a.getNonResourceString(p2);
    }

    private float i(int p3)
    {
        return this.a.getFloat(p3, -1082130432);
    }

    private android.content.res.ColorStateList j(int p2)
    {
        return this.a.getColorStateList(p2);
    }

    private CharSequence[] k(int p2)
    {
        return this.a.getTextArray(p2);
    }

    private int l(int p2)
    {
        return this.a.getType(p2);
    }

    private android.util.TypedValue m(int p2)
    {
        return this.a.peekValue(p2);
    }

    public final int a(int p2, int p3)
    {
        return this.a.getInt(p2, p3);
    }

    public final android.graphics.drawable.Drawable a(int p4)
    {
        android.graphics.drawable.Drawable v0_5;
        if (!this.a.hasValue(p4)) {
            v0_5 = this.a.getDrawable(p4);
        } else {
            android.graphics.drawable.Drawable v0_3 = this.a.getResourceId(p4, 0);
            if (v0_3 == null) {
            } else {
                v0_5 = this.a().a(v0_3, 0);
            }
        }
        return v0_5;
    }

    public final android.support.v7.internal.widget.av a()
    {
        if (this.c == null) {
            this.c = android.support.v7.internal.widget.av.a(this.b);
        }
        return this.c;
    }

    public final boolean a(int p2, boolean p3)
    {
        return this.a.getBoolean(p2, p3);
    }

    public final int b(int p2, int p3)
    {
        return this.a.getDimensionPixelOffset(p2, p3);
    }

    public final android.graphics.drawable.Drawable b(int p4)
    {
        android.graphics.drawable.Drawable v0_4;
        if (!this.a.hasValue(p4)) {
            v0_4 = 0;
        } else {
            android.graphics.drawable.Drawable v0_3 = this.a.getResourceId(p4, 0);
            if (v0_3 == null) {
            } else {
                v0_4 = this.a().a(v0_3, 1);
            }
        }
        return v0_4;
    }

    public final int c(int p2, int p3)
    {
        return this.a.getDimensionPixelSize(p2, p3);
    }

    public final CharSequence c(int p2)
    {
        return this.a.getText(p2);
    }

    public final int d(int p3)
    {
        return this.a.getColor(p3, -1);
    }

    public final int d(int p2, int p3)
    {
        return this.a.getLayoutDimension(p2, p3);
    }

    public final int e(int p2, int p3)
    {
        return this.a.getResourceId(p2, p3);
    }

    public final boolean e(int p2)
    {
        return this.a.hasValue(p2);
    }
}
