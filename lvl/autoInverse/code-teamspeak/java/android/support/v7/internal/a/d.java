package android.support.v7.internal.a;
public final class d {
    static final int a = 3;
    static final int b = 5;

    public d()
    {
        return;
    }

    private static int a(int p1)
    {
        int v0_1;
        if (p1 > 3) {
            v0_1 = android.support.v7.a.k.notification_template_big_media;
        } else {
            v0_1 = android.support.v7.a.k.notification_template_big_media_narrow;
        }
        return v0_1;
    }

    public static android.widget.RemoteViews a(android.content.Context p4, android.support.v4.app.ek p5)
    {
        int v0_1;
        if (p5.c() != null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        android.widget.RemoteViews v1_1 = new android.widget.RemoteViews(p4.getPackageName(), android.support.v7.a.k.notification_media_action);
        v1_1.setImageViewResource(android.support.v7.a.i.action0, p5.a());
        if (v0_1 == 0) {
            v1_1.setOnClickPendingIntent(android.support.v7.a.i.action0, p5.c());
        }
        if (android.os.Build$VERSION.SDK_INT >= 15) {
            v1_1.setContentDescription(android.support.v7.a.i.action0, p5.b());
        }
        return v1_1;
    }

    public static android.widget.RemoteViews a(android.content.Context p12, CharSequence p13, CharSequence p14, CharSequence p15, int p16, android.graphics.Bitmap p17, CharSequence p18, boolean p19, long p20, int p22, boolean p23)
    {
        android.widget.RemoteViews v2_1 = new android.widget.RemoteViews(p12.getPackageName(), p22);
        String v4_0 = 0;
        int v3_1 = 0;
        if ((p17 == null) || (android.os.Build$VERSION.SDK_INT < 16)) {
            v2_1.setViewVisibility(android.support.v7.a.i.icon, 8);
        } else {
            v2_1.setImageViewBitmap(android.support.v7.a.i.icon, p17);
        }
        if (p13 != null) {
            v2_1.setTextViewText(android.support.v7.a.i.title, p13);
        }
        if (p14 != null) {
            v2_1.setTextViewText(android.support.v7.a.i.text, p14);
            v4_0 = 1;
        }
        int v8;
        if (p15 == null) {
            if (p16 <= 0) {
                v2_1.setViewVisibility(android.support.v7.a.i.info, 8);
                v8 = v4_0;
            } else {
                if (p16 <= p12.getResources().getInteger(android.support.v7.a.j.status_bar_notification_info_maxnum)) {
                    v2_1.setTextViewText(android.support.v7.a.i.info, java.text.NumberFormat.getIntegerInstance().format(((long) p16)));
                } else {
                    v2_1.setTextViewText(android.support.v7.a.i.info, p12.getResources().getString(android.support.v7.a.l.status_bar_notification_info_overflow));
                }
                v2_1.setViewVisibility(android.support.v7.a.i.info, 0);
                v8 = 1;
            }
        } else {
            v2_1.setTextViewText(android.support.v7.a.i.info, p15);
            v2_1.setViewVisibility(android.support.v7.a.i.info, 0);
            v8 = 1;
        }
        if ((p18 != null) && (android.os.Build$VERSION.SDK_INT >= 16)) {
            v2_1.setTextViewText(android.support.v7.a.i.text, p18);
            if (p14 == null) {
                v2_1.setViewVisibility(android.support.v7.a.i.text2, 8);
            } else {
                v2_1.setTextViewText(android.support.v7.a.i.text2, p14);
                v2_1.setViewVisibility(android.support.v7.a.i.text2, 0);
                v3_1 = 1;
            }
        }
        if ((v3_1 != 0) && (android.os.Build$VERSION.SDK_INT >= 16)) {
            if (p23) {
                v2_1.setTextViewTextSize(android.support.v7.a.i.text, 0, ((float) p12.getResources().getDimensionPixelSize(android.support.v7.a.g.notification_subtext_size)));
            }
            v2_1.setViewPadding(android.support.v7.a.i.line1, 0, 0, 0, 0);
        }
        if (p20 != 0) {
            if (!p19) {
                v2_1.setViewVisibility(android.support.v7.a.i.time, 0);
                v2_1.setLong(android.support.v7.a.i.time, "setTime", p20);
            } else {
                v2_1.setViewVisibility(android.support.v7.a.i.chronometer, 0);
                v2_1.setLong(android.support.v7.a.i.chronometer, "setBase", ((android.os.SystemClock.elapsedRealtime() - System.currentTimeMillis()) + p20));
                v2_1.setBoolean(android.support.v7.a.i.chronometer, "setStarted", 1);
            }
        }
        int v3_15;
        if (v8 == 0) {
            v3_15 = 8;
        } else {
            v3_15 = 0;
        }
        v2_1.setViewVisibility(android.support.v7.a.i.line3, v3_15);
        return v2_1;
    }

    private static android.widget.RemoteViews a(android.content.Context p16, CharSequence p17, CharSequence p18, CharSequence p19, int p20, android.graphics.Bitmap p21, CharSequence p22, boolean p23, long p24, java.util.List p26, boolean p27, android.app.PendingIntent p28)
    {
        int v12;
        int v14 = Math.min(p26.size(), 5);
        if (v14 > 3) {
            v12 = android.support.v7.a.k.notification_template_big_media;
        } else {
            v12 = android.support.v7.a.k.notification_template_big_media_narrow;
        }
        android.widget.RemoteViews v4_1 = android.support.v7.internal.a.d.a(p16, p17, p18, p19, p20, p21, p22, p23, p24, v12, 0);
        v4_1.removeAllViews(android.support.v7.a.i.media_actions);
        if (v14 > 0) {
            int v3_2 = 0;
            while (v3_2 < v14) {
                v4_1.addView(android.support.v7.a.i.media_actions, android.support.v7.internal.a.d.a(p16, ((android.support.v4.app.ek) p26.get(v3_2))));
                v3_2++;
            }
        }
        if (!p27) {
            v4_1.setViewVisibility(android.support.v7.a.i.cancel_action, 8);
        } else {
            v4_1.setViewVisibility(android.support.v7.a.i.cancel_action, 0);
            v4_1.setInt(android.support.v7.a.i.cancel_action, "setAlpha", p16.getResources().getInteger(android.support.v7.a.j.cancel_button_image_alpha));
            v4_1.setOnClickPendingIntent(android.support.v7.a.i.cancel_action, p28);
        }
        return v4_1;
    }

    private static android.widget.RemoteViews a(android.content.Context p14, CharSequence p15, CharSequence p16, CharSequence p17, int p18, android.graphics.Bitmap p19, CharSequence p20, boolean p21, long p22, java.util.List p24, int[] p25, boolean p26, android.app.PendingIntent p27)
    {
        String v3_2;
        Object[] v5_1 = android.support.v7.internal.a.d.a(p14, p15, p16, p17, p18, p19, p20, p21, p22, android.support.v7.a.k.notification_template_media, 1);
        Integer v6_1 = p24.size();
        if (p25 != null) {
            v3_2 = Math.min(p25.length, 3);
        } else {
            v3_2 = 0;
        }
        v5_1.removeAllViews(android.support.v7.a.i.media_actions);
        if (v3_2 > null) {
            int v4_1 = 0;
            while (v4_1 < v3_2) {
                if (v4_1 < v6_1) {
                    v5_1.addView(android.support.v7.a.i.media_actions, android.support.v7.internal.a.d.a(p14, ((android.support.v4.app.ek) p24.get(p25[v4_1]))));
                    v4_1++;
                } else {
                    Object[] v5_3 = new Object[2];
                    v5_3[0] = Integer.valueOf(v4_1);
                    v5_3[1] = Integer.valueOf((v6_1 - 1));
                    throw new IllegalArgumentException(String.format("setShowActionsInCompactView: action %d out of bounds (max %d)", v5_3));
                }
            }
        }
        if (!p26) {
            v5_1.setViewVisibility(android.support.v7.a.i.end_padder, 0);
            v5_1.setViewVisibility(android.support.v7.a.i.cancel_action, 8);
        } else {
            v5_1.setViewVisibility(android.support.v7.a.i.end_padder, 8);
            v5_1.setViewVisibility(android.support.v7.a.i.cancel_action, 0);
            v5_1.setOnClickPendingIntent(android.support.v7.a.i.cancel_action, p27);
            v5_1.setInt(android.support.v7.a.i.cancel_action, "setAlpha", p14.getResources().getInteger(android.support.v7.a.j.cancel_button_image_alpha));
        }
        return v5_1;
    }

    private static void a(android.app.Notification p15, android.content.Context p16, CharSequence p17, CharSequence p18, CharSequence p19, int p20, android.graphics.Bitmap p21, CharSequence p22, boolean p23, long p24, java.util.List p26, boolean p27, android.app.PendingIntent p28)
    {
        int v12;
        int v14 = Math.min(p26.size(), 5);
        if (v14 > 3) {
            v12 = android.support.v7.a.k.notification_template_big_media;
        } else {
            v12 = android.support.v7.a.k.notification_template_big_media_narrow;
        }
        android.widget.RemoteViews v4_1 = android.support.v7.internal.a.d.a(p16, p17, p18, p19, p20, p21, p22, p23, p24, v12, 0);
        v4_1.removeAllViews(android.support.v7.a.i.media_actions);
        if (v14 > 0) {
            int v3_2 = 0;
            while (v3_2 < v14) {
                v4_1.addView(android.support.v7.a.i.media_actions, android.support.v7.internal.a.d.a(p16, ((android.support.v4.app.ek) p26.get(v3_2))));
                v3_2++;
            }
        }
        if (!p27) {
            v4_1.setViewVisibility(android.support.v7.a.i.cancel_action, 8);
        } else {
            v4_1.setViewVisibility(android.support.v7.a.i.cancel_action, 0);
            v4_1.setInt(android.support.v7.a.i.cancel_action, "setAlpha", p16.getResources().getInteger(android.support.v7.a.j.cancel_button_image_alpha));
            v4_1.setOnClickPendingIntent(android.support.v7.a.i.cancel_action, p28);
        }
        p15.bigContentView = v4_1;
        if (p27) {
            p15.flags = (p15.flags | 2);
        }
        return;
    }

    private static void a(android.support.v4.app.dc p15, android.content.Context p16, CharSequence p17, CharSequence p18, CharSequence p19, int p20, android.graphics.Bitmap p21, CharSequence p22, boolean p23, long p24, java.util.List p26, int[] p27, boolean p28, android.app.PendingIntent p29)
    {
        int v3_2;
        Object[] v5_1 = android.support.v7.internal.a.d.a(p16, p17, p18, p19, p20, p21, p22, p23, p24, android.support.v7.a.k.notification_template_media, 1);
        Integer v6_1 = p26.size();
        if (p27 != null) {
            v3_2 = Math.min(p27.length, 3);
        } else {
            v3_2 = 0;
        }
        v5_1.removeAllViews(android.support.v7.a.i.media_actions);
        if (v3_2 > 0) {
            int v4_1 = 0;
            while (v4_1 < v3_2) {
                if (v4_1 < v6_1) {
                    v5_1.addView(android.support.v7.a.i.media_actions, android.support.v7.internal.a.d.a(p16, ((android.support.v4.app.ek) p26.get(p27[v4_1]))));
                    v4_1++;
                } else {
                    Object[] v5_3 = new Object[2];
                    v5_3[0] = Integer.valueOf(v4_1);
                    v5_3[1] = Integer.valueOf((v6_1 - 1));
                    throw new IllegalArgumentException(String.format("setShowActionsInCompactView: action %d out of bounds (max %d)", v5_3));
                }
            }
        }
        if (!p28) {
            v5_1.setViewVisibility(android.support.v7.a.i.end_padder, 0);
            v5_1.setViewVisibility(android.support.v7.a.i.cancel_action, 8);
        } else {
            v5_1.setViewVisibility(android.support.v7.a.i.end_padder, 8);
            v5_1.setViewVisibility(android.support.v7.a.i.cancel_action, 0);
            v5_1.setOnClickPendingIntent(android.support.v7.a.i.cancel_action, p29);
            v5_1.setInt(android.support.v7.a.i.cancel_action, "setAlpha", p16.getResources().getInteger(android.support.v7.a.j.cancel_button_image_alpha));
        }
        p15.a().setContent(v5_1);
        if (p28) {
            p15.a().setOngoing(1);
        }
        return;
    }
}
