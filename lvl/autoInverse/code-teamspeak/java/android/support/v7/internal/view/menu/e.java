package android.support.v7.internal.view.menu;
abstract class e extends android.support.v7.internal.view.menu.f {
    final android.content.Context a;
    java.util.Map b;
    java.util.Map c;

    e(android.content.Context p1, Object p2)
    {
        this(p2);
        this.a = p1;
        return;
    }

    private void a(int p3)
    {
        if (this.b != null) {
            java.util.Iterator v1 = this.b.keySet().iterator();
            while (v1.hasNext()) {
                if (p3 == ((android.view.MenuItem) v1.next()).getGroupId()) {
                    v1.remove();
                }
            }
        }
        return;
    }

    private void b()
    {
        if (this.b != null) {
            this.b.clear();
        }
        if (this.c != null) {
            this.c.clear();
        }
        return;
    }

    private void b(int p3)
    {
        if (this.b != null) {
            java.util.Iterator v1 = this.b.keySet().iterator();
            while (v1.hasNext()) {
                if (p3 == ((android.view.MenuItem) v1.next()).getItemId()) {
                    v1.remove();
                    break;
                }
            }
        }
        return;
    }

    final android.view.MenuItem a(android.view.MenuItem p4)
    {
        android.view.MenuItem v1_0;
        if (!(p4 instanceof android.support.v4.g.a.b)) {
            v1_0 = p4;
        } else {
            if (this.b == null) {
                this.b = new android.support.v4.n.a();
            }
            v1_0 = ((android.view.MenuItem) this.b.get(p4));
            if (v1_0 == null) {
                v1_0 = android.support.v7.internal.view.menu.ab.a(this.a, ((android.support.v4.g.a.b) p4));
                this.b.put(((android.support.v4.g.a.b) p4), v1_0);
            }
        }
        return v1_0;
    }

    final android.view.SubMenu a(android.view.SubMenu p4)
    {
        UnsupportedOperationException v0_1;
        if (!(p4 instanceof android.support.v4.g.a.c)) {
            v0_1 = p4;
        } else {
            if (this.c == null) {
                this.c = new android.support.v4.n.a();
            }
            v0_1 = ((android.view.SubMenu) this.c.get(((android.support.v4.g.a.c) p4)));
            if (v0_1 == null) {
                if (android.os.Build$VERSION.SDK_INT < 14) {
                    throw new UnsupportedOperationException();
                } else {
                    v0_1 = new android.support.v7.internal.view.menu.ae(this.a, ((android.support.v4.g.a.c) p4));
                    this.c.put(((android.support.v4.g.a.c) p4), v0_1);
                }
            }
        }
        return v0_1;
    }
}
