package android.support.v7.internal.a;
public final class p extends android.support.v7.c.a implements android.support.v7.internal.view.menu.j {
    final synthetic android.support.v7.internal.a.l a;
    private final android.content.Context d;
    private final android.support.v7.internal.view.menu.i e;
    private android.support.v7.c.b f;
    private ref.WeakReference g;

    public p(android.support.v7.internal.a.l p3, android.content.Context p4, android.support.v7.c.b p5)
    {
        this.a = p3;
        this.d = p4;
        this.f = p5;
        android.support.v7.internal.view.menu.i v0_1 = new android.support.v7.internal.view.menu.i(p4);
        v0_1.i = 1;
        this.e = v0_1;
        this.e.a(this);
        return;
    }

    private boolean a(android.support.v7.internal.view.menu.ad p4)
    {
        int v0 = 1;
        if (this.f != null) {
            if (p4.hasVisibleItems()) {
                new android.support.v7.internal.view.menu.v(this.a.r(), p4).d();
            }
        } else {
            v0 = 0;
        }
        return v0;
    }

    private static void j()
    {
        return;
    }

    private static void k()
    {
        return;
    }

    public final android.view.MenuInflater a()
    {
        return new android.support.v7.internal.view.f(this.d);
    }

    public final void a(int p2)
    {
        this.b(android.support.v7.internal.a.l.j(this.a).getResources().getString(p2));
        return;
    }

    public final void a(android.support.v7.internal.view.menu.i p2)
    {
        if (this.f != null) {
            this.d();
            android.support.v7.internal.a.l.h(this.a).a();
        }
        return;
    }

    public final void a(android.view.View p2)
    {
        android.support.v7.internal.a.l.h(this.a).setCustomView(p2);
        this.g = new ref.WeakReference(p2);
        return;
    }

    public final void a(CharSequence p2)
    {
        android.support.v7.internal.a.l.h(this.a).setSubtitle(p2);
        return;
    }

    public final void a(boolean p2)
    {
        super.a(p2);
        android.support.v7.internal.a.l.h(this.a).setTitleOptional(p2);
        return;
    }

    public final boolean a(android.support.v7.internal.view.menu.i p2, android.view.MenuItem p3)
    {
        int v0_1;
        if (this.f == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.f.a(this, p3);
        }
        return v0_1;
    }

    public final android.view.Menu b()
    {
        return this.e;
    }

    public final void b(int p2)
    {
        this.a(android.support.v7.internal.a.l.j(this.a).getResources().getString(p2));
        return;
    }

    public final void b(CharSequence p2)
    {
        android.support.v7.internal.a.l.h(this.a).setTitle(p2);
        return;
    }

    public final void c()
    {
        if (this.a.j == this) {
            if (android.support.v7.internal.a.l.a(android.support.v7.internal.a.l.f(this.a), android.support.v7.internal.a.l.g(this.a))) {
                this.f.a(this);
            } else {
                this.a.k = this;
                this.a.l = this.f;
            }
            this.f = 0;
            this.a.j(0);
            android.support.v7.internal.a.l v0_10 = android.support.v7.internal.a.l.h(this.a);
            if (v0_10.g == null) {
                v0_10.i();
            }
            android.support.v7.internal.a.l.i(this.a).a().sendAccessibilityEvent(32);
            android.support.v7.internal.a.l.e(this.a).setHideOnContentScrollEnabled(this.a.m);
            this.a.j = 0;
        }
        return;
    }

    public final void d()
    {
        if (this.a.j == this) {
            this.e.d();
            try {
                this.f.b(this, this.e);
                this.e.e();
            } catch (Throwable v0_5) {
                this.e.e();
                throw v0_5;
            }
        }
        return;
    }

    public final boolean e()
    {
        this.e.d();
        try {
            Throwable v0_2 = this.f.a(this, this.e);
            this.e.e();
            return v0_2;
        } catch (Throwable v0_3) {
            this.e.e();
            throw v0_3;
        }
    }

    public final CharSequence f()
    {
        return android.support.v7.internal.a.l.h(this.a).getTitle();
    }

    public final CharSequence g()
    {
        return android.support.v7.internal.a.l.h(this.a).getSubtitle();
    }

    public final boolean h()
    {
        return android.support.v7.internal.a.l.h(this.a).h;
    }

    public final android.view.View i()
    {
        int v0_1;
        if (this.g == null) {
            v0_1 = 0;
        } else {
            v0_1 = ((android.view.View) this.g.get());
        }
        return v0_1;
    }
}
