package android.support.v7.internal.widget;
public final class k extends android.view.ViewGroup$MarginLayoutParams {

    public k()
    {
        this(-1, -1);
        return;
    }

    public k(android.content.Context p1, android.util.AttributeSet p2)
    {
        this(p1, p2);
        return;
    }

    public k(android.view.ViewGroup$LayoutParams p1)
    {
        this(p1);
        return;
    }

    private k(android.view.ViewGroup$MarginLayoutParams p1)
    {
        this(p1);
        return;
    }
}
