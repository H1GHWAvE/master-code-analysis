package android.support.v7.internal.widget;
final class e extends android.support.v7.internal.widget.d {

    public e(android.support.v7.internal.widget.ActionBarContainer p1)
    {
        this(p1);
        return;
    }

    public final void getOutline(android.graphics.Outline p2)
    {
        if (!this.a.d) {
            if (this.a.a != null) {
                this.a.a.getOutline(p2);
            }
        } else {
            if (this.a.c != null) {
                this.a.c.getOutline(p2);
            }
        }
        return;
    }
}
