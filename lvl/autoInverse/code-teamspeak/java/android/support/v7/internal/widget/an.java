package android.support.v7.internal.widget;
public final class an extends android.widget.BaseAdapter {
    final synthetic android.support.v7.internal.widget.al a;

    private an(android.support.v7.internal.widget.al p1)
    {
        this.a = p1;
        return;
    }

    synthetic an(android.support.v7.internal.widget.al p1, byte p2)
    {
        this(p1);
        return;
    }

    public final int getCount()
    {
        return android.support.v7.internal.widget.al.a(this.a).getChildCount();
    }

    public final Object getItem(int p2)
    {
        return ((android.support.v7.internal.widget.ap) android.support.v7.internal.widget.al.a(this.a).getChildAt(p2)).a;
    }

    public final long getItemId(int p3)
    {
        return ((long) p3);
    }

    public final android.view.View getView(int p3, android.view.View p4, android.view.ViewGroup p5)
    {
        if (p4 != null) {
            ((android.support.v7.internal.widget.ap) p4).a = ((android.support.v7.app.g) this.getItem(p3));
            ((android.support.v7.internal.widget.ap) p4).a();
        } else {
            p4 = android.support.v7.internal.widget.al.a(this.a, ((android.support.v7.app.g) this.getItem(p3)));
        }
        return p4;
    }
}
