package android.support.v7.internal.view;
public final class a {
    public android.content.Context a;

    private a(android.content.Context p1)
    {
        this.a = p1;
        return;
    }

    public static android.support.v7.internal.view.a a(android.content.Context p1)
    {
        return new android.support.v7.internal.view.a(p1);
    }

    private int d()
    {
        return this.a.getResources().getInteger(android.support.v7.a.j.abc_max_action_buttons);
    }

    private boolean e()
    {
        int v0 = 1;
        if ((android.os.Build$VERSION.SDK_INT < 19) && (android.support.v4.view.du.b(android.view.ViewConfiguration.get(this.a)))) {
            v0 = 0;
        }
        return v0;
    }

    private int f()
    {
        return (this.a.getResources().getDisplayMetrics().widthPixels / 2);
    }

    private boolean g()
    {
        int v0_3;
        if (this.a.getApplicationInfo().targetSdkVersion >= 14) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final boolean a()
    {
        boolean v0_5;
        if (this.a.getApplicationInfo().targetSdkVersion < 16) {
            v0_5 = this.a.getResources().getBoolean(android.support.v7.a.e.abc_action_bar_embed_tabs_pre_jb);
        } else {
            v0_5 = this.a.getResources().getBoolean(android.support.v7.a.e.abc_action_bar_embed_tabs);
        }
        return v0_5;
    }

    public final int b()
    {
        android.content.res.TypedArray v1_1 = this.a.obtainStyledAttributes(0, android.support.v7.a.n.ActionBar, android.support.v7.a.d.actionBarStyle, 0);
        int v0_2 = v1_1.getLayoutDimension(android.support.v7.a.n.ActionBar_height, 0);
        int v2_2 = this.a.getResources();
        if (!this.a()) {
            v0_2 = Math.min(v0_2, v2_2.getDimensionPixelSize(android.support.v7.a.g.abc_action_bar_stacked_max_height));
        }
        v1_1.recycle();
        return v0_2;
    }

    public final int c()
    {
        return this.a.getResources().getDimensionPixelSize(android.support.v7.a.g.abc_action_bar_stacked_tab_max_width);
    }
}
