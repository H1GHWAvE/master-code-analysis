package android.support.v7.internal.view;
public final class c extends android.support.v7.c.a implements android.support.v7.internal.view.menu.j {
    private android.content.Context a;
    private android.support.v7.internal.widget.ActionBarContextView d;
    private android.support.v7.c.b e;
    private ref.WeakReference f;
    private boolean g;
    private boolean h;
    private android.support.v7.internal.view.menu.i i;

    public c(android.content.Context p3, android.support.v7.internal.widget.ActionBarContextView p4, android.support.v7.c.b p5, boolean p6)
    {
        this.a = p3;
        this.d = p4;
        this.e = p5;
        android.support.v7.internal.view.menu.i v0_1 = new android.support.v7.internal.view.menu.i(p4.getContext());
        v0_1.i = 1;
        this.i = v0_1;
        this.i.a(this);
        this.h = p6;
        return;
    }

    private boolean a(android.support.v7.internal.view.menu.ad p4)
    {
        if (p4.hasVisibleItems()) {
            new android.support.v7.internal.view.menu.v(this.d.getContext(), p4).d();
        }
        return 1;
    }

    private static void j()
    {
        return;
    }

    private static void k()
    {
        return;
    }

    public final android.view.MenuInflater a()
    {
        return new android.view.MenuInflater(this.d.getContext());
    }

    public final void a(int p2)
    {
        this.b(this.a.getString(p2));
        return;
    }

    public final void a(android.support.v7.internal.view.menu.i p2)
    {
        this.d();
        this.d.a();
        return;
    }

    public final void a(android.view.View p2)
    {
        int v0_1;
        this.d.setCustomView(p2);
        if (p2 == null) {
            v0_1 = 0;
        } else {
            v0_1 = new ref.WeakReference(p2);
        }
        this.f = v0_1;
        return;
    }

    public final void a(CharSequence p2)
    {
        this.d.setSubtitle(p2);
        return;
    }

    public final void a(boolean p2)
    {
        super.a(p2);
        this.d.setTitleOptional(p2);
        return;
    }

    public final boolean a(android.support.v7.internal.view.menu.i p2, android.view.MenuItem p3)
    {
        return this.e.a(this, p3);
    }

    public final android.view.Menu b()
    {
        return this.i;
    }

    public final void b(int p2)
    {
        this.a(this.a.getString(p2));
        return;
    }

    public final void b(CharSequence p2)
    {
        this.d.setTitle(p2);
        return;
    }

    public final boolean b_()
    {
        return this.h;
    }

    public final void c()
    {
        if (!this.g) {
            this.g = 1;
            this.d.sendAccessibilityEvent(32);
            this.e.a(this);
        }
        return;
    }

    public final void d()
    {
        this.e.b(this, this.i);
        return;
    }

    public final CharSequence f()
    {
        return this.d.getTitle();
    }

    public final CharSequence g()
    {
        return this.d.getSubtitle();
    }

    public final boolean h()
    {
        return this.d.h;
    }

    public final android.view.View i()
    {
        int v0_1;
        if (this.f == null) {
            v0_1 = 0;
        } else {
            v0_1 = ((android.view.View) this.f.get());
        }
        return v0_1;
    }
}
