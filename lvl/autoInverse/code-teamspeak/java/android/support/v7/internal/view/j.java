package android.support.v7.internal.view;
final class j extends android.support.v4.view.ge {
    final synthetic android.support.v7.internal.view.i a;
    private boolean b;
    private int c;

    j(android.support.v7.internal.view.i p2)
    {
        this.a = p2;
        this.b = 0;
        this.c = 0;
        return;
    }

    private void a()
    {
        this.c = 0;
        this.b = 0;
        this.a.c = 0;
        return;
    }

    public final void a(android.view.View p3)
    {
        if (!this.b) {
            this.b = 1;
            if (this.a.b != null) {
                this.a.b.a(0);
            }
        }
        return;
    }

    public final void b(android.view.View p4)
    {
        android.support.v7.internal.view.i v0_1 = (this.c + 1);
        this.c = v0_1;
        if (v0_1 == this.a.a.size()) {
            if (this.a.b != null) {
                this.a.b.b(0);
            }
            this.c = 0;
            this.b = 0;
            this.a.c = 0;
        }
        return;
    }
}
