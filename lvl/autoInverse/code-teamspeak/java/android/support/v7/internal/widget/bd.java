package android.support.v7.internal.widget;
public final class bd {
    private static final String a = "ViewUtils";
    private static reflect.Method b;

    static bd()
    {
        if (android.os.Build$VERSION.SDK_INT >= 18) {
            try {
                Class[] v2_1 = new Class[2];
                v2_1[0] = android.graphics.Rect;
                v2_1[1] = android.graphics.Rect;
                reflect.Method v0_2 = android.view.View.getDeclaredMethod("computeFitSystemWindows", v2_1);
                android.support.v7.internal.widget.bd.b = v0_2;
            } catch (reflect.Method v0) {
                android.util.Log.d("ViewUtils", "Could not find method computeFitSystemWindows. Oh well.");
            }
            if (!v0_2.isAccessible()) {
                android.support.v7.internal.widget.bd.b.setAccessible(1);
            }
        }
        return;
    }

    private bd()
    {
        return;
    }

    public static int a(int p1, int p2)
    {
        return (p1 | p2);
    }

    public static void a(android.view.View p3, android.graphics.Rect p4, android.graphics.Rect p5)
    {
        if (android.support.v7.internal.widget.bd.b != null) {
            try {
                String v1_1 = new Object[2];
                v1_1[0] = p4;
                v1_1[1] = p5;
                android.support.v7.internal.widget.bd.b.invoke(p3, v1_1);
            } catch (Exception v0_2) {
                android.util.Log.d("ViewUtils", "Could not invoke computeFitSystemWindows", v0_2);
            }
        }
        return;
    }

    public static boolean a(android.view.View p2)
    {
        int v0 = 1;
        if (android.support.v4.view.cx.f(p2) != 1) {
            v0 = 0;
        }
        return v0;
    }

    public static void b(android.view.View p3)
    {
        if (android.os.Build$VERSION.SDK_INT >= 16) {
            try {
                String v2_1 = new Class[0];
                IllegalAccessException v0_2 = p3.getClass().getMethod("makeOptionalFitsSystemWindows", v2_1);
            } catch (IllegalAccessException v0) {
                android.util.Log.d("ViewUtils", "Could not find method makeOptionalFitsSystemWindows. Oh well...");
            } catch (IllegalAccessException v0_3) {
                android.util.Log.d("ViewUtils", "Could not invoke makeOptionalFitsSystemWindows", v0_3);
            } catch (IllegalAccessException v0_4) {
                android.util.Log.d("ViewUtils", "Could not invoke makeOptionalFitsSystemWindows", v0_4);
            }
            if (!v0_2.isAccessible()) {
                v0_2.setAccessible(1);
            }
            Object[] v1_5 = new Object[0];
            v0_2.invoke(p3, v1_5);
        }
        return;
    }
}
