package android.support.v7.internal.widget;
public class ActionBarOverlayLayout extends android.view.ViewGroup implements android.support.v4.view.bv, android.support.v7.internal.widget.ac {
    static final int[] c = None;
    private static final String d = "ActionBarOverlayLayout";
    private final Runnable A;
    private final Runnable B;
    private final android.support.v4.view.bw C;
    public boolean a;
    public boolean b;
    private int e;
    private int f;
    private android.support.v7.internal.widget.ContentFrameLayout g;
    private android.support.v7.internal.widget.ActionBarContainer h;
    private android.support.v7.internal.widget.ad i;
    private android.graphics.drawable.Drawable j;
    private boolean k;
    private boolean l;
    private boolean m;
    private int n;
    private int o;
    private final android.graphics.Rect p;
    private final android.graphics.Rect q;
    private final android.graphics.Rect r;
    private final android.graphics.Rect s;
    private final android.graphics.Rect t;
    private final android.graphics.Rect u;
    private android.support.v7.internal.widget.j v;
    private final int w;
    private android.support.v4.widget.ca x;
    private android.support.v4.view.fk y;
    private final android.support.v4.view.gd z;

    static ActionBarOverlayLayout()
    {
        int[] v0_1 = new int[2];
        v0_1[0] = android.support.v7.a.d.actionBarSize;
        v0_1[1] = 16842841;
        android.support.v7.internal.widget.ActionBarOverlayLayout.c = v0_1;
        return;
    }

    public ActionBarOverlayLayout(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    public ActionBarOverlayLayout(android.content.Context p2, android.util.AttributeSet p3)
    {
        this(p2, p3);
        this.f = 0;
        this.p = new android.graphics.Rect();
        this.q = new android.graphics.Rect();
        this.r = new android.graphics.Rect();
        this.s = new android.graphics.Rect();
        this.t = new android.graphics.Rect();
        this.u = new android.graphics.Rect();
        this.w = 600;
        this.z = new android.support.v7.internal.widget.g(this);
        this.A = new android.support.v7.internal.widget.h(this);
        this.B = new android.support.v7.internal.widget.i(this);
        this.a(p2);
        this.C = new android.support.v4.view.bw(this);
        return;
    }

    static synthetic android.support.v4.view.fk a(android.support.v7.internal.widget.ActionBarOverlayLayout p0, android.support.v4.view.fk p1)
    {
        p0.y = p1;
        return p1;
    }

    private static android.support.v7.internal.widget.ad a(android.view.View p3)
    {
        android.support.v7.internal.widget.ad v3_2;
        if (!(p3 instanceof android.support.v7.internal.widget.ad)) {
            if (!(p3 instanceof android.support.v7.widget.Toolbar)) {
                throw new IllegalStateException(new StringBuilder("Can\'t make a decor toolbar out of ").append(p3.getClass().getSimpleName()).toString());
            } else {
                v3_2 = ((android.support.v7.widget.Toolbar) p3).getWrapper();
            }
        } else {
            v3_2 = ((android.support.v7.internal.widget.ad) p3);
        }
        return v3_2;
    }

    private android.support.v7.internal.widget.k a(android.util.AttributeSet p3)
    {
        return new android.support.v7.internal.widget.k(this.getContext(), p3);
    }

    private void a(android.content.Context p5)
    {
        android.support.v4.widget.ca v0_5;
        int v1 = 1;
        int v3_1 = this.getContext().getTheme().obtainStyledAttributes(android.support.v7.internal.widget.ActionBarOverlayLayout.c);
        this.e = v3_1.getDimensionPixelSize(0, 0);
        this.j = v3_1.getDrawable(1);
        if (this.j != null) {
            v0_5 = 0;
        } else {
            v0_5 = 1;
        }
        this.setWillNotDraw(v0_5);
        v3_1.recycle();
        if (p5.getApplicationInfo().targetSdkVersion >= 19) {
            v1 = 0;
        }
        this.k = v1;
        this.x = android.support.v4.widget.ca.a(p5, 0);
        return;
    }

    private boolean a(float p10)
    {
        int v1 = 0;
        this.x.a(0, 0, 0, ((int) p10), 0, 0, -2147483648, 2147483647);
        if (this.x.d() > this.h.getHeight()) {
            v1 = 1;
        }
        return v1;
    }

    static synthetic boolean a(android.support.v7.internal.widget.ActionBarOverlayLayout p1)
    {
        p1.m = 0;
        return 0;
    }

    private static boolean a(android.view.View p5, android.graphics.Rect p6, boolean p7)
    {
        int v2 = 1;
        int v1_0 = 0;
        android.support.v7.internal.widget.k v0_1 = ((android.support.v7.internal.widget.k) p5.getLayoutParams());
        if (v0_1.leftMargin != p6.left) {
            v0_1.leftMargin = p6.left;
            v1_0 = 1;
        }
        if (v0_1.topMargin != p6.top) {
            v0_1.topMargin = p6.top;
            v1_0 = 1;
        }
        if (v0_1.rightMargin != p6.right) {
            v0_1.rightMargin = p6.right;
            v1_0 = 1;
        }
        if ((!p7) || (v0_1.bottomMargin == p6.bottom)) {
            v2 = v1_0;
        } else {
            v0_1.bottomMargin = p6.bottom;
        }
        return v2;
    }

    static synthetic void b(android.support.v7.internal.widget.ActionBarOverlayLayout p0)
    {
        p0.n();
        return;
    }

    static synthetic android.support.v4.view.gd c(android.support.v7.internal.widget.ActionBarOverlayLayout p1)
    {
        return p1.z;
    }

    static synthetic android.support.v7.internal.widget.ActionBarContainer d(android.support.v7.internal.widget.ActionBarOverlayLayout p1)
    {
        return p1.h;
    }

    private boolean j()
    {
        return this.a;
    }

    private static android.support.v7.internal.widget.k k()
    {
        return new android.support.v7.internal.widget.k();
    }

    private void l()
    {
        if (this.g == null) {
            String v0_14;
            this.g = ((android.support.v7.internal.widget.ContentFrameLayout) this.findViewById(android.support.v7.a.i.action_bar_activity_content));
            this.h = ((android.support.v7.internal.widget.ActionBarContainer) this.findViewById(android.support.v7.a.i.action_bar_container));
            String v0_8 = this.findViewById(android.support.v7.a.i.action_bar);
            if (!(v0_8 instanceof android.support.v7.internal.widget.ad)) {
                if (!(v0_8 instanceof android.support.v7.widget.Toolbar)) {
                    throw new IllegalStateException(new StringBuilder("Can\'t make a decor toolbar out of ").append(v0_8.getClass().getSimpleName()).toString());
                } else {
                    v0_14 = ((android.support.v7.widget.Toolbar) v0_8).getWrapper();
                }
            } else {
                v0_14 = ((android.support.v7.internal.widget.ad) v0_8);
            }
            this.i = v0_14;
        }
        return;
    }

    private boolean m()
    {
        return this.b;
    }

    private void n()
    {
        this.removeCallbacks(this.A);
        this.removeCallbacks(this.B);
        if (this.y != null) {
            this.y.a();
        }
        return;
    }

    private void o()
    {
        this.n();
        this.postDelayed(this.A, 600);
        return;
    }

    private void p()
    {
        this.n();
        this.postDelayed(this.B, 600);
        return;
    }

    private void q()
    {
        this.n();
        this.A.run();
        return;
    }

    private void r()
    {
        this.n();
        this.B.run();
        return;
    }

    public final void a(int p2)
    {
        this.l();
        switch (p2) {
            case 2:
                this.i.g();
                break;
            case 5:
                this.i.h();
                break;
            case 109:
                this.setOverlayMode(1);
                break;
        }
        return;
    }

    public final void a(android.util.SparseArray p2)
    {
        this.l();
        this.i.a(p2);
        return;
    }

    public final void a(android.view.Menu p2, android.support.v7.internal.view.menu.y p3)
    {
        this.l();
        this.i.a(p2, p3);
        return;
    }

    public final boolean a()
    {
        this.l();
        return this.i.i();
    }

    public final void b(android.util.SparseArray p2)
    {
        this.l();
        this.i.b(p2);
        return;
    }

    public final boolean b()
    {
        this.l();
        return this.i.j();
    }

    public final boolean c()
    {
        this.l();
        return this.i.k();
    }

    protected boolean checkLayoutParams(android.view.ViewGroup$LayoutParams p2)
    {
        return (p2 instanceof android.support.v7.internal.widget.k);
    }

    public final boolean d()
    {
        this.l();
        return this.i.l();
    }

    public void draw(android.graphics.Canvas p6)
    {
        super.draw(p6);
        if ((this.j != null) && (!this.k)) {
            android.graphics.drawable.Drawable v0_4;
            if (this.h.getVisibility() != 0) {
                v0_4 = 0;
            } else {
                v0_4 = ((int) ((((float) this.h.getBottom()) + android.support.v4.view.cx.n(this.h)) + 1056964608));
            }
            this.j.setBounds(0, v0_4, this.getWidth(), (this.j.getIntrinsicHeight() + v0_4));
            this.j.draw(p6);
        }
        return;
    }

    public final boolean e()
    {
        this.l();
        return this.i.m();
    }

    public final boolean f()
    {
        this.l();
        return this.i.n();
    }

    protected boolean fitSystemWindows(android.graphics.Rect p5)
    {
        this.l();
        android.support.v4.view.cx.s(this);
        int v0_1 = android.support.v7.internal.widget.ActionBarOverlayLayout.a(this.h, p5, 0);
        this.s.set(p5);
        android.support.v7.internal.widget.bd.a(this, this.s, this.p);
        if (!this.q.equals(this.p)) {
            this.q.set(this.p);
            v0_1 = 1;
        }
        if (v0_1 != 0) {
            this.requestLayout();
        }
        return 1;
    }

    public final boolean g()
    {
        this.l();
        return this.i.o();
    }

    protected synthetic android.view.ViewGroup$LayoutParams generateDefaultLayoutParams()
    {
        return new android.support.v7.internal.widget.k();
    }

    public synthetic android.view.ViewGroup$LayoutParams generateLayoutParams(android.util.AttributeSet p3)
    {
        return new android.support.v7.internal.widget.k(this.getContext(), p3);
    }

    protected android.view.ViewGroup$LayoutParams generateLayoutParams(android.view.ViewGroup$LayoutParams p2)
    {
        return new android.support.v7.internal.widget.k(p2);
    }

    public int getActionBarHideOffset()
    {
        int v0_1;
        if (this.h == null) {
            v0_1 = 0;
        } else {
            v0_1 = (- ((int) android.support.v4.view.cx.n(this.h)));
        }
        return v0_1;
    }

    public int getNestedScrollAxes()
    {
        return this.C.a;
    }

    public CharSequence getTitle()
    {
        this.l();
        return this.i.e();
    }

    public final void h()
    {
        this.l();
        this.i.p();
        return;
    }

    public final void i()
    {
        this.l();
        this.i.q();
        return;
    }

    protected void onConfigurationChanged(android.content.res.Configuration p3)
    {
        if (android.os.Build$VERSION.SDK_INT >= 8) {
            super.onConfigurationChanged(p3);
        }
        this.a(this.getContext());
        android.support.v4.view.cx.t(this);
        return;
    }

    protected void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        this.n();
        return;
    }

    protected void onLayout(boolean p10, int p11, int p12, int p13, int p14)
    {
        int v2 = this.getChildCount();
        int v3 = this.getPaddingLeft();
        this.getPaddingRight();
        int v4 = this.getPaddingTop();
        this.getPaddingBottom();
        int v1 = 0;
        while (v1 < v2) {
            android.view.View v5 = this.getChildAt(v1);
            if (v5.getVisibility() != 8) {
                int v0_3 = ((android.support.v7.internal.widget.k) v5.getLayoutParams());
                int v8_1 = (v0_3.leftMargin + v3);
                int v0_5 = (v0_3.topMargin + v4);
                v5.layout(v8_1, v0_5, (v5.getMeasuredWidth() + v8_1), (v5.getMeasuredHeight() + v0_5));
            }
            v1++;
        }
        return;
    }

    protected void onMeasure(int p11, int p12)
    {
        int v1_8;
        this.l();
        this.measureChildWithMargins(this.h, p11, 0, p12, 0);
        int v0_3 = ((android.support.v7.internal.widget.k) this.h.getLayoutParams());
        int v7 = Math.max(0, ((this.h.getMeasuredWidth() + v0_3.leftMargin) + v0_3.rightMargin));
        int v8 = Math.max(0, (v0_3.bottomMargin + (this.h.getMeasuredHeight() + v0_3.topMargin)));
        int v9 = android.support.v7.internal.widget.bd.a(0, android.support.v4.view.cx.j(this.h));
        if ((android.support.v4.view.cx.s(this) & 256) == 0) {
            v1_8 = 0;
        } else {
            v1_8 = 1;
        }
        int v0_12;
        if (v1_8 == 0) {
            if (this.h.getVisibility() == 8) {
                v0_12 = 0;
            } else {
                v0_12 = this.h.getMeasuredHeight();
            }
        } else {
            v0_12 = this.e;
            if ((this.l) && (this.h.getTabContainer() != null)) {
                v0_12 += this.e;
            }
        }
        this.r.set(this.p);
        this.t.set(this.s);
        if ((this.a) || (v1_8 != 0)) {
            int v1_9 = this.t;
            v1_9.top = (v0_12 + v1_9.top);
            int v0_15 = this.t;
            v0_15.bottom = (v0_15.bottom + 0);
        } else {
            int v1_12 = this.r;
            v1_12.top = (v0_12 + v1_12.top);
            int v0_17 = this.r;
            v0_17.bottom = (v0_17.bottom + 0);
        }
        android.support.v7.internal.widget.ActionBarOverlayLayout.a(this.g, this.r, 1);
        if (!this.u.equals(this.t)) {
            this.u.set(this.t);
            this.g.a(this.t);
        }
        this.measureChildWithMargins(this.g, p11, 0, p12, 0);
        int v0_26 = ((android.support.v7.internal.widget.k) this.g.getLayoutParams());
        int v1_24 = Math.max(v7, ((this.g.getMeasuredWidth() + v0_26.leftMargin) + v0_26.rightMargin));
        int v0_29 = Math.max(v8, (v0_26.bottomMargin + (this.g.getMeasuredHeight() + v0_26.topMargin)));
        int v2_22 = android.support.v7.internal.widget.bd.a(v9, android.support.v4.view.cx.j(this.g));
        this.setMeasuredDimension(android.support.v4.view.cx.a(Math.max((v1_24 + (this.getPaddingLeft() + this.getPaddingRight())), this.getSuggestedMinimumWidth()), p11, v2_22), android.support.v4.view.cx.a(Math.max((v0_29 + (this.getPaddingTop() + this.getPaddingBottom())), this.getSuggestedMinimumHeight()), p12, (v2_22 << 16)));
        return;
    }

    public boolean onNestedFling(android.view.View p11, float p12, float p13, boolean p14)
    {
        int v1 = 0;
        if ((this.b) && (p14)) {
            this.x.a(0, 0, 0, ((int) p13), 0, 0, -2147483648, 2147483647);
            if (this.x.d() > this.h.getHeight()) {
                v1 = 1;
            }
            if (v1 == 0) {
                this.n();
                this.A.run();
            } else {
                this.n();
                this.B.run();
            }
            this.m = 1;
            v1 = 1;
        }
        return v1;
    }

    public boolean onNestedPreFling(android.view.View p2, float p3, float p4)
    {
        return 0;
    }

    public void onNestedPreScroll(android.view.View p1, int p2, int p3, int[] p4)
    {
        return;
    }

    public void onNestedScroll(android.view.View p2, int p3, int p4, int p5, int p6)
    {
        this.n = (this.n + p4);
        this.setActionBarHideOffset(this.n);
        return;
    }

    public void onNestedScrollAccepted(android.view.View p2, android.view.View p3, int p4)
    {
        this.C.a = p4;
        this.n = this.getActionBarHideOffset();
        this.n();
        if (this.v != null) {
            this.v.C();
        }
        return;
    }

    public boolean onStartNestedScroll(android.view.View p2, android.view.View p3, int p4)
    {
        if (((p4 & 2) != 0) && (this.h.getVisibility() == 0)) {
            boolean v0_3 = this.b;
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public void onStopNestedScroll(android.view.View p5)
    {
        if ((this.b) && (!this.m)) {
            if (this.n > this.h.getHeight()) {
                this.n();
                this.postDelayed(this.B, 600);
            } else {
                this.n();
                this.postDelayed(this.A, 600);
            }
        }
        return;
    }

    public void onWindowSystemUiVisibilityChanged(int p7)
    {
        int v1 = 1;
        if (android.os.Build$VERSION.SDK_INT >= 16) {
            super.onWindowSystemUiVisibilityChanged(p7);
        }
        int v3_1;
        this.l();
        int v4 = (this.o ^ p7);
        this.o = p7;
        if ((p7 & 4) != 0) {
            v3_1 = 0;
        } else {
            v3_1 = 1;
        }
        android.support.v7.internal.widget.j v0_4;
        if ((p7 & 256) == 0) {
            v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        if (this.v != null) {
            if (v0_4 != null) {
                v1 = 0;
            }
            this.v.i(v1);
            if ((v3_1 == 0) && (v0_4 != null)) {
                this.v.B();
            } else {
                this.v.A();
            }
        }
        if (((v4 & 256) != 0) && (this.v != null)) {
            android.support.v4.view.cx.t(this);
        }
        return;
    }

    protected void onWindowVisibilityChanged(int p2)
    {
        super.onWindowVisibilityChanged(p2);
        this.f = p2;
        if (this.v != null) {
            this.v.n(p2);
        }
        return;
    }

    public void setActionBarHideOffset(int p3)
    {
        this.n();
        android.support.v4.view.cx.b(this.h, ((float) (- Math.max(0, Math.min(p3, this.h.getHeight())))));
        return;
    }

    public void setActionBarVisibilityCallback(android.support.v7.internal.widget.j p3)
    {
        this.v = p3;
        if (this.getWindowToken() != null) {
            this.v.n(this.f);
            if (this.o != 0) {
                this.onWindowSystemUiVisibilityChanged(this.o);
                android.support.v4.view.cx.t(this);
            }
        }
        return;
    }

    public void setHasNonEmbeddedTabs(boolean p1)
    {
        this.l = p1;
        return;
    }

    public void setHideOnContentScrollEnabled(boolean p2)
    {
        if (p2 != this.b) {
            this.b = p2;
            if (!p2) {
                this.n();
                this.setActionBarHideOffset(0);
            }
        }
        return;
    }

    public void setIcon(int p2)
    {
        this.l();
        this.i.a(p2);
        return;
    }

    public void setIcon(android.graphics.drawable.Drawable p2)
    {
        this.l();
        this.i.a(p2);
        return;
    }

    public void setLogo(int p2)
    {
        this.l();
        this.i.b(p2);
        return;
    }

    public void setOverlayMode(boolean p3)
    {
        int v0_3;
        this.a = p3;
        if ((!p3) || (this.getContext().getApplicationInfo().targetSdkVersion >= 19)) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        this.k = v0_3;
        return;
    }

    public void setShowingForActionMode(boolean p1)
    {
        return;
    }

    public void setUiOptions(int p1)
    {
        return;
    }

    public void setWindowCallback(android.view.Window$Callback p2)
    {
        this.l();
        this.i.a(p2);
        return;
    }

    public void setWindowTitle(CharSequence p2)
    {
        this.l();
        this.i.a(p2);
        return;
    }

    public boolean shouldDelayChildPressedState()
    {
        return 0;
    }
}
