package android.support.v7.app;
public final class ag {
    public final android.support.v7.app.x a;
    private int b;

    public ag(android.content.Context p2)
    {
        this(p2, android.support.v7.app.af.a(p2, 0));
        return;
    }

    private ag(android.content.Context p4, int p5)
    {
        this.a = new android.support.v7.app.x(new android.view.ContextThemeWrapper(p4, android.support.v7.app.af.a(p4, p5)));
        this.b = p5;
        return;
    }

    private android.support.v7.app.ag a(int p3)
    {
        this.a.f = this.a.a.getText(p3);
        return this;
    }

    private android.support.v7.app.ag a(int p3, int p4, android.content.DialogInterface$OnClickListener p5)
    {
        this.a.s = this.a.a.getResources().getTextArray(p3);
        this.a.u = p5;
        this.a.F = p4;
        this.a.E = 1;
        return this;
    }

    private android.support.v7.app.ag a(int p3, android.content.DialogInterface$OnClickListener p4)
    {
        this.a.i = this.a.a.getText(p3);
        this.a.j = p4;
        return this;
    }

    private android.support.v7.app.ag a(int p3, boolean[] p4, android.content.DialogInterface$OnMultiChoiceClickListener p5)
    {
        this.a.s = this.a.a.getResources().getTextArray(p3);
        this.a.G = p5;
        this.a.C = p4;
        this.a.D = 1;
        return this;
    }

    private android.support.v7.app.ag a(android.content.DialogInterface$OnCancelListener p2)
    {
        this.a.p = p2;
        return this;
    }

    private android.support.v7.app.ag a(android.content.DialogInterface$OnDismissListener p2)
    {
        this.a.q = p2;
        return this;
    }

    private android.support.v7.app.ag a(android.content.DialogInterface$OnKeyListener p2)
    {
        this.a.r = p2;
        return this;
    }

    private android.support.v7.app.ag a(android.database.Cursor p3, int p4, String p5, android.content.DialogInterface$OnClickListener p6)
    {
        this.a.H = p3;
        this.a.u = p6;
        this.a.F = p4;
        this.a.I = p5;
        this.a.E = 1;
        return this;
    }

    private android.support.v7.app.ag a(android.database.Cursor p2, android.content.DialogInterface$OnClickListener p3, String p4)
    {
        this.a.H = p2;
        this.a.I = p4;
        this.a.u = p3;
        return this;
    }

    private android.support.v7.app.ag a(android.database.Cursor p3, String p4, String p5, android.content.DialogInterface$OnMultiChoiceClickListener p6)
    {
        this.a.H = p3;
        this.a.G = p6;
        this.a.J = p4;
        this.a.I = p5;
        this.a.D = 1;
        return this;
    }

    private android.support.v7.app.ag a(android.graphics.drawable.Drawable p2)
    {
        this.a.d = p2;
        return this;
    }

    private android.support.v7.app.ag a(android.view.View p2)
    {
        this.a.g = p2;
        return this;
    }

    private android.support.v7.app.ag a(android.view.View p3, int p4, int p5, int p6, int p7)
    {
        this.a.w = p3;
        this.a.v = 0;
        this.a.B = 1;
        this.a.x = p4;
        this.a.y = p5;
        this.a.z = p6;
        this.a.A = p7;
        return this;
    }

    private android.support.v7.app.ag a(android.widget.AdapterView$OnItemSelectedListener p2)
    {
        this.a.L = p2;
        return this;
    }

    private android.support.v7.app.ag a(android.widget.ListAdapter p3, int p4, android.content.DialogInterface$OnClickListener p5)
    {
        this.a.t = p3;
        this.a.u = p5;
        this.a.F = p4;
        this.a.E = 1;
        return this;
    }

    private android.support.v7.app.ag a(android.widget.ListAdapter p2, android.content.DialogInterface$OnClickListener p3)
    {
        this.a.t = p2;
        this.a.u = p3;
        return this;
    }

    private android.support.v7.app.ag a(CharSequence p2)
    {
        this.a.f = p2;
        return this;
    }

    private android.support.v7.app.ag a(CharSequence p2, android.content.DialogInterface$OnClickListener p3)
    {
        this.a.i = p2;
        this.a.j = p3;
        return this;
    }

    private android.support.v7.app.ag a(boolean p2)
    {
        this.a.o = p2;
        return this;
    }

    private android.support.v7.app.ag a(CharSequence[] p3, int p4, android.content.DialogInterface$OnClickListener p5)
    {
        this.a.s = p3;
        this.a.u = p5;
        this.a.F = p4;
        this.a.E = 1;
        return this;
    }

    private android.support.v7.app.ag a(CharSequence[] p2, android.content.DialogInterface$OnClickListener p3)
    {
        this.a.s = p2;
        this.a.u = p3;
        return this;
    }

    private android.support.v7.app.ag a(CharSequence[] p3, boolean[] p4, android.content.DialogInterface$OnMultiChoiceClickListener p5)
    {
        this.a.s = p3;
        this.a.G = p5;
        this.a.C = p4;
        this.a.D = 1;
        return this;
    }

    private android.content.Context b()
    {
        return this.a.a;
    }

    private android.support.v7.app.ag b(int p3)
    {
        this.a.h = this.a.a.getText(p3);
        return this;
    }

    private android.support.v7.app.ag b(int p3, android.content.DialogInterface$OnClickListener p4)
    {
        this.a.k = this.a.a.getText(p3);
        this.a.l = p4;
        return this;
    }

    private android.support.v7.app.ag b(android.view.View p3)
    {
        this.a.w = p3;
        this.a.v = 0;
        this.a.B = 0;
        return this;
    }

    private android.support.v7.app.ag b(CharSequence p2)
    {
        this.a.h = p2;
        return this;
    }

    private android.support.v7.app.ag b(CharSequence p2, android.content.DialogInterface$OnClickListener p3)
    {
        this.a.k = p2;
        this.a.l = p3;
        return this;
    }

    private android.support.v7.app.ag b(boolean p2)
    {
        this.a.K = p2;
        return this;
    }

    private android.support.v7.app.af c()
    {
        android.support.v7.app.af v0 = this.a();
        v0.show();
        return v0;
    }

    private android.support.v7.app.ag c(int p2)
    {
        this.a.c = p2;
        return this;
    }

    private android.support.v7.app.ag c(int p3, android.content.DialogInterface$OnClickListener p4)
    {
        this.a.m = this.a.a.getText(p3);
        this.a.n = p4;
        return this;
    }

    private android.support.v7.app.ag c(CharSequence p2, android.content.DialogInterface$OnClickListener p3)
    {
        this.a.m = p2;
        this.a.n = p3;
        return this;
    }

    private android.support.v7.app.ag c(boolean p2)
    {
        this.a.N = p2;
        return this;
    }

    private android.support.v7.app.ag d(int p4)
    {
        int v0_1 = new android.util.TypedValue();
        this.a.a.getTheme().resolveAttribute(p4, v0_1, 1);
        this.a.c = v0_1.resourceId;
        return this;
    }

    private android.support.v7.app.ag d(int p3, android.content.DialogInterface$OnClickListener p4)
    {
        this.a.s = this.a.a.getResources().getTextArray(p3);
        this.a.u = p4;
        return this;
    }

    private android.support.v7.app.ag e(int p3)
    {
        this.a.w = 0;
        this.a.v = p3;
        this.a.B = 0;
        return this;
    }

    public final android.support.v7.app.af a()
    {
        android.support.v7.app.af v19 = new android.support.v7.app.af;
        v19(this.a.a, this.b, 0);
        int v2_1 = this.a;
        android.support.v7.app.v v12 = android.support.v7.app.af.a(v19);
        if (v2_1.g == null) {
            if (v2_1.f != null) {
                v12.a(v2_1.f);
            }
            if (v2_1.d != null) {
                v12.a(v2_1.d);
            }
            if (v2_1.c != 0) {
                v12.a(v2_1.c);
            }
            if (v2_1.e != 0) {
                int v1_10 = v2_1.e;
                android.content.Context v3_2 = new android.util.TypedValue();
                v12.a.getTheme().resolveAttribute(v1_10, v3_2, 1);
                v12.a(v3_2.resourceId);
            }
        } else {
            v12.C = v2_1.g;
        }
        if (v2_1.h != null) {
            v12.b(v2_1.h);
        }
        if (v2_1.i != null) {
            v12.a(-1, v2_1.i, v2_1.j, 0);
        }
        if (v2_1.k != null) {
            v12.a(-2, v2_1.k, v2_1.l, 0);
        }
        if (v2_1.m != null) {
            v12.a(-3, v2_1.m, v2_1.n, 0);
        }
        if ((v2_1.s != null) || ((v2_1.H != null) || (v2_1.t != null))) {
            int v1_32;
            android.widget.ListView v6_1 = ((android.widget.ListView) v2_1.b.inflate(v12.H, 0));
            if (!v2_1.D) {
                int v15_0;
                if (!v2_1.E) {
                    v15_0 = v12.K;
                } else {
                    v15_0 = v12.J;
                }
                if (v2_1.H != null) {
                    android.content.Context v14_0 = v2_1.a;
                    android.database.Cursor v16_0 = v2_1.H;
                    android.support.v7.app.af v0_5 = new String[1];
                    String[] v17_0 = v0_5;
                    v17_0[0] = v2_1.I;
                    android.support.v7.app.af v0_6 = new int[1];
                    int[] v18 = v0_6;
                    v18[0] = 16908308;
                    v1_32 = new android.widget.SimpleCursorAdapter(v14_0, v15_0, v16_0, v17_0, v18);
                } else {
                    if (v2_1.t == null) {
                        v1_32 = new android.support.v7.app.ae(v2_1.a, v15_0, v2_1.s);
                    } else {
                        v1_32 = v2_1.t;
                    }
                }
            } else {
                if (v2_1.H != null) {
                    v1_32 = new android.support.v7.app.z(v2_1, v2_1.a, v2_1.H, v6_1, v12);
                } else {
                    v1_32 = new android.support.v7.app.y(v2_1, v2_1.a, v12.I, v2_1.s, v6_1);
                }
            }
            v12.D = v1_32;
            v12.E = v2_1.F;
            if (v2_1.u == null) {
                if (v2_1.G != null) {
                    v6_1.setOnItemClickListener(new android.support.v7.app.ab(v2_1, v6_1, v12));
                }
            } else {
                v6_1.setOnItemClickListener(new android.support.v7.app.aa(v2_1, v12));
            }
            if (v2_1.L != null) {
                v6_1.setOnItemSelectedListener(v2_1.L);
            }
            if (!v2_1.E) {
                if (v2_1.D) {
                    v6_1.setChoiceMode(2);
                }
            } else {
                v6_1.setChoiceMode(1);
            }
            v12.f = v6_1;
        }
        if (v2_1.w == null) {
            if (v2_1.v != 0) {
                int v1_52 = v2_1.v;
                v12.g = 0;
                v12.h = v1_52;
                v12.m = 0;
            }
        } else {
            if (!v2_1.B) {
                v12.b(v2_1.w);
            } else {
                v12.a(v2_1.w, v2_1.x, v2_1.y, v2_1.z, v2_1.A);
            }
        }
        v19.setCancelable(this.a.o);
        if (this.a.o) {
            v19.setCanceledOnTouchOutside(1);
        }
        v19.setOnCancelListener(this.a.p);
        v19.setOnDismissListener(this.a.q);
        if (this.a.r != null) {
            v19.setOnKeyListener(this.a.r);
        }
        return v19;
    }
}
