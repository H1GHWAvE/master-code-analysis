package android.support.v7.app;
final class bd implements android.os.Parcelable$Creator {

    bd()
    {
        return;
    }

    private static android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState$SavedState a(android.os.Parcel p1)
    {
        return android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState$SavedState.a(p1);
    }

    private static android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState$SavedState[] a(int p1)
    {
        android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState$SavedState[] v0 = new android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState$SavedState[p1];
        return v0;
    }

    public final synthetic Object createFromParcel(android.os.Parcel p2)
    {
        return android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState$SavedState.a(p2);
    }

    public final bridge synthetic Object[] newArray(int p2)
    {
        android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState$SavedState[] v0 = new android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState$SavedState[p2];
        return v0;
    }
}
