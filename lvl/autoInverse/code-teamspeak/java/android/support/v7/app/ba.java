package android.support.v7.app;
final class ba implements android.support.v7.c.b {
    final synthetic android.support.v7.app.AppCompatDelegateImplV7 a;
    private android.support.v7.c.b b;

    public ba(android.support.v7.app.AppCompatDelegateImplV7 p1, android.support.v7.c.b p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    public final void a(android.support.v7.c.a p4)
    {
        this.b.a(p4);
        if (this.a.v != null) {
            this.a.f.getDecorView().removeCallbacks(this.a.w);
        }
        if (this.a.u != null) {
            this.a.n();
            this.a.x = android.support.v4.view.cx.p(this.a.u).a(0);
            this.a.x.a(new android.support.v7.app.bb(this));
        }
        this.a.t = 0;
        return;
    }

    public final boolean a(android.support.v7.c.a p2, android.view.Menu p3)
    {
        return this.b.a(p2, p3);
    }

    public final boolean a(android.support.v7.c.a p2, android.view.MenuItem p3)
    {
        return this.b.a(p2, p3);
    }

    public final boolean b(android.support.v7.c.a p2, android.view.Menu p3)
    {
        return this.b.b(p2, p3);
    }
}
