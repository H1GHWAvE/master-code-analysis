package android.support.v7.app;
public abstract class aj {
    static final String a = "AppCompatDelegate";
    public static final int b = 108;
    public static final int c = 109;
    public static final int d = 10;

    aj()
    {
        return;
    }

    private static android.support.v7.app.aj a(android.app.Activity p1, android.support.v7.app.ai p2)
    {
        return android.support.v7.app.aj.a(p1, p1.getWindow(), p2);
    }

    private static android.support.v7.app.aj a(android.app.Dialog p2, android.support.v7.app.ai p3)
    {
        return android.support.v7.app.aj.a(p2.getContext(), p2.getWindow(), p3);
    }

    static android.support.v7.app.aj a(android.content.Context p2, android.view.Window p3, android.support.v7.app.ai p4)
    {
        android.support.v7.app.AppCompatDelegateImplV7 v0_2;
        android.support.v7.app.AppCompatDelegateImplV7 v0_0 = android.os.Build$VERSION.SDK_INT;
        if (v0_0 < 23) {
            if (v0_0 < 14) {
                if (v0_0 < 11) {
                    v0_2 = new android.support.v7.app.AppCompatDelegateImplV7(p2, p3, p4);
                } else {
                    v0_2 = new android.support.v7.app.ao(p2, p3, p4);
                }
            } else {
                v0_2 = new android.support.v7.app.ap(p2, p3, p4);
            }
        } else {
            v0_2 = new android.support.v7.app.ar(p2, p3, p4);
        }
        return v0_2;
    }

    public abstract android.support.v7.app.a a();

    public abstract android.support.v7.c.a a();

    public abstract void a();

    public abstract void a();

    public abstract void a();

    public abstract void a();

    public abstract void a();

    public abstract void a();

    public abstract void a();

    public abstract android.view.MenuInflater b();

    public abstract android.view.View b();

    public abstract void b();

    public abstract boolean b();

    public abstract void c();

    public abstract boolean c();

    public abstract void d();

    public abstract void e();

    public abstract void f();

    public abstract void g();

    public abstract void h();

    public abstract android.support.v7.app.l i();

    public abstract void j();

    public abstract boolean k();
}
