package android.support.v7.app;
final class AppCompatDelegateImplV7$PanelFeatureState {
    int a;
    int b;
    int c;
    int d;
    int e;
    int f;
    android.view.ViewGroup g;
    android.view.View h;
    android.view.View i;
    android.support.v7.internal.view.menu.i j;
    android.support.v7.internal.view.menu.g k;
    android.content.Context l;
    boolean m;
    boolean n;
    boolean o;
    public boolean p;
    boolean q;
    boolean r;
    boolean s;
    android.os.Bundle t;
    android.os.Bundle u;

    AppCompatDelegateImplV7$PanelFeatureState(int p2)
    {
        this.a = p2;
        this.q = 0;
        return;
    }

    private android.support.v7.internal.view.menu.z a(android.support.v7.internal.view.menu.y p4)
    {
        android.support.v7.internal.view.menu.z v0_7;
        if (this.j != null) {
            if (this.k == null) {
                this.k = new android.support.v7.internal.view.menu.g(this.l, android.support.v7.a.k.abc_list_menu_item_layout);
                this.k.g = p4;
                this.j.a(this.k);
            }
            v0_7 = this.k.a(this.g);
        } else {
            v0_7 = 0;
        }
        return v0_7;
    }

    private void a(android.content.Context p6)
    {
        android.content.res.TypedArray v0_1 = new android.util.TypedValue();
        int v1_1 = p6.getResources().newTheme();
        v1_1.setTo(p6.getTheme());
        v1_1.resolveAttribute(android.support.v7.a.d.actionBarPopupTheme, v0_1, 1);
        if (v0_1.resourceId != 0) {
            v1_1.applyStyle(v0_1.resourceId, 1);
        }
        v1_1.resolveAttribute(android.support.v7.a.d.panelMenuListTheme, v0_1, 1);
        if (v0_1.resourceId == 0) {
            v1_1.applyStyle(android.support.v7.a.m.Theme_AppCompat_CompactMenu, 1);
        } else {
            v1_1.applyStyle(v0_1.resourceId, 1);
        }
        android.content.res.TypedArray v0_5 = new android.support.v7.internal.view.b(p6, 0);
        v0_5.getTheme().setTo(v1_1);
        this.l = v0_5;
        android.content.res.TypedArray v0_6 = v0_5.obtainStyledAttributes(android.support.v7.a.n.Theme);
        this.b = v0_6.getResourceId(android.support.v7.a.n.Theme_panelBackground, 0);
        this.f = v0_6.getResourceId(android.support.v7.a.n.Theme_android_windowAnimationStyle, 0);
        v0_6.recycle();
        return;
    }

    private void a(android.os.Parcelable p3)
    {
        this.a = ((android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState$SavedState) p3).a;
        this.s = ((android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState$SavedState) p3).b;
        this.t = ((android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState$SavedState) p3).c;
        this.h = 0;
        this.g = 0;
        return;
    }

    private boolean a()
    {
        int v0 = 0;
        if (this.h != null) {
            if (this.i == null) {
                if (this.k.d().getCount() > 0) {
                    v0 = 1;
                }
            } else {
                v0 = 1;
            }
        }
        return v0;
    }

    private void b()
    {
        if (this.j != null) {
            this.j.b(this.k);
        }
        this.k = 0;
        return;
    }

    private android.os.Parcelable c()
    {
        android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState$SavedState v0_1 = new android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState$SavedState(0);
        v0_1.a = this.a;
        v0_1.b = this.o;
        if (this.j != null) {
            v0_1.c = new android.os.Bundle();
            this.j.a(v0_1.c);
        }
        return v0_1;
    }

    private void d()
    {
        if ((this.j != null) && (this.t != null)) {
            this.j.b(this.t);
            this.t = 0;
        }
        return;
    }

    final void a(android.support.v7.internal.view.menu.i p3)
    {
        if (p3 != this.j) {
            if (this.j != null) {
                this.j.b(this.k);
            }
            this.j = p3;
            if ((p3 != null) && (this.k != null)) {
                p3.a(this.k);
            }
        }
        return;
    }
}
