package android.support.v7.app;
 class AppCompatDelegateImplV7 extends android.support.v7.app.ak implements android.support.v4.view.as, android.support.v7.internal.view.menu.j {
    private android.support.v7.app.az A;
    private android.support.v7.app.be B;
    private boolean C;
    private android.view.ViewGroup D;
    private android.view.ViewGroup E;
    private android.widget.TextView F;
    private android.view.View G;
    private boolean H;
    private boolean I;
    private boolean J;
    private android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState[] K;
    private android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState L;
    private final Runnable M;
    private boolean N;
    private android.graphics.Rect O;
    private android.graphics.Rect P;
    private android.support.v7.internal.a.a Q;
    private android.support.v7.internal.widget.ac s;
    android.support.v7.c.a t;
    android.support.v7.internal.widget.ActionBarContextView u;
    android.widget.PopupWindow v;
    Runnable w;
    android.support.v4.view.fk x;
    boolean y;
    int z;

    AppCompatDelegateImplV7(android.content.Context p2, android.view.Window p3, android.support.v7.app.ai p4)
    {
        this(p2, p3, p4);
        this.x = 0;
        this.M = new android.support.v7.app.at(this);
        return;
    }

    private static synthetic int a(android.support.v7.app.AppCompatDelegateImplV7 p1)
    {
        return p1.z;
    }

    private static synthetic android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState a(android.support.v7.app.AppCompatDelegateImplV7 p1, android.view.Menu p2)
    {
        return p1.a(p2);
    }

    private void a(android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState p11, android.view.KeyEvent p12)
    {
        int v1_0 = -1;
        if ((!p11.o) && (!this.r)) {
            if (p11.a == 0) {
                android.view.View v0_7;
                android.view.View v4_0 = this.e;
                if ((v4_0.getResources().getConfiguration().screenLayout & 15) != 4) {
                    v0_7 = 0;
                } else {
                    v0_7 = 1;
                }
                android.view.View v4_3;
                if (v4_0.getApplicationInfo().targetSdkVersion < 11) {
                    v4_3 = 0;
                } else {
                    v4_3 = 1;
                }
                if ((v0_7 != null) && (v4_3 != null)) {
                    return;
                }
            }
            android.view.View v0_9 = this.f.getCallback();
            if ((v0_9 == null) || (v0_9.onMenuOpened(p11.a, p11.j))) {
                android.view.WindowManager v8_1 = ((android.view.WindowManager) this.e.getSystemService("window"));
                if ((v8_1 != null) && (this.b(p11, p12))) {
                    if ((p11.g != null) && (!p11.q)) {
                        if (p11.i != null) {
                            android.view.View v0_18 = p11.i.getLayoutParams();
                            if ((v0_18 != null) && (v0_18.width == -1)) {
                                p11.n = 0;
                                android.view.View v0_65 = new android.view.WindowManager$LayoutParams(v1_0, -2, p11.d, p11.e, 1002, 8519680, -3);
                                v0_65.gravity = p11.c;
                                v0_65.windowAnimations = p11.f;
                                v8_1.addView(p11.g, v0_65);
                                p11.o = 1;
                                return;
                            }
                        }
                        v1_0 = -2;
                    } else {
                        if (p11.g != null) {
                            if ((p11.q) && (p11.g.getChildCount() > 0)) {
                                p11.g.removeAllViews();
                            }
                        } else {
                            android.view.View v0_25 = this.m();
                            int v1_2 = new android.util.TypedValue();
                            android.view.View v4_7 = v0_25.getResources().newTheme();
                            v4_7.setTo(v0_25.getTheme());
                            v4_7.resolveAttribute(android.support.v7.a.d.actionBarPopupTheme, v1_2, 1);
                            if (v1_2.resourceId != 0) {
                                v4_7.applyStyle(v1_2.resourceId, 1);
                            }
                            v4_7.resolveAttribute(android.support.v7.a.d.panelMenuListTheme, v1_2, 1);
                            if (v1_2.resourceId == 0) {
                                v4_7.applyStyle(android.support.v7.a.m.Theme_AppCompat_CompactMenu, 1);
                            } else {
                                v4_7.applyStyle(v1_2.resourceId, 1);
                            }
                            int v1_6 = new android.support.v7.internal.view.b(v0_25, 0);
                            v1_6.getTheme().setTo(v4_7);
                            p11.l = v1_6;
                            android.view.View v0_28 = v1_6.obtainStyledAttributes(android.support.v7.a.n.Theme);
                            p11.b = v0_28.getResourceId(android.support.v7.a.n.Theme_panelBackground, 0);
                            p11.f = v0_28.getResourceId(android.support.v7.a.n.Theme_android_windowAnimationStyle, 0);
                            v0_28.recycle();
                            p11.g = new android.support.v7.app.bc(this, p11.l);
                            p11.c = 81;
                            if (p11.g == null) {
                                return;
                            }
                        }
                        android.view.View v0_44;
                        if (p11.i == null) {
                            if (p11.j != null) {
                                if (this.B == null) {
                                    this.B = new android.support.v7.app.be(this, 0);
                                }
                                android.view.View v0_41;
                                android.view.View v0_38 = this.B;
                                if (p11.j != null) {
                                    if (p11.k == null) {
                                        p11.k = new android.support.v7.internal.view.menu.g(p11.l, android.support.v7.a.k.abc_list_menu_item_layout);
                                        p11.k.g = v0_38;
                                        p11.j.a(p11.k);
                                    }
                                    v0_41 = p11.k.a(p11.g);
                                } else {
                                    v0_41 = 0;
                                }
                                p11.h = ((android.view.View) v0_41);
                                if (p11.h != null) {
                                    v0_44 = 1;
                                    if (v0_44 == null) {
                                        return;
                                    } else {
                                        android.view.View v0_51;
                                        if (p11.h == null) {
                                            v0_51 = 0;
                                        } else {
                                            if (p11.i == null) {
                                                if (p11.k.d().getCount() <= 0) {
                                                } else {
                                                    v0_51 = 1;
                                                }
                                            } else {
                                                v0_51 = 1;
                                            }
                                        }
                                        if (v0_51 == null) {
                                            return;
                                        } else {
                                            int v1_19;
                                            android.view.View v0_53 = p11.h.getLayoutParams();
                                            if (v0_53 != null) {
                                                v1_19 = v0_53;
                                            } else {
                                                v1_19 = new android.view.ViewGroup$LayoutParams(-2, -2);
                                            }
                                            p11.g.setBackgroundResource(p11.b);
                                            android.view.View v0_58 = p11.h.getParent();
                                            if ((v0_58 != null) && ((v0_58 instanceof android.view.ViewGroup))) {
                                                ((android.view.ViewGroup) v0_58).removeView(p11.h);
                                            }
                                            p11.g.addView(p11.h, v1_19);
                                            if (!p11.h.hasFocus()) {
                                                p11.h.requestFocus();
                                            }
                                            v1_0 = -2;
                                        }
                                    }
                                }
                            }
                            v0_44 = 0;
                        } else {
                            p11.h = p11.i;
                            v0_44 = 1;
                        }
                    }
                }
            } else {
                this.a(p11, 1);
            }
        }
        return;
    }

    static synthetic void a(android.support.v7.app.AppCompatDelegateImplV7 p5, int p6)
    {
        android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState v0_0 = p5.f(p6);
        if (v0_0.j != null) {
            int v1_2 = new android.os.Bundle();
            v0_0.j.c(v1_2);
            if (v1_2.size() > 0) {
                v0_0.u = v1_2;
            }
            v0_0.j.d();
            v0_0.j.clear();
        }
        v0_0.r = 1;
        v0_0.q = 1;
        if (((p6 == 108) || (p6 == 0)) && (p5.s != null)) {
            android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState v0_3 = p5.f(0);
            if (v0_3 != null) {
                v0_3.m = 0;
                p5.b(v0_3, 0);
            }
        }
        return;
    }

    private static synthetic void a(android.support.v7.app.AppCompatDelegateImplV7 p0, int p1, android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState p2, android.view.Menu p3)
    {
        p0.a(p1, p2, p3);
        return;
    }

    private static synthetic void a(android.support.v7.app.AppCompatDelegateImplV7 p0, android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState p1, boolean p2)
    {
        p0.a(p1, p2);
        return;
    }

    private static synthetic void a(android.support.v7.app.AppCompatDelegateImplV7 p0, android.support.v7.internal.view.menu.i p1)
    {
        p0.b(p1);
        return;
    }

    private boolean a(android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState p7)
    {
        int v0_0 = this.m();
        android.content.Context v1_1 = new android.util.TypedValue();
        android.content.res.Resources$Theme v2_1 = v0_0.getResources().newTheme();
        v2_1.setTo(v0_0.getTheme());
        v2_1.resolveAttribute(android.support.v7.a.d.actionBarPopupTheme, v1_1, 1);
        if (v1_1.resourceId != 0) {
            v2_1.applyStyle(v1_1.resourceId, 1);
        }
        v2_1.resolveAttribute(android.support.v7.a.d.panelMenuListTheme, v1_1, 1);
        if (v1_1.resourceId == 0) {
            v2_1.applyStyle(android.support.v7.a.m.Theme_AppCompat_CompactMenu, 1);
        } else {
            v2_1.applyStyle(v1_1.resourceId, 1);
        }
        android.content.Context v1_5 = new android.support.v7.internal.view.b(v0_0, 0);
        v1_5.getTheme().setTo(v2_1);
        p7.l = v1_5;
        int v0_3 = v1_5.obtainStyledAttributes(android.support.v7.a.n.Theme);
        p7.b = v0_3.getResourceId(android.support.v7.a.n.Theme_panelBackground, 0);
        p7.f = v0_3.getResourceId(android.support.v7.a.n.Theme_android_windowAnimationStyle, 0);
        v0_3.recycle();
        p7.g = new android.support.v7.app.bc(this, p7.l);
        p7.c = 81;
        return 1;
    }

    private boolean a(android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState p3, int p4, android.view.KeyEvent p5)
    {
        boolean v0_0 = 0;
        if (((!p5.isSystem()) && ((p3.m) || (this.b(p3, p5)))) && (p3.j != null)) {
            v0_0 = p3.j.performShortcut(p4, p5, 1);
        }
        return v0_0;
    }

    private boolean a(android.view.ViewParent p4)
    {
        boolean v0_5;
        if (p4 != null) {
            android.view.ViewParent v1 = p4;
            while (v1 != null) {
                if ((v1 != this.D) && (((v1 instanceof android.view.View)) && (!android.support.v4.view.cx.D(((android.view.View) v1))))) {
                    v1 = v1.getParent();
                } else {
                    v0_5 = 0;
                }
            }
            v0_5 = 1;
        } else {
            v0_5 = 0;
        }
        return v0_5;
    }

    static synthetic int b(android.support.v7.app.AppCompatDelegateImplV7 p8, int p9)
    {
        int v0_4;
        android.support.v7.internal.widget.ActionBarContextView v3_0 = 1;
        int v2 = 0;
        if ((p8.u == null) || (!(p8.u.getLayoutParams() instanceof android.view.ViewGroup$MarginLayoutParams))) {
            v0_4 = 0;
        } else {
            int v1_3;
            int v0_7 = ((android.view.ViewGroup$MarginLayoutParams) p8.u.getLayoutParams());
            if (!p8.u.isShown()) {
                if (v0_7.topMargin == 0) {
                    v3_0 = 0;
                    v1_3 = 0;
                } else {
                    v0_7.topMargin = 0;
                    v1_3 = 0;
                }
            } else {
                if (p8.O == null) {
                    p8.O = new android.graphics.Rect();
                    p8.P = new android.graphics.Rect();
                }
                int v1_11;
                int v1_9 = p8.O;
                boolean v4_0 = p8.P;
                v1_9.set(0, p9, 0, 0);
                android.support.v7.internal.widget.bd.a(p8.E, v1_9, v4_0);
                if (v4_0.top != 0) {
                    v1_11 = 0;
                } else {
                    v1_11 = p9;
                }
                int v1_12;
                if (v0_7.topMargin == v1_11) {
                    v1_12 = 0;
                } else {
                    v0_7.topMargin = p9;
                    if (p8.G != null) {
                        int v1_15 = p8.G.getLayoutParams();
                        if (v1_15.height != p9) {
                            v1_15.height = p9;
                            p8.G.setLayoutParams(v1_15);
                        }
                        v1_12 = 1;
                    } else {
                        p8.G = new android.view.View(p8.e);
                        p8.G.setBackgroundColor(p8.e.getResources().getColor(android.support.v7.a.f.abc_input_method_navigation_guard));
                        p8.E.addView(p8.G, -1, new android.view.ViewGroup$LayoutParams(-1, p9));
                        v1_12 = 1;
                    }
                }
                if (p8.G == null) {
                    v3_0 = 0;
                }
                if ((!p8.n) && (v3_0 != null)) {
                    p9 = 0;
                }
                v1_3 = v3_0;
                v3_0 = v1_12;
            }
            if (v3_0 != null) {
                p8.u.setLayoutParams(v0_7);
            }
            v0_4 = v1_3;
        }
        if (p8.G != null) {
            if (v0_4 == 0) {
                v2 = 8;
            }
            p8.G.setVisibility(v2);
        }
        return p9;
    }

    private boolean b(int p5, android.view.KeyEvent p6)
    {
        int v0_23;
        switch (p5) {
            case 4:
                int v0_24 = this.f(0);
                if ((v0_24 == 0) || (!v0_24.o)) {
                    int v0_28;
                    if (this.t == null) {
                        int v0_26 = this.a();
                        if ((v0_26 == 0) || (!v0_26.z())) {
                            v0_28 = 0;
                        } else {
                            v0_28 = 1;
                        }
                    } else {
                        this.t.c();
                        v0_28 = 1;
                    }
                    if (v0_28 == 0) {
                        v0_23 = 0;
                    } else {
                        v0_23 = 1;
                    }
                } else {
                    this.a(v0_24, 1);
                    v0_23 = 1;
                }
                break;
            case 82:
                if (this.t == null) {
                    int v0_12;
                    String v3_0 = this.f(0);
                    if ((this.s == null) || ((!this.s.c()) || (android.support.v4.view.du.b(android.view.ViewConfiguration.get(this.e))))) {
                        if ((!v3_0.o) && (!v3_0.n)) {
                            if (!v3_0.m) {
                                v0_12 = 0;
                            } else {
                                int v0_11;
                                if (!v3_0.r) {
                                    v0_11 = 1;
                                } else {
                                    v3_0.m = 0;
                                    v0_11 = this.b(v3_0, p6);
                                }
                                if (v0_11 == 0) {
                                } else {
                                    this.a(v3_0, p6);
                                    v0_12 = 1;
                                }
                            }
                        } else {
                            v0_12 = v3_0.o;
                            this.a(v3_0, 1);
                        }
                    } else {
                        if (this.s.d()) {
                            v0_12 = this.s.g();
                        } else {
                            if ((this.r) || (!this.b(v3_0, p6))) {
                            } else {
                                v0_12 = this.s.f();
                            }
                        }
                    }
                    if (v0_12 != 0) {
                        int v0_21 = ((android.media.AudioManager) this.e.getSystemService("audio"));
                        if (v0_21 == 0) {
                            android.util.Log.w("AppCompatDelegate", "Couldn\'t get audio manager");
                        } else {
                            v0_21.playSoundEffect(0);
                        }
                    }
                }
                v0_23 = 1;
                break;
            default:
        }
        return v0_23;
    }

    private boolean b(android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState p7)
    {
        android.support.v7.internal.view.b v0_7;
        android.content.res.Resources$Theme v1_0 = this.e;
        if (((p7.a != 0) && (p7.a != 108)) || (this.s == null)) {
            v0_7 = v1_0;
        } else {
            int v2_2 = new android.util.TypedValue();
            int v3_0 = v1_0.getTheme();
            v3_0.resolveAttribute(android.support.v7.a.d.actionBarTheme, v2_2, 1);
            android.support.v7.internal.view.b v0_4 = 0;
            if (v2_2.resourceId == 0) {
                v3_0.resolveAttribute(android.support.v7.a.d.actionBarWidgetTheme, v2_2, 1);
            } else {
                v0_4 = v1_0.getResources().newTheme();
                v0_4.setTo(v3_0);
                v0_4.applyStyle(v2_2.resourceId, 1);
                v0_4.resolveAttribute(android.support.v7.a.d.actionBarWidgetTheme, v2_2, 1);
            }
            if (v2_2.resourceId != 0) {
                if (v0_4 == null) {
                    v0_4 = v1_0.getResources().newTheme();
                    v0_4.setTo(v3_0);
                }
                v0_4.applyStyle(v2_2.resourceId, 1);
            }
            int v2_4 = v0_4;
            if (v2_4 == 0) {
            } else {
                v0_7 = new android.support.v7.internal.view.b(v1_0, 0);
                v0_7.getTheme().setTo(v2_4);
            }
        }
        android.content.res.Resources$Theme v1_3 = new android.support.v7.internal.view.menu.i(v0_7);
        v1_3.a(this);
        p7.a(v1_3);
        return 1;
    }

    private boolean b(android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState p11, android.view.KeyEvent p12)
    {
        int v4 = 0;
        if (!this.r) {
            if (!p11.m) {
                if ((this.L != null) && (this.L != p11)) {
                    this.a(this.L, 0);
                }
                android.view.Window$Callback v7 = this.f.getCallback();
                if (v7 != null) {
                    p11.i = v7.onCreatePanelView(p11.a);
                }
                if ((p11.a != 0) && (p11.a != 108)) {
                    int v6 = 0;
                } else {
                    v6 = 1;
                }
                if ((v6 != 0) && (this.s != null)) {
                    this.s.h();
                }
                if ((p11.i == null) && ((v6 == 0) || (!(this.j instanceof android.support.v7.internal.a.e)))) {
                    if ((p11.j == null) || (p11.r)) {
                        if (p11.j == null) {
                            android.support.v7.internal.view.b v0_27;
                            android.content.res.Resources$Theme v2_0 = this.e;
                            if (((p11.a != 0) && (p11.a != 108)) || (this.s == null)) {
                                v0_27 = v2_0;
                            } else {
                                android.support.v7.internal.view.b v0_24;
                                int v5_2 = new android.util.TypedValue();
                                android.content.res.Resources$Theme v8 = v2_0.getTheme();
                                v8.resolveAttribute(android.support.v7.a.d.actionBarTheme, v5_2, 1);
                                if (v5_2.resourceId == 0) {
                                    v8.resolveAttribute(android.support.v7.a.d.actionBarWidgetTheme, v5_2, 1);
                                    v0_24 = 0;
                                } else {
                                    v0_24 = v2_0.getResources().newTheme();
                                    v0_24.setTo(v8);
                                    v0_24.applyStyle(v5_2.resourceId, 1);
                                    v0_24.resolveAttribute(android.support.v7.a.d.actionBarWidgetTheme, v5_2, 1);
                                }
                                if (v5_2.resourceId != 0) {
                                    if (v0_24 == null) {
                                        v0_24 = v2_0.getResources().newTheme();
                                        v0_24.setTo(v8);
                                    }
                                    v0_24.applyStyle(v5_2.resourceId, 1);
                                }
                                int v5_4 = v0_24;
                                if (v5_4 == 0) {
                                } else {
                                    v0_27 = new android.support.v7.internal.view.b(v2_0, 0);
                                    v0_27.getTheme().setTo(v5_4);
                                }
                            }
                            android.content.res.Resources$Theme v2_3 = new android.support.v7.internal.view.menu.i(v0_27);
                            v2_3.a(this);
                            p11.a(v2_3);
                            if (p11.j == null) {
                                return v4;
                            }
                        }
                        if ((v6 != 0) && (this.s != null)) {
                            if (this.A == null) {
                                this.A = new android.support.v7.app.az(this, 0);
                            }
                            this.s.a(p11.j, this.A);
                        }
                        p11.j.d();
                        if (v7.onCreatePanelMenu(p11.a, p11.j)) {
                            p11.r = 0;
                        } else {
                            p11.a(0);
                            if ((v6 == 0) || (this.s == null)) {
                                return v4;
                            } else {
                                this.s.a(0, this.A);
                                return v4;
                            }
                        }
                    }
                    p11.j.d();
                    if (p11.u != null) {
                        p11.j.d(p11.u);
                        p11.u = 0;
                    }
                    if (v7.onPreparePanel(0, p11.i, p11.j)) {
                        android.support.v7.internal.view.b v0_43;
                        if (p12 == null) {
                            v0_43 = -1;
                        } else {
                            v0_43 = p12.getDeviceId();
                        }
                        android.support.v7.internal.view.b v0_46;
                        if (android.view.KeyCharacterMap.load(v0_43).getKeyboardType() == 1) {
                            v0_46 = 0;
                        } else {
                            v0_46 = 1;
                        }
                        p11.p = v0_46;
                        p11.j.setQwertyMode(p11.p);
                        p11.j.e();
                    } else {
                        if ((v6 != 0) && (this.s != null)) {
                            this.s.a(0, this.A);
                        }
                        p11.j.e();
                        return v4;
                    }
                }
                p11.m = 1;
                p11.n = 0;
                this.L = p11;
                v4 = 1;
            } else {
                v4 = 1;
            }
        }
        return v4;
    }

    private static synthetic boolean b(android.support.v7.app.AppCompatDelegateImplV7 p1)
    {
        p1.y = 0;
        return 0;
    }

    private boolean b(android.view.KeyEvent p4)
    {
        boolean v0 = 0;
        if (p4.getRepeatCount() == 0) {
            android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState v1_1 = this.f(0);
            if (!v1_1.o) {
                v0 = this.b(v1_1, p4);
            }
        }
        return v0;
    }

    private static synthetic int c(android.support.v7.app.AppCompatDelegateImplV7 p1)
    {
        p1.z = 0;
        return 0;
    }

    private boolean c(int p4, android.view.KeyEvent p5)
    {
        switch (p4) {
            case 82:
                if (p5.getRepeatCount() != 0) {
                } else {
                    android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState v0_1 = this.f(0);
                    if (v0_1.o) {
                    } else {
                        this.b(v0_1, p5);
                    }
                }
                break;
        }
        if (android.os.Build$VERSION.SDK_INT < 11) {
            this.a(p4, p5);
        }
        return 0;
    }

    private boolean c(android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState p7)
    {
        int v0_11;
        if (p7.i == null) {
            if (p7.j != null) {
                if (this.B == null) {
                    this.B = new android.support.v7.app.be(this, 0);
                }
                int v0_8;
                int v0_5 = this.B;
                if (p7.j != null) {
                    if (p7.k == null) {
                        p7.k = new android.support.v7.internal.view.menu.g(p7.l, android.support.v7.a.k.abc_list_menu_item_layout);
                        p7.k.g = v0_5;
                        p7.j.a(p7.k);
                    }
                    v0_8 = p7.k.a(p7.g);
                } else {
                    v0_8 = 0;
                }
                p7.h = ((android.view.View) v0_8);
                if (p7.h == null) {
                    v0_11 = 0;
                } else {
                    v0_11 = 1;
                }
            } else {
                v0_11 = 0;
            }
        } else {
            p7.h = p7.i;
            v0_11 = 1;
        }
        return v0_11;
    }

    private boolean c(android.view.KeyEvent p5)
    {
        android.support.v7.internal.widget.ac v0_23;
        boolean v2 = 1;
        if (this.t == null) {
            String v3_0 = this.f(0);
            if ((this.s == null) || ((!this.s.c()) || (android.support.v4.view.du.b(android.view.ViewConfiguration.get(this.e))))) {
                if ((!v3_0.o) && (!v3_0.n)) {
                    if (!v3_0.m) {
                        v2 = 0;
                    } else {
                        android.support.v7.internal.widget.ac v0_11;
                        if (!v3_0.r) {
                            v0_11 = 1;
                        } else {
                            v3_0.m = 0;
                            v0_11 = this.b(v3_0, p5);
                        }
                        if (v0_11 == null) {
                        } else {
                            this.a(v3_0, p5);
                        }
                    }
                } else {
                    android.support.v7.internal.widget.ac v0_12 = v3_0.o;
                    this.a(v3_0, 1);
                    v2 = v0_12;
                }
            } else {
                if (this.s.d()) {
                    v2 = this.s.g();
                } else {
                    if ((this.r) || (!this.b(v3_0, p5))) {
                    } else {
                        v2 = this.s.f();
                    }
                }
            }
            if (v2) {
                android.support.v7.internal.widget.ac v0_21 = ((android.media.AudioManager) this.e.getSystemService("audio"));
                if (v0_21 == null) {
                    android.util.Log.w("AppCompatDelegate", "Couldn\'t get audio manager");
                } else {
                    v0_21.playSoundEffect(0);
                }
            }
            v0_23 = v2;
        } else {
            v0_23 = 0;
        }
        return v0_23;
    }

    private static synthetic void d(android.support.v7.app.AppCompatDelegateImplV7 p0)
    {
        p0.n();
        return;
    }

    private static synthetic void e(android.support.v7.app.AppCompatDelegateImplV7 p2)
    {
        p2.a(p2.f(0), 1);
        return;
    }

    private void g(int p3)
    {
        this.a(this.f(p3), 1);
        return;
    }

    private void h(int p4)
    {
        this.z = (this.z | (1 << p4));
        if ((!this.y) && (this.D != null)) {
            android.support.v4.view.cx.a(this.D, this.M);
            this.y = 1;
        }
        return;
    }

    private void i(int p6)
    {
        android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState v0_0 = this.f(p6);
        if (v0_0.j != null) {
            int v1_2 = new android.os.Bundle();
            v0_0.j.c(v1_2);
            if (v1_2.size() > 0) {
                v0_0.u = v1_2;
            }
            v0_0.j.d();
            v0_0.j.clear();
        }
        v0_0.r = 1;
        v0_0.q = 1;
        if (((p6 == 108) || (p6 == 0)) && (this.s != null)) {
            android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState v0_3 = this.f(0);
            if (v0_3 != null) {
                v0_3.m = 0;
                this.b(v0_3, 0);
            }
        }
        return;
    }

    private int j(int p9)
    {
        int v0_4;
        android.support.v7.internal.widget.ActionBarContextView v3_0 = 1;
        int v2 = 0;
        if ((this.u == null) || (!(this.u.getLayoutParams() instanceof android.view.ViewGroup$MarginLayoutParams))) {
            v0_4 = 0;
        } else {
            int v1_3;
            int v0_7 = ((android.view.ViewGroup$MarginLayoutParams) this.u.getLayoutParams());
            if (!this.u.isShown()) {
                if (v0_7.topMargin == 0) {
                    v3_0 = 0;
                    v1_3 = 0;
                } else {
                    v0_7.topMargin = 0;
                    v1_3 = 0;
                }
            } else {
                if (this.O == null) {
                    this.O = new android.graphics.Rect();
                    this.P = new android.graphics.Rect();
                }
                int v1_11;
                int v1_9 = this.O;
                boolean v4_0 = this.P;
                v1_9.set(0, p9, 0, 0);
                android.support.v7.internal.widget.bd.a(this.E, v1_9, v4_0);
                if (v4_0.top != 0) {
                    v1_11 = 0;
                } else {
                    v1_11 = p9;
                }
                int v1_12;
                if (v0_7.topMargin == v1_11) {
                    v1_12 = 0;
                } else {
                    v0_7.topMargin = p9;
                    if (this.G != null) {
                        int v1_15 = this.G.getLayoutParams();
                        if (v1_15.height != p9) {
                            v1_15.height = p9;
                            this.G.setLayoutParams(v1_15);
                        }
                        v1_12 = 1;
                    } else {
                        this.G = new android.view.View(this.e);
                        this.G.setBackgroundColor(this.e.getResources().getColor(android.support.v7.a.f.abc_input_method_navigation_guard));
                        this.E.addView(this.G, -1, new android.view.ViewGroup$LayoutParams(-1, p9));
                        v1_12 = 1;
                    }
                }
                if (this.G == null) {
                    v3_0 = 0;
                }
                if ((!this.n) && (v3_0 != null)) {
                    p9 = 0;
                }
                v1_3 = v3_0;
                v3_0 = v1_12;
            }
            if (v3_0 != null) {
                this.u.setLayoutParams(v0_7);
            }
            v0_4 = v1_3;
        }
        if (this.G != null) {
            if (v0_4 == 0) {
                v2 = 8;
            }
            this.G.setVisibility(v2);
        }
        return p9;
    }

    private static int k(int p2)
    {
        if (p2 != 8) {
            if (p2 == 9) {
                android.util.Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR_OVERLAY id when requesting this feature.");
                p2 = 109;
            }
        } else {
            android.util.Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR id when requesting this feature.");
            p2 = 108;
        }
        return p2;
    }

    private void o()
    {
        if (!this.C) {
            android.support.v7.internal.view.menu.i v0_2 = this.e.obtainStyledAttributes(android.support.v7.a.n.Theme);
            if (v0_2.hasValue(android.support.v7.a.n.Theme_windowActionBar)) {
                if (!v0_2.getBoolean(android.support.v7.a.n.Theme_windowNoTitle, 0)) {
                    if (v0_2.getBoolean(android.support.v7.a.n.Theme_windowActionBar, 0)) {
                        this.b(108);
                    }
                } else {
                    this.b(1);
                }
                if (v0_2.getBoolean(android.support.v7.a.n.Theme_windowActionBarOverlay, 0)) {
                    this.b(109);
                }
                if (v0_2.getBoolean(android.support.v7.a.n.Theme_windowActionModeOverlay, 0)) {
                    this.b(10);
                }
                int v2_3;
                this.o = v0_2.getBoolean(android.support.v7.a.n.Theme_android_windowIsFloating, 0);
                v0_2.recycle();
                android.support.v7.internal.view.menu.i v0_4 = android.view.LayoutInflater.from(this.e);
                if (this.p) {
                    boolean v1_18;
                    if (!this.n) {
                        v1_18 = ((android.view.ViewGroup) v0_4.inflate(android.support.v7.a.k.abc_screen_simple, 0));
                    } else {
                        v1_18 = ((android.view.ViewGroup) v0_4.inflate(android.support.v7.a.k.abc_screen_simple_overlay_action_mode, 0));
                    }
                    if (android.os.Build$VERSION.SDK_INT < 21) {
                        ((android.support.v7.internal.widget.af) v1_18).setOnFitSystemWindowsListener(new android.support.v7.app.av(this));
                        v2_3 = v1_18;
                    } else {
                        android.support.v4.view.cx.a(v1_18, new android.support.v7.app.au(this));
                        v2_3 = v1_18;
                    }
                } else {
                    if (!this.o) {
                        if (!this.l) {
                            v2_3 = 0;
                        } else {
                            android.support.v7.internal.view.menu.i v0_18;
                            boolean v1_22 = new android.util.TypedValue();
                            this.e.getTheme().resolveAttribute(android.support.v7.a.d.actionBarTheme, v1_22, 1);
                            if (v1_22.resourceId == 0) {
                                v0_18 = this.e;
                            } else {
                                v0_18 = new android.support.v7.internal.view.b(this.e, v1_22.resourceId);
                            }
                            android.support.v7.internal.view.menu.i v0_22 = ((android.view.ViewGroup) android.view.LayoutInflater.from(v0_18).inflate(android.support.v7.a.k.abc_screen_toolbar, 0));
                            this.s = ((android.support.v7.internal.widget.ac) v0_22.findViewById(android.support.v7.a.i.decor_content_parent));
                            this.s.setWindowCallback(this.f.getCallback());
                            if (this.m) {
                                this.s.a(109);
                            }
                            if (this.H) {
                                this.s.a(2);
                            }
                            if (this.I) {
                                this.s.a(5);
                            }
                            v2_3 = v0_22;
                        }
                    } else {
                        android.support.v7.internal.view.menu.i v0_24 = ((android.view.ViewGroup) v0_4.inflate(android.support.v7.a.k.abc_dialog_title_material, 0));
                        this.m = 0;
                        this.l = 0;
                        v2_3 = v0_24;
                    }
                }
                if (v2_3 != 0) {
                    if (this.s == null) {
                        this.F = ((android.widget.TextView) v2_3.findViewById(android.support.v7.a.i.title));
                    }
                    android.support.v7.internal.widget.bd.b(v2_3);
                    android.support.v7.internal.view.menu.i v0_31 = ((android.view.ViewGroup) this.f.findViewById(16908290));
                    boolean v1_38 = ((android.support.v7.internal.widget.ContentFrameLayout) v2_3.findViewById(android.support.v7.a.i.action_bar_activity_content));
                    while (v0_31.getChildCount() > 0) {
                        int v4_5 = v0_31.getChildAt(0);
                        v0_31.removeViewAt(0);
                        v1_38.addView(v4_5);
                    }
                    this.f.setContentView(v2_3);
                    v0_31.setId(-1);
                    v1_38.setId(16908290);
                    if ((v0_31 instanceof android.widget.FrameLayout)) {
                        ((android.widget.FrameLayout) v0_31).setForeground(0);
                    }
                    android.support.v7.internal.view.menu.i v0_35;
                    this.E = v2_3;
                    if (!(this.g instanceof android.app.Activity)) {
                        v0_35 = this.q;
                    } else {
                        v0_35 = ((android.app.Activity) this.g).getTitle();
                    }
                    if (!android.text.TextUtils.isEmpty(v0_35)) {
                        this.b(v0_35);
                    }
                    android.support.v7.internal.view.menu.i v0_40 = ((android.support.v7.internal.widget.ContentFrameLayout) this.E.findViewById(16908290));
                    v0_40.a.set(this.D.getPaddingLeft(), this.D.getPaddingTop(), this.D.getPaddingRight(), this.D.getPaddingBottom());
                    if (android.support.v4.view.cx.B(v0_40)) {
                        v0_40.requestLayout();
                    }
                    boolean v1_45 = this.e.obtainStyledAttributes(android.support.v7.a.n.Theme);
                    v1_45.getValue(android.support.v7.a.n.Theme_windowMinWidthMajor, v0_40.getMinWidthMajor());
                    v1_45.getValue(android.support.v7.a.n.Theme_windowMinWidthMinor, v0_40.getMinWidthMinor());
                    if (v1_45.hasValue(android.support.v7.a.n.Theme_windowFixedWidthMajor)) {
                        v1_45.getValue(android.support.v7.a.n.Theme_windowFixedWidthMajor, v0_40.getFixedWidthMajor());
                    }
                    if (v1_45.hasValue(android.support.v7.a.n.Theme_windowFixedWidthMinor)) {
                        v1_45.getValue(android.support.v7.a.n.Theme_windowFixedWidthMinor, v0_40.getFixedWidthMinor());
                    }
                    if (v1_45.hasValue(android.support.v7.a.n.Theme_windowFixedHeightMajor)) {
                        v1_45.getValue(android.support.v7.a.n.Theme_windowFixedHeightMajor, v0_40.getFixedHeightMajor());
                    }
                    if (v1_45.hasValue(android.support.v7.a.n.Theme_windowFixedHeightMinor)) {
                        v1_45.getValue(android.support.v7.a.n.Theme_windowFixedHeightMinor, v0_40.getFixedHeightMinor());
                    }
                    v1_45.recycle();
                    v0_40.requestLayout();
                    this.C = 1;
                    android.support.v7.internal.view.menu.i v0_41 = this.f(0);
                    if ((!this.r) && ((v0_41 == null) || (v0_41.j == null))) {
                        this.h(108);
                    }
                } else {
                    throw new IllegalArgumentException(new StringBuilder("AppCompat does not support the current theme features: { windowActionBar: ").append(this.l).append(", windowActionBarOverlay: ").append(this.m).append(", android:windowIsFloating: ").append(this.o).append(", windowActionModeOverlay: ").append(this.n).append(", windowNoTitle: ").append(this.p).append(" }").toString());
                }
            } else {
                v0_2.recycle();
                throw new IllegalStateException("You need to use a Theme.AppCompat theme (or descendant) with this activity.");
            }
        }
        return;
    }

    private android.view.ViewGroup p()
    {
        android.widget.FrameLayout v0_1 = this.e.obtainStyledAttributes(android.support.v7.a.n.Theme);
        if (v0_1.hasValue(android.support.v7.a.n.Theme_windowActionBar)) {
            if (!v0_1.getBoolean(android.support.v7.a.n.Theme_windowNoTitle, 0)) {
                if (v0_1.getBoolean(android.support.v7.a.n.Theme_windowActionBar, 0)) {
                    this.b(108);
                }
            } else {
                this.b(1);
            }
            if (v0_1.getBoolean(android.support.v7.a.n.Theme_windowActionBarOverlay, 0)) {
                this.b(109);
            }
            if (v0_1.getBoolean(android.support.v7.a.n.Theme_windowActionModeOverlay, 0)) {
                this.b(10);
            }
            android.widget.FrameLayout v2_3;
            this.o = v0_1.getBoolean(android.support.v7.a.n.Theme_android_windowIsFloating, 0);
            v0_1.recycle();
            android.widget.FrameLayout v0_3 = android.view.LayoutInflater.from(this.e);
            if (this.p) {
                android.support.v7.internal.widget.ac v1_18;
                if (!this.n) {
                    v1_18 = ((android.view.ViewGroup) v0_3.inflate(android.support.v7.a.k.abc_screen_simple, 0));
                } else {
                    v1_18 = ((android.view.ViewGroup) v0_3.inflate(android.support.v7.a.k.abc_screen_simple_overlay_action_mode, 0));
                }
                if (android.os.Build$VERSION.SDK_INT < 21) {
                    ((android.support.v7.internal.widget.af) v1_18).setOnFitSystemWindowsListener(new android.support.v7.app.av(this));
                    v2_3 = v1_18;
                } else {
                    android.support.v4.view.cx.a(v1_18, new android.support.v7.app.au(this));
                    v2_3 = v1_18;
                }
            } else {
                if (!this.o) {
                    if (!this.l) {
                        v2_3 = 0;
                    } else {
                        android.widget.FrameLayout v0_17;
                        android.support.v7.internal.widget.ac v1_22 = new android.util.TypedValue();
                        this.e.getTheme().resolveAttribute(android.support.v7.a.d.actionBarTheme, v1_22, 1);
                        if (v1_22.resourceId == 0) {
                            v0_17 = this.e;
                        } else {
                            v0_17 = new android.support.v7.internal.view.b(this.e, v1_22.resourceId);
                        }
                        android.widget.FrameLayout v0_21 = ((android.view.ViewGroup) android.view.LayoutInflater.from(v0_17).inflate(android.support.v7.a.k.abc_screen_toolbar, 0));
                        this.s = ((android.support.v7.internal.widget.ac) v0_21.findViewById(android.support.v7.a.i.decor_content_parent));
                        this.s.setWindowCallback(this.f.getCallback());
                        if (this.m) {
                            this.s.a(109);
                        }
                        if (this.H) {
                            this.s.a(2);
                        }
                        if (this.I) {
                            this.s.a(5);
                        }
                        v2_3 = v0_21;
                    }
                } else {
                    android.widget.FrameLayout v0_23 = ((android.view.ViewGroup) v0_3.inflate(android.support.v7.a.k.abc_dialog_title_material, 0));
                    this.m = 0;
                    this.l = 0;
                    v2_3 = v0_23;
                }
            }
            if (v2_3 != null) {
                if (this.s == null) {
                    this.F = ((android.widget.TextView) v2_3.findViewById(android.support.v7.a.i.title));
                }
                android.support.v7.internal.widget.bd.b(v2_3);
                android.widget.FrameLayout v0_30 = ((android.view.ViewGroup) this.f.findViewById(16908290));
                android.support.v7.internal.widget.ac v1_38 = ((android.support.v7.internal.widget.ContentFrameLayout) v2_3.findViewById(android.support.v7.a.i.action_bar_activity_content));
                while (v0_30.getChildCount() > 0) {
                    int v4_4 = v0_30.getChildAt(0);
                    v0_30.removeViewAt(0);
                    v1_38.addView(v4_4);
                }
                this.f.setContentView(v2_3);
                v0_30.setId(-1);
                v1_38.setId(16908290);
                if ((v0_30 instanceof android.widget.FrameLayout)) {
                    ((android.widget.FrameLayout) v0_30).setForeground(0);
                }
                return v2_3;
            } else {
                throw new IllegalArgumentException(new StringBuilder("AppCompat does not support the current theme features: { windowActionBar: ").append(this.l).append(", windowActionBarOverlay: ").append(this.m).append(", android:windowIsFloating: ").append(this.o).append(", windowActionModeOverlay: ").append(this.n).append(", windowNoTitle: ").append(this.p).append(" }").toString());
            }
        } else {
            v0_1.recycle();
            throw new IllegalStateException("You need to use a Theme.AppCompat theme (or descendant) with this activity.");
        }
    }

    private static void q()
    {
        return;
    }

    private void r()
    {
        android.support.v7.internal.widget.ContentFrameLayout v0_2 = ((android.support.v7.internal.widget.ContentFrameLayout) this.E.findViewById(16908290));
        v0_2.a.set(this.D.getPaddingLeft(), this.D.getPaddingTop(), this.D.getPaddingRight(), this.D.getPaddingBottom());
        if (android.support.v4.view.cx.B(v0_2)) {
            v0_2.requestLayout();
        }
        android.content.res.TypedArray v1_5 = this.e.obtainStyledAttributes(android.support.v7.a.n.Theme);
        v1_5.getValue(android.support.v7.a.n.Theme_windowMinWidthMajor, v0_2.getMinWidthMajor());
        v1_5.getValue(android.support.v7.a.n.Theme_windowMinWidthMinor, v0_2.getMinWidthMinor());
        if (v1_5.hasValue(android.support.v7.a.n.Theme_windowFixedWidthMajor)) {
            v1_5.getValue(android.support.v7.a.n.Theme_windowFixedWidthMajor, v0_2.getFixedWidthMajor());
        }
        if (v1_5.hasValue(android.support.v7.a.n.Theme_windowFixedWidthMinor)) {
            v1_5.getValue(android.support.v7.a.n.Theme_windowFixedWidthMinor, v0_2.getFixedWidthMinor());
        }
        if (v1_5.hasValue(android.support.v7.a.n.Theme_windowFixedHeightMajor)) {
            v1_5.getValue(android.support.v7.a.n.Theme_windowFixedHeightMajor, v0_2.getFixedHeightMajor());
        }
        if (v1_5.hasValue(android.support.v7.a.n.Theme_windowFixedHeightMinor)) {
            v1_5.getValue(android.support.v7.a.n.Theme_windowFixedHeightMinor, v0_2.getFixedHeightMinor());
        }
        v1_5.recycle();
        v0_2.requestLayout();
        return;
    }

    private boolean s()
    {
        int v0 = 1;
        if (this.t == null) {
            boolean v1_1 = this.a();
            if ((!v1_1) || (!v1_1.z())) {
                v0 = 0;
            }
        } else {
            this.t.c();
        }
        return v0;
    }

    private void t()
    {
        if ((this.s == null) || ((!this.s.c()) || ((android.support.v4.view.du.b(android.view.ViewConfiguration.get(this.e))) && (!this.s.e())))) {
            android.support.v7.internal.widget.ac v0_8 = this.f(0);
            v0_8.q = 1;
            this.a(v0_8, 0);
            this.a(v0_8, 0);
        } else {
            android.support.v7.internal.widget.ac v0_10 = this.f.getCallback();
            if (this.s.d()) {
                this.s.g();
                if (!this.r) {
                    v0_10.onPanelClosed(108, this.f(0).j);
                }
            } else {
                if ((v0_10 != null) && (!this.r)) {
                    if ((this.y) && ((this.z & 1) != 0)) {
                        this.D.removeCallbacks(this.M);
                        this.M.run();
                    }
                    android.support.v7.internal.view.menu.i v1_14 = this.f(0);
                    if ((v1_14.j != null) && ((!v1_14.r) && (v0_10.onPreparePanel(0, v1_14.i, v1_14.j)))) {
                        v0_10.onMenuOpened(108, v1_14.j);
                        this.s.f();
                    }
                }
            }
        }
        return;
    }

    private void u()
    {
        if (!this.C) {
            return;
        } else {
            throw new android.util.AndroidRuntimeException("Window feature must be requested before adding content");
        }
    }

    private android.view.ViewGroup v()
    {
        return this.E;
    }

    final android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState a(android.view.Menu p6)
    {
        int v0_0;
        android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState[] v3 = this.K;
        if (v3 == null) {
            v0_0 = 0;
        } else {
            v0_0 = v3.length;
        }
        int v2 = 0;
        while (v2 < v0_0) {
            int v1_1 = v3[v2];
            if ((v1_1 == 0) || (v1_1.j != p6)) {
                v2++;
            } else {
                int v0_1 = v1_1;
            }
            return v0_1;
        }
        v0_1 = 0;
        return v0_1;
    }

    public final android.support.v7.c.a a(android.support.v7.c.b p10)
    {
        if (p10 != null) {
            if (this.t != null) {
                this.t.c();
            }
            android.support.v7.app.ba v3_1 = new android.support.v7.app.ba(this, p10);
            android.view.View v0_2 = this.a();
            if (v0_2 != null) {
                this.t = v0_2.a(v3_1);
            }
            if (this.t == null) {
                this.n();
                if (this.t != null) {
                    this.t.c();
                }
                android.support.v7.app.ba v4_1 = new android.support.v7.app.ba(this, v3_1);
                if (this.u == null) {
                    if (!this.o) {
                        android.view.View v0_11 = ((android.support.v7.internal.widget.ViewStubCompat) this.E.findViewById(android.support.v7.a.i.action_mode_bar_stub));
                        if (v0_11 != null) {
                            v0_11.setLayoutInflater(android.view.LayoutInflater.from(this.m()));
                            this.u = ((android.support.v7.internal.widget.ActionBarContextView) v0_11.a());
                        }
                    } else {
                        android.view.View v0_16;
                        int v5_4 = new android.util.TypedValue();
                        android.view.View v0_15 = this.e.getTheme();
                        v0_15.resolveAttribute(android.support.v7.a.d.actionBarTheme, v5_4, 1);
                        if (v5_4.resourceId == 0) {
                            v0_16 = this.e;
                        } else {
                            android.content.res.Resources$Theme v6_4 = this.e.getResources().newTheme();
                            v6_4.setTo(v0_15);
                            v6_4.applyStyle(v5_4.resourceId, 1);
                            v0_16 = new android.support.v7.internal.view.b(this.e, 0);
                            v0_16.getTheme().setTo(v6_4);
                        }
                        this.u = new android.support.v7.internal.widget.ActionBarContextView(v0_16);
                        this.v = new android.widget.PopupWindow(v0_16, 0, android.support.v7.a.d.actionModePopupWindowStyle);
                        android.support.v4.widget.bo.b(this.v);
                        this.v.setContentView(this.u);
                        this.v.setWidth(-1);
                        v0_16.getTheme().resolveAttribute(android.support.v7.a.d.actionBarSize, v5_4, 1);
                        this.u.setContentHeight(android.util.TypedValue.complexToDimensionPixelSize(v5_4.data, v0_16.getResources().getDisplayMetrics()));
                        this.v.setHeight(-2);
                        this.w = new android.support.v7.app.aw(this);
                    }
                }
                if (this.u != null) {
                    android.view.View v0_29;
                    this.n();
                    this.u.i();
                    android.content.res.Resources$Theme v6_13 = this.u.getContext();
                    if (this.v != null) {
                        v0_29 = 0;
                    } else {
                        v0_29 = 1;
                    }
                    int v5_9 = new android.support.v7.internal.view.c(v6_13, this.u, v4_1, v0_29);
                    if (!v3_1.a(v5_9, v5_9.b())) {
                        this.t = 0;
                    } else {
                        v5_9.d();
                        this.u.a(v5_9);
                        this.t = v5_9;
                        android.support.v4.view.cx.c(this.u, 0);
                        this.x = android.support.v4.view.cx.p(this.u).a(1065353216);
                        this.x.a(new android.support.v7.app.ay(this));
                        if (this.v != null) {
                            this.f.getDecorView().post(this.w);
                        }
                    }
                }
                this.t = this.t;
            }
            return this.t;
        } else {
            throw new IllegalArgumentException("ActionMode callback can not be null.");
        }
    }

    public final android.view.View a(android.view.View p7, String p8, android.content.Context p9, android.util.AttributeSet p10)
    {
        int v4_0 = 0;
        int v0_0 = this.a(p8, p9, p10);
        if (v0_0 == 0) {
            android.content.Context v2_0;
            if (android.os.Build$VERSION.SDK_INT >= 21) {
                v2_0 = 0;
            } else {
                v2_0 = 1;
            }
            if (this.Q == null) {
                this.Q = new android.support.v7.internal.a.a();
            }
            if ((v2_0 == null) || (!this.C)) {
                int v0_14 = 0;
            } else {
                int v0_13;
                if (((android.view.ViewParent) p7) != null) {
                    android.view.ViewParent v1_1 = ((android.view.ViewParent) p7);
                    while (v1_1 != null) {
                        if ((v1_1 != this.D) && (((v1_1 instanceof android.view.View)) && (!android.support.v4.view.cx.D(((android.view.View) v1_1))))) {
                            v1_1 = v1_1.getParent();
                        } else {
                            v0_13 = 0;
                        }
                    }
                    v0_13 = 1;
                } else {
                    v0_13 = 0;
                }
                if (v0_13 == 0) {
                } else {
                    v0_14 = 1;
                }
            }
            int v0_15;
            android.view.ViewParent v1_2 = this.Q;
            if ((v0_14 == 0) || (p7 == null)) {
                v0_15 = p9;
            } else {
                v0_15 = p7.getContext();
            }
            android.content.Context v2_1 = android.support.v7.internal.a.a.a(v0_15, p10, v2_0);
            switch (p8.hashCode()) {
                case -1946472170:
                    if (!p8.equals("RatingBar")) {
                        v4_0 = -1;
                    } else {
                        v4_0 = 7;
                    }
                    break;
                case -1455429095:
                    if (!p8.equals("CheckedTextView")) {
                    } else {
                        v4_0 = 4;
                    }
                    break;
                case -1346021293:
                    if (!p8.equals("MultiAutoCompleteTextView")) {
                    } else {
                        v4_0 = 6;
                    }
                    break;
                case -938935918:
                    if (!p8.equals("TextView")) {
                    } else {
                        v4_0 = 9;
                    }
                    break;
                case -339785223:
                    if (!p8.equals("Spinner")) {
                    } else {
                        v4_0 = 1;
                    }
                    break;
                case 776382189:
                    if (!p8.equals("RadioButton")) {
                    } else {
                        v4_0 = 3;
                    }
                    break;
                case 1413872058:
                    if (!p8.equals("AutoCompleteTextView")) {
                    } else {
                        v4_0 = 5;
                    }
                    break;
                case 1601505219:
                    if (!p8.equals("CheckBox")) {
                    } else {
                        v4_0 = 2;
                    }
                    break;
                case 1666676343:
                    if (!p8.equals("EditText")) {
                    } else {
                    }
                    break;
                case 2001146706:
                    if (!p8.equals("Button")) {
                    } else {
                        v4_0 = 8;
                    }
                    break;
                default:
            }
            switch (v4_0) {
                case 0:
                    v0_0 = new android.support.v7.widget.w(v2_1, p10);
                    break;
                case 1:
                    v0_0 = new android.support.v7.widget.aa(v2_1, p10);
                    break;
                case 2:
                    v0_0 = new android.support.v7.widget.s(v2_1, p10);
                    break;
                case 3:
                    v0_0 = new android.support.v7.widget.y(v2_1, p10);
                    break;
                case 4:
                    v0_0 = new android.support.v7.widget.t(v2_1, p10);
                    break;
                case 5:
                    v0_0 = new android.support.v7.widget.p(v2_1, p10);
                    break;
                case 6:
                    v0_0 = new android.support.v7.widget.x(v2_1, p10);
                    break;
                case 7:
                    v0_0 = new android.support.v7.widget.z(v2_1, p10);
                    break;
                case 8:
                    v0_0 = new android.support.v7.widget.r(v2_1, p10);
                    break;
                case 9:
                    v0_0 = new android.support.v7.widget.ai(v2_1, p10);
                    break;
                default:
                    if (p9 == v2_1) {
                        v0_0 = 0;
                    } else {
                        v0_0 = v1_2.a(v2_1, p8, p10);
                    }
            }
        }
        return v0_0;
    }

    android.view.View a(String p2, android.content.Context p3, android.util.AttributeSet p4)
    {
        int v0_4;
        if (!(this.g instanceof android.view.LayoutInflater$Factory)) {
            v0_4 = 0;
        } else {
            v0_4 = ((android.view.LayoutInflater$Factory) this.g).onCreateView(p2, p3, p4);
            if (v0_4 == 0) {
            }
        }
        return v0_4;
    }

    public final void a(int p3)
    {
        this.o();
        android.view.Window$Callback v0_2 = ((android.view.ViewGroup) this.E.findViewById(16908290));
        v0_2.removeAllViews();
        android.view.LayoutInflater.from(this.e).inflate(p3, v0_2);
        this.g.onContentChanged();
        return;
    }

    final void a(int p2, android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState p3, android.view.Menu p4)
    {
        if (p4 == null) {
            if ((p3 == null) && ((p2 >= 0) && (p2 < this.K.length))) {
                p3 = this.K[p2];
            }
            if (p3 != null) {
                p4 = p3.j;
            }
        }
        if (((p3 == null) || (p3.o)) && (!this.r)) {
            this.g.onPanelClosed(p2, p4);
        }
        return;
    }

    public final void a(android.content.res.Configuration p2)
    {
        if ((this.l) && (this.C)) {
            android.support.v7.app.a v0_2 = this.a();
            if (v0_2 != null) {
                v0_2.a(p2);
            }
        }
        return;
    }

    final void a(android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState p5, boolean p6)
    {
        if ((!p6) || ((p5.a != 0) || ((this.s == null) || (!this.s.d())))) {
            int v0_6 = ((android.view.WindowManager) this.e.getSystemService("window"));
            if ((v0_6 != 0) && ((p5.o) && (p5.g != null))) {
                v0_6.removeView(p5.g);
                if (p6) {
                    this.a(p5.a, p5, 0);
                }
            }
            p5.m = 0;
            p5.n = 0;
            p5.o = 0;
            p5.h = 0;
            p5.q = 1;
            if (this.L == p5) {
                this.L = 0;
            }
        } else {
            this.b(p5.j);
        }
        return;
    }

    public final void a(android.support.v7.internal.view.menu.i p7)
    {
        if ((this.s == null) || ((!this.s.c()) || ((android.support.v4.view.du.b(android.view.ViewConfiguration.get(this.e))) && (!this.s.e())))) {
            android.support.v7.internal.widget.ac v0_8 = this.f(0);
            v0_8.q = 1;
            this.a(v0_8, 0);
            this.a(v0_8, 0);
        } else {
            android.support.v7.internal.widget.ac v0_10 = this.f.getCallback();
            if (this.s.d()) {
                this.s.g();
                if (!this.r) {
                    v0_10.onPanelClosed(108, this.f(0).j);
                }
            } else {
                if ((v0_10 != null) && (!this.r)) {
                    if ((this.y) && ((this.z & 1) != 0)) {
                        this.D.removeCallbacks(this.M);
                        this.M.run();
                    }
                    android.support.v7.internal.view.menu.i v1_14 = this.f(0);
                    if ((v1_14.j != null) && ((!v1_14.r) && (v0_10.onPreparePanel(0, v1_14.i, v1_14.j)))) {
                        v0_10.onMenuOpened(108, v1_14.j);
                        this.s.f();
                    }
                }
            }
        }
        return;
    }

    public final void a(android.support.v7.widget.Toolbar p4)
    {
        if ((this.g instanceof android.app.Activity)) {
            if (!(this.a() instanceof android.support.v7.internal.a.l)) {
                this.k = 0;
                android.support.v7.internal.a.e v1_1 = new android.support.v7.internal.a.e(p4, ((android.app.Activity) this.e).getTitle(), this.h);
                this.j = v1_1;
                this.f.setCallback(v1_1.k);
                v1_1.y();
            } else {
                throw new IllegalStateException("This Activity already has an action bar supplied by the window decor. Do not request Window.FEATURE_SUPPORT_ACTION_BAR and set windowActionBar to false in your theme to use a Toolbar instead.");
            }
        }
        return;
    }

    public final void a(android.view.View p3)
    {
        this.o();
        android.view.Window$Callback v0_2 = ((android.view.ViewGroup) this.E.findViewById(16908290));
        v0_2.removeAllViews();
        v0_2.addView(p3);
        this.g.onContentChanged();
        return;
    }

    public final void a(android.view.View p3, android.view.ViewGroup$LayoutParams p4)
    {
        this.o();
        android.view.Window$Callback v0_2 = ((android.view.ViewGroup) this.E.findViewById(16908290));
        v0_2.removeAllViews();
        v0_2.addView(p3, p4);
        this.g.onContentChanged();
        return;
    }

    final boolean a(int p5, android.view.KeyEvent p6)
    {
        android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState v0 = 1;
        android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState v2_0 = this.a();
        if ((v2_0 == null) || (!v2_0.a(p5, p6))) {
            if ((this.L == null) || (!this.a(this.L, p6.getKeyCode(), p6))) {
                if (this.L == null) {
                    android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState v2_6 = this.f(0);
                    this.b(v2_6, p6);
                    boolean v3_2 = this.a(v2_6, p6.getKeyCode(), p6);
                    v2_6.m = 0;
                    if (v3_2) {
                        return v0;
                    }
                }
                v0 = 0;
            } else {
                if (this.L != null) {
                    this.L.n = 1;
                }
            }
        }
        return v0;
    }

    public final boolean a(android.support.v7.internal.view.menu.i p3, android.view.MenuItem p4)
    {
        boolean v0_2;
        boolean v0_1 = this.f.getCallback();
        if ((!v0_1) || (this.r)) {
            v0_2 = 0;
        } else {
            int v1_2 = this.a(p3.k());
            if (v1_2 == 0) {
            } else {
                v0_2 = v0_1.onMenuItemSelected(v1_2.a, p4);
            }
        }
        return v0_2;
    }

    final boolean a(android.view.KeyEvent p5)
    {
        if ((p5.getKeyCode() != 82) || (!this.g.dispatchKeyEvent(p5))) {
            boolean v0_4;
            String v3_1 = p5.getKeyCode();
            if (p5.getAction() != 0) {
                v0_4 = 0;
            } else {
                v0_4 = 1;
            }
            if (!v0_4) {
                switch (v3_1) {
                    case 4:
                        boolean v0_29 = this.f(0);
                        if ((!v0_29) || (!v0_29.o)) {
                            boolean v0_33;
                            if (this.t == null) {
                                boolean v0_31 = this.a();
                                if ((!v0_31) || (!v0_31.z())) {
                                    v0_33 = 0;
                                } else {
                                    v0_33 = 1;
                                }
                            } else {
                                this.t.c();
                                v0_33 = 1;
                            }
                            if (!v0_33) {
                            } else {
                                boolean v0_28 = 1;
                            }
                        } else {
                            this.a(v0_29, 1);
                            v0_28 = 1;
                        }
                        break;
                    case 82:
                        if (this.t == null) {
                            boolean v0_17;
                            String v3_2 = this.f(0);
                            if ((this.s == null) || ((!this.s.c()) || (android.support.v4.view.du.b(android.view.ViewConfiguration.get(this.e))))) {
                                if ((!v3_2.o) && (!v3_2.n)) {
                                    if (!v3_2.m) {
                                        v0_17 = 0;
                                    } else {
                                        boolean v0_16;
                                        if (!v3_2.r) {
                                            v0_16 = 1;
                                        } else {
                                            v3_2.m = 0;
                                            v0_16 = this.b(v3_2, p5);
                                        }
                                        if (!v0_16) {
                                        } else {
                                            this.a(v3_2, p5);
                                            v0_17 = 1;
                                        }
                                    }
                                } else {
                                    v0_17 = v3_2.o;
                                    this.a(v3_2, 1);
                                }
                            } else {
                                if (this.s.d()) {
                                    v0_17 = this.s.g();
                                } else {
                                    if ((this.r) || (!this.b(v3_2, p5))) {
                                    } else {
                                        v0_17 = this.s.f();
                                    }
                                }
                            }
                            if (v0_17) {
                                boolean v0_26 = ((android.media.AudioManager) this.e.getSystemService("audio"));
                                if (!v0_26) {
                                    android.util.Log.w("AppCompatDelegate", "Couldn\'t get audio manager");
                                } else {
                                    v0_26.playSoundEffect(0);
                                }
                            }
                        }
                        v0_28 = 1;
                        break;
                }
                v0_28 = 0;
            } else {
                switch (v3_1) {
                    case 82:
                        if (p5.getRepeatCount() != 0) {
                        } else {
                            boolean v0_36 = this.f(0);
                            if (v0_36.o) {
                            } else {
                                this.b(v0_36, p5);
                            }
                        }
                        break;
                }
                if (android.os.Build$VERSION.SDK_INT < 11) {
                    this.a(v3_1, p5);
                }
                v0_28 = 0;
            }
        } else {
            v0_28 = 1;
        }
        return v0_28;
    }

    final android.support.v7.c.a b(android.support.v7.c.b p9)
    {
        this.n();
        if (this.t != null) {
            this.t.c();
        }
        android.support.v7.app.ba v3_1 = new android.support.v7.app.ba(this, p9);
        if (this.u == null) {
            if (!this.o) {
                android.view.View v0_6 = ((android.support.v7.internal.widget.ViewStubCompat) this.E.findViewById(android.support.v7.a.i.action_mode_bar_stub));
                if (v0_6 != null) {
                    v0_6.setLayoutInflater(android.view.LayoutInflater.from(this.m()));
                    this.u = ((android.support.v7.internal.widget.ActionBarContextView) v0_6.a());
                }
            } else {
                android.view.View v0_11;
                int v4_4 = new android.util.TypedValue();
                android.view.View v0_10 = this.e.getTheme();
                v0_10.resolveAttribute(android.support.v7.a.d.actionBarTheme, v4_4, 1);
                if (v4_4.resourceId == 0) {
                    v0_11 = this.e;
                } else {
                    android.content.res.Resources$Theme v5_4 = this.e.getResources().newTheme();
                    v5_4.setTo(v0_10);
                    v5_4.applyStyle(v4_4.resourceId, 1);
                    v0_11 = new android.support.v7.internal.view.b(this.e, 0);
                    v0_11.getTheme().setTo(v5_4);
                }
                this.u = new android.support.v7.internal.widget.ActionBarContextView(v0_11);
                this.v = new android.widget.PopupWindow(v0_11, 0, android.support.v7.a.d.actionModePopupWindowStyle);
                android.support.v4.widget.bo.b(this.v);
                this.v.setContentView(this.u);
                this.v.setWidth(-1);
                v0_11.getTheme().resolveAttribute(android.support.v7.a.d.actionBarSize, v4_4, 1);
                this.u.setContentHeight(android.util.TypedValue.complexToDimensionPixelSize(v4_4.data, v0_11.getResources().getDisplayMetrics()));
                this.v.setHeight(-2);
                this.w = new android.support.v7.app.aw(this);
            }
        }
        if (this.u != null) {
            android.view.View v0_24;
            this.n();
            this.u.i();
            android.content.res.Resources$Theme v5_13 = this.u.getContext();
            if (this.v != null) {
                v0_24 = 0;
            } else {
                v0_24 = 1;
            }
            int v4_9 = new android.support.v7.internal.view.c(v5_13, this.u, v3_1, v0_24);
            if (!p9.a(v4_9, v4_9.b())) {
                this.t = 0;
            } else {
                v4_9.d();
                this.u.a(v4_9);
                this.t = v4_9;
                android.support.v4.view.cx.c(this.u, 0);
                this.x = android.support.v4.view.cx.p(this.u).a(1065353216);
                this.x.a(new android.support.v7.app.ay(this));
                if (this.v != null) {
                    this.f.getDecorView().post(this.w);
                }
            }
        }
        return this.t;
    }

    public final android.view.View b(android.view.View p7, String p8, android.content.Context p9, android.util.AttributeSet p10)
    {
        android.content.Context v2_0;
        int v4_0 = 0;
        if (android.os.Build$VERSION.SDK_INT >= 21) {
            v2_0 = 0;
        } else {
            v2_0 = 1;
        }
        if (this.Q == null) {
            this.Q = new android.support.v7.internal.a.a();
        }
        if ((v2_0 == null) || (!this.C)) {
            int v0_13 = 0;
        } else {
            int v0_12;
            if (((android.view.ViewParent) p7) != null) {
                android.view.ViewParent v1_1 = ((android.view.ViewParent) p7);
                while (v1_1 != null) {
                    if ((v1_1 != this.D) && (((v1_1 instanceof android.view.View)) && (!android.support.v4.view.cx.D(((android.view.View) v1_1))))) {
                        v1_1 = v1_1.getParent();
                    } else {
                        v0_12 = 0;
                    }
                }
                v0_12 = 1;
            } else {
                v0_12 = 0;
            }
            if (v0_12 == 0) {
            } else {
                v0_13 = 1;
            }
        }
        int v0_14;
        android.view.ViewParent v1_2 = this.Q;
        if ((v0_13 == 0) || (p7 == null)) {
            v0_14 = p9;
        } else {
            v0_14 = p7.getContext();
        }
        android.content.Context v2_1 = android.support.v7.internal.a.a.a(v0_14, p10, v2_0);
        switch (p8.hashCode()) {
            case -1946472170:
                if (!p8.equals("RatingBar")) {
                    v4_0 = -1;
                } else {
                    v4_0 = 7;
                }
                break;
            case -1455429095:
                if (!p8.equals("CheckedTextView")) {
                } else {
                    v4_0 = 4;
                }
                break;
            case -1346021293:
                if (!p8.equals("MultiAutoCompleteTextView")) {
                } else {
                    v4_0 = 6;
                }
                break;
            case -938935918:
                if (!p8.equals("TextView")) {
                } else {
                    v4_0 = 9;
                }
                break;
            case -339785223:
                if (!p8.equals("Spinner")) {
                } else {
                    v4_0 = 1;
                }
                break;
            case 776382189:
                if (!p8.equals("RadioButton")) {
                } else {
                    v4_0 = 3;
                }
                break;
            case 1413872058:
                if (!p8.equals("AutoCompleteTextView")) {
                } else {
                    v4_0 = 5;
                }
                break;
            case 1601505219:
                if (!p8.equals("CheckBox")) {
                } else {
                    v4_0 = 2;
                }
                break;
            case 1666676343:
                if (!p8.equals("EditText")) {
                } else {
                }
                break;
            case 2001146706:
                if (!p8.equals("Button")) {
                } else {
                    v4_0 = 8;
                }
                break;
            default:
        }
        int v0_17;
        switch (v4_0) {
            case 0:
                v0_17 = new android.support.v7.widget.w(v2_1, p10);
                break;
            case 1:
                v0_17 = new android.support.v7.widget.aa(v2_1, p10);
                break;
            case 2:
                v0_17 = new android.support.v7.widget.s(v2_1, p10);
                break;
            case 3:
                v0_17 = new android.support.v7.widget.y(v2_1, p10);
                break;
            case 4:
                v0_17 = new android.support.v7.widget.t(v2_1, p10);
                break;
            case 5:
                v0_17 = new android.support.v7.widget.p(v2_1, p10);
                break;
            case 6:
                v0_17 = new android.support.v7.widget.x(v2_1, p10);
                break;
            case 7:
                v0_17 = new android.support.v7.widget.z(v2_1, p10);
                break;
            case 8:
                v0_17 = new android.support.v7.widget.r(v2_1, p10);
                break;
            case 9:
                v0_17 = new android.support.v7.widget.ai(v2_1, p10);
                break;
            default:
                if (p9 == v2_1) {
                    v0_17 = 0;
                } else {
                    v0_17 = v1_2.a(v2_1, p8, p10);
                }
        }
        return v0_17;
    }

    final void b(android.support.v7.internal.view.menu.i p3)
    {
        if (!this.J) {
            this.J = 1;
            this.s.i();
            int v0_4 = this.f.getCallback();
            if ((v0_4 != 0) && (!this.r)) {
                v0_4.onPanelClosed(108, p3);
            }
            this.J = 0;
        }
        return;
    }

    public final void b(android.view.View p3, android.view.ViewGroup$LayoutParams p4)
    {
        this.o();
        ((android.view.ViewGroup) this.E.findViewById(16908290)).addView(p3, p4);
        this.g.onContentChanged();
        return;
    }

    final void b(CharSequence p2)
    {
        if (this.s == null) {
            if (this.j == null) {
                if (this.F != null) {
                    this.F.setText(p2);
                }
            } else {
                this.j.d(p2);
            }
        } else {
            this.s.setWindowTitle(p2);
        }
        return;
    }

    public final boolean b(int p5)
    {
        int v0_0 = 0;
        int v2 = android.support.v7.app.AppCompatDelegateImplV7.k(p5);
        if ((!this.p) || (v2 != 108)) {
            if ((this.l) && (v2 == 1)) {
                this.l = 0;
            }
            switch (v2) {
                case 1:
                    this.u();
                    this.p = 1;
                    v0_0 = 1;
                    break;
                case 2:
                    this.u();
                    this.H = 1;
                    v0_0 = 1;
                    break;
                case 5:
                    this.u();
                    this.I = 1;
                    v0_0 = 1;
                    break;
                case 10:
                    this.u();
                    this.n = 1;
                    v0_0 = 1;
                    break;
                case 108:
                    this.u();
                    this.l = 1;
                    v0_0 = 1;
                    break;
                case 109:
                    this.u();
                    this.m = 1;
                    v0_0 = 1;
                    break;
                default:
                    v0_0 = this.f.requestFeature(v2);
            }
        }
        return v0_0;
    }

    public final void c()
    {
        this.D = ((android.view.ViewGroup) this.f.getDecorView());
        if (((this.g instanceof android.app.Activity)) && (android.support.v4.app.cv.b(((android.app.Activity) this.g)) != null)) {
            android.support.v7.app.a v0_8 = this.j;
            if (v0_8 != null) {
                v0_8.f(1);
            } else {
                this.N = 1;
            }
        }
        return;
    }

    public final boolean c(int p3)
    {
        boolean v0_1;
        boolean v0_0 = android.support.v7.app.AppCompatDelegateImplV7.k(p3);
        switch (v0_0) {
            case 1:
                v0_1 = this.p;
                break;
            case 2:
                v0_1 = this.H;
                break;
            case 5:
                v0_1 = this.I;
                break;
            case 10:
                v0_1 = this.n;
                break;
            case 108:
                v0_1 = this.l;
                break;
            case 109:
                v0_1 = this.m;
                break;
            default:
                v0_1 = this.f.hasFeature(v0_0);
        }
        return v0_1;
    }

    public final void d()
    {
        this.o();
        return;
    }

    final void d(int p4)
    {
        if (p4 != 108) {
            if (p4 == 0) {
                android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState v0_1 = this.f(p4);
                if (v0_1.o) {
                    this.a(v0_1, 0);
                }
            }
        } else {
            android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState v0_2 = this.a();
            if (v0_2 != null) {
                v0_2.h(0);
            }
        }
        return;
    }

    public final void e()
    {
        android.support.v7.app.a v0 = this.a();
        if (v0 != null) {
            v0.g(0);
        }
        return;
    }

    final boolean e(int p3)
    {
        int v0 = 1;
        if (p3 != 108) {
            v0 = 0;
        } else {
            android.support.v7.app.a v1_1 = this.a();
            if (v1_1 != null) {
                v1_1.h(1);
            }
        }
        return v0;
    }

    final android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState f(int p5)
    {
        android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState v0_0 = this.K;
        if ((v0_0 == null) || (v0_0.length <= p5)) {
            android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState v1_2 = new android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState[(p5 + 1)];
            if (v0_0 != null) {
                System.arraycopy(v0_0, 0, v1_2, 0, v0_0.length);
            }
            this.K = v1_2;
            v0_0 = v1_2;
        }
        android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState v0_1;
        android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState v1_3 = v0_0[p5];
        if (v1_3 != null) {
            v0_1 = v1_3;
        } else {
            android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState v1_5 = new android.support.v7.app.AppCompatDelegateImplV7$PanelFeatureState(p5);
            v0_0[p5] = v1_5;
            v0_1 = v1_5;
        }
        return v0_1;
    }

    public final void f()
    {
        android.support.v7.app.a v0 = this.a();
        if (v0 != null) {
            v0.g(1);
        }
        return;
    }

    public final void g()
    {
        int v0_0 = this.a();
        if ((v0_0 == 0) || (!v0_0.y())) {
            this.h(0);
        }
        return;
    }

    public final void j()
    {
        String v0_1 = android.view.LayoutInflater.from(this.e);
        if (v0_1.getFactory() != null) {
            android.util.Log.i("AppCompatDelegate", "The Activity\'s LayoutInflater already has a Factory installed so we can not install AppCompat\'s");
        } else {
            android.support.v4.view.ai.a(v0_1, this);
        }
        return;
    }

    public final void l()
    {
        this.o();
        if ((this.l) && (this.j == null)) {
            if (!(this.g instanceof android.app.Activity)) {
                if ((this.g instanceof android.app.Dialog)) {
                    this.j = new android.support.v7.internal.a.l(((android.app.Dialog) this.g));
                }
            } else {
                this.j = new android.support.v7.internal.a.l(((android.app.Activity) this.g), this.m);
            }
            if (this.j != null) {
                this.j.f(this.N);
            }
        }
        return;
    }

    final void n()
    {
        if (this.x != null) {
            this.x.a();
        }
        return;
    }
}
