package android.support.v7.widget;
final class q {
    private final android.view.View a;
    private final android.support.v7.internal.widget.av b;
    private android.support.v7.internal.widget.au c;
    private android.support.v7.internal.widget.au d;

    q(android.view.View p1, android.support.v7.internal.widget.av p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    private void d()
    {
        this.b(0);
        return;
    }

    final android.content.res.ColorStateList a()
    {
        int v0_1;
        if (this.d == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.d.a;
        }
        return v0_1;
    }

    final void a(int p2)
    {
        int v0_1;
        if (this.b == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.b.a(p2);
        }
        this.b(v0_1);
        return;
    }

    final void a(android.content.res.ColorStateList p3)
    {
        if (this.d == null) {
            this.d = new android.support.v7.internal.widget.au();
        }
        this.d.a = p3;
        this.d.d = 1;
        this.c();
        return;
    }

    final void a(android.graphics.PorterDuff$Mode p3)
    {
        if (this.d == null) {
            this.d = new android.support.v7.internal.widget.au();
        }
        this.d.b = p3;
        this.d.c = 1;
        this.c();
        return;
    }

    final void a(android.util.AttributeSet p5, int p6)
    {
        android.content.res.TypedArray v1_1 = this.a.getContext().obtainStyledAttributes(p5, android.support.v7.a.n.ViewBackgroundHelper, p6, 0);
        try {
            if (v1_1.hasValue(android.support.v7.a.n.ViewBackgroundHelper_android_background)) {
                android.view.View v0_5 = this.b.a(v1_1.getResourceId(android.support.v7.a.n.ViewBackgroundHelper_android_background, -1));
                if (v0_5 != null) {
                    this.b(v0_5);
                }
            }
        } catch (android.view.View v0_12) {
            v1_1.recycle();
            throw v0_12;
        }
        if (v1_1.hasValue(android.support.v7.a.n.ViewBackgroundHelper_backgroundTint)) {
            android.support.v4.view.cx.a(this.a, v1_1.getColorStateList(android.support.v7.a.n.ViewBackgroundHelper_backgroundTint));
        }
        if (v1_1.hasValue(android.support.v7.a.n.ViewBackgroundHelper_backgroundTintMode)) {
            android.support.v4.view.cx.a(this.a, android.support.v7.b.a.a.a(v1_1.getInt(android.support.v7.a.n.ViewBackgroundHelper_backgroundTintMode, -1)));
        }
        v1_1.recycle();
        return;
    }

    final android.graphics.PorterDuff$Mode b()
    {
        int v0_1;
        if (this.d == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.d.b;
        }
        return v0_1;
    }

    final void b(android.content.res.ColorStateList p3)
    {
        if (p3 == null) {
            this.c = 0;
        } else {
            if (this.c == null) {
                this.c = new android.support.v7.internal.widget.au();
            }
            this.c.a = p3;
            this.c.d = 1;
        }
        this.c();
        return;
    }

    final void c()
    {
        if (this.a.getBackground() != null) {
            if (this.d == null) {
                if (this.c != null) {
                    android.support.v7.internal.widget.av.a(this.a, this.c);
                }
            } else {
                android.support.v7.internal.widget.av.a(this.a, this.d);
            }
        }
        return;
    }
}
