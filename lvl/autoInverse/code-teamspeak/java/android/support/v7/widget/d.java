package android.support.v7.widget;
final class d implements java.lang.Runnable {
    final synthetic android.support.v7.widget.ActionMenuPresenter a;
    private android.support.v7.widget.g b;

    public d(android.support.v7.widget.ActionMenuPresenter p1, android.support.v7.widget.g p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    public final void run()
    {
        android.support.v7.widget.ActionMenuPresenter v0_1 = this.a.c;
        if (v0_1.f != null) {
            v0_1.f.a(v0_1);
        }
        android.support.v7.widget.ActionMenuPresenter v0_4 = ((android.view.View) this.a.g);
        if ((v0_4 != null) && ((v0_4.getWindowToken() != null) && (this.b.e()))) {
            this.a.p = this.b;
        }
        this.a.r = 0;
        return;
    }
}
