package android.support.v7.widget;
final class ae implements android.widget.AdapterView$OnItemClickListener {
    final synthetic android.support.v7.widget.aa a;
    final synthetic android.support.v7.widget.ad b;

    ae(android.support.v7.widget.ad p1, android.support.v7.widget.aa p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final void onItemClick(android.widget.AdapterView p5, android.view.View p6, int p7, long p8)
    {
        this.b.b.setSelection(p7);
        if (this.b.b.getOnItemClickListener() != null) {
            this.b.b.performItemClick(p6, p7, android.support.v7.widget.ad.a(this.b).getItemId(p7));
        }
        this.b.d();
        return;
    }
}
