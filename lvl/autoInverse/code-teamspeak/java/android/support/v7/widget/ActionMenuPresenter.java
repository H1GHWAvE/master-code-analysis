package android.support.v7.widget;
public final class ActionMenuPresenter extends android.support.v7.internal.view.menu.d implements android.support.v4.view.o {
    private static final String u = "ActionMenuPresenter";
    private int A;
    private final android.util.SparseBooleanArray B;
    private android.view.View C;
    private android.support.v7.widget.c D;
    android.support.v7.widget.e i;
    android.graphics.drawable.Drawable j;
    boolean k;
    public boolean l;
    public int m;
    public boolean n;
    public boolean o;
    android.support.v7.widget.g p;
    android.support.v7.widget.b q;
    android.support.v7.widget.d r;
    final android.support.v7.widget.h s;
    int t;
    private boolean v;
    private int w;
    private int x;
    private boolean y;
    private boolean z;

    public ActionMenuPresenter(android.content.Context p3)
    {
        this(p3, android.support.v7.a.k.abc_action_menu_layout, android.support.v7.a.k.abc_action_menu_item_layout);
        this.B = new android.util.SparseBooleanArray();
        this.s = new android.support.v7.widget.h(this, 0);
        return;
    }

    private static synthetic android.support.v7.widget.g a(android.support.v7.widget.ActionMenuPresenter p1)
    {
        return p1.p;
    }

    private static synthetic android.support.v7.widget.g a(android.support.v7.widget.ActionMenuPresenter p0, android.support.v7.widget.g p1)
    {
        p0.p = p1;
        return p1;
    }

    private android.view.View a(android.view.MenuItem p7)
    {
        int v2;
        android.view.ViewGroup v0_1 = ((android.view.ViewGroup) this.g);
        if (v0_1 != null) {
            int v5 = v0_1.getChildCount();
            int v4 = 0;
            while (v4 < v5) {
                v2 = v0_1.getChildAt(v4);
                if ((!(v2 instanceof android.support.v7.internal.view.menu.aa)) || (((android.support.v7.internal.view.menu.aa) v2).getItemData() != p7)) {
                    v4++;
                }
            }
            v2 = 0;
        } else {
            v2 = 0;
        }
        return v2;
    }

    private void a(int p2)
    {
        this.m = p2;
        this.n = 1;
        return;
    }

    private void a(int p2, boolean p3)
    {
        this.w = p2;
        this.y = p3;
        this.z = 1;
        return;
    }

    private void a(android.graphics.drawable.Drawable p2)
    {
        if (this.i == null) {
            this.k = 1;
            this.j = p2;
        } else {
            this.i.setImageDrawable(p2);
        }
        return;
    }

    private static synthetic android.support.v7.widget.d b(android.support.v7.widget.ActionMenuPresenter p1)
    {
        return p1.r;
    }

    private static synthetic android.support.v7.internal.view.menu.i c(android.support.v7.widget.ActionMenuPresenter p1)
    {
        return p1.c;
    }

    private void c(boolean p1)
    {
        this.o = p1;
        return;
    }

    private static synthetic android.support.v7.widget.e d(android.support.v7.widget.ActionMenuPresenter p1)
    {
        return p1.i;
    }

    private static synthetic android.support.v7.internal.view.menu.z e(android.support.v7.widget.ActionMenuPresenter p1)
    {
        return p1.g;
    }

    private static synthetic android.support.v7.widget.b f(android.support.v7.widget.ActionMenuPresenter p1)
    {
        p1.q = 0;
        return 0;
    }

    private static synthetic android.support.v7.internal.view.menu.i g(android.support.v7.widget.ActionMenuPresenter p1)
    {
        return p1.c;
    }

    private static synthetic android.support.v7.internal.view.menu.z h(android.support.v7.widget.ActionMenuPresenter p1)
    {
        return p1.g;
    }

    private static synthetic android.support.v7.widget.d i(android.support.v7.widget.ActionMenuPresenter p1)
    {
        p1.r = 0;
        return 0;
    }

    private static synthetic android.support.v7.widget.b j(android.support.v7.widget.ActionMenuPresenter p1)
    {
        return p1.q;
    }

    private void k()
    {
        if (!this.n) {
            this.m = this.b.getResources().getInteger(android.support.v7.a.j.abc_max_action_buttons);
        }
        if (this.c != null) {
            this.c.c(1);
        }
        return;
    }

    private android.graphics.drawable.Drawable l()
    {
        int v0_2;
        if (this.i == null) {
            if (!this.k) {
                v0_2 = 0;
            } else {
                v0_2 = this.j;
            }
        } else {
            v0_2 = this.i.getDrawable();
        }
        return v0_2;
    }

    private boolean m()
    {
        return this.l;
    }

    public final android.support.v7.internal.view.menu.z a(android.view.ViewGroup p3)
    {
        android.support.v7.internal.view.menu.z v1 = super.a(p3);
        ((android.support.v7.widget.ActionMenuView) v1).setPresenter(this);
        return v1;
    }

    public final android.view.View a(android.support.v7.internal.view.menu.m p4, android.view.View p5, android.view.ViewGroup p6)
    {
        android.view.View v0 = p4.getActionView();
        if ((v0 == null) || (p4.i())) {
            v0 = super.a(p4, p5, p6);
        }
        android.support.v7.widget.m v1_2;
        if (!p4.isActionViewExpanded()) {
            v1_2 = 0;
        } else {
            v1_2 = 8;
        }
        v0.setVisibility(v1_2);
        android.support.v7.widget.m v1_3 = v0.getLayoutParams();
        if (!((android.support.v7.widget.ActionMenuView) p6).checkLayoutParams(v1_3)) {
            v0.setLayoutParams(android.support.v7.widget.ActionMenuView.a(v1_3));
        }
        return v0;
    }

    public final void a(android.content.Context p8, android.support.v7.internal.view.menu.i p9)
    {
        int v0_0 = 1;
        super.a(p8, p9);
        android.content.res.Resources v2 = p8.getResources();
        android.support.v7.widget.e v3_0 = android.support.v7.internal.view.a.a(p8);
        if (!this.v) {
            if ((android.os.Build$VERSION.SDK_INT < 19) && (android.support.v4.view.du.b(android.view.ViewConfiguration.get(v3_0.a)))) {
                v0_0 = 0;
            }
            this.l = v0_0;
        }
        if (!this.z) {
            this.w = (v3_0.a.getResources().getDisplayMetrics().widthPixels / 2);
        }
        if (!this.n) {
            this.m = v3_0.a.getResources().getInteger(android.support.v7.a.j.abc_max_action_buttons);
        }
        int v0_11 = this.w;
        if (!this.l) {
            this.i = 0;
        } else {
            if (this.i == null) {
                this.i = new android.support.v7.widget.e(this, this.a);
                if (this.k) {
                    this.i.setImageDrawable(this.j);
                    this.j = 0;
                    this.k = 0;
                }
                this.i.measure(android.view.View$MeasureSpec.makeMeasureSpec(0, 0), android.view.View$MeasureSpec.makeMeasureSpec(0, 0));
            }
            v0_11 -= this.i.getMeasuredWidth();
        }
        this.x = v0_11;
        this.A = ((int) (1113587712 * v2.getDisplayMetrics().density));
        this.C = 0;
        return;
    }

    public final void a(android.os.Parcelable p3)
    {
        if (((android.support.v7.widget.ActionMenuPresenter$SavedState) p3).a > 0) {
            android.support.v7.internal.view.menu.ad v0_2 = this.c.findItem(((android.support.v7.widget.ActionMenuPresenter$SavedState) p3).a);
            if (v0_2 != null) {
                this.a(((android.support.v7.internal.view.menu.ad) v0_2.getSubMenu()));
            }
        }
        return;
    }

    public final void a(android.support.v7.internal.view.menu.i p1, boolean p2)
    {
        this.g();
        super.a(p1, p2);
        return;
    }

    public final void a(android.support.v7.internal.view.menu.m p3, android.support.v7.internal.view.menu.aa p4)
    {
        p4.a(p3);
        ((android.support.v7.internal.view.menu.ActionMenuItemView) p4).setItemInvoker(((android.support.v7.widget.ActionMenuView) this.g));
        if (this.D == null) {
            this.D = new android.support.v7.widget.c(this, 0);
        }
        ((android.support.v7.internal.view.menu.ActionMenuItemView) p4).setPopupCallback(this.D);
        return;
    }

    public final void a(android.support.v7.widget.ActionMenuView p2)
    {
        this.g = p2;
        p2.c = this.c;
        return;
    }

    public final void a(boolean p7)
    {
        android.support.v7.widget.e v1_0 = 1;
        android.support.v7.widget.m v2_0 = 0;
        ((android.view.View) this.g).getParent();
        super.a(p7);
        ((android.view.View) this.g).requestLayout();
        if (this.c != null) {
            android.support.v7.widget.ActionMenuView v0_5 = this.c;
            v0_5.i();
            java.util.ArrayList v4 = v0_5.h;
            int v5 = v4.size();
            int v3_0 = 0;
            while (v3_0 < v5) {
                android.support.v7.widget.ActionMenuView v0_30 = ((android.support.v7.internal.view.menu.m) v4.get(v3_0)).i;
                if (v0_30 != null) {
                    v0_30.a = this;
                }
                v3_0++;
            }
        }
        android.support.v7.widget.ActionMenuView v0_7;
        if (this.c == null) {
            v0_7 = 0;
        } else {
            v0_7 = this.c.j();
        }
        if ((this.l) && (v0_7 != null)) {
            int v3_2 = v0_7.size();
            if (v3_2 != 1) {
                if (v3_2 <= 0) {
                    v1_0 = 0;
                }
                v2_0 = v1_0;
            } else {
                android.support.v7.widget.ActionMenuView v0_12;
                if (((android.support.v7.internal.view.menu.m) v0_7.get(0)).isActionViewExpanded()) {
                    v0_12 = 0;
                } else {
                    v0_12 = 1;
                }
                v2_0 = v0_12;
            }
        }
        if (v2_0 == null) {
            if ((this.i != null) && (this.i.getParent() == this.g)) {
                ((android.view.ViewGroup) this.g).removeView(this.i);
            }
        } else {
            if (this.i == null) {
                this.i = new android.support.v7.widget.e(this, this.a);
            }
            android.support.v7.widget.ActionMenuView v0_23 = ((android.view.ViewGroup) this.i.getParent());
            if (v0_23 != this.g) {
                if (v0_23 != null) {
                    v0_23.removeView(this.i);
                }
                ((android.support.v7.widget.ActionMenuView) this.g).addView(this.i, android.support.v7.widget.ActionMenuView.a());
            }
        }
        ((android.support.v7.widget.ActionMenuView) this.g).setOverflowReserved(this.l);
        return;
    }

    public final boolean a()
    {
        java.util.ArrayList v13 = this.c.h();
        int v14 = v13.size();
        int v7_0 = this.m;
        int v9_0 = this.x;
        int v15 = android.view.View$MeasureSpec.makeMeasureSpec(0, 0);
        int v2_3 = ((android.view.ViewGroup) this.g);
        int v6_0 = 0;
        int v5_0 = 0;
        int v4_0 = 0;
        int v10_0 = 0;
        while (v10_0 < v14) {
            int v3_33 = ((android.support.v7.internal.view.menu.m) v13.get(v10_0));
            if (!v3_33.h()) {
                if (!v3_33.g()) {
                    v4_0 = 1;
                } else {
                    v5_0++;
                }
            } else {
                v6_0++;
            }
            if ((!this.o) || (!v3_33.isActionViewExpanded())) {
                int v3_35 = v7_0;
            } else {
                v3_35 = 0;
            }
            v10_0++;
            v7_0 = v3_35;
        }
        if ((this.l) && ((v4_0 != 0) || ((v6_0 + v5_0) > v7_0))) {
            v7_0--;
        }
        int v3_5;
        int v5_2;
        int v10_1 = (v7_0 - v6_0);
        android.util.SparseBooleanArray v16 = this.B;
        v16.clear();
        if (!this.y) {
            v5_2 = 0;
            v3_5 = 0;
        } else {
            int v4_2 = (v9_0 / this.A);
            v5_2 = (((v9_0 % this.A) / v4_2) + this.A);
            v3_5 = v4_2;
        }
        int v7_1 = 0;
        int v11_0 = 0;
        int v6_1 = v3_5;
        while (v11_0 < v14) {
            int v4_6;
            int v6_2;
            int v7_2;
            int v3_15;
            int v4_5 = ((android.support.v7.internal.view.menu.m) v13.get(v11_0));
            if (!v4_5.h()) {
                if (!v4_5.g()) {
                    v4_5.d(0);
                    v3_15 = v6_1;
                    v4_6 = v9_0;
                    v6_2 = v7_1;
                    v7_2 = v10_1;
                } else {
                    int v3_17;
                    int v17 = v4_5.getGroupId();
                    int v18_0 = v16.get(v17);
                    if (((v10_1 <= 0) && (v18_0 == 0)) || ((v9_0 <= 0) || ((this.y) && (v6_1 <= 0)))) {
                        v3_17 = 0;
                    } else {
                        v3_17 = 1;
                    }
                    int v12_0;
                    int v8_1;
                    if (v3_17 == 0) {
                        v12_0 = v3_17;
                        v8_1 = v6_1;
                    } else {
                        int v8_3 = this.a(v4_5, this.C, v2_3);
                        if (this.C == null) {
                            this.C = v8_3;
                        }
                        if (!this.y) {
                            v8_3.measure(v15, v15);
                        } else {
                            int v12_4 = android.support.v7.widget.ActionMenuView.a(v8_3, v5_2, v6_1, v15, 0);
                            v6_1 -= v12_4;
                            if (v12_4 == 0) {
                                v3_17 = 0;
                            }
                        }
                        int v8_4 = v8_3.getMeasuredWidth();
                        v9_0 -= v8_4;
                        if (v7_1 == 0) {
                            v7_1 = v8_4;
                        }
                        if (!this.y) {
                            int v8_7;
                            if ((v9_0 + v7_1) <= 0) {
                                v8_7 = 0;
                            } else {
                                v8_7 = 1;
                            }
                            v12_0 = (v3_17 & v8_7);
                            v8_1 = v6_1;
                        } else {
                            int v8_8;
                            if (v9_0 < 0) {
                                v8_8 = 0;
                            } else {
                                v8_8 = 1;
                            }
                            v12_0 = (v3_17 & v8_8);
                            v8_1 = v6_1;
                        }
                    }
                    if ((v12_0 == 0) || (v17 == 0)) {
                        if (v18_0 == 0) {
                            int v3_20 = v10_1;
                        } else {
                            v16.put(v17, 0);
                            int v6_3 = v10_1;
                            int v10_2 = 0;
                            while (v10_2 < v11_0) {
                                int v3_24 = ((android.support.v7.internal.view.menu.m) v13.get(v10_2));
                                if (v3_24.getGroupId() == v17) {
                                    if (v3_24.f()) {
                                        v6_3++;
                                    }
                                    v3_24.d(0);
                                }
                                v10_2++;
                            }
                            v3_20 = v6_3;
                        }
                    } else {
                        v16.put(v17, 1);
                        v3_20 = v10_1;
                    }
                    if (v12_0 != 0) {
                        v3_20--;
                    }
                    v4_5.d(v12_0);
                    v6_2 = v7_1;
                    v4_6 = v9_0;
                    v7_2 = v3_20;
                    v3_15 = v8_1;
                }
            } else {
                int v8_9 = this.a(v4_5, this.C, v2_3);
                if (this.C == null) {
                    this.C = v8_9;
                }
                if (!this.y) {
                    v8_9.measure(v15, v15);
                    v3_15 = v6_1;
                } else {
                    v3_15 = (v6_1 - android.support.v7.widget.ActionMenuView.a(v8_9, v5_2, v6_1, v15, 0));
                }
                v6_2 = v8_9.getMeasuredWidth();
                int v8_10 = (v9_0 - v6_2);
                if (v7_1 != 0) {
                    v6_2 = v7_1;
                }
                int v7_3 = v4_5.getGroupId();
                if (v7_3 != 0) {
                    v16.put(v7_3, 1);
                }
                v4_5.d(1);
                v4_6 = v8_10;
                v7_2 = v10_1;
            }
            v11_0++;
            v9_0 = v4_6;
            v10_1 = v7_2;
            v7_1 = v6_2;
            v6_1 = v3_15;
        }
        return 1;
    }

    public final boolean a(android.support.v7.internal.view.menu.ad p8)
    {
        android.content.Context v0_7;
        if (p8.hasVisibleItems()) {
            android.content.Context v0_1 = p8;
            while (v0_1.p != this.c) {
                v0_1 = ((android.support.v7.internal.view.menu.ad) v0_1.p);
            }
            android.content.Context v0_4;
            android.view.MenuItem v5 = v0_1.getItem();
            android.content.Context v0_3 = ((android.view.ViewGroup) this.g);
            if (v0_3 == null) {
                v0_4 = 0;
            } else {
                int v6 = v0_3.getChildCount();
                int v4 = 0;
                while (v4 < v6) {
                    android.content.Context v2_1 = v0_3.getChildAt(v4);
                    if ((!(v2_1 instanceof android.support.v7.internal.view.menu.aa)) || (((android.support.v7.internal.view.menu.aa) v2_1).getItemData() != v5)) {
                        v4++;
                    } else {
                        v0_4 = v2_1;
                    }
                }
            }
            if (v0_4 == null) {
                if (this.i != null) {
                    v0_4 = this.i;
                } else {
                    v0_7 = 0;
                    return v0_7;
                }
            }
            this.t = p8.getItem().getItemId();
            this.q = new android.support.v7.widget.b(this, this.b, p8);
            this.q.b = v0_4;
            this.q.d();
            super.a(p8);
            v0_7 = 1;
        } else {
            v0_7 = 0;
        }
        return v0_7;
    }

    public final boolean a(android.view.ViewGroup p3, int p4)
    {
        boolean v0_1;
        if (p3.getChildAt(p4) != this.i) {
            v0_1 = super.a(p3, p4);
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    public final void b(boolean p3)
    {
        if (!p3) {
            this.c.b(0);
        } else {
            super.a(0);
        }
        return;
    }

    public final android.os.Parcelable c()
    {
        android.support.v7.widget.ActionMenuPresenter$SavedState v0_1 = new android.support.v7.widget.ActionMenuPresenter$SavedState();
        v0_1.a = this.t;
        return v0_1;
    }

    public final boolean c(android.support.v7.internal.view.menu.m p2)
    {
        return p2.f();
    }

    public final void d()
    {
        this.l = 1;
        this.v = 1;
        return;
    }

    public final boolean e()
    {
        if ((!this.l) || ((this.i()) || ((this.c == null) || ((this.g == null) || ((this.r != null) || (this.c.j().isEmpty())))))) {
            int v0_8 = 0;
        } else {
            this.r = new android.support.v7.widget.d(this, new android.support.v7.widget.g(this, this.b, this.c, this.i));
            ((android.view.View) this.g).post(this.r);
            super.a(0);
            v0_8 = 1;
        }
        return v0_8;
    }

    public final boolean f()
    {
        if ((this.r == null) || (this.g == null)) {
            int v0_2 = this.p;
            if (v0_2 == 0) {
                int v0_3 = 0;
            } else {
                v0_2.f();
                v0_3 = 1;
            }
        } else {
            ((android.view.View) this.g).removeCallbacks(this.r);
            this.r = 0;
            v0_3 = 1;
        }
        return v0_3;
    }

    public final boolean g()
    {
        return (this.f() | this.h());
    }

    public final boolean h()
    {
        int v0_1;
        if (this.q == null) {
            v0_1 = 0;
        } else {
            this.q.f();
            v0_1 = 1;
        }
        return v0_1;
    }

    public final boolean i()
    {
        if ((this.p == null) || (!this.p.g())) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final boolean j()
    {
        if ((this.r == null) && (!this.i())) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }
}
