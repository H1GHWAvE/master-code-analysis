package android.support.v7.widget;
public final class cb extends android.widget.CompoundButton {
    private static final int[] N = None;
    private static final int a = 250;
    private static final int b = 0;
    private static final int c = 1;
    private static final int d = 2;
    private static final String e = "android.widget.Switch";
    private static final int f = 1;
    private static final int g = 2;
    private static final int h = 3;
    private int A;
    private int B;
    private int C;
    private int D;
    private int E;
    private android.text.TextPaint F;
    private android.content.res.ColorStateList G;
    private android.text.Layout H;
    private android.text.Layout I;
    private android.text.method.TransformationMethod J;
    private android.support.v7.widget.cd K;
    private final android.graphics.Rect L;
    private final android.support.v7.internal.widget.av M;
    private android.graphics.drawable.Drawable i;
    private android.graphics.drawable.Drawable j;
    private int k;
    private int l;
    private int m;
    private boolean n;
    private CharSequence o;
    private CharSequence p;
    private boolean q;
    private int r;
    private int s;
    private float t;
    private float u;
    private android.view.VelocityTracker v;
    private int w;
    private float x;
    private int y;
    private int z;

    static cb()
    {
        int[] v0_1 = new int[1];
        v0_1[0] = 16842912;
        android.support.v7.widget.cb.N = v0_1;
        return;
    }

    private cb(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    private cb(android.content.Context p2, byte p3)
    {
        this(p2, android.support.v7.a.d.switchStyle);
        return;
    }

    private cb(android.content.Context p11, int p12)
    {
        this(p11, 0, p12);
        this.v = android.view.VelocityTracker.obtain();
        this.L = new android.graphics.Rect();
        this.F = new android.text.TextPaint(1);
        this.F.density = this.getResources().getDisplayMetrics().density;
        android.support.v7.internal.widget.ax v5_1 = android.support.v7.internal.widget.ax.a(p11, 0, android.support.v7.a.n.SwitchCompat, p12);
        this.i = v5_1.a(android.support.v7.a.n.SwitchCompat_android_thumb);
        if (this.i != null) {
            this.i.setCallback(this);
        }
        this.j = v5_1.a(android.support.v7.a.n.SwitchCompat_track);
        if (this.j != null) {
            this.j.setCallback(this);
        }
        this.o = v5_1.c(android.support.v7.a.n.SwitchCompat_android_textOn);
        this.p = v5_1.c(android.support.v7.a.n.SwitchCompat_android_textOff);
        this.q = v5_1.a(android.support.v7.a.n.SwitchCompat_showText, 1);
        this.k = v5_1.c(android.support.v7.a.n.SwitchCompat_thumbTextPadding, 0);
        this.l = v5_1.c(android.support.v7.a.n.SwitchCompat_switchMinWidth, 0);
        this.m = v5_1.c(android.support.v7.a.n.SwitchCompat_switchPadding, 0);
        this.n = v5_1.a(android.support.v7.a.n.SwitchCompat_splitTrack, 0);
        int v0_32 = v5_1.e(android.support.v7.a.n.SwitchCompat_switchTextAppearance, 0);
        if (v0_32 != 0) {
            android.content.res.TypedArray v6_1 = p11.obtainStyledAttributes(v0_32, android.support.v7.a.n.TextAppearance);
            int v0_34 = v6_1.getColorStateList(android.support.v7.a.n.TextAppearance_android_textColor);
            if (v0_34 == 0) {
                this.G = this.getTextColors();
            } else {
                this.G = v0_34;
            }
            int v0_37 = v6_1.getDimensionPixelSize(android.support.v7.a.n.TextAppearance_android_textSize, 0);
            if ((v0_37 != 0) && (((float) v0_37) != this.F.getTextSize())) {
                this.F.setTextSize(((float) v0_37));
                this.requestLayout();
            }
            int v0_41;
            int v0_40 = v6_1.getInt(android.support.v7.a.n.TextAppearance_android_typeface, -1);
            int v7_4 = v6_1.getInt(android.support.v7.a.n.TextAppearance_android_textStyle, -1);
            switch (v0_40) {
                case 1:
                    v0_41 = android.graphics.Typeface.SANS_SERIF;
                    break;
                case 2:
                    v0_41 = android.graphics.Typeface.SERIF;
                    break;
                case 3:
                    v0_41 = android.graphics.Typeface.MONOSPACE;
                    break;
                default:
                    v0_41 = 0;
            }
            if (v7_4 <= 0) {
                this.F.setFakeBoldText(0);
                this.F.setTextSkewX(0);
                this.setSwitchTypeface(v0_41);
            } else {
                int v0_42;
                if (v0_41 != 0) {
                    v0_42 = android.graphics.Typeface.create(v0_41, v7_4);
                } else {
                    v0_42 = android.graphics.Typeface.defaultFromStyle(v7_4);
                }
                int v0_43;
                this.setSwitchTypeface(v0_42);
                if (v0_42 == 0) {
                    v0_43 = 0;
                } else {
                    v0_43 = v0_42.getStyle();
                }
                int v0_46;
                int v7_5 = (v7_4 & (v0_43 ^ -1));
                if ((v7_5 & 1) == 0) {
                    v0_46 = 0;
                } else {
                    v0_46 = 1;
                }
                int v0_48;
                this.F.setFakeBoldText(v0_46);
                if ((v7_5 & 2) == 0) {
                    v0_48 = 0;
                } else {
                    v0_48 = -1098907648;
                }
                this.F.setTextSkewX(v0_48);
            }
            if (!v6_1.getBoolean(android.support.v7.a.n.TextAppearance_textAllCaps, 0)) {
                this.J = 0;
            } else {
                this.J = new android.support.v7.internal.b.a(this.getContext());
            }
            v6_1.recycle();
        }
        this.M = v5_1.a();
        v5_1.a.recycle();
        int v0_55 = android.view.ViewConfiguration.get(p11);
        this.s = v0_55.getScaledTouchSlop();
        this.w = v0_55.getScaledMinimumFlingVelocity();
        this.refreshDrawableState();
        this.setChecked(this.isChecked());
        return;
    }

    private static float a(float p3)
    {
        if (p3 >= 0) {
            if (p3 > 1065353216) {
                p3 = 1065353216;
            }
        } else {
            p3 = 0;
        }
        return p3;
    }

    private android.text.Layout a(CharSequence p9)
    {
        CharSequence v1;
        if (this.J == null) {
            v1 = p9;
        } else {
            v1 = this.J.getTransformation(p9, this);
        }
        int v3_0;
        if (v1 == null) {
            v3_0 = 0;
        } else {
            v3_0 = ((int) Math.ceil(((double) android.text.Layout.getDesiredWidth(v1, this.F))));
        }
        return new android.text.StaticLayout(v1, this.F, v3_0, android.text.Layout$Alignment.ALIGN_NORMAL, 1065353216, 0, 1);
    }

    private void a()
    {
        if (this.K != null) {
            this.clearAnimation();
            this.K = 0;
        }
        return;
    }

    private void a(int p6, int p7)
    {
        android.text.TextPaint v1_0 = 0;
        int v0_0 = 0;
        switch (p6) {
            case 1:
                v0_0 = android.graphics.Typeface.SANS_SERIF;
                break;
            case 2:
                v0_0 = android.graphics.Typeface.SERIF;
                break;
            case 3:
                v0_0 = android.graphics.Typeface.MONOSPACE;
                break;
        }
        if (p7 <= 0) {
            this.F.setFakeBoldText(0);
            this.F.setTextSkewX(0);
            this.setSwitchTypeface(v0_0);
        } else {
            int v0_1;
            if (v0_0 != 0) {
                v0_1 = android.graphics.Typeface.create(v0_0, p7);
            } else {
                v0_1 = android.graphics.Typeface.defaultFromStyle(p7);
            }
            int v0_2;
            this.setSwitchTypeface(v0_1);
            if (v0_1 == 0) {
                v0_2 = 0;
            } else {
                v0_2 = v0_1.getStyle();
            }
            int v0_4 = ((v0_2 ^ -1) & p7);
            if ((v0_4 & 1) != 0) {
                v1_0 = 1;
            }
            int v0_6;
            this.F.setFakeBoldText(v1_0);
            if ((v0_4 & 2) == 0) {
                v0_6 = 0;
            } else {
                v0_6 = -1098907648;
            }
            this.F.setTextSkewX(v0_6);
        }
        return;
    }

    private void a(android.content.Context p9, int p10)
    {
        android.content.res.TypedArray v4 = p9.obtainStyledAttributes(p10, android.support.v7.a.n.TextAppearance);
        int v0_2 = v4.getColorStateList(android.support.v7.a.n.TextAppearance_android_textColor);
        if (v0_2 == 0) {
            this.G = this.getTextColors();
        } else {
            this.G = v0_2;
        }
        int v0_5 = v4.getDimensionPixelSize(android.support.v7.a.n.TextAppearance_android_textSize, 0);
        if ((v0_5 != 0) && (((float) v0_5) != this.F.getTextSize())) {
            this.F.setTextSize(((float) v0_5));
            this.requestLayout();
        }
        int v0_9;
        int v0_8 = v4.getInt(android.support.v7.a.n.TextAppearance_android_typeface, -1);
        int v5_4 = v4.getInt(android.support.v7.a.n.TextAppearance_android_textStyle, -1);
        switch (v0_8) {
            case 1:
                v0_9 = android.graphics.Typeface.SANS_SERIF;
                break;
            case 2:
                v0_9 = android.graphics.Typeface.SERIF;
                break;
            case 3:
                v0_9 = android.graphics.Typeface.MONOSPACE;
                break;
            default:
                v0_9 = 0;
        }
        if (v5_4 <= 0) {
            this.F.setFakeBoldText(0);
            this.F.setTextSkewX(0);
            this.setSwitchTypeface(v0_9);
        } else {
            int v0_10;
            if (v0_9 != 0) {
                v0_10 = android.graphics.Typeface.create(v0_9, v5_4);
            } else {
                v0_10 = android.graphics.Typeface.defaultFromStyle(v5_4);
            }
            int v0_11;
            this.setSwitchTypeface(v0_10);
            if (v0_10 == 0) {
                v0_11 = 0;
            } else {
                v0_11 = v0_10.getStyle();
            }
            int v0_14;
            int v5_7 = (v5_4 & (v0_11 ^ -1));
            if ((v5_7 & 1) == 0) {
                v0_14 = 0;
            } else {
                v0_14 = 1;
            }
            int v0_16;
            this.F.setFakeBoldText(v0_14);
            if ((v5_7 & 2) == 0) {
                v0_16 = 0;
            } else {
                v0_16 = -1098907648;
            }
            this.F.setTextSkewX(v0_16);
        }
        if (!v4.getBoolean(android.support.v7.a.n.TextAppearance_textAllCaps, 0)) {
            this.J = 0;
        } else {
            this.J = new android.support.v7.internal.b.a(this.getContext());
        }
        v4.recycle();
        return;
    }

    private void a(android.graphics.Typeface p6, int p7)
    {
        android.text.TextPaint v1_0 = 0;
        if (p7 <= 0) {
            this.F.setFakeBoldText(0);
            this.F.setTextSkewX(0);
            this.setSwitchTypeface(p6);
        } else {
            int v0_2;
            if (p6 != null) {
                v0_2 = android.graphics.Typeface.create(p6, p7);
            } else {
                v0_2 = android.graphics.Typeface.defaultFromStyle(p7);
            }
            int v0_3;
            this.setSwitchTypeface(v0_2);
            if (v0_2 == 0) {
                v0_3 = 0;
            } else {
                v0_3 = v0_2.getStyle();
            }
            int v0_5 = ((v0_3 ^ -1) & p7);
            if ((v0_5 & 1) != 0) {
                v1_0 = 1;
            }
            int v0_7;
            this.F.setFakeBoldText(v1_0);
            if ((v0_5 & 2) == 0) {
                v0_7 = 0;
            } else {
                v0_7 = -1098907648;
            }
            this.F.setTextSkewX(v0_7);
        }
        return;
    }

    static synthetic void a(android.support.v7.widget.cb p0, float p1)
    {
        p0.setThumbPosition(p1);
        return;
    }

    private void a(android.view.MotionEvent p3)
    {
        android.view.MotionEvent v0 = android.view.MotionEvent.obtain(p3);
        v0.setAction(3);
        super.onTouchEvent(v0);
        v0.recycle();
        return;
    }

    private void a(boolean p5)
    {
        android.support.v7.widget.cd v0_0;
        if (!p5) {
            v0_0 = 0;
        } else {
            v0_0 = 1065353216;
        }
        this.K = new android.support.v7.widget.cd(this, this.x, v0_0, 0);
        this.K.setDuration(250);
        this.startAnimation(this.K);
        return;
    }

    private boolean a(float p7, float p8)
    {
        int v0 = 0;
        if (this.i != null) {
            float v1_1 = this.getThumbOffset();
            this.i.getPadding(this.L);
            float v1_3 = ((v1_1 + this.B) - this.s);
            if ((p7 > ((float) v1_3)) && ((p7 < ((float) ((((this.A + v1_3) + this.L.left) + this.L.right) + this.s))) && ((p8 > ((float) (this.C - this.s))) && (p8 < ((float) (this.E + this.s)))))) {
                v0 = 1;
            }
        }
        return v0;
    }

    private void b(android.view.MotionEvent p8)
    {
        android.view.MotionEvent v0_2;
        int v1_0 = 1;
        this.r = 0;
        if ((p8.getAction() != 1) || (!this.isEnabled())) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        boolean v3 = this.isChecked();
        if (v0_2 == null) {
            v1_0 = v3;
        } else {
            this.v.computeCurrentVelocity(1000);
            android.view.MotionEvent v0_5 = this.v.getXVelocity();
            if (Math.abs(v0_5) <= ((float) this.w)) {
                v1_0 = this.getTargetCheckedState();
            } else {
                if (!android.support.v7.internal.widget.bd.a(this)) {
                    if (v0_5 <= 0) {
                        v1_0 = 0;
                    }
                } else {
                    if (v0_5 >= 0) {
                        v1_0 = 0;
                    }
                }
            }
        }
        if (v1_0 != v3) {
            this.playSoundEffect(0);
            this.setChecked(v1_0);
        }
        android.view.MotionEvent v0_8 = android.view.MotionEvent.obtain(p8);
        v0_8.setAction(3);
        super.onTouchEvent(v0_8);
        v0_8.recycle();
        return;
    }

    private boolean getTargetCheckedState()
    {
        int v0_2;
        if (this.x <= 1056964608) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private int getThumbOffset()
    {
        int v0_1;
        if (!android.support.v7.internal.widget.bd.a(this)) {
            v0_1 = this.x;
        } else {
            v0_1 = (1065353216 - this.x);
        }
        return ((int) ((v0_1 * ((float) this.getThumbScrollRange())) + 1056964608));
    }

    private int getThumbScrollRange()
    {
        int v0_1;
        if (this.j == null) {
            v0_1 = 0;
        } else {
            int v0_4;
            int v1_0 = this.L;
            this.j.getPadding(v1_0);
            if (this.i == null) {
                v0_4 = android.support.v7.internal.widget.ae.a;
            } else {
                v0_4 = android.support.v7.internal.widget.ae.a(this.i);
            }
            v0_1 = (((((this.y - this.A) - v1_0.left) - v1_0.right) - v0_4.left) - v0_4.right);
        }
        return v0_1;
    }

    private void setThumbPosition(float p1)
    {
        this.x = p1;
        this.invalidate();
        return;
    }

    public final void draw(android.graphics.Canvas p11)
    {
        int v0_2;
        android.graphics.Rect v7 = this.L;
        int v1_0 = this.B;
        int v3 = this.C;
        int v4 = this.D;
        int v5 = this.E;
        int v2_0 = (v1_0 + this.getThumbOffset());
        if (this.i == null) {
            v0_2 = android.support.v7.internal.widget.ae.a;
        } else {
            v0_2 = android.support.v7.internal.widget.ae.a(this.i);
        }
        int v0_4;
        if (this.j == null) {
            v0_4 = v2_0;
        } else {
            int v2_2;
            int v0_5;
            this.j.getPadding(v7);
            int v6_3 = (v7.left + v2_0);
            if ((v0_2 == 0) || (v0_2.isEmpty())) {
                v0_5 = v5;
                v2_2 = v3;
            } else {
                if (v0_2.left > v7.left) {
                    v1_0 += (v0_2.left - v7.left);
                }
                if (v0_2.top <= v7.top) {
                    v2_2 = v3;
                } else {
                    v2_2 = ((v0_2.top - v7.top) + v3);
                }
                if (v0_2.right > v7.right) {
                    v4 -= (v0_2.right - v7.right);
                }
                if (v0_2.bottom <= v7.bottom) {
                    v0_5 = v5;
                } else {
                    v0_5 = (v5 - (v0_2.bottom - v7.bottom));
                }
            }
            this.j.setBounds(v1_0, v2_2, v4, v0_5);
            v0_4 = v6_3;
        }
        if (this.i != null) {
            this.i.getPadding(v7);
            int v1_4 = (v0_4 - v7.left);
            int v0_9 = ((v0_4 + this.A) + v7.right);
            this.i.setBounds(v1_4, v3, v0_9, v5);
            int v2_12 = this.getBackground();
            if (v2_12 != 0) {
                android.support.v4.e.a.a.a(v2_12, v1_4, v3, v0_9, v5);
            }
        }
        super.draw(p11);
        return;
    }

    public final void drawableHotspotChanged(float p3, float p4)
    {
        if (android.os.Build$VERSION.SDK_INT >= 21) {
            super.drawableHotspotChanged(p3, p4);
        }
        if (this.i != null) {
            android.support.v4.e.a.a.a(this.i, p3, p4);
        }
        if (this.j != null) {
            android.support.v4.e.a.a.a(this.j, p3, p4);
        }
        return;
    }

    protected final void drawableStateChanged()
    {
        super.drawableStateChanged();
        int[] v0 = this.getDrawableState();
        if (this.i != null) {
            this.i.setState(v0);
        }
        if (this.j != null) {
            this.j.setState(v0);
        }
        this.invalidate();
        return;
    }

    public final int getCompoundPaddingLeft()
    {
        int v0_2;
        if (android.support.v7.internal.widget.bd.a(this)) {
            v0_2 = (super.getCompoundPaddingLeft() + this.y);
            if (!android.text.TextUtils.isEmpty(this.getText())) {
                v0_2 += this.m;
            }
        } else {
            v0_2 = super.getCompoundPaddingLeft();
        }
        return v0_2;
    }

    public final int getCompoundPaddingRight()
    {
        int v0_2;
        if (!android.support.v7.internal.widget.bd.a(this)) {
            v0_2 = (super.getCompoundPaddingRight() + this.y);
            if (!android.text.TextUtils.isEmpty(this.getText())) {
                v0_2 += this.m;
            }
        } else {
            v0_2 = super.getCompoundPaddingRight();
        }
        return v0_2;
    }

    public final boolean getShowText()
    {
        return this.q;
    }

    public final boolean getSplitTrack()
    {
        return this.n;
    }

    public final int getSwitchMinWidth()
    {
        return this.l;
    }

    public final int getSwitchPadding()
    {
        return this.m;
    }

    public final CharSequence getTextOff()
    {
        return this.p;
    }

    public final CharSequence getTextOn()
    {
        return this.o;
    }

    public final android.graphics.drawable.Drawable getThumbDrawable()
    {
        return this.i;
    }

    public final int getThumbTextPadding()
    {
        return this.k;
    }

    public final android.graphics.drawable.Drawable getTrackDrawable()
    {
        return this.j;
    }

    public final void jumpDrawablesToCurrentState()
    {
        if (android.os.Build$VERSION.SDK_INT >= 11) {
            super.jumpDrawablesToCurrentState();
            if (this.i != null) {
                this.i.jumpToCurrentState();
            }
            if (this.j != null) {
                this.j.jumpToCurrentState();
            }
            if ((this.K != null) && (!this.K.hasEnded())) {
                this.clearAnimation();
                this.setThumbPosition(this.K.b);
                this.K = 0;
            }
        }
        return;
    }

    protected final int[] onCreateDrawableState(int p3)
    {
        int[] v0_1 = super.onCreateDrawableState((p3 + 1));
        if (this.isChecked()) {
            android.support.v7.widget.cb.mergeDrawableStates(v0_1, android.support.v7.widget.cb.N);
        }
        return v0_1;
    }

    protected final void onDraw(android.graphics.Canvas p10)
    {
        super.onDraw(p10);
        float v0_0 = this.L;
        android.text.Layout v1_0 = this.j;
        if (v1_0 == null) {
            v0_0.setEmpty();
        } else {
            v1_0.getPadding(v0_0);
        }
        float v2_1 = (this.C + v0_0.top);
        int v3_1 = (this.E - v0_0.bottom);
        int v4_2 = this.i;
        if (v1_0 != null) {
            if ((!this.n) || (v4_2 == 0)) {
                v1_0.draw(p10);
            } else {
                int v5_1 = android.support.v7.internal.widget.ae.a(v4_2);
                v4_2.copyBounds(v0_0);
                v0_0.left = (v0_0.left + v5_1.left);
                v0_0.right = (v0_0.right - v5_1.right);
                int v5_4 = p10.save();
                p10.clipRect(v0_0, android.graphics.Region$Op.DIFFERENCE);
                v1_0.draw(p10);
                p10.restoreToCount(v5_4);
            }
        }
        int v5_5 = p10.save();
        if (v4_2 != 0) {
            v4_2.draw(p10);
        }
        android.text.Layout v1_1;
        if (!this.getTargetCheckedState()) {
            v1_1 = this.I;
        } else {
            v1_1 = this.H;
        }
        if (v1_1 != null) {
            float v0_4 = this.getDrawableState();
            if (this.G != null) {
                this.F.setColor(this.G.getColorForState(v0_4, 0));
            }
            float v0_5;
            this.F.drawableState = v0_4;
            if (v4_2 == 0) {
                v0_5 = this.getWidth();
            } else {
                float v0_6 = v4_2.getBounds();
                v0_5 = (v0_6.right + v0_6.left);
            }
            p10.translate(((float) ((v0_5 / 2) - (v1_1.getWidth() / 2))), ((float) (((v2_1 + v3_1) / 2) - (v1_1.getHeight() / 2))));
            v1_1.draw(p10);
        }
        p10.restoreToCount(v5_5);
        return;
    }

    public final void onInitializeAccessibilityEvent(android.view.accessibility.AccessibilityEvent p2)
    {
        super.onInitializeAccessibilityEvent(p2);
        p2.setClassName("android.widget.Switch");
        return;
    }

    public final void onInitializeAccessibilityNodeInfo(android.view.accessibility.AccessibilityNodeInfo p5)
    {
        if (android.os.Build$VERSION.SDK_INT >= 14) {
            CharSequence v0_3;
            super.onInitializeAccessibilityNodeInfo(p5);
            p5.setClassName("android.widget.Switch");
            if (!this.isChecked()) {
                v0_3 = this.p;
            } else {
                v0_3 = this.o;
            }
            if (!android.text.TextUtils.isEmpty(v0_3)) {
                StringBuilder v1_2 = p5.getText();
                if (!android.text.TextUtils.isEmpty(v1_2)) {
                    StringBuilder v2_2 = new StringBuilder();
                    v2_2.append(v1_2).append(32).append(v0_3);
                    p5.setText(v2_2);
                } else {
                    p5.setText(v0_3);
                }
            }
        }
        return;
    }

    protected final void onLayout(boolean p6, int p7, int p8, int p9, int p10)
    {
        int v1_1;
        int v0_0 = 0;
        this = super.onLayout(p6, p7, p8, p9, p10);
        if (this.i == null) {
            v1_1 = 0;
        } else {
            int v2_0 = this.L;
            if (this.j == null) {
                v2_0.setEmpty();
            } else {
                this.j.getPadding(v2_0);
            }
            int v3_0 = android.support.v7.internal.widget.ae.a(this.i);
            v1_1 = Math.max(0, (v3_0.left - v2_0.left));
            v0_0 = Math.max(0, (v3_0.right - v2_0.right));
        }
        int v0_1;
        int v1_8;
        if (!android.support.v7.internal.widget.bd.a(this)) {
            int v2_6 = ((this.getWidth() - this.getPaddingRight()) - v0_0);
            v0_1 = (v0_0 + (v1_1 + (v2_6 - this.y)));
            v1_8 = v2_6;
        } else {
            int v2_8 = (this.getPaddingLeft() + v1_1);
            v1_8 = (((this.y + v2_8) - v1_1) - v0_0);
            v0_1 = v2_8;
        }
        int v3_9;
        int v2_12;
        switch ((this.getGravity() & 112)) {
            case 16:
                v3_9 = ((((this.getPaddingTop() + this.getHeight()) - this.getPaddingBottom()) / 2) - (this.z / 2));
                v2_12 = (this.z + v3_9);
                break;
            case 80:
                v2_12 = (this.getHeight() - this.getPaddingBottom());
                v3_9 = (v2_12 - this.z);
                break;
            default:
                v3_9 = this.getPaddingTop();
                v2_12 = (this.z + v3_9);
        }
        this.B = v0_1;
        this.C = v3_9;
        this.E = v2_12;
        this.D = v1_8;
        return;
    }

    public final void onMeasure(int p7, int p8)
    {
        int v1_0 = 0;
        if (this.q) {
            if (this.H == null) {
                this.H = this.a(this.o);
            }
            if (this.I == null) {
                this.I = this.a(this.p);
            }
        }
        int v2_0;
        int v0_8;
        int v4_0 = this.L;
        if (this.i == null) {
            v0_8 = 0;
            v2_0 = 0;
        } else {
            this.i.getPadding(v4_0);
            v2_0 = ((this.i.getIntrinsicWidth() - v4_0.left) - v4_0.right);
            v0_8 = this.i.getIntrinsicHeight();
        }
        int v3_1;
        if (!this.q) {
            v3_1 = 0;
        } else {
            v3_1 = (Math.max(this.H.getWidth(), this.I.getWidth()) + (this.k * 2));
        }
        this.A = Math.max(v3_1, v2_0);
        if (this.j == null) {
            v4_0.setEmpty();
        } else {
            this.j.getPadding(v4_0);
            v1_0 = this.j.getIntrinsicHeight();
        }
        int v2_5 = v4_0.left;
        int v3_5 = v4_0.right;
        if (this.i != null) {
            int v4_3 = android.support.v7.internal.widget.ae.a(this.i);
            v2_5 = Math.max(v2_5, v4_3.left);
            v3_5 = Math.max(v3_5, v4_3.right);
        }
        int v2_8 = Math.max(this.l, ((v2_5 + (this.A * 2)) + v3_5));
        int v0_14 = Math.max(v1_0, v0_8);
        this.y = v2_8;
        this.z = v0_14;
        super.onMeasure(p7, p8);
        if (this.getMeasuredHeight() < v0_14) {
            this.setMeasuredDimension(android.support.v4.view.cx.i(this), v0_14);
        }
        return;
    }

    public final void onPopulateAccessibilityEvent(android.view.accessibility.AccessibilityEvent p3)
    {
        CharSequence v0_1;
        super.onPopulateAccessibilityEvent(p3);
        if (!this.isChecked()) {
            v0_1 = this.p;
        } else {
            v0_1 = this.o;
        }
        if (v0_1 != null) {
            p3.getText().add(v0_1);
        }
        return;
    }

    public final boolean onTouchEvent(android.view.MotionEvent p10)
    {
        float v1_0 = 1065353216;
        boolean v4_0 = 0;
        boolean v3 = 1;
        this.v.addMovement(p10);
        switch (android.support.v4.view.bk.a(p10)) {
            case 0:
                boolean v0_23 = p10.getX();
                float v1_4 = p10.getY();
                if (!this.isEnabled()) {
                    v3 = super.onTouchEvent(p10);
                } else {
                    if (this.i != null) {
                        float v2_13 = this.getThumbOffset();
                        this.i.getPadding(this.L);
                        float v2_15 = ((v2_13 + this.B) - this.s);
                        if ((v0_23 > ((float) v2_15)) && ((v0_23 < ((float) ((((this.A + v2_15) + this.L.left) + this.L.right) + this.s))) && ((v1_4 > ((float) (this.C - this.s))) && (v1_4 < ((float) (this.E + this.s)))))) {
                            v4_0 = 1;
                        }
                    }
                    if (!v4_0) {
                    } else {
                        this.r = 1;
                        this.t = v0_23;
                        this.u = v1_4;
                    }
                }
                break;
            case 1:
            case 3:
                if (this.r != 2) {
                    this.r = 0;
                    this.v.clear();
                } else {
                    boolean v0_15;
                    this.r = 0;
                    if ((p10.getAction() != 1) || (!this.isEnabled())) {
                        v0_15 = 0;
                    } else {
                        v0_15 = 1;
                    }
                    boolean v0_16;
                    float v1_2 = this.isChecked();
                    if (!v0_15) {
                        v0_16 = v1_2;
                    } else {
                        this.v.computeCurrentVelocity(1000);
                        boolean v0_19 = this.v.getXVelocity();
                        if (Math.abs(v0_19) <= ((float) this.w)) {
                            v0_16 = this.getTargetCheckedState();
                        } else {
                            if (!android.support.v7.internal.widget.bd.a(this)) {
                                if (v0_19 <= 0) {
                                    v0_16 = 0;
                                } else {
                                    v0_16 = 1;
                                }
                            } else {
                                if (v0_19 >= 0) {
                                    v0_16 = 0;
                                } else {
                                    v0_16 = 1;
                                }
                            }
                        }
                    }
                    if (v0_16 != v1_2) {
                        this.playSoundEffect(0);
                        this.setChecked(v0_16);
                    }
                    boolean v0_22 = android.view.MotionEvent.obtain(p10);
                    v0_22.setAction(3);
                    super.onTouchEvent(v0_22);
                    v0_22.recycle();
                    super.onTouchEvent(p10);
                }
                break;
            case 2:
                switch (this.r) {
                    case 1:
                        boolean v0_10 = p10.getX();
                        float v1_1 = p10.getY();
                        if ((Math.abs((v0_10 - this.t)) <= ((float) this.s)) && (Math.abs((v1_1 - this.u)) <= ((float) this.s))) {
                        } else {
                            this.r = 2;
                            this.getParent().requestDisallowInterceptTouchEvent(1);
                            this.t = v0_10;
                            this.u = v1_1;
                        }
                        break;
                    case 2:
                        boolean v0_5;
                        boolean v4_1 = p10.getX();
                        boolean v0_3 = this.getThumbScrollRange();
                        boolean v5_2 = (v4_1 - this.t);
                        if (!v0_3) {
                            if (v5_2 <= 0) {
                                v0_5 = -1082130432;
                            } else {
                                v0_5 = 1065353216;
                            }
                        } else {
                            v0_5 = (v5_2 / ((float) v0_3));
                        }
                        if (android.support.v7.internal.widget.bd.a(this)) {
                            v0_5 = (- v0_5);
                        }
                        boolean v0_7 = (v0_5 + this.x);
                        if (v0_7 >= 0) {
                            if (v0_7 <= 1065353216) {
                                v1_0 = v0_7;
                            }
                        } else {
                            v1_0 = 0;
                        }
                        if (v1_0 == this.x) {
                        } else {
                            this.t = v4_1;
                            this.setThumbPosition(v1_0);
                        }
                        break;
                    default:
                }
                break;
            default:
        }
        return v3;
    }

    public final void setChecked(boolean p6)
    {
        int v0_0 = 1065353216;
        super.setChecked(p6);
        long v2_0 = this.isChecked();
        if ((this.getWindowToken() == null) || (!android.support.v4.view.cx.B(this))) {
            if (this.K != null) {
                this.clearAnimation();
                this.K = 0;
            }
            if (v2_0 == 0) {
                v0_0 = 0;
            }
            this.setThumbPosition(v0_0);
        } else {
            if (v2_0 == 0) {
                v0_0 = 0;
            }
            this.K = new android.support.v7.widget.cd(this, this.x, v0_0, 0);
            this.K.setDuration(250);
            this.startAnimation(this.K);
        }
        return;
    }

    public final void setShowText(boolean p2)
    {
        if (this.q != p2) {
            this.q = p2;
            this.requestLayout();
        }
        return;
    }

    public final void setSplitTrack(boolean p1)
    {
        this.n = p1;
        this.invalidate();
        return;
    }

    public final void setSwitchMinWidth(int p1)
    {
        this.l = p1;
        this.requestLayout();
        return;
    }

    public final void setSwitchPadding(int p1)
    {
        this.m = p1;
        this.requestLayout();
        return;
    }

    public final void setSwitchTypeface(android.graphics.Typeface p2)
    {
        if (this.F.getTypeface() != p2) {
            this.F.setTypeface(p2);
            this.requestLayout();
            this.invalidate();
        }
        return;
    }

    public final void setTextOff(CharSequence p1)
    {
        this.p = p1;
        this.requestLayout();
        return;
    }

    public final void setTextOn(CharSequence p1)
    {
        this.o = p1;
        this.requestLayout();
        return;
    }

    public final void setThumbDrawable(android.graphics.drawable.Drawable p1)
    {
        this.i = p1;
        this.requestLayout();
        return;
    }

    public final void setThumbResource(int p3)
    {
        this.setThumbDrawable(this.M.a(p3, 0));
        return;
    }

    public final void setThumbTextPadding(int p1)
    {
        this.k = p1;
        this.requestLayout();
        return;
    }

    public final void setTrackDrawable(android.graphics.drawable.Drawable p1)
    {
        this.j = p1;
        this.requestLayout();
        return;
    }

    public final void setTrackResource(int p3)
    {
        this.setTrackDrawable(this.M.a(p3, 0));
        return;
    }

    public final void toggle()
    {
        int v0_1;
        if (this.isChecked()) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        this.setChecked(v0_1);
        return;
    }

    protected final boolean verifyDrawable(android.graphics.drawable.Drawable p2)
    {
        if ((!super.verifyDrawable(p2)) && ((p2 != this.i) && (p2 != this.j))) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }
}
