package android.support.v7.widget;
public class aj extends android.view.ViewGroup {
    public static final int h = 0;
    public static final int i = 1;
    public static final int j = 0;
    public static final int k = 1;
    public static final int l = 2;
    public static final int m = 4;
    private static final int q = 4;
    private static final int r = 0;
    private static final int s = 1;
    private static final int t = 2;
    private static final int u = 3;
    private boolean a;
    private int b;
    private int c;
    private int d;
    private int e;
    private int f;
    private float g;
    private boolean n;
    private int[] o;
    private int[] p;
    private android.graphics.drawable.Drawable v;
    private int w;
    private int x;
    private int y;
    private int z;

    public aj(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    public aj(android.content.Context p2, android.util.AttributeSet p3)
    {
        this(p2, p3, 0);
        return;
    }

    public aj(android.content.Context p7, android.util.AttributeSet p8, int p9)
    {
        this(p7, p8, p9);
        this.a = 1;
        this.b = -1;
        this.c = 0;
        this.e = 8388659;
        android.content.res.TypedArray v0_2 = android.support.v7.internal.widget.ax.a(p7, p8, android.support.v7.a.n.LinearLayoutCompat, p9);
        int v1_1 = v0_2.a(android.support.v7.a.n.LinearLayoutCompat_android_orientation, -1);
        if (v1_1 >= 0) {
            this.setOrientation(v1_1);
        }
        int v1_3 = v0_2.a(android.support.v7.a.n.LinearLayoutCompat_android_gravity, -1);
        if (v1_3 >= 0) {
            this.setGravity(v1_3);
        }
        int v1_5 = v0_2.a(android.support.v7.a.n.LinearLayoutCompat_android_baselineAligned, 1);
        if (v1_5 == 0) {
            this.setBaselineAligned(v1_5);
        }
        this.g = v0_2.a.getFloat(android.support.v7.a.n.LinearLayoutCompat_android_weightSum, -1082130432);
        this.b = v0_2.a(android.support.v7.a.n.LinearLayoutCompat_android_baselineAlignedChildIndex, -1);
        this.n = v0_2.a(android.support.v7.a.n.LinearLayoutCompat_measureWithLargestChild, 0);
        this.setDividerDrawable(v0_2.a(android.support.v7.a.n.LinearLayoutCompat_divider));
        this.y = v0_2.a(android.support.v7.a.n.LinearLayoutCompat_showDividers, 0);
        this.z = v0_2.c(android.support.v7.a.n.LinearLayoutCompat_dividerPadding, 0);
        v0_2.a.recycle();
        return;
    }

    private android.view.View a(int p2)
    {
        return this.getChildAt(p2);
    }

    private void a(int p28, int p29)
    {
        this.f = 0;
        int v18 = 0;
        int v17 = 0;
        int v16_0 = 0;
        int v15_0 = 0;
        int v14_0 = 1;
        int v5_0 = 0;
        int v21 = this.getVirtualChildCount();
        int v22 = android.view.View$MeasureSpec.getMode(p28);
        int v23 = android.view.View$MeasureSpec.getMode(p29);
        int v10_0 = 0;
        int v12_0 = 0;
        int v24 = this.b;
        boolean v25 = this.n;
        int v11_0 = -2147483648;
        int v19_0 = 0;
        while (v19_0 < v21) {
            int v3_80;
            int v4_27 = this.getChildAt(v19_0);
            if (v4_27 != 0) {
                int v6_19;
                int v3_54;
                int v10_1;
                int v8_7;
                int v9_6;
                int v4_28;
                int v5_11;
                int v11_3;
                int v7_8;
                if (v4_27.getVisibility() == 8) {
                    v3_54 = v11_0;
                    v4_28 = v12_0;
                    v6_19 = v5_0;
                    v7_8 = v14_0;
                    v8_7 = v15_0;
                    v9_6 = v16_0;
                    v5_11 = v10_0;
                    v11_3 = v18;
                    v10_1 = v17;
                } else {
                    if (this.b(v19_0)) {
                        this.f = (this.f + this.x);
                    }
                    int v11_4;
                    int v8_9;
                    int v9_8 = ((android.support.v7.widget.al) v4_27.getLayoutParams());
                    int v13_3 = (v5_0 + v9_8.g);
                    if ((v23 != 1073741824) || ((v9_8.height != 0) || (v9_8.g <= 0))) {
                        int v3_64 = -2147483648;
                        if ((v9_8.height == 0) && (v9_8.g > 0)) {
                            v3_64 = 0;
                            v9_8.height = -2;
                        }
                        int v8_8;
                        float v20 = v3_64;
                        if (v13_3 != 0) {
                            v8_8 = 0;
                        } else {
                            v8_8 = this.f;
                        }
                        this.a(v4_27, p28, 0, p29, v8_8);
                        if (v20 != -2147483648) {
                            v9_8.height = v20;
                        }
                        int v3_69 = v4_27.getMeasuredHeight();
                        int v5_18 = this.f;
                        this.f = Math.max(v5_18, ((((v5_18 + v3_69) + v9_8.topMargin) + v9_8.bottomMargin) + 0));
                        if (!v25) {
                            v8_9 = v11_0;
                            v11_4 = v12_0;
                        } else {
                            v8_9 = Math.max(v3_69, v11_0);
                            v11_4 = v12_0;
                        }
                    } else {
                        int v3_70 = this.f;
                        this.f = Math.max(v3_70, ((v9_8.topMargin + v3_70) + v9_8.bottomMargin));
                        v8_9 = v11_0;
                        v11_4 = 1;
                    }
                    if ((v24 >= 0) && (v24 == (v19_0 + 1))) {
                        this.c = this.f;
                    }
                    if ((v19_0 >= v24) || (v9_8.g <= 0)) {
                        int v3_76 = 0;
                        if ((v22 == 1073741824) || (v9_8.width != -1)) {
                            v5_11 = v10_0;
                        } else {
                            v5_11 = 1;
                            v3_76 = 1;
                        }
                        int v4_31;
                        int v6_30 = (v9_8.leftMargin + v9_8.rightMargin);
                        int v7_14 = (v4_27.getMeasuredWidth() + v6_30);
                        int v12_5 = Math.max(v18, v7_14);
                        v10_1 = android.support.v7.internal.widget.bd.a(v17, android.support.v4.view.cx.j(v4_27));
                        if ((v14_0 == 0) || (v9_8.width != -1)) {
                            v4_31 = 0;
                        } else {
                            v4_31 = 1;
                        }
                        if (v9_8.g <= 0) {
                            if (v3_76 == 0) {
                                v6_30 = v7_14;
                            }
                            v6_19 = v13_3;
                            v7_8 = v4_31;
                            v9_6 = Math.max(v16_0, v6_30);
                            v4_28 = v11_4;
                            v3_54 = v8_9;
                            v8_7 = v15_0;
                            v11_3 = v12_5;
                        } else {
                            int v3_78;
                            if (v3_76 == 0) {
                                v3_78 = v7_14;
                            } else {
                                v3_78 = v6_30;
                            }
                            v6_19 = v13_3;
                            v7_8 = v4_31;
                            v9_6 = v16_0;
                            v4_28 = v11_4;
                            v11_3 = v12_5;
                            v8_7 = Math.max(v15_0, v3_78);
                            v3_54 = v8_9;
                        }
                    } else {
                        throw new RuntimeException("A child of LinearLayout with index less than mBaselineAlignedChildIndex has weight > 0, which won\'t work.  Either remove the weight, or don\'t set mBaselineAlignedChildIndex.");
                    }
                }
                v14_0 = v7_8;
                v15_0 = v8_7;
                v16_0 = v9_6;
                v17 = v10_1;
                v18 = v11_3;
                v11_0 = v3_54;
                v10_0 = v5_11;
                v3_80 = (v19_0 + 0);
                v5_0 = v6_19;
                v12_0 = v4_28;
            } else {
                this.f = (this.f + 0);
                v3_80 = v19_0;
            }
            v19_0 = (v3_80 + 1);
        }
        if ((this.f > 0) && (this.b(v21))) {
            this.f = (this.f + this.x);
        }
        if ((v25) && ((v23 == -2147483648) || (v23 == 0))) {
            this.f = 0;
            int v4_1 = 0;
            while (v4_1 < v21) {
                int v3_50;
                int v3_43 = this.getChildAt(v4_1);
                if (v3_43 != 0) {
                    if (v3_43.getVisibility() != 8) {
                        int v3_45 = ((android.support.v7.widget.al) v3_43.getLayoutParams());
                        int v6_17 = this.f;
                        this.f = Math.max(v6_17, ((v3_45.bottomMargin + ((v6_17 + v11_0) + v3_45.topMargin)) + 0));
                        v3_50 = v4_1;
                    } else {
                        v3_50 = (v4_1 + 0);
                    }
                } else {
                    this.f = (this.f + 0);
                    v3_50 = v4_1;
                }
                v4_1 = (v3_50 + 1);
            }
        }
        int v4_8;
        int v3_17;
        this.f = (this.f + (this.getPaddingTop() + this.getPaddingBottom()));
        int v19_1 = android.support.v4.view.cx.a(Math.max(this.f, this.getSuggestedMinimumHeight()), p29, 0);
        int v6_1 = ((16777215 & v19_1) - this.f);
        if ((v12_0 == 0) && ((v6_1 == 0) || (v5_0 <= 0))) {
            int v13_0 = Math.max(v16_0, v15_0);
            if ((v25) && (v23 != 1073741824)) {
                int v4_7 = 0;
                while (v4_7 < v21) {
                    int v5_1 = this.getChildAt(v4_7);
                    if ((v5_1 != 0) && ((v5_1.getVisibility() != 8) && (((android.support.v7.widget.al) v5_1.getLayoutParams()).g > 0))) {
                        v5_1.measure(android.view.View$MeasureSpec.makeMeasureSpec(v5_1.getMeasuredWidth(), 1073741824), android.view.View$MeasureSpec.makeMeasureSpec(v11_0, 1073741824));
                    }
                    v4_7++;
                }
            }
            v3_17 = v13_0;
            v4_8 = v18;
        } else {
            if (this.g > 0) {
                v5_0 = this.g;
            }
            this.f = 0;
            int v15_1 = 0;
            int v12_1 = v14_0;
            int v13_1 = v16_0;
            int v11_1 = v17;
            int v14_1 = v18;
            while (v15_1 < v21) {
                int v5_5;
                int v3_36;
                int v4_16;
                int v8_0;
                int v6_7;
                int v7_1;
                int v7_0 = this.getChildAt(v15_1);
                if (v7_0.getVisibility() == 8) {
                    v4_16 = v5_0;
                    v8_0 = v12_1;
                    v3_36 = v13_1;
                    v7_1 = v14_1;
                    v5_5 = v6_1;
                    v6_7 = v11_1;
                } else {
                    int v3_38 = ((android.support.v7.widget.al) v7_0.getLayoutParams());
                    int v8_1 = v3_38.g;
                    if (v8_1 <= 0) {
                        v4_16 = v5_0;
                        v5_5 = v6_1;
                        v6_7 = v11_1;
                    } else {
                        int v6_14;
                        int v4_22 = ((int) ((((float) v6_1) * v8_1) / v5_0));
                        int v8_2 = (v5_0 - v8_1);
                        int v9_0 = (v6_1 - v4_22);
                        int v5_10 = android.support.v7.widget.aj.getChildMeasureSpec(p28, (((this.getPaddingLeft() + this.getPaddingRight()) + v3_38.leftMargin) + v3_38.rightMargin), v3_38.width);
                        if ((v3_38.height == 0) && (v23 == 1073741824)) {
                            if (v4_22 <= 0) {
                                v4_22 = 0;
                                v6_14 = v7_0;
                            } else {
                                v6_14 = v7_0;
                            }
                        } else {
                            v4_22 += v7_0.getMeasuredHeight();
                            if (v4_22 < 0) {
                                v4_22 = 0;
                            }
                            v6_14 = v7_0;
                        }
                        v6_14.measure(v5_10, android.view.View$MeasureSpec.makeMeasureSpec(v4_22, 1073741824));
                        v5_5 = v9_0;
                        v6_7 = android.support.v7.internal.widget.bd.a(v11_1, (android.support.v4.view.cx.j(v7_0) & -256));
                        v4_16 = v8_2;
                    }
                    int v14_4;
                    int v8_4 = (v3_38.leftMargin + v3_38.rightMargin);
                    int v9_3 = (v7_0.getMeasuredWidth() + v8_4);
                    int v11_2 = Math.max(v14_1, v9_3);
                    if ((v22 == 1073741824) || (v3_38.width != -1)) {
                        v14_4 = 0;
                    } else {
                        v14_4 = 1;
                    }
                    if (v14_4 == 0) {
                        v8_4 = v9_3;
                    }
                    int v9_4 = Math.max(v13_1, v8_4);
                    if ((v12_1 == 0) || (v3_38.width != -1)) {
                        v8_0 = 0;
                    } else {
                        v8_0 = 1;
                    }
                    int v12_3 = this.f;
                    this.f = Math.max(v12_3, ((v3_38.bottomMargin + ((v7_0.getMeasuredHeight() + v12_3) + v3_38.topMargin)) + 0));
                    v3_36 = v9_4;
                    v7_1 = v11_2;
                }
                v15_1++;
                v12_1 = v8_0;
                v13_1 = v3_36;
                v11_1 = v6_7;
                v14_1 = v7_1;
                v6_1 = v5_5;
                v5_0 = v4_16;
            }
            this.f = (this.f + (this.getPaddingTop() + this.getPaddingBottom()));
            v3_17 = v13_1;
            v17 = v11_1;
            v4_8 = v14_1;
            v14_0 = v12_1;
        }
        if ((v14_0 != 0) || (v22 == 1073741824)) {
            v3_17 = v4_8;
        }
        this.setMeasuredDimension(android.support.v4.view.cx.a(Math.max((v3_17 + (this.getPaddingLeft() + this.getPaddingRight())), this.getSuggestedMinimumWidth()), p28, v17), v19_1);
        if (v10_0 != 0) {
            this.b(v21, p29);
        }
        return;
    }

    private void a(int p14, int p15, int p16, int p17)
    {
        int v0_7;
        int v5 = this.getPaddingLeft();
        int v0_0 = (p16 - p14);
        int v6 = (v0_0 - this.getPaddingRight());
        int v7 = ((v0_0 - v5) - this.getPaddingRight());
        int v8 = this.getVirtualChildCount();
        int v2_1 = (8388615 & this.e);
        switch ((this.e & 112)) {
            case 16:
                v0_7 = (this.getPaddingTop() + (((p17 - p15) - this.f) / 2));
                break;
            case 80:
                v0_7 = (((this.getPaddingTop() + p17) - p15) - this.f);
                break;
            default:
                v0_7 = this.getPaddingTop();
        }
        int v4 = 0;
        int v3_1 = v0_7;
        while (v4 < v8) {
            int v0_10;
            android.view.View v9 = this.getChildAt(v4);
            if (v9 != null) {
                if (v9.getVisibility() == 8) {
                    v0_10 = v4;
                } else {
                    int v10 = v9.getMeasuredWidth();
                    int v11 = v9.getMeasuredHeight();
                    int v0_12 = ((android.support.v7.widget.al) v9.getLayoutParams());
                    int v1_8 = v0_12.h;
                    if (v1_8 < 0) {
                        v1_8 = v2_1;
                    }
                    int v1_12;
                    switch ((android.support.v4.view.v.a(v1_8, android.support.v4.view.cx.f(this)) & 7)) {
                        case 1:
                            v1_12 = (((((v7 - v10) / 2) + v5) + v0_12.leftMargin) - v0_12.rightMargin);
                            break;
                        case 5:
                            v1_12 = ((v6 - v10) - v0_12.rightMargin);
                            break;
                        default:
                            v1_12 = (v0_12.leftMargin + v5);
                    }
                    if (this.b(v4)) {
                        v3_1 += this.x;
                    }
                    int v3_2 = (v3_1 + v0_12.topMargin);
                    android.support.v7.widget.aj.b(v9, v1_12, (v3_2 + 0), v10, v11);
                    v3_1 = (v3_2 + ((v0_12.bottomMargin + v11) + 0));
                    v0_10 = (v4 + 0);
                }
            } else {
                v3_1 += 0;
                v0_10 = v4;
            }
            v4 = (v0_10 + 1);
        }
        return;
    }

    private void a(android.graphics.Canvas p6)
    {
        int v2 = this.getVirtualChildCount();
        int v1_0 = 0;
        while (v1_0 < v2) {
            int v3_0 = this.getChildAt(v1_0);
            if ((v3_0 != 0) && ((v3_0.getVisibility() != 8) && (this.b(v1_0)))) {
                this.a(p6, ((v3_0.getTop() - ((android.support.v7.widget.al) v3_0.getLayoutParams()).topMargin) - this.x));
            }
            v1_0++;
        }
        if (this.b(v2)) {
            int v0_6;
            int v1_1 = this.getChildAt((v2 - 1));
            if (v1_1 != 0) {
                v0_6 = (((android.support.v7.widget.al) v1_1.getLayoutParams()).bottomMargin + v1_1.getBottom());
            } else {
                v0_6 = ((this.getHeight() - this.getPaddingBottom()) - this.x);
            }
            this.a(p6, v0_6);
        }
        return;
    }

    private void a(android.graphics.Canvas p5, int p6)
    {
        this.v.setBounds((this.getPaddingLeft() + this.z), p6, ((this.getWidth() - this.getPaddingRight()) - this.z), (this.x + p6));
        this.v.draw(p5);
        return;
    }

    private void a(android.view.View p1, int p2, int p3, int p4, int p5)
    {
        this.measureChildWithMargins(p1, p2, p3, p4, p5);
        return;
    }

    private boolean a()
    {
        return this.a;
    }

    private void b(int p10, int p11)
    {
        int v2 = android.view.View$MeasureSpec.makeMeasureSpec(this.getMeasuredWidth(), 1073741824);
        int v7 = 0;
        while (v7 < p10) {
            android.view.View v1_1 = this.getChildAt(v7);
            if (v1_1.getVisibility() != 8) {
                android.support.v7.widget.al v6_1 = ((android.support.v7.widget.al) v1_1.getLayoutParams());
                if (v6_1.width == -1) {
                    int v8 = v6_1.height;
                    v6_1.height = v1_1.getMeasuredHeight();
                    this.measureChildWithMargins(v1_1, v2, 0, p11, 0);
                    v6_1.height = v8;
                }
            }
            v7++;
        }
        return;
    }

    private void b(int p24, int p25, int p26, int p27)
    {
        int v9_0;
        int v5_0 = android.support.v7.internal.widget.bd.a(this);
        int v8 = this.getPaddingTop();
        int v3_0 = (p27 - p25);
        int v12 = (v3_0 - this.getPaddingBottom());
        int v13 = ((v3_0 - v8) - this.getPaddingBottom());
        int v14 = this.getVirtualChildCount();
        int v11 = (this.e & 112);
        boolean v15 = this.a;
        int[] v16 = this.o;
        int[] v17 = this.p;
        switch (android.support.v4.view.v.a((this.e & 8388615), android.support.v4.view.cx.f(this))) {
            case 1:
                v9_0 = (this.getPaddingLeft() + (((p26 - p24) - this.f) / 2));
                break;
            case 5:
                v9_0 = (((this.getPaddingLeft() + p26) - p24) - this.f);
                break;
            default:
                v9_0 = this.getPaddingLeft();
        }
        int v5_1;
        int v4_10;
        if (v5_0 == 0) {
            v5_1 = 0;
            v4_10 = 1;
        } else {
            v5_1 = (v14 - 1);
            v4_10 = -1;
        }
        int v10 = 0;
        while (v10 < v14) {
            int v3_13;
            int v18 = (v5_1 + (v4_10 * v10));
            android.view.View v19 = this.getChildAt(v18);
            if (v19 != null) {
                if (v19.getVisibility() == 8) {
                    v3_13 = v10;
                } else {
                    int v20 = v19.getMeasuredWidth();
                    int v21 = v19.getMeasuredHeight();
                    int v6_2 = -1;
                    int v3_15 = ((android.support.v7.widget.al) v19.getLayoutParams());
                    if ((v15) && (v3_15.height != -1)) {
                        v6_2 = v19.getBaseline();
                    }
                    int v7_1 = v3_15.h;
                    if (v7_1 < 0) {
                        v7_1 = v11;
                    }
                    int v6_5;
                    int v7_4;
                    switch ((v7_1 & 112)) {
                        case 16:
                            v6_5 = (((((v13 - v21) / 2) + v8) + v3_15.topMargin) - v3_15.bottomMargin);
                            break;
                        case 48:
                            v7_4 = (v3_15.topMargin + v8);
                            if (v6_2 == -1) {
                                v6_5 = v7_4;
                            } else {
                                v6_5 = ((v16[1] - v6_2) + v7_4);
                            }
                            break;
                        case 80:
                            v7_4 = ((v12 - v21) - v3_15.bottomMargin);
                            if (v6_2 == -1) {
                            } else {
                                v6_5 = (v7_4 - (v17[2] - (v19.getMeasuredHeight() - v6_2)));
                            }
                            break;
                        default:
                            v6_5 = v8;
                    }
                    int v7_9;
                    if (!this.b(v18)) {
                        v7_9 = v9_0;
                    } else {
                        v7_9 = (this.w + v9_0);
                    }
                    int v7_11 = (v7_9 + v3_15.leftMargin);
                    android.support.v7.widget.aj.b(v19, (v7_11 + 0), v6_5, v20, v21);
                    v9_0 = (v7_11 + ((v3_15.rightMargin + v20) + 0));
                    v3_13 = (v10 + 0);
                }
            } else {
                v9_0 += 0;
                v3_13 = v10;
            }
            v10 = (v3_13 + 1);
        }
        return;
    }

    private void b(android.graphics.Canvas p7)
    {
        int v2 = this.getVirtualChildCount();
        boolean v3 = android.support.v7.internal.widget.bd.a(this);
        int v1_0 = 0;
        while (v1_0 < v2) {
            int v4_0 = this.getChildAt(v1_0);
            if ((v4_0 != 0) && ((v4_0.getVisibility() != 8) && (this.b(v1_0)))) {
                int v0_17;
                int v0_14 = ((android.support.v7.widget.al) v4_0.getLayoutParams());
                if (!v3) {
                    v0_17 = ((v4_0.getLeft() - v0_14.leftMargin) - this.w);
                } else {
                    v0_17 = (v0_14.rightMargin + v4_0.getRight());
                }
                this.b(p7, v0_17);
            }
            v1_0++;
        }
        if (this.b(v2)) {
            int v0_6;
            int v1_1 = this.getChildAt((v2 - 1));
            if (v1_1 != 0) {
                int v0_4 = ((android.support.v7.widget.al) v1_1.getLayoutParams());
                if (!v3) {
                    v0_6 = (v0_4.rightMargin + v1_1.getRight());
                } else {
                    v0_6 = ((v1_1.getLeft() - v0_4.leftMargin) - this.w);
                }
            } else {
                if (!v3) {
                    v0_6 = ((this.getWidth() - this.getPaddingRight()) - this.w);
                } else {
                    v0_6 = this.getPaddingLeft();
                }
            }
            this.b(p7, v0_6);
        }
        return;
    }

    private void b(android.graphics.Canvas p6, int p7)
    {
        this.v.setBounds(p7, (this.getPaddingTop() + this.z), (this.w + p7), ((this.getHeight() - this.getPaddingBottom()) - this.z));
        this.v.draw(p6);
        return;
    }

    private static void b(android.view.View p2, int p3, int p4, int p5, int p6)
    {
        p2.layout(p3, p4, (p3 + p5), (p4 + p6));
        return;
    }

    private boolean b()
    {
        return this.n;
    }

    private boolean b(int p6)
    {
        int v0 = 1;
        if (p6 != 0) {
            if (p6 != this.getChildCount()) {
                if ((this.y & 2) == 0) {
                    v0 = 0;
                } else {
                    int v2_3 = (p6 - 1);
                    while (v2_3 >= 0) {
                        if (this.getChildAt(v2_3).getVisibility() == 8) {
                            v2_3--;
                        }
                    }
                    v0 = 0;
                }
            } else {
                if ((this.y & 4) == 0) {
                    v0 = 0;
                }
            }
        } else {
            if ((this.y & 1) == 0) {
                v0 = 0;
            }
        }
        return v0;
    }

    private void c(int p30, int p31)
    {
        this.f = 0;
        int v18_0 = 0;
        int v17_0 = 0;
        int v16_0 = 0;
        int v15_0 = 0;
        int v14_0 = 1;
        int v4_0 = 0;
        int v21 = this.getVirtualChildCount();
        int v22 = android.view.View$MeasureSpec.getMode(p30);
        int v23 = android.view.View$MeasureSpec.getMode(p31);
        int v10_0 = 0;
        int v12_0 = 0;
        if ((this.o == null) || (this.p == null)) {
            int v2_4 = new int[4];
            this.o = v2_4;
            int v2_6 = new int[4];
            this.p = v2_6;
        }
        int v9_0;
        int[] v24 = this.o;
        int[] v25 = this.p;
        v24[3] = -1;
        v24[2] = -1;
        v24[1] = -1;
        v24[0] = -1;
        v25[3] = -1;
        v25[2] = -1;
        v25[1] = -1;
        v25[0] = -1;
        boolean v26 = this.a;
        boolean v27 = this.n;
        if (v22 != 1073741824) {
            v9_0 = 0;
        } else {
            v9_0 = 1;
        }
        int v11_0 = -2147483648;
        int v19 = 0;
        while (v19 < v21) {
            int v2_130;
            int v3_65 = this.getChildAt(v19);
            if (v3_65 != 0) {
                int v7_17;
                int v4_38;
                int v11_8;
                int v2_104;
                int v3_66;
                int v6_28;
                int v8_9;
                int v10_2;
                int v5_23;
                if (v3_65.getVisibility() == 8) {
                    v2_104 = v11_0;
                    v3_66 = v12_0;
                    v5_23 = v4_0;
                    v6_28 = v14_0;
                    v7_17 = v15_0;
                    v8_9 = v16_0;
                    v4_38 = v10_0;
                    v11_8 = v18_0;
                    v10_2 = v17_0;
                } else {
                    if (this.b(v19)) {
                        this.f = (this.f + this.w);
                    }
                    int v11_9;
                    int v7_19;
                    int v8_11 = ((android.support.v7.widget.al) v3_65.getLayoutParams());
                    int v13_6 = (v4_0 + v8_11.g);
                    if ((v22 != 1073741824) || ((v8_11.width != 0) || (v8_11.g <= 0))) {
                        int v2_114 = -2147483648;
                        if ((v8_11.width == 0) && (v8_11.g > 0)) {
                            v2_114 = 0;
                            v8_11.width = -2;
                        }
                        int v5_26;
                        float v20 = v2_114;
                        if (v13_6 != 0) {
                            v5_26 = 0;
                        } else {
                            v5_26 = this.f;
                        }
                        this.a(v3_65, p30, v5_26, p31, 0);
                        if (v20 != -2147483648) {
                            v8_11.width = v20;
                        }
                        int v2_119 = v3_65.getMeasuredWidth();
                        if (v9_0 == 0) {
                            int v4_45 = this.f;
                            this.f = Math.max(v4_45, ((((v4_45 + v2_119) + v8_11.leftMargin) + v8_11.rightMargin) + 0));
                        } else {
                            this.f = (this.f + (((v8_11.leftMargin + v2_119) + v8_11.rightMargin) + 0));
                        }
                        if (!v27) {
                            v7_19 = v11_0;
                            v11_9 = v12_0;
                        } else {
                            v7_19 = Math.max(v2_119, v11_0);
                            v11_9 = v12_0;
                        }
                    } else {
                        if (v9_0 == 0) {
                            int v2_120 = this.f;
                            this.f = Math.max(v2_120, ((v8_11.leftMargin + v2_120) + v8_11.rightMargin));
                        } else {
                            this.f = (this.f + (v8_11.leftMargin + v8_11.rightMargin));
                        }
                        if (!v26) {
                            v7_19 = v11_0;
                            v11_9 = 1;
                        } else {
                            v3_65.measure(android.view.View$MeasureSpec.makeMeasureSpec(0, 0), android.view.View$MeasureSpec.makeMeasureSpec(0, 0));
                            v7_19 = v11_0;
                            v11_9 = v12_0;
                        }
                    }
                    int v2_126 = 0;
                    if ((v23 == 1073741824) || (v8_11.height != -1)) {
                        v4_38 = v10_0;
                    } else {
                        v4_38 = 1;
                        v2_126 = 1;
                    }
                    int v5_39 = (v8_11.topMargin + v8_11.bottomMargin);
                    int v6_35 = (v3_65.getMeasuredHeight() + v5_39);
                    v10_2 = android.support.v7.internal.widget.bd.a(v17_0, android.support.v4.view.cx.j(v3_65));
                    if (v26) {
                        int v12_4 = v3_65.getBaseline();
                        if (v12_4 != -1) {
                            int v3_69;
                            if (v8_11.h >= 0) {
                                v3_69 = v8_11.h;
                            } else {
                                v3_69 = this.e;
                            }
                            int v3_73 = ((((v3_69 & 112) >> 4) & -2) >> 1);
                            v24[v3_73] = Math.max(v24[v3_73], v12_4);
                            v25[v3_73] = Math.max(v25[v3_73], (v6_35 - v12_4));
                        }
                    }
                    int v3_75;
                    int v12_7 = Math.max(v18_0, v6_35);
                    if ((v14_0 == 0) || (v8_11.height != -1)) {
                        v3_75 = 0;
                    } else {
                        v3_75 = 1;
                    }
                    if (v8_11.g <= 0) {
                        if (v2_126 == 0) {
                            v5_39 = v6_35;
                        }
                        v5_23 = v13_6;
                        v6_28 = v3_75;
                        v8_9 = Math.max(v16_0, v5_39);
                        v3_66 = v11_9;
                        v2_104 = v7_19;
                        v7_17 = v15_0;
                        v11_8 = v12_7;
                    } else {
                        int v2_128;
                        if (v2_126 == 0) {
                            v2_128 = v6_35;
                        } else {
                            v2_128 = v5_39;
                        }
                        v5_23 = v13_6;
                        v6_28 = v3_75;
                        v8_9 = v16_0;
                        v3_66 = v11_9;
                        v11_8 = v12_7;
                        v7_17 = Math.max(v15_0, v2_128);
                        v2_104 = v7_19;
                    }
                }
                v14_0 = v6_28;
                v15_0 = v7_17;
                v16_0 = v8_9;
                v17_0 = v10_2;
                v18_0 = v11_8;
                v11_0 = v2_104;
                v10_0 = v4_38;
                v2_130 = (v19 + 0);
                v4_0 = v5_23;
                v12_0 = v3_66;
            } else {
                this.f = (this.f + 0);
                v2_130 = v19;
            }
            v19 = (v2_130 + 1);
        }
        if ((this.f > 0) && (this.b(v21))) {
            this.f = (this.f + this.w);
        }
        if ((v24[1] == -1) && ((v24[0] == -1) && ((v24[2] == -1) && (v24[3] == -1)))) {
            int v5_2 = v18_0;
        } else {
            v5_2 = Math.max(v18_0, (Math.max(v24[3], Math.max(v24[0], Math.max(v24[1], v24[2]))) + Math.max(v25[3], Math.max(v25[0], Math.max(v25[1], v25[2])))));
        }
        if ((v27) && ((v22 == -2147483648) || (v22 == 0))) {
            this.f = 0;
            int v3_13 = 0;
            while (v3_13 < v21) {
                int v2_96;
                int v2_89 = this.getChildAt(v3_13);
                if (v2_89 != 0) {
                    if (v2_89.getVisibility() != 8) {
                        int v2_91 = ((android.support.v7.widget.al) v2_89.getLayoutParams());
                        if (v9_0 == 0) {
                            int v6_26 = this.f;
                            this.f = Math.max(v6_26, ((v2_91.rightMargin + ((v6_26 + v11_0) + v2_91.leftMargin)) + 0));
                            v2_96 = v3_13;
                        } else {
                            this.f = (((v2_91.rightMargin + (v2_91.leftMargin + v11_0)) + 0) + this.f);
                            v2_96 = v3_13;
                        }
                    } else {
                        v2_96 = (v3_13 + 0);
                    }
                } else {
                    this.f = (this.f + 0);
                    v2_96 = v3_13;
                }
                v3_13 = (v2_96 + 1);
            }
        }
        int v2_40;
        int v3_20;
        this.f = (this.f + (this.getPaddingLeft() + this.getPaddingRight()));
        int v18_1 = android.support.v4.view.cx.a(Math.max(this.f, this.getSuggestedMinimumWidth()), p30, 0);
        int v6_8 = ((16777215 & v18_1) - this.f);
        if ((v12_0 == 0) && ((v6_8 == 0) || (v4_0 <= 0))) {
            int v12_1 = Math.max(v16_0, v15_0);
            if ((v27) && (v22 != 1073741824)) {
                int v3_19 = 0;
                while (v3_19 < v21) {
                    int v4_1 = this.getChildAt(v3_19);
                    if ((v4_1 != 0) && ((v4_1.getVisibility() != 8) && (((android.support.v7.widget.al) v4_1.getLayoutParams()).g > 0))) {
                        v4_1.measure(android.view.View$MeasureSpec.makeMeasureSpec(v11_0, 1073741824), android.view.View$MeasureSpec.makeMeasureSpec(v4_1.getMeasuredHeight(), 1073741824));
                    }
                    v3_19++;
                }
            }
            v2_40 = v12_1;
            v3_20 = v5_2;
        } else {
            if (this.g > 0) {
                v4_0 = this.g;
            }
            v24[3] = -1;
            v24[2] = -1;
            v24[1] = -1;
            v24[0] = -1;
            v25[3] = -1;
            v25[2] = -1;
            v25[1] = -1;
            v25[0] = -1;
            this.f = 0;
            int v15_1 = 0;
            int v11_1 = v14_0;
            int v12_2 = v16_0;
            int v14_1 = -1;
            int v7_8 = v17_0;
            while (v15_1 < v21) {
                int v6_16;
                int v7_10;
                int v5_18;
                int v2_81;
                int v4_15;
                int v3_45;
                int v5_17 = this.getChildAt(v15_1);
                if ((v5_17 == 0) || (v5_17.getVisibility() == 8)) {
                    v2_81 = v4_0;
                    v3_45 = v6_8;
                    v5_18 = v12_2;
                    v4_15 = v11_1;
                    v6_16 = v7_8;
                    v7_10 = v14_1;
                } else {
                    int v7_11;
                    int v13_0;
                    int v8_5;
                    int v2_83 = ((android.support.v7.widget.al) v5_17.getLayoutParams());
                    int v8_4 = v2_83.g;
                    if (v8_4 <= 0) {
                        v8_5 = v6_8;
                        v13_0 = v7_8;
                        v7_11 = v4_0;
                    } else {
                        int v4_22;
                        int v3_51 = ((int) ((((float) v6_8) * v8_4) / v4_0));
                        int v8_6 = (v4_0 - v8_4);
                        int v6_17 = (v6_8 - v3_51);
                        int v13_5 = android.support.v7.widget.aj.getChildMeasureSpec(p31, (((this.getPaddingTop() + this.getPaddingBottom()) + v2_83.topMargin) + v2_83.bottomMargin), v2_83.height);
                        if ((v2_83.width == 0) && (v22 == 1073741824)) {
                            if (v3_51 <= 0) {
                                v3_51 = 0;
                                v4_22 = v5_17;
                            } else {
                                v4_22 = v5_17;
                            }
                        } else {
                            v3_51 += v5_17.getMeasuredWidth();
                            if (v3_51 < 0) {
                                v3_51 = 0;
                            }
                            v4_22 = v5_17;
                        }
                        v4_22.measure(android.view.View$MeasureSpec.makeMeasureSpec(v3_51, 1073741824), v13_5);
                        v13_0 = android.support.v7.internal.widget.bd.a(v7_8, (android.support.v4.view.cx.j(v5_17) & -16777216));
                        v7_11 = v8_6;
                        v8_5 = v6_17;
                    }
                    if (v9_0 == 0) {
                        int v3_55 = this.f;
                        this.f = Math.max(v3_55, ((((v5_17.getMeasuredWidth() + v3_55) + v2_83.leftMargin) + v2_83.rightMargin) + 0));
                    } else {
                        this.f = (this.f + (((v5_17.getMeasuredWidth() + v2_83.leftMargin) + v2_83.rightMargin) + 0));
                    }
                    if ((v23 == 1073741824) || (v2_83.height != -1)) {
                        int v3_61 = 0;
                    } else {
                        v3_61 = 1;
                    }
                    int v3_62;
                    int v4_36 = (v2_83.topMargin + v2_83.bottomMargin);
                    int v6_24 = (v5_17.getMeasuredHeight() + v4_36);
                    int v14_2 = Math.max(v14_1, v6_24);
                    if (v3_61 == 0) {
                        v3_62 = v6_24;
                    } else {
                        v3_62 = v4_36;
                    }
                    int v3_64;
                    int v4_37 = Math.max(v12_2, v3_62);
                    if ((v11_1 == 0) || (v2_83.height != -1)) {
                        v3_64 = 0;
                    } else {
                        v3_64 = 1;
                    }
                    if (v26) {
                        int v5_19 = v5_17.getBaseline();
                        if (v5_19 != -1) {
                            int v2_84;
                            if (v2_83.h >= 0) {
                                v2_84 = v2_83.h;
                            } else {
                                v2_84 = this.e;
                            }
                            int v2_88 = ((((v2_84 & 112) >> 4) & -2) >> 1);
                            v24[v2_88] = Math.max(v24[v2_88], v5_19);
                            v25[v2_88] = Math.max(v25[v2_88], (v6_24 - v5_19));
                        }
                    }
                    v2_81 = v7_11;
                    v5_18 = v4_37;
                    v6_16 = v13_0;
                    v4_15 = v3_64;
                    v7_10 = v14_2;
                    v3_45 = v8_5;
                }
                v15_1++;
                v11_1 = v4_15;
                v12_2 = v5_18;
                v14_1 = v7_10;
                v7_8 = v6_16;
                v4_0 = v2_81;
                v6_8 = v3_45;
            }
            this.f = (this.f + (this.getPaddingLeft() + this.getPaddingRight()));
            if ((v24[1] != -1) || ((v24[0] != -1) || ((v24[2] != -1) || (v24[3] != -1)))) {
                v14_1 = Math.max(v14_1, (Math.max(v24[3], Math.max(v24[0], Math.max(v24[1], v24[2]))) + Math.max(v25[3], Math.max(v25[0], Math.max(v25[1], v25[2])))));
            }
            v2_40 = v12_2;
            v17_0 = v7_8;
            v3_20 = v14_1;
            v14_0 = v11_1;
        }
        if ((v14_0 != 0) || (v23 == 1073741824)) {
            v2_40 = v3_20;
        }
        this.setMeasuredDimension(((-16777216 & v17_0) | v18_1), android.support.v4.view.cx.a(Math.max((v2_40 + (this.getPaddingTop() + this.getPaddingBottom())), this.getSuggestedMinimumHeight()), p31, (v17_0 << 16)));
        if (v10_0 != 0) {
            int v6_15 = android.view.View$MeasureSpec.makeMeasureSpec(this.getMeasuredHeight(), 1073741824);
            int v9_1 = 0;
            while (v9_1 < v21) {
                int v3_43 = this.getChildAt(v9_1);
                if (v3_43.getVisibility() != 8) {
                    int v8_3 = ((android.support.v7.widget.al) v3_43.getLayoutParams());
                    if (v8_3.height == -1) {
                        int v10_1 = v8_3.width;
                        v8_3.width = v3_43.getMeasuredWidth();
                        this.measureChildWithMargins(v3_43, p30, 0, v6_15, 0);
                        v8_3.width = v10_1;
                    }
                }
                v9_1++;
            }
        }
        return;
    }

    private static int d()
    {
        return 0;
    }

    private void d(int p10, int p11)
    {
        int v4 = android.view.View$MeasureSpec.makeMeasureSpec(this.getMeasuredHeight(), 1073741824);
        int v7 = 0;
        while (v7 < p10) {
            android.view.View v1_1 = this.getChildAt(v7);
            if (v1_1.getVisibility() != 8) {
                android.support.v7.widget.al v6_1 = ((android.support.v7.widget.al) v1_1.getLayoutParams());
                if (v6_1.height == -1) {
                    int v8 = v6_1.width;
                    v6_1.width = v1_1.getMeasuredWidth();
                    this.measureChildWithMargins(v1_1, p11, 0, v4, 0);
                    v6_1.width = v8;
                }
            }
            v7++;
        }
        return;
    }

    private static int getChildrenSkipCount$5359dca7()
    {
        return 0;
    }

    private static int getLocationOffset$3c7ec8d0()
    {
        return 0;
    }

    private static int getNextLocationOffset$3c7ec8d0()
    {
        return 0;
    }

    public android.support.v7.widget.al a(android.util.AttributeSet p3)
    {
        return new android.support.v7.widget.al(this.getContext(), p3);
    }

    protected android.support.v7.widget.al b(android.view.ViewGroup$LayoutParams p2)
    {
        return new android.support.v7.widget.al(p2);
    }

    protected android.support.v7.widget.al c()
    {
        int v0_2;
        if (this.d != 0) {
            if (this.d != 1) {
                v0_2 = 0;
            } else {
                v0_2 = new android.support.v7.widget.al(-1, -2);
            }
        } else {
            v0_2 = new android.support.v7.widget.al(-2, -2);
        }
        return v0_2;
    }

    protected boolean checkLayoutParams(android.view.ViewGroup$LayoutParams p2)
    {
        return (p2 instanceof android.support.v7.widget.al);
    }

    protected synthetic android.view.ViewGroup$LayoutParams generateDefaultLayoutParams()
    {
        return this.c();
    }

    public synthetic android.view.ViewGroup$LayoutParams generateLayoutParams(android.util.AttributeSet p2)
    {
        return this.a(p2);
    }

    protected synthetic android.view.ViewGroup$LayoutParams generateLayoutParams(android.view.ViewGroup$LayoutParams p2)
    {
        return this.b(p2);
    }

    public int getBaseline()
    {
        int v0_0 = -1;
        if (this.b >= 0) {
            if (this.getChildCount() > this.b) {
                android.view.View v2_1 = this.getChildAt(this.b);
                int v3 = v2_1.getBaseline();
                if (v3 != -1) {
                    int v1_9;
                    int v0_1 = this.c;
                    if (this.d != 1) {
                        v1_9 = v0_1;
                    } else {
                        int v1_5 = (this.e & 112);
                        if (v1_5 == 48) {
                        } else {
                            switch (v1_5) {
                                case 16:
                                    v1_9 = (v0_1 + (((((this.getBottom() - this.getTop()) - this.getPaddingTop()) - this.getPaddingBottom()) - this.f) / 2));
                                    break;
                                case 80:
                                    v1_9 = (((this.getBottom() - this.getTop()) - this.getPaddingBottom()) - this.f);
                                    break;
                                default:
                            }
                        }
                    }
                    v0_0 = ((((android.support.v7.widget.al) v2_1.getLayoutParams()).topMargin + v1_9) + v3);
                } else {
                    if (this.b != 0) {
                        throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout points to a View that doesn\'t know how to get its baseline.");
                    }
                }
            } else {
                throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout set to an index that is out of bounds.");
            }
        } else {
            v0_0 = super.getBaseline();
        }
        return v0_0;
    }

    public int getBaselineAlignedChildIndex()
    {
        return this.b;
    }

    public android.graphics.drawable.Drawable getDividerDrawable()
    {
        return this.v;
    }

    public int getDividerPadding()
    {
        return this.z;
    }

    public int getDividerWidth()
    {
        return this.w;
    }

    public int getOrientation()
    {
        return this.d;
    }

    public int getShowDividers()
    {
        return this.y;
    }

    int getVirtualChildCount()
    {
        return this.getChildCount();
    }

    public float getWeightSum()
    {
        return this.g;
    }

    protected void onDraw(android.graphics.Canvas p7)
    {
        if (this.v != null) {
            if (this.d != 1) {
                int v2_1 = this.getVirtualChildCount();
                int v3_0 = android.support.v7.internal.widget.bd.a(this);
                int v1_2 = 0;
                while (v1_2 < v2_1) {
                    int v4_0 = this.getChildAt(v1_2);
                    if ((v4_0 != 0) && ((v4_0.getVisibility() != 8) && (this.b(v1_2)))) {
                        int v0_17;
                        int v0_14 = ((android.support.v7.widget.al) v4_0.getLayoutParams());
                        if (v3_0 == 0) {
                            v0_17 = ((v4_0.getLeft() - v0_14.leftMargin) - this.w);
                        } else {
                            v0_17 = (v0_14.rightMargin + v4_0.getRight());
                        }
                        this.b(p7, v0_17);
                    }
                    v1_2++;
                }
                if (this.b(v2_1)) {
                    int v0_6;
                    int v1_3 = this.getChildAt((v2_1 - 1));
                    if (v1_3 != 0) {
                        int v0_4 = ((android.support.v7.widget.al) v1_3.getLayoutParams());
                        if (v3_0 == 0) {
                            v0_6 = (v0_4.rightMargin + v1_3.getRight());
                        } else {
                            v0_6 = ((v1_3.getLeft() - v0_4.leftMargin) - this.w);
                        }
                    } else {
                        if (v3_0 == 0) {
                            v0_6 = ((this.getWidth() - this.getPaddingRight()) - this.w);
                        } else {
                            v0_6 = this.getPaddingLeft();
                        }
                    }
                    this.b(p7, v0_6);
                }
            } else {
                int v2_2 = this.getVirtualChildCount();
                int v1_9 = 0;
                while (v1_9 < v2_2) {
                    int v3_1 = this.getChildAt(v1_9);
                    if ((v3_1 != 0) && ((v3_1.getVisibility() != 8) && (this.b(v1_9)))) {
                        this.a(p7, ((v3_1.getTop() - ((android.support.v7.widget.al) v3_1.getLayoutParams()).topMargin) - this.x));
                    }
                    v1_9++;
                }
                if (this.b(v2_2)) {
                    int v0_25;
                    int v1_10 = this.getChildAt((v2_2 - 1));
                    if (v1_10 != 0) {
                        v0_25 = (((android.support.v7.widget.al) v1_10.getLayoutParams()).bottomMargin + v1_10.getBottom());
                    } else {
                        v0_25 = ((this.getHeight() - this.getPaddingBottom()) - this.x);
                    }
                    this.a(p7, v0_25);
                }
            }
        }
        return;
    }

    public void onInitializeAccessibilityEvent(android.view.accessibility.AccessibilityEvent p3)
    {
        if (android.os.Build$VERSION.SDK_INT >= 14) {
            super.onInitializeAccessibilityEvent(p3);
            p3.setClassName(android.support.v7.widget.aj.getName());
        }
        return;
    }

    public void onInitializeAccessibilityNodeInfo(android.view.accessibility.AccessibilityNodeInfo p3)
    {
        if (android.os.Build$VERSION.SDK_INT >= 14) {
            super.onInitializeAccessibilityNodeInfo(p3);
            p3.setClassName(android.support.v7.widget.aj.getName());
        }
        return;
    }

    protected void onLayout(boolean p24, int p25, int p26, int p27, int p28)
    {
        if (this.d != 1) {
            int v9_0;
            int v5_0 = android.support.v7.internal.widget.bd.a(this);
            int v8_0 = this.getPaddingTop();
            int v3_1 = (p28 - p26);
            android.view.View v12_0 = (v3_1 - this.getPaddingBottom());
            int v13_0 = ((v3_1 - v8_0) - this.getPaddingBottom());
            int v14_0 = this.getVirtualChildCount();
            int v11_0 = (this.e & 112);
            int v15_0 = this.a;
            int[] v16 = this.o;
            int[] v17 = this.p;
            switch (android.support.v4.view.v.a((this.e & 8388615), android.support.v4.view.cx.f(this))) {
                case 1:
                    v9_0 = (this.getPaddingLeft() + (((p27 - p25) - this.f) / 2));
                    break;
                case 5:
                    v9_0 = (((this.getPaddingLeft() + p27) - p25) - this.f);
                    break;
                default:
                    v9_0 = this.getPaddingLeft();
            }
            int v4_11;
            int v5_1;
            if (v5_0 == 0) {
                v5_1 = 0;
                v4_11 = 1;
            } else {
                v5_1 = (v14_0 - 1);
                v4_11 = -1;
            }
            int v10_0 = 0;
            while (v10_0 < v14_0) {
                int v3_14;
                int v18 = (v5_1 + (v4_11 * v10_0));
                android.view.View v19 = this.getChildAt(v18);
                if (v19 != null) {
                    if (v19.getVisibility() == 8) {
                        v3_14 = v10_0;
                    } else {
                        int v20 = v19.getMeasuredWidth();
                        int v21 = v19.getMeasuredHeight();
                        int v6_2 = -1;
                        int v3_16 = ((android.support.v7.widget.al) v19.getLayoutParams());
                        if ((v15_0 != 0) && (v3_16.height != -1)) {
                            v6_2 = v19.getBaseline();
                        }
                        int v7_1 = v3_16.h;
                        if (v7_1 < 0) {
                            v7_1 = v11_0;
                        }
                        int v6_5;
                        int v7_4;
                        switch ((v7_1 & 112)) {
                            case 16:
                                v6_5 = (((((v13_0 - v21) / 2) + v8_0) + v3_16.topMargin) - v3_16.bottomMargin);
                                break;
                            case 48:
                                v7_4 = (v3_16.topMargin + v8_0);
                                if (v6_2 == -1) {
                                    v6_5 = v7_4;
                                } else {
                                    v6_5 = ((v16[1] - v6_2) + v7_4);
                                }
                                break;
                            case 80:
                                v7_4 = ((v12_0 - v21) - v3_16.bottomMargin);
                                if (v6_2 == -1) {
                                } else {
                                    v6_5 = (v7_4 - (v17[2] - (v19.getMeasuredHeight() - v6_2)));
                                }
                                break;
                            default:
                                v6_5 = v8_0;
                        }
                        int v7_9;
                        if (!this.b(v18)) {
                            v7_9 = v9_0;
                        } else {
                            v7_9 = (this.w + v9_0);
                        }
                        int v7_11 = (v7_9 + v3_16.leftMargin);
                        android.support.v7.widget.aj.b(v19, (v7_11 + 0), v6_5, v20, v21);
                        v9_0 = (v7_11 + ((v3_16.rightMargin + v20) + 0));
                        v3_14 = (v10_0 + 0);
                    }
                } else {
                    v9_0 += 0;
                    v3_14 = v10_0;
                }
                v10_0 = (v3_14 + 1);
            }
        } else {
            int v3_27;
            int v8_1 = this.getPaddingLeft();
            int v3_20 = (p27 - p25);
            int v9_3 = (v3_20 - this.getPaddingRight());
            int v10_1 = ((v3_20 - v8_1) - this.getPaddingRight());
            int v11_1 = this.getVirtualChildCount();
            int v5_3 = (8388615 & this.e);
            switch ((this.e & 112)) {
                case 16:
                    v3_27 = (this.getPaddingTop() + (((p28 - p26) - this.f) / 2));
                    break;
                case 80:
                    v3_27 = (((this.getPaddingTop() + p28) - p26) - this.f);
                    break;
                default:
                    v3_27 = this.getPaddingTop();
            }
            int v7_12 = 0;
            int v6_12 = v3_27;
            while (v7_12 < v11_1) {
                int v3_30;
                android.view.View v12_1 = this.getChildAt(v7_12);
                if (v12_1 != null) {
                    if (v12_1.getVisibility() == 8) {
                        v3_30 = v7_12;
                    } else {
                        int v13_1 = v12_1.getMeasuredWidth();
                        int v14_1 = v12_1.getMeasuredHeight();
                        int v3_32 = ((android.support.v7.widget.al) v12_1.getLayoutParams());
                        int v4_21 = v3_32.h;
                        if (v4_21 < 0) {
                            v4_21 = v5_3;
                        }
                        int v4_25;
                        switch ((android.support.v4.view.v.a(v4_21, android.support.v4.view.cx.f(this)) & 7)) {
                            case 1:
                                v4_25 = (((((v10_1 - v13_1) / 2) + v8_1) + v3_32.leftMargin) - v3_32.rightMargin);
                                break;
                            case 5:
                                v4_25 = ((v9_3 - v13_1) - v3_32.rightMargin);
                                break;
                            default:
                                v4_25 = (v3_32.leftMargin + v8_1);
                        }
                        if (this.b(v7_12)) {
                            v6_12 += this.x;
                        }
                        int v6_13 = (v6_12 + v3_32.topMargin);
                        android.support.v7.widget.aj.b(v12_1, v4_25, (v6_13 + 0), v13_1, v14_1);
                        v6_12 = (v6_13 + ((v3_32.bottomMargin + v14_1) + 0));
                        v3_30 = (v7_12 + 0);
                    }
                } else {
                    v6_12 += 0;
                    v3_30 = v7_12;
                }
                v7_12 = (v3_30 + 1);
            }
        }
        return;
    }

    public void onMeasure(int p28, int p29)
    {
        if (this.d != 1) {
            this.c(p28, p29);
        } else {
            this.f = 0;
            int v18 = 0;
            int v17 = 0;
            int v16_0 = 0;
            int v15_0 = 0;
            int v14_0 = 1;
            int v5_0 = 0;
            int v21 = this.getVirtualChildCount();
            int v22 = android.view.View$MeasureSpec.getMode(p28);
            int v23 = android.view.View$MeasureSpec.getMode(p29);
            int v10_0 = 0;
            int v12_0 = 0;
            int v24 = this.b;
            boolean v25 = this.n;
            int v11_0 = -2147483648;
            int v19_0 = 0;
            while (v19_0 < v21) {
                int v3_81;
                int v4_28 = this.getChildAt(v19_0);
                if (v4_28 != 0) {
                    int v11_3;
                    int v5_11;
                    int v6_19;
                    int v3_55;
                    int v4_29;
                    int v10_1;
                    int v8_7;
                    int v9_6;
                    int v7_8;
                    if (v4_28.getVisibility() == 8) {
                        v3_55 = v11_0;
                        v4_29 = v12_0;
                        v6_19 = v5_0;
                        v7_8 = v14_0;
                        v8_7 = v15_0;
                        v9_6 = v16_0;
                        v5_11 = v10_0;
                        v11_3 = v18;
                        v10_1 = v17;
                    } else {
                        if (this.b(v19_0)) {
                            this.f = (this.f + this.x);
                        }
                        int v11_4;
                        int v8_9;
                        int v9_8 = ((android.support.v7.widget.al) v4_28.getLayoutParams());
                        int v13_3 = (v5_0 + v9_8.g);
                        if ((v23 != 1073741824) || ((v9_8.height != 0) || (v9_8.g <= 0))) {
                            int v3_65 = -2147483648;
                            if ((v9_8.height == 0) && (v9_8.g > 0)) {
                                v3_65 = 0;
                                v9_8.height = -2;
                            }
                            int v8_8;
                            float v20 = v3_65;
                            if (v13_3 != 0) {
                                v8_8 = 0;
                            } else {
                                v8_8 = this.f;
                            }
                            this.a(v4_28, p28, 0, p29, v8_8);
                            if (v20 != -2147483648) {
                                v9_8.height = v20;
                            }
                            int v3_70 = v4_28.getMeasuredHeight();
                            int v5_18 = this.f;
                            this.f = Math.max(v5_18, ((((v5_18 + v3_70) + v9_8.topMargin) + v9_8.bottomMargin) + 0));
                            if (!v25) {
                                v8_9 = v11_0;
                                v11_4 = v12_0;
                            } else {
                                v8_9 = Math.max(v3_70, v11_0);
                                v11_4 = v12_0;
                            }
                        } else {
                            int v3_71 = this.f;
                            this.f = Math.max(v3_71, ((v9_8.topMargin + v3_71) + v9_8.bottomMargin));
                            v8_9 = v11_0;
                            v11_4 = 1;
                        }
                        if ((v24 >= 0) && (v24 == (v19_0 + 1))) {
                            this.c = this.f;
                        }
                        if ((v19_0 >= v24) || (v9_8.g <= 0)) {
                            int v3_77 = 0;
                            if ((v22 == 1073741824) || (v9_8.width != -1)) {
                                v5_11 = v10_0;
                            } else {
                                v5_11 = 1;
                                v3_77 = 1;
                            }
                            int v4_32;
                            int v6_30 = (v9_8.leftMargin + v9_8.rightMargin);
                            int v7_14 = (v4_28.getMeasuredWidth() + v6_30);
                            int v12_5 = Math.max(v18, v7_14);
                            v10_1 = android.support.v7.internal.widget.bd.a(v17, android.support.v4.view.cx.j(v4_28));
                            if ((v14_0 == 0) || (v9_8.width != -1)) {
                                v4_32 = 0;
                            } else {
                                v4_32 = 1;
                            }
                            if (v9_8.g <= 0) {
                                if (v3_77 == 0) {
                                    v6_30 = v7_14;
                                }
                                v6_19 = v13_3;
                                v7_8 = v4_32;
                                v9_6 = Math.max(v16_0, v6_30);
                                v4_29 = v11_4;
                                v3_55 = v8_9;
                                v8_7 = v15_0;
                                v11_3 = v12_5;
                            } else {
                                int v3_79;
                                if (v3_77 == 0) {
                                    v3_79 = v7_14;
                                } else {
                                    v3_79 = v6_30;
                                }
                                v6_19 = v13_3;
                                v7_8 = v4_32;
                                v9_6 = v16_0;
                                v4_29 = v11_4;
                                v11_3 = v12_5;
                                v8_7 = Math.max(v15_0, v3_79);
                                v3_55 = v8_9;
                            }
                        } else {
                            throw new RuntimeException("A child of LinearLayout with index less than mBaselineAlignedChildIndex has weight > 0, which won\'t work.  Either remove the weight, or don\'t set mBaselineAlignedChildIndex.");
                        }
                    }
                    v14_0 = v7_8;
                    v15_0 = v8_7;
                    v16_0 = v9_6;
                    v17 = v10_1;
                    v18 = v11_3;
                    v11_0 = v3_55;
                    v10_0 = v5_11;
                    v3_81 = (v19_0 + 0);
                    v5_0 = v6_19;
                    v12_0 = v4_29;
                } else {
                    this.f = (this.f + 0);
                    v3_81 = v19_0;
                }
                v19_0 = (v3_81 + 1);
            }
            if ((this.f > 0) && (this.b(v21))) {
                this.f = (this.f + this.x);
            }
            if ((v25) && ((v23 == -2147483648) || (v23 == 0))) {
                this.f = 0;
                int v4_2 = 0;
                while (v4_2 < v21) {
                    int v3_51;
                    int v3_44 = this.getChildAt(v4_2);
                    if (v3_44 != 0) {
                        if (v3_44.getVisibility() != 8) {
                            int v3_46 = ((android.support.v7.widget.al) v3_44.getLayoutParams());
                            int v6_17 = this.f;
                            this.f = Math.max(v6_17, ((v3_46.bottomMargin + ((v6_17 + v11_0) + v3_46.topMargin)) + 0));
                            v3_51 = v4_2;
                        } else {
                            v3_51 = (v4_2 + 0);
                        }
                    } else {
                        this.f = (this.f + 0);
                        v3_51 = v4_2;
                    }
                    v4_2 = (v3_51 + 1);
                }
            }
            int v3_18;
            int v4_9;
            this.f = (this.f + (this.getPaddingTop() + this.getPaddingBottom()));
            int v19_1 = android.support.v4.view.cx.a(Math.max(this.f, this.getSuggestedMinimumHeight()), p29, 0);
            int v6_1 = ((16777215 & v19_1) - this.f);
            if ((v12_0 == 0) && ((v6_1 == 0) || (v5_0 <= 0))) {
                int v13_0 = Math.max(v16_0, v15_0);
                if ((v25) && (v23 != 1073741824)) {
                    int v4_8 = 0;
                    while (v4_8 < v21) {
                        int v5_1 = this.getChildAt(v4_8);
                        if ((v5_1 != 0) && ((v5_1.getVisibility() != 8) && (((android.support.v7.widget.al) v5_1.getLayoutParams()).g > 0))) {
                            v5_1.measure(android.view.View$MeasureSpec.makeMeasureSpec(v5_1.getMeasuredWidth(), 1073741824), android.view.View$MeasureSpec.makeMeasureSpec(v11_0, 1073741824));
                        }
                        v4_8++;
                    }
                }
                v3_18 = v13_0;
                v4_9 = v18;
            } else {
                if (this.g > 0) {
                    v5_0 = this.g;
                }
                this.f = 0;
                int v15_1 = 0;
                int v12_1 = v14_0;
                int v13_1 = v16_0;
                int v11_1 = v17;
                int v14_1 = v18;
                while (v15_1 < v21) {
                    int v3_37;
                    int v5_5;
                    int v4_17;
                    int v8_0;
                    int v6_7;
                    int v7_1;
                    int v7_0 = this.getChildAt(v15_1);
                    if (v7_0.getVisibility() == 8) {
                        v4_17 = v5_0;
                        v8_0 = v12_1;
                        v3_37 = v13_1;
                        v7_1 = v14_1;
                        v5_5 = v6_1;
                        v6_7 = v11_1;
                    } else {
                        int v3_39 = ((android.support.v7.widget.al) v7_0.getLayoutParams());
                        int v8_1 = v3_39.g;
                        if (v8_1 <= 0) {
                            v4_17 = v5_0;
                            v5_5 = v6_1;
                            v6_7 = v11_1;
                        } else {
                            int v6_14;
                            int v4_23 = ((int) ((((float) v6_1) * v8_1) / v5_0));
                            int v8_2 = (v5_0 - v8_1);
                            int v9_0 = (v6_1 - v4_23);
                            int v5_10 = android.support.v7.widget.aj.getChildMeasureSpec(p28, (((this.getPaddingLeft() + this.getPaddingRight()) + v3_39.leftMargin) + v3_39.rightMargin), v3_39.width);
                            if ((v3_39.height == 0) && (v23 == 1073741824)) {
                                if (v4_23 <= 0) {
                                    v4_23 = 0;
                                    v6_14 = v7_0;
                                } else {
                                    v6_14 = v7_0;
                                }
                            } else {
                                v4_23 += v7_0.getMeasuredHeight();
                                if (v4_23 < 0) {
                                    v4_23 = 0;
                                }
                                v6_14 = v7_0;
                            }
                            v6_14.measure(v5_10, android.view.View$MeasureSpec.makeMeasureSpec(v4_23, 1073741824));
                            v5_5 = v9_0;
                            v6_7 = android.support.v7.internal.widget.bd.a(v11_1, (android.support.v4.view.cx.j(v7_0) & -256));
                            v4_17 = v8_2;
                        }
                        int v14_4;
                        int v8_4 = (v3_39.leftMargin + v3_39.rightMargin);
                        int v9_3 = (v7_0.getMeasuredWidth() + v8_4);
                        int v11_2 = Math.max(v14_1, v9_3);
                        if ((v22 == 1073741824) || (v3_39.width != -1)) {
                            v14_4 = 0;
                        } else {
                            v14_4 = 1;
                        }
                        if (v14_4 == 0) {
                            v8_4 = v9_3;
                        }
                        int v9_4 = Math.max(v13_1, v8_4);
                        if ((v12_1 == 0) || (v3_39.width != -1)) {
                            v8_0 = 0;
                        } else {
                            v8_0 = 1;
                        }
                        int v12_3 = this.f;
                        this.f = Math.max(v12_3, ((v3_39.bottomMargin + ((v7_0.getMeasuredHeight() + v12_3) + v3_39.topMargin)) + 0));
                        v3_37 = v9_4;
                        v7_1 = v11_2;
                    }
                    v15_1++;
                    v12_1 = v8_0;
                    v13_1 = v3_37;
                    v11_1 = v6_7;
                    v14_1 = v7_1;
                    v6_1 = v5_5;
                    v5_0 = v4_17;
                }
                this.f = (this.f + (this.getPaddingTop() + this.getPaddingBottom()));
                v3_18 = v13_1;
                v17 = v11_1;
                v4_9 = v14_1;
                v14_0 = v12_1;
            }
            if ((v14_0 != 0) || (v22 == 1073741824)) {
                v3_18 = v4_9;
            }
            this.setMeasuredDimension(android.support.v4.view.cx.a(Math.max((v3_18 + (this.getPaddingLeft() + this.getPaddingRight())), this.getSuggestedMinimumWidth()), p28, v17), v19_1);
            if (v10_0 != 0) {
                this.b(v21, p29);
            }
        }
        return;
    }

    public void setBaselineAligned(boolean p1)
    {
        this.a = p1;
        return;
    }

    public void setBaselineAlignedChildIndex(int p4)
    {
        if ((p4 >= 0) && (p4 < this.getChildCount())) {
            this.b = p4;
            return;
        } else {
            throw new IllegalArgumentException(new StringBuilder("base aligned child index out of range (0, ").append(this.getChildCount()).append(")").toString());
        }
    }

    public void setDividerDrawable(android.graphics.drawable.Drawable p3)
    {
        int v0 = 0;
        if (p3 != this.v) {
            this.v = p3;
            if (p3 == null) {
                this.w = 0;
                this.x = 0;
            } else {
                this.w = p3.getIntrinsicWidth();
                this.x = p3.getIntrinsicHeight();
            }
            if (p3 == null) {
                v0 = 1;
            }
            this.setWillNotDraw(v0);
            this.requestLayout();
        }
        return;
    }

    public void setDividerPadding(int p1)
    {
        this.z = p1;
        return;
    }

    public void setGravity(int p3)
    {
        if (this.e != p3) {
            int v0_3;
            if ((8388615 & p3) != 0) {
                v0_3 = p3;
            } else {
                v0_3 = (8388611 | p3);
            }
            if ((v0_3 & 112) == 0) {
                v0_3 |= 48;
            }
            this.e = v0_3;
            this.requestLayout();
        }
        return;
    }

    public void setHorizontalGravity(int p4)
    {
        int v0_0 = (p4 & 8388615);
        if ((this.e & 8388615) != v0_0) {
            this.e = (v0_0 | (this.e & -8388616));
            this.requestLayout();
        }
        return;
    }

    public void setMeasureWithLargestChildEnabled(boolean p1)
    {
        this.n = p1;
        return;
    }

    public void setOrientation(int p2)
    {
        if (this.d != p2) {
            this.d = p2;
            this.requestLayout();
        }
        return;
    }

    public void setShowDividers(int p2)
    {
        if (p2 != this.y) {
            this.requestLayout();
        }
        this.y = p2;
        return;
    }

    public void setVerticalGravity(int p3)
    {
        int v0_0 = (p3 & 112);
        if ((this.e & 112) != v0_0) {
            this.e = (v0_0 | (this.e & -113));
            this.requestLayout();
        }
        return;
    }

    public void setWeightSum(float p2)
    {
        this.g = Math.max(0, p2);
        return;
    }

    public boolean shouldDelayChildPressedState()
    {
        return 0;
    }
}
