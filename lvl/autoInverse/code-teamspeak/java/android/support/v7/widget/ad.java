package android.support.v7.widget;
final class ad extends android.support.v7.widget.an {
    CharSequence a;
    final synthetic android.support.v7.widget.aa b;
    private android.widget.ListAdapter u;
    private final android.graphics.Rect v;

    public ad(android.support.v7.widget.aa p2, android.content.Context p3, android.util.AttributeSet p4, int p5)
    {
        this.b = p2;
        this(p3, p4, p5);
        this.v = new android.graphics.Rect();
        this.l = p2;
        this.c();
        this.k = 0;
        this.m = new android.support.v7.widget.ae(this, p2);
        return;
    }

    static synthetic android.widget.ListAdapter a(android.support.v7.widget.ad p1)
    {
        return p1.u;
    }

    private void a(CharSequence p1)
    {
        this.a = p1;
        return;
    }

    static synthetic boolean a(android.support.v7.widget.ad p1, android.view.View p2)
    {
        if ((!android.support.v4.view.cx.D(p2)) || (!p2.getGlobalVisibleRect(p1.v))) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    private boolean a(android.view.View p2)
    {
        if ((!android.support.v4.view.cx.D(p2)) || (!p2.getGlobalVisibleRect(this.v))) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    static synthetic void b(android.support.v7.widget.ad p0)
    {
        super.b();
        return;
    }

    private CharSequence h()
    {
        return this.a;
    }

    final void a()
    {
        int v1_4;
        int v1_1 = this.c.getBackground();
        if (v1_1 == 0) {
            int v1_3 = android.support.v7.widget.aa.b(this.b);
            android.support.v7.widget.aa.b(this.b).right = 0;
            v1_3.left = 0;
            v1_4 = 0;
        } else {
            int v0_8;
            v1_1.getPadding(android.support.v7.widget.aa.b(this.b));
            if (!android.support.v7.internal.widget.bd.a(this.b)) {
                v0_8 = (- android.support.v7.widget.aa.b(this.b).left);
            } else {
                v0_8 = android.support.v7.widget.aa.b(this.b).right;
            }
            v1_4 = v0_8;
        }
        int v3 = this.b.getPaddingLeft();
        int v4 = this.b.getPaddingRight();
        int v5 = this.b.getWidth();
        if (android.support.v7.widget.aa.c(this.b) != -2) {
            if (android.support.v7.widget.aa.c(this.b) != -1) {
                this.a(android.support.v7.widget.aa.c(this.b));
            } else {
                this.a(((v5 - v3) - v4));
            }
        } else {
            int v2_5 = android.support.v7.widget.aa.a(this.b, ((android.widget.SpinnerAdapter) this.u), this.c.getBackground());
            int v0_30 = ((this.b.getContext().getResources().getDisplayMetrics().widthPixels - android.support.v7.widget.aa.b(this.b).left) - android.support.v7.widget.aa.b(this.b).right);
            if (v2_5 <= v0_30) {
                v0_30 = v2_5;
            }
            this.a(Math.max(v0_30, ((v5 - v3) - v4)));
        }
        int v0_34;
        if (!android.support.v7.internal.widget.bd.a(this.b)) {
            v0_34 = (v1_4 + v3);
        } else {
            v0_34 = (((v5 - v4) - this.e) + v1_4);
        }
        this.f = v0_34;
        return;
    }

    public final void a(android.widget.ListAdapter p1)
    {
        super.a(p1);
        this.u = p1;
        return;
    }

    public final void b()
    {
        android.support.v7.widget.ag v0_1 = this.c.isShowing();
        this.a();
        this.e();
        super.b();
        this.d.setChoiceMode(1);
        android.support.v7.widget.af v1_2 = this.b.getSelectedItemPosition();
        android.support.v7.widget.ar v2 = this.d;
        if ((this.c.isShowing()) && (v2 != null)) {
            android.support.v7.widget.ar.a(v2, 0);
            v2.setSelection(v1_2);
            if ((android.os.Build$VERSION.SDK_INT >= 11) && (v2.getChoiceMode() != 0)) {
                v2.setItemChecked(v1_2, 1);
            }
        }
        if (v0_1 == null) {
            android.support.v7.widget.ag v0_3 = this.b.getViewTreeObserver();
            if (v0_3 != null) {
                android.support.v7.widget.af v1_4 = new android.support.v7.widget.af(this);
                v0_3.addOnGlobalLayoutListener(v1_4);
                this.a(new android.support.v7.widget.ag(this, v1_4));
            }
        }
        return;
    }
}
