package android.support.v7.widget;
final class ah {
    private static final int[] a;
    private static final int[] b;
    private final android.widget.TextView c;

    static ah()
    {
        int[] v0_0 = new int[1];
        v0_0[0] = 16842804;
        android.support.v7.widget.ah.a = v0_0;
        int[] v0_1 = new int[1];
        v0_1[0] = android.support.v7.a.d.textAllCaps;
        android.support.v7.widget.ah.b = v0_1;
        return;
    }

    ah(android.widget.TextView p1)
    {
        this.c = p1;
        return;
    }

    final void a(android.content.Context p4, int p5)
    {
        android.content.res.TypedArray v0_1 = p4.obtainStyledAttributes(p5, android.support.v7.widget.ah.b);
        if (v0_1.hasValue(0)) {
            this.a(v0_1.getBoolean(0, 0));
        }
        v0_1.recycle();
        return;
    }

    final void a(android.util.AttributeSet p7, int p8)
    {
        android.content.res.ColorStateList v0_1 = this.c.getContext();
        int v1_1 = v0_1.obtainStyledAttributes(p7, android.support.v7.widget.ah.a, p8, 0);
        android.widget.TextView v2_0 = v1_1.getResourceId(0, -1);
        v1_1.recycle();
        if (v2_0 != -1) {
            int v1_3 = v0_1.obtainStyledAttributes(v2_0, android.support.v7.a.n.TextAppearance);
            if (v1_3.hasValue(android.support.v7.a.n.TextAppearance_textAllCaps)) {
                this.a(v1_3.getBoolean(android.support.v7.a.n.TextAppearance_textAllCaps, 0));
            }
            v1_3.recycle();
        }
        int v1_5 = v0_1.obtainStyledAttributes(p7, android.support.v7.widget.ah.b, p8, 0);
        if (v1_5.hasValue(0)) {
            this.a(v1_5.getBoolean(0, 0));
        }
        v1_5.recycle();
        int v1_7 = this.c.getTextColors();
        if ((v1_7 != 0) && (!v1_7.isStateful())) {
            android.content.res.ColorStateList v0_2;
            if (android.os.Build$VERSION.SDK_INT >= 21) {
                v0_2 = android.support.v7.internal.widget.ar.a(v0_1, 16842808);
            } else {
                v0_2 = android.support.v7.internal.widget.ar.c(v0_1, 16842808);
            }
            this.c.setTextColor(android.support.v7.internal.widget.ar.a(v1_7.getDefaultColor(), v0_2));
        }
        return;
    }

    final void a(boolean p4)
    {
        int v0_0;
        android.widget.TextView v1 = this.c;
        if (!p4) {
            v0_0 = 0;
        } else {
            v0_0 = new android.support.v7.internal.b.a(this.c.getContext());
        }
        v1.setTransformationMethod(v0_0);
        return;
    }
}
