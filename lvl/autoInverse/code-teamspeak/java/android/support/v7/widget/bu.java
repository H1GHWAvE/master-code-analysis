package android.support.v7.widget;
public final class bu extends android.support.v4.view.n {
    public static final String c = "share_history.xml";
    private static final int f = 4;
    final android.content.Context d;
    String e;
    private int g;
    private final android.support.v7.widget.by h;
    private android.support.v7.widget.bw i;
    private android.support.v7.internal.widget.s j;

    private bu(android.content.Context p3)
    {
        this(p3);
        this.g = 4;
        this.h = new android.support.v7.widget.by(this, 0);
        this.e = "share_history.xml";
        this.d = p3;
        return;
    }

    private static synthetic android.content.Context a(android.support.v7.widget.bu p1)
    {
        return p1.d;
    }

    static void a(android.content.Intent p2)
    {
        if (android.os.Build$VERSION.SDK_INT < 21) {
            p2.addFlags(524288);
        } else {
            p2.addFlags(134742016);
        }
        return;
    }

    private void a(android.support.v7.widget.bw p1)
    {
        this.i = p1;
        this.g();
        return;
    }

    private void a(String p1)
    {
        this.e = p1;
        this.g();
        return;
    }

    private static synthetic String b(android.support.v7.widget.bu p1)
    {
        return p1.e;
    }

    private void b(android.content.Intent p4)
    {
        if (p4 != null) {
            Throwable v0_0 = p4.getAction();
            if (("android.intent.action.SEND".equals(v0_0)) || ("android.intent.action.SEND_MULTIPLE".equals(v0_0))) {
                android.support.v7.widget.bu.a(p4);
            }
        }
        Throwable v0_3 = android.support.v7.internal.widget.l.a(this.d, this.e);
        try {
            if (v0_3.e != p4) {
                v0_3.e = p4;
                v0_3.f = 1;
                v0_3.d();
            } else {
            }
        } catch (Throwable v0_4) {
            throw v0_4;
        }
        return;
    }

    private static synthetic android.support.v7.widget.bw c(android.support.v7.widget.bu p1)
    {
        return p1.i;
    }

    private static synthetic void c(android.content.Intent p0)
    {
        android.support.v7.widget.bu.a(p0);
        return;
    }

    private void g()
    {
        if (this.i != null) {
            if (this.j == null) {
                this.j = new android.support.v7.widget.bx(this, 0);
            }
            try {
                android.support.v7.internal.widget.l.a(this.d, this.e).g = this.j;
            } catch (Throwable v0_6) {
                throw v0_6;
            }
        }
        return;
    }

    public final android.view.View a()
    {
        android.support.v7.internal.widget.ActivityChooserView v0_1 = new android.support.v7.internal.widget.ActivityChooserView(this.d);
        if (!v0_1.isInEditMode()) {
            v0_1.setActivityChooserModel(android.support.v7.internal.widget.l.a(this.d, this.e));
        }
        int v1_5 = new android.util.TypedValue();
        this.d.getTheme().resolveAttribute(android.support.v7.a.d.actionModeShareDrawable, v1_5, 1);
        v0_1.setExpandActivityOverflowButtonDrawable(android.support.v7.internal.widget.av.a(this.d, v1_5.resourceId));
        v0_1.setProvider(this);
        v0_1.setDefaultActionButtonContentDescription(android.support.v7.a.l.abc_shareactionprovider_share_with_application);
        v0_1.setExpandActivityOverflowButtonContentDescription(android.support.v7.a.l.abc_shareactionprovider_share_with);
        return v0_1;
    }

    public final void a(android.view.SubMenu p9)
    {
        p9.clear();
        android.support.v7.internal.widget.l v2_1 = android.support.v7.internal.widget.l.a(this.d, this.e);
        android.content.pm.PackageManager v3 = this.d.getPackageManager();
        int v4 = v2_1.a();
        android.view.SubMenu v5_0 = Math.min(v4, this.g);
        int v0_3 = 0;
        while (v0_3 < v5_0) {
            android.view.MenuItem v6_4 = v2_1.a(v0_3);
            p9.add(0, v0_3, v0_3, v6_4.loadLabel(v3)).setIcon(v6_4.loadIcon(v3)).setOnMenuItemClickListener(this.h);
            v0_3++;
        }
        if (v5_0 < v4) {
            android.view.SubMenu v5_1 = p9.addSubMenu(0, v5_0, v5_0, this.d.getString(android.support.v7.a.l.abc_activity_chooser_view_see_all));
            int v0_6 = 0;
            while (v0_6 < v4) {
                android.view.MenuItem v6_1 = v2_1.a(v0_6);
                v5_1.add(0, v0_6, v0_6, v6_1.loadLabel(v3)).setIcon(v6_1.loadIcon(v3)).setOnMenuItemClickListener(this.h);
                v0_6++;
            }
        }
        return;
    }

    public final boolean f()
    {
        return 1;
    }
}
