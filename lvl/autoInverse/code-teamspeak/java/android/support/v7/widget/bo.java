package android.support.v7.widget;
final class bo implements android.view.View$OnKeyListener {
    final synthetic android.support.v7.widget.SearchView a;

    bo(android.support.v7.widget.SearchView p1)
    {
        this.a = p1;
        return;
    }

    public final boolean onKey(android.view.View p5, int p6, android.view.KeyEvent p7)
    {
        int v0_0 = 0;
        if (android.support.v7.widget.SearchView.o(this.a) != null) {
            if ((!android.support.v7.widget.SearchView.m(this.a).isPopupShowing()) || (android.support.v7.widget.SearchView.m(this.a).getListSelection() == -1)) {
                if ((!android.support.v7.widget.SearchView$SearchAutoComplete.a(android.support.v7.widget.SearchView.m(this.a))) && ((android.support.v4.view.ab.b(p7)) && ((p7.getAction() == 1) && (p6 == 66)))) {
                    p5.cancelLongPress();
                    android.support.v7.widget.SearchView.a(this.a, android.support.v7.widget.SearchView.m(this.a).getText().toString());
                    v0_0 = 1;
                }
            } else {
                v0_0 = android.support.v7.widget.SearchView.a(this.a, p6, p7);
            }
        }
        return v0_0;
    }
}
