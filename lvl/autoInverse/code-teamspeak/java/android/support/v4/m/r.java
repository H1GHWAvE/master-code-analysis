package android.support.v4.m;
abstract class r implements android.support.v4.m.l {
    private final android.support.v4.m.q a;

    public r(android.support.v4.m.q p1)
    {
        this.a = p1;
        return;
    }

    private boolean b(CharSequence p2, int p3, int p4)
    {
        int v0_2;
        switch (this.a.a(p2, p3, p4)) {
            case 0:
                v0_2 = 1;
                break;
            case 1:
                v0_2 = 0;
                break;
            default:
                v0_2 = this.a();
        }
        return v0_2;
    }

    protected abstract boolean a();

    public final boolean a(CharSequence p2, int p3, int p4)
    {
        if ((p2 != null) && ((p3 >= 0) && ((p4 >= 0) && ((p2.length() - p4) >= p3)))) {
            int v0_5;
            if (this.a != null) {
                switch (this.a.a(p2, p3, p4)) {
                    case 0:
                        v0_5 = 1;
                        break;
                    case 1:
                        v0_5 = 0;
                        break;
                    default:
                        v0_5 = this.a();
                }
            } else {
                v0_5 = this.a();
            }
            return v0_5;
        } else {
            throw new IllegalArgumentException();
        }
    }

    public final boolean a(char[] p2, int p3, int p4)
    {
        return this.a(java.nio.CharBuffer.wrap(p2), p3, p4);
    }
}
