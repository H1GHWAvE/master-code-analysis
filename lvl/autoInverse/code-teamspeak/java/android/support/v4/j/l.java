package android.support.v4.j;
final class l extends android.os.AsyncTask {
    final synthetic android.os.CancellationSignal a;
    final synthetic android.print.PrintAttributes b;
    final synthetic android.print.PrintAttributes c;
    final synthetic android.print.PrintDocumentAdapter$LayoutResultCallback d;
    final synthetic android.support.v4.j.k e;

    l(android.support.v4.j.k p1, android.os.CancellationSignal p2, android.print.PrintAttributes p3, android.print.PrintAttributes p4, android.print.PrintDocumentAdapter$LayoutResultCallback p5)
    {
        this.e = p1;
        this.a = p2;
        this.b = p3;
        this.c = p4;
        this.d = p5;
        return;
    }

    private varargs android.graphics.Bitmap a()
    {
        try {
            int v0_2 = this.e.g.a(this.e.d);
        } catch (int v0) {
            v0_2 = 0;
        }
        return v0_2;
    }

    private void a(android.graphics.Bitmap p6)
    {
        android.support.v4.j.k v0_0 = 1;
        super.onPostExecute(p6);
        this.e.b = p6;
        if (p6 == null) {
            this.d.onLayoutFailed(0);
        } else {
            android.print.PrintDocumentInfo v1_5 = new android.print.PrintDocumentInfo$Builder(this.e.c).setContentType(1).setPageCount(1).build();
            if (this.b.equals(this.c)) {
                v0_0 = 0;
            }
            this.d.onLayoutFinished(v1_5, v0_0);
        }
        this.e.a = 0;
        return;
    }

    private void b()
    {
        this.d.onLayoutCancelled();
        this.e.a = 0;
        return;
    }

    protected final synthetic Object doInBackground(Object[] p2)
    {
        return this.a();
    }

    protected final synthetic void onCancelled(Object p3)
    {
        this.d.onLayoutCancelled();
        this.e.a = 0;
        return;
    }

    protected final synthetic void onPostExecute(Object p6)
    {
        android.support.v4.j.k v0_0 = 1;
        super.onPostExecute(((android.graphics.Bitmap) p6));
        this.e.b = ((android.graphics.Bitmap) p6);
        if (((android.graphics.Bitmap) p6) == null) {
            this.d.onLayoutFailed(0);
        } else {
            android.print.PrintDocumentInfo v1_5 = new android.print.PrintDocumentInfo$Builder(this.e.c).setContentType(1).setPageCount(1).build();
            if (this.b.equals(this.c)) {
                v0_0 = 0;
            }
            this.d.onLayoutFinished(v1_5, v0_0);
        }
        this.e.a = 0;
        return;
    }

    protected final void onPreExecute()
    {
        this.a.setOnCancelListener(new android.support.v4.j.m(this));
        return;
    }
}
