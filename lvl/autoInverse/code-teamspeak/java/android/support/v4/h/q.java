package android.support.v4.h;
final class q {

    q()
    {
        return;
    }

    private static void a()
    {
        android.net.TrafficStats.clearThreadStatsTag();
        return;
    }

    private static void a(int p0)
    {
        android.net.TrafficStats.incrementOperationCount(p0);
        return;
    }

    private static void a(int p0, int p1)
    {
        android.net.TrafficStats.incrementOperationCount(p0, p1);
        return;
    }

    private static void a(java.net.Socket p0)
    {
        android.net.TrafficStats.tagSocket(p0);
        return;
    }

    private static int b()
    {
        return android.net.TrafficStats.getThreadStatsTag();
    }

    private static void b(int p0)
    {
        android.net.TrafficStats.setThreadStatsTag(p0);
        return;
    }

    private static void b(java.net.Socket p0)
    {
        android.net.TrafficStats.untagSocket(p0);
        return;
    }
}
