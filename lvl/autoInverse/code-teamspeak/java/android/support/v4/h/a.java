package android.support.v4.h;
public final class a {
    private static final android.support.v4.h.c a;

    static a()
    {
        if (android.os.Build$VERSION.SDK_INT < 16) {
            if (android.os.Build$VERSION.SDK_INT < 13) {
                if (android.os.Build$VERSION.SDK_INT < 8) {
                    android.support.v4.h.a.a = new android.support.v4.h.b();
                } else {
                    android.support.v4.h.a.a = new android.support.v4.h.d();
                }
            } else {
                android.support.v4.h.a.a = new android.support.v4.h.e();
            }
        } else {
            android.support.v4.h.a.a = new android.support.v4.h.f();
        }
        return;
    }

    public a()
    {
        return;
    }

    private static android.net.NetworkInfo a(android.net.ConnectivityManager p1, android.content.Intent p2)
    {
        int v0_3;
        int v0_2 = ((android.net.NetworkInfo) p2.getParcelableExtra("networkInfo"));
        if (v0_2 == 0) {
            v0_3 = 0;
        } else {
            v0_3 = p1.getNetworkInfo(v0_2.getType());
        }
        return v0_3;
    }

    private static boolean a(android.net.ConnectivityManager p1)
    {
        return android.support.v4.h.a.a.a(p1);
    }
}
