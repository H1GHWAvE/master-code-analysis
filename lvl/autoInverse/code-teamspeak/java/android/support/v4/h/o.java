package android.support.v4.h;
final class o implements android.support.v4.h.p {

    o()
    {
        return;
    }

    public final void a()
    {
        android.net.TrafficStats.clearThreadStatsTag();
        return;
    }

    public final void a(int p1)
    {
        android.net.TrafficStats.incrementOperationCount(p1);
        return;
    }

    public final void a(int p1, int p2)
    {
        android.net.TrafficStats.incrementOperationCount(p1, p2);
        return;
    }

    public final void a(java.net.Socket p1)
    {
        android.net.TrafficStats.tagSocket(p1);
        return;
    }

    public final int b()
    {
        return android.net.TrafficStats.getThreadStatsTag();
    }

    public final void b(int p1)
    {
        android.net.TrafficStats.setThreadStatsTag(p1);
        return;
    }

    public final void b(java.net.Socket p1)
    {
        android.net.TrafficStats.untagSocket(p1);
        return;
    }
}
