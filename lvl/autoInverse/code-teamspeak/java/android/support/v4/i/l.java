package android.support.v4.i;
public final class l implements android.os.Parcelable$ClassLoaderCreator {
    private final android.support.v4.i.k a;

    public l(android.support.v4.i.k p1)
    {
        this.a = p1;
        return;
    }

    public final Object createFromParcel(android.os.Parcel p3)
    {
        return this.a.a(p3, 0);
    }

    public final Object createFromParcel(android.os.Parcel p2, ClassLoader p3)
    {
        return this.a.a(p2, p3);
    }

    public final Object[] newArray(int p2)
    {
        return this.a.a(p2);
    }
}
