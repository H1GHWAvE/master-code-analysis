package android.support.v4.k;
final class c {
    private static final String a = "DocumentFile";

    c()
    {
        return;
    }

    private static android.net.Uri a(android.content.Context p1, android.net.Uri p2, String p3)
    {
        return android.support.v4.k.c.a(p1, p2, "vnd.android.document/directory", p3);
    }

    public static android.net.Uri a(android.content.Context p1, android.net.Uri p2, String p3, String p4)
    {
        return android.provider.DocumentsContract.createDocument(p1.getContentResolver(), p2, p3, p4);
    }

    private static android.net.Uri a(android.net.Uri p1)
    {
        return android.provider.DocumentsContract.buildDocumentUriUsingTree(p1, android.provider.DocumentsContract.getTreeDocumentId(p1));
    }

    private static void a(AutoCloseable p1)
    {
        if (p1 != null) {
            try {
                p1.close();
            } catch (Exception v0) {
                throw v0;
            } catch (Exception v0) {
            }
        }
        return;
    }

    public static android.net.Uri[] a(android.content.Context p8, android.net.Uri p9)
    {
        android.net.Uri[] v0_0 = p8.getContentResolver();
        int v1_1 = android.provider.DocumentsContract.buildChildDocumentsUriUsingTree(p9, android.provider.DocumentsContract.getDocumentId(p9));
        java.util.ArrayList v7_1 = new java.util.ArrayList();
        try {
            String v2_1 = new String[1];
            v2_1[0] = "document_id";
            int v1_2 = v0_0.query(v1_1, v2_1, 0, 0, 0);
            try {
                while (v1_2.moveToNext()) {
                    v7_1.add(android.provider.DocumentsContract.buildDocumentUriUsingTree(p9, v1_2.getString(0)));
                }
            } catch (android.net.Uri[] v0_1) {
                android.util.Log.w("DocumentFile", new StringBuilder("Failed query: ").append(v0_1).toString());
                android.support.v4.k.c.a(v1_2);
                android.net.Uri[] v0_7 = new android.net.Uri[v7_1.size()];
                return ((android.net.Uri[]) v7_1.toArray(v0_7));
            }
            android.support.v4.k.c.a(v1_2);
            v0_7 = new android.net.Uri[v7_1.size()];
            return ((android.net.Uri[]) v7_1.toArray(v0_7));
        } catch (android.net.Uri[] v0_2) {
            v1_2 = 0;
            android.support.v4.k.c.a(v1_2);
            throw v0_2;
        } catch (android.net.Uri[] v0_1) {
            v1_2 = 0;
        } catch (android.net.Uri[] v0_2) {
        }
    }

    private static android.net.Uri b(android.content.Context p1, android.net.Uri p2, String p3)
    {
        return android.provider.DocumentsContract.renameDocument(p1.getContentResolver(), p2, p3);
    }
}
