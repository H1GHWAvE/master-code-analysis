package android.support.v4.k;
final class d extends android.support.v4.k.a {
    private java.io.File b;

    d(android.support.v4.k.a p1, java.io.File p2)
    {
        this(p1);
        this.b = p2;
        return;
    }

    private static boolean a(java.io.File p8)
    {
        java.io.File[] v3 = p8.listFiles();
        int v0_0 = 1;
        if (v3 != null) {
            int v4 = v3.length;
            int v2 = 0;
            while (v2 < v4) {
                String v5_0 = v3[v2];
                if (v5_0.isDirectory()) {
                    v0_0 &= android.support.v4.k.d.a(v5_0);
                }
                if (!v5_0.delete()) {
                    android.util.Log.w("DocumentFile", new StringBuilder("Failed to delete ").append(v5_0).toString());
                    v0_0 = 0;
                }
                v2++;
            }
        }
        return v0_0;
    }

    private static String c(String p2)
    {
        String v0_5;
        String v0_1 = p2.lastIndexOf(46);
        if (v0_1 < null) {
            v0_5 = "application/octet-stream";
        } else {
            v0_5 = android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension(p2.substring((v0_1 + 1)).toLowerCase());
            if (v0_5 == null) {
            }
        }
        return v0_5;
    }

    public final android.net.Uri a()
    {
        return android.net.Uri.fromFile(this.b);
    }

    public final android.support.v4.k.a a(String p3)
    {
        int v0_3;
        java.io.File v1_1 = new java.io.File(this.b, p3);
        if ((!v1_1.isDirectory()) && (!v1_1.mkdir())) {
            v0_3 = 0;
        } else {
            v0_3 = new android.support.v4.k.d(this, v1_1);
        }
        return v0_3;
    }

    public final android.support.v4.k.a a(String p5, String p6)
    {
        int v0_1 = android.webkit.MimeTypeMap.getSingleton().getExtensionFromMimeType(p5);
        if (v0_1 != 0) {
            p6 = new StringBuilder().append(p6).append(".").append(v0_1).toString();
        }
        String v1_5 = new java.io.File(this.b, p6);
        try {
            v1_5.createNewFile();
            int v0_5 = new android.support.v4.k.d(this, v1_5);
        } catch (int v0_6) {
            android.util.Log.w("DocumentFile", new StringBuilder("Failed to createFile: ").append(v0_6).toString());
            v0_5 = 0;
        }
        return v0_5;
    }

    public final String b()
    {
        return this.b.getName();
    }

    public final boolean b(String p3)
    {
        int v0_2;
        int v0_1 = new java.io.File(this.b.getParentFile(), p3);
        if (!this.b.renameTo(v0_1)) {
            v0_2 = 0;
        } else {
            this.b = v0_1;
            v0_2 = 1;
        }
        return v0_2;
    }

    public final String c()
    {
        String v0_6;
        if (!this.b.isDirectory()) {
            String v0_3 = this.b.getName();
            android.webkit.MimeTypeMap v1_1 = v0_3.lastIndexOf(46);
            if (v1_1 >= null) {
                v0_6 = android.webkit.MimeTypeMap.getSingleton().getMimeTypeFromExtension(v0_3.substring((v1_1 + 1)).toLowerCase());
                if (v0_6 != null) {
                    return v0_6;
                }
            }
            v0_6 = "application/octet-stream";
        } else {
            v0_6 = 0;
        }
        return v0_6;
    }

    public final boolean d()
    {
        return this.b.isDirectory();
    }

    public final boolean e()
    {
        return this.b.isFile();
    }

    public final long f()
    {
        return this.b.lastModified();
    }

    public final long g()
    {
        return this.b.length();
    }

    public final boolean h()
    {
        return this.b.canRead();
    }

    public final boolean i()
    {
        return this.b.canWrite();
    }

    public final boolean j()
    {
        android.support.v4.k.d.a(this.b);
        return this.b.delete();
    }

    public final boolean k()
    {
        return this.b.exists();
    }

    public final android.support.v4.k.a[] l()
    {
        java.util.ArrayList v1_1 = new java.util.ArrayList();
        java.io.File[] v2 = this.b.listFiles();
        if (v2 != null) {
            int v3 = v2.length;
            int v0_1 = 0;
            while (v0_1 < v3) {
                v1_1.add(new android.support.v4.k.d(this, v2[v0_1]));
                v0_1++;
            }
        }
        int v0_3 = new android.support.v4.k.a[v1_1.size()];
        return ((android.support.v4.k.a[]) v1_1.toArray(v0_3));
    }
}
