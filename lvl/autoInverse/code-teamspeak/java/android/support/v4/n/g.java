package android.support.v4.n;
public final class g {

    public g()
    {
        return;
    }

    public static void a(Object p2, StringBuilder p3)
    {
        if (p2 != null) {
            String v0_1 = p2.getClass().getSimpleName();
            if ((v0_1 == null) || (v0_1.length() <= 0)) {
                v0_1 = p2.getClass().getName();
                int v1_2 = v0_1.lastIndexOf(46);
                if (v1_2 > 0) {
                    v0_1 = v0_1.substring((v1_2 + 1));
                }
            }
            p3.append(v0_1);
            p3.append(123);
            p3.append(Integer.toHexString(System.identityHashCode(p2)));
        } else {
            p3.append("null");
        }
        return;
    }
}
