package android.support.v4.c.b;
public final class a {

    public a()
    {
        return;
    }

    private static android.graphics.drawable.Drawable a(android.content.res.Resources p2, int p3, int p4, android.content.res.Resources$Theme p5)
    {
        android.graphics.drawable.Drawable v0_1;
        android.graphics.drawable.Drawable v0_0 = android.os.Build$VERSION.SDK_INT;
        if (v0_0 < 21) {
            if (v0_0 < 15) {
                v0_1 = p2.getDrawable(p3);
            } else {
                v0_1 = p2.getDrawableForDensity(p3, p4);
            }
        } else {
            v0_1 = p2.getDrawableForDensity(p3, p4, p5);
        }
        return v0_1;
    }

    private static android.graphics.drawable.Drawable a(android.content.res.Resources p2, int p3, android.content.res.Resources$Theme p4)
    {
        android.graphics.drawable.Drawable v0_1;
        if (android.os.Build$VERSION.SDK_INT < 21) {
            v0_1 = p2.getDrawable(p3);
        } else {
            v0_1 = p2.getDrawable(p3, p4);
        }
        return v0_1;
    }
}
