package android.support.v4.c;
public class aa {
    public int p;
    public android.support.v4.c.ad q;
    public android.support.v4.c.ac r;
    android.content.Context s;
    boolean t;
    public boolean u;
    boolean v;
    boolean w;
    boolean x;

    public aa(android.content.Context p3)
    {
        this.t = 0;
        this.u = 0;
        this.v = 1;
        this.w = 0;
        this.x = 0;
        this.s = p3.getApplicationContext();
        return;
    }

    private static String a(Object p2)
    {
        String v0_1 = new StringBuilder(64);
        android.support.v4.n.g.a(p2, v0_1);
        v0_1.append("}");
        return v0_1.toString();
    }

    private void a(int p3, android.support.v4.c.ad p4)
    {
        if (this.q == null) {
            this.q = p4;
            this.p = p3;
            return;
        } else {
            throw new IllegalStateException("There is already a listener registered");
        }
    }

    private void b(android.support.v4.c.ac p3)
    {
        if (this.r == null) {
            this.r = p3;
            return;
        } else {
            throw new IllegalStateException("There is already a listener registered");
        }
    }

    private void c()
    {
        if (this.r != null) {
            this.r.d();
        }
        return;
    }

    private android.content.Context d()
    {
        return this.s;
    }

    private int e()
    {
        return this.p;
    }

    private boolean o()
    {
        return this.t;
    }

    private boolean p()
    {
        return this.u;
    }

    private boolean q()
    {
        return this.v;
    }

    private void r()
    {
        this.u = 1;
        return;
    }

    private static void s()
    {
        return;
    }

    private boolean t()
    {
        boolean v0 = this.w;
        this.w = 0;
        this.x = (this.x | v0);
        return v0;
    }

    private void u()
    {
        this.x = 0;
        return;
    }

    private void v()
    {
        if (this.x) {
            this.w = 1;
        }
        return;
    }

    protected void a()
    {
        return;
    }

    public final void a(android.support.v4.c.ac p3)
    {
        if (this.r != null) {
            if (this.r == p3) {
                this.r = 0;
                return;
            } else {
                throw new IllegalArgumentException("Attempting to unregister the wrong listener");
            }
        } else {
            throw new IllegalStateException("No listener register");
        }
    }

    public final void a(android.support.v4.c.ad p3)
    {
        if (this.q != null) {
            if (this.q == p3) {
                this.q = 0;
                return;
            } else {
                throw new IllegalArgumentException("Attempting to unregister the wrong listener");
            }
        } else {
            throw new IllegalStateException("No listener register");
        }
    }

    public void a(String p2, java.io.FileDescriptor p3, java.io.PrintWriter p4, String[] p5)
    {
        p4.print(p2);
        p4.print("mId=");
        p4.print(this.p);
        p4.print(" mListener=");
        p4.println(this.q);
        if ((this.t) || ((this.w) || (this.x))) {
            p4.print(p2);
            p4.print("mStarted=");
            p4.print(this.t);
            p4.print(" mContentChanged=");
            p4.print(this.w);
            p4.print(" mProcessingChange=");
            p4.println(this.x);
        }
        if ((this.u) || (this.v)) {
            p4.print(p2);
            p4.print("mAbandoned=");
            p4.print(this.u);
            p4.print(" mReset=");
            p4.println(this.v);
        }
        return;
    }

    public void b(Object p2)
    {
        if (this.q != null) {
            this.q.a(this, p2);
        }
        return;
    }

    protected boolean b()
    {
        return 0;
    }

    protected void f()
    {
        return;
    }

    protected void g()
    {
        return;
    }

    protected void h()
    {
        return;
    }

    public final void i()
    {
        this.t = 1;
        this.v = 0;
        this.u = 0;
        this.f();
        return;
    }

    public final boolean j()
    {
        return this.b();
    }

    public final void k()
    {
        this.a();
        return;
    }

    public final void l()
    {
        this.t = 0;
        this.g();
        return;
    }

    public final void m()
    {
        this.h();
        this.v = 1;
        this.t = 0;
        this.u = 0;
        this.w = 0;
        this.x = 0;
        return;
    }

    public final void n()
    {
        if (!this.t) {
            this.w = 1;
        } else {
            this.a();
        }
        return;
    }

    public String toString()
    {
        String v0_1 = new StringBuilder(64);
        android.support.v4.n.g.a(this, v0_1);
        v0_1.append(" id=");
        v0_1.append(this.p);
        v0_1.append("}");
        return v0_1.toString();
    }
}
