package android.support.v4.c;
public final class t {
    public static final String a = "android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE";
    public static final String b = "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE";
    public static final String c = "android.intent.extra.changed_package_list";
    public static final String d = "android.intent.extra.changed_uid_list";
    public static final String e = "android.intent.extra.HTML_TEXT";
    public static final int f = 16384;
    public static final int g = 32768;
    private static final android.support.v4.c.u h;

    static t()
    {
        android.support.v4.c.v v0_0 = android.os.Build$VERSION.SDK_INT;
        if (v0_0 < 15) {
            if (v0_0 < 11) {
                android.support.v4.c.t.h = new android.support.v4.c.v();
            } else {
                android.support.v4.c.t.h = new android.support.v4.c.w();
            }
        } else {
            android.support.v4.c.t.h = new android.support.v4.c.x();
        }
        return;
    }

    private t()
    {
        return;
    }

    public static android.content.Intent a(android.content.ComponentName p1)
    {
        return android.support.v4.c.t.h.a(p1);
    }

    private static android.content.Intent a(String p1, String p2)
    {
        return android.support.v4.c.t.h.a(p1, p2);
    }

    private static android.content.Intent b(android.content.ComponentName p1)
    {
        return android.support.v4.c.t.h.b(p1);
    }
}
