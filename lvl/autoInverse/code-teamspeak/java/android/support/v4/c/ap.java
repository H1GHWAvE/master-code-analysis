package android.support.v4.c;
public final class ap extends java.lang.Enum {
    public static final enum int a;
    public static final enum int b;
    public static final enum int c;
    private static final synthetic int[] d;

    static ap()
    {
        android.support.v4.c.ap.a = 1;
        android.support.v4.c.ap.b = 2;
        android.support.v4.c.ap.c = 3;
        int[] v0_1 = new int[3];
        v0_1[0] = android.support.v4.c.ap.a;
        v0_1[1] = android.support.v4.c.ap.b;
        v0_1[2] = android.support.v4.c.ap.c;
        android.support.v4.c.ap.d = v0_1;
        return;
    }

    private ap(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    public static int[] a()
    {
        return ((int[]) android.support.v4.c.ap.d.clone());
    }
}
