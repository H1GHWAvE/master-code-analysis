package android.support.v4.app;
public final class eu implements android.support.v4.app.db, android.support.v4.app.dc {
    private android.app.Notification$Builder a;
    private final android.os.Bundle b;
    private java.util.List c;

    public eu(android.content.Context p9, android.app.Notification p10, CharSequence p11, CharSequence p12, CharSequence p13, android.widget.RemoteViews p14, int p15, android.app.PendingIntent p16, android.app.PendingIntent p17, android.graphics.Bitmap p18, int p19, int p20, boolean p21, boolean p22, int p23, CharSequence p24, boolean p25, android.os.Bundle p26, String p27, boolean p28, String p29)
    {
        android.os.Bundle v4_12;
        this.c = new java.util.ArrayList();
        String v5_6 = new android.app.Notification$Builder(p9).setWhen(p10.when).setSmallIcon(p10.icon, p10.iconLevel).setContent(p10.contentView).setTicker(p10.tickerText, p14).setSound(p10.sound, p10.audioStreamType).setVibrate(p10.vibrate).setLights(p10.ledARGB, p10.ledOnMS, p10.ledOffMS);
        if ((p10.flags & 2) == 0) {
            v4_12 = 0;
        } else {
            v4_12 = 1;
        }
        android.os.Bundle v4_15;
        String v5_7 = v5_6.setOngoing(v4_12);
        if ((p10.flags & 8) == 0) {
            v4_15 = 0;
        } else {
            v4_15 = 1;
        }
        android.os.Bundle v4_18;
        String v5_8 = v5_7.setOnlyAlertOnce(v4_15);
        if ((p10.flags & 16) == 0) {
            v4_18 = 0;
        } else {
            v4_18 = 1;
        }
        android.os.Bundle v4_28;
        String v5_11 = v5_8.setAutoCancel(v4_18).setDefaults(p10.defaults).setContentTitle(p11).setContentText(p12).setSubText(p24).setContentInfo(p13).setContentIntent(p16).setDeleteIntent(p10.deleteIntent);
        if ((p10.flags & 128) == 0) {
            v4_28 = 0;
        } else {
            v4_28 = 1;
        }
        this.a = v5_11.setFullScreenIntent(p17, v4_28).setLargeIcon(p18).setNumber(p15).setUsesChronometer(p22).setPriority(p23).setProgress(p19, p20, p21);
        this.b = new android.os.Bundle();
        if (p26 != null) {
            this.b.putAll(p26);
        }
        if (p25) {
            this.b.putBoolean("android.support.localOnly", 1);
        }
        if (p27 != null) {
            this.b.putString("android.support.groupKey", p27);
            if (!p28) {
                this.b.putBoolean("android.support.useSideChannel", 1);
            } else {
                this.b.putBoolean("android.support.isGroupSummary", 1);
            }
        }
        if (p29 != null) {
            this.b.putString("android.support.sortKey", p29);
        }
        return;
    }

    public final android.app.Notification$Builder a()
    {
        return this.a;
    }

    public final void a(android.support.v4.app.ek p3)
    {
        this.c.add(android.support.v4.app.et.a(this.a, p3));
        return;
    }

    public final android.app.Notification b()
    {
        android.app.Notification v1 = this.a.build();
        android.os.Bundle v2_0 = android.support.v4.app.et.a(v1);
        String v3_1 = new android.os.Bundle(this.b);
        java.util.Iterator v4 = this.b.keySet().iterator();
        while (v4.hasNext()) {
            android.util.SparseArray v0_8 = ((String) v4.next());
            if (v2_0.containsKey(v0_8)) {
                v3_1.remove(v0_8);
            }
        }
        v2_0.putAll(v3_1);
        android.util.SparseArray v0_6 = android.support.v4.app.et.a(this.c);
        if (v0_6 != null) {
            android.support.v4.app.et.a(v1).putSparseParcelableArray("android.support.actionExtras", v0_6);
        }
        return v1;
    }
}
