package android.support.v4.app;
public abstract class gj {
    static int b = 0;
    private static final String c = "sharedElement:snapshot:bitmap";
    private static final String d = "sharedElement:snapshot:imageScaleType";
    private static final String e = "sharedElement:snapshot:imageMatrix";
    android.graphics.Matrix a;

    static gj()
    {
        android.support.v4.app.gj.b = 1048576;
        return;
    }

    public gj()
    {
        return;
    }

    static android.graphics.Bitmap a(android.graphics.drawable.Drawable p9)
    {
        android.graphics.Bitmap v0_6;
        android.graphics.Bitmap v0_0 = p9.getIntrinsicWidth();
        int v1_0 = p9.getIntrinsicHeight();
        if ((v0_0 > null) && (v1_0 > 0)) {
            android.graphics.Canvas v2_3 = Math.min(1065353216, (((float) android.support.v4.app.gj.b) / ((float) (v0_0 * v1_0))));
            if ((!(p9 instanceof android.graphics.drawable.BitmapDrawable)) || (v2_3 != 1065353216)) {
                int v3_4 = ((int) (((float) v0_0) * v2_3));
                int v1_1 = ((int) (((float) v1_0) * v2_3));
                v0_6 = android.graphics.Bitmap.createBitmap(v3_4, v1_1, android.graphics.Bitmap$Config.ARGB_8888);
                android.graphics.Canvas v2_5 = new android.graphics.Canvas(v0_6);
                int v4_1 = p9.getBounds();
                int v5 = v4_1.left;
                int v6 = v4_1.top;
                int v7 = v4_1.right;
                int v4_2 = v4_1.bottom;
                p9.setBounds(0, 0, v3_4, v1_1);
                p9.draw(v2_5);
                p9.setBounds(v5, v6, v7, v4_2);
            } else {
                v0_6 = ((android.graphics.drawable.BitmapDrawable) p9).getBitmap();
            }
        } else {
            v0_6 = 0;
        }
        return v0_6;
    }

    private android.os.Parcelable a(android.view.View p13, android.graphics.Matrix p14, android.graphics.RectF p15)
    {
        android.graphics.Bitmap v0_5;
        if (!(p13 instanceof android.widget.ImageView)) {
            android.graphics.Bitmap v1_4 = Math.round(p15.width());
            float[] v2_8 = Math.round(p15.height());
            v0_5 = 0;
            if ((v1_4 > null) && (v2_8 > null)) {
                android.graphics.Bitmap v0_7 = Math.min(1065353216, (((float) android.support.v4.app.gj.b) / ((float) (v1_4 * v2_8))));
                android.graphics.Bitmap v1_7 = ((int) (((float) v1_4) * v0_7));
                float[] v2_11 = ((int) (((float) v2_8) * v0_7));
                if (this.a == null) {
                    this.a = new android.graphics.Matrix();
                }
                this.a.set(p14);
                this.a.postTranslate((- p15.left), (- p15.top));
                this.a.postScale(v0_7, v0_7);
                v0_5 = android.graphics.Bitmap.createBitmap(v1_7, v2_11, android.graphics.Bitmap$Config.ARGB_8888);
                android.graphics.Bitmap v1_9 = new android.graphics.Canvas(v0_5);
                v1_9.concat(this.a);
                p13.draw(v1_9);
            }
        } else {
            android.graphics.Bitmap v1_0 = ((android.widget.ImageView) p13).getDrawable();
            float[] v2_0 = ((android.widget.ImageView) p13).getBackground();
            if ((v1_0 == null) || (v2_0 != null)) {
            } else {
                float[] v2_7;
                float[] v2_1 = v1_0.getIntrinsicWidth();
                android.widget.ImageView$ScaleType v3_0 = v1_0.getIntrinsicHeight();
                if ((v2_1 > null) && (v3_0 > null)) {
                    android.graphics.Canvas v4_1 = Math.min(1065353216, (((float) android.support.v4.app.gj.b) / ((float) (v2_1 * v3_0))));
                    if ((!(v1_0 instanceof android.graphics.drawable.BitmapDrawable)) || (v4_1 != 1065353216)) {
                        int v5_6 = ((int) (((float) v2_1) * v4_1));
                        android.widget.ImageView$ScaleType v3_1 = ((int) (((float) v3_0) * v4_1));
                        v2_7 = android.graphics.Bitmap.createBitmap(v5_6, v3_1, android.graphics.Bitmap$Config.ARGB_8888);
                        android.graphics.Canvas v4_3 = new android.graphics.Canvas(v2_7);
                        int v6_2 = v1_0.getBounds();
                        int v7 = v6_2.left;
                        int v8 = v6_2.top;
                        int v9 = v6_2.right;
                        int v6_3 = v6_2.bottom;
                        v1_0.setBounds(0, 0, v5_6, v3_1);
                        v1_0.draw(v4_3);
                        v1_0.setBounds(v7, v8, v9, v6_3);
                    } else {
                        v2_7 = ((android.graphics.drawable.BitmapDrawable) v1_0).getBitmap();
                    }
                } else {
                    v2_7 = 0;
                }
                if (v2_7 == null) {
                } else {
                    android.graphics.Bitmap v1_11 = new android.os.Bundle();
                    v1_11.putParcelable("sharedElement:snapshot:bitmap", v2_7);
                    v1_11.putString("sharedElement:snapshot:imageScaleType", ((android.widget.ImageView) p13).getScaleType().toString());
                    if (((android.widget.ImageView) p13).getScaleType() == android.widget.ImageView$ScaleType.MATRIX) {
                        float[] v2_16 = new float[9];
                        ((android.widget.ImageView) p13).getImageMatrix().getValues(v2_16);
                        v1_11.putFloatArray("sharedElement:snapshot:imageMatrix", v2_16);
                    }
                    v0_5 = v1_11;
                }
            }
        }
        return v0_5;
    }

    public static android.view.View a(android.content.Context p3, android.os.Parcelable p4)
    {
        android.widget.ImageView v0_2;
        android.widget.ImageView v1_0 = 0;
        if (!(p4 instanceof android.os.Bundle)) {
            if (!(p4 instanceof android.graphics.Bitmap)) {
                v0_2 = 0;
                v1_0 = v0_2;
            } else {
                v0_2 = new android.widget.ImageView(p3);
                v0_2.setImageBitmap(((android.graphics.Bitmap) p4));
            }
        } else {
            android.widget.ImageView v0_6 = ((android.graphics.Bitmap) ((android.os.Bundle) p4).getParcelable("sharedElement:snapshot:bitmap"));
            if (v0_6 != null) {
                android.widget.ImageView v1_2 = new android.widget.ImageView(p3);
                v1_2.setImageBitmap(v0_6);
                v1_2.setScaleType(android.widget.ImageView$ScaleType.valueOf(((android.os.Bundle) p4).getString("sharedElement:snapshot:imageScaleType")));
                if (v1_2.getScaleType() == android.widget.ImageView$ScaleType.MATRIX) {
                    android.widget.ImageView v0_12 = ((android.os.Bundle) p4).getFloatArray("sharedElement:snapshot:imageMatrix");
                    android.graphics.Matrix v2_2 = new android.graphics.Matrix();
                    v2_2.setValues(v0_12);
                    v1_2.setImageMatrix(v2_2);
                }
                v0_2 = v1_2;
            }
        }
        return v1_0;
    }

    private static void a()
    {
        return;
    }

    private static void b()
    {
        return;
    }

    private static void c()
    {
        return;
    }

    private static void d()
    {
        return;
    }
}
