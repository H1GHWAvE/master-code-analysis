package android.support.v4.app;
final class p extends android.support.v4.app.r {
    private android.support.v4.app.gj a;

    public p(android.support.v4.app.gj p1)
    {
        this.a = p1;
        return;
    }

    public final android.os.Parcelable a(android.view.View p8, android.graphics.Matrix p9, android.graphics.RectF p10)
    {
        android.graphics.Matrix v0_5;
        android.graphics.Matrix v1_0 = this.a;
        if (!(p8 instanceof android.widget.ImageView)) {
            float[] v2_2 = Math.round(p10.width());
            android.widget.ImageView$ScaleType v3_1 = Math.round(p10.height());
            v0_5 = 0;
            if ((v2_2 > null) && (v3_1 > null)) {
                android.graphics.Matrix v0_7 = Math.min(1065353216, (((float) android.support.v4.app.gj.b) / ((float) (v2_2 * v3_1))));
                float[] v2_5 = ((int) (((float) v2_2) * v0_7));
                android.widget.ImageView$ScaleType v3_4 = ((int) (((float) v3_1) * v0_7));
                if (v1_0.a == null) {
                    v1_0.a = new android.graphics.Matrix();
                }
                v1_0.a.set(p9);
                v1_0.a.postTranslate((- p10.left), (- p10.top));
                v1_0.a.postScale(v0_7, v0_7);
                v0_5 = android.graphics.Bitmap.createBitmap(v2_5, v3_4, android.graphics.Bitmap$Config.ARGB_8888);
                float[] v2_7 = new android.graphics.Canvas(v0_5);
                v2_7.concat(v1_0.a);
                p8.draw(v2_7);
            }
        } else {
            float[] v2_0 = ((android.widget.ImageView) p8).getDrawable();
            android.widget.ImageView$ScaleType v3_0 = ((android.widget.ImageView) p8).getBackground();
            if ((v2_0 == null) || (v3_0 != null)) {
            } else {
                float[] v2_1 = android.support.v4.app.gj.a(v2_0);
                if (v2_1 == null) {
                } else {
                    android.graphics.Matrix v1_3 = new android.os.Bundle();
                    v1_3.putParcelable("sharedElement:snapshot:bitmap", v2_1);
                    v1_3.putString("sharedElement:snapshot:imageScaleType", ((android.widget.ImageView) p8).getScaleType().toString());
                    if (((android.widget.ImageView) p8).getScaleType() == android.widget.ImageView$ScaleType.MATRIX) {
                        float[] v2_11 = new float[9];
                        ((android.widget.ImageView) p8).getImageMatrix().getValues(v2_11);
                        v1_3.putFloatArray("sharedElement:snapshot:imageMatrix", v2_11);
                    }
                    v0_5 = v1_3;
                }
            }
        }
        return v0_5;
    }

    public final android.view.View a(android.content.Context p2, android.os.Parcelable p3)
    {
        return android.support.v4.app.gj.a(p2, p3);
    }

    public final void a()
    {
        return;
    }

    public final void b()
    {
        return;
    }

    public final void c()
    {
        return;
    }

    public final void d()
    {
        return;
    }
}
