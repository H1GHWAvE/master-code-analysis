package android.support.v4.app;
public class aa {

    protected aa()
    {
        return;
    }

    private static android.support.v4.app.aa a(android.app.Activity p3, android.view.View p4, String p5)
    {
        android.support.v4.app.aa v0_2;
        if (android.os.Build$VERSION.SDK_INT < 21) {
            v0_2 = new android.support.v4.app.aa();
        } else {
            v0_2 = new android.support.v4.app.ab(new android.support.v4.app.ad(android.app.ActivityOptions.makeSceneTransitionAnimation(p3, p4, p5)));
        }
        return v0_2;
    }

    private static varargs android.support.v4.app.aa a(android.app.Activity p4, android.support.v4.n.q[] p5)
    {
        android.support.v4.app.ab v0_2;
        android.support.v4.app.ab v0_0 = 0;
        if (android.os.Build$VERSION.SDK_INT < 21) {
            v0_2 = new android.support.v4.app.aa();
        } else {
            android.view.View[] v1_1;
            if (p5 == null) {
                v1_1 = 0;
            } else {
                android.view.View[] v3 = new android.view.View[p5.length];
                android.support.v4.app.ab v2_1 = new String[p5.length];
                android.view.View[] v1_2 = 0;
                while (v1_2 < p5.length) {
                    v3[v1_2] = ((android.view.View) p5[v1_2].a);
                    v2_1[v1_2] = ((String) p5[v1_2].b);
                    v1_2++;
                }
                v0_0 = v2_1;
                v1_1 = v3;
            }
            v0_2 = new android.support.v4.app.ab(android.support.v4.app.ad.a(p4, v1_1, v0_0));
        }
        return v0_2;
    }

    private static android.support.v4.app.aa a(android.content.Context p3, int p4, int p5)
    {
        android.support.v4.app.aa v0_2;
        if (android.os.Build$VERSION.SDK_INT < 16) {
            v0_2 = new android.support.v4.app.aa();
        } else {
            v0_2 = new android.support.v4.app.ac(new android.support.v4.app.ae(android.app.ActivityOptions.makeCustomAnimation(p3, p4, p5)));
        }
        return v0_2;
    }

    private static android.support.v4.app.aa a(android.view.View p3, int p4, int p5, int p6, int p7)
    {
        android.support.v4.app.aa v0_2;
        if (android.os.Build$VERSION.SDK_INT < 16) {
            v0_2 = new android.support.v4.app.aa();
        } else {
            v0_2 = new android.support.v4.app.ac(new android.support.v4.app.ae(android.app.ActivityOptions.makeScaleUpAnimation(p3, p4, p5, p6, p7)));
        }
        return v0_2;
    }

    private static android.support.v4.app.aa a(android.view.View p3, android.graphics.Bitmap p4, int p5, int p6)
    {
        android.support.v4.app.aa v0_2;
        if (android.os.Build$VERSION.SDK_INT < 16) {
            v0_2 = new android.support.v4.app.aa();
        } else {
            v0_2 = new android.support.v4.app.ac(new android.support.v4.app.ae(android.app.ActivityOptions.makeThumbnailScaleUpAnimation(p3, p4, p5, p6)));
        }
        return v0_2;
    }

    public android.os.Bundle a()
    {
        return 0;
    }

    public void a(android.support.v4.app.aa p1)
    {
        return;
    }
}
