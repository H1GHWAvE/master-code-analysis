package android.support.v4.app;
public final class ga {
    public static final String a = "android.support.v4.app.EXTRA_CALLING_PACKAGE";
    public static final String b = "android.support.v4.app.EXTRA_CALLING_ACTIVITY";
    private static android.support.v4.app.gd c;

    static ga()
    {
        if (android.os.Build$VERSION.SDK_INT < 16) {
            if (android.os.Build$VERSION.SDK_INT < 14) {
                android.support.v4.app.ga.c = new android.support.v4.app.ge();
            } else {
                android.support.v4.app.ga.c = new android.support.v4.app.gf();
            }
        } else {
            android.support.v4.app.ga.c = new android.support.v4.app.gg();
        }
        return;
    }

    public ga()
    {
        return;
    }

    static synthetic android.support.v4.app.gd a()
    {
        return android.support.v4.app.ga.c;
    }

    public static String a(android.app.Activity p2)
    {
        String v0_0 = p2.getCallingPackage();
        if (v0_0 == null) {
            v0_0 = p2.getIntent().getStringExtra("android.support.v4.app.EXTRA_CALLING_PACKAGE");
        }
        return v0_0;
    }

    private static void a(android.view.Menu p3, int p4, android.support.v4.app.gb p5)
    {
        IllegalArgumentException v0_0 = p3.findItem(p4);
        if (v0_0 != null) {
            android.support.v4.app.ga.c.a(v0_0, p5);
            return;
        } else {
            throw new IllegalArgumentException(new StringBuilder("Could not find menu item with id ").append(p4).append(" in the supplied menu").toString());
        }
    }

    private static void a(android.view.MenuItem p1, android.support.v4.app.gb p2)
    {
        android.support.v4.app.ga.c.a(p1, p2);
        return;
    }

    public static android.content.ComponentName b(android.app.Activity p2)
    {
        android.content.ComponentName v0_0 = p2.getCallingActivity();
        if (v0_0 == null) {
            v0_0 = ((android.content.ComponentName) p2.getIntent().getParcelableExtra("android.support.v4.app.EXTRA_CALLING_ACTIVITY"));
        }
        return v0_0;
    }
}
