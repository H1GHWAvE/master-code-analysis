package android.support.v4.app;
final class ce {

    ce()
    {
        return;
    }

    static android.graphics.Rect a(android.view.View p7)
    {
        android.graphics.Rect v0_1 = new android.graphics.Rect();
        int v1_1 = new int[2];
        p7.getLocationOnScreen(v1_1);
        v0_1.set(v1_1[0], v1_1[1], (v1_1[0] + p7.getWidth()), (v1_1[1] + p7.getHeight()));
        return v0_1;
    }

    public static Object a(Object p0)
    {
        if (p0 != null) {
            p0 = ((android.transition.Transition) p0).clone();
        }
        return p0;
    }

    private static Object a(Object p1, android.view.View p2, java.util.ArrayList p3, java.util.Map p4, android.view.View p5)
    {
        if (p1 != 0) {
            android.support.v4.app.ce.a(p3, p2);
            if (p4 != null) {
                p3.removeAll(p4.values());
            }
            if (!p3.isEmpty()) {
                p3.add(p5);
                android.support.v4.app.ce.b(((android.transition.Transition) p1), p3);
            } else {
                p1 = 0;
            }
        }
        return p1;
    }

    private static Object a(Object p2, Object p3, Object p4, boolean p5)
    {
        if ((((android.transition.Transition) p2) == null) || (((android.transition.Transition) p3) == null)) {
            p5 = 1;
        }
        android.transition.Transition v0_1;
        if (p5 == 0) {
            android.transition.Transition v1_0 = 0;
            if ((((android.transition.Transition) p3) == null) || (((android.transition.Transition) p2) == null)) {
                if (((android.transition.Transition) p3) == null) {
                    if (((android.transition.Transition) p2) != null) {
                        v1_0 = ((android.transition.Transition) p2);
                    }
                } else {
                    v1_0 = ((android.transition.Transition) p3);
                }
            } else {
                v1_0 = new android.transition.TransitionSet().addTransition(((android.transition.Transition) p3)).addTransition(((android.transition.Transition) p2)).setOrdering(1);
            }
            if (((android.transition.Transition) p4) == null) {
                v0_1 = v1_0;
            } else {
                v0_1 = new android.transition.TransitionSet();
                if (v1_0 != null) {
                    v0_1.addTransition(v1_0);
                }
                v0_1.addTransition(((android.transition.Transition) p4));
            }
        } else {
            v0_1 = new android.transition.TransitionSet();
            if (((android.transition.Transition) p2) != null) {
                v0_1.addTransition(((android.transition.Transition) p2));
            }
            if (((android.transition.Transition) p3) != null) {
                v0_1.addTransition(((android.transition.Transition) p3));
            }
            if (((android.transition.Transition) p4) != null) {
                v0_1.addTransition(((android.transition.Transition) p4));
            }
        }
        return v0_1;
    }

    private static void a(android.transition.Transition p1, android.support.v4.app.cj p2)
    {
        if (p1 != null) {
            p1.setEpicenterCallback(new android.support.v4.app.ch(p2));
        }
        return;
    }

    private static void a(android.view.View p13, android.view.View p14, Object p15, java.util.ArrayList p16, Object p17, java.util.ArrayList p18, Object p19, java.util.ArrayList p20, Object p21, java.util.ArrayList p22, java.util.Map p23)
    {
        if (((android.transition.Transition) p21) != null) {
            p13.getViewTreeObserver().addOnPreDrawListener(new android.support.v4.app.ci(p13, ((android.transition.Transition) p15), p16, ((android.transition.Transition) p17), p18, ((android.transition.Transition) p19), p20, p23, p22, ((android.transition.Transition) p21), p14));
        }
        return;
    }

    private static void a(android.view.ViewGroup p0, Object p1)
    {
        android.transition.TransitionManager.beginDelayedTransition(p0, ((android.transition.Transition) p1));
        return;
    }

    public static void a(Object p2, android.view.View p3)
    {
        ((android.transition.Transition) p2).setEpicenterCallback(new android.support.v4.app.cf(android.support.v4.app.ce.a(p3)));
        return;
    }

    public static void a(Object p11, android.view.View p12, java.util.Map p13, java.util.ArrayList p14)
    {
        p14.clear();
        p14.addAll(p13.values());
        java.util.List v6 = ((android.transition.TransitionSet) p11).getTargets();
        v6.clear();
        int v7 = p14.size();
        int v5 = 0;
        while (v5 < v7) {
            int v0_2 = ((android.view.View) p14.get(v5));
            int v2 = v6.size();
            if (!android.support.v4.app.ce.a(v6, v0_2, v2)) {
                v6.add(v0_2);
                int v1_1 = v2;
                while (v1_1 < v6.size()) {
                    int v0_6 = ((android.view.View) v6.get(v1_1));
                    if ((v0_6 instanceof android.view.ViewGroup)) {
                        int v0_7 = ((android.view.ViewGroup) v0_6);
                        int v8 = v0_7.getChildCount();
                        int v3_1 = 0;
                        while (v3_1 < v8) {
                            android.view.View v9 = v0_7.getChildAt(v3_1);
                            if (!android.support.v4.app.ce.a(v6, v9, v2)) {
                                v6.add(v9);
                            }
                            v3_1++;
                        }
                    }
                    v1_1++;
                }
            }
            v5++;
        }
        p14.add(p12);
        android.support.v4.app.ce.b(((android.transition.TransitionSet) p11), p14);
        return;
    }

    public static void a(Object p0, android.view.View p1, boolean p2)
    {
        ((android.transition.Transition) p0).excludeTarget(p1, p2);
        return;
    }

    private static void a(Object p11, Object p12, android.view.View p13, android.support.v4.app.ck p14, android.view.View p15, android.support.v4.app.cj p16, java.util.Map p17, java.util.ArrayList p18, java.util.Map p19, java.util.Map p20, java.util.ArrayList p21)
    {
        if ((p11 != null) || (p12 != null)) {
            if (((android.transition.Transition) p11) != null) {
                ((android.transition.Transition) p11).addTarget(p15);
            }
            if (p12 != null) {
                android.support.v4.app.ce.a(p12, p15, p19, p21);
            }
            p13.getViewTreeObserver().addOnPreDrawListener(new android.support.v4.app.cg(p13, ((android.transition.Transition) p11), p15, p14, p17, p20, p18));
            if (((android.transition.Transition) p11) != null) {
                ((android.transition.Transition) p11).setEpicenterCallback(new android.support.v4.app.ch(p16));
            }
        }
        return;
    }

    public static void a(Object p3, java.util.ArrayList p4)
    {
        if (!(((android.transition.Transition) p3) instanceof android.transition.TransitionSet)) {
            if (!android.support.v4.app.ce.a(((android.transition.Transition) p3))) {
                int v0_2 = ((android.transition.Transition) p3).getTargets();
                if ((v0_2 != 0) && ((v0_2.size() == p4.size()) && (v0_2.containsAll(p4)))) {
                    int v1_1 = (p4.size() - 1);
                    while (v1_1 >= 0) {
                        ((android.transition.Transition) p3).removeTarget(((android.view.View) p4.get(v1_1)));
                        v1_1--;
                    }
                }
            }
        } else {
            int v1_2 = ((android.transition.TransitionSet) ((android.transition.Transition) p3)).getTransitionCount();
            int v0_9 = 0;
            while (v0_9 < v1_2) {
                android.support.v4.app.ce.a(((android.transition.TransitionSet) ((android.transition.Transition) p3)).getTransitionAt(v0_9), p4);
                v0_9++;
            }
        }
        return;
    }

    static void a(java.util.ArrayList p3, android.view.View p4)
    {
        if (p4.getVisibility() == 0) {
            if (!(p4 instanceof android.view.ViewGroup)) {
                p3.add(p4);
            } else {
                if (!((android.view.ViewGroup) p4).isTransitionGroup()) {
                    int v1 = ((android.view.ViewGroup) p4).getChildCount();
                    int v0_3 = 0;
                    while (v0_3 < v1) {
                        android.support.v4.app.ce.a(p3, ((android.view.ViewGroup) p4).getChildAt(v0_3));
                        v0_3++;
                    }
                } else {
                    p3.add(((android.view.ViewGroup) p4));
                }
            }
        }
        return;
    }

    private static void a(java.util.List p7, android.view.View p8)
    {
        int v2 = p7.size();
        if (!android.support.v4.app.ce.a(p7, p8, v2)) {
            p7.add(p8);
            int v1 = v2;
            while (v1 < p7.size()) {
                int v0_3 = ((android.view.View) p7.get(v1));
                if ((v0_3 instanceof android.view.ViewGroup)) {
                    int v0_4 = ((android.view.ViewGroup) v0_3);
                    int v4 = v0_4.getChildCount();
                    int v3_1 = 0;
                    while (v3_1 < v4) {
                        android.view.View v5 = v0_4.getChildAt(v3_1);
                        if (!android.support.v4.app.ce.a(p7, v5, v2)) {
                            p7.add(v5);
                        }
                        v3_1++;
                    }
                }
                v1++;
            }
        }
        return;
    }

    public static void a(java.util.Map p3, android.view.View p4)
    {
        if (p4.getVisibility() == 0) {
            int v0_1 = p4.getTransitionName();
            if (v0_1 != 0) {
                p3.put(v0_1, p4);
            }
            if ((p4 instanceof android.view.ViewGroup)) {
                int v1 = ((android.view.ViewGroup) p4).getChildCount();
                int v0_3 = 0;
                while (v0_3 < v1) {
                    android.support.v4.app.ce.a(p3, ((android.view.ViewGroup) p4).getChildAt(v0_3));
                    v0_3++;
                }
            }
        }
        return;
    }

    private static boolean a(android.transition.Transition p1)
    {
        if ((android.support.v4.app.ce.a(p1.getTargetIds())) && ((android.support.v4.app.ce.a(p1.getTargetNames())) && (android.support.v4.app.ce.a(p1.getTargetTypes())))) {
            int v0_6 = 0;
        } else {
            v0_6 = 1;
        }
        return v0_6;
    }

    private static boolean a(java.util.List p1)
    {
        if ((p1 != null) && (!p1.isEmpty())) {
            int v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    private static boolean a(java.util.List p3, android.view.View p4, int p5)
    {
        int v0 = 0;
        int v1 = 0;
        while (v1 < p5) {
            if (p3.get(v1) != p4) {
                v1++;
            } else {
                v0 = 1;
                break;
            }
        }
        return v0;
    }

    private static Object b(Object p1)
    {
        android.transition.TransitionSet v0_0 = 0;
        if ((p1 != null) && (((android.transition.Transition) p1) != null)) {
            v0_0 = new android.transition.TransitionSet();
            v0_0.addTransition(((android.transition.Transition) p1));
        }
        return v0_0;
    }

    private static String b(android.view.View p1)
    {
        return p1.getTransitionName();
    }

    public static void b(Object p3, java.util.ArrayList p4)
    {
        int v0_0 = 0;
        if (!(((android.transition.Transition) p3) instanceof android.transition.TransitionSet)) {
            if ((!android.support.v4.app.ce.a(((android.transition.Transition) p3))) && (android.support.v4.app.ce.a(((android.transition.Transition) p3).getTargets()))) {
                int v2_0 = p4.size();
                int v1_4 = 0;
                while (v1_4 < v2_0) {
                    ((android.transition.Transition) p3).addTarget(((android.view.View) p4.get(v1_4)));
                    v1_4++;
                }
            }
        } else {
            int v1_5 = ((android.transition.TransitionSet) ((android.transition.Transition) p3)).getTransitionCount();
            while (v0_0 < v1_5) {
                android.support.v4.app.ce.b(((android.transition.TransitionSet) ((android.transition.Transition) p3)).getTransitionAt(v0_0), p4);
                v0_0++;
            }
        }
        return;
    }

    private static synthetic void b(java.util.ArrayList p0, android.view.View p1)
    {
        android.support.v4.app.ce.a(p0, p1);
        return;
    }

    private static synthetic android.graphics.Rect c(android.view.View p1)
    {
        return android.support.v4.app.ce.a(p1);
    }
}
