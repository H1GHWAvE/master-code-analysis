package android.support.v4.app;
final class cn implements android.support.v4.app.cl {
    private android.os.IBinder a;

    cn(android.os.IBinder p1)
    {
        this.a = p1;
        return;
    }

    private static String a()
    {
        return "android.support.v4.app.INotificationSideChannel";
    }

    public final void a(String p6)
    {
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v1.writeInterfaceToken("android.support.v4.app.INotificationSideChannel");
            v1.writeString(p6);
            this.a.transact(3, v1, 0, 1);
            v1.recycle();
            return;
        } catch (Throwable v0_2) {
            v1.recycle();
            throw v0_2;
        }
    }

    public final void a(String p6, int p7, String p8)
    {
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v1.writeInterfaceToken("android.support.v4.app.INotificationSideChannel");
            v1.writeString(p6);
            v1.writeInt(p7);
            v1.writeString(p8);
            this.a.transact(2, v1, 0, 1);
            v1.recycle();
            return;
        } catch (Throwable v0_2) {
            v1.recycle();
            throw v0_2;
        }
    }

    public final void a(String p6, int p7, String p8, android.app.Notification p9)
    {
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v1.writeInterfaceToken("android.support.v4.app.INotificationSideChannel");
            v1.writeString(p6);
            v1.writeInt(p7);
            v1.writeString(p8);
        } catch (android.os.IBinder v0_4) {
            v1.recycle();
            throw v0_4;
        }
        if (p9 == null) {
            v1.writeInt(0);
        } else {
            v1.writeInt(1);
            p9.writeToParcel(v1, 0);
        }
        this.a.transact(1, v1, 0, 1);
        v1.recycle();
        return;
    }

    public final android.os.IBinder asBinder()
    {
        return this.a;
    }
}
