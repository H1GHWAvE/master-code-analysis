package android.support.v4.app;
public final class cv {
    public static final String a = "android.support.PARENT_ACTIVITY";
    private static final String b = "NavUtils";
    private static final android.support.v4.app.cw c;

    static cv()
    {
        if (android.os.Build$VERSION.SDK_INT < 16) {
            android.support.v4.app.cv.c = new android.support.v4.app.cx();
        } else {
            android.support.v4.app.cv.c = new android.support.v4.app.cy();
        }
        return;
    }

    private cv()
    {
        return;
    }

    public static android.content.Intent a(android.app.Activity p1)
    {
        return android.support.v4.app.cv.c.a(p1);
    }

    public static android.content.Intent a(android.content.Context p3, android.content.ComponentName p4)
    {
        android.content.Intent v0_4;
        android.content.Intent v0_0 = android.support.v4.app.cv.b(p3, p4);
        if (v0_0 != null) {
            android.content.ComponentName v1_1 = new android.content.ComponentName(p4.getPackageName(), v0_0);
            if (android.support.v4.app.cv.b(p3, v1_1) != null) {
                v0_4 = new android.content.Intent().setComponent(v1_1);
            } else {
                v0_4 = android.support.v4.c.t.a(v1_1);
            }
        } else {
            v0_4 = 0;
        }
        return v0_4;
    }

    private static android.content.Intent a(android.content.Context p2, Class p3)
    {
        android.content.Intent v0_6;
        android.content.Intent v0_2 = android.support.v4.app.cv.b(p2, new android.content.ComponentName(p2, p3));
        if (v0_2 != null) {
            android.content.ComponentName v1_1 = new android.content.ComponentName(p2, v0_2);
            if (android.support.v4.app.cv.b(p2, v1_1) != null) {
                v0_6 = new android.content.Intent().setComponent(v1_1);
            } else {
                v0_6 = android.support.v4.c.t.a(v1_1);
            }
        } else {
            v0_6 = 0;
        }
        return v0_6;
    }

    public static boolean a(android.app.Activity p1, android.content.Intent p2)
    {
        return android.support.v4.app.cv.c.a(p1, p2);
    }

    public static String b(android.app.Activity p2)
    {
        try {
            return android.support.v4.app.cv.b(p2, p2.getComponentName());
        } catch (android.content.pm.PackageManager$NameNotFoundException v0_2) {
            throw new IllegalArgumentException(v0_2);
        }
    }

    public static String b(android.content.Context p2, android.content.ComponentName p3)
    {
        return android.support.v4.app.cv.c.a(p2, p2.getPackageManager().getActivityInfo(p3, 128));
    }

    public static void b(android.app.Activity p1, android.content.Intent p2)
    {
        android.support.v4.app.cv.c.b(p1, p2);
        return;
    }

    private static void c(android.app.Activity p3)
    {
        IllegalArgumentException v0_0 = android.support.v4.app.cv.a(p3);
        if (v0_0 != null) {
            android.support.v4.app.cv.b(p3, v0_0);
            return;
        } else {
            throw new IllegalArgumentException(new StringBuilder("Activity ").append(p3.getClass().getSimpleName()).append(" does not have a parent activity name specified. (Did you forget to add the android.support.PARENT_ACTIVITY <meta-data>  element in your manifest?)").toString());
        }
    }
}
