package android.support.v4.app;
final class FragmentState implements android.os.Parcelable {
    public static final android.os.Parcelable$Creator CREATOR;
    final String a;
    final int b;
    final boolean c;
    final int d;
    final int e;
    final String f;
    final boolean g;
    final boolean h;
    final android.os.Bundle i;
    android.os.Bundle j;
    android.support.v4.app.Fragment k;

    static FragmentState()
    {
        android.support.v4.app.FragmentState.CREATOR = new android.support.v4.app.bx();
        return;
    }

    public FragmentState(android.os.Parcel p4)
    {
        android.os.Bundle v0_3;
        int v1 = 1;
        this.a = p4.readString();
        this.b = p4.readInt();
        if (p4.readInt() == 0) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        android.os.Bundle v0_8;
        this.c = v0_3;
        this.d = p4.readInt();
        this.e = p4.readInt();
        this.f = p4.readString();
        if (p4.readInt() == 0) {
            v0_8 = 0;
        } else {
            v0_8 = 1;
        }
        this.g = v0_8;
        if (p4.readInt() == 0) {
            v1 = 0;
        }
        this.h = v1;
        this.i = p4.readBundle();
        this.j = p4.readBundle();
        return;
    }

    public FragmentState(android.support.v4.app.Fragment p2)
    {
        this.a = p2.getClass().getName();
        this.b = p2.z;
        this.c = p2.I;
        this.d = p2.Q;
        this.e = p2.R;
        this.f = p2.S;
        this.g = p2.V;
        this.h = p2.U;
        this.i = p2.B;
        return;
    }

    private android.support.v4.app.Fragment a(android.support.v4.app.bh p4, android.support.v4.app.Fragment p5)
    {
        android.support.v4.app.Fragment v0_15;
        if (this.k == null) {
            android.support.v4.app.Fragment v0_1 = p4.c;
            if (this.i != null) {
                this.i.setClassLoader(v0_1.getClassLoader());
            }
            this.k = android.support.v4.app.Fragment.a(v0_1, this.a, this.i);
            if (this.j != null) {
                this.j.setClassLoader(v0_1.getClassLoader());
                this.k.x = this.j;
            }
            this.k.a(this.b, p5);
            this.k.I = this.c;
            this.k.K = 1;
            this.k.Q = this.d;
            this.k.R = this.e;
            this.k.S = this.f;
            this.k.V = this.g;
            this.k.U = this.h;
            this.k.M = p4.f;
            if (android.support.v4.app.bl.b) {
                android.util.Log.v("FragmentManager", new StringBuilder("Instantiated fragment ").append(this.k).toString());
            }
            v0_15 = this.k;
        } else {
            v0_15 = this.k;
        }
        return v0_15;
    }

    public final int describeContents()
    {
        return 0;
    }

    public final void writeToParcel(android.os.Parcel p4, int p5)
    {
        android.os.Bundle v0_3;
        int v1 = 1;
        p4.writeString(this.a);
        p4.writeInt(this.b);
        if (!this.c) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        android.os.Bundle v0_8;
        p4.writeInt(v0_3);
        p4.writeInt(this.d);
        p4.writeInt(this.e);
        p4.writeString(this.f);
        if (!this.g) {
            v0_8 = 0;
        } else {
            v0_8 = 1;
        }
        p4.writeInt(v0_8);
        if (!this.h) {
            v1 = 0;
        }
        p4.writeInt(v1);
        p4.writeBundle(this.i);
        p4.writeBundle(this.j);
        return;
    }
}
