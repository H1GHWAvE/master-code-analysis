package android.support.v4.app;
final class FragmentManagerState implements android.os.Parcelable {
    public static final android.os.Parcelable$Creator CREATOR;
    android.support.v4.app.FragmentState[] a;
    int[] b;
    android.support.v4.app.BackStackState[] c;

    static FragmentManagerState()
    {
        android.support.v4.app.FragmentManagerState.CREATOR = new android.support.v4.app.bv();
        return;
    }

    public FragmentManagerState()
    {
        return;
    }

    public FragmentManagerState(android.os.Parcel p2)
    {
        this.a = ((android.support.v4.app.FragmentState[]) p2.createTypedArray(android.support.v4.app.FragmentState.CREATOR));
        this.b = p2.createIntArray();
        this.c = ((android.support.v4.app.BackStackState[]) p2.createTypedArray(android.support.v4.app.BackStackState.CREATOR));
        return;
    }

    public final int describeContents()
    {
        return 0;
    }

    public final void writeToParcel(android.os.Parcel p2, int p3)
    {
        p2.writeTypedArray(this.a, p3);
        p2.writeIntArray(this.b);
        p2.writeTypedArray(this.c, p3);
        return;
    }
}
