package android.support.v4.app;
final class au {
    static reflect.Method a = None;
    static boolean b = False;
    private static final String c = "BundleCompatDonut";
    private static reflect.Method d;
    private static boolean e;

    au()
    {
        return;
    }

    public static android.os.IBinder a(android.os.Bundle p7, String p8)
    {
        if (!android.support.v4.app.au.e) {
            try {
                String v3_1 = new Class[1];
                v3_1[0] = String;
                IllegalArgumentException v0_2 = android.os.Bundle.getMethod("getIBinder", v3_1);
                android.support.v4.app.au.d = v0_2;
                v0_2.setAccessible(1);
            } catch (IllegalArgumentException v0_3) {
                android.util.Log.i("BundleCompatDonut", "Failed to retrieve getIBinder method", v0_3);
            }
            android.support.v4.app.au.e = 1;
        }
        IllegalArgumentException v0_7;
        if (android.support.v4.app.au.d == null) {
            v0_7 = 0;
        } else {
            try {
                String v2_4 = new Object[1];
                v2_4[0] = p8;
                v0_7 = ((android.os.IBinder) android.support.v4.app.au.d.invoke(p7, v2_4));
            } catch (IllegalArgumentException v0_8) {
                android.util.Log.i("BundleCompatDonut", "Failed to invoke getIBinder via reflection", v0_8);
                android.support.v4.app.au.d = 0;
            } catch (IllegalArgumentException v0_8) {
            } catch (IllegalArgumentException v0_8) {
            }
        }
        return v0_7;
    }

    private static void a(android.os.Bundle p6, String p7, android.os.IBinder p8)
    {
        if (!android.support.v4.app.au.b) {
            try {
                String v2_1 = new Class[2];
                v2_1[0] = String;
                v2_1[1] = android.os.IBinder;
                int v0_2 = android.os.Bundle.getMethod("putIBinder", v2_1);
                android.support.v4.app.au.a = v0_2;
                v0_2.setAccessible(1);
            } catch (int v0_3) {
                android.util.Log.i("BundleCompatDonut", "Failed to retrieve putIBinder method", v0_3);
            }
            android.support.v4.app.au.b = 1;
        }
        if (android.support.v4.app.au.a != null) {
            try {
                String v1_4 = new Object[2];
                v1_4[0] = p7;
                v1_4[1] = p8;
                android.support.v4.app.au.a.invoke(p6, v1_4);
            } catch (int v0_6) {
                android.util.Log.i("BundleCompatDonut", "Failed to invoke putIBinder via reflection", v0_6);
                android.support.v4.app.au.a = 0;
            } catch (int v0_6) {
            } catch (int v0_6) {
            }
        }
        return;
    }
}
