package android.support.v4.app;
final class ad {
    final android.app.ActivityOptions a;

    ad(android.app.ActivityOptions p1)
    {
        this.a = p1;
        return;
    }

    private android.os.Bundle a()
    {
        return this.a.toBundle();
    }

    private static android.support.v4.app.ad a(android.app.Activity p2, android.view.View p3, String p4)
    {
        return new android.support.v4.app.ad(android.app.ActivityOptions.makeSceneTransitionAnimation(p2, p3, p4));
    }

    public static android.support.v4.app.ad a(android.app.Activity p4, android.view.View[] p5, String[] p6)
    {
        android.support.v4.app.ad v0_0 = 0;
        if (p5 != null) {
            android.support.v4.app.ad v1_0 = new android.util.Pair[p5.length];
            android.support.v4.app.ad v0_2 = 0;
            while (v0_2 < v1_0.length) {
                v1_0[v0_2] = android.util.Pair.create(p5[v0_2], p6[v0_2]);
                v0_2++;
            }
            v0_0 = v1_0;
        }
        return new android.support.v4.app.ad(android.app.ActivityOptions.makeSceneTransitionAnimation(p4, v0_0));
    }

    private void a(android.support.v4.app.ad p3)
    {
        this.a.update(p3.a);
        return;
    }
}
