package android.support.v4.app;
final class ez extends android.support.v4.app.cm {
    final synthetic android.support.v4.app.ex d;

    private ez(android.support.v4.app.ex p1)
    {
        this.d = p1;
        return;
    }

    synthetic ez(android.support.v4.app.ex p1, byte p2)
    {
        this(p1);
        return;
    }

    public final void a(String p3)
    {
        android.support.v4.app.ex.a(this.d, android.support.v4.app.ez.getCallingUid(), p3);
        android.support.v4.app.ez.restoreCallingIdentity(android.support.v4.app.ez.clearCallingIdentity());
        return;
    }

    public final void a(String p3, int p4, String p5)
    {
        android.support.v4.app.ex.a(this.d, android.support.v4.app.ez.getCallingUid(), p3);
        android.support.v4.app.ez.restoreCallingIdentity(android.support.v4.app.ez.clearCallingIdentity());
        return;
    }

    public final void a(String p3, int p4, String p5, android.app.Notification p6)
    {
        android.support.v4.app.ex.a(this.d, android.support.v4.app.ez.getCallingUid(), p3);
        android.support.v4.app.ez.restoreCallingIdentity(android.support.v4.app.ez.clearCallingIdentity());
        return;
    }
}
