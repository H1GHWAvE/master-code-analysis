package android.support.v4.app;
public final class m extends android.support.v4.c.h {

    public m()
    {
        return;
    }

    static android.support.v4.app.r a(android.support.v4.app.gj p1)
    {
        android.support.v4.app.p v0_0 = 0;
        if (p1 != null) {
            v0_0 = new android.support.v4.app.p(p1);
        }
        return v0_0;
    }

    private static void a(android.app.Activity p2, android.content.Intent p3, int p4, android.os.Bundle p5)
    {
        if (android.os.Build$VERSION.SDK_INT < 16) {
            p2.startActivityForResult(p3, p4);
        } else {
            p2.startActivityForResult(p3, p4, p5);
        }
        return;
    }

    private static void a(android.app.Activity p2, android.content.Intent p3, android.os.Bundle p4)
    {
        if (android.os.Build$VERSION.SDK_INT < 16) {
            p2.startActivity(p3);
        } else {
            p2.startActivity(p3, p4);
        }
        return;
    }

    private static void a(android.app.Activity p2, android.support.v4.app.gj p3)
    {
        if (android.os.Build$VERSION.SDK_INT >= 21) {
            p2.setEnterSharedElementCallback(android.support.v4.app.q.a(android.support.v4.app.m.a(p3)));
        }
        return;
    }

    public static void a(android.app.Activity p2, String[] p3, int p4)
    {
        if (android.os.Build$VERSION.SDK_INT < 23) {
            if ((p2 instanceof android.support.v4.app.o)) {
                new android.os.Handler(android.os.Looper.getMainLooper()).post(new android.support.v4.app.n(p3, p2, p4));
            }
        } else {
            if ((p2 instanceof android.support.v4.app.v)) {
                ((android.support.v4.app.v) p2).a(p4);
            }
            p2.requestPermissions(p3, p4);
        }
        return;
    }

    private static boolean a(android.app.Activity p2)
    {
        int v0_1;
        if (android.os.Build$VERSION.SDK_INT < 11) {
            v0_1 = 0;
        } else {
            p2.invalidateOptionsMenu();
            v0_1 = 1;
        }
        return v0_1;
    }

    private static boolean a(android.app.Activity p2, String p3)
    {
        int v0_1;
        if (android.os.Build$VERSION.SDK_INT < 23) {
            v0_1 = 0;
        } else {
            v0_1 = p2.shouldShowRequestPermissionRationale(p3);
        }
        return v0_1;
    }

    private static void b(android.app.Activity p2)
    {
        if (android.os.Build$VERSION.SDK_INT < 16) {
            p2.finish();
        } else {
            p2.finishAffinity();
        }
        return;
    }

    private static void b(android.app.Activity p2, android.support.v4.app.gj p3)
    {
        if (android.os.Build$VERSION.SDK_INT >= 21) {
            p2.setExitSharedElementCallback(android.support.v4.app.q.a(android.support.v4.app.m.a(p3)));
        }
        return;
    }

    private static void c(android.app.Activity p2)
    {
        if (android.os.Build$VERSION.SDK_INT < 21) {
            p2.finish();
        } else {
            p2.finishAfterTransition();
        }
        return;
    }

    private static android.net.Uri d(android.app.Activity p2)
    {
        int v0_3;
        if (android.os.Build$VERSION.SDK_INT < 22) {
            android.content.Intent v1_1 = p2.getIntent();
            v0_3 = ((android.net.Uri) v1_1.getParcelableExtra("android.intent.extra.REFERRER"));
            if (v0_3 == 0) {
                int v0_5 = v1_1.getStringExtra("android.intent.extra.REFERRER_NAME");
                if (v0_5 == 0) {
                    v0_3 = 0;
                } else {
                    v0_3 = android.net.Uri.parse(v0_5);
                }
            }
        } else {
            v0_3 = p2.getReferrer();
        }
        return v0_3;
    }

    private static void e(android.app.Activity p2)
    {
        if (android.os.Build$VERSION.SDK_INT >= 21) {
            p2.postponeEnterTransition();
        }
        return;
    }

    private static void f(android.app.Activity p2)
    {
        if (android.os.Build$VERSION.SDK_INT >= 21) {
            p2.startPostponedEnterTransition();
        }
        return;
    }
}
