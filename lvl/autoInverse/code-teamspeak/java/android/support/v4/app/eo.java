package android.support.v4.app;
public final class eo {
    public static final String a = "android.support.localOnly";
    public static final String b = "android.support.groupKey";
    public static final String c = "android.support.isGroupSummary";
    public static final String d = "android.support.sortKey";
    public static final String e = "android.support.actionExtras";
    public static final String f = "android.support.remoteInputs";

    private eo()
    {
        return;
    }
}
