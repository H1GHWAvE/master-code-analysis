package android.support.v4.app;
public final class fn extends android.support.v4.app.fw {
    public static final String a = "android.remoteinput.results";
    public static final String b = "android.remoteinput.resultsData";
    public static final android.support.v4.app.fx c = None;
    private static final String d = "RemoteInput";
    private static final android.support.v4.app.fq j;
    private final String e;
    private final CharSequence f;
    private final CharSequence[] g;
    private final boolean h;
    private final android.os.Bundle i;

    static fn()
    {
        if (android.os.Build$VERSION.SDK_INT < 20) {
            if (android.os.Build$VERSION.SDK_INT < 16) {
                android.support.v4.app.fn.j = new android.support.v4.app.fs();
            } else {
                android.support.v4.app.fn.j = new android.support.v4.app.ft();
            }
        } else {
            android.support.v4.app.fn.j = new android.support.v4.app.fr();
        }
        android.support.v4.app.fn.c = new android.support.v4.app.fo();
        return;
    }

    fn(String p1, CharSequence p2, CharSequence[] p3, boolean p4, android.os.Bundle p5)
    {
        this.e = p1;
        this.f = p2;
        this.g = p3;
        this.h = p4;
        this.i = p5;
        return;
    }

    private static android.os.Bundle a(android.content.Intent p1)
    {
        return android.support.v4.app.fn.j.a(p1);
    }

    private static void a(android.support.v4.app.fn[] p1, android.content.Intent p2, android.os.Bundle p3)
    {
        android.support.v4.app.fn.j.a(p1, p2, p3);
        return;
    }

    public final String a()
    {
        return this.e;
    }

    public final CharSequence b()
    {
        return this.f;
    }

    public final CharSequence[] c()
    {
        return this.g;
    }

    public final boolean d()
    {
        return this.h;
    }

    public final android.os.Bundle e()
    {
        return this.i;
    }
}
