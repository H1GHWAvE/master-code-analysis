package android.support.v4.app;
public final class gl implements java.lang.Iterable {
    private static final String c = "TaskStackBuilder";
    private static final android.support.v4.app.gn d;
    public final java.util.ArrayList a;
    public final android.content.Context b;

    static gl()
    {
        if (android.os.Build$VERSION.SDK_INT < 11) {
            android.support.v4.app.gl.d = new android.support.v4.app.go();
        } else {
            android.support.v4.app.gl.d = new android.support.v4.app.gp();
        }
        return;
    }

    private gl(android.content.Context p2)
    {
        this.a = new java.util.ArrayList();
        this.b = p2;
        return;
    }

    private int a()
    {
        return this.a.size();
    }

    private android.app.PendingIntent a(int p5, int p6)
    {
        if (!this.a.isEmpty()) {
            android.support.v4.app.gn v1_2 = new android.content.Intent[this.a.size()];
            android.app.PendingIntent v0_4 = ((android.content.Intent[]) this.a.toArray(v1_2));
            v0_4[0] = new android.content.Intent(v0_4[0]).addFlags(268484608);
            return android.support.v4.app.gl.d.a(this.b, v0_4, p5, p6);
        } else {
            throw new IllegalStateException("No intents added to TaskStackBuilder; cannot getPendingIntent");
        }
    }

    private android.content.Intent a(int p2)
    {
        return ((android.content.Intent) this.a.get(p2));
    }

    private android.support.v4.app.gl a(android.app.Activity p3)
    {
        android.content.ComponentName v0_0 = 0;
        if ((p3 instanceof android.support.v4.app.gm)) {
            v0_0 = ((android.support.v4.app.gm) p3).a_();
        }
        android.content.Intent v1_1;
        if (v0_0 != null) {
            v1_1 = v0_0;
        } else {
            v1_1 = android.support.v4.app.cv.a(p3);
        }
        if (v1_1 != null) {
            android.content.ComponentName v0_4 = v1_1.getComponent();
            if (v0_4 == null) {
                v0_4 = v1_1.resolveActivity(this.b.getPackageManager());
            }
            this.a(v0_4);
            this.a(v1_1);
        }
        return this;
    }

    public static android.support.v4.app.gl a(android.content.Context p1)
    {
        return new android.support.v4.app.gl(p1);
    }

    private android.support.v4.app.gl a(Class p3)
    {
        return this.a(new android.content.ComponentName(this.b, p3));
    }

    private android.app.PendingIntent b(int p5, int p6)
    {
        if (!this.a.isEmpty()) {
            android.support.v4.app.gn v1_2 = new android.content.Intent[this.a.size()];
            android.app.PendingIntent v0_4 = ((android.content.Intent[]) this.a.toArray(v1_2));
            v0_4[0] = new android.content.Intent(v0_4[0]).addFlags(268484608);
            return android.support.v4.app.gl.d.a(this.b, v0_4, p5, p6);
        } else {
            throw new IllegalStateException("No intents added to TaskStackBuilder; cannot getPendingIntent");
        }
    }

    private android.content.Intent b(int p2)
    {
        return ((android.content.Intent) this.a.get(p2));
    }

    private static android.support.v4.app.gl b(android.content.Context p1)
    {
        return android.support.v4.app.gl.a(p1);
    }

    private android.support.v4.app.gl b(android.content.Intent p2)
    {
        android.content.ComponentName v0_0 = p2.getComponent();
        if (v0_0 == null) {
            v0_0 = p2.resolveActivity(this.b.getPackageManager());
        }
        if (v0_0 != null) {
            this.a(v0_0);
        }
        this.a(p2);
        return this;
    }

    private void b()
    {
        if (!this.a.isEmpty()) {
            android.content.Intent v1_2 = new android.content.Intent[this.a.size()];
            android.content.Context v0_4 = ((android.content.Intent[]) this.a.toArray(v1_2));
            v0_4[0] = new android.content.Intent(v0_4[0]).addFlags(268484608);
            if (!android.support.v4.c.h.a(this.b, v0_4)) {
                android.content.Intent v1_9 = new android.content.Intent(v0_4[(v0_4.length - 1)]);
                v1_9.addFlags(268435456);
                this.b.startActivity(v1_9);
            }
            return;
        } else {
            throw new IllegalStateException("No intents added to TaskStackBuilder; cannot startActivities");
        }
    }

    private void c()
    {
        if (!this.a.isEmpty()) {
            android.content.Intent v1_2 = new android.content.Intent[this.a.size()];
            android.content.Context v0_4 = ((android.content.Intent[]) this.a.toArray(v1_2));
            v0_4[0] = new android.content.Intent(v0_4[0]).addFlags(268484608);
            if (!android.support.v4.c.h.a(this.b, v0_4)) {
                android.content.Intent v1_9 = new android.content.Intent(v0_4[(v0_4.length - 1)]);
                v1_9.addFlags(268435456);
                this.b.startActivity(v1_9);
            }
            return;
        } else {
            throw new IllegalStateException("No intents added to TaskStackBuilder; cannot startActivities");
        }
    }

    private android.content.Intent[] d()
    {
        android.content.Intent[] v0_10;
        android.content.Intent[] v2 = new android.content.Intent[this.a.size()];
        if (v2.length != 0) {
            v2[0] = new android.content.Intent(((android.content.Intent) this.a.get(0))).addFlags(268484608);
            int v1_2 = 1;
            while (v1_2 < v2.length) {
                v2[v1_2] = new android.content.Intent(((android.content.Intent) this.a.get(v1_2)));
                v1_2++;
            }
            v0_10 = v2;
        } else {
            v0_10 = v2;
        }
        return v0_10;
    }

    public final android.support.v4.app.gl a(android.content.ComponentName p4)
    {
        IllegalArgumentException v1_0 = this.a.size();
        try {
            android.content.Intent v0_2 = android.support.v4.app.cv.a(this.b, p4);
        } catch (android.content.Intent v0_3) {
            android.util.Log.e("TaskStackBuilder", "Bad ComponentName while traversing activity parent metadata");
            throw new IllegalArgumentException(v0_3);
        }
        while (v0_2 != null) {
            this.a.add(v1_0, v0_2);
            v0_2 = android.support.v4.app.cv.a(this.b, v0_2.getComponent());
        }
        return this;
    }

    public final android.support.v4.app.gl a(android.content.Intent p2)
    {
        this.a.add(p2);
        return this;
    }

    public final java.util.Iterator iterator()
    {
        return this.a.iterator();
    }
}
