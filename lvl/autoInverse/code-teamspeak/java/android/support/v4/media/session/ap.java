package android.support.v4.media.session;
final class ap implements android.os.Parcelable$Creator {

    ap()
    {
        return;
    }

    private static android.support.v4.media.session.MediaSessionCompat$QueueItem a(android.os.Parcel p2)
    {
        return new android.support.v4.media.session.MediaSessionCompat$QueueItem(p2, 0);
    }

    private static android.support.v4.media.session.MediaSessionCompat$QueueItem[] a(int p1)
    {
        android.support.v4.media.session.MediaSessionCompat$QueueItem[] v0 = new android.support.v4.media.session.MediaSessionCompat$QueueItem[p1];
        return v0;
    }

    public final synthetic Object createFromParcel(android.os.Parcel p3)
    {
        return new android.support.v4.media.session.MediaSessionCompat$QueueItem(p3, 0);
    }

    public final bridge synthetic Object[] newArray(int p2)
    {
        android.support.v4.media.session.MediaSessionCompat$QueueItem[] v0 = new android.support.v4.media.session.MediaSessionCompat$QueueItem[p2];
        return v0;
    }
}
