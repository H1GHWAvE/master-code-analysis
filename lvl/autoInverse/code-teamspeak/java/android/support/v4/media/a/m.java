package android.support.v4.media.a;
public final class m implements java.lang.Runnable {
    private static final int a = 15000;
    private final android.hardware.display.DisplayManager b;
    private final android.os.Handler c;
    private reflect.Method d;
    private boolean e;

    private m(android.content.Context p4, android.os.Handler p5)
    {
        if (android.os.Build$VERSION.SDK_INT == 17) {
            this.b = ((android.hardware.display.DisplayManager) p4.getSystemService("display"));
            this.c = p5;
            try {
                Class[] v2_1 = new Class[0];
                this.d = android.hardware.display.DisplayManager.getMethod("scanWifiDisplays", v2_1);
            } catch (NoSuchMethodException v0) {
            }
            return;
        } else {
            throw new UnsupportedOperationException();
        }
    }

    private void a(int p3)
    {
        if ((p3 & 2) == 0) {
            if (this.e) {
                this.e = 0;
                this.c.removeCallbacks(this);
            }
        } else {
            if (!this.e) {
                if (this.d == null) {
                    android.util.Log.w("MediaRouterJellybeanMr1", "Cannot scan for wifi displays because the DisplayManager.scanWifiDisplays() method is not available on this device.");
                } else {
                    this.e = 1;
                    this.c.post(this);
                }
            }
        }
        return;
    }

    public final void run()
    {
        if (this.e) {
            try {
                long v2_1 = new Object[0];
                this.d.invoke(this.b, v2_1);
            } catch (android.os.Handler v0_3) {
                android.util.Log.w("MediaRouterJellybeanMr1", "Cannot scan for wifi displays.", v0_3);
            } catch (android.os.Handler v0_2) {
                android.util.Log.w("MediaRouterJellybeanMr1", "Cannot scan for wifi displays.", v0_2);
            }
            this.c.postDelayed(this, 15000);
        }
        return;
    }
}
