package android.support.v4.media.session;
final class ai implements android.support.v4.media.session.ag {
    private boolean A;
    private boolean B;
    private android.support.v4.media.ag C;
    final android.support.v4.media.session.an a;
    final String b;
    final String c;
    final android.media.AudioManager d;
    final Object e;
    final android.os.RemoteCallbackList f;
    boolean g;
    android.support.v4.media.session.ad h;
    int i;
    android.support.v4.media.MediaMetadataCompat j;
    android.support.v4.media.session.PlaybackStateCompat k;
    android.app.PendingIntent l;
    java.util.List m;
    CharSequence n;
    int o;
    android.os.Bundle p;
    int q;
    int r;
    android.support.v4.media.ae s;
    private final android.content.Context t;
    private final android.content.ComponentName u;
    private final android.app.PendingIntent v;
    private final Object w;
    private final android.support.v4.media.session.am x;
    private final android.support.v4.media.session.MediaSessionCompat$Token y;
    private boolean z;

    public ai(android.content.Context p4, String p5, android.content.ComponentName p6, android.app.PendingIntent p7)
    {
        this.e = new Object();
        this.f = new android.os.RemoteCallbackList();
        this.g = 0;
        this.z = 0;
        this.A = 0;
        this.B = 0;
        this.C = new android.support.v4.media.session.aj(this);
        if (p6 != null) {
            this.t = p4;
            this.b = p4.getPackageName();
            this.d = ((android.media.AudioManager) p4.getSystemService("audio"));
            this.c = p5;
            this.u = p6;
            this.v = p7;
            this.x = new android.support.v4.media.session.am(this);
            this.y = new android.support.v4.media.session.MediaSessionCompat$Token(this.x);
            this.a = new android.support.v4.media.session.an(this, android.os.Looper.myLooper());
            this.o = 0;
            this.q = 1;
            this.r = 3;
            if (android.os.Build$VERSION.SDK_INT < 14) {
                this.w = 0;
            } else {
                this.w = new android.media.RemoteControlClient(p7);
            }
            return;
        } else {
            throw new IllegalArgumentException("MediaButtonReceiver component may not be null.");
        }
    }

    private static synthetic android.support.v4.media.ae a(android.support.v4.media.session.ai p1)
    {
        return p1.s;
    }

    private void a(int p3, int p4)
    {
        if (this.q != 2) {
            this.d.adjustStreamVolume(p3, this.r, p4);
        } else {
            // Both branches of the condition point to the same code.
            // if (this.s == null) {
            // }
        }
        return;
    }

    static synthetic void a(android.support.v4.media.session.ai p2, int p3, int p4)
    {
        if (p2.q != 2) {
            p2.d.adjustStreamVolume(p3, p2.r, p4);
        } else {
            // Both branches of the condition point to the same code.
            // if (p2.s == null) {
            // }
        }
        return;
    }

    private static synthetic void a(android.support.v4.media.session.ai p0, android.support.v4.media.session.ParcelableVolumeInfo p1)
    {
        p0.a(p1);
        return;
    }

    private static synthetic int b(android.support.v4.media.session.ai p1)
    {
        return p1.q;
    }

    private void b(int p3, int p4)
    {
        if (this.q != 2) {
            this.d.setStreamVolume(this.r, p3, p4);
        } else {
            // Both branches of the condition point to the same code.
            // if (this.s == null) {
            // }
        }
        return;
    }

    private void b(android.support.v4.media.MediaMetadataCompat p3)
    {
        int v1 = (this.f.beginBroadcast() - 1);
        while (v1 >= 0) {
            try {
                ((android.support.v4.media.session.a) this.f.getBroadcastItem(v1)).a(p3);
            } catch (int v0) {
            }
            v1--;
        }
        this.f.finishBroadcast();
        return;
    }

    private void b(android.support.v4.media.session.PlaybackStateCompat p3)
    {
        int v1 = (this.f.beginBroadcast() - 1);
        while (v1 >= 0) {
            try {
                ((android.support.v4.media.session.a) this.f.getBroadcastItem(v1)).a(p3);
            } catch (int v0) {
            }
            v1--;
        }
        this.f.finishBroadcast();
        return;
    }

    static synthetic void b(android.support.v4.media.session.ai p2, int p3, int p4)
    {
        if (p2.q != 2) {
            p2.d.setStreamVolume(p2.r, p3, p4);
        } else {
            // Both branches of the condition point to the same code.
            // if (p2.s == null) {
            // }
        }
        return;
    }

    private void b(CharSequence p3)
    {
        int v1 = (this.f.beginBroadcast() - 1);
        while (v1 >= 0) {
            try {
                ((android.support.v4.media.session.a) this.f.getBroadcastItem(v1)).a(p3);
            } catch (int v0) {
            }
            v1--;
        }
        this.f.finishBroadcast();
        return;
    }

    private void b(String p3, android.os.Bundle p4)
    {
        int v1 = (this.f.beginBroadcast() - 1);
        while (v1 >= 0) {
            try {
                ((android.support.v4.media.session.a) this.f.getBroadcastItem(v1)).a(p3, p4);
            } catch (int v0) {
            }
            v1--;
        }
        this.f.finishBroadcast();
        return;
    }

    private void b(java.util.List p3)
    {
        int v1 = (this.f.beginBroadcast() - 1);
        while (v1 >= 0) {
            try {
                ((android.support.v4.media.session.a) this.f.getBroadcastItem(v1)).a(p3);
            } catch (int v0) {
            }
            v1--;
        }
        this.f.finishBroadcast();
        return;
    }

    private static synthetic int c(android.support.v4.media.session.ai p1)
    {
        return p1.r;
    }

    private static synthetic android.support.v4.media.session.an d(android.support.v4.media.session.ai p1)
    {
        return p1.a;
    }

    private static synthetic int e(android.support.v4.media.session.ai p1)
    {
        return p1.i;
    }

    private static synthetic boolean f(android.support.v4.media.session.ai p1)
    {
        return p1.g;
    }

    private static synthetic android.os.RemoteCallbackList g(android.support.v4.media.session.ai p1)
    {
        return p1.f;
    }

    private boolean g()
    {
        android.media.RemoteControlClient v0_32;
        if (!this.z) {
            if (this.B) {
                if (android.os.Build$VERSION.SDK_INT < 18) {
                    android.support.v4.media.session.bh.a(this.t, this.u);
                } else {
                    android.support.v4.media.session.av.a(this.t, this.v);
                }
                this.B = 0;
            }
            if (!this.A) {
                v0_32 = 0;
            } else {
                android.support.v4.media.session.at.a(this.w, 0);
                android.support.v4.media.session.at.a(this.t, this.w);
                this.A = 0;
            }
        } else {
            if (android.os.Build$VERSION.SDK_INT >= 8) {
                if ((this.B) || ((this.i & 1) == 0)) {
                    if ((this.B) && ((this.i & 1) == 0)) {
                        if (android.os.Build$VERSION.SDK_INT < 18) {
                            android.support.v4.media.session.bh.a(this.t, this.u);
                        } else {
                            android.support.v4.media.session.av.a(this.t, this.v);
                        }
                        this.B = 0;
                    }
                } else {
                    if (android.os.Build$VERSION.SDK_INT < 18) {
                        ((android.media.AudioManager) this.t.getSystemService("audio")).registerMediaButtonEventReceiver(this.u);
                    } else {
                        ((android.media.AudioManager) this.t.getSystemService("audio")).registerMediaButtonEventReceiver(this.v);
                    }
                    this.B = 1;
                }
            }
            if (android.os.Build$VERSION.SDK_INT < 14) {
            } else {
                if ((this.A) || ((this.i & 2) == 0)) {
                    if ((!this.A) || ((this.i & 2) != 0)) {
                    } else {
                        android.support.v4.media.session.at.a(this.w, 0);
                        android.support.v4.media.session.at.a(this.t, this.w);
                        this.A = 0;
                        v0_32 = 0;
                    }
                } else {
                    ((android.media.AudioManager) this.t.getSystemService("audio")).registerRemoteControlClient(((android.media.RemoteControlClient) this.w));
                    this.A = 1;
                    v0_32 = 1;
                }
            }
        }
        return v0_32;
    }

    private static synthetic String h(android.support.v4.media.session.ai p1)
    {
        return p1.b;
    }

    private void h()
    {
        int v1 = (this.f.beginBroadcast() - 1);
        while (v1 >= 0) {
            try {
                ((android.support.v4.media.session.a) this.f.getBroadcastItem(v1)).a();
            } catch (int v0) {
            }
            v1--;
        }
        this.f.finishBroadcast();
        this.f.kill();
        return;
    }

    private static synthetic String i(android.support.v4.media.session.ai p1)
    {
        return p1.c;
    }

    private static synthetic Object j(android.support.v4.media.session.ai p1)
    {
        return p1.e;
    }

    private static synthetic android.app.PendingIntent k(android.support.v4.media.session.ai p1)
    {
        return p1.l;
    }

    private static synthetic android.media.AudioManager l(android.support.v4.media.session.ai p1)
    {
        return p1.d;
    }

    private static synthetic android.support.v4.media.MediaMetadataCompat m(android.support.v4.media.session.ai p1)
    {
        return p1.j;
    }

    private static synthetic android.support.v4.media.session.PlaybackStateCompat n(android.support.v4.media.session.ai p1)
    {
        return p1.f();
    }

    private static synthetic java.util.List o(android.support.v4.media.session.ai p1)
    {
        return p1.m;
    }

    private static synthetic CharSequence p(android.support.v4.media.session.ai p1)
    {
        return p1.n;
    }

    private static synthetic android.os.Bundle q(android.support.v4.media.session.ai p1)
    {
        return p1.p;
    }

    private static synthetic int r(android.support.v4.media.session.ai p1)
    {
        return p1.o;
    }

    private static synthetic android.support.v4.media.session.ad s(android.support.v4.media.session.ai p1)
    {
        return p1.h;
    }

    private static synthetic android.support.v4.media.session.PlaybackStateCompat t(android.support.v4.media.session.ai p1)
    {
        return p1.k;
    }

    public final void a(int p3)
    {
        this.i = p3;
        this.g();
        return;
    }

    public final void a(android.app.PendingIntent p3)
    {
        try {
            this.l = p3;
            return;
        } catch (Throwable v0) {
            throw v0;
        }
    }

    public final void a(android.os.Bundle p1)
    {
        this.p = p1;
        return;
    }

    public final void a(android.support.v4.media.MediaMetadataCompat p12)
    {
        android.os.Parcelable v1_0 = 0;
        this.j = p12;
        long v2_0 = (this.f.beginBroadcast() - 1);
        while (v2_0 >= 0) {
            try {
                ((android.support.v4.media.session.a) this.f.getBroadcastItem(v2_0)).a(p12);
            } catch (android.media.RemoteControlClient$MetadataEditor v0) {
            }
            v2_0--;
        }
        this.f.finishBroadcast();
        if (this.z) {
            if (android.os.Build$VERSION.SDK_INT < 19) {
                if (android.os.Build$VERSION.SDK_INT >= 14) {
                    if (p12 != null) {
                        v1_0 = p12.C;
                    }
                    android.media.RemoteControlClient$MetadataEditor v0_10 = ((android.media.RemoteControlClient) this.w).editMetadata(1);
                    android.support.v4.media.session.at.a(v1_0, v0_10);
                    v0_10.apply();
                }
            } else {
                if (p12 != null) {
                    v1_0 = p12.C;
                }
                long v2_5;
                if (this.k != null) {
                    v2_5 = this.k.F;
                } else {
                    v2_5 = 0;
                }
                android.media.RemoteControlClient$MetadataEditor v0_13 = ((android.media.RemoteControlClient) this.w).editMetadata(1);
                android.support.v4.media.session.at.a(v1_0, v0_13);
                if (v1_0 != null) {
                    if (v1_0.containsKey("android.media.metadata.YEAR")) {
                        v0_13.putLong(8, v1_0.getLong("android.media.metadata.YEAR"));
                    }
                    if (v1_0.containsKey("android.media.metadata.RATING")) {
                        v0_13.putObject(101, v1_0.getParcelable("android.media.metadata.RATING"));
                    }
                    if (v1_0.containsKey("android.media.metadata.USER_RATING")) {
                        v0_13.putObject(268435457, v1_0.getParcelable("android.media.metadata.USER_RATING"));
                    }
                }
                if ((v2_5 & 128) != 0) {
                    v0_13.addEditableKey(268435457);
                }
                v0_13.apply();
            }
        }
        return;
    }

    public final void a(android.support.v4.media.ae p7)
    {
        if (p7 != null) {
            if (this.s != null) {
                this.s.g = 0;
            }
            this.q = 2;
            this.s = p7;
            this.a(new android.support.v4.media.session.ParcelableVolumeInfo(this.q, this.r, this.s.d, this.s.e, this.s.f));
            p7.g = this.C;
            return;
        } else {
            throw new IllegalArgumentException("volumeProvider may not be null");
        }
    }

    final void a(android.support.v4.media.session.ParcelableVolumeInfo p3)
    {
        int v1 = (this.f.beginBroadcast() - 1);
        while (v1 >= 0) {
            try {
                ((android.support.v4.media.session.a) this.f.getBroadcastItem(v1)).a(p3);
            } catch (int v0) {
            }
            v1--;
        }
        this.f.finishBroadcast();
        return;
    }

    public final void a(android.support.v4.media.session.PlaybackStateCompat p15)
    {
        this.k = p15;
        int v1_0 = (this.f.beginBroadcast() - 1);
        while (v1_0 >= 0) {
            try {
                ((android.support.v4.media.session.a) this.f.getBroadcastItem(v1_0)).a(p15);
            } catch (Object v0) {
            }
            v1_0--;
        }
        this.f.finishBroadcast();
        if (this.z) {
            if (p15 != null) {
                if (android.os.Build$VERSION.SDK_INT < 18) {
                    if (android.os.Build$VERSION.SDK_INT >= 14) {
                        android.support.v4.media.session.at.a(this.w, p15.B);
                    }
                } else {
                    long v2_1;
                    Object v0_9 = this.w;
                    int v1_2 = p15.B;
                    long v6_0 = p15.C;
                    float v8 = p15.E;
                    long v2_0 = p15.H;
                    long v10 = android.os.SystemClock.elapsedRealtime();
                    if ((v1_2 != 3) || (v6_0 <= 0)) {
                        v2_1 = v6_0;
                    } else {
                        long v2_2;
                        if (v2_0 <= 0) {
                            v2_2 = 0;
                        } else {
                            v2_2 = (v10 - v2_0);
                            if ((v8 > 0) && (v8 != 1065353216)) {
                                v2_2 = ((long) (((float) v2_2) * v8));
                            }
                        }
                        v2_1 = (v2_2 + v6_0);
                    }
                    ((android.media.RemoteControlClient) v0_9).setPlaybackState(android.support.v4.media.session.at.a(v1_2), v2_1, v8);
                }
                if (android.os.Build$VERSION.SDK_INT < 19) {
                    if (android.os.Build$VERSION.SDK_INT < 18) {
                        if (android.os.Build$VERSION.SDK_INT >= 14) {
                            android.support.v4.media.session.at.a(this.w, p15.F);
                        }
                    } else {
                        ((android.media.RemoteControlClient) this.w).setTransportControlFlags(android.support.v4.media.session.av.a(p15.F));
                    }
                } else {
                    long v2_7 = p15.F;
                    Object v0_18 = ((android.media.RemoteControlClient) this.w);
                    int v1_6 = android.support.v4.media.session.av.a(v2_7);
                    if ((v2_7 & 128) != 0) {
                        v1_6 |= 512;
                    }
                    v0_18.setTransportControlFlags(v1_6);
                }
            } else {
                if (android.os.Build$VERSION.SDK_INT >= 14) {
                    android.support.v4.media.session.at.a(this.w, 0);
                    android.support.v4.media.session.at.a(this.w, 0);
                }
            }
        }
        return;
    }

    public final void a(android.support.v4.media.session.ad p5, android.os.Handler p6)
    {
        if (p5 != this.h) {
            if ((p5 != null) && (android.os.Build$VERSION.SDK_INT >= 18)) {
                Object v0_3 = new android.support.v4.media.session.ak(this, p5);
                if (android.os.Build$VERSION.SDK_INT >= 18) {
                    android.support.v4.media.session.av.a(this.w, new android.support.v4.media.session.aw(v0_3));
                }
                if (android.os.Build$VERSION.SDK_INT >= 19) {
                    android.support.v4.media.session.ax.a(this.w, new android.support.v4.media.session.ay(v0_3));
                }
            } else {
                if (android.os.Build$VERSION.SDK_INT >= 18) {
                    android.support.v4.media.session.av.a(this.w, 0);
                }
                if (android.os.Build$VERSION.SDK_INT >= 19) {
                    android.support.v4.media.session.ax.a(this.w, 0);
                }
            }
            this.h = p5;
        }
        return;
    }

    public final void a(CharSequence p3)
    {
        this.n = p3;
        int v1 = (this.f.beginBroadcast() - 1);
        while (v1 >= 0) {
            try {
                ((android.support.v4.media.session.a) this.f.getBroadcastItem(v1)).a(p3);
            } catch (int v0) {
            }
            v1--;
        }
        this.f.finishBroadcast();
        return;
    }

    public final void a(String p3, android.os.Bundle p4)
    {
        int v1 = (this.f.beginBroadcast() - 1);
        while (v1 >= 0) {
            try {
                ((android.support.v4.media.session.a) this.f.getBroadcastItem(v1)).a(p3, p4);
            } catch (int v0) {
            }
            v1--;
        }
        this.f.finishBroadcast();
        return;
    }

    public final void a(java.util.List p3)
    {
        this.m = p3;
        int v1 = (this.f.beginBroadcast() - 1);
        while (v1 >= 0) {
            try {
                ((android.support.v4.media.session.a) this.f.getBroadcastItem(v1)).a(p3);
            } catch (int v0) {
            }
            v1--;
        }
        this.f.finishBroadcast();
        return;
    }

    public final void a(boolean p2)
    {
        if (p2 != this.z) {
            this.z = p2;
            if (this.g()) {
                this.a(this.j);
                this.a(this.k);
            }
        }
        return;
    }

    public final boolean a()
    {
        return this.z;
    }

    public final void b()
    {
        this.z = 0;
        this.g = 1;
        this.g();
        int v1 = (this.f.beginBroadcast() - 1);
        while (v1 >= 0) {
            try {
                ((android.support.v4.media.session.a) this.f.getBroadcastItem(v1)).a();
            } catch (int v0) {
            }
            v1--;
        }
        this.f.finishBroadcast();
        this.f.kill();
        return;
    }

    public final void b(int p8)
    {
        if (this.s != null) {
            this.s.g = 0;
        }
        this.q = 1;
        this.a(new android.support.v4.media.session.ParcelableVolumeInfo(this.q, this.r, 2, this.d.getStreamMaxVolume(this.r), this.d.getStreamVolume(this.r)));
        return;
    }

    public final void b(android.app.PendingIntent p1)
    {
        return;
    }

    public final android.support.v4.media.session.MediaSessionCompat$Token c()
    {
        return this.y;
    }

    public final void c(int p1)
    {
        this.o = p1;
        return;
    }

    public final Object d()
    {
        return 0;
    }

    public final Object e()
    {
        return this.w;
    }

    final android.support.v4.media.session.PlaybackStateCompat f()
    {
        android.support.v4.media.session.PlaybackStateCompat v4_0 = -1;
        android.support.v4.media.session.PlaybackStateCompat v19 = this.k;
        if ((this.j != null) && (this.j.C.containsKey("android.media.metadata.DURATION"))) {
            v4_0 = this.j.b("android.media.metadata.DURATION");
        }
        android.support.v4.media.session.PlaybackStateCompat v2_6 = 0;
        if ((v19 != null) && ((v19.B == 3) || ((v19.B == 4) || (v19.B == 5)))) {
            long v10_0 = v19.H;
            long v7 = android.os.SystemClock.elapsedRealtime();
            if (v10_0 > 0) {
                android.support.v4.media.session.PlaybackStateCompat v2_10 = (((long) (v19.E * ((float) (v7 - v10_0)))) + v19.C);
                if ((v4_0 < 0) || (v2_10 <= v4_0)) {
                    if (v2_10 >= 0) {
                        v4_0 = v2_10;
                    } else {
                        v4_0 = 0;
                    }
                }
                android.support.v4.media.session.PlaybackStateCompat v2_12 = new android.support.v4.media.session.bl(v19);
                v2_12.a(v19.B, v4_0, v19.E, v7);
                android.support.v4.media.session.PlaybackStateCompat v20 = new android.support.v4.media.session.PlaybackStateCompat;
                v20(v2_12.b, v2_12.c, v2_12.d, v2_12.e, v2_12.f, v2_12.g, v2_12.h, v2_12.a, v2_12.i, v2_12.j, 0);
                v2_6 = v20;
            }
        }
        if (v2_6 == null) {
            v2_6 = v19;
        }
        return v2_6;
    }
}
