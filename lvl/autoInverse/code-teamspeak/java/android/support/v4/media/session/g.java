package android.support.v4.media.session;
public final class g {
    private static final String a = "MediaControllerCompat";
    private final android.support.v4.media.session.m b;
    private final android.support.v4.media.session.MediaSessionCompat$Token c;

    private g(android.content.Context p3, android.support.v4.media.session.MediaSessionCompat$Token p4)
    {
        if (p4 != null) {
            this.c = p4;
            if (android.os.Build$VERSION.SDK_INT < 21) {
                this.b = new android.support.v4.media.session.p(this.c);
            } else {
                this.b = new android.support.v4.media.session.n(p3, p4);
            }
            return;
        } else {
            throw new IllegalArgumentException("sessionToken must not be null");
        }
    }

    public g(android.content.Context p3, android.support.v4.media.session.MediaSessionCompat p4)
    {
        if (p4 != null) {
            this.c = p4.a();
            if (android.os.Build$VERSION.SDK_INT < 23) {
                if (android.os.Build$VERSION.SDK_INT < 21) {
                    this.b = new android.support.v4.media.session.p(this.c);
                } else {
                    this.b = new android.support.v4.media.session.n(p3, p4);
                }
            } else {
                this.b = new android.support.v4.media.session.o(p3, p4);
            }
            return;
        } else {
            throw new IllegalArgumentException("session must not be null");
        }
    }

    private android.support.v4.media.session.r a()
    {
        return this.b.a();
    }

    private void a(int p2, int p3)
    {
        this.b.a(p2, p3);
        return;
    }

    private void a(android.support.v4.media.session.i p3)
    {
        if (p3 != null) {
            this.b.a(p3, new android.os.Handler());
            return;
        } else {
            throw new IllegalArgumentException("callback cannot be null");
        }
    }

    private void a(String p3, android.os.Bundle p4, android.os.ResultReceiver p5)
    {
        if (!android.text.TextUtils.isEmpty(p3)) {
            this.b.a(p3, p4, p5);
            return;
        } else {
            throw new IllegalArgumentException("command cannot be null or empty");
        }
    }

    private boolean a(android.view.KeyEvent p3)
    {
        if (p3 != null) {
            return this.b.a(p3);
        } else {
            throw new IllegalArgumentException("KeyEvent may not be null");
        }
    }

    private android.support.v4.media.session.PlaybackStateCompat b()
    {
        return this.b.b();
    }

    private void b(int p2, int p3)
    {
        this.b.b(p2, p3);
        return;
    }

    private void b(android.support.v4.media.session.i p3)
    {
        if (p3 != null) {
            this.b.a(p3, new android.os.Handler());
            return;
        } else {
            throw new IllegalArgumentException("callback cannot be null");
        }
    }

    private android.support.v4.media.MediaMetadataCompat c()
    {
        return this.b.c();
    }

    private void c(android.support.v4.media.session.i p3)
    {
        if (p3 != null) {
            this.b.a(p3);
            return;
        } else {
            throw new IllegalArgumentException("callback cannot be null");
        }
    }

    private java.util.List d()
    {
        return this.b.d();
    }

    private CharSequence e()
    {
        return this.b.e();
    }

    private android.os.Bundle f()
    {
        return this.b.f();
    }

    private int g()
    {
        return this.b.g();
    }

    private long h()
    {
        return this.b.h();
    }

    private android.support.v4.media.session.q i()
    {
        return this.b.i();
    }

    private android.app.PendingIntent j()
    {
        return this.b.j();
    }

    private android.support.v4.media.session.MediaSessionCompat$Token k()
    {
        return this.c;
    }

    private String l()
    {
        return this.b.k();
    }

    private Object m()
    {
        return this.b.l();
    }
}
