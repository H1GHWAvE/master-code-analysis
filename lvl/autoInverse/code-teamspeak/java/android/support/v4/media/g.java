package android.support.v4.media;
final class g implements android.os.Parcelable$Creator {

    g()
    {
        return;
    }

    private static android.support.v4.media.MediaMetadataCompat a(android.os.Parcel p2)
    {
        return new android.support.v4.media.MediaMetadataCompat(p2, 0);
    }

    private static android.support.v4.media.MediaMetadataCompat[] a(int p1)
    {
        android.support.v4.media.MediaMetadataCompat[] v0 = new android.support.v4.media.MediaMetadataCompat[p1];
        return v0;
    }

    public final synthetic Object createFromParcel(android.os.Parcel p3)
    {
        return new android.support.v4.media.MediaMetadataCompat(p3, 0);
    }

    public final bridge synthetic Object[] newArray(int p2)
    {
        android.support.v4.media.MediaMetadataCompat[] v0 = new android.support.v4.media.MediaMetadataCompat[p2];
        return v0;
    }
}
