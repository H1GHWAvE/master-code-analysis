package android.support.v4.media.session;
public final class MediaSessionCompat {
    public static final int a = 1;
    public static final int b = 2;
    private final android.support.v4.media.session.ag c;
    private final android.support.v4.media.session.g d;
    private final java.util.ArrayList e;

    private MediaSessionCompat(android.content.Context p2, android.support.v4.media.session.ag p3)
    {
        this.e = new java.util.ArrayList();
        this.c = p3;
        this.d = new android.support.v4.media.session.g(p2, this);
        return;
    }

    private MediaSessionCompat(android.content.Context p4, String p5, android.content.ComponentName p6, android.app.PendingIntent p7)
    {
        this.e = new java.util.ArrayList();
        if (p4 != null) {
            if (!android.text.TextUtils.isEmpty(p5)) {
                if ((p6 != null) && (p7 == null)) {
                    android.support.v4.media.session.g v0_4 = new android.content.Intent("android.intent.action.MEDIA_BUTTON");
                    v0_4.setComponent(p6);
                    p7 = android.app.PendingIntent.getBroadcast(p4, 0, v0_4, 0);
                }
                if (android.os.Build$VERSION.SDK_INT < 21) {
                    this.c = new android.support.v4.media.session.ai(p4, p5, p6, p7);
                } else {
                    this.c = new android.support.v4.media.session.ah(p4, p5);
                    this.c.b(p7);
                }
                this.d = new android.support.v4.media.session.g(p4, this);
                return;
            } else {
                throw new IllegalArgumentException("tag must not be null or empty");
            }
        } else {
            throw new IllegalArgumentException("context must not be null");
        }
    }

    private static android.support.v4.media.session.MediaSessionCompat a(android.content.Context p2, Object p3)
    {
        return new android.support.v4.media.session.MediaSessionCompat(p2, new android.support.v4.media.session.ah(p3));
    }

    private void a(int p2)
    {
        this.c.a(p2);
        return;
    }

    private void a(android.app.PendingIntent p2)
    {
        this.c.a(p2);
        return;
    }

    private void a(android.os.Bundle p2)
    {
        this.c.a(p2);
        return;
    }

    private void a(android.support.v4.media.MediaMetadataCompat p2)
    {
        this.c.a(p2);
        return;
    }

    private void a(android.support.v4.media.ae p3)
    {
        if (p3 != null) {
            this.c.a(p3);
            return;
        } else {
            throw new IllegalArgumentException("volumeProvider may not be null!");
        }
    }

    private void a(android.support.v4.media.session.PlaybackStateCompat p2)
    {
        this.c.a(p2);
        return;
    }

    private void a(android.support.v4.media.session.ad p3)
    {
        this.c.a(p3, new android.os.Handler());
        return;
    }

    private void a(android.support.v4.media.session.ao p3)
    {
        if (p3 != null) {
            this.e.add(p3);
            return;
        } else {
            throw new IllegalArgumentException("Listener may not be null");
        }
    }

    private void a(CharSequence p2)
    {
        this.c.a(p2);
        return;
    }

    private void a(String p3, android.os.Bundle p4)
    {
        if (!android.text.TextUtils.isEmpty(p3)) {
            this.c.a(p3, p4);
            return;
        } else {
            throw new IllegalArgumentException("event cannot be null or empty");
        }
    }

    private void a(java.util.List p2)
    {
        this.c.a(p2);
        return;
    }

    private void a(boolean p3)
    {
        this.c.a(p3);
        java.util.Iterator v0_2 = this.e.iterator();
        while (v0_2.hasNext()) {
            v0_2.next();
        }
        return;
    }

    private void b(int p2)
    {
        this.c.b(p2);
        return;
    }

    private void b(android.app.PendingIntent p2)
    {
        this.c.b(p2);
        return;
    }

    private void b(android.support.v4.media.session.ad p3)
    {
        this.c.a(p3, new android.os.Handler());
        return;
    }

    private void b(android.support.v4.media.session.ao p3)
    {
        if (p3 != null) {
            this.e.remove(p3);
            return;
        } else {
            throw new IllegalArgumentException("Listener may not be null");
        }
    }

    private boolean b()
    {
        return this.c.a();
    }

    private void c()
    {
        this.c.b();
        return;
    }

    private void c(int p2)
    {
        this.c.c(p2);
        return;
    }

    private android.support.v4.media.session.g d()
    {
        return this.d;
    }

    private Object e()
    {
        return this.c.d();
    }

    private Object f()
    {
        return this.c.e();
    }

    public final android.support.v4.media.session.MediaSessionCompat$Token a()
    {
        return this.c.c();
    }
}
