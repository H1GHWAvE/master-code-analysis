package android.support.v4.media.session;
final class bp {

    bp()
    {
        return;
    }

    private static int a(Object p1)
    {
        return ((android.media.session.PlaybackState) p1).getState();
    }

    public static Object a(int p9, long p10, long p12, float p14, long p15, CharSequence p17, long p18, java.util.List p20, long p21)
    {
        android.media.session.PlaybackState v2_1 = new android.media.session.PlaybackState$Builder();
        v2_1.setState(p9, p10, p14, p18);
        v2_1.setBufferedPosition(p12);
        v2_1.setActions(p15);
        v2_1.setErrorMessage(p17);
        java.util.Iterator v4_1 = p20.iterator();
        while (v4_1.hasNext()) {
            v2_1.addCustomAction(((android.media.session.PlaybackState$CustomAction) v4_1.next()));
        }
        v2_1.setActiveQueueItemId(p21);
        return v2_1.build();
    }

    private static long b(Object p2)
    {
        return ((android.media.session.PlaybackState) p2).getPosition();
    }

    private static long c(Object p2)
    {
        return ((android.media.session.PlaybackState) p2).getBufferedPosition();
    }

    private static float d(Object p1)
    {
        return ((android.media.session.PlaybackState) p1).getPlaybackSpeed();
    }

    private static long e(Object p2)
    {
        return ((android.media.session.PlaybackState) p2).getActions();
    }

    private static CharSequence f(Object p1)
    {
        return ((android.media.session.PlaybackState) p1).getErrorMessage();
    }

    private static long g(Object p2)
    {
        return ((android.media.session.PlaybackState) p2).getLastPositionUpdateTime();
    }

    private static java.util.List h(Object p1)
    {
        return ((android.media.session.PlaybackState) p1).getCustomActions();
    }

    private static long i(Object p2)
    {
        return ((android.media.session.PlaybackState) p2).getActiveQueueItemId();
    }
}
