package android.support.v4.media;
public final class RatingCompat implements android.os.Parcelable {
    public static final android.os.Parcelable$Creator CREATOR = None;
    public static final int a = 0;
    public static final int b = 1;
    public static final int c = 2;
    public static final int d = 3;
    public static final int e = 4;
    public static final int f = 5;
    public static final int g = 6;
    private static final String h = "Rating";
    private static final float i = 49024;
    private final int j;
    private final float k;
    private Object l;

    static RatingCompat()
    {
        android.support.v4.media.RatingCompat.CREATOR = new android.support.v4.media.o();
        return;
    }

    private RatingCompat(int p1, float p2)
    {
        this.j = p1;
        this.k = p2;
        return;
    }

    synthetic RatingCompat(int p1, float p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    private static android.support.v4.media.RatingCompat a(float p2)
    {
        if ((p2 >= 0) && (p2 <= 1120403456)) {
            android.support.v4.media.RatingCompat v0_5 = new android.support.v4.media.RatingCompat(6, p2);
        } else {
            android.util.Log.e("Rating", "Invalid percentage-based rating value");
            v0_5 = 0;
        }
        return v0_5;
    }

    private static android.support.v4.media.RatingCompat a(int p2)
    {
        android.support.v4.media.RatingCompat v0_1;
        switch (p2) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                v0_1 = new android.support.v4.media.RatingCompat(p2, -1082130432);
                break;
            default:
                v0_1 = 0;
        }
        return v0_1;
    }

    private static android.support.v4.media.RatingCompat a(int p4, float p5)
    {
        String v1_0;
        android.support.v4.media.RatingCompat v0_0 = 0;
        switch (p4) {
            case 3:
                v1_0 = 1077936128;
                if ((p5 >= 0) && (p5 <= v1_0)) {
                    v0_0 = new android.support.v4.media.RatingCompat(p4, p5);
                } else {
                    android.util.Log.e("Rating", "Trying to set out of range star-based rating");
                }
                break;
            case 4:
                v1_0 = 1082130432;
                break;
            case 5:
                v1_0 = 1084227584;
                break;
            default:
                android.util.Log.e("Rating", new StringBuilder("Invalid rating style (").append(p4).append(") for a star rating").toString());
        }
        return v0_0;
    }

    public static android.support.v4.media.RatingCompat a(Object p5)
    {
        String v1_0 = 1065353216;
        android.support.v4.media.RatingCompat v3_0 = 0;
        if ((p5 != null) && (android.os.Build$VERSION.SDK_INT >= 21)) {
            int v4_1 = ((android.media.Rating) p5).getRatingStyle();
            if (!((android.media.Rating) p5).isRated()) {
                switch (v4_1) {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                        v3_0 = new android.support.v4.media.RatingCompat(v4_1, -1082130432);
                        break;
                    default:
                }
            } else {
                switch (v4_1) {
                    case 1:
                        android.support.v4.media.RatingCompat v0_25;
                        if (!((android.media.Rating) p5).hasHeart()) {
                            v0_25 = 0;
                        } else {
                            v0_25 = 1065353216;
                        }
                        v3_0 = new android.support.v4.media.RatingCompat(1, v0_25);
                        break;
                    case 2:
                        if (!((android.media.Rating) p5).isThumbUp()) {
                            v1_0 = 0;
                        }
                        v3_0 = new android.support.v4.media.RatingCompat(2, v1_0);
                        break;
                    case 3:
                    case 4:
                    case 5:
                        android.support.v4.media.RatingCompat v0_13;
                        android.support.v4.media.RatingCompat v0_16;
                        String v1_6 = ((android.media.Rating) p5).getStarRating();
                        switch (v4_1) {
                            case 3:
                                v0_13 = 1077936128;
                                if ((v1_6 >= 0) && (v1_6 <= v0_13)) {
                                    v0_16 = new android.support.v4.media.RatingCompat(v4_1, v1_6);
                                } else {
                                    android.util.Log.e("Rating", "Trying to set out of range star-based rating");
                                    v0_16 = 0;
                                }
                                break;
                            case 4:
                                v0_13 = 1082130432;
                                break;
                            case 5:
                                v0_13 = 1084227584;
                                break;
                            default:
                                android.util.Log.e("Rating", new StringBuilder("Invalid rating style (").append(v4_1).append(") for a star rating").toString());
                                v0_16 = 0;
                        }
                        v3_0 = v0_16;
                        break;
                    case 6:
                        android.support.v4.media.RatingCompat v0_9 = ((android.media.Rating) p5).getPercentRating();
                        if ((v0_9 >= 0) && (v0_9 <= 1120403456)) {
                            v3_0 = new android.support.v4.media.RatingCompat(6, v0_9);
                        } else {
                            android.util.Log.e("Rating", "Invalid percentage-based rating value");
                        }
                        break;
                    default:
                        return v3_0;
                }
            }
            v3_0.l = p5;
        }
        return v3_0;
    }

    private static android.support.v4.media.RatingCompat a(boolean p3)
    {
        int v0;
        if (!p3) {
            v0 = 0;
        } else {
            v0 = 1065353216;
        }
        return new android.support.v4.media.RatingCompat(1, v0);
    }

    private static android.support.v4.media.RatingCompat b(boolean p3)
    {
        int v0;
        if (!p3) {
            v0 = 0;
        } else {
            v0 = 1065353216;
        }
        return new android.support.v4.media.RatingCompat(2, v0);
    }

    private boolean b()
    {
        int v0_2;
        if (this.k < 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private int c()
    {
        return this.j;
    }

    private boolean d()
    {
        int v0 = 0;
        if ((this.j == 1) && (this.k == 1065353216)) {
            v0 = 1;
        }
        return v0;
    }

    private boolean e()
    {
        int v0 = 0;
        if ((this.j == 2) && (this.k == 1065353216)) {
            v0 = 1;
        }
        return v0;
    }

    private float f()
    {
        float v0_2;
        switch (this.j) {
            case 3:
            case 4:
            case 5:
                if (!this.b()) {
                    v0_2 = -1082130432;
                } else {
                    v0_2 = this.k;
                }
                break;
            default:
        }
        return v0_2;
    }

    private float g()
    {
        if ((this.j == 6) && (this.b())) {
            float v0_2 = this.k;
        } else {
            v0_2 = -1082130432;
        }
        return v0_2;
    }

    public final Object a()
    {
        float v0_12;
        float v2_0 = -1082130432;
        float v0_0 = 1;
        if ((this.l == null) && (android.os.Build$VERSION.SDK_INT >= 21)) {
            if (!this.b()) {
                this.l = android.media.Rating.newUnratedRating(this.j);
            } else {
                switch (this.j) {
                    case 1:
                        if ((this.j != 1) || (this.k != 1065353216)) {
                            v0_0 = 0;
                        }
                        this.l = android.media.Rating.newHeartRating(v0_0);
                        break;
                    case 2:
                        if ((this.j != 2) || (this.k != 1065353216)) {
                            v0_0 = 0;
                        }
                        this.l = android.media.Rating.newThumbRating(v0_0);
                        break;
                    case 3:
                    case 4:
                    case 5:
                        float v0_8;
                        switch (this.j) {
                            case 3:
                            case 4:
                            case 5:
                                if (!this.b()) {
                                    v0_8 = -1082130432;
                                } else {
                                    v0_8 = this.k;
                                }
                                break;
                            default:
                        }
                        this.l = android.media.Rating.newStarRating(this.j, v0_8);
                        break;
                    case 6:
                        if ((this.j == 6) && (this.b())) {
                            v2_0 = this.k;
                        }
                        this.l = android.media.Rating.newPercentageRating(v2_0);
                        break;
                }
                v0_12 = 0;
                return v0_12;
            }
            v0_12 = this.l;
        } else {
            v0_12 = this.l;
        }
        return v0_12;
    }

    public final int describeContents()
    {
        return this.j;
    }

    public final String toString()
    {
        String v0_6;
        StringBuilder v1_3 = new StringBuilder("Rating:style=").append(this.j).append(" rating=");
        if (this.k >= 0) {
            v0_6 = String.valueOf(this.k);
        } else {
            v0_6 = "unrated";
        }
        return v1_3.append(v0_6).toString();
    }

    public final void writeToParcel(android.os.Parcel p2, int p3)
    {
        p2.writeInt(this.j);
        p2.writeFloat(this.k);
        return;
    }
}
