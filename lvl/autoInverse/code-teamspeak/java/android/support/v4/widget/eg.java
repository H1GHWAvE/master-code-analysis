package android.support.v4.widget;
public final class eg {
    private static final android.view.animation.Interpolator L = None;
    public static final int a = 255;
    public static final int b = 0;
    public static final int c = 1;
    public static final int d = 2;
    public static final int e = 1;
    public static final int f = 2;
    public static final int g = 4;
    public static final int h = 8;
    public static final int i = 15;
    public static final int j = 1;
    public static final int k = 2;
    public static final int l = 3;
    private static final String w = "ViewDragHelper";
    private static final int x = 20;
    private static final int y = 256;
    private static final int z = 600;
    private int A;
    private int[] B;
    private int[] C;
    private int[] D;
    private int E;
    private android.view.VelocityTracker F;
    private float G;
    private android.support.v4.widget.ca H;
    private final android.support.v4.widget.ej I;
    private boolean J;
    private final android.view.ViewGroup K;
    private final Runnable M;
    public int m;
    int n;
    float[] o;
    float[] p;
    float[] q;
    float[] r;
    float s;
    int t;
    int u;
    android.view.View v;

    static eg()
    {
        android.support.v4.widget.eg.L = new android.support.v4.widget.eh();
        return;
    }

    private eg(android.content.Context p4, android.view.ViewGroup p5, android.support.v4.widget.ej p6)
    {
        this.A = -1;
        this.M = new android.support.v4.widget.ei(this);
        if (p5 != null) {
            if (p6 != null) {
                this.K = p5;
                this.I = p6;
                android.support.v4.widget.ca v0_3 = android.view.ViewConfiguration.get(p4);
                this.t = ((int) ((p4.getResources().getDisplayMetrics().density * 1101004800) + 1056964608));
                this.n = v0_3.getScaledTouchSlop();
                this.G = ((float) v0_3.getScaledMaximumFlingVelocity());
                this.s = ((float) v0_3.getScaledMinimumFlingVelocity());
                this.H = android.support.v4.widget.ca.a(p4, android.support.v4.widget.eg.L);
                return;
            } else {
                throw new IllegalArgumentException("Callback may not be null");
            }
        } else {
            throw new IllegalArgumentException("Parent view may not be null");
        }
    }

    private static float a(float p3, float p4, float p5)
    {
        float v1_0 = Math.abs(p3);
        if (v1_0 >= p4) {
            if (v1_0 <= p5) {
                p5 = p3;
            } else {
                if (p3 <= 0) {
                    p5 = (- p5);
                }
            }
        } else {
            p5 = 0;
        }
        return p5;
    }

    private int a(int p10, int p11, int p12)
    {
        int v0_20;
        if (p10 != 0) {
            int v0_15;
            int v0_1 = this.K.getWidth();
            int v1_0 = (v0_1 / 2);
            int v0_9 = ((((float) Math.sin(((double) ((float) (((double) (Math.min(1065353216, (((float) Math.abs(p10)) / ((float) v0_1))) - 1056964608)) * 0.4712389167638204))))) * ((float) v1_0)) + ((float) v1_0));
            int v1_2 = Math.abs(p11);
            if (v1_2 <= 0) {
                v0_15 = ((int) (((((float) Math.abs(p10)) / ((float) p12)) + 1065353216) * 1132462080));
            } else {
                v0_15 = (Math.round((Math.abs((v0_9 / ((float) v1_2))) * 1148846080)) * 4);
            }
            v0_20 = Math.min(v0_15, 600);
        } else {
            v0_20 = 0;
        }
        return v0_20;
    }

    private int a(android.view.View p9, int p10, int p11, int p12, int p13)
    {
        float v1_6;
        float v2_0 = android.support.v4.widget.eg.b(p12, ((int) this.s), ((int) this.G));
        int v3_0 = android.support.v4.widget.eg.b(p13, ((int) this.s), ((int) this.G));
        int v0_4 = Math.abs(p10);
        int v4_0 = Math.abs(p11);
        float v1_4 = Math.abs(v2_0);
        int v5 = Math.abs(v3_0);
        int v6 = (v1_4 + v5);
        int v7 = (v0_4 + v4_0);
        if (v2_0 == 0) {
            v1_6 = (((float) v0_4) / ((float) v7));
        } else {
            v1_6 = (((float) v1_4) / ((float) v6));
        }
        int v0_10;
        if (v3_0 == 0) {
            v0_10 = (((float) v4_0) / ((float) v7));
        } else {
            v0_10 = (((float) v5) / ((float) v6));
        }
        return ((int) ((v0_10 * ((float) this.a(p11, v3_0, 0))) + (v1_6 * ((float) this.a(p10, v2_0, this.I.b(p9))))));
    }

    public static android.support.v4.widget.eg a(android.view.ViewGroup p3, float p4, android.support.v4.widget.ej p5)
    {
        android.support.v4.widget.eg v0 = android.support.v4.widget.eg.a(p3, p5);
        v0.n = ((int) (((float) v0.n) * (1065353216 / p4)));
        return v0;
    }

    public static android.support.v4.widget.eg a(android.view.ViewGroup p2, android.support.v4.widget.ej p3)
    {
        return new android.support.v4.widget.eg(p2.getContext(), p2, p3);
    }

    private void a(float p1)
    {
        this.s = p1;
        return;
    }

    private void a(float p12, float p13, int p14)
    {
        int v0_0 = 0;
        if ((this.o == null) || (this.o.length <= p14)) {
            int[] v2_4 = new float[(p14 + 1)];
            int v3_1 = new float[(p14 + 1)];
            int v4_1 = new float[(p14 + 1)];
            int v5_1 = new float[(p14 + 1)];
            int v6_1 = new int[(p14 + 1)];
            int[] v7_1 = new int[(p14 + 1)];
            int[] v8_1 = new int[(p14 + 1)];
            if (this.o != null) {
                System.arraycopy(this.o, 0, v2_4, 0, this.o.length);
                System.arraycopy(this.p, 0, v3_1, 0, this.p.length);
                System.arraycopy(this.q, 0, v4_1, 0, this.q.length);
                System.arraycopy(this.r, 0, v5_1, 0, this.r.length);
                System.arraycopy(this.B, 0, v6_1, 0, this.B.length);
                System.arraycopy(this.C, 0, v7_1, 0, this.C.length);
                System.arraycopy(this.D, 0, v8_1, 0, this.D.length);
            }
            this.o = v2_4;
            this.p = v3_1;
            this.q = v4_1;
            this.r = v5_1;
            this.B = v6_1;
            this.C = v7_1;
            this.D = v8_1;
        }
        int[] v2_5 = this.o;
        this.q[p14] = p12;
        v2_5[p14] = p12;
        int[] v2_6 = this.p;
        this.r[p14] = p13;
        v2_6[p14] = p13;
        if (((int) p12) < (this.K.getLeft() + this.t)) {
            v0_0 = 1;
        }
        if (((int) p13) < (this.K.getTop() + this.t)) {
            v0_0 |= 4;
        }
        if (((int) p12) > (this.K.getRight() - this.t)) {
            v0_0 |= 2;
        }
        if (((int) p13) > (this.K.getBottom() - this.t)) {
            v0_0 |= 8;
        }
        this.B[p14] = v0_0;
        this.E = (this.E | (1 << p14));
        return;
    }

    private boolean a(float p5, float p6, int p7, int p8)
    {
        int v0 = 0;
        float v1_0 = Math.abs(p5);
        float v2_0 = Math.abs(p6);
        if ((((this.B[p7] & p8) == p8) && (((this.u & p8) != 0) && (((this.D[p7] & p8) != p8) && (((this.C[p7] & p8) != p8) && ((v1_0 > ((float) this.n)) || (v2_0 > ((float) this.n))))))) && (((this.C[p7] & p8) == 0) && (v1_0 > ((float) this.n)))) {
            v0 = 1;
        }
        return v0;
    }

    private boolean a(int p15, int p16, int p17, int p18)
    {
        int v1_18;
        int v3 = this.v.getLeft();
        int v4 = this.v.getTop();
        int v5 = (p15 - v3);
        int v6 = (p16 - v4);
        if ((v5 != 0) || (v6 != 0)) {
            Object v2_6;
            int v7_0 = this.v;
            int v8_0 = android.support.v4.widget.eg.b(p17, ((int) this.s), ((int) this.G));
            int v9 = android.support.v4.widget.eg.b(p18, ((int) this.s), ((int) this.G));
            int v1_6 = Math.abs(v5);
            android.support.v4.widget.ej v10_0 = Math.abs(v6);
            Object v2_4 = Math.abs(v8_0);
            int v11 = Math.abs(v9);
            int v12 = (v2_4 + v11);
            int v13 = (v1_6 + v10_0);
            if (v8_0 == 0) {
                v2_6 = (((float) v1_6) / ((float) v13));
            } else {
                v2_6 = (((float) v2_4) / ((float) v12));
            }
            int v1_12;
            if (v9 == 0) {
                v1_12 = (((float) v10_0) / ((float) v13));
            } else {
                v1_12 = (((float) v11) / ((float) v12));
            }
            Object v2_9 = this.H;
            v2_9.b.a(v2_9.a, v3, v4, v5, v6, ((int) ((v1_12 * ((float) this.a(v6, v9, 0))) + (v2_6 * ((float) this.a(v5, v8_0, this.I.b(v7_0)))))));
            this.b(2);
            v1_18 = 1;
        } else {
            this.H.g();
            this.b(0);
            v1_18 = 0;
        }
        return v1_18;
    }

    private boolean a(android.view.View p5, float p6)
    {
        int v0 = 0;
        if (p5 != null) {
            float v2_2;
            if (this.I.b(p5) <= 0) {
                v2_2 = 0;
            } else {
                v2_2 = 1;
            }
            if ((v2_2 != 0) && (Math.abs(p6) > ((float) this.n))) {
                v0 = 1;
            }
        }
        return v0;
    }

    private static float b(float p4)
    {
        return ((float) Math.sin(((double) ((float) (((double) (p4 - 1056964608)) * 0.4712389167638204)))));
    }

    private static int b(int p1, int p2, int p3)
    {
        int v0 = Math.abs(p1);
        if (v0 >= p2) {
            if (v0 <= p3) {
                p3 = p1;
            } else {
                if (p1 <= 0) {
                    p3 = (- p3);
                }
            }
        } else {
            p3 = 0;
        }
        return p3;
    }

    private void b(float p4, float p5, int p6)
    {
        int v0 = 1;
        if (!this.a(p4, p5, p6, 1)) {
            v0 = 0;
        }
        if (this.a(p5, p4, p6, 4)) {
            v0 |= 4;
        }
        if (this.a(p4, p5, p6, 2)) {
            v0 |= 2;
        }
        if (this.a(p5, p4, p6, 8)) {
            v0 |= 8;
        }
        if (v0 != 0) {
            android.support.v4.widget.ej v1_7 = this.C;
            v1_7[p6] = (v1_7[p6] | v0);
            this.I.a(v0, p6);
        }
        return;
    }

    private void b(int p10, int p11, int p12, int p13)
    {
        if (this.J) {
            this.H.a(this.v.getLeft(), this.v.getTop(), ((int) android.support.v4.view.cs.a(this.F, this.A)), ((int) android.support.v4.view.cs.b(this.F, this.A)), p10, p12, p11, p13);
            this.b(2);
            return;
        } else {
            throw new IllegalStateException("Cannot flingCapturedView outside of a call to Callback#onViewReleased");
        }
    }

    private boolean b(android.view.View p3, int p4)
    {
        int v0 = 1;
        if ((p3 != this.v) || (this.A != p4)) {
            if ((p3 == null) || (!this.I.a(p3))) {
                v0 = 0;
            } else {
                this.A = p4;
                this.a(p3, p4);
            }
        }
        return v0;
    }

    public static boolean b(android.view.View p2, int p3, int p4)
    {
        int v0 = 0;
        if ((p2 != null) && ((p3 >= p2.getLeft()) && ((p3 < p2.getRight()) && ((p4 >= p2.getTop()) && (p4 < p2.getBottom()))))) {
            v0 = 1;
        }
        return v0;
    }

    private boolean b(android.view.View p11, int p12, int p13, int p14, int p15)
    {
        int v0_7;
        if (!(p11 instanceof android.view.ViewGroup)) {
            if ((!android.support.v4.view.cx.a(p11, (- p12))) && (!android.support.v4.view.cx.b(p11, (- p13)))) {
                v0_7 = 0;
            } else {
                v0_7 = 1;
            }
        } else {
            int v8 = p11.getScrollX();
            int v9 = p11.getScrollY();
            int v7 = (((android.view.ViewGroup) p11).getChildCount() - 1);
            while (v7 >= 0) {
                android.view.View v1 = ((android.view.ViewGroup) p11).getChildAt(v7);
                if (((p14 + v8) < v1.getLeft()) || (((p14 + v8) >= v1.getRight()) || (((p15 + v9) < v1.getTop()) || (((p15 + v9) >= v1.getBottom()) || (!this.b(v1, p12, p13, ((p14 + v8) - v1.getLeft()), ((p15 + v9) - v1.getTop()))))))) {
                    v7--;
                } else {
                    v0_7 = 1;
                }
            }
        }
        return v0_7;
    }

    private void c(float p5)
    {
        this.J = 1;
        this.I.a(this.v, p5);
        this.J = 0;
        if (this.m == 1) {
            this.b(0);
        }
        return;
    }

    private void c(int p1)
    {
        this.u = p1;
        return;
    }

    private void c(int p5, int p6, int p7)
    {
        android.support.v4.widget.ej v0_1 = this.v.getLeft();
        android.view.View v1_1 = this.v.getTop();
        if (p6 != 0) {
            p5 = this.I.a(this.v, p5);
            this.v.offsetLeftAndRight((p5 - v0_1));
        }
        if (p7 != 0) {
            this.v.offsetTopAndBottom((this.I.c(this.v) - v1_1));
        }
        if ((p6 != 0) || (p7 != 0)) {
            this.I.b(this.v, p5);
        }
        return;
    }

    private void c(android.view.MotionEvent p7)
    {
        int v1 = android.support.v4.view.bk.c(p7);
        int v0 = 0;
        while (v0 < v1) {
            int v2 = android.support.v4.view.bk.b(p7, v0);
            float[] v3_0 = android.support.v4.view.bk.c(p7, v0);
            float v4 = android.support.v4.view.bk.d(p7, v0);
            this.q[v2] = v3_0;
            this.r[v2] = v4;
            v0++;
        }
        return;
    }

    private boolean c(int p8, int p9)
    {
        int v2 = 0;
        if (this.a(p9)) {
            float v3_0;
            if ((p8 & 1) != 1) {
                v3_0 = 0;
            } else {
                v3_0 = 1;
            }
            float v0_3;
            if ((p8 & 2) != 2) {
                v0_3 = 0;
            } else {
                v0_3 = 1;
            }
            int v4_3 = (this.q[p9] - this.o[p9]);
            float v5_4 = (this.r[p9] - this.p[p9]);
            if ((v3_0 == 0) || (v0_3 == 0)) {
                if (v3_0 == 0) {
                    if ((v0_3 != 0) && (Math.abs(v5_4) > ((float) this.n))) {
                        v2 = 1;
                    }
                } else {
                    if (Math.abs(v4_3) > ((float) this.n)) {
                        v2 = 1;
                    }
                }
            } else {
                if (((v4_3 * v4_3) + (v5_4 * v5_4)) > ((float) (this.n * this.n))) {
                    v2 = 1;
                }
            }
        }
        return v2;
    }

    private float d()
    {
        return this.s;
    }

    private void d(int p4)
    {
        if (this.o != null) {
            this.o[p4] = 0;
            this.p[p4] = 0;
            this.q[p4] = 0;
            this.r[p4] = 0;
            this.B[p4] = 0;
            this.C[p4] = 0;
            this.D[p4] = 0;
            this.E = (this.E & ((1 << p4) ^ -1));
        }
        return;
    }

    private boolean d(int p2, int p3)
    {
        if ((!this.a(p3)) || ((this.B[p3] & p2) == 0)) {
            int v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    private int e()
    {
        return this.m;
    }

    private void e(int p11)
    {
        if ((this.o == null) || (this.o.length <= p11)) {
            float[] v0_4 = new float[(p11 + 1)];
            float[] v1_1 = new float[(p11 + 1)];
            float[] v2_1 = new float[(p11 + 1)];
            float[] v3_1 = new float[(p11 + 1)];
            int[] v4_1 = new int[(p11 + 1)];
            int[] v5_1 = new int[(p11 + 1)];
            int[] v6_1 = new int[(p11 + 1)];
            if (this.o != null) {
                System.arraycopy(this.o, 0, v0_4, 0, this.o.length);
                System.arraycopy(this.p, 0, v1_1, 0, this.p.length);
                System.arraycopy(this.q, 0, v2_1, 0, this.q.length);
                System.arraycopy(this.r, 0, v3_1, 0, this.r.length);
                System.arraycopy(this.B, 0, v4_1, 0, this.B.length);
                System.arraycopy(this.C, 0, v5_1, 0, this.C.length);
                System.arraycopy(this.D, 0, v6_1, 0, this.D.length);
            }
            this.o = v0_4;
            this.p = v1_1;
            this.q = v2_1;
            this.r = v3_1;
            this.B = v4_1;
            this.C = v5_1;
            this.D = v6_1;
        }
        return;
    }

    private boolean e(int p2, int p3)
    {
        return android.support.v4.widget.eg.b(this.v, p2, p3);
    }

    private int f()
    {
        return this.t;
    }

    private int f(int p4, int p5)
    {
        int v0 = 0;
        if (p4 < (this.K.getLeft() + this.t)) {
            v0 = 1;
        }
        if (p5 < (this.K.getTop() + this.t)) {
            v0 |= 4;
        }
        if (p4 > (this.K.getRight() - this.t)) {
            v0 |= 2;
        }
        if (p5 > (this.K.getBottom() - this.t)) {
            v0 |= 8;
        }
        return v0;
    }

    private boolean f(int p6)
    {
        int v0 = 1;
        int v3 = 0;
        while (v3 < this.B.length) {
            if ((!this.a(v3)) || ((this.B[v3] & p6) == 0)) {
                int v2_5 = 0;
            } else {
                v2_5 = 1;
            }
            if (v2_5 == 0) {
                v3++;
            }
            return v0;
        }
        v0 = 0;
        return v0;
    }

    private android.view.View g()
    {
        return this.v;
    }

    private int h()
    {
        return this.A;
    }

    private int i()
    {
        return this.n;
    }

    private void j()
    {
        if (this.o != null) {
            java.util.Arrays.fill(this.o, 0);
            java.util.Arrays.fill(this.p, 0);
            java.util.Arrays.fill(this.q, 0);
            java.util.Arrays.fill(this.r, 0);
            java.util.Arrays.fill(this.B, 0);
            java.util.Arrays.fill(this.C, 0);
            java.util.Arrays.fill(this.D, 0);
            this.E = 0;
        }
        return;
    }

    private boolean k()
    {
        int v0 = 1;
        int v3 = 0;
        while (v3 < this.o.length) {
            int v2_2;
            if (!this.a(v3)) {
                v2_2 = 0;
            } else {
                if ((((this.q[v3] - this.o[v3]) * (this.q[v3] - this.o[v3])) + ((this.r[v3] - this.p[v3]) * (this.r[v3] - this.p[v3]))) <= ((float) (this.n * this.n))) {
                    v2_2 = 0;
                } else {
                    v2_2 = 1;
                }
            }
            if (v2_2 == 0) {
                v3++;
            }
            return v0;
        }
        v0 = 0;
        return v0;
    }

    private void l()
    {
        this.F.computeCurrentVelocity(1000, this.G);
        float v0_3 = android.support.v4.widget.eg.a(android.support.v4.view.cs.a(this.F, this.A), this.s, this.G);
        android.support.v4.widget.eg.a(android.support.v4.view.cs.b(this.F, this.A), this.s, this.G);
        this.c(v0_3);
        return;
    }

    public final void a()
    {
        this.A = -1;
        if (this.o != null) {
            java.util.Arrays.fill(this.o, 0);
            java.util.Arrays.fill(this.p, 0);
            java.util.Arrays.fill(this.q, 0);
            java.util.Arrays.fill(this.r, 0);
            java.util.Arrays.fill(this.B, 0);
            java.util.Arrays.fill(this.C, 0);
            java.util.Arrays.fill(this.D, 0);
            this.E = 0;
        }
        if (this.F != null) {
            this.F.recycle();
            this.F = 0;
        }
        return;
    }

    public final void a(android.view.View p4, int p5)
    {
        if (p4.getParent() == this.K) {
            this.v = p4;
            this.A = p5;
            this.I.d(p4);
            this.b(1);
            return;
        } else {
            throw new IllegalArgumentException(new StringBuilder("captureChildView: parameter must be a descendant of the ViewDragHelper\'s tracked parent view (").append(this.K).append(")").toString());
        }
    }

    public final boolean a(int p4)
    {
        int v0 = 1;
        if ((this.E & (1 << p4)) == 0) {
            v0 = 0;
        }
        return v0;
    }

    public final boolean a(int p4, int p5)
    {
        if (this.J) {
            return this.a(p4, p5, ((int) android.support.v4.view.cs.a(this.F, this.A)), ((int) android.support.v4.view.cs.b(this.F, this.A)));
        } else {
            throw new IllegalStateException("Cannot settleCapturedViewAt outside of a call to Callback#onViewReleased");
        }
    }

    public final boolean a(android.view.MotionEvent p13)
    {
        int v0 = 1;
        int v2_0 = android.support.v4.view.bk.a(p13);
        int v3_0 = android.support.v4.view.bk.b(p13);
        if (v2_0 == 0) {
            this.a();
        }
        if (this.F == null) {
            this.F = android.view.VelocityTracker.obtain();
        }
        this.F.addMovement(p13);
        switch (v2_0) {
            case 0:
                int v2_14 = p13.getX();
                int v3_7 = p13.getY();
                android.view.View v4_7 = android.support.v4.view.bk.b(p13, 0);
                this.a(v2_14, v3_7, v4_7);
                int v2_16 = this.b(((int) v2_14), ((int) v3_7));
                if ((v2_16 == this.v) && (this.m == 2)) {
                    this.b(v2_16, v4_7);
                }
                if ((this.B[v4_7] & this.u) == 0) {
                } else {
                    this.I.c();
                }
                break;
            case 1:
            case 3:
                this.a();
                break;
            case 2:
                if ((this.o == null) || (this.p == null)) {
                } else {
                    android.view.View v4_6 = android.support.v4.view.bk.c(p13);
                    int v3_6 = 0;
                    while (v3_6 < v4_6) {
                        int v2_11;
                        int v5_2 = android.support.v4.view.bk.b(p13, v3_6);
                        int v2_8 = android.support.v4.view.bk.c(p13, v3_6);
                        android.view.View v6_1 = android.support.v4.view.bk.d(p13, v3_6);
                        int v7_2 = (v2_8 - this.o[v5_2]);
                        float v8_2 = (v6_1 - this.p[v5_2]);
                        android.view.View v6_3 = this.b(((int) v2_8), ((int) v6_1));
                        if ((v6_3 == null) || (!this.a(v6_3, v7_2))) {
                            v2_11 = 0;
                        } else {
                            v2_11 = 1;
                        }
                        if (v2_11 != 0) {
                            int v9 = v6_3.getLeft();
                            int v10_2 = this.I.a(v6_3, (((int) v7_2) + v9));
                            v6_3.getTop();
                            this.I.c(v6_3);
                            int v11_3 = this.I.b(v6_3);
                            if ((v11_3 == 0) || ((v11_3 > 0) && (v10_2 == v9))) {
                                break;
                            }
                        }
                        this.b(v7_2, v8_2, v5_2);
                        if ((this.m == 1) || ((v2_11 != 0) && (this.b(v6_3, v5_2)))) {
                            break;
                        }
                        v3_6++;
                    }
                    this.c(p13);
                }
                break;
            case 4:
            default:
                break;
            case 5:
                int v2_2 = android.support.v4.view.bk.b(p13, v3_0);
                android.view.View v4_3 = android.support.v4.view.bk.c(p13, v3_0);
                int v3_1 = android.support.v4.view.bk.d(p13, v3_0);
                this.a(v4_3, v3_1, v2_2);
                if (this.m != 0) {
                    if (this.m != 2) {
                    } else {
                        int v3_3 = this.b(((int) v4_3), ((int) v3_1));
                        if (v3_3 != this.v) {
                        } else {
                            this.b(v3_3, v2_2);
                        }
                    }
                } else {
                    if ((this.B[v2_2] & this.u) == 0) {
                    } else {
                        this.I.c();
                    }
                }
                break;
            case 6:
                this.d(android.support.v4.view.bk.b(p13, v3_0));
                break;
        }
        if (this.m != 1) {
            v0 = 0;
        }
        return v0;
    }

    public final boolean a(android.view.View p3, int p4, int p5)
    {
        this.v = p3;
        this.A = -1;
        boolean v0_1 = this.a(p4, p5, 0, 0);
        if ((!v0_1) && ((this.m == 0) && (this.v != null))) {
            this.v = 0;
        }
        return v0_1;
    }

    public final android.view.View b(int p4, int p5)
    {
        int v1 = (this.K.getChildCount() - 1);
        while (v1 >= 0) {
            int v0_3 = this.K.getChildAt(v1);
            if ((p4 < v0_3.getLeft()) || ((p4 >= v0_3.getRight()) || ((p5 < v0_3.getTop()) || (p5 >= v0_3.getBottom())))) {
                v1--;
            }
            return v0_3;
        }
        v0_3 = 0;
        return v0_3;
    }

    public final void b()
    {
        this.a();
        if (this.m == 2) {
            this.H.b();
            this.H.c();
            this.H.g();
            int v0_5 = this.H.b();
            this.H.c();
            this.I.b(this.v, v0_5);
        }
        this.b(0);
        return;
    }

    final void b(int p3)
    {
        this.K.removeCallbacks(this.M);
        if (this.m != p3) {
            this.m = p3;
            this.I.a(p3);
            if (this.m == 0) {
                this.v = 0;
            }
        }
        return;
    }

    public final void b(android.view.MotionEvent p9)
    {
        int v0_0 = 0;
        boolean v2_0 = android.support.v4.view.bk.a(p9);
        android.view.View v3_0 = android.support.v4.view.bk.b(p9);
        if (!v2_0) {
            this.a();
        }
        if (this.F == null) {
            this.F = android.view.VelocityTracker.obtain();
        }
        this.F.addMovement(p9);
        switch (v2_0) {
            case 0:
                android.support.v4.widget.ej v1_15 = p9.getX();
                boolean v2_13 = p9.getY();
                int v0_16 = android.support.v4.view.bk.b(p9, 0);
                android.view.View v3_19 = this.b(((int) v1_15), ((int) v2_13));
                this.a(v1_15, v2_13, v0_16);
                this.b(v3_19, v0_16);
                if ((this.B[v0_16] & this.u) == 0) {
                } else {
                    this.I.c();
                }
                break;
            case 1:
                if (this.m == 1) {
                    this.l();
                }
                this.a();
                break;
            case 2:
                if (this.m != 1) {
                    android.support.v4.widget.ej v1_10 = android.support.v4.view.bk.c(p9);
                    while (v0_0 < v1_10) {
                        boolean v2_5 = android.support.v4.view.bk.b(p9, v0_0);
                        android.view.View v3_6 = android.support.v4.view.bk.c(p9, v0_0);
                        boolean v4_5 = android.support.v4.view.bk.d(p9, v0_0);
                        android.view.View v5_7 = (v3_6 - this.o[v2_5]);
                        this.b(v5_7, (v4_5 - this.p[v2_5]), v2_5);
                        if (this.m == 1) {
                            break;
                        }
                        android.view.View v3_8 = this.b(((int) v3_6), ((int) v4_5));
                        if ((this.a(v3_8, v5_7)) && (this.b(v3_8, v2_5))) {
                            break;
                        }
                        v0_0++;
                    }
                    this.c(p9);
                } else {
                    int v0_9 = android.support.v4.view.bk.a(p9, this.A);
                    android.support.v4.widget.ej v1_13 = ((int) (android.support.v4.view.bk.c(p9, v0_9) - this.q[this.A]));
                    boolean v2_11 = ((int) (android.support.v4.view.bk.d(p9, v0_9) - this.r[this.A]));
                    int v0_14 = (this.v.getLeft() + v1_13);
                    this.v.getTop();
                    android.view.View v3_13 = this.v.getLeft();
                    boolean v4_9 = this.v.getTop();
                    if (v1_13 != null) {
                        v0_14 = this.I.a(this.v, v0_14);
                        this.v.offsetLeftAndRight((v0_14 - v3_13));
                    }
                    if (v2_11) {
                        this.v.offsetTopAndBottom((this.I.c(this.v) - v4_9));
                    }
                    if ((v1_13 != null) || (v2_11)) {
                        this.I.b(this.v, v0_14);
                    }
                    this.c(p9);
                }
                break;
            case 3:
                if (this.m == 1) {
                    this.c(0);
                }
                this.a();
            case 4:
            default:
                break;
            case 5:
                int v0_2 = android.support.v4.view.bk.b(p9, v3_0);
                android.support.v4.widget.ej v1_1 = android.support.v4.view.bk.c(p9, v3_0);
                boolean v2_2 = android.support.v4.view.bk.d(p9, v3_0);
                this.a(v1_1, v2_2, v0_2);
                if (this.m != 0) {
                    if (!android.support.v4.widget.eg.b(this.v, ((int) v1_1), ((int) v2_2))) {
                    } else {
                        this.b(this.v, v0_2);
                    }
                } else {
                    this.b(this.b(((int) v1_1), ((int) v2_2)), v0_2);
                    if ((this.B[v0_2] & this.u) == 0) {
                    } else {
                        this.I.c();
                    }
                }
                break;
            case 6:
                boolean v2_1 = android.support.v4.view.bk.b(p9, v3_0);
                if ((this.m == 1) && (v2_1 == this.A)) {
                    android.view.View v3_3 = android.support.v4.view.bk.c(p9);
                    while (v0_0 < v3_3) {
                        boolean v4_3 = android.support.v4.view.bk.b(p9, v0_0);
                        if ((v4_3 == this.A) || ((this.b(((int) android.support.v4.view.bk.c(p9, v0_0)), ((int) android.support.v4.view.bk.d(p9, v0_0))) != this.v) || (!this.b(this.v, v4_3)))) {
                            v0_0++;
                        } else {
                            int v0_1 = this.A;
                        }
                        if (v0_1 == -1) {
                            this.l();
                        }
                    }
                    v0_1 = -1;
                }
                this.d(v2_1);
                break;
        }
        return;
    }

    public final boolean c()
    {
        int v0 = 0;
        if (this.m == 2) {
            int v1_2 = this.H.f();
            Runnable v2_1 = this.H.b();
            int v3_1 = this.H.c();
            int v4_2 = (v2_1 - this.v.getLeft());
            android.support.v4.widget.cb v5_2 = (v3_1 - this.v.getTop());
            if (v4_2 != 0) {
                this.v.offsetLeftAndRight(v4_2);
            }
            if (v5_2 != null) {
                this.v.offsetTopAndBottom(v5_2);
            }
            if ((v4_2 != 0) || (v5_2 != null)) {
                this.I.b(this.v, v2_1);
            }
            if (v1_2 != 0) {
                int v4_4 = this.H;
                if ((v2_1 == v4_4.b.h(v4_4.a)) && (v3_1 == this.H.d())) {
                    this.H.g();
                    v1_2 = 0;
                }
            }
            if (v1_2 == 0) {
                this.K.post(this.M);
            }
        }
        if (this.m == 2) {
            v0 = 1;
        }
        return v0;
    }
}
