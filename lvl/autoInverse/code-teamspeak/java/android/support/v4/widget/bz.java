package android.support.v4.widget;
public abstract class bz extends android.support.v4.widget.r {
    private int l;
    private int m;
    private android.view.LayoutInflater n;

    public bz(android.content.Context p2, int p3)
    {
        this(p2);
        this.m = p3;
        this.l = p3;
        this.n = ((android.view.LayoutInflater) p2.getSystemService("layout_inflater"));
        return;
    }

    public bz(android.content.Context p2, int p3, android.database.Cursor p4)
    {
        this(p2, p4);
        this.m = p3;
        this.l = p3;
        this.n = ((android.view.LayoutInflater) p2.getSystemService("layout_inflater"));
        return;
    }

    public bz(android.content.Context p2, int p3, android.database.Cursor p4, int p5)
    {
        this(p2, p4, p5);
        this.m = p3;
        this.l = p3;
        this.n = ((android.view.LayoutInflater) p2.getSystemService("layout_inflater"));
        return;
    }

    private void a(int p1)
    {
        this.l = p1;
        return;
    }

    private void b(int p1)
    {
        this.m = p1;
        return;
    }

    public android.view.View a(android.content.Context p4, android.database.Cursor p5, android.view.ViewGroup p6)
    {
        return this.n.inflate(this.l, p6, 0);
    }

    public final android.view.View b(android.content.Context p4, android.database.Cursor p5, android.view.ViewGroup p6)
    {
        return this.n.inflate(this.m, p6, 0);
    }
}
