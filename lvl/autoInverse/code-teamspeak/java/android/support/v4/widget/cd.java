package android.support.v4.widget;
 class cd implements android.support.v4.widget.cb {

    cd()
    {
        return;
    }

    public final Object a(android.content.Context p2, android.view.animation.Interpolator p3)
    {
        android.widget.OverScroller v0_1;
        if (p3 == null) {
            v0_1 = new android.widget.OverScroller(p2);
        } else {
            v0_1 = new android.widget.OverScroller(p2, p3);
        }
        return v0_1;
    }

    public final void a(Object p2, int p3, int p4, int p5)
    {
        ((android.widget.OverScroller) p2).startScroll(p3, p4, 0, p5);
        return;
    }

    public final void a(Object p7, int p8, int p9, int p10, int p11, int p12)
    {
        ((android.widget.OverScroller) p7).startScroll(p8, p9, p10, p11, p12);
        return;
    }

    public final void a(Object p10, int p11, int p12, int p13, int p14, int p15, int p16, int p17, int p18)
    {
        ((android.widget.OverScroller) p10).fling(p11, p12, p13, p14, p15, p16, p17, p18);
        return;
    }

    public final boolean a(Object p2)
    {
        return ((android.widget.OverScroller) p2).isFinished();
    }

    public final int b(Object p2)
    {
        return ((android.widget.OverScroller) p2).getCurrX();
    }

    public final void b(Object p1, int p2, int p3, int p4)
    {
        ((android.widget.OverScroller) p1).notifyHorizontalEdgeReached(p2, p3, p4);
        return;
    }

    public final void b(Object p12, int p13, int p14, int p15, int p16, int p17)
    {
        ((android.widget.OverScroller) p12).fling(p13, p14, 0, p15, 0, 0, 0, p16, 0, p17);
        return;
    }

    public final int c(Object p2)
    {
        return ((android.widget.OverScroller) p2).getCurrY();
    }

    public final void c(Object p1, int p2, int p3, int p4)
    {
        ((android.widget.OverScroller) p1).notifyVerticalEdgeReached(p2, p3, p4);
        return;
    }

    public float d(Object p2)
    {
        return 0;
    }

    public final boolean e(Object p2)
    {
        return ((android.widget.OverScroller) p2).computeScrollOffset();
    }

    public final void f(Object p1)
    {
        ((android.widget.OverScroller) p1).abortAnimation();
        return;
    }

    public final boolean g(Object p2)
    {
        return ((android.widget.OverScroller) p2).isOverScrolled();
    }

    public final int h(Object p2)
    {
        return ((android.widget.OverScroller) p2).getFinalX();
    }

    public final int i(Object p2)
    {
        return ((android.widget.OverScroller) p2).getFinalY();
    }
}
