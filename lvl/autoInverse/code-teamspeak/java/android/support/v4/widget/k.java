package android.support.v4.widget;
 class k extends android.support.v4.widget.i {

    k()
    {
        return;
    }

    public final void a(android.widget.CompoundButton p1, android.content.res.ColorStateList p2)
    {
        p1.setButtonTintList(p2);
        return;
    }

    public final void a(android.widget.CompoundButton p1, android.graphics.PorterDuff$Mode p2)
    {
        p1.setButtonTintMode(p2);
        return;
    }

    public final android.content.res.ColorStateList b(android.widget.CompoundButton p2)
    {
        return p2.getButtonTintList();
    }

    public final android.graphics.PorterDuff$Mode c(android.widget.CompoundButton p2)
    {
        return p2.getButtonTintMode();
    }
}
