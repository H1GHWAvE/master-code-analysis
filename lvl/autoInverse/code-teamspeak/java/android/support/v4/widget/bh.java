package android.support.v4.widget;
final class bh extends android.support.v4.view.a {

    bh()
    {
        return;
    }

    public final void a(android.view.View p3, android.support.v4.view.a.q p4)
    {
        super.a(p3, p4);
        p4.b(android.widget.ScrollView.getName());
        if (((android.support.v4.widget.NestedScrollView) p3).isEnabled()) {
            int v0_3 = android.support.v4.widget.NestedScrollView.a(((android.support.v4.widget.NestedScrollView) p3));
            if (v0_3 > 0) {
                p4.i(1);
                if (((android.support.v4.widget.NestedScrollView) p3).getScrollY() > 0) {
                    p4.a(8192);
                }
                if (((android.support.v4.widget.NestedScrollView) p3).getScrollY() < v0_3) {
                    p4.a(4096);
                }
            }
        }
        return;
    }

    public final void a(android.view.View p5, android.view.accessibility.AccessibilityEvent p6)
    {
        int v0_3;
        super.a(p5, p6);
        p6.setClassName(android.widget.ScrollView.getName());
        Object v1_0 = android.support.v4.view.a.a.a(p6);
        if (android.support.v4.widget.NestedScrollView.a(((android.support.v4.widget.NestedScrollView) p5)) <= 0) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        v1_0.a(v0_3);
        android.support.v4.view.a.bd.a.f(v1_0.b, ((android.support.v4.widget.NestedScrollView) p5).getScrollX());
        android.support.v4.view.a.bd.a.g(v1_0.b, ((android.support.v4.widget.NestedScrollView) p5).getScrollY());
        android.support.v4.view.a.bd.a.i(v1_0.b, ((android.support.v4.widget.NestedScrollView) p5).getScrollX());
        android.support.v4.view.a.bd.a.j(v1_0.b, android.support.v4.widget.NestedScrollView.a(((android.support.v4.widget.NestedScrollView) p5)));
        return;
    }

    public final boolean a(android.view.View p5, int p6, android.os.Bundle p7)
    {
        int v0 = 1;
        if (!super.a(p5, p6, p7)) {
            if (((android.support.v4.widget.NestedScrollView) p5).isEnabled()) {
                switch (p6) {
                    case 4096:
                        int v2_11 = Math.min((((((android.support.v4.widget.NestedScrollView) p5).getHeight() - ((android.support.v4.widget.NestedScrollView) p5).getPaddingBottom()) - ((android.support.v4.widget.NestedScrollView) p5).getPaddingTop()) + ((android.support.v4.widget.NestedScrollView) p5).getScrollY()), android.support.v4.widget.NestedScrollView.a(((android.support.v4.widget.NestedScrollView) p5)));
                        if (v2_11 == ((android.support.v4.widget.NestedScrollView) p5).getScrollY()) {
                            v0 = 0;
                        } else {
                            ((android.support.v4.widget.NestedScrollView) p5).a(v2_11);
                        }
                        break;
                    case 8192:
                        int v2_6 = Math.max((((android.support.v4.widget.NestedScrollView) p5).getScrollY() - ((((android.support.v4.widget.NestedScrollView) p5).getHeight() - ((android.support.v4.widget.NestedScrollView) p5).getPaddingBottom()) - ((android.support.v4.widget.NestedScrollView) p5).getPaddingTop())), 0);
                        if (v2_6 == ((android.support.v4.widget.NestedScrollView) p5).getScrollY()) {
                            v0 = 0;
                        } else {
                            ((android.support.v4.widget.NestedScrollView) p5).a(v2_6);
                        }
                        break;
                    default:
                        v0 = 0;
                }
            } else {
                v0 = 0;
            }
        }
        return v0;
    }
}
