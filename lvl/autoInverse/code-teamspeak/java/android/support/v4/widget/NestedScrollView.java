package android.support.v4.widget;
public final class NestedScrollView extends android.widget.FrameLayout implements android.support.v4.view.bt, android.support.v4.view.bv {
    private static final int[] A = None;
    static final int a = 250;
    static final float b = 63;
    private static final String c = "NestedScrollView";
    private static final int x = 255;
    private static final android.support.v4.widget.bh z;
    private final android.support.v4.view.bw B;
    private final android.support.v4.view.bu C;
    private float D;
    private long d;
    private final android.graphics.Rect e;
    private android.support.v4.widget.ca f;
    private android.support.v4.widget.al g;
    private android.support.v4.widget.al h;
    private int i;
    private boolean j;
    private boolean k;
    private android.view.View l;
    private boolean m;
    private android.view.VelocityTracker n;
    private boolean o;
    private boolean p;
    private int q;
    private int r;
    private int s;
    private int t;
    private final int[] u;
    private final int[] v;
    private int w;
    private android.support.v4.widget.NestedScrollView$SavedState y;

    static NestedScrollView()
    {
        android.support.v4.widget.NestedScrollView.z = new android.support.v4.widget.bh();
        int[] v0_3 = new int[1];
        v0_3[0] = 16843130;
        android.support.v4.widget.NestedScrollView.A = v0_3;
        return;
    }

    private NestedScrollView(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    private NestedScrollView(android.content.Context p2, byte p3)
    {
        this(p2, 0);
        return;
    }

    private NestedScrollView(android.content.Context p6, char p7)
    {
        this(p6, 0, 0);
        this.e = new android.graphics.Rect();
        this.j = 1;
        this.k = 0;
        this.l = 0;
        this.m = 0;
        this.p = 1;
        this.t = -1;
        android.support.v4.widget.bh v0_3 = new int[2];
        this.u = v0_3;
        android.support.v4.widget.bh v0_4 = new int[2];
        this.v = v0_4;
        this.f = new android.support.v4.widget.ca(this.getContext(), 0);
        this.setFocusable(1);
        this.setDescendantFocusability(262144);
        this.setWillNotDraw(0);
        android.support.v4.widget.bh v0_9 = android.view.ViewConfiguration.get(this.getContext());
        this.q = v0_9.getScaledTouchSlop();
        this.r = v0_9.getScaledMinimumFlingVelocity();
        this.s = v0_9.getScaledMaximumFlingVelocity();
        android.support.v4.widget.bh v0_12 = p6.obtainStyledAttributes(0, android.support.v4.widget.NestedScrollView.A, 0, 0);
        this.setFillViewport(v0_12.getBoolean(0, 0));
        v0_12.recycle();
        this.B = new android.support.v4.view.bw(this);
        this.C = new android.support.v4.view.bu(this);
        this.setNestedScrollingEnabled(1);
        android.support.v4.view.cx.a(this, android.support.v4.widget.NestedScrollView.z);
        return;
    }

    private int a(android.graphics.Rect p8)
    {
        int v2_0 = 0;
        if (this.getChildCount() != 0) {
            int v3_0 = this.getHeight();
            int v0_1 = this.getScrollY();
            int v1_0 = (v0_1 + v3_0);
            int v4_0 = this.getVerticalFadingEdgeLength();
            if (p8.top > 0) {
                v0_1 += v4_0;
            }
            if (p8.bottom < this.getChildAt(0).getHeight()) {
                v1_0 -= v4_0;
            }
            if ((p8.bottom <= v1_0) || (p8.top <= v0_1)) {
                if ((p8.top >= v0_1) || (p8.bottom >= v1_0)) {
                    int v0_2 = 0;
                } else {
                    int v0_4;
                    if (p8.height() <= v3_0) {
                        v0_4 = (0 - (v0_1 - p8.top));
                    } else {
                        v0_4 = (0 - (v1_0 - p8.bottom));
                    }
                    v0_2 = Math.max(v0_4, (- this.getScrollY()));
                }
            } else {
                int v0_9;
                if (p8.height() <= v3_0) {
                    v0_9 = ((p8.bottom - v1_0) + 0);
                } else {
                    v0_9 = ((p8.top - v0_1) + 0);
                }
                v0_2 = Math.min(v0_9, (this.getChildAt(0).getBottom() - v1_0));
            }
            v2_0 = v0_2;
        }
        return v2_0;
    }

    static synthetic int a(android.support.v4.widget.NestedScrollView p1)
    {
        return p1.getScrollRange();
    }

    private android.view.View a(boolean p12, int p13, int p14)
    {
        java.util.ArrayList v6 = this.getFocusables(2);
        int v3 = 0;
        int v2_0 = 0;
        int v7 = v6.size();
        int v5 = 0;
        while (v5 < v7) {
            int v0_4;
            int v1_1;
            int v0_3 = ((android.view.View) v6.get(v5));
            int v4_0 = v0_3.getTop();
            int v8 = v0_3.getBottom();
            if ((p13 >= v8) || (v4_0 >= p14)) {
                v0_4 = v2_0;
                v1_1 = v3;
            } else {
                if ((p13 >= v4_0) || (v8 >= p14)) {
                    int v1_0 = 0;
                } else {
                    v1_0 = 1;
                }
                if (v3 != 0) {
                    if (((!p12) || (v4_0 >= v3.getTop())) && ((p12) || (v8 <= v3.getBottom()))) {
                        int v4_2 = 0;
                    } else {
                        v4_2 = 1;
                    }
                    if (v2_0 == 0) {
                        if (v1_0 == 0) {
                            if (v4_2 == 0) {
                            } else {
                                v1_1 = v0_3;
                                v0_4 = v2_0;
                            }
                        } else {
                            v1_1 = v0_3;
                            v0_4 = 1;
                        }
                    } else {
                        if ((v1_0 == 0) || (v4_2 == 0)) {
                        } else {
                            v1_1 = v0_3;
                            v0_4 = v2_0;
                        }
                    }
                } else {
                    v1_1 = v0_3;
                    v0_4 = v1_0;
                }
            }
            v5++;
            v3 = v1_1;
            v2_0 = v0_4;
        }
        return v3;
    }

    private void a()
    {
        this.f = new android.support.v4.widget.ca(this.getContext(), 0);
        this.setFocusable(1);
        this.setDescendantFocusability(262144);
        this.setWillNotDraw(0);
        int v0_6 = android.view.ViewConfiguration.get(this.getContext());
        this.q = v0_6.getScaledTouchSlop();
        this.r = v0_6.getScaledMinimumFlingVelocity();
        this.s = v0_6.getScaledMaximumFlingVelocity();
        return;
    }

    private void a(android.view.MotionEvent p4)
    {
        android.view.VelocityTracker v0_2 = ((p4.getAction() & 65280) >> 8);
        if (android.support.v4.view.bk.b(p4, v0_2) == this.t) {
            android.view.VelocityTracker v0_3;
            if (v0_2 != null) {
                v0_3 = 0;
            } else {
                v0_3 = 1;
            }
            this.i = ((int) android.support.v4.view.bk.d(p4, v0_3));
            this.t = android.support.v4.view.bk.b(p4, v0_3);
            if (this.n != null) {
                this.n.clear();
            }
        }
        return;
    }

    private boolean a(int p5, int p6)
    {
        int v0 = 0;
        if (this.getChildCount() > 0) {
            int v1_1 = this.getScrollY();
            android.view.View v2 = this.getChildAt(0);
            if ((p6 >= (v2.getTop() - v1_1)) && ((p6 < (v2.getBottom() - v1_1)) && ((p5 >= v2.getLeft()) && (p5 < v2.getRight())))) {
                v0 = 1;
            }
        }
        return v0;
    }

    private boolean a(int p17, int p18, int p19)
    {
        android.view.View v2_0;
        int v1_0 = this.getHeight();
        int v9 = this.getScrollY();
        int v10 = (v9 + v1_0);
        if (p17 != 33) {
            v2_0 = 0;
        } else {
            v2_0 = 1;
        }
        java.util.ArrayList v11 = this.getFocusables(2);
        int v5 = 0;
        int v4_0 = 0;
        int v12 = v11.size();
        int v8 = 0;
        while (v8 < v12) {
            int v3_1;
            int v1_10;
            int v1_9 = ((android.view.View) v11.get(v8));
            int v7_0 = v1_9.getTop();
            int v13 = v1_9.getBottom();
            if ((p18 >= v13) || (v7_0 >= p19)) {
                v1_10 = v4_0;
                v3_1 = v5;
            } else {
                if ((p18 >= v7_0) || (v13 >= p19)) {
                    int v3_0 = 0;
                } else {
                    v3_0 = 1;
                }
                if (v5 != 0) {
                    if (((v2_0 == null) || (v7_0 >= v5.getTop())) && ((v2_0 != null) || (v13 <= v5.getBottom()))) {
                        int v7_2 = 0;
                    } else {
                        v7_2 = 1;
                    }
                    if (v4_0 == 0) {
                        if (v3_0 == 0) {
                            if (v7_2 == 0) {
                            } else {
                                v3_1 = v1_9;
                                v1_10 = v4_0;
                            }
                        } else {
                            v3_1 = v1_9;
                            v1_10 = 1;
                        }
                    } else {
                        if ((v3_0 == 0) || (v7_2 == 0)) {
                        } else {
                            v3_1 = v1_9;
                            v1_10 = v4_0;
                        }
                    }
                } else {
                    v3_1 = v1_9;
                    v1_10 = v3_0;
                }
            }
            v8++;
            v5 = v3_1;
            v4_0 = v1_10;
        }
        if (v5 == 0) {
            v5 = this;
        }
        if ((p18 < v9) || (p19 > v10)) {
            int v1_6;
            if (v2_0 == null) {
                v1_6 = (p19 - v10);
            } else {
                v1_6 = (p18 - v9);
            }
            this.e(v1_6);
            int v1_7 = 1;
        } else {
            v1_7 = 0;
        }
        if (v5 != this.findFocus()) {
            v5.requestFocus(p17);
        }
        return v1_7;
    }

    private boolean a(int p7, int p8, int p9, int p10, int p11)
    {
        int v5;
        int v4_1;
        int v2 = 0;
        android.support.v4.view.cx.a(this);
        this.computeHorizontalScrollRange();
        this.computeHorizontalScrollExtent();
        this.computeVerticalScrollRange();
        this.computeVerticalScrollExtent();
        int v4_0 = (p9 + p7);
        int v3 = (p10 + p8);
        int v0_0 = (p11 + 0);
        if (v4_0 <= 0) {
            if (v4_0 >= 0) {
                v5 = v4_0;
                v4_1 = 0;
            } else {
                v4_1 = 1;
                v5 = 0;
            }
        } else {
            v4_1 = 1;
            v5 = 0;
        }
        int v0_1;
        if (v3 <= v0_0) {
            if (v3 >= 0) {
                v0_1 = 0;
            } else {
                v0_1 = 1;
                v3 = 0;
            }
        } else {
            v3 = v0_0;
            v0_1 = 1;
        }
        this.onOverScrolled(v5, v3, v4_1, v0_1);
        if ((v4_1 != 0) || (v0_1 != 0)) {
            v2 = 1;
        }
        return v2;
    }

    private boolean a(android.graphics.Rect p4, boolean p5)
    {
        int v0;
        int v2 = this.a(p4);
        if (v2 == 0) {
            v0 = 0;
        } else {
            v0 = 1;
        }
        if (v0 != 0) {
            if (!p5) {
                this.b(0, v2);
            } else {
                this.scrollBy(0, v2);
            }
        }
        return v0;
    }

    private boolean a(android.view.KeyEvent p8)
    {
        int v3_2;
        boolean v2 = 0;
        this.e.setEmpty();
        int v3_1 = this.getChildAt(0);
        if (v3_1 == 0) {
            v3_2 = 0;
        } else {
            if (this.getHeight() >= ((v3_1.getHeight() + this.getPaddingTop()) + this.getPaddingBottom())) {
                v3_2 = 0;
            } else {
                v3_2 = 1;
            }
        }
        if (v3_2 != 0) {
            if (p8.getAction() == 0) {
                switch (p8.getKeyCode()) {
                    case 19:
                        if (p8.isAltPressed()) {
                            v2 = this.c(33);
                        } else {
                            v2 = this.d(33);
                        }
                        break;
                    case 20:
                        if (p8.isAltPressed()) {
                            v2 = this.c(130);
                        } else {
                            v2 = this.d(130);
                        }
                        break;
                    case 62:
                        int v3_9;
                        if (!p8.isShiftPressed()) {
                            v3_9 = 130;
                        } else {
                            v3_9 = 33;
                        }
                        int v0_1;
                        if (v3_9 != 130) {
                            v0_1 = 0;
                        } else {
                            v0_1 = 1;
                        }
                        int v1_1 = this.getHeight();
                        if (v0_1 == 0) {
                            this.e.top = (this.getScrollY() - v1_1);
                            if (this.e.top < 0) {
                                this.e.top = 0;
                            }
                        } else {
                            this.e.top = (this.getScrollY() + v1_1);
                            int v0_7 = this.getChildCount();
                            if (v0_7 > 0) {
                                int v0_9 = this.getChildAt((v0_7 - 1));
                                if ((this.e.top + v1_1) > v0_9.getBottom()) {
                                    this.e.top = (v0_9.getBottom() - v1_1);
                                }
                            }
                        }
                        this.e.bottom = (v1_1 + this.e.top);
                        this.a(v3_9, this.e.top, this.e.bottom);
                        break;
                    default:
                }
            }
        } else {
            if ((this.isFocused()) && (p8.getKeyCode() != 4)) {
                int v0_18 = this.findFocus();
                if (v0_18 == this) {
                    v0_18 = 0;
                }
                int v0_19 = android.view.FocusFinder.getInstance().findNextFocus(this, v0_18, 130);
                if ((v0_19 != 0) && ((v0_19 != this) && (v0_19.requestFocus(130)))) {
                    v2 = 1;
                }
            }
        }
        return v2;
    }

    private boolean a(android.view.View p3)
    {
        int v0 = 0;
        if (!this.a(p3, 0, this.getHeight())) {
            v0 = 1;
        }
        return v0;
    }

    private boolean a(android.view.View p3, int p4, int p5)
    {
        int v0_8;
        p3.getDrawingRect(this.e);
        this.offsetDescendantRectToMyCoords(p3, this.e);
        if (((this.e.bottom + p4) < this.getScrollY()) || ((this.e.top - p4) > (this.getScrollY() + p5))) {
            v0_8 = 0;
        } else {
            v0_8 = 1;
        }
        return v0_8;
    }

    private static boolean a(android.view.View p3, android.view.View p4)
    {
        int v0_3;
        if (p3 != p4) {
            int v0_0 = p3.getParent();
            if ((!(v0_0 instanceof android.view.ViewGroup)) || (!android.support.v4.widget.NestedScrollView.a(((android.view.View) v0_0), p4))) {
                v0_3 = 0;
            } else {
                v0_3 = 1;
            }
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    private static int b(int p1, int p2, int p3)
    {
        if ((p2 < p3) && (p1 >= 0)) {
            if ((p2 + p1) > p3) {
                p1 = (p3 - p2);
            }
        } else {
            p1 = 0;
        }
        return p1;
    }

    private void b(int p6, int p7)
    {
        if (this.getChildCount() != 0) {
            if ((android.view.animation.AnimationUtils.currentAnimationTimeMillis() - this.d) <= 250) {
                if (!this.f.a()) {
                    this.f.g();
                }
                this.scrollBy(p6, p7);
            } else {
                android.support.v4.widget.ca v0_11 = Math.max(0, (this.getChildAt(0).getHeight() - ((this.getHeight() - this.getPaddingBottom()) - this.getPaddingTop())));
                int v1_4 = this.getScrollY();
                Object v2_3 = this.f;
                v2_3.b.a(v2_3.a, this.getScrollX(), v1_4, (Math.max(0, Math.min((v1_4 + p7), v0_11)) - v1_4));
                android.support.v4.view.cx.b(this);
            }
            this.d = android.view.animation.AnimationUtils.currentAnimationTimeMillis();
        }
        return;
    }

    private void b(android.view.View p3)
    {
        p3.getDrawingRect(this.e);
        this.offsetDescendantRectToMyCoords(p3, this.e);
        int v0_3 = this.a(this.e);
        if (v0_3 != 0) {
            this.scrollBy(0, v0_3);
        }
        return;
    }

    private boolean b()
    {
        int v0 = 0;
        int v1_0 = this.getChildAt(0);
        if ((v1_0 != 0) && (this.getHeight() < ((v1_0.getHeight() + this.getPaddingTop()) + this.getPaddingBottom()))) {
            v0 = 1;
        }
        return v0;
    }

    private boolean b(int p5)
    {
        int v0_1;
        if (p5 != 130) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        int v2 = this.getHeight();
        if (v0_1 == 0) {
            this.e.top = (this.getScrollY() - v2);
            if (this.e.top < 0) {
                this.e.top = 0;
            }
        } else {
            this.e.top = (this.getScrollY() + v2);
            int v0_7 = this.getChildCount();
            if (v0_7 > 0) {
                int v0_9 = this.getChildAt((v0_7 - 1));
                if ((this.e.top + v2) > v0_9.getBottom()) {
                    this.e.top = (v0_9.getBottom() - v2);
                }
            }
        }
        this.e.bottom = (this.e.top + v2);
        return this.a(p5, this.e.top, this.e.bottom);
    }

    private boolean c()
    {
        return this.o;
    }

    private boolean c(int p5)
    {
        android.graphics.Rect v0_1;
        if (p5 != 130) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        int v2 = this.getHeight();
        this.e.top = 0;
        this.e.bottom = v2;
        if (v0_1 != null) {
            android.graphics.Rect v0_2 = this.getChildCount();
            if (v0_2 > null) {
                this.e.bottom = (this.getChildAt((v0_2 - 1)).getBottom() + this.getPaddingBottom());
                this.e.top = (this.e.bottom - v2);
            }
        }
        return this.a(p5, this.e.top, this.e.bottom);
    }

    private boolean d()
    {
        return this.p;
    }

    private boolean d(int p8)
    {
        int v0_0 = this.findFocus();
        if (v0_0 == this) {
            v0_0 = 0;
        }
        int v0_1;
        int v3_0 = android.view.FocusFinder.getInstance().findNextFocus(this, v0_0, p8);
        int v1_1 = this.getMaxScrollAmount();
        if ((v3_0 == 0) || (!this.a(v3_0, v1_1, this.getHeight()))) {
            if ((p8 != 33) || (this.getScrollY() >= v1_1)) {
                if ((p8 == 130) && (this.getChildCount() > 0)) {
                    int v3_5 = this.getChildAt(0).getBottom();
                    int v4_4 = ((this.getScrollY() + this.getHeight()) - this.getPaddingBottom());
                    if ((v3_5 - v4_4) < v1_1) {
                        v1_1 = (v3_5 - v4_4);
                    }
                }
            } else {
                v1_1 = this.getScrollY();
            }
            if (v1_1 != 0) {
                if (p8 != 130) {
                    v1_1 = (- v1_1);
                }
                this.e(v1_1);
                if ((v0_0 != 0) && ((v0_0.isFocused()) && (this.a(v0_0)))) {
                    int v0_3 = this.getDescendantFocusability();
                    this.setDescendantFocusability(131072);
                    this.requestFocus();
                    this.setDescendantFocusability(v0_3);
                }
                v0_1 = 1;
            } else {
                v0_1 = 0;
            }
        } else {
            v3_0.getDrawingRect(this.e);
            this.offsetDescendantRectToMyCoords(v3_0, this.e);
            this.e(this.a(this.e));
            v3_0.requestFocus(p8);
        }
        return v0_1;
    }

    private void e()
    {
        if (this.n != null) {
            this.n.clear();
        } else {
            this.n = android.view.VelocityTracker.obtain();
        }
        return;
    }

    private void e(int p3)
    {
        if (p3 != 0) {
            if (!this.p) {
                this.scrollBy(0, p3);
            } else {
                this.b(0, p3);
            }
        }
        return;
    }

    private void f()
    {
        if (this.n == null) {
            this.n = android.view.VelocityTracker.obtain();
        }
        return;
    }

    private void f(int p8)
    {
        if (this.getChildCount() > 0) {
            android.support.v4.widget.cb v0_3 = ((this.getHeight() - this.getPaddingBottom()) - this.getPaddingTop());
            int v4_0 = this.f;
            v4_0.b.b(v4_0.a, this.getScrollX(), this.getScrollY(), p8, Math.max(0, (this.getChildAt(0).getHeight() - v0_3)), (v0_3 / 2));
            android.support.v4.view.cx.b(this);
        }
        return;
    }

    private void g()
    {
        if (this.n != null) {
            this.n.recycle();
            this.n = 0;
        }
        return;
    }

    private void g(int p9)
    {
        android.support.v4.widget.cb v0_1;
        android.support.v4.widget.cb v0_0 = this.getScrollY();
        if (((v0_0 <= null) && (p9 <= 0)) || ((v0_0 >= this.getScrollRange()) && (p9 >= 0))) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        if (!this.dispatchNestedPreFling(0, ((float) p9))) {
            this.dispatchNestedFling(0, ((float) p9), v0_1);
            if ((v0_1 != null) && (this.getChildCount() > 0)) {
                android.support.v4.widget.cb v0_5 = ((this.getHeight() - this.getPaddingBottom()) - this.getPaddingTop());
                android.support.v4.widget.ca v7 = this.f;
                v7.b.b(v7.a, this.getScrollX(), this.getScrollY(), p9, Math.max(0, (this.getChildAt(0).getHeight() - v0_5)), (v0_5 / 2));
                android.support.v4.view.cx.b(this);
            }
        }
        return;
    }

    private int getScrollRange()
    {
        int v0 = 0;
        if (this.getChildCount() > 0) {
            v0 = Math.max(0, (this.getChildAt(0).getHeight() - ((this.getHeight() - this.getPaddingBottom()) - this.getPaddingTop())));
        }
        return v0;
    }

    private float getVerticalScrollFactorCompat()
    {
        if (this.D == 0) {
            float v0_3 = new android.util.TypedValue();
            android.util.DisplayMetrics v1_1 = this.getContext();
            if (v1_1.getTheme().resolveAttribute(16842829, v0_3, 1)) {
                this.D = v0_3.getDimension(v1_1.getResources().getDisplayMetrics());
            } else {
                throw new IllegalStateException("Expected theme to define listPreferredItemHeight.");
            }
        }
        return this.D;
    }

    private void h()
    {
        this.m = 0;
        this.g();
        this.stopNestedScroll();
        if (this.g != null) {
            this.g.c();
            this.h.c();
        }
        return;
    }

    private void i()
    {
        if (android.support.v4.view.cx.a(this) == 2) {
            this.g = 0;
            this.h = 0;
        } else {
            if (this.g == null) {
                android.content.Context v0_2 = this.getContext();
                this.g = new android.support.v4.widget.al(v0_2);
                this.h = new android.support.v4.widget.al(v0_2);
            }
        }
        return;
    }

    public final void a(int p3)
    {
        this.b((0 - this.getScrollX()), (p3 - this.getScrollY()));
        return;
    }

    public final void addView(android.view.View p3)
    {
        if (this.getChildCount() <= 0) {
            super.addView(p3);
            return;
        } else {
            throw new IllegalStateException("ScrollView can host only one direct child");
        }
    }

    public final void addView(android.view.View p3, int p4)
    {
        if (this.getChildCount() <= 0) {
            super.addView(p3, p4);
            return;
        } else {
            throw new IllegalStateException("ScrollView can host only one direct child");
        }
    }

    public final void addView(android.view.View p3, int p4, android.view.ViewGroup$LayoutParams p5)
    {
        if (this.getChildCount() <= 0) {
            super.addView(p3, p4, p5);
            return;
        } else {
            throw new IllegalStateException("ScrollView can host only one direct child");
        }
    }

    public final void addView(android.view.View p3, android.view.ViewGroup$LayoutParams p4)
    {
        if (this.getChildCount() <= 0) {
            super.addView(p3, p4);
            return;
        } else {
            throw new IllegalStateException("ScrollView can host only one direct child");
        }
    }

    public final void computeScroll()
    {
        if (this.f.f()) {
            int v3 = this.getScrollX();
            int v4 = this.getScrollY();
            int v1_3 = this.f.b();
            int v7 = this.f.c();
            if ((v3 != v1_3) || (v4 != v7)) {
                int v6;
                int v5 = this.getScrollRange();
                int v2_1 = android.support.v4.view.cx.a(this);
                if ((v2_1 != 0) && ((v2_1 != 1) || (v5 <= 0))) {
                    v6 = 0;
                } else {
                    v6 = 1;
                }
                this.a((v1_3 - v3), (v7 - v4), v3, v4, v5);
                if (v6 != 0) {
                    this.i();
                    if ((v7 > 0) || (v4 <= 0)) {
                        if ((v7 >= v5) && (v4 < v5)) {
                            this.h.a(((int) this.f.e()));
                        }
                    } else {
                        this.g.a(((int) this.f.e()));
                    }
                }
            }
        }
        return;
    }

    protected final int computeVerticalScrollOffset()
    {
        return Math.max(0, super.computeVerticalScrollOffset());
    }

    protected final int computeVerticalScrollRange()
    {
        int v0_2;
        int v0_0 = this.getChildCount();
        int v1_2 = ((this.getHeight() - this.getPaddingBottom()) - this.getPaddingTop());
        if (v0_0 != 0) {
            v0_2 = this.getChildAt(0).getBottom();
            int v2_2 = this.getScrollY();
            int v1_4 = Math.max(0, (v0_2 - v1_2));
            if (v2_2 >= 0) {
                if (v2_2 > v1_4) {
                    v0_2 += (v2_2 - v1_4);
                }
            } else {
                v0_2 -= v2_2;
            }
        } else {
            v0_2 = v1_2;
        }
        return v0_2;
    }

    public final boolean dispatchKeyEvent(android.view.KeyEvent p8)
    {
        int v2 = 0;
        if (super.dispatchKeyEvent(p8)) {
            v2 = 1;
        } else {
            int v3_3;
            this.e.setEmpty();
            int v3_2 = this.getChildAt(0);
            if (v3_2 == 0) {
                v3_3 = 0;
            } else {
                if (this.getHeight() >= ((v3_2.getHeight() + this.getPaddingTop()) + this.getPaddingBottom())) {
                    v3_3 = 0;
                } else {
                    v3_3 = 1;
                }
            }
            int v0_16;
            if (v3_3 != 0) {
                if (p8.getAction() == 0) {
                    switch (p8.getKeyCode()) {
                        case 19:
                            if (p8.isAltPressed()) {
                                v0_16 = this.c(33);
                                if (v0_16 != 0) {
                                }
                                return v2;
                            } else {
                                v0_16 = this.d(33);
                            }
                        case 20:
                            if (p8.isAltPressed()) {
                                v0_16 = this.c(130);
                            } else {
                                v0_16 = this.d(130);
                            }
                            break;
                        case 62:
                            int v3_10;
                            if (!p8.isShiftPressed()) {
                                v3_10 = 130;
                            } else {
                                v3_10 = 33;
                            }
                            int v0_1;
                            if (v3_10 != 130) {
                                v0_1 = 0;
                            } else {
                                v0_1 = 1;
                            }
                            int v4_1 = this.getHeight();
                            if (v0_1 == 0) {
                                this.e.top = (this.getScrollY() - v4_1);
                                if (this.e.top < 0) {
                                    this.e.top = 0;
                                }
                            } else {
                                this.e.top = (this.getScrollY() + v4_1);
                                int v0_7 = this.getChildCount();
                                if (v0_7 > 0) {
                                    int v0_9 = this.getChildAt((v0_7 - 1));
                                    if ((this.e.top + v4_1) > v0_9.getBottom()) {
                                        this.e.top = (v0_9.getBottom() - v4_1);
                                    }
                                }
                            }
                            this.e.bottom = (v4_1 + this.e.top);
                            this.a(v3_10, this.e.top, this.e.bottom);
                            break;
                    }
                }
                v0_16 = 0;
            } else {
                if ((!this.isFocused()) || (p8.getKeyCode() == 4)) {
                    v0_16 = 0;
                } else {
                    int v0_19 = this.findFocus();
                    if (v0_19 == this) {
                        v0_19 = 0;
                    }
                    int v0_20 = android.view.FocusFinder.getInstance().findNextFocus(this, v0_19, 130);
                    if ((v0_20 == 0) || ((v0_20 == this) || (!v0_20.requestFocus(130)))) {
                        v0_16 = 0;
                    } else {
                        v0_16 = 1;
                    }
                }
            }
        }
        return v2;
    }

    public final boolean dispatchNestedFling(float p2, float p3, boolean p4)
    {
        return this.C.a(p2, p3, p4);
    }

    public final boolean dispatchNestedPreFling(float p2, float p3)
    {
        return this.C.a(p2, p3);
    }

    public final boolean dispatchNestedPreScroll(int p2, int p3, int[] p4, int[] p5)
    {
        return this.C.a(p2, p3, p4, p5);
    }

    public final boolean dispatchNestedScroll(int p7, int p8, int p9, int p10, int[] p11)
    {
        return this.C.a(p7, p8, p9, p10, p11);
    }

    public final void draw(android.graphics.Canvas p7)
    {
        super.draw(p7);
        if (this.g != null) {
            boolean v0_1 = this.getScrollY();
            if (!this.g.a()) {
                int v1_2 = p7.save();
                int v2_2 = ((this.getWidth() - this.getPaddingLeft()) - this.getPaddingRight());
                p7.translate(((float) this.getPaddingLeft()), ((float) Math.min(0, v0_1)));
                this.g.a(v2_2, this.getHeight());
                if (this.g.a(p7)) {
                    android.support.v4.view.cx.b(this);
                }
                p7.restoreToCount(v1_2);
            }
            if (!this.h.a()) {
                int v1_5 = p7.save();
                int v2_7 = ((this.getWidth() - this.getPaddingLeft()) - this.getPaddingRight());
                int v3_7 = this.getHeight();
                p7.translate(((float) ((- v2_7) + this.getPaddingLeft())), ((float) (Math.max(this.getScrollRange(), v0_1) + v3_7)));
                p7.rotate(1127481344, ((float) v2_7), 0);
                this.h.a(v2_7, v3_7);
                if (this.h.a(p7)) {
                    android.support.v4.view.cx.b(this);
                }
                p7.restoreToCount(v1_5);
            }
        }
        return;
    }

    protected final float getBottomFadingEdgeStrength()
    {
        int v0_2;
        if (this.getChildCount() != 0) {
            int v0_1 = this.getVerticalFadingEdgeLength();
            float v1_2 = ((this.getChildAt(0).getBottom() - this.getScrollY()) - (this.getHeight() - this.getPaddingBottom()));
            if (v1_2 >= v0_1) {
                v0_2 = 1065353216;
            } else {
                v0_2 = (((float) v1_2) / ((float) v0_1));
            }
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public final int getMaxScrollAmount()
    {
        return ((int) (1056964608 * ((float) this.getHeight())));
    }

    public final int getNestedScrollAxes()
    {
        return this.B.a;
    }

    protected final float getTopFadingEdgeStrength()
    {
        int v0_2;
        if (this.getChildCount() != 0) {
            int v0_1 = this.getVerticalFadingEdgeLength();
            float v1_0 = this.getScrollY();
            if (v1_0 >= v0_1) {
                v0_2 = 1065353216;
            } else {
                v0_2 = (((float) v1_0) / ((float) v0_1));
            }
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public final boolean hasNestedScrollingParent()
    {
        return this.C.a();
    }

    public final boolean isNestedScrollingEnabled()
    {
        return this.C.a;
    }

    protected final void measureChild(android.view.View p5, int p6, int p7)
    {
        p5.measure(android.support.v4.widget.NestedScrollView.getChildMeasureSpec(p6, (this.getPaddingLeft() + this.getPaddingRight()), p5.getLayoutParams().width), android.view.View$MeasureSpec.makeMeasureSpec(0, 0));
        return;
    }

    protected final void measureChildWithMargins(android.view.View p4, int p5, int p6, int p7, int p8)
    {
        int v0_1 = ((android.view.ViewGroup$MarginLayoutParams) p4.getLayoutParams());
        p4.measure(android.support.v4.widget.NestedScrollView.getChildMeasureSpec(p5, ((((this.getPaddingLeft() + this.getPaddingRight()) + v0_1.leftMargin) + v0_1.rightMargin) + p6), v0_1.width), android.view.View$MeasureSpec.makeMeasureSpec((v0_1.bottomMargin + v0_1.topMargin), 0));
        return;
    }

    public final void onAttachedToWindow()
    {
        this.k = 0;
        return;
    }

    public final boolean onGenericMotionEvent(android.view.MotionEvent p5)
    {
        int v0_0 = 0;
        if ((android.support.v4.view.bk.d(p5) & 2) != 0) {
            switch (p5.getAction()) {
                case 8:
                    if (!this.m) {
                        int v1_4 = android.support.v4.view.bk.e(p5);
                        if (v1_4 != 0) {
                            int v2_3 = ((int) (v1_4 * this.getVerticalScrollFactorCompat()));
                            int v1_6 = this.getScrollRange();
                            int v3 = this.getScrollY();
                            int v2_4 = (v3 - v2_3);
                            if (v2_4 >= 0) {
                                if (v2_4 <= v1_6) {
                                    v1_6 = v2_4;
                                }
                            } else {
                                v1_6 = 0;
                            }
                            if (v1_6 != v3) {
                                super.scrollTo(this.getScrollX(), v1_6);
                                v0_0 = 1;
                            }
                        }
                    }
                    break;
            }
        }
        return v0_0;
    }

    public final boolean onInterceptTouchEvent(android.view.MotionEvent p9)
    {
        android.view.ViewParent v0_0 = 1;
        boolean v2_0 = p9.getAction();
        if ((v2_0 != 2) || (!this.m)) {
            if ((this.getScrollY() != 0) || (android.support.v4.view.cx.b(this, 1))) {
                switch ((v2_0 & 255)) {
                    case 0:
                        boolean v2_10;
                        int v3_10 = ((int) p9.getY());
                        boolean v2_9 = ((int) p9.getX());
                        if (this.getChildCount() <= 0) {
                            v2_10 = 0;
                        } else {
                            int v4_3 = this.getScrollY();
                            android.view.View v5 = this.getChildAt(0);
                            if ((v3_10 < (v5.getTop() - v4_3)) || ((v3_10 >= (v5.getBottom() - v4_3)) || ((v2_9 < v5.getLeft()) || (v2_9 >= v5.getRight())))) {
                                v2_10 = 0;
                            } else {
                                v2_10 = 1;
                            }
                        }
                        if (v2_10) {
                            this.i = v3_10;
                            this.t = android.support.v4.view.bk.b(p9, 0);
                            if (this.n != null) {
                                this.n.clear();
                            } else {
                                this.n = android.view.VelocityTracker.obtain();
                            }
                            this.n.addMovement(p9);
                            if (this.f.a()) {
                                v0_0 = 0;
                            }
                            this.m = v0_0;
                            this.startNestedScroll(2);
                        } else {
                            this.m = 0;
                            this.g();
                        }
                        break;
                    case 1:
                    case 3:
                        this.m = 0;
                        this.t = -1;
                        this.g();
                        this.stopNestedScroll();
                        break;
                    case 2:
                        boolean v2_2 = this.t;
                        if (v2_2 == -1) {
                        } else {
                            int v3_3 = android.support.v4.view.bk.a(p9, v2_2);
                            if (v3_3 != -1) {
                                boolean v2_4 = ((int) android.support.v4.view.bk.d(p9, v3_3));
                                if ((Math.abs((v2_4 - this.i)) <= this.q) || ((this.getNestedScrollAxes() & 2) != 0)) {
                                } else {
                                    this.m = 1;
                                    this.i = v2_4;
                                    this.f();
                                    this.n.addMovement(p9);
                                    this.w = 0;
                                    android.view.ViewParent v1_1 = this.getParent();
                                    if (v1_1 == null) {
                                    } else {
                                        v1_1.requestDisallowInterceptTouchEvent(1);
                                    }
                                }
                            } else {
                                android.util.Log.e("NestedScrollView", new StringBuilder("Invalid pointerId=").append(v2_2).append(" in onInterceptTouchEvent").toString());
                            }
                        }
                        break;
                    case 4:
                    case 5:
                    default:
                        break;
                    case 4:
                    case 5:
                        break;
                    case 6:
                        this.a(p9);
                        break;
                }
                v0_0 = this.m;
            } else {
                v0_0 = 0;
            }
        }
        return v0_0;
    }

    protected final void onLayout(boolean p5, int p6, int p7, int p8, int p9)
    {
        this = super.onLayout(p5, p6, p7, p8, p9);
        this.j = 0;
        if ((this.l != null) && (android.support.v4.widget.NestedScrollView.a(this.l, this))) {
            this.b(this.l);
        }
        this.l = 0;
        if (!this.k) {
            if (this.y != null) {
                this.scrollTo(this.getScrollX(), this.y.a);
                this.y = 0;
            }
            int v0_8;
            if (this.getChildCount() <= 0) {
                v0_8 = 0;
            } else {
                v0_8 = this.getChildAt(0).getMeasuredHeight();
            }
            int v0_11 = Math.max(0, (v0_8 - (((p9 - p7) - this.getPaddingBottom()) - this.getPaddingTop())));
            if (this.getScrollY() <= v0_11) {
                if (this.getScrollY() < 0) {
                    this.scrollTo(this.getScrollX(), 0);
                }
            } else {
                this.scrollTo(this.getScrollX(), v0_11);
            }
        }
        this.scrollTo(this.getScrollX(), this.getScrollY());
        this.k = 1;
        return;
    }

    protected final void onMeasure(int p6, int p7)
    {
        super.onMeasure(p6, p7);
        if ((this.o) && ((android.view.View$MeasureSpec.getMode(p7) != 0) && (this.getChildCount() > 0))) {
            android.view.View v1 = this.getChildAt(0);
            int v2_0 = this.getMeasuredHeight();
            if (v1.getMeasuredHeight() < v2_0) {
                v1.measure(android.support.v4.widget.NestedScrollView.getChildMeasureSpec(p6, (this.getPaddingLeft() + this.getPaddingRight()), ((android.widget.FrameLayout$LayoutParams) v1.getLayoutParams()).width), android.view.View$MeasureSpec.makeMeasureSpec(((v2_0 - this.getPaddingTop()) - this.getPaddingBottom()), 1073741824));
            }
        }
        return;
    }

    public final boolean onNestedFling(android.view.View p2, float p3, float p4, boolean p5)
    {
        int v0_0;
        if (p5) {
            v0_0 = 0;
        } else {
            this.g(((int) p4));
            v0_0 = 1;
        }
        return v0_0;
    }

    public final boolean onNestedPreFling(android.view.View p2, float p3, float p4)
    {
        return 0;
    }

    public final void onNestedPreScroll(android.view.View p1, int p2, int p3, int[] p4)
    {
        return;
    }

    public final void onNestedScroll(android.view.View p7, int p8, int p9, int p10, int p11)
    {
        android.support.v4.widget.NestedScrollView v0_0 = this.getScrollY();
        this.scrollBy(0, p11);
        int v2_1 = (this.getScrollY() - v0_0);
        this.dispatchNestedScroll(0, v2_1, 0, (p11 - v2_1), 0);
        return;
    }

    public final void onNestedScrollAccepted(android.view.View p2, android.view.View p3, int p4)
    {
        this.B.a = p4;
        this.startNestedScroll(2);
        return;
    }

    protected final void onOverScrolled(int p1, int p2, boolean p3, boolean p4)
    {
        super.scrollTo(p1, p2);
        return;
    }

    protected final boolean onRequestFocusInDescendants(int p4, android.graphics.Rect p5)
    {
        boolean v0 = 0;
        if (p4 != 2) {
            if (p4 == 1) {
                p4 = 33;
            }
        } else {
            p4 = 130;
        }
        android.view.View v1_3;
        if (p5 != null) {
            v1_3 = android.view.FocusFinder.getInstance().findNextFocusFromRect(this, p5, p4);
        } else {
            v1_3 = android.view.FocusFinder.getInstance().findNextFocus(this, 0, p4);
        }
        if ((v1_3 != null) && (!this.a(v1_3))) {
            v0 = v1_3.requestFocus(p4, p5);
        }
        return v0;
    }

    protected final void onRestoreInstanceState(android.os.Parcelable p2)
    {
        super.onRestoreInstanceState(((android.support.v4.widget.NestedScrollView$SavedState) p2).getSuperState());
        this.y = ((android.support.v4.widget.NestedScrollView$SavedState) p2);
        this.requestLayout();
        return;
    }

    protected final android.os.Parcelable onSaveInstanceState()
    {
        android.support.v4.widget.NestedScrollView$SavedState v1_1 = new android.support.v4.widget.NestedScrollView$SavedState(super.onSaveInstanceState());
        v1_1.a = this.getScrollY();
        return v1_1;
    }

    protected final void onSizeChanged(int p3, int p4, int p5, int p6)
    {
        super.onSizeChanged(p3, p4, p5, p6);
        int v0_0 = this.findFocus();
        if ((v0_0 != 0) && ((this != v0_0) && (this.a(v0_0, 0, p6)))) {
            v0_0.getDrawingRect(this.e);
            this.offsetDescendantRectToMyCoords(v0_0, this.e);
            this.e(this.a(this.e));
        }
        return;
    }

    public final boolean onStartNestedScroll(android.view.View p2, android.view.View p3, int p4)
    {
        int v0_1;
        if ((p4 & 2) == 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final void onStopNestedScroll(android.view.View p1)
    {
        this.stopNestedScroll();
        return;
    }

    public final boolean onTouchEvent(android.view.MotionEvent p18)
    {
        void v17_1 = this.f();
        android.view.MotionEvent v14 = android.view.MotionEvent.obtain(p18);
        boolean v1_0 = android.support.v4.view.bk.a(p18);
        if (!v1_0) {
            v17_1.w = 0;
        }
        boolean v1_69;
        v14.offsetLocation(0, ((float) v17_1.w));
        switch (v1_0) {
            case 0:
                if (v17_1.getChildCount() != 0) {
                    boolean v1_59;
                    if (v17_1.f.a()) {
                        v1_59 = 0;
                    } else {
                        v1_59 = 1;
                    }
                    v17_1.m = v1_59;
                    if (v1_59) {
                        boolean v1_60 = v17_1.getParent();
                        if (v1_60) {
                            v1_60.requestDisallowInterceptTouchEvent(1);
                        }
                    }
                    if (!v17_1.f.a()) {
                        v17_1.f.g();
                    }
                    v17_1.i = ((int) p18.getY());
                    v17_1.t = android.support.v4.view.bk.b(p18, 0);
                    v17_1.startNestedScroll(2);
                    if (v17_1.n != null) {
                        v17_1.n.addMovement(v14);
                    }
                    v14.recycle();
                    v1_69 = 1;
                } else {
                    v1_69 = 0;
                }
                break;
            case 1:
                if (!v17_1.m) {
                } else {
                    boolean v1_51 = v17_1.n;
                    v1_51.computeCurrentVelocity(1000, ((float) v17_1.s));
                    boolean v1_53 = ((int) android.support.v4.view.cs.b(v1_51, v17_1.t));
                    if (Math.abs(v1_53) > v17_1.r) {
                        v17_1.g((- v1_53));
                    }
                    v17_1.t = -1;
                    v17_1 = v17_1.h();
                }
                break;
            case 2:
                int v15 = android.support.v4.view.bk.a(p18, v17_1.t);
                if (v15 != -1) {
                    float v2_4 = ((int) android.support.v4.view.bk.d(p18, v15));
                    boolean v1_14 = (v17_1.i - v2_4);
                    if (v17_1.dispatchNestedPreScroll(0, v1_14, v17_1.v, v17_1.u)) {
                        v1_14 -= v17_1.v[1];
                        v14.offsetLocation(0, ((float) v17_1.u[1]));
                        v17_1.w = (v17_1.w + v17_1.u[1]);
                    }
                    if ((v17_1.m) || (Math.abs(v1_14) <= v17_1.q)) {
                        float v3_11 = v1_14;
                    } else {
                        float v3_12 = v17_1.getParent();
                        if (v3_12 != 0) {
                            v3_12.requestDisallowInterceptTouchEvent(1);
                        }
                        v17_1.m = 1;
                        if (v1_14) {
                            v3_11 = (v1_14 + v17_1.q);
                        } else {
                            v3_11 = (v1_14 - v17_1.q);
                        }
                    }
                    if (!v17_1.m) {
                    } else {
                        int v13;
                        v17_1.i = (v2_4 - v17_1.u[1]);
                        int v16 = v17_1.getScrollY();
                        int v6 = v17_1.getScrollRange();
                        boolean v1_21 = android.support.v4.view.cx.a(v17_1);
                        if ((v1_21) && ((v1_21 != 1) || (v6 <= 0))) {
                            v13 = 0;
                        } else {
                            v13 = 1;
                        }
                        if ((v17_1.a(0, v3_11, 0, v17_1.getScrollY(), v6)) && (!v17_1.hasNestedScrollingParent())) {
                            v17_1.n.clear();
                        }
                        int v9 = (v17_1.getScrollY() - v16);
                        if (!v17_1.dispatchNestedScroll(0, v9, 0, (v3_11 - v9), v17_1.u)) {
                            if (v13 == 0) {
                            } else {
                                v17_1 = v17_1.i();
                                boolean v1_30 = (v16 + v3_11);
                                if (v1_30) {
                                    if (v1_30 > v6) {
                                        v17_1.h.a((((float) v3_11) / ((float) v17_1.getHeight())), (1065353216 - (android.support.v4.view.bk.c(p18, v15) / ((float) v17_1.getWidth()))));
                                        if (!v17_1.g.a()) {
                                            v17_1.g.c();
                                        }
                                    }
                                } else {
                                    v17_1.g.a((((float) v3_11) / ((float) v17_1.getHeight())), (android.support.v4.view.bk.c(p18, v15) / ((float) v17_1.getWidth())));
                                    if (!v17_1.h.a()) {
                                        v17_1.h.c();
                                    }
                                }
                                if ((v17_1.g == null) || ((v17_1.g.a()) && (v17_1.h.a()))) {
                                } else {
                                    android.support.v4.view.cx.b(v17_1);
                                }
                            }
                        } else {
                            v17_1.i = (v17_1.i - v17_1.u[1]);
                            v14.offsetLocation(0, ((float) v17_1.u[1]));
                            v17_1.w = (v17_1.w + v17_1.u[1]);
                        }
                    }
                } else {
                    android.util.Log.e("NestedScrollView", new StringBuilder("Invalid pointerId=").append(v17_1.t).append(" in onTouchEvent").toString());
                }
                break;
            case 3:
                if ((!v17_1.m) || (v17_1.getChildCount() <= 0)) {
                } else {
                    v17_1.t = -1;
                    v17_1 = v17_1.h();
                }
            case 5:
                boolean v1_5 = android.support.v4.view.bk.b(p18);
                v17_1.i = ((int) android.support.v4.view.bk.d(p18, v1_5));
                v17_1.t = android.support.v4.view.bk.b(p18, v1_5);
                break;
            case 6:
                v17_1 = v17_1.a(p18);
                v17_1.i = ((int) android.support.v4.view.bk.d(p18, android.support.v4.view.bk.a(p18, v17_1.t)));
                break;
            default:
        }
        return v1_69;
    }

    public final void requestChildFocus(android.view.View p2, android.view.View p3)
    {
        if (this.j) {
            this.l = p3;
        } else {
            this.b(p3);
        }
        super.requestChildFocus(p2, p3);
        return;
    }

    public final boolean requestChildRectangleOnScreen(android.view.View p5, android.graphics.Rect p6, boolean p7)
    {
        int v0_2;
        p6.offset((p5.getLeft() - p5.getScrollX()), (p5.getTop() - p5.getScrollY()));
        int v2_3 = this.a(p6);
        if (v2_3 == 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        if (v0_2 != 0) {
            if (!p7) {
                this.b(0, v2_3);
            } else {
                this.scrollBy(0, v2_3);
            }
        }
        return v0_2;
    }

    public final void requestDisallowInterceptTouchEvent(boolean p1)
    {
        if (p1) {
            this.g();
        }
        super.requestDisallowInterceptTouchEvent(p1);
        return;
    }

    public final void requestLayout()
    {
        this.j = 1;
        super.requestLayout();
        return;
    }

    public final void scrollTo(int p5, int p6)
    {
        if (this.getChildCount() > 0) {
            int v0_2 = this.getChildAt(0);
            int v1_3 = android.support.v4.widget.NestedScrollView.b(p5, ((this.getWidth() - this.getPaddingRight()) - this.getPaddingLeft()), v0_2.getWidth());
            int v0_4 = android.support.v4.widget.NestedScrollView.b(p6, ((this.getHeight() - this.getPaddingBottom()) - this.getPaddingTop()), v0_2.getHeight());
            if ((v1_3 != this.getScrollX()) || (v0_4 != this.getScrollY())) {
                super.scrollTo(v1_3, v0_4);
            }
        }
        return;
    }

    public final void setFillViewport(boolean p2)
    {
        if (p2 != this.o) {
            this.o = p2;
            this.requestLayout();
        }
        return;
    }

    public final void setNestedScrollingEnabled(boolean p2)
    {
        this.C.a(p2);
        return;
    }

    public final void setSmoothScrollingEnabled(boolean p1)
    {
        this.p = p1;
        return;
    }

    public final boolean shouldDelayChildPressedState()
    {
        return 1;
    }

    public final boolean startNestedScroll(int p2)
    {
        return this.C.a(p2);
    }

    public final void stopNestedScroll()
    {
        this.C.b();
        return;
    }
}
