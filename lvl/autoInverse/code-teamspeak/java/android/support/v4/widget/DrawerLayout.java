package android.support.v4.widget;
public class DrawerLayout extends android.view.ViewGroup implements android.support.v4.widget.ak {
    public static final int a = 0;
    public static final int b = 1;
    public static final int c = 2;
    public static final int d = 0;
    public static final int e = 1;
    public static final int f = 2;
    static final android.support.v4.widget.z n = None;
    private static final String o = "DrawerLayout";
    private static final int p = 64;
    private static final int q = 10;
    private static final int r = 2566914048;
    private static final int s = 160;
    private static final int t = 400;
    private static final boolean u = False;
    private static final boolean v = True;
    private static final float w = 16256;
    private static final int[] x;
    private static final boolean y;
    private static final boolean z;
    private final android.support.v4.widget.y A;
    private float B;
    private int C;
    private int D;
    private float E;
    private android.graphics.Paint F;
    private final android.support.v4.widget.ag G;
    private final android.support.v4.widget.ag H;
    private boolean I;
    private boolean J;
    private int K;
    private int L;
    private boolean M;
    private float N;
    private float O;
    private android.graphics.drawable.Drawable P;
    private android.graphics.drawable.Drawable Q;
    private android.graphics.drawable.Drawable R;
    private Object S;
    private boolean T;
    private android.graphics.drawable.Drawable U;
    private android.graphics.drawable.Drawable V;
    private android.graphics.drawable.Drawable W;
    private android.graphics.drawable.Drawable aa;
    private final java.util.ArrayList ab;
    final android.support.v4.widget.eg g;
    final android.support.v4.widget.eg h;
    int i;
    boolean j;
    android.support.v4.widget.ac k;
    CharSequence l;
    CharSequence m;

    static DrawerLayout()
    {
        android.support.v4.widget.ab v0_2;
        int v1 = 1;
        android.support.v4.widget.ab v0_0 = new int[1];
        v0_0[0] = 16842931;
        android.support.v4.widget.DrawerLayout.x = v0_0;
        if (android.os.Build$VERSION.SDK_INT < 19) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        android.support.v4.widget.DrawerLayout.y = v0_2;
        if (android.os.Build$VERSION.SDK_INT < 21) {
            v1 = 0;
        }
        android.support.v4.widget.DrawerLayout.z = v1;
        if (android.os.Build$VERSION.SDK_INT < 21) {
            android.support.v4.widget.DrawerLayout.n = new android.support.v4.widget.ab();
        } else {
            android.support.v4.widget.DrawerLayout.n = new android.support.v4.widget.aa();
        }
        return;
    }

    private DrawerLayout(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    private DrawerLayout(android.content.Context p2, byte p3)
    {
        this(p2, 0);
        return;
    }

    private DrawerLayout(android.content.Context p7, char p8)
    {
        this(p7, 0, 0);
        this.A = new android.support.v4.widget.y(this);
        this.D = -1728053248;
        this.F = new android.graphics.Paint();
        this.J = 1;
        this.U = 0;
        this.V = 0;
        this.W = 0;
        this.aa = 0;
        this.setDescendantFocusability(262144);
        java.util.ArrayList v0_9 = this.getResources().getDisplayMetrics().density;
        this.C = ((int) ((1115684864 * v0_9) + 1056964608));
        int v1_6 = (1137180672 * v0_9);
        this.G = new android.support.v4.widget.ag(this, 3);
        this.H = new android.support.v4.widget.ag(this, 5);
        this.g = android.support.v4.widget.eg.a(this, 1065353216, this.G);
        this.g.u = 1;
        this.g.s = v1_6;
        this.G.b = this.g;
        this.h = android.support.v4.widget.eg.a(this, 1065353216, this.H);
        this.h.u = 2;
        this.h.s = v1_6;
        this.H.b = this.h;
        this.setFocusableInTouchMode(1);
        android.support.v4.view.cx.c(this, 1);
        android.support.v4.view.cx.a(this, new android.support.v4.widget.x(this));
        android.support.v4.view.ec.a(this);
        if (android.support.v4.view.cx.u(this)) {
            android.support.v4.widget.DrawerLayout.n.a(this);
            this.P = android.support.v4.widget.DrawerLayout.n.a(p7);
        }
        this.B = (v0_9 * 1092616192);
        this.ab = new java.util.ArrayList();
        return;
    }

    static synthetic android.view.View a(android.support.v4.widget.DrawerLayout p1)
    {
        return p1.n();
    }

    private void a(float p2)
    {
        if (this.k != null) {
            this.k.a(p2);
        }
        return;
    }

    private void a(int p5, int p6)
    {
        android.graphics.drawable.Drawable v0_1 = this.getResources().getDrawable(p5);
        if (!android.support.v4.widget.DrawerLayout.z) {
            if ((p6 & 8388611) != 8388611) {
                if ((p6 & 8388613) != 8388613) {
                    if ((p6 & 3) != 3) {
                        if ((p6 & 5) != 5) {
                            return;
                        } else {
                            this.aa = v0_1;
                        }
                    } else {
                        this.W = v0_1;
                    }
                } else {
                    this.V = v0_1;
                }
            } else {
                this.U = v0_1;
            }
            this.h();
            this.invalidate();
        }
        return;
    }

    private void a(int p4, android.view.View p5)
    {
        if (android.support.v4.widget.DrawerLayout.d(p5)) {
            this.b(p4, ((android.support.v4.widget.ad) p5.getLayoutParams()).a);
            return;
        } else {
            throw new IllegalArgumentException(new StringBuilder("View ").append(p5).append(" is not a drawer with appropriate layout_gravity").toString());
        }
    }

    private void a(int p3, CharSequence p4)
    {
        int v0_1 = android.support.v4.view.v.a(p3, android.support.v4.view.cx.f(this));
        if (v0_1 != 3) {
            if (v0_1 == 5) {
                this.m = p4;
            }
        } else {
            this.l = p4;
        }
        return;
    }

    private void a(android.graphics.drawable.Drawable p4, int p5)
    {
        if (!android.support.v4.widget.DrawerLayout.z) {
            if ((p5 & 8388611) != 8388611) {
                if ((p5 & 8388613) != 8388613) {
                    if ((p5 & 3) != 3) {
                        if ((p5 & 5) != 5) {
                            return;
                        } else {
                            this.aa = p4;
                        }
                    } else {
                        this.W = p4;
                    }
                } else {
                    this.V = p4;
                }
            } else {
                this.U = p4;
            }
            this.h();
            this.invalidate();
        }
        return;
    }

    private void a(boolean p10)
    {
        int v4 = this.getChildCount();
        int v2 = 0;
        int v1 = 0;
        while (v2 < v4) {
            boolean v5_0 = this.getChildAt(v2);
            int v0_3 = ((android.support.v4.widget.ad) v5_0.getLayoutParams());
            if ((android.support.v4.widget.DrawerLayout.d(v5_0)) && ((!p10) || (v0_3.c))) {
                android.support.v4.widget.eg v6_2 = v5_0.getWidth();
                if (!this.a(v5_0, 3)) {
                    v1 |= this.h.a(v5_0, this.getWidth(), v5_0.getTop());
                } else {
                    v1 |= this.g.a(v5_0, (- v6_2), v5_0.getTop());
                }
                v0_3.c = 0;
            }
            v2++;
        }
        this.G.a();
        this.H.a();
        if (v1 != 0) {
            this.invalidate();
        }
        return;
    }

    static float b(android.view.View p1)
    {
        return ((android.support.v4.widget.ad) p1.getLayoutParams()).b;
    }

    private int b(int p3)
    {
        int v0_2;
        int v0_1 = android.support.v4.view.v.a(p3, android.support.v4.view.cx.f(this));
        if (v0_1 != 3) {
            if (v0_1 != 5) {
                v0_2 = 0;
            } else {
                v0_2 = this.L;
            }
        } else {
            v0_2 = this.K;
        }
        return v0_2;
    }

    private void b(int p4, int p5)
    {
        int v1 = android.support.v4.view.v.a(p5, android.support.v4.view.cx.f(this));
        if (v1 != 3) {
            if (v1 == 5) {
                this.L = p4;
            }
        } else {
            this.K = p4;
        }
        if (p4 != 0) {
            android.view.View v0_2;
            if (v1 != 3) {
                v0_2 = this.h;
            } else {
                v0_2 = this.g;
            }
            v0_2.a();
        }
        switch (p4) {
            case 1:
                android.view.View v0_4 = this.a(v1);
                if (v0_4 == null) {
                } else {
                    this.e(v0_4);
                }
                break;
            case 2:
                android.view.View v0_3 = this.a(v1);
                if (v0_3 == null) {
                } else {
                    this.k(v0_3);
                }
                break;
        }
        return;
    }

    private void b(int p8, android.view.View p9)
    {
        int v1_2;
        int v1_1 = this.g.m;
        float v4_1 = this.h.m;
        if ((v1_1 != 1) && (v4_1 != 1)) {
            if ((v1_1 != 2) && (v4_1 != 2)) {
                v1_2 = 0;
            } else {
                v1_2 = 2;
            }
        } else {
            v1_2 = 1;
        }
        if ((p9 != null) && (p8 == 0)) {
            boolean v0_2 = ((android.support.v4.widget.ad) p9.getLayoutParams());
            if (v0_2.b != 0) {
                if (v0_2.b == 1065353216) {
                    boolean v0_6 = ((android.support.v4.widget.ad) p9.getLayoutParams());
                    if (!v0_6.d) {
                        v0_6.d = 1;
                        if (this.k != null) {
                            this.k.a();
                        }
                        this.a(p9, 1);
                        if (this.hasWindowFocus()) {
                            this.sendAccessibilityEvent(32);
                        }
                        p9.requestFocus();
                    }
                }
            } else {
                boolean v0_11 = ((android.support.v4.widget.ad) p9.getLayoutParams());
                if (v0_11.d) {
                    v0_11.d = 0;
                    if (this.k != null) {
                        this.k.b();
                    }
                    this.a(p9, 0);
                    if (this.hasWindowFocus()) {
                        boolean v0_15 = this.getRootView();
                        if (v0_15) {
                            v0_15.sendAccessibilityEvent(32);
                        }
                    }
                }
            }
        }
        if (v1_2 != this.i) {
            this.i = v1_2;
        }
        return;
    }

    private void b(android.view.View p4, float p5)
    {
        int v0_0 = android.support.v4.widget.DrawerLayout.b(p4);
        boolean v1_0 = p4.getWidth();
        int v0_3 = (((int) (((float) v1_0) * p5)) - ((int) (v0_0 * ((float) v1_0))));
        if (!this.a(p4, 3)) {
            v0_3 = (- v0_3);
        }
        p4.offsetLeftAndRight(v0_3);
        this.a(p4, p5);
        return;
    }

    private static boolean b(android.graphics.drawable.Drawable p1, int p2)
    {
        if ((p1 != null) && (android.support.v4.e.a.a.b(p1))) {
            android.support.v4.e.a.a.b(p1, p2);
            int v0_1 = 1;
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    private CharSequence c(int p3)
    {
        int v0_2;
        int v0_1 = android.support.v4.view.v.a(p3, android.support.v4.view.cx.f(this));
        if (v0_1 != 3) {
            if (v0_1 != 5) {
                v0_2 = 0;
            } else {
                v0_2 = this.m;
            }
        } else {
            v0_2 = this.l;
        }
        return v0_2;
    }

    private static String d(int p2)
    {
        String v0_2;
        if ((p2 & 3) != 3) {
            if ((p2 & 5) != 5) {
                v0_2 = Integer.toHexString(p2);
            } else {
                v0_2 = "RIGHT";
            }
        } else {
            v0_2 = "LEFT";
        }
        return v0_2;
    }

    static boolean d(android.view.View p2)
    {
        int v0_5;
        if ((android.support.v4.view.v.a(((android.support.v4.widget.ad) p2.getLayoutParams()).a, android.support.v4.view.cx.f(p2)) & 7) == 0) {
            v0_5 = 0;
        } else {
            v0_5 = 1;
        }
        return v0_5;
    }

    static synthetic int[] e()
    {
        return android.support.v4.widget.DrawerLayout.x;
    }

    static synthetic boolean f()
    {
        return android.support.v4.widget.DrawerLayout.y;
    }

    static synthetic boolean f(android.view.View p2)
    {
        if ((android.support.v4.view.cx.c(p2) == 4) || (android.support.v4.view.cx.c(p2) == 2)) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private android.view.View g()
    {
        int v3 = this.getChildCount();
        int v2 = 0;
        while (v2 < v3) {
            android.view.View v1 = this.getChildAt(v2);
            if (!((android.support.v4.widget.ad) v1.getLayoutParams()).d) {
                v2++;
            } else {
                int v0_1 = v1;
            }
            return v0_1;
        }
        v0_1 = 0;
        return v0_1;
    }

    private void g(android.view.View p4)
    {
        android.view.View v0_1 = ((android.support.v4.widget.ad) p4.getLayoutParams());
        if (v0_1.d) {
            v0_1.d = 0;
            if (this.k != null) {
                this.k.b();
            }
            this.a(p4, 0);
            if (this.hasWindowFocus()) {
                android.view.View v0_5 = this.getRootView();
                if (v0_5 != null) {
                    v0_5.sendAccessibilityEvent(32);
                }
            }
        }
        return;
    }

    private void h()
    {
        if (!android.support.v4.widget.DrawerLayout.z) {
            android.graphics.drawable.Drawable v0_2;
            android.graphics.drawable.Drawable v0_1 = android.support.v4.view.cx.f(this);
            if (v0_1 != null) {
                if (this.V == null) {
                    v0_2 = this.W;
                } else {
                    android.support.v4.widget.DrawerLayout.b(this.V, v0_1);
                    v0_2 = this.V;
                }
            } else {
                if (this.U == null) {
                } else {
                    android.support.v4.widget.DrawerLayout.b(this.U, v0_1);
                    v0_2 = this.U;
                }
            }
            android.graphics.drawable.Drawable v0_4;
            this.Q = v0_2;
            android.graphics.drawable.Drawable v0_3 = android.support.v4.view.cx.f(this);
            if (v0_3 != null) {
                if (this.U == null) {
                    v0_4 = this.aa;
                } else {
                    android.support.v4.widget.DrawerLayout.b(this.U, v0_3);
                    v0_4 = this.U;
                }
            } else {
                if (this.V == null) {
                } else {
                    android.support.v4.widget.DrawerLayout.b(this.V, v0_3);
                    v0_4 = this.V;
                }
            }
            this.R = v0_4;
        }
        return;
    }

    private void h(android.view.View p4)
    {
        int v0_1 = ((android.support.v4.widget.ad) p4.getLayoutParams());
        if (!v0_1.d) {
            v0_1.d = 1;
            if (this.k != null) {
                this.k.a();
            }
            this.a(p4, 1);
            if (this.hasWindowFocus()) {
                this.sendAccessibilityEvent(32);
            }
            p4.requestFocus();
        }
        return;
    }

    private android.graphics.drawable.Drawable i()
    {
        android.graphics.drawable.Drawable v0_1;
        android.graphics.drawable.Drawable v0_0 = android.support.v4.view.cx.f(this);
        if (v0_0 != null) {
            if (this.V == null) {
                v0_1 = this.W;
            } else {
                android.support.v4.widget.DrawerLayout.b(this.V, v0_0);
                v0_1 = this.V;
            }
        } else {
            if (this.U == null) {
            } else {
                android.support.v4.widget.DrawerLayout.b(this.U, v0_0);
                v0_1 = this.U;
            }
        }
        return v0_1;
    }

    private static boolean i(android.view.View p3)
    {
        int v0 = 0;
        int v1_0 = p3.getBackground();
        if ((v1_0 != 0) && (v1_0.getOpacity() == -1)) {
            v0 = 1;
        }
        return v0;
    }

    private android.graphics.drawable.Drawable j()
    {
        android.graphics.drawable.Drawable v0_1;
        android.graphics.drawable.Drawable v0_0 = android.support.v4.view.cx.f(this);
        if (v0_0 != null) {
            if (this.U == null) {
                v0_1 = this.aa;
            } else {
                android.support.v4.widget.DrawerLayout.b(this.U, v0_0);
                v0_1 = this.U;
            }
        } else {
            if (this.V == null) {
            } else {
                android.support.v4.widget.DrawerLayout.b(this.V, v0_0);
                v0_1 = this.V;
            }
        }
        return v0_1;
    }

    private static boolean j(android.view.View p1)
    {
        int v0_3;
        if (((android.support.v4.widget.ad) p1.getLayoutParams()).a != 0) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    private void k()
    {
        this.a(0);
        return;
    }

    private void k(android.view.View p4)
    {
        if (android.support.v4.widget.DrawerLayout.d(p4)) {
            if (!this.J) {
                if (!this.a(p4, 3)) {
                    this.h.a(p4, (this.getWidth() - p4.getWidth()), p4.getTop());
                } else {
                    this.g.a(p4, 0, p4.getTop());
                }
            } else {
                android.support.v4.widget.eg v0_7 = ((android.support.v4.widget.ad) p4.getLayoutParams());
                v0_7.b = 1065353216;
                v0_7.d = 1;
                this.a(p4, 1);
            }
            this.invalidate();
            return;
        } else {
            throw new IllegalArgumentException(new StringBuilder("View ").append(p4).append(" is not a sliding drawer").toString());
        }
    }

    private boolean l()
    {
        int v3 = this.getChildCount();
        int v2 = 0;
        while (v2 < v3) {
            if (!((android.support.v4.widget.ad) this.getChildAt(v2).getLayoutParams()).c) {
                v2++;
            } else {
                int v0_0 = 1;
            }
            return v0_0;
        }
        v0_0 = 0;
        return v0_0;
    }

    private static boolean l(android.view.View p3)
    {
        if (android.support.v4.widget.DrawerLayout.d(p3)) {
            return ((android.support.v4.widget.ad) p3.getLayoutParams()).d;
        } else {
            throw new IllegalArgumentException(new StringBuilder("View ").append(p3).append(" is not a drawer").toString());
        }
    }

    private boolean m()
    {
        int v0_1;
        if (this.n() == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    private static boolean m(android.view.View p3)
    {
        if (android.support.v4.widget.DrawerLayout.d(p3)) {
            int v0_5;
            if (((android.support.v4.widget.ad) p3.getLayoutParams()).b <= 0) {
                v0_5 = 0;
            } else {
                v0_5 = 1;
            }
            return v0_5;
        } else {
            throw new IllegalArgumentException(new StringBuilder("View ").append(p3).append(" is not a drawer").toString());
        }
    }

    private android.view.View n()
    {
        int v2 = this.getChildCount();
        int v1 = 0;
        while (v1 < v2) {
            int v0_1 = this.getChildAt(v1);
            if ((!android.support.v4.widget.DrawerLayout.d(v0_1)) || (!android.support.v4.widget.DrawerLayout.m(v0_1))) {
                v1++;
            }
            return v0_1;
        }
        v0_1 = 0;
        return v0_1;
    }

    private static boolean n(android.view.View p2)
    {
        if ((android.support.v4.view.cx.c(p2) == 4) || (android.support.v4.view.cx.c(p2) == 2)) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private void o()
    {
        int v7 = 0;
        if (!this.j) {
            int v0_1 = android.os.SystemClock.uptimeMillis();
            int v0_2 = android.view.MotionEvent.obtain(v0_1, v0_1, 3, 0, 0, 0);
            int v1 = this.getChildCount();
            while (v7 < v1) {
                this.getChildAt(v7).dispatchTouchEvent(v0_2);
                v7++;
            }
            v0_2.recycle();
            this.j = 1;
        }
        return;
    }

    public final int a(android.view.View p3)
    {
        int v0_1;
        int v0_0 = this.c(p3);
        if (v0_0 != 3) {
            if (v0_0 != 5) {
                v0_1 = 0;
            } else {
                v0_1 = this.L;
            }
        } else {
            v0_1 = this.K;
        }
        return v0_1;
    }

    final android.view.View a(int p6)
    {
        int v2 = (android.support.v4.view.v.a(p6, android.support.v4.view.cx.f(this)) & 7);
        int v3 = this.getChildCount();
        int v1 = 0;
        while (v1 < v3) {
            int v0_3 = this.getChildAt(v1);
            if ((this.c(v0_3) & 7) != v2) {
                v1++;
            }
            return v0_3;
        }
        v0_3 = 0;
        return v0_3;
    }

    public final void a()
    {
        IllegalArgumentException v0_0 = this.a(8388611);
        if (v0_0 != null) {
            this.k(v0_0);
            return;
        } else {
            throw new IllegalArgumentException(new StringBuilder("No drawer view found with gravity ").append(android.support.v4.widget.DrawerLayout.d(8388611)).toString());
        }
    }

    final void a(android.view.View p3, float p4)
    {
        android.support.v4.widget.ac v0_1 = ((android.support.v4.widget.ad) p3.getLayoutParams());
        if (p4 != v0_1.b) {
            v0_1.b = p4;
            if (this.k != null) {
                this.k.a(p4);
            }
        }
        return;
    }

    final void a(android.view.View p5, boolean p6)
    {
        int v1 = this.getChildCount();
        int v0 = 0;
        while (v0 < v1) {
            android.view.View v2 = this.getChildAt(v0);
            if (((p6) || (android.support.v4.widget.DrawerLayout.d(v2))) && ((!p6) || (v2 != p5))) {
                android.support.v4.view.cx.c(v2, 4);
            } else {
                android.support.v4.view.cx.c(v2, 1);
            }
            v0++;
        }
        return;
    }

    public final void a(Object p2, boolean p3)
    {
        int v0_1;
        this.S = p2;
        this.T = p3;
        if ((p3) || (this.getBackground() != null)) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        this.setWillNotDraw(v0_1);
        this.requestLayout();
        return;
    }

    final boolean a(android.view.View p2, int p3)
    {
        int v0_2;
        if ((this.c(p2) & p3) != p3) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public void addFocusables(java.util.ArrayList p7, int p8, int p9)
    {
        int v1 = 0;
        if (this.getDescendantFocusability() != 393216) {
            int v3_0 = this.getChildCount();
            int v2_1 = 0;
            int v0_1 = 0;
            while (v2_1 < v3_0) {
                android.view.View v4 = this.getChildAt(v2_1);
                if (!android.support.v4.widget.DrawerLayout.d(v4)) {
                    this.ab.add(v4);
                } else {
                    if (android.support.v4.widget.DrawerLayout.l(v4)) {
                        v0_1 = 1;
                        v4.addFocusables(p7, p8, p9);
                    }
                }
                v2_1++;
            }
            if (v0_1 == 0) {
                int v2_2 = this.ab.size();
                while (v1 < v2_2) {
                    int v0_6 = ((android.view.View) this.ab.get(v1));
                    if (v0_6.getVisibility() == 0) {
                        v0_6.addFocusables(p7, p8, p9);
                    }
                    v1++;
                }
            }
            this.ab.clear();
        }
        return;
    }

    public void addView(android.view.View p2, int p3, android.view.ViewGroup$LayoutParams p4)
    {
        super.addView(p2, p3, p4);
        if ((this.g() == null) && (!android.support.v4.widget.DrawerLayout.d(p2))) {
            android.support.v4.view.cx.c(p2, 1);
        } else {
            android.support.v4.view.cx.c(p2, 4);
        }
        if (!android.support.v4.widget.DrawerLayout.y) {
            android.support.v4.view.cx.a(p2, this.A);
        }
        return;
    }

    public final void b()
    {
        IllegalArgumentException v0_0 = this.a(8388611);
        if (v0_0 != null) {
            this.e(v0_0);
            return;
        } else {
            throw new IllegalArgumentException(new StringBuilder("No drawer view found with gravity ").append(android.support.v4.widget.DrawerLayout.d(8388611)).toString());
        }
    }

    final int c(android.view.View p3)
    {
        return android.support.v4.view.v.a(((android.support.v4.widget.ad) p3.getLayoutParams()).a, android.support.v4.view.cx.f(this));
    }

    public final boolean c()
    {
        int v0_2;
        int v0_1 = this.a(8388611);
        if (v0_1 == 0) {
            v0_2 = 0;
        } else {
            v0_2 = android.support.v4.widget.DrawerLayout.l(v0_1);
        }
        return v0_2;
    }

    protected boolean checkLayoutParams(android.view.ViewGroup$LayoutParams p2)
    {
        if ((!(p2 instanceof android.support.v4.widget.ad)) || (!super.checkLayoutParams(p2))) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public void computeScroll()
    {
        int v3 = this.getChildCount();
        float v2 = 0;
        boolean v1_1 = 0;
        while (v1_1 < v3) {
            v2 = Math.max(v2, ((android.support.v4.widget.ad) this.getChildAt(v1_1).getLayoutParams()).b);
            v1_1++;
        }
        this.E = v2;
        if ((this.g.c() | this.h.c()) != 0) {
            android.support.v4.view.cx.b(this);
        }
        return;
    }

    public final boolean d()
    {
        int v0_2;
        int v0_1 = this.a(8388611);
        if (v0_1 == 0) {
            v0_2 = 0;
        } else {
            v0_2 = android.support.v4.widget.DrawerLayout.m(v0_1);
        }
        return v0_2;
    }

    protected boolean drawChild(android.graphics.Canvas p12, android.view.View p13, long p14)
    {
        int v4_0 = this.getHeight();
        int v5_0 = android.support.v4.widget.DrawerLayout.j(p13);
        int v1_0 = 0;
        float v2_0 = this.getWidth();
        int v6 = p12.save();
        if (v5_0 != 0) {
            boolean v7_0 = this.getChildCount();
            android.graphics.drawable.Drawable v3_0 = 0;
            while (v3_0 < v7_0) {
                int v0_29;
                android.view.View v8 = this.getChildAt(v3_0);
                if ((v8 == p13) || (v8.getVisibility() != 0)) {
                    v0_29 = v2_0;
                } else {
                    int v0_23;
                    int v0_22 = v8.getBackground();
                    if (v0_22 == 0) {
                        v0_23 = 0;
                    } else {
                        if (v0_22.getOpacity() != -1) {
                            v0_23 = 0;
                        } else {
                            v0_23 = 1;
                        }
                    }
                    if ((v0_23 == 0) || ((!android.support.v4.widget.DrawerLayout.d(v8)) || (v8.getHeight() < v4_0))) {
                    } else {
                        if (!this.a(v8, 3)) {
                            v0_29 = v8.getLeft();
                            if (v0_29 >= v2_0) {
                            }
                        } else {
                            int v0_30 = v8.getRight();
                            if (v0_30 <= v1_0) {
                                v0_30 = v1_0;
                            }
                            v1_0 = v0_30;
                            v0_29 = v2_0;
                        }
                    }
                }
                v3_0++;
                v2_0 = v0_29;
            }
            p12.clipRect(v1_0, 0, v2_0, this.getHeight());
        }
        int v0_2 = v2_0;
        boolean v7_1 = super.drawChild(p12, p13, p14);
        p12.restoreToCount(v6);
        if ((this.E <= 0) || (v5_0 == 0)) {
            if ((this.Q == null) || (!this.a(p13, 3))) {
                if ((this.R != null) && (this.a(p13, 5))) {
                    int v0_10 = this.R.getIntrinsicWidth();
                    int v1_1 = p13.getLeft();
                    float v2_8 = Math.max(0, Math.min((((float) (this.getWidth() - v1_1)) / ((float) this.h.t)), 1065353216));
                    this.R.setBounds((v1_1 - v0_10), p13.getTop(), v1_1, p13.getBottom());
                    this.R.setAlpha(((int) (1132396544 * v2_8)));
                    this.R.draw(p12);
                }
            } else {
                int v0_15 = this.Q.getIntrinsicWidth();
                int v1_5 = p13.getRight();
                float v2_14 = Math.max(0, Math.min((((float) v1_5) / ((float) this.g.t)), 1065353216));
                this.Q.setBounds(v1_5, p13.getTop(), (v0_15 + v1_5), p13.getBottom());
                this.Q.setAlpha(((int) (1132396544 * v2_14)));
                this.Q.draw(p12);
            }
        } else {
            this.F.setColor(((((int) (((float) ((this.D & -16777216) >> 24)) * this.E)) << 24) | (this.D & 16777215)));
            p12.drawRect(((float) v1_0), 0, ((float) v0_2), ((float) this.getHeight()), this.F);
        }
        return v7_1;
    }

    public final void e(android.view.View p4)
    {
        if (android.support.v4.widget.DrawerLayout.d(p4)) {
            if (!this.J) {
                if (!this.a(p4, 3)) {
                    this.h.a(p4, this.getWidth(), p4.getTop());
                } else {
                    this.g.a(p4, (- p4.getWidth()), p4.getTop());
                }
            } else {
                android.support.v4.widget.eg v0_7 = ((android.support.v4.widget.ad) p4.getLayoutParams());
                v0_7.b = 0;
                v0_7.d = 0;
            }
            this.invalidate();
            return;
        } else {
            throw new IllegalArgumentException(new StringBuilder("View ").append(p4).append(" is not a sliding drawer").toString());
        }
    }

    protected android.view.ViewGroup$LayoutParams generateDefaultLayoutParams()
    {
        return new android.support.v4.widget.ad(-1, -1);
    }

    public android.view.ViewGroup$LayoutParams generateLayoutParams(android.util.AttributeSet p3)
    {
        return new android.support.v4.widget.ad(this.getContext(), p3);
    }

    protected android.view.ViewGroup$LayoutParams generateLayoutParams(android.view.ViewGroup$LayoutParams p2)
    {
        android.support.v4.widget.ad v0_3;
        if (!(p2 instanceof android.support.v4.widget.ad)) {
            if (!(p2 instanceof android.view.ViewGroup$MarginLayoutParams)) {
                v0_3 = new android.support.v4.widget.ad(p2);
            } else {
                v0_3 = new android.support.v4.widget.ad(((android.view.ViewGroup$MarginLayoutParams) p2));
            }
        } else {
            v0_3 = new android.support.v4.widget.ad(((android.support.v4.widget.ad) p2));
        }
        return v0_3;
    }

    public float getDrawerElevation()
    {
        int v0_1;
        if (!android.support.v4.widget.DrawerLayout.z) {
            v0_1 = 0;
        } else {
            v0_1 = this.B;
        }
        return v0_1;
    }

    public android.graphics.drawable.Drawable getStatusBarBackgroundDrawable()
    {
        return this.P;
    }

    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        this.J = 1;
        return;
    }

    protected void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        this.J = 1;
        return;
    }

    public void onDraw(android.graphics.Canvas p5)
    {
        super.onDraw(p5);
        if ((this.T) && (this.P != null)) {
            android.graphics.drawable.Drawable v0_3 = android.support.v4.widget.DrawerLayout.n.a(this.S);
            if (v0_3 > null) {
                this.P.setBounds(0, 0, this.getWidth(), v0_3);
                this.P.draw(p5);
            }
        }
        return;
    }

    public boolean onInterceptTouchEvent(android.view.MotionEvent p10)
    {
        boolean v0_6;
        int v2 = 0;
        boolean v0_0 = android.support.v4.view.bk.a(p10);
        int v4_2 = (this.h.a(p10) | this.g.a(p10));
        switch (v0_0) {
            case 0:
                boolean v0_7 = p10.getX();
                int v3_10 = p10.getY();
                this.N = v0_7;
                this.O = v3_10;
                if (this.E <= 0) {
                    v0_6 = 0;
                } else {
                    boolean v0_9 = this.g.b(((int) v0_7), ((int) v3_10));
                    if ((!v0_9) || (!android.support.v4.widget.DrawerLayout.j(v0_9))) {
                    } else {
                        v0_6 = 1;
                    }
                }
                this.M = 0;
                this.j = 0;
                break;
            case 1:
            case 3:
                this.a(1);
                this.M = 0;
                this.j = 0;
                v0_6 = 0;
                break;
            case 2:
                android.support.v4.widget.eg v5_0 = this.g;
                boolean v0_2 = 0;
                while (v0_2 < v5_0.o.length) {
                    int v3_3;
                    if (!v5_0.a(v0_2)) {
                        v3_3 = 0;
                    } else {
                        if ((((v5_0.q[v0_2] - v5_0.o[v0_2]) * (v5_0.q[v0_2] - v5_0.o[v0_2])) + ((v5_0.r[v0_2] - v5_0.p[v0_2]) * (v5_0.r[v0_2] - v5_0.p[v0_2]))) <= ((float) (v5_0.n * v5_0.n))) {
                            v3_3 = 0;
                        } else {
                            v3_3 = 1;
                        }
                    }
                    if (v3_3 == 0) {
                        v0_2++;
                    } else {
                        boolean v0_3 = 1;
                    }
                    if (!v0_3) {
                    } else {
                        this.G.a();
                        this.H.a();
                        v0_6 = 0;
                    }
                }
                v0_3 = 0;
                break;
            default:
        }
        if ((v4_2 != 0) || (v0_6)) {
            v2 = 1;
        } else {
            int v4_3 = this.getChildCount();
            int v3_12 = 0;
            while (v3_12 < v4_3) {
                if (!((android.support.v4.widget.ad) this.getChildAt(v3_12).getLayoutParams()).c) {
                    v3_12++;
                } else {
                    boolean v0_11 = 1;
                }
                if ((v0_11) || (this.j)) {
                }
            }
            v0_11 = 0;
        }
        return v2;
    }

    public boolean onKeyDown(int p3, android.view.KeyEvent p4)
    {
        boolean v0 = 1;
        if (p3 != 4) {
            v0 = super.onKeyDown(p3, p4);
        } else {
            int v1_2;
            if (this.n() == null) {
                v1_2 = 0;
            } else {
                v1_2 = 1;
            }
            if (v1_2 == 0) {
            } else {
                android.support.v4.view.ab.c(p4);
            }
        }
        return v0;
    }

    public boolean onKeyUp(int p4, android.view.KeyEvent p5)
    {
        int v0 = 0;
        if (p4 != 4) {
            v0 = super.onKeyUp(p4, p5);
        } else {
            android.view.View v1_1 = this.n();
            if ((v1_1 != null) && (this.a(v1_1) == 0)) {
                this.a(0);
            }
            if (v1_1 != null) {
                v0 = 1;
            }
        }
        return v0;
    }

    protected void onLayout(boolean p15, int p16, int p17, int p18, int p19)
    {
        this.I = 1;
        int v6 = (p18 - p16);
        int v7 = this.getChildCount();
        int v5 = 0;
        while (v5 < v7) {
            android.view.View v8 = this.getChildAt(v5);
            if (v8.getVisibility() != 8) {
                int v0_6 = ((android.support.v4.widget.ad) v8.getLayoutParams());
                if (!android.support.v4.widget.DrawerLayout.j(v8)) {
                    int v1_9;
                    int v2_1;
                    int v9_0 = v8.getMeasuredWidth();
                    int v10_0 = v8.getMeasuredHeight();
                    if (!this.a(v8, 3)) {
                        v2_1 = (v6 - ((int) (((float) v9_0) * v0_6.b)));
                        v1_9 = (((float) (v6 - v2_1)) / ((float) v9_0));
                    } else {
                        v2_1 = (((int) (((float) v9_0) * v0_6.b)) + (- v9_0));
                        v1_9 = (((float) (v9_0 + v2_1)) / ((float) v9_0));
                    }
                    int v3_5;
                    if (v1_9 == v0_6.b) {
                        v3_5 = 0;
                    } else {
                        v3_5 = 1;
                    }
                    switch ((v0_6.a & 112)) {
                        case 16:
                            int v11_2 = (p19 - p17);
                            int v4_5 = ((v11_2 - v10_0) / 2);
                            if (v4_5 >= v0_6.topMargin) {
                                if ((v4_5 + v10_0) > (v11_2 - v0_6.bottomMargin)) {
                                    v4_5 = ((v11_2 - v0_6.bottomMargin) - v10_0);
                                }
                            } else {
                                v4_5 = v0_6.topMargin;
                            }
                            v8.layout(v2_1, v4_5, (v9_0 + v2_1), (v10_0 + v4_5));
                            break;
                        case 80:
                            int v4_2 = (p19 - p17);
                            v8.layout(v2_1, ((v4_2 - v0_6.bottomMargin) - v8.getMeasuredHeight()), (v9_0 + v2_1), (v4_2 - v0_6.bottomMargin));
                            break;
                        default:
                            v8.layout(v2_1, v0_6.topMargin, (v9_0 + v2_1), (v10_0 + v0_6.topMargin));
                    }
                    if (v3_5 != 0) {
                        this.a(v8, v1_9);
                    }
                    int v0_9;
                    if (v0_6.b <= 0) {
                        v0_9 = 4;
                    } else {
                        v0_9 = 0;
                    }
                    if (v8.getVisibility() != v0_9) {
                        v8.setVisibility(v0_9);
                    }
                } else {
                    v8.layout(v0_6.leftMargin, v0_6.topMargin, (v0_6.leftMargin + v8.getMeasuredWidth()), (v0_6.topMargin + v8.getMeasuredHeight()));
                }
            }
            v5++;
        }
        this.I = 0;
        this.J = 0;
        return;
    }

    protected void onMeasure(int p13, int p14)
    {
        String v1_0 = 300;
        int v4 = 0;
        int v3_0 = android.view.View$MeasureSpec.getMode(p13);
        int v5_0 = android.view.View$MeasureSpec.getMode(p14);
        String v2_0 = android.view.View$MeasureSpec.getSize(p13);
        int v0_0 = android.view.View$MeasureSpec.getSize(p14);
        if ((v3_0 == 1073741824) && (v5_0 == 1073741824)) {
            v1_0 = v0_0;
        } else {
            if (!this.isInEditMode()) {
                throw new IllegalArgumentException("DrawerLayout must be measured with MeasureSpec.EXACTLY.");
            } else {
                if ((v3_0 != -2147483648) && (v3_0 == 0)) {
                    v2_0 = 300;
                }
                if ((v5_0 == -2147483648) || (v5_0 != 0)) {
                }
            }
        }
        int v3_1;
        this.setMeasuredDimension(v2_0, v1_0);
        if ((this.S == null) || (!android.support.v4.view.cx.u(this))) {
            v3_1 = 0;
        } else {
            v3_1 = 1;
        }
        int v5_1 = android.support.v4.view.cx.f(this);
        int v6_1 = this.getChildCount();
        while (v4 < v6_1) {
            android.view.View v7_1 = this.getChildAt(v4);
            if (v7_1.getVisibility() != 8) {
                int v0_8 = ((android.support.v4.widget.ad) v7_1.getLayoutParams());
                if (v3_1 != 0) {
                    int v8_2 = android.support.v4.view.v.a(v0_8.a, v5_1);
                    if (!android.support.v4.view.cx.u(v7_1)) {
                        android.support.v4.widget.DrawerLayout.n.a(v0_8, this.S, v8_2);
                    } else {
                        android.support.v4.widget.DrawerLayout.n.a(v7_1, this.S, v8_2);
                    }
                }
                if (!android.support.v4.widget.DrawerLayout.j(v7_1)) {
                    if (!android.support.v4.widget.DrawerLayout.d(v7_1)) {
                        throw new IllegalStateException(new StringBuilder("Child ").append(v7_1).append(" at index ").append(v4).append(" does not have a valid layout_gravity - must be Gravity.LEFT, Gravity.RIGHT or Gravity.NO_GRAVITY").toString());
                    } else {
                        if ((android.support.v4.widget.DrawerLayout.z) && (android.support.v4.view.cx.r(v7_1) != this.B)) {
                            android.support.v4.view.cx.f(v7_1, this.B);
                        }
                        int v8_10 = (this.c(v7_1) & 7);
                        if ((v8_10 & 0) == 0) {
                            v7_1.measure(android.support.v4.widget.DrawerLayout.getChildMeasureSpec(p13, ((this.C + v0_8.leftMargin) + v0_8.rightMargin), v0_8.width), android.support.v4.widget.DrawerLayout.getChildMeasureSpec(p14, (v0_8.topMargin + v0_8.bottomMargin), v0_8.height));
                        } else {
                            throw new IllegalStateException(new StringBuilder("Child drawer has absolute gravity ").append(android.support.v4.widget.DrawerLayout.d(v8_10)).append(" but this DrawerLayout already has a drawer view along that edge").toString());
                        }
                    }
                } else {
                    v7_1.measure(android.view.View$MeasureSpec.makeMeasureSpec(((v2_0 - v0_8.leftMargin) - v0_8.rightMargin), 1073741824), android.view.View$MeasureSpec.makeMeasureSpec(((v1_0 - v0_8.topMargin) - v0_8.bottomMargin), 1073741824));
                }
            }
            v4++;
        }
        return;
    }

    protected void onRestoreInstanceState(android.os.Parcelable p3)
    {
        super.onRestoreInstanceState(((android.support.v4.widget.DrawerLayout$SavedState) p3).getSuperState());
        if (((android.support.v4.widget.DrawerLayout$SavedState) p3).a != 0) {
            int v0_3 = this.a(((android.support.v4.widget.DrawerLayout$SavedState) p3).a);
            if (v0_3 != 0) {
                this.k(v0_3);
            }
        }
        this.b(((android.support.v4.widget.DrawerLayout$SavedState) p3).b, 3);
        this.b(((android.support.v4.widget.DrawerLayout$SavedState) p3).c, 5);
        return;
    }

    public void onRtlPropertiesChanged(int p1)
    {
        this.h();
        return;
    }

    protected android.os.Parcelable onSaveInstanceState()
    {
        android.support.v4.widget.DrawerLayout$SavedState v1_1 = new android.support.v4.widget.DrawerLayout$SavedState(super.onSaveInstanceState());
        int v0_1 = this.g();
        if (v0_1 != 0) {
            v1_1.a = ((android.support.v4.widget.ad) v0_1.getLayoutParams()).a;
        }
        v1_1.b = this.K;
        v1_1.c = this.L;
        return v1_1;
    }

    public boolean onTouchEvent(android.view.MotionEvent p8)
    {
        this.g.b(p8);
        this.h.b(p8);
        switch ((p8.getAction() & 255)) {
            case 0:
                int v0_12 = p8.getX();
                int v3_6 = p8.getY();
                this.N = v0_12;
                this.O = v3_6;
                this.M = 0;
                this.j = 0;
                break;
            case 1:
                int v0_10;
                int v0_4 = p8.getX();
                int v3_0 = p8.getY();
                int v4_1 = this.g.b(((int) v0_4), ((int) v3_0));
                if ((v4_1 == 0) || ((!android.support.v4.widget.DrawerLayout.j(v4_1)) || ((((v0_4 - this.N) * (v0_4 - this.N)) + ((v3_0 - this.O) * (v3_0 - this.O))) >= ((float) (this.g.n * this.g.n))))) {
                    v0_10 = 1;
                } else {
                    int v0_9 = this.g();
                    if (v0_9 == 0) {
                    } else {
                        if (this.a(v0_9) != 2) {
                            v0_10 = 0;
                        } else {
                            v0_10 = 1;
                        }
                    }
                }
                this.a(v0_10);
                this.M = 0;
            case 2:
            default:
                break;
            case 3:
                this.a(1);
                this.M = 0;
                this.j = 0;
                break;
        }
        return 1;
    }

    public void requestDisallowInterceptTouchEvent(boolean p2)
    {
        super.requestDisallowInterceptTouchEvent(p2);
        this.M = p2;
        if (p2) {
            this.a(1);
        }
        return;
    }

    public void requestLayout()
    {
        if (!this.I) {
            super.requestLayout();
        }
        return;
    }

    public void setDrawerElevation(float p4)
    {
        this.B = p4;
        int v0 = 0;
        while (v0 < this.getChildCount()) {
            android.view.View v1_1 = this.getChildAt(v0);
            if (android.support.v4.widget.DrawerLayout.d(v1_1)) {
                android.support.v4.view.cx.f(v1_1, this.B);
            }
            v0++;
        }
        return;
    }

    public void setDrawerListener(android.support.v4.widget.ac p1)
    {
        this.k = p1;
        return;
    }

    public void setDrawerLockMode(int p2)
    {
        this.b(p2, 3);
        this.b(p2, 5);
        return;
    }

    public void setScrimColor(int p1)
    {
        this.D = p1;
        this.invalidate();
        return;
    }

    public void setStatusBarBackground(int p2)
    {
        int v0_0;
        if (p2 == 0) {
            v0_0 = 0;
        } else {
            v0_0 = android.support.v4.c.h.a(this.getContext(), p2);
        }
        this.P = v0_0;
        this.invalidate();
        return;
    }

    public void setStatusBarBackground(android.graphics.drawable.Drawable p1)
    {
        this.P = p1;
        this.invalidate();
        return;
    }

    public void setStatusBarBackgroundColor(int p2)
    {
        this.P = new android.graphics.drawable.ColorDrawable(p2);
        this.invalidate();
        return;
    }
}
