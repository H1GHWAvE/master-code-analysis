package android.support.v4.widget;
final class m {
    private static final String a = "CompoundButtonCompatDonut";
    private static reflect.Field b;
    private static boolean c;

    m()
    {
        return;
    }

    static android.graphics.drawable.Drawable a(android.widget.CompoundButton p5)
    {
        if (!android.support.v4.widget.m.c) {
            try {
                IllegalAccessException v0_2 = android.widget.CompoundButton.getDeclaredField("mButtonDrawable");
                android.support.v4.widget.m.b = v0_2;
                v0_2.setAccessible(1);
            } catch (IllegalAccessException v0_3) {
                android.util.Log.i("CompoundButtonCompatDonut", "Failed to retrieve mButtonDrawable field", v0_3);
            }
            android.support.v4.widget.m.c = 1;
        }
        IllegalAccessException v0_7;
        if (android.support.v4.widget.m.b == null) {
            v0_7 = 0;
        } else {
            try {
                v0_7 = ((android.graphics.drawable.Drawable) android.support.v4.widget.m.b.get(p5));
            } catch (IllegalAccessException v0_8) {
                android.util.Log.i("CompoundButtonCompatDonut", "Failed to get button drawable via reflection", v0_8);
                android.support.v4.widget.m.b = 0;
            }
        }
        return v0_7;
    }

    private static void a(android.widget.CompoundButton p1, android.content.res.ColorStateList p2)
    {
        if ((p1 instanceof android.support.v4.widget.ef)) {
            ((android.support.v4.widget.ef) p1).setSupportButtonTintList(p2);
        }
        return;
    }

    private static void a(android.widget.CompoundButton p1, android.graphics.PorterDuff$Mode p2)
    {
        if ((p1 instanceof android.support.v4.widget.ef)) {
            ((android.support.v4.widget.ef) p1).setSupportButtonTintMode(p2);
        }
        return;
    }

    private static android.content.res.ColorStateList b(android.widget.CompoundButton p1)
    {
        int v0_1;
        if (!(p1 instanceof android.support.v4.widget.ef)) {
            v0_1 = 0;
        } else {
            v0_1 = ((android.support.v4.widget.ef) p1).getSupportButtonTintList();
        }
        return v0_1;
    }

    private static android.graphics.PorterDuff$Mode c(android.widget.CompoundButton p1)
    {
        int v0_1;
        if (!(p1 instanceof android.support.v4.widget.ef)) {
            v0_1 = 0;
        } else {
            v0_1 = ((android.support.v4.widget.ef) p1).getSupportButtonTintMode();
        }
        return v0_1;
    }
}
