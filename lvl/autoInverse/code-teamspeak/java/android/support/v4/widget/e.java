package android.support.v4.widget;
final class e extends android.widget.ImageView {
    private static final int b = 503316480;
    private static final int c = 1023410176;
    private static final float d = 0;
    private static final float e = 16352;
    private static final float f = 16480;
    private static final int g = 4;
    android.view.animation.Animation$AnimationListener a;
    private int h;

    public e(android.content.Context p7)
    {
        android.graphics.drawable.ShapeDrawable v0_8;
        this(p7);
        android.graphics.Paint v1_0 = this.getContext().getResources().getDisplayMetrics().density;
        android.graphics.drawable.ShapeDrawable v0_6 = ((int) ((1101004800 * v1_0) * 1073741824));
        int v2_3 = ((int) (1071644672 * v1_0));
        float v3_2 = ((int) (0 * v1_0));
        this.h = ((int) (1080033280 * v1_0));
        if (!android.support.v4.widget.e.a()) {
            v0_8 = new android.graphics.drawable.ShapeDrawable(new android.support.v4.widget.f(this, this.h, v0_6));
            android.support.v4.view.cx.a(this, 1, v0_8.getPaint());
            v0_8.getPaint().setShadowLayer(((float) this.h), ((float) v3_2), ((float) v2_3), 503316480);
            this.setPadding(this.h, this.h, this.h, this.h);
        } else {
            v0_8 = new android.graphics.drawable.ShapeDrawable(new android.graphics.drawable.shapes.OvalShape());
            android.support.v4.view.cx.f(this, (v1_0 * 1082130432));
        }
        v0_8.getPaint().setColor(-328966);
        this.setBackgroundDrawable(v0_8);
        return;
    }

    static synthetic int a(android.support.v4.widget.e p1)
    {
        return p1.h;
    }

    static synthetic int a(android.support.v4.widget.e p0, int p1)
    {
        p0.h = p1;
        return p1;
    }

    private void a(int p2)
    {
        this.setBackgroundColor(this.getContext().getResources().getColor(p2));
        return;
    }

    private void a(android.view.animation.Animation$AnimationListener p1)
    {
        this.a = p1;
        return;
    }

    private static boolean a()
    {
        int v0_1;
        if (android.os.Build$VERSION.SDK_INT < 21) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final void onAnimationEnd()
    {
        super.onAnimationEnd();
        if (this.a != null) {
            this.a.onAnimationEnd(this.getAnimation());
        }
        return;
    }

    public final void onAnimationStart()
    {
        super.onAnimationStart();
        if (this.a != null) {
            this.a.onAnimationStart(this.getAnimation());
        }
        return;
    }

    protected final void onMeasure(int p4, int p5)
    {
        super.onMeasure(p4, p5);
        if (!android.support.v4.widget.e.a()) {
            this.setMeasuredDimension((this.getMeasuredWidth() + (this.h * 2)), (this.getMeasuredHeight() + (this.h * 2)));
        }
        return;
    }

    public final void setBackgroundColor(int p2)
    {
        if ((this.getBackground() instanceof android.graphics.drawable.ShapeDrawable)) {
            ((android.graphics.drawable.ShapeDrawable) this.getBackground()).getPaint().setColor(p2);
        }
        return;
    }
}
