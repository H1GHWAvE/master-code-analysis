package android.support.v4.widget;
public final class o extends android.widget.ProgressBar {
    private static final int a = 500;
    private static final int b = 500;
    private long c;
    private boolean d;
    private boolean e;
    private boolean f;
    private final Runnable g;
    private final Runnable h;

    private o(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    private o(android.content.Context p4, byte p5)
    {
        this(p4, 0, 0);
        this.c = -1;
        this.d = 0;
        this.e = 0;
        this.f = 0;
        this.g = new android.support.v4.widget.p(this);
        this.h = new android.support.v4.widget.q(this);
        return;
    }

    static synthetic long a(android.support.v4.widget.o p1, long p2)
    {
        p1.c = p2;
        return p2;
    }

    private void a()
    {
        this.removeCallbacks(this.g);
        this.removeCallbacks(this.h);
        return;
    }

    static synthetic boolean a(android.support.v4.widget.o p1)
    {
        p1.d = 0;
        return 0;
    }

    private void b()
    {
        this.f = 1;
        this.removeCallbacks(this.h);
        long v0_2 = (System.currentTimeMillis() - this.c);
        if ((v0_2 < 500) && (this.c != -1)) {
            if (!this.d) {
                this.postDelayed(this.g, (500 - v0_2));
                this.d = 1;
            }
        } else {
            this.setVisibility(8);
        }
        return;
    }

    static synthetic boolean b(android.support.v4.widget.o p1)
    {
        p1.e = 0;
        return 0;
    }

    private void c()
    {
        this.c = -1;
        this.f = 0;
        this.removeCallbacks(this.g);
        if (!this.e) {
            this.postDelayed(this.h, 500);
            this.e = 1;
        }
        return;
    }

    static synthetic boolean c(android.support.v4.widget.o p1)
    {
        return p1.f;
    }

    public final void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        this.a();
        return;
    }

    public final void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        this.a();
        return;
    }
}
