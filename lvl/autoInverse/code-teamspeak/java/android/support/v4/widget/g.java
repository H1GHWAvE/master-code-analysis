package android.support.v4.widget;
public final class g {
    private static final android.support.v4.widget.j a;

    static g()
    {
        android.support.v4.widget.i v0_0 = android.os.Build$VERSION.SDK_INT;
        if (v0_0 < 23) {
            if (v0_0 < 21) {
                android.support.v4.widget.g.a = new android.support.v4.widget.i();
            } else {
                android.support.v4.widget.g.a = new android.support.v4.widget.k();
            }
        } else {
            android.support.v4.widget.g.a = new android.support.v4.widget.h();
        }
        return;
    }

    private g()
    {
        return;
    }

    public static android.graphics.drawable.Drawable a(android.widget.CompoundButton p1)
    {
        return android.support.v4.widget.g.a.a(p1);
    }

    public static void a(android.widget.CompoundButton p1, android.content.res.ColorStateList p2)
    {
        android.support.v4.widget.g.a.a(p1, p2);
        return;
    }

    public static void a(android.widget.CompoundButton p1, android.graphics.PorterDuff$Mode p2)
    {
        android.support.v4.widget.g.a.a(p1, p2);
        return;
    }

    private static android.content.res.ColorStateList b(android.widget.CompoundButton p1)
    {
        return android.support.v4.widget.g.a.b(p1);
    }

    private static android.graphics.PorterDuff$Mode c(android.widget.CompoundButton p1)
    {
        return android.support.v4.widget.g.a.c(p1);
    }
}
