package android.support.v4.e.a;
final class p {

    p()
    {
        return;
    }

    private static android.graphics.drawable.Drawable a(android.graphics.drawable.Drawable p1)
    {
        if (((p1 instanceof android.graphics.drawable.GradientDrawable)) || ((p1 instanceof android.graphics.drawable.DrawableContainer))) {
            p1 = new android.support.v4.e.a.u(p1);
        }
        return p1;
    }

    private static void a(android.graphics.drawable.Drawable p0, float p1, float p2)
    {
        p0.setHotspot(p1, p2);
        return;
    }

    private static void a(android.graphics.drawable.Drawable p1, int p2)
    {
        if (!(p1 instanceof android.support.v4.e.a.u)) {
            p1.setTint(p2);
        } else {
            android.support.v4.e.a.l.a(p1, p2);
        }
        return;
    }

    private static void a(android.graphics.drawable.Drawable p0, int p1, int p2, int p3, int p4)
    {
        p0.setHotspotBounds(p1, p2, p3, p4);
        return;
    }

    private static void a(android.graphics.drawable.Drawable p1, android.content.res.ColorStateList p2)
    {
        if (!(p1 instanceof android.support.v4.e.a.u)) {
            p1.setTintList(p2);
        } else {
            android.support.v4.e.a.l.a(p1, p2);
        }
        return;
    }

    private static void a(android.graphics.drawable.Drawable p1, android.graphics.PorterDuff$Mode p2)
    {
        if (!(p1 instanceof android.support.v4.e.a.u)) {
            p1.setTintMode(p2);
        } else {
            android.support.v4.e.a.l.a(p1, p2);
        }
        return;
    }
}
