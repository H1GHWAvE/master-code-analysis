package android.support.v4.e;
public final class a {
    static final android.support.v4.e.c a;

    static a()
    {
        android.support.v4.e.b v0_0 = android.os.Build$VERSION.SDK_INT;
        if (v0_0 < 19) {
            if (v0_0 < 18) {
                if (v0_0 < 12) {
                    android.support.v4.e.a.a = new android.support.v4.e.b();
                } else {
                    android.support.v4.e.a.a = new android.support.v4.e.d();
                }
            } else {
                android.support.v4.e.a.a = new android.support.v4.e.e();
            }
        } else {
            android.support.v4.e.a.a = new android.support.v4.e.f();
        }
        return;
    }

    public a()
    {
        return;
    }

    public static void a(android.graphics.Bitmap p1, boolean p2)
    {
        android.support.v4.e.a.a.a(p1, p2);
        return;
    }

    public static boolean a(android.graphics.Bitmap p1)
    {
        return android.support.v4.e.a.a.a(p1);
    }

    private static int b(android.graphics.Bitmap p1)
    {
        return android.support.v4.e.a.a.b(p1);
    }
}
