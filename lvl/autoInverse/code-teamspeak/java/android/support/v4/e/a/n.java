package android.support.v4.e.a;
final class n {
    static reflect.Method a = None;
    static boolean b = False;
    private static final String c = "DrawableCompatJellybeanMr1";
    private static reflect.Method d;
    private static boolean e;

    n()
    {
        return;
    }

    public static int a(android.graphics.drawable.Drawable p4)
    {
        if (!android.support.v4.e.a.n.e) {
            try {
                String v2_1 = new Class[0];
                int v0_2 = android.graphics.drawable.Drawable.getDeclaredMethod("getLayoutDirection", v2_1);
                android.support.v4.e.a.n.d = v0_2;
                v0_2.setAccessible(1);
            } catch (int v0_3) {
                android.util.Log.i("DrawableCompatJellybeanMr1", "Failed to retrieve getLayoutDirection() method", v0_3);
            }
            android.support.v4.e.a.n.e = 1;
        }
        int v0_8;
        if (android.support.v4.e.a.n.d == null) {
            v0_8 = -1;
        } else {
            try {
                String v1_4 = new Object[0];
                v0_8 = ((Integer) android.support.v4.e.a.n.d.invoke(p4, v1_4)).intValue();
            } catch (int v0_9) {
                android.util.Log.i("DrawableCompatJellybeanMr1", "Failed to invoke getLayoutDirection() via reflection", v0_9);
                android.support.v4.e.a.n.d = 0;
            }
        }
        return v0_8;
    }

    private static void a(android.graphics.drawable.Drawable p6, int p7)
    {
        if (!android.support.v4.e.a.n.b) {
            try {
                String v2_1 = new Class[1];
                v2_1[0] = Integer.TYPE;
                int v0_2 = android.graphics.drawable.Drawable.getDeclaredMethod("setLayoutDirection", v2_1);
                android.support.v4.e.a.n.a = v0_2;
                v0_2.setAccessible(1);
            } catch (int v0_3) {
                android.util.Log.i("DrawableCompatJellybeanMr1", "Failed to retrieve setLayoutDirection(int) method", v0_3);
            }
            android.support.v4.e.a.n.b = 1;
        }
        if (android.support.v4.e.a.n.a != null) {
            try {
                String v1_4 = new Object[1];
                v1_4[0] = Integer.valueOf(p7);
                android.support.v4.e.a.n.a.invoke(p6, v1_4);
            } catch (int v0_6) {
                android.util.Log.i("DrawableCompatJellybeanMr1", "Failed to invoke setLayoutDirection(int) via reflection", v0_6);
                android.support.v4.e.a.n.a = 0;
            }
        }
        return;
    }
}
