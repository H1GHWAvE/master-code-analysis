package android.support.v4.f.a;
final class c extends android.support.v4.f.a.a {
    private final android.view.WindowManager b;

    public c(android.content.Context p2)
    {
        this.b = ((android.view.WindowManager) p2.getSystemService("window"));
        return;
    }

    public final android.view.Display a(int p3)
    {
        int v0_1 = this.b.getDefaultDisplay();
        if (v0_1.getDisplayId() != p3) {
            v0_1 = 0;
        }
        return v0_1;
    }

    public final android.view.Display[] a()
    {
        android.view.Display[] v0_1 = new android.view.Display[1];
        v0_1[0] = this.b.getDefaultDisplay();
        return v0_1;
    }

    public final android.view.Display[] a(String p4)
    {
        android.view.Display[] v0_0;
        if (p4 != null) {
            v0_0 = new android.view.Display[0];
        } else {
            v0_0 = new android.view.Display[1];
            v0_0[0] = this.b.getDefaultDisplay();
        }
        return v0_0;
    }
}
