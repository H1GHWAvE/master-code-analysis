package android.support.v4.view;
public class a {
    private static final android.support.v4.view.d a;
    private static final Object c;
    final Object b;

    static a()
    {
        if (android.os.Build$VERSION.SDK_INT < 16) {
            if (android.os.Build$VERSION.SDK_INT < 14) {
                android.support.v4.view.a.a = new android.support.v4.view.g();
            } else {
                android.support.v4.view.a.a = new android.support.v4.view.b();
            }
        } else {
            android.support.v4.view.a.a = new android.support.v4.view.e();
        }
        android.support.v4.view.a.c = android.support.v4.view.a.a.a();
        return;
    }

    public a()
    {
        this.b = android.support.v4.view.a.a.a(this);
        return;
    }

    private Object a()
    {
        return this.b;
    }

    public static void a(android.view.View p2, int p3)
    {
        android.support.v4.view.a.a.a(android.support.v4.view.a.c, p2, p3);
        return;
    }

    public static void c(android.view.View p2, android.view.accessibility.AccessibilityEvent p3)
    {
        android.support.v4.view.a.a.d(android.support.v4.view.a.c, p2, p3);
        return;
    }

    public android.support.v4.view.a.aq a(android.view.View p3)
    {
        return android.support.v4.view.a.a.a(android.support.v4.view.a.c, p3);
    }

    public void a(android.view.View p3, android.support.v4.view.a.q p4)
    {
        android.support.v4.view.a.a.a(android.support.v4.view.a.c, p3, p4);
        return;
    }

    public void a(android.view.View p3, android.view.accessibility.AccessibilityEvent p4)
    {
        android.support.v4.view.a.a.b(android.support.v4.view.a.c, p3, p4);
        return;
    }

    public boolean a(android.view.View p3, int p4, android.os.Bundle p5)
    {
        return android.support.v4.view.a.a.a(android.support.v4.view.a.c, p3, p4, p5);
    }

    public boolean a(android.view.ViewGroup p3, android.view.View p4, android.view.accessibility.AccessibilityEvent p5)
    {
        return android.support.v4.view.a.a.a(android.support.v4.view.a.c, p3, p4, p5);
    }

    public void b(android.view.View p3, android.view.accessibility.AccessibilityEvent p4)
    {
        android.support.v4.view.a.a.c(android.support.v4.view.a.c, p3, p4);
        return;
    }

    public boolean d(android.view.View p3, android.view.accessibility.AccessibilityEvent p4)
    {
        return android.support.v4.view.a.a.a(android.support.v4.view.a.c, p3, p4);
    }
}
