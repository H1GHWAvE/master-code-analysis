package android.support.v4.view;
final class dj {
    private static final String a = "ViewCompatBase";
    private static reflect.Field b;
    private static boolean c;
    private static reflect.Field d;
    private static boolean e;

    dj()
    {
        return;
    }

    static int a(android.view.View p3)
    {
        if (!android.support.v4.view.dj.c) {
            try {
                Exception v0_2 = android.view.View.getDeclaredField("mMinWidth");
                android.support.v4.view.dj.b = v0_2;
                v0_2.setAccessible(1);
            } catch (Exception v0) {
            }
            android.support.v4.view.dj.c = 1;
        }
        Exception v0_7;
        if (android.support.v4.view.dj.b == null) {
            v0_7 = 0;
        } else {
            try {
                v0_7 = ((Integer) android.support.v4.view.dj.b.get(p3)).intValue();
            } catch (Exception v0) {
            }
        }
        return v0_7;
    }

    private static void a(android.view.View p1, android.content.res.ColorStateList p2)
    {
        if ((p1 instanceof android.support.v4.view.cr)) {
            ((android.support.v4.view.cr) p1).setSupportBackgroundTintList(p2);
        }
        return;
    }

    private static void a(android.view.View p1, android.graphics.PorterDuff$Mode p2)
    {
        if ((p1 instanceof android.support.v4.view.cr)) {
            ((android.support.v4.view.cr) p1).setSupportBackgroundTintMode(p2);
        }
        return;
    }

    static int b(android.view.View p3)
    {
        if (!android.support.v4.view.dj.e) {
            try {
                Exception v0_2 = android.view.View.getDeclaredField("mMinHeight");
                android.support.v4.view.dj.d = v0_2;
                v0_2.setAccessible(1);
            } catch (Exception v0) {
            }
            android.support.v4.view.dj.e = 1;
        }
        Exception v0_7;
        if (android.support.v4.view.dj.d == null) {
            v0_7 = 0;
        } else {
            try {
                v0_7 = ((Integer) android.support.v4.view.dj.d.get(p3)).intValue();
            } catch (Exception v0) {
            }
        }
        return v0_7;
    }

    private static android.content.res.ColorStateList c(android.view.View p1)
    {
        int v0_1;
        if (!(p1 instanceof android.support.v4.view.cr)) {
            v0_1 = 0;
        } else {
            v0_1 = ((android.support.v4.view.cr) p1).getSupportBackgroundTintList();
        }
        return v0_1;
    }

    private static android.graphics.PorterDuff$Mode d(android.view.View p1)
    {
        int v0_1;
        if (!(p1 instanceof android.support.v4.view.cr)) {
            v0_1 = 0;
        } else {
            v0_1 = ((android.support.v4.view.cr) p1).getSupportBackgroundTintMode();
        }
        return v0_1;
    }

    private static boolean e(android.view.View p1)
    {
        if ((p1.getWidth() <= 0) || (p1.getHeight() <= 0)) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private static boolean f(android.view.View p1)
    {
        int v0_1;
        if (p1.getWindowToken() == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }
}
