package android.support.v4.view;
public final class az {
    public static final int a = 0;
    public static final int b = 1;
    public static final int c = 2;
    public static final int d = 4;
    public static final int e = 8;
    static final android.support.v4.view.be f = None;
    private static final String g = "MenuItemCompat";

    static az()
    {
        android.support.v4.view.ba v0_0 = android.os.Build$VERSION.SDK_INT;
        if (v0_0 < 14) {
            if (v0_0 < 11) {
                android.support.v4.view.az.f = new android.support.v4.view.ba();
            } else {
                android.support.v4.view.az.f = new android.support.v4.view.bb();
            }
        } else {
            android.support.v4.view.az.f = new android.support.v4.view.bc();
        }
        return;
    }

    public az()
    {
        return;
    }

    private static android.view.MenuItem a(android.view.MenuItem p1, android.support.v4.view.bf p2)
    {
        android.view.MenuItem v0_2;
        if (!(p1 instanceof android.support.v4.g.a.b)) {
            v0_2 = android.support.v4.view.az.f.a(p1, p2);
        } else {
            v0_2 = ((android.support.v4.g.a.b) p1).a(p2);
        }
        return v0_2;
    }

    public static android.view.MenuItem a(android.view.MenuItem p2, android.support.v4.view.n p3)
    {
        if (!(p2 instanceof android.support.v4.g.a.b)) {
            android.util.Log.w("MenuItemCompat", "setActionProvider: item does not implement SupportMenuItem; ignoring");
        } else {
            p2 = ((android.support.v4.g.a.b) p2).a(p3);
        }
        return p2;
    }

    public static android.view.MenuItem a(android.view.MenuItem p1, android.view.View p2)
    {
        android.view.MenuItem v0_2;
        if (!(p1 instanceof android.support.v4.g.a.b)) {
            v0_2 = android.support.v4.view.az.f.a(p1, p2);
        } else {
            v0_2 = ((android.support.v4.g.a.b) p1).setActionView(p2);
        }
        return v0_2;
    }

    public static android.view.View a(android.view.MenuItem p1)
    {
        android.view.View v0_2;
        if (!(p1 instanceof android.support.v4.g.a.b)) {
            v0_2 = android.support.v4.view.az.f.a(p1);
        } else {
            v0_2 = ((android.support.v4.g.a.b) p1).getActionView();
        }
        return v0_2;
    }

    public static void a(android.view.MenuItem p1, int p2)
    {
        if (!(p1 instanceof android.support.v4.g.a.b)) {
            android.support.v4.view.az.f.a(p1, p2);
        } else {
            ((android.support.v4.g.a.b) p1).setShowAsAction(p2);
        }
        return;
    }

    public static android.view.MenuItem b(android.view.MenuItem p1, int p2)
    {
        android.view.MenuItem v0_2;
        if (!(p1 instanceof android.support.v4.g.a.b)) {
            v0_2 = android.support.v4.view.az.f.b(p1, p2);
        } else {
            v0_2 = ((android.support.v4.g.a.b) p1).setActionView(p2);
        }
        return v0_2;
    }

    public static boolean b(android.view.MenuItem p1)
    {
        boolean v0_2;
        if (!(p1 instanceof android.support.v4.g.a.b)) {
            v0_2 = android.support.v4.view.az.f.b(p1);
        } else {
            v0_2 = ((android.support.v4.g.a.b) p1).expandActionView();
        }
        return v0_2;
    }

    public static boolean c(android.view.MenuItem p1)
    {
        boolean v0_2;
        if (!(p1 instanceof android.support.v4.g.a.b)) {
            v0_2 = android.support.v4.view.az.f.d(p1);
        } else {
            v0_2 = ((android.support.v4.g.a.b) p1).isActionViewExpanded();
        }
        return v0_2;
    }

    private static android.support.v4.view.n d(android.view.MenuItem p2)
    {
        int v0_2;
        if (!(p2 instanceof android.support.v4.g.a.b)) {
            android.util.Log.w("MenuItemCompat", "getActionProvider: item does not implement SupportMenuItem; returning null");
            v0_2 = 0;
        } else {
            v0_2 = ((android.support.v4.g.a.b) p2).a();
        }
        return v0_2;
    }

    private static boolean e(android.view.MenuItem p1)
    {
        boolean v0_2;
        if (!(p1 instanceof android.support.v4.g.a.b)) {
            v0_2 = android.support.v4.view.az.f.c(p1);
        } else {
            v0_2 = ((android.support.v4.g.a.b) p1).collapseActionView();
        }
        return v0_2;
    }
}
