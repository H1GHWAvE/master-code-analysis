package android.support.v4.view;
public class ViewPager extends android.view.ViewGroup {
    private static final int W = 255;
    private static final int af = 2;
    private static final int at = 0;
    private static final int au = 1;
    private static final int av = 2;
    private static final android.support.v4.view.fa ay = None;
    public static final int b = 0;
    public static final int c = 1;
    public static final int d = 2;
    private static final String e = "ViewPager";
    private static final boolean f = False;
    private static final boolean g = False;
    private static final int h = 1;
    private static final int i = 600;
    private static final int j = 25;
    private static final int k = 16;
    private static final int l = 400;
    private static final int[] m;
    private static final java.util.Comparator o;
    private static final android.view.animation.Interpolator p;
    private int A;
    private android.graphics.drawable.Drawable B;
    private int C;
    private int D;
    private float E;
    private float F;
    private int G;
    private int H;
    private boolean I;
    private boolean J;
    private boolean K;
    private int L;
    private boolean M;
    private boolean N;
    private int O;
    private int P;
    private int Q;
    private float R;
    private float S;
    private float T;
    private float U;
    private int V;
    public java.util.List a;
    private int aA;
    private android.view.VelocityTracker aa;
    private int ab;
    private int ac;
    private int ad;
    private int ae;
    private boolean ag;
    private long ah;
    private android.support.v4.widget.al ai;
    private android.support.v4.widget.al aj;
    private boolean ak;
    private boolean al;
    private boolean am;
    private int an;
    private android.support.v4.view.ev ao;
    private android.support.v4.view.ev ap;
    private android.support.v4.view.eu aq;
    private android.support.v4.view.ew ar;
    private reflect.Method as;
    private int aw;
    private java.util.ArrayList ax;
    private final Runnable az;
    private int n;
    private final java.util.ArrayList q;
    private final android.support.v4.view.er r;
    private final android.graphics.Rect s;
    private android.support.v4.view.by t;
    private int u;
    private int v;
    private android.os.Parcelable w;
    private ClassLoader x;
    private android.widget.Scroller y;
    private android.support.v4.view.ex z;

    static ViewPager()
    {
        android.support.v4.view.fa v0_1 = new int[1];
        v0_1[0] = 16842931;
        android.support.v4.view.ViewPager.m = v0_1;
        android.support.v4.view.ViewPager.o = new android.support.v4.view.en();
        android.support.v4.view.ViewPager.p = new android.support.v4.view.eo();
        android.support.v4.view.ViewPager.ay = new android.support.v4.view.fa();
        return;
    }

    private ViewPager(android.content.Context p6)
    {
        this(p6);
        this.q = new java.util.ArrayList();
        this.r = new android.support.v4.view.er();
        this.s = new android.graphics.Rect();
        this.v = -1;
        this.w = 0;
        this.x = 0;
        this.E = -8388609;
        this.F = 2139095039;
        this.L = 1;
        this.V = -1;
        this.ak = 1;
        this.al = 0;
        this.az = new android.support.v4.view.ep(this);
        this.aA = 0;
        this.d();
        return;
    }

    private ViewPager(android.content.Context p6, android.util.AttributeSet p7)
    {
        this(p6, p7);
        this.q = new java.util.ArrayList();
        this.r = new android.support.v4.view.er();
        this.s = new android.graphics.Rect();
        this.v = -1;
        this.w = 0;
        this.x = 0;
        this.E = -8388609;
        this.F = 2139095039;
        this.L = 1;
        this.V = -1;
        this.ak = 1;
        this.al = 0;
        this.az = new android.support.v4.view.ep(this);
        this.aA = 0;
        this.d();
        return;
    }

    private static float a(float p4)
    {
        return ((float) Math.sin(((double) ((float) (((double) (p4 - 1056964608)) * 0.4712389167638204)))));
    }

    private int a(int p4, float p5, int p6, int p7)
    {
        if ((Math.abs(p7) <= this.ad) || (Math.abs(p6) <= this.ab)) {
            int v0_3;
            if (p4 < this.u) {
                v0_3 = 1058642330;
            } else {
                v0_3 = 1053609165;
            }
            p4 = ((int) (v0_3 + (((float) p4) + p5)));
        } else {
            if (p6 <= 0) {
                p4++;
            }
        }
        if (this.q.size() > 0) {
            p4 = Math.max(((android.support.v4.view.er) this.q.get(0)).b, Math.min(p4, ((android.support.v4.view.er) this.q.get((this.q.size() - 1))).b));
        }
        return p4;
    }

    private android.graphics.Rect a(android.graphics.Rect p5, android.view.View p6)
    {
        android.graphics.Rect v1_0;
        if (p5 != null) {
            v1_0 = p5;
        } else {
            v1_0 = new android.graphics.Rect();
        }
        android.view.ViewParent v0_6;
        if (p6 != null) {
            v1_0.left = p6.getLeft();
            v1_0.right = p6.getRight();
            v1_0.top = p6.getTop();
            v1_0.bottom = p6.getBottom();
            android.view.ViewParent v0_5 = p6.getParent();
            while (((v0_5 instanceof android.view.ViewGroup)) && (v0_5 != this)) {
                android.view.ViewParent v0_7 = ((android.view.ViewGroup) v0_5);
                v1_0.left = (v1_0.left + v0_7.getLeft());
                v1_0.right = (v1_0.right + v0_7.getRight());
                v1_0.top = (v1_0.top + v0_7.getTop());
                v1_0.bottom = (v1_0.bottom + v0_7.getBottom());
                v0_5 = v0_7.getParent();
            }
            v0_6 = v1_0;
        } else {
            v1_0.set(0, 0, 0, 0);
            v0_6 = v1_0;
        }
        return v0_6;
    }

    private android.support.v4.view.er a(android.view.View p5)
    {
        int v1 = 0;
        while (v1 < this.q.size()) {
            int v0_3 = ((android.support.v4.view.er) this.q.get(v1));
            if (!this.t.a(p5, v0_3.a)) {
                v1++;
            }
            return v0_3;
        }
        v0_3 = 0;
        return v0_3;
    }

    private void a(int p19)
    {
        boolean v4_1;
        IllegalStateException v3_1;
        if (this.u == p19) {
            v4_1 = 0;
            v3_1 = 2;
        } else {
            int v2_2;
            if (this.u >= p19) {
                v2_2 = 17;
            } else {
                v2_2 = 66;
            }
            IllegalStateException v3_3 = this.b(this.u);
            this.u = p19;
            v4_1 = v3_3;
            v3_1 = v2_2;
        }
        if (this.t != null) {
            if (!this.K) {
                if (this.getWindowToken() != null) {
                    int v2_6 = this.L;
                    android.support.v4.view.by v11_0 = Math.max(0, (this.u - v2_6));
                    int v12 = this.t.e();
                    int v13 = Math.min((v12 - 1), (v2_6 + this.u));
                    if (v12 == this.n) {
                        int v5_3 = 0;
                        while (v5_3 < this.q.size()) {
                            int v2_14 = ((android.support.v4.view.er) this.q.get(v5_3));
                            if (v2_14.b < this.u) {
                                v5_3++;
                            } else {
                                if (v2_14.b != this.u) {
                                    break;
                                }
                            }
                            if ((v2_14 != 0) || (v12 <= 0)) {
                                android.support.v4.view.er v10 = v2_14;
                            } else {
                                v10 = this.b(this.u, v5_3);
                            }
                            if (v10 != null) {
                                int v2_18;
                                int v8_2 = (v5_3 - 1);
                                if (v8_2 < 0) {
                                    v2_18 = 0;
                                } else {
                                    v2_18 = ((android.support.v4.view.er) this.q.get(v8_2));
                                }
                                int v6_6;
                                int v14 = this.getClientWidth();
                                if (v14 > 0) {
                                    v6_6 = ((1073741824 - v10.d) + (((float) this.getPaddingLeft()) / ((float) v14)));
                                } else {
                                    v6_6 = 0;
                                }
                                int v7_8 = 0;
                                int v9_1 = (this.u - 1);
                                int v8_3 = v5_3;
                                int v5_4 = v8_2;
                                while (v9_1 >= 0) {
                                    if ((v7_8 < v6_6) || (v9_1 >= v11_0)) {
                                        if ((v2_18 == 0) || (v9_1 != v2_18.b)) {
                                            v7_8 += this.b(v9_1, (v5_4 + 1)).d;
                                            v8_3++;
                                            if (v5_4 < 0) {
                                                v2_18 = 0;
                                            } else {
                                                v2_18 = ((android.support.v4.view.er) this.q.get(v5_4));
                                            }
                                        } else {
                                            v7_8 += v2_18.d;
                                            v5_4--;
                                            if (v5_4 < 0) {
                                                v2_18 = 0;
                                            } else {
                                                v2_18 = ((android.support.v4.view.er) this.q.get(v5_4));
                                            }
                                        }
                                    } else {
                                        if (v2_18 == 0) {
                                            break;
                                        }
                                        if ((v9_1 == v2_18.b) && (!v2_18.c)) {
                                            this.q.remove(v5_4);
                                            this.t.a(v9_1, v2_18.a);
                                            v5_4--;
                                            v8_3--;
                                            if (v5_4 < 0) {
                                                v2_18 = 0;
                                            } else {
                                                v2_18 = ((android.support.v4.view.er) this.q.get(v5_4));
                                            }
                                        }
                                    }
                                    v9_1--;
                                }
                                int v6_7 = v10.d;
                                int v9_2 = (v8_3 + 1);
                                if (v6_7 < 1073741824) {
                                    int v7_9;
                                    if (v9_2 >= this.q.size()) {
                                        v7_9 = 0;
                                    } else {
                                        v7_9 = ((android.support.v4.view.er) this.q.get(v9_2));
                                    }
                                    int v5_7;
                                    if (v14 > 0) {
                                        v5_7 = ((((float) this.getPaddingRight()) / ((float) v14)) + 1073741824);
                                    } else {
                                        v5_7 = 0;
                                    }
                                    int v7_10 = v9_2;
                                    int v9_3 = (this.u + 1);
                                    int v2_43 = v7_9;
                                    while (v9_3 < v12) {
                                        if ((v6_7 < v5_7) || (v9_3 <= v13)) {
                                            if ((v2_43 == 0) || (v9_3 != v2_43.b)) {
                                                int v2_48;
                                                int v2_44 = this.b(v9_3, v7_10);
                                                v7_10++;
                                                if (v7_10 >= this.q.size()) {
                                                    v2_48 = 0;
                                                } else {
                                                    v2_48 = ((android.support.v4.view.er) this.q.get(v7_10));
                                                }
                                                int v6_9 = v2_48;
                                                int v2_51 = (v6_7 + v2_44.d);
                                            } else {
                                                int v2_55;
                                                v7_10++;
                                                if (v7_10 >= this.q.size()) {
                                                    v2_55 = 0;
                                                } else {
                                                    v2_55 = ((android.support.v4.view.er) this.q.get(v7_10));
                                                }
                                                v6_9 = v2_55;
                                                v2_51 = (v6_7 + v2_43.d);
                                            }
                                        } else {
                                            if (v2_43 == 0) {
                                                break;
                                            }
                                            if ((v9_3 != v2_43.b) || (v2_43.c)) {
                                                v6_9 = v2_43;
                                                v2_51 = v6_7;
                                            } else {
                                                int v2_72;
                                                this.q.remove(v7_10);
                                                this.t.a(v9_3, v2_43.a);
                                                if (v7_10 >= this.q.size()) {
                                                    v2_72 = 0;
                                                } else {
                                                    v2_72 = ((android.support.v4.view.er) this.q.get(v7_10));
                                                }
                                                v6_9 = v2_72;
                                                v2_51 = v6_7;
                                            }
                                        }
                                        v9_3++;
                                        v2_43 = v6_9;
                                        v6_7 = v2_51;
                                    }
                                }
                                this.a(v10, v8_3, v4_1);
                            }
                            int v2_58;
                            if (v10 == null) {
                                v2_58 = 0;
                            } else {
                                v2_58 = v10.a;
                            }
                            this.t.a(v2_58);
                            this.t.c();
                            int v5_8 = this.getChildCount();
                            boolean v4_3 = 0;
                            while (v4_3 < v5_8) {
                                int v6_12 = this.getChildAt(v4_3);
                                int v2_67 = ((android.support.v4.view.es) v6_12.getLayoutParams());
                                v2_67.f = v4_3;
                                if ((!v2_67.a) && (v2_67.c == 0)) {
                                    int v6_13 = this.a(v6_12);
                                    if (v6_13 != 0) {
                                        v2_67.c = v6_13.d;
                                        v2_67.e = v6_13.b;
                                    }
                                }
                                v4_3++;
                            }
                            void v18_1 = this.g();
                            if (v18_1.hasFocus()) {
                                int v2_63;
                                int v2_62 = v18_1.findFocus();
                                if (v2_62 == 0) {
                                    v2_63 = 0;
                                } else {
                                    v2_63 = v18_1.b(v2_62);
                                }
                                if ((v2_63 == 0) || (v2_63.b != v18_1.u)) {
                                    int v2_65 = 0;
                                    while (v2_65 < v18_1.getChildCount()) {
                                        boolean v4_6 = v18_1.getChildAt(v2_65);
                                        int v5_9 = v18_1.a(v4_6);
                                        if ((v5_9 != 0) && ((v5_9.b == v18_1.u) && (v4_6.requestFocus(v3_1)))) {
                                            break;
                                        }
                                        v2_65++;
                                    }
                                }
                            }
                        }
                        v2_14 = 0;
                    } else {
                        try {
                            int v2_79 = this.getResources().getResourceName(this.getId());
                        } catch (int v2) {
                            v2_79 = Integer.toHexString(this.getId());
                        }
                        throw new IllegalStateException(new StringBuilder("The application\'s PagerAdapter changed the adapter\'s contents without calling PagerAdapter#notifyDataSetChanged! Expected adapter item count: ").append(this.n).append(", found: ").append(v12).append(" Pager id: ").append(v2_79).append(" Pager class: ").append(this.getClass()).append(" Problematic adapter: ").append(this.t.getClass()).toString());
                    }
                }
            } else {
                this.g();
            }
        } else {
            this.g();
        }
        return;
    }

    private void a(int p12, float p13)
    {
        if (this.an > 0) {
            int v5 = this.getScrollX();
            int v1_0 = this.getPaddingLeft();
            int v2_0 = this.getPaddingRight();
            int v6 = this.getWidth();
            int v7 = this.getChildCount();
            int v4 = 0;
            while (v4 < v7) {
                int v2_3;
                int v1_3;
                android.view.View v8 = this.getChildAt(v4);
                int v0_18 = ((android.support.v4.view.es) v8.getLayoutParams());
                if (!v0_18.a) {
                    v2_3 = v1_0;
                    v1_3 = v2_0;
                } else {
                    int v0_22;
                    switch ((v0_18.b & 7)) {
                        case 1:
                            v0_22 = Math.max(((v6 - v8.getMeasuredWidth()) / 2), v1_0);
                            v2_3 = v1_0;
                            v1_3 = v2_0;
                            break;
                        case 2:
                        case 4:
                        default:
                            v0_22 = v1_0;
                            v2_3 = v1_0;
                            v1_3 = v2_0;
                            break;
                        case 3:
                            v1_3 = v2_0;
                            v2_3 = (v8.getWidth() + v1_0);
                            v0_22 = v1_0;
                            break;
                        case 5:
                            v0_22 = ((v6 - v2_0) - v8.getMeasuredWidth());
                            v2_3 = v1_0;
                            v1_3 = (v2_0 + v8.getMeasuredWidth());
                            break;
                    }
                    int v0_29 = ((v0_22 + v5) - v8.getLeft());
                    if (v0_29 != 0) {
                        v8.offsetLeftAndRight(v0_29);
                    }
                }
                v4++;
                v1_0 = v2_3;
                v2_0 = v1_3;
            }
        }
        if (this.ao != null) {
            this.ao.a(p12, p13);
        }
        if (this.a != null) {
            int v2_1 = this.a.size();
            int v1_1 = 0;
            while (v1_1 < v2_1) {
                int v0_15 = ((android.support.v4.view.ev) this.a.get(v1_1));
                if (v0_15 != 0) {
                    v0_15.a(p12, p13);
                }
                v1_1++;
            }
        }
        if (this.ap != null) {
            this.ap.a(p12, p13);
        }
        if (this.ar != null) {
            this.getScrollX();
            int v2_2 = this.getChildCount();
            int v1_2 = 0;
            while (v1_2 < v2_2) {
                android.view.View v3_1 = this.getChildAt(v1_2);
                if (!((android.support.v4.view.es) v3_1.getLayoutParams()).a) {
                    v3_1.getLeft();
                    this.getClientWidth();
                }
                v1_2++;
            }
        }
        this.am = 1;
        return;
    }

    private void a(int p2, int p3)
    {
        this.a(p2, p3, 0);
        return;
    }

    private void a(int p13, int p14, int p15)
    {
        if (this.getChildCount() != 0) {
            int v1 = this.getScrollX();
            int v2 = this.getScrollY();
            int v3 = (p13 - v1);
            int v4 = (p14 - v2);
            if ((v3 != 0) || (v4 != 0)) {
                android.widget.Scroller v0_10;
                this.setScrollingCacheEnabled(1);
                this.setScrollState(2);
                android.widget.Scroller v0_3 = this.getClientWidth();
                int v5_0 = (v0_3 / 2);
                int v5_3 = ((((float) v5_0) * ((float) Math.sin(((double) ((float) (((double) (Math.min(1065353216, ((1065353216 * ((float) Math.abs(v3))) / ((float) v0_3))) - 1056964608)) * 0.4712389167638204)))))) + ((float) v5_0));
                float v6_5 = Math.abs(p15);
                if (v6_5 <= 0) {
                    v0_10 = ((int) (((((float) Math.abs(v3)) / ((((float) v0_3) * 1065353216) + ((float) this.A))) + 1065353216) * 1120403456));
                } else {
                    v0_10 = (Math.round((1148846080 * Math.abs((v5_3 / ((float) v6_5))))) * 4);
                }
                this.y.startScroll(v1, v2, v3, v4, Math.min(v0_10, 600));
                android.support.v4.view.cx.b(this);
            } else {
                this.a(0);
                this.b();
                this.setScrollState(0);
            }
        } else {
            this.setScrollingCacheEnabled(0);
        }
        return;
    }

    private void a(int p7, int p8, int p9, int p10)
    {
        if ((p8 <= 0) || (this.q.isEmpty())) {
            int v0_4;
            int v0_3 = this.b(this.u);
            if (v0_3 == 0) {
                v0_4 = 0;
            } else {
                v0_4 = Math.min(v0_3.e, this.F);
            }
            int v0_7 = ((int) (v0_4 * ((float) ((p7 - this.getPaddingLeft()) - this.getPaddingRight()))));
            if (v0_7 != this.getScrollX()) {
                this.a(0);
                this.scrollTo(v0_7, this.getScrollY());
            }
        } else {
            int v1_14 = ((int) (((float) (((p7 - this.getPaddingLeft()) - this.getPaddingRight()) + p9)) * (((float) this.getScrollX()) / ((float) (((p8 - this.getPaddingLeft()) - this.getPaddingRight()) + p10)))));
            this.scrollTo(v1_14, this.getScrollY());
            if (!this.y.isFinished()) {
                this.y.startScroll(v1_14, 0, ((int) (this.b(this.u).e * ((float) p7))), 0, (this.y.getDuration() - this.y.timePassed()));
            }
        }
        return;
    }

    private void a(int p6, boolean p7, int p8, boolean p9)
    {
        int v0_1;
        int v0_0 = this.b(p6);
        if (v0_0 == 0) {
            v0_1 = 0;
        } else {
            v0_1 = ((int) (Math.max(this.E, Math.min(v0_0.e, this.F)) * ((float) this.getClientWidth())));
        }
        if (!p7) {
            if (p9) {
                this.d(p6);
            }
            this.a(0);
            this.scrollTo(v0_1, 0);
            this.c(v0_1);
        } else {
            this.a(v0_1, 0, p8);
            if (p9) {
                this.d(p6);
            }
        }
        return;
    }

    private void a(int p2, boolean p3, boolean p4)
    {
        this.a(p2, p3, p4, 0);
        return;
    }

    private void a(int p5, boolean p6, boolean p7, int p8)
    {
        int v1 = 0;
        if ((this.t != null) && (this.t.e() > 0)) {
            if ((p7) || ((this.u != p5) || (this.q.size() == 0))) {
                if (p5 >= 0) {
                    if (p5 >= this.t.e()) {
                        p5 = (this.t.e() - 1);
                    }
                } else {
                    p5 = 0;
                }
                int v0_10 = this.L;
                if ((p5 > (this.u + v0_10)) || (p5 < (this.u - v0_10))) {
                    int v2_3 = 0;
                    while (v2_3 < this.q.size()) {
                        ((android.support.v4.view.er) this.q.get(v2_3)).c = 1;
                        v2_3++;
                    }
                }
                if (this.u != p5) {
                    v1 = 1;
                }
                if (!this.ak) {
                    this.a(p5);
                    this.a(p5, p6, p8, v1);
                } else {
                    this.u = p5;
                    if (v1 != 0) {
                        this.d(p5);
                    }
                    this.requestLayout();
                }
            } else {
                this.setScrollingCacheEnabled(0);
            }
        } else {
            this.setScrollingCacheEnabled(0);
        }
        return;
    }

    static synthetic void a(android.support.v4.view.ViewPager p1)
    {
        p1.setScrollState(0);
        return;
    }

    private void a(android.support.v4.view.er p13, int p14, android.support.v4.view.er p15)
    {
        int v6;
        int v7 = this.t.e();
        int v0_1 = this.getClientWidth();
        if (v0_1 <= 0) {
            v6 = 0;
        } else {
            v6 = (((float) this.A) / ((float) v0_1));
        }
        if (p15 != null) {
            int v0_5 = p15.b;
            if (v0_5 >= p13.b) {
                if (v0_5 > p13.b) {
                    int v3_0 = (this.q.size() - 1);
                    int v2_1 = p15.e;
                    float v1_7 = (v0_5 - 1);
                    while ((v1_7 >= p13.b) && (v3_0 >= 0)) {
                        int v0_10 = ((android.support.v4.view.er) this.q.get(v3_0));
                        while ((v1_7 < v0_10.b) && (v3_0 > 0)) {
                            v3_0--;
                            v0_10 = ((android.support.v4.view.er) this.q.get(v3_0));
                        }
                        float v1_8 = v2_1;
                        int v2_2 = v1_7;
                        while (v2_2 > v0_10.b) {
                            v2_2--;
                            v1_8 -= (1065353216 + v6);
                        }
                        float v1_9 = (v1_8 - (v0_10.d + v6));
                        v0_10.e = v1_9;
                        v2_1 = v1_9;
                        v1_7 = (v2_2 - 1);
                    }
                }
            } else {
                int v2_4 = ((p15.e + p15.d) + v6);
                int v3_1 = 0;
                float v1_14 = (v0_5 + 1);
                while ((v1_14 <= p13.b) && (v3_1 < this.q.size())) {
                    int v0_44 = ((android.support.v4.view.er) this.q.get(v3_1));
                    while ((v1_14 > v0_44.b) && (v3_1 < (this.q.size() - 1))) {
                        v3_1++;
                        v0_44 = ((android.support.v4.view.er) this.q.get(v3_1));
                    }
                    float v1_18 = v2_4;
                    int v2_7 = v1_14;
                    while (v2_7 < v0_44.b) {
                        v2_7++;
                        v1_18 = ((1065353216 + v6) + v1_18);
                    }
                    v0_44.e = v1_18;
                    v2_4 = (v1_18 + (v0_44.d + v6));
                    v1_14 = (v2_7 + 1);
                }
            }
        }
        int v0_21;
        float v5_6 = this.q.size();
        float v1_15 = p13.e;
        int v2_5 = (p13.b - 1);
        if (p13.b != 0) {
            v0_21 = -8388609;
        } else {
            v0_21 = p13.e;
        }
        int v0_23;
        this.E = v0_21;
        if (p13.b != (v7 - 1)) {
            v0_23 = 2139095039;
        } else {
            v0_23 = ((p13.e + p13.d) - 1065353216);
        }
        this.F = v0_23;
        int v3_4 = (p14 - 1);
        while (v3_4 >= 0) {
            int v0_39 = ((android.support.v4.view.er) this.q.get(v3_4));
            while (v2_5 > v0_39.b) {
                v2_5--;
                v1_15 -= (1065353216 + v6);
            }
            v1_15 -= (v0_39.d + v6);
            v0_39.e = v1_15;
            if (v0_39.b == 0) {
                this.E = v1_15;
            }
            v2_5--;
            v3_4--;
        }
        float v1_17 = ((p13.e + p13.d) + v6);
        int v2_6 = (p13.b + 1);
        int v3_5 = (p14 + 1);
        while (v3_5 < v5_6) {
            int v0_33 = ((android.support.v4.view.er) this.q.get(v3_5));
            while (v2_6 < v0_33.b) {
                v2_6++;
                v1_17 += (1065353216 + v6);
            }
            if (v0_33.b == (v7 - 1)) {
                this.F = ((v0_33.d + v1_17) - 1065353216);
            }
            v0_33.e = v1_17;
            v1_17 += (v0_33.d + v6);
            v2_6++;
            v3_5++;
        }
        this.al = 0;
        return;
    }

    private void a(android.view.MotionEvent p4)
    {
        android.view.VelocityTracker v0_0 = android.support.v4.view.bk.b(p4);
        if (android.support.v4.view.bk.b(p4, v0_0) == this.V) {
            android.view.VelocityTracker v0_1;
            if (v0_0 != null) {
                v0_1 = 0;
            } else {
                v0_1 = 1;
            }
            this.R = android.support.v4.view.bk.c(p4, v0_1);
            this.V = android.support.v4.view.bk.b(p4, v0_1);
            if (this.aa != null) {
                this.aa.clear();
            }
        }
        return;
    }

    private void a(boolean p8)
    {
        Runnable v0_1;
        if (this.aA != 2) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        if (v0_1 != null) {
            this.setScrollingCacheEnabled(0);
            this.y.abortAnimation();
            int v1_2 = this.getScrollX();
            int v3_0 = this.getScrollY();
            boolean v5_1 = this.y.getCurrX();
            int v6_1 = this.y.getCurrY();
            if ((v1_2 != v5_1) || (v3_0 != v6_1)) {
                this.scrollTo(v5_1, v6_1);
                if (v5_1 != v1_2) {
                    this.c(v5_1);
                }
            }
        }
        this.K = 0;
        int v1_3 = 0;
        int v3_1 = v0_1;
        while (v1_3 < this.q.size()) {
            Runnable v0_8 = ((android.support.v4.view.er) this.q.get(v1_3));
            if (v0_8.c) {
                v0_8.c = 0;
                v3_1 = 1;
            }
            v1_3++;
        }
        if (v3_1 != 0) {
            if (!p8) {
                this.az.run();
            } else {
                android.support.v4.view.cx.a(this, this.az);
            }
        }
        return;
    }

    private void a(boolean p5, android.support.v4.view.ew p6)
    {
        int v1 = 1;
        if (android.os.Build$VERSION.SDK_INT >= 11) {
            int v0_1;
            if (p6 == null) {
                v0_1 = 0;
            } else {
                v0_1 = 1;
            }
            int v3_2;
            if (this.ar == null) {
                v3_2 = 0;
            } else {
                v3_2 = 1;
            }
            int v3_3;
            if (v0_1 == v3_2) {
                v3_3 = 0;
            } else {
                v3_3 = 1;
            }
            this.ar = p6;
            this.setChildrenDrawingOrderEnabledCompat(v0_1);
            if (v0_1 == 0) {
                this.aw = 0;
            } else {
                if (p5) {
                    v1 = 2;
                }
                this.aw = v1;
            }
            if (v3_3 != 0) {
                this.b();
            }
        }
        return;
    }

    private boolean a(float p4, float p5)
    {
        if (((p4 >= ((float) this.P)) || (p5 <= 0)) && ((p4 <= ((float) (this.getWidth() - this.P))) || (p5 >= 0))) {
            int v0_9 = 0;
        } else {
            v0_9 = 1;
        }
        return v0_9;
    }

    private boolean a(android.view.KeyEvent p4)
    {
        boolean v0_0 = 0;
        if (p4.getAction() == 0) {
            switch (p4.getKeyCode()) {
                case 21:
                    v0_0 = this.f(17);
                    break;
                case 22:
                    v0_0 = this.f(66);
                    break;
                case 61:
                    if (android.os.Build$VERSION.SDK_INT >= 11) {
                        if (!android.support.v4.view.ab.b(p4)) {
                            if (android.support.v4.view.ab.a(p4)) {
                                v0_0 = this.f(1);
                            }
                        } else {
                            v0_0 = this.f(2);
                        }
                    }
                    break;
            }
        }
        return v0_0;
    }

    private boolean a(android.view.View p11, boolean p12, int p13, int p14, int p15)
    {
        int v2 = 1;
        if (!(p11 instanceof android.view.ViewGroup)) {
            if ((!p12) || (!android.support.v4.view.cx.a(p11, (- p13)))) {
                v2 = 0;
            }
        } else {
            int v8 = p11.getScrollX();
            int v9 = p11.getScrollY();
            int v7 = (((android.view.ViewGroup) p11).getChildCount() - 1);
            while (v7 >= 0) {
                android.view.View v1 = ((android.view.ViewGroup) p11).getChildAt(v7);
                if (((p14 + v8) < v1.getLeft()) || (((p14 + v8) >= v1.getRight()) || (((p15 + v9) < v1.getTop()) || (((p15 + v9) >= v1.getBottom()) || (!this.a(v1, 1, p13, ((p14 + v8) - v1.getLeft()), ((p15 + v9) - v1.getTop()))))))) {
                    v7--;
                }
            }
        }
        return v2;
    }

    static synthetic android.support.v4.view.by b(android.support.v4.view.ViewPager p1)
    {
        return p1.t;
    }

    private android.support.v4.view.er b(int p4)
    {
        int v1 = 0;
        while (v1 < this.q.size()) {
            int v0_3 = ((android.support.v4.view.er) this.q.get(v1));
            if (v0_3.b != p4) {
                v1++;
            }
            return v0_3;
        }
        v0_3 = 0;
        return v0_3;
    }

    private android.support.v4.view.er b(int p3, int p4)
    {
        android.support.v4.view.er v0_1 = new android.support.v4.view.er();
        v0_1.b = p3;
        v0_1.a = this.t.a(this, p3);
        v0_1.d = 1065353216;
        if ((p4 >= 0) && (p4 < this.q.size())) {
            this.q.add(p4, v0_1);
        } else {
            this.q.add(v0_1);
        }
        return v0_1;
    }

    private android.support.v4.view.er b(android.view.View p3)
    {
        while(true) {
            android.view.View v0_1;
            android.view.View v0_0 = p3.getParent();
            if (v0_0 == this) {
                v0_1 = this.a(p3);
            } else {
                if ((v0_0 == null) || (!(v0_0 instanceof android.view.View))) {
                    break;
                }
                p3 = ((android.view.View) v0_0);
            }
            return v0_1;
        }
        v0_1 = 0;
        return v0_1;
    }

    private void b(int p4, float p5)
    {
        if (this.ao != null) {
            this.ao.a(p4, p5);
        }
        if (this.a != null) {
            int v2 = this.a.size();
            int v1_1 = 0;
            while (v1_1 < v2) {
                int v0_8 = ((android.support.v4.view.ev) this.a.get(v1_1));
                if (v0_8 != 0) {
                    v0_8.a(p4, p5);
                }
                v1_1++;
            }
        }
        if (this.ap != null) {
            this.ap.a(p4, p5);
        }
        return;
    }

    private void b(android.support.v4.view.ev p2)
    {
        if (this.a == null) {
            this.a = new java.util.ArrayList();
        }
        this.a.add(p2);
        return;
    }

    private void b(boolean p7)
    {
        int v3 = this.getChildCount();
        int v2 = 0;
        while (v2 < v3) {
            int v0_0;
            if (!p7) {
                v0_0 = 0;
            } else {
                v0_0 = 2;
            }
            android.support.v4.view.cx.a(this.getChildAt(v2), v0_0, 0);
            v2++;
        }
        return;
    }

    private boolean b(float p11)
    {
        float v0_7;
        float v3_0 = 1;
        boolean v2_0 = 0;
        float v0_1 = (this.R - p11);
        this.R = p11;
        float v5 = (((float) this.getScrollX()) + v0_1);
        int v7 = this.getClientWidth();
        int v4_0 = (((float) v7) * this.E);
        float v0_6 = ((android.support.v4.view.er) this.q.get(0));
        int v1_6 = ((android.support.v4.view.er) this.q.get((this.q.size() - 1)));
        if (v0_6.b == 0) {
            v0_7 = 1;
        } else {
            v4_0 = (v0_6.e * ((float) v7));
            v0_7 = 0;
        }
        int v1_7;
        if (v1_6.b == (this.t.e() - 1)) {
            v1_7 = (((float) v7) * this.F);
        } else {
            v1_7 = (v1_6.e * ((float) v7));
            v3_0 = 0;
        }
        if (v5 >= v4_0) {
            if (v5 <= v1_7) {
                v4_0 = v5;
            } else {
                if (v3_0 != 0) {
                    v2_0 = this.aj.a((Math.abs((v5 - v1_7)) / ((float) v7)));
                }
                v4_0 = v1_7;
            }
        } else {
            if (v0_7 != 0) {
                v2_0 = this.ai.a((Math.abs((v4_0 - v5)) / ((float) v7)));
            }
        }
        this.R = (this.R + (v4_0 - ((float) ((int) v4_0))));
        this.scrollTo(((int) v4_0), this.getScrollY());
        this.c(((int) v4_0));
        return v2_0;
    }

    static synthetic int c(android.support.v4.view.ViewPager p1)
    {
        return p1.u;
    }

    private void c(float p9)
    {
        if (this.ag) {
            float v0_10;
            this.R = (this.R + p9);
            float v3 = (((float) this.getScrollX()) - p9);
            float v5_0 = this.getClientWidth();
            float v0_9 = ((android.support.v4.view.er) this.q.get(0));
            android.view.VelocityTracker v1_4 = ((android.support.v4.view.er) this.q.get((this.q.size() - 1)));
            if (v0_9.b == 0) {
                v0_10 = (((float) v5_0) * this.E);
            } else {
                v0_10 = (v0_9.e * ((float) v5_0));
            }
            android.view.VelocityTracker v1_5;
            if (v1_4.b == (this.t.e() - 1)) {
                v1_5 = (((float) v5_0) * this.F);
            } else {
                v1_5 = (v1_4.e * ((float) v5_0));
            }
            if (v3 >= v0_10) {
                if (v3 <= v1_5) {
                    v0_10 = v3;
                } else {
                    v0_10 = v1_5;
                }
            }
            this.R = (this.R + (v0_10 - ((float) ((int) v0_10))));
            this.scrollTo(((int) v0_10), this.getScrollY());
            this.c(((int) v0_10));
            float v0_15 = android.view.MotionEvent.obtain(this.ah, android.os.SystemClock.uptimeMillis(), 2, this.R, 0, 0);
            this.aa.addMovement(v0_15);
            v0_15.recycle();
            return;
        } else {
            throw new IllegalStateException("No fake drag in progress. Call beginFakeDrag first.");
        }
    }

    private void c(android.support.v4.view.ev p2)
    {
        if (this.a != null) {
            this.a.remove(p2);
        }
        return;
    }

    private boolean c(int p7)
    {
        int v0_0 = 0;
        if (this.q.size() != 0) {
            String v1_2 = this.i();
            float v2_0 = this.getClientWidth();
            int v4_1 = v1_2.b;
            String v1_5 = (((((float) p7) / ((float) v2_0)) - v1_2.e) / (v1_2.d + (((float) this.A) / ((float) v2_0))));
            this.am = 0;
            this.a(v4_1, v1_5);
            if (this.am) {
                v0_0 = 1;
            } else {
                throw new IllegalStateException("onPageScrolled did not call superclass implementation");
            }
        } else {
            this.am = 0;
            this.a(0, 0);
            if (!this.am) {
                throw new IllegalStateException("onPageScrolled did not call superclass implementation");
            }
        }
        return v0_0;
    }

    static synthetic int[] c()
    {
        return android.support.v4.view.ViewPager.m;
    }

    private void d()
    {
        this.setWillNotDraw(0);
        this.setDescendantFocusability(262144);
        this.setFocusable(1);
        int v0_2 = this.getContext();
        this.y = new android.widget.Scroller(v0_2, android.support.v4.view.ViewPager.p);
        android.support.v4.widget.al v1_2 = android.view.ViewConfiguration.get(v0_2);
        float v2_3 = v0_2.getResources().getDisplayMetrics().density;
        this.Q = android.support.v4.view.du.a(v1_2);
        this.ab = ((int) (1137180672 * v2_3));
        this.ac = v1_2.getScaledMaximumFlingVelocity();
        this.ai = new android.support.v4.widget.al(v0_2);
        this.aj = new android.support.v4.widget.al(v0_2);
        this.ad = ((int) (1103626240 * v2_3));
        this.ae = ((int) (1073741824 * v2_3));
        this.O = ((int) (1098907648 * v2_3));
        android.support.v4.view.cx.a(this, new android.support.v4.view.et(this));
        if (android.support.v4.view.cx.c(this) == 0) {
            android.support.v4.view.cx.c(this, 1);
        }
        return;
    }

    private void d(int p4)
    {
        if (this.ao != null) {
            this.ao.b(p4);
        }
        if (this.a != null) {
            int v2 = this.a.size();
            int v1_1 = 0;
            while (v1_1 < v2) {
                int v0_8 = ((android.support.v4.view.ev) this.a.get(v1_1));
                if (v0_8 != 0) {
                    v0_8.b(p4);
                }
                v1_1++;
            }
        }
        if (this.ap != null) {
            this.ap.b(p4);
        }
        return;
    }

    private void e()
    {
        int v1 = 0;
        while (v1 < this.getChildCount()) {
            if (!((android.support.v4.view.es) this.getChildAt(v1).getLayoutParams()).a) {
                this.removeViewAt(v1);
                v1--;
            }
            v1++;
        }
        return;
    }

    private void e(int p4)
    {
        if (this.ao != null) {
            this.ao.a(p4);
        }
        if (this.a != null) {
            int v2 = this.a.size();
            int v1_1 = 0;
            while (v1_1 < v2) {
                int v0_8 = ((android.support.v4.view.ev) this.a.get(v1_1));
                if (v0_8 != 0) {
                    v0_8.a(p4);
                }
                v1_1++;
            }
        }
        if (this.ap != null) {
            this.ap.a(p4);
        }
        return;
    }

    private void f()
    {
        if (this.a != null) {
            this.a.clear();
        }
        return;
    }

    private boolean f(int p10)
    {
        int v0_2;
        int v4 = 0;
        String v2_0 = this.findFocus();
        if (v2_0 != this) {
            if (v2_0 != null) {
                int v0_0 = v2_0.getParent();
                while ((v0_0 instanceof android.view.ViewGroup)) {
                    if (v0_0 != this) {
                        v0_0 = v0_0.getParent();
                    } else {
                        int v0_1 = 1;
                    }
                    if (v0_1 == 0) {
                        String v5_2 = new StringBuilder();
                        v5_2.append(v2_0.getClass().getSimpleName());
                        int v0_5 = v2_0.getParent();
                        while ((v0_5 instanceof android.view.ViewGroup)) {
                            v5_2.append(" => ").append(v0_5.getClass().getSimpleName());
                            v0_5 = v0_5.getParent();
                        }
                        android.util.Log.e("ViewPager", new StringBuilder("arrowScroll tried to find focus based on non-child current focused view ").append(v5_2.toString()).toString());
                        v0_2 = 0;
                        int v0_8;
                        int v1_2 = android.view.FocusFinder.getInstance().findNextFocus(this, v0_2, p10);
                        if ((v1_2 == 0) || (v1_2 == v0_2)) {
                            if ((p10 != 17) && (p10 != 1)) {
                                if ((p10 == 66) || (p10 == 2)) {
                                    if ((this.t == null) || (this.u >= (this.t.e() - 1))) {
                                        v0_8 = 0;
                                        v4 = v0_8;
                                    } else {
                                        this.setCurrentItem$2563266((this.u + 1));
                                        v0_8 = 1;
                                    }
                                }
                            } else {
                                v0_8 = this.n();
                            }
                        } else {
                            if (p10 != 17) {
                                if (p10 == 66) {
                                    if ((v0_2 != 0) && (this.a(this.s, v1_2).left <= this.a(this.s, v0_2).left)) {
                                    } else {
                                        v0_8 = v1_2.requestFocus();
                                    }
                                }
                            } else {
                                if ((v0_2 == 0) || (this.a(this.s, v1_2).left < this.a(this.s, v0_2).left)) {
                                    v0_8 = v1_2.requestFocus();
                                } else {
                                    v0_8 = this.n();
                                }
                            }
                        }
                        if (v4 != 0) {
                            this.playSoundEffect(android.view.SoundEffectConstants.getContantForFocusDirection(p10));
                        }
                        return v4;
                    }
                }
                v0_1 = 0;
            }
            v0_2 = v2_0;
        } else {
            v0_2 = 0;
        }
    }

    private void g()
    {
        if (this.aw != 0) {
            if (this.ax != null) {
                this.ax.clear();
            } else {
                this.ax = new java.util.ArrayList();
            }
            android.support.v4.view.fa v1_0 = this.getChildCount();
            java.util.ArrayList v0_5 = 0;
            while (v0_5 < v1_0) {
                this.ax.add(this.getChildAt(v0_5));
                v0_5++;
            }
            java.util.Collections.sort(this.ax, android.support.v4.view.ViewPager.ay);
        }
        return;
    }

    private int getClientWidth()
    {
        return ((this.getMeasuredWidth() - this.getPaddingLeft()) - this.getPaddingRight());
    }

    private void h()
    {
        android.view.ViewParent v0 = this.getParent();
        if (v0 != null) {
            v0.requestDisallowInterceptTouchEvent(1);
        }
        return;
    }

    private android.support.v4.view.er i()
    {
        int v9;
        int v1_0 = this.getClientWidth();
        if (v1_0 <= 0) {
            v9 = 0;
        } else {
            v9 = (((float) this.getScrollX()) / ((float) v1_0));
        }
        int v1_1;
        if (v1_0 <= 0) {
            v1_1 = 0;
        } else {
            v1_1 = (((float) this.A) / ((float) v1_0));
        }
        float v6_0 = 0;
        float v7_0 = 0;
        int v8 = -1;
        int v2_1 = 0;
        int v5_1 = 1;
        android.support.v4.view.er v4_2 = 0;
        while (v2_1 < this.q.size()) {
            int v0_12;
            int v2_2;
            int v0_11 = ((android.support.v4.view.er) this.q.get(v2_1));
            if ((v5_1 != 0) || (v0_11.b == (v8 + 1))) {
                v0_12 = v2_1;
                v2_2 = v0_11;
            } else {
                int v0_13 = this.r;
                v0_13.e = ((v6_0 + v7_0) + v1_1);
                v0_13.b = (v8 + 1);
                v0_13.d = 1065353216;
                v0_12 = (v2_1 - 1);
                v2_2 = v0_13;
            }
            float v6_5 = v2_2.e;
            if ((v5_1 == 0) && (v9 < v6_5)) {
                break;
            }
            if ((v9 >= ((v2_2.d + v6_5) + v1_1)) && (v0_12 != (this.q.size() - 1))) {
                v7_0 = v6_5;
                v8 = v2_2.b;
                v5_1 = 0;
                v6_0 = v2_2.d;
                v4_2 = v2_2;
                v2_1 = (v0_12 + 1);
            } else {
                v4_2 = v2_2;
                break;
            }
        }
        return v4_2;
    }

    private boolean j()
    {
        int v4 = 0;
        if (!this.M) {
            this.ag = 1;
            this.setScrollState(1);
            this.R = 0;
            this.T = 0;
            if (this.aa != null) {
                this.aa.clear();
            } else {
                this.aa = android.view.VelocityTracker.obtain();
            }
            long v0_4 = android.os.SystemClock.uptimeMillis();
            android.view.MotionEvent v2_1 = android.view.MotionEvent.obtain(v0_4, v0_4, 0, 0, 0, 0);
            this.aa.addMovement(v2_1);
            v2_1.recycle();
            this.ah = v0_4;
            v4 = 1;
        }
        return v4;
    }

    private void k()
    {
        if (this.ag) {
            int v0_1 = this.aa;
            v0_1.computeCurrentVelocity(1000, ((float) this.ac));
            int v0_3 = ((int) android.support.v4.view.cs.a(v0_1, this.V));
            this.K = 1;
            int v1_2 = this.getClientWidth();
            int v2_2 = this.getScrollX();
            float v3_0 = this.i();
            this.a(this.a(v3_0.b, (((((float) v2_2) / ((float) v1_2)) - v3_0.e) / v3_0.d), v0_3, ((int) (this.R - this.T))), 1, 1, v0_3);
            this.m();
            this.ag = 0;
            return;
        } else {
            throw new IllegalStateException("No fake drag in progress. Call beginFakeDrag first.");
        }
    }

    private boolean l()
    {
        return this.ag;
    }

    private void m()
    {
        this.M = 0;
        this.N = 0;
        if (this.aa != null) {
            this.aa.recycle();
            this.aa = 0;
        }
        return;
    }

    private boolean n()
    {
        int v0_1;
        if (this.u <= 0) {
            v0_1 = 0;
        } else {
            this.setCurrentItem$2563266((this.u - 1));
            v0_1 = 1;
        }
        return v0_1;
    }

    private boolean o()
    {
        if ((this.t == null) || (this.u >= (this.t.e() - 1))) {
            int v0_2 = 0;
        } else {
            this.setCurrentItem$2563266((this.u + 1));
            v0_2 = 1;
        }
        return v0_2;
    }

    private void setCurrentItem$2563266(int p3)
    {
        this.K = 0;
        this.a(p3, 1, 0);
        return;
    }

    private void setScrollState(int p8)
    {
        int v1 = 0;
        if (this.aA != p8) {
            this.aA = p8;
            if (this.ar != null) {
                android.support.v4.view.ev v0_2;
                if (p8 == 0) {
                    v0_2 = 0;
                } else {
                    v0_2 = 1;
                }
                int v4 = this.getChildCount();
                int v3 = 0;
                while (v3 < v4) {
                    int v2_1;
                    if (v0_2 == null) {
                        v2_1 = 0;
                    } else {
                        v2_1 = 2;
                    }
                    android.support.v4.view.cx.a(this.getChildAt(v3), v2_1, 0);
                    v3++;
                }
            }
            if (this.ao != null) {
                this.ao.a(p8);
            }
            if (this.a != null) {
                int v2_0 = this.a.size();
                while (v1 < v2_0) {
                    android.support.v4.view.ev v0_11 = ((android.support.v4.view.ev) this.a.get(v1));
                    if (v0_11 != null) {
                        v0_11.a(p8);
                    }
                    v1++;
                }
            }
            if (this.ap != null) {
                this.ap.a(p8);
            }
        }
        return;
    }

    private void setScrollingCacheEnabled(boolean p2)
    {
        if (this.J != p2) {
            this.J = p2;
        }
        return;
    }

    final android.support.v4.view.ev a(android.support.v4.view.ev p2)
    {
        android.support.v4.view.ev v0 = this.ap;
        this.ap = p2;
        return v0;
    }

    final void a()
    {
        int v0_2;
        int v0_1 = this.t.e();
        this.n = v0_1;
        if ((this.q.size() >= ((this.L * 2) + 1)) || (this.q.size() >= v0_1)) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        int v4_3 = this.u;
        int v3_4 = 0;
        while (v3_4 < this.q.size()) {
            this.q.get(v3_4);
            v3_4++;
        }
        java.util.Collections.sort(this.q, android.support.v4.view.ViewPager.o);
        if (v0_2 != 0) {
            int v5_3 = this.getChildCount();
            int v3_6 = 0;
            while (v3_6 < v5_3) {
                int v0_5 = ((android.support.v4.view.es) this.getChildAt(v3_6).getLayoutParams());
                if (!v0_5.a) {
                    v0_5.c = 0;
                }
                v3_6++;
            }
            this.a(v4_3, 0, 1);
            this.requestLayout();
        }
        return;
    }

    public void addFocusables(java.util.ArrayList p7, int p8, int p9)
    {
        int v1_0 = p7.size();
        int v2 = this.getDescendantFocusability();
        if (v2 != 393216) {
            boolean v0_1 = 0;
            while (v0_1 < this.getChildCount()) {
                android.view.View v3_1 = this.getChildAt(v0_1);
                if (v3_1.getVisibility() == 0) {
                    int v4_1 = this.a(v3_1);
                    if ((v4_1 != 0) && (v4_1.b == this.u)) {
                        v3_1.addFocusables(p7, p8, p9);
                    }
                }
                v0_1++;
            }
        }
        if (((v2 != 262144) || (v1_0 == p7.size())) && (((this.isFocusable()) && (((p9 & 1) != 1) || ((!this.isInTouchMode()) || (this.isFocusableInTouchMode())))) && (p7 != null))) {
            p7.add(this);
        }
        return;
    }

    public void addTouchables(java.util.ArrayList p5)
    {
        int v0 = 0;
        while (v0 < this.getChildCount()) {
            android.view.View v1_1 = this.getChildAt(v0);
            if (v1_1.getVisibility() == 0) {
                int v2_1 = this.a(v1_1);
                if ((v2_1 != 0) && (v2_1.b == this.u)) {
                    v1_1.addTouchables(p5);
                }
            }
            v0++;
        }
        return;
    }

    public void addView(android.view.View p5, int p6, android.view.ViewGroup$LayoutParams p7)
    {
        String v1_0;
        if (this.checkLayoutParams(p7)) {
            v1_0 = p7;
        } else {
            v1_0 = this.generateLayoutParams(p7);
        }
        IllegalStateException v0_2 = ((android.support.v4.view.es) v1_0);
        v0_2.a = (v0_2.a | (p5 instanceof android.support.v4.view.eq));
        if (!this.I) {
            super.addView(p5, p6, v1_0);
        } else {
            if ((v0_2 == null) || (!v0_2.a)) {
                v0_2.d = 1;
                this.addViewInLayout(p5, p6, v1_0);
            } else {
                throw new IllegalStateException("Cannot add pager decor view during layout");
            }
        }
        return;
    }

    final void b()
    {
        this.a(this.u);
        return;
    }

    public boolean canScrollHorizontally(int p6)
    {
        int v0 = 0;
        if (this.t != null) {
            int v2_1 = this.getClientWidth();
            int v3 = this.getScrollX();
            if (p6 >= 0) {
                if ((p6 > 0) && (v3 < ((int) (((float) v2_1) * this.F)))) {
                    v0 = 1;
                }
            } else {
                if (v3 > ((int) (((float) v2_1) * this.E))) {
                    v0 = 1;
                }
            }
        }
        return v0;
    }

    protected boolean checkLayoutParams(android.view.ViewGroup$LayoutParams p2)
    {
        if ((!(p2 instanceof android.support.v4.view.es)) || (!super.checkLayoutParams(p2))) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public void computeScroll()
    {
        if ((this.y.isFinished()) || (!this.y.computeScrollOffset())) {
            this.a(1);
        } else {
            int v0_5 = this.getScrollX();
            int v1 = this.getScrollY();
            int v2_1 = this.y.getCurrX();
            int v3_1 = this.y.getCurrY();
            if ((v0_5 != v2_1) || (v1 != v3_1)) {
                this.scrollTo(v2_1, v3_1);
                if (!this.c(v2_1)) {
                    this.y.abortAnimation();
                    this.scrollTo(0, v3_1);
                }
            }
            android.support.v4.view.cx.b(this);
        }
        return;
    }

    public boolean dispatchKeyEvent(android.view.KeyEvent p5)
    {
        int v0 = 0;
        if (super.dispatchKeyEvent(p5)) {
            v0 = 1;
        } else {
            boolean v2_6;
            if (p5.getAction() != 0) {
                v2_6 = 0;
            } else {
                switch (p5.getKeyCode()) {
                    case 21:
                        v2_6 = this.f(17);
                        break;
                    case 22:
                        v2_6 = this.f(66);
                        break;
                    case 61:
                        if (android.os.Build$VERSION.SDK_INT < 11) {
                        } else {
                            if (!android.support.v4.view.ab.b(p5)) {
                                if (!android.support.v4.view.ab.a(p5)) {
                                } else {
                                    v2_6 = this.f(1);
                                }
                            } else {
                                v2_6 = this.f(2);
                            }
                        }
                        break;
                    default:
                }
            }
            if (v2_6) {
            }
        }
        return v0;
    }

    public boolean dispatchPopulateAccessibilityEvent(android.view.accessibility.AccessibilityEvent p7)
    {
        int v0 = 0;
        if (p7.getEventType() != 4096) {
            int v2_1 = this.getChildCount();
            int v1_1 = 0;
            while (v1_1 < v2_1) {
                boolean v3_0 = this.getChildAt(v1_1);
                if (v3_0.getVisibility() == 0) {
                    int v4_1 = this.a(v3_0);
                    if ((v4_1 != 0) && ((v4_1.b == this.u) && (v3_0.dispatchPopulateAccessibilityEvent(p7)))) {
                        v0 = 1;
                        break;
                    }
                }
                v1_1++;
            }
        } else {
            v0 = super.dispatchPopulateAccessibilityEvent(p7);
        }
        return v0;
    }

    public void draw(android.graphics.Canvas p8)
    {
        super.draw(p8);
        int v0_0 = 0;
        int v1_0 = android.support.v4.view.cx.a(this);
        if ((v1_0 != 0) && ((v1_0 != 1) || ((this.t == null) || (this.t.e() <= 1)))) {
            this.ai.b();
            this.aj.b();
        } else {
            if (!this.ai.a()) {
                int v1_8 = p8.save();
                int v0_3 = ((this.getHeight() - this.getPaddingTop()) - this.getPaddingBottom());
                boolean v2_3 = this.getWidth();
                p8.rotate(1132920832);
                p8.translate(((float) ((- v0_3) + this.getPaddingTop())), (this.E * ((float) v2_3)));
                this.ai.a(v0_3, v2_3);
                v0_0 = (this.ai.a(p8) | 0);
                p8.restoreToCount(v1_8);
            }
            if (!this.aj.a()) {
                int v1_11 = p8.save();
                boolean v2_4 = this.getWidth();
                int v3_7 = ((this.getHeight() - this.getPaddingTop()) - this.getPaddingBottom());
                p8.rotate(1119092736);
                p8.translate(((float) (- this.getPaddingTop())), ((- (this.F + 1065353216)) * ((float) v2_4)));
                this.aj.a(v3_7, v2_4);
                v0_0 |= this.aj.a(p8);
                p8.restoreToCount(v1_11);
            }
        }
        if (v0_0 != 0) {
            android.support.v4.view.cx.b(this);
        }
        return;
    }

    protected void drawableStateChanged()
    {
        super.drawableStateChanged();
        android.graphics.drawable.Drawable v0 = this.B;
        if ((v0 != null) && (v0.isStateful())) {
            v0.setState(this.getDrawableState());
        }
        return;
    }

    protected android.view.ViewGroup$LayoutParams generateDefaultLayoutParams()
    {
        return new android.support.v4.view.es();
    }

    public android.view.ViewGroup$LayoutParams generateLayoutParams(android.util.AttributeSet p3)
    {
        return new android.support.v4.view.es(this.getContext(), p3);
    }

    protected android.view.ViewGroup$LayoutParams generateLayoutParams(android.view.ViewGroup$LayoutParams p2)
    {
        return this.generateDefaultLayoutParams();
    }

    public android.support.v4.view.by getAdapter()
    {
        return this.t;
    }

    protected int getChildDrawingOrder(int p3, int p4)
    {
        if (this.aw == 2) {
            p4 = ((p3 - 1) - p4);
        }
        return ((android.support.v4.view.es) ((android.view.View) this.ax.get(p4)).getLayoutParams()).f;
    }

    public int getCurrentItem()
    {
        return this.u;
    }

    public int getOffscreenPageLimit()
    {
        return this.L;
    }

    public int getPageMargin()
    {
        return this.A;
    }

    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        this.ak = 1;
        return;
    }

    protected void onDetachedFromWindow()
    {
        this.removeCallbacks(this.az);
        super.onDetachedFromWindow();
        return;
    }

    protected void onDraw(android.graphics.Canvas p17)
    {
        this = super.onDraw(p17);
        if ((this.A > 0) && ((this.B != null) && ((this.q.size() > 0) && (this.t != null)))) {
            int v6 = this.getScrollX();
            int v7 = this.getWidth();
            float v8 = (((float) this.A) / ((float) v7));
            android.support.v4.view.er v1_9 = ((android.support.v4.view.er) this.q.get(0));
            float v4_0 = v1_9.e;
            int v9 = this.q.size();
            int v10_1 = ((android.support.v4.view.er) this.q.get((v9 - 1))).b;
            int v2_6 = 0;
            int v5_1 = v1_9.b;
            while (v5_1 < v10_1) {
                while ((v5_1 > v1_9.b) && (v2_6 < v9)) {
                    v2_6++;
                    v1_9 = ((android.support.v4.view.er) this.q.get(v2_6));
                }
                int v3_5;
                if (v5_1 != v1_9.b) {
                    v3_5 = ((1065353216 + v4_0) * ((float) v7));
                    v4_0 += (1065353216 + v8);
                } else {
                    v3_5 = ((v1_9.e + v1_9.d) * ((float) v7));
                    v4_0 = ((v1_9.e + v1_9.d) + v8);
                }
                if ((((float) this.A) + v3_5) > ((float) v6)) {
                    this.B.setBounds(((int) v3_5), this.C, ((int) ((((float) this.A) + v3_5) + 1056964608)), this.D);
                    this.B.draw(p17);
                }
                if (v3_5 > ((float) (v6 + v7))) {
                    break;
                }
                v5_1++;
            }
        }
        return;
    }

    public boolean onInterceptTouchEvent(android.view.MotionEvent p14)
    {
        boolean v2 = 0;
        boolean v0_1 = (p14.getAction() & 255);
        if ((v0_1 != 3) && (v0_1 != 1)) {
            if (v0_1) {
                if (!this.M) {
                    if (this.N) {
                        return v2;
                    }
                } else {
                    v2 = 1;
                    return v2;
                }
            }
            switch (v0_1) {
                case 0:
                    boolean v0_28 = p14.getX();
                    this.T = v0_28;
                    this.R = v0_28;
                    boolean v0_29 = p14.getY();
                    this.U = v0_29;
                    this.S = v0_29;
                    this.V = android.support.v4.view.bk.b(p14, 0);
                    this.N = 0;
                    this.y.computeScrollOffset();
                    if ((this.aA != 2) || (Math.abs((this.y.getFinalX() - this.y.getCurrX())) <= this.ae)) {
                        this.a(0);
                        this.M = 0;
                    } else {
                        this.y.abortAnimation();
                        this.K = 0;
                        this.b();
                        this.M = 1;
                        this.h();
                        this.setScrollState(1);
                    }
                case 2:
                    boolean v0_2 = this.V;
                    if (v0_2 == -1) {
                    } else {
                        boolean v0_3 = android.support.v4.view.bk.a(p14, v0_2);
                        float v7 = android.support.v4.view.bk.c(p14, v0_3);
                        float v8 = (v7 - this.R);
                        float v9 = Math.abs(v8);
                        float v10 = android.support.v4.view.bk.d(p14, v0_3);
                        float v11 = Math.abs((v10 - this.U));
                        if (v8 != 0) {
                            boolean v0_10;
                            boolean v0_7 = this.R;
                            if (((v0_7 >= ((float) this.P)) || (v8 <= 0)) && ((v0_7 <= ((float) (this.getWidth() - this.P))) || (v8 >= 0))) {
                                v0_10 = 0;
                            } else {
                                v0_10 = 1;
                            }
                            if ((!v0_10) && (this.a(this, 0, ((int) v8), ((int) v7), ((int) v10)))) {
                                this.R = v7;
                                this.S = v10;
                                this.N = 1;
                                return v2;
                            }
                        }
                        if ((v9 <= ((float) this.Q)) || ((1056964608 * v9) <= v11)) {
                            if (v11 > ((float) this.Q)) {
                                this.N = 1;
                            }
                        } else {
                            boolean v0_24;
                            this.M = 1;
                            this.h();
                            this.setScrollState(1);
                            if (v8 <= 0) {
                                v0_24 = (this.T - ((float) this.Q));
                            } else {
                                v0_24 = (this.T + ((float) this.Q));
                            }
                            this.R = v0_24;
                            this.S = v10;
                            this.setScrollingCacheEnabled(1);
                        }
                        if ((!this.M) || (!this.b(v7))) {
                        } else {
                            android.support.v4.view.cx.b(this);
                        }
                    }
                case 6:
                    this.a(p14);
                    break;
            }
            if (this.aa == null) {
                this.aa = android.view.VelocityTracker.obtain();
            }
            this.aa.addMovement(p14);
            v2 = this.M;
        } else {
            this.M = 0;
            this.N = 0;
            this.V = -1;
            if (this.aa != null) {
                this.aa.recycle();
                this.aa = 0;
            }
        }
        return v2;
    }

    protected void onLayout(boolean p18, int p19, int p20, int p21, int p22)
    {
        int v9 = this.getChildCount();
        int v10_0 = (p21 - p19);
        int v11 = (p22 - p20);
        int v6_0 = this.getPaddingLeft();
        int v2_0 = this.getPaddingTop();
        int v5_0 = this.getPaddingRight();
        int v3_0 = this.getPaddingBottom();
        int v12_0 = this.getScrollX();
        int v4_0 = 0;
        android.view.View v8_0 = 0;
        while (v8_0 < v9) {
            int v5_2;
            int v4_2;
            int v2_2;
            int v1_20;
            int v13_1 = this.getChildAt(v8_0);
            if (v13_1.getVisibility() == 8) {
                v1_20 = v4_0;
                v4_2 = v2_0;
                v2_2 = v5_0;
                v5_2 = v6_0;
            } else {
                int v1_19 = ((android.support.v4.view.es) v13_1.getLayoutParams());
                if (!v1_19.a) {
                } else {
                    int v7_7;
                    int v14_0 = (v1_19.b & 112);
                    switch ((v1_19.b & 7)) {
                        case 1:
                            v7_7 = Math.max(((v10_0 - v13_1.getMeasuredWidth()) / 2), v6_0);
                            break;
                        case 2:
                        case 4:
                        default:
                            v7_7 = v6_0;
                            break;
                        case 3:
                            v7_7 = v6_0;
                            v6_0 = (v13_1.getMeasuredWidth() + v6_0);
                            break;
                        case 5:
                            int v1_23 = ((v10_0 - v5_0) - v13_1.getMeasuredWidth());
                            v5_0 += v13_1.getMeasuredWidth();
                            v7_7 = v1_23;
                            break;
                    }
                    int v3_3;
                    int v1_31;
                    int v2_3;
                    switch (v14_0) {
                        case 16:
                            v1_31 = Math.max(((v11 - v13_1.getMeasuredHeight()) / 2), v2_0);
                            v3_3 = v2_0;
                            v2_3 = v3_0;
                            break;
                        case 48:
                            v2_3 = v3_0;
                            v3_3 = (v13_1.getMeasuredHeight() + v2_0);
                            v1_31 = v2_0;
                            break;
                        case 80:
                            v1_31 = ((v11 - v3_0) - v13_1.getMeasuredHeight());
                            v3_3 = v2_0;
                            v2_3 = (v3_0 + v13_1.getMeasuredHeight());
                            break;
                        default:
                            v1_31 = v2_0;
                            v3_3 = v2_0;
                            v2_3 = v3_0;
                    }
                    int v7_8 = (v7_7 + v12_0);
                    v13_1.layout(v7_8, v1_31, (v13_1.getMeasuredWidth() + v7_8), (v13_1.getMeasuredHeight() + v1_31));
                    v1_20 = (v4_0 + 1);
                    v4_2 = v3_3;
                    v3_0 = v2_3;
                    v2_2 = v5_0;
                    v5_2 = v6_0;
                }
            }
            v8_0++;
            v6_0 = v5_2;
            v5_0 = v2_2;
            v2_0 = v4_2;
            v4_0 = v1_20;
        }
        int v7_0 = ((v10_0 - v6_0) - v5_0);
        int v5_1 = 0;
        while (v5_1 < v9) {
            android.view.View v8_1 = this.getChildAt(v5_1);
            if (v8_1.getVisibility() != 8) {
                int v1_9 = ((android.support.v4.view.es) v8_1.getLayoutParams());
                if (!v1_9.a) {
                    int v10_3 = this.a(v8_1);
                    if (v10_3 != 0) {
                        int v10_7 = (((int) (v10_3.e * ((float) v7_0))) + v6_0);
                        if (v1_9.d) {
                            v1_9.d = 0;
                            v8_1.measure(android.view.View$MeasureSpec.makeMeasureSpec(((int) (v1_9.c * ((float) v7_0))), 1073741824), android.view.View$MeasureSpec.makeMeasureSpec(((v11 - v2_0) - v3_0), 1073741824));
                        }
                        v8_1.layout(v10_7, v2_0, (v8_1.getMeasuredWidth() + v10_7), (v8_1.getMeasuredHeight() + v2_0));
                    }
                }
            }
            v5_1++;
        }
        this.C = v2_0;
        this.D = (v11 - v3_0);
        this.an = v4_0;
        if (this.ak) {
            this.a(this.u, 0, 0, 0);
        }
        this.ak = 0;
        return;
    }

    protected void onMeasure(int p14, int p15)
    {
        this.setMeasuredDimension(android.support.v4.view.ViewPager.getDefaultSize(0, p14), android.support.v4.view.ViewPager.getDefaultSize(0, p15));
        int v0_2 = this.getMeasuredWidth();
        this.P = Math.min((v0_2 / 10), this.O);
        int v3 = ((v0_2 - this.getPaddingLeft()) - this.getPaddingRight());
        int v5_0 = ((this.getMeasuredHeight() - this.getPaddingTop()) - this.getPaddingBottom());
        int v9 = this.getChildCount();
        int v8 = 0;
        while (v8 < v9) {
            android.view.View v10 = this.getChildAt(v8);
            if (v10.getVisibility() != 8) {
                int v0_24 = ((android.support.v4.view.es) v10.getLayoutParams());
                if ((v0_24 != 0) && (v0_24.a)) {
                    int v7_2;
                    int v6_0 = (v0_24.b & 7);
                    int v4_1 = (v0_24.b & 112);
                    int v2_2 = -2147483648;
                    int v1_13 = -2147483648;
                    if ((v4_1 != 48) && (v4_1 != 80)) {
                        v7_2 = 0;
                    } else {
                        v7_2 = 1;
                    }
                    if ((v6_0 != 3) && (v6_0 != 5)) {
                        int v6_1 = 0;
                    } else {
                        v6_1 = 1;
                    }
                    if (v7_2 == 0) {
                        if (v6_1 != 0) {
                            v1_13 = 1073741824;
                        }
                    } else {
                        v2_2 = 1073741824;
                    }
                    int v4_9;
                    int v2_3;
                    if (v0_24.width == -2) {
                        v4_9 = v2_2;
                        v2_3 = v3;
                    } else {
                        v4_9 = 1073741824;
                        if (v0_24.width == -1) {
                            v2_3 = v3;
                        } else {
                            v2_3 = v0_24.width;
                        }
                    }
                    int v0_25;
                    if (v0_24.height == -2) {
                        v0_25 = v5_0;
                    } else {
                        v1_13 = 1073741824;
                        if (v0_24.height == -1) {
                        } else {
                            v0_25 = v0_24.height;
                        }
                    }
                    v10.measure(android.view.View$MeasureSpec.makeMeasureSpec(v2_3, v4_9), android.view.View$MeasureSpec.makeMeasureSpec(v0_25, v1_13));
                    if (v7_2 == 0) {
                        if (v6_1 != 0) {
                            v3 -= v10.getMeasuredWidth();
                        }
                    } else {
                        v5_0 -= v10.getMeasuredHeight();
                    }
                }
            }
            v8++;
        }
        this.G = android.view.View$MeasureSpec.makeMeasureSpec(v3, 1073741824);
        this.H = android.view.View$MeasureSpec.makeMeasureSpec(v5_0, 1073741824);
        this.I = 1;
        this.b();
        this.I = 0;
        int v2_1 = this.getChildCount();
        int v1_8 = 0;
        while (v1_8 < v2_1) {
            int v4_0 = this.getChildAt(v1_8);
            if (v4_0.getVisibility() != 8) {
                int v0_16 = ((android.support.v4.view.es) v4_0.getLayoutParams());
                if ((v0_16 == 0) || (!v0_16.a)) {
                    v4_0.measure(android.view.View$MeasureSpec.makeMeasureSpec(((int) (v0_16.c * ((float) v3))), 1073741824), this.H);
                }
            }
            v1_8++;
        }
        return;
    }

    protected boolean onRequestFocusInDescendants(int p9, android.graphics.Rect p10)
    {
        int v4_1;
        int v0 = 1;
        int v3 = -1;
        int v2_0 = this.getChildCount();
        if ((p9 & 2) == 0) {
            v4_1 = (v2_0 - 1);
            v2_0 = -1;
        } else {
            v3 = 1;
            v4_1 = 0;
        }
        while (v4_1 != v2_0) {
            boolean v5_0 = this.getChildAt(v4_1);
            if (v5_0.getVisibility() == 0) {
                int v6_1 = this.a(v5_0);
                if ((v6_1 != 0) && ((v6_1.b == this.u) && (v5_0.requestFocus(p9, p10)))) {
                    return v0;
                }
            }
            v4_1 += v3;
        }
        v0 = 0;
        return v0;
    }

    public void onRestoreInstanceState(android.os.Parcelable p4)
    {
        if ((p4 instanceof android.support.v4.view.ViewPager$SavedState)) {
            super.onRestoreInstanceState(((android.support.v4.view.ViewPager$SavedState) p4).getSuperState());
            if (this.t == null) {
                this.v = ((android.support.v4.view.ViewPager$SavedState) p4).a;
                this.w = ((android.support.v4.view.ViewPager$SavedState) p4).b;
                this.x = ((android.support.v4.view.ViewPager$SavedState) p4).c;
            } else {
                this.t.a(((android.support.v4.view.ViewPager$SavedState) p4).b, ((android.support.v4.view.ViewPager$SavedState) p4).c);
                this.a(((android.support.v4.view.ViewPager$SavedState) p4).a, 0, 1);
            }
        } else {
            super.onRestoreInstanceState(p4);
        }
        return;
    }

    public android.os.Parcelable onSaveInstanceState()
    {
        android.support.v4.view.ViewPager$SavedState v1_1 = new android.support.v4.view.ViewPager$SavedState(super.onSaveInstanceState());
        v1_1.a = this.u;
        if (this.t != null) {
            v1_1.b = this.t.d();
        }
        return v1_1;
    }

    protected void onSizeChanged(int p3, int p4, int p5, int p6)
    {
        super.onSizeChanged(p3, p4, p5, p6);
        if (p3 != p5) {
            this.a(p3, p5, this.A, this.A);
        }
        return;
    }

    public boolean onTouchEvent(android.view.MotionEvent p8)
    {
        android.view.ViewParent v0_49;
        int v2_0 = 0;
        if (!this.ag) {
            if ((p8.getAction() != 0) || (p8.getEdgeFlags() == 0)) {
                if ((this.t != null) && (this.t.e() != 0)) {
                    if (this.aa == null) {
                        this.aa = android.view.VelocityTracker.obtain();
                    }
                    this.aa.addMovement(p8);
                    switch ((p8.getAction() & 255)) {
                        case 0:
                            this.y.abortAnimation();
                            this.K = 0;
                            this.b();
                            android.view.ViewParent v0_46 = p8.getX();
                            this.T = v0_46;
                            this.R = v0_46;
                            android.view.ViewParent v0_47 = p8.getY();
                            this.U = v0_47;
                            this.S = v0_47;
                            this.V = android.support.v4.view.bk.b(p8, 0);
                            break;
                        case 1:
                            if (!this.M) {
                            } else {
                                android.view.ViewParent v0_40 = this.aa;
                                v0_40.computeCurrentVelocity(1000, ((float) this.ac));
                                android.view.ViewParent v0_42 = ((int) android.support.v4.view.cs.a(v0_40, this.V));
                                this.K = 1;
                                int v2_5 = this.getClientWidth();
                                float v3_9 = this.getScrollX();
                                float v4_3 = this.i();
                                this.a(this.a(v4_3.b, (((((float) v3_9) / ((float) v2_5)) - v4_3.e) / v4_3.d), v0_42, ((int) (android.support.v4.view.bk.c(p8, android.support.v4.view.bk.a(p8, this.V)) - this.T))), 1, 1, v0_42);
                                this.V = -1;
                                this.m();
                                v2_0 = (this.aj.c() | this.ai.c());
                            }
                            break;
                        case 2:
                            if (!this.M) {
                                android.view.ViewParent v0_22 = android.support.v4.view.bk.a(p8, this.V);
                                float v3_1 = android.support.v4.view.bk.c(p8, v0_22);
                                float v4_2 = Math.abs((v3_1 - this.R));
                                float v5_0 = android.support.v4.view.bk.d(p8, v0_22);
                                android.view.ViewParent v0_25 = Math.abs((v5_0 - this.S));
                                if ((v4_2 > ((float) this.Q)) && (v4_2 > v0_25)) {
                                    android.view.ViewParent v0_31;
                                    this.M = 1;
                                    this.h();
                                    if ((v3_1 - this.T) <= 0) {
                                        v0_31 = (this.T - ((float) this.Q));
                                    } else {
                                        v0_31 = (this.T + ((float) this.Q));
                                    }
                                    this.R = v0_31;
                                    this.S = v5_0;
                                    this.setScrollState(1);
                                    this.setScrollingCacheEnabled(1);
                                    android.view.ViewParent v0_33 = this.getParent();
                                    if (v0_33 != null) {
                                        v0_33.requestDisallowInterceptTouchEvent(1);
                                    }
                                }
                            }
                            if (!this.M) {
                            } else {
                                v2_0 = (this.b(android.support.v4.view.bk.c(p8, android.support.v4.view.bk.a(p8, this.V))) | 0);
                            }
                            break;
                        case 3:
                            if (this.M) {
                                this.a(this.u, 1, 0, 0);
                                this.V = -1;
                                this.m();
                                v2_0 = (this.aj.c() | this.ai.c());
                            } else {
                            }
                        case 4:
                        default:
                            break;
                        case 5:
                            android.view.ViewParent v0_14 = android.support.v4.view.bk.b(p8);
                            this.R = android.support.v4.view.bk.c(p8, v0_14);
                            this.V = android.support.v4.view.bk.b(p8, v0_14);
                            break;
                        case 6:
                            this.a(p8);
                            this.R = android.support.v4.view.bk.c(p8, android.support.v4.view.bk.a(p8, this.V));
                            break;
                    }
                    if (v2_0 != 0) {
                        android.support.v4.view.cx.b(this);
                    }
                    v0_49 = 1;
                } else {
                    v0_49 = 0;
                }
            } else {
                v0_49 = 0;
            }
        } else {
            v0_49 = 1;
        }
        return v0_49;
    }

    public void removeView(android.view.View p2)
    {
        if (!this.I) {
            super.removeView(p2);
        } else {
            this.removeViewInLayout(p2);
        }
        return;
    }

    public void setAdapter(android.support.v4.view.by p8)
    {
        if (this.t != null) {
            this.t.b(this.z);
            int v1_1 = 0;
            while (v1_1 < this.q.size()) {
                int v0_15 = ((android.support.v4.view.er) this.q.get(v1_1));
                this.t.a(v0_15.b, v0_15.a);
                v1_1++;
            }
            this.t.c();
            this.q.clear();
            int v1_2 = 0;
            while (v1_2 < this.getChildCount()) {
                if (!((android.support.v4.view.es) this.getChildAt(v1_2).getLayoutParams()).a) {
                    this.removeViewAt(v1_2);
                    v1_2--;
                }
                v1_2++;
            }
            this.u = 0;
            this.scrollTo(0, 0);
        }
        int v0_7 = this.t;
        this.t = p8;
        this.n = 0;
        if (this.t != null) {
            if (this.z == null) {
                this.z = new android.support.v4.view.ex(this, 0);
            }
            this.t.a(this.z);
            this.K = 0;
            int v1_8 = this.ak;
            this.ak = 1;
            this.n = this.t.e();
            if (this.v < 0) {
                if (v1_8 != 0) {
                    this.requestLayout();
                } else {
                    this.b();
                }
            } else {
                this.t.a(this.w, this.x);
                this.a(this.v, 0, 1);
                this.v = -1;
                this.w = 0;
                this.x = 0;
            }
        }
        if ((this.aq != null) && (v0_7 != p8)) {
            this.aq.a(v0_7, p8);
        }
        return;
    }

    void setChildrenDrawingOrderEnabledCompat(boolean p6)
    {
        if (android.os.Build$VERSION.SDK_INT >= 7) {
            if (this.as == null) {
                try {
                    String v2_1 = new Class[1];
                    v2_1[0] = Boolean.TYPE;
                    this.as = android.view.ViewGroup.getDeclaredMethod("setChildrenDrawingOrderEnabled", v2_1);
                } catch (Exception v0_4) {
                    android.util.Log.e("ViewPager", "Can\'t find setChildrenDrawingOrderEnabled", v0_4);
                }
                Exception v0_5 = this.as;
                String v1_4 = new Object[1];
                v1_4[0] = Boolean.valueOf(p6);
                v0_5.invoke(this, v1_4);
                return;
            }
            try {
            } catch (Exception v0_6) {
                android.util.Log.e("ViewPager", "Error changing children drawing order", v0_6);
            }
        }
        return;
    }

    public void setCurrentItem(int p3)
    {
        int v0_1;
        this.K = 0;
        if (this.ak) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        this.a(p3, v0_1, 0);
        return;
    }

    public void setOffscreenPageLimit(int p4)
    {
        if (p4 <= 0) {
            android.util.Log.w("ViewPager", new StringBuilder("Requested offscreen page limit ").append(p4).append(" too small; defaulting to 1").toString());
            p4 = 1;
        }
        if (p4 != this.L) {
            this.L = p4;
            this.b();
        }
        return;
    }

    void setOnAdapterChangeListener(android.support.v4.view.eu p1)
    {
        this.aq = p1;
        return;
    }

    public void setOnPageChangeListener(android.support.v4.view.ev p1)
    {
        this.ao = p1;
        return;
    }

    public void setPageMargin(int p3)
    {
        int v0 = this.A;
        this.A = p3;
        this.a(this.getWidth(), this.getWidth(), p3, v0);
        this.requestLayout();
        return;
    }

    public void setPageMarginDrawable(int p2)
    {
        this.setPageMarginDrawable(this.getContext().getResources().getDrawable(p2));
        return;
    }

    public void setPageMarginDrawable(android.graphics.drawable.Drawable p2)
    {
        this.B = p2;
        if (p2 != null) {
            this.refreshDrawableState();
        }
        int v0;
        if (p2 != null) {
            v0 = 0;
        } else {
            v0 = 1;
        }
        this.setWillNotDraw(v0);
        this.invalidate();
        return;
    }

    protected boolean verifyDrawable(android.graphics.drawable.Drawable p2)
    {
        if ((!super.verifyDrawable(p2)) && (p2 != this.B)) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }
}
