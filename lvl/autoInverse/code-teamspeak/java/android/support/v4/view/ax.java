package android.support.v4.view;
final class ax {

    ax()
    {
        return;
    }

    private static int a(android.view.ViewGroup$MarginLayoutParams p1)
    {
        return p1.getMarginStart();
    }

    private static void a(android.view.ViewGroup$MarginLayoutParams p0, int p1)
    {
        p0.setMarginStart(p1);
        return;
    }

    private static int b(android.view.ViewGroup$MarginLayoutParams p1)
    {
        return p1.getMarginEnd();
    }

    private static void b(android.view.ViewGroup$MarginLayoutParams p0, int p1)
    {
        p0.setMarginEnd(p1);
        return;
    }

    private static void c(android.view.ViewGroup$MarginLayoutParams p0, int p1)
    {
        p0.setLayoutDirection(p1);
        return;
    }

    private static boolean c(android.view.ViewGroup$MarginLayoutParams p1)
    {
        return p1.isMarginRelative();
    }

    private static int d(android.view.ViewGroup$MarginLayoutParams p1)
    {
        return p1.getLayoutDirection();
    }

    private static void d(android.view.ViewGroup$MarginLayoutParams p0, int p1)
    {
        p0.resolveLayoutDirection(p1);
        return;
    }
}
