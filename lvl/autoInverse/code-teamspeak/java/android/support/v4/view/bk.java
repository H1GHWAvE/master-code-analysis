package android.support.v4.view;
public final class bk {
    public static final int A = 16;
    public static final int B = 17;
    public static final int C = 18;
    public static final int D = 19;
    public static final int E = 20;
    public static final int F = 21;
    public static final int G = 22;
    public static final int H = 23;
    public static final int I = 24;
    public static final int J = 25;
    public static final int K = 32;
    public static final int L = 33;
    public static final int M = 34;
    public static final int N = 35;
    public static final int O = 36;
    public static final int P = 37;
    public static final int Q = 38;
    public static final int R = 39;
    public static final int S = 40;
    public static final int T = 41;
    public static final int U = 42;
    public static final int V = 43;
    public static final int W = 44;
    public static final int X = 45;
    public static final int Y = 46;
    public static final int Z = 47;
    static final android.support.v4.view.bp a = None;
    public static final int b = 255;
    public static final int c = 5;
    public static final int d = 6;
    public static final int e = 7;
    public static final int f = 8;
    public static final int g = 65280;
    public static final int h = 8;
    public static final int i = 9;
    public static final int j = 10;
    public static final int k = 0;
    public static final int l = 1;
    public static final int m = 2;
    public static final int n = 3;
    public static final int o = 4;
    public static final int p = 5;
    public static final int q = 6;
    public static final int r = 7;
    public static final int s = 8;
    public static final int t = 9;
    public static final int u = 10;
    public static final int v = 11;
    public static final int w = 12;
    public static final int x = 13;
    public static final int y = 14;
    public static final int z = 15;

    static bk()
    {
        if (android.os.Build$VERSION.SDK_INT < 12) {
            if (android.os.Build$VERSION.SDK_INT < 9) {
                if (android.os.Build$VERSION.SDK_INT < 5) {
                    android.support.v4.view.bk.a = new android.support.v4.view.bl();
                } else {
                    android.support.v4.view.bk.a = new android.support.v4.view.bm();
                }
            } else {
                android.support.v4.view.bk.a = new android.support.v4.view.bn();
            }
        } else {
            android.support.v4.view.bk.a = new android.support.v4.view.bo();
        }
        return;
    }

    public bk()
    {
        return;
    }

    private static float a(android.view.MotionEvent p1, int p2, int p3)
    {
        return android.support.v4.view.bk.a.a(p1, p2, p3);
    }

    public static int a(android.view.MotionEvent p1)
    {
        return (p1.getAction() & 255);
    }

    public static int a(android.view.MotionEvent p1, int p2)
    {
        return android.support.v4.view.bk.a.a(p1, p2);
    }

    public static int b(android.view.MotionEvent p2)
    {
        return ((p2.getAction() & 65280) >> 8);
    }

    public static int b(android.view.MotionEvent p1, int p2)
    {
        return android.support.v4.view.bk.a.b(p1, p2);
    }

    public static float c(android.view.MotionEvent p1, int p2)
    {
        return android.support.v4.view.bk.a.c(p1, p2);
    }

    public static int c(android.view.MotionEvent p1)
    {
        return android.support.v4.view.bk.a.a(p1);
    }

    public static float d(android.view.MotionEvent p1, int p2)
    {
        return android.support.v4.view.bk.a.d(p1, p2);
    }

    public static int d(android.view.MotionEvent p1)
    {
        return android.support.v4.view.bk.a.b(p1);
    }

    public static float e(android.view.MotionEvent p1)
    {
        return android.support.v4.view.bk.a.c(p1);
    }
}
