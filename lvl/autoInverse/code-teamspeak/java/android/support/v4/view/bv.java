package android.support.v4.view;
public interface bv {

    public abstract int getNestedScrollAxes();

    public abstract boolean onNestedFling();

    public abstract boolean onNestedPreFling();

    public abstract void onNestedPreScroll();

    public abstract void onNestedScroll();

    public abstract void onNestedScrollAccepted();

    public abstract boolean onStartNestedScroll();

    public abstract void onStopNestedScroll();
}
