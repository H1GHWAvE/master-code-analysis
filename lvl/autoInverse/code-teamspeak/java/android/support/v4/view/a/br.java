package android.support.v4.view.a;
final class br {

    br()
    {
        return;
    }

    private static Object a()
    {
        return android.view.accessibility.AccessibilityWindowInfo.obtain();
    }

    private static Object a(Object p1)
    {
        return android.view.accessibility.AccessibilityWindowInfo.obtain(((android.view.accessibility.AccessibilityWindowInfo) p1));
    }

    private static Object a(Object p1, int p2)
    {
        return ((android.view.accessibility.AccessibilityWindowInfo) p1).getChild(p2);
    }

    private static void a(Object p0, android.graphics.Rect p1)
    {
        ((android.view.accessibility.AccessibilityWindowInfo) p0).getBoundsInScreen(p1);
        return;
    }

    private static int b(Object p1)
    {
        return ((android.view.accessibility.AccessibilityWindowInfo) p1).getType();
    }

    private static int c(Object p1)
    {
        return ((android.view.accessibility.AccessibilityWindowInfo) p1).getLayer();
    }

    private static Object d(Object p1)
    {
        return ((android.view.accessibility.AccessibilityWindowInfo) p1).getRoot();
    }

    private static Object e(Object p1)
    {
        return ((android.view.accessibility.AccessibilityWindowInfo) p1).getParent();
    }

    private static int f(Object p1)
    {
        return ((android.view.accessibility.AccessibilityWindowInfo) p1).getId();
    }

    private static boolean g(Object p1)
    {
        return ((android.view.accessibility.AccessibilityWindowInfo) p1).isActive();
    }

    private static boolean h(Object p1)
    {
        return ((android.view.accessibility.AccessibilityWindowInfo) p1).isFocused();
    }

    private static boolean i(Object p1)
    {
        return ((android.view.accessibility.AccessibilityWindowInfo) p1).isAccessibilityFocused();
    }

    private static int j(Object p1)
    {
        return ((android.view.accessibility.AccessibilityWindowInfo) p1).getChildCount();
    }

    private static void k(Object p0)
    {
        ((android.view.accessibility.AccessibilityWindowInfo) p0).recycle();
        return;
    }
}
