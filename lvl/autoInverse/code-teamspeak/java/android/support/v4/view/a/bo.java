package android.support.v4.view.a;
final class bo extends android.support.v4.view.a.bq {

    private bo()
    {
        this(0);
        return;
    }

    synthetic bo(byte p1)
    {
        return;
    }

    public final Object a()
    {
        return android.view.accessibility.AccessibilityWindowInfo.obtain();
    }

    public final Object a(Object p2)
    {
        return android.view.accessibility.AccessibilityWindowInfo.obtain(((android.view.accessibility.AccessibilityWindowInfo) p2));
    }

    public final Object a(Object p2, int p3)
    {
        return ((android.view.accessibility.AccessibilityWindowInfo) p2).getChild(p3);
    }

    public final void a(Object p1, android.graphics.Rect p2)
    {
        ((android.view.accessibility.AccessibilityWindowInfo) p1).getBoundsInScreen(p2);
        return;
    }

    public final int b(Object p2)
    {
        return ((android.view.accessibility.AccessibilityWindowInfo) p2).getType();
    }

    public final int c(Object p2)
    {
        return ((android.view.accessibility.AccessibilityWindowInfo) p2).getLayer();
    }

    public final Object d(Object p2)
    {
        return ((android.view.accessibility.AccessibilityWindowInfo) p2).getRoot();
    }

    public final Object e(Object p2)
    {
        return ((android.view.accessibility.AccessibilityWindowInfo) p2).getParent();
    }

    public final int f(Object p2)
    {
        return ((android.view.accessibility.AccessibilityWindowInfo) p2).getId();
    }

    public final boolean g(Object p2)
    {
        return ((android.view.accessibility.AccessibilityWindowInfo) p2).isActive();
    }

    public final boolean h(Object p2)
    {
        return ((android.view.accessibility.AccessibilityWindowInfo) p2).isFocused();
    }

    public final boolean i(Object p2)
    {
        return ((android.view.accessibility.AccessibilityWindowInfo) p2).isAccessibilityFocused();
    }

    public final int j(Object p2)
    {
        return ((android.view.accessibility.AccessibilityWindowInfo) p2).getChildCount();
    }

    public final void k(Object p1)
    {
        ((android.view.accessibility.AccessibilityWindowInfo) p1).recycle();
        return;
    }
}
