package android.support.v4.view;
final class u implements android.support.v4.view.r {
    private final android.view.GestureDetector a;

    public u(android.content.Context p3, android.view.GestureDetector$OnGestureListener p4)
    {
        this.a = new android.view.GestureDetector(p3, p4, 0);
        return;
    }

    public final void a(android.view.GestureDetector$OnDoubleTapListener p2)
    {
        this.a.setOnDoubleTapListener(p2);
        return;
    }

    public final void a(boolean p2)
    {
        this.a.setIsLongpressEnabled(p2);
        return;
    }

    public final boolean a()
    {
        return this.a.isLongpressEnabled();
    }

    public final boolean a(android.view.MotionEvent p2)
    {
        return this.a.onTouchEvent(p2);
    }
}
