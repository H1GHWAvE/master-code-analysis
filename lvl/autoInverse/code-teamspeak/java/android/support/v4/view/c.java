package android.support.v4.view;
final class c implements android.support.v4.view.j {
    final synthetic android.support.v4.view.a a;
    final synthetic android.support.v4.view.b b;

    c(android.support.v4.view.b p1, android.support.v4.view.a p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final void a(android.view.View p1, int p2)
    {
        android.support.v4.view.a.a(p1, p2);
        return;
    }

    public final void a(android.view.View p3, Object p4)
    {
        this.a.a(p3, new android.support.v4.view.a.q(p4));
        return;
    }

    public final boolean a(android.view.View p2, android.view.accessibility.AccessibilityEvent p3)
    {
        return this.a.d(p2, p3);
    }

    public final boolean a(android.view.ViewGroup p2, android.view.View p3, android.view.accessibility.AccessibilityEvent p4)
    {
        return this.a.a(p2, p3, p4);
    }

    public final void b(android.view.View p2, android.view.accessibility.AccessibilityEvent p3)
    {
        this.a.a(p2, p3);
        return;
    }

    public final void c(android.view.View p2, android.view.accessibility.AccessibilityEvent p3)
    {
        this.a.b(p2, p3);
        return;
    }

    public final void d(android.view.View p1, android.view.accessibility.AccessibilityEvent p2)
    {
        android.support.v4.view.a.c(p1, p2);
        return;
    }
}
