package android.support.v4.view;
final class aw implements android.support.v4.view.au {

    aw()
    {
        return;
    }

    public final int a(android.view.ViewGroup$MarginLayoutParams p2)
    {
        return p2.getMarginStart();
    }

    public final void a(android.view.ViewGroup$MarginLayoutParams p1, int p2)
    {
        p1.setMarginStart(p2);
        return;
    }

    public final int b(android.view.ViewGroup$MarginLayoutParams p2)
    {
        return p2.getMarginEnd();
    }

    public final void b(android.view.ViewGroup$MarginLayoutParams p1, int p2)
    {
        p1.setMarginEnd(p2);
        return;
    }

    public final void c(android.view.ViewGroup$MarginLayoutParams p1, int p2)
    {
        p1.setLayoutDirection(p2);
        return;
    }

    public final boolean c(android.view.ViewGroup$MarginLayoutParams p2)
    {
        return p2.isMarginRelative();
    }

    public final int d(android.view.ViewGroup$MarginLayoutParams p2)
    {
        return p2.getLayoutDirection();
    }

    public final void d(android.view.ViewGroup$MarginLayoutParams p1, int p2)
    {
        p1.resolveLayoutDirection(p2);
        return;
    }
}
