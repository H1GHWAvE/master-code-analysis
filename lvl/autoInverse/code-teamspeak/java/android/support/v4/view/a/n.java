package android.support.v4.view.a;
final class n {

    n()
    {
        return;
    }

    private static Object a(android.support.v4.view.a.p p1)
    {
        return new android.support.v4.view.a.o(p1);
    }

    private static java.util.List a(android.view.accessibility.AccessibilityManager p1)
    {
        return p1.getInstalledAccessibilityServiceList();
    }

    private static java.util.List a(android.view.accessibility.AccessibilityManager p1, int p2)
    {
        return p1.getEnabledAccessibilityServiceList(p2);
    }

    private static boolean a(android.view.accessibility.AccessibilityManager p1, Object p2)
    {
        return p1.addAccessibilityStateChangeListener(((android.view.accessibility.AccessibilityManager$AccessibilityStateChangeListener) p2));
    }

    private static boolean b(android.view.accessibility.AccessibilityManager p1)
    {
        return p1.isTouchExplorationEnabled();
    }

    private static boolean b(android.view.accessibility.AccessibilityManager p1, Object p2)
    {
        return p1.removeAccessibilityStateChangeListener(((android.view.accessibility.AccessibilityManager$AccessibilityStateChangeListener) p2));
    }
}
