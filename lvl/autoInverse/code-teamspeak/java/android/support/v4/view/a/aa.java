package android.support.v4.view.a;
 class aa extends android.support.v4.view.a.z {

    aa()
    {
        return;
    }

    public final int H(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).getLiveRegion();
    }

    public final Object I(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).getCollectionInfo();
    }

    public final Object J(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).getCollectionItemInfo();
    }

    public final Object K(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).getRangeInfo();
    }

    public final int L(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo$CollectionInfo) p2).getColumnCount();
    }

    public final int M(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo$CollectionInfo) p2).getRowCount();
    }

    public final boolean N(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo$CollectionInfo) p2).isHierarchical();
    }

    public final int O(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo$CollectionItemInfo) p2).getColumnIndex();
    }

    public final int P(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo$CollectionItemInfo) p2).getColumnSpan();
    }

    public final int Q(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo$CollectionItemInfo) p2).getRowIndex();
    }

    public final int R(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo$CollectionItemInfo) p2).getRowSpan();
    }

    public final boolean S(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo$CollectionItemInfo) p2).isHeading();
    }

    public final void T(Object p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p2).setContentInvalid(1);
        return;
    }

    public final boolean U(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).isContentInvalid();
    }

    public final boolean X(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).canOpenPopup();
    }

    public final android.os.Bundle Y(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).getExtras();
    }

    public final int Z(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).getInputType();
    }

    public Object a(int p2, int p3, int p4, int p5, boolean p6, boolean p7)
    {
        return android.view.accessibility.AccessibilityNodeInfo$CollectionItemInfo.obtain(p2, p3, p4, p5, p6);
    }

    public Object a(int p2, int p3, boolean p4, int p5)
    {
        return android.view.accessibility.AccessibilityNodeInfo$CollectionInfo.obtain(p2, p3, p4);
    }

    public final boolean ac(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).isDismissable();
    }

    public final boolean ae(Object p2)
    {
        return ((android.view.accessibility.AccessibilityNodeInfo) p2).isMultiLine();
    }

    public final void c(Object p1, Object p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setCollectionInfo(((android.view.accessibility.AccessibilityNodeInfo$CollectionInfo) p2));
        return;
    }

    public final void d(Object p1, Object p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setCollectionItemInfo(((android.view.accessibility.AccessibilityNodeInfo$CollectionItemInfo) p2));
        return;
    }

    public final void e(Object p1, Object p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setRangeInfo(((android.view.accessibility.AccessibilityNodeInfo$RangeInfo) p2));
        return;
    }

    public final void h(Object p1, int p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setLiveRegion(p2);
        return;
    }

    public final void i(Object p1, int p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setInputType(p2);
        return;
    }

    public final void m(Object p1, boolean p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setCanOpenPopup(p2);
        return;
    }

    public final void n(Object p1, boolean p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setDismissable(p2);
        return;
    }

    public final void p(Object p1, boolean p2)
    {
        ((android.view.accessibility.AccessibilityNodeInfo) p1).setMultiLine(p2);
        return;
    }
}
