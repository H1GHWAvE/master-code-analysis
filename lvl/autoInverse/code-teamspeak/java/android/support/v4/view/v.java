package android.support.v4.view;
public final class v {
    static final android.support.v4.view.w a = None;
    public static final int b = 8388608;
    public static final int c = 8388611;
    public static final int d = 8388613;
    public static final int e = 8388615;

    static v()
    {
        if (android.os.Build$VERSION.SDK_INT < 17) {
            android.support.v4.view.v.a = new android.support.v4.view.x();
        } else {
            android.support.v4.view.v.a = new android.support.v4.view.y();
        }
        return;
    }

    public v()
    {
        return;
    }

    public static int a(int p1, int p2)
    {
        return android.support.v4.view.v.a.a(p1, p2);
    }

    private static void a(int p9, int p10, int p11, android.graphics.Rect p12, int p13, int p14, android.graphics.Rect p15, int p16)
    {
        android.support.v4.view.v.a.a(p9, p10, p11, p12, p13, p14, p15, p16);
        return;
    }

    public static void a(int p7, int p8, int p9, android.graphics.Rect p10, android.graphics.Rect p11, int p12)
    {
        android.support.v4.view.v.a.a(p7, p8, p9, p10, p11, p12);
        return;
    }

    private static void a(int p1, android.graphics.Rect p2, android.graphics.Rect p3, int p4)
    {
        android.support.v4.view.v.a.a(p1, p2, p3, p4);
        return;
    }
}
