package android.support.v4.view;
 class dc extends android.support.v4.view.db {
    static reflect.Field b;
    static boolean c;

    static dc()
    {
        android.support.v4.view.dc.c = 0;
        return;
    }

    dc()
    {
        return;
    }

    public final android.support.v4.view.fk H(android.view.View p3)
    {
        if (this.a == null) {
            this.a = new java.util.WeakHashMap();
        }
        android.support.v4.view.fk v0_5 = ((android.support.v4.view.fk) this.a.get(p3));
        if (v0_5 == null) {
            v0_5 = new android.support.v4.view.fk(p3);
            this.a.put(p3, v0_5);
        }
        return v0_5;
    }

    public final void a(android.view.View p2, android.support.v4.view.a.q p3)
    {
        p2.onInitializeAccessibilityNodeInfo(((android.view.accessibility.AccessibilityNodeInfo) p3.b));
        return;
    }

    public final void a(android.view.View p2, android.support.v4.view.a p3)
    {
        android.view.View$AccessibilityDelegate v0_0;
        if (p3 != null) {
            v0_0 = p3.b;
        } else {
            v0_0 = 0;
        }
        p2.setAccessibilityDelegate(((android.view.View$AccessibilityDelegate) v0_0));
        return;
    }

    public final void a(android.view.View p1, android.view.accessibility.AccessibilityEvent p2)
    {
        p1.onPopulateAccessibilityEvent(p2);
        return;
    }

    public final boolean a(android.view.View p2, int p3)
    {
        return p2.canScrollHorizontally(p3);
    }

    public final void b(android.view.View p1, android.view.accessibility.AccessibilityEvent p2)
    {
        p1.onInitializeAccessibilityEvent(p2);
        return;
    }

    public final void b(android.view.View p1, boolean p2)
    {
        p1.setFitsSystemWindows(p2);
        return;
    }

    public final boolean b(android.view.View p5)
    {
        int v0 = 0;
        if (!android.support.v4.view.dc.c) {
            if (android.support.v4.view.dc.b == null) {
                try {
                    Throwable v2_3 = android.view.View.getDeclaredField("mAccessibilityDelegate");
                    android.support.v4.view.dc.b = v2_3;
                    v2_3.setAccessible(1);
                } catch (Throwable v2) {
                    android.support.v4.view.dc.c = 1;
                    return v0;
                }
            }
            try {
                if (android.support.v4.view.dc.b.get(p5) != null) {
                    v0 = 1;
                }
            } catch (Throwable v2) {
                android.support.v4.view.dc.c = 1;
            }
        }
        return v0;
    }

    public final boolean b(android.view.View p2, int p3)
    {
        return p2.canScrollVertically(p3);
    }
}
