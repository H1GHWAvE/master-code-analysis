package android.support.v4.view;
final class ag {

    ag()
    {
        return;
    }

    private static Object a(android.view.View p1)
    {
        return p1.getKeyDispatcherState();
    }

    private static void a(android.view.KeyEvent p0)
    {
        p0.startTracking();
        return;
    }

    private static boolean a(android.view.KeyEvent p1, android.view.KeyEvent$Callback p2, Object p3, Object p4)
    {
        return p1.dispatch(p2, ((android.view.KeyEvent$DispatcherState) p3), p4);
    }

    private static boolean b(android.view.KeyEvent p1)
    {
        return p1.isTracking();
    }
}
