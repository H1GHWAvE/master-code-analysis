package android.support.design.widget;
public class AppBarLayout$Behavior extends android.support.design.widget.df {
    private static final int a = 255;
    private static final int b = 255;
    private int c;
    private boolean d;
    private Runnable e;
    private android.support.v4.widget.ca f;
    private android.support.design.widget.ck g;
    private int h;
    private boolean i;
    private float j;
    private boolean k;
    private int l;
    private int m;
    private int n;
    private ref.WeakReference o;

    public AppBarLayout$Behavior()
    {
        this.h = -1;
        this.l = -1;
        this.n = -1;
        return;
    }

    public AppBarLayout$Behavior(android.content.Context p2, android.util.AttributeSet p3)
    {
        this(p2, p3);
        this.h = -1;
        this.l = -1;
        this.n = -1;
        return;
    }

    private static int a(android.support.design.widget.AppBarLayout p8, int p9)
    {
        int v3 = Math.abs(p9);
        int v4_0 = p8.getChildCount();
        int v2_0 = 0;
        while (v2_0 < v4_0) {
            android.view.View v5 = p8.getChildAt(v2_0);
            int v0_1 = ((android.support.design.widget.g) v5.getLayoutParams());
            android.view.animation.Interpolator v6 = v0_1.g;
            if ((v3 < v5.getTop()) || (v3 > v5.getBottom())) {
                v2_0++;
            } else {
                if (v6 == null) {
                    break;
                }
                int v0_3;
                int v2_1 = v0_1.f;
                if ((v2_1 & 1) == 0) {
                    v0_3 = 0;
                } else {
                    v0_3 = ((v0_1.bottomMargin + (v5.getHeight() + v0_1.topMargin)) + 0);
                    if ((v2_1 & 2) != 0) {
                        v0_3 -= android.support.v4.view.cx.o(v5);
                    }
                }
                if (v0_3 <= 0) {
                    break;
                }
                p9 = (Integer.signum(p9) * (Math.round((v6.getInterpolation((((float) (v3 - v5.getTop())) / ((float) v0_3))) * ((float) v0_3))) + v5.getTop()));
                break;
            }
        }
        return p9;
    }

    private int a(android.support.design.widget.CoordinatorLayout p7, android.support.design.widget.AppBarLayout p8, int p9, int p10, int p11)
    {
        return this.b(p7, p8, (this.a() - p9), p10, p11);
    }

    private android.os.Parcelable a(android.support.design.widget.CoordinatorLayout p9, android.support.design.widget.AppBarLayout p10)
    {
        android.support.design.widget.AppBarLayout$Behavior$SavedState v0_0 = 0;
        float v2_0 = super.a(p9, p10);
        int v4 = super.c();
        int v5 = p10.getChildCount();
        int v3 = 0;
        while (v3 < v5) {
            android.view.View v6 = p10.getChildAt(v3);
            int v7 = (v6.getBottom() + v4);
            if (((v6.getTop() + v4) > 0) || (v7 < 0)) {
                v3++;
            } else {
                android.support.design.widget.AppBarLayout$Behavior$SavedState v1_5 = new android.support.design.widget.AppBarLayout$Behavior$SavedState(v2_0);
                v1_5.a = v3;
                if (v7 == android.support.v4.view.cx.o(v6)) {
                    v0_0 = 1;
                }
                v1_5.c = v0_0;
                v1_5.b = (((float) v7) / ((float) v6.getHeight()));
                android.support.design.widget.AppBarLayout$Behavior$SavedState v0_1 = v1_5;
            }
            return v0_1;
        }
        v0_1 = v2_0;
        return v0_1;
    }

    static synthetic android.support.v4.widget.ca a(android.support.design.widget.AppBarLayout$Behavior p1)
    {
        return p1.f;
    }

    private void a(android.support.design.widget.AppBarLayout p6)
    {
        java.util.List v2 = android.support.design.widget.AppBarLayout.a(p6);
        int v3 = v2.size();
        int v1 = 0;
        while (v1 < v3) {
            int v0_2 = ((android.support.design.widget.i) v2.get(v1));
            if (v0_2 != 0) {
                v0_2.a(p6, super.c());
            }
            v1++;
        }
        return;
    }

    private void a(android.support.design.widget.CoordinatorLayout p8, android.support.design.widget.AppBarLayout p9, int p10, int[] p11)
    {
        if ((p10 != 0) && (!this.d)) {
            int v4;
            int v5;
            if (p10 >= 0) {
                v4 = (- p9.getUpNestedPreScrollRange());
                v5 = 0;
            } else {
                v4 = (- p9.getTotalScrollRange());
                v5 = (v4 + p9.getDownNestedPreScrollRange());
            }
            p11[1] = this.a(p8, p9, p10, v4, v5);
        }
        return;
    }

    private void a(android.support.design.widget.CoordinatorLayout p2, android.support.design.widget.AppBarLayout p3, android.os.Parcelable p4)
    {
        if (!(p4 instanceof android.support.design.widget.AppBarLayout$Behavior$SavedState)) {
            super.a(p2, p3, p4);
            this.h = -1;
        } else {
            super.a(p2, p3, ((android.support.design.widget.AppBarLayout$Behavior$SavedState) p4).getSuperState());
            this.h = ((android.support.design.widget.AppBarLayout$Behavior$SavedState) p4).a;
            this.j = ((android.support.design.widget.AppBarLayout$Behavior$SavedState) p4).b;
            this.i = ((android.support.design.widget.AppBarLayout$Behavior$SavedState) p4).c;
        }
        return;
    }

    private boolean a(android.support.design.widget.CoordinatorLayout p12, android.support.design.widget.AppBarLayout p13, float p14, boolean p15)
    {
        int v1 = 0;
        if (p15) {
            Runnable v0_3;
            if (p14 >= 0) {
                v0_3 = (- p13.getUpNestedPreScrollRange());
                if (this.a() < v0_3) {
                    return v1;
                }
            } else {
                v0_3 = ((- p13.getTotalScrollRange()) + p13.getDownNestedPreScrollRange());
                if (this.a() > v0_3) {
                    return v1;
                }
            }
            if (this.a() != v0_3) {
                this.c(p12, p13, v0_3);
                v1 = 1;
            }
        } else {
            int v7 = (- p13.getTotalScrollRange());
            int v3_0 = (- p14);
            if (this.e != null) {
                p13.removeCallbacks(this.e);
            }
            if (this.f == null) {
                this.f = android.support.v4.widget.ca.a(p13.getContext(), 0);
            }
            this.f.a(0, this.a(), 0, Math.round(v3_0), 0, 0, v7, 0);
            if (!this.f.f()) {
                this.e = 0;
            } else {
                this.e = new android.support.design.widget.e(this, p12, p13);
                android.support.v4.view.cx.a(p13, this.e);
                v1 = 1;
            }
        }
        return v1;
    }

    private boolean a(android.support.design.widget.CoordinatorLayout p11, android.support.design.widget.AppBarLayout p12, int p13, float p14)
    {
        int v1 = 0;
        if (this.e != null) {
            p12.removeCallbacks(this.e);
        }
        if (this.f == null) {
            this.f = android.support.v4.widget.ca.a(p12.getContext(), 0);
        }
        this.f.a(0, this.a(), 0, Math.round(p14), 0, 0, p13, 0);
        if (!this.f.f()) {
            this.e = 0;
        } else {
            this.e = new android.support.design.widget.e(this, p11, p12);
            android.support.v4.view.cx.a(p12, this.e);
            v1 = 1;
        }
        return v1;
    }

    private boolean a(android.support.design.widget.CoordinatorLayout p6, android.support.design.widget.AppBarLayout p7, android.view.MotionEvent p8)
    {
        int v0_0 = 1;
        if (this.n < 0) {
            this.n = android.view.ViewConfiguration.get(p6.getContext()).getScaledTouchSlop();
        }
        if ((p8.getAction() != 2) || (!this.k)) {
            switch (android.support.v4.view.bk.a(p8)) {
                case 0:
                    this.k = 0;
                    int v1_12 = ((int) p8.getY());
                    if ((!p6.a(p7, ((int) p8.getX()), v1_12)) || (!this.d())) {
                    } else {
                        this.m = v1_12;
                        this.l = android.support.v4.view.bk.b(p8, 0);
                    }
                    break;
                case 1:
                case 3:
                    this.k = 0;
                    this.l = -1;
                    break;
                case 2:
                    int v1_7 = this.l;
                    if (v1_7 == -1) {
                    } else {
                        int v1_8 = android.support.v4.view.bk.a(p8, v1_7);
                        if (v1_8 == -1) {
                        } else {
                            int v1_10 = ((int) android.support.v4.view.bk.d(p8, v1_8));
                            if (Math.abs((v1_10 - this.m)) <= this.n) {
                            } else {
                                this.k = 1;
                                this.m = v1_10;
                            }
                        }
                    }
                    break;
            }
            v0_0 = this.k;
        }
        return v0_0;
    }

    private boolean a(android.support.design.widget.CoordinatorLayout p5, android.support.design.widget.AppBarLayout p6, android.view.View p7, int p8)
    {
        int v0 = 1;
        if ((p8 & 2) == 0) {
            v0 = 0;
        } else {
            int v2_2;
            if (p6.getTotalScrollRange() == 0) {
                v2_2 = 0;
            } else {
                v2_2 = 1;
            }
            if ((v2_2 == 0) || ((p5.getHeight() - p7.getHeight()) > p6.getHeight())) {
            }
        }
        if ((v0 != 0) && (this.g != null)) {
            this.g.a.e();
        }
        this.o = 0;
        return v0;
    }

    private int b(android.support.design.widget.CoordinatorLayout p11, android.support.design.widget.AppBarLayout p12, int p13, int p14, int p15)
    {
        int v0_0;
        int v2_0 = 0;
        int v4_0 = this.a();
        if ((p14 == 0) || ((v4_0 < p14) || (v4_0 > p15))) {
            v0_0 = 0;
        } else {
            boolean v1_0 = android.support.design.widget.an.a(p13, p14, p15);
            if (v4_0 == v1_0) {
            } else {
                int v0_2;
                if (!p12.a) {
                    v0_2 = v1_0;
                } else {
                    int v5_0 = Math.abs(v1_0);
                    android.support.design.widget.t v6_0 = p12.getChildCount();
                    int v3_0 = 0;
                    while (v3_0 < v6_0) {
                        android.view.View v7 = p12.getChildAt(v3_0);
                        int v0_4 = ((android.support.design.widget.g) v7.getLayoutParams());
                        android.view.animation.Interpolator v8 = v0_4.g;
                        if ((v5_0 < v7.getTop()) || (v5_0 > v7.getBottom())) {
                            v3_0++;
                        } else {
                            if (v8 == null) {
                                break;
                            }
                            int v0_6;
                            int v3_1 = v0_4.f;
                            if ((v3_1 & 1) == 0) {
                                v0_6 = 0;
                            } else {
                                v0_6 = ((v0_4.bottomMargin + (v7.getHeight() + v0_4.topMargin)) + 0);
                                if ((v3_1 & 2) != 0) {
                                    v0_6 -= android.support.v4.view.cx.o(v7);
                                }
                            }
                            if (v0_6 <= 0) {
                                break;
                            }
                            v0_2 = ((Math.round((v8.getInterpolation((((float) (v5_0 - v7.getTop())) / ((float) v0_6))) * ((float) v0_6))) + v7.getTop()) * Integer.signum(v1_0));
                            int v5_3 = super.b(v0_2);
                            int v3_8 = (v4_0 - v1_0);
                            this.c = (v1_0 - v0_2);
                            if ((v5_3 == 0) && (p12.a)) {
                                int v5_4 = p11.h.size();
                                int v4_1 = 0;
                                while (v4_1 < v5_4) {
                                    int v0_21;
                                    int v0_20 = ((android.view.View) p11.h.get(v4_1));
                                    if (v0_20 != p12) {
                                        if (v2_0 != 0) {
                                            boolean v1_2 = ((android.support.design.widget.w) v0_20.getLayoutParams());
                                            android.support.design.widget.t v6_4 = v1_2.a;
                                            if ((v6_4 != null) && (v1_2.a(p12))) {
                                                v6_4.a(p11, v0_20, p12);
                                            }
                                        }
                                        v0_21 = v2_0;
                                    } else {
                                        v0_21 = 1;
                                    }
                                    v4_1++;
                                    v2_0 = v0_21;
                                }
                            }
                            this.a(p12);
                            v0_0 = v3_8;
                            return v0_0;
                        }
                    }
                    v0_2 = v1_0;
                }
            }
        }
        return v0_0;
    }

    private void b(android.support.design.widget.CoordinatorLayout p7, android.support.design.widget.AppBarLayout p8, int p9)
    {
        if (p9 >= 0) {
            this.d = 0;
        } else {
            this.a(p7, p8, p9, (- p8.getDownNestedScrollRange()), 0);
            this.d = 1;
        }
        return;
    }

    private boolean b(android.support.design.widget.CoordinatorLayout p8, android.support.design.widget.AppBarLayout p9, android.view.MotionEvent p10)
    {
        int v5 = 0;
        if (this.n < 0) {
            this.n = android.view.ViewConfiguration.get(p8.getContext()).getScaledTouchSlop();
        }
        int v1_1 = ((int) p10.getY());
        switch (android.support.v4.view.bk.a(p10)) {
            case 0:
                if ((!p8.a(p9, ((int) p10.getX()), v1_1)) || (!this.d())) {
                } else {
                    this.m = v1_1;
                    this.l = android.support.v4.view.bk.b(p10, 0);
                    v5 = 1;
                }
                break;
            case 1:
            case 3:
                this.k = 0;
                this.l = -1;
                break;
            case 2:
                android.support.design.widget.AppBarLayout$Behavior v0_7 = android.support.v4.view.bk.a(p10, this.l);
                if (v0_7 == -1) {
                } else {
                    android.support.design.widget.AppBarLayout$Behavior v0_9 = ((int) android.support.v4.view.bk.d(p10, v0_7));
                    int v3_1 = (this.m - v0_9);
                    if ((!this.k) && (Math.abs(v3_1) > this.n)) {
                        this.k = 1;
                        if (v3_1 <= 0) {
                            v3_1 += this.n;
                        } else {
                            v3_1 -= this.n;
                        }
                    }
                    if (!this.k) {
                    } else {
                        this.m = v0_9;
                        this.a(p8, p9, v3_1, (- p9.getDownNestedScrollRange()), 0);
                    }
                }
                break;
            default:
        }
        return v5;
    }

    private void c(android.support.design.widget.CoordinatorLayout p3, android.support.design.widget.AppBarLayout p4, int p5)
    {
        if (this.g != null) {
            this.g.a.e();
        } else {
            this.g = android.support.design.widget.dh.a();
            this.g.a(android.support.design.widget.a.c);
            this.g.a(new android.support.design.widget.d(this, p3, p4));
        }
        this.g.a(this.a(), p5);
        this.g.a.a();
        return;
    }

    private void c(android.view.View p2)
    {
        this.d = 0;
        this.o = new ref.WeakReference(p2);
        return;
    }

    private boolean d()
    {
        int v0_1;
        if (this.o == null) {
            v0_1 = 0;
        } else {
            int v0_4 = ((android.view.View) this.o.get());
            if ((v0_4 == 0) || ((!v0_4.isShown()) || (android.support.v4.view.cx.b(v0_4, -1)))) {
                v0_1 = 0;
            } else {
                v0_1 = 1;
            }
        }
        return v0_1;
    }

    private boolean d(android.support.design.widget.CoordinatorLayout p6, android.support.design.widget.AppBarLayout p7, int p8)
    {
        boolean v2 = super.a(p6, p7, p8);
        int v3_0 = p7.getPendingAction();
        if (v3_0 == 0) {
            if (this.h >= 0) {
                int v0_7;
                int v0_2 = p7.getChildAt(this.h);
                int v1_2 = (- v0_2.getBottom());
                if (!this.i) {
                    v0_7 = (Math.round((((float) v0_2.getHeight()) * this.j)) + v1_2);
                } else {
                    v0_7 = (android.support.v4.view.cx.o(v0_2) + v1_2);
                }
                super.b(v0_7);
                this.h = -1;
            }
        } else {
            int v0_11;
            if ((v3_0 & 4) == 0) {
                v0_11 = 0;
            } else {
                v0_11 = 1;
            }
            if ((v3_0 & 2) == 0) {
                if ((v3_0 & 1) != 0) {
                    if (v0_11 == 0) {
                        this.a(p6, p7, 0);
                    } else {
                        this.c(p6, p7, 0);
                    }
                }
            } else {
                int v3_5 = (- p7.getUpNestedPreScrollRange());
                if (v0_11 == 0) {
                    this.a(p6, p7, v3_5);
                } else {
                    this.c(p6, p7, v3_5);
                }
            }
            p7.b = 0;
        }
        this.a(p7);
        return v2;
    }

    final int a()
    {
        return (super.c() + this.c);
    }

    final int a(android.support.design.widget.CoordinatorLayout p7, android.support.design.widget.AppBarLayout p8, int p9)
    {
        return this.b(p7, p8, p9, -2147483648, 2147483647);
    }

    public final synthetic android.os.Parcelable a(android.support.design.widget.CoordinatorLayout p9, android.view.View p10)
    {
        android.support.design.widget.AppBarLayout$Behavior$SavedState v0_0 = 0;
        float v2_0 = super.a(p9, ((android.support.design.widget.AppBarLayout) p10));
        int v4 = super.c();
        int v5 = ((android.support.design.widget.AppBarLayout) p10).getChildCount();
        int v3 = 0;
        while (v3 < v5) {
            android.view.View v6 = ((android.support.design.widget.AppBarLayout) p10).getChildAt(v3);
            int v7 = (v6.getBottom() + v4);
            if (((v6.getTop() + v4) > 0) || (v7 < 0)) {
                v3++;
            } else {
                android.support.design.widget.AppBarLayout$Behavior$SavedState v1_5 = new android.support.design.widget.AppBarLayout$Behavior$SavedState(v2_0);
                v1_5.a = v3;
                if (v7 == android.support.v4.view.cx.o(v6)) {
                    v0_0 = 1;
                }
                v1_5.c = v0_0;
                v1_5.b = (((float) v7) / ((float) v6.getHeight()));
                android.support.design.widget.AppBarLayout$Behavior$SavedState v0_1 = v1_5;
            }
            return v0_1;
        }
        v0_1 = v2_0;
        return v0_1;
    }

    public final synthetic void a(android.support.design.widget.CoordinatorLayout p8, android.view.View p9, int p10, int[] p11)
    {
        if ((p10 != 0) && (!this.d)) {
            int v5;
            int v4;
            if (p10 >= 0) {
                v4 = (- ((android.support.design.widget.AppBarLayout) p9).getUpNestedPreScrollRange());
                v5 = 0;
            } else {
                v4 = (- ((android.support.design.widget.AppBarLayout) p9).getTotalScrollRange());
                v5 = (v4 + ((android.support.design.widget.AppBarLayout) p9).getDownNestedPreScrollRange());
            }
            p11[1] = this.a(p8, ((android.support.design.widget.AppBarLayout) p9), p10, v4, v5);
        }
        return;
    }

    public final synthetic void a(android.support.design.widget.CoordinatorLayout p2, android.view.View p3, android.os.Parcelable p4)
    {
        if (!(p4 instanceof android.support.design.widget.AppBarLayout$Behavior$SavedState)) {
            super.a(p2, ((android.support.design.widget.AppBarLayout) p3), p4);
            this.h = -1;
        } else {
            super.a(p2, ((android.support.design.widget.AppBarLayout) p3), ((android.support.design.widget.AppBarLayout$Behavior$SavedState) p4).getSuperState());
            this.h = ((android.support.design.widget.AppBarLayout$Behavior$SavedState) p4).a;
            this.j = ((android.support.design.widget.AppBarLayout$Behavior$SavedState) p4).b;
            this.i = ((android.support.design.widget.AppBarLayout$Behavior$SavedState) p4).c;
        }
        return;
    }

    public final synthetic void a(android.view.View p2)
    {
        this.d = 0;
        this.o = new ref.WeakReference(p2);
        return;
    }

    public final bridge synthetic boolean a(int p2)
    {
        return super.a(p2);
    }

    public final synthetic boolean a(android.support.design.widget.CoordinatorLayout p12, android.view.View p13, float p14, boolean p15)
    {
        int v1 = 0;
        if (p15) {
            Runnable v0_3;
            if (p14 >= 0) {
                v0_3 = (- ((android.support.design.widget.AppBarLayout) p13).getUpNestedPreScrollRange());
                if (this.a() < v0_3) {
                    return v1;
                }
            } else {
                v0_3 = ((- ((android.support.design.widget.AppBarLayout) p13).getTotalScrollRange()) + ((android.support.design.widget.AppBarLayout) p13).getDownNestedPreScrollRange());
                if (this.a() > v0_3) {
                    return v1;
                }
            }
            if (this.a() != v0_3) {
                this.c(p12, ((android.support.design.widget.AppBarLayout) p13), v0_3);
                v1 = 1;
            }
        } else {
            int v7 = (- ((android.support.design.widget.AppBarLayout) p13).getTotalScrollRange());
            int v3_0 = (- p14);
            if (this.e != null) {
                ((android.support.design.widget.AppBarLayout) p13).removeCallbacks(this.e);
            }
            if (this.f == null) {
                this.f = android.support.v4.widget.ca.a(((android.support.design.widget.AppBarLayout) p13).getContext(), 0);
            }
            this.f.a(0, this.a(), 0, Math.round(v3_0), 0, 0, v7, 0);
            if (!this.f.f()) {
                this.e = 0;
            } else {
                this.e = new android.support.design.widget.e(this, p12, ((android.support.design.widget.AppBarLayout) p13));
                android.support.v4.view.cx.a(((android.support.design.widget.AppBarLayout) p13), this.e);
                v1 = 1;
            }
        }
        return v1;
    }

    public final synthetic boolean a(android.support.design.widget.CoordinatorLayout p6, android.view.View p7, int p8)
    {
        boolean v2 = super.a(p6, ((android.support.design.widget.AppBarLayout) p7), p8);
        int v3_0 = ((android.support.design.widget.AppBarLayout) p7).getPendingAction();
        if (v3_0 == 0) {
            if (this.h >= 0) {
                int v0_7;
                int v0_2 = ((android.support.design.widget.AppBarLayout) p7).getChildAt(this.h);
                int v1_2 = (- v0_2.getBottom());
                if (!this.i) {
                    v0_7 = (Math.round((((float) v0_2.getHeight()) * this.j)) + v1_2);
                } else {
                    v0_7 = (android.support.v4.view.cx.o(v0_2) + v1_2);
                }
                super.b(v0_7);
                this.h = -1;
            }
        } else {
            int v0_11;
            if ((v3_0 & 4) == 0) {
                v0_11 = 0;
            } else {
                v0_11 = 1;
            }
            if ((v3_0 & 2) == 0) {
                if ((v3_0 & 1) != 0) {
                    if (v0_11 == 0) {
                        this.a(p6, ((android.support.design.widget.AppBarLayout) p7), 0);
                    } else {
                        this.c(p6, ((android.support.design.widget.AppBarLayout) p7), 0);
                    }
                }
            } else {
                int v3_5 = (- ((android.support.design.widget.AppBarLayout) p7).getUpNestedPreScrollRange());
                if (v0_11 == 0) {
                    this.a(p6, ((android.support.design.widget.AppBarLayout) p7), v3_5);
                } else {
                    this.c(p6, ((android.support.design.widget.AppBarLayout) p7), v3_5);
                }
            }
            ((android.support.design.widget.AppBarLayout) p7).b = 0;
        }
        this.a(((android.support.design.widget.AppBarLayout) p7));
        return v2;
    }

    public final synthetic boolean a(android.support.design.widget.CoordinatorLayout p8, android.view.View p9, android.view.MotionEvent p10)
    {
        int v5 = 0;
        if (this.n < 0) {
            this.n = android.view.ViewConfiguration.get(p8.getContext()).getScaledTouchSlop();
        }
        int v1_1 = ((int) p10.getY());
        switch (android.support.v4.view.bk.a(p10)) {
            case 0:
                if ((!p8.a(((android.support.design.widget.AppBarLayout) p9), ((int) p10.getX()), v1_1)) || (!this.d())) {
                } else {
                    this.m = v1_1;
                    this.l = android.support.v4.view.bk.b(p10, 0);
                    v5 = 1;
                }
                break;
            case 1:
            case 3:
                this.k = 0;
                this.l = -1;
                break;
            case 2:
                android.support.design.widget.AppBarLayout$Behavior v0_7 = android.support.v4.view.bk.a(p10, this.l);
                if (v0_7 == -1) {
                } else {
                    android.support.design.widget.AppBarLayout$Behavior v0_9 = ((int) android.support.v4.view.bk.d(p10, v0_7));
                    int v3_1 = (this.m - v0_9);
                    if ((!this.k) && (Math.abs(v3_1) > this.n)) {
                        this.k = 1;
                        if (v3_1 <= 0) {
                            v3_1 += this.n;
                        } else {
                            v3_1 -= this.n;
                        }
                    }
                    if (!this.k) {
                    } else {
                        this.m = v0_9;
                        this.a(p8, ((android.support.design.widget.AppBarLayout) p9), v3_1, (- ((android.support.design.widget.AppBarLayout) p9).getDownNestedScrollRange()), 0);
                    }
                }
                break;
            default:
        }
        return v5;
    }

    public final synthetic boolean a(android.support.design.widget.CoordinatorLayout p5, android.view.View p6, android.view.View p7, int p8)
    {
        int v0 = 1;
        if ((p8 & 2) == 0) {
            v0 = 0;
        } else {
            int v2_2;
            if (((android.support.design.widget.AppBarLayout) p6).getTotalScrollRange() == 0) {
                v2_2 = 0;
            } else {
                v2_2 = 1;
            }
            if ((v2_2 == 0) || ((p5.getHeight() - p7.getHeight()) > ((android.support.design.widget.AppBarLayout) p6).getHeight())) {
            }
        }
        if ((v0 != 0) && (this.g != null)) {
            this.g.a.e();
        }
        this.o = 0;
        return v0;
    }

    public final bridge synthetic int b()
    {
        return super.b();
    }

    public final synthetic void b(android.support.design.widget.CoordinatorLayout p7, android.view.View p8, int p9)
    {
        if (p9 >= 0) {
            this.d = 0;
        } else {
            this.a(p7, ((android.support.design.widget.AppBarLayout) p8), p9, (- ((android.support.design.widget.AppBarLayout) p8).getDownNestedScrollRange()), 0);
            this.d = 1;
        }
        return;
    }

    public final bridge synthetic boolean b(int p2)
    {
        return super.b(p2);
    }

    public final synthetic boolean b(android.support.design.widget.CoordinatorLayout p6, android.view.View p7, android.view.MotionEvent p8)
    {
        int v0_0 = 1;
        if (this.n < 0) {
            this.n = android.view.ViewConfiguration.get(p6.getContext()).getScaledTouchSlop();
        }
        if ((p8.getAction() != 2) || (!this.k)) {
            switch (android.support.v4.view.bk.a(p8)) {
                case 0:
                    this.k = 0;
                    int v1_12 = ((int) p8.getY());
                    if ((!p6.a(((android.support.design.widget.AppBarLayout) p7), ((int) p8.getX()), v1_12)) || (!this.d())) {
                    } else {
                        this.m = v1_12;
                        this.l = android.support.v4.view.bk.b(p8, 0);
                    }
                    break;
                case 1:
                case 3:
                    this.k = 0;
                    this.l = -1;
                    break;
                case 2:
                    int v1_7 = this.l;
                    if (v1_7 == -1) {
                    } else {
                        int v1_8 = android.support.v4.view.bk.a(p8, v1_7);
                        if (v1_8 == -1) {
                        } else {
                            int v1_10 = ((int) android.support.v4.view.bk.d(p8, v1_8));
                            if (Math.abs((v1_10 - this.m)) <= this.n) {
                            } else {
                                this.k = 1;
                                this.m = v1_10;
                            }
                        }
                    }
                    break;
            }
            v0_0 = this.k;
        }
        return v0_0;
    }

    public final bridge synthetic int c()
    {
        return super.c();
    }
}
