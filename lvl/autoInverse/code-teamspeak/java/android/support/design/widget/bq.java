package android.support.design.widget;
final class bq implements java.lang.Runnable {
    final synthetic android.support.design.widget.SwipeDismissBehavior a;
    private final android.view.View b;
    private final boolean c;

    bq(android.support.design.widget.SwipeDismissBehavior p1, android.view.View p2, boolean p3)
    {
        this.a = p1;
        this.b = p2;
        this.c = p3;
        return;
    }

    public final void run()
    {
        if ((android.support.design.widget.SwipeDismissBehavior.b(this.a) == null) || (!android.support.design.widget.SwipeDismissBehavior.b(this.a).c())) {
            if ((this.c) && (android.support.design.widget.SwipeDismissBehavior.a(this.a) != null)) {
                android.support.design.widget.SwipeDismissBehavior.a(this.a).a();
            }
        } else {
            android.support.v4.view.cx.a(this.b, this);
        }
        return;
    }
}
