package android.support.design.widget;
public class Snackbar$SnackbarLayout extends android.widget.LinearLayout {
    android.widget.TextView a;
    android.widget.Button b;
    private int c;
    private int d;
    private android.support.design.widget.bg e;

    public Snackbar$SnackbarLayout(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    public Snackbar$SnackbarLayout(android.content.Context p4, android.util.AttributeSet p5)
    {
        this(p4, p5);
        android.view.LayoutInflater v0_1 = p4.obtainStyledAttributes(p5, android.support.design.n.SnackbarLayout);
        this.c = v0_1.getDimensionPixelSize(android.support.design.n.SnackbarLayout_android_maxWidth, -1);
        this.d = v0_1.getDimensionPixelSize(android.support.design.n.SnackbarLayout_maxActionInlineWidth, -1);
        if (v0_1.hasValue(android.support.design.n.SnackbarLayout_elevation)) {
            android.support.v4.view.cx.f(this, ((float) v0_1.getDimensionPixelSize(android.support.design.n.SnackbarLayout_elevation, 0)));
        }
        v0_1.recycle();
        this.setClickable(1);
        android.view.LayoutInflater.from(p4).inflate(android.support.design.k.design_layout_snackbar_include, this);
        return;
    }

    private void a()
    {
        android.support.v4.view.cx.c(this.a, 0);
        android.support.v4.view.cx.p(this.a).a(1065353216).a(180).b(70).b();
        if (this.b.getVisibility() == 0) {
            android.support.v4.view.cx.c(this.b, 0);
            android.support.v4.view.cx.p(this.b).a(1065353216).a(180).b(70).b();
        }
        return;
    }

    private static void a(android.view.View p2, int p3, int p4)
    {
        if (!android.support.v4.view.cx.y(p2)) {
            p2.setPadding(p2.getPaddingLeft(), p3, p2.getPaddingRight(), p4);
        } else {
            android.support.v4.view.cx.b(p2, android.support.v4.view.cx.k(p2), p3, android.support.v4.view.cx.l(p2), p4);
        }
        return;
    }

    private boolean a(int p5, int p6, int p7)
    {
        int v0_0 = 0;
        if (p5 != this.getOrientation()) {
            this.setOrientation(p5);
            v0_0 = 1;
        }
        if ((this.a.getPaddingTop() != p6) || (this.a.getPaddingBottom() != p7)) {
            int v0_1 = this.a;
            if (!android.support.v4.view.cx.y(v0_1)) {
                v0_1.setPadding(v0_1.getPaddingLeft(), p6, v0_1.getPaddingRight(), p7);
            } else {
                android.support.v4.view.cx.b(v0_1, android.support.v4.view.cx.k(v0_1), p6, android.support.v4.view.cx.l(v0_1), p7);
            }
            v0_0 = 1;
        }
        return v0_0;
    }

    private void b()
    {
        android.support.v4.view.cx.c(this.a, 1065353216);
        android.support.v4.view.cx.p(this.a).a(0).a(180).b(0).b();
        if (this.b.getVisibility() == 0) {
            android.support.v4.view.cx.c(this.b, 1065353216);
            android.support.v4.view.cx.p(this.b).a(0).a(180).b(0).b();
        }
        return;
    }

    android.widget.Button getActionView()
    {
        return this.b;
    }

    android.widget.TextView getMessageView()
    {
        return this.a;
    }

    protected void onFinishInflate()
    {
        super.onFinishInflate();
        this.a = ((android.widget.TextView) this.findViewById(android.support.design.i.snackbar_text));
        this.b = ((android.widget.Button) this.findViewById(android.support.design.i.snackbar_action));
        return;
    }

    protected void onLayout(boolean p2, int p3, int p4, int p5, int p6)
    {
        this = super.onLayout(p2, p3, p4, p5, p6);
        if ((p2) && (this.e != null)) {
            this.e.a();
        }
        return;
    }

    protected void onMeasure(int p8, int p9)
    {
        super.onMeasure(p8, p9);
        if ((this.c > 0) && (this.getMeasuredWidth() > this.c)) {
            p8 = android.view.View$MeasureSpec.makeMeasureSpec(this.c, 1073741824);
            super.onMeasure(p8, p9);
        }
        int v4_4;
        int v0_4 = this.getResources().getDimensionPixelSize(android.support.design.g.design_snackbar_padding_vertical_2lines);
        int v1_4 = this.getResources().getDimensionPixelSize(android.support.design.g.design_snackbar_padding_vertical);
        if (this.a.getLayout().getLineCount() <= 1) {
            v4_4 = 0;
        } else {
            v4_4 = 1;
        }
        if ((v4_4 == 0) || ((this.d <= 0) || (this.b.getMeasuredWidth() <= this.d))) {
            if (v4_4 == 0) {
                v0_4 = v1_4;
            }
            if (!this.a(0, v0_4, v0_4)) {
                int v0_6 = 0;
            } else {
                v0_6 = 1;
            }
        } else {
            if (!this.a(1, v0_4, (v0_4 - v1_4))) {
            } else {
                v0_6 = 1;
            }
        }
        if (v0_6 != 0) {
            super.onMeasure(p8, p9);
        }
        return;
    }

    void setOnLayoutChangeListener(android.support.design.widget.bg p1)
    {
        this.e = p1;
        return;
    }
}
