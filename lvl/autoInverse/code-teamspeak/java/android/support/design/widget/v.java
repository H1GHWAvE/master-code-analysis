package android.support.design.widget;
final class v implements android.view.ViewGroup$OnHierarchyChangeListener {
    final synthetic android.support.design.widget.CoordinatorLayout a;

    v(android.support.design.widget.CoordinatorLayout p1)
    {
        this.a = p1;
        return;
    }

    public final void onChildViewAdded(android.view.View p2, android.view.View p3)
    {
        if (android.support.design.widget.CoordinatorLayout.a(this.a) != null) {
            android.support.design.widget.CoordinatorLayout.a(this.a).onChildViewAdded(p2, p3);
        }
        return;
    }

    public final void onChildViewRemoved(android.view.View p7, android.view.View p8)
    {
        android.support.design.widget.CoordinatorLayout v2 = this.a;
        int v3 = v2.getChildCount();
        int v1 = 0;
        while (v1 < v3) {
            android.view.View v4 = v2.getChildAt(v1);
            android.view.ViewGroup$OnHierarchyChangeListener v0_7 = ((android.support.design.widget.w) v4.getLayoutParams()).a;
            if ((v0_7 != null) && (v0_7.b(p8))) {
                v0_7.a(v4, p8);
            }
            v1++;
        }
        if (android.support.design.widget.CoordinatorLayout.a(this.a) != null) {
            android.support.design.widget.CoordinatorLayout.a(this.a).onChildViewRemoved(p7, p8);
        }
        return;
    }
}
