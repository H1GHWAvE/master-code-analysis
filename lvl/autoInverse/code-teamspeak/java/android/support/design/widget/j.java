package android.support.design.widget;
 class j extends android.graphics.drawable.Drawable {
    private static final float k = 1068149139;
    final android.graphics.Paint a;
    final android.graphics.Rect b;
    final android.graphics.RectF c;
    float d;
    int e;
    int f;
    int g;
    int h;
    int i;
    boolean j;

    public j()
    {
        this.b = new android.graphics.Rect();
        this.c = new android.graphics.RectF();
        this.j = 1;
        this.a = new android.graphics.Paint(1);
        this.a.setStyle(android.graphics.Paint$Style.STROKE);
        return;
    }

    private android.graphics.Shader a()
    {
        int v3_0 = this.b;
        this.copyBounds(v3_0);
        android.graphics.LinearGradient v0_1 = (this.d / ((float) v3_0.height()));
        int[] v5 = new int[6];
        v5[0] = android.support.v4.e.j.a(this.e, this.i);
        v5[1] = android.support.v4.e.j.a(this.f, this.i);
        v5[2] = android.support.v4.e.j.a(android.support.v4.e.j.b(this.f, 0), this.i);
        v5[3] = android.support.v4.e.j.a(android.support.v4.e.j.b(this.h, 0), this.i);
        v5[4] = android.support.v4.e.j.a(this.h, this.i);
        v5[5] = android.support.v4.e.j.a(this.g, this.i);
        float[] v6_4 = new float[6];
        v6_4[0] = 0;
        v6_4[1] = v0_1;
        v6_4[2] = 1056964608;
        v6_4[3] = 1056964608;
        v6_4[4] = (1065353216 - v0_1);
        v6_4[5] = 1065353216;
        return new android.graphics.LinearGradient(0, ((float) v3_0.top), 0, ((float) v3_0.bottom), v5, v6_4, android.graphics.Shader$TileMode.CLAMP);
    }

    private void a(float p3)
    {
        if (this.d != p3) {
            this.d = p3;
            this.a.setStrokeWidth((1068149139 * p3));
            this.j = 1;
            this.invalidateSelf();
        }
        return;
    }

    private void a(int p2)
    {
        this.i = p2;
        this.j = 1;
        this.invalidateSelf();
        return;
    }

    private void a(int p1, int p2, int p3, int p4)
    {
        this.e = p1;
        this.f = p2;
        this.g = p3;
        this.h = p4;
        return;
    }

    public void draw(android.graphics.Canvas p13)
    {
        if (this.j) {
            android.graphics.Paint v8 = this.a;
            int v3_0 = this.b;
            this.copyBounds(v3_0);
            android.graphics.Paint v0_2 = (this.d / ((float) v3_0.height()));
            int[] v5 = new int[6];
            v5[0] = android.support.v4.e.j.a(this.e, this.i);
            v5[1] = android.support.v4.e.j.a(this.f, this.i);
            v5[2] = android.support.v4.e.j.a(android.support.v4.e.j.b(this.f, 0), this.i);
            v5[3] = android.support.v4.e.j.a(android.support.v4.e.j.b(this.h, 0), this.i);
            v5[4] = android.support.v4.e.j.a(this.h, this.i);
            v5[5] = android.support.v4.e.j.a(this.g, this.i);
            float[] v6_4 = new float[6];
            v6_4[0] = 0;
            v6_4[1] = v0_2;
            v6_4[2] = 1056964608;
            v6_4[3] = 1056964608;
            v6_4[4] = (1065353216 - v0_2);
            v6_4[5] = 1065353216;
            v8.setShader(new android.graphics.LinearGradient(0, ((float) v3_0.top), 0, ((float) v3_0.bottom), v5, v6_4, android.graphics.Shader$TileMode.CLAMP));
            this.j = 0;
        }
        android.graphics.Paint v0_9 = (this.a.getStrokeWidth() / 1073741824);
        android.graphics.RectF v1_2 = this.c;
        this.copyBounds(this.b);
        v1_2.set(this.b);
        v1_2.left = (v1_2.left + v0_9);
        v1_2.top = (v1_2.top + v0_9);
        v1_2.right = (v1_2.right - v0_9);
        v1_2.bottom = (v1_2.bottom - v0_9);
        p13.drawOval(v1_2, this.a);
        return;
    }

    public int getOpacity()
    {
        int v0_2;
        if (this.d <= 0) {
            v0_2 = -2;
        } else {
            v0_2 = -3;
        }
        return v0_2;
    }

    public boolean getPadding(android.graphics.Rect p2)
    {
        p2.set(Math.round(this.d), Math.round(this.d), Math.round(this.d), Math.round(this.d));
        return 1;
    }

    protected void onBoundsChange(android.graphics.Rect p2)
    {
        this.j = 1;
        return;
    }

    public void setAlpha(int p2)
    {
        this.a.setAlpha(p2);
        this.invalidateSelf();
        return;
    }

    public void setColorFilter(android.graphics.ColorFilter p2)
    {
        this.a.setColorFilter(p2);
        this.invalidateSelf();
        return;
    }
}
