package android.support.design.widget;
final class bo extends android.support.v4.widget.ej {
    final synthetic android.support.design.widget.SwipeDismissBehavior a;
    private int b;

    bo(android.support.design.widget.SwipeDismissBehavior p1)
    {
        this.a = p1;
        return;
    }

    private boolean b(android.view.View p7, float p8)
    {
        int v1 = 1;
        if (p8 == 0) {
            if (Math.abs((p7.getLeft() - this.b)) < Math.round((((float) p7.getWidth()) * android.support.design.widget.SwipeDismissBehavior.d(this.a)))) {
                v1 = 0;
            }
        } else {
            float v0_5;
            if (android.support.v4.view.cx.f(p7) != 1) {
                v0_5 = 0;
            } else {
                v0_5 = 1;
            }
            if (android.support.design.widget.SwipeDismissBehavior.c(this.a) != 2) {
                if (android.support.design.widget.SwipeDismissBehavior.c(this.a) != 0) {
                    if (android.support.design.widget.SwipeDismissBehavior.c(this.a) != 1) {
                        v1 = 0;
                    } else {
                        if (v0_5 == 0) {
                            if (p8 >= 0) {
                                v1 = 0;
                            }
                        } else {
                            if (p8 <= 0) {
                                v1 = 0;
                            }
                        }
                    }
                } else {
                    if (v0_5 == 0) {
                        if (p8 <= 0) {
                            v1 = 0;
                        }
                    } else {
                        if (p8 >= 0) {
                            v1 = 0;
                        }
                    }
                }
            }
        }
        return v1;
    }

    public final int a(android.view.View p4, int p5)
    {
        int v0_1;
        if (android.support.v4.view.cx.f(p4) != 1) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        int v1_3;
        int v0_3;
        if (android.support.design.widget.SwipeDismissBehavior.c(this.a) != 0) {
            if (android.support.design.widget.SwipeDismissBehavior.c(this.a) != 1) {
                v0_3 = (this.b - p4.getWidth());
                v1_3 = (this.b + p4.getWidth());
            } else {
                if (v0_1 == 0) {
                    v0_3 = (this.b - p4.getWidth());
                    v1_3 = this.b;
                } else {
                    v0_3 = this.b;
                    v1_3 = (this.b + p4.getWidth());
                }
            }
        } else {
            if (v0_1 == 0) {
                v0_3 = this.b;
                v1_3 = (this.b + p4.getWidth());
            } else {
                v0_3 = (this.b - p4.getWidth());
                v1_3 = this.b;
            }
        }
        return android.support.design.widget.SwipeDismissBehavior.a(v0_3, p5, v1_3);
    }

    public final void a(int p2)
    {
        if (android.support.design.widget.SwipeDismissBehavior.a(this.a) != null) {
            android.support.design.widget.SwipeDismissBehavior.a(this.a).a(p2);
        }
        return;
    }

    public final void a(android.view.View p8, float p9)
    {
        android.support.design.widget.bp v0_4;
        int v1 = 1;
        int v3_0 = p8.getWidth();
        if (p9 == 0) {
            if (Math.abs((p8.getLeft() - this.b)) < Math.round((((float) p8.getWidth()) * android.support.design.widget.SwipeDismissBehavior.d(this.a)))) {
                v0_4 = 0;
            } else {
                v0_4 = 1;
            }
        } else {
            android.support.design.widget.bp v0_6;
            if (android.support.v4.view.cx.f(p8) != 1) {
                v0_6 = 0;
            } else {
                v0_6 = 1;
            }
            if (android.support.design.widget.SwipeDismissBehavior.c(this.a) != 2) {
                if (android.support.design.widget.SwipeDismissBehavior.c(this.a) != 0) {
                    if (android.support.design.widget.SwipeDismissBehavior.c(this.a) != 1) {
                        v0_4 = 0;
                    } else {
                        if (v0_6 == null) {
                            if (p9 >= 0) {
                                v0_4 = 0;
                            } else {
                                v0_4 = 1;
                            }
                        } else {
                            if (p9 <= 0) {
                                v0_4 = 0;
                            } else {
                                v0_4 = 1;
                            }
                        }
                    }
                } else {
                    if (v0_6 == null) {
                        if (p9 <= 0) {
                            v0_4 = 0;
                        } else {
                            v0_4 = 1;
                        }
                    } else {
                        if (p9 >= 0) {
                            v0_4 = 0;
                        } else {
                            v0_4 = 1;
                        }
                    }
                }
            } else {
                v0_4 = 1;
            }
        }
        android.support.design.widget.bp v0_11;
        if (v0_4 == null) {
            v0_11 = this.b;
            v1 = 0;
        } else {
            if (p8.getLeft() >= this.b) {
                v0_11 = (this.b + v3_0);
            } else {
                v0_11 = (this.b - v3_0);
            }
        }
        if (!android.support.design.widget.SwipeDismissBehavior.b(this.a).a(v0_11, p8.getTop())) {
            if ((v1 != 0) && (android.support.design.widget.SwipeDismissBehavior.a(this.a) != null)) {
                android.support.design.widget.SwipeDismissBehavior.a(this.a).a();
            }
        } else {
            android.support.v4.view.cx.a(p8, new android.support.design.widget.bq(this.a, p8, v1));
        }
        return;
    }

    public final boolean a(android.view.View p2)
    {
        this.b = p2.getLeft();
        return 1;
    }

    public final int b(android.view.View p2)
    {
        return p2.getWidth();
    }

    public final void b(android.view.View p6, int p7)
    {
        float v0_2 = (((float) this.b) + (((float) p6.getWidth()) * android.support.design.widget.SwipeDismissBehavior.e(this.a)));
        float v1_5 = (((float) this.b) + (((float) p6.getWidth()) * android.support.design.widget.SwipeDismissBehavior.f(this.a)));
        if (((float) p7) > v0_2) {
            if (((float) p7) < v1_5) {
                android.support.v4.view.cx.c(p6, android.support.design.widget.SwipeDismissBehavior.b((1065353216 - android.support.design.widget.SwipeDismissBehavior.a(v0_2, v1_5, ((float) p7)))));
            } else {
                android.support.v4.view.cx.c(p6, 0);
            }
        } else {
            android.support.v4.view.cx.c(p6, 1065353216);
        }
        return;
    }

    public final int c(android.view.View p2)
    {
        return p2.getTop();
    }
}
