package android.support.design.widget;
public class SwipeDismissBehavior extends android.support.design.widget.t {
    private static final float a = 63;
    public static final int b = 0;
    public static final int c = 1;
    public static final int d = 2;
    public static final int e = 0;
    public static final int f = 1;
    public static final int g = 2;
    private static final float m = 0;
    private static final float n = 63;
    android.support.v4.widget.eg h;
    android.support.design.widget.bp i;
    int j;
    float k;
    float l;
    private boolean o;
    private float p;
    private boolean q;
    private float r;
    private final android.support.v4.widget.ej s;

    public SwipeDismissBehavior()
    {
        this.p = 0;
        this.j = 2;
        this.r = 1056964608;
        this.k = 0;
        this.l = 1056964608;
        this.s = new android.support.design.widget.bo(this);
        return;
    }

    static float a(float p2)
    {
        return Math.min(Math.max(0, p2), 1065353216);
    }

    static float a(float p2, float p3, float p4)
    {
        return ((p4 - p2) / (p3 - p2));
    }

    static synthetic int a(int p1, int p2, int p3)
    {
        return Math.min(Math.max(p1, p2), p3);
    }

    static synthetic android.support.design.widget.bp a(android.support.design.widget.SwipeDismissBehavior p1)
    {
        return p1.i;
    }

    private void a()
    {
        this.j = 0;
        return;
    }

    private void a(android.support.design.widget.bp p1)
    {
        this.i = p1;
        return;
    }

    private void a(android.view.ViewGroup p3)
    {
        if (this.h == null) {
            android.support.v4.widget.eg v0_3;
            if (!this.q) {
                v0_3 = android.support.v4.widget.eg.a(p3, this.s);
            } else {
                v0_3 = android.support.v4.widget.eg.a(p3, this.p, this.s);
            }
            this.h = v0_3;
        }
        return;
    }

    static synthetic float b(float p1)
    {
        return android.support.design.widget.SwipeDismissBehavior.a(p1);
    }

    private static int b(int p1, int p2, int p3)
    {
        return Math.min(Math.max(p1, p2), p3);
    }

    static synthetic android.support.v4.widget.eg b(android.support.design.widget.SwipeDismissBehavior p1)
    {
        return p1.h;
    }

    private void b()
    {
        this.k = android.support.design.widget.SwipeDismissBehavior.a(1036831949);
        return;
    }

    static synthetic int c(android.support.design.widget.SwipeDismissBehavior p1)
    {
        return p1.j;
    }

    private void c()
    {
        this.l = android.support.design.widget.SwipeDismissBehavior.a(1058642330);
        return;
    }

    private void c(float p2)
    {
        this.r = android.support.design.widget.SwipeDismissBehavior.a(p2);
        return;
    }

    static synthetic float d(android.support.design.widget.SwipeDismissBehavior p1)
    {
        return p1.r;
    }

    private int d()
    {
        int v0_1;
        if (this.h == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.h.m;
        }
        return v0_1;
    }

    private void d(float p2)
    {
        this.p = p2;
        this.q = 1;
        return;
    }

    static synthetic float e(android.support.design.widget.SwipeDismissBehavior p1)
    {
        return p1.k;
    }

    static synthetic float f(android.support.design.widget.SwipeDismissBehavior p1)
    {
        return p1.l;
    }

    public final boolean a(android.support.design.widget.CoordinatorLayout p2, android.view.View p3, android.view.MotionEvent p4)
    {
        int v0_1;
        if (this.h == null) {
            v0_1 = 0;
        } else {
            this.h.b(p4);
            v0_1 = 1;
        }
        return v0_1;
    }

    public boolean b(android.support.design.widget.CoordinatorLayout p4, android.view.View p5, android.view.MotionEvent p6)
    {
        android.support.v4.widget.ej v1_0 = 0;
        switch (android.support.v4.view.bk.a(p6)) {
            case 1:
            case 3:
                if (!this.o) {
                    if (!this.o) {
                        if (this.h == null) {
                            android.support.v4.widget.eg v0_10;
                            if (!this.q) {
                                v0_10 = android.support.v4.widget.eg.a(p4, this.s);
                            } else {
                                v0_10 = android.support.v4.widget.eg.a(p4, this.p, this.s);
                            }
                            this.h = v0_10;
                        }
                        v1_0 = this.h.a(p6);
                    } else {
                    }
                } else {
                    this.o = 0;
                }
                break;
            case 2:
            default:
                android.support.v4.widget.eg v0_5;
                if (p4.a(p5, ((int) p6.getX()), ((int) p6.getY()))) {
                    v0_5 = 0;
                } else {
                    v0_5 = 1;
                }
                this.o = v0_5;
                break;
        }
        return v1_0;
    }
}
