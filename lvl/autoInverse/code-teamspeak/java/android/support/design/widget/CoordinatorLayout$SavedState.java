package android.support.design.widget;
public class CoordinatorLayout$SavedState extends android.view.View$BaseSavedState {
    public static final android.os.Parcelable$Creator CREATOR;
    android.util.SparseArray a;

    static CoordinatorLayout$SavedState()
    {
        android.support.design.widget.CoordinatorLayout$SavedState.CREATOR = new android.support.design.widget.y();
        return;
    }

    public CoordinatorLayout$SavedState(android.os.Parcel p8)
    {
        this(p8);
        int v1 = p8.readInt();
        int[] v2 = new int[v1];
        p8.readIntArray(v2);
        android.os.Parcelable[] v3 = p8.readParcelableArray(android.support.design.widget.CoordinatorLayout.getClassLoader());
        this.a = new android.util.SparseArray(v1);
        int v0_4 = 0;
        while (v0_4 < v1) {
            this.a.append(v2[v0_4], v3[v0_4]);
            v0_4++;
        }
        return;
    }

    public CoordinatorLayout$SavedState(android.os.Parcelable p1)
    {
        this(p1);
        return;
    }

    public void writeToParcel(android.os.Parcel p6, int p7)
    {
        int v1;
        int v2 = 0;
        super.writeToParcel(p6, p7);
        if (this.a == null) {
            v1 = 0;
        } else {
            v1 = this.a.size();
        }
        p6.writeInt(v1);
        int[] v3 = new int[v1];
        android.os.Parcelable[] v4 = new android.os.Parcelable[v1];
        while (v2 < v1) {
            v3[v2] = this.a.keyAt(v2);
            v4[v2] = ((android.os.Parcelable) this.a.valueAt(v2));
            v2++;
        }
        p6.writeIntArray(v3);
        p6.writeParcelableArray(v4, p7);
        return;
    }
}
