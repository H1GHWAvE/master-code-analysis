package android.support.design.widget;
final class ar extends android.support.v7.b.a.b {
    static final double a = 0;
    static final float b = 16320;
    static final float c = 16000;
    static final float d = 63;
    static final float e = 16256;
    final android.graphics.Paint f;
    final android.graphics.Paint g;
    final android.graphics.RectF h;
    float i;
    android.graphics.Path j;
    float k;
    float l;
    float m;
    float n;
    boolean o;
    private boolean q;
    private final int r;
    private final int s;
    private final int t;
    private boolean u;

    static ar()
    {
        android.support.design.widget.ar.a = Math.cos(Math.toRadians(45.0));
        return;
    }

    public ar(android.content.res.Resources p4, android.graphics.drawable.Drawable p5, float p6, float p7, float p8)
    {
        this(p5);
        this.q = 1;
        this.o = 1;
        this.u = 0;
        this.r = p4.getColor(android.support.design.f.design_fab_shadow_start_color);
        this.s = p4.getColor(android.support.design.f.design_fab_shadow_mid_color);
        this.t = p4.getColor(android.support.design.f.design_fab_shadow_end_color);
        this.f = new android.graphics.Paint(5);
        this.f.setStyle(android.graphics.Paint$Style.FILL);
        this.i = ((float) Math.round(p6));
        this.h = new android.graphics.RectF();
        this.g = new android.graphics.Paint(this.f);
        this.g.setAntiAlias(0);
        this.a(p7, p8);
        return;
    }

    private static float a(float p6, float p7, boolean p8)
    {
        float v0_1;
        if (!p8) {
            v0_1 = (1069547520 * p6);
        } else {
            v0_1 = ((float) (((double) (1069547520 * p6)) + ((1.0 - android.support.design.widget.ar.a) * ((double) p7))));
        }
        return v0_1;
    }

    private static int a(float p3)
    {
        int v0 = Math.round(p3);
        if ((v0 % 2) == 1) {
            v0--;
        }
        return v0;
    }

    private void a()
    {
        this.o = 0;
        this.invalidateSelf();
        return;
    }

    private void a(android.graphics.Canvas p14)
    {
        int v6_0;
        float v2 = ((- this.i) - this.m);
        float v8 = this.i;
        if ((this.h.width() - (1073741824 * v8)) <= 0) {
            v6_0 = 0;
        } else {
            v6_0 = 1;
        }
        int v7;
        if ((this.h.height() - (1073741824 * v8)) <= 0) {
            v7 = 0;
        } else {
            v7 = 1;
        }
        float v9 = (v8 / ((this.n - (this.n * 1056964608)) + v8));
        float v10 = (v8 / ((this.n - (this.n * 1048576000)) + v8));
        float v11 = (v8 / (v8 + (this.n - (this.n * 1065353216))));
        int v12_0 = p14.save();
        p14.translate((this.h.left + v8), (this.h.top + v8));
        p14.scale(v9, v10);
        p14.drawPath(this.j, this.f);
        if (v6_0 != 0) {
            p14.scale((1065353216 / v9), 1065353216);
            p14.drawRect(0, v2, (this.h.width() - (1073741824 * v8)), (- this.i), this.g);
        }
        p14.restoreToCount(v12_0);
        int v12_1 = p14.save();
        p14.translate((this.h.right - v8), (this.h.bottom - v8));
        p14.scale(v9, v11);
        p14.rotate(1127481344);
        p14.drawPath(this.j, this.f);
        if (v6_0 != 0) {
            p14.scale((1065353216 / v9), 1065353216);
            p14.drawRect(0, v2, (this.h.width() - (1073741824 * v8)), (this.m + (- this.i)), this.g);
        }
        p14.restoreToCount(v12_1);
        int v6_1 = p14.save();
        p14.translate((this.h.left + v8), (this.h.bottom - v8));
        p14.scale(v9, v11);
        p14.rotate(1132920832);
        p14.drawPath(this.j, this.f);
        if (v7 != 0) {
            p14.scale((1065353216 / v11), 1065353216);
            p14.drawRect(0, v2, (this.h.height() - (1073741824 * v8)), (- this.i), this.g);
        }
        p14.restoreToCount(v6_1);
        int v6_2 = p14.save();
        p14.translate((this.h.right - v8), (this.h.top + v8));
        p14.scale(v9, v10);
        p14.rotate(1119092736);
        p14.drawPath(this.j, this.f);
        if (v7 != 0) {
            p14.scale((1065353216 / v10), 1065353216);
            p14.drawRect(0, v2, (this.h.height() - (1073741824 * v8)), (- this.i), this.g);
        }
        p14.restoreToCount(v6_2);
        return;
    }

    private void a(android.graphics.Rect p15)
    {
        android.graphics.Paint v0_1 = (this.l * 1069547520);
        this.h.set((((float) p15.left) + this.l), (((float) p15.top) + v0_1), (((float) p15.right) - this.l), (((float) p15.bottom) - v0_1));
        this.p.setBounds(((int) this.h.left), ((int) this.h.top), ((int) this.h.right), ((int) this.h.bottom));
        android.graphics.Shader$TileMode v7_1 = new android.graphics.RectF((- this.i), (- this.i), this.i, this.i);
        android.graphics.RectF v8_1 = new android.graphics.RectF(v7_1);
        v8_1.inset((- this.m), (- this.m));
        if (this.j != null) {
            this.j.reset();
        } else {
            this.j = new android.graphics.Path();
        }
        this.j.setFillType(android.graphics.Path$FillType.EVEN_ODD);
        this.j.moveTo((- this.i), 0);
        this.j.rLineTo((- this.m), 0);
        this.j.arcTo(v8_1, 1127481344, 1119092736, 0);
        this.j.arcTo(v7_1, 1132920832, -1028390912, 0);
        this.j.close();
        int v3_9 = (- v8_1.top);
        if (v3_9 > 0) {
            float v2_16 = (this.i / v3_9);
            float[] v6_3 = (v2_16 + ((1065353216 - v2_16) / 1073741824));
            android.graphics.Paint v9_0 = this.f;
            float v4_10 = new int[4];
            v4_10[0] = 0;
            v4_10[1] = this.r;
            v4_10[2] = this.s;
            v4_10[3] = this.t;
            int[] v5_10 = new float[4];
            v5_10[0] = 0;
            v5_10[1] = v2_16;
            v5_10[2] = v6_3;
            v5_10[3] = 1065353216;
            v9_0.setShader(new android.graphics.RadialGradient(0, 0, v3_9, v4_10, v5_10, android.graphics.Shader$TileMode.CLAMP));
        }
        android.graphics.Paint v9_1 = this.g;
        float v2_19 = v7_1.top;
        float v4_11 = v8_1.top;
        int[] v5_11 = new int[3];
        v5_11[0] = this.r;
        v5_11[1] = this.s;
        v5_11[2] = this.t;
        float[] v6_5 = new float[3];
        v6_5 = {0, 1056964608, 1065353216};
        v9_1.setShader(new android.graphics.LinearGradient(0, v2_19, 0, v4_11, v5_11, v6_5, android.graphics.Shader$TileMode.CLAMP));
        this.g.setAntiAlias(0);
        return;
    }

    private static float b(float p6, float p7, boolean p8)
    {
        if (p8) {
            p6 = ((float) (((double) p6) + ((1.0 - android.support.design.widget.ar.a) * ((double) p7))));
        }
        return p6;
    }

    private void b()
    {
        android.graphics.Shader$TileMode v7_1 = new android.graphics.RectF((- this.i), (- this.i), this.i, this.i);
        android.graphics.RectF v8_1 = new android.graphics.RectF(v7_1);
        v8_1.inset((- this.m), (- this.m));
        if (this.j != null) {
            this.j.reset();
        } else {
            this.j = new android.graphics.Path();
        }
        this.j.setFillType(android.graphics.Path$FillType.EVEN_ODD);
        this.j.moveTo((- this.i), 0);
        this.j.rLineTo((- this.m), 0);
        this.j.arcTo(v8_1, 1127481344, 1119092736, 0);
        this.j.arcTo(v7_1, 1132920832, -1028390912, 0);
        this.j.close();
        int v3_3 = (- v8_1.top);
        if (v3_3 > 0) {
            float v2_11 = (this.i / v3_3);
            float[] v6_0 = (v2_11 + ((1065353216 - v2_11) / 1073741824));
            android.graphics.Paint v9_0 = this.f;
            float v4_3 = new int[4];
            v4_3[0] = 0;
            v4_3[1] = this.r;
            v4_3[2] = this.s;
            v4_3[3] = this.t;
            int[] v5_4 = new float[4];
            v5_4[0] = 0;
            v5_4[1] = v2_11;
            v5_4[2] = v6_0;
            v5_4[3] = 1065353216;
            v9_0.setShader(new android.graphics.RadialGradient(0, 0, v3_3, v4_3, v5_4, android.graphics.Shader$TileMode.CLAMP));
        }
        android.graphics.Paint v9_1 = this.g;
        float v2_14 = v7_1.top;
        float v4_4 = v8_1.top;
        int[] v5_5 = new int[3];
        v5_5[0] = this.r;
        v5_5[1] = this.s;
        v5_5[2] = this.t;
        float[] v6_2 = new float[3];
        v6_2 = {0, 1056964608, 1065353216};
        v9_1.setShader(new android.graphics.LinearGradient(0, v2_14, 0, v4_4, v5_5, v6_2, android.graphics.Shader$TileMode.CLAMP));
        this.g.setAntiAlias(0);
        return;
    }

    private void b(float p3)
    {
        int v0_1 = ((float) Math.round(p3));
        if (this.i != v0_1) {
            this.i = v0_1;
            this.q = 1;
            this.invalidateSelf();
        }
        return;
    }

    private float c()
    {
        return this.i;
    }

    private void c(float p2)
    {
        this.a(p2, this.l);
        return;
    }

    private float d()
    {
        return this.n;
    }

    private void d(float p2)
    {
        this.a(this.n, p2);
        return;
    }

    private float e()
    {
        return this.l;
    }

    private float f()
    {
        return ((Math.max(this.l, (this.i + (this.l / 1073741824))) * 1073741824) + (this.l * 1073741824));
    }

    private float g()
    {
        return ((Math.max(this.l, (this.i + ((this.l * 1069547520) / 1073741824))) * 1073741824) + ((this.l * 1069547520) * 1073741824));
    }

    final void a(float p5, float p6)
    {
        if ((p5 >= 0) && (p6 >= 0)) {
            float v0_3 = ((float) android.support.design.widget.ar.a(p5));
            float v1_2 = ((float) android.support.design.widget.ar.a(p6));
            if (v0_3 > v1_2) {
                if (!this.u) {
                    this.u = 1;
                }
                v0_3 = v1_2;
            }
            if ((this.n != v0_3) || (this.l != v1_2)) {
                this.n = v0_3;
                this.l = v1_2;
                this.m = ((float) Math.round((v0_3 * 1069547520)));
                this.k = v1_2;
                this.q = 1;
                this.invalidateSelf();
            }
            return;
        } else {
            throw new IllegalArgumentException("invalid shadow size");
        }
    }

    public final void draw(android.graphics.Canvas p14)
    {
        if (this.q) {
            android.graphics.Canvas v0_1 = this.getBounds();
            int v1_1 = (this.l * 1069547520);
            this.h.set((((float) v0_1.left) + this.l), (((float) v0_1.top) + v1_1), (((float) v0_1.right) - this.l), (((float) v0_1.bottom) - v1_1));
            this.p.setBounds(((int) this.h.left), ((int) this.h.top), ((int) this.h.right), ((int) this.h.bottom));
            int v7_1 = new android.graphics.RectF((- this.i), (- this.i), this.i, this.i);
            float v8_1 = new android.graphics.RectF(v7_1);
            v8_1.inset((- this.m), (- this.m));
            if (this.j != null) {
                this.j.reset();
            } else {
                this.j = new android.graphics.Path();
            }
            this.j.setFillType(android.graphics.Path$FillType.EVEN_ODD);
            this.j.moveTo((- this.i), 0);
            this.j.rLineTo((- this.m), 0);
            this.j.arcTo(v8_1, 1127481344, 1119092736, 0);
            this.j.arcTo(v7_1, 1132920832, -1028390912, 0);
            this.j.close();
            float v3_9 = (- v8_1.top);
            if (v3_9 > 0) {
                int v6_1 = (this.i / v3_9);
                float v9_0 = (v6_1 + ((1065353216 - v6_1) / 1073741824));
                float v10_0 = this.f;
                float v4_8 = new int[4];
                v4_8[0] = 0;
                v4_8[1] = this.r;
                v4_8[2] = this.s;
                v4_8[3] = this.t;
                android.graphics.Paint v5_8 = new float[4];
                v5_8[0] = 0;
                v5_8[1] = v6_1;
                v5_8[2] = v9_0;
                v5_8[3] = 1065353216;
                v10_0.setShader(new android.graphics.RadialGradient(0, 0, v3_9, v4_8, v5_8, android.graphics.Shader$TileMode.CLAMP));
            }
            float v9_2 = this.g;
            float v2_11 = v7_1.top;
            float v4_9 = v8_1.top;
            android.graphics.Paint v5_10 = new int[3];
            v5_10[0] = this.r;
            v5_10[1] = this.s;
            v5_10[2] = this.t;
            int v6_9 = new float[3];
            v6_9 = {0, 1056964608, 1065353216};
            v9_2.setShader(new android.graphics.LinearGradient(0, v2_11, 0, v4_9, v5_10, v6_9, android.graphics.Shader$TileMode.CLAMP));
            this.g.setAntiAlias(0);
            this.q = 0;
        }
        int v6_10;
        float v2_12 = ((- this.i) - this.m);
        float v8_2 = this.i;
        if ((this.h.width() - (1073741824 * v8_2)) <= 0) {
            v6_10 = 0;
        } else {
            v6_10 = 1;
        }
        int v7_6;
        if ((this.h.height() - (1073741824 * v8_2)) <= 0) {
            v7_6 = 0;
        } else {
            v7_6 = 1;
        }
        float v9_3 = (v8_2 / ((this.n - (this.n * 1056964608)) + v8_2));
        float v10_1 = (v8_2 / ((this.n - (this.n * 1048576000)) + v8_2));
        float v11_6 = (v8_2 / (v8_2 + (this.n - (this.n * 1065353216))));
        int v12_1 = p14.save();
        p14.translate((this.h.left + v8_2), (this.h.top + v8_2));
        p14.scale(v9_3, v10_1);
        p14.drawPath(this.j, this.f);
        if (v6_10 != 0) {
            p14.scale((1065353216 / v9_3), 1065353216);
            p14.drawRect(0, v2_12, (this.h.width() - (1073741824 * v8_2)), (- this.i), this.g);
        }
        p14.restoreToCount(v12_1);
        int v12_2 = p14.save();
        p14.translate((this.h.right - v8_2), (this.h.bottom - v8_2));
        p14.scale(v9_3, v11_6);
        p14.rotate(1127481344);
        p14.drawPath(this.j, this.f);
        if (v6_10 != 0) {
            p14.scale((1065353216 / v9_3), 1065353216);
            p14.drawRect(0, v2_12, (this.h.width() - (1073741824 * v8_2)), (this.m + (- this.i)), this.g);
        }
        p14.restoreToCount(v12_2);
        int v6_11 = p14.save();
        p14.translate((this.h.left + v8_2), (this.h.bottom - v8_2));
        p14.scale(v9_3, v11_6);
        p14.rotate(1132920832);
        p14.drawPath(this.j, this.f);
        if (v7_6 != 0) {
            p14.scale((1065353216 / v11_6), 1065353216);
            p14.drawRect(0, v2_12, (this.h.height() - (1073741824 * v8_2)), (- this.i), this.g);
        }
        p14.restoreToCount(v6_11);
        int v6_12 = p14.save();
        p14.translate((this.h.right - v8_2), (this.h.top + v8_2));
        p14.scale(v9_3, v10_1);
        p14.rotate(1119092736);
        p14.drawPath(this.j, this.f);
        if (v7_6 != 0) {
            p14.scale((1065353216 / v10_1), 1065353216);
            p14.drawRect(0, v2_12, (this.h.height() - (1073741824 * v8_2)), (- this.i), this.g);
        }
        p14.restoreToCount(v6_12);
        super.draw(p14);
        return;
    }

    public final int getOpacity()
    {
        return -3;
    }

    public final boolean getPadding(android.graphics.Rect p11)
    {
        int v0_1;
        int v0_0 = this.l;
        if (!this.o) {
            v0_1 = (v0_0 * 1069547520);
        } else {
            v0_1 = ((float) ((((double) this.i) * (1.0 - android.support.design.widget.ar.a)) + ((double) (v0_0 * 1069547520))));
        }
        int v0_8 = this.l;
        if (this.o) {
            v0_8 = ((float) ((((double) this.i) * (1.0 - android.support.design.widget.ar.a)) + ((double) v0_8)));
        }
        p11.set(((int) Math.ceil(((double) v0_8))), ((int) Math.ceil(((double) v0_1))), ((int) Math.ceil(((double) v0_8))), ((int) Math.ceil(((double) v0_1))));
        return 1;
    }

    protected final void onBoundsChange(android.graphics.Rect p2)
    {
        this.q = 1;
        return;
    }

    public final void setAlpha(int p2)
    {
        super.setAlpha(p2);
        this.f.setAlpha(p2);
        this.g.setAlpha(p2);
        return;
    }
}
