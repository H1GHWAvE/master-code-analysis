package android.support.design.widget;
public final class w extends android.view.ViewGroup$MarginLayoutParams {
    android.support.design.widget.t a;
    boolean b;
    public int c;
    public int d;
    public int e;
    int f;
    android.view.View g;
    android.view.View h;
    boolean i;
    boolean j;
    boolean k;
    final android.graphics.Rect l;
    Object m;

    public w()
    {
        this(-2, -2);
        this.b = 0;
        this.c = 0;
        this.d = 0;
        this.e = -1;
        this.f = -1;
        this.l = new android.graphics.Rect();
        return;
    }

    w(android.content.Context p5, android.util.AttributeSet p6)
    {
        this(p5, p6);
        this.b = 0;
        this.c = 0;
        this.d = 0;
        this.e = -1;
        this.f = -1;
        this.l = new android.graphics.Rect();
        android.content.res.TypedArray v0_3 = p5.obtainStyledAttributes(p6, android.support.design.n.CoordinatorLayout_LayoutParams);
        this.c = v0_3.getInteger(android.support.design.n.CoordinatorLayout_LayoutParams_android_layout_gravity, 0);
        this.f = v0_3.getResourceId(android.support.design.n.CoordinatorLayout_LayoutParams_layout_anchor, -1);
        this.d = v0_3.getInteger(android.support.design.n.CoordinatorLayout_LayoutParams_layout_anchorGravity, 0);
        this.e = v0_3.getInteger(android.support.design.n.CoordinatorLayout_LayoutParams_layout_keyline, -1);
        this.b = v0_3.hasValue(android.support.design.n.CoordinatorLayout_LayoutParams_layout_behavior);
        if (this.b) {
            this.a = android.support.design.widget.CoordinatorLayout.a(p5, p6, v0_3.getString(android.support.design.n.CoordinatorLayout_LayoutParams_layout_behavior));
        }
        v0_3.recycle();
        return;
    }

    public w(android.support.design.widget.w p3)
    {
        this(p3);
        this.b = 0;
        this.c = 0;
        this.d = 0;
        this.e = -1;
        this.f = -1;
        this.l = new android.graphics.Rect();
        return;
    }

    public w(android.view.ViewGroup$LayoutParams p3)
    {
        this(p3);
        this.b = 0;
        this.c = 0;
        this.d = 0;
        this.e = -1;
        this.f = -1;
        this.l = new android.graphics.Rect();
        return;
    }

    public w(android.view.ViewGroup$MarginLayoutParams p3)
    {
        this(p3);
        this.b = 0;
        this.c = 0;
        this.d = 0;
        this.e = -1;
        this.f = -1;
        this.l = new android.graphics.Rect();
        return;
    }

    private int a()
    {
        return this.f;
    }

    private android.view.View a(android.support.design.widget.CoordinatorLayout p6, android.view.View p7)
    {
        android.view.View v0_18;
        if (this.f != -1) {
            if (this.g == null) {
                this.g = p6.findViewById(this.f);
                if (this.g == null) {
                    if (!p6.isInEditMode()) {
                        throw new IllegalStateException(new StringBuilder("Could not find CoordinatorLayout descendant view with id ").append(p6.getResources().getResourceName(this.f)).append(" to anchor view ").append(p7).toString());
                    } else {
                        this.h = 0;
                        this.g = 0;
                    }
                } else {
                    android.view.View v0_13 = this.g;
                    android.view.ViewParent v1_11 = this.g.getParent();
                    while ((v1_11 != p6) && (v1_11 != null)) {
                        if (v1_11 != p7) {
                            if ((v1_11 instanceof android.view.View)) {
                                v0_13 = ((android.view.View) v1_11);
                            }
                            v1_11 = v1_11.getParent();
                        } else {
                            if (!p6.isInEditMode()) {
                                throw new IllegalStateException("Anchor must not be a descendant of the anchored view");
                            } else {
                                this.h = 0;
                                this.g = 0;
                            }
                        }
                    }
                    this.h = v0_13;
                }
            } else {
                android.view.View v0_5;
                if (this.g.getId() == this.f) {
                    android.view.View v0_4 = this.g;
                    android.view.ViewParent v1_3 = this.g.getParent();
                    while (v1_3 != p6) {
                        if ((v1_3 != null) && (v1_3 != p7)) {
                            if ((v1_3 instanceof android.view.View)) {
                                v0_4 = ((android.view.View) v1_3);
                            }
                            v1_3 = v1_3.getParent();
                        } else {
                            this.h = 0;
                            this.g = 0;
                            v0_5 = 0;
                        }
                    }
                    this.h = v0_4;
                    v0_5 = 1;
                } else {
                    v0_5 = 0;
                }
                if (v0_5 == null) {
                }
            }
            v0_18 = this.g;
        } else {
            this.h = 0;
            this.g = 0;
            v0_18 = 0;
        }
        return v0_18;
    }

    private void a(int p2)
    {
        this.h = 0;
        this.g = 0;
        this.f = p2;
        return;
    }

    private void a(android.graphics.Rect p2)
    {
        this.l.set(p2);
        return;
    }

    private void a(android.view.View p5, android.support.design.widget.CoordinatorLayout p6)
    {
        this.g = p6.findViewById(this.f);
        if (this.g == null) {
            if (!p6.isInEditMode()) {
                throw new IllegalStateException(new StringBuilder("Could not find CoordinatorLayout descendant view with id ").append(p6.getResources().getResourceName(this.f)).append(" to anchor view ").append(p5).toString());
            } else {
                this.h = 0;
                this.g = 0;
            }
        } else {
            android.view.View v0_6 = this.g;
            android.view.ViewParent v1_7 = this.g.getParent();
            while ((v1_7 != p6) && (v1_7 != null)) {
                if (v1_7 != p5) {
                    if ((v1_7 instanceof android.view.View)) {
                        v0_6 = ((android.view.View) v1_7);
                    }
                    v1_7 = v1_7.getParent();
                } else {
                    if (!p6.isInEditMode()) {
                        throw new IllegalStateException("Anchor must not be a descendant of the anchored view");
                    } else {
                        this.h = 0;
                        this.g = 0;
                    }
                }
            }
            this.h = v0_6;
        }
        return;
    }

    private void a(boolean p1)
    {
        this.j = p1;
        return;
    }

    private android.support.design.widget.t b()
    {
        return this.a;
    }

    private void b(boolean p1)
    {
        this.k = p1;
        return;
    }

    private boolean b(android.view.View p5, android.support.design.widget.CoordinatorLayout p6)
    {
        android.view.View v0_3;
        if (this.g.getId() == this.f) {
            android.view.View v0_2 = this.g;
            android.view.ViewParent v1_2 = this.g.getParent();
            while (v1_2 != p6) {
                if ((v1_2 != null) && (v1_2 != p5)) {
                    if ((v1_2 instanceof android.view.View)) {
                        v0_2 = ((android.view.View) v1_2);
                    }
                    v1_2 = v1_2.getParent();
                } else {
                    this.h = 0;
                    this.g = 0;
                    v0_3 = 0;
                }
            }
            this.h = v0_2;
            v0_3 = 1;
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    private android.graphics.Rect c()
    {
        return this.l;
    }

    private boolean d()
    {
        if ((this.g != null) || (this.f == -1)) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private boolean e()
    {
        if (this.a == null) {
            this.i = 0;
        }
        return this.i;
    }

    private boolean f()
    {
        int v0_2;
        if (!this.i) {
            v0_2 = (this.i | 0);
            this.i = v0_2;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private void g()
    {
        this.i = 0;
        return;
    }

    private void h()
    {
        this.j = 0;
        return;
    }

    private boolean i()
    {
        return this.j;
    }

    private boolean j()
    {
        return this.k;
    }

    private void k()
    {
        this.k = 0;
        return;
    }

    private void l()
    {
        this.h = 0;
        this.g = 0;
        return;
    }

    private static boolean m()
    {
        return 0;
    }

    public final void a(android.support.design.widget.t p2)
    {
        if (this.a != p2) {
            this.a = p2;
            this.m = 0;
            this.b = 1;
        }
        return;
    }

    final boolean a(android.view.View p2)
    {
        if ((p2 != this.h) && ((this.a == null) || (!this.a.b(p2)))) {
            int v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }
}
