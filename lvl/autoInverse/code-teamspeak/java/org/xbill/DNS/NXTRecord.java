package org.xbill.DNS;
public class NXTRecord extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 9595289672944044096;
    private java.util.BitSet bitmap;
    private org.xbill.DNS.Name next;

    NXTRecord()
    {
        return;
    }

    public NXTRecord(org.xbill.DNS.Name p8, int p9, long p10, org.xbill.DNS.Name p12, java.util.BitSet p13)
    {
        this(p8, 30, p9, p10);
        this.next = org.xbill.DNS.NXTRecord.checkName("next", p12);
        this.bitmap = p13;
        return;
    }

    public java.util.BitSet getBitmap()
    {
        return this.bitmap;
    }

    public org.xbill.DNS.Name getNext()
    {
        return this.next;
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.NXTRecord();
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p4, org.xbill.DNS.Name p5)
    {
        this.next = p4.getName(p5);
        this.bitmap = new java.util.BitSet();
        while(true) {
            java.util.BitSet v0_3 = p4.get();
            if (!v0_3.isString()) {
                p4.unget();
                return;
            } else {
                StringBuffer v1_2 = org.xbill.DNS.Type.value(v0_3.value, 1);
                if ((v1_2 <= null) || (v1_2 > 128)) {
                    break;
                }
                this.bitmap.set(v1_2);
            }
        }
        throw p4.exception(new StringBuffer("Invalid type: ").append(v0_3.value).toString());
    }

    void rrFromWire(org.xbill.DNS.DNSInput p8)
    {
        this.next = new org.xbill.DNS.Name(p8);
        this.bitmap = new java.util.BitSet();
        int v3 = p8.remaining();
        int v2 = 0;
        while (v2 < v3) {
            int v4 = p8.readU8();
            int v0_4 = 0;
            while (v0_4 < 8) {
                if (((1 << (7 - v0_4)) & v4) != 0) {
                    this.bitmap.set(((v2 * 8) + v0_4));
                }
                v0_4++;
            }
            v2++;
        }
        return;
    }

    String rrToString()
    {
        StringBuffer v1_1 = new StringBuffer();
        v1_1.append(this.next);
        int v2 = this.bitmap.length();
        short v0_2 = 0;
        while (v0_2 < v2) {
            if (this.bitmap.get(v0_2)) {
                v1_1.append(" ");
                v1_1.append(org.xbill.DNS.Type.string(v0_2));
            }
            v0_2 = ((short) (v0_2 + 1));
        }
        return v1_1.toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p7, org.xbill.DNS.Compression p8, boolean p9)
    {
        this.next.toWire(p7, 0, p9);
        int v4 = this.bitmap.length();
        int v2_1 = 0;
        int v3 = 0;
        while (v3 < v4) {
            int v0_4;
            if (!this.bitmap.get(v3)) {
                v0_4 = 0;
            } else {
                v0_4 = (1 << (7 - (v3 % 8)));
            }
            int v0_6 = (v0_4 | v2_1);
            if (((v3 % 8) == 7) || (v3 == (v4 - 1))) {
                p7.writeU8(v0_6);
                v0_6 = 0;
            }
            v3++;
            v2_1 = v0_6;
        }
        return;
    }
}
