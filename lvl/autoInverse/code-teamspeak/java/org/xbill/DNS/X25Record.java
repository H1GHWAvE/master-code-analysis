package org.xbill.DNS;
public class X25Record extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 4267576252335579764;
    private byte[] address;

    X25Record()
    {
        return;
    }

    public X25Record(org.xbill.DNS.Name p8, int p9, long p10, String p12)
    {
        this(p8, 19, p9, p10);
        this.address = org.xbill.DNS.X25Record.checkAndConvertAddress(p12);
        if (this.address != null) {
            return;
        } else {
            throw new IllegalArgumentException(new StringBuffer("invalid PSDN address ").append(p12).toString());
        }
    }

    private static final byte[] checkAndConvertAddress(String p5)
    {
        int v2 = p5.length();
        int v0 = new byte[v2];
        int v1 = 0;
        while (v1 < v2) {
            byte v3_0 = p5.charAt(v1);
            if (Character.isDigit(v3_0)) {
                v0[v1] = ((byte) v3_0);
                v1++;
            } else {
                v0 = 0;
                break;
            }
        }
        return v0;
    }

    public String getAddress()
    {
        return org.xbill.DNS.X25Record.byteArrayToString(this.address, 0);
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.X25Record();
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p4, org.xbill.DNS.Name p5)
    {
        org.xbill.DNS.TextParseException v0_0 = p4.getString();
        this.address = org.xbill.DNS.X25Record.checkAndConvertAddress(v0_0);
        if (this.address != null) {
            return;
        } else {
            throw p4.exception(new StringBuffer("invalid PSDN address ").append(v0_0).toString());
        }
    }

    void rrFromWire(org.xbill.DNS.DNSInput p2)
    {
        this.address = p2.readCountedString();
        return;
    }

    String rrToString()
    {
        return org.xbill.DNS.X25Record.byteArrayToString(this.address, 1);
    }

    void rrToWire(org.xbill.DNS.DNSOutput p2, org.xbill.DNS.Compression p3, boolean p4)
    {
        p2.writeCountedString(this.address);
        return;
    }
}
