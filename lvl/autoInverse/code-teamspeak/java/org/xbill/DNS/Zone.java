package org.xbill.DNS;
public class Zone implements java.io.Serializable {
    public static final int PRIMARY = 1;
    public static final int SECONDARY = 2;
    private static final long serialVersionUID = 9226233182520040674;
    private org.xbill.DNS.RRset NS;
    private org.xbill.DNS.SOARecord SOA;
    private java.util.Map data;
    private int dclass;
    private boolean hasWild;
    private org.xbill.DNS.Name origin;
    private Object originNode;

    public Zone(org.xbill.DNS.Name p2, int p3, String p4)
    {
        this.dclass = 1;
        org.xbill.DNS.ZoneTransferIn v0_2 = org.xbill.DNS.ZoneTransferIn.newAXFR(p2, p4, 0);
        v0_2.setDClass(p3);
        this.fromXFR(v0_2);
        return;
    }

    public Zone(org.xbill.DNS.Name p3, String p4)
    {
        this.dclass = 1;
        this.data = new java.util.TreeMap();
        if (p3 != null) {
            org.xbill.DNS.Master v0_4 = new org.xbill.DNS.Master(p4, p3);
            this.origin = p3;
            while(true) {
                org.xbill.DNS.Record v1_0 = v0_4.nextRecord();
                if (v1_0 == null) {
                    break;
                }
                this.maybeAddRecord(v1_0);
            }
            this.validate();
            return;
        } else {
            throw new IllegalArgumentException("no zone name specified");
        }
    }

    public Zone(org.xbill.DNS.Name p3, org.xbill.DNS.Record[] p4)
    {
        this.dclass = 1;
        this.data = new java.util.TreeMap();
        if (p3 != null) {
            this.origin = p3;
            int v0_3 = 0;
            while (v0_3 < p4.length) {
                this.maybeAddRecord(p4[v0_3]);
                v0_3++;
            }
            this.validate();
            return;
        } else {
            throw new IllegalArgumentException("no zone name specified");
        }
    }

    public Zone(org.xbill.DNS.ZoneTransferIn p2)
    {
        this.dclass = 1;
        this.fromXFR(p2);
        return;
    }

    static java.util.Map access$000(org.xbill.DNS.Zone p1)
    {
        return p1.data;
    }

    static Object access$100(org.xbill.DNS.Zone p1)
    {
        return p1.originNode;
    }

    static org.xbill.DNS.RRset[] access$200(org.xbill.DNS.Zone p1, Object p2)
    {
        return p1.allRRsets(p2);
    }

    static org.xbill.DNS.RRset access$300(org.xbill.DNS.Zone p1, Object p2, int p3)
    {
        return p1.oneRRset(p2, p3);
    }

    static org.xbill.DNS.Name access$400(org.xbill.DNS.Zone p1)
    {
        return p1.origin;
    }

    private declared_synchronized void addRRset(org.xbill.DNS.Name p5, org.xbill.DNS.RRset p6)
    {
        try {
            if ((!this.hasWild) && (p5.isWild())) {
                this.hasWild = 1;
            }
        } catch (java.util.Map v0_10) {
            throw v0_10;
        }
        java.util.Map v0_4 = this.data.get(p5);
        if (v0_4 != null) {
            int v3 = p6.getType();
            if (!(v0_4 instanceof java.util.List)) {
                java.util.Map v0_5 = ((org.xbill.DNS.RRset) v0_4);
                if (v0_5.getType() != v3) {
                    int v1_3 = new java.util.LinkedList();
                    v1_3.add(v0_5);
                    v1_3.add(p6);
                    this.data.put(p5, v1_3);
                } else {
                    this.data.put(p5, p6);
                }
            } else {
                java.util.Map v0_8 = ((java.util.List) v0_4);
                int v2 = 0;
                while (v2 < v0_8.size()) {
                    if (((org.xbill.DNS.RRset) v0_8.get(v2)).getType() != v3) {
                        v2++;
                    } else {
                        v0_8.set(v2, p6);
                    }
                }
                v0_8.add(p6);
            }
        } else {
            this.data.put(p5, p6);
        }
        return;
    }

    private declared_synchronized org.xbill.DNS.RRset[] allRRsets(Object p3)
    {
        try {
            org.xbill.DNS.RRset[] v0_2;
            if (!(p3 instanceof java.util.List)) {
                v0_2 = new org.xbill.DNS.RRset[1];
                v0_2[0] = ((org.xbill.DNS.RRset) p3);
            } else {
                org.xbill.DNS.RRset[] v0_4 = new org.xbill.DNS.RRset[((java.util.List) p3).size()];
                v0_2 = ((org.xbill.DNS.RRset[]) ((org.xbill.DNS.RRset[]) ((java.util.List) p3).toArray(v0_4)));
            }
        } catch (org.xbill.DNS.RRset[] v0_7) {
            throw v0_7;
        }
        return v0_2;
    }

    private declared_synchronized Object exactName(org.xbill.DNS.Name p2)
    {
        try {
            return this.data.get(p2);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    private declared_synchronized org.xbill.DNS.RRset findRRset(org.xbill.DNS.Name p2, int p3)
    {
        try {
            org.xbill.DNS.RRset v0_1;
            org.xbill.DNS.RRset v0_0 = this.exactName(p2);
        } catch (org.xbill.DNS.RRset v0_2) {
            throw v0_2;
        }
        if (v0_0 != null) {
            v0_1 = this.oneRRset(v0_0, p3);
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    private void fromXFR(org.xbill.DNS.ZoneTransferIn p3)
    {
        this.data = new java.util.TreeMap();
        this.origin = p3.getName();
        String v1_0 = p3.run().iterator();
        while (v1_0.hasNext()) {
            this.maybeAddRecord(((org.xbill.DNS.Record) v1_0.next()));
        }
        if (p3.isAXFR()) {
            this.validate();
            return;
        } else {
            throw new IllegalArgumentException("zones can only be created from AXFRs");
        }
    }

    private declared_synchronized org.xbill.DNS.SetResponse lookup(org.xbill.DNS.Name p10, int p11)
    {
        int v1_0 = 0;
        try {
            org.xbill.DNS.SetResponse v0_6;
            if (p10.subdomain(this.origin)) {
                int v7 = p10.labels();
                int v6 = this.origin.labels();
                int v5 = v6;
                while (v5 <= v7) {
                    org.xbill.DNS.RRset v4_0;
                    if (v5 != v6) {
                        v4_0 = 0;
                    } else {
                        v4_0 = 1;
                    }
                    org.xbill.DNS.RRset[] v2_1;
                    if (v5 != v7) {
                        v2_1 = 0;
                    } else {
                        v2_1 = 1;
                    }
                    org.xbill.DNS.SetResponse v0_9;
                    if (v4_0 == null) {
                        if (v2_1 == null) {
                            v0_9 = new org.xbill.DNS.Name(p10, (v7 - v5));
                        } else {
                            v0_9 = p10;
                        }
                    } else {
                        v0_9 = this.origin;
                    }
                    int v8_1 = this.exactName(v0_9);
                    if (v8_1 != 0) {
                        if (v4_0 == null) {
                            org.xbill.DNS.RRset v4_1 = this.oneRRset(v8_1, 2);
                            if (v4_1 != null) {
                                v0_6 = new org.xbill.DNS.SetResponse(3, v4_1);
                            }
                        }
                        if ((v2_1 == null) || (p11 != 255)) {
                            if (v2_1 == null) {
                                org.xbill.DNS.RRset v4_2 = this.oneRRset(v8_1, 39);
                                if (v4_2 != null) {
                                    v0_6 = new org.xbill.DNS.SetResponse(5, v4_2);
                                }
                            } else {
                                org.xbill.DNS.RRset v4_3 = this.oneRRset(v8_1, p11);
                                if (v4_3 == null) {
                                    org.xbill.DNS.RRset v4_4 = this.oneRRset(v8_1, 5);
                                    if (v4_4 != null) {
                                        v0_6 = new org.xbill.DNS.SetResponse(4, v4_4);
                                    }
                                } else {
                                    v0_6 = new org.xbill.DNS.SetResponse(6);
                                    v0_6.addRRset(v4_3);
                                }
                            }
                            if (v2_1 != null) {
                                v0_6 = org.xbill.DNS.SetResponse.ofType(2);
                            }
                        } else {
                            v0_6 = new org.xbill.DNS.SetResponse(6);
                            org.xbill.DNS.RRset[] v2_3 = this.allRRsets(v8_1);
                            while (v1_0 < v2_3.length) {
                                v0_6.addRRset(v2_3[v1_0]);
                                v1_0++;
                            }
                        }
                    }
                    v5++;
                }
                if (this.hasWild) {
                    org.xbill.DNS.SetResponse v0_4 = 0;
                    while (v0_4 < (v7 - v6)) {
                        int v1_4 = this.exactName(p10.wild((v0_4 + 1)));
                        if (v1_4 != 0) {
                            int v1_5 = this.oneRRset(v1_4, p11);
                            if (v1_5 != 0) {
                                v0_6 = new org.xbill.DNS.SetResponse(6);
                                v0_6.addRRset(v1_5);
                            }
                        }
                        v0_4++;
                    }
                }
                v0_6 = org.xbill.DNS.SetResponse.ofType(1);
            } else {
                v0_6 = org.xbill.DNS.SetResponse.ofType(1);
            }
        } catch (org.xbill.DNS.SetResponse v0_20) {
            throw v0_20;
        }
        return v0_6;
    }

    private final void maybeAddRecord(org.xbill.DNS.Record p5)
    {
        java.io.IOException v0_0 = p5.getType();
        String v1_0 = p5.getName();
        if ((v0_0 != 6) || (v1_0.equals(this.origin))) {
            if (v1_0.subdomain(this.origin)) {
                this.addRecord(p5);
            }
            return;
        } else {
            throw new java.io.IOException(new StringBuffer("SOA owner ").append(v1_0).append(" does not match zone origin ").append(this.origin).toString());
        }
    }

    private void nodeToString(StringBuffer p7, Object p8)
    {
        org.xbill.DNS.RRset[] v1 = this.allRRsets(p8);
        int v0 = 0;
        while (v0 < v1.length) {
            java.util.Iterator v2_1 = v1[v0];
            String v3_0 = v2_1.rrs();
            while (v3_0.hasNext()) {
                p7.append(new StringBuffer().append(v3_0.next()).append("\n").toString());
            }
            java.util.Iterator v2_2 = v2_1.sigs();
            while (v2_2.hasNext()) {
                p7.append(new StringBuffer().append(v2_2.next()).append("\n").toString());
            }
            v0++;
        }
        return;
    }

    private declared_synchronized org.xbill.DNS.RRset oneRRset(Object p4, int p5)
    {
        try {
            if (p5 != 255) {
                int v0_3;
                if (!(p4 instanceof java.util.List)) {
                    if (((org.xbill.DNS.RRset) p4).getType() != p5) {
                        v0_3 = 0;
                    } else {
                        v0_3 = ((org.xbill.DNS.RRset) p4);
                    }
                } else {
                    int v1_0 = 0;
                    while (v1_0 < ((java.util.List) p4).size()) {
                        v0_3 = ((org.xbill.DNS.RRset) ((java.util.List) p4).get(v1_0));
                        if (v0_3.getType() != p5) {
                            v1_0++;
                        }
                    }
                }
                return v0_3;
            } else {
                throw new IllegalArgumentException("oneRRset(ANY)");
            }
        } catch (int v0_10) {
            throw v0_10;
        }
    }

    private declared_synchronized void removeRRset(org.xbill.DNS.Name p4, int p5)
    {
        try {
            java.util.Map v0_1 = this.data.get(p4);
        } catch (java.util.Map v0_7) {
            throw v0_7;
        }
        if (v0_1 != null) {
            if (!(v0_1 instanceof java.util.List)) {
                if (((org.xbill.DNS.RRset) v0_1).getType() == p5) {
                    this.data.remove(p4);
                }
            } else {
                java.util.Map v0_5 = ((java.util.List) v0_1);
                int v2 = 0;
                while (v2 < v0_5.size()) {
                    if (((org.xbill.DNS.RRset) v0_5.get(v2)).getType() != p5) {
                        v2++;
                    } else {
                        v0_5.remove(v2);
                        if (v0_5.size() != 0) {
                            break;
                        }
                        this.data.remove(p4);
                        break;
                    }
                }
            }
        }
        return;
    }

    private void validate()
    {
        this.originNode = this.exactName(this.origin);
        if (this.originNode != null) {
            java.io.IOException v0_4 = this.oneRRset(this.originNode, 6);
            if ((v0_4 != null) && (v0_4.size() == 1)) {
                this.SOA = ((org.xbill.DNS.SOARecord) v0_4.rrs().next());
                this.NS = this.oneRRset(this.originNode, 2);
                if (this.NS != null) {
                    return;
                } else {
                    throw new java.io.IOException(new StringBuffer().append(this.origin).append(": no NS set specified").toString());
                }
            } else {
                throw new java.io.IOException(new StringBuffer().append(this.origin).append(": exactly 1 SOA must be specified").toString());
            }
        } else {
            throw new java.io.IOException(new StringBuffer().append(this.origin).append(": no data specified").toString());
        }
    }

    public java.util.Iterator AXFR()
    {
        return new org.xbill.DNS.Zone$ZoneIterator(this, 1);
    }

    public void addRRset(org.xbill.DNS.RRset p2)
    {
        this.addRRset(p2.getName(), p2);
        return;
    }

    public void addRecord(org.xbill.DNS.Record p3)
    {
        Throwable v0_0 = p3.getName();
        try {
            org.xbill.DNS.RRset v1_1 = this.findRRset(v0_0, p3.getRRsetType());
        } catch (Throwable v0_1) {
            throw v0_1;
        }
        if (v1_1 != null) {
            v1_1.addRR(p3);
        } else {
            this.addRRset(v0_0, new org.xbill.DNS.RRset(p3));
        }
        return;
    }

    public org.xbill.DNS.RRset findExactMatch(org.xbill.DNS.Name p2, int p3)
    {
        org.xbill.DNS.RRset v0_1;
        org.xbill.DNS.RRset v0_0 = this.exactName(p2);
        if (v0_0 != null) {
            v0_1 = this.oneRRset(v0_0, p3);
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    public org.xbill.DNS.SetResponse findRecords(org.xbill.DNS.Name p2, int p3)
    {
        return this.lookup(p2, p3);
    }

    public int getDClass()
    {
        return this.dclass;
    }

    public org.xbill.DNS.RRset getNS()
    {
        return this.NS;
    }

    public org.xbill.DNS.Name getOrigin()
    {
        return this.origin;
    }

    public org.xbill.DNS.SOARecord getSOA()
    {
        return this.SOA;
    }

    public java.util.Iterator iterator()
    {
        return new org.xbill.DNS.Zone$ZoneIterator(this, 0);
    }

    public void removeRecord(org.xbill.DNS.Record p6)
    {
        Throwable v0_0 = p6.getName();
        int v1 = p6.getRRsetType();
        try {
            org.xbill.DNS.RRset v2 = this.findRRset(v0_0, v1);
        } catch (Throwable v0_1) {
            throw v0_1;
        }
        if (v2 != null) {
            if ((v2.size() != 1) || (!v2.first().equals(p6))) {
                v2.deleteRR(p6);
            } else {
                this.removeRRset(v0_0, v1);
            }
        } else {
        }
        return;
    }

    public declared_synchronized String toMasterFile()
    {
        try {
            java.util.Iterator v1 = this.data.entrySet().iterator();
            StringBuffer v2_1 = new StringBuffer();
            this.nodeToString(v2_1, this.originNode);
        } catch (Object v0_7) {
            throw v0_7;
        }
        while (v1.hasNext()) {
            Object v0_6 = ((java.util.Map$Entry) v1.next());
            if (!this.origin.equals(v0_6.getKey())) {
                this.nodeToString(v2_1, v0_6.getValue());
            }
        }
        return v2_1.toString();
    }

    public String toString()
    {
        return this.toMasterFile();
    }
}
