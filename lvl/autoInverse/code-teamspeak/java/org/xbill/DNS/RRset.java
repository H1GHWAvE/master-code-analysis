package org.xbill.DNS;
public class RRset implements java.io.Serializable {
    private static final long serialVersionUID = 15176494783538311921;
    private short nsigs;
    private short position;
    private java.util.List rrs;

    public RRset()
    {
        this.rrs = new java.util.ArrayList(1);
        this.nsigs = 0;
        this.position = 0;
        return;
    }

    public RRset(org.xbill.DNS.RRset p2)
    {
        try {
            this.rrs = ((java.util.List) ((java.util.ArrayList) p2.rrs).clone());
            this.nsigs = p2.nsigs;
            this.position = p2.position;
            return;
        } catch (Throwable v0_6) {
            throw v0_6;
        }
    }

    public RRset(org.xbill.DNS.Record p1)
    {
        this.safeAddRR(p1);
        return;
    }

    private declared_synchronized java.util.Iterator iterator(boolean p5, boolean p6)
    {
        java.util.Iterator v0_0 = 0;
        try {
            java.util.List v1_1;
            int v2_0 = this.rrs.size();
        } catch (java.util.Iterator v0_7) {
            throw v0_7;
        }
        if (!p5) {
            v1_1 = this.nsigs;
        } else {
            v1_1 = (v2_0 - this.nsigs);
        }
        java.util.Iterator v0_6;
        if (v1_1 != null) {
            if (!p5) {
                v0_0 = (v2_0 - this.nsigs);
            } else {
                if (p6) {
                    if (this.position >= v1_1) {
                        this.position = 0;
                    }
                    v0_0 = this.position;
                    this.position = ((short) (v0_0 + 1));
                }
            }
            short v3_3 = new java.util.ArrayList(v1_1);
            if (!p5) {
                v3_3.addAll(this.rrs.subList(v0_0, v2_0));
            } else {
                v3_3.addAll(this.rrs.subList(v0_0, v1_1));
                if (v0_0 != null) {
                    v3_3.addAll(this.rrs.subList(0, v0_0));
                }
            }
            v0_6 = v3_3.iterator();
        } else {
            v0_6 = java.util.Collections.EMPTY_LIST.iterator();
        }
        return v0_6;
    }

    private String iteratorToString(java.util.Iterator p4)
    {
        StringBuffer v1_1 = new StringBuffer();
        while (p4.hasNext()) {
            String v0_3 = ((org.xbill.DNS.Record) p4.next());
            v1_1.append("[");
            v1_1.append(v0_3.rdataToString());
            v1_1.append("]");
            if (p4.hasNext()) {
                v1_1.append(" ");
            }
        }
        return v1_1.toString();
    }

    private void safeAddRR(org.xbill.DNS.Record p4)
    {
        if ((p4 instanceof org.xbill.DNS.RRSIGRecord)) {
            this.rrs.add(p4);
            this.nsigs = ((short) (this.nsigs + 1));
        } else {
            if (this.nsigs != 0) {
                this.rrs.add((this.rrs.size() - this.nsigs), p4);
            } else {
                this.rrs.add(p4);
            }
        }
        return;
    }

    public declared_synchronized void addRR(org.xbill.DNS.Record p7)
    {
        try {
            if (this.rrs.size() != 0) {
                int v0_2 = this.first();
                if (p7.sameRRset(v0_2)) {
                    if (p7.getTTL() != v0_2.getTTL()) {
                        if (p7.getTTL() <= v0_2.getTTL()) {
                            int v1_3 = 0;
                            while (v1_3 < this.rrs.size()) {
                                int v0_9 = ((org.xbill.DNS.Record) this.rrs.get(v1_3)).cloneRecord();
                                v0_9.setTTL(p7.getTTL());
                                this.rrs.set(v1_3, v0_9);
                                v1_3++;
                            }
                        } else {
                            p7 = p7.cloneRecord();
                            p7.setTTL(v0_2.getTTL());
                        }
                    }
                    if (!this.rrs.contains(p7)) {
                        this.safeAddRR(p7);
                    }
                } else {
                    throw new IllegalArgumentException("record does not match rrset");
                }
            } else {
                this.safeAddRR(p7);
            }
        } catch (int v0_16) {
            throw v0_16;
        }
        return;
    }

    public declared_synchronized void clear()
    {
        try {
            this.rrs.clear();
            this.position = 0;
            this.nsigs = 0;
            return;
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public declared_synchronized void deleteRR(org.xbill.DNS.Record p2)
    {
        try {
            if ((this.rrs.remove(p2)) && ((p2 instanceof org.xbill.DNS.RRSIGRecord))) {
                this.nsigs = ((short) (this.nsigs - 1));
            }
        } catch (short v0_6) {
            throw v0_6;
        }
        return;
    }

    public declared_synchronized org.xbill.DNS.Record first()
    {
        try {
            if (this.rrs.size() != 0) {
                return ((org.xbill.DNS.Record) this.rrs.get(0));
            } else {
                throw new IllegalStateException("rrset is empty");
            }
        } catch (org.xbill.DNS.Record v0_7) {
            throw v0_7;
        }
    }

    public int getDClass()
    {
        return this.first().getDClass();
    }

    public org.xbill.DNS.Name getName()
    {
        return this.first().getName();
    }

    public declared_synchronized long getTTL()
    {
        try {
            return this.first().getTTL();
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public int getType()
    {
        return this.first().getRRsetType();
    }

    public declared_synchronized java.util.Iterator rrs()
    {
        try {
            return this.iterator(1, 1);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public declared_synchronized java.util.Iterator rrs(boolean p2)
    {
        try {
            return this.iterator(1, p2);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public declared_synchronized java.util.Iterator sigs()
    {
        try {
            return this.iterator(0, 0);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public declared_synchronized int size()
    {
        try {
            return (this.rrs.size() - this.nsigs);
        } catch (Throwable v0_3) {
            throw v0_3;
        }
    }

    public String toString()
    {
        String v0_4;
        if (this.rrs.size() != 0) {
            String v0_3 = new StringBuffer();
            v0_3.append("{ ");
            v0_3.append(new StringBuffer().append(this.getName()).append(" ").toString());
            v0_3.append(new StringBuffer().append(this.getTTL()).append(" ").toString());
            v0_3.append(new StringBuffer().append(org.xbill.DNS.DClass.string(this.getDClass())).append(" ").toString());
            v0_3.append(new StringBuffer().append(org.xbill.DNS.Type.string(this.getType())).append(" ").toString());
            v0_3.append(this.iteratorToString(this.iterator(1, 0)));
            if (this.nsigs > 0) {
                v0_3.append(" sigs: ");
                v0_3.append(this.iteratorToString(this.iterator(0, 0)));
            }
            v0_3.append(" }");
            v0_4 = v0_3.toString();
        } else {
            v0_4 = "{empty}";
        }
        return v0_4;
    }
}
