package org.xbill.DNS;
public final class DClass {
    public static final int ANY = 255;
    public static final int CH = 3;
    public static final int CHAOS = 3;
    public static final int HESIOD = 4;
    public static final int HS = 4;
    public static final int IN = 1;
    public static final int NONE = 254;
    private static org.xbill.DNS.Mnemonic classes;

    static DClass()
    {
        org.xbill.DNS.Mnemonic v0_1 = new org.xbill.DNS.DClass$DClassMnemonic();
        org.xbill.DNS.DClass.classes = v0_1;
        v0_1.add(1, "IN");
        org.xbill.DNS.DClass.classes.add(3, "CH");
        org.xbill.DNS.DClass.classes.addAlias(3, "CHAOS");
        org.xbill.DNS.DClass.classes.add(4, "HS");
        org.xbill.DNS.DClass.classes.addAlias(4, "HESIOD");
        org.xbill.DNS.DClass.classes.add(254, "NONE");
        org.xbill.DNS.DClass.classes.add(255, "ANY");
        return;
    }

    private DClass()
    {
        return;
    }

    public static void check(int p1)
    {
        if ((p1 >= 0) && (p1 <= 65535)) {
            return;
        } else {
            throw new org.xbill.DNS.InvalidDClassException(p1);
        }
    }

    public static String string(int p1)
    {
        return org.xbill.DNS.DClass.classes.getText(p1);
    }

    public static int value(String p1)
    {
        return org.xbill.DNS.DClass.classes.getValue(p1);
    }
}
