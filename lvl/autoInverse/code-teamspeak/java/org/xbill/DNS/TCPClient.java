package org.xbill.DNS;
final class TCPClient extends org.xbill.DNS.Client {

    public TCPClient(long p2)
    {
        this(java.nio.channels.SocketChannel.open(), p2);
        return;
    }

    private byte[] _recv(int p11)
    {
        java.net.SocketTimeoutException v0_2 = ((java.nio.channels.SocketChannel) this.key.channel());
        byte[] v3 = new byte[p11];
        java.nio.ByteBuffer v4 = java.nio.ByteBuffer.wrap(v3);
        this.key.interestOps(1);
        int v1_1 = 0;
        while (v1_1 < p11) {
            try {
                if (!this.key.isReadable()) {
                    org.xbill.DNS.TCPClient.blockUntil(this.key, this.endTime);
                } else {
                    long v6_1 = ((long) v0_2.read(v4));
                    if (v6_1 >= 0) {
                        v1_1 += ((int) v6_1);
                        if ((v1_1 < p11) && (System.currentTimeMillis() > this.endTime)) {
                            throw new java.net.SocketTimeoutException();
                        }
                    } else {
                        throw new java.io.EOFException();
                    }
                }
            } catch (java.net.SocketTimeoutException v0_10) {
                if (this.key.isValid()) {
                    this.key.interestOps(0);
                }
                throw v0_10;
            }
        }
        if (this.key.isValid()) {
            this.key.interestOps(0);
        }
        return v3;
    }

    static byte[] sendrecv(java.net.SocketAddress p3, java.net.SocketAddress p4, byte[] p5, long p6)
    {
        org.xbill.DNS.TCPClient v1_1 = new org.xbill.DNS.TCPClient(p6);
        if (p3 != null) {
            try {
                v1_1.bind(p3);
            } catch (Throwable v0_0) {
                v1_1.cleanup();
                throw v0_0;
            }
        }
        v1_1.connect(p4);
        v1_1.send(p5);
        Throwable v0_1 = v1_1.recv();
        v1_1.cleanup();
        return v0_1;
    }

    static byte[] sendrecv(java.net.SocketAddress p2, byte[] p3, long p4)
    {
        return org.xbill.DNS.TCPClient.sendrecv(0, p2, p3, p4);
    }

    final void bind(java.net.SocketAddress p2)
    {
        ((java.nio.channels.SocketChannel) this.key.channel()).socket().bind(p2);
        return;
    }

    final void connect(java.net.SocketAddress p6)
    {
        java.nio.channels.SelectionKey v0_2 = ((java.nio.channels.SocketChannel) this.key.channel());
        if (!v0_2.connect(p6)) {
            this.key.interestOps(8);
            try {
                while (!v0_2.finishConnect()) {
                    if (!this.key.isConnectable()) {
                        org.xbill.DNS.TCPClient.blockUntil(this.key, this.endTime);
                    }
                }
            } catch (java.nio.channels.SelectionKey v0_6) {
                if (this.key.isValid()) {
                    this.key.interestOps(0);
                }
                throw v0_6;
            }
            if (this.key.isValid()) {
                this.key.interestOps(0);
            }
        }
        return;
    }

    final byte[] recv()
    {
        java.net.SocketAddress v0_1 = this._recv(2);
        byte[] v1_4 = this._recv(((v0_1[1] & 255) + ((v0_1[0] & 255) << 8)));
        java.net.SocketAddress v0_7 = ((java.nio.channels.SocketChannel) this.key.channel());
        org.xbill.DNS.TCPClient.verboseLog("TCP read", v0_7.socket().getLocalSocketAddress(), v0_7.socket().getRemoteSocketAddress(), v1_4);
        return v1_4;
    }

    final void send(byte[] p9)
    {
        java.net.SocketTimeoutException v0_2 = ((java.nio.channels.SocketChannel) this.key.channel());
        org.xbill.DNS.TCPClient.verboseLog("TCP write", v0_2.socket().getLocalSocketAddress(), v0_2.socket().getRemoteSocketAddress(), p9);
        int v1_1 = new byte[2];
        v1_1[0] = ((byte) (p9.length >> 8));
        v1_1[1] = ((byte) (p9.length & 255));
        java.nio.ByteBuffer[] v3_8 = new java.nio.ByteBuffer[2];
        v3_8[0] = java.nio.ByteBuffer.wrap(v1_1);
        v3_8[1] = java.nio.ByteBuffer.wrap(p9);
        this.key.interestOps(4);
        int v1_5 = 0;
        try {
            while (v1_5 < (p9.length + 2)) {
                if (!this.key.isWritable()) {
                    org.xbill.DNS.TCPClient.blockUntil(this.key, this.endTime);
                } else {
                    long v4_8 = v0_2.write(v3_8);
                    if (v4_8 >= 0) {
                        v1_5 += ((int) v4_8);
                        if ((v1_5 < (p9.length + 2)) && (System.currentTimeMillis() > this.endTime)) {
                            throw new java.net.SocketTimeoutException();
                        }
                    } else {
                        throw new java.io.EOFException();
                    }
                }
            }
        } catch (java.net.SocketTimeoutException v0_10) {
            if (this.key.isValid()) {
                this.key.interestOps(0);
            }
            throw v0_10;
        }
        if (this.key.isValid()) {
            this.key.interestOps(0);
        }
        return;
    }
}
