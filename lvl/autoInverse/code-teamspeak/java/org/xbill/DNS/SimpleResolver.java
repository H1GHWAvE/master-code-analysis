package org.xbill.DNS;
public class SimpleResolver implements org.xbill.DNS.Resolver {
    public static final int DEFAULT_EDNS_PAYLOADSIZE = 1280;
    public static final int DEFAULT_PORT = 53;
    private static final short DEFAULT_UDPSIZE = 512;
    private static String defaultResolver;
    private static int uniqueID;
    private java.net.InetSocketAddress address;
    private boolean ignoreTruncation;
    private java.net.InetSocketAddress localAddress;
    private org.xbill.DNS.OPTRecord queryOPT;
    private long timeoutValue;
    private org.xbill.DNS.TSIG tsig;
    private boolean useTCP;

    static SimpleResolver()
    {
        org.xbill.DNS.SimpleResolver.defaultResolver = "localhost";
        org.xbill.DNS.SimpleResolver.uniqueID = 0;
        return;
    }

    public SimpleResolver()
    {
        this(0);
        return;
    }

    public SimpleResolver(String p4)
    {
        this.timeoutValue = 10000;
        if (p4 == null) {
            p4 = org.xbill.DNS.ResolverConfig.getCurrentConfig().server();
            if (p4 == null) {
                p4 = org.xbill.DNS.SimpleResolver.defaultResolver;
            }
        }
        java.net.InetAddress v0_4;
        if (!p4.equals("0")) {
            v0_4 = java.net.InetAddress.getByName(p4);
        } else {
            v0_4 = java.net.InetAddress.getLocalHost();
        }
        this.address = new java.net.InetSocketAddress(v0_4, 53);
        return;
    }

    private void applyEDNS(org.xbill.DNS.Message p3)
    {
        if ((this.queryOPT != null) && (p3.getOPT() == null)) {
            p3.addRecord(this.queryOPT, 3);
        }
        return;
    }

    private int maxUDPSize(org.xbill.DNS.Message p2)
    {
        int v0_1;
        int v0_0 = p2.getOPT();
        if (v0_0 != 0) {
            v0_1 = v0_0.getPayloadSize();
        } else {
            v0_1 = 512;
        }
        return v0_1;
    }

    private org.xbill.DNS.Message parseMessage(byte[] p3)
    {
        try {
            return new org.xbill.DNS.Message(p3);
        } catch (org.xbill.DNS.WireParseException v0_2) {
            if (org.xbill.DNS.Options.check("verbose")) {
                v0_2.printStackTrace();
            }
            if (!(v0_2 instanceof org.xbill.DNS.WireParseException)) {
                v0_2 = new org.xbill.DNS.WireParseException("Error parsing message");
            }
            throw ((org.xbill.DNS.WireParseException) v0_2);
        }
    }

    private org.xbill.DNS.Message sendAXFR(org.xbill.DNS.Message p8)
    {
        org.xbill.DNS.Record v0_2 = org.xbill.DNS.ZoneTransferIn.newAXFR(p8.getQuestion().getName(), this.address, this.tsig);
        v0_2.setTimeout(((int) (this.getTimeout() / 1000)));
        v0_2.setLocalAddress(this.localAddress);
        try {
            v0_2.run();
            org.xbill.DNS.Record v0_3 = v0_2.getAXFR();
            org.xbill.DNS.WireParseException v1_4 = new org.xbill.DNS.Message(p8.getHeader().getID());
            v1_4.getHeader().setFlag(5);
            v1_4.getHeader().setFlag(0);
            v1_4.addRecord(p8.getQuestion(), 0);
            java.util.Iterator v2_8 = v0_3.iterator();
        } catch (org.xbill.DNS.Record v0_4) {
            throw new org.xbill.DNS.WireParseException(v0_4.getMessage());
        }
        while (v2_8.hasNext()) {
            v1_4.addRecord(((org.xbill.DNS.Record) v2_8.next()), 1);
        }
        return v1_4;
    }

    public static void setDefaultResolver(String p0)
    {
        org.xbill.DNS.SimpleResolver.defaultResolver = p0;
        return;
    }

    private void verifyTSIG(org.xbill.DNS.Message p5, org.xbill.DNS.Message p6, byte[] p7, org.xbill.DNS.TSIG p8)
    {
        if (p8 != null) {
            String v0_1 = p8.verify(p6, p7, p5.getTSIG());
            if (org.xbill.DNS.Options.check("verbose")) {
                System.err.println(new StringBuffer("TSIG verify: ").append(org.xbill.DNS.Rcode.TSIGstring(v0_1)).toString());
            }
        }
        return;
    }

    public java.net.InetSocketAddress getAddress()
    {
        return this.address;
    }

    org.xbill.DNS.TSIG getTSIGKey()
    {
        return this.tsig;
    }

    long getTimeout()
    {
        return this.timeoutValue;
    }

    public org.xbill.DNS.Message send(org.xbill.DNS.Message p13)
    {
        if (org.xbill.DNS.Options.check("verbose")) {
            System.err.println(new StringBuffer("Sending to ").append(this.address.getAddress().getHostAddress()).append(":").append(this.address.getPort()).toString());
        }
        int v0_19;
        if (p13.getHeader().getOpcode() != 0) {
            org.xbill.DNS.Message v6_1 = ((org.xbill.DNS.Message) p13.clone());
            this.applyEDNS(v6_1);
            if (this.tsig != null) {
                this.tsig.apply(v6_1, 0);
            }
            byte[] v2_7 = v6_1.toWire(65535);
            int v3 = this.maxUDPSize(v6_1);
            long v4_1 = (this.timeoutValue + System.currentTimeMillis());
            int v0_12 = 0;
            while(true) {
                if ((!this.useTCP) && (v2_7.length <= v3)) {
                    int v8_0 = v0_12;
                } else {
                    v8_0 = 1;
                }
                int v0_14;
                if (v8_0 == 0) {
                    v0_14 = org.xbill.DNS.UDPClient.sendrecv(this.localAddress, this.address, v2_7, v3, v4_1);
                } else {
                    v0_14 = org.xbill.DNS.TCPClient.sendrecv(this.localAddress, this.address, v2_7, v4_1);
                }
                if (v0_14.length >= 12) {
                    java.io.PrintStream v1_16 = (((v0_14[0] & 255) << 8) + (v0_14[1] & 255));
                    org.xbill.DNS.TSIG v10_4 = v6_1.getHeader().getID();
                    if (v1_16 == v10_4) {
                        java.io.PrintStream v1_17 = this.parseMessage(v0_14);
                        this.verifyTSIG(v6_1, v1_17, v0_14, this.tsig);
                        if ((v8_0 != 0) || ((this.ignoreTruncation) || (!v1_17.getHeader().getFlag(6)))) {
                            break;
                        }
                        v0_12 = 1;
                    } else {
                        int v0_25 = new StringBuffer("invalid message id: expected ").append(v10_4).append("; got id ").append(v1_16).toString();
                        if (v8_0 == 0) {
                            if (!org.xbill.DNS.Options.check("verbose")) {
                                v0_12 = v8_0;
                            } else {
                                System.err.println(v0_25);
                                v0_12 = v8_0;
                            }
                        } else {
                            throw new org.xbill.DNS.WireParseException(v0_25);
                        }
                    }
                } else {
                    throw new org.xbill.DNS.WireParseException("invalid DNS header - too short");
                }
            }
            v0_19 = v1_17;
        } else {
            int v0_5 = p13.getQuestion();
            if ((v0_5 == 0) || (v0_5.getType() != 252)) {
            } else {
                v0_19 = this.sendAXFR(p13);
            }
        }
        return v0_19;
    }

    public Object sendAsync(org.xbill.DNS.Message p5, org.xbill.DNS.ResolverListener p6)
    {
        int v0_3;
        int v0_0 = org.xbill.DNS.SimpleResolver.uniqueID;
        org.xbill.DNS.SimpleResolver.uniqueID = (v0_0 + 1);
        Integer v1_1 = new Integer(v0_0);
        int v0_1 = p5.getQuestion();
        if (v0_1 == 0) {
            v0_3 = "(none)";
        } else {
            v0_3 = v0_1.getName().toString();
        }
        int v0_6 = new StringBuffer().append(this.getClass()).append(": ").append(v0_3).toString();
        org.xbill.DNS.ResolveThread v2_6 = new org.xbill.DNS.ResolveThread(this, p5, v1_1, p6);
        v2_6.setName(v0_6);
        v2_6.setDaemon(1);
        v2_6.start();
        return v1_1;
    }

    public void setAddress(java.net.InetAddress p3)
    {
        this.address = new java.net.InetSocketAddress(p3, this.address.getPort());
        return;
    }

    public void setAddress(java.net.InetSocketAddress p1)
    {
        this.address = p1;
        return;
    }

    public void setEDNS(int p3)
    {
        this.setEDNS(p3, 0, 0, 0);
        return;
    }

    public void setEDNS(int p7, int p8, int p9, java.util.List p10)
    {
        if ((p7 == 0) || (p7 == -1)) {
            int v1_0;
            if (p8 != 0) {
                v1_0 = p8;
            } else {
                v1_0 = 1280;
            }
            this.queryOPT = new org.xbill.DNS.OPTRecord(v1_0, 0, p7, p9, p10);
            return;
        } else {
            throw new IllegalArgumentException("invalid EDNS level - must be 0 or -1");
        }
    }

    public void setIgnoreTruncation(boolean p1)
    {
        this.ignoreTruncation = p1;
        return;
    }

    public void setLocalAddress(java.net.InetAddress p3)
    {
        this.localAddress = new java.net.InetSocketAddress(p3, 0);
        return;
    }

    public void setLocalAddress(java.net.InetSocketAddress p1)
    {
        this.localAddress = p1;
        return;
    }

    public void setPort(int p3)
    {
        this.address = new java.net.InetSocketAddress(this.address.getAddress(), p3);
        return;
    }

    public void setTCP(boolean p1)
    {
        this.useTCP = p1;
        return;
    }

    public void setTSIGKey(org.xbill.DNS.TSIG p1)
    {
        this.tsig = p1;
        return;
    }

    public void setTimeout(int p2)
    {
        this.setTimeout(p2, 0);
        return;
    }

    public void setTimeout(int p5, int p6)
    {
        this.timeoutValue = ((((long) p5) * 1000) + ((long) p6));
        return;
    }
}
