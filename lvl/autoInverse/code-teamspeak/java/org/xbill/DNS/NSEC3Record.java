package org.xbill.DNS;
public class NSEC3Record extends org.xbill.DNS.Record {
    public static final int SHA1_DIGEST_ID = 1;
    private static final org.xbill.DNS.utils.base32 b32 = None;
    private static final long serialVersionUID = 11323239437740618761;
    private int flags;
    private int hashAlg;
    private int iterations;
    private byte[] next;
    private byte[] salt;
    private org.xbill.DNS.TypeBitmap types;

    static NSEC3Record()
    {
        org.xbill.DNS.NSEC3Record.b32 = new org.xbill.DNS.utils.base32("0123456789ABCDEFGHIJKLMNOPQRSTUV=", 0, 0);
        return;
    }

    NSEC3Record()
    {
        return;
    }

    public NSEC3Record(org.xbill.DNS.Name p10, int p11, long p12, int p14, int p15, int p16, byte[] p17, byte[] p18, int[] p19)
    {
        this(p10, 50, p11, p12);
        this.hashAlg = org.xbill.DNS.NSEC3Record.checkU8("hashAlg", p14);
        this.flags = org.xbill.DNS.NSEC3Record.checkU8("flags", p15);
        this.iterations = org.xbill.DNS.NSEC3Record.checkU16("iterations", p16);
        if (p17 != null) {
            if (p17.length <= 255) {
                if (p17.length > 0) {
                    int v2_10 = new byte[p17.length];
                    this.salt = v2_10;
                    System.arraycopy(p17, 0, this.salt, 0, p17.length);
                }
            } else {
                throw new IllegalArgumentException("Invalid salt");
            }
        }
        if (p18.length <= 255) {
            int v2_14 = new byte[p18.length];
            this.next = v2_14;
            System.arraycopy(p18, 0, this.next, 0, p18.length);
            this.types = new org.xbill.DNS.TypeBitmap(p19);
            return;
        } else {
            throw new IllegalArgumentException("Invalid next hash");
        }
    }

    static byte[] hashName(org.xbill.DNS.Name p3, int p4, int p5, byte[] p6)
    {
        switch (p4) {
            case 1:
                java.security.MessageDigest v2_0 = java.security.MessageDigest.getInstance("sha-1");
                byte[] v1_0 = 0;
                int v0_1 = 0;
                while (v0_1 <= p5) {
                    v2_0.reset();
                    if (v0_1 != 0) {
                        v2_0.update(v1_0);
                    } else {
                        v2_0.update(p3.toWireCanonical());
                    }
                    if (p6 != null) {
                        v2_0.update(p6);
                    }
                    v1_0 = v2_0.digest();
                    v0_1++;
                }
                return v1_0;
            default:
                throw new java.security.NoSuchAlgorithmException(new StringBuffer("Unknown NSEC3 algorithmidentifier: ").append(p4).toString());
        }
    }

    public int getFlags()
    {
        return this.flags;
    }

    public int getHashAlgorithm()
    {
        return this.hashAlg;
    }

    public int getIterations()
    {
        return this.iterations;
    }

    public byte[] getNext()
    {
        return this.next;
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.NSEC3Record();
    }

    public byte[] getSalt()
    {
        return this.salt;
    }

    public int[] getTypes()
    {
        return this.types.toArray();
    }

    public boolean hasType(int p2)
    {
        return this.types.contains(p2);
    }

    public byte[] hashName(org.xbill.DNS.Name p4)
    {
        return org.xbill.DNS.NSEC3Record.hashName(p4, this.hashAlg, this.iterations, this.salt);
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p3, org.xbill.DNS.Name p4)
    {
        this.hashAlg = p3.getUInt8();
        this.flags = p3.getUInt8();
        this.iterations = p3.getUInt16();
        if (!p3.getString().equals("-")) {
            p3.unget();
            this.salt = p3.getHexString();
            if (this.salt.length > 255) {
                throw p3.exception("salt value too long");
            }
        } else {
            this.salt = 0;
        }
        this.next = p3.getBase32String(org.xbill.DNS.NSEC3Record.b32);
        this.types = new org.xbill.DNS.TypeBitmap(p3);
        return;
    }

    void rrFromWire(org.xbill.DNS.DNSInput p2)
    {
        this.hashAlg = p2.readU8();
        this.flags = p2.readU8();
        this.iterations = p2.readU16();
        org.xbill.DNS.TypeBitmap v0_3 = p2.readU8();
        if (v0_3 <= null) {
            this.salt = 0;
        } else {
            this.salt = p2.readByteArray(v0_3);
        }
        this.next = p2.readByteArray(p2.readU8());
        this.types = new org.xbill.DNS.TypeBitmap(p2);
        return;
    }

    String rrToString()
    {
        String v0_1 = new StringBuffer();
        v0_1.append(this.hashAlg);
        v0_1.append(32);
        v0_1.append(this.flags);
        v0_1.append(32);
        v0_1.append(this.iterations);
        v0_1.append(32);
        if (this.salt != null) {
            v0_1.append(org.xbill.DNS.utils.base16.toString(this.salt));
        } else {
            v0_1.append(45);
        }
        v0_1.append(32);
        v0_1.append(org.xbill.DNS.NSEC3Record.b32.toString(this.next));
        if (!this.types.empty()) {
            v0_1.append(32);
            v0_1.append(this.types.toString());
        }
        return v0_1.toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p2, org.xbill.DNS.Compression p3, boolean p4)
    {
        p2.writeU8(this.hashAlg);
        p2.writeU8(this.flags);
        p2.writeU16(this.iterations);
        if (this.salt == null) {
            p2.writeU8(0);
        } else {
            p2.writeU8(this.salt.length);
            p2.writeByteArray(this.salt);
        }
        p2.writeU8(this.next.length);
        p2.writeByteArray(this.next);
        this.types.toWire(p2);
        return;
    }
}
