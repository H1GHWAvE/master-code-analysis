package org.xbill.DNS;
 class ZoneTransferIn$BasicHandler implements org.xbill.DNS.ZoneTransferIn$ZoneTransferHandler {
    private java.util.List axfr;
    private java.util.List ixfr;

    private ZoneTransferIn$BasicHandler()
    {
        return;
    }

    ZoneTransferIn$BasicHandler(org.xbill.DNS.ZoneTransferIn$1 p1)
    {
        return;
    }

    static java.util.List access$300(org.xbill.DNS.ZoneTransferIn$BasicHandler p1)
    {
        return p1.axfr;
    }

    static java.util.List access$400(org.xbill.DNS.ZoneTransferIn$BasicHandler p1)
    {
        return p1.ixfr;
    }

    public void handleRecord(org.xbill.DNS.Record p3)
    {
        java.util.List v0_1;
        if (this.ixfr == null) {
            v0_1 = this.axfr;
        } else {
            java.util.List v0_4 = ((org.xbill.DNS.ZoneTransferIn$Delta) this.ixfr.get((this.ixfr.size() - 1)));
            if (v0_4.adds.size() <= 0) {
                v0_1 = v0_4.deletes;
            } else {
                v0_1 = v0_4.adds;
            }
        }
        v0_1.add(p3);
        return;
    }

    public void startAXFR()
    {
        this.axfr = new java.util.ArrayList();
        return;
    }

    public void startIXFR()
    {
        this.ixfr = new java.util.ArrayList();
        return;
    }

    public void startIXFRAdds(org.xbill.DNS.Record p5)
    {
        org.xbill.DNS.ZoneTransferIn$Delta v0_2 = ((org.xbill.DNS.ZoneTransferIn$Delta) this.ixfr.get((this.ixfr.size() - 1)));
        v0_2.adds.add(p5);
        v0_2.end = org.xbill.DNS.ZoneTransferIn.access$100(p5);
        return;
    }

    public void startIXFRDeletes(org.xbill.DNS.Record p5)
    {
        org.xbill.DNS.ZoneTransferIn$Delta v0_1 = new org.xbill.DNS.ZoneTransferIn$Delta(0);
        v0_1.deletes.add(p5);
        v0_1.start = org.xbill.DNS.ZoneTransferIn.access$100(p5);
        this.ixfr.add(v0_1);
        return;
    }
}
