package org.xbill.DNS;
public class MINFORecord extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 14484596901369197820;
    private org.xbill.DNS.Name errorAddress;
    private org.xbill.DNS.Name responsibleAddress;

    MINFORecord()
    {
        return;
    }

    public MINFORecord(org.xbill.DNS.Name p8, int p9, long p10, org.xbill.DNS.Name p12, org.xbill.DNS.Name p13)
    {
        this(p8, 14, p9, p10);
        this.responsibleAddress = org.xbill.DNS.MINFORecord.checkName("responsibleAddress", p12);
        this.errorAddress = org.xbill.DNS.MINFORecord.checkName("errorAddress", p13);
        return;
    }

    public org.xbill.DNS.Name getErrorAddress()
    {
        return this.errorAddress;
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.MINFORecord();
    }

    public org.xbill.DNS.Name getResponsibleAddress()
    {
        return this.responsibleAddress;
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p2, org.xbill.DNS.Name p3)
    {
        this.responsibleAddress = p2.getName(p3);
        this.errorAddress = p2.getName(p3);
        return;
    }

    void rrFromWire(org.xbill.DNS.DNSInput p2)
    {
        this.responsibleAddress = new org.xbill.DNS.Name(p2);
        this.errorAddress = new org.xbill.DNS.Name(p2);
        return;
    }

    String rrToString()
    {
        String v0_1 = new StringBuffer();
        v0_1.append(this.responsibleAddress);
        v0_1.append(" ");
        v0_1.append(this.errorAddress);
        return v0_1.toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p3, org.xbill.DNS.Compression p4, boolean p5)
    {
        this.responsibleAddress.toWire(p3, 0, p5);
        this.errorAddress.toWire(p3, 0, p5);
        return;
    }
}
