package org.xbill.DNS.spi;
public class DNSJavaNameService implements java.lang.reflect.InvocationHandler {
    static Class array$$B = None;
    static Class array$Ljava$net$InetAddress = None;
    private static final String domainProperty = "sun.net.spi.nameservice.domain";
    private static final String nsProperty = "sun.net.spi.nameservice.nameservers";
    private static final String v6Property = "java.net.preferIPv6Addresses";
    private boolean preferV6;

    protected DNSJavaNameService()
    {
        java.io.PrintStream v0_0 = 0;
        this.preferV6 = 0;
        String v1_1 = System.getProperty("sun.net.spi.nameservice.nameservers");
        String v2_1 = System.getProperty("sun.net.spi.nameservice.domain");
        String v3_1 = System.getProperty("java.net.preferIPv6Addresses");
        if (v1_1 != null) {
            java.util.StringTokenizer v4_1 = new java.util.StringTokenizer(v1_1, ",");
            String[] v5_1 = new String[v4_1.countTokens()];
            while (v4_1.hasMoreTokens()) {
                String v1_7 = (v0_0 + 1);
                v5_1[v0_0] = v4_1.nextToken();
                v0_0 = v1_7;
            }
            try {
                org.xbill.DNS.Lookup.setDefaultResolver(new org.xbill.DNS.ExtendedResolver(v5_1));
            } catch (java.io.PrintStream v0) {
                System.err.println("DNSJavaNameService: invalid sun.net.spi.nameservice.nameservers");
            }
        }
        if (v2_1 != null) {
            try {
                java.io.PrintStream v0_5 = new String[1];
                v0_5[0] = v2_1;
                org.xbill.DNS.Lookup.setDefaultSearchPath(v0_5);
            } catch (java.io.PrintStream v0) {
                System.err.println("DNSJavaNameService: invalid sun.net.spi.nameservice.domain");
            }
        }
        if ((v3_1 != null) && (v3_1.equalsIgnoreCase("true"))) {
            this.preferV6 = 1;
        }
        return;
    }

    static Class class$(String p2)
    {
        try {
            return Class.forName(p2);
        } catch (Throwable v0_1) {
            throw new NoClassDefFoundError().initCause(v0_1);
        }
    }

    public String getHostByAddr(byte[] p4)
    {
        String v0_2 = new org.xbill.DNS.Lookup(org.xbill.DNS.ReverseMap.fromAddress(java.net.InetAddress.getByAddress(p4)), 12).run();
        if (v0_2 != null) {
            return ((org.xbill.DNS.PTRRecord) v0_2[0]).getTarget().toString();
        } else {
            throw new java.net.UnknownHostException();
        }
    }

    public Object invoke(Object p6, reflect.Method p7, Object[] p8)
    {
        int v2_0 = 0;
        try {
            byte[][] v0_7;
            if (!p7.getName().equals("getHostByAddr")) {
                if (p7.getName().equals("lookupAllHostAddr")) {
                    byte[][] v1_3;
                    v0_7 = this.lookupAllHostAddr(((String) p8[0]));
                    int v3_0 = p7.getReturnType();
                    if (org.xbill.DNS.spi.DNSJavaNameService.array$Ljava$net$InetAddress != null) {
                        v1_3 = org.xbill.DNS.spi.DNSJavaNameService.array$Ljava$net$InetAddress;
                    } else {
                        v1_3 = org.xbill.DNS.spi.DNSJavaNameService.class$("[Ljava.net.InetAddress;");
                        org.xbill.DNS.spi.DNSJavaNameService.array$Ljava$net$InetAddress = v1_3;
                    }
                    if (v3_0.equals(v1_3)) {
                    } else {
                        byte[][] v1_7;
                        if (org.xbill.DNS.spi.DNSJavaNameService.array$$B != null) {
                            v1_7 = org.xbill.DNS.spi.DNSJavaNameService.array$$B;
                        } else {
                            v1_7 = org.xbill.DNS.spi.DNSJavaNameService.class$("[[B");
                            org.xbill.DNS.spi.DNSJavaNameService.array$$B = v1_7;
                        }
                        if (v3_0.equals(v1_7)) {
                            int v3_1 = v0_7.length;
                            byte[][] v1_11 = new byte[][v3_1];
                            while (v2_0 < v3_1) {
                                v1_11[v2_0] = v0_7[v2_0].getAddress();
                                v2_0++;
                            }
                            v0_7 = v1_11;
                        }
                    }
                }
                throw new IllegalArgumentException("Unknown function name or arguments.");
            } else {
                v0_7 = this.getHostByAddr(((byte[]) ((byte[]) p8[0])));
            }
        } catch (byte[][] v0_14) {
            System.err.println("DNSJavaNameService: Unexpected error.");
            v0_14.printStackTrace();
            throw v0_14;
        }
        return v0_7;
    }

    public java.net.InetAddress[] lookupAllHostAddr(String p5)
    {
        try {
            int v1_1 = new org.xbill.DNS.Name(p5);
            int v0_0 = 0;
        } catch (int v0) {
            throw new java.net.UnknownHostException(p5);
        }
        if (this.preferV6) {
            v0_0 = new org.xbill.DNS.Lookup(v1_1, 28).run();
        }
        if (v0_0 == 0) {
            v0_0 = new org.xbill.DNS.Lookup(v1_1, 1).run();
        }
        if ((v0_0 != 0) || (this.preferV6)) {
            org.xbill.DNS.Record[] v2_3 = v0_0;
        } else {
            v2_3 = new org.xbill.DNS.Lookup(v1_1, 28).run();
        }
        if (v2_3 != null) {
            java.net.InetAddress[] v3_1 = new java.net.InetAddress[v2_3.length];
            int v1_2 = 0;
            while (v1_2 < v2_3.length) {
                if (!(v2_3[v1_2] instanceof org.xbill.DNS.ARecord)) {
                    v3_1[v1_2] = ((org.xbill.DNS.AAAARecord) v2_3[v1_2]).getAddress();
                } else {
                    v3_1[v1_2] = ((org.xbill.DNS.ARecord) v2_3[v1_2]).getAddress();
                }
                v1_2++;
            }
            return v3_1;
        } else {
            throw new java.net.UnknownHostException(p5);
        }
    }
}
