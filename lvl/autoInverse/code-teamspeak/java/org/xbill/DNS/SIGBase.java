package org.xbill.DNS;
abstract class SIGBase extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 14708299682175739247;
    protected int alg;
    protected int covered;
    protected java.util.Date expire;
    protected int footprint;
    protected int labels;
    protected long origttl;
    protected byte[] signature;
    protected org.xbill.DNS.Name signer;
    protected java.util.Date timeSigned;

    protected SIGBase()
    {
        return;
    }

    public SIGBase(org.xbill.DNS.Name p5, int p6, int p7, long p8, int p10, int p11, long p12, java.util.Date p14, java.util.Date p15, int p16, org.xbill.DNS.Name p17, byte[] p18)
    {
        org.xbill.DNS.SIGBase v4_1 = this(p5, p6, p7, p8);
        org.xbill.DNS.Type.check(p10);
        org.xbill.DNS.TTL.check(p12);
        v4_1.covered = p10;
        v4_1.alg = org.xbill.DNS.SIGBase.checkU8("alg", p11);
        v4_1.labels = (p5.labels() - 1);
        if (p5.isWild()) {
            v4_1.labels = (v4_1.labels - 1);
        }
        v4_1.origttl = p12;
        v4_1.expire = p14;
        v4_1.timeSigned = p15;
        v4_1.footprint = org.xbill.DNS.SIGBase.checkU16("footprint", p16);
        v4_1.signer = org.xbill.DNS.SIGBase.checkName("signer", p17);
        v4_1.signature = p18;
        return;
    }

    public int getAlgorithm()
    {
        return this.alg;
    }

    public java.util.Date getExpire()
    {
        return this.expire;
    }

    public int getFootprint()
    {
        return this.footprint;
    }

    public int getLabels()
    {
        return this.labels;
    }

    public long getOrigTTL()
    {
        return this.origttl;
    }

    public byte[] getSignature()
    {
        return this.signature;
    }

    public org.xbill.DNS.Name getSigner()
    {
        return this.signer;
    }

    public java.util.Date getTimeSigned()
    {
        return this.timeSigned;
    }

    public int getTypeCovered()
    {
        return this.covered;
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p4, org.xbill.DNS.Name p5)
    {
        byte[] v0_0 = p4.getString();
        this.covered = org.xbill.DNS.Type.value(v0_0);
        if (this.covered >= 0) {
            byte[] v0_1 = p4.getString();
            this.alg = org.xbill.DNS.DNSSEC$Algorithm.value(v0_1);
            if (this.alg >= 0) {
                this.labels = p4.getUInt8();
                this.origttl = p4.getTTL();
                this.expire = org.xbill.DNS.FormattedTime.parse(p4.getString());
                this.timeSigned = org.xbill.DNS.FormattedTime.parse(p4.getString());
                this.footprint = p4.getUInt16();
                this.signer = p4.getName(p5);
                this.signature = p4.getBase64();
                return;
            } else {
                throw p4.exception(new StringBuffer("Invalid algorithm: ").append(v0_1).toString());
            }
        } else {
            throw p4.exception(new StringBuffer("Invalid type: ").append(v0_0).toString());
        }
    }

    void rrFromWire(org.xbill.DNS.DNSInput p7)
    {
        this.covered = p7.readU16();
        this.alg = p7.readU8();
        this.labels = p7.readU8();
        this.origttl = p7.readU32();
        this.expire = new java.util.Date((p7.readU32() * 1000));
        this.timeSigned = new java.util.Date((p7.readU32() * 1000));
        this.footprint = p7.readU16();
        this.signer = new org.xbill.DNS.Name(p7);
        this.signature = p7.readByteArray();
        return;
    }

    String rrToString()
    {
        String v0_1 = new StringBuffer();
        v0_1.append(org.xbill.DNS.Type.string(this.covered));
        v0_1.append(" ");
        v0_1.append(this.alg);
        v0_1.append(" ");
        v0_1.append(this.labels);
        v0_1.append(" ");
        v0_1.append(this.origttl);
        v0_1.append(" ");
        if (org.xbill.DNS.Options.check("multiline")) {
            v0_1.append("(\n\t");
        }
        v0_1.append(org.xbill.DNS.FormattedTime.format(this.expire));
        v0_1.append(" ");
        v0_1.append(org.xbill.DNS.FormattedTime.format(this.timeSigned));
        v0_1.append(" ");
        v0_1.append(this.footprint);
        v0_1.append(" ");
        v0_1.append(this.signer);
        if (!org.xbill.DNS.Options.check("multiline")) {
            v0_1.append(" ");
            v0_1.append(org.xbill.DNS.utils.base64.toString(this.signature));
        } else {
            v0_1.append("\n");
            v0_1.append(org.xbill.DNS.utils.base64.formatString(this.signature, 64, "\t", 1));
        }
        return v0_1.toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p5, org.xbill.DNS.Compression p6, boolean p7)
    {
        p5.writeU16(this.covered);
        p5.writeU8(this.alg);
        p5.writeU8(this.labels);
        p5.writeU32(this.origttl);
        p5.writeU32((this.expire.getTime() / 1000));
        p5.writeU32((this.timeSigned.getTime() / 1000));
        p5.writeU16(this.footprint);
        this.signer.toWire(p5, 0, p7);
        p5.writeByteArray(this.signature);
        return;
    }

    void setSignature(byte[] p1)
    {
        this.signature = p1;
        return;
    }
}
