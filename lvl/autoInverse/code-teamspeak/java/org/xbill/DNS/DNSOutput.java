package org.xbill.DNS;
public class DNSOutput {
    private byte[] array;
    private int pos;
    private int saved_pos;

    public DNSOutput()
    {
        this(32);
        return;
    }

    public DNSOutput(int p2)
    {
        int v0_0 = new byte[p2];
        this.array = v0_0;
        this.pos = 0;
        this.saved_pos = -1;
        return;
    }

    private void check(long p6, int p8)
    {
        if ((p6 >= 0) && (p6 <= (1 << p8))) {
            return;
        } else {
            throw new IllegalArgumentException(new StringBuffer().append(p6).append(" out of range for ").append(p8).append(" bit value").toString());
        }
    }

    private void need(int p5)
    {
        if ((this.array.length - this.pos) < p5) {
            byte[] v0_5 = (this.array.length * 2);
            if (v0_5 < (this.pos + p5)) {
                v0_5 = (this.pos + p5);
            }
            byte[] v0_7 = new byte[v0_5];
            System.arraycopy(this.array, 0, v0_7, 0, this.pos);
            this.array = v0_7;
        }
        return;
    }

    public int current()
    {
        return this.pos;
    }

    public void jump(int p3)
    {
        if (p3 <= this.pos) {
            this.pos = p3;
            return;
        } else {
            throw new IllegalArgumentException("cannot jump past end of data");
        }
    }

    public void restore()
    {
        if (this.saved_pos >= 0) {
            this.pos = this.saved_pos;
            this.saved_pos = -1;
            return;
        } else {
            throw new IllegalStateException("no previous state");
        }
    }

    public void save()
    {
        this.saved_pos = this.pos;
        return;
    }

    public byte[] toByteArray()
    {
        byte[] v0_1 = new byte[this.pos];
        System.arraycopy(this.array, 0, v0_1, 0, this.pos);
        return v0_1;
    }

    public void writeByteArray(byte[] p3)
    {
        this.writeByteArray(p3, 0, p3.length);
        return;
    }

    public void writeByteArray(byte[] p3, int p4, int p5)
    {
        this.need(p5);
        System.arraycopy(p3, p4, this.array, this.pos, p5);
        this.pos = (this.pos + p5);
        return;
    }

    public void writeCountedString(byte[] p4)
    {
        if (p4.length <= 255) {
            this.need((p4.length + 1));
            int v0_3 = this.array;
            int v1_1 = this.pos;
            this.pos = (v1_1 + 1);
            v0_3[v1_1] = ((byte) (p4.length & 255));
            this.writeByteArray(p4, 0, p4.length);
            return;
        } else {
            throw new IllegalArgumentException("Invalid counted string");
        }
    }

    public void writeU16(int p4)
    {
        this.check(((long) p4), 16);
        this.need(2);
        byte[] v0_2 = this.array;
        int v1_0 = this.pos;
        this.pos = (v1_0 + 1);
        v0_2[v1_0] = ((byte) ((p4 >> 8) & 255));
        byte[] v0_3 = this.array;
        int v1_1 = this.pos;
        this.pos = (v1_1 + 1);
        v0_3[v1_1] = ((byte) (p4 & 255));
        return;
    }

    public void writeU16At(int p4, int p5)
    {
        this.check(((long) p4), 16);
        if (p5 <= (this.pos - 2)) {
            int v1_0 = (p5 + 1);
            this.array[p5] = ((byte) ((p4 >> 8) & 255));
            this.array[v1_0] = ((byte) (p4 & 255));
            return;
        } else {
            throw new IllegalArgumentException("cannot write past end of data");
        }
    }

    public void writeU32(long p8)
    {
        this.check(p8, 32);
        this.need(4);
        byte[] v0_2 = this.array;
        int v1_0 = this.pos;
        this.pos = (v1_0 + 1);
        v0_2[v1_0] = ((byte) ((int) ((p8 >> 24) & 255)));
        byte[] v0_3 = this.array;
        int v1_1 = this.pos;
        this.pos = (v1_1 + 1);
        v0_3[v1_1] = ((byte) ((int) ((p8 >> 16) & 255)));
        byte[] v0_4 = this.array;
        int v1_2 = this.pos;
        this.pos = (v1_2 + 1);
        v0_4[v1_2] = ((byte) ((int) ((p8 >> 8) & 255)));
        byte[] v0_5 = this.array;
        int v1_3 = this.pos;
        this.pos = (v1_3 + 1);
        v0_5[v1_3] = ((byte) ((int) (p8 & 255)));
        return;
    }

    public void writeU8(int p4)
    {
        this.check(((long) p4), 8);
        this.need(1);
        byte[] v0_2 = this.array;
        int v1 = this.pos;
        this.pos = (v1 + 1);
        v0_2[v1] = ((byte) (p4 & 255));
        return;
    }
}
