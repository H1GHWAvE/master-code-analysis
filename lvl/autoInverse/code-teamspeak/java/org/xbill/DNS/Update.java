package org.xbill.DNS;
public class Update extends org.xbill.DNS.Message {
    private int dclass;
    private org.xbill.DNS.Name origin;

    public Update(org.xbill.DNS.Name p2)
    {
        this(p2, 1);
        return;
    }

    public Update(org.xbill.DNS.Name p3, int p4)
    {
        if (p3.isAbsolute()) {
            org.xbill.DNS.DClass.check(p4);
            this.getHeader().setOpcode(5);
            this.addRecord(org.xbill.DNS.Record.newRecord(p3, 6, 1), 0);
            this.origin = p3;
            this.dclass = p4;
            return;
        } else {
            throw new org.xbill.DNS.RelativeNameException(p3);
        }
    }

    private void newPrereq(org.xbill.DNS.Record p2)
    {
        this.addRecord(p2, 1);
        return;
    }

    private void newUpdate(org.xbill.DNS.Record p2)
    {
        this.addRecord(p2, 2);
        return;
    }

    public void absent(org.xbill.DNS.Name p5)
    {
        this.newPrereq(org.xbill.DNS.Record.newRecord(p5, 255, 254, 0));
        return;
    }

    public void absent(org.xbill.DNS.Name p5, int p6)
    {
        this.newPrereq(org.xbill.DNS.Record.newRecord(p5, p6, 254, 0));
        return;
    }

    public void add(org.xbill.DNS.Name p10, int p11, long p12, String p14)
    {
        this.newUpdate(org.xbill.DNS.Record.fromString(p10, p11, this.dclass, p12, p14, this.origin));
        return;
    }

    public void add(org.xbill.DNS.Name p10, int p11, long p12, org.xbill.DNS.Tokenizer p14)
    {
        this.newUpdate(org.xbill.DNS.Record.fromString(p10, p11, this.dclass, p12, p14, this.origin));
        return;
    }

    public void add(org.xbill.DNS.RRset p3)
    {
        java.util.Iterator v1 = p3.rrs();
        while (v1.hasNext()) {
            this.add(((org.xbill.DNS.Record) v1.next()));
        }
        return;
    }

    public void add(org.xbill.DNS.Record p1)
    {
        this.newUpdate(p1);
        return;
    }

    public void add(org.xbill.DNS.Record[] p3)
    {
        int v0 = 0;
        while (v0 < p3.length) {
            this.add(p3[v0]);
            v0++;
        }
        return;
    }

    public void delete(org.xbill.DNS.Name p4)
    {
        this.newUpdate(org.xbill.DNS.Record.newRecord(p4, 255, 255, 0));
        return;
    }

    public void delete(org.xbill.DNS.Name p5, int p6)
    {
        this.newUpdate(org.xbill.DNS.Record.newRecord(p5, p6, 255, 0));
        return;
    }

    public void delete(org.xbill.DNS.Name p9, int p10, String p11)
    {
        this.newUpdate(org.xbill.DNS.Record.fromString(p9, p10, 254, 0, p11, this.origin));
        return;
    }

    public void delete(org.xbill.DNS.Name p9, int p10, org.xbill.DNS.Tokenizer p11)
    {
        this.newUpdate(org.xbill.DNS.Record.fromString(p9, p10, 254, 0, p11, this.origin));
        return;
    }

    public void delete(org.xbill.DNS.RRset p3)
    {
        java.util.Iterator v1 = p3.rrs();
        while (v1.hasNext()) {
            this.delete(((org.xbill.DNS.Record) v1.next()));
        }
        return;
    }

    public void delete(org.xbill.DNS.Record p5)
    {
        this.newUpdate(p5.withDClass(254, 0));
        return;
    }

    public void delete(org.xbill.DNS.Record[] p3)
    {
        int v0 = 0;
        while (v0 < p3.length) {
            this.delete(p3[v0]);
            v0++;
        }
        return;
    }

    public void present(org.xbill.DNS.Name p4)
    {
        this.newPrereq(org.xbill.DNS.Record.newRecord(p4, 255, 255, 0));
        return;
    }

    public void present(org.xbill.DNS.Name p5, int p6)
    {
        this.newPrereq(org.xbill.DNS.Record.newRecord(p5, p6, 255, 0));
        return;
    }

    public void present(org.xbill.DNS.Name p9, int p10, String p11)
    {
        this.newPrereq(org.xbill.DNS.Record.fromString(p9, p10, this.dclass, 0, p11, this.origin));
        return;
    }

    public void present(org.xbill.DNS.Name p9, int p10, org.xbill.DNS.Tokenizer p11)
    {
        this.newPrereq(org.xbill.DNS.Record.fromString(p9, p10, this.dclass, 0, p11, this.origin));
        return;
    }

    public void present(org.xbill.DNS.Record p1)
    {
        this.newPrereq(p1);
        return;
    }

    public void replace(org.xbill.DNS.Name p2, int p3, long p4, String p6)
    {
        this.delete(p2, p3);
        this.add(p2, p3, p4, p6);
        return;
    }

    public void replace(org.xbill.DNS.Name p2, int p3, long p4, org.xbill.DNS.Tokenizer p6)
    {
        this.delete(p2, p3);
        this.add(p2, p3, p4, p6);
        return;
    }

    public void replace(org.xbill.DNS.RRset p3)
    {
        this.delete(p3.getName(), p3.getType());
        java.util.Iterator v1_1 = p3.rrs();
        while (v1_1.hasNext()) {
            this.add(((org.xbill.DNS.Record) v1_1.next()));
        }
        return;
    }

    public void replace(org.xbill.DNS.Record p3)
    {
        this.delete(p3.getName(), p3.getType());
        this.add(p3);
        return;
    }

    public void replace(org.xbill.DNS.Record[] p3)
    {
        int v0 = 0;
        while (v0 < p3.length) {
            this.replace(p3[v0]);
            v0++;
        }
        return;
    }
}
