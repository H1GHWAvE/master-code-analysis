package org.xbill.DNS;
public class DNSSEC$UnsupportedAlgorithmException extends org.xbill.DNS.DNSSEC$DNSSECException {

    DNSSEC$UnsupportedAlgorithmException(int p3)
    {
        this(new StringBuffer("Unsupported algorithm: ").append(p3).toString());
        return;
    }
}
