package org.xbill.DNS;
public class Compression {
    private static final int MAX_POINTER = 16383;
    private static final int TABLE_SIZE = 17;
    private org.xbill.DNS.Compression$Entry[] table;
    private boolean verbose;

    public Compression()
    {
        this.verbose = org.xbill.DNS.Options.check("verbosecompression");
        org.xbill.DNS.Compression$Entry[] v0_3 = new org.xbill.DNS.Compression$Entry[17];
        this.table = v0_3;
        return;
    }

    public void add(int p4, org.xbill.DNS.Name p5)
    {
        if (p4 <= 16383) {
            java.io.PrintStream v0_3 = ((p5.hashCode() & 2147483647) % 17);
            String v1_2 = new org.xbill.DNS.Compression$Entry(0);
            v1_2.name = p5;
            v1_2.pos = p4;
            v1_2.next = this.table[v0_3];
            this.table[v0_3] = v1_2;
            if (this.verbose) {
                System.err.println(new StringBuffer("Adding ").append(p5).append(" at ").append(p4).toString());
            }
        }
        return;
    }

    public int get(org.xbill.DNS.Name p5)
    {
        int v0_2 = -1;
        java.io.PrintStream v1_2 = this.table[((p5.hashCode() & 2147483647) % 17)];
        while (v1_2 != null) {
            if (v1_2.name.equals(p5)) {
                v0_2 = v1_2.pos;
            }
            v1_2 = v1_2.next;
        }
        if (this.verbose) {
            System.err.println(new StringBuffer("Looking for ").append(p5).append(", found ").append(v0_2).toString());
        }
        return v0_2;
    }
}
