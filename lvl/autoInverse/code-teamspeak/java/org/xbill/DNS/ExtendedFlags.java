package org.xbill.DNS;
public final class ExtendedFlags {
    public static final int DO = 32768;
    private static org.xbill.DNS.Mnemonic extflags;

    static ExtendedFlags()
    {
        org.xbill.DNS.Mnemonic v0_1 = new org.xbill.DNS.Mnemonic("EDNS Flag", 3);
        org.xbill.DNS.ExtendedFlags.extflags = v0_1;
        v0_1.setMaximum(65535);
        org.xbill.DNS.ExtendedFlags.extflags.setPrefix("FLAG");
        org.xbill.DNS.ExtendedFlags.extflags.setNumericAllowed(1);
        org.xbill.DNS.ExtendedFlags.extflags.add(32768, "do");
        return;
    }

    private ExtendedFlags()
    {
        return;
    }

    public static String string(int p1)
    {
        return org.xbill.DNS.ExtendedFlags.extflags.getText(p1);
    }

    public static int value(String p1)
    {
        return org.xbill.DNS.ExtendedFlags.extflags.getValue(p1);
    }
}
