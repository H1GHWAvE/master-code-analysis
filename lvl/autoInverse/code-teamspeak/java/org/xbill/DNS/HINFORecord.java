package org.xbill.DNS;
public class HINFORecord extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 13713873442762099504;
    private byte[] cpu;
    private byte[] os;

    HINFORecord()
    {
        return;
    }

    public HINFORecord(org.xbill.DNS.Name p8, int p9, long p10, String p12, String p13)
    {
        this(p8, 13, p9, p10);
        try {
            this.cpu = org.xbill.DNS.HINFORecord.byteArrayFromString(p12);
            this.os = org.xbill.DNS.HINFORecord.byteArrayFromString(p13);
            return;
        } catch (String v0_3) {
            throw new IllegalArgumentException(v0_3.getMessage());
        }
    }

    public String getCPU()
    {
        return org.xbill.DNS.HINFORecord.byteArrayToString(this.cpu, 0);
    }

    public String getOS()
    {
        return org.xbill.DNS.HINFORecord.byteArrayToString(this.os, 0);
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.HINFORecord();
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p2, org.xbill.DNS.Name p3)
    {
        try {
            this.cpu = org.xbill.DNS.HINFORecord.byteArrayFromString(p2.getString());
            this.os = org.xbill.DNS.HINFORecord.byteArrayFromString(p2.getString());
            return;
        } catch (org.xbill.DNS.TextParseException v0_4) {
            throw p2.exception(v0_4.getMessage());
        }
    }

    void rrFromWire(org.xbill.DNS.DNSInput p2)
    {
        this.cpu = p2.readCountedString();
        this.os = p2.readCountedString();
        return;
    }

    String rrToString()
    {
        String v0_1 = new StringBuffer();
        v0_1.append(org.xbill.DNS.HINFORecord.byteArrayToString(this.cpu, 1));
        v0_1.append(" ");
        v0_1.append(org.xbill.DNS.HINFORecord.byteArrayToString(this.os, 1));
        return v0_1.toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p2, org.xbill.DNS.Compression p3, boolean p4)
    {
        p2.writeCountedString(this.cpu);
        p2.writeCountedString(this.os);
        return;
    }
}
