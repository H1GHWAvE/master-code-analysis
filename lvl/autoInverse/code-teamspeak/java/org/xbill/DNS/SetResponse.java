package org.xbill.DNS;
public class SetResponse {
    static final int CNAME = 4;
    static final int DELEGATION = 3;
    static final int DNAME = 5;
    static final int NXDOMAIN = 1;
    static final int NXRRSET = 2;
    static final int SUCCESSFUL = 6;
    static final int UNKNOWN;
    private static final org.xbill.DNS.SetResponse nxdomain;
    private static final org.xbill.DNS.SetResponse nxrrset;
    private static final org.xbill.DNS.SetResponse unknown;
    private Object data;
    private int type;

    static SetResponse()
    {
        org.xbill.DNS.SetResponse.unknown = new org.xbill.DNS.SetResponse(0);
        org.xbill.DNS.SetResponse.nxdomain = new org.xbill.DNS.SetResponse(1);
        org.xbill.DNS.SetResponse.nxrrset = new org.xbill.DNS.SetResponse(2);
        return;
    }

    private SetResponse()
    {
        return;
    }

    SetResponse(int p3)
    {
        if ((p3 >= 0) && (p3 <= 6)) {
            this.type = p3;
            this.data = 0;
            return;
        } else {
            throw new IllegalArgumentException("invalid type");
        }
    }

    SetResponse(int p3, org.xbill.DNS.RRset p4)
    {
        if ((p3 >= 0) && (p3 <= 6)) {
            this.type = p3;
            this.data = p4;
            return;
        } else {
            throw new IllegalArgumentException("invalid type");
        }
    }

    static org.xbill.DNS.SetResponse ofType(int p2)
    {
        org.xbill.DNS.SetResponse v0_1;
        switch (p2) {
            case 0:
                v0_1 = org.xbill.DNS.SetResponse.unknown;
                break;
            case 1:
                v0_1 = org.xbill.DNS.SetResponse.nxdomain;
                break;
            case 2:
                v0_1 = org.xbill.DNS.SetResponse.nxrrset;
                break;
            case 3:
            case 4:
            case 5:
            case 6:
                v0_1 = new org.xbill.DNS.SetResponse();
                v0_1.type = p2;
                v0_1.data = 0;
                break;
            default:
                throw new IllegalArgumentException("invalid type");
        }
        return v0_1;
    }

    void addRRset(org.xbill.DNS.RRset p2)
    {
        if (this.data == null) {
            this.data = new java.util.ArrayList();
        }
        ((java.util.List) this.data).add(p2);
        return;
    }

    public org.xbill.DNS.RRset[] answers()
    {
        org.xbill.DNS.RRset[] v0_5;
        if (this.type == 6) {
            org.xbill.DNS.RRset[] v0_2 = ((java.util.List) this.data);
            org.xbill.DNS.RRset[] v1_2 = new org.xbill.DNS.RRset[v0_2.size()];
            v0_5 = ((org.xbill.DNS.RRset[]) ((org.xbill.DNS.RRset[]) v0_2.toArray(v1_2)));
        } else {
            v0_5 = 0;
        }
        return v0_5;
    }

    public org.xbill.DNS.CNAMERecord getCNAME()
    {
        return ((org.xbill.DNS.CNAMERecord) ((org.xbill.DNS.RRset) this.data).first());
    }

    public org.xbill.DNS.DNAMERecord getDNAME()
    {
        return ((org.xbill.DNS.DNAMERecord) ((org.xbill.DNS.RRset) this.data).first());
    }

    public org.xbill.DNS.RRset getNS()
    {
        return ((org.xbill.DNS.RRset) this.data);
    }

    public boolean isCNAME()
    {
        int v0_1;
        if (this.type != 4) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public boolean isDNAME()
    {
        int v0_1;
        if (this.type != 5) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public boolean isDelegation()
    {
        int v0_1;
        if (this.type != 3) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public boolean isNXDOMAIN()
    {
        int v0 = 1;
        if (this.type != 1) {
            v0 = 0;
        }
        return v0;
    }

    public boolean isNXRRSET()
    {
        int v0_1;
        if (this.type != 2) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public boolean isSuccessful()
    {
        int v0_1;
        if (this.type != 6) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public boolean isUnknown()
    {
        int v0_1;
        if (this.type != 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public String toString()
    {
        String v0_1;
        switch (this.type) {
            case 0:
                v0_1 = "unknown";
                break;
            case 1:
                v0_1 = "NXDOMAIN";
                break;
            case 2:
                v0_1 = "NXRRSET";
                break;
            case 3:
                v0_1 = new StringBuffer("delegation: ").append(this.data).toString();
                break;
            case 4:
                v0_1 = new StringBuffer("CNAME: ").append(this.data).toString();
                break;
            case 5:
                v0_1 = new StringBuffer("DNAME: ").append(this.data).toString();
                break;
            case 6:
                v0_1 = "successful";
                break;
            default:
                throw new IllegalStateException();
        }
        return v0_1;
    }
}
