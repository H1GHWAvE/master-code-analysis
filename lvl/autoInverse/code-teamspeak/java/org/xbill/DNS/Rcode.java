package org.xbill.DNS;
public final class Rcode {
    public static final int BADKEY = 17;
    public static final int BADMODE = 19;
    public static final int BADSIG = 16;
    public static final int BADTIME = 18;
    public static final int BADVERS = 16;
    public static final int FORMERR = 1;
    public static final int NOERROR = 0;
    public static final int NOTAUTH = 9;
    public static final int NOTIMP = 4;
    public static final int NOTIMPL = 4;
    public static final int NOTZONE = 10;
    public static final int NXDOMAIN = 3;
    public static final int NXRRSET = 8;
    public static final int REFUSED = 5;
    public static final int SERVFAIL = 2;
    public static final int YXDOMAIN = 6;
    public static final int YXRRSET = 7;
    private static org.xbill.DNS.Mnemonic rcodes;
    private static org.xbill.DNS.Mnemonic tsigrcodes;

    static Rcode()
    {
        org.xbill.DNS.Rcode.rcodes = new org.xbill.DNS.Mnemonic("DNS Rcode", 2);
        org.xbill.DNS.Rcode.tsigrcodes = new org.xbill.DNS.Mnemonic("TSIG rcode", 2);
        org.xbill.DNS.Rcode.rcodes.setMaximum(4095);
        org.xbill.DNS.Rcode.rcodes.setPrefix("RESERVED");
        org.xbill.DNS.Rcode.rcodes.setNumericAllowed(1);
        org.xbill.DNS.Rcode.rcodes.add(0, "NOERROR");
        org.xbill.DNS.Rcode.rcodes.add(1, "FORMERR");
        org.xbill.DNS.Rcode.rcodes.add(2, "SERVFAIL");
        org.xbill.DNS.Rcode.rcodes.add(3, "NXDOMAIN");
        org.xbill.DNS.Rcode.rcodes.add(4, "NOTIMP");
        org.xbill.DNS.Rcode.rcodes.addAlias(4, "NOTIMPL");
        org.xbill.DNS.Rcode.rcodes.add(5, "REFUSED");
        org.xbill.DNS.Rcode.rcodes.add(6, "YXDOMAIN");
        org.xbill.DNS.Rcode.rcodes.add(7, "YXRRSET");
        org.xbill.DNS.Rcode.rcodes.add(8, "NXRRSET");
        org.xbill.DNS.Rcode.rcodes.add(9, "NOTAUTH");
        org.xbill.DNS.Rcode.rcodes.add(10, "NOTZONE");
        org.xbill.DNS.Rcode.rcodes.add(16, "BADVERS");
        org.xbill.DNS.Rcode.tsigrcodes.setMaximum(65535);
        org.xbill.DNS.Rcode.tsigrcodes.setPrefix("RESERVED");
        org.xbill.DNS.Rcode.tsigrcodes.setNumericAllowed(1);
        org.xbill.DNS.Rcode.tsigrcodes.addAll(org.xbill.DNS.Rcode.rcodes);
        org.xbill.DNS.Rcode.tsigrcodes.add(16, "BADSIG");
        org.xbill.DNS.Rcode.tsigrcodes.add(17, "BADKEY");
        org.xbill.DNS.Rcode.tsigrcodes.add(18, "BADTIME");
        org.xbill.DNS.Rcode.tsigrcodes.add(19, "BADMODE");
        return;
    }

    private Rcode()
    {
        return;
    }

    public static String TSIGstring(int p1)
    {
        return org.xbill.DNS.Rcode.tsigrcodes.getText(p1);
    }

    public static String string(int p1)
    {
        return org.xbill.DNS.Rcode.rcodes.getText(p1);
    }

    public static int value(String p1)
    {
        return org.xbill.DNS.Rcode.rcodes.getValue(p1);
    }
}
