package org.xbill.DNS;
public class MXRecord extends org.xbill.DNS.U16NameBase {
    private static final long serialVersionUID = 2914841027584208546;

    MXRecord()
    {
        return;
    }

    public MXRecord(org.xbill.DNS.Name p12, int p13, long p14, int p16, org.xbill.DNS.Name p17)
    {
        this(p12, 15, p13, p14, p16, "priority", p17, "target");
        return;
    }

    public org.xbill.DNS.Name getAdditionalName()
    {
        return this.getNameField();
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.MXRecord();
    }

    public int getPriority()
    {
        return this.getU16Field();
    }

    public org.xbill.DNS.Name getTarget()
    {
        return this.getNameField();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p2, org.xbill.DNS.Compression p3, boolean p4)
    {
        p2.writeU16(this.u16Field);
        this.nameField.toWire(p2, p3, p4);
        return;
    }
}
