package org.xbill.DNS;
public class NSAP_PTRRecord extends org.xbill.DNS.SingleNameBase {
    private static final long serialVersionUID = 2386284746382064904;

    NSAP_PTRRecord()
    {
        return;
    }

    public NSAP_PTRRecord(org.xbill.DNS.Name p10, int p11, long p12, org.xbill.DNS.Name p14)
    {
        this(p10, 23, p11, p12, p14, "target");
        return;
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.NSAP_PTRRecord();
    }

    public org.xbill.DNS.Name getTarget()
    {
        return this.getSingleName();
    }
}
