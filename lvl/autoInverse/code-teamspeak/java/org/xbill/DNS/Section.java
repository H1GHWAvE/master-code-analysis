package org.xbill.DNS;
public final class Section {
    public static final int ADDITIONAL = 3;
    public static final int ANSWER = 1;
    public static final int AUTHORITY = 2;
    public static final int PREREQ = 1;
    public static final int QUESTION = 0;
    public static final int UPDATE = 2;
    public static final int ZONE;
    private static String[] longSections;
    private static org.xbill.DNS.Mnemonic sections;
    private static String[] updateSections;

    static Section()
    {
        org.xbill.DNS.Section.sections = new org.xbill.DNS.Mnemonic("Message Section", 3);
        String[] v0_2 = new String[4];
        org.xbill.DNS.Section.longSections = v0_2;
        String[] v0_3 = new String[4];
        org.xbill.DNS.Section.updateSections = v0_3;
        org.xbill.DNS.Section.sections.setMaximum(3);
        org.xbill.DNS.Section.sections.setNumericAllowed(1);
        org.xbill.DNS.Section.sections.add(0, "qd");
        org.xbill.DNS.Section.sections.add(1, "an");
        org.xbill.DNS.Section.sections.add(2, "au");
        org.xbill.DNS.Section.sections.add(3, "ad");
        org.xbill.DNS.Section.longSections[0] = "QUESTIONS";
        org.xbill.DNS.Section.longSections[1] = "ANSWERS";
        org.xbill.DNS.Section.longSections[2] = "AUTHORITY RECORDS";
        org.xbill.DNS.Section.longSections[3] = "ADDITIONAL RECORDS";
        org.xbill.DNS.Section.updateSections[0] = "ZONE";
        org.xbill.DNS.Section.updateSections[1] = "PREREQUISITES";
        org.xbill.DNS.Section.updateSections[2] = "UPDATE RECORDS";
        org.xbill.DNS.Section.updateSections[3] = "ADDITIONAL RECORDS";
        return;
    }

    private Section()
    {
        return;
    }

    public static String longString(int p1)
    {
        org.xbill.DNS.Section.sections.check(p1);
        return org.xbill.DNS.Section.longSections[p1];
    }

    public static String string(int p1)
    {
        return org.xbill.DNS.Section.sections.getText(p1);
    }

    public static String updString(int p1)
    {
        org.xbill.DNS.Section.sections.check(p1);
        return org.xbill.DNS.Section.updateSections[p1];
    }

    public static int value(String p1)
    {
        return org.xbill.DNS.Section.sections.getValue(p1);
    }
}
