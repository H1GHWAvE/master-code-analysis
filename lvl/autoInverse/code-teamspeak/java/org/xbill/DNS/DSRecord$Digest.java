package org.xbill.DNS;
public class DSRecord$Digest {
    public static final int GOST3411 = 3;
    public static final int SHA1 = 1;
    public static final int SHA256 = 2;
    public static final int SHA384 = 4;

    private DSRecord$Digest()
    {
        return;
    }
}
