package org.xbill.DNS;
public class GenericEDNSOption extends org.xbill.DNS.EDNSOption {
    private byte[] data;

    GenericEDNSOption(int p1)
    {
        this(p1);
        return;
    }

    public GenericEDNSOption(int p3, byte[] p4)
    {
        this(p3);
        this.data = org.xbill.DNS.Record.checkByteArrayLength("option data", p4, 65535);
        return;
    }

    void optionFromWire(org.xbill.DNS.DNSInput p2)
    {
        this.data = p2.readByteArray();
        return;
    }

    String optionToString()
    {
        return new StringBuffer("<").append(org.xbill.DNS.utils.base16.toString(this.data)).append(">").toString();
    }

    void optionToWire(org.xbill.DNS.DNSOutput p2)
    {
        p2.writeByteArray(this.data);
        return;
    }
}
