package org.xbill.DNS;
public class NSAPRecord extends org.xbill.DNS.Record {
    private static final long serialVersionUID = 17409534670523893023;
    private byte[] address;

    NSAPRecord()
    {
        return;
    }

    public NSAPRecord(org.xbill.DNS.Name p8, int p9, long p10, String p12)
    {
        this(p8, 22, p9, p10);
        this.address = org.xbill.DNS.NSAPRecord.checkAndConvertAddress(p12);
        if (this.address != null) {
            return;
        } else {
            throw new IllegalArgumentException(new StringBuffer("invalid NSAP address ").append(p12).toString());
        }
    }

    private static final byte[] checkAndConvertAddress(String p8)
    {
        int v0_1;
        int v0_0 = 2;
        if (p8.substring(0, 2).equalsIgnoreCase("0x")) {
            java.io.ByteArrayOutputStream v5_1 = new java.io.ByteArrayOutputStream();
            int v1_2 = 0;
            int v3_1 = 0;
            while (v0_0 < p8.length()) {
                int v6_1 = p8.charAt(v0_0);
                if (v6_1 != 46) {
                    int v6_2 = Character.digit(v6_1, 16);
                    if (v6_2 != -1) {
                        if (v3_1 == 0) {
                            v1_2 = (v6_2 << 4);
                            v3_1 = 1;
                        } else {
                            v1_2 += v6_2;
                            v5_1.write(v1_2);
                            v3_1 = 0;
                        }
                    } else {
                        v0_1 = 0;
                        return v0_1;
                    }
                }
                v0_0++;
            }
            if (v3_1 == 0) {
                v0_1 = v5_1.toByteArray();
            } else {
                v0_1 = 0;
            }
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }

    public String getAddress()
    {
        return org.xbill.DNS.NSAPRecord.byteArrayToString(this.address, 0);
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.NSAPRecord();
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p4, org.xbill.DNS.Name p5)
    {
        org.xbill.DNS.TextParseException v0_0 = p4.getString();
        this.address = org.xbill.DNS.NSAPRecord.checkAndConvertAddress(v0_0);
        if (this.address != null) {
            return;
        } else {
            throw p4.exception(new StringBuffer("invalid NSAP address ").append(v0_0).toString());
        }
    }

    void rrFromWire(org.xbill.DNS.DNSInput p2)
    {
        this.address = p2.readByteArray();
        return;
    }

    String rrToString()
    {
        return new StringBuffer("0x").append(org.xbill.DNS.utils.base16.toString(this.address)).toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p2, org.xbill.DNS.Compression p3, boolean p4)
    {
        p2.writeByteArray(this.address);
        return;
    }
}
