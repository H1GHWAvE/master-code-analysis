package org.xbill.DNS;
public interface Resolver {

    public abstract org.xbill.DNS.Message send();

    public abstract Object sendAsync();

    public abstract void setEDNS();

    public abstract void setEDNS();

    public abstract void setIgnoreTruncation();

    public abstract void setPort();

    public abstract void setTCP();

    public abstract void setTSIGKey();

    public abstract void setTimeout();

    public abstract void setTimeout();
}
