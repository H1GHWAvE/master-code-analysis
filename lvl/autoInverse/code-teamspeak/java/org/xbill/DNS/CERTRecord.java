package org.xbill.DNS;
public class CERTRecord extends org.xbill.DNS.Record {
    public static final int OID = 254;
    public static final int PGP = 3;
    public static final int PKIX = 1;
    public static final int SPKI = 2;
    public static final int URI = 253;
    private static final long serialVersionUID = 4763014646517016835;
    private int alg;
    private byte[] cert;
    private int certType;
    private int keyTag;

    CERTRecord()
    {
        return;
    }

    public CERTRecord(org.xbill.DNS.Name p8, int p9, long p10, int p12, int p13, int p14, byte[] p15)
    {
        this(p8, 37, p9, p10);
        this.certType = org.xbill.DNS.CERTRecord.checkU16("certType", p12);
        this.keyTag = org.xbill.DNS.CERTRecord.checkU16("keyTag", p13);
        this.alg = org.xbill.DNS.CERTRecord.checkU8("alg", p14);
        this.cert = p15;
        return;
    }

    public int getAlgorithm()
    {
        return this.alg;
    }

    public byte[] getCert()
    {
        return this.cert;
    }

    public int getCertType()
    {
        return this.certType;
    }

    public int getKeyTag()
    {
        return this.keyTag;
    }

    org.xbill.DNS.Record getObject()
    {
        return new org.xbill.DNS.CERTRecord();
    }

    void rdataFromString(org.xbill.DNS.Tokenizer p4, org.xbill.DNS.Name p5)
    {
        byte[] v0_0 = p4.getString();
        this.certType = org.xbill.DNS.CERTRecord$CertificateType.value(v0_0);
        if (this.certType >= 0) {
            this.keyTag = p4.getUInt16();
            byte[] v0_2 = p4.getString();
            this.alg = org.xbill.DNS.DNSSEC$Algorithm.value(v0_2);
            if (this.alg >= 0) {
                this.cert = p4.getBase64();
                return;
            } else {
                throw p4.exception(new StringBuffer("Invalid algorithm: ").append(v0_2).toString());
            }
        } else {
            throw p4.exception(new StringBuffer("Invalid certificate type: ").append(v0_0).toString());
        }
    }

    void rrFromWire(org.xbill.DNS.DNSInput p2)
    {
        this.certType = p2.readU16();
        this.keyTag = p2.readU16();
        this.alg = p2.readU8();
        this.cert = p2.readByteArray();
        return;
    }

    String rrToString()
    {
        String v0_1 = new StringBuffer();
        v0_1.append(this.certType);
        v0_1.append(" ");
        v0_1.append(this.keyTag);
        v0_1.append(" ");
        v0_1.append(this.alg);
        if (this.cert != null) {
            if (!org.xbill.DNS.Options.check("multiline")) {
                v0_1.append(" ");
                v0_1.append(org.xbill.DNS.utils.base64.toString(this.cert));
            } else {
                v0_1.append(" (\n");
                v0_1.append(org.xbill.DNS.utils.base64.formatString(this.cert, 64, "\t", 1));
            }
        }
        return v0_1.toString();
    }

    void rrToWire(org.xbill.DNS.DNSOutput p2, org.xbill.DNS.Compression p3, boolean p4)
    {
        p2.writeU16(this.certType);
        p2.writeU16(this.keyTag);
        p2.writeU8(this.alg);
        p2.writeByteArray(this.cert);
        return;
    }
}
