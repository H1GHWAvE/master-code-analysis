package com.teamspeak.ts3client.data.c;
final class b extends android.os.AsyncTask implements com.teamspeak.ts3client.data.w {
    final synthetic com.teamspeak.ts3client.data.c.a a;
    private int b;
    private String c;
    private String d;
    private String e;
    private com.teamspeak.ts3client.data.c f;
    private String g;
    private java.io.File h;

    b(com.teamspeak.ts3client.data.c.a p3)
    {
        this.a = p3;
        this.c = new StringBuilder().append(android.os.Environment.getExternalStorageDirectory()).append("/TS3/cache/").toString();
        this.e = "";
        return;
    }

    private android.graphics.Bitmap a()
    {
        this.g = new StringBuilder().append(this.c).append(com.teamspeak.ts3client.Ts3Application.a().a.q.replace("/", "")).append("/avatar/").toString();
        int v2_7 = new java.io.File(this.g);
        if (!v2_7.exists()) {
            v2_7.mkdirs();
        }
        String v0_1;
        this.h = new java.io.File(new StringBuilder().append(this.g).append("avatar_").append(this.e).toString());
        if (!this.h.exists()) {
            this.b();
            v0_1 = 0;
        } else {
            int v2_12 = "";
            try {
                String v3_15 = java.security.MessageDigest.getInstance("MD5");
                byte[] v4_5 = new java.io.FileInputStream(this.h);
                String v5_3 = new byte[4000];
            } catch (String v0_3) {
                String v0_2 = v2_12;
                int v2_13 = v0_3;
                v2_13.printStackTrace();
                if (v0_2.equals("")) {
                    String v0_7 = new android.graphics.BitmapFactory$Options();
                    v0_7.inTargetDensity = 160;
                    v0_1 = android.graphics.BitmapFactory.decodeFile(this.h.getAbsolutePath(), v0_7);
                } else {
                    if (!this.d.equals(v0_2)) {
                        this.h.delete();
                        this.b();
                        v0_1 = 0;
                    } else {
                        String v0_11 = new android.graphics.BitmapFactory$Options();
                        v0_11.inTargetDensity = 160;
                        v0_1 = android.graphics.BitmapFactory.decodeFile(this.h.getAbsolutePath(), v0_11);
                    }
                }
            } catch (String v0_4) {
                v0_2 = v2_12;
                int v2_14 = v0_4;
                v2_14.printStackTrace();
            } catch (String v0_5) {
                v0_2 = v2_12;
                int v2_15 = v0_5;
                v2_15.printStackTrace();
            }
            while(true) {
                int v6_0 = v4_5.read(v5_3);
                if (v6_0 <= 0) {
                    break;
                }
                v3_15.update(v5_3, 0, v6_0);
            }
            byte[] v4_6 = v3_15.digest();
            v0_2 = "";
            v2_12 = 0;
            try {
                while (v2_12 < v4_6.length) {
                    String v3_21 = new StringBuilder().append(v0_2).append(Integer.toString(((v4_6[v2_12] & 255) + 256), 16).substring(1)).toString();
                    v2_12++;
                    v0_2 = v3_21;
                }
            } catch (int v2_15) {
            } catch (int v2_13) {
            } catch (int v2_14) {
            }
        }
        return v0_1;
    }

    private varargs Void a(com.teamspeak.ts3client.data.c[] p3)
    {
        this.f = p3[0];
        this.d = this.f.b();
        this.e = com.teamspeak.ts3client.Ts3Application.a().a.a.ts3client_identityStringToFilename(this.f.b);
        int v0_8 = this.a();
        if (v0_8 != 0) {
            this.f.a(v0_8);
        }
        return 0;
    }

    private void b()
    {
        com.teamspeak.ts3client.Ts3Application.a().d.log(java.util.logging.Level.INFO, new StringBuilder("Loading Avatar avatar_").append(this.e).append(" from Server").toString());
        com.teamspeak.ts3client.Ts3Application.a().a.c.a(this);
        com.teamspeak.ts3client.Ts3Application.a().a.a.ts3client_requestFileInfo(com.teamspeak.ts3client.Ts3Application.a().a.e, 0, "", new StringBuilder("/avatar_").append(this.e).toString(), new StringBuilder("Request info: /avatar_").append(this.e).toString());
        return;
    }

    public final void a(com.teamspeak.ts3client.jni.k p13)
    {
        if (((p13 instanceof com.teamspeak.ts3client.jni.events.rare.FileTransferStatus)) && ((((com.teamspeak.ts3client.jni.events.rare.FileTransferStatus) p13).b == 2065) && (((com.teamspeak.ts3client.jni.events.rare.FileTransferStatus) p13).a == this.b))) {
            com.teamspeak.ts3client.jni.l v0_9 = new android.graphics.BitmapFactory$Options();
            v0_9.inTargetDensity = 160;
            this.f.a(android.graphics.BitmapFactory.decodeFile(this.h.getAbsolutePath(), v0_9));
            com.teamspeak.ts3client.Ts3Application.a().a.c.b(this);
        }
        if (((p13 instanceof com.teamspeak.ts3client.jni.events.rare.FileInfo)) && (((com.teamspeak.ts3client.jni.events.rare.FileInfo) p13).a.equals(new StringBuilder("/avatar_").append(this.e).toString()))) {
            com.teamspeak.ts3client.data.c v1_11;
            com.teamspeak.ts3client.jni.l v0_21 = com.teamspeak.ts3client.Ts3Application.a().e.getString("avatarlimit", "500");
            if (!v0_21.equals("")) {
                v1_11 = v0_21;
            } else {
                v1_11 = "500";
            }
            if ((((com.teamspeak.ts3client.jni.events.rare.FileInfo) p13).b >= ((long) (Integer.parseInt(v1_11) * 1000))) && (!com.teamspeak.ts3client.Ts3Application.a().h.getNetworkInfo(1).isConnected())) {
                com.teamspeak.ts3client.jni.l v0_33 = 0;
            } else {
                v0_33 = 1;
            }
            if (v0_33 == null) {
                new android.graphics.BitmapFactory$Options().inTargetDensity = 160;
                this.f.a(android.graphics.BitmapFactory.decodeResource(com.teamspeak.ts3client.Ts3Application.a().getResources(), 2130837650));
                com.teamspeak.ts3client.Ts3Application.a().a.c.b(this);
            } else {
                com.teamspeak.ts3client.Ts3Application.a().a.c.a(this);
                this.b = com.teamspeak.ts3client.Ts3Application.a().a.a.ts3client_requestFile(com.teamspeak.ts3client.Ts3Application.a().a.e, 0, "", new StringBuilder("/avatar_").append(this.e).toString(), 1, 0, this.g, new StringBuilder("avatar_").append(this.f.c).toString());
            }
        }
        if (((p13 instanceof com.teamspeak.ts3client.jni.events.ServerError)) && ((((com.teamspeak.ts3client.jni.events.ServerError) p13).b == 768) && (((com.teamspeak.ts3client.jni.events.ServerError) p13).c.equals(new StringBuilder("Request info: /avatar_").append(this.e).toString())))) {
            com.teamspeak.ts3client.Ts3Application.a().a.c.a(this);
            this.b = com.teamspeak.ts3client.Ts3Application.a().a.a.ts3client_requestFile(com.teamspeak.ts3client.Ts3Application.a().a.e, 0, "", new StringBuilder("/avatar_").append(this.e).toString(), 1, 0, this.g, new StringBuilder("avatar_").append(this.f.c).toString());
        }
        return;
    }

    protected final synthetic Object doInBackground(Object[] p3)
    {
        this.f = ((com.teamspeak.ts3client.data.c[]) p3)[0];
        this.d = this.f.b();
        this.e = com.teamspeak.ts3client.Ts3Application.a().a.a.ts3client_identityStringToFilename(this.f.b);
        int v0_8 = this.a();
        if (v0_8 != 0) {
            this.f.a(v0_8);
        }
        return 0;
    }
}
