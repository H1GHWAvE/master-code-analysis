package com.teamspeak.ts3client.data.d;
public final class w {

    public w()
    {
        return;
    }

    public static String a(long p14)
    {
        long v2 = (p14 % 60);
        String v0_2 = (p14 / 60);
        String v4_1 = (v0_2 % 60);
        String v0_3 = (v0_2 / 60);
        String v6_2 = (v0_3 % 24);
        String v0_4 = (v0_3 / 24);
        String v8_2 = (v0_4 % 365);
        long v10_1 = (v0_4 / 365);
        String v0_5 = "";
        if (v10_1 > 0) {
            v0_5 = new StringBuilder().append("").append(v10_1).append("a ").toString();
        }
        if (v8_2 > 0) {
            v0_5 = new StringBuilder().append(v0_5).append(v8_2).append("d ").toString();
        }
        if (v6_2 > 0) {
            String v0_13;
            StringBuilder v1_11 = new StringBuilder().append(v0_5);
            if (v6_2 >= 10) {
                v0_13 = Long.valueOf(v6_2);
            } else {
                v0_13 = new StringBuilder("0").append(v6_2).toString();
            }
            v0_5 = v1_11.append(v0_13).append(":").toString();
        }
        String v0_20;
        StringBuilder v1_15 = new StringBuilder().append(v0_5);
        if (v4_1 >= 10) {
            v0_20 = Long.valueOf(v4_1);
        } else {
            v0_20 = new StringBuilder("0").append(v4_1).toString();
        }
        String v0_26;
        StringBuilder v1_17 = v1_15.append(v0_20).append(":");
        if (v2 >= 10) {
            v0_26 = Long.valueOf(v2);
        } else {
            v0_26 = new StringBuilder("0").append(v2).toString();
        }
        return v1_17.append(v0_26).toString();
    }

    public static String a(long p6, boolean p8)
    {
        String v0_1;
        java.text.DecimalFormat v3_1 = new java.text.DecimalFormat("#.##");
        int v2_0 = 0;
        if (!p8) {
            v0_1 = 1000;
        } else {
            v0_1 = 1024;
        }
        while (p6 > (v0_1 * v0_1)) {
            v2_0++;
            p6 /= v0_1;
        }
        String v0_6;
        if (p6 < v0_1) {
            v0_6 = new StringBuilder().append(p6).append(" Bytes").toString();
        } else {
            String v0_14;
            int v2_1 = (v2_0 + 1);
            String v0_11 = new StringBuilder().append(v3_1.format((((double) p6) / ((double) v0_1)))).toString();
            if (v2_1 != 1) {
                if (v2_1 != 2) {
                    if (v2_1 != 3) {
                        if (v2_1 != 4) {
                            if (v2_1 != 5) {
                                if (v2_1 != 6) {
                                    v0_6 = "overflow";
                                    return v0_6;
                                } else {
                                    v0_14 = new StringBuilder().append(v0_11).append(" E").toString();
                                }
                            } else {
                                v0_14 = new StringBuilder().append(v0_11).append(" P").toString();
                            }
                        } else {
                            v0_14 = new StringBuilder().append(v0_11).append(" T").toString();
                        }
                    } else {
                        v0_14 = new StringBuilder().append(v0_11).append(" G").toString();
                    }
                } else {
                    v0_14 = new StringBuilder().append(v0_11).append(" M").toString();
                }
            } else {
                String v0_23;
                String v1_24 = new StringBuilder().append(v0_11);
                if (!p8) {
                    v0_23 = " k";
                } else {
                    v0_23 = " K";
                }
                v0_14 = v1_24.append(v0_23).toString();
            }
            String v0_25;
            String v1_27 = new StringBuilder().append(v0_14);
            if (!p8) {
                v0_25 = "";
            } else {
                v0_25 = "iB";
            }
            v0_6 = v1_27.append(v0_25).toString();
        }
        return v0_6;
    }

    private static String b(long p14)
    {
        String v0_0 = "";
        long v2_1 = (p14 % 60);
        long v4_1 = (p14 / 60);
        long v6_1 = (v4_1 % 60);
        long v4_2 = (v4_1 / 60);
        long v8_2 = (v4_2 % 24);
        long v4_3 = (v4_2 / 24);
        long v10_2 = (v4_3 % 365);
        long v4_4 = (v4_3 / 365);
        if (v4_4 > 0) {
            v0_0 = new StringBuilder().append("").append(v4_4).append("a ").toString();
        }
        if (v10_2 > 0) {
            v0_0 = new StringBuilder().append(v0_0).append(v10_2).append("d ").toString();
        }
        if (v8_2 > 0) {
            v0_0 = new StringBuilder().append(v0_0).append(v8_2).append("h ").toString();
        }
        if (v6_1 > 0) {
            v0_0 = new StringBuilder().append(v0_0).append(v6_1).append("m ").toString();
        }
        if (v2_1 > 0) {
            v0_0 = new StringBuilder().append(v0_0).append(v2_1).append("s").toString();
        }
        return v0_0;
    }
}
