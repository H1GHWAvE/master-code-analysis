package com.teamspeak.ts3client.data.d;
public final class n extends android.text.method.LinkMovementMethod {
    private static com.teamspeak.ts3client.data.d.n a;

    static n()
    {
        com.teamspeak.ts3client.data.d.n.a = new com.teamspeak.ts3client.data.d.n();
        return;
    }

    public n()
    {
        return;
    }

    public static android.text.method.MovementMethod a()
    {
        return com.teamspeak.ts3client.data.d.n.a;
    }

    private void a(android.text.style.URLSpan p11)
    {
        int v2_1 = new com.teamspeak.ts3client.data.ab();
        String v3_0 = android.net.Uri.parse(p11.getURL());
        if (v3_0 != null) {
            if (v3_0.getQuery() != null) {
                com.teamspeak.ts3client.data.d.p v4_1 = new java.util.HashMap();
                StringBuilder v5_1 = v3_0.getQuery().split("&");
                int v6 = v5_1.length;
                String v0_3 = 0;
                while (v0_3 < v6) {
                    String v7_1 = v5_1[v0_3].split("=");
                    if (v7_1.length <= 1) {
                        v4_1.put(v7_1[0], "");
                    } else {
                        v4_1.put(v7_1[0], v7_1[1]);
                    }
                    v0_3++;
                }
                v2_1.a(v3_0.getHost());
                if (v4_1.containsKey("port")) {
                    v2_1.a(new StringBuilder().append(v2_1.c).append(":").append(((String) v4_1.get("port"))).toString());
                }
                if (v4_1.containsKey("nickname")) {
                    v2_1.f = ((String) v4_1.get("nickname"));
                }
                if (v4_1.containsKey("password")) {
                    v2_1.e = ((String) v4_1.get("password"));
                }
                if (v4_1.containsKey("channel")) {
                    v2_1.g = ((String) v4_1.get("channel"));
                }
                if (v4_1.containsKey("channelpassword")) {
                    v2_1.h = ((String) v4_1.get("channelpassword"));
                }
                if (v4_1.containsKey("addbookmark")) {
                    v2_1.a = ((String) v4_1.get("addbookmark"));
                }
                if (v4_1.containsKey("token")) {
                    v2_1.i = ((String) v4_1.get("token"));
                }
            } else {
                v2_1.a(v3_0.getHost());
            }
            String v0_48 = new android.app.AlertDialog$Builder(com.teamspeak.ts3client.Ts3Application.a().c.i()).create();
            v0_48.setTitle("TS3 URL");
            v0_48.setMessage(new StringBuilder("Found the following Host:").append(v2_1.c).toString());
            v0_48.setButton(-3, "Bookmark", new com.teamspeak.ts3client.data.d.o(this, v3_0, v0_48));
            v0_48.setButton(-2, "Cancel", new com.teamspeak.ts3client.data.d.p(this, v0_48));
            v0_48.setCancelable(0);
            v0_48.show();
        }
        return;
    }

    public final boolean onTouchEvent(android.widget.TextView p11, android.text.Spannable p12, android.view.MotionEvent p13)
    {
        String v0_0;
        int v3_0 = p13.getAction();
        if ((v3_0 != 1) && (v3_0 != 0)) {
            android.text.Selection.removeSelection(p12);
            v0_0 = super.onTouchEvent(p11, p12, p13);
        } else {
            com.teamspeak.ts3client.d.a.a v4_1 = ((int) p13.getY());
            String v0_3 = (((int) p13.getX()) - p11.getTotalPaddingLeft());
            com.teamspeak.ts3client.d.a.a v4_2 = (v4_1 - p11.getTotalPaddingTop());
            String v0_4 = (v0_3 + p11.getScrollX());
            com.teamspeak.ts3client.d.a.a v4_3 = (v4_2 + p11.getScrollY());
            com.teamspeak.ts3client.data.b v5_4 = p11.getLayout();
            String v0_8 = ((android.text.style.ClickableSpan[]) p12.getSpans(v5_4.getOffsetForHorizontal(v5_4.getLineForVertical(v4_3), ((float) v0_4)), v5_4.getOffsetForHorizontal(v5_4.getLineForVertical(v4_3), ((float) v0_4)), android.text.style.ClickableSpan));
            if (v0_8.length == 0) {
                if (v3_0 == 0) {
                    try {
                        android.text.Selection.setSelection(p12, p12.getSpanStart(v0_8[0]), p12.getSpanEnd(v0_8[0]));
                    } catch (String v0) {
                    }
                }
            } else {
                if (v3_0 == 1) {
                    if (!(v0_8[0] instanceof android.text.style.URLSpan)) {
                        v0_8[0].onClick(p11);
                    } else {
                        String v0_13 = ((android.text.style.URLSpan) v0_8[0]);
                        if ((!v0_13.getURL().startsWith("client://")) && (!v0_13.getURL().startsWith("ts3file://"))) {
                            if (!v0_13.getURL().startsWith("ts3server://")) {
                                if (!v0_13.getURL().startsWith("channelid://")) {
                                    String v0_14 = v0_13.getURL();
                                    if (com.teamspeak.ts3client.Ts3Application.a().a.a.ts3client_bbcode_shouldPrependHTTP(v0_14)) {
                                        v0_14 = new StringBuilder("http://").append(v0_14).toString();
                                    }
                                    try {
                                        String v0_16 = android.net.Uri.parse(v0_14);
                                        String v2_12 = p11.getContext();
                                        int v3_13 = new android.content.Intent("android.intent.action.VIEW", v0_16);
                                        v3_13.putExtra("com.android.browser.application_id", v2_12.getPackageName());
                                        v2_12.startActivity(v3_13);
                                    } catch (String v0) {
                                    }
                                } else {
                                    try {
                                        String v2_14 = Long.parseLong(v0_13.getURL().substring(12));
                                        String v0_21 = com.teamspeak.ts3client.Ts3Application.a().a;
                                    } catch (String v0) {
                                    }
                                    if ((v0_21 != null) && (v0_21.c().d(Long.valueOf(v2_14)))) {
                                        new com.teamspeak.ts3client.d.a.a(v0_21.c().a(Long.valueOf(v2_14)), 0).a(v0_21.k.M, "ChannelActionDialog");
                                    }
                                    v0_0 = 1;
                                    return v0_0;
                                }
                            } else {
                                int v3_16 = new com.teamspeak.ts3client.data.ab();
                                com.teamspeak.ts3client.d.a.a v4_16 = android.net.Uri.parse(v0_13.getURL());
                                if (v4_16 != null) {
                                    if (v4_16.getQuery() != null) {
                                        com.teamspeak.ts3client.data.b v5_8 = new java.util.HashMap();
                                        StringBuilder v6_1 = v4_16.getQuery().split("&");
                                        int v7 = v6_1.length;
                                        String v0_27 = 0;
                                        while (v0_27 < v7) {
                                            String v8_1 = v6_1[v0_27].split("=");
                                            if (v8_1.length <= 1) {
                                                v5_8.put(v8_1[0], "");
                                            } else {
                                                v5_8.put(v8_1[0], v8_1[1]);
                                            }
                                            v0_27++;
                                        }
                                        v3_16.a(v4_16.getHost());
                                        if (v5_8.containsKey("port")) {
                                            v3_16.a(new StringBuilder().append(v3_16.c).append(":").append(((String) v5_8.get("port"))).toString());
                                        }
                                        if (v5_8.containsKey("nickname")) {
                                            v3_16.f = ((String) v5_8.get("nickname"));
                                        }
                                        if (v5_8.containsKey("password")) {
                                            v3_16.e = ((String) v5_8.get("password"));
                                        }
                                        if (v5_8.containsKey("channel")) {
                                            v3_16.g = ((String) v5_8.get("channel"));
                                        }
                                        if (v5_8.containsKey("channelpassword")) {
                                            v3_16.h = ((String) v5_8.get("channelpassword"));
                                        }
                                        if (v5_8.containsKey("addbookmark")) {
                                            v3_16.a = ((String) v5_8.get("addbookmark"));
                                        }
                                        if (v5_8.containsKey("token")) {
                                            v3_16.i = ((String) v5_8.get("token"));
                                        }
                                    } else {
                                        v3_16.a(v4_16.getHost());
                                    }
                                    String v0_72 = new android.app.AlertDialog$Builder(com.teamspeak.ts3client.Ts3Application.a().c.i()).create();
                                    v0_72.setTitle("TS3 URL");
                                    v0_72.setMessage(new StringBuilder("Found the following Host:").append(v3_16.c).toString());
                                    v0_72.setButton(-3, "Bookmark", new com.teamspeak.ts3client.data.d.o(this, v4_16, v0_72));
                                    v0_72.setButton(-2, "Cancel", new com.teamspeak.ts3client.data.d.p(this, v0_72));
                                    v0_72.setCancelable(0);
                                    v0_72.show();
                                }
                            }
                        }
                        v0_0 = 1;
                        return v0_0;
                    }
                }
            }
            v0_0 = 1;
        }
        return v0_0;
    }
}
