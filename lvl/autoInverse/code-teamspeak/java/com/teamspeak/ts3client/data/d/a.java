package com.teamspeak.ts3client.data.d;
public final class a {
    static android.text.Html$ImageGetter a;
    private static java.util.regex.Pattern b;
    private static java.util.regex.Pattern c;
    private static java.util.regex.Pattern d;
    private static String[] e;
    private static int f;

    static a()
    {
        com.teamspeak.ts3client.data.d.a.a = new com.teamspeak.ts3client.data.d.b();
        com.teamspeak.ts3client.data.d.a.b = java.util.regex.Pattern.compile("<img src=\"(ts3image://.*?)\">", 34);
        com.teamspeak.ts3client.data.d.a.c = java.util.regex.Pattern.compile("<span style=\\\".*? font-size:([+|-]\\d+?pt);.*?\\\">", 2);
        com.teamspeak.ts3client.data.d.a.d = java.util.regex.Pattern.compile("ts3image://(.*)\\?channel=(\\d*)(?:&|&amp;)path=(.*)");
        String[] v0_9 = new String[7];
        v0_9[0] = "xx-small";
        v0_9[1] = "x-small";
        v0_9[2] = "small";
        v0_9[3] = "medium";
        v0_9[4] = "large";
        v0_9[5] = "x-large";
        v0_9[6] = "xx-large";
        com.teamspeak.ts3client.data.d.a.e = v0_9;
        com.teamspeak.ts3client.data.d.a.f = 3;
        return;
    }

    public a()
    {
        return;
    }

    public static android.text.Spanned a(String p4)
    {
        return android.text.Html.fromHtml(com.teamspeak.ts3client.Ts3Application.a().a.a.ts3client_convertBBCodetoHTMLtags_v2(p4.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;").replace("\"", "&quot;"), com.teamspeak.ts3client.jni.b.y.B, 0).replace("\n", "<br>").replace("\\n", "<br>"));
    }

    private static android.text.Spanned a(String p4, String p5)
    {
        return android.text.Html.fromHtml(new StringBuilder().append(p4).append(com.teamspeak.ts3client.Ts3Application.a().a.a.ts3client_convertBBCodetoHTMLtags_v2(p5.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;").replace("\"", "&quot;"), com.teamspeak.ts3client.jni.b.y.B, 1).replace("\n", "<br>")).toString());
    }

    public static String a(String p11, long p12, com.teamspeak.ts3client.data.d.c p14)
    {
        com.teamspeak.ts3client.data.d.d v0_10 = new StringBuilder("<body text=\"white\">").append(com.teamspeak.ts3client.Ts3Application.a().a.a.ts3client_convertBBCodetoHTMLtags_v2(p11.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;").replace("\"", "&quot;"), com.teamspeak.ts3client.jni.b.A.B, 1)).append("</body>").toString().replace("\\[", "[").replace("\\]", "]");
        com.teamspeak.ts3client.data.d.d[] v2_13 = com.teamspeak.ts3client.data.d.a.c.matcher(v0_10);
        com.teamspeak.ts3client.data.d.d[] v3_9 = new StringBuffer(v0_10.length());
        while (v2_13.find()) {
            com.teamspeak.ts3client.data.d.d v0_21 = (Integer.parseInt(v2_13.group(1).replace("pt", "").replace("+", "")) + com.teamspeak.ts3client.data.d.a.f);
            if (v0_21 > 6) {
                v0_21 = 6;
            }
            if (v0_21 < null) {
                v0_21 = 0;
            }
            v2_13.appendReplacement(v3_9, java.util.regex.Matcher.quoteReplacement(v2_13.group(0).replace(v2_13.group(1), com.teamspeak.ts3client.data.d.a.e[v0_21])));
        }
        v2_13.appendTail(v3_9);
        String[] v7_0 = v3_9.toString();
        java.util.regex.Matcher v8 = com.teamspeak.ts3client.data.d.a.b.matcher(v7_0);
        while (v8.find()) {
            com.teamspeak.ts3client.data.d.d v0_16 = new com.teamspeak.ts3client.data.d.d(v8.group(0), v8.group(1), p14, p12);
            if (android.os.Build$VERSION.SDK_INT < 11) {
                com.teamspeak.ts3client.data.d.e v1_4 = new com.teamspeak.ts3client.data.d.e();
                com.teamspeak.ts3client.data.d.d[] v2_16 = new com.teamspeak.ts3client.data.d.d[1];
                v2_16[0] = v0_16;
                v1_4.execute(v2_16);
            } else {
                com.teamspeak.ts3client.data.d.e v1_6 = new com.teamspeak.ts3client.data.d.e();
                com.teamspeak.ts3client.data.d.d[] v3_11 = new com.teamspeak.ts3client.data.d.d[1];
                v3_11[0] = v0_16;
                v1_6.executeOnExecutor(android.os.AsyncTask.THREAD_POOL_EXECUTOR, v3_11);
            }
        }
        return v7_0;
    }

    static synthetic java.util.regex.Pattern a()
    {
        return com.teamspeak.ts3client.data.d.a.d;
    }

    private static android.text.Spanned b(String p5, String p6)
    {
        android.text.Spanned v0_8;
        if (p6 != null) {
            v0_8 = android.text.Html.fromHtml(new StringBuilder().append(new StringBuilder("\"").append(p6).append("\": ").toString()).append(com.teamspeak.ts3client.Ts3Application.a().a.a.ts3client_convertBBCodetoHTMLtags_v2(p5.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;").replace("\"", "&quot;"), com.teamspeak.ts3client.jni.b.y.B, 1).replace("\n", "<br>")).toString());
        } else {
            v0_8 = com.teamspeak.ts3client.data.d.a.a(p5);
        }
        return v0_8;
    }
}
