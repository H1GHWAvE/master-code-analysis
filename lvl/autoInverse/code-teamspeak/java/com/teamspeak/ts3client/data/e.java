package com.teamspeak.ts3client.data;
public final class e {
    private static java.util.regex.Pattern A;
    private com.teamspeak.ts3client.data.b B;
    private String C;
    private int D;
    private int E;
    public com.teamspeak.ts3client.jni.Ts3Jni a;
    public com.teamspeak.ts3client.a.k b;
    public com.teamspeak.ts3client.jni.l c;
    public com.teamspeak.ts3client.data.d d;
    public long e;
    public com.teamspeak.ts3client.data.g f;
    public long g;
    public int h;
    public java.util.HashMap i;
    public java.util.HashMap j;
    public com.teamspeak.ts3client.t k;
    public com.teamspeak.ts3client.ConnectionBackground l;
    public com.teamspeak.ts3client.Ts3Application m;
    public int n;
    public com.teamspeak.ts3client.data.g.b o;
    public com.teamspeak.ts3client.data.a.b p;
    public String q;
    public com.teamspeak.ts3client.data.c.d r;
    public com.teamspeak.ts3client.data.f.a s;
    public String t;
    public com.teamspeak.ts3client.data.d.v u;
    public com.teamspeak.ts3client.data.ab v;
    public com.a.b.d.bw w;
    public int x;
    public int y;
    public long z;

    static e()
    {
        com.teamspeak.ts3client.data.e.A = java.util.regex.Pattern.compile("(((\\\\/)*[^/])*)");
        return;
    }

    private e(android.content.Context p4)
    {
        this.g = 0;
        this.h = 0;
        this.j = new java.util.HashMap();
        this.k = 0;
        this.n = 0;
        this.w = com.a.b.d.hy.a();
        this.m = ((com.teamspeak.ts3client.Ts3Application) p4.getApplicationContext());
        this.a = com.teamspeak.ts3client.jni.Ts3Jni.b();
        this.b = com.teamspeak.ts3client.a.k.a();
        this.b.d = this.a;
        this.E();
        this.c = com.teamspeak.ts3client.jni.l.a();
        this.B = new com.teamspeak.ts3client.data.b();
        this.d = new com.teamspeak.ts3client.data.d();
        this.o = new com.teamspeak.ts3client.data.g.b();
        this.p = new com.teamspeak.ts3client.data.a.b();
        this.i = new java.util.HashMap();
        return;
    }

    public e(String p1, android.content.Context p2, com.teamspeak.ts3client.data.ab p3)
    {
        this(p2);
        this.C = p1;
        this.v = p3;
        return;
    }

    private String A()
    {
        return this.q;
    }

    private int B()
    {
        this.m.d.log(java.util.logging.Level.INFO, "Loading lib");
        int v0_2 = com.teamspeak.ts3client.jni.Ts3Jni.a();
        if (v0_2 <= 0) {
            this.a.ts3client_prepareAudioDevice(this.y, this.x);
            this.s = new com.teamspeak.ts3client.data.f.a(this.m);
            this.e = this.a.ts3client_spawnNewServerConnectionHandler();
        }
        return v0_2;
    }

    private void C()
    {
        this.b.b();
        new android.os.Handler().postDelayed(new com.teamspeak.ts3client.data.f(this), 3000);
        return;
    }

    private void D()
    {
        this.m.d.log(java.util.logging.Level.INFO, "Stop AUDIO");
        this.b.f();
        this.b.e();
        return;
    }

    private void E()
    {
        this.D = this.m.e.getInt("playbackStream", 0);
        this.E = this.m.e.getInt("recordStream", 0);
        this.x = this.m.e.getInt("samplePlay", this.m.i());
        this.y = this.m.e.getInt("sampleRec", this.m.h());
        this.a.ts3client_prepareAudioDevice(this.y, this.x);
        this.b.a(this.y, this.x, this.D, this.E);
        return;
    }

    private long F()
    {
        return this.z;
    }

    static synthetic com.teamspeak.ts3client.a.k a(com.teamspeak.ts3client.data.e p1)
    {
        return p1.b;
    }

    private void a(int p9)
    {
        this.n = p9;
        if (this.n == 519) {
            com.teamspeak.ts3client.data.b.f.a().b();
            this.m.a.k.a(new StringBuilder().append(this.m.getApplicationContext().getString(2131034152)).append(this.m.a.n).toString(), this.m.getApplicationContext().getString(2131034151), Boolean.valueOf(1), Boolean.valueOf(1), Boolean.valueOf(0), this.m.getApplicationContext().getString(2131034142));
        }
        this.m.d.log(java.util.logging.Level.INFO, new StringBuilder("ClientLib Connect_return:").append(p9).toString());
        return;
    }

    private void a(com.teamspeak.ts3client.ConnectionBackground p1)
    {
        this.l = p1;
        return;
    }

    private void a(com.teamspeak.ts3client.data.g p1)
    {
        this.f = p1;
        return;
    }

    private void a(com.teamspeak.ts3client.t p1)
    {
        this.k = p1;
        return;
    }

    private void b(int p1)
    {
        this.h = p1;
        return;
    }

    private void b(long p2)
    {
        this.z = p2;
        return;
    }

    private void c(String p6)
    {
        com.teamspeak.ts3client.data.d.q.a();
        com.teamspeak.ts3client.data.v.a().b = 0;
        this.m.f.o.b.a(this.v.l, this.v);
        this.m.d.log(java.util.logging.Level.INFO, "Disconnecting from Server");
        this.a.ts3client_stopConnection(this.e, p6);
        this.a.ts3client_closeCaptureDevice(this.e);
        this.a.ts3client_closePlaybackDevice(this.e);
        this.a.ts3client_destroyServerConnectionHandler(this.e);
        this.m.stopService(this.m.k);
        this.b.b();
        new android.os.Handler().postDelayed(new com.teamspeak.ts3client.data.f(this), 3000);
        this.a.ts3client_unregisterCustomDevice("");
        this.a = 0;
        java.util.logging.Logger v0_18 = com.teamspeak.ts3client.chat.d.a(this.m.getBaseContext());
        v0_18.c = 0;
        v0_18.d = 0;
        v0_18.a.clear();
        v0_18.b.clear();
        v0_18.a = new java.util.ArrayList();
        v0_18.b = new java.util.HashMap();
        this.m.d.log(java.util.logging.Level.INFO, "ClientLib closed");
        return;
    }

    private void d(String p1)
    {
        this.t = p1;
        return;
    }

    private com.teamspeak.ts3client.a.k s()
    {
        return this.b;
    }

    private com.teamspeak.ts3client.data.g t()
    {
        return this.f;
    }

    private com.teamspeak.ts3client.data.f.a u()
    {
        return this.s;
    }

    private int v()
    {
        return this.n;
    }

    private com.teamspeak.ts3client.ConnectionBackground w()
    {
        return this.l;
    }

    private String x()
    {
        return this.C;
    }

    private com.teamspeak.ts3client.jni.l y()
    {
        return this.c;
    }

    private String z()
    {
        return this.t;
    }

    public final com.a.b.d.bw a()
    {
        return this.w;
    }

    public final void a(long p2)
    {
        this.g = p2;
        return;
    }

    public final void a(com.teamspeak.ts3client.data.c.d p1)
    {
        this.r = p1;
        return;
    }

    public final void a(com.teamspeak.ts3client.data.d.v p1)
    {
        this.u = p1;
        return;
    }

    public final void a(String p4)
    {
        this.v.b = p4;
        com.teamspeak.ts3client.t v0_3 = this.m.a.k;
        v0_3.i().runOnUiThread(new com.teamspeak.ts3client.ap(v0_3));
        return;
    }

    public final void a(String p17, int p18, String p19, String p20, String p21, String p22, String p23)
    {
        this.m.a.t = p17;
        if (!p23.equals("")) {
            this.m.a.a.a(this.e, com.teamspeak.ts3client.jni.d.ad, p23);
        }
        int v2_8 = new java.util.ArrayList();
        int v11 = 0;
        if (!p20.startsWith("/")) {
            int v3_4 = com.teamspeak.ts3client.data.e.A.matcher(p20);
            while (v3_4.find()) {
                com.teamspeak.ts3client.data.v v4_3 = v3_4.group(1);
                if (!v4_3.equals("")) {
                    v2_8.add(v4_3.replace("\\/", "/"));
                }
            }
        } else {
            v2_8.add(p20);
            v11 = 1;
        }
        int v3_6 = new String[v2_8.size()];
        int v10_1 = ((String[]) v2_8.toArray(v3_6));
        String v14 = android.provider.Settings$Secure.getString(this.m.getContentResolver(), "android_id");
        if (v14 == null) {
            v14 = "";
        }
        com.teamspeak.ts3client.data.e v15 = this.m.a;
        int v9_1 = this.a.ts3client_startConnectionEx(this.e, this.C, p17, p18, p19, v10_1, v11, p21, p22, v14);
        v15.n = v9_1;
        if (v15.n == 519) {
            com.teamspeak.ts3client.data.b.f.a().b();
            v15.m.a.k.a(new StringBuilder().append(v15.m.getApplicationContext().getString(2131034152)).append(v15.m.a.n).toString(), v15.m.getApplicationContext().getString(2131034151), Boolean.valueOf(1), Boolean.valueOf(1), Boolean.valueOf(0), v15.m.getApplicationContext().getString(2131034142));
        }
        v15.m.d.log(java.util.logging.Level.INFO, new StringBuilder("ClientLib Connect_return:").append(v9_1).toString());
        int v2_21 = com.teamspeak.ts3client.chat.d.a(this.m.getBaseContext());
        v2_21.c = new com.teamspeak.ts3client.chat.a("Server Tab", "SERVER", 0);
        v2_21.c.b = 1;
        v2_21.d = new com.teamspeak.ts3client.chat.a("Channel Tab", "CHANNEL", 0);
        v2_21.d.b = 1;
        v2_21.a(v2_21.c);
        v2_21.a(v2_21.d);
        com.teamspeak.ts3client.chat.d.a(this.m.a.k.M);
        int v2_28 = this.m.e.getString("ptt_key_setting", "");
        if (!v2_28.isEmpty()) {
            int v3_27 = new java.util.BitSet();
            com.teamspeak.ts3client.data.v v4_25 = v2_28.split(",");
            int v5_10 = v4_25.length;
            int v2_29 = 0;
            while (v2_29 < v5_10) {
                try {
                    v3_27.set(Integer.parseInt(v4_25[v2_29]));
                } catch (Exception v6) {
                }
                v2_29++;
            }
            com.teamspeak.ts3client.data.v.a().a(v3_27, this.m.e.getBoolean("ptt_key_setting_intercept", 0));
            com.teamspeak.ts3client.data.v.a().b = 1;
        }
        return;
    }

    public final com.teamspeak.ts3client.data.a.b b()
    {
        return this.p;
    }

    public final void b(String p1)
    {
        this.q = p1;
        return;
    }

    public final declared_synchronized com.teamspeak.ts3client.data.b c()
    {
        try {
            return this.B;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    public final java.util.HashMap d()
    {
        return this.i;
    }

    public final com.teamspeak.ts3client.t e()
    {
        return this.k;
    }

    public final long f()
    {
        return this.g;
    }

    public final com.teamspeak.ts3client.data.c.d g()
    {
        return this.r;
    }

    public final com.teamspeak.ts3client.jni.Ts3Jni h()
    {
        return this.a;
    }

    public final int i()
    {
        return this.h;
    }

    public final com.teamspeak.ts3client.data.d.v j()
    {
        return this.u;
    }

    public final long k()
    {
        return this.e;
    }

    public final com.teamspeak.ts3client.data.g.b l()
    {
        return this.o;
    }

    public final String m()
    {
        String v0_4;
        if (!this.v.b.equals("")) {
            v0_4 = this.v.b;
        } else {
            v0_4 = this.v.a;
        }
        return v0_4;
    }

    public final void n()
    {
        this.m.d.log(java.util.logging.Level.INFO, "Start AUDIO");
        com.teamspeak.ts3client.a.j.a();
        this.a.ts3client_activateCaptureDevice(this.e);
        this.a.ts3client_openCaptureDevice(this.e, "", "");
        this.a.ts3client_openPlaybackDevice(this.e, "", "");
        this.m.a.a.ts3client_setPreProcessorConfigValue(this.m.a.e, "agc", "true");
        this.b.g();
        this.b.c();
        this.a.ts3client_setPreProcessorConfigValue(this.e, "voiceactivation_level", String.valueOf(this.m.e.getInt("voiceactivation_level", 10)));
        com.teamspeak.ts3client.jni.Ts3Jni v0_16 = this.m.e.getBoolean("audio_ptt", 0);
        this.m.d.log(java.util.logging.Level.INFO, new StringBuilder("Setting PTT: ").append(v0_16).toString());
        if (v0_16 == null) {
            this.b.d();
        } else {
            this.m.a.a.a(this.m.a.e, com.teamspeak.ts3client.jni.d.k, 1);
            this.a.ts3client_setPreProcessorConfigValue(this.e, "vad", "false");
            this.a.ts3client_setPreProcessorConfigValue(this.e, "voiceactivation_level", "-50");
        }
        com.teamspeak.ts3client.jni.Ts3Jni v0_25 = this.m.e.getBoolean("audio_bt", 0);
        this.m.d.log(java.util.logging.Level.INFO, new StringBuilder("Setting BT:").append(v0_25).toString());
        if (v0_25 != null) {
            this.b.b(1);
        }
        this.a.ts3client_setPlaybackConfigValue(this.e, "volume_modifier", new StringBuilder().append((1056964608 * ((float) this.m.e.getInt("volume_modifier", 0)))).toString());
        return;
    }

    public final java.util.HashMap o()
    {
        return this.j;
    }

    public final com.teamspeak.ts3client.data.ab p()
    {
        return this.v;
    }

    public final com.teamspeak.ts3client.data.d q()
    {
        return this.d;
    }

    public final void r()
    {
        this.E();
        this.n();
        return;
    }
}
