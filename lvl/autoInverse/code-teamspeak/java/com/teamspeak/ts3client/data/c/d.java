package com.teamspeak.ts3client.data.c;
public final class d {
    String a;
    android.graphics.drawable.Drawable b;
    java.util.HashMap c;
    private java.io.File d;
    private com.teamspeak.ts3client.Ts3Application e;

    public d(android.content.Context p9)
    {
        this.a = new StringBuilder().append(android.os.Environment.getExternalStorageDirectory()).append("/TS3/cache/").toString();
        this.c = new java.util.HashMap();
        this.e = ((com.teamspeak.ts3client.Ts3Application) p9.getApplicationContext());
        this.a = new StringBuilder().append(this.a).append(this.e.a.q.replace("/", "")).append("/icons/").toString();
        this.d = new java.io.File(this.a);
        if (!this.d.exists()) {
            this.d.mkdirs();
        }
        this.c.put(Long.valueOf(0), new com.teamspeak.ts3client.data.c.c(0, this.d, this.e));
        return;
    }

    private boolean b(long p4)
    {
        return this.c.containsKey(Long.valueOf(p4));
    }

    public final com.teamspeak.ts3client.data.c.c a(long p4)
    {
        com.teamspeak.ts3client.data.c.c v0_6;
        if (!this.c.containsKey(Long.valueOf(p4))) {
            this.c.put(Long.valueOf(p4), new com.teamspeak.ts3client.data.c.c(p4, this.d, this.e));
            v0_6 = ((com.teamspeak.ts3client.data.c.c) this.c.get(Long.valueOf(p4)));
        } else {
            v0_6 = ((com.teamspeak.ts3client.data.c.c) this.c.get(Long.valueOf(p4)));
        }
        return v0_6;
    }
}
