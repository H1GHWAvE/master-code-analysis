package com.teamspeak.ts3client.data.d;
public final class q {
    private static java.util.Vector a;
    private static java.util.Vector b;
    private static com.a.b.d.bw c;

    static q()
    {
        com.teamspeak.ts3client.data.d.q.a = new java.util.Vector();
        com.teamspeak.ts3client.data.d.q.b = new java.util.Vector();
        com.teamspeak.ts3client.data.d.q.c = com.a.b.d.hy.a();
        return;
    }

    public q()
    {
        return;
    }

    public static void a()
    {
        java.util.Iterator v1_0 = com.teamspeak.ts3client.data.d.q.a.iterator();
        while (v1_0.hasNext()) {
            ((android.app.Dialog) v1_0.next()).dismiss();
        }
        com.teamspeak.ts3client.data.d.q.a.clear();
        java.util.Iterator v1_1 = com.teamspeak.ts3client.data.d.q.b.iterator();
        while (v1_1.hasNext()) {
            com.a.b.d.bw v0_8 = ((android.support.v4.app.ax) v1_1.next());
            if (v0_8.k()) {
                v0_8.b();
            }
        }
        com.teamspeak.ts3client.data.d.q.b.clear();
        com.teamspeak.ts3client.data.d.q.c.clear();
        return;
    }

    public static void a(android.app.Dialog p1)
    {
        p1.setOnDismissListener(new com.teamspeak.ts3client.data.d.r(p1));
        com.teamspeak.ts3client.data.d.q.a.add(p1);
        return;
    }

    public static void a(android.support.v4.app.ax p1)
    {
        com.teamspeak.ts3client.data.d.q.b.add(p1);
        return;
    }

    public static void a(android.support.v4.app.ax p1, String p2)
    {
        com.teamspeak.ts3client.data.d.q.b.add(p1);
        com.teamspeak.ts3client.data.d.q.c.put(p2, p1);
        return;
    }

    public static void a(String p1)
    {
        if (com.teamspeak.ts3client.data.d.q.c.containsKey(p1)) {
            ((android.support.v4.app.ax) com.teamspeak.ts3client.data.d.q.c.get(p1)).b();
        }
        return;
    }

    static synthetic java.util.Vector b()
    {
        return com.teamspeak.ts3client.data.d.q.a;
    }

    public static void b(android.support.v4.app.ax p1)
    {
        com.teamspeak.ts3client.data.d.q.b.remove(p1);
        com.teamspeak.ts3client.data.d.q.c.b().remove(p1);
        return;
    }

    private static void b(String p3)
    {
        if (com.teamspeak.ts3client.data.d.q.c.containsKey(p3)) {
            com.teamspeak.ts3client.data.d.q.b.remove(com.teamspeak.ts3client.data.d.q.c.get(com.teamspeak.ts3client.data.d.q.c));
            com.teamspeak.ts3client.data.d.q.c.remove(p3);
        }
        return;
    }
}
