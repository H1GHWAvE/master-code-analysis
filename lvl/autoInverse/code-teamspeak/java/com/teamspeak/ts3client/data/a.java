package com.teamspeak.ts3client.data;
public final class a {
    public String a;
    public long b;
    public java.util.concurrent.CopyOnWriteArrayList c;
    public boolean d;
    public long e;
    public int f;
    public long g;
    public int h;
    public int i;
    public boolean j;
    public boolean k;
    public int l;
    public boolean m;
    boolean n;
    public boolean o;
    public boolean p;
    public boolean q;
    public boolean r;
    long s;
    private java.util.ArrayList t;
    private boolean u;
    private Object v;

    public a()
    {
        this.a = "";
        this.b = 0;
        this.d = 1;
        this.e = 0;
        this.f = 0;
        this.g = 0;
        this.h = 0;
        this.i = 0;
        this.j = 0;
        this.k = 0;
        this.l = 0;
        this.m = 0;
        this.n = 0;
        this.o = 1;
        this.p = 0;
        this.q = 0;
        this.r = 0;
        this.s = 0;
        this.u = 0;
        this.v = new Object();
        return;
    }

    private a(String p7, long p8, long p10, int p12, long p13)
    {
        this.a = "";
        this.b = 0;
        this.d = 1;
        this.e = 0;
        this.f = 0;
        this.g = 0;
        this.h = 0;
        this.i = 0;
        this.j = 0;
        this.k = 0;
        this.l = 0;
        this.m = 0;
        this.n = 0;
        this.o = 1;
        this.p = 0;
        this.q = 0;
        this.r = 0;
        this.s = 0;
        this.u = 0;
        this.v = new Object();
        this.a = com.teamspeak.ts3client.data.d.x.a(p7);
        this.b = p8;
        this.f = p12;
        this.e = p10;
        this.g = p13;
        this.c = new java.util.concurrent.CopyOnWriteArrayList();
        return;
    }

    public a(String p7, long p8, long p10, long p12)
    {
        this.a = "";
        this.b = 0;
        this.d = 1;
        this.e = 0;
        this.f = 0;
        this.g = 0;
        this.h = 0;
        this.i = 0;
        this.j = 0;
        this.k = 0;
        this.l = 0;
        this.m = 0;
        this.n = 0;
        this.o = 1;
        this.p = 0;
        this.q = 0;
        this.r = 0;
        this.s = 0;
        this.u = 0;
        this.v = new Object();
        this.a = com.teamspeak.ts3client.data.d.x.a(p7);
        this.b = p8;
        this.e = p10;
        this.g = p12;
        this.c = new java.util.concurrent.CopyOnWriteArrayList();
        return;
    }

    private void f(boolean p1)
    {
        this.p = p1;
        return;
    }

    private boolean f(int p3)
    {
        return this.c.contains(Integer.valueOf(p3));
    }

    private void g(boolean p1)
    {
        this.q = p1;
        return;
    }

    private int l()
    {
        return this.i;
    }

    private int m()
    {
        return this.h;
    }

    private void n()
    {
        this.b = -1;
        return;
    }

    private boolean o()
    {
        return this.r;
    }

    private boolean p()
    {
        return this.j;
    }

    private boolean q()
    {
        return this.o;
    }

    private boolean r()
    {
        return this.p;
    }

    private boolean s()
    {
        return this.q;
    }

    private boolean t()
    {
        return this.k;
    }

    private long u()
    {
        return this.s;
    }

    private boolean v()
    {
        return this.n;
    }

    private int w()
    {
        return this.l;
    }

    private void x()
    {
        this.d = 1;
        return;
    }

    public final void a()
    {
        this.c = new java.util.concurrent.CopyOnWriteArrayList();
        this.d = 1;
        return;
    }

    public final void a(int p1)
    {
        this.i = p1;
        return;
    }

    public final void a(long p2)
    {
        this.g = p2;
        return;
    }

    public final void a(String p2)
    {
        this.a = com.teamspeak.ts3client.data.d.x.a(p2);
        return;
    }

    public final void a(boolean p1)
    {
        this.r = p1;
        return;
    }

    public final long b()
    {
        return this.b;
    }

    public final void b(int p1)
    {
        this.h = p1;
        return;
    }

    public final void b(long p2)
    {
        this.e = p2;
        return;
    }

    public final void b(boolean p1)
    {
        this.j = p1;
        return;
    }

    public final int c()
    {
        return this.f;
    }

    public final void c(int p1)
    {
        this.l = p1;
        return;
    }

    public final void c(long p4)
    {
        this.s = (2.1219957905e-314 & p4);
        return;
    }

    public final void c(boolean p1)
    {
        this.k = p1;
        return;
    }

    public final void d()
    {
        this.f = 0;
        return;
    }

    public final void d(int p4)
    {
        if (this.u) {
            android.util.Log.d("Channel", new StringBuilder("Add Client ").append(p4).append(" in ").append(this.a).toString());
        }
        if (!this.c.contains(Integer.valueOf(p4))) {
            this.c.add(Integer.valueOf(p4));
        }
        this.d = 1;
        return;
    }

    public final void d(boolean p1)
    {
        this.m = p1;
        return;
    }

    public final String e()
    {
        return this.a;
    }

    public final void e(int p4)
    {
        if (this.u) {
            android.util.Log.d("Channel", new StringBuilder("Removed Client ").append(p4).append(" in ").append(this.a).toString());
        }
        this.c.remove(Integer.valueOf(p4));
        this.d = 1;
        return;
    }

    public final void e(boolean p1)
    {
        this.n = p1;
        return;
    }

    public final long f()
    {
        return this.g;
    }

    public final long g()
    {
        return this.e;
    }

    public final void h()
    {
        this.o = 1;
        return;
    }

    public final boolean i()
    {
        return this.m;
    }

    public final java.util.concurrent.CopyOnWriteArrayList j()
    {
        if (this.u) {
            android.util.Log.d("Channel", new StringBuilder("Clients in ").append(this.a).append(":").append(this.c.toString()).toString());
        }
        return this.c;
    }

    public final java.util.ArrayList k()
    {
        java.util.ArrayList v0_23;
        if (this.d) {
            boolean v3 = com.teamspeak.ts3client.Ts3Application.a().e.getBoolean("show_squeryclient", 0);
            try {
                if ((com.teamspeak.ts3client.Ts3Application.a().a == null) || (com.teamspeak.ts3client.Ts3Application.a().a.s == null)) {
                    java.util.ArrayList v0_12 = 0;
                    java.util.ArrayList v2_8 = v0_12;
                } else {
                    java.util.ArrayList v0_11;
                    java.util.ArrayList v0_10 = com.teamspeak.ts3client.Ts3Application.a().a.s;
                    if ((v0_10.b.a.u == null) || (v0_10.b.a.u.c)) {
                        v0_11 = v0_10.c;
                    } else {
                        v0_11 = 0;
                    }
                    if (v0_11 == null) {
                    } else {
                        v0_12 = com.teamspeak.ts3client.Ts3Application.a().a.s.b(com.teamspeak.ts3client.Ts3Application.a().a.u.a(com.teamspeak.ts3client.jni.g.cK));
                    }
                }
            } catch (java.util.ArrayList v0) {
                v2_8 = 0;
            }
            this.t = new java.util.ArrayList();
            java.util.ArrayList v5_1 = new java.util.ArrayList();
            java.util.Iterator v6 = this.c.iterator();
            while (v6.hasNext()) {
                java.util.ArrayList v0_31 = com.teamspeak.ts3client.Ts3Application.a().a.d.b(((Integer) v6.next()).intValue());
                if (v0_31 != null) {
                    if (!v3) {
                        if (v0_31.v == 0) {
                            v5_1.add(v0_31);
                        }
                    } else {
                        if (v0_31.e <= v2_8) {
                            v5_1.add(v0_31);
                        }
                    }
                }
            }
            this.d = 0;
            java.util.Collections.sort(v5_1);
            java.util.Iterator v1_1 = v5_1.iterator();
            while (v1_1.hasNext()) {
                this.t.add(Integer.valueOf(((com.teamspeak.ts3client.data.c) v1_1.next()).c));
            }
            v0_23 = this.t;
        } else {
            v0_23 = this.t;
        }
        return v0_23;
    }

    public final String toString()
    {
        return this.a;
    }
}
