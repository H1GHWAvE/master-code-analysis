package com.teamspeak.ts3client.data.b;
public final class c {
    public static com.teamspeak.ts3client.data.b.c a = None;
    private static final String d = "create table contacts (contact_id integer primary key autoincrement, u_identifier text not null, customname text not null , display integer not null, status integer not null, mute integer not null, ignorepublicchat integer not null, ignoreprivatechat integer not null, ignorepokes integer not null, hideaway integer not null,hideavatar integer not null, whisperallow integer not null, volumemodifier float not null);";
    private static final String e = "Create Index u_identifier_idx ON contacts(u_identifier);";
    private static final String f = "contacts";
    private static final String g = "Teamspeak-Contacts";
    private static final int h = 2;
    public java.util.Vector b;
    public com.teamspeak.ts3client.c.b c;
    private android.database.sqlite.SQLiteDatabase i;
    private java.util.HashMap j;

    public c(android.content.Context p2)
    {
        this.b = new java.util.Vector();
        this.j = 0;
        this.i = new com.teamspeak.ts3client.data.b.d(this, p2).getWritableDatabase();
        com.teamspeak.ts3client.data.b.c.a = this;
        this.f();
        return;
    }

    private com.teamspeak.ts3client.c.a a(int p14)
    {
        com.teamspeak.ts3client.c.a v9_1 = new com.teamspeak.ts3client.c.a();
        try {
            float v0_0 = this.i;
            String[] v2_1 = new String[13];
            v2_1[0] = "contact_id";
            v2_1[1] = "u_identifier";
            v2_1[2] = "customname";
            v2_1[3] = "display";
            v2_1[4] = "status";
            v2_1[5] = "mute";
            v2_1[6] = "ignorepublicchat";
            v2_1[7] = "ignoreprivatechat";
            v2_1[8] = "ignorepokes";
            v2_1[9] = "hideaway";
            v2_1[10] = "hideavatar";
            v2_1[11] = "whisperallow";
            v2_1[12] = "volumemodifier";
            android.database.Cursor v1_1 = v0_0.query("contacts", v2_1, new StringBuilder("contact_id=").append(p14).toString(), 0, 0, 0, 0, 0);
        } catch (float v0) {
            float v0_48 = 0;
            return v0_48;
        }
        if (v1_1.moveToFirst()) {
            float v0_20;
            v9_1.b = v1_1.getInt(v1_1.getColumnIndex("contact_id"));
            v9_1.a(v1_1.getString(v1_1.getColumnIndex("u_identifier")));
            v9_1.a = v1_1.getString(v1_1.getColumnIndex("customname"));
            v9_1.d = v1_1.getInt(v1_1.getColumnIndex("display"));
            v9_1.e = v1_1.getInt(v1_1.getColumnIndex("status"));
            if (v1_1.getInt(v1_1.getColumnIndex("mute")) != 0) {
                v0_20 = 1;
            } else {
                v0_20 = 0;
            }
            float v0_24;
            v9_1.f = v0_20;
            if (v1_1.getInt(v1_1.getColumnIndex("ignorepublicchat")) != 0) {
                v0_24 = 1;
            } else {
                v0_24 = 0;
            }
            float v0_28;
            v9_1.g = v0_24;
            if (v1_1.getInt(v1_1.getColumnIndex("ignoreprivatechat")) != 0) {
                v0_28 = 1;
            } else {
                v0_28 = 0;
            }
            float v0_32;
            v9_1.h = v0_28;
            if (v1_1.getInt(v1_1.getColumnIndex("ignorepokes")) != 0) {
                v0_32 = 1;
            } else {
                v0_32 = 0;
            }
            float v0_36;
            v9_1.i = v0_32;
            if (v1_1.getInt(v1_1.getColumnIndex("hideaway")) != 0) {
                v0_36 = 1;
            } else {
                v0_36 = 0;
            }
            float v0_40;
            v9_1.j = v0_36;
            if (v1_1.getInt(v1_1.getColumnIndex("hideavatar")) != 0) {
                v0_40 = 1;
            } else {
                v0_40 = 0;
            }
            float v0_44;
            v9_1.k = v0_40;
            if (v1_1.getInt(v1_1.getColumnIndex("whisperallow")) != 0) {
                v0_44 = 1;
            } else {
                v0_44 = 0;
            }
            v9_1.l = v0_44;
            v9_1.m = v1_1.getFloat(v1_1.getColumnIndex("volumemodifier"));
        }
        v1_1.close();
        v0_48 = v9_1;
        return v0_48;
    }

    public static com.teamspeak.ts3client.data.b.c a()
    {
        return com.teamspeak.ts3client.data.b.c.a;
    }

    private void a(com.teamspeak.ts3client.c.b p1)
    {
        this.c = p1;
        return;
    }

    private void d()
    {
        this.b = new java.util.Vector();
        return;
    }

    private void e()
    {
        this.i.close();
        return;
    }

    private void f()
    {
        com.teamspeak.ts3client.c.a v0_0 = this.b();
        this.j = new java.util.HashMap();
        if (v0_0 != null) {
            java.util.Iterator v1_2 = v0_0.iterator();
            while (v1_2.hasNext()) {
                com.teamspeak.ts3client.c.a v0_3 = ((com.teamspeak.ts3client.c.a) v1_2.next());
                this.j.put(v0_3.c, v0_3);
            }
        }
        return;
    }

    private void g()
    {
        this.c = 0;
        return;
    }

    public final long a(com.teamspeak.ts3client.c.a p6)
    {
        long v0_5;
        String v1_0 = 1;
        android.content.ContentValues v3_1 = new android.content.ContentValues();
        v3_1.put("customname", p6.a);
        v3_1.put("u_identifier", p6.c);
        v3_1.put("display", Integer.valueOf(p6.d));
        v3_1.put("status", Integer.valueOf(p6.e));
        if (!p6.f) {
            v0_5 = 0;
        } else {
            v0_5 = 1;
        }
        long v0_8;
        v3_1.put("mute", Integer.valueOf(v0_5));
        if (!p6.g) {
            v0_8 = 0;
        } else {
            v0_8 = 1;
        }
        long v0_11;
        v3_1.put("ignorepublicchat", Integer.valueOf(v0_8));
        if (!p6.h) {
            v0_11 = 0;
        } else {
            v0_11 = 1;
        }
        long v0_14;
        v3_1.put("ignoreprivatechat", Integer.valueOf(v0_11));
        if (!p6.i) {
            v0_14 = 0;
        } else {
            v0_14 = 1;
        }
        long v0_17;
        v3_1.put("ignorepokes", Integer.valueOf(v0_14));
        if (!p6.j) {
            v0_17 = 0;
        } else {
            v0_17 = 1;
        }
        long v0_20;
        v3_1.put("hideaway", Integer.valueOf(v0_17));
        if (!p6.k) {
            v0_20 = 0;
        } else {
            v0_20 = 1;
        }
        v3_1.put("hideavatar", Integer.valueOf(v0_20));
        if (!p6.l) {
            v1_0 = 0;
        }
        v3_1.put("whisperallow", Integer.valueOf(v1_0));
        v3_1.put("volumemodifier", Float.valueOf(p6.m));
        try {
            long v0_25 = this.i.insert("contacts", 0, v3_1);
            this.f();
        } catch (long v0_26) {
            v0_26.printStackTrace();
            v0_25 = -1;
        }
        return v0_25;
    }

    public final com.teamspeak.ts3client.c.a a(String p4)
    {
        com.teamspeak.ts3client.c.a v0_0 = 0;
        if (p4 != null) {
            String v1 = android.database.DatabaseUtils.sqlEscapeString(p4);
            if (this.j.containsKey(v1)) {
                v0_0 = ((com.teamspeak.ts3client.c.a) this.j.get(v1));
            }
        }
        return v0_0;
    }

    public final boolean a(long p6)
    {
        try {
            this.i.delete("contacts", new StringBuilder("contact_id=").append(p6).toString(), 0);
            this.f();
            this.c();
            int v0_1 = 1;
        } catch (int v0_2) {
            v0_2.printStackTrace();
            v0_1 = 0;
        }
        return v0_1;
    }

    public final boolean a(long p8, com.teamspeak.ts3client.c.a p10)
    {
        Exception v0_5;
        int v1 = 1;
        android.content.ContentValues v3_1 = new android.content.ContentValues();
        v3_1.put("customname", p10.a);
        v3_1.put("u_identifier", p10.c);
        v3_1.put("display", Integer.valueOf(p10.d));
        v3_1.put("status", Integer.valueOf(p10.e));
        if (!p10.f) {
            v0_5 = 0;
        } else {
            v0_5 = 1;
        }
        Exception v0_8;
        v3_1.put("mute", Integer.valueOf(v0_5));
        if (!p10.g) {
            v0_8 = 0;
        } else {
            v0_8 = 1;
        }
        Exception v0_11;
        v3_1.put("ignorepublicchat", Integer.valueOf(v0_8));
        if (!p10.h) {
            v0_11 = 0;
        } else {
            v0_11 = 1;
        }
        Exception v0_14;
        v3_1.put("ignoreprivatechat", Integer.valueOf(v0_11));
        if (!p10.i) {
            v0_14 = 0;
        } else {
            v0_14 = 1;
        }
        Exception v0_17;
        v3_1.put("ignorepokes", Integer.valueOf(v0_14));
        if (!p10.j) {
            v0_17 = 0;
        } else {
            v0_17 = 1;
        }
        Exception v0_20;
        v3_1.put("hideaway", Integer.valueOf(v0_17));
        if (!p10.k) {
            v0_20 = 0;
        } else {
            v0_20 = 1;
        }
        Exception v0_23;
        v3_1.put("hideavatar", Integer.valueOf(v0_20));
        if (!p10.l) {
            v0_23 = 0;
        } else {
            v0_23 = 1;
        }
        v3_1.put("whisperallow", Integer.valueOf(v0_23));
        v3_1.put("volumemodifier", Float.valueOf(p10.m));
        try {
            this.i.update("contacts", v3_1, new StringBuilder("contact_id=").append(p8).toString(), 0);
            this.f();
            this.c();
        } catch (Exception v0_27) {
            v0_27.printStackTrace();
            v1 = 0;
        }
        return v1;
    }

    public final java.util.ArrayList b()
    {
        java.util.ArrayList v9_1 = new java.util.ArrayList();
        try {
            boolean v0_50;
            boolean v0_0 = this.i;
            com.teamspeak.ts3client.c.a v2_1 = new String[13];
            v2_1[0] = "contact_id";
            v2_1[1] = "u_identifier";
            v2_1[2] = "customname";
            v2_1[3] = "display";
            v2_1[4] = "status";
            v2_1[5] = "mute";
            v2_1[6] = "ignorepublicchat";
            v2_1[7] = "ignoreprivatechat";
            v2_1[8] = "ignorepokes";
            v2_1[9] = "hideaway";
            v2_1[10] = "hideavatar";
            v2_1[11] = "whisperallow";
            v2_1[12] = "volumemodifier";
            android.database.Cursor v1_1 = v0_0.query("contacts", v2_1, 0, 0, 0, 0, 0);
        } catch (boolean v0_51) {
            v0_51.printStackTrace();
            v0_50 = 0;
            return v0_50;
        }
        if (v1_1.moveToFirst()) {
            if (!v1_1.isAfterLast()) {
                do {
                    boolean v0_21;
                    com.teamspeak.ts3client.c.a v2_3 = new com.teamspeak.ts3client.c.a();
                    v2_3.b = v1_1.getInt(v1_1.getColumnIndex("contact_id"));
                    v2_3.a(v1_1.getString(v1_1.getColumnIndex("u_identifier")));
                    v2_3.a = v1_1.getString(v1_1.getColumnIndex("customname"));
                    v2_3.d = v1_1.getInt(v1_1.getColumnIndex("display"));
                    v2_3.e = v1_1.getInt(v1_1.getColumnIndex("status"));
                    if (v1_1.getInt(v1_1.getColumnIndex("mute")) != 0) {
                        v0_21 = 1;
                    } else {
                        v0_21 = 0;
                    }
                    boolean v0_25;
                    v2_3.f = v0_21;
                    if (v1_1.getInt(v1_1.getColumnIndex("ignorepublicchat")) != 0) {
                        v0_25 = 1;
                    } else {
                        v0_25 = 0;
                    }
                    boolean v0_29;
                    v2_3.g = v0_25;
                    if (v1_1.getInt(v1_1.getColumnIndex("ignoreprivatechat")) != 0) {
                        v0_29 = 1;
                    } else {
                        v0_29 = 0;
                    }
                    boolean v0_33;
                    v2_3.h = v0_29;
                    if (v1_1.getInt(v1_1.getColumnIndex("ignorepokes")) != 0) {
                        v0_33 = 1;
                    } else {
                        v0_33 = 0;
                    }
                    boolean v0_37;
                    v2_3.i = v0_33;
                    if (v1_1.getInt(v1_1.getColumnIndex("hideaway")) != 0) {
                        v0_37 = 1;
                    } else {
                        v0_37 = 0;
                    }
                    boolean v0_41;
                    v2_3.j = v0_37;
                    if (v1_1.getInt(v1_1.getColumnIndex("hideavatar")) != 0) {
                        v0_41 = 1;
                    } else {
                        v0_41 = 0;
                    }
                    boolean v0_45;
                    v2_3.k = v0_41;
                    if (v1_1.getInt(v1_1.getColumnIndex("whisperallow")) != 0) {
                        v0_45 = 1;
                    } else {
                        v0_45 = 0;
                    }
                    v2_3.l = v0_45;
                    v2_3.m = v1_1.getFloat(v1_1.getColumnIndex("volumemodifier"));
                    v9_1.add(v2_3);
                } while(v1_1.moveToNext());
            }
            v1_1.close();
            v0_50 = v9_1;
            return v0_50;
        } else {
            v1_1.close();
            v0_50 = 0;
            return v0_50;
        }
    }

    public final void c()
    {
        if (this.c != null) {
            this.c.a();
        }
        if (com.teamspeak.ts3client.Ts3Application.a().a != null) {
            java.util.Iterator v1 = com.teamspeak.ts3client.Ts3Application.a().a.d.a().entrySet().iterator();
            while (v1.hasNext()) {
                ((com.teamspeak.ts3client.data.c) ((java.util.Map$Entry) v1.next()).getValue()).a();
            }
        }
        return;
    }
}
