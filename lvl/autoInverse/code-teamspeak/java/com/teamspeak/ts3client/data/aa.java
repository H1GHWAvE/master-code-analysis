package com.teamspeak.ts3client.data;
public final class aa {
    private static final java.util.HashMap a;

    static aa()
    {
        java.util.HashMap v0_1 = new java.util.HashMap();
        com.teamspeak.ts3client.data.aa.a = v0_1;
        v0_1.put(Integer.valueOf(0), "ERROR_ok");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(1), "ERROR_undefined");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2), "ERROR_not_implemented");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(3), "ERROR_ok_no_update");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(4), "ERROR_dont_notify");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(5), "ERROR_lib_time_limit_reached");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(256), "ERROR_command_not_found");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(257), "ERROR_unable_to_bind_network_port");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(258), "ERROR_no_network_port_available");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(512), "ERROR_client_invalid_id");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(513), "ERROR_client_nickname_inuse");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(515), "ERROR_client_protocol_limit_reached");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(516), "ERROR_client_invalid_type");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(517), "ERROR_client_already_subscribed");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(518), "ERROR_client_not_logged_in");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(519), "ERROR_client_could_not_validate_identity");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(522), "ERROR_client_version_outdated");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(524), "ERROR_client_is_flooding");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(768), "ERROR_channel_invalid_id");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(769), "ERROR_channel_protocol_limit_reached");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(770), "ERROR_channel_already_in");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(771), "ERROR_channel_name_inuse");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(772), "ERROR_channel_not_empty");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(773), "ERROR_channel_can_not_delete_default");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(774), "ERROR_channel_default_require_permanent");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(775), "ERROR_channel_invalid_flags");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(776), "ERROR_channel_parent_not_permanent");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(777), "ERROR_channel_maxclients_reached");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(778), "ERROR_channel_maxfamily_reached");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(779), "ERROR_channel_invalid_order");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(780), "ERROR_channel_no_filetransfer_supported");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(781), "ERROR_channel_invalid_password");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(1024), "ERROR_server_invalid_id");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(1025), "ERROR_server_running");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(1026), "ERROR_server_is_shutting_down");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(1027), "ERROR_server_maxclients_reached");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(1028), "ERROR_server_invalid_password");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(1031), "ERROR_server_is_virtual");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(1033), "ERROR_server_is_not_running");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(1034), "ERROR_server_is_booting");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(1035), "ERROR_server_status_invalid");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(1536), "ERROR_parameter_quote");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(1537), "ERROR_parameter_invalid_count");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(1538), "ERROR_parameter_invalid");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(1539), "ERROR_parameter_not_found");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(1540), "ERROR_parameter_convert");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(1541), "ERROR_parameter_invalid_size");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(1542), "ERROR_parameter_missing");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(1792), "ERROR_vs_critical");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(1793), "ERROR_connection_lost");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(1794), "ERROR_not_connected");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(1795), "ERROR_no_cached_connection_info");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(1796), "ERROR_currently_not_possible");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(1797), "ERROR_failed_connection_initialisation");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(1798), "ERROR_could_not_resolve_hostname");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(1799), "ERROR_invalid_server_connection_handler_id");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(1800), "ERROR_could_not_initialise_input_manager");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(1801), "ERROR_clientlibrary_not_initialised");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(1802), "ERROR_serverlibrary_not_initialised");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(1803), "ERROR_whisper_too_many_targets");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(1804), "ERROR_whisper_no_targets");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2304), "ERROR_sound_preprocessor_disabled");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2305), "ERROR_sound_internal_preprocessor");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2306), "ERROR_sound_internal_encoder");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2307), "ERROR_sound_internal_playback");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2308), "ERROR_sound_no_capture_device_available");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2309), "ERROR_sound_no_playback_device_available");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2310), "ERROR_sound_could_not_open_capture_device");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2311), "ERROR_sound_could_not_open_playback_device");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2312), "ERROR_sound_handler_has_device");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2313), "ERROR_sound_invalid_capture_device");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2314), "ERROR_sound_invalid_playback_device");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2315), "ERROR_sound_invalid_wave");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2316), "ERROR_sound_unsupported_wave");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2317), "ERROR_sound_open_wave");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2318), "ERROR_sound_internal_capture");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2319), "ERROR_sound_device_in_use");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2320), "ERROR_sound_device_already_registerred");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2321), "ERROR_sound_unknown_device");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2322), "ERROR_sound_unsupported_frequency");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2323), "ERROR_sound_invalid_channel_count");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2324), "ERROR_sound_read_wave");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2325), "ERROR_sound_need_more_data");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2326), "ERROR_sound_device_busy");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2327), "ERROR_sound_no_data");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2328), "ERROR_sound_channel_mask_mismatch");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2816), "ERROR_accounting_virtualserver_limit_reached");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2817), "ERROR_accounting_slot_limit_reached");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2818), "ERROR_accounting_license_file_not_found");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2819), "ERROR_accounting_license_date_not_ok");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2820), "ERROR_accounting_unable_to_connect_to_server");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2821), "ERROR_accounting_unknown_error");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2822), "ERROR_accounting_server_error");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2823), "ERROR_accounting_instance_limit_reached");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2824), "ERROR_accounting_instance_check_error");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2825), "ERROR_accounting_license_file_invalid");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2826), "ERROR_accounting_running_elsewhere");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2827), "ERROR_accounting_instance_duplicated");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2828), "ERROR_accounting_already_started");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2829), "ERROR_accounting_not_started");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(2830), "ERROR_accounting_to_many_starts");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(4352), "ERROR_provisioning_invalid_password");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(4353), "ERROR_provisioning_invalid_request");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(4354), "ERROR_provisioning_no_slots_available");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(4355), "ERROR_provisioning_pool_missing");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(4356), "ERROR_provisioning_pool_unknown");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(4357), "ERROR_provisioning_unknown_ip_location");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(4358), "ERROR_provisioning_internal_tries_exceeded");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(4359), "ERROR_provisioning_too_many_slots_requested");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(4360), "ERROR_provisioning_too_many_reserved");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(4361), "ERROR_provisioning_could_not_connect");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(4368), "ERROR_provisioning_auth_server_not_connected");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(4369), "ERROR_provisioning_auth_data_too_large");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(4370), "ERROR_provisioning_already_initialized");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(4371), "ERROR_provisioning_not_initialized");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(4372), "ERROR_provisioning_connecting");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(4373), "ERROR_provisioning_already_connected");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(4374), "ERROR_provisioning_not_connected");
        com.teamspeak.ts3client.data.aa.a.put(Integer.valueOf(4375), "ERROR_provisioning_io_error");
        return;
    }

    public aa()
    {
        return;
    }

    public static String a(int p2)
    {
        return ((String) com.teamspeak.ts3client.data.aa.a.get(Integer.valueOf(p2)));
    }
}
