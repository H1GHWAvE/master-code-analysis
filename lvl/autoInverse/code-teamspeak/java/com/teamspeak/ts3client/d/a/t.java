package com.teamspeak.ts3client.d.a;
public final class t extends android.support.v4.app.ax {
    private com.teamspeak.ts3client.d.a.m at;
    private android.widget.EditText au;
    private android.widget.Button av;
    private android.view.View aw;
    private android.view.View ax;
    private android.view.View ay;

    public t(com.teamspeak.ts3client.d.a.m p1)
    {
        this.at = p1;
        return;
    }

    static synthetic android.widget.EditText a(com.teamspeak.ts3client.d.a.t p1)
    {
        return p1.au;
    }

    static synthetic com.teamspeak.ts3client.d.a.m b(com.teamspeak.ts3client.d.a.t p1)
    {
        return p1.at;
    }

    public final android.view.View a(android.view.LayoutInflater p4, android.view.ViewGroup p5)
    {
        android.widget.LinearLayout v0_2 = ((android.widget.LinearLayout) p4.inflate(2130903074, p5, 0));
        this.j.setTitle(com.teamspeak.ts3client.data.e.a.a("channeldialog.editdesc.dialog"));
        this.ay = v0_2.findViewById(2131493064);
        this.ay.setOnClickListener(new com.teamspeak.ts3client.d.a.u(this));
        this.ax = v0_2.findViewById(2131493063);
        this.ax.setOnClickListener(new com.teamspeak.ts3client.d.a.v(this));
        this.aw = v0_2.findViewById(2131493065);
        this.aw.setOnClickListener(new com.teamspeak.ts3client.d.a.w(this));
        this.au = ((android.widget.EditText) v0_2.findViewById(2131493066));
        this.au.setText(this.at.at);
        this.av = ((android.widget.Button) v0_2.findViewById(2131493067));
        this.av.setText(com.teamspeak.ts3client.data.e.a.a("button.save"));
        this.av.setOnClickListener(new com.teamspeak.ts3client.d.a.x(this));
        return v0_2;
    }

    protected final void b(String p8)
    {
        android.widget.EditText v0_2 = this.au.getText().toString();
        int v1_1 = this.au.getSelectionStart();
        android.widget.EditText v2_1 = this.au.getSelectionEnd();
        if (v1_1 == v2_1) {
            android.widget.EditText v0_5;
            if (this.au.getSelectionStart() != v0_2.length()) {
                v0_5 = new StringBuilder().append(v0_2.substring(0, v1_1)).append(p8).append(v0_2.substring(v1_1, v0_2.length())).toString();
            } else {
                v0_5 = new StringBuilder().append(v0_2).append(p8).toString();
            }
            this.au.setText(v0_5);
            this.au.setSelection((v1_1 + 3));
        } else {
            this.au.setText(new StringBuilder().append(v0_2.substring(0, v1_1)).append(p8.substring(0, 3)).append(v0_2.substring(v1_1, v2_1)).append(p8.substring(3, 7)).append(v0_2.substring(v2_1, v0_2.length())).toString());
            this.au.setSelection(((v2_1 + (v1_1 + 3)) - v1_1));
        }
        return;
    }

    public final void e()
    {
        super.e();
        com.teamspeak.ts3client.data.d.q.a(this);
        return;
    }

    public final void f()
    {
        super.f();
        com.teamspeak.ts3client.data.d.q.b(this);
        return;
    }
}
