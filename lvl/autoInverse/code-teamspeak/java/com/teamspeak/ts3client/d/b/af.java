package com.teamspeak.ts3client.d.b;
public final class af extends android.support.v4.app.ax {
    private com.teamspeak.ts3client.data.c at;
    private boolean au;
    private android.widget.EditText av;
    private android.widget.TextView aw;

    public af(com.teamspeak.ts3client.data.c p1, boolean p2)
    {
        this.at = p1;
        this.au = p2;
        return;
    }

    static synthetic boolean a(com.teamspeak.ts3client.d.b.af p1)
    {
        return p1.au;
    }

    static synthetic com.teamspeak.ts3client.data.c b(com.teamspeak.ts3client.d.b.af p1)
    {
        return p1.at;
    }

    static synthetic android.widget.EditText c(com.teamspeak.ts3client.d.b.af p1)
    {
        return p1.av;
    }

    public final android.view.View a(android.view.LayoutInflater p12, android.view.ViewGroup p13)
    {
        android.app.Dialog v0_2 = ((com.teamspeak.ts3client.Ts3Application) p12.getContext().getApplicationContext());
        android.widget.RelativeLayout v1_1 = new android.widget.RelativeLayout(p12.getContext());
        v1_1.setLayoutParams(new android.widget.RelativeLayout$LayoutParams(-1, -2));
        this.aw = new android.widget.TextView(p12.getContext());
        this.aw.setId(1);
        if (!this.au) {
            String v2_7 = this.aw;
            com.teamspeak.ts3client.d.b.ag v4_0 = new Object[1];
            v4_0[0] = this.at.a;
            v2_7.setText(com.teamspeak.ts3client.data.e.a.a("clientdialog.kick.text.channel", v4_0));
        } else {
            String v2_8 = this.aw;
            com.teamspeak.ts3client.d.b.ag v4_1 = new Object[1];
            v4_1[0] = this.at.a;
            v2_8.setText(com.teamspeak.ts3client.data.e.a.a("clientdialog.kick.text.server", v4_1));
        }
        String v2_10 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        v2_10.addRule(10);
        v1_1.addView(this.aw, v2_10);
        this.av = new android.widget.EditText(p12.getContext());
        this.av.setId(2);
        String v2_14 = new android.text.InputFilter[1];
        v2_14[0] = new android.text.InputFilter$LengthFilter(80);
        this.av.setFilters(v2_14);
        String v2_16 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        v2_16.addRule(3, this.aw.getId());
        v1_1.addView(this.av, v2_16);
        String v2_18 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        v2_18.addRule(3, this.av.getId());
        android.widget.Button v3_18 = new android.widget.Button(p12.getContext());
        v3_18.setText(com.teamspeak.ts3client.data.e.a.a("clientdialog.kick.info"));
        v3_18.setOnClickListener(new com.teamspeak.ts3client.d.b.ag(this, v0_2, v1_1));
        v1_1.addView(v3_18, v2_18);
        this.j.setTitle(this.at.a);
        return v1_1;
    }

    public final void e()
    {
        super.e();
        com.teamspeak.ts3client.data.d.q.a(this);
        return;
    }

    public final void f()
    {
        super.f();
        com.teamspeak.ts3client.data.d.q.b(this);
        return;
    }
}
