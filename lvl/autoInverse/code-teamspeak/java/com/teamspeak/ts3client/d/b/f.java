package com.teamspeak.ts3client.d.b;
final class f implements android.widget.SeekBar$OnSeekBarChangeListener {
    final synthetic android.widget.TextView a;
    final synthetic com.teamspeak.ts3client.d.b.e b;

    f(com.teamspeak.ts3client.d.b.e p1, android.widget.TextView p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final void onProgressChanged(android.widget.SeekBar p7, int p8, boolean p9)
    {
        this.a.setText(new StringBuilder().append((((float) (p8 - 60)) * 1056964608)).append(" / 30").toString());
        if (this.b.a.a != null) {
            com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_setClientVolumeModifier(this.b.a.a.e, com.teamspeak.ts3client.d.b.a.a(this.b.b).c, (((float) (p8 - 60)) * 1056964608));
        }
        return;
    }

    public final void onStartTrackingTouch(android.widget.SeekBar p1)
    {
        return;
    }

    public final void onStopTrackingTouch(android.widget.SeekBar p1)
    {
        return;
    }
}
