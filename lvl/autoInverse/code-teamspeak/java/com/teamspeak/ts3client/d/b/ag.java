package com.teamspeak.ts3client.d.b;
final class ag implements android.view.View$OnClickListener {
    final synthetic com.teamspeak.ts3client.Ts3Application a;
    final synthetic android.widget.RelativeLayout b;
    final synthetic com.teamspeak.ts3client.d.b.af c;

    ag(com.teamspeak.ts3client.d.b.af p1, com.teamspeak.ts3client.Ts3Application p2, android.widget.RelativeLayout p3)
    {
        this.c = p1;
        this.a = p2;
        this.b = p3;
        return;
    }

    public final void onClick(android.view.View p8)
    {
        p8.performHapticFeedback(1);
        if (!com.teamspeak.ts3client.d.b.af.a(this.c)) {
            this.a.a.a.ts3client_requestClientKickFromChannel(this.a.a.e, com.teamspeak.ts3client.d.b.af.b(this.c).c, com.teamspeak.ts3client.d.b.af.c(this.c).getText().toString(), new StringBuilder("Kick ").append(com.teamspeak.ts3client.d.b.af.b(this.c).c).toString());
        } else {
            this.a.a.a.ts3client_requestClientKickFromServer(this.a.a.e, com.teamspeak.ts3client.d.b.af.b(this.c).c, com.teamspeak.ts3client.d.b.af.c(this.c).getText().toString(), new StringBuilder("Kick ").append(com.teamspeak.ts3client.d.b.af.b(this.c).c).toString());
        }
        this.c.b();
        this.b.removeAllViews();
        return;
    }
}
