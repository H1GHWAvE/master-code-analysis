package com.teamspeak.ts3client.d.d;
public final class n extends android.support.v4.app.ax {
    com.teamspeak.ts3client.d.d.c at;
    private com.teamspeak.ts3client.Ts3Application au;

    public n(com.teamspeak.ts3client.d.d.c p1)
    {
        this.at = p1;
        return;
    }

    static synthetic com.teamspeak.ts3client.Ts3Application a(com.teamspeak.ts3client.d.d.n p1)
    {
        return p1.au;
    }

    public final android.view.View a(android.view.LayoutInflater p17, android.view.ViewGroup p18)
    {
        this.au = ((com.teamspeak.ts3client.Ts3Application) p17.getContext().getApplicationContext());
        android.widget.ScrollView v2_5 = ((android.widget.ScrollView) p17.inflate(2130903127, p18, 0));
        this.j.setTitle(com.teamspeak.ts3client.data.e.a.a("temppass.title"));
        com.teamspeak.ts3client.d.d.p v3_4 = ((android.widget.TextView) v2_5.findViewById(2131493334));
        StringBuilder v4_4 = ((android.widget.TextView) v2_5.findViewById(2131493336));
        java.text.DateFormat v5_2 = ((android.widget.TextView) v2_5.findViewById(2131493338));
        java.util.Date v6_2 = ((android.widget.TextView) v2_5.findViewById(2131493340));
        android.widget.TextView v7_2 = ((android.widget.TextView) v2_5.findViewById(2131493342));
        android.widget.TextView v8_2 = ((android.widget.TextView) v2_5.findViewById(2131493344));
        com.teamspeak.ts3client.data.e.a.a("temppass.info.password", v2_5, 2131493333);
        com.teamspeak.ts3client.data.e.a.a("temppass.info.creator", v2_5, 2131493335);
        com.teamspeak.ts3client.data.e.a.a("temppass.info.channel", v2_5, 2131493337);
        com.teamspeak.ts3client.data.e.a.a("temppass.info.channelpassword", v2_5, 2131493339);
        com.teamspeak.ts3client.data.e.a.a("temppass.info.valid", v2_5, 2131493341);
        com.teamspeak.ts3client.data.e.a.a("temppass.info.description", v2_5, 2131493343);
        android.widget.Button v9_8 = ((android.widget.Button) v2_5.findViewById(2131493345));
        android.widget.Button v10_8 = ((android.widget.Button) v2_5.findViewById(2131493346));
        v10_8.setText(com.teamspeak.ts3client.data.e.a.a("temppass.info.button.share"));
        v9_8.setText(com.teamspeak.ts3client.data.e.a.a("button.delete"));
        v3_4.setText(this.at.c);
        v4_4.setText(this.at.a);
        if (this.at.f != 0) {
            v5_2.setText(this.au.a.c().a(Long.valueOf(this.at.f)).a);
        }
        v6_2.setText(this.at.g);
        com.teamspeak.ts3client.d.d.p v3_16 = this.at;
        v7_2.setText(new StringBuilder().append(java.text.DateFormat.getDateTimeInstance().format(new java.util.Date((v3_16.d * 1000)))).append(" - ").append(java.text.DateFormat.getDateTimeInstance().format(new java.util.Date((v3_16.e * 1000)))).toString());
        v8_2.setText(this.at.b);
        v9_8.setOnClickListener(new com.teamspeak.ts3client.d.d.o(this));
        v10_8.setOnClickListener(new com.teamspeak.ts3client.d.d.p(this));
        return v2_5;
    }

    public final void e()
    {
        super.e();
        com.teamspeak.ts3client.data.d.q.a(this);
        return;
    }

    public final void f()
    {
        super.f();
        com.teamspeak.ts3client.data.d.q.b(this);
        return;
    }
}
