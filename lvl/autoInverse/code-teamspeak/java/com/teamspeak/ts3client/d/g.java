package com.teamspeak.ts3client.d;
final class g implements android.view.View$OnClickListener {
    final synthetic com.teamspeak.ts3client.Ts3Application a;
    final synthetic com.teamspeak.ts3client.d.e b;

    g(com.teamspeak.ts3client.d.e p1, com.teamspeak.ts3client.Ts3Application p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final void onClick(android.view.View p5)
    {
        if (!com.teamspeak.ts3client.d.e.l(this.b).getText().toString().equals("")) {
            if (com.teamspeak.ts3client.d.e.m(this.b) == null) {
                com.a.b.d.bw v0_8 = new com.teamspeak.ts3client.c.a();
                v0_8.a(com.teamspeak.ts3client.d.e.o(this.b).b);
                v0_8.a = com.teamspeak.ts3client.d.e.l(this.b).getText().toString();
                v0_8.d = com.teamspeak.ts3client.d.e.c(this.b).getSelectedItemPosition();
                v0_8.e = com.teamspeak.ts3client.d.e.n(this.b).getSelectedItemPosition();
                v0_8.f = com.teamspeak.ts3client.d.e.d(this.b).isChecked();
                v0_8.g = com.teamspeak.ts3client.d.e.e(this.b).isChecked();
                v0_8.h = com.teamspeak.ts3client.d.e.f(this.b).isChecked();
                v0_8.i = com.teamspeak.ts3client.d.e.g(this.b).isChecked();
                v0_8.k = com.teamspeak.ts3client.d.e.i(this.b).isChecked();
                v0_8.j = com.teamspeak.ts3client.d.e.h(this.b).isChecked();
                v0_8.l = com.teamspeak.ts3client.d.e.j(this.b).isChecked();
                com.teamspeak.ts3client.data.b.c.a.a(v0_8);
                com.teamspeak.ts3client.data.b.c.a.c();
            } else {
                com.teamspeak.ts3client.d.e.m(this.b).a = com.teamspeak.ts3client.d.e.l(this.b).getText().toString();
                com.teamspeak.ts3client.d.e.m(this.b).d = com.teamspeak.ts3client.d.e.c(this.b).getSelectedItemPosition();
                com.teamspeak.ts3client.d.e.m(this.b).e = com.teamspeak.ts3client.d.e.n(this.b).getSelectedItemPosition();
                com.teamspeak.ts3client.d.e.m(this.b).f = com.teamspeak.ts3client.d.e.d(this.b).isChecked();
                com.teamspeak.ts3client.d.e.m(this.b).g = com.teamspeak.ts3client.d.e.e(this.b).isChecked();
                com.teamspeak.ts3client.d.e.m(this.b).h = com.teamspeak.ts3client.d.e.f(this.b).isChecked();
                com.teamspeak.ts3client.d.e.m(this.b).i = com.teamspeak.ts3client.d.e.g(this.b).isChecked();
                com.teamspeak.ts3client.d.e.m(this.b).k = com.teamspeak.ts3client.d.e.i(this.b).isChecked();
                com.teamspeak.ts3client.d.e.m(this.b).j = com.teamspeak.ts3client.d.e.h(this.b).isChecked();
                com.teamspeak.ts3client.d.e.m(this.b).l = com.teamspeak.ts3client.d.e.j(this.b).isChecked();
                com.teamspeak.ts3client.data.b.c.a.a(((long) com.teamspeak.ts3client.d.e.m(this.b).b), com.teamspeak.ts3client.d.e.m(this.b));
            }
            if ((this.a.a != null) && (this.a.a.w.b().containsKey(com.teamspeak.ts3client.d.e.o(this.b).b))) {
                com.a.b.d.bw v0_44 = ((Integer) this.a.a.w.b().get(com.teamspeak.ts3client.d.e.o(this.b).b)).intValue();
                if (v0_44 != null) {
                    this.a.a.a.ts3client_removeFromAllowedWhispersFrom(this.a.a.e, v0_44);
                }
                this.a.a.w.b().remove(com.teamspeak.ts3client.d.e.o(this.b).b);
            }
            if (this.a.a != null) {
                this.a.a.k.y();
            }
            this.b.b();
        }
        return;
    }
}
