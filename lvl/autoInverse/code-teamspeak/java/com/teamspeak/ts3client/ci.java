package com.teamspeak.ts3client;
final class ci implements android.view.animation.Animation$AnimationListener {
    final synthetic android.view.animation.Animation a;
    final synthetic com.teamspeak.ts3client.StartGUIFragment b;

    ci(com.teamspeak.ts3client.StartGUIFragment p1, android.view.animation.Animation p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final void onAnimationEnd(android.view.animation.Animation p9)
    {
        this.b.runOnUiThread(new com.teamspeak.ts3client.cj(this));
        com.teamspeak.ts3client.StartGUIFragment.h(this.b);
        com.teamspeak.ts3client.StartGUIFragment v1_2 = this.b;
        if (v1_2.getIntent().hasExtra("UrlStart")) {
            int v0_7 = ((android.net.Uri) v1_2.getIntent().getExtras().get("uri"));
            android.app.AlertDialog v2_4 = new android.app.AlertDialog$Builder(v1_2).create();
            v2_4.setTitle(com.teamspeak.ts3client.data.e.a.a("gui.extern.info"));
            com.teamspeak.ts3client.bx v4_1 = new Object[1];
            v4_1[0] = v1_2.getIntent().getExtras().get("address");
            v2_4.setMessage(com.teamspeak.ts3client.data.e.a.a("gui.extern.text", v4_1));
            v2_4.setButton(-1, com.teamspeak.ts3client.data.e.a.a("gui.extern.button1"), new com.teamspeak.ts3client.bv(v1_2, v0_7, v2_4));
            v2_4.setButton(-3, com.teamspeak.ts3client.data.e.a.a("gui.extern.button2"), new com.teamspeak.ts3client.bw(v1_2, v0_7, v2_4));
            v2_4.setButton(-2, com.teamspeak.ts3client.data.e.a.a("button.cancel"), new com.teamspeak.ts3client.bx(v1_2, v2_4));
            v2_4.setCancelable(0);
            v2_4.show();
        }
        return;
    }

    public final void onAnimationRepeat(android.view.animation.Animation p1)
    {
        return;
    }

    public final void onAnimationStart(android.view.animation.Animation p1)
    {
        return;
    }
}
