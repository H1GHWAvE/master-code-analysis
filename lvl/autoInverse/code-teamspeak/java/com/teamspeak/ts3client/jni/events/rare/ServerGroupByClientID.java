package com.teamspeak.ts3client.jni.events.rare;
public class ServerGroupByClientID implements com.teamspeak.ts3client.jni.k {
    private long a;
    private String b;
    private long c;
    private long d;

    public ServerGroupByClientID()
    {
        return;
    }

    private ServerGroupByClientID(long p1, String p3, long p4, long p6)
    {
        this.a = p1;
        this.b = p3;
        this.c = p4;
        this.d = p6;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private long a()
    {
        return this.a;
    }

    private String b()
    {
        return this.b;
    }

    private long c()
    {
        return this.c;
    }

    private long d()
    {
        return this.d;
    }

    public String toString()
    {
        return new StringBuilder("ServerGroupByClientID [serverConnectionHandlerID=").append(this.a).append(", name=").append(this.b).append(", serverGroupID=").append(this.c).append(", clientDatabaseID=").append(this.d).append("]").toString();
    }
}
