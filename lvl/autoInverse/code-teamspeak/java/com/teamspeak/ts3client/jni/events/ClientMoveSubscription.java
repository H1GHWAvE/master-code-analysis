package com.teamspeak.ts3client.jni.events;
public class ClientMoveSubscription implements com.teamspeak.ts3client.jni.k {
    private long a;
    private int b;
    private long c;
    private long d;
    private com.teamspeak.ts3client.jni.j e;

    public ClientMoveSubscription()
    {
        return;
    }

    private ClientMoveSubscription(long p3, int p5, long p6, long p8, int p10)
    {
        this.a = p3;
        this.b = p5;
        this.c = p6;
        this.d = p8;
        if (p10 == 0) {
            this.e = com.teamspeak.ts3client.jni.j.a;
        }
        if (p10 == 1) {
            this.e = com.teamspeak.ts3client.jni.j.b;
        }
        if (p10 == 2) {
            this.e = com.teamspeak.ts3client.jni.j.c;
        }
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private int a()
    {
        return this.b;
    }

    private long b()
    {
        return this.d;
    }

    private long c()
    {
        return this.c;
    }

    private long d()
    {
        return this.a;
    }

    private com.teamspeak.ts3client.jni.j e()
    {
        return this.e;
    }

    public String toString()
    {
        return new StringBuilder("ClientMoveSubscription [serverConnectionHandlerID=").append(this.a).append(", clientID=").append(this.b).append(", oldChannelID=").append(this.c).append(", newChannelID=").append(this.d).append(", visibility=").append(this.e).append("]").toString();
    }
}
