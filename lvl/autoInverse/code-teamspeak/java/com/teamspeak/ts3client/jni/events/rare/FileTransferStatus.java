package com.teamspeak.ts3client.jni.events.rare;
public class FileTransferStatus implements com.teamspeak.ts3client.jni.k {
    public int a;
    public long b;
    private String c;
    private long d;
    private long e;

    public FileTransferStatus()
    {
        return;
    }

    private FileTransferStatus(int p2, long p3, String p5, long p6, long p8)
    {
        this.a = p2;
        this.b = p3;
        this.c = p5;
        this.d = p6;
        this.e = p8;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private long a()
    {
        return this.d;
    }

    private long b()
    {
        return this.e;
    }

    private long c()
    {
        return this.b;
    }

    private String d()
    {
        return this.c;
    }

    private int e()
    {
        return this.a;
    }

    public String toString()
    {
        return new StringBuilder("FileTransferStatus [transferID=").append(this.a).append(", status=").append(this.b).append(", statusMessage=").append(this.c).append(", remotefileSize=").append(this.d).append(", serverConnectionHandlerID=").append(this.e).append("]").toString();
    }
}
