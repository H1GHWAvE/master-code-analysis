package com.teamspeak.ts3client.jni.events.rare;
public class BanList implements com.teamspeak.ts3client.jni.k {
    public long a;
    public String b;
    public String c;
    public String d;
    public String e;
    public long f;
    public String g;
    public String h;
    public String i;
    public String j;
    public String k;
    private long l;
    private long m;
    private long n;
    private String o;
    private int p;

    public BanList()
    {
        return;
    }

    private BanList(long p9, long p11, String p13, String p14, String p15, long p16, long p18, String p20, long p21, String p23, String p24, int p25, String p26)
    {
        this.l = p9;
        this.a = p11;
        this.b = p13;
        this.c = p14;
        this.d = p15;
        this.m = p16;
        this.n = p18;
        this.e = p20;
        this.f = p21;
        this.o = p23;
        this.g = p24;
        this.p = p25;
        this.h = p26;
        if (p18 == 0) {
            this.j = "---";
        } else {
            this.j = java.text.DateFormat.getDateTimeInstance(3, 3).format(new java.util.Date(((p18 + p16) * 1000)));
        }
        this.k = java.text.DateFormat.getDateTimeInstance(3, 3).format(new java.util.Date((1000 * p16)));
        this.i = "";
        if (!this.b.equals("")) {
            this.i = this.i.concat(new StringBuilder("ip=").append(this.b).toString());
        }
        if (!this.c.equals("")) {
            this.i = this.i.concat(new StringBuilder("name=").append(this.c).toString());
        }
        if (!this.d.equals("")) {
            this.i = this.i.concat(new StringBuilder("uid").append(this.d).toString());
        }
        if (!this.h.equals("")) {
            this.i = this.i.concat(this.h);
        }
        if (!this.g.equals("")) {
            this.i = this.i.concat(this.g);
        }
        this.i = this.i.concat(this.j);
        this.i = this.i.concat(this.k);
        this.i = this.i.concat(this.e);
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private String a()
    {
        return this.i;
    }

    private void b()
    {
        this.i = "";
        if (!this.b.equals("")) {
            this.i = this.i.concat(new StringBuilder("ip=").append(this.b).toString());
        }
        if (!this.c.equals("")) {
            this.i = this.i.concat(new StringBuilder("name=").append(this.c).toString());
        }
        if (!this.d.equals("")) {
            this.i = this.i.concat(new StringBuilder("uid").append(this.d).toString());
        }
        if (!this.h.equals("")) {
            this.i = this.i.concat(this.h);
        }
        if (!this.g.equals("")) {
            this.i = this.i.concat(this.g);
        }
        this.i = this.i.concat(this.j);
        this.i = this.i.concat(this.k);
        this.i = this.i.concat(this.e);
        return;
    }

    private long c()
    {
        return this.l;
    }

    private long d()
    {
        return this.a;
    }

    private String e()
    {
        return this.b;
    }

    private String f()
    {
        return this.c;
    }

    private String g()
    {
        return this.d;
    }

    private long h()
    {
        return this.m;
    }

    private long i()
    {
        return this.n;
    }

    private String j()
    {
        return this.e;
    }

    private long k()
    {
        return this.f;
    }

    private String l()
    {
        return this.o;
    }

    private String m()
    {
        return this.g;
    }

    private int n()
    {
        return this.p;
    }

    private String o()
    {
        return this.h;
    }

    private String p()
    {
        return this.j;
    }

    private String q()
    {
        return this.k;
    }

    public String toString()
    {
        return new StringBuilder("BanList [serverConnectionHandlerID=").append(this.l).append(", banid=").append(this.a).append(", ip=").append(this.b).append(", name=").append(this.c).append(", uid=").append(this.d).append(", creationTime=").append(this.m).append(", durationTime=").append(this.n).append(", invokerName=").append(this.e).append(", invokercldbid=").append(this.f).append(", invokeruid=").append(this.o).append(", reason=").append(this.g).append(", numberOfEnforcements=").append(this.p).append(", lastNickName=").append(this.h).append("]").toString();
    }
}
