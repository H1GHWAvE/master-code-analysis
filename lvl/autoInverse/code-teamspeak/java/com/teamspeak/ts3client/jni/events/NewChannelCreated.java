package com.teamspeak.ts3client.jni.events;
public class NewChannelCreated implements com.teamspeak.ts3client.jni.k {
    private long a;
    private long b;
    private long c;
    private int d;
    private String e;
    private String f;

    public NewChannelCreated()
    {
        return;
    }

    private NewChannelCreated(long p2, long p4, long p6, int p8, String p9, String p10)
    {
        this.a = p2;
        this.b = p4;
        this.c = p6;
        this.d = p8;
        this.e = p9;
        this.f = p10;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private String d()
    {
        return this.e;
    }

    private String e()
    {
        return this.f;
    }

    private long f()
    {
        return this.a;
    }

    public final long a()
    {
        return this.b;
    }

    public final long b()
    {
        return this.c;
    }

    public final int c()
    {
        return this.d;
    }

    public String toString()
    {
        return new StringBuilder("NewChannelCreated [serverConnectionHandlerID=").append(this.a).append(", channelID=").append(this.b).append(", channelParentID=").append(this.c).append(", invokerID=").append(this.d).append(", invokerName=").append(this.e).append(", invokerUniqueIdentifier=").append(this.f).append("]").toString();
    }
}
