package com.teamspeak.ts3client.jni.events.rare;
public class ClientUIDfromClientID implements com.teamspeak.ts3client.jni.k {
    public int a;
    public String b;
    private long c;
    private String d;

    public ClientUIDfromClientID()
    {
        return;
    }

    private ClientUIDfromClientID(long p2, int p4, String p5, String p6)
    {
        this.c = p2;
        this.a = p4;
        this.b = p5;
        this.d = p6;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private String d()
    {
        return this.d;
    }

    public final long a()
    {
        return this.c;
    }

    public final int b()
    {
        return this.a;
    }

    public final String c()
    {
        return this.b;
    }

    public String toString()
    {
        return new StringBuilder("ClientUIDfromClientID [serverConnectionHandlerID=").append(this.c).append(", clientID=").append(this.a).append(", uniqueClientIdentifier=").append(this.b).append(", clientNickName=").append(this.d).append("]").toString();
    }
}
