package com.teamspeak.ts3client.jni.events;
public class TextMessage implements com.teamspeak.ts3client.jni.k {
    public int a;
    public int b;
    public int c;
    public String d;
    public String e;
    public String f;
    private long g;

    public TextMessage()
    {
        return;
    }

    private TextMessage(long p2, int p4, int p5, int p6, String p7, String p8, String p9)
    {
        this.g = p2;
        this.a = p4;
        this.b = p5;
        this.c = p6;
        this.d = p7;
        this.e = p8;
        this.f = p9;
        com.teamspeak.ts3client.jni.l.a(this);
        return;
    }

    private long f()
    {
        return this.g;
    }

    private int g()
    {
        return this.b;
    }

    public final int a()
    {
        return this.c;
    }

    public final String b()
    {
        return this.d;
    }

    public final String c()
    {
        return this.e;
    }

    public final String d()
    {
        return this.f;
    }

    public final int e()
    {
        return this.a;
    }

    public String toString()
    {
        return new StringBuilder("TextMessage [serverConnectionHandlerID=").append(this.g).append(", targetMode=").append(this.a).append(", toID=").append(this.b).append(", fromID=").append(this.c).append(", fromName=").append(this.d).append(", fromUniqueIdentifier=").append(this.e).append(", message=").append(this.f).append("]").toString();
    }
}
