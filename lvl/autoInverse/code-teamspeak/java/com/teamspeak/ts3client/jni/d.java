package com.teamspeak.ts3client.jni;
public final enum class d extends java.lang.Enum {
    public static final enum com.teamspeak.ts3client.jni.d A;
    public static final enum com.teamspeak.ts3client.jni.d B;
    public static final enum com.teamspeak.ts3client.jni.d C;
    public static final enum com.teamspeak.ts3client.jni.d D;
    public static final enum com.teamspeak.ts3client.jni.d E;
    public static final enum com.teamspeak.ts3client.jni.d F;
    public static final enum com.teamspeak.ts3client.jni.d G;
    public static final enum com.teamspeak.ts3client.jni.d H;
    public static final enum com.teamspeak.ts3client.jni.d I;
    public static final enum com.teamspeak.ts3client.jni.d J;
    public static final enum com.teamspeak.ts3client.jni.d K;
    public static final enum com.teamspeak.ts3client.jni.d L;
    public static final enum com.teamspeak.ts3client.jni.d M;
    public static final enum com.teamspeak.ts3client.jni.d N;
    public static final enum com.teamspeak.ts3client.jni.d O;
    public static final enum com.teamspeak.ts3client.jni.d P;
    public static final enum com.teamspeak.ts3client.jni.d Q;
    public static final enum com.teamspeak.ts3client.jni.d R;
    public static final enum com.teamspeak.ts3client.jni.d S;
    public static final enum com.teamspeak.ts3client.jni.d T;
    public static final enum com.teamspeak.ts3client.jni.d U;
    public static final enum com.teamspeak.ts3client.jni.d V;
    public static final enum com.teamspeak.ts3client.jni.d W;
    public static final enum com.teamspeak.ts3client.jni.d X;
    public static final enum com.teamspeak.ts3client.jni.d Y;
    public static final enum com.teamspeak.ts3client.jni.d Z;
    public static final enum com.teamspeak.ts3client.jni.d a;
    public static final enum com.teamspeak.ts3client.jni.d aa;
    public static final enum com.teamspeak.ts3client.jni.d ab;
    public static final enum com.teamspeak.ts3client.jni.d ac;
    public static final enum com.teamspeak.ts3client.jni.d ad;
    public static final enum com.teamspeak.ts3client.jni.d ae;
    public static final enum com.teamspeak.ts3client.jni.d af;
    public static final enum com.teamspeak.ts3client.jni.d ag;
    public static final enum com.teamspeak.ts3client.jni.d ah;
    public static final enum com.teamspeak.ts3client.jni.d ai;
    public static final enum com.teamspeak.ts3client.jni.d aj;
    private static final synthetic com.teamspeak.ts3client.jni.d[] al;
    public static final enum com.teamspeak.ts3client.jni.d b;
    public static final enum com.teamspeak.ts3client.jni.d c;
    public static final enum com.teamspeak.ts3client.jni.d d;
    public static final enum com.teamspeak.ts3client.jni.d e;
    public static final enum com.teamspeak.ts3client.jni.d f;
    public static final enum com.teamspeak.ts3client.jni.d g;
    public static final enum com.teamspeak.ts3client.jni.d h;
    public static final enum com.teamspeak.ts3client.jni.d i;
    public static final enum com.teamspeak.ts3client.jni.d j;
    public static final enum com.teamspeak.ts3client.jni.d k;
    public static final enum com.teamspeak.ts3client.jni.d l;
    public static final enum com.teamspeak.ts3client.jni.d m;
    public static final enum com.teamspeak.ts3client.jni.d n;
    public static final enum com.teamspeak.ts3client.jni.d o;
    public static final enum com.teamspeak.ts3client.jni.d p;
    public static final enum com.teamspeak.ts3client.jni.d q;
    public static final enum com.teamspeak.ts3client.jni.d r;
    public static final enum com.teamspeak.ts3client.jni.d s;
    public static final enum com.teamspeak.ts3client.jni.d t;
    public static final enum com.teamspeak.ts3client.jni.d u;
    public static final enum com.teamspeak.ts3client.jni.d v;
    public static final enum com.teamspeak.ts3client.jni.d w;
    public static final enum com.teamspeak.ts3client.jni.d x;
    public static final enum com.teamspeak.ts3client.jni.d y;
    public static final enum com.teamspeak.ts3client.jni.d z;
    int ak;

    static d()
    {
        com.teamspeak.ts3client.jni.d.a = new com.teamspeak.ts3client.jni.d("CLIENT_UNIQUE_IDENTIFIER", 0, 0);
        com.teamspeak.ts3client.jni.d.b = new com.teamspeak.ts3client.jni.d("CLIENT_NICKNAME", 1, 1);
        com.teamspeak.ts3client.jni.d.c = new com.teamspeak.ts3client.jni.d("CLIENT_VERSION", 2, 2);
        com.teamspeak.ts3client.jni.d.d = new com.teamspeak.ts3client.jni.d("CLIENT_PLATFORM", 3, 3);
        com.teamspeak.ts3client.jni.d.e = new com.teamspeak.ts3client.jni.d("CLIENT_FLAG_TALKING", 4, 4);
        com.teamspeak.ts3client.jni.d.f = new com.teamspeak.ts3client.jni.d("CLIENT_INPUT_MUTED", 5, 5);
        com.teamspeak.ts3client.jni.d.g = new com.teamspeak.ts3client.jni.d("CLIENT_OUTPUT_MUTED", 6, 6);
        com.teamspeak.ts3client.jni.d.h = new com.teamspeak.ts3client.jni.d("CLIENT_OUTPUTONLY_MUTED", 7, 7);
        com.teamspeak.ts3client.jni.d.i = new com.teamspeak.ts3client.jni.d("CLIENT_INPUT_HARDWARE", 8, 8);
        com.teamspeak.ts3client.jni.d.j = new com.teamspeak.ts3client.jni.d("CLIENT_OUTPUT_HARDWARE", 9, 9);
        com.teamspeak.ts3client.jni.d.k = new com.teamspeak.ts3client.jni.d("CLIENT_INPUT_DEACTIVATED", 10, 10);
        com.teamspeak.ts3client.jni.d.l = new com.teamspeak.ts3client.jni.d("CLIENT_IDLE_TIME", 11, 11);
        com.teamspeak.ts3client.jni.d.m = new com.teamspeak.ts3client.jni.d("CLIENT_DEFAULT_CHANNEL", 12, 12);
        com.teamspeak.ts3client.jni.d.n = new com.teamspeak.ts3client.jni.d("CLIENT_DEFAULT_CHANNEL_PASSWORD", 13, 13);
        com.teamspeak.ts3client.jni.d.o = new com.teamspeak.ts3client.jni.d("CLIENT_SERVER_PASSWORD", 14, 14);
        com.teamspeak.ts3client.jni.d.p = new com.teamspeak.ts3client.jni.d("CLIENT_META_DATA", 15, 15);
        com.teamspeak.ts3client.jni.d.q = new com.teamspeak.ts3client.jni.d("CLIENT_IS_MUTED", 16, 16);
        com.teamspeak.ts3client.jni.d.r = new com.teamspeak.ts3client.jni.d("CLIENT_IS_RECORDING", 17, 17);
        com.teamspeak.ts3client.jni.d.s = new com.teamspeak.ts3client.jni.d("CLIENT_VOLUME_MODIFICATOR", 18, 18);
        com.teamspeak.ts3client.jni.d.t = new com.teamspeak.ts3client.jni.d("CLIENT_VERSION_SIGN", 19, 19);
        com.teamspeak.ts3client.jni.d.u = new com.teamspeak.ts3client.jni.d("CLIENT_SECURITY_HASH", 20, 20);
        com.teamspeak.ts3client.jni.d.v = new com.teamspeak.ts3client.jni.d("CLIENT_DUMMY_3", 21, 21);
        com.teamspeak.ts3client.jni.d.w = new com.teamspeak.ts3client.jni.d("CLIENT_DUMMY_4", 22, 22);
        com.teamspeak.ts3client.jni.d.x = new com.teamspeak.ts3client.jni.d("CLIENT_DUMMY_5", 23, 23);
        com.teamspeak.ts3client.jni.d.y = new com.teamspeak.ts3client.jni.d("CLIENT_DUMMY_6", 24, 24);
        com.teamspeak.ts3client.jni.d.z = new com.teamspeak.ts3client.jni.d("CLIENT_DUMMY_7", 25, 25);
        com.teamspeak.ts3client.jni.d.A = new com.teamspeak.ts3client.jni.d("CLIENT_DUMMY_8", 26, 26);
        com.teamspeak.ts3client.jni.d.B = new com.teamspeak.ts3client.jni.d("CLIENT_DUMMY_9", 27, 27);
        com.teamspeak.ts3client.jni.d.C = new com.teamspeak.ts3client.jni.d("CLIENT_KEY_OFFSET", 28, 28);
        com.teamspeak.ts3client.jni.d.D = new com.teamspeak.ts3client.jni.d("CLIENT_LAST_VAR_REQUEST", 29, 29);
        com.teamspeak.ts3client.jni.d.E = new com.teamspeak.ts3client.jni.d("CLIENT_LOGIN_NAME", 30, 30);
        com.teamspeak.ts3client.jni.d.F = new com.teamspeak.ts3client.jni.d("CLIENT_LOGIN_PASSWORD", 31, 31);
        com.teamspeak.ts3client.jni.d.G = new com.teamspeak.ts3client.jni.d("CLIENT_DATABASE_ID", 32, 32);
        com.teamspeak.ts3client.jni.d.H = new com.teamspeak.ts3client.jni.d("CLIENT_CHANNEL_GROUP_ID", 33, 33);
        com.teamspeak.ts3client.jni.d.I = new com.teamspeak.ts3client.jni.d("CLIENT_SERVERGROUPS", 34, 34);
        com.teamspeak.ts3client.jni.d.J = new com.teamspeak.ts3client.jni.d("CLIENT_CREATED", 35, 35);
        com.teamspeak.ts3client.jni.d.K = new com.teamspeak.ts3client.jni.d("CLIENT_LASTCONNECTED", 36, 36);
        com.teamspeak.ts3client.jni.d.L = new com.teamspeak.ts3client.jni.d("CLIENT_TOTALCONNECTIONS", 37, 37);
        com.teamspeak.ts3client.jni.d.M = new com.teamspeak.ts3client.jni.d("CLIENT_AWAY", 38, 38);
        com.teamspeak.ts3client.jni.d.N = new com.teamspeak.ts3client.jni.d("CLIENT_AWAY_MESSAGE", 39, 39);
        com.teamspeak.ts3client.jni.d.O = new com.teamspeak.ts3client.jni.d("CLIENT_TYPE", 40, 40);
        com.teamspeak.ts3client.jni.d.P = new com.teamspeak.ts3client.jni.d("CLIENT_FLAG_AVATAR", 41, 41);
        com.teamspeak.ts3client.jni.d.Q = new com.teamspeak.ts3client.jni.d("CLIENT_TALK_POWER", 42, 42);
        com.teamspeak.ts3client.jni.d.R = new com.teamspeak.ts3client.jni.d("CLIENT_TALK_REQUEST", 43, 43);
        com.teamspeak.ts3client.jni.d.S = new com.teamspeak.ts3client.jni.d("CLIENT_TALK_REQUEST_MSG", 44, 44);
        com.teamspeak.ts3client.jni.d.T = new com.teamspeak.ts3client.jni.d("CLIENT_DESCRIPTION", 45, 45);
        com.teamspeak.ts3client.jni.d.U = new com.teamspeak.ts3client.jni.d("CLIENT_IS_TALKER", 46, 46);
        com.teamspeak.ts3client.jni.d.V = new com.teamspeak.ts3client.jni.d("CLIENT_MONTH_BYTES_UPLOADED", 47, 47);
        com.teamspeak.ts3client.jni.d.W = new com.teamspeak.ts3client.jni.d("CLIENT_MONTH_BYTES_DOWNLOADED", 48, 48);
        com.teamspeak.ts3client.jni.d.X = new com.teamspeak.ts3client.jni.d("CLIENT_TOTAL_BYTES_UPLOADED", 49, 49);
        com.teamspeak.ts3client.jni.d.Y = new com.teamspeak.ts3client.jni.d("CLIENT_TOTAL_BYTES_DOWNLOADED", 50, 50);
        com.teamspeak.ts3client.jni.d.Z = new com.teamspeak.ts3client.jni.d("CLIENT_IS_PRIORITY_SPEAKER", 51, 51);
        com.teamspeak.ts3client.jni.d.aa = new com.teamspeak.ts3client.jni.d("CLIENT_UNREAD_MESSAGES", 52, 52);
        com.teamspeak.ts3client.jni.d.ab = new com.teamspeak.ts3client.jni.d("CLIENT_NICKNAME_PHONETIC", 53, 53);
        com.teamspeak.ts3client.jni.d.ac = new com.teamspeak.ts3client.jni.d("CLIENT_NEEDED_SERVERQUERY_VIEW_POWER", 54, 54);
        com.teamspeak.ts3client.jni.d.ad = new com.teamspeak.ts3client.jni.d("CLIENT_DEFAULT_TOKEN", 55, 55);
        com.teamspeak.ts3client.jni.d.ae = new com.teamspeak.ts3client.jni.d("CLIENT_ICON_ID", 56, 56);
        com.teamspeak.ts3client.jni.d.af = new com.teamspeak.ts3client.jni.d("CLIENT_IS_CHANNEL_COMMANDER", 57, 57);
        com.teamspeak.ts3client.jni.d.ag = new com.teamspeak.ts3client.jni.d("CLIENT_COUNTRY", 58, 58);
        com.teamspeak.ts3client.jni.d.ah = new com.teamspeak.ts3client.jni.d("CLIENT_CHANNEL_GROUP_INHERITED_CHANNEL_ID", 59, 59);
        com.teamspeak.ts3client.jni.d.ai = new com.teamspeak.ts3client.jni.d("CLIENT_BADGES", 60, 60);
        com.teamspeak.ts3client.jni.d.aj = new com.teamspeak.ts3client.jni.d("CLIENT_ENDMARKER_RARE", 61, 61);
        com.teamspeak.ts3client.jni.d[] v0_125 = new com.teamspeak.ts3client.jni.d[62];
        v0_125[0] = com.teamspeak.ts3client.jni.d.a;
        v0_125[1] = com.teamspeak.ts3client.jni.d.b;
        v0_125[2] = com.teamspeak.ts3client.jni.d.c;
        v0_125[3] = com.teamspeak.ts3client.jni.d.d;
        v0_125[4] = com.teamspeak.ts3client.jni.d.e;
        v0_125[5] = com.teamspeak.ts3client.jni.d.f;
        v0_125[6] = com.teamspeak.ts3client.jni.d.g;
        v0_125[7] = com.teamspeak.ts3client.jni.d.h;
        v0_125[8] = com.teamspeak.ts3client.jni.d.i;
        v0_125[9] = com.teamspeak.ts3client.jni.d.j;
        v0_125[10] = com.teamspeak.ts3client.jni.d.k;
        v0_125[11] = com.teamspeak.ts3client.jni.d.l;
        v0_125[12] = com.teamspeak.ts3client.jni.d.m;
        v0_125[13] = com.teamspeak.ts3client.jni.d.n;
        v0_125[14] = com.teamspeak.ts3client.jni.d.o;
        v0_125[15] = com.teamspeak.ts3client.jni.d.p;
        v0_125[16] = com.teamspeak.ts3client.jni.d.q;
        v0_125[17] = com.teamspeak.ts3client.jni.d.r;
        v0_125[18] = com.teamspeak.ts3client.jni.d.s;
        v0_125[19] = com.teamspeak.ts3client.jni.d.t;
        v0_125[20] = com.teamspeak.ts3client.jni.d.u;
        v0_125[21] = com.teamspeak.ts3client.jni.d.v;
        v0_125[22] = com.teamspeak.ts3client.jni.d.w;
        v0_125[23] = com.teamspeak.ts3client.jni.d.x;
        v0_125[24] = com.teamspeak.ts3client.jni.d.y;
        v0_125[25] = com.teamspeak.ts3client.jni.d.z;
        v0_125[26] = com.teamspeak.ts3client.jni.d.A;
        v0_125[27] = com.teamspeak.ts3client.jni.d.B;
        v0_125[28] = com.teamspeak.ts3client.jni.d.C;
        v0_125[29] = com.teamspeak.ts3client.jni.d.D;
        v0_125[30] = com.teamspeak.ts3client.jni.d.E;
        v0_125[31] = com.teamspeak.ts3client.jni.d.F;
        v0_125[32] = com.teamspeak.ts3client.jni.d.G;
        v0_125[33] = com.teamspeak.ts3client.jni.d.H;
        v0_125[34] = com.teamspeak.ts3client.jni.d.I;
        v0_125[35] = com.teamspeak.ts3client.jni.d.J;
        v0_125[36] = com.teamspeak.ts3client.jni.d.K;
        v0_125[37] = com.teamspeak.ts3client.jni.d.L;
        v0_125[38] = com.teamspeak.ts3client.jni.d.M;
        v0_125[39] = com.teamspeak.ts3client.jni.d.N;
        v0_125[40] = com.teamspeak.ts3client.jni.d.O;
        v0_125[41] = com.teamspeak.ts3client.jni.d.P;
        v0_125[42] = com.teamspeak.ts3client.jni.d.Q;
        v0_125[43] = com.teamspeak.ts3client.jni.d.R;
        v0_125[44] = com.teamspeak.ts3client.jni.d.S;
        v0_125[45] = com.teamspeak.ts3client.jni.d.T;
        v0_125[46] = com.teamspeak.ts3client.jni.d.U;
        v0_125[47] = com.teamspeak.ts3client.jni.d.V;
        v0_125[48] = com.teamspeak.ts3client.jni.d.W;
        v0_125[49] = com.teamspeak.ts3client.jni.d.X;
        v0_125[50] = com.teamspeak.ts3client.jni.d.Y;
        v0_125[51] = com.teamspeak.ts3client.jni.d.Z;
        v0_125[52] = com.teamspeak.ts3client.jni.d.aa;
        v0_125[53] = com.teamspeak.ts3client.jni.d.ab;
        v0_125[54] = com.teamspeak.ts3client.jni.d.ac;
        v0_125[55] = com.teamspeak.ts3client.jni.d.ad;
        v0_125[56] = com.teamspeak.ts3client.jni.d.ae;
        v0_125[57] = com.teamspeak.ts3client.jni.d.af;
        v0_125[58] = com.teamspeak.ts3client.jni.d.ag;
        v0_125[59] = com.teamspeak.ts3client.jni.d.ah;
        v0_125[60] = com.teamspeak.ts3client.jni.d.ai;
        v0_125[61] = com.teamspeak.ts3client.jni.d.aj;
        com.teamspeak.ts3client.jni.d.al = v0_125;
        return;
    }

    private d(String p1, int p2, int p3)
    {
        this(p1, p2);
        this.ak = p3;
        return;
    }

    private int a()
    {
        return this.ak;
    }

    private static int a(int p0)
    {
        return p0;
    }

    public static com.teamspeak.ts3client.jni.d valueOf(String p1)
    {
        return ((com.teamspeak.ts3client.jni.d) Enum.valueOf(com.teamspeak.ts3client.jni.d, p1));
    }

    public static com.teamspeak.ts3client.jni.d[] values()
    {
        return ((com.teamspeak.ts3client.jni.d[]) com.teamspeak.ts3client.jni.d.al.clone());
    }
}
