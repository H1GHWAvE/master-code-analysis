package com.teamspeak.ts3client;
public final class br {
    public static final int abc_action_bar_home_description = 2131034112;
    public static final int abc_action_bar_home_description_format = 2131034113;
    public static final int abc_action_bar_home_subtitle_description_format = 2131034114;
    public static final int abc_action_bar_up_description = 2131034115;
    public static final int abc_action_menu_overflow_description = 2131034116;
    public static final int abc_action_mode_done = 2131034117;
    public static final int abc_activity_chooser_view_see_all = 2131034118;
    public static final int abc_activitychooserview_choose_application = 2131034119;
    public static final int abc_search_hint = 2131034120;
    public static final int abc_searchview_description_clear = 2131034121;
    public static final int abc_searchview_description_query = 2131034122;
    public static final int abc_searchview_description_search = 2131034123;
    public static final int abc_searchview_description_submit = 2131034124;
    public static final int abc_searchview_description_voice = 2131034125;
    public static final int abc_shareactionprovider_share_with = 2131034126;
    public static final int abc_shareactionprovider_share_with_application = 2131034127;
    public static final int abc_toolbar_collapse_description = 2131034128;
    public static final int app_name = 2131034130;
    public static final int appbar_scrolling_view_behavior = 2131034131;
    public static final int bookmark_entry_address = 2131034132;
    public static final int bookmark_entry_defaultchannel = 2131034133;
    public static final int bookmark_entry_defaultchannelpassword = 2131034134;
    public static final int bookmark_entry_ident = 2131034135;
    public static final int bookmark_entry_label = 2131034136;
    public static final int bookmark_entry_nickname = 2131034137;
    public static final int bookmark_entry_port = 2131034138;
    public static final int bookmark_entry_save = 2131034139;
    public static final int bookmark_entry_serverpass = 2131034140;
    public static final int button_disconnect = 2131034141;
    public static final int button_exit = 2131034142;
    public static final int clientaction_ban = 2131034143;
    public static final int clientaction_complain = 2131034144;
    public static final int clientaction_contact = 2131034145;
    public static final int clientaction_kickfromchannel = 2131034146;
    public static final int clientaction_kickfromserver = 2131034147;
    public static final int clientaction_mute = 2131034148;
    public static final int clientaction_poke = 2131034149;
    public static final int clientaction_priorityspeaker = 2131034150;
    public static final int connection_connectionerror = 2131034151;
    public static final int connection_error = 2131034152;
    public static final int crash_dialog_comment_prompt = 2131034153;
    public static final int crash_dialog_ok_toast = 2131034154;
    public static final int crash_dialog_text = 2131034155;
    public static final int crash_dialog_title = 2131034156;
    public static final int crash_toast_text = 2131034157;
    public static final int new_bookmark = 2131034158;
    public static final int settings_showqueryclient_text = 2131034159;
    public static final int settings_showqueryclient_text_info = 2131034160;
    public static final int settings_soundpack_text = 2131034161;
    public static final int settings_soundpack_text_info = 2131034162;
    public static final int show_changelog = 2131034163;
    public static final int status_bar_notification_info_overflow = 2131034129;

    public br()
    {
        return;
    }
}
