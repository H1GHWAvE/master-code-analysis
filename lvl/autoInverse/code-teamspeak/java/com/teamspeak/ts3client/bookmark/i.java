package com.teamspeak.ts3client.bookmark;
final class i implements android.widget.AdapterView$OnItemClickListener {
    final synthetic com.teamspeak.ts3client.bookmark.e a;

    i(com.teamspeak.ts3client.bookmark.e p1)
    {
        this.a = p1;
        return;
    }

    public final void onItemClick(android.widget.AdapterView p7, android.view.View p8, int p9, long p10)
    {
        if (!this.a.a) {
            com.teamspeak.ts3client.data.ab v0_6;
            com.teamspeak.ts3client.data.ab v0_3 = this.a.i();
            if (android.os.Build$VERSION.SDK_INT < 17) {
                if (android.provider.Settings$System.getInt(v0_3.getContentResolver(), "airplane_mode_on", 0) == 0) {
                    v0_6 = 0;
                } else {
                    v0_6 = 1;
                }
            } else {
                if (android.provider.Settings$System.getInt(v0_3.getContentResolver(), "airplane_mode_on", 0) == 0) {
                    v0_6 = 0;
                } else {
                    v0_6 = 1;
                }
            }
            if ((v0_6 == null) || (com.teamspeak.ts3client.data.d.u.a(this.a.i()))) {
                if (com.teamspeak.ts3client.data.d.u.a(this.a.i())) {
                    this.a.a = 1;
                    this.a.a(((com.teamspeak.ts3client.data.ab) com.teamspeak.ts3client.bookmark.e.a(this.a).getItem(p9)));
                } else {
                    com.teamspeak.ts3client.data.ab v0_22 = new android.app.AlertDialog$Builder(this.a.i()).create();
                    v0_22.setTitle(com.teamspeak.ts3client.data.e.a.a("network.noconnection"));
                    v0_22.setMessage(com.teamspeak.ts3client.data.e.a.a("network.noconnection.text"));
                    v0_22.setButton(-2, com.teamspeak.ts3client.data.e.a.a("button.cancel"), new com.teamspeak.ts3client.bookmark.k(this, v0_22));
                    v0_22.setCancelable(0);
                    v0_22.show();
                }
            } else {
                com.teamspeak.ts3client.data.ab v0_25 = new android.app.AlertDialog$Builder(this.a.i()).create();
                v0_25.setTitle(com.teamspeak.ts3client.data.e.a.a("network.airplane"));
                v0_25.setMessage(com.teamspeak.ts3client.data.e.a.a("network.airplane.text"));
                v0_25.setButton(-2, com.teamspeak.ts3client.data.e.a.a("button.cancel"), new com.teamspeak.ts3client.bookmark.j(this, v0_25));
                v0_25.setCancelable(0);
                v0_25.show();
            }
        }
        return;
    }
}
