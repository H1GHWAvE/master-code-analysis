package com.teamspeak.ts3client.bookmark;
public final class a extends android.support.v4.app.ax {
    private com.teamspeak.ts3client.data.ab aA;
    private com.teamspeak.ts3client.Ts3Application aB;
    private android.widget.Spinner aC;
    private com.teamspeak.ts3client.data.b.a at;
    private com.teamspeak.ts3client.bookmark.CustomEditText au;
    private com.teamspeak.ts3client.bookmark.CustomEditText av;
    private com.teamspeak.ts3client.bookmark.CustomEditText aw;
    private com.teamspeak.ts3client.bookmark.CustomEditText ax;
    private com.teamspeak.ts3client.bookmark.CustomEditText ay;
    private com.teamspeak.ts3client.bookmark.CustomEditText az;

    public a(com.teamspeak.ts3client.data.b.a p2)
    {
        this.aA = 0;
        this.at = p2;
        return;
    }

    public a(com.teamspeak.ts3client.data.b.a p2, com.teamspeak.ts3client.data.ab p3)
    {
        this.aA = 0;
        this.at = p2;
        this.aA = p3;
        return;
    }

    static synthetic boolean a(com.teamspeak.ts3client.bookmark.a p4)
    {
        int v0 = 0;
        if (!p4.au.getText().toString().equals("")) {
            p4.au.setInputerror(0);
            if (!p4.av.getText().toString().equals("")) {
                p4.av.setInputerror(0);
                if ((!p4.ay.getText().toString().equals("")) && (p4.ay.getText().length() >= 3)) {
                    p4.ay.setInputerror(0);
                    v0 = 1;
                } else {
                    p4.ay.setInputerror(1);
                }
            } else {
                p4.av.setInputerror(1);
            }
        } else {
            p4.au.setInputerror(1);
        }
        return v0;
    }

    static synthetic com.teamspeak.ts3client.data.ab b(com.teamspeak.ts3client.bookmark.a p1)
    {
        return p1.aA;
    }

    static synthetic com.teamspeak.ts3client.bookmark.CustomEditText c(com.teamspeak.ts3client.bookmark.a p1)
    {
        return p1.au;
    }

    static synthetic com.teamspeak.ts3client.bookmark.CustomEditText d(com.teamspeak.ts3client.bookmark.a p1)
    {
        return p1.av;
    }

    static synthetic com.teamspeak.ts3client.bookmark.CustomEditText e(com.teamspeak.ts3client.bookmark.a p1)
    {
        return p1.ay;
    }

    static synthetic com.teamspeak.ts3client.bookmark.CustomEditText f(com.teamspeak.ts3client.bookmark.a p1)
    {
        return p1.aw;
    }

    static synthetic com.teamspeak.ts3client.bookmark.CustomEditText g(com.teamspeak.ts3client.bookmark.a p1)
    {
        return p1.ax;
    }

    static synthetic com.teamspeak.ts3client.bookmark.CustomEditText h(com.teamspeak.ts3client.bookmark.a p1)
    {
        return p1.az;
    }

    static synthetic android.widget.Spinner i(com.teamspeak.ts3client.bookmark.a p1)
    {
        return p1.aC;
    }

    static synthetic com.teamspeak.ts3client.data.b.a j(com.teamspeak.ts3client.bookmark.a p1)
    {
        return p1.at;
    }

    static synthetic com.teamspeak.ts3client.Ts3Application k(com.teamspeak.ts3client.bookmark.a p1)
    {
        return p1.aB;
    }

    private boolean y()
    {
        int v0 = 0;
        if (!this.au.getText().toString().equals("")) {
            this.au.setInputerror(0);
            if (!this.av.getText().toString().equals("")) {
                this.av.setInputerror(0);
                if ((!this.ay.getText().toString().equals("")) && (this.ay.getText().length() >= 3)) {
                    this.ay.setInputerror(0);
                    v0 = 1;
                } else {
                    this.ay.setInputerror(1);
                }
            } else {
                this.av.setInputerror(1);
            }
        } else {
            this.au.setInputerror(1);
        }
        return v0;
    }

    public final android.view.View a(android.view.LayoutInflater p9, android.view.ViewGroup p10)
    {
        android.widget.ScrollView v0_2 = ((android.widget.ScrollView) p9.inflate(2130903069, p10, 0));
        android.app.Dialog v1_2 = ((android.widget.Button) v0_2.findViewById(2131493019));
        this.aB = ((com.teamspeak.ts3client.Ts3Application) this.i().getApplicationContext());
        com.teamspeak.ts3client.data.e.a.a("bookmark.entry.label", v0_2, 2131493005);
        com.teamspeak.ts3client.data.e.a.a("bookmark.entry.serveraddress", v0_2, 2131493007);
        com.teamspeak.ts3client.data.e.a.a("bookmark.entry.serverpassword", v0_2, 2131493009);
        com.teamspeak.ts3client.data.e.a.a("bookmark.entry.nickname", v0_2, 2131493011);
        com.teamspeak.ts3client.data.e.a.a("bookmark.entry.defaultchannel", v0_2, 2131493013);
        com.teamspeak.ts3client.data.e.a.a("bookmark.entry.defaultchannelpassword", v0_2, 2131493015);
        com.teamspeak.ts3client.data.e.a.a("bookmark.entry.ident", v0_2, 2131493017);
        v1_2.setText(com.teamspeak.ts3client.data.e.a.a("button.save"));
        this.au = ((com.teamspeak.ts3client.bookmark.CustomEditText) v0_2.findViewById(2131493006));
        this.av = ((com.teamspeak.ts3client.bookmark.CustomEditText) v0_2.findViewById(2131493008));
        this.aw = ((com.teamspeak.ts3client.bookmark.CustomEditText) v0_2.findViewById(2131493010));
        this.ay = ((com.teamspeak.ts3client.bookmark.CustomEditText) v0_2.findViewById(2131493012));
        this.ax = ((com.teamspeak.ts3client.bookmark.CustomEditText) v0_2.findViewById(2131493014));
        this.az = ((com.teamspeak.ts3client.bookmark.CustomEditText) v0_2.findViewById(2131493016));
        this.aC = ((android.widget.Spinner) v0_2.findViewById(2131493018));
        String v2_35 = ((android.view.inputmethod.InputMethodManager) this.aB.getSystemService("input_method"));
        v2_35.toggleSoftInputFromWindow(this.au.getWindowToken(), 2, 0);
        java.util.ArrayList v5_1 = com.teamspeak.ts3client.data.b.f.a().c();
        if (v5_1 == null) {
            v1_2.setEnabled(0);
        } else {
            this.aC.setAdapter(new android.widget.ArrayAdapter(this.i(), 17367048, v5_1));
        }
        if (this.aA == null) {
            if (v5_1 != null) {
                int v4_14 = 0;
                while (v4_14 < v5_1.size()) {
                    if (!((com.teamspeak.ts3client.e.a) v5_1.get(v4_14)).e) {
                        v4_14++;
                    } else {
                        this.aC.setSelection(v4_14);
                        break;
                    }
                }
            }
        } else {
            this.au.setText(this.aA.a);
            this.av.setText(this.aA.c);
            this.aw.setText(this.aA.e);
            this.ay.setText(this.aA.f);
            this.ax.setText(this.aA.g);
            this.az.setText(this.aA.h);
            if (v5_1 != null) {
                int v4_21 = 0;
                while (v4_21 < v5_1.size()) {
                    if (((com.teamspeak.ts3client.e.a) v5_1.get(v4_21)).a != this.aA.d) {
                        v4_21++;
                    } else {
                        this.aC.setSelection(v4_21);
                        break;
                    }
                }
            }
        }
        v1_2.setOnClickListener(new com.teamspeak.ts3client.bookmark.b(this, v2_35));
        this.j.setTitle(com.teamspeak.ts3client.data.e.a.a("bookmark.entry.title"));
        return v0_2;
    }

    public final void a(android.os.Bundle p1)
    {
        super.a(p1);
        return;
    }

    public final void a(android.support.v4.app.bi p2, String p3)
    {
        android.support.v4.app.cd v0 = p2.a();
        v0.a(this, p3);
        v0.j();
        return;
    }
}
