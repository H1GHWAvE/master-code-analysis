package com.teamspeak.ts3client.f;
public final class v extends android.widget.RelativeLayout implements android.view.View$OnClickListener {
    private String a;
    private com.teamspeak.ts3client.f.x b;
    private android.support.v4.app.bi c;
    private java.util.TreeMap d;
    private String e;

    public v(android.content.Context p8, String p9, String p10, String p11, java.util.TreeMap p12, android.support.v4.app.bi p13)
    {
        this(p8);
        this.setLayoutParams(new android.widget.RelativeLayout$LayoutParams(-1, -2));
        this.setPadding(0, 18, 0, 18);
        this.setBackgroundResource(2130837686);
        this.c = p13;
        this.a = p9;
        this.d = p12;
        this.e = p11;
        int v0_4 = new android.widget.TextView(p8);
        android.widget.TextView v1_2 = new android.widget.TextView(p8);
        v0_4.setText(p9);
        v0_4.setTextSize(1101004800);
        v0_4.setTypeface(0, 1);
        v0_4.setId(1);
        v1_2.setText(p10);
        v1_2.setTextSize(1092616192);
        v1_2.setTypeface(0, 2);
        v1_2.setId(2);
        android.widget.RelativeLayout$LayoutParams v2_6 = new android.widget.RelativeLayout$LayoutParams(-2, -2);
        v2_6.addRule(10);
        this.addView(v0_4, v2_6);
        android.widget.RelativeLayout$LayoutParams v2_8 = new android.widget.RelativeLayout$LayoutParams(-2, -2);
        v2_8.addRule(3, v0_4.getId());
        this.addView(v1_2, v2_8);
        this.setClickable(1);
        this.setOnClickListener(this);
        return;
    }

    static synthetic String a(com.teamspeak.ts3client.f.v p1)
    {
        return p1.e;
    }

    static synthetic java.util.TreeMap b(com.teamspeak.ts3client.f.v p1)
    {
        return p1.d;
    }

    static synthetic String c(com.teamspeak.ts3client.f.v p1)
    {
        return p1.a;
    }

    public final void onClick(android.view.View p4)
    {
        this.b = new com.teamspeak.ts3client.f.x(this, 0);
        this.b.a(this.c, this.a);
        p4.performHapticFeedback(1);
        return;
    }
}
