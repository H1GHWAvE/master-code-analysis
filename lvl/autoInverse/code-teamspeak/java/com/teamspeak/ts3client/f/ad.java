package com.teamspeak.ts3client.f;
final class ad extends android.support.v4.app.ax {
    final synthetic com.teamspeak.ts3client.f.ab at;

    private ad(com.teamspeak.ts3client.f.ab p1)
    {
        this.at = p1;
        return;
    }

    synthetic ad(com.teamspeak.ts3client.f.ab p1, byte p2)
    {
        this(p1);
        return;
    }

    public final android.view.View a(android.view.LayoutInflater p12, android.view.ViewGroup p13)
    {
        android.app.Dialog v0_2 = ((com.teamspeak.ts3client.Ts3Application) p12.getContext().getApplicationContext());
        String v1_1 = v0_2.e.getInt(com.teamspeak.ts3client.f.ab.a(this.at), com.teamspeak.ts3client.f.ab.b(this.at));
        android.widget.RelativeLayout v2_3 = new android.widget.RelativeLayout(p12.getContext());
        v2_3.setLayoutParams(new android.widget.RelativeLayout$LayoutParams(-1, -2));
        com.teamspeak.ts3client.f.ab.a(this.at, new android.widget.TextView(p12.getContext()));
        com.teamspeak.ts3client.f.ab.a(this.at, new android.widget.SeekBar(p12.getContext()));
        com.teamspeak.ts3client.f.ab.c(this.at).setId(3);
        com.teamspeak.ts3client.f.ab.c(this.at).setMax(com.teamspeak.ts3client.f.ab.d(this.at));
        com.teamspeak.ts3client.f.ab.c(this.at).setProgress((com.teamspeak.ts3client.f.ab.e(this.at) + v1_1));
        com.teamspeak.ts3client.f.ab.c(this.at).setOnSeekBarChangeListener(new com.teamspeak.ts3client.f.ae(this));
        com.teamspeak.ts3client.f.ab.f(this.at).setTextSize(1090519040);
        com.teamspeak.ts3client.f.ab.f(this.at).setTypeface(0, 2);
        com.teamspeak.ts3client.f.ab.f(this.at).setText(new StringBuilder().append(v1_1).append("/").append((com.teamspeak.ts3client.f.ab.d(this.at) - com.teamspeak.ts3client.f.ab.e(this.at))).toString());
        com.teamspeak.ts3client.f.ab.f(this.at).setPadding(0, 0, 10, 0);
        com.teamspeak.ts3client.f.ab.f(this.at).setId(4);
        String v1_11 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        v1_11.addRule(10);
        v2_3.addView(com.teamspeak.ts3client.f.ab.c(this.at), v1_11);
        String v1_13 = new android.widget.RelativeLayout$LayoutParams(-2, -2);
        v1_13.addRule(3, com.teamspeak.ts3client.f.ab.c(this.at).getId());
        v1_13.addRule(11);
        v2_3.addView(com.teamspeak.ts3client.f.ab.f(this.at), v1_13);
        String v1_15 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        v1_15.addRule(3, com.teamspeak.ts3client.f.ab.f(this.at).getId());
        android.widget.Button v3_34 = new android.widget.Button(p12.getContext());
        v3_34.setText(com.teamspeak.ts3client.data.e.a.a("button.save"));
        v3_34.setOnClickListener(new com.teamspeak.ts3client.f.af(this, v0_2, v2_3));
        v2_3.addView(v3_34, v1_15);
        this.j.setTitle(com.teamspeak.ts3client.f.ab.h(this.at));
        return v2_3;
    }
}
