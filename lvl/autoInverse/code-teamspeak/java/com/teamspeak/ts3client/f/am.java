package com.teamspeak.ts3client.f;
final class am extends android.support.v4.app.ax implements com.teamspeak.ts3client.data.w {
    public boolean at;
    Thread au;
    final synthetic com.teamspeak.ts3client.f.ak av;
    private android.graphics.drawable.Drawable aw;

    private am(com.teamspeak.ts3client.f.ak p3)
    {
        this.av = p3;
        this.at = 1;
        this.au = new Thread(new com.teamspeak.ts3client.f.an(this));
        return;
    }

    synthetic am(com.teamspeak.ts3client.f.ak p1, byte p2)
    {
        this(p1);
        return;
    }

    static synthetic android.graphics.drawable.Drawable a(com.teamspeak.ts3client.f.am p1)
    {
        return p1.aw;
    }

    private void y()
    {
        com.teamspeak.ts3client.f.ak.a(this.av, com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_spawnNewServerConnectionHandler());
        com.teamspeak.ts3client.a.k v0_3 = com.teamspeak.ts3client.Ts3Application.a().e.getInt("playbackStream", 0);
        com.teamspeak.ts3client.f.ak v1_4 = com.teamspeak.ts3client.Ts3Application.a().e.getInt("recordStream", 0);
        long v2_4 = com.teamspeak.ts3client.Ts3Application.a().e.getInt("samplePlay", com.teamspeak.ts3client.Ts3Application.a().i());
        int v3_3 = com.teamspeak.ts3client.Ts3Application.a().e.getInt("sampleRec", com.teamspeak.ts3client.Ts3Application.a().h());
        com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_prepareAudioDevice(v3_3, v2_4);
        com.teamspeak.ts3client.a.k.a().d = com.teamspeak.ts3client.jni.Ts3Jni.b();
        com.teamspeak.ts3client.a.k.a().a(v3_3, v2_4, v0_3, v1_4);
        com.teamspeak.ts3client.a.k.a().a(Boolean.valueOf(1));
        if (!com.teamspeak.ts3client.f.ak.a(this.av)) {
            com.teamspeak.ts3client.a.j.a().b(1);
        } else {
            com.teamspeak.ts3client.a.j.a().a(1);
        }
        com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_activateCaptureDevice(com.teamspeak.ts3client.f.ak.b(this.av));
        com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_openCaptureDevice(com.teamspeak.ts3client.f.ak.b(this.av), "", "");
        com.teamspeak.ts3client.jni.Ts3Jni.b().a(com.teamspeak.ts3client.f.ak.b(this.av), com.teamspeak.ts3client.jni.d.k, 0);
        com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_setPreProcessorConfigValue(com.teamspeak.ts3client.f.ak.b(this.av), "vad", "true");
        com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_setPreProcessorConfigValue(com.teamspeak.ts3client.f.ak.b(this.av), "voiceactivation_level", new StringBuilder().append(com.teamspeak.ts3client.Ts3Application.a().e.getInt(com.teamspeak.ts3client.f.ak.d(this.av), 0)).toString());
        com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_setLocalTestMode(com.teamspeak.ts3client.f.ak.b(this.av), 1);
        com.teamspeak.ts3client.jni.l.a().a(this);
        com.teamspeak.ts3client.a.k.a().d();
        return;
    }

    private void z()
    {
        if (com.teamspeak.ts3client.Ts3Application.a().a == null) {
            com.teamspeak.ts3client.a.k.a().f();
            com.teamspeak.ts3client.a.k.a().b();
            com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_closeCaptureDevice(com.teamspeak.ts3client.f.ak.b(this.av));
            com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_destroyServerConnectionHandler(com.teamspeak.ts3client.f.ak.b(this.av));
        } else {
            com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_closeCaptureDevice(com.teamspeak.ts3client.f.ak.b(this.av));
            com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_setLocalTestMode(com.teamspeak.ts3client.f.ak.b(this.av), 0);
            com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_destroyServerConnectionHandler(com.teamspeak.ts3client.f.ak.b(this.av));
            com.teamspeak.ts3client.Ts3Application.a().a.r();
        }
        return;
    }

    public final android.view.View a(android.view.LayoutInflater p12, android.view.ViewGroup p13)
    {
        Thread v0_2 = ((com.teamspeak.ts3client.Ts3Application) p12.getContext().getApplicationContext());
        if (v0_2.a != null) {
            v0_2.a.a.ts3client_closeCaptureDevice(v0_2.a.e);
            v0_2.a.a.ts3client_closePlaybackDevice(v0_2.a.e);
        }
        com.teamspeak.ts3client.f.ak v1_6 = v0_2.e.getInt(com.teamspeak.ts3client.f.ak.d(this.av), 0);
        android.widget.RelativeLayout v2_7 = new android.widget.RelativeLayout(p12.getContext());
        v2_7.setLayoutParams(new android.widget.RelativeLayout$LayoutParams(-1, -2));
        com.teamspeak.ts3client.f.ak.a(this.av, new android.widget.SeekBar(p12.getContext()));
        com.teamspeak.ts3client.f.ak.c(this.av).setId(3);
        com.teamspeak.ts3client.f.ak.c(this.av).setMax(100);
        com.teamspeak.ts3client.f.ak.c(this.av).setProgress((v1_6 + 50));
        com.teamspeak.ts3client.f.ak.c(this.av).setOnSeekBarChangeListener(new com.teamspeak.ts3client.f.ao(this));
        com.teamspeak.ts3client.f.ak.c(this.av).setProgressDrawable(this.j().getDrawable(2130837676));
        if (android.os.Build$VERSION.SDK_INT >= 16) {
            this.aw = com.teamspeak.ts3client.f.ak.c(this.av).getThumb();
        }
        com.teamspeak.ts3client.f.ak.a(this.av, new android.widget.TextView(p12.getContext()));
        com.teamspeak.ts3client.f.ak.e(this.av).setTextSize(1090519040);
        com.teamspeak.ts3client.f.ak.e(this.av).setTypeface(0, 2);
        com.teamspeak.ts3client.f.ak.e(this.av).setText(new StringBuilder().append((com.teamspeak.ts3client.f.ak.c(this.av).getProgress() - 50)).append(" / -50").toString());
        com.teamspeak.ts3client.f.ak.e(this.av).setPadding(0, 0, 10, 0);
        com.teamspeak.ts3client.f.ak.e(this.av).setId(4);
        com.teamspeak.ts3client.f.ak v1_28 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        v1_28.addRule(10);
        v2_7.addView(com.teamspeak.ts3client.f.ak.c(this.av), v1_28);
        com.teamspeak.ts3client.f.ak v1_30 = new android.widget.RelativeLayout$LayoutParams(-2, -2);
        v1_30.addRule(3, com.teamspeak.ts3client.f.ak.c(this.av).getId());
        v1_30.addRule(11);
        v2_7.addView(com.teamspeak.ts3client.f.ak.e(this.av), v1_30);
        com.teamspeak.ts3client.f.ak v1_32 = new android.widget.RelativeLayout$LayoutParams(-1, -2);
        v1_32.addRule(3, com.teamspeak.ts3client.f.ak.e(this.av).getId());
        String v3_39 = new android.widget.Button(p12.getContext());
        v3_39.setText(com.teamspeak.ts3client.data.e.a.a("button.save"));
        v3_39.setOnClickListener(new com.teamspeak.ts3client.f.ap(this, v0_2, v2_7));
        v2_7.addView(v3_39, v1_32);
        this.j.setTitle(com.teamspeak.ts3client.f.ak.g(this.av));
        com.teamspeak.ts3client.f.ak.a(this.av, com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_spawnNewServerConnectionHandler());
        Thread v0_7 = com.teamspeak.ts3client.Ts3Application.a().e.getInt("playbackStream", 0);
        com.teamspeak.ts3client.f.ak v1_39 = com.teamspeak.ts3client.Ts3Application.a().e.getInt("recordStream", 0);
        String v3_43 = com.teamspeak.ts3client.Ts3Application.a().e.getInt("samplePlay", com.teamspeak.ts3client.Ts3Application.a().i());
        long v4_20 = com.teamspeak.ts3client.Ts3Application.a().e.getInt("sampleRec", com.teamspeak.ts3client.Ts3Application.a().h());
        com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_prepareAudioDevice(v4_20, v3_43);
        com.teamspeak.ts3client.a.k.a().d = com.teamspeak.ts3client.jni.Ts3Jni.b();
        com.teamspeak.ts3client.a.k.a().a(v4_20, v3_43, v0_7, v1_39);
        com.teamspeak.ts3client.a.k.a().a(Boolean.valueOf(1));
        if (!com.teamspeak.ts3client.f.ak.a(this.av)) {
            com.teamspeak.ts3client.a.j.a().b(1);
        } else {
            com.teamspeak.ts3client.a.j.a().a(1);
        }
        com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_activateCaptureDevice(com.teamspeak.ts3client.f.ak.b(this.av));
        com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_openCaptureDevice(com.teamspeak.ts3client.f.ak.b(this.av), "", "");
        com.teamspeak.ts3client.jni.Ts3Jni.b().a(com.teamspeak.ts3client.f.ak.b(this.av), com.teamspeak.ts3client.jni.d.k, 0);
        com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_setPreProcessorConfigValue(com.teamspeak.ts3client.f.ak.b(this.av), "vad", "true");
        com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_setPreProcessorConfigValue(com.teamspeak.ts3client.f.ak.b(this.av), "voiceactivation_level", new StringBuilder().append(com.teamspeak.ts3client.Ts3Application.a().e.getInt(com.teamspeak.ts3client.f.ak.d(this.av), 0)).toString());
        com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_setLocalTestMode(com.teamspeak.ts3client.f.ak.b(this.av), 1);
        com.teamspeak.ts3client.jni.l.a().a(this);
        com.teamspeak.ts3client.a.k.a().d();
        this.au.start();
        return v2_7;
    }

    public final void a(com.teamspeak.ts3client.jni.k p5)
    {
        if (((p5 instanceof com.teamspeak.ts3client.jni.events.TalkStatusChange)) && ((((com.teamspeak.ts3client.jni.events.TalkStatusChange) p5).a == com.teamspeak.ts3client.f.ak.b(this.av)) && (this.aw != null))) {
            if (((com.teamspeak.ts3client.jni.events.TalkStatusChange) p5).b != 1) {
                this.i().runOnUiThread(new com.teamspeak.ts3client.f.ar(this));
            } else {
                this.i().runOnUiThread(new com.teamspeak.ts3client.f.aq(this));
            }
        }
        return;
    }

    public final void e()
    {
        android.content.IntentFilter v0_1 = new android.content.IntentFilter();
        v0_1.addAction("android.intent.action.HEADSET_PLUG");
        this.i().registerReceiver(com.teamspeak.ts3client.f.ak.h(this.av), v0_1);
        super.e();
        return;
    }

    public final void f()
    {
        super.f();
        return;
    }

    public final void onDismiss(android.content.DialogInterface p6)
    {
        this.at = 0;
        com.teamspeak.ts3client.jni.l.a().b(this);
        this.au.interrupt();
        try {
            this.i().unregisterReceiver(com.teamspeak.ts3client.f.ak.h(this.av));
        } catch (com.teamspeak.ts3client.jni.Ts3Jni v0) {
            com.teamspeak.ts3client.Ts3Application.a().d.log(java.util.logging.Level.WARNING, "java.lang.IllegalArgumentException: Receiver not registered");
        }
        if (com.teamspeak.ts3client.Ts3Application.a().a == null) {
            com.teamspeak.ts3client.a.k.a().f();
            com.teamspeak.ts3client.a.k.a().b();
            com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_closeCaptureDevice(com.teamspeak.ts3client.f.ak.b(this.av));
            com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_destroyServerConnectionHandler(com.teamspeak.ts3client.f.ak.b(this.av));
        } else {
            com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_closeCaptureDevice(com.teamspeak.ts3client.f.ak.b(this.av));
            com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_setLocalTestMode(com.teamspeak.ts3client.f.ak.b(this.av), 0);
            com.teamspeak.ts3client.jni.Ts3Jni.b().ts3client_destroyServerConnectionHandler(com.teamspeak.ts3client.f.ak.b(this.av));
            com.teamspeak.ts3client.Ts3Application.a().a.r();
        }
        super.onDismiss(p6);
        return;
    }
}
