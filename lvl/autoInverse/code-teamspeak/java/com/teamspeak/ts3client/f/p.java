package com.teamspeak.ts3client.f;
public final class p extends android.widget.RelativeLayout implements android.view.View$OnClickListener {
    private String a;
    private com.teamspeak.ts3client.f.r b;
    private android.support.v4.app.bi c;
    private String d;
    private String e;
    private String f;
    private String g;

    public p(android.content.Context p8, String p9, String p10, String p11, String p12, String p13, android.support.v4.app.bi p14)
    {
        this(p8);
        this.setLayoutParams(new android.widget.RelativeLayout$LayoutParams(-1, -2));
        this.setPadding(0, 18, 0, 18);
        this.setBackgroundResource(2130837686);
        this.c = p14;
        this.a = p10;
        this.e = p11;
        this.f = p13;
        this.d = p9;
        this.g = p12;
        int v0_4 = new android.widget.TextView(p8);
        android.widget.TextView v1_2 = new android.widget.TextView(p8);
        v0_4.setText(this.a);
        v0_4.setTextSize(1101004800);
        v0_4.setTypeface(0, 1);
        v0_4.setId(1);
        v1_2.setText(this.e);
        v1_2.setTextSize(1092616192);
        v1_2.setTypeface(0, 2);
        v1_2.setId(2);
        android.widget.RelativeLayout$LayoutParams v2_8 = new android.widget.RelativeLayout$LayoutParams(-2, -2);
        v2_8.addRule(10);
        this.addView(v0_4, v2_8);
        android.widget.RelativeLayout$LayoutParams v2_10 = new android.widget.RelativeLayout$LayoutParams(-2, -2);
        v2_10.addRule(3, v0_4.getId());
        this.addView(v1_2, v2_10);
        this.setClickable(1);
        this.setOnClickListener(this);
        return;
    }

    static synthetic String a(com.teamspeak.ts3client.f.p p1)
    {
        return p1.d;
    }

    static synthetic String b(com.teamspeak.ts3client.f.p p1)
    {
        return p1.f;
    }

    static synthetic String c(com.teamspeak.ts3client.f.p p1)
    {
        return p1.g;
    }

    static synthetic com.teamspeak.ts3client.f.r d(com.teamspeak.ts3client.f.p p1)
    {
        return p1.b;
    }

    static synthetic String e(com.teamspeak.ts3client.f.p p1)
    {
        return p1.a;
    }

    public final void onClick(android.view.View p4)
    {
        this.b = new com.teamspeak.ts3client.f.r(this, 0);
        this.b.a(this.c, this.a);
        p4.performHapticFeedback(1);
        return;
    }
}
