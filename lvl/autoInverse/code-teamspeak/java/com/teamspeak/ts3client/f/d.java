package com.teamspeak.ts3client.f;
final class d implements android.widget.CompoundButton$OnCheckedChangeListener {
    final synthetic com.teamspeak.ts3client.Ts3Application a;
    final synthetic com.teamspeak.ts3client.f.c b;

    d(com.teamspeak.ts3client.f.c p1, com.teamspeak.ts3client.Ts3Application p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final void onCheckedChanged(android.widget.CompoundButton p6, boolean p7)
    {
        if (com.teamspeak.ts3client.f.c.a(this.b).equals("")) {
            this.a.e.edit().putBoolean(com.teamspeak.ts3client.f.c.b(this.b), p7).commit();
        } else {
            if (((this.a.e.getBoolean(new StringBuilder().append(com.teamspeak.ts3client.f.c.b(this.b)).append("_warn").toString(), 0)) && (!com.teamspeak.ts3client.f.c.c(this.b))) || (!p7)) {
                this.a.e.edit().putBoolean(com.teamspeak.ts3client.f.c.b(this.b), p7).commit();
            } else {
                android.app.AlertDialog v0_18 = new android.app.AlertDialog$Builder(this.b.getContext()).create();
                com.teamspeak.ts3client.data.d.q.a(v0_18);
                v0_18.setTitle("Warning");
                v0_18.setMessage(com.teamspeak.ts3client.f.c.a(this.b));
                v0_18.setButton(-1, com.teamspeak.ts3client.data.e.a.a("button.ok"), new com.teamspeak.ts3client.f.e(this, p7, v0_18));
                v0_18.setCancelable(0);
                v0_18.show();
            }
        }
        return;
    }
}
