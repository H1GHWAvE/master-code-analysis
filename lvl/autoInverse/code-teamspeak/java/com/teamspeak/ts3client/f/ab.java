package com.teamspeak.ts3client.f;
public final class ab extends android.widget.RelativeLayout implements android.view.View$OnClickListener {
    private android.widget.SeekBar a;
    private android.widget.TextView b;
    private String c;
    private com.teamspeak.ts3client.f.ad d;
    private android.support.v4.app.bi e;
    private String f;
    private int g;
    private int h;
    private int i;
    private String j;

    public ab(android.content.Context p8, String p9, String p10, String p11, android.support.v4.app.bi p12)
    {
        this(p8);
        this.setLayoutParams(new android.widget.RelativeLayout$LayoutParams(-1, -2));
        this.setPadding(0, 18, 0, 18);
        this.setBackgroundResource(2130837686);
        this.e = p12;
        this.c = p10;
        this.j = p11;
        this.g = -20;
        this.h = 40;
        this.f = p9;
        this.i = 25;
        int v0_7 = new android.widget.TextView(p8);
        android.widget.TextView v1_2 = new android.widget.TextView(p8);
        v0_7.setText(this.c);
        v0_7.setTextSize(1101004800);
        v0_7.setTypeface(0, 1);
        v0_7.setId(1);
        v1_2.setText(this.j);
        v1_2.setTextSize(1092616192);
        v1_2.setTypeface(0, 2);
        v1_2.setId(2);
        android.widget.RelativeLayout$LayoutParams v2_8 = new android.widget.RelativeLayout$LayoutParams(-2, -2);
        v2_8.addRule(10);
        this.addView(v0_7, v2_8);
        android.widget.RelativeLayout$LayoutParams v2_10 = new android.widget.RelativeLayout$LayoutParams(-2, -2);
        v2_10.addRule(3, v0_7.getId());
        this.addView(v1_2, v2_10);
        this.setClickable(1);
        this.setOnClickListener(this);
        return;
    }

    static synthetic android.widget.SeekBar a(com.teamspeak.ts3client.f.ab p0, android.widget.SeekBar p1)
    {
        p0.a = p1;
        return p1;
    }

    static synthetic android.widget.TextView a(com.teamspeak.ts3client.f.ab p0, android.widget.TextView p1)
    {
        p0.b = p1;
        return p1;
    }

    static synthetic String a(com.teamspeak.ts3client.f.ab p1)
    {
        return p1.f;
    }

    static synthetic int b(com.teamspeak.ts3client.f.ab p1)
    {
        return p1.i;
    }

    static synthetic android.widget.SeekBar c(com.teamspeak.ts3client.f.ab p1)
    {
        return p1.a;
    }

    static synthetic int d(com.teamspeak.ts3client.f.ab p1)
    {
        return p1.h;
    }

    static synthetic int e(com.teamspeak.ts3client.f.ab p1)
    {
        return p1.g;
    }

    static synthetic android.widget.TextView f(com.teamspeak.ts3client.f.ab p1)
    {
        return p1.b;
    }

    static synthetic com.teamspeak.ts3client.f.ad g(com.teamspeak.ts3client.f.ab p1)
    {
        return p1.d;
    }

    static synthetic String h(com.teamspeak.ts3client.f.ab p1)
    {
        return p1.c;
    }

    public final void onClick(android.view.View p4)
    {
        this.d = new com.teamspeak.ts3client.f.ad(this, 0);
        this.d.a(this.e, this.c);
        p4.performHapticFeedback(1);
        return;
    }
}
