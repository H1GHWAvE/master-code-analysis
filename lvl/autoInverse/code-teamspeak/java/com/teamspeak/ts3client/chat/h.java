package com.teamspeak.ts3client.chat;
public final class h extends android.widget.BaseAdapter {
    java.util.ArrayList a;
    com.teamspeak.ts3client.chat.a b;
    private android.content.Context c;
    private android.view.LayoutInflater d;
    private com.teamspeak.ts3client.Ts3Application e;
    private java.text.DateFormat f;

    public h(android.content.Context p2)
    {
        this.c = p2;
        this.e = ((com.teamspeak.ts3client.Ts3Application) p2.getApplicationContext());
        this.d = android.view.LayoutInflater.from(p2);
        this.f = java.text.DateFormat.getDateTimeInstance();
        return;
    }

    private void a(com.teamspeak.ts3client.chat.a p2)
    {
        this.b = p2;
        p2.f = this;
        this.a = p2.c;
        this.a();
        return;
    }

    public final void a()
    {
        this.e.c.i().runOnUiThread(new com.teamspeak.ts3client.chat.i(this));
        return;
    }

    public final int getCount()
    {
        return this.a.size();
    }

    public final Object getItem(int p2)
    {
        return this.a.get(p2);
    }

    public final long getItemId(int p3)
    {
        return 0;
    }

    public final android.view.View getView(int p13, android.view.View p14, android.view.ViewGroup p15)
    {
        android.view.View v7 = this.d.inflate(2130903080, 0);
        int v0_3 = ((android.widget.ImageView) v7.findViewById(2131493112));
        String v1_3 = ((android.widget.ImageView) v7.findViewById(2131493114));
        android.widget.TextView v2_3 = ((android.widget.TextView) v7.findViewById(2131493117));
        v2_3.setMovementMethod(com.teamspeak.ts3client.data.d.n.a());
        android.widget.TextView v3_3 = ((android.widget.TextView) v7.findViewById(2131493118));
        android.graphics.Bitmap v4_2 = ((android.widget.TextView) v7.findViewById(2131493116));
        android.widget.RelativeLayout v5_2 = ((android.widget.RelativeLayout) v7.findViewById(2131493113));
        if (!((com.teamspeak.ts3client.chat.y) this.a.get(p13)).f) {
            if (!this.b.e.equals("CHANNEL")) {
                if (!this.b.e.equals("SERVER")) {
                    v4_2.setText("");
                    if (!((com.teamspeak.ts3client.chat.y) this.a.get(p13)).d) {
                        if (this.b.g.y != null) {
                            v0_3.setImageBitmap(this.b.g.y);
                        }
                        v1_3.setVisibility(4);
                        v5_2.setBackgroundResource(2130837647);
                    } else {
                        v0_3.setVisibility(4);
                        v5_2.setBackgroundResource(2130837649);
                    }
                } else {
                    v1_3.setVisibility(8);
                    v0_3.setVisibility(8);
                    v4_2.setText(new StringBuilder("\"").append(((com.teamspeak.ts3client.chat.y) this.a.get(p13)).a).append("\": ").toString());
                }
            } else {
                v1_3.setVisibility(8);
                v0_3.setVisibility(8);
                v4_2.setText(new StringBuilder("\"").append(((com.teamspeak.ts3client.chat.y) this.a.get(p13)).a).append("\": ").toString());
            }
            v2_3.setText(((com.teamspeak.ts3client.chat.y) this.a.get(p13)).c);
            v3_3.setText(this.f.format(((com.teamspeak.ts3client.chat.y) this.a.get(p13)).e));
        } else {
            v1_3.setVisibility(8);
            v0_3.setVisibility(8);
            v4_2.setText("");
            v2_3.setText(((com.teamspeak.ts3client.chat.y) this.a.get(p13)).b);
            v3_3.setText(this.f.format(((com.teamspeak.ts3client.chat.y) this.a.get(p13)).e));
            if ((!this.b.e.equals("CHANNEL")) && (!this.b.e.equals("SERVER"))) {
                v5_2.setBackgroundResource(2130837648);
            }
        }
        v2_3.setFocusable(1);
        v2_3.setFocusableInTouchMode(1);
        return v7;
    }
}
