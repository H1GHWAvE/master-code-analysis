package com.teamspeak.ts3client.chat;
public final class d extends android.widget.BaseAdapter {
    private static com.teamspeak.ts3client.chat.d e;
    private static android.support.v4.app.bi f;
    public java.util.ArrayList a;
    public java.util.HashMap b;
    public com.teamspeak.ts3client.chat.a c;
    public com.teamspeak.ts3client.chat.a d;
    private android.view.LayoutInflater g;
    private android.content.Context h;
    private com.teamspeak.ts3client.Ts3Application i;
    private java.text.DateFormat j;

    private d(android.content.Context p2)
    {
        this.a = new java.util.ArrayList();
        this.b = new java.util.HashMap();
        this.g = android.view.LayoutInflater.from(p2);
        this.h = p2;
        this.i = ((com.teamspeak.ts3client.Ts3Application) p2.getApplicationContext());
        this.j = java.text.DateFormat.getDateTimeInstance();
        return;
    }

    static synthetic com.teamspeak.ts3client.Ts3Application a(com.teamspeak.ts3client.chat.d p1)
    {
        return p1.i;
    }

    public static com.teamspeak.ts3client.chat.d a()
    {
        return com.teamspeak.ts3client.chat.d.e;
    }

    public static com.teamspeak.ts3client.chat.d a(android.content.Context p1)
    {
        if (com.teamspeak.ts3client.chat.d.e == null) {
            com.teamspeak.ts3client.chat.d.e = new com.teamspeak.ts3client.chat.d(p1);
        }
        return com.teamspeak.ts3client.chat.d.e;
    }

    public static void a(android.support.v4.app.bi p0)
    {
        com.teamspeak.ts3client.chat.d.f = p0;
        return;
    }

    public static void a(String p7)
    {
        com.teamspeak.ts3client.chat.d.e.d("CHANNEL").a(new com.teamspeak.ts3client.chat.y(0, "", p7, Boolean.valueOf(0), Boolean.valueOf(1)));
        return;
    }

    static synthetic java.util.ArrayList b(com.teamspeak.ts3client.chat.d p1)
    {
        return p1.a;
    }

    private void b(com.teamspeak.ts3client.chat.a p3)
    {
        if (this.a.contains(p3)) {
            this.a.remove(p3);
            this.b.remove(p3.g.b);
            this.d();
        }
        return;
    }

    public static void b(String p7)
    {
        com.teamspeak.ts3client.chat.d.e.d("SERVER").a(new com.teamspeak.ts3client.chat.y(0, "", p7, Boolean.valueOf(0), Boolean.valueOf(1)));
        return;
    }

    static synthetic android.support.v4.app.bi e()
    {
        return com.teamspeak.ts3client.chat.d.f;
    }

    private void f()
    {
        this.c = 0;
        this.d = 0;
        this.a.clear();
        this.b.clear();
        this.a = new java.util.ArrayList();
        this.b = new java.util.HashMap();
        return;
    }

    private void g()
    {
        this.c = new com.teamspeak.ts3client.chat.a("Server Tab", "SERVER", 0);
        this.c.b = 1;
        this.d = new com.teamspeak.ts3client.chat.a("Channel Tab", "CHANNEL", 0);
        this.d.b = 1;
        this.a(this.c);
        this.a(this.d);
        return;
    }

    public final void a(com.teamspeak.ts3client.chat.a p3)
    {
        if (!this.a.contains(p3)) {
            this.a.add(p3);
            this.b.put(p3.e, p3);
        }
        return;
    }

    public final com.teamspeak.ts3client.chat.a b()
    {
        return this.d;
    }

    public final com.teamspeak.ts3client.chat.a c()
    {
        return this.c;
    }

    public final Boolean c(String p2)
    {
        Boolean v0_3;
        if (!this.b.containsKey(p2)) {
            v0_3 = Boolean.valueOf(0);
        } else {
            v0_3 = Boolean.valueOf(1);
        }
        return v0_3;
    }

    public final com.teamspeak.ts3client.chat.a d(String p2)
    {
        int v0_2;
        if (!this.b.containsKey(p2)) {
            v0_2 = 0;
        } else {
            v0_2 = ((com.teamspeak.ts3client.chat.a) this.b.get(p2));
        }
        return v0_2;
    }

    public final void d()
    {
        this.i.c.i().runOnUiThread(new com.teamspeak.ts3client.chat.g(this));
        return;
    }

    public final int getCount()
    {
        return this.a.size();
    }

    public final Object getItem(int p2)
    {
        return this.a.get(p2);
    }

    public final long getItemId(int p3)
    {
        return 0;
    }

    public final android.view.View getView(int p7, android.view.View p8, android.view.ViewGroup p9)
    {
        android.view.View v5 = this.g.inflate(2130903078, 0);
        android.widget.RelativeLayout v0_3 = ((android.widget.TextView) v5.findViewById(2131493101));
        com.teamspeak.ts3client.chat.f v1_3 = ((android.widget.ImageView) v5.findViewById(2131493100));
        int v2_3 = ((android.widget.TextView) v5.findViewById(2131493102));
        android.widget.TextView v3_2 = ((android.widget.TextView) v5.findViewById(2131493103));
        if (((com.teamspeak.ts3client.chat.a) this.a.get(p7)).b) {
            v1_3.setVisibility(4);
        }
        v0_3.setText(((com.teamspeak.ts3client.chat.a) this.a.get(p7)).a);
        v2_3.setText(this.j.format(((com.teamspeak.ts3client.chat.a) this.a.get(p7)).h));
        if ((((com.teamspeak.ts3client.chat.a) this.a.get(p7)).c.size() - ((com.teamspeak.ts3client.chat.a) this.a.get(p7)).d) > 0) {
            v3_2.setText(com.teamspeak.ts3client.data.e.a.a("chat.newmessages"));
        }
        v1_3.setOnClickListener(new com.teamspeak.ts3client.chat.e(this, p7));
        ((android.widget.RelativeLayout) v5.findViewById(2131493099)).setOnClickListener(new com.teamspeak.ts3client.chat.f(this, p7));
        return v5;
    }
}
