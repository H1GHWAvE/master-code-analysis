package com.teamspeak.ts3client;
final class d extends android.webkit.WebViewClient {
    final synthetic com.teamspeak.ts3client.c a;

    d(com.teamspeak.ts3client.c p1)
    {
        this.a = p1;
        return;
    }

    public final boolean shouldOverrideUrlLoading(android.webkit.WebView p7, String p8)
    {
        if ((!p8.startsWith("client://")) && ((!p8.startsWith("ts3file://")) && (!p8.startsWith("ts3server://")))) {
            if (!p8.startsWith("channelid://")) {
                try {
                    if (com.teamspeak.ts3client.Ts3Application.a().a.a.ts3client_bbcode_shouldPrependHTTP(p8)) {
                        p8 = new StringBuilder("http://").append(p8).toString();
                    }
                } catch (android.support.v4.app.bl v0) {
                }
                android.support.v4.app.bl v0_15 = android.net.Uri.parse(p8);
                String v1_1 = p7.getContext();
                android.content.Intent v2_1 = new android.content.Intent("android.intent.action.VIEW", v0_15);
                v2_1.putExtra("com.android.browser.application_id", v1_1.getPackageName());
                v1_1.startActivity(v2_1);
            } else {
                try {
                    android.support.v4.app.bl v0_19 = Long.parseLong(p8.replaceAll("channelid://(\\d+).*", "$1"));
                    android.content.Intent v2_3 = com.teamspeak.ts3client.Ts3Application.a().a;
                } catch (android.support.v4.app.bl v0) {
                }
                if ((v2_3 != null) && (v2_3.c().d(Long.valueOf(v0_19)))) {
                    new com.teamspeak.ts3client.d.a.a(v2_3.c().a(Long.valueOf(v0_19)), 0).a(v2_3.k.M, "ChannelActionDialog");
                }
            }
        }
        return 1;
    }
}
