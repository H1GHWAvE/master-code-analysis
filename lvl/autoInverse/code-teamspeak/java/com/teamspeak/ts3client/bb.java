package com.teamspeak.ts3client;
final class bb implements java.lang.Runnable {
    final synthetic boolean a;
    final synthetic com.teamspeak.ts3client.t b;

    bb(com.teamspeak.ts3client.t p1, boolean p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final void run()
    {
        android.app.Dialog v0_1 = new android.app.Dialog(this.b.i(), 2131165365);
        com.teamspeak.ts3client.data.d.q.a(v0_1);
        android.widget.LinearLayout v1_3 = new android.widget.LinearLayout(this.b.i());
        v1_3.setOrientation(1);
        if (this.a) {
            String v2_6 = new android.widget.TextView(this.b.i());
            v2_6.setText(com.teamspeak.ts3client.data.e.a.a("messages.newserver.text"));
            v1_3.addView(v2_6);
        }
        String v2_8 = new android.widget.EditText(this.b.i());
        v1_3.addView(v2_8);
        android.widget.Button v3_7 = new android.widget.Button(this.b.i());
        v3_7.setText(com.teamspeak.ts3client.data.e.a.a("button.ok"));
        v3_7.setOnClickListener(new com.teamspeak.ts3client.bc(this, v0_1, v2_8));
        v1_3.addView(v3_7);
        v0_1.setTitle(com.teamspeak.ts3client.data.e.a.a("messages.newserver.info"));
        v0_1.setContentView(v1_3);
        v0_1.show();
        return;
    }
}
