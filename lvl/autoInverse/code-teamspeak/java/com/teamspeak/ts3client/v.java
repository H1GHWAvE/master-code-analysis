package com.teamspeak.ts3client;
final class v implements java.lang.Runnable {
    final synthetic com.teamspeak.ts3client.data.a[] a;
    final synthetic com.teamspeak.ts3client.t b;

    v(com.teamspeak.ts3client.t p1, com.teamspeak.ts3client.data.a[] p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final void run()
    {
        if (com.teamspeak.ts3client.t.b(this.b).a != null) {
            com.teamspeak.ts3client.data.g v8 = com.teamspeak.ts3client.t.b(this.b).a.f;
            java.util.List v1_0 = this.a;
            if (v1_0.length > 0) {
                v8.a.clear();
                long v4_0 = v1_0.length;
                int v0_8 = 0;
                while (v0_8 < v4_0) {
                    com.teamspeak.ts3client.data.a v5 = v1_0[v0_8];
                    if (v5 != null) {
                        v8.a.add(v5);
                    }
                    v0_8++;
                }
                int v0_10 = new com.teamspeak.ts3client.data.a("empty", 0, 0, 0);
                v0_10.q = 1;
                v8.a.add(v0_10);
            }
            com.teamspeak.ts3client.t.b(this.b).a.f.notifyDataSetChanged();
        }
        return;
    }
}
