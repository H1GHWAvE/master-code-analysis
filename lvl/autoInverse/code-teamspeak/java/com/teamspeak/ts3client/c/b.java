package com.teamspeak.ts3client.c;
public final class b extends android.support.v4.app.Fragment {
    private com.teamspeak.ts3client.Ts3Application a;
    private android.widget.ListView b;
    private com.teamspeak.ts3client.c.c c;
    private com.teamspeak.ts3client.data.b.c d;

    public b()
    {
        return;
    }

    public final android.view.View a(android.view.LayoutInflater p3, android.view.ViewGroup p4)
    {
        this.a = ((com.teamspeak.ts3client.Ts3Application) this.i().getApplicationContext());
        this.n();
        this.a.o.b(com.teamspeak.ts3client.data.e.a.a("menu.contact"));
        this.a.c = this;
        android.view.View v1_3 = p3.inflate(2130903090, p4, 0);
        this.b = ((android.widget.ListView) v1_3.findViewById(2131493236));
        return v1_3;
    }

    public final void a()
    {
        this.c = new com.teamspeak.ts3client.c.c(this.i(), this.M);
        java.util.ArrayList v2_1 = this.d.b();
        if (v2_1 != null) {
            int v1_1 = 0;
            while (v1_1 < v2_1.size()) {
                java.util.ArrayList v3_0 = this.c;
                int v0_7 = ((com.teamspeak.ts3client.c.a) v2_1.get(v1_1));
                if (!v3_0.a.contains(v0_7)) {
                    v3_0.a.add(v0_7);
                }
                v1_1++;
            }
        }
        this.b.setAdapter(this.c);
        return;
    }

    public final void a(android.os.Bundle p2)
    {
        super.a(p2);
        this.a = ((com.teamspeak.ts3client.Ts3Application) this.i().getApplicationContext());
        return;
    }

    public final void a(android.view.Menu p1, android.view.MenuInflater p2)
    {
        p1.clear();
        super.a(p1, p2);
        return;
    }

    public final void c(android.os.Bundle p2)
    {
        super.c(p2);
        this.d = com.teamspeak.ts3client.data.b.c.a;
        this.d.c = this;
        this.a();
        return;
    }

    public final void g()
    {
        super.g();
        this.d.c = 0;
        return;
    }
}
