package com.a.b.g;
abstract class h implements com.a.b.g.ak {

    h()
    {
        return;
    }

    public com.a.b.g.ag a(long p2)
    {
        return this.a().a(p2).a();
    }

    public com.a.b.g.ag a(CharSequence p2)
    {
        return this.a().a(p2).a();
    }

    public final com.a.b.g.ag a(CharSequence p2, java.nio.charset.Charset p3)
    {
        return this.a().a(p2, p3).a();
    }

    public final com.a.b.g.ag a(Object p2, com.a.b.g.w p3)
    {
        return this.a().a(p2, p3).a();
    }

    public final com.a.b.g.ag a(byte[] p2)
    {
        return this.a().b(p2).a();
    }

    public final com.a.b.g.ag a(byte[] p3, int p4)
    {
        return this.a().b(p3, 0, p4).a();
    }

    public final com.a.b.g.al a(int p2)
    {
        com.a.b.g.al v0_0;
        if (p2 < 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        com.a.b.b.cn.a(v0_0);
        return this.a();
    }

    public com.a.b.g.ag b(int p2)
    {
        return this.a().a(p2).a();
    }
}
