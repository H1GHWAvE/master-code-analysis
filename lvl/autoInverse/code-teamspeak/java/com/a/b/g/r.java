package com.a.b.g;
final class r extends com.a.b.g.h implements java.io.Serializable {
    private static final long d;
    private final com.a.b.b.dz a;
    private final int b;
    private final String c;

    r(com.a.b.b.dz p6, int p7, String p8)
    {
        String v0_4;
        this.a = ((com.a.b.b.dz) com.a.b.b.cn.a(p6));
        if ((p7 != 32) && (p7 != 64)) {
            v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        Object[] v2_1 = new Object[1];
        v2_1[0] = Integer.valueOf(p7);
        com.a.b.b.cn.a(v0_4, "bits (%s) must be either 32 or 64", v2_1);
        this.b = p7;
        this.c = ((String) com.a.b.b.cn.a(p8));
        return;
    }

    static synthetic int a(com.a.b.g.r p1)
    {
        return p1.b;
    }

    public final com.a.b.g.al a()
    {
        return new com.a.b.g.t(this, ((java.util.zip.Checksum) this.a.a()), 0);
    }

    public final int b()
    {
        return this.b;
    }

    public final String toString()
    {
        return this.c;
    }
}
