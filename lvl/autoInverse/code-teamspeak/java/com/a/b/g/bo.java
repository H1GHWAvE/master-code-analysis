package com.a.b.g;
final class bo extends com.a.b.g.h implements java.io.Serializable {
    private static final long e;
    private final int a;
    private final int b;
    private final long c;
    private final long d;

    bo(long p8, long p10)
    {
        Object[] v1_0 = new Object[1];
        v1_0[0] = Integer.valueOf(2);
        com.a.b.b.cn.a(1, "The number of SipRound iterations (c=%s) during Compression must be positive.", v1_0);
        Object[] v1_1 = new Object[1];
        v1_1[0] = Integer.valueOf(4);
        com.a.b.b.cn.a(1, "The number of SipRound iterations (d=%s) during Finalization must be positive.", v1_1);
        this.a = 2;
        this.b = 4;
        this.c = p8;
        this.d = p10;
        return;
    }

    public final com.a.b.g.al a()
    {
        return new com.a.b.g.bp(this.a, this.b, this.c, this.d);
    }

    public final int b()
    {
        return 64;
    }

    public final boolean equals(Object p7)
    {
        int v0 = 0;
        if (((p7 instanceof com.a.b.g.bo)) && ((this.a == ((com.a.b.g.bo) p7).a) && ((this.b == ((com.a.b.g.bo) p7).b) && ((this.c == ((com.a.b.g.bo) p7).c) && (this.d == ((com.a.b.g.bo) p7).d))))) {
            v0 = 1;
        }
        return v0;
    }

    public final int hashCode()
    {
        return ((int) ((((long) ((this.getClass().hashCode() ^ this.a) ^ this.b)) ^ this.c) ^ this.d));
    }

    public final String toString()
    {
        return new StringBuilder(81).append("Hashing.sipHash").append(this.a).append(this.b).append("(").append(this.c).append(", ").append(this.d).append(")").toString();
    }
}
