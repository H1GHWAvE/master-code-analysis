package com.a.b.g;
public final class j implements com.a.b.b.co, java.io.Serializable {
    private static final com.a.b.g.m e;
    private final com.a.b.g.q a;
    private final int b;
    private final com.a.b.g.w c;
    private final com.a.b.g.m d;

    static j()
    {
        com.a.b.g.j.e = com.a.b.g.n.b;
        return;
    }

    private j(com.a.b.g.q p7, int p8, com.a.b.g.w p9, com.a.b.g.m p10)
    {
        com.a.b.g.m v0_0;
        if (p8 <= 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        com.a.b.g.m v0_2;
        Integer v4_0 = new Object[1];
        v4_0[0] = Integer.valueOf(p8);
        com.a.b.b.cn.a(v0_0, "numHashFunctions (%s) must be > 0", v4_0);
        if (p8 > 255) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        Object[] v1_1 = new Object[1];
        v1_1[0] = Integer.valueOf(p8);
        com.a.b.b.cn.a(v0_2, "numHashFunctions (%s) must be <= 255", v1_1);
        this.a = ((com.a.b.g.q) com.a.b.b.cn.a(p7));
        this.b = p8;
        this.c = ((com.a.b.g.w) com.a.b.b.cn.a(p9));
        this.d = ((com.a.b.g.m) com.a.b.b.cn.a(p10));
        return;
    }

    synthetic j(com.a.b.g.q p1, int p2, com.a.b.g.w p3, com.a.b.g.m p4, byte p5)
    {
        this(p1, p2, p3, p4);
        return;
    }

    private static int a(long p6, long p8)
    {
        return Math.max(1, ((int) Math.round(((((double) p8) / ((double) p6)) * Math.log(2.0)))));
    }

    private static long a(long p6, double p8)
    {
        if (p8 == 0) {
            p8 = 1;
        }
        return ((long) ((((double) (- p6)) * Math.log(p8)) / (Math.log(2.0) * Math.log(2.0))));
    }

    private com.a.b.g.j a()
    {
        return new com.a.b.g.j(this.a.b(), this.b, this.c, this.d);
    }

    private static com.a.b.g.j a(com.a.b.g.w p1, int p2)
    {
        return com.a.b.g.j.a(p1, p2, com.a.b.g.j.e);
    }

    private static com.a.b.g.j a(com.a.b.g.w p10, int p11, com.a.b.g.m p12)
    {
        IllegalArgumentException v0_0;
        com.a.b.b.cn.a(p10);
        if (p11 < 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        StringBuilder v4_0 = new Object[1];
        v4_0[0] = Integer.valueOf(p11);
        com.a.b.b.cn.a(v0_0, "Expected insertions (%s) must be >= 0", v4_0);
        String v3_1 = new Object[1];
        v3_1[0] = Double.valueOf(0.03);
        com.a.b.b.cn.a(1, "False positive probability (%s) must be > 0.0", v3_1);
        String v3_2 = new Object[1];
        v3_2[0] = Double.valueOf(0.03);
        com.a.b.b.cn.a(1, "False positive probability (%s) must be < 1.0", v3_2);
        com.a.b.b.cn.a(p12);
        if (p11 == 0) {
            p11 = 1;
        }
        String v2_6 = ((long) ((((double) (- ((long) p11))) * Math.log(0.03)) / (Math.log(2.0) * Math.log(2.0))));
        try {
            return new com.a.b.g.j(new com.a.b.g.q(v2_6), Math.max(1, ((int) Math.round(((((double) v2_6) / ((double) ((long) p11))) * Math.log(2.0))))), p10, p12);
        } catch (IllegalArgumentException v0_5) {
            throw new IllegalArgumentException(new StringBuilder(57).append("Could not create BloomFilter of ").append(v2_6).append(" bits").toString(), v0_5);
        }
    }

    private static com.a.b.g.j a(java.io.InputStream p10, com.a.b.g.w p11)
    {
        int v1_0 = -1;
        com.a.b.b.cn.a(p10, "InputStream");
        com.a.b.b.cn.a(p11, "Funnel");
        try {
            com.a.b.g.q v4_1 = new java.io.DataInputStream(p10);
            String v3_0 = v4_1.readByte();
            try {
                String v2_0 = (v4_1.readByte() & 255);
                try {
                    v1_0 = v4_1.readInt();
                    com.a.b.g.n v5_0 = com.a.b.g.n.values()[v3_0];
                    long[] v6_0 = new long[v1_0];
                    com.a.b.g.j v0_5 = 0;
                } catch (com.a.b.g.j v0_2) {
                    com.a.b.g.n v5_3 = String.valueOf(String.valueOf("Unable to deserialize BloomFilter from InputStream. strategyOrdinal: "));
                    com.a.b.g.q v4_5 = new java.io.IOException(new StringBuilder((v5_3.length() + 65)).append(v5_3).append(v3_0).append(" numHashFunctions: ").append(v2_0).append(" dataLength: ").append(v1_0).toString());
                    v4_5.initCause(v0_2);
                    throw v4_5;
                }
                while (v0_5 < v1_0) {
                    v6_0[v0_5] = v4_1.readLong();
                    v0_5++;
                }
                return new com.a.b.g.j(new com.a.b.g.q(v6_0), v2_0, p11, v5_0);
            } catch (com.a.b.g.j v0_2) {
                v2_0 = -1;
            }
        } catch (com.a.b.g.j v0_2) {
            v2_0 = -1;
            v3_0 = -1;
        }
    }

    static synthetic com.a.b.g.q a(com.a.b.g.j p1)
    {
        return p1.a;
    }

    private void a(java.io.OutputStream p10)
    {
        String v1_1 = new java.io.DataOutputStream(p10);
        long[] v2_0 = ((long) this.d.ordinal());
        int v0_3 = ((byte) ((int) v2_0));
        if (((long) v0_3) == v2_0) {
            v1_1.writeByte(v0_3);
            long[] v2_1 = ((long) this.b);
            if ((v2_1 >> 8) == 0) {
                v1_1.writeByte(((byte) ((int) v2_1)));
                v1_1.writeInt(this.a.a.length);
                long[] v2_2 = this.a.a;
                int v3 = v2_2.length;
                int v0_13 = 0;
                while (v0_13 < v3) {
                    v1_1.writeLong(v2_2[v0_13]);
                    v0_13++;
                }
                return;
            } else {
                throw new IllegalArgumentException(new StringBuilder(34).append("Out of range: ").append(v2_1).toString());
            }
        } else {
            throw new IllegalArgumentException(new StringBuilder(34).append("Out of range: ").append(v2_0).toString());
        }
    }

    private double b()
    {
        return Math.pow((((double) this.a.b) / ((double) this.a.a())), ((double) this.b));
    }

    static synthetic int b(com.a.b.g.j p1)
    {
        return p1.b;
    }

    private static com.a.b.g.j b(com.a.b.g.w p1, int p2)
    {
        return com.a.b.g.j.a(p1, p2, com.a.b.g.j.e);
    }

    private boolean b(Object p5)
    {
        return this.d.b(p5, this.c, this.b, this.a);
    }

    private long c()
    {
        return this.a.a();
    }

    static synthetic com.a.b.g.w c(com.a.b.g.j p1)
    {
        return p1.c;
    }

    private boolean c(Object p5)
    {
        return this.d.a(p5, this.c, this.b, this.a);
    }

    static synthetic com.a.b.g.m d(com.a.b.g.j p1)
    {
        return p1.d;
    }

    private Object d()
    {
        return new com.a.b.g.l(this);
    }

    private boolean e(com.a.b.g.j p5)
    {
        int v0_8;
        com.a.b.b.cn.a(p5);
        if ((this == p5) || ((this.b != p5.b) || ((this.a.a() != p5.a.a()) || ((!this.d.equals(p5.d)) || (!this.c.equals(p5.c)))))) {
            v0_8 = 0;
        } else {
            v0_8 = 1;
        }
        return v0_8;
    }

    private void f(com.a.b.g.j p11)
    {
        long v0_0;
        int v2 = 0;
        com.a.b.b.cn.a(p11);
        if (this == p11) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        long v0_2;
        com.a.b.b.cn.a(v0_0, "Cannot combine a BloomFilter with itself.");
        if (this.b != p11.b) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        long v0_6;
        com.a.b.g.q v4_0 = new Object[2];
        v4_0[0] = Integer.valueOf(this.b);
        v4_0[1] = Integer.valueOf(p11.b);
        com.a.b.b.cn.a(v0_2, "BloomFilters must have the same number of hash functions (%s != %s)", v4_0);
        if (this.a.a() != p11.a.a()) {
            v0_6 = 0;
        } else {
            v0_6 = 1;
        }
        long v0_13;
        com.a.b.g.q v4_2 = new Object[2];
        v4_2[0] = Long.valueOf(this.a.a());
        v4_2[1] = Long.valueOf(p11.a.a());
        com.a.b.b.cn.a(v0_6, "BloomFilters must have the same size underlying bit arrays (%s != %s)", v4_2);
        long v0_8 = this.d.equals(p11.d);
        com.a.b.g.q v4_3 = new Object[2];
        v4_3[0] = this.d;
        v4_3[1] = p11.d;
        com.a.b.b.cn.a(v0_8, "BloomFilters must have equal strategies (%s != %s)", v4_3);
        long v0_10 = this.c.equals(p11.c);
        com.a.b.g.q v4_4 = new Object[2];
        v4_4[0] = this.c;
        v4_4[1] = p11.c;
        com.a.b.b.cn.a(v0_10, "BloomFilters must have equal funnels (%s != %s)", v4_4);
        com.a.b.g.q v3_8 = this.a;
        com.a.b.g.q v4_5 = p11.a;
        if (v3_8.a.length != v4_5.a.length) {
            v0_13 = 0;
        } else {
            v0_13 = 1;
        }
        long v6_3 = new Object[2];
        v6_3[0] = Integer.valueOf(v3_8.a.length);
        v6_3[1] = Integer.valueOf(v4_5.a.length);
        com.a.b.b.cn.a(v0_13, "BitArrays must be of equal length (%s != %s)", v6_3);
        v3_8.b = 0;
        while (v2 < v3_8.a.length) {
            long v0_17 = v3_8.a;
            v0_17[v2] = (v0_17[v2] | v4_5.a[v2]);
            v3_8.b = (v3_8.b + ((long) Long.bitCount(v3_8.a[v2])));
            v2++;
        }
        return;
    }

    public final boolean a(Object p5)
    {
        return this.d.b(p5, this.c, this.b, this.a);
    }

    public final boolean equals(Object p5)
    {
        int v0 = 1;
        if (p5 != this) {
            if (!(p5 instanceof com.a.b.g.j)) {
                v0 = 0;
            } else {
                if ((this.b != ((com.a.b.g.j) p5).b) || ((!this.c.equals(((com.a.b.g.j) p5).c)) || ((!this.a.equals(((com.a.b.g.j) p5).a)) || (!this.d.equals(((com.a.b.g.j) p5).d))))) {
                    v0 = 0;
                }
            }
        }
        return v0;
    }

    public final int hashCode()
    {
        int v0_1 = new Object[4];
        v0_1[0] = Integer.valueOf(this.b);
        v0_1[1] = this.c;
        v0_1[2] = this.d;
        v0_1[3] = this.a;
        return java.util.Arrays.hashCode(v0_1);
    }
}
