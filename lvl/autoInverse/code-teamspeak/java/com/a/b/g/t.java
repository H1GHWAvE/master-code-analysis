package com.a.b.g;
final class t extends com.a.b.g.a {
    final synthetic com.a.b.g.r a;
    private final java.util.zip.Checksum b;

    private t(com.a.b.g.r p2, java.util.zip.Checksum p3)
    {
        this.a = p2;
        this.b = ((java.util.zip.Checksum) com.a.b.b.cn.a(p3));
        return;
    }

    synthetic t(com.a.b.g.r p1, java.util.zip.Checksum p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    public final com.a.b.g.ag a()
    {
        com.a.b.g.ag v0_2;
        com.a.b.g.ag v0_1 = this.b.getValue();
        if (com.a.b.g.r.a(this.a) != 32) {
            v0_2 = com.a.b.g.ag.a(v0_1);
        } else {
            v0_2 = com.a.b.g.ag.a(((int) v0_1));
        }
        return v0_2;
    }

    protected final void a(byte p2)
    {
        this.b.update(p2);
        return;
    }

    protected final void a(byte[] p2, int p3, int p4)
    {
        this.b.update(p2, p3, p4);
        return;
    }
}
