package com.a.b.g;
final enum class z extends java.lang.Enum implements com.a.b.g.w {
    public static final enum com.a.b.g.z a;
    private static final synthetic com.a.b.g.z[] b;

    static z()
    {
        com.a.b.g.z.a = new com.a.b.g.z("INSTANCE");
        com.a.b.g.z[] v0_3 = new com.a.b.g.z[1];
        v0_3[0] = com.a.b.g.z.a;
        com.a.b.g.z.b = v0_3;
        return;
    }

    private z(String p2)
    {
        this(p2, 0);
        return;
    }

    private static void a(Integer p1, com.a.b.g.bn p2)
    {
        p2.b(p1.intValue());
        return;
    }

    public static com.a.b.g.z valueOf(String p1)
    {
        return ((com.a.b.g.z) Enum.valueOf(com.a.b.g.z, p1));
    }

    public static com.a.b.g.z[] values()
    {
        return ((com.a.b.g.z[]) com.a.b.g.z.b.clone());
    }

    public final synthetic void a(Object p2, com.a.b.g.bn p3)
    {
        p3.b(((Integer) p2).intValue());
        return;
    }

    public final String toString()
    {
        return "Funnels.integerFunnel()";
    }
}
