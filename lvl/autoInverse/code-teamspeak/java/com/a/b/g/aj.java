package com.a.b.g;
final class aj extends com.a.b.g.ag implements java.io.Serializable {
    private static final long b;
    final long a;

    aj(long p2)
    {
        this.a = p2;
        return;
    }

    public final int a()
    {
        return 64;
    }

    final void a(byte[] p6, int p7, int p8)
    {
        int v0 = 0;
        while (v0 < p8) {
            p6[(p7 + v0)] = ((byte) ((int) (this.a >> (v0 * 8))));
            v0++;
        }
        return;
    }

    final boolean a(com.a.b.g.ag p5)
    {
        int v0_2;
        if (this.a != p5.c()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final int b()
    {
        return ((int) this.a);
    }

    public final long c()
    {
        return this.a;
    }

    public final long d()
    {
        return this.a;
    }

    public final byte[] e()
    {
        byte[] v0 = new byte[8];
        v0[0] = ((byte) ((int) this.a));
        v0[1] = ((byte) ((int) (this.a >> 8)));
        v0[2] = ((byte) ((int) (this.a >> 16)));
        v0[3] = ((byte) ((int) (this.a >> 24)));
        v0[4] = ((byte) ((int) (this.a >> 32)));
        v0[5] = ((byte) ((int) (this.a >> 40)));
        v0[6] = ((byte) ((int) (this.a >> 48)));
        v0[7] = ((byte) ((int) (this.a >> 56)));
        return v0;
    }
}
