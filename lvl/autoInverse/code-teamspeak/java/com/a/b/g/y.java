package com.a.b.g;
final enum class y extends java.lang.Enum implements com.a.b.g.w {
    public static final enum com.a.b.g.y a;
    private static final synthetic com.a.b.g.y[] b;

    static y()
    {
        com.a.b.g.y.a = new com.a.b.g.y("INSTANCE");
        com.a.b.g.y[] v0_3 = new com.a.b.g.y[1];
        v0_3[0] = com.a.b.g.y.a;
        com.a.b.g.y.b = v0_3;
        return;
    }

    private y(String p2)
    {
        this(p2, 0);
        return;
    }

    private static void a(byte[] p0, com.a.b.g.bn p1)
    {
        p1.c(p0);
        return;
    }

    public static com.a.b.g.y valueOf(String p1)
    {
        return ((com.a.b.g.y) Enum.valueOf(com.a.b.g.y, p1));
    }

    public static com.a.b.g.y[] values()
    {
        return ((com.a.b.g.y[]) com.a.b.g.y.b.clone());
    }

    public final synthetic void a(Object p1, com.a.b.g.bn p2)
    {
        p2.c(((byte[]) p1));
        return;
    }

    public final String toString()
    {
        return "Funnels.byteArrayFunnel()";
    }
}
