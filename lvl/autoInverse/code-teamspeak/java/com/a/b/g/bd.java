package com.a.b.g;
public final class bd extends java.io.FilterInputStream {
    private final com.a.b.g.al a;

    private bd(com.a.b.g.ak p2, java.io.InputStream p3)
    {
        this(((java.io.InputStream) com.a.b.b.cn.a(p3)));
        this.a = ((com.a.b.g.al) com.a.b.b.cn.a(p2.a()));
        return;
    }

    private com.a.b.g.ag a()
    {
        return this.a.a();
    }

    public final void mark(int p1)
    {
        return;
    }

    public final boolean markSupported()
    {
        return 0;
    }

    public final int read()
    {
        int v0_1 = this.in.read();
        if (v0_1 != -1) {
            this.a.b(((byte) v0_1));
        }
        return v0_1;
    }

    public final int read(byte[] p3, int p4, int p5)
    {
        int v0_1 = this.in.read(p3, p4, p5);
        if (v0_1 != -1) {
            this.a.b(p3, p4, v0_1);
        }
        return v0_1;
    }

    public final void reset()
    {
        throw new java.io.IOException("reset not supported");
    }
}
