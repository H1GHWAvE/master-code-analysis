package com.a.b.g;
final enum class aa extends java.lang.Enum implements com.a.b.g.w {
    public static final enum com.a.b.g.aa a;
    private static final synthetic com.a.b.g.aa[] b;

    static aa()
    {
        com.a.b.g.aa.a = new com.a.b.g.aa("INSTANCE");
        com.a.b.g.aa[] v0_3 = new com.a.b.g.aa[1];
        v0_3[0] = com.a.b.g.aa.a;
        com.a.b.g.aa.b = v0_3;
        return;
    }

    private aa(String p2)
    {
        this(p2, 0);
        return;
    }

    private static void a(Long p2, com.a.b.g.bn p3)
    {
        p3.b(p2.longValue());
        return;
    }

    public static com.a.b.g.aa valueOf(String p1)
    {
        return ((com.a.b.g.aa) Enum.valueOf(com.a.b.g.aa, p1));
    }

    public static com.a.b.g.aa[] values()
    {
        return ((com.a.b.g.aa[]) com.a.b.g.aa.b.clone());
    }

    public final synthetic void a(Object p3, com.a.b.g.bn p4)
    {
        p4.b(((Long) p3).longValue());
        return;
    }

    public final String toString()
    {
        return "Funnels.longFunnel()";
    }
}
