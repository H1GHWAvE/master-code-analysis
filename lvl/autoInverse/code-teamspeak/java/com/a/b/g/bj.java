package com.a.b.g;
final class bj extends com.a.b.g.h implements java.io.Serializable {
    private static final long b;
    private final int a;

    bj(int p1)
    {
        this.a = p1;
        return;
    }

    public final com.a.b.g.al a()
    {
        return new com.a.b.g.bk(this.a);
    }

    public final int b()
    {
        return 128;
    }

    public final boolean equals(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof com.a.b.g.bj)) && (this.a == ((com.a.b.g.bj) p4).a)) {
            v0 = 1;
        }
        return v0;
    }

    public final int hashCode()
    {
        return (this.getClass().hashCode() ^ this.a);
    }

    public final String toString()
    {
        return new StringBuilder(32).append("Hashing.murmur3_128(").append(this.a).append(")").toString();
    }
}
