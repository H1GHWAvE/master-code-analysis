package com.a.b.g;
abstract enum class ap extends java.lang.Enum implements com.a.b.b.dz {
    public static final enum com.a.b.g.ap a;
    public static final enum com.a.b.g.ap b;
    private static final synthetic com.a.b.g.ap[] d;
    private final int c;

    static ap()
    {
        com.a.b.g.ap.a = new com.a.b.g.aq("CRC_32");
        com.a.b.g.ap.b = new com.a.b.g.ar("ADLER_32");
        com.a.b.g.ap[] v0_5 = new com.a.b.g.ap[2];
        v0_5[0] = com.a.b.g.ap.a;
        v0_5[1] = com.a.b.g.ap.b;
        com.a.b.g.ap.d = v0_5;
        return;
    }

    private ap(String p2, int p3)
    {
        this(p2, p3);
        this.c = 32;
        return;
    }

    synthetic ap(String p1, int p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    static synthetic int a(com.a.b.g.ap p1)
    {
        return p1.c;
    }

    public static com.a.b.g.ap valueOf(String p1)
    {
        return ((com.a.b.g.ap) Enum.valueOf(com.a.b.g.ap, p1));
    }

    public static com.a.b.g.ap[] values()
    {
        return ((com.a.b.g.ap[]) com.a.b.g.ap.d.clone());
    }

    public synthetic Object a()
    {
        return this.b();
    }

    public abstract java.util.zip.Checksum b();
}
