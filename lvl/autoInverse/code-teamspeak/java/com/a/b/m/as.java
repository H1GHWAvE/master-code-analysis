package com.a.b.m;
 class as extends com.a.b.m.an {
    private final com.a.b.m.an c;

    as(com.a.b.m.an p2)
    {
        this(0);
        this.c = p2;
        return;
    }

    final Class b(Object p2)
    {
        return this.c.b(p2);
    }

    Iterable c(Object p2)
    {
        return this.c.c(p2);
    }

    final Object d(Object p2)
    {
        return this.c.d(p2);
    }
}
