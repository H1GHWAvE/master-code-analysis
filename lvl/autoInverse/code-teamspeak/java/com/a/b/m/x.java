package com.a.b.m;
final class x extends com.a.b.m.ax {
    final synthetic java.util.Map a;
    final synthetic reflect.Type b;

    x(java.util.Map p1, reflect.Type p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    final void a(Class p5)
    {
        String v1_1 = String.valueOf(String.valueOf(p5));
        throw new IllegalArgumentException(new StringBuilder((v1_1.length() + 21)).append("No type mapping from ").append(v1_1).toString());
    }

    final void a(reflect.GenericArrayType p7)
    {
        java.util.Map v0_1;
        reflect.Type v3 = com.a.b.m.ay.c(this.b);
        if (v3 == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        reflect.Type v1_1 = new Object[1];
        v1_1[0] = this.b;
        com.a.b.b.cn.a(v0_1, "%s is not an array type.", v1_1);
        com.a.b.m.w.a(this.a, p7.getGenericComponentType(), v3);
        return;
    }

    final void a(reflect.ParameterizedType p9)
    {
        reflect.Type v1_4;
        int v3 = 0;
        java.util.Map v0_2 = ((reflect.ParameterizedType) com.a.b.m.w.a(reflect.ParameterizedType, this.b));
        reflect.Type v1_2 = p9.getRawType().equals(v0_2.getRawType());
        reflect.Type[] v5_0 = new Object[2];
        v5_0[0] = p9;
        v5_0[1] = this.b;
        com.a.b.b.cn.a(v1_2, "Inconsistent raw type: %s vs. %s", v5_0);
        reflect.Type[] v4_2 = p9.getActualTypeArguments();
        reflect.Type[] v5_1 = v0_2.getActualTypeArguments();
        if (v4_2.length != v5_1.length) {
            v1_4 = 0;
        } else {
            v1_4 = 1;
        }
        Object[] v7_1 = new Object[2];
        v7_1[0] = p9;
        v7_1[1] = v0_2;
        com.a.b.b.cn.a(v1_4, "%s not compatible with %s", v7_1);
        while (v3 < v4_2.length) {
            com.a.b.m.w.a(this.a, v4_2[v3], v5_1[v3]);
            v3++;
        }
        return;
    }

    final void a(reflect.TypeVariable p4)
    {
        this.a.put(new com.a.b.m.ab(p4), this.b);
        return;
    }

    final void a(reflect.WildcardType p11)
    {
        java.util.Map v0_5;
        int v2 = 0;
        java.util.Map v0_2 = ((reflect.WildcardType) com.a.b.m.w.a(reflect.WildcardType, this.b));
        reflect.Type v3_1 = p11.getUpperBounds();
        reflect.Type[] v4 = v0_2.getUpperBounds();
        reflect.Type[] v5 = p11.getLowerBounds();
        reflect.Type[] v6 = v0_2.getLowerBounds();
        if ((v3_1.length != v4.length) || (v5.length != v6.length)) {
            v0_5 = 0;
        } else {
            v0_5 = 1;
        }
        reflect.Type v8_1 = new Object[2];
        v8_1[0] = p11;
        v8_1[1] = this.b;
        com.a.b.b.cn.a(v0_5, "Incompatible type: %s vs. %s", v8_1);
        java.util.Map v0_6 = 0;
        while (v0_6 < v3_1.length) {
            com.a.b.m.w.a(this.a, v3_1[v0_6], v4[v0_6]);
            v0_6++;
        }
        while (v2 < v5.length) {
            com.a.b.m.w.a(this.a, v5[v2], v6[v2]);
            v2++;
        }
        return;
    }
}
