package com.a.b.m;
abstract class ax {
    private final java.util.Set a;

    ax()
    {
        this.a = new java.util.HashSet();
        return;
    }

    void a(Class p1)
    {
        return;
    }

    void a(reflect.GenericArrayType p1)
    {
        return;
    }

    void a(reflect.ParameterizedType p1)
    {
        return;
    }

    void a(reflect.TypeVariable p1)
    {
        return;
    }

    void a(reflect.WildcardType p1)
    {
        return;
    }

    public final varargs void a(reflect.Type[] p7)
    {
        StringBuilder v4_0 = p7.length;
        String v3_0 = 0;
        while (v3_0 < v4_0) {
            reflect.Type v2 = p7[v3_0];
            try {
                if ((v2 != null) && (this.a.add(v2))) {
                    if (!(v2 instanceof reflect.TypeVariable)) {
                        if (!(v2 instanceof reflect.WildcardType)) {
                            if (!(v2 instanceof reflect.ParameterizedType)) {
                                if (!(v2 instanceof Class)) {
                                    if (!(v2 instanceof reflect.GenericArrayType)) {
                                        String v3_2 = String.valueOf(String.valueOf(v2));
                                        throw new AssertionError(new StringBuilder((v3_2.length() + 14)).append("Unknown type: ").append(v3_2).toString());
                                    } else {
                                        this.a(((reflect.GenericArrayType) v2));
                                    }
                                } else {
                                    this.a(((Class) v2));
                                }
                            } else {
                                this.a(((reflect.ParameterizedType) v2));
                            }
                        } else {
                            this.a(((reflect.WildcardType) v2));
                        }
                    } else {
                        this.a(((reflect.TypeVariable) v2));
                    }
                }
                v3_0++;
            } catch (AssertionError v1_16) {
                this.a.remove(v2);
                throw v1_16;
            }
        }
        return;
    }
}
