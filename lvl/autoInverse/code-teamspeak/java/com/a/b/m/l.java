package com.a.b.m;
 class l extends com.a.b.m.k {
    final reflect.Constructor a;

    l(reflect.Constructor p1)
    {
        this(p1);
        this.a = p1;
        return;
    }

    private boolean h()
    {
        int v0 = 1;
        boolean v2_1 = this.a.getDeclaringClass();
        if (v2_1.getEnclosingConstructor() == null) {
            Class v3_1 = v2_1.getEnclosingMethod();
            if (v3_1 == null) {
                if ((v2_1.getEnclosingClass() == null) || (reflect.Modifier.isStatic(v2_1.getModifiers()))) {
                    v0 = 0;
                }
            } else {
                if (reflect.Modifier.isStatic(v3_1.getModifiers())) {
                    v0 = 0;
                }
            }
        }
        return v0;
    }

    final Object a(Object p6, Object[] p7)
    {
        try {
            return this.a.newInstance(p7);
        } catch (InstantiationException v0_2) {
            String v2_2 = String.valueOf(String.valueOf(this.a));
            throw new RuntimeException(new StringBuilder((v2_2.length() + 8)).append(v2_2).append(" failed.").toString(), v0_2);
        }
    }

    public final boolean b()
    {
        return 0;
    }

    public final boolean c()
    {
        return this.a.isVarArgs();
    }

    reflect.Type[] d()
    {
        reflect.Type[] v0_1 = this.a.getGenericParameterTypes();
        if (v0_1.length > 0) {
            int v1_5;
            int v1_2 = this.a.getDeclaringClass();
            if (v1_2.getEnclosingConstructor() == null) {
                int v4_1 = v1_2.getEnclosingMethod();
                if (v4_1 == 0) {
                    if ((v1_2.getEnclosingClass() == null) || (reflect.Modifier.isStatic(v1_2.getModifiers()))) {
                        v1_5 = 0;
                    } else {
                        v1_5 = 1;
                    }
                } else {
                    if (reflect.Modifier.isStatic(v4_1.getModifiers())) {
                        v1_5 = 0;
                    } else {
                        v1_5 = 1;
                    }
                }
            } else {
                v1_5 = 1;
            }
            if (v1_5 != 0) {
                int v1_9 = this.a.getParameterTypes();
                if ((v0_1.length == v1_9.length) && (v1_9[0] == this.getDeclaringClass().getEnclosingClass())) {
                    v0_1 = ((reflect.Type[]) java.util.Arrays.copyOfRange(v0_1, 1, v0_1.length));
                }
            }
        }
        return v0_1;
    }

    reflect.Type[] e()
    {
        return this.a.getGenericExceptionTypes();
    }

    final otation.Annotation[][] f()
    {
        return this.a.getParameterAnnotations();
    }

    reflect.Type g()
    {
        reflect.ParameterizedType v0 = this.getDeclaringClass();
        reflect.TypeVariable[] v1 = v0.getTypeParameters();
        if (v1.length > 0) {
            v0 = com.a.b.m.ay.a(v0, v1);
        }
        return v0;
    }

    public final reflect.TypeVariable[] getTypeParameters()
    {
        int v0_1 = this.getDeclaringClass().getTypeParameters();
        reflect.TypeVariable[] v1_1 = this.a.getTypeParameters();
        reflect.TypeVariable[] v2_2 = new reflect.TypeVariable[(v0_1.length + v1_1.length)];
        System.arraycopy(v0_1, 0, v2_2, 0, v0_1.length);
        System.arraycopy(v1_1, 0, v2_2, v0_1.length, v1_1.length);
        return v2_2;
    }
}
