package com.a.b.m;
final enum class bk extends com.a.b.m.bh {

    bk(String p3)
    {
        this(p3, 2, 0);
        return;
    }

    final reflect.Type a(reflect.Type p2)
    {
        return com.a.b.m.bk.b.a(p2);
    }

    final reflect.Type b(reflect.Type p2)
    {
        return com.a.b.m.bk.b.b(p2);
    }

    final String c(reflect.Type p4)
    {
        try {
            Class[] v2_1 = new Class[0];
            RuntimeException v1_2 = new Object[0];
            return ((String) reflect.Type.getMethod("getTypeName", v2_1).invoke(p4, v1_2));
        } catch (IllegalAccessException v0_5) {
            throw new RuntimeException(v0_5);
        } catch (IllegalAccessException v0) {
            throw new AssertionError("Type.getTypeName should be available in Java 8");
        } catch (IllegalAccessException v0_4) {
            throw new RuntimeException(v0_4);
        }
    }
}
