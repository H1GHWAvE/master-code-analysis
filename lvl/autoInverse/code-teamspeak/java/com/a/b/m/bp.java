package com.a.b.m;
final class bp implements java.io.Serializable, java.lang.reflect.WildcardType {
    private static final long c;
    private final com.a.b.d.jl a;
    private final com.a.b.d.jl b;

    bp(reflect.Type[] p2, reflect.Type[] p3)
    {
        com.a.b.m.ay.a(p2, "lower bound for wildcard");
        com.a.b.m.ay.a(p3, "upper bound for wildcard");
        this.a = com.a.b.m.bh.d.a(p2);
        this.b = com.a.b.m.bh.d.a(p3);
        return;
    }

    public final boolean equals(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof reflect.WildcardType)) && ((this.a.equals(java.util.Arrays.asList(((reflect.WildcardType) p4).getLowerBounds()))) && (this.b.equals(java.util.Arrays.asList(((reflect.WildcardType) p4).getUpperBounds()))))) {
            v0 = 1;
        }
        return v0;
    }

    public final reflect.Type[] getLowerBounds()
    {
        return com.a.b.m.ay.a(this.a);
    }

    public final reflect.Type[] getUpperBounds()
    {
        return com.a.b.m.ay.a(this.b);
    }

    public final int hashCode()
    {
        return (this.a.hashCode() ^ this.b.hashCode());
    }

    public final String toString()
    {
        StringBuilder v1_1 = new StringBuilder("?");
        java.util.Iterator v2_0 = this.a.iterator();
        while (v2_0.hasNext()) {
            v1_1.append(" super ").append(com.a.b.m.bh.d.c(((reflect.Type) v2_0.next())));
        }
        java.util.Iterator v2_1 = com.a.b.m.ay.a(this.b).iterator();
        while (v2_1.hasNext()) {
            v1_1.append(" extends ").append(com.a.b.m.bh.d.c(((reflect.Type) v2_1.next())));
        }
        return v1_1.toString();
    }
}
