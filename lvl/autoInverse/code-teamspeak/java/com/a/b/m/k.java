package com.a.b.m;
public abstract class k extends com.a.b.m.g implements java.lang.reflect.GenericDeclaration {

    k(reflect.AccessibleObject p1)
    {
        this(p1);
        return;
    }

    private com.a.b.m.k a(com.a.b.m.ae p7)
    {
        if (p7.a(com.a.b.m.ae.a(this.g()))) {
            return this;
        } else {
            String v1_3 = String.valueOf(String.valueOf(com.a.b.m.ae.a(this.g())));
            String v2_1 = String.valueOf(String.valueOf(p7));
            throw new IllegalArgumentException(new StringBuilder(((v1_3.length() + 35) + v2_1.length())).append("Invokable is known to return ").append(v1_3).append(", not ").append(v2_1).toString());
        }
    }

    private com.a.b.m.k a(Class p7)
    {
        String v0_0 = com.a.b.m.ae.a(p7);
        if (v0_0.a(com.a.b.m.ae.a(this.g()))) {
            return this;
        } else {
            StringBuilder v2_3 = String.valueOf(String.valueOf(com.a.b.m.ae.a(this.g())));
            String v0_2 = String.valueOf(String.valueOf(v0_0));
            throw new IllegalArgumentException(new StringBuilder(((v2_3.length() + 35) + v0_2.length())).append("Invokable is known to return ").append(v2_3).append(", not ").append(v0_2).toString());
        }
    }

    private static com.a.b.m.k a(reflect.Constructor p1)
    {
        return new com.a.b.m.l(p1);
    }

    private static com.a.b.m.k a(reflect.Method p1)
    {
        return new com.a.b.m.m(p1);
    }

    private varargs Object b(Object p2, Object[] p3)
    {
        return this.a(p2, ((Object[]) com.a.b.b.cn.a(p3)));
    }

    private com.a.b.m.ae h()
    {
        return com.a.b.m.ae.a(this.g());
    }

    private com.a.b.d.jl i()
    {
        reflect.Type[] v1 = this.d();
        otation.Annotation[][] v2 = this.f();
        com.a.b.d.jn v3 = com.a.b.d.jl.h();
        com.a.b.d.jl v0_0 = 0;
        while (v0_0 < v1.length) {
            v3.c(new com.a.b.m.s(this, v0_0, com.a.b.m.ae.a(v1[v0_0]), v2[v0_0]));
            v0_0++;
        }
        return v3.b();
    }

    private com.a.b.d.jl j()
    {
        com.a.b.d.jn v1 = com.a.b.d.jl.h();
        reflect.Type[] v2 = this.e();
        int v3 = v2.length;
        com.a.b.d.jl v0_0 = 0;
        while (v0_0 < v3) {
            v1.c(com.a.b.m.ae.a(v2[v0_0]));
            v0_0++;
        }
        return v1.b();
    }

    public com.a.b.m.ae a()
    {
        return com.a.b.m.ae.a(this.getDeclaringClass());
    }

    abstract Object a();

    public abstract boolean b();

    public abstract boolean c();

    abstract reflect.Type[] d();

    abstract reflect.Type[] e();

    public bridge synthetic boolean equals(Object p2)
    {
        return super.equals(p2);
    }

    abstract otation.Annotation[][] f();

    abstract reflect.Type g();

    public final Class getDeclaringClass()
    {
        return super.getDeclaringClass();
    }

    public bridge synthetic int hashCode()
    {
        return super.hashCode();
    }

    public bridge synthetic String toString()
    {
        return super.toString();
    }
}
