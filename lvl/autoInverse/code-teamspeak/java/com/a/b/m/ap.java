package com.a.b.m;
final class ap extends com.a.b.m.an {

    ap()
    {
        this(0);
        return;
    }

    private static Class a(Class p0)
    {
        return p0;
    }

    private static Iterable b(Class p1)
    {
        return java.util.Arrays.asList(p1.getInterfaces());
    }

    private static Class c(Class p1)
    {
        return p1.getSuperclass();
    }

    final bridge synthetic Class b(Object p1)
    {
        return ((Class) p1);
    }

    final synthetic Iterable c(Object p2)
    {
        return java.util.Arrays.asList(((Class) p2).getInterfaces());
    }

    final synthetic Object d(Object p2)
    {
        return ((Class) p2).getSuperclass();
    }
}
