package com.a.b.m;
public final class b {
    private static final java.util.logging.Logger a = None;
    private static final com.a.b.b.co b = None;
    private static final com.a.b.b.di c = None;
    private static final String d = ".class";
    private final com.a.b.d.lo e;

    static b()
    {
        com.a.b.m.b.a = java.util.logging.Logger.getLogger(com.a.b.m.b.getName());
        com.a.b.m.b.b = new com.a.b.m.c();
        com.a.b.m.b.c = com.a.b.b.di.a(" ").a();
        return;
    }

    private b(com.a.b.d.lo p1)
    {
        this.e = p1;
        return;
    }

    private static com.a.b.m.b a(ClassLoader p4)
    {
        com.a.b.m.f v2_1 = new com.a.b.m.f();
        java.util.Iterator v3 = com.a.b.m.b.b(p4).e().iterator();
        while (v3.hasNext()) {
            com.a.b.m.b v0_6 = ((java.util.Map$Entry) v3.next());
            v2_1.a(((java.net.URI) v0_6.getKey()), ((ClassLoader) v0_6.getValue()));
        }
        return new com.a.b.m.b(v2_1.a.c());
    }

    static String a(String p3)
    {
        return p3.substring(0, (p3.length() - 6)).replace(47, 46);
    }

    static synthetic java.util.logging.Logger a()
    {
        return com.a.b.m.b.a;
    }

    static synthetic com.a.b.b.di b()
    {
        return com.a.b.m.b.c;
    }

    private static com.a.b.d.jt b(ClassLoader p6)
    {
        IllegalArgumentException v1_0 = com.a.b.d.sz.d();
        int v0_0 = p6.getParent();
        if (v0_0 != 0) {
            v1_0.putAll(com.a.b.m.b.b(v0_0));
        }
        if ((p6 instanceof java.net.URLClassLoader)) {
            java.net.URL[] v2 = ((java.net.URLClassLoader) p6).getURLs();
            int v3 = v2.length;
            int v0_5 = 0;
            while (v0_5 < v3) {
                try {
                    java.net.URI v4_1 = v2[v0_5].toURI();
                } catch (int v0_7) {
                    throw new IllegalArgumentException(v0_7);
                }
                if (!v1_0.containsKey(v4_1)) {
                    v1_0.put(v4_1, p6);
                }
                v0_5++;
            }
        }
        return com.a.b.d.jt.a(v1_0);
    }

    private com.a.b.d.lo b(String p5)
    {
        com.a.b.b.cn.a(p5);
        com.a.b.d.lp v1 = com.a.b.d.lo.i();
        java.util.Iterator v2 = this.e().iterator();
        while (v2.hasNext()) {
            com.a.b.d.lo v0_4 = ((com.a.b.m.d) v2.next());
            if (com.a.b.m.t.a(v0_4.a).equals(p5)) {
                v1.c(v0_4);
            }
        }
        return v1.b();
    }

    private com.a.b.d.lo c()
    {
        return this.e;
    }

    private com.a.b.d.lo c(String p6)
    {
        com.a.b.b.cn.a(p6);
        com.a.b.d.lo v0_1 = String.valueOf(String.valueOf(p6));
        String v1_3 = new StringBuilder((v0_1.length() + 1)).append(v0_1).append(".").toString();
        com.a.b.d.lp v2_2 = com.a.b.d.lo.i();
        java.util.Iterator v3 = this.e().iterator();
        while (v3.hasNext()) {
            com.a.b.d.lo v0_8 = ((com.a.b.m.d) v3.next());
            if (v0_8.a.startsWith(v1_3)) {
                v2_2.c(v0_8);
            }
        }
        return v2_2.b();
    }

    private com.a.b.d.lo d()
    {
        return com.a.b.d.lo.a(com.a.b.d.gd.a(this.e).a(com.a.b.m.d).c);
    }

    private com.a.b.d.lo e()
    {
        return com.a.b.d.lo.a(com.a.b.d.gd.a(this.e).a(com.a.b.m.d).a(com.a.b.m.b.b).c);
    }
}
