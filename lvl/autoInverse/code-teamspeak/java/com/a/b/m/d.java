package com.a.b.m;
public final class d extends com.a.b.m.e {
    final String a;

    d(String p2, ClassLoader p3)
    {
        this(p2, p3);
        this.a = com.a.b.m.b.a(p2);
        return;
    }

    private String a()
    {
        return com.a.b.m.t.a(this.a);
    }

    static synthetic String a(com.a.b.m.d p1)
    {
        return p1.a;
    }

    private String b()
    {
        String v0_6;
        String v0_1 = this.a.lastIndexOf(36);
        if (v0_1 == -1) {
            String v0_3 = com.a.b.m.t.a(this.a);
            if (!v0_3.isEmpty()) {
                v0_6 = this.a.substring((v0_3.length() + 1));
            } else {
                v0_6 = this.a;
            }
        } else {
            v0_6 = com.a.b.b.m.c.j(this.a.substring((v0_1 + 1)));
        }
        return v0_6;
    }

    private String c()
    {
        return this.a;
    }

    private Class d()
    {
        try {
            return this.b.loadClass(this.a);
        } catch (ClassNotFoundException v0_2) {
            throw new IllegalStateException(v0_2);
        }
    }

    public final String toString()
    {
        return this.a;
    }
}
