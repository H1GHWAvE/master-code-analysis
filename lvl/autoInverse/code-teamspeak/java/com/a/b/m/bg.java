package com.a.b.m;
final class bg implements java.io.Serializable, java.lang.reflect.GenericArrayType {
    private static final long b;
    private final reflect.Type a;

    bg(reflect.Type p2)
    {
        this.a = com.a.b.m.bh.d.b(p2);
        return;
    }

    public final boolean equals(Object p3)
    {
        int v0_1;
        if (!(p3 instanceof reflect.GenericArrayType)) {
            v0_1 = 0;
        } else {
            v0_1 = com.a.b.b.ce.a(this.getGenericComponentType(), ((reflect.GenericArrayType) p3).getGenericComponentType());
        }
        return v0_1;
    }

    public final reflect.Type getGenericComponentType()
    {
        return this.a;
    }

    public final int hashCode()
    {
        return this.a.hashCode();
    }

    public final String toString()
    {
        return String.valueOf(com.a.b.m.ay.b(this.a)).concat("[]");
    }
}
