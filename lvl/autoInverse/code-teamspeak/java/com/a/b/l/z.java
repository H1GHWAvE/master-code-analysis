package com.a.b.l;
public final class z {
    private static final java.util.Map a;
    private static final java.util.Map b;

    static z()
    {
        java.util.Map v0_1 = new java.util.HashMap(16);
        java.util.HashMap v1_1 = new java.util.HashMap(16);
        com.a.b.l.z.a(v0_1, v1_1, Boolean.TYPE, Boolean);
        com.a.b.l.z.a(v0_1, v1_1, Byte.TYPE, Byte);
        com.a.b.l.z.a(v0_1, v1_1, Character.TYPE, Character);
        com.a.b.l.z.a(v0_1, v1_1, Double.TYPE, Double);
        com.a.b.l.z.a(v0_1, v1_1, Float.TYPE, Float);
        com.a.b.l.z.a(v0_1, v1_1, Integer.TYPE, Integer);
        com.a.b.l.z.a(v0_1, v1_1, Long.TYPE, Long);
        com.a.b.l.z.a(v0_1, v1_1, Short.TYPE, Short);
        com.a.b.l.z.a(v0_1, v1_1, Void.TYPE, Void);
        com.a.b.l.z.a = java.util.Collections.unmodifiableMap(v0_1);
        com.a.b.l.z.b = java.util.Collections.unmodifiableMap(v1_1);
        return;
    }

    private z()
    {
        return;
    }

    public static Class a(Class p1)
    {
        com.a.b.b.cn.a(p1);
        Class v0_2 = ((Class) com.a.b.l.z.a.get(p1));
        if (v0_2 != null) {
            p1 = v0_2;
        }
        return p1;
    }

    public static java.util.Set a()
    {
        return com.a.b.l.z.b.keySet();
    }

    private static void a(java.util.Map p0, java.util.Map p1, Class p2, Class p3)
    {
        p0.put(p2, p3);
        p1.put(p3, p2);
        return;
    }

    public static Class b(Class p1)
    {
        com.a.b.b.cn.a(p1);
        Class v0_2 = ((Class) com.a.b.l.z.b.get(p1));
        if (v0_2 != null) {
            p1 = v0_2;
        }
        return p1;
    }

    private static java.util.Set b()
    {
        return com.a.b.l.z.a.keySet();
    }

    private static boolean c(Class p2)
    {
        return com.a.b.l.z.b.containsKey(com.a.b.b.cn.a(p2));
    }
}
