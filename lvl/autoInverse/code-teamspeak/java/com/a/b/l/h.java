package com.a.b.l;
final enum class h extends java.lang.Enum implements java.util.Comparator {
    public static final enum com.a.b.l.h a;
    private static final synthetic com.a.b.l.h[] b;

    static h()
    {
        com.a.b.l.h.a = new com.a.b.l.h("INSTANCE");
        com.a.b.l.h[] v0_3 = new com.a.b.l.h[1];
        v0_3[0] = com.a.b.l.h.a;
        com.a.b.l.h.b = v0_3;
        return;
    }

    private h(String p2)
    {
        this(p2, 0);
        return;
    }

    private static int a(char[] p4, char[] p5)
    {
        int v2 = Math.min(p4.length, p5.length);
        int v1_1 = 0;
        while (v1_1 < v2) {
            int v0_3 = (p4[v1_1] - p5[v1_1]);
            if (v0_3 == 0) {
                v1_1++;
            }
            return v0_3;
        }
        v0_3 = (p4.length - p5.length);
        return v0_3;
    }

    public static com.a.b.l.h valueOf(String p1)
    {
        return ((com.a.b.l.h) Enum.valueOf(com.a.b.l.h, p1));
    }

    public static com.a.b.l.h[] values()
    {
        return ((com.a.b.l.h[]) com.a.b.l.h.b.clone());
    }

    public final synthetic int compare(Object p5, Object p6)
    {
        int v2 = Math.min(((char[]) p5).length, ((char[]) p6).length);
        int v1_1 = 0;
        while (v1_1 < v2) {
            int v0_3 = (((char[]) p5)[v1_1] - ((char[]) p6)[v1_1]);
            if (v0_3 == 0) {
                v1_1++;
            }
            return v0_3;
        }
        v0_3 = (((char[]) p5).length - ((char[]) p6).length);
        return v0_3;
    }
}
