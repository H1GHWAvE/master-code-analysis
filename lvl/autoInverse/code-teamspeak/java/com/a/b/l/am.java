package com.a.b.l;
public final class am {
    static final long a = 4294967295;

    private am()
    {
        return;
    }

    public static int a(int p2, int p3)
    {
        return com.a.b.l.q.a((p2 ^ -2147483648), (-2147483648 ^ p3));
    }

    private static int a(String p5)
    {
        String v0_0 = com.a.b.l.y.a(p5);
        try {
            return com.a.b.l.am.a(v0_0.a, v0_0.b);
        } catch (String v0_3) {
            NumberFormatException v1_1 = v0_3;
            String v0_4 = String.valueOf(p5);
            if (v0_4.length() == 0) {
                String v0_6 = new String("Error parsing value: ");
            } else {
                v0_6 = "Error parsing value: ".concat(v0_4);
            }
            NumberFormatException v2_1 = new NumberFormatException(v0_6);
            v2_1.initCause(v1_1);
            throw v2_1;
        }
    }

    public static int a(String p4, int p5)
    {
        com.a.b.b.cn.a(p4);
        int v0_0 = Long.parseLong(p4, p5);
        if ((2.1219957905e-314 & v0_0) == v0_0) {
            return ((int) v0_0);
        } else {
            String v1_1 = String.valueOf(String.valueOf(p4));
            throw new NumberFormatException(new StringBuilder((v1_1.length() + 69)).append("Input ").append(v1_1).append(" in base ").append(p5).append(" is not in the range of an unsigned integer").toString());
        }
    }

    private static varargs int a(int[] p4)
    {
        int v0_1;
        int v1 = 1;
        if (p4.length <= 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.a(v0_1);
        int v0_3 = (p4[0] ^ -2147483648);
        while (v1 < p4.length) {
            int v2_3 = (p4[v1] ^ -2147483648);
            if (v2_3 < v0_3) {
                v0_3 = v2_3;
            }
            v1++;
        }
        return (v0_3 ^ -2147483648);
    }

    public static String a(int p4)
    {
        return Long.toString((((long) p4) & 2.1219957905e-314), 10);
    }

    private static varargs String a(String p4, int[] p5)
    {
        String v0_7;
        com.a.b.b.cn.a(p4);
        if (p5.length != 0) {
            StringBuilder v1_1 = new StringBuilder((p5.length * 5));
            v1_1.append(com.a.b.l.am.a(p5[0]));
            String v0_6 = 1;
            while (v0_6 < p5.length) {
                v1_1.append(p4).append(com.a.b.l.am.a(p5[v0_6]));
                v0_6++;
            }
            v0_7 = v1_1.toString();
        } else {
            v0_7 = "";
        }
        return v0_7;
    }

    private static java.util.Comparator a()
    {
        return com.a.b.l.an.a;
    }

    private static int b(int p1)
    {
        return (-2147483648 ^ p1);
    }

    private static int b(int p6, int p7)
    {
        return ((int) ((((long) p6) & 2.1219957905e-314) / (((long) p7) & 2.1219957905e-314)));
    }

    private static int b(String p1)
    {
        return com.a.b.l.am.a(p1, 10);
    }

    private static varargs int b(int[] p4)
    {
        int v0_1;
        int v1 = 1;
        if (p4.length <= 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.a(v0_1);
        int v0_3 = (p4[0] ^ -2147483648);
        while (v1 < p4.length) {
            int v2_3 = (p4[v1] ^ -2147483648);
            if (v2_3 > v0_3) {
                v0_3 = v2_3;
            }
            v1++;
        }
        return (v0_3 ^ -2147483648);
    }

    private static int c(int p6, int p7)
    {
        return ((int) ((((long) p6) & 2.1219957905e-314) % (((long) p7) & 2.1219957905e-314)));
    }

    private static long c(int p4)
    {
        return (((long) p4) & 2.1219957905e-314);
    }

    private static String d(int p1)
    {
        return com.a.b.l.am.a(p1);
    }
}
