package com.a.b.l;
final enum class af extends java.lang.Enum implements java.util.Comparator {
    public static final enum com.a.b.l.af a;
    private static final synthetic com.a.b.l.af[] b;

    static af()
    {
        com.a.b.l.af.a = new com.a.b.l.af("INSTANCE");
        com.a.b.l.af[] v0_3 = new com.a.b.l.af[1];
        v0_3[0] = com.a.b.l.af.a;
        com.a.b.l.af.b = v0_3;
        return;
    }

    private af(String p2)
    {
        this(p2, 0);
        return;
    }

    private static int a(byte[] p4, byte[] p5)
    {
        int v2 = Math.min(p4.length, p5.length);
        int v1_1 = 0;
        while (v1_1 < v2) {
            int v0_3 = (p4[v1_1] - p5[v1_1]);
            if (v0_3 == 0) {
                v1_1++;
            }
            return v0_3;
        }
        v0_3 = (p4.length - p5.length);
        return v0_3;
    }

    public static com.a.b.l.af valueOf(String p1)
    {
        return ((com.a.b.l.af) Enum.valueOf(com.a.b.l.af, p1));
    }

    public static com.a.b.l.af[] values()
    {
        return ((com.a.b.l.af[]) com.a.b.l.af.b.clone());
    }

    public final synthetic int compare(Object p5, Object p6)
    {
        int v2 = Math.min(((byte[]) p5).length, ((byte[]) p6).length);
        int v1_1 = 0;
        while (v1_1 < v2) {
            int v0_3 = (((byte[]) p5)[v1_1] - ((byte[]) p6)[v1_1]);
            if (v0_3 == 0) {
                v1_1++;
            }
            return v0_3;
        }
        v0_3 = (((byte[]) p5).length - ((byte[]) p6).length);
        return v0_3;
    }
}
