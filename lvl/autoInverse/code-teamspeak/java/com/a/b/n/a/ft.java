package com.a.b.n.a;
final class ft implements java.util.concurrent.Callable {
    final synthetic reflect.Method a;
    final synthetic Object[] b;
    final synthetic com.a.b.n.a.fs c;

    ft(com.a.b.n.a.fs p1, reflect.Method p2, Object[] p3)
    {
        this.c = p1;
        this.a = p2;
        this.b = p3;
        return;
    }

    public final Object call()
    {
        try {
            return this.a.invoke(this.c.a, this.b);
        } catch (AssertionError v0_2) {
            com.a.b.n.a.fr.a(v0_2, 0);
            throw new AssertionError("can\'t get here");
        }
    }
}
