package com.a.b.n.a;
final class fx extends com.a.b.n.a.fu {
    private final long d;
    private double e;
    private double f;

    fx(com.a.b.n.a.em p3, long p4, java.util.concurrent.TimeUnit p6)
    {
        this(p3, 0);
        this.d = p6.toMicros(p4);
        return;
    }

    private double a(double p6)
    {
        return (this.c + (this.e * p6));
    }

    final void a(double p10, double p12)
    {
        double v0_0 = 0;
        double v2 = this.b;
        this.b = (((double) this.d) / p12);
        this.f = (this.b / 2.0);
        this.e = (((3.0 * p12) - p12) / this.f);
        if (v2 != inf) {
            if (v2 != 0) {
                v0_0 = ((this.a * this.b) / v2);
            } else {
                v0_0 = this.b;
            }
        }
        this.a = v0_0;
        return;
    }

    final long b(double p8, double p10)
    {
        double v2_0 = (p8 - this.f);
        long v0_1 = 0;
        if (v2_0 > 0) {
            double v4_2 = Math.min(v2_0, p10);
            v0_1 = ((long) (((this.a(v2_0) + this.a((v2_0 - v4_2))) * v4_2) / 2.0));
            p10 -= v4_2;
        }
        return ((long) (((double) v0_1) + (this.c * p10)));
    }
}
