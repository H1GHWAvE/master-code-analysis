package com.a.b.n.a;
abstract class gt implements java.util.concurrent.ExecutorService {
    private final java.util.concurrent.ExecutorService a;

    protected gt(java.util.concurrent.ExecutorService p2)
    {
        this.a = ((java.util.concurrent.ExecutorService) com.a.b.b.cn.a(p2));
        return;
    }

    private final com.a.b.d.jl a(java.util.Collection p4)
    {
        com.a.b.d.jn v1 = com.a.b.d.jl.h();
        java.util.Iterator v2 = p4.iterator();
        while (v2.hasNext()) {
            v1.c(this.a(((java.util.concurrent.Callable) v2.next())));
        }
        return v1.b();
    }

    protected Runnable a(Runnable p3)
    {
        return new com.a.b.n.a.gu(this, this.a(java.util.concurrent.Executors.callable(p3, 0)));
    }

    protected abstract java.util.concurrent.Callable a();

    public final boolean awaitTermination(long p2, java.util.concurrent.TimeUnit p4)
    {
        return this.a.awaitTermination(p2, p4);
    }

    public final void execute(Runnable p3)
    {
        this.a.execute(this.a(p3));
        return;
    }

    public final java.util.List invokeAll(java.util.Collection p3)
    {
        return this.a.invokeAll(this.a(p3));
    }

    public final java.util.List invokeAll(java.util.Collection p3, long p4, java.util.concurrent.TimeUnit p6)
    {
        return this.a.invokeAll(this.a(p3), p4, p6);
    }

    public final Object invokeAny(java.util.Collection p3)
    {
        return this.a.invokeAny(this.a(p3));
    }

    public final Object invokeAny(java.util.Collection p3, long p4, java.util.concurrent.TimeUnit p6)
    {
        return this.a.invokeAny(this.a(p3), p4, p6);
    }

    public final boolean isShutdown()
    {
        return this.a.isShutdown();
    }

    public final boolean isTerminated()
    {
        return this.a.isTerminated();
    }

    public final void shutdown()
    {
        this.a.shutdown();
        return;
    }

    public final java.util.List shutdownNow()
    {
        return this.a.shutdownNow();
    }

    public final java.util.concurrent.Future submit(Runnable p3)
    {
        return this.a.submit(this.a(p3));
    }

    public final java.util.concurrent.Future submit(Runnable p3, Object p4)
    {
        return this.a.submit(this.a(p3), p4);
    }

    public final java.util.concurrent.Future submit(java.util.concurrent.Callable p3)
    {
        return this.a.submit(this.a(((java.util.concurrent.Callable) com.a.b.b.cn.a(p3))));
    }
}
