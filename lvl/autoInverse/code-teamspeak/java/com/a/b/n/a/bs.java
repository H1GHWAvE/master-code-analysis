package com.a.b.n.a;
public final class bs extends com.a.b.n.a.bd {
    private final java.util.Map b;

    bs(com.a.b.n.a.bq p2, java.util.Map p3)
    {
        this(p2, 0);
        this.b = p3;
        return;
    }

    private java.util.concurrent.locks.ReentrantLock a(Enum p4)
    {
        com.a.b.n.a.bg v0_4;
        if (this.a != com.a.b.n.a.bm.c) {
            v0_4 = new com.a.b.n.a.bg(this, ((com.a.b.n.a.bl) this.b.get(p4)), 0);
        } else {
            v0_4 = new java.util.concurrent.locks.ReentrantLock(0);
        }
        return v0_4;
    }

    private java.util.concurrent.locks.ReentrantLock b(Enum p4)
    {
        com.a.b.n.a.bg v0_4;
        if (this.a != com.a.b.n.a.bm.c) {
            v0_4 = new com.a.b.n.a.bg(this, ((com.a.b.n.a.bl) this.b.get(p4)), 0);
        } else {
            v0_4 = new java.util.concurrent.locks.ReentrantLock(0);
        }
        return v0_4;
    }

    private java.util.concurrent.locks.ReentrantReadWriteLock c(Enum p4)
    {
        com.a.b.n.a.bi v0_4;
        if (this.a != com.a.b.n.a.bm.c) {
            v0_4 = new com.a.b.n.a.bi(this, ((com.a.b.n.a.bl) this.b.get(p4)), 0);
        } else {
            v0_4 = new java.util.concurrent.locks.ReentrantReadWriteLock(0);
        }
        return v0_4;
    }

    private java.util.concurrent.locks.ReentrantReadWriteLock d(Enum p4)
    {
        com.a.b.n.a.bi v0_4;
        if (this.a != com.a.b.n.a.bm.c) {
            v0_4 = new com.a.b.n.a.bi(this, ((com.a.b.n.a.bl) this.b.get(p4)), 0);
        } else {
            v0_4 = new java.util.concurrent.locks.ReentrantReadWriteLock(0);
        }
        return v0_4;
    }
}
