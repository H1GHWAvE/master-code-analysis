package com.a.b.n.a;
final class gp implements java.lang.Thread$UncaughtExceptionHandler {
    private static final java.util.logging.Logger a;
    private final Runtime b;

    static gp()
    {
        com.a.b.n.a.gp.a = java.util.logging.Logger.getLogger(com.a.b.n.a.gp.getName());
        return;
    }

    gp(Runtime p1)
    {
        this.b = p1;
        return;
    }

    public final void uncaughtException(Thread p7, Throwable p8)
    {
        try {
            Object[] v3_1 = new Object[1];
            v3_1[0] = p7;
            com.a.b.n.a.gp.a.log(java.util.logging.Level.SEVERE, String.format("Caught an exception in %s.  Shutting down.", v3_1), p8);
            this.b.exit(1);
        } catch (Runtime v0_2) {
            System.err.println(p8.getMessage());
            System.err.println(v0_2.getMessage());
            this.b.exit(1);
        } catch (Runtime v0_5) {
            this.b.exit(1);
            throw v0_5;
        }
        return;
    }
}
