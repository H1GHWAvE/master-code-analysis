package com.a.b.n.a;
public abstract class fy {
    private static final int a = 1024;
    private static final com.a.b.b.dz b = None;
    private static final int c = 255;

    static fy()
    {
        com.a.b.n.a.fy.b = new com.a.b.n.a.gd();
        return;
    }

    private fy()
    {
        return;
    }

    synthetic fy(byte p1)
    {
        return;
    }

    private static com.a.b.n.a.fy a(int p3, int p4)
    {
        return new com.a.b.n.a.ge(p3, new com.a.b.n.a.gb(p4), 0);
    }

    private static com.a.b.n.a.fy a(int p1, com.a.b.b.dz p2)
    {
        com.a.b.n.a.gf v0_2;
        if (p1 >= 1024) {
            v0_2 = new com.a.b.n.a.gf(p1, p2);
        } else {
            v0_2 = new com.a.b.n.a.gj(p1, p2);
        }
        return v0_2;
    }

    private Iterable a(Iterable p6)
    {
        int v0_7;
        Object[] v3 = com.a.b.d.mq.a(p6, Object);
        if (v3.length != 0) {
            int[] v4 = new int[v3.length];
            int v0_3 = 0;
            while (v0_3 < v3.length) {
                v4[v0_3] = this.b(v3[v0_3]);
                v0_3++;
            }
            java.util.Arrays.sort(v4);
            Object v2_1 = v4[0];
            v3[0] = this.a(v2_1);
            int v0_5 = 1;
            Object v1_1 = v2_1;
            while (v0_5 < v3.length) {
                Object v2_3 = v4[v0_5];
                if (v2_3 != v1_1) {
                    v3[v0_5] = this.a(v2_3);
                    v1_1 = v2_3;
                } else {
                    v3[v0_5] = v3[(v0_5 - 1)];
                }
                v0_5++;
            }
            v0_7 = java.util.Collections.unmodifiableList(java.util.Arrays.asList(v3));
        } else {
            v0_7 = com.a.b.d.jl.d();
        }
        return v0_7;
    }

    private static com.a.b.n.a.fy b(int p3)
    {
        return new com.a.b.n.a.ge(p3, new com.a.b.n.a.fz(), 0);
    }

    private static com.a.b.n.a.fy b(int p1, int p2)
    {
        return com.a.b.n.a.fy.a(p1, new com.a.b.n.a.gc(p2));
    }

    private static com.a.b.n.a.fy c(int p1)
    {
        return com.a.b.n.a.fy.a(p1, new com.a.b.n.a.ga());
    }

    private static com.a.b.n.a.fy d(int p3)
    {
        return new com.a.b.n.a.ge(p3, com.a.b.n.a.fy.b, 0);
    }

    private static com.a.b.n.a.fy e(int p1)
    {
        return com.a.b.n.a.fy.a(p1, com.a.b.n.a.fy.b);
    }

    private static int f(int p2)
    {
        return (1 << com.a.b.j.g.a(p2, java.math.RoundingMode.CEILING));
    }

    private static int g(int p2)
    {
        int v0_2 = (((p2 >> 20) ^ (p2 >> 12)) ^ p2);
        return ((v0_2 >> 4) ^ ((v0_2 >> 7) ^ v0_2));
    }

    private static synthetic int h(int p2)
    {
        return (1 << com.a.b.j.g.a(p2, java.math.RoundingMode.CEILING));
    }

    private static synthetic int i(int p2)
    {
        int v0_2 = (((p2 >> 20) ^ (p2 >> 12)) ^ p2);
        return ((v0_2 >> 4) ^ ((v0_2 >> 7) ^ v0_2));
    }

    public abstract int a();

    public abstract Object a();

    public abstract Object a();

    abstract int b();
}
