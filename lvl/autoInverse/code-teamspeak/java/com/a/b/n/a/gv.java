package com.a.b.n.a;
abstract class gv extends com.a.b.n.a.gt implements java.util.concurrent.ScheduledExecutorService {
    final java.util.concurrent.ScheduledExecutorService b;

    protected gv(java.util.concurrent.ScheduledExecutorService p1)
    {
        this(p1);
        this.b = p1;
        return;
    }

    public final java.util.concurrent.ScheduledFuture schedule(Runnable p3, long p4, java.util.concurrent.TimeUnit p6)
    {
        return this.b.schedule(this.a(p3), p4, p6);
    }

    public final java.util.concurrent.ScheduledFuture schedule(java.util.concurrent.Callable p3, long p4, java.util.concurrent.TimeUnit p6)
    {
        return this.b.schedule(this.a(p3), p4, p6);
    }

    public final java.util.concurrent.ScheduledFuture scheduleAtFixedRate(Runnable p9, long p10, long p12, java.util.concurrent.TimeUnit p14)
    {
        return this.b.scheduleAtFixedRate(this.a(p9), p10, p12, p14);
    }

    public final java.util.concurrent.ScheduledFuture scheduleWithFixedDelay(Runnable p9, long p10, long p12, java.util.concurrent.TimeUnit p14)
    {
        return this.b.scheduleWithFixedDelay(this.a(p9), p10, p12, p14);
    }
}
