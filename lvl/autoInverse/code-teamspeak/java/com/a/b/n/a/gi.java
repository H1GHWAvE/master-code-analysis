package com.a.b.n.a;
abstract class gi extends com.a.b.n.a.fy {
    final int d;

    gi(int p4)
    {
        int v0_0 = 0;
        this(0);
        if (p4 > 0) {
            v0_0 = 1;
        }
        int v0_5;
        com.a.b.b.cn.a(v0_0, "Stripes must be positive");
        if (p4 <= 1073741824) {
            v0_5 = ((1 << com.a.b.j.g.a(p4, java.math.RoundingMode.CEILING)) - 1);
        } else {
            v0_5 = -1;
        }
        this.d = v0_5;
        return;
    }

    public final Object a(Object p2)
    {
        return this.a(this.b(p2));
    }

    final int b(Object p4)
    {
        int v0_0 = p4.hashCode();
        int v0_1 = (v0_0 ^ ((v0_0 >> 20) ^ (v0_0 >> 12)));
        return (((v0_1 >> 4) ^ ((v0_1 >> 7) ^ v0_1)) & this.d);
    }
}
