package com.a.b.n.a;
final class ee implements java.lang.Runnable {
    final synthetic java.util.concurrent.ExecutorService a;
    final synthetic long b;
    final synthetic java.util.concurrent.TimeUnit c;
    final synthetic com.a.b.n.a.ed d;

    ee(com.a.b.n.a.ed p2, java.util.concurrent.ExecutorService p3, long p4, java.util.concurrent.TimeUnit p6)
    {
        this.d = p2;
        this.a = p3;
        this.b = p4;
        this.c = p6;
        return;
    }

    public final void run()
    {
        try {
            this.a.shutdown();
            this.a.awaitTermination(this.b, this.c);
        } catch (InterruptedException v0) {
        }
        return;
    }
}
