package com.a.b.n.a;
public abstract class bx extends com.a.b.d.hh implements java.util.concurrent.BlockingQueue {

    protected bx()
    {
        return;
    }

    protected final synthetic java.util.Queue a()
    {
        return this.c();
    }

    protected final synthetic java.util.Collection b()
    {
        return this.c();
    }

    protected abstract java.util.concurrent.BlockingQueue c();

    public int drainTo(java.util.Collection p2)
    {
        return this.c().drainTo(p2);
    }

    public int drainTo(java.util.Collection p2, int p3)
    {
        return this.c().drainTo(p2, p3);
    }

    protected final synthetic Object k_()
    {
        return this.c();
    }

    public boolean offer(Object p3, long p4, java.util.concurrent.TimeUnit p6)
    {
        return this.c().offer(p3, p4, p6);
    }

    public Object poll(long p2, java.util.concurrent.TimeUnit p4)
    {
        return this.c().poll(p2, p4);
    }

    public void put(Object p2)
    {
        this.c().put(p2);
        return;
    }

    public int remainingCapacity()
    {
        return this.c().remainingCapacity();
    }

    public Object take()
    {
        return this.c().take();
    }
}
