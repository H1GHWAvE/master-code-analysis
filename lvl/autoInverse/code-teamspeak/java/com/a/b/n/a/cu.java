package com.a.b.n.a;
 class cu extends com.a.b.n.a.g {
    private static final java.util.logging.Logger i;
    com.a.b.d.iz b;
    final boolean c;
    final java.util.concurrent.atomic.AtomicInteger d;
    com.a.b.n.a.db e;
    java.util.List f;
    final Object g;
    java.util.Set h;

    static cu()
    {
        com.a.b.n.a.cu.i = java.util.logging.Logger.getLogger(com.a.b.n.a.cu.getName());
        return;
    }

    cu(com.a.b.d.iz p3, boolean p4, java.util.concurrent.Executor p5, com.a.b.n.a.db p6)
    {
        this.g = new Object();
        this.b = p3;
        this.c = p4;
        this.d = new java.util.concurrent.atomic.AtomicInteger(p3.size());
        this.e = p6;
        this.f = com.a.b.d.ov.a(p3.size());
        this.a(p5);
        return;
    }

    private void a(int p6, java.util.concurrent.Future p7)
    {
        String v2 = 1;
        java.util.List v3 = this.f;
        if ((this.isDone()) || (v3 == null)) {
            if ((!this.c) && (!this.isCancelled())) {
                Object v0_3 = 0;
            } else {
                v0_3 = 1;
            }
            com.a.b.b.cn.b(v0_3, "Future was done before all dependencies completed");
        }
        try {
            com.a.b.b.cn.b(p7.isDone(), "Tried to set value from future which is not done");
            Object v0_5 = com.a.b.n.a.gs.a(p7);
        } catch (Object v0_13) {
            this.b(v0_13.getCause());
            Object v0_16 = this.d.decrementAndGet();
            if (v0_16 < null) {
                v2 = 0;
            }
            com.a.b.b.cn.b(v2, "Less than 0 remaining futures");
            if (v0_16 != null) {
                return;
            } else {
                Object v0_17 = this.e;
                if (v0_17 != null) {
                    if (v3 != null) {
                        this.a(v0_17.a(v3));
                        return;
                    }
                }
                com.a.b.b.cn.b(this.isDone());
                return;
            }
            Object v0_29 = this.d.decrementAndGet();
            if (v0_29 < null) {
                v2 = 0;
            }
            com.a.b.b.cn.b(v2, "Less than 0 remaining futures");
            if (v0_29 != null) {
                return;
            } else {
                Object v0_30 = this.e;
                if ((v0_30 == null) || (v3 == null)) {
                    com.a.b.b.cn.b(this.isDone());
                    return;
                } else {
                    this.a(v0_30.a(v3));
                    return;
                }
            }
        } catch (Object v0_7) {
            this.b(v0_7);
            Object v0_9 = this.d.decrementAndGet();
            if (v0_9 < null) {
                v2 = 0;
            }
            com.a.b.b.cn.b(v2, "Less than 0 remaining futures");
            if (v0_9 != null) {
                return;
            } else {
                Object v0_10 = this.e;
                if (v0_10 != null) {
                    if (v3 != null) {
                        this.a(v0_10.a(v3));
                        return;
                    }
                }
                com.a.b.b.cn.b(this.isDone());
                return;
            }
        } catch (Object v0_27) {
            String v4_3 = this.d.decrementAndGet();
            if (v4_3 < null) {
                v2 = 0;
            }
            com.a.b.b.cn.b(v2, "Less than 0 remaining futures");
            if (v4_3 == null) {
                Object v1_5 = this.e;
                if (v1_5 != null) {
                    if (v3 != null) {
                        this.a(v1_5.a(v3));
                        throw v0_27;
                    }
                }
                com.a.b.b.cn.b(this.isDone());
            }
        } catch (Object v0) {
            if (this.c) {
                this.cancel(0);
            }
            Object v0_23 = this.d.decrementAndGet();
            if (v0_23 < null) {
                v2 = 0;
            }
            com.a.b.b.cn.b(v2, "Less than 0 remaining futures");
            if (v0_23 != null) {
                return;
            } else {
                Object v0_24 = this.e;
                if (v0_24 != null) {
                    if (v3 != null) {
                        this.a(v0_24.a(v3));
                        return;
                    }
                }
                com.a.b.b.cn.b(this.isDone());
                return;
            }
        }
        if (v3 != null) {
            v3.set(p6, com.a.b.b.ci.c(v0_5));
        }
    }

    static synthetic void a(com.a.b.n.a.cu p5, int p6, java.util.concurrent.Future p7)
    {
        String v2 = 1;
        java.util.List v3 = p5.f;
        if ((p5.isDone()) || (v3 == null)) {
            if ((!p5.c) && (!p5.isCancelled())) {
                Object v0_3 = 0;
            } else {
                v0_3 = 1;
            }
            com.a.b.b.cn.b(v0_3, "Future was done before all dependencies completed");
        }
        try {
            com.a.b.b.cn.b(p7.isDone(), "Tried to set value from future which is not done");
            Object v0_5 = com.a.b.n.a.gs.a(p7);
        } catch (Object v0) {
            if (p5.c) {
                p5.cancel(0);
            }
            Object v0_23 = p5.d.decrementAndGet();
            if (v0_23 < null) {
                v2 = 0;
            }
            com.a.b.b.cn.b(v2, "Less than 0 remaining futures");
            if (v0_23 != null) {
                return;
            } else {
                Object v0_24 = p5.e;
                if (v0_24 != null) {
                    if (v3 != null) {
                        p5.a(v0_24.a(v3));
                        return;
                    }
                }
                com.a.b.b.cn.b(p5.isDone());
                return;
            }
            Object v0_29 = p5.d.decrementAndGet();
            if (v0_29 < null) {
                v2 = 0;
            }
            com.a.b.b.cn.b(v2, "Less than 0 remaining futures");
            if (v0_29 != null) {
                return;
            } else {
                Object v0_30 = p5.e;
                if ((v0_30 == null) || (v3 == null)) {
                    com.a.b.b.cn.b(p5.isDone());
                    return;
                } else {
                    p5.a(v0_30.a(v3));
                    return;
                }
            }
        } catch (Object v0_7) {
            p5 = p5.b(v0_7);
            Object v0_9 = p5.d.decrementAndGet();
            if (v0_9 < null) {
                v2 = 0;
            }
            com.a.b.b.cn.b(v2, "Less than 0 remaining futures");
            if (v0_9 != null) {
                return;
            } else {
                Object v0_10 = p5.e;
                if (v0_10 != null) {
                    if (v3 != null) {
                        p5.a(v0_10.a(v3));
                        return;
                    }
                }
                com.a.b.b.cn.b(p5.isDone());
                return;
            }
        } catch (Object v0_27) {
            String v4_3 = p5.d.decrementAndGet();
            if (v4_3 < null) {
                v2 = 0;
            }
            com.a.b.b.cn.b(v2, "Less than 0 remaining futures");
            if (v4_3 == null) {
                Object v1_5 = p5.e;
                if (v1_5 != null) {
                    if (v3 != null) {
                        p5.a(v1_5.a(v3));
                        throw v0_27;
                    }
                }
                com.a.b.b.cn.b(p5.isDone());
            }
        } catch (Object v0_13) {
            p5 = p5.b(v0_13.getCause());
            Object v0_16 = p5.d.decrementAndGet();
            if (v0_16 < null) {
                v2 = 0;
            }
            com.a.b.b.cn.b(v2, "Less than 0 remaining futures");
            if (v0_16 != null) {
                return;
            } else {
                Object v0_17 = p5.e;
                if (v0_17 != null) {
                    if (v3 != null) {
                        p5.a(v0_17.a(v3));
                        return;
                    }
                }
                com.a.b.b.cn.b(p5.isDone());
                return;
            }
        }
        if (v3 != null) {
            v3.set(p6, com.a.b.b.ci.c(v0_5));
        }
    }

    private void a(java.util.concurrent.Executor p6)
    {
        int v1_0 = 0;
        this.a(new com.a.b.n.a.cv(this), com.a.b.n.a.ef.a);
        if (!this.b.isEmpty()) {
            com.a.b.n.a.dp v0_4 = 0;
            while (v0_4 < this.b.size()) {
                this.f.add(0);
                v0_4++;
            }
            java.util.Iterator v3_0 = this.b.iterator();
            while (v3_0.hasNext()) {
                com.a.b.n.a.dp v0_8 = ((com.a.b.n.a.dp) v3_0.next());
                int v2_3 = (v1_0 + 1);
                v0_8.a(new com.a.b.n.a.cw(this, v1_0, v0_8), p6);
                v1_0 = v2_3;
            }
        } else {
            this.a(this.e.a(com.a.b.d.jl.d()));
        }
        return;
    }

    private void b(Throwable p4)
    {
        java.util.logging.Level v1_0 = 0;
        boolean v0_0 = 1;
        if (this.c) {
            v1_0 = super.a(p4);
            if (this.h == null) {
                this.h = new java.util.HashSet();
            }
            v0_0 = this.h.add(p4);
        }
        if (((p4 instanceof Error)) || ((this.c) && ((v1_0 == null) && (v0_0)))) {
            com.a.b.n.a.cu.i.log(java.util.logging.Level.SEVERE, "input future failed.", p4);
        }
        return;
    }
}
