package com.a.b.n.a;
public abstract class cf extends com.a.b.n.a.ca implements com.a.b.n.a.du {

    protected cf()
    {
        return;
    }

    public final com.a.b.n.a.dp a(Runnable p2)
    {
        return this.b().a(p2);
    }

    public final com.a.b.n.a.dp a(Runnable p2, Object p3)
    {
        return this.b().a(p2, p3);
    }

    public final com.a.b.n.a.dp a(java.util.concurrent.Callable p2)
    {
        return this.b().a(p2);
    }

    protected final synthetic java.util.concurrent.ExecutorService a()
    {
        return this.b();
    }

    protected abstract com.a.b.n.a.du b();

    protected final synthetic Object k_()
    {
        return this.b();
    }

    public synthetic java.util.concurrent.Future submit(Runnable p2)
    {
        return this.a(p2);
    }

    public synthetic java.util.concurrent.Future submit(Runnable p2, Object p3)
    {
        return this.a(p2, p3);
    }

    public synthetic java.util.concurrent.Future submit(java.util.concurrent.Callable p2)
    {
        return this.a(p2);
    }
}
