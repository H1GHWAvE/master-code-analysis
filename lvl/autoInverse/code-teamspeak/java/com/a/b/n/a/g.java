package com.a.b.n.a;
public abstract class g implements com.a.b.n.a.dp {
    final com.a.b.n.a.h a;
    private final com.a.b.n.a.bu b;

    protected g()
    {
        this.a = new com.a.b.n.a.h();
        this.b = new com.a.b.n.a.bu();
        return;
    }

    static final java.util.concurrent.CancellationException a(String p1, Throwable p2)
    {
        java.util.concurrent.CancellationException v0_1 = new java.util.concurrent.CancellationException(p1);
        v0_1.initCause(p2);
        return v0_1;
    }

    private static void a()
    {
        return;
    }

    private boolean b()
    {
        return this.a.d();
    }

    public final void a(Runnable p2, java.util.concurrent.Executor p3)
    {
        this.b.a(p2, p3);
        return;
    }

    protected boolean a(Object p4)
    {
        boolean v0_1 = this.a.a(p4, 0, 2);
        if (v0_1) {
            this.b.a();
        }
        return v0_1;
    }

    protected boolean a(Throwable p5)
    {
        boolean v0_2 = this.a.a(0, ((Throwable) com.a.b.b.cn.a(p5)), 2);
        if (v0_2) {
            this.b.a();
        }
        return v0_2;
    }

    public boolean cancel(boolean p4)
    {
        int v0_0;
        if (!p4) {
            v0_0 = 4;
        } else {
            v0_0 = 8;
        }
        int v0_3;
        if (this.a.a(0, 0, v0_0)) {
            this.b.a();
            v0_3 = 1;
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public Object get()
    {
        Object v0_0 = this.a;
        v0_0.acquireSharedInterruptibly(-1);
        return v0_0.a();
    }

    public Object get(long p6, java.util.concurrent.TimeUnit p8)
    {
        Object v0_0 = this.a;
        if (v0_0.tryAcquireSharedNanos(-1, p8.toNanos(p6))) {
            return v0_0.a();
        } else {
            throw new java.util.concurrent.TimeoutException("Timeout waiting for task.");
        }
    }

    public boolean isCancelled()
    {
        return this.a.c();
    }

    public boolean isDone()
    {
        return this.a.b();
    }
}
