package com.a.b.n.a;
final class h extends java.util.concurrent.locks.AbstractQueuedSynchronizer {
    static final int a = 0;
    static final int b = 1;
    static final int c = 2;
    static final int d = 4;
    static final int e = 8;
    private static final long f;
    private Object g;
    private Throwable h;

    h()
    {
        return;
    }

    private Object a(long p4)
    {
        if (this.tryAcquireSharedNanos(-1, p4)) {
            return this.a();
        } else {
            throw new java.util.concurrent.TimeoutException("Timeout waiting for task.");
        }
    }

    private boolean a(Object p3)
    {
        return this.a(p3, 0, 2);
    }

    private boolean a(Throwable p3)
    {
        return this.a(0, p3, 2);
    }

    private boolean a(boolean p3)
    {
        boolean v0_0;
        if (!p3) {
            v0_0 = 4;
        } else {
            v0_0 = 8;
        }
        return this.a(0, 0, v0_0);
    }

    private Object e()
    {
        this.acquireSharedInterruptibly(-1);
        return this.a();
    }

    final Object a()
    {
        Object v0_0 = this.getState();
        switch (v0_0) {
            case 2:
                if (this.h == null) {
                    return this.g;
                } else {
                    throw new java.util.concurrent.ExecutionException(this.h);
                }
            case 4:
            case 8:
                throw com.a.b.n.a.g.a("Task was cancelled.", this.h);
                break;
            default:
                throw new IllegalStateException(new StringBuilder(49).append("Error, synchronizer in invalid state: ").append(v0_0).toString());
        }
    }

    final boolean a(Object p4, Throwable p5, int p6)
    {
        boolean v0_1 = this.compareAndSetState(0, 1);
        if (!v0_1) {
            if (this.getState() == 1) {
                this.acquireShared(-1);
            }
        } else {
            this.g = p4;
            if ((p6 & 12) != 0) {
                p5 = new java.util.concurrent.CancellationException("Future.cancel() was called.");
            }
            this.h = p5;
            this.releaseShared(p6);
        }
        return v0_1;
    }

    final boolean b()
    {
        int v0_2;
        if ((this.getState() & 14) == 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    final boolean c()
    {
        int v0_2;
        if ((this.getState() & 12) == 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    final boolean d()
    {
        int v0_1;
        if (this.getState() != 8) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    protected final int tryAcquireShared(int p2)
    {
        int v0_1;
        if (!this.b()) {
            v0_1 = -1;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    protected final boolean tryReleaseShared(int p2)
    {
        this.setState(p2);
        return 1;
    }
}
