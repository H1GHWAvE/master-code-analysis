package com.a.b.n.a;
final class u implements java.lang.Runnable {
    final synthetic com.a.b.n.a.q a;

    u(com.a.b.n.a.q p1)
    {
        this.a = p1;
        return;
    }

    public final void run()
    {
        try {
            com.a.b.n.a.q.a(this.a).lock();
        } catch (com.a.b.n.a.q v0_11) {
            this.a.a(v0_11);
            throw com.a.b.b.ei.a(v0_11);
        }
        try {
            if (this.a.f() == com.a.b.n.a.ew.d) {
                com.a.b.n.a.p.c();
                com.a.b.n.a.q.a(this.a).unlock();
                this.a.d();
            } else {
                com.a.b.n.a.q.a(this.a).unlock();
            }
        } catch (com.a.b.n.a.q v0_5) {
            com.a.b.n.a.q.a(this.a).unlock();
            throw v0_5;
        }
        return;
    }
}
