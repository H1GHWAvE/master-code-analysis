package com.a.b.n.a;
public abstract class p implements com.a.b.n.a.et {
    private static final java.util.logging.Logger a;
    private final com.a.b.n.a.ad b;

    static p()
    {
        com.a.b.n.a.p.a = java.util.logging.Logger.getLogger(com.a.b.n.a.p.getName());
        return;
    }

    protected p()
    {
        this.b = new com.a.b.n.a.q(this);
        return;
    }

    static synthetic com.a.b.n.a.ad a(com.a.b.n.a.p p1)
    {
        return p1.b;
    }

    protected static void b()
    {
        return;
    }

    protected static void c()
    {
        return;
    }

    static synthetic java.util.logging.Logger m()
    {
        return com.a.b.n.a.p.a;
    }

    private String n()
    {
        return this.getClass().getSimpleName();
    }

    protected abstract void a();

    public final void a(long p2, java.util.concurrent.TimeUnit p4)
    {
        this.b.a(p2, p4);
        return;
    }

    public final void a(com.a.b.n.a.ev p2, java.util.concurrent.Executor p3)
    {
        this.b.a(p2, p3);
        return;
    }

    public final void b(long p2, java.util.concurrent.TimeUnit p4)
    {
        this.b.b(p2, p4);
        return;
    }

    protected abstract com.a.b.n.a.aa d();

    public final boolean e()
    {
        return this.b.e();
    }

    public final com.a.b.n.a.ew f()
    {
        return this.b.f();
    }

    public final Throwable g()
    {
        return this.b.g();
    }

    public final com.a.b.n.a.et h()
    {
        this.b.h();
        return this;
    }

    public final com.a.b.n.a.et i()
    {
        this.b.i();
        return this;
    }

    public final void j()
    {
        this.b.j();
        return;
    }

    public final void k()
    {
        this.b.k();
        return;
    }

    protected final java.util.concurrent.ScheduledExecutorService l()
    {
        java.util.concurrent.ScheduledExecutorService v0_2 = java.util.concurrent.Executors.newSingleThreadScheduledExecutor(new com.a.b.n.a.v(this));
        this.a(new com.a.b.n.a.w(this, v0_2), com.a.b.n.a.ef.a);
        return v0_2;
    }

    public String toString()
    {
        String v0_3 = String.valueOf(String.valueOf(this.getClass().getSimpleName()));
        String v1_3 = String.valueOf(String.valueOf(this.b.f()));
        return new StringBuilder(((v0_3.length() + 3) + v1_3.length())).append(v0_3).append(" [").append(v1_3).append("]").toString();
    }
}
