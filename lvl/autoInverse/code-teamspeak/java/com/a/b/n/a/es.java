package com.a.b.n.a;
final class es implements java.lang.Runnable {
    final synthetic com.a.b.n.a.eq a;

    private es(com.a.b.n.a.eq p1)
    {
        this.a = p1;
        return;
    }

    synthetic es(com.a.b.n.a.eq p1, byte p2)
    {
        this(p1);
        return;
    }

    public final void run()
    {
        int v1_0 = 1;
        try {
            while(true) {
                com.a.b.b.cn.b(com.a.b.n.a.eq.a(this.a));
                com.a.b.n.a.eq.b(this.a);
                java.util.logging.Logger v3 = com.a.b.n.a.eq.a();
                String v0_8 = String.valueOf(String.valueOf(String v0_6));
                v3.log(java.util.logging.Level.SEVERE, new StringBuilder((v0_8.length() + 35)).append("Exception while executing runnable ").append(v0_8).toString(), RuntimeException v2_0);
            }
            com.a.b.n.a.eq.d(this.a);
            v1_0 = 0;
            return;
        } catch (String v0_13) {
            if (v1_0 != 0) {
                com.a.b.n.a.eq.b(this.a);
                com.a.b.n.a.eq.d(this.a);
            }
            throw v0_13;
        }
        v0_6 = ((Runnable) com.a.b.n.a.eq.c(this.a).poll());
        if (v0_6 != null) {
            try {
                v0_6.run();
            } catch (RuntimeException v2_0) {
            }
        }
        com.a.b.n.a.eq.d(this.a);
        v1_0 = 0;
        return;
    }
}
