package com.a.b.b;
final enum class bu extends java.lang.Enum implements com.a.b.b.bj {
    public static final enum com.a.b.b.bu a;
    private static final synthetic com.a.b.b.bu[] b;

    static bu()
    {
        com.a.b.b.bu.a = new com.a.b.b.bu("INSTANCE");
        com.a.b.b.bu[] v0_3 = new com.a.b.b.bu[1];
        v0_3[0] = com.a.b.b.bu.a;
        com.a.b.b.bu.b = v0_3;
        return;
    }

    private bu(String p2)
    {
        this(p2, 0);
        return;
    }

    private static String a(Object p1)
    {
        com.a.b.b.cn.a(p1);
        return p1.toString();
    }

    public static com.a.b.b.bu valueOf(String p1)
    {
        return ((com.a.b.b.bu) Enum.valueOf(com.a.b.b.bu, p1));
    }

    public static com.a.b.b.bu[] values()
    {
        return ((com.a.b.b.bu[]) com.a.b.b.bu.b.clone());
    }

    public final synthetic Object e(Object p2)
    {
        com.a.b.b.cn.a(p2);
        return p2.toString();
    }

    public final String toString()
    {
        return "toString";
    }
}
