package com.a.b.b;
final class bo implements com.a.b.b.bj, java.io.Serializable {
    private static final long c;
    final java.util.Map a;
    final Object b;

    bo(java.util.Map p2, Object p3)
    {
        this.a = ((java.util.Map) com.a.b.b.cn.a(p2));
        this.b = p3;
        return;
    }

    public final Object e(Object p3)
    {
        Object v0_1 = this.a.get(p3);
        if ((v0_1 == null) && (!this.a.containsKey(p3))) {
            v0_1 = this.b;
        }
        return v0_1;
    }

    public final boolean equals(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof com.a.b.b.bo)) && ((this.a.equals(((com.a.b.b.bo) p4).a)) && (com.a.b.b.ce.a(this.b, ((com.a.b.b.bo) p4).b)))) {
            v0 = 1;
        }
        return v0;
    }

    public final int hashCode()
    {
        int v0_1 = new Object[2];
        v0_1[0] = this.a;
        v0_1[1] = this.b;
        return java.util.Arrays.hashCode(v0_1);
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        String v1_2 = String.valueOf(String.valueOf(this.b));
        return new StringBuilder(((v0_2.length() + 23) + v1_2.length())).append("forMap(").append(v0_2).append(", defaultValue=").append(v1_2).append(")").toString();
    }
}
