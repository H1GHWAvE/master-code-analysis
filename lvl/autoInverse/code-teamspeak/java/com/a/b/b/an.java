package com.a.b.b;
final class an extends com.a.b.b.ak implements java.io.Serializable {
    private static final long c;
    final com.a.b.b.ak a;
    final com.a.b.b.ak b;

    an(com.a.b.b.ak p1, com.a.b.b.ak p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    protected final Object a(Object p2)
    {
        throw new AssertionError();
    }

    protected final Object b(Object p2)
    {
        throw new AssertionError();
    }

    final Object c(Object p3)
    {
        return this.b.c(this.a.c(p3));
    }

    final Object d(Object p3)
    {
        return this.a.d(this.b.d(p3));
    }

    public final boolean equals(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof com.a.b.b.an)) && ((this.a.equals(((com.a.b.b.an) p4).a)) && (this.b.equals(((com.a.b.b.an) p4).b)))) {
            v0 = 1;
        }
        return v0;
    }

    public final int hashCode()
    {
        return ((this.a.hashCode() * 31) + this.b.hashCode());
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        String v1_2 = String.valueOf(String.valueOf(this.b));
        return new StringBuilder(((v0_2.length() + 10) + v1_2.length())).append(v0_2).append(".andThen(").append(v1_2).append(")").toString();
    }
}
