package com.a.b.b;
public final class bz {
    private final com.a.b.b.bv a;
    private final String b;

    private bz(com.a.b.b.bv p2, String p3)
    {
        this.a = p2;
        this.b = ((String) com.a.b.b.cn.a(p3));
        return;
    }

    synthetic bz(com.a.b.b.bv p1, String p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    private com.a.b.b.bz a(String p4)
    {
        return new com.a.b.b.bz(this.a.b(p4), this.b);
    }

    private Appendable a(Appendable p2, Iterable p3)
    {
        return this.a(p2, p3.iterator());
    }

    private Appendable a(Appendable p4, java.util.Iterator p5)
    {
        com.a.b.b.cn.a(p4);
        if (p5.hasNext()) {
            CharSequence v0_2 = ((java.util.Map$Entry) p5.next());
            p4.append(this.a.a(v0_2.getKey()));
            p4.append(this.b);
            p4.append(this.a.a(v0_2.getValue()));
            while (p5.hasNext()) {
                p4.append(this.a.a);
                CharSequence v0_9 = ((java.util.Map$Entry) p5.next());
                p4.append(this.a.a(v0_9.getKey()));
                p4.append(this.b);
                p4.append(this.a.a(v0_9.getValue()));
            }
        }
        return p4;
    }

    private Appendable a(Appendable p2, java.util.Map p3)
    {
        return this.a(p2, p3.entrySet().iterator());
    }

    private String a(Iterable p3)
    {
        return this.a(new StringBuilder(), p3.iterator()).toString();
    }

    private String a(java.util.Iterator p2)
    {
        return this.a(new StringBuilder(), p2).toString();
    }

    private String a(java.util.Map p3)
    {
        return this.a(new StringBuilder(), p3.entrySet().iterator()).toString();
    }

    private StringBuilder a(StringBuilder p3, java.util.Iterator p4)
    {
        try {
            this.a(p3, p4);
            return p3;
        } catch (java.io.IOException v0) {
            throw new AssertionError(v0);
        }
    }

    private StringBuilder a(StringBuilder p2, java.util.Map p3)
    {
        return this.a(p2, p3.entrySet());
    }

    public final StringBuilder a(StringBuilder p2, Iterable p3)
    {
        return this.a(p2, p3.iterator());
    }
}
