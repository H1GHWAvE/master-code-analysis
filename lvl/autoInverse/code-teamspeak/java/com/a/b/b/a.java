package com.a.b.b;
final class a extends com.a.b.b.ci {
    static final com.a.b.b.a a;
    private static final long b;

    static a()
    {
        com.a.b.b.a.a = new com.a.b.b.a();
        return;
    }

    private a()
    {
        return;
    }

    static com.a.b.b.ci a()
    {
        return com.a.b.b.a.a;
    }

    private static Object g()
    {
        return com.a.b.b.a.a;
    }

    public final com.a.b.b.ci a(com.a.b.b.bj p2)
    {
        com.a.b.b.cn.a(p2);
        return com.a.b.b.a.a;
    }

    public final com.a.b.b.ci a(com.a.b.b.ci p2)
    {
        return ((com.a.b.b.ci) com.a.b.b.cn.a(p2));
    }

    public final Object a(com.a.b.b.dz p3)
    {
        return com.a.b.b.cn.a(p3.a(), "use Optional.orNull() instead of a Supplier that returns null");
    }

    public final Object a(Object p2)
    {
        return com.a.b.b.cn.a(p2, "use Optional.orNull() instead of Optional.or(null)");
    }

    public final boolean b()
    {
        return 0;
    }

    public final Object c()
    {
        throw new IllegalStateException("Optional.get() cannot be called on an absent value");
    }

    public final Object d()
    {
        return 0;
    }

    public final java.util.Set e()
    {
        return java.util.Collections.emptySet();
    }

    public final boolean equals(Object p2)
    {
        int v0;
        if (p2 != this) {
            v0 = 0;
        } else {
            v0 = 1;
        }
        return v0;
    }

    public final int hashCode()
    {
        return 1502476572;
    }

    public final String toString()
    {
        return "Optional.absent()";
    }
}
