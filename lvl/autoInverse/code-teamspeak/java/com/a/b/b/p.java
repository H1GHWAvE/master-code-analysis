package com.a.b.b;
final class p extends com.a.b.b.m {
    final synthetic char[] s;

    p(String p1, char[] p2)
    {
        this.s = p2;
        this(p1);
        return;
    }

    final void a(java.util.BitSet p5)
    {
        char[] v1 = this.s;
        int v2 = v1.length;
        int v0 = 0;
        while (v0 < v2) {
            p5.set(v1[v0]);
            v0++;
        }
        return;
    }

    public final bridge synthetic boolean a(Object p2)
    {
        return super.a(((Character) p2));
    }

    public final boolean c(char p2)
    {
        int v0_2;
        if (java.util.Arrays.binarySearch(this.s, p2) < 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }
}
