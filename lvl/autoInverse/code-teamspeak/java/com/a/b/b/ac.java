package com.a.b.b;
final class ac extends com.a.b.b.m {
    final com.a.b.b.m s;
    final com.a.b.b.m t;

    ac(com.a.b.b.m p6, com.a.b.b.m p7)
    {
        String v0_1 = String.valueOf(String.valueOf(p6));
        String v1_1 = String.valueOf(String.valueOf(p7));
        this(p6, p7, new StringBuilder(((v0_1.length() + 19) + v1_1.length())).append("CharMatcher.and(").append(v0_1).append(", ").append(v1_1).append(")").toString());
        return;
    }

    private ac(com.a.b.b.m p2, com.a.b.b.m p3, String p4)
    {
        this(p4);
        this.s = ((com.a.b.b.m) com.a.b.b.cn.a(p2));
        this.t = ((com.a.b.b.m) com.a.b.b.cn.a(p3));
        return;
    }

    final com.a.b.b.m a(String p4)
    {
        return new com.a.b.b.ac(this.s, this.t, p4);
    }

    final void a(java.util.BitSet p4)
    {
        java.util.BitSet v0_1 = new java.util.BitSet();
        this.s.a(v0_1);
        java.util.BitSet v1_2 = new java.util.BitSet();
        this.t.a(v1_2);
        v0_1.and(v1_2);
        p4.or(v0_1);
        return;
    }

    public final bridge synthetic boolean a(Object p2)
    {
        return super.a(((Character) p2));
    }

    public final boolean c(char p2)
    {
        if ((!this.s.c(p2)) || (!this.t.c(p2))) {
            int v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }
}
