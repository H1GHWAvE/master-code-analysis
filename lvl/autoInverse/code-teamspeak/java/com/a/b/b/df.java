package com.a.b.b;
final class df implements com.a.b.b.co, java.io.Serializable {
    private static final long b;
    private final java.util.List a;

    private df(java.util.List p1)
    {
        this.a = p1;
        return;
    }

    synthetic df(java.util.List p1, byte p2)
    {
        this(p1);
        return;
    }

    public final boolean a(Object p4)
    {
        int v2 = 0;
        int v1 = 0;
        while (v1 < this.a.size()) {
            if (!((com.a.b.b.co) this.a.get(v1)).a(p4)) {
                v1++;
            } else {
                v2 = 1;
                break;
            }
        }
        return v2;
    }

    public final boolean equals(Object p3)
    {
        int v0_1;
        if (!(p3 instanceof com.a.b.b.df)) {
            v0_1 = 0;
        } else {
            v0_1 = this.a.equals(((com.a.b.b.df) p3).a);
        }
        return v0_1;
    }

    public final int hashCode()
    {
        return (this.a.hashCode() + 87855567);
    }

    public final String toString()
    {
        String v0_3 = String.valueOf(String.valueOf(com.a.b.b.cp.b().a(this.a)));
        return new StringBuilder((v0_3.length() + 15)).append("Predicates.or(").append(v0_3).append(")").toString();
    }
}
