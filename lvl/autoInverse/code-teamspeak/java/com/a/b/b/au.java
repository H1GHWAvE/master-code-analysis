package com.a.b.b;
public abstract class au {

    protected au()
    {
        return;
    }

    private com.a.b.b.au a()
    {
        return new com.a.b.b.cl(this);
    }

    private com.a.b.b.au a(com.a.b.b.bj p2)
    {
        return new com.a.b.b.bk(p2, this);
    }

    private static com.a.b.b.au b()
    {
        return com.a.b.b.aw.a;
    }

    private static com.a.b.b.au c()
    {
        return com.a.b.b.ay.a;
    }

    private com.a.b.b.az c(Object p3)
    {
        return new com.a.b.b.az(this, p3, 0);
    }

    private com.a.b.b.co d(Object p2)
    {
        return new com.a.b.b.ax(this, p2);
    }

    public final int a(Object p2)
    {
        int v0;
        if (p2 != null) {
            v0 = this.b(p2);
        } else {
            v0 = 0;
        }
        return v0;
    }

    public final boolean a(Object p2, Object p3)
    {
        boolean v0;
        if (p2 != p3) {
            if ((p2 != null) && (p3 != null)) {
                v0 = this.b(p2, p3);
            } else {
                v0 = 0;
            }
        } else {
            v0 = 1;
        }
        return v0;
    }

    protected abstract int b();

    protected abstract boolean b();
}
