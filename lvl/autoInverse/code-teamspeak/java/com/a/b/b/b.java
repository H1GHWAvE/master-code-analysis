package com.a.b.b;
abstract class b implements java.util.Iterator {
    private int a;
    private Object b;

    protected b()
    {
        this.a = com.a.b.b.d.b;
        return;
    }

    private boolean c()
    {
        int v0_3;
        this.a = com.a.b.b.d.d;
        this.b = this.a();
        if (this.a == com.a.b.b.d.c) {
            v0_3 = 0;
        } else {
            this.a = com.a.b.b.d.a;
            v0_3 = 1;
        }
        return v0_3;
    }

    protected abstract Object a();

    protected final Object b()
    {
        this.a = com.a.b.b.d.c;
        return 0;
    }

    public final boolean hasNext()
    {
        int v0_1;
        int v2 = 0;
        if (this.a == com.a.b.b.d.d) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.b(v0_1);
        switch (com.a.b.b.c.a[(this.a - 1)]) {
            case 1:
                break;
            case 2:
                v2 = 1;
                break;
            default:
                this.a = com.a.b.b.d.d;
                this.b = this.a();
                if (this.a == com.a.b.b.d.c) {
                } else {
                    this.a = com.a.b.b.d.a;
                    v2 = 1;
                }
        }
        return v2;
    }

    public final Object next()
    {
        if (this.hasNext()) {
            this.a = com.a.b.b.d.b;
            Object v0_2 = this.b;
            this.b = 0;
            return v0_2;
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public final void remove()
    {
        throw new UnsupportedOperationException();
    }
}
