package com.a.b.b;
public final class em {

    private em()
    {
        return;
    }

    private static Object a(Object p3)
    {
        com.a.b.b.en v0_0 = 0;
        Object[] v2 = new Object[0];
        if (p3 != null) {
            v0_0 = 1;
        }
        if (v0_0 != null) {
            return p3;
        } else {
            throw new com.a.b.b.en(com.a.b.b.cn.a("expected a non-null reference", v2));
        }
    }

    private static varargs Object a(Object p2, String p3, Object[] p4)
    {
        com.a.b.b.en v0_0;
        if (p2 == null) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        if (v0_0 != null) {
            return p2;
        } else {
            throw new com.a.b.b.en(com.a.b.b.cn.a(p3, p4));
        }
    }

    private static void a(boolean p1)
    {
        if (p1) {
            return;
        } else {
            throw new com.a.b.b.en();
        }
    }

    private static varargs void a(boolean p2, String p3, Object[] p4)
    {
        if (p2) {
            return;
        } else {
            throw new com.a.b.b.en(com.a.b.b.cn.a(p3, p4));
        }
    }
}
