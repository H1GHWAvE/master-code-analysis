package com.a.b.b;
final class aq extends com.a.b.b.ak implements java.io.Serializable {
    private static final long b;
    final com.a.b.b.ak a;

    aq(com.a.b.b.ak p1)
    {
        this.a = p1;
        return;
    }

    public final com.a.b.b.ak a()
    {
        return this.a;
    }

    protected final Object a(Object p2)
    {
        throw new AssertionError();
    }

    protected final Object b(Object p2)
    {
        throw new AssertionError();
    }

    final Object c(Object p2)
    {
        return this.a.d(p2);
    }

    final Object d(Object p2)
    {
        return this.a.c(p2);
    }

    public final boolean equals(Object p3)
    {
        int v0_1;
        if (!(p3 instanceof com.a.b.b.aq)) {
            v0_1 = 0;
        } else {
            v0_1 = this.a.equals(((com.a.b.b.aq) p3).a);
        }
        return v0_1;
    }

    public final int hashCode()
    {
        return (this.a.hashCode() ^ -1);
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        return new StringBuilder((v0_2.length() + 10)).append(v0_2).append(".reverse()").toString();
    }
}
