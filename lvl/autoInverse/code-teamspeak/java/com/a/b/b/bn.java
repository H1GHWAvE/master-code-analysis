package com.a.b.b;
final class bn implements com.a.b.b.bj, java.io.Serializable {
    private static final long b;
    private final Object a;

    public bn(Object p1)
    {
        this.a = p1;
        return;
    }

    public final Object e(Object p2)
    {
        return this.a;
    }

    public final boolean equals(Object p3)
    {
        int v0_1;
        if (!(p3 instanceof com.a.b.b.bn)) {
            v0_1 = 0;
        } else {
            v0_1 = com.a.b.b.ce.a(this.a, ((com.a.b.b.bn) p3).a);
        }
        return v0_1;
    }

    public final int hashCode()
    {
        int v0_2;
        if (this.a != null) {
            v0_2 = this.a.hashCode();
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        return new StringBuilder((v0_2.length() + 10)).append("constant(").append(v0_2).append(")").toString();
    }
}
