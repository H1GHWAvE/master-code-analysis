package com.a.b.b;
final class ax implements com.a.b.b.co, java.io.Serializable {
    private static final long c;
    private final com.a.b.b.au a;
    private final Object b;

    ax(com.a.b.b.au p2, Object p3)
    {
        this.a = ((com.a.b.b.au) com.a.b.b.cn.a(p2));
        this.b = p3;
        return;
    }

    public final boolean a(Object p3)
    {
        return this.a.a(p3, this.b);
    }

    public final boolean equals(Object p5)
    {
        int v0 = 1;
        if (this != p5) {
            if (!(p5 instanceof com.a.b.b.ax)) {
                v0 = 0;
            } else {
                if ((!this.a.equals(((com.a.b.b.ax) p5).a)) || (!com.a.b.b.ce.a(this.b, ((com.a.b.b.ax) p5).b))) {
                    v0 = 0;
                }
            }
        }
        return v0;
    }

    public final int hashCode()
    {
        int v0_1 = new Object[2];
        v0_1[0] = this.a;
        v0_1[1] = this.b;
        return java.util.Arrays.hashCode(v0_1);
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        String v1_2 = String.valueOf(String.valueOf(this.b));
        return new StringBuilder(((v0_2.length() + 15) + v1_2.length())).append(v0_2).append(".equivalentTo(").append(v1_2).append(")").toString();
    }
}
