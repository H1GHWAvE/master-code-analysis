package com.a.b.f;
 class n {
    final Object a;
    final reflect.Method b;

    n(Object p2, reflect.Method p3)
    {
        com.a.b.b.cn.a(p2, "EventSubscriber target cannot be null.");
        com.a.b.b.cn.a(p3, "EventSubscriber method cannot be null.");
        this.a = p2;
        this.b = p3;
        p3.setAccessible(1);
        return;
    }

    private Object a()
    {
        return this.a;
    }

    private reflect.Method b()
    {
        return this.b;
    }

    public void a(Object p6)
    {
        com.a.b.b.cn.a(p6);
        try {
            Error v0_0 = this.b;
            boolean v1_0 = this.a;
            String v2_1 = new Object[1];
            v2_1[0] = p6;
            v0_0.invoke(v1_0, v2_1);
            return;
        } catch (Error v0_5) {
            String v2_7 = String.valueOf(String.valueOf(p6));
            throw new Error(new StringBuilder((v2_7.length() + 33)).append("Method rejected target/argument: ").append(v2_7).toString(), v0_5);
        } catch (Error v0_1) {
            if (!(v0_1.getCause() instanceof Error)) {
                throw v0_1;
            } else {
                throw ((Error) v0_1.getCause());
            }
        } catch (Error v0_4) {
            String v2_3 = String.valueOf(String.valueOf(p6));
            throw new Error(new StringBuilder((v2_3.length() + 28)).append("Method became inaccessible: ").append(v2_3).toString(), v0_4);
        }
    }

    public boolean equals(Object p4)
    {
        int v0 = 0;
        if (((p4 instanceof com.a.b.f.n)) && ((this.a == ((com.a.b.f.n) p4).a) && (this.b.equals(((com.a.b.f.n) p4).b)))) {
            v0 = 1;
        }
        return v0;
    }

    public int hashCode()
    {
        return (((this.b.hashCode() + 31) * 31) + System.identityHashCode(this.a));
    }

    public String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.b));
        return new StringBuilder((v0_2.length() + 10)).append("[wrapper ").append(v0_2).append("]").toString();
    }
}
