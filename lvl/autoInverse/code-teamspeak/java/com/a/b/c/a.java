package com.a.b.c;
public abstract class a implements com.a.b.c.e {

    protected a()
    {
        return;
    }

    public final com.a.b.d.jt a(Iterable p5)
    {
        com.a.b.d.jt v0_0 = com.a.b.d.sz.d();
        java.util.Iterator v1 = p5.iterator();
        while (v1.hasNext()) {
            Object v2_1 = v1.next();
            if (!v0_0.containsKey(v2_1)) {
                Object v3_1 = this.d(v2_1);
                if (v3_1 != null) {
                    v0_0.put(v2_1, v3_1);
                }
            }
        }
        return com.a.b.d.jt.a(v0_0);
    }

    public final Object a(Object p2, java.util.concurrent.Callable p3)
    {
        throw new UnsupportedOperationException();
    }

    public final void a()
    {
        return;
    }

    public final void a(Object p2)
    {
        throw new UnsupportedOperationException();
    }

    public final void a(Object p2, Object p3)
    {
        throw new UnsupportedOperationException();
    }

    public final void a(java.util.Map p3)
    {
        UnsupportedOperationException v0_1 = p3.entrySet().iterator();
        if (!v0_1.hasNext()) {
            return;
        } else {
            UnsupportedOperationException v0_3 = ((java.util.Map$Entry) v0_1.next());
            v0_3.getKey();
            v0_3.getValue();
            throw new UnsupportedOperationException();
        }
    }

    public final long b()
    {
        throw new UnsupportedOperationException();
    }

    public final void b(Iterable p3)
    {
        UnsupportedOperationException v0_0 = p3.iterator();
        if (!v0_0.hasNext()) {
            return;
        } else {
            v0_0.next();
            throw new UnsupportedOperationException();
        }
    }

    public final void c()
    {
        throw new UnsupportedOperationException();
    }

    public final com.a.b.c.ai d()
    {
        throw new UnsupportedOperationException();
    }

    public final java.util.concurrent.ConcurrentMap e()
    {
        throw new UnsupportedOperationException();
    }
}
