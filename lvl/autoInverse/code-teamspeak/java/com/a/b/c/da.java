package com.a.b.c;
public abstract enum class da extends java.lang.Enum {
    public static final enum com.a.b.c.da a;
    public static final enum com.a.b.c.da b;
    public static final enum com.a.b.c.da c;
    public static final enum com.a.b.c.da d;
    public static final enum com.a.b.c.da e;
    private static final synthetic com.a.b.c.da[] f;

    static da()
    {
        com.a.b.c.da.a = new com.a.b.c.db("EXPLICIT");
        com.a.b.c.da.b = new com.a.b.c.dc("REPLACED");
        com.a.b.c.da.c = new com.a.b.c.dd("COLLECTED");
        com.a.b.c.da.d = new com.a.b.c.de("EXPIRED");
        com.a.b.c.da.e = new com.a.b.c.df("SIZE");
        com.a.b.c.da[] v0_11 = new com.a.b.c.da[5];
        v0_11[0] = com.a.b.c.da.a;
        v0_11[1] = com.a.b.c.da.b;
        v0_11[2] = com.a.b.c.da.c;
        v0_11[3] = com.a.b.c.da.d;
        v0_11[4] = com.a.b.c.da.e;
        com.a.b.c.da.f = v0_11;
        return;
    }

    private da(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    synthetic da(String p1, int p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    public static com.a.b.c.da valueOf(String p1)
    {
        return ((com.a.b.c.da) Enum.valueOf(com.a.b.c.da, p1));
    }

    public static com.a.b.c.da[] values()
    {
        return ((com.a.b.c.da[]) com.a.b.c.da.f.clone());
    }

    abstract boolean a();
}
