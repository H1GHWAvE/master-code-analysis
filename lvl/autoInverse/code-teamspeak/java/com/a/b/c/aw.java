package com.a.b.c;
abstract enum class aw extends java.lang.Enum {
    public static final enum com.a.b.c.aw a = None;
    public static final enum com.a.b.c.aw b = None;
    public static final enum com.a.b.c.aw c = None;
    public static final enum com.a.b.c.aw d = None;
    public static final enum com.a.b.c.aw e = None;
    public static final enum com.a.b.c.aw f = None;
    public static final enum com.a.b.c.aw g = None;
    public static final enum com.a.b.c.aw h = None;
    static final int i = 1;
    static final int j = 2;
    static final int k = 4;
    static final com.a.b.c.aw[] l;
    private static final synthetic com.a.b.c.aw[] m;

    static aw()
    {
        com.a.b.c.aw.a = new com.a.b.c.ax("STRONG");
        com.a.b.c.aw.b = new com.a.b.c.ay("STRONG_ACCESS");
        com.a.b.c.aw.c = new com.a.b.c.az("STRONG_WRITE");
        com.a.b.c.aw.d = new com.a.b.c.ba("STRONG_ACCESS_WRITE");
        com.a.b.c.aw.e = new com.a.b.c.bb("WEAK");
        com.a.b.c.aw.f = new com.a.b.c.bc("WEAK_ACCESS");
        com.a.b.c.aw.g = new com.a.b.c.bd("WEAK_WRITE");
        com.a.b.c.aw.h = new com.a.b.c.be("WEAK_ACCESS_WRITE");
        com.a.b.c.aw[] v0_17 = new com.a.b.c.aw[8];
        v0_17[0] = com.a.b.c.aw.a;
        v0_17[1] = com.a.b.c.aw.b;
        v0_17[2] = com.a.b.c.aw.c;
        v0_17[3] = com.a.b.c.aw.d;
        v0_17[4] = com.a.b.c.aw.e;
        v0_17[5] = com.a.b.c.aw.f;
        v0_17[6] = com.a.b.c.aw.g;
        v0_17[7] = com.a.b.c.aw.h;
        com.a.b.c.aw.m = v0_17;
        com.a.b.c.aw[] v0_19 = new com.a.b.c.aw[8];
        v0_19[0] = com.a.b.c.aw.a;
        v0_19[1] = com.a.b.c.aw.b;
        v0_19[2] = com.a.b.c.aw.c;
        v0_19[3] = com.a.b.c.aw.d;
        v0_19[4] = com.a.b.c.aw.e;
        v0_19[5] = com.a.b.c.aw.f;
        v0_19[6] = com.a.b.c.aw.g;
        v0_19[7] = com.a.b.c.aw.h;
        com.a.b.c.aw.l = v0_19;
        return;
    }

    private aw(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    synthetic aw(String p1, int p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    static com.a.b.c.aw a(com.a.b.c.bw p3, boolean p4, boolean p5)
    {
        int v2;
        com.a.b.c.aw v0_0 = 0;
        if (p3 != com.a.b.c.bw.c) {
            v2 = 0;
        } else {
            v2 = 4;
        }
        com.a.b.c.aw[] v1_2;
        if (!p4) {
            v1_2 = 0;
        } else {
            v1_2 = 1;
        }
        if (p5) {
            v0_0 = 2;
        }
        return com.a.b.c.aw.l[(v0_0 | (v1_2 | v2))];
    }

    static void a(com.a.b.c.bs p2, com.a.b.c.bs p3)
    {
        p3.a(p2.e());
        com.a.b.c.ao.a(p2.g(), p3);
        com.a.b.c.ao.a(p3, p2.f());
        com.a.b.c.ao.a(p2);
        return;
    }

    static void b(com.a.b.c.bs p2, com.a.b.c.bs p3)
    {
        p3.b(p2.h());
        com.a.b.c.ao.b(p2.j(), p3);
        com.a.b.c.ao.b(p3, p2.i());
        com.a.b.c.ao.b(p2);
        return;
    }

    public static com.a.b.c.aw valueOf(String p1)
    {
        return ((com.a.b.c.aw) Enum.valueOf(com.a.b.c.aw, p1));
    }

    public static com.a.b.c.aw[] values()
    {
        return ((com.a.b.c.aw[]) com.a.b.c.aw.m.clone());
    }

    com.a.b.c.bs a(com.a.b.c.bt p3, com.a.b.c.bs p4, com.a.b.c.bs p5)
    {
        return this.a(p3, p4.d(), p4.c(), p5);
    }

    abstract com.a.b.c.bs a();
}
