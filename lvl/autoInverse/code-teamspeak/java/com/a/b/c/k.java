package com.a.b.c;
final enum class k extends java.lang.Enum implements com.a.b.c.do {
    public static final enum com.a.b.c.k a;
    private static final synthetic com.a.b.c.k[] b;

    static k()
    {
        com.a.b.c.k.a = new com.a.b.c.k("INSTANCE");
        com.a.b.c.k[] v0_3 = new com.a.b.c.k[1];
        v0_3[0] = com.a.b.c.k.a;
        com.a.b.c.k.b = v0_3;
        return;
    }

    private k(String p2)
    {
        this(p2, 0);
        return;
    }

    public static com.a.b.c.k valueOf(String p1)
    {
        return ((com.a.b.c.k) Enum.valueOf(com.a.b.c.k, p1));
    }

    public static com.a.b.c.k[] values()
    {
        return ((com.a.b.c.k[]) com.a.b.c.k.b.clone());
    }

    public final int a()
    {
        return 1;
    }
}
