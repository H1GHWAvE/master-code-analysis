package com.a.b.c;
public abstract class aj extends com.a.b.d.hg implements com.a.b.c.e {

    protected aj()
    {
        return;
    }

    public final com.a.b.d.jt a(Iterable p2)
    {
        return this.f().a(p2);
    }

    public final Object a(Object p2, java.util.concurrent.Callable p3)
    {
        return this.f().a(p2, p3);
    }

    public final void a()
    {
        this.f().a();
        return;
    }

    public final void a(Object p2)
    {
        this.f().a(p2);
        return;
    }

    public final void a(Object p2, Object p3)
    {
        this.f().a(p2, p3);
        return;
    }

    public final void a(java.util.Map p2)
    {
        this.f().a(p2);
        return;
    }

    public final long b()
    {
        return this.f().b();
    }

    public final void b(Iterable p2)
    {
        this.f().b(p2);
        return;
    }

    public final void c()
    {
        this.f().c();
        return;
    }

    public final com.a.b.c.ai d()
    {
        return this.f().d();
    }

    public final Object d(Object p2)
    {
        return this.f().d(p2);
    }

    public final java.util.concurrent.ConcurrentMap e()
    {
        return this.f().e();
    }

    protected abstract com.a.b.c.e f();

    protected synthetic Object k_()
    {
        return this.f();
    }
}
