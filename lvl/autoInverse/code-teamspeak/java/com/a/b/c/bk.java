package com.a.b.c;
final class bk extends com.a.b.c.bq implements com.a.b.c.an, java.io.Serializable {
    private static final long o = 1;
    transient com.a.b.c.an a;

    bk(com.a.b.c.ao p1)
    {
        this(p1);
        return;
    }

    private void a(java.io.ObjectInputStream p3)
    {
        p3.defaultReadObject();
        this.a = this.h().a(this.m);
        return;
    }

    private Object i()
    {
        return this.a;
    }

    public final Object b(Object p2)
    {
        return this.a.b(p2);
    }

    public final com.a.b.d.jt c(Iterable p2)
    {
        return this.a.c(p2);
    }

    public final void c(Object p2)
    {
        this.a.c(p2);
        return;
    }

    public final Object e(Object p2)
    {
        return this.a.e(p2);
    }

    public final Object f(Object p2)
    {
        return this.a.f(p2);
    }
}
