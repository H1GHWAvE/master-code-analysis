package com.a.b.i;
public abstract class ag {

    protected ag()
    {
        return;
    }

    private long a(Readable p5)
    {
        com.a.b.b.cn.a(p5);
        com.a.b.i.ar v1 = com.a.b.i.ar.a();
        try {
            RuntimeException v0_2 = ((java.io.Writer) v1.a(this.a()));
            long v2 = com.a.b.i.an.a(p5, v0_2);
            v0_2.flush();
            v1.close();
            return v2;
        } catch (RuntimeException v0_5) {
            v1.close();
            throw v0_5;
        } catch (RuntimeException v0_3) {
            throw v1.a(v0_3);
        }
    }

    private void a(Iterable p6)
    {
        String v2 = System.getProperty("line.separator");
        com.a.b.b.cn.a(p6);
        com.a.b.b.cn.a(v2);
        com.a.b.i.ar v3 = com.a.b.i.ar.a();
        try {
            java.io.Writer v0_2;
            java.io.Writer v0_1 = this.a();
        } catch (java.io.Writer v0_7) {
            v3.close();
            throw v0_7;
        } catch (java.io.Writer v0_5) {
            throw v3.a(v0_5);
        }
        if (!(v0_1 instanceof java.io.BufferedWriter)) {
            v0_2 = new java.io.BufferedWriter(v0_1);
        } else {
            v0_2 = ((java.io.BufferedWriter) v0_1);
        }
        java.io.Writer v0_4 = ((java.io.Writer) v3.a(v0_2));
        java.util.Iterator v4 = p6.iterator();
        while (v4.hasNext()) {
            v0_4.append(((CharSequence) v4.next())).append(v2);
        }
        v0_4.flush();
        v3.close();
        return;
    }

    private void a(Iterable p5, String p6)
    {
        com.a.b.b.cn.a(p5);
        com.a.b.b.cn.a(p6);
        com.a.b.i.ar v2 = com.a.b.i.ar.a();
        try {
            java.io.Writer v0_1;
            java.io.Writer v0_0 = this.a();
        } catch (java.io.Writer v0_6) {
            v2.close();
            throw v0_6;
        } catch (java.io.Writer v0_4) {
            throw v2.a(v0_4);
        }
        if (!(v0_0 instanceof java.io.BufferedWriter)) {
            v0_1 = new java.io.BufferedWriter(v0_0);
        } else {
            v0_1 = ((java.io.BufferedWriter) v0_0);
        }
        java.io.Writer v0_3 = ((java.io.Writer) v2.a(v0_1));
        java.util.Iterator v3 = p5.iterator();
        while (v3.hasNext()) {
            v0_3.append(((CharSequence) v3.next())).append(p6);
        }
        v0_3.flush();
        v2.close();
        return;
    }

    private java.io.Writer b()
    {
        java.io.BufferedWriter v0_1;
        java.io.BufferedWriter v0_0 = this.a();
        if (!(v0_0 instanceof java.io.BufferedWriter)) {
            v0_1 = new java.io.BufferedWriter(v0_0);
        } else {
            v0_1 = ((java.io.BufferedWriter) v0_0);
        }
        return v0_1;
    }

    public abstract java.io.Writer a();

    public final void a(CharSequence p3)
    {
        com.a.b.b.cn.a(p3);
        com.a.b.i.ar v1 = com.a.b.i.ar.a();
        try {
            RuntimeException v0_2 = ((java.io.Writer) v1.a(this.a()));
            v0_2.append(p3);
            v0_2.flush();
            v1.close();
            return;
        } catch (RuntimeException v0_3) {
            throw v1.a(v0_3);
        } catch (RuntimeException v0_5) {
            v1.close();
            throw v0_5;
        }
    }
}
