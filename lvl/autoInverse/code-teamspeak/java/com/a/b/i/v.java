package com.a.b.i;
 class v extends com.a.b.i.s {
    protected final byte[] a;

    protected v(byte[] p2)
    {
        this.a = ((byte[]) com.a.b.b.cn.a(p2));
        return;
    }

    public final long a(java.io.OutputStream p3)
    {
        p3.write(this.a);
        return ((long) this.a.length);
    }

    public final com.a.b.g.ag a(com.a.b.g.ak p2)
    {
        return p2.a(this.a);
    }

    public final java.io.InputStream a()
    {
        return new java.io.ByteArrayInputStream(this.a);
    }

    public final Object a(com.a.b.i.o p2)
    {
        return p2.b();
    }

    public final java.io.InputStream b()
    {
        return this.a();
    }

    public final boolean c()
    {
        int v0_2;
        if (this.a.length != 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final long d()
    {
        return ((long) this.a.length);
    }

    public byte[] e()
    {
        return ((byte[]) this.a.clone());
    }

    public String toString()
    {
        String v2_0 = this.a;
        String v0_5 = String.valueOf(String.valueOf(com.a.b.b.e.a(com.a.b.i.b.e().a(((byte[]) com.a.b.b.cn.a(v2_0)), v2_0.length), "...")));
        return new StringBuilder((v0_5.length() + 17)).append("ByteSource.wrap(").append(v0_5).append(")").toString();
    }
}
