package com.a.b.i;
final class bl {

    private bl()
    {
        return;
    }

    private static com.a.b.i.bu a(java.io.Reader p1)
    {
        com.a.b.b.cn.a(p1);
        return new com.a.b.i.bm(p1);
    }

    private static com.a.b.i.bu a(CharSequence p1)
    {
        com.a.b.b.cn.a(p1);
        return new com.a.b.i.bn(p1);
    }

    private static com.a.b.i.bv a(int p2)
    {
        return new com.a.b.i.br(new StringBuilder(p2));
    }

    private static com.a.b.i.bv a(java.io.Writer p1)
    {
        com.a.b.b.cn.a(p1);
        return new com.a.b.i.bq(p1);
    }

    private static java.io.InputStream a(com.a.b.i.bs p1)
    {
        com.a.b.b.cn.a(p1);
        return new com.a.b.i.bo(p1);
    }

    private static java.io.OutputStream a(com.a.b.i.bt p1)
    {
        com.a.b.b.cn.a(p1);
        return new com.a.b.i.bp(p1);
    }
}
