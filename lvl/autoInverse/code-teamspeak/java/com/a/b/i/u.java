package com.a.b.i;
final class u extends com.a.b.i.ah {
    final synthetic com.a.b.i.s a;
    private final java.nio.charset.Charset b;

    private u(com.a.b.i.s p2, java.nio.charset.Charset p3)
    {
        this.a = p2;
        this.b = ((java.nio.charset.Charset) com.a.b.b.cn.a(p3));
        return;
    }

    synthetic u(com.a.b.i.s p1, java.nio.charset.Charset p2, byte p3)
    {
        this(p1, p2);
        return;
    }

    public final java.io.Reader a()
    {
        return new java.io.InputStreamReader(this.a.a(), this.b);
    }

    public final String toString()
    {
        String v0_3 = String.valueOf(String.valueOf(this.a.toString()));
        String v1_2 = String.valueOf(String.valueOf(this.b));
        return new StringBuilder(((v0_3.length() + 15) + v1_2.length())).append(v0_3).append(".asCharSource(").append(v1_2).append(")").toString();
    }
}
