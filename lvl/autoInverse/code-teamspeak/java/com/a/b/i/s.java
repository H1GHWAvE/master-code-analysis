package com.a.b.i;
public abstract class s {
    private static final int a = 4096;
    private static final byte[] b;

    static s()
    {
        byte[] v0_1 = new byte[4096];
        com.a.b.i.s.b = v0_1;
        return;
    }

    protected s()
    {
        return;
    }

    private static long a(java.io.InputStream p7)
    {
        java.io.IOException v0_0 = 0;
        while(true) {
            long v4_3 = p7.skip(((long) Math.min(p7.available(), 2147483647)));
            if (v4_3 > 0) {
                v0_0 += v4_3;
            } else {
                if (p7.read() != -1) {
                    if ((v0_0 == 0) && (p7.available() == 0)) {
                        break;
                    }
                    v0_0++;
                } else {
                    return v0_0;
                }
            }
        }
        throw new java.io.IOException();
    }

    private static com.a.b.i.s a(Iterable p1)
    {
        return new com.a.b.i.w(p1);
    }

    private static com.a.b.i.s a(java.util.Iterator p1)
    {
        return com.a.b.i.s.a(com.a.b.d.jl.a(p1));
    }

    private static com.a.b.i.s a(byte[] p1)
    {
        return new com.a.b.i.v(p1);
    }

    private static varargs com.a.b.i.s a(com.a.b.i.s[] p1)
    {
        return com.a.b.i.s.a(com.a.b.d.jl.a(p1));
    }

    private static long b(java.io.InputStream p6)
    {
        long v0 = 0;
        while(true) {
            long v2_2 = ((long) p6.read(com.a.b.i.s.b));
            if (v2_2 == -1) {
                break;
            }
            v0 += v2_2;
        }
        return v0;
    }

    private static com.a.b.i.s f()
    {
        return com.a.b.i.x.f();
    }

    public final long a(com.a.b.i.p p4)
    {
        com.a.b.b.cn.a(p4);
        com.a.b.i.ar v2 = com.a.b.i.ar.a();
        try {
            RuntimeException v0_3 = com.a.b.i.z.a(((java.io.InputStream) v2.a(this.a())), ((java.io.OutputStream) v2.a(p4.a())));
            v2.close();
            return v0_3;
        } catch (RuntimeException v0_6) {
            v2.close();
            throw v0_6;
        } catch (RuntimeException v0_4) {
            throw v2.a(v0_4);
        }
    }

    public long a(java.io.OutputStream p5)
    {
        com.a.b.b.cn.a(p5);
        com.a.b.i.ar v1 = com.a.b.i.ar.a();
        try {
            long v2 = com.a.b.i.z.a(((java.io.InputStream) v1.a(this.a())), p5);
            v1.close();
            return v2;
        } catch (RuntimeException v0_3) {
            throw v1.a(v0_3);
        } catch (RuntimeException v0_5) {
            v1.close();
            throw v0_5;
        }
    }

    public com.a.b.g.ag a(com.a.b.g.ak p3)
    {
        com.a.b.g.ag v0_0 = p3.a();
        this.a(new com.a.b.g.ac(v0_0));
        return v0_0.a();
    }

    public com.a.b.i.ah a(java.nio.charset.Charset p3)
    {
        return new com.a.b.i.u(this, p3, 0);
    }

    public com.a.b.i.s a(long p8, long p10)
    {
        return new com.a.b.i.y(this, p8, p10, 0);
    }

    public abstract java.io.InputStream a();

    public Object a(com.a.b.i.o p3)
    {
        com.a.b.b.cn.a(p3);
        com.a.b.i.ar v1 = com.a.b.i.ar.a();
        try {
            RuntimeException v0_3 = com.a.b.i.z.a(((java.io.InputStream) v1.a(this.a())), p3);
            v1.close();
            return v0_3;
        } catch (RuntimeException v0_4) {
            throw v1.a(v0_4);
        } catch (RuntimeException v0_6) {
            v1.close();
            throw v0_6;
        }
    }

    public final boolean a(com.a.b.i.s p11)
    {
        com.a.b.b.cn.a(p11);
        byte[] v3 = new byte[4096];
        byte[] v4 = new byte[4096];
        com.a.b.i.ar v5 = com.a.b.i.ar.a();
        try {
            java.io.InputStream v1_2 = ((java.io.InputStream) v5.a(p11.a()));
        } catch (int v0_5) {
            v5.close();
            throw v0_5;
        } catch (int v0_3) {
            throw v5.a(v0_3);
        }
        do {
            int v0_6;
            int v6_1 = com.a.b.i.z.b(((java.io.InputStream) v5.a(this.a())), v3, 0, 4096);
            if ((v6_1 == com.a.b.i.z.b(v1_2, v4, 0, 4096)) && (java.util.Arrays.equals(v3, v4))) {
            } else {
                v5.close();
                v0_6 = 0;
            }
            return v0_6;
        } while(v6_1 == 4096);
        v5.close();
        v0_6 = 1;
        return v0_6;
    }

    public java.io.InputStream b()
    {
        java.io.BufferedInputStream v0_1;
        java.io.BufferedInputStream v0_0 = this.a();
        if (!(v0_0 instanceof java.io.BufferedInputStream)) {
            v0_1 = new java.io.BufferedInputStream(v0_0);
        } else {
            v0_1 = ((java.io.BufferedInputStream) v0_0);
        }
        return v0_1;
    }

    public boolean c()
    {
        com.a.b.i.ar v1 = com.a.b.i.ar.a();
        try {
            RuntimeException v0_7;
            if (((java.io.InputStream) v1.a(this.a())).read() != -1) {
                v0_7 = 0;
            } else {
                v0_7 = 1;
            }
        } catch (RuntimeException v0_6) {
            v1.close();
            throw v0_6;
        } catch (RuntimeException v0_4) {
            throw v1.a(v0_4);
        }
        v1.close();
        return v0_7;
    }

    public long d()
    {
        com.a.b.i.ar v1 = com.a.b.i.ar.a();
        try {
            java.io.IOException v0_2 = ((java.io.InputStream) v1.a(this.a()));
            long v2_0 = 0;
        } catch (java.io.IOException v0_12) {
            v1.close();
            throw v0_12;
        } catch (java.io.IOException v0) {
            v1.close();
            long v2_1 = com.a.b.i.ar.a();
            try {
                java.io.IOException v0_8 = com.a.b.i.s.b(((java.io.InputStream) v2_1.a(this.a())));
                v2_1.close();
            } catch (java.io.IOException v0_9) {
                throw v2_1.a(v0_9);
            } catch (java.io.IOException v0_11) {
                v2_1.close();
                throw v0_11;
            }
            return v0_8;
        }
        while(true) {
            long v6_3 = v0_2.skip(((long) Math.min(v0_2.available(), 2147483647)));
            if (v6_3 > 0) {
                v2_0 += v6_3;
            } else {
                if (v0_2.read() != -1) {
                    if ((v2_0 == 0) && (v0_2.available() == 0)) {
                        break;
                    }
                    v2_0++;
                } else {
                    v1.close();
                    v0_8 = v2_0;
                    return v0_8;
                }
            }
        }
        throw new java.io.IOException();
    }

    public byte[] e()
    {
        com.a.b.i.ar v1 = com.a.b.i.ar.a();
        try {
            RuntimeException v0_3 = com.a.b.i.z.a(((java.io.InputStream) v1.a(this.a())));
            v1.close();
            return v0_3;
        } catch (RuntimeException v0_6) {
            v1.close();
            throw v0_6;
        } catch (RuntimeException v0_4) {
            throw v1.a(v0_4);
        }
    }
}
