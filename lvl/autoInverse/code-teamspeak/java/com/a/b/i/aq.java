package com.a.b.i;
public final class aq {
    static final java.util.logging.Logger a;

    static aq()
    {
        com.a.b.i.aq.a = java.util.logging.Logger.getLogger(com.a.b.i.aq.getName());
        return;
    }

    private aq()
    {
        return;
    }

    private static void a(java.io.Closeable p4)
    {
        if (p4 != null) {
            try {
                p4.close();
            } catch (java.io.IOException v0) {
                com.a.b.i.aq.a.log(java.util.logging.Level.WARNING, "IOException thrown while closing Closeable.", v0);
            }
        }
        return;
    }

    private static void a(java.io.InputStream p2)
    {
        try {
            com.a.b.i.aq.a(p2);
            return;
        } catch (java.io.IOException v0) {
            throw new AssertionError(v0);
        }
    }

    private static void a(java.io.Reader p2)
    {
        try {
            com.a.b.i.aq.a(p2);
            return;
        } catch (java.io.IOException v0) {
            throw new AssertionError(v0);
        }
    }
}
