package com.a.b.i;
public abstract class ah {

    protected ah()
    {
        return;
    }

    private long a(com.a.b.i.ag p4)
    {
        com.a.b.b.cn.a(p4);
        com.a.b.i.ar v2 = com.a.b.i.ar.a();
        try {
            RuntimeException v0_3 = com.a.b.i.an.a(((java.io.Reader) v2.a(this.a())), ((java.io.Writer) v2.a(p4.a())));
            v2.close();
            return v0_3;
        } catch (RuntimeException v0_4) {
            throw v2.a(v0_4);
        } catch (RuntimeException v0_6) {
            v2.close();
            throw v0_6;
        }
    }

    private static com.a.b.i.ah a(CharSequence p1)
    {
        return new com.a.b.i.ai(p1);
    }

    private static com.a.b.i.ah a(Iterable p1)
    {
        return new com.a.b.i.al(p1);
    }

    private static com.a.b.i.ah a(java.util.Iterator p1)
    {
        return com.a.b.i.ah.a(com.a.b.d.jl.a(p1));
    }

    private static varargs com.a.b.i.ah a(com.a.b.i.ah[] p1)
    {
        return com.a.b.i.ah.a(com.a.b.d.jl.a(p1));
    }

    private java.io.BufferedReader f()
    {
        java.io.BufferedReader v0_1;
        java.io.BufferedReader v0_0 = this.a();
        if (!(v0_0 instanceof java.io.BufferedReader)) {
            v0_1 = new java.io.BufferedReader(v0_0);
        } else {
            v0_1 = ((java.io.BufferedReader) v0_0);
        }
        return v0_1;
    }

    private static com.a.b.i.ah g()
    {
        return com.a.b.i.am.g();
    }

    public final long a(Appendable p5)
    {
        com.a.b.b.cn.a(p5);
        com.a.b.i.ar v1 = com.a.b.i.ar.a();
        try {
            long v2 = com.a.b.i.an.a(((java.io.Reader) v1.a(this.a())), p5);
            v1.close();
            return v2;
        } catch (RuntimeException v0_3) {
            throw v1.a(v0_3);
        } catch (RuntimeException v0_5) {
            v1.close();
            throw v0_5;
        }
    }

    public abstract java.io.Reader a();

    public Object a(com.a.b.i.by p4)
    {
        com.a.b.b.cn.a(p4);
        com.a.b.i.ar v1 = com.a.b.i.ar.a();
        try {
            Object v0_2 = ((java.io.Reader) v1.a(this.a()));
            com.a.b.b.cn.a(v0_2);
            com.a.b.b.cn.a(p4);
            com.a.b.i.bz v2_1 = new com.a.b.i.bz(v0_2);
        } catch (Object v0_5) {
            throw v1.a(v0_5);
        } catch (Object v0_7) {
            v1.close();
            throw v0_7;
        }
        while(true) {
            Object v0_3 = v2_1.a();
            if (v0_3 == null) {
                break;
            }
            p4.a(v0_3);
        }
        Object v0_4 = p4.a();
        v1.close();
        return v0_4;
    }

    public String b()
    {
        com.a.b.i.ar v1 = com.a.b.i.ar.a();
        try {
            RuntimeException v0_2 = ((java.io.Reader) v1.a(this.a()));
            StringBuilder v2_1 = new StringBuilder();
            com.a.b.i.an.a(v0_2, v2_1);
            RuntimeException v0_3 = v2_1.toString();
            v1.close();
            return v0_3;
        } catch (RuntimeException v0_6) {
            v1.close();
            throw v0_6;
        } catch (RuntimeException v0_4) {
            throw v1.a(v0_4);
        }
    }

    public String c()
    {
        com.a.b.i.ar v1 = com.a.b.i.ar.a();
        try {
            RuntimeException v0_3 = ((java.io.BufferedReader) v1.a(this.f())).readLine();
            v1.close();
            return v0_3;
        } catch (RuntimeException v0_4) {
            throw v1.a(v0_4);
        } catch (RuntimeException v0_6) {
            v1.close();
            throw v0_6;
        }
    }

    public com.a.b.d.jl d()
    {
        com.a.b.i.ar v1 = com.a.b.i.ar.a();
        try {
            com.a.b.d.jl v0_2 = ((java.io.BufferedReader) v1.a(this.f()));
            java.util.ArrayList v2_1 = new java.util.ArrayList();
        } catch (com.a.b.d.jl v0_4) {
            throw v1.a(v0_4);
        } catch (com.a.b.d.jl v0_6) {
            v1.close();
            throw v0_6;
        }
        while(true) {
            String v3 = v0_2.readLine();
            if (v3 == null) {
                break;
            }
            v2_1.add(v3);
        }
        com.a.b.d.jl v0_3 = com.a.b.d.jl.a(v2_1);
        v1.close();
        return v0_3;
    }

    public boolean e()
    {
        com.a.b.i.ar v1 = com.a.b.i.ar.a();
        try {
            RuntimeException v0_7;
            if (((java.io.Reader) v1.a(this.a())).read() != -1) {
                v0_7 = 0;
            } else {
                v0_7 = 1;
            }
        } catch (RuntimeException v0_4) {
            throw v1.a(v0_4);
        } catch (RuntimeException v0_6) {
            v1.close();
            throw v0_6;
        }
        v1.close();
        return v0_7;
    }
}
