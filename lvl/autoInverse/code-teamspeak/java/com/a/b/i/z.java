package com.a.b.i;
public final class z {
    private static final int a = 4096;
    private static final java.io.OutputStream b;

    static z()
    {
        com.a.b.i.z.b = new com.a.b.i.aa();
        return;
    }

    private z()
    {
        return;
    }

    public static long a(java.io.InputStream p6, java.io.OutputStream p7)
    {
        com.a.b.b.cn.a(p6);
        com.a.b.b.cn.a(p7);
        byte[] v2 = new byte[4096];
        long v0_1 = 0;
        while(true) {
            int v3 = p6.read(v2);
            if (v3 == -1) {
                break;
            }
            p7.write(v2, 0, v3);
            v0_1 += ((long) v3);
        }
        return v0_1;
    }

    private static long a(java.nio.channels.ReadableByteChannel p6, java.nio.channels.WritableByteChannel p7)
    {
        com.a.b.b.cn.a(p6);
        com.a.b.b.cn.a(p7);
        java.nio.ByteBuffer v2 = java.nio.ByteBuffer.allocate(4096);
        long v0_1 = 0;
        while (p6.read(v2) != -1) {
            v2.flip();
            while (v2.hasRemaining()) {
                v0_1 += ((long) p7.write(v2));
            }
            v2.clear();
        }
        return v0_1;
    }

    private static com.a.b.i.m a(java.io.ByteArrayInputStream p2)
    {
        return new com.a.b.i.ab(((java.io.ByteArrayInputStream) com.a.b.b.cn.a(p2)));
    }

    public static com.a.b.i.m a(byte[] p1)
    {
        return com.a.b.i.z.a(new java.io.ByteArrayInputStream(p1));
    }

    public static com.a.b.i.m a(byte[] p2, int p3)
    {
        com.a.b.b.cn.b(p3, p2.length);
        return com.a.b.i.z.a(new java.io.ByteArrayInputStream(p2, p3, (p2.length - p3)));
    }

    private static com.a.b.i.n a()
    {
        return com.a.b.i.z.a(new java.io.ByteArrayOutputStream());
    }

    private static com.a.b.i.n a(int p5)
    {
        com.a.b.i.n v0_0;
        if (p5 < 0) {
            v0_0 = 0;
        } else {
            v0_0 = 1;
        }
        Object[] v1_1 = new Object[1];
        v1_1[0] = Integer.valueOf(p5);
        com.a.b.b.cn.a(v0_0, "Invalid size: %s", v1_1);
        return com.a.b.i.z.a(new java.io.ByteArrayOutputStream(p5));
    }

    private static com.a.b.i.n a(java.io.ByteArrayOutputStream p2)
    {
        return new com.a.b.i.ac(((java.io.ByteArrayOutputStream) com.a.b.b.cn.a(p2)));
    }

    public static java.io.InputStream a(java.io.InputStream p1, long p2)
    {
        return new com.a.b.i.ae(p1, p2);
    }

    public static Object a(java.io.InputStream p3, com.a.b.i.o p4)
    {
        com.a.b.b.cn.a(p3);
        com.a.b.b.cn.a(p4);
        Object v0_1 = new byte[4096];
        while (p3.read(v0_1) != -1) {
            if (!p4.a()) {
                break;
            }
        }
        return p4.b();
    }

    public static void a(java.io.InputStream p2, byte[] p3)
    {
        com.a.b.i.z.a(p2, p3, 0, p3.length);
        return;
    }

    public static void a(java.io.InputStream p4, byte[] p5, int p6, int p7)
    {
        String v0_0 = com.a.b.i.z.b(p4, p5, p6, p7);
        if (v0_0 == p7) {
            return;
        } else {
            throw new java.io.EOFException(new StringBuilder(81).append("reached end of stream after reading ").append(v0_0).append(" bytes; ").append(p7).append(" bytes expected").toString());
        }
    }

    public static byte[] a(java.io.InputStream p1)
    {
        byte[] v0_1 = new java.io.ByteArrayOutputStream();
        com.a.b.i.z.a(p1, v0_1);
        return v0_1.toByteArray();
    }

    static byte[] a(java.io.InputStream p6, int p7)
    {
        byte[] v0_0 = new byte[p7];
        byte[] v1_0 = p7;
        while (v1_0 > null) {
            com.a.b.i.ad v2_2 = (p7 - v1_0);
            int v3_2 = p6.read(v0_0, v2_2, v1_0);
            if (v3_2 != -1) {
                v1_0 -= v3_2;
            } else {
                v0_0 = java.util.Arrays.copyOf(v0_0, v2_2);
            }
            return v0_0;
        }
        byte[] v1_1 = p6.read();
        if (v1_1 != -1) {
            com.a.b.i.ad v2_1 = new com.a.b.i.ad(0);
            v2_1.write(v1_1);
            com.a.b.i.z.a(p6, v2_1);
            byte[] v1_4 = new byte[(v0_0.length + v2_1.size())];
            System.arraycopy(v0_0, 0, v1_4, 0, v0_0.length);
            v2_1.a(v1_4, v0_0.length);
            v0_0 = v1_4;
        }
        return v0_0;
    }

    public static int b(java.io.InputStream p3, byte[] p4, int p5, int p6)
    {
        com.a.b.b.cn.a(p3);
        com.a.b.b.cn.a(p4);
        if (p6 >= 0) {
            int v0_0 = 0;
            while (v0_0 < p6) {
                int v1_1 = p3.read(p4, (p5 + v0_0), (p6 - v0_0));
                if (v1_1 == -1) {
                    break;
                }
                v0_0 += v1_1;
            }
            return v0_0;
        } else {
            throw new IndexOutOfBoundsException("len is negative");
        }
    }

    private static java.io.OutputStream b()
    {
        return com.a.b.i.z.b;
    }

    public static void b(java.io.InputStream p9, long p10)
    {
        long v0_0 = p10;
        while (v0_0 > 0) {
            long v2_1 = p9.skip(v0_0);
            if (v2_1 != 0) {
                v0_0 -= v2_1;
            } else {
                if (p9.read() != -1) {
                    v0_0--;
                } else {
                    throw new java.io.EOFException(new StringBuilder(100).append("reached end of stream after skipping ").append((p10 - v0_0)).append(" bytes; ").append(p10).append(" bytes expected").toString());
                }
            }
        }
        return;
    }
}
