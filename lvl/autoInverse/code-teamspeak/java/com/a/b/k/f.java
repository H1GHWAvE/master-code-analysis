package com.a.b.k;
public final class f {
    private static final com.a.b.b.m a = None;
    private static final com.a.b.b.di b = None;
    private static final com.a.b.b.bv c = None;
    private static final int d = 255;
    private static final String e = "\.";
    private static final int f = 127;
    private static final int g = 253;
    private static final int h = 63;
    private static final com.a.b.b.m l;
    private static final com.a.b.b.m m;
    private final String i;
    private final com.a.b.d.jl j;
    private final int k;

    static f()
    {
        com.a.b.k.f.a = com.a.b.b.m.a(".\u3002\uff0e\uff61");
        com.a.b.k.f.b = com.a.b.b.di.a(46);
        com.a.b.k.f.c = com.a.b.b.bv.a(46);
        com.a.b.k.f.l = com.a.b.b.m.a("-_");
        com.a.b.k.f.m = com.a.b.b.m.f.b(com.a.b.k.f.l);
        return;
    }

    private f(String p7)
    {
        int v0_2 = com.a.b.b.e.a(com.a.b.k.f.a.a(p7, 46));
        if (v0_2.endsWith(".")) {
            v0_2 = v0_2.substring(0, (v0_2.length() - 1));
        }
        boolean v1_6;
        if (v0_2.length() > 253) {
            v1_6 = 0;
        } else {
            v1_6 = 1;
        }
        boolean v1_12;
        Object[] v5_0 = new Object[1];
        v5_0[0] = v0_2;
        com.a.b.b.cn.a(v1_6, "Domain name too long: \'%s\':", v5_0);
        this.i = v0_2;
        this.j = com.a.b.d.jl.a(com.a.b.k.f.b.a(v0_2));
        if (this.j.size() > 127) {
            v1_12 = 0;
        } else {
            v1_12 = 1;
        }
        Object[] v5_1 = new Object[1];
        v5_1[0] = v0_2;
        com.a.b.b.cn.a(v1_12, "Domain has too many parts: \'%s\'", v5_1);
        boolean v1_14 = com.a.b.k.f.a(this.j);
        Object[] v2_1 = new Object[1];
        v2_1[0] = v0_2;
        com.a.b.b.cn.a(v1_14, "Not a valid domain name: \'%s\'", v2_1);
        this.k = this.b();
        return;
    }

    private com.a.b.k.f a(int p4)
    {
        return com.a.b.k.f.a(com.a.b.k.f.c.a(this.j.a(p4, this.j.size())));
    }

    public static com.a.b.k.f a(String p2)
    {
        return new com.a.b.k.f(((String) com.a.b.b.cn.a(p2)));
    }

    private static boolean a(String p3, boolean p4)
    {
        int v0 = 0;
        if ((p3.length() > 0) && ((p3.length() <= 63) && ((com.a.b.k.f.m.c(com.a.b.b.m.b.a().h(p3))) && ((!com.a.b.k.f.l.c(p3.charAt(0))) && ((!com.a.b.k.f.l.c(p3.charAt((p3.length() - 1)))) && ((!p4) || (!com.a.b.b.m.c.c(p3.charAt(0))))))))) {
            v0 = 1;
        }
        return v0;
    }

    private static boolean a(java.util.List p5)
    {
        int v0_4;
        int v4 = (p5.size() - 1);
        if (com.a.b.k.f.a(((String) p5.get(v4)), 1)) {
            int v3 = 0;
            while (v3 < v4) {
                if (com.a.b.k.f.a(((String) p5.get(v3)), 0)) {
                    v3++;
                } else {
                    v0_4 = 0;
                }
            }
            v0_4 = 1;
        } else {
            v0_4 = 0;
        }
        return v0_4;
    }

    private int b()
    {
        int v4 = this.j.size();
        int v0_1 = 0;
        while (v0_1 < v4) {
            int v1_1 = com.a.b.k.f.c.a(this.j.a(v0_1, v4));
            if (!com.a.d.a.a.a.containsKey(v1_1)) {
                if (!com.a.d.a.a.c.containsKey(v1_1)) {
                    int v1_5;
                    int v1_2 = v1_1.split("\\.", 2);
                    if ((v1_2.length != 2) || (!com.a.d.a.a.b.containsKey(v1_2[1]))) {
                        v1_5 = 0;
                    } else {
                        v1_5 = 1;
                    }
                    if (v1_5 == 0) {
                        v0_1++;
                    }
                } else {
                    v0_1++;
                }
            }
            return v0_1;
        }
        v0_1 = -1;
        return v0_1;
    }

    private com.a.b.k.f b(String p6)
    {
        com.a.b.k.f v0_3 = String.valueOf(String.valueOf(((String) com.a.b.b.cn.a(p6))));
        String v1_2 = String.valueOf(String.valueOf(this.i));
        return com.a.b.k.f.a(new StringBuilder(((v0_3.length() + 1) + v1_2.length())).append(v0_3).append(".").append(v1_2).toString());
    }

    private com.a.b.d.jl c()
    {
        return this.j;
    }

    private static boolean c(String p1)
    {
        try {
            com.a.b.k.f.a(p1);
            int v0 = 1;
        } catch (int v0) {
            v0 = 0;
        }
        return v0;
    }

    private boolean d()
    {
        int v0_1;
        if (this.k != 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    private static boolean d(String p4)
    {
        int v0 = 1;
        boolean v1_1 = p4.split("\\.", 2);
        if ((v1_1.length != 2) || (!com.a.d.a.a.b.containsKey(v1_1[1]))) {
            v0 = 0;
        }
        return v0;
    }

    private com.a.b.k.f e()
    {
        int v0_1;
        if (!this.a()) {
            v0_1 = 0;
        } else {
            v0_1 = this.a(this.k);
        }
        return v0_1;
    }

    private boolean f()
    {
        int v0_1;
        if (this.k <= 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    private boolean g()
    {
        int v0 = 1;
        if (this.k != 1) {
            v0 = 0;
        }
        return v0;
    }

    private com.a.b.k.f h()
    {
        int v0_1;
        if (this.k != 1) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        if (v0_1 == 0) {
            int v0_3;
            if (this.k <= 0) {
                v0_3 = 0;
            } else {
                v0_3 = 1;
            }
            Object[] v1_1 = new Object[1];
            v1_1[0] = this.i;
            com.a.b.b.cn.b(v0_3, "Not under a public suffix: %s", v1_1);
            this = this.a((this.k - 1));
        }
        return this;
    }

    private boolean i()
    {
        int v0 = 1;
        if (this.j.size() <= 1) {
            v0 = 0;
        }
        return v0;
    }

    private com.a.b.k.f j()
    {
        com.a.b.k.f v0_2;
        if (this.j.size() <= 1) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        Object[] v4 = new Object[1];
        v4[0] = this.i;
        com.a.b.b.cn.b(v0_2, "Domain \'%s\' has no parent", v4);
        return this.a(1);
    }

    public final boolean a()
    {
        int v0_1;
        if (this.k == -1) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final boolean equals(Object p3)
    {
        int v0_1;
        if (p3 != this) {
            if (!(p3 instanceof com.a.b.k.f)) {
                v0_1 = 0;
            } else {
                v0_1 = this.i.equals(((com.a.b.k.f) p3).i);
            }
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final int hashCode()
    {
        return this.i.hashCode();
    }

    public final String toString()
    {
        return this.i;
    }
}
