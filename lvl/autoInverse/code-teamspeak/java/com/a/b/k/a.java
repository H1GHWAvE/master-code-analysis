package com.a.b.k;
public final class a implements java.io.Serializable {
    private static final int b = 255;
    private static final long e;
    final String a;
    private final int c;
    private final boolean d;

    private a(String p1, int p2, boolean p3)
    {
        this.a = p1;
        this.c = p2;
        this.d = p3;
        return;
    }

    private int a(int p2)
    {
        if (this.a()) {
            p2 = this.c;
        }
        return p2;
    }

    public static com.a.b.k.a a(String p10)
    {
        char v4_2;
        String v0_3;
        com.a.b.b.cn.a(p10);
        String v3_0 = 0;
        if (!p10.startsWith("[")) {
            String v0_2 = p10.indexOf(58);
            if ((v0_2 < null) || (p10.indexOf(58, (v0_2 + 1)) != -1)) {
                if (v0_2 < null) {
                    v0_3 = 0;
                } else {
                    v0_3 = 1;
                }
                v4_2 = p10;
            } else {
                v4_2 = p10.substring(0, v0_2);
                v3_0 = p10.substring((v0_2 + 1));
                v0_3 = 0;
            }
        } else {
            String v0_7;
            if (p10.charAt(0) != 91) {
                v0_7 = 0;
            } else {
                v0_7 = 1;
            }
            String v0_9;
            char v4_3 = new Object[1];
            v4_3[0] = p10;
            com.a.b.b.cn.a(v0_7, "Bracketed host-port string must start with a bracket: %s", v4_3);
            String v0_8 = p10.indexOf(58);
            String v3_5 = p10.lastIndexOf(93);
            if ((v0_8 < null) || (v3_5 <= v0_8)) {
                v0_9 = 0;
            } else {
                v0_9 = 1;
            }
            String v0_15;
            boolean v6_0 = new Object[1];
            v6_0[0] = p10;
            com.a.b.b.cn.a(v0_9, "Invalid bracketed host/port: %s", v6_0);
            char v4_5 = p10.substring(1, v3_5);
            if ((v3_5 + 1) != p10.length()) {
                String v0_13;
                if (p10.charAt((v3_5 + 1)) != 58) {
                    v0_13 = 0;
                } else {
                    v0_13 = 1;
                }
                String v7_1 = new Object[1];
                v7_1[0] = p10;
                com.a.b.b.cn.a(v0_13, "Only a colon may follow a close bracket: %s", v7_1);
                String v0_14 = (v3_5 + 2);
                while (v0_14 < p10.length()) {
                    boolean v6_5 = Character.isDigit(p10.charAt(v0_14));
                    Object[] v8 = new Object[1];
                    v8[0] = p10;
                    com.a.b.b.cn.a(v6_5, "Port must be numeric: %s", v8);
                    v0_14++;
                }
                v0_15 = new String[2];
                v0_15[0] = v4_5;
                v0_15[1] = p10.substring((v3_5 + 2));
            } else {
                v0_15 = new String[2];
                v0_15[0] = v4_5;
                v0_15[1] = "";
            }
            v4_2 = v0_15[0];
            v3_0 = v0_15[1];
            v0_3 = 0;
        }
        IllegalArgumentException v1_1;
        if (com.a.b.b.dy.a(v3_0)) {
            v1_1 = -1;
        } else {
            boolean v5_3;
            if (v3_0.startsWith("+")) {
                v5_3 = 0;
            } else {
                v5_3 = 1;
            }
            String v7_3 = new Object[1];
            v7_3[0] = p10;
            com.a.b.b.cn.a(v5_3, "Unparseable port number: %s", v7_3);
            try {
                String v3_10 = Integer.parseInt(v3_0);
                boolean v5_4 = com.a.b.k.a.c(v3_10);
                IllegalArgumentException v1_2 = new Object[1];
                v1_2[0] = p10;
                com.a.b.b.cn.a(v5_4, "Port number out of range: %s", v1_2);
                v1_1 = v3_10;
            } catch (String v0) {
                String v0_17 = String.valueOf(p10);
                if (v0_17.length() == 0) {
                    String v0_19 = new String("Unparseable port number: ");
                } else {
                    v0_19 = "Unparseable port number: ".concat(v0_17);
                }
                throw new IllegalArgumentException(v0_19);
            }
        }
        return new com.a.b.k.a(v4_2, v1_1, v0_3);
    }

    private static com.a.b.k.a a(String p6, int p7)
    {
        com.a.b.k.a v0_2;
        com.a.b.k.a v0_0 = com.a.b.k.a.c(p7);
        String v4_0 = new Object[1];
        v4_0[0] = Integer.valueOf(p7);
        com.a.b.b.cn.a(v0_0, "Port out of range: %s", v4_0);
        com.a.b.k.a v3_1 = com.a.b.k.a.a(p6);
        if (v3_1.a()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        String v1_1 = new Object[1];
        v1_1[0] = p6;
        com.a.b.b.cn.a(v0_2, "Host has a port: %s", v1_1);
        return new com.a.b.k.a(v3_1.a, p7, v3_1.d);
    }

    private com.a.b.k.a b(int p4)
    {
        com.a.b.b.cn.a(com.a.b.k.a.c(p4));
        if ((!this.a()) && (this.c != p4)) {
            this = new com.a.b.k.a(this.a, p4, this.d);
        }
        return this;
    }

    private static com.a.b.k.a b(String p5)
    {
        int v0_1;
        com.a.b.k.a v3 = com.a.b.k.a.a(p5);
        if (v3.a()) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        Object[] v1_1 = new Object[1];
        v1_1[0] = p5;
        com.a.b.b.cn.a(v0_1, "Host has a port: %s", v1_1);
        return v3;
    }

    private String b()
    {
        return this.a;
    }

    private int c()
    {
        com.a.b.b.cn.b(this.a());
        return this.c;
    }

    private static boolean c(int p1)
    {
        if ((p1 < 0) || (p1 > 65535)) {
            int v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    private static String[] c(String p9)
    {
        String[] v0_1;
        if (p9.charAt(0) != 91) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        String[] v0_3;
        String v4_0 = new Object[1];
        v4_0[0] = p9;
        com.a.b.b.cn.a(v0_1, "Bracketed host-port string must start with a bracket: %s", v4_0);
        String[] v0_2 = p9.indexOf(58);
        int v3_3 = p9.lastIndexOf(93);
        if ((v0_2 < null) || (v3_3 <= v0_2)) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        String[] v0_9;
        boolean v5_0 = new Object[1];
        v5_0[0] = p9;
        com.a.b.b.cn.a(v0_3, "Invalid bracketed host/port: %s", v5_0);
        String v4_2 = p9.substring(1, v3_3);
        if ((v3_3 + 1) != p9.length()) {
            String[] v0_7;
            if (p9.charAt((v3_3 + 1)) != 58) {
                v0_7 = 0;
            } else {
                v0_7 = 1;
            }
            String v6_1 = new Object[1];
            v6_1[0] = p9;
            com.a.b.b.cn.a(v0_7, "Only a colon may follow a close bracket: %s", v6_1);
            String[] v0_8 = (v3_3 + 2);
            while (v0_8 < p9.length()) {
                boolean v5_5 = Character.isDigit(p9.charAt(v0_8));
                Object[] v7 = new Object[1];
                v7[0] = p9;
                com.a.b.b.cn.a(v5_5, "Port must be numeric: %s", v7);
                v0_8++;
            }
            v0_9 = new String[2];
            v0_9[0] = v4_2;
            v0_9[1] = p9.substring((v3_3 + 2));
        } else {
            v0_9 = new String[2];
            v0_9[0] = v4_2;
            v0_9[1] = "";
        }
        return v0_9;
    }

    private com.a.b.k.a d()
    {
        int v0_1;
        if (this.d) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        Object[] v1_1 = new Object[1];
        v1_1[0] = this.a;
        com.a.b.b.cn.a(v0_1, "Possible bracketless IPv6 literal: %s", v1_1);
        return this;
    }

    public final boolean a()
    {
        int v0_1;
        if (this.c < 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final boolean equals(Object p5)
    {
        int v0 = 1;
        if (this != p5) {
            if (!(p5 instanceof com.a.b.k.a)) {
                v0 = 0;
            } else {
                if ((!com.a.b.b.ce.a(this.a, ((com.a.b.k.a) p5).a)) || ((this.c != ((com.a.b.k.a) p5).c) || (this.d != ((com.a.b.k.a) p5).d))) {
                    v0 = 0;
                }
            }
        }
        return v0;
    }

    public final int hashCode()
    {
        int v0_1 = new Object[3];
        v0_1[0] = this.a;
        v0_1[1] = Integer.valueOf(this.c);
        v0_1[2] = Boolean.valueOf(this.d);
        return java.util.Arrays.hashCode(v0_1);
    }

    public final String toString()
    {
        String v0_1 = new StringBuilder((this.a.length() + 8));
        if (this.a.indexOf(58) < 0) {
            v0_1.append(this.a);
        } else {
            v0_1.append(91).append(this.a).append(93);
        }
        if (this.a()) {
            v0_1.append(58).append(this.c);
        }
        return v0_1.toString();
    }
}
