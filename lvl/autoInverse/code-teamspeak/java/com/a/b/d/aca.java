package com.a.b.d;
final class aca extends com.a.b.d.uj {
    final Object a;
    final synthetic com.a.b.d.abx b;

    aca(com.a.b.d.abx p2, Object p3)
    {
        this.b = p2;
        this.a = com.a.b.b.cn.a(p3);
        return;
    }

    final java.util.Set a()
    {
        return new com.a.b.d.acb(this, 0);
    }

    final boolean a(com.a.b.b.co p6)
    {
        java.util.Iterator v3 = this.b.a.entrySet().iterator();
        int v2 = 0;
        while (v3.hasNext()) {
            boolean v0_3 = ((java.util.Map$Entry) v3.next());
            java.util.Map v1_4 = ((java.util.Map) v0_3.getValue());
            Object v4_1 = v1_4.get(this.a);
            if ((v4_1 != null) && (p6.a(com.a.b.d.sz.a(v0_3.getKey(), v4_1)))) {
                v1_4.remove(this.a);
                v2 = 1;
                if (v1_4.isEmpty()) {
                    v3.remove();
                }
            }
        }
        return v2;
    }

    final java.util.Collection c_()
    {
        return new com.a.b.d.acf(this);
    }

    public final boolean containsKey(Object p3)
    {
        return this.b.a(p3, this.a);
    }

    final java.util.Set e()
    {
        return new com.a.b.d.ace(this);
    }

    public final Object get(Object p3)
    {
        return this.b.b(p3, this.a);
    }

    public final Object put(Object p3, Object p4)
    {
        return this.b.a(p3, this.a, p4);
    }

    public final Object remove(Object p3)
    {
        return this.b.c(p3, this.a);
    }
}
