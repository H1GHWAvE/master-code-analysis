package com.a.b.d;
abstract class en extends com.a.b.d.gy implements com.a.b.d.abn {
    private transient java.util.Comparator a;
    private transient java.util.NavigableSet b;
    private transient java.util.Set c;

    en()
    {
        return;
    }

    private java.util.Set q()
    {
        return new com.a.b.d.eo(this);
    }

    public final com.a.b.d.abn a(Object p2, com.a.b.d.ce p3, Object p4, com.a.b.d.ce p5)
    {
        return this.c().a(p4, p5, p2, p3).m();
    }

    public final java.util.Set a()
    {
        com.a.b.d.eo v0_0 = this.c;
        if (v0_0 == null) {
            v0_0 = new com.a.b.d.eo(this);
            this.c = v0_0;
        }
        return v0_0;
    }

    protected final synthetic java.util.Collection b()
    {
        return this.c();
    }

    abstract com.a.b.d.abn c();

    public final com.a.b.d.abn c(Object p2, com.a.b.d.ce p3)
    {
        return this.c().d(p2, p3).m();
    }

    public java.util.Comparator comparator()
    {
        com.a.b.d.yd v0_0 = this.a;
        if (v0_0 == null) {
            v0_0 = com.a.b.d.yd.a(this.c().comparator()).a();
            this.a = v0_0;
        }
        return v0_0;
    }

    public final com.a.b.d.abn d(Object p2, com.a.b.d.ce p3)
    {
        return this.c().c(p2, p3).m();
    }

    abstract java.util.Iterator e();

    public final java.util.NavigableSet e_()
    {
        com.a.b.d.abr v0_0 = this.b;
        if (v0_0 == null) {
            v0_0 = new com.a.b.d.abr(this);
            this.b = v0_0;
        }
        return v0_0;
    }

    protected final com.a.b.d.xc f()
    {
        return this.c();
    }

    public final com.a.b.d.xd h()
    {
        return this.c().i();
    }

    public final com.a.b.d.xd i()
    {
        return this.c().h();
    }

    public java.util.Iterator iterator()
    {
        return com.a.b.d.xe.b(this);
    }

    public final com.a.b.d.xd j()
    {
        return this.c().k();
    }

    public final com.a.b.d.xd k()
    {
        return this.c().j();
    }

    protected final synthetic Object k_()
    {
        return this.c();
    }

    public final com.a.b.d.abn m()
    {
        return this.c();
    }

    public final synthetic java.util.SortedSet n()
    {
        return this.e_();
    }

    public final synthetic java.util.Set n_()
    {
        return this.e_();
    }

    public Object[] toArray()
    {
        return this.p();
    }

    public Object[] toArray(Object[] p2)
    {
        return com.a.b.d.yc.a(this, p2);
    }

    public String toString()
    {
        return this.a().toString();
    }
}
