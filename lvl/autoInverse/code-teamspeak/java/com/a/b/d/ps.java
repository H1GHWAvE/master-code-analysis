package com.a.b.d;
final class ps extends com.a.b.d.hi {
    private final com.a.b.d.pn a;
    private final java.util.Set b;

    ps(java.util.Set p1, com.a.b.d.pn p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    static synthetic com.a.b.d.pn a(com.a.b.d.ps p1)
    {
        return p1.a;
    }

    protected final java.util.Set a()
    {
        return this.b;
    }

    protected final bridge synthetic java.util.Collection b()
    {
        return this.b;
    }

    public final boolean contains(Object p2)
    {
        return com.a.b.d.sz.a(this.b, p2);
    }

    public final boolean containsAll(java.util.Collection p2)
    {
        return com.a.b.d.cm.a(this, p2);
    }

    public final boolean equals(Object p2)
    {
        return com.a.b.d.aad.a(this, p2);
    }

    public final int hashCode()
    {
        return com.a.b.d.aad.a(this);
    }

    public final java.util.Iterator iterator()
    {
        return new com.a.b.d.pt(this, this.b.iterator());
    }

    protected final bridge synthetic Object k_()
    {
        return this.b;
    }

    public final boolean remove(Object p2)
    {
        return com.a.b.d.sz.b(this.b, p2);
    }

    public final boolean removeAll(java.util.Collection p2)
    {
        return this.b(p2);
    }

    public final boolean retainAll(java.util.Collection p2)
    {
        return this.c(p2);
    }

    public final Object[] toArray()
    {
        return this.p();
    }

    public final Object[] toArray(Object[] p2)
    {
        return com.a.b.d.yc.a(this, p2);
    }
}
