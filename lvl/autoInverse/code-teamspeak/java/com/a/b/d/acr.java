package com.a.b.d;
final class acr extends com.a.b.d.act {
    final synthetic com.a.b.d.acq a;

    acr(com.a.b.d.acq p3)
    {
        this.a = p3;
        this(p3.b, 0);
        return;
    }

    public final boolean contains(Object p3)
    {
        int v0 = 0;
        if (((p3 instanceof java.util.Map$Entry)) && ((((java.util.Map$Entry) p3).getKey() != null) && (((((java.util.Map$Entry) p3).getValue() instanceof java.util.Map)) && (com.a.b.d.cm.a(this.a.b.a.entrySet(), ((java.util.Map$Entry) p3)))))) {
            v0 = 1;
        }
        return v0;
    }

    public final java.util.Iterator iterator()
    {
        return com.a.b.d.sz.a(this.a.b.a.keySet(), new com.a.b.d.acs(this));
    }

    public final boolean remove(Object p3)
    {
        int v0 = 0;
        if (((p3 instanceof java.util.Map$Entry)) && ((((java.util.Map$Entry) p3).getKey() != null) && (((((java.util.Map$Entry) p3).getValue() instanceof java.util.Map)) && (this.a.b.a.entrySet().remove(((java.util.Map$Entry) p3)))))) {
            v0 = 1;
        }
        return v0;
    }

    public final int size()
    {
        return this.a.b.a.size();
    }
}
