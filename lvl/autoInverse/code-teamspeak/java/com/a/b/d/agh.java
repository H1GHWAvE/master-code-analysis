package com.a.b.d;
final class agh extends com.a.b.d.agi {
    final synthetic com.a.b.d.aga a;
    private final java.util.Deque b;

    agh(com.a.b.d.aga p3, Object p4)
    {
        this.a = p3;
        this.b = new java.util.ArrayDeque();
        this.b.addLast(com.a.b.d.nj.a(com.a.b.b.cn.a(p4)));
        return;
    }

    public final boolean hasNext()
    {
        int v0_2;
        if (this.b.isEmpty()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final Object next()
    {
        java.util.Iterator v0_2 = ((java.util.Iterator) this.b.getLast());
        Object v1_1 = com.a.b.b.cn.a(v0_2.next());
        if (!v0_2.hasNext()) {
            this.b.removeLast();
        }
        java.util.Iterator v0_7 = this.a.a(v1_1).iterator();
        if (v0_7.hasNext()) {
            this.b.addLast(v0_7);
        }
        return v1_1;
    }
}
