package com.a.b.d;
final class acj extends com.a.b.d.act {
    final synthetic com.a.b.d.aci a;

    acj(com.a.b.d.aci p3)
    {
        this.a = p3;
        this(p3.a, 0);
        return;
    }

    public final boolean contains(Object p3)
    {
        if ((!(p3 instanceof java.util.Map$Entry)) || (!this.a.a.b(((java.util.Map$Entry) p3).getKey()))) {
            boolean v0_4 = 0;
        } else {
            v0_4 = this.a.a(((java.util.Map$Entry) p3).getKey()).equals(((java.util.Map$Entry) p3).getValue());
        }
        return v0_4;
    }

    public final java.util.Iterator iterator()
    {
        return com.a.b.d.sz.a(this.a.a.b(), new com.a.b.d.ack(this));
    }

    public final boolean remove(Object p3)
    {
        int v0_1;
        if (!this.contains(p3)) {
            v0_1 = 0;
        } else {
            com.a.b.d.abx.a(this.a.a, ((java.util.Map$Entry) p3).getKey());
            v0_1 = 1;
        }
        return v0_1;
    }

    public final boolean removeAll(java.util.Collection p2)
    {
        com.a.b.b.cn.a(p2);
        return com.a.b.d.aad.a(this, p2.iterator());
    }

    public final boolean retainAll(java.util.Collection p5)
    {
        com.a.b.b.cn.a(p5);
        int v0_0 = 0;
        java.util.Iterator v1_5 = com.a.b.d.ov.a(this.a.a.b().iterator()).iterator();
        while (v1_5.hasNext()) {
            Object v2_1 = v1_5.next();
            if (!p5.contains(com.a.b.d.sz.a(v2_1, this.a.a.d(v2_1)))) {
                com.a.b.d.abx.a(this.a.a, v2_1);
                v0_0 = 1;
            }
        }
        return v0_0;
    }

    public final int size()
    {
        return this.a.a.b().size();
    }
}
