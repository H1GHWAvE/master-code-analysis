package com.a.b.d;
public abstract class lw extends com.a.b.d.lz implements java.util.NavigableMap {
    private static final java.util.Comparator a;
    private static final com.a.b.d.lw b;
    private static final long d;
    private transient com.a.b.d.lw c;

    static lw()
    {
        com.a.b.d.lw.a = com.a.b.d.yd.d();
        com.a.b.d.lw.b = new com.a.b.d.fa(com.a.b.d.lw.a);
        return;
    }

    lw()
    {
        return;
    }

    lw(com.a.b.d.lw p1)
    {
        this.c = p1;
        return;
    }

    static com.a.b.d.lw a(com.a.b.d.me p1, com.a.b.d.jl p2)
    {
        com.a.b.d.zl v0_2;
        if (!p1.isEmpty()) {
            v0_2 = new com.a.b.d.zl(((com.a.b.d.zq) p1), p2);
        } else {
            v0_2 = com.a.b.d.lw.a(p1.comparator());
        }
        return v0_2;
    }

    private static com.a.b.d.lw a(Comparable p2, Object p3)
    {
        return com.a.b.d.lw.a(com.a.b.d.me.a(p2), com.a.b.d.jl.a(p3));
    }

    private static com.a.b.d.lw a(Comparable p6, Object p7, Comparable p8, Object p9)
    {
        com.a.b.d.lw v0_0 = com.a.b.d.yd.d();
        java.util.Map$Entry[] v1 = new java.util.Map$Entry[2];
        v1[0] = com.a.b.d.lw.d(p6, p7);
        v1[1] = com.a.b.d.lw.d(p8, p9);
        return com.a.b.d.lw.a(v0_0, 0, 2, v1);
    }

    private static com.a.b.d.lw a(Comparable p6, Object p7, Comparable p8, Object p9, Comparable p10, Object p11)
    {
        com.a.b.d.lw v0_0 = com.a.b.d.yd.d();
        java.util.Map$Entry[] v1 = new java.util.Map$Entry[3];
        v1[0] = com.a.b.d.lw.d(p6, p7);
        v1[1] = com.a.b.d.lw.d(p8, p9);
        v1[2] = com.a.b.d.lw.d(p10, p11);
        return com.a.b.d.lw.a(v0_0, 0, 3, v1);
    }

    private static com.a.b.d.lw a(Comparable p6, Object p7, Comparable p8, Object p9, Comparable p10, Object p11, Comparable p12, Object p13)
    {
        com.a.b.d.lw v0_0 = com.a.b.d.yd.d();
        java.util.Map$Entry[] v1 = new java.util.Map$Entry[4];
        v1[0] = com.a.b.d.lw.d(p6, p7);
        v1[1] = com.a.b.d.lw.d(p8, p9);
        v1[2] = com.a.b.d.lw.d(p10, p11);
        v1[3] = com.a.b.d.lw.d(p12, p13);
        return com.a.b.d.lw.a(v0_0, 0, 4, v1);
    }

    private static com.a.b.d.lw a(Comparable p6, Object p7, Comparable p8, Object p9, Comparable p10, Object p11, Comparable p12, Object p13, Comparable p14, Object p15)
    {
        com.a.b.d.lw v0_0 = com.a.b.d.yd.d();
        java.util.Map$Entry[] v1 = new java.util.Map$Entry[5];
        v1[0] = com.a.b.d.lw.d(p6, p7);
        v1[1] = com.a.b.d.lw.d(p8, p9);
        v1[2] = com.a.b.d.lw.d(p10, p11);
        v1[3] = com.a.b.d.lw.d(p12, p13);
        v1[4] = com.a.b.d.lw.d(p14, p15);
        return com.a.b.d.lw.a(v0_0, 0, 5, v1);
    }

    private com.a.b.d.lw a(Object p2)
    {
        return this.a(p2, 0);
    }

    private com.a.b.d.lw a(Object p3, Object p4)
    {
        return this.a(p3, 1, p4, 0);
    }

    private com.a.b.d.lw a(Object p6, boolean p7, Object p8, boolean p9)
    {
        com.a.b.d.lw v0_2;
        com.a.b.b.cn.a(p6);
        com.a.b.b.cn.a(p8);
        if (this.comparator().compare(p6, p8) > 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        Object[] v4_1 = new Object[2];
        v4_1[0] = p6;
        v4_1[1] = p8;
        com.a.b.b.cn.a(v0_2, "expected fromKey <= toKey but %s > %s", v4_1);
        return this.a(p8, p9).b(p6, p7);
    }

    static com.a.b.d.lw a(java.util.Comparator p1)
    {
        com.a.b.d.fa v0_3;
        if (!com.a.b.d.yd.d().equals(p1)) {
            v0_3 = new com.a.b.d.fa(p1);
        } else {
            v0_3 = com.a.b.d.lw.b;
        }
        return v0_3;
    }

    private static com.a.b.d.lw a(java.util.Comparator p5, int p6, java.util.Map$Entry[] p7)
    {
        com.a.b.d.zl v0_2;
        if (p6 != 0) {
            com.a.b.d.jl v1_0 = com.a.b.d.jl.h();
            com.a.b.d.jn v2 = com.a.b.d.jl.h();
            com.a.b.d.zl v0_0 = 0;
            while (v0_0 < p6) {
                com.a.b.d.zq v3_2 = p7[v0_0];
                v1_0.c(v3_2.getKey());
                v2.c(v3_2.getValue());
                v0_0++;
            }
            v0_2 = new com.a.b.d.zl(new com.a.b.d.zq(v1_0.b(), p5), v2.b());
        } else {
            v0_2 = com.a.b.d.lw.a(p5);
        }
        return v0_2;
    }

    static varargs com.a.b.d.lw a(java.util.Comparator p7, boolean p8, int p9, java.util.Map$Entry[] p10)
    {
        com.a.b.d.zq v2_0 = 0;
        int v0_0 = 0;
        while (v0_0 < p9) {
            int v3_3 = p10[v0_0];
            p10[v0_0] = com.a.b.d.lw.d(v3_3.getKey(), v3_3.getValue());
            v0_0++;
        }
        if (!p8) {
            java.util.Arrays.sort(p10, 0, p9, com.a.b.d.yd.a(p7).a(com.a.b.d.sz.a()));
            int v3_1 = 1;
            while (v3_1 < p9) {
                int v0_11;
                if (p7.compare(p10[(v3_1 - 1)].getKey(), p10[v3_1].getKey()) == 0) {
                    v0_11 = 0;
                } else {
                    v0_11 = 1;
                }
                com.a.b.d.lw.a(v0_11, "key", p10[(v3_1 - 1)], p10[v3_1]);
                v3_1++;
            }
        }
        int v0_4;
        if (p9 != 0) {
            com.a.b.d.jl v1_1 = com.a.b.d.jl.h();
            int v3_2 = com.a.b.d.jl.h();
            while (v2_0 < p9) {
                int v0_5 = p10[v2_0];
                v1_1.c(v0_5.getKey());
                v3_2.c(v0_5.getValue());
                v2_0++;
            }
            v0_4 = new com.a.b.d.zl(new com.a.b.d.zq(v1_1.b(), p7), v3_2.b());
        } else {
            v0_4 = com.a.b.d.lw.a(p7);
        }
        return v0_4;
    }

    private static com.a.b.d.lw a(java.util.Map p1, java.util.Comparator p2)
    {
        return com.a.b.d.lw.b(p1, ((java.util.Comparator) com.a.b.b.cn.a(p2)));
    }

    private static com.a.b.d.lw a(java.util.SortedMap p1)
    {
        com.a.b.d.lw v0_0 = p1.comparator();
        if (v0_0 == null) {
            v0_0 = com.a.b.d.lw.a;
        }
        return com.a.b.d.lw.b(p1, v0_0);
    }

    private static void a(int p6, java.util.Map$Entry[] p7, java.util.Comparator p8)
    {
        int v2 = 1;
        while (v2 < p6) {
            int v0_4;
            if (p8.compare(p7[(v2 - 1)].getKey(), p7[v2].getKey()) == 0) {
                v0_4 = 0;
            } else {
                v0_4 = 1;
            }
            com.a.b.d.lw.a(v0_4, "key", p7[(v2 - 1)], p7[v2]);
            v2++;
        }
        return;
    }

    private com.a.b.d.lw b(Object p2)
    {
        return this.b(p2, 1);
    }

    private static com.a.b.d.lw b(java.util.Map p1)
    {
        return com.a.b.d.lw.b(p1, com.a.b.d.yd.d());
    }

    private static com.a.b.d.lw b(java.util.Map p4, java.util.Comparator p5)
    {
        int v2;
        if (!(p4 instanceof java.util.SortedMap)) {
            v2 = 0;
        } else {
            com.a.b.d.lw v0_4;
            com.a.b.d.lw v0_3 = ((java.util.SortedMap) p4).comparator();
            if (v0_3 != null) {
                v0_4 = p5.equals(v0_3);
            } else {
                if (p5 != com.a.b.d.lw.a) {
                    v0_4 = 0;
                } else {
                    v0_4 = 1;
                }
            }
            v2 = v0_4;
        }
        if ((v2 == 0) || (!(p4 instanceof com.a.b.d.lw))) {
            int v1_1 = new java.util.Map$Entry[0];
            com.a.b.d.lw v0_11 = ((java.util.Map$Entry[]) p4.entrySet().toArray(v1_1));
            com.a.b.d.lw v0_8 = com.a.b.d.lw.a(p5, v2, v0_11.length, v0_11);
        } else {
            v0_8 = ((com.a.b.d.lw) p4);
            if (((com.a.b.d.lw) p4).i_()) {
            }
        }
        return v0_8;
    }

    private static com.a.b.d.lx b(java.util.Comparator p1)
    {
        return new com.a.b.d.lx(p1);
    }

    private static void b(java.util.Comparator p3, int p4, java.util.Map$Entry[] p5)
    {
        java.util.Arrays.sort(p5, 0, p4, com.a.b.d.yd.a(p3).a(com.a.b.d.sz.a()));
        return;
    }

    public static com.a.b.d.lw m()
    {
        return com.a.b.d.lw.b;
    }

    private static com.a.b.d.lx n()
    {
        return new com.a.b.d.lx(com.a.b.d.yd.d());
    }

    private static com.a.b.d.lx o()
    {
        return new com.a.b.d.lx(com.a.b.d.yd.d().a());
    }

    private com.a.b.d.lw p()
    {
        com.a.b.d.lw v0 = this.c;
        if (v0 == null) {
            v0 = this.i();
            this.c = v0;
        }
        return v0;
    }

    private com.a.b.d.me q()
    {
        return this.a();
    }

    private com.a.b.d.me r()
    {
        return this.a().b();
    }

    public abstract com.a.b.d.lw a();

    public abstract com.a.b.d.me a();

    public abstract com.a.b.d.lw b();

    public java.util.Map$Entry ceilingEntry(Object p2)
    {
        return this.b(p2, 1).firstEntry();
    }

    public Object ceilingKey(Object p2)
    {
        return com.a.b.d.sz.b(this.ceilingEntry(p2));
    }

    public java.util.Comparator comparator()
    {
        return this.a().comparator();
    }

    public boolean containsValue(Object p2)
    {
        return this.h().contains(p2);
    }

    public synthetic java.util.NavigableSet descendingKeySet()
    {
        return this.a().b();
    }

    public synthetic java.util.NavigableMap descendingMap()
    {
        com.a.b.d.lw v0 = this.c;
        if (v0 == null) {
            v0 = this.i();
            this.c = v0;
        }
        return v0;
    }

    public com.a.b.d.lo e()
    {
        return super.e();
    }

    public synthetic java.util.Set entrySet()
    {
        return this.e();
    }

    public java.util.Map$Entry firstEntry()
    {
        java.util.Map$Entry v0_4;
        if (!this.isEmpty()) {
            v0_4 = ((java.util.Map$Entry) this.e().f().get(0));
        } else {
            v0_4 = 0;
        }
        return v0_4;
    }

    public Object firstKey()
    {
        return this.a().first();
    }

    public java.util.Map$Entry floorEntry(Object p2)
    {
        return this.a(p2, 1).lastEntry();
    }

    public Object floorKey(Object p2)
    {
        return com.a.b.d.sz.b(this.floorEntry(p2));
    }

    public synthetic com.a.b.d.lo g()
    {
        return this.a();
    }

    public abstract com.a.b.d.iz h();

    public synthetic java.util.NavigableMap headMap(Object p2, boolean p3)
    {
        return this.a(p2, p3);
    }

    public synthetic java.util.SortedMap headMap(Object p2)
    {
        return this.a(p2, 0);
    }

    public java.util.Map$Entry higherEntry(Object p2)
    {
        return this.b(p2, 0).firstEntry();
    }

    public Object higherKey(Object p2)
    {
        return com.a.b.d.sz.b(this.higherEntry(p2));
    }

    abstract com.a.b.d.lw i();

    boolean i_()
    {
        if ((!this.a().h_()) && (!this.h().h_())) {
            int v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    final Object j()
    {
        return new com.a.b.d.ly(this);
    }

    public synthetic java.util.Set keySet()
    {
        return this.a();
    }

    public java.util.Map$Entry lastEntry()
    {
        java.util.Map$Entry v0_4;
        if (!this.isEmpty()) {
            v0_4 = ((java.util.Map$Entry) this.e().f().get((this.size() - 1)));
        } else {
            v0_4 = 0;
        }
        return v0_4;
    }

    public Object lastKey()
    {
        return this.a().last();
    }

    public java.util.Map$Entry lowerEntry(Object p2)
    {
        return this.a(p2, 0).lastEntry();
    }

    public Object lowerKey(Object p2)
    {
        return com.a.b.d.sz.b(this.lowerEntry(p2));
    }

    public synthetic java.util.NavigableSet navigableKeySet()
    {
        return this.a();
    }

    public final java.util.Map$Entry pollFirstEntry()
    {
        throw new UnsupportedOperationException();
    }

    public final java.util.Map$Entry pollLastEntry()
    {
        throw new UnsupportedOperationException();
    }

    public int size()
    {
        return this.h().size();
    }

    public synthetic java.util.NavigableMap subMap(Object p2, boolean p3, Object p4, boolean p5)
    {
        return this.a(p2, p3, p4, p5);
    }

    public synthetic java.util.SortedMap subMap(Object p3, Object p4)
    {
        return this.a(p3, 1, p4, 0);
    }

    public synthetic java.util.NavigableMap tailMap(Object p2, boolean p3)
    {
        return this.b(p2, p3);
    }

    public synthetic java.util.SortedMap tailMap(Object p2)
    {
        return this.b(p2, 1);
    }

    public synthetic java.util.Collection values()
    {
        return this.h();
    }
}
