package com.a.b.d;
final class aft extends com.a.b.d.av {
    private final java.util.NavigableMap a;
    private final com.a.b.d.yl b;

    aft(java.util.NavigableMap p2)
    {
        this.a = p2;
        this.b = com.a.b.d.yl.c();
        return;
    }

    private aft(java.util.NavigableMap p1, com.a.b.d.yl p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    static synthetic com.a.b.d.yl a(com.a.b.d.aft p1)
    {
        return p1.b;
    }

    private com.a.b.d.yl a(Object p4)
    {
        try {
            com.a.b.d.yl v0_8;
            if (!(p4 instanceof com.a.b.d.dw)) {
                v0_8 = 0;
            } else {
                if (this.b.c(((com.a.b.d.dw) p4))) {
                    java.util.Map$Entry v2 = this.a.lowerEntry(((com.a.b.d.dw) p4));
                    if ((v2 == null) || (!((com.a.b.d.yl) v2.getValue()).c.equals(((com.a.b.d.dw) p4)))) {
                    } else {
                        v0_8 = ((com.a.b.d.yl) v2.getValue());
                    }
                } else {
                    v0_8 = 0;
                }
            }
        } catch (com.a.b.d.yl v0) {
            v0_8 = 0;
        }
        return v0_8;
    }

    private java.util.NavigableMap a(com.a.b.d.dw p2, boolean p3)
    {
        return this.a(com.a.b.d.yl.a(p2, com.a.b.d.ce.a(p3)));
    }

    private java.util.NavigableMap a(com.a.b.d.dw p3, boolean p4, com.a.b.d.dw p5, boolean p6)
    {
        return this.a(com.a.b.d.yl.a(p3, com.a.b.d.ce.a(p4), p5, com.a.b.d.ce.a(p6)));
    }

    private java.util.NavigableMap a(com.a.b.d.yl p4)
    {
        com.a.b.d.lw v0_2;
        if (!p4.b(this.b)) {
            v0_2 = com.a.b.d.lw.m();
        } else {
            v0_2 = new com.a.b.d.aft(this.a, p4.c(this.b));
        }
        return v0_2;
    }

    private java.util.NavigableMap b(com.a.b.d.dw p2, boolean p3)
    {
        return this.a(com.a.b.d.yl.b(p2, com.a.b.d.ce.a(p3)));
    }

    final java.util.Iterator a()
    {
        java.util.Iterator v0_11;
        if (this.b.d()) {
            Comparable v1_3 = this.a.lowerEntry(this.b.b.c());
            if (v1_3 != null) {
                if (!this.b.b.a(((com.a.b.d.yl) v1_3.getValue()).c)) {
                    v0_11 = this.a.tailMap(this.b.b.c(), 1).values().iterator();
                } else {
                    v0_11 = this.a.tailMap(v1_3.getKey(), 1).values().iterator();
                }
            } else {
                v0_11 = this.a.values().iterator();
            }
        } else {
            v0_11 = this.a.values().iterator();
        }
        return new com.a.b.d.afu(this, v0_11);
    }

    final java.util.Iterator b()
    {
        com.a.b.d.afv v0_4;
        if (!this.b.e()) {
            v0_4 = this.a.descendingMap().values();
        } else {
            v0_4 = this.a.headMap(this.b.c.c(), 0).descendingMap().values();
        }
        com.a.b.d.yi v1_3 = com.a.b.d.nj.j(v0_4.iterator());
        if ((v1_3.hasNext()) && (this.b.c.a(((com.a.b.d.yl) v1_3.a()).c))) {
            v1_3.next();
        }
        return new com.a.b.d.afv(this, v1_3);
    }

    public final java.util.Comparator comparator()
    {
        return com.a.b.d.yd.d();
    }

    public final boolean containsKey(Object p2)
    {
        int v0_1;
        if (this.a(p2) == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final synthetic Object get(Object p2)
    {
        return this.a(p2);
    }

    public final synthetic java.util.NavigableMap headMap(Object p2, boolean p3)
    {
        return this.a(com.a.b.d.yl.a(((com.a.b.d.dw) p2), com.a.b.d.ce.a(p3)));
    }

    public final boolean isEmpty()
    {
        int v0_4;
        if (!this.b.equals(com.a.b.d.yl.c())) {
            if (this.a().hasNext()) {
                v0_4 = 0;
            } else {
                v0_4 = 1;
            }
        } else {
            v0_4 = this.a.isEmpty();
        }
        return v0_4;
    }

    public final int size()
    {
        int v0_3;
        if (!this.b.equals(com.a.b.d.yl.c())) {
            v0_3 = com.a.b.d.nj.b(this.a());
        } else {
            v0_3 = this.a.size();
        }
        return v0_3;
    }

    public final synthetic java.util.NavigableMap subMap(Object p3, boolean p4, Object p5, boolean p6)
    {
        return this.a(com.a.b.d.yl.a(((com.a.b.d.dw) p3), com.a.b.d.ce.a(p4), ((com.a.b.d.dw) p5), com.a.b.d.ce.a(p6)));
    }

    public final synthetic java.util.NavigableMap tailMap(Object p2, boolean p3)
    {
        return this.a(com.a.b.d.yl.b(((com.a.b.d.dw) p2), com.a.b.d.ce.a(p3)));
    }
}
