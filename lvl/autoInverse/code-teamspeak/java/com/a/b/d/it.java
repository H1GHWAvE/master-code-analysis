package com.a.b.d;
public abstract class it extends com.a.b.d.jt implements com.a.b.d.bw {
    private static final java.util.Map$Entry[] a;

    static it()
    {
        java.util.Map$Entry[] v0_1 = new java.util.Map$Entry[0];
        com.a.b.d.it.a = v0_1;
        return;
    }

    it()
    {
        return;
    }

    private static com.a.b.d.it a(Object p4, Object p5, Object p6, Object p7)
    {
        com.a.b.d.kb[] v1_1 = new com.a.b.d.kb[2];
        v1_1[0] = com.a.b.d.it.d(p4, p5);
        v1_1[1] = com.a.b.d.it.d(p6, p7);
        return new com.a.b.d.yx(v1_1);
    }

    private static com.a.b.d.it a(Object p4, Object p5, Object p6, Object p7, Object p8, Object p9)
    {
        com.a.b.d.kb[] v1_1 = new com.a.b.d.kb[3];
        v1_1[0] = com.a.b.d.it.d(p4, p5);
        v1_1[1] = com.a.b.d.it.d(p6, p7);
        v1_1[2] = com.a.b.d.it.d(p8, p9);
        return new com.a.b.d.yx(v1_1);
    }

    private static com.a.b.d.it a(Object p4, Object p5, Object p6, Object p7, Object p8, Object p9, Object p10, Object p11)
    {
        com.a.b.d.kb[] v1_1 = new com.a.b.d.kb[4];
        v1_1[0] = com.a.b.d.it.d(p4, p5);
        v1_1[1] = com.a.b.d.it.d(p6, p7);
        v1_1[2] = com.a.b.d.it.d(p8, p9);
        v1_1[3] = com.a.b.d.it.d(p10, p11);
        return new com.a.b.d.yx(v1_1);
    }

    private static com.a.b.d.it a(Object p4, Object p5, Object p6, Object p7, Object p8, Object p9, Object p10, Object p11, Object p12, Object p13)
    {
        com.a.b.d.kb[] v1_1 = new com.a.b.d.kb[5];
        v1_1[0] = com.a.b.d.it.d(p4, p5);
        v1_1[1] = com.a.b.d.it.d(p6, p7);
        v1_1[2] = com.a.b.d.it.d(p8, p9);
        v1_1[3] = com.a.b.d.it.d(p10, p11);
        v1_1[4] = com.a.b.d.it.d(p12, p13);
        return new com.a.b.d.yx(v1_1);
    }

    public static com.a.b.d.it b(Object p1, Object p2)
    {
        return new com.a.b.d.aau(p1, p2);
    }

    private static com.a.b.d.it b(java.util.Map p2)
    {
        com.a.b.d.it v0_2;
        if (!(p2 instanceof com.a.b.d.it)) {
            com.a.b.d.it v0_5 = ((java.util.Map$Entry[]) p2.entrySet().toArray(com.a.b.d.it.a));
            switch (v0_5.length) {
                case 0:
                    v0_2 = com.a.b.d.ew.a;
                    break;
                case 1:
                    com.a.b.d.it v0_6 = v0_5[0];
                    v0_2 = com.a.b.d.it.b(v0_6.getKey(), v0_6.getValue());
                    break;
                default:
                    v0_2 = new com.a.b.d.yx(v0_5);
            }
        } else {
            v0_2 = ((com.a.b.d.it) p2);
            if (((com.a.b.d.it) p2).i_()) {
            }
        }
        return v0_2;
    }

    public static com.a.b.d.it i()
    {
        return com.a.b.d.ew.a;
    }

    private static com.a.b.d.iu m()
    {
        return new com.a.b.d.iu();
    }

    private com.a.b.d.lo n()
    {
        return this.a().g();
    }

    public abstract com.a.b.d.it a();

    public final Object a(Object p2, Object p3)
    {
        throw new UnsupportedOperationException();
    }

    public synthetic com.a.b.d.bw b()
    {
        return this.a();
    }

    public final synthetic com.a.b.d.iz h()
    {
        return this.a().g();
    }

    Object j()
    {
        return new com.a.b.d.iv(this);
    }

    public final synthetic java.util.Set j_()
    {
        return this.a().g();
    }

    public synthetic java.util.Collection values()
    {
        return this.a().g();
    }
}
