package com.a.b.d;
abstract class rc extends com.a.b.d.gi implements java.io.Serializable {
    private static final long a = 3;
    final com.a.b.d.sh b;
    final com.a.b.d.sh c;
    final com.a.b.b.au d;
    final com.a.b.b.au e;
    final long f;
    final long g;
    final int h;
    final int i;
    final com.a.b.d.qw j;
    transient java.util.concurrent.ConcurrentMap k;

    rc(com.a.b.d.sh p2, com.a.b.d.sh p3, com.a.b.b.au p4, com.a.b.b.au p5, long p6, long p8, int p10, int p11, com.a.b.d.qw p12, java.util.concurrent.ConcurrentMap p13)
    {
        this.b = p2;
        this.c = p3;
        this.d = p4;
        this.e = p5;
        this.f = p6;
        this.g = p8;
        this.h = p10;
        this.i = p11;
        this.j = p12;
        this.k = p13;
        return;
    }

    final com.a.b.d.ql a(java.io.ObjectInputStream p7)
    {
        int v0_6;
        com.a.b.d.ql v2_6 = new com.a.b.d.ql().d(p7.readInt()).a(this.b).b(this.c).b(this.d).f(this.i);
        java.util.concurrent.TimeUnit v3_0 = this.j;
        if (v2_6.a != null) {
            v0_6 = 0;
        } else {
            v0_6 = 1;
        }
        com.a.b.b.cn.b(v0_6);
        v2_6.a = ((com.a.b.d.qw) com.a.b.b.cn.a(v3_0));
        v2_6.c = 1;
        if (this.f > 0) {
            v2_6.c(this.f, java.util.concurrent.TimeUnit.NANOSECONDS);
        }
        if (this.g > 0) {
            v2_6.d(this.g, java.util.concurrent.TimeUnit.NANOSECONDS);
        }
        if (this.h != -1) {
            v2_6.e(this.h);
        }
        return v2_6;
    }

    protected final bridge synthetic java.util.Map a()
    {
        return this.k;
    }

    final void a(java.io.ObjectOutputStream p4)
    {
        p4.writeInt(this.k.size());
        java.util.Iterator v1 = this.k.entrySet().iterator();
        while (v1.hasNext()) {
            int v0_7 = ((java.util.Map$Entry) v1.next());
            p4.writeObject(v0_7.getKey());
            p4.writeObject(v0_7.getValue());
        }
        p4.writeObject(0);
        return;
    }

    protected final java.util.concurrent.ConcurrentMap b()
    {
        return this.k;
    }

    final void b(java.io.ObjectInputStream p4)
    {
        while(true) {
            Object v0 = p4.readObject();
            if (v0 == null) {
                break;
            }
            this.k.put(v0, p4.readObject());
        }
        return;
    }

    protected final bridge synthetic Object k_()
    {
        return this.k;
    }
}
