package com.a.b.d;
public final class ls extends com.a.b.d.kn {

    public ls()
    {
        this.a = new com.a.b.d.lt();
        return;
    }

    private com.a.b.d.ls b(com.a.b.d.vi p4)
    {
        java.util.Iterator v1 = p4.b().entrySet().iterator();
        while (v1.hasNext()) {
            Iterable v0_4 = ((java.util.Map$Entry) v1.next());
            this.b(v0_4.getKey(), ((Iterable) v0_4.getValue()));
        }
        return this;
    }

    private com.a.b.d.ls b(Object p4, Iterable p5)
    {
        java.util.Collection v0_1 = this.a.c(com.a.b.b.cn.a(p4));
        java.util.Iterator v1_1 = p5.iterator();
        while (v1_1.hasNext()) {
            v0_1.add(com.a.b.b.cn.a(v1_1.next()));
        }
        return this;
    }

    private varargs com.a.b.d.ls b(Object p2, Object[] p3)
    {
        return this.b(p2, java.util.Arrays.asList(p3));
    }

    private com.a.b.d.ls b(java.util.Map$Entry p4)
    {
        this.a.a(com.a.b.b.cn.a(p4.getKey()), com.a.b.b.cn.a(p4.getValue()));
        return this;
    }

    private com.a.b.d.ls c(java.util.Comparator p2)
    {
        this.b = ((java.util.Comparator) com.a.b.b.cn.a(p2));
        return this;
    }

    private com.a.b.d.ls d(java.util.Comparator p1)
    {
        super.a(p1);
        return this;
    }

    public final synthetic com.a.b.d.kn a(com.a.b.d.vi p4)
    {
        java.util.Iterator v1 = p4.b().entrySet().iterator();
        while (v1.hasNext()) {
            Iterable v0_4 = ((java.util.Map$Entry) v1.next());
            this.b(v0_4.getKey(), ((Iterable) v0_4.getValue()));
        }
        return this;
    }

    public final synthetic com.a.b.d.kn a(Object p2, Iterable p3)
    {
        return this.b(p2, p3);
    }

    public final synthetic com.a.b.d.kn a(Object p2, Object[] p3)
    {
        return this.b(p2, java.util.Arrays.asList(p3));
    }

    public final bridge synthetic com.a.b.d.kn a(java.util.Comparator p1)
    {
        super.a(p1);
        return this;
    }

    public final synthetic com.a.b.d.kn a(java.util.Map$Entry p4)
    {
        this.a.a(com.a.b.b.cn.a(p4.getKey()), com.a.b.b.cn.a(p4.getValue()));
        return this;
    }

    public final com.a.b.d.lr a()
    {
        if (this.b != null) {
            java.util.Comparator v1_1 = new com.a.b.d.lt();
            Iterable v0_4 = com.a.b.d.ov.a(this.a.b().entrySet());
            java.util.Collections.sort(v0_4, com.a.b.d.yd.a(this.b).a(com.a.b.d.sz.a()));
            java.util.Iterator v2_3 = v0_4.iterator();
            while (v2_3.hasNext()) {
                Iterable v0_9 = ((java.util.Map$Entry) v2_3.next());
                v1_1.c(v0_9.getKey(), ((Iterable) v0_9.getValue()));
            }
            this.a = v1_1;
        }
        return com.a.b.d.lr.a(this.a, this.c);
    }

    public final com.a.b.d.ls a(Object p4, Object p5)
    {
        this.a.a(com.a.b.b.cn.a(p4), com.a.b.b.cn.a(p5));
        return this;
    }

    public final synthetic com.a.b.d.kk b()
    {
        return this.a();
    }

    public final synthetic com.a.b.d.kn b(Object p2, Object p3)
    {
        return this.a(p2, p3);
    }

    public final synthetic com.a.b.d.kn b(java.util.Comparator p2)
    {
        this.b = ((java.util.Comparator) com.a.b.b.cn.a(p2));
        return this;
    }
}
