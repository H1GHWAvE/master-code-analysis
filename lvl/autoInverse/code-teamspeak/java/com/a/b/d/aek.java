package com.a.b.d;
abstract class aek extends com.a.b.d.aej implements java.util.ListIterator {

    aek(java.util.ListIterator p1)
    {
        this(p1);
        return;
    }

    private java.util.ListIterator a()
    {
        return com.a.b.d.nj.k(this.c);
    }

    public void add(Object p2)
    {
        throw new UnsupportedOperationException();
    }

    public final boolean hasPrevious()
    {
        return com.a.b.d.nj.k(this.c).hasPrevious();
    }

    public final int nextIndex()
    {
        return com.a.b.d.nj.k(this.c).nextIndex();
    }

    public final Object previous()
    {
        return this.a(com.a.b.d.nj.k(this.c).previous());
    }

    public final int previousIndex()
    {
        return com.a.b.d.nj.k(this.c).previousIndex();
    }

    public void set(Object p2)
    {
        throw new UnsupportedOperationException();
    }
}
