package com.a.b.d;
final class jo extends com.a.b.d.jl {
    private final transient com.a.b.d.jl a;

    jo(com.a.b.d.jl p1)
    {
        this.a = p1;
        return;
    }

    private int b(int p2)
    {
        return ((this.size() - 1) - p2);
    }

    private int c(int p2)
    {
        return (this.size() - p2);
    }

    public final com.a.b.d.jl a(int p4, int p5)
    {
        com.a.b.b.cn.a(p4, p5, this.size());
        return this.a.a((this.size() - p5), (this.size() - p4)).e();
    }

    public final boolean contains(Object p2)
    {
        return this.a.contains(p2);
    }

    public final com.a.b.d.jl e()
    {
        return this.a;
    }

    public final Object get(int p3)
    {
        com.a.b.b.cn.a(p3, this.size());
        return this.a.get(this.b(p3));
    }

    final boolean h_()
    {
        return this.a.h_();
    }

    public final int indexOf(Object p2)
    {
        int v0_2;
        int v0_1 = this.a.lastIndexOf(p2);
        if (v0_1 < 0) {
            v0_2 = -1;
        } else {
            v0_2 = this.b(v0_1);
        }
        return v0_2;
    }

    public final synthetic java.util.Iterator iterator()
    {
        return super.c();
    }

    public final int lastIndexOf(Object p2)
    {
        int v0_2;
        int v0_1 = this.a.indexOf(p2);
        if (v0_1 < 0) {
            v0_2 = -1;
        } else {
            v0_2 = this.b(v0_1);
        }
        return v0_2;
    }

    public final synthetic java.util.ListIterator listIterator()
    {
        return this.a(0);
    }

    public final synthetic java.util.ListIterator listIterator(int p2)
    {
        return super.a(p2);
    }

    public final int size()
    {
        return this.a.size();
    }

    public final synthetic java.util.List subList(int p2, int p3)
    {
        return this.a(p2, p3);
    }
}
