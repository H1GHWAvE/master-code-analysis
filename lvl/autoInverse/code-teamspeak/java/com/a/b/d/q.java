package com.a.b.d;
 class q extends com.a.b.d.uj {
    final transient java.util.Map a;
    final synthetic com.a.b.d.n b;

    q(com.a.b.d.n p1, java.util.Map p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    private java.util.Collection a(Object p3)
    {
        java.util.Collection v0_3;
        java.util.Collection v0_2 = ((java.util.Collection) com.a.b.d.sz.a(this.a, p3));
        if (v0_2 != null) {
            v0_3 = this.b.a(p3, v0_2);
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    private java.util.Collection b(Object p5)
    {
        java.util.Collection v0_3;
        java.util.Collection v0_2 = ((java.util.Collection) this.a.remove(p5));
        if (v0_2 != null) {
            java.util.Collection v1_1 = this.b.c();
            v1_1.addAll(v0_2);
            com.a.b.d.n.b(this.b, v0_2.size());
            v0_2.clear();
            v0_3 = v1_1;
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    final java.util.Map$Entry a(java.util.Map$Entry p4)
    {
        Object v1 = p4.getKey();
        return com.a.b.d.sz.a(v1, this.b.a(v1, ((java.util.Collection) p4.getValue())));
    }

    protected final java.util.Set a()
    {
        return new com.a.b.d.r(this);
    }

    public void clear()
    {
        if (this.a != com.a.b.d.n.a(this.b)) {
            com.a.b.d.nj.i(new com.a.b.d.s(this));
        } else {
            this.b.g();
        }
        return;
    }

    public boolean containsKey(Object p2)
    {
        return com.a.b.d.sz.b(this.a, p2);
    }

    public boolean equals(Object p2)
    {
        if ((this != p2) && (!this.a.equals(p2))) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public synthetic Object get(Object p3)
    {
        java.util.Collection v0_3;
        java.util.Collection v0_2 = ((java.util.Collection) com.a.b.d.sz.a(this.a, p3));
        if (v0_2 != null) {
            v0_3 = this.b.a(p3, v0_2);
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public int hashCode()
    {
        return this.a.hashCode();
    }

    public java.util.Set keySet()
    {
        return this.b.p();
    }

    public synthetic Object remove(Object p5)
    {
        java.util.Collection v0_3;
        java.util.Collection v0_2 = ((java.util.Collection) this.a.remove(p5));
        if (v0_2 != null) {
            java.util.Collection v1_1 = this.b.c();
            v1_1.addAll(v0_2);
            com.a.b.d.n.b(this.b, v0_2.size());
            v0_2.clear();
            v0_3 = v1_1;
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public int size()
    {
        return this.a.size();
    }

    public String toString()
    {
        return this.a.toString();
    }
}
