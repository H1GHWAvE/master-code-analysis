package com.a.b.d;
final class uf extends com.a.b.d.ty implements java.util.SortedMap {

    uf(java.util.SortedMap p1, com.a.b.b.co p2)
    {
        this(p1, p2);
        return;
    }

    private java.util.SortedMap c()
    {
        return ((java.util.SortedMap) this.a);
    }

    private java.util.SortedSet d()
    {
        return ((java.util.SortedSet) super.keySet());
    }

    private java.util.SortedSet f()
    {
        return new com.a.b.d.ug(this);
    }

    public final java.util.Comparator comparator()
    {
        return ((java.util.SortedMap) this.a).comparator();
    }

    final synthetic java.util.Set e()
    {
        return new com.a.b.d.ug(this);
    }

    public final Object firstKey()
    {
        return ((java.util.SortedSet) super.keySet()).iterator().next();
    }

    public final java.util.SortedMap headMap(Object p4)
    {
        return new com.a.b.d.uf(((java.util.SortedMap) this.a).headMap(p4), this.b);
    }

    public final bridge synthetic java.util.Set keySet()
    {
        return ((java.util.SortedSet) super.keySet());
    }

    public final Object lastKey()
    {
        java.util.SortedMap v0_1 = ((java.util.SortedMap) this.a);
        while(true) {
            Object v1 = v0_1.lastKey();
            if (this.b(v1, this.a.get(v1))) {
                break;
            }
            v0_1 = ((java.util.SortedMap) this.a).headMap(v1);
        }
        return v1;
    }

    public final java.util.SortedMap subMap(Object p4, Object p5)
    {
        return new com.a.b.d.uf(((java.util.SortedMap) this.a).subMap(p4, p5), this.b);
    }

    public final java.util.SortedMap tailMap(Object p4)
    {
        return new com.a.b.d.uf(((java.util.SortedMap) this.a).tailMap(p4), this.b);
    }
}
