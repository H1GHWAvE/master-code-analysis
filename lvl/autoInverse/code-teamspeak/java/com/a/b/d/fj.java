package com.a.b.d;
final class fj extends com.a.b.d.uj {
    final synthetic com.a.b.d.fi a;

    fj(com.a.b.d.fi p1)
    {
        this.a = p1;
        return;
    }

    private java.util.Collection b(Object p5)
    {
        int v0_5;
        int v0_4 = ((java.util.Collection) this.a.a.b().get(p5));
        if (v0_4 != 0) {
            v0_5 = com.a.b.d.fi.a(v0_4, new com.a.b.d.fr(this.a, p5));
            if (v0_5.isEmpty()) {
                v0_5 = 0;
            }
        } else {
            v0_5 = 0;
        }
        return v0_5;
    }

    public final java.util.Collection a(Object p6)
    {
        java.util.List v0_10;
        java.util.List v0_4 = ((java.util.Collection) this.a.a.b().get(p6));
        if (v0_4 != null) {
            java.util.ArrayList v2_1 = new java.util.ArrayList();
            java.util.List v0_5 = v0_4.iterator();
            while (v0_5.hasNext()) {
                Object v3_1 = v0_5.next();
                if (com.a.b.d.fi.a(this.a, p6, v3_1)) {
                    v0_5.remove();
                    v2_1.add(v3_1);
                }
            }
            if (!v2_1.isEmpty()) {
                if (!(this.a.a instanceof com.a.b.d.aac)) {
                    v0_10 = java.util.Collections.unmodifiableList(v2_1);
                } else {
                    v0_10 = java.util.Collections.unmodifiableSet(new java.util.LinkedHashSet(com.a.b.d.cm.a(v2_1)));
                }
            } else {
                v0_10 = 0;
            }
        } else {
            v0_10 = 0;
        }
        return v0_10;
    }

    final java.util.Set a()
    {
        return new com.a.b.d.fl(this);
    }

    final java.util.Collection c_()
    {
        return new com.a.b.d.fn(this, this);
    }

    public final void clear()
    {
        this.a.g();
        return;
    }

    public final boolean containsKey(Object p2)
    {
        int v0_1;
        if (this.b(p2) == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    final java.util.Set e()
    {
        return new com.a.b.d.fk(this, this);
    }

    public final synthetic Object get(Object p2)
    {
        return this.b(p2);
    }

    public final synthetic Object remove(Object p2)
    {
        return this.a(p2);
    }
}
