package com.a.b.d;
final class g extends com.a.b.d.hi {
    final synthetic com.a.b.d.a a;

    private g(com.a.b.d.a p1)
    {
        this.a = p1;
        return;
    }

    synthetic g(com.a.b.d.a p1, byte p2)
    {
        this(p1);
        return;
    }

    protected final java.util.Set a()
    {
        return com.a.b.d.a.a(this.a).keySet();
    }

    protected final synthetic java.util.Collection b()
    {
        return this.a();
    }

    public final void clear()
    {
        this.a.clear();
        return;
    }

    public final java.util.Iterator iterator()
    {
        return com.a.b.d.sz.a(this.a.entrySet().iterator());
    }

    protected final synthetic Object k_()
    {
        return this.a();
    }

    public final boolean remove(Object p2)
    {
        int v0_2;
        if (this.contains(p2)) {
            com.a.b.d.a.a(this.a, p2);
            v0_2 = 1;
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public final boolean removeAll(java.util.Collection p2)
    {
        return this.b(p2);
    }

    public final boolean retainAll(java.util.Collection p2)
    {
        return this.c(p2);
    }
}
