package com.a.b.d;
 class adi extends com.a.b.d.adn implements java.util.Map {
    private static final long a;
    transient java.util.Set c;
    transient java.util.Collection d;
    transient java.util.Set e;

    adi(java.util.Map p1, Object p2)
    {
        this(p1, p2);
        return;
    }

    java.util.Map a()
    {
        return ((java.util.Map) super.d());
    }

    public void clear()
    {
        try {
            this.a().clear();
            return;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    public boolean containsKey(Object p3)
    {
        try {
            return this.a().containsKey(p3);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public boolean containsValue(Object p3)
    {
        try {
            return this.a().containsValue(p3);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    synthetic Object d()
    {
        return this.a();
    }

    public java.util.Set entrySet()
    {
        try {
            if (this.e == null) {
                this.e = com.a.b.d.acu.a(this.a().entrySet(), this.h);
            }
        } catch (java.util.Set v0_5) {
            throw v0_5;
        }
        return this.e;
    }

    public boolean equals(Object p3)
    {
        Throwable v0_1;
        if (p3 != this) {
            try {
                v0_1 = this.a().equals(p3);
            } catch (Throwable v0_2) {
                throw v0_2;
            }
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public Object get(Object p3)
    {
        try {
            return this.a().get(p3);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public int hashCode()
    {
        try {
            return this.a().hashCode();
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public boolean isEmpty()
    {
        try {
            return this.a().isEmpty();
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public java.util.Set keySet()
    {
        try {
            if (this.c == null) {
                this.c = com.a.b.d.acu.a(this.a().keySet(), this.h);
            }
        } catch (java.util.Set v0_5) {
            throw v0_5;
        }
        return this.c;
    }

    public Object put(Object p3, Object p4)
    {
        try {
            return this.a().put(p3, p4);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public void putAll(java.util.Map p3)
    {
        try {
            this.a().putAll(p3);
            return;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    public Object remove(Object p3)
    {
        try {
            return this.a().remove(p3);
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public int size()
    {
        try {
            return this.a().size();
        } catch (Throwable v0_2) {
            throw v0_2;
        }
    }

    public java.util.Collection values()
    {
        try {
            if (this.d == null) {
                this.d = com.a.b.d.acu.a(this.a().values(), this.h);
            }
        } catch (java.util.Collection v0_5) {
            throw v0_5;
        }
        return this.d;
    }
}
