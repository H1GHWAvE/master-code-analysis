package com.a.b.d;
final class s implements java.util.Iterator {
    final java.util.Iterator a;
    java.util.Collection b;
    final synthetic com.a.b.d.q c;

    s(com.a.b.d.q p2)
    {
        this.c = p2;
        this.a = this.c.a.entrySet().iterator();
        return;
    }

    private java.util.Map$Entry a()
    {
        java.util.Map$Entry v0_2 = ((java.util.Map$Entry) this.a.next());
        this.b = ((java.util.Collection) v0_2.getValue());
        return this.c.a(v0_2);
    }

    public final boolean hasNext()
    {
        return this.a.hasNext();
    }

    public final synthetic Object next()
    {
        java.util.Map$Entry v0_2 = ((java.util.Map$Entry) this.a.next());
        this.b = ((java.util.Collection) v0_2.getValue());
        return this.c.a(v0_2);
    }

    public final void remove()
    {
        this.a.remove();
        com.a.b.d.n.b(this.c.b, this.b.size());
        this.b.clear();
        return;
    }
}
