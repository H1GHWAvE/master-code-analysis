package com.a.b.d;
public abstract class hr extends com.a.b.d.hg implements com.a.b.d.adv {

    protected hr()
    {
        return;
    }

    public Object a(Object p2, Object p3, Object p4)
    {
        return this.f().a(p2, p3, p4);
    }

    public java.util.Set a()
    {
        return this.f().a();
    }

    public void a(com.a.b.d.adv p2)
    {
        this.f().a(p2);
        return;
    }

    public final boolean a(Object p2)
    {
        return this.f().a(p2);
    }

    public final boolean a(Object p2, Object p3)
    {
        return this.f().a(p2, p3);
    }

    public final Object b(Object p2, Object p3)
    {
        return this.f().b(p2, p3);
    }

    public java.util.Set b()
    {
        return this.f().b();
    }

    public final boolean b(Object p2)
    {
        return this.f().b(p2);
    }

    public Object c(Object p2, Object p3)
    {
        return this.f().c(p2, p3);
    }

    public final boolean c()
    {
        return this.f().c();
    }

    public final boolean c(Object p2)
    {
        return this.f().c(p2);
    }

    public java.util.Map d(Object p2)
    {
        return this.f().d(p2);
    }

    public void d()
    {
        this.f().d();
        return;
    }

    public java.util.Map e(Object p2)
    {
        return this.f().e(p2);
    }

    public java.util.Set e()
    {
        return this.f().e();
    }

    public boolean equals(Object p2)
    {
        if ((p2 != this) && (!this.f().equals(p2))) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    protected abstract com.a.b.d.adv f();

    public java.util.Collection h()
    {
        return this.f().h();
    }

    public int hashCode()
    {
        return this.f().hashCode();
    }

    public final int k()
    {
        return this.f().k();
    }

    protected synthetic Object k_()
    {
        return this.f();
    }

    public java.util.Map l()
    {
        return this.f().l();
    }

    public java.util.Map m()
    {
        return this.f().m();
    }
}
