package com.a.b.d;
final class cw extends com.a.b.d.cv {

    cw()
    {
        this(0);
        return;
    }

    private static com.a.b.d.cv a(int p1)
    {
        com.a.b.d.cv v0;
        if (p1 >= 0) {
            if (p1 <= 0) {
                v0 = com.a.b.d.cv.a;
            } else {
                v0 = com.a.b.d.cv.c;
            }
        } else {
            v0 = com.a.b.d.cv.b;
        }
        return v0;
    }

    public final com.a.b.d.cv a(double p2, double p4)
    {
        return com.a.b.d.cw.a(Double.compare(p2, p4));
    }

    public final com.a.b.d.cv a(float p2, float p3)
    {
        return com.a.b.d.cw.a(Float.compare(p2, p3));
    }

    public final com.a.b.d.cv a(int p2, int p3)
    {
        return com.a.b.d.cw.a(com.a.b.l.q.a(p2, p3));
    }

    public final com.a.b.d.cv a(long p2, long p4)
    {
        return com.a.b.d.cw.a(com.a.b.l.u.a(p2, p4));
    }

    public final com.a.b.d.cv a(Comparable p2, Comparable p3)
    {
        return com.a.b.d.cw.a(p2.compareTo(p3));
    }

    public final com.a.b.d.cv a(Object p2, Object p3, java.util.Comparator p4)
    {
        return com.a.b.d.cw.a(p4.compare(p2, p3));
    }

    public final com.a.b.d.cv a(boolean p2, boolean p3)
    {
        return com.a.b.d.cw.a(com.a.b.l.a.a(p3, p2));
    }

    public final int b()
    {
        return 0;
    }

    public final com.a.b.d.cv b(boolean p2, boolean p3)
    {
        return com.a.b.d.cw.a(com.a.b.l.a.a(p2, p3));
    }
}
