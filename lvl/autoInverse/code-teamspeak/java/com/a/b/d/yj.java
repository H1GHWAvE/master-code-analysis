package com.a.b.d;
final class yj {

    private yj()
    {
        return;
    }

    private static com.a.b.d.ql a(com.a.b.d.ql p1)
    {
        return p1.a(com.a.b.d.sh.c);
    }

    private static java.util.Set a(java.util.Map p1)
    {
        return java.util.Collections.newSetFromMap(p1);
    }

    private static java.util.SortedMap a(java.util.SortedMap p1, com.a.b.b.co p2)
    {
        java.util.SortedMap v0_1;
        if (!(p1 instanceof java.util.NavigableMap)) {
            v0_1 = com.a.b.d.sz.a(p1, p2);
        } else {
            v0_1 = com.a.b.d.sz.a(((java.util.NavigableMap) p1), p2);
        }
        return v0_1;
    }

    private static java.util.SortedMap a(java.util.SortedMap p1, com.a.b.d.tv p2)
    {
        java.util.SortedMap v0_1;
        if (!(p1 instanceof java.util.NavigableMap)) {
            v0_1 = com.a.b.d.sz.b(p1, p2);
        } else {
            v0_1 = com.a.b.d.sz.a(((java.util.NavigableMap) p1), p2);
        }
        return v0_1;
    }

    private static java.util.SortedMap a(java.util.SortedSet p1, com.a.b.b.bj p2)
    {
        java.util.SortedMap v0_1;
        if (!(p1 instanceof java.util.NavigableSet)) {
            v0_1 = com.a.b.d.sz.b(p1, p2);
        } else {
            v0_1 = com.a.b.d.sz.a(((java.util.NavigableSet) p1), p2);
        }
        return v0_1;
    }

    private static java.util.SortedSet a(java.util.SortedSet p3, com.a.b.b.co p4)
    {
        com.a.b.d.aam v0_4;
        if (!(p3 instanceof java.util.NavigableSet)) {
            if (!(p3 instanceof com.a.b.d.aal)) {
                v0_4 = new com.a.b.d.aam(((java.util.SortedSet) com.a.b.b.cn.a(p3)), ((com.a.b.b.co) com.a.b.b.cn.a(p4)));
            } else {
                v0_4 = new com.a.b.d.aam(((java.util.SortedSet) ((com.a.b.d.aal) p3).a), com.a.b.b.cp.a(((com.a.b.d.aal) p3).b, p4));
            }
        } else {
            v0_4 = com.a.b.d.aad.a(((java.util.NavigableSet) p3), p4);
        }
        return v0_4;
    }

    private static Object[] a(Object[] p1, int p2)
    {
        return ((Object[]) ((Object[]) reflect.Array.newInstance(p1.getClass().getComponentType(), p2)));
    }
}
