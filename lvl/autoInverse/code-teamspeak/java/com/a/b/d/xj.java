package com.a.b.d;
final class xj extends com.a.b.d.as {
    final synthetic com.a.b.d.xc a;
    final synthetic com.a.b.d.xc b;

    xj(com.a.b.d.xc p1, com.a.b.d.xc p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    public final int a(Object p3)
    {
        return (this.a.a(p3) + this.b.a(p3));
    }

    final java.util.Iterator b()
    {
        return new com.a.b.d.xk(this, this.a.a().iterator(), this.b.a().iterator());
    }

    final int c()
    {
        return this.n_().size();
    }

    public final boolean contains(Object p2)
    {
        if ((!this.a.contains(p2)) && (!this.b.contains(p2))) {
            int v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    final java.util.Set e()
    {
        return com.a.b.d.aad.a(this.a.n_(), this.b.n_());
    }

    public final boolean isEmpty()
    {
        if ((!this.a.isEmpty()) || (!this.b.isEmpty())) {
            int v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    public final int size()
    {
        return (this.a.size() + this.b.size());
    }
}
