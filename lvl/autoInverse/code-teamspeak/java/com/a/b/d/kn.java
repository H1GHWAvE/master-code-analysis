package com.a.b.d;
public class kn {
    com.a.b.d.vi a;
    java.util.Comparator b;
    java.util.Comparator c;

    public kn()
    {
        this.a = new com.a.b.d.ko();
        return;
    }

    public com.a.b.d.kn a(com.a.b.d.vi p4)
    {
        java.util.Iterator v1 = p4.b().entrySet().iterator();
        while (v1.hasNext()) {
            Iterable v0_4 = ((java.util.Map$Entry) v1.next());
            this.a(v0_4.getKey(), ((Iterable) v0_4.getValue()));
        }
        return this;
    }

    public com.a.b.d.kn a(Object p5, Iterable p6)
    {
        if (p5 != null) {
            String v0_1 = this.a.c(p5);
            java.util.Iterator v1_0 = p6.iterator();
            while (v1_0.hasNext()) {
                Object v2_1 = v1_0.next();
                com.a.b.d.cl.a(p5, v2_1);
                v0_1.add(v2_1);
            }
            return this;
        } else {
            String v0_5;
            String v0_3 = String.valueOf(com.a.b.d.mq.a(p6));
            if (v0_3.length() == 0) {
                v0_5 = new String("null key in entry: null=");
            } else {
                v0_5 = "null key in entry: null=".concat(v0_3);
            }
            throw new NullPointerException(v0_5);
        }
    }

    public varargs com.a.b.d.kn a(Object p2, Object[] p3)
    {
        return this.a(p2, java.util.Arrays.asList(p3));
    }

    public com.a.b.d.kn a(java.util.Comparator p2)
    {
        this.c = ((java.util.Comparator) com.a.b.b.cn.a(p2));
        return this;
    }

    public com.a.b.d.kn a(java.util.Map$Entry p3)
    {
        return this.b(p3.getKey(), p3.getValue());
    }

    public com.a.b.d.kk b()
    {
        if (this.c != null) {
            com.a.b.d.ko v1_0 = this.a.b().values().iterator();
            while (v1_0.hasNext()) {
                java.util.Collections.sort(((java.util.List) ((java.util.Collection) v1_0.next())), this.c);
            }
        }
        if (this.b != null) {
            com.a.b.d.ko v1_2 = new com.a.b.d.ko();
            Iterable v0_9 = com.a.b.d.ov.a(this.a.b().entrySet());
            java.util.Collections.sort(v0_9, com.a.b.d.yd.a(this.b).a(com.a.b.d.sz.a()));
            java.util.Comparator v2_3 = v0_9.iterator();
            while (v2_3.hasNext()) {
                Iterable v0_14 = ((java.util.Map$Entry) v2_3.next());
                v1_2.c(v0_14.getKey(), ((Iterable) v0_14.getValue()));
            }
            this.a = v1_2;
        }
        return com.a.b.d.kk.b(this.a);
    }

    public com.a.b.d.kn b(Object p2, Object p3)
    {
        com.a.b.d.cl.a(p2, p3);
        this.a.a(p2, p3);
        return this;
    }

    public com.a.b.d.kn b(java.util.Comparator p2)
    {
        this.b = ((java.util.Comparator) com.a.b.b.cn.a(p2));
        return this;
    }
}
