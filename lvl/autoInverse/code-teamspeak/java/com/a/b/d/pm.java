package com.a.b.d;
final class pm extends java.util.AbstractList implements java.io.Serializable, java.util.RandomAccess {
    private static final long d;
    final Object a;
    final Object b;
    final Object[] c;

    pm(Object p2, Object p3, Object[] p4)
    {
        this.a = p2;
        this.b = p3;
        this.c = ((Object[]) com.a.b.b.cn.a(p4));
        return;
    }

    public final Object get(int p3)
    {
        Object v0_0;
        switch (p3) {
            case 0:
                v0_0 = this.a;
                break;
            case 1:
                v0_0 = this.b;
                break;
            default:
                com.a.b.b.cn.a(p3, this.size());
                v0_0 = this.c[(p3 - 2)];
        }
        return v0_0;
    }

    public final int size()
    {
        return (this.c.length + 2);
    }
}
