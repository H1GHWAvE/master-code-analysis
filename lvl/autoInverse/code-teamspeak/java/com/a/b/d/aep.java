package com.a.b.d;
final class aep extends com.a.b.d.acm implements java.util.SortedMap {
    final Object d;
    final Object e;
    transient java.util.SortedMap f;
    final synthetic com.a.b.d.ael g;

    aep(com.a.b.d.ael p2, Object p3)
    {
        this(p2, p3, 0, 0);
        return;
    }

    private aep(com.a.b.d.ael p2, Object p3, Object p4, Object p5)
    {
        int v0_1;
        this.g = p2;
        this(p2, p3);
        this.d = p4;
        this.e = p5;
        if ((p4 != null) && ((p5 != null) && (this.a(p4, p5) > 0))) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.a(v0_1);
        return;
    }

    private int a(Object p2, Object p3)
    {
        return this.comparator().compare(p2, p3);
    }

    private boolean a(Object p2)
    {
        if (((p2 == null) || ((this.d != null) && (this.a(this.d, p2) > 0))) || ((this.e != null) && (this.a(this.e, p2) <= 0))) {
            int v0_6 = 0;
        } else {
            v0_6 = 1;
        }
        return v0_6;
    }

    private java.util.SortedSet g()
    {
        return new com.a.b.d.up(this);
    }

    private java.util.SortedMap h()
    {
        if ((this.f == null) || ((this.f.isEmpty()) && (this.g.a.containsKey(this.a)))) {
            this.f = ((java.util.SortedMap) this.g.a.get(this.a));
        }
        return this.f;
    }

    private java.util.SortedMap i()
    {
        return ((java.util.SortedMap) super.c());
    }

    private java.util.SortedMap j()
    {
        java.util.SortedMap v0 = this.h();
        if (v0 == null) {
            v0 = 0;
        } else {
            if (this.d != null) {
                v0 = v0.tailMap(this.d);
            }
            if (this.e != null) {
                v0 = v0.headMap(this.e);
            }
        }
        return v0;
    }

    final bridge synthetic java.util.Map c()
    {
        return ((java.util.SortedMap) super.c());
    }

    public final java.util.Comparator comparator()
    {
        return this.g.c;
    }

    public final boolean containsKey(Object p2)
    {
        if ((!this.a(p2)) || (!super.containsKey(p2))) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    final synthetic java.util.Map d()
    {
        java.util.SortedMap v0 = this.h();
        if (v0 == null) {
            v0 = 0;
        } else {
            if (this.d != null) {
                v0 = v0.tailMap(this.d);
            }
            if (this.e != null) {
                v0 = v0.headMap(this.e);
            }
        }
        return v0;
    }

    final void f()
    {
        if ((this.h() != null) && (this.f.isEmpty())) {
            this.g.a.remove(this.a);
            this.f = 0;
            this.b = 0;
        }
        return;
    }

    public final Object firstKey()
    {
        if (((java.util.SortedMap) super.c()) != null) {
            return ((java.util.SortedMap) super.c()).firstKey();
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public final java.util.SortedMap headMap(Object p5)
    {
        com.a.b.b.cn.a(this.a(com.a.b.b.cn.a(p5)));
        return new com.a.b.d.aep(this.g, this.a, this.d, p5);
    }

    public final synthetic java.util.Set keySet()
    {
        return new com.a.b.d.up(this);
    }

    public final Object lastKey()
    {
        if (((java.util.SortedMap) super.c()) != null) {
            return ((java.util.SortedMap) super.c()).lastKey();
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public final Object put(Object p2, Object p3)
    {
        com.a.b.b.cn.a(this.a(com.a.b.b.cn.a(p2)));
        return super.put(p2, p3);
    }

    public final java.util.SortedMap subMap(Object p4, Object p5)
    {
        if ((!this.a(com.a.b.b.cn.a(p4))) || (!this.a(com.a.b.b.cn.a(p5)))) {
            com.a.b.d.aep v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        com.a.b.b.cn.a(v0_4);
        return new com.a.b.d.aep(this.g, this.a, p4, p5);
    }

    public final java.util.SortedMap tailMap(Object p5)
    {
        com.a.b.b.cn.a(this.a(com.a.b.b.cn.a(p5)));
        return new com.a.b.d.aep(this.g, this.a, p5, this.e);
    }
}
