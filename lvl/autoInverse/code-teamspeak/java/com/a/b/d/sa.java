package com.a.b.d;
 class sa extends java.util.concurrent.locks.ReentrantLock {
    final com.a.b.d.qy a;
    volatile int b;
    int c;
    int d;
    volatile java.util.concurrent.atomic.AtomicReferenceArray e;
    final int f;
    final ref.ReferenceQueue g;
    final ref.ReferenceQueue h;
    final java.util.Queue i;
    final java.util.concurrent.atomic.AtomicInteger j;
    final java.util.Queue k;
    final java.util.Queue l;

    sa(com.a.b.d.qy p5, int p6, int p7)
    {
        ref.ReferenceQueue v1_0 = 0;
        this.j = new java.util.concurrent.atomic.AtomicInteger();
        this.a = p5;
        this.f = p7;
        java.util.Queue v0_2 = com.a.b.d.sa.a(p6);
        this.d = ((v0_2.length() * 3) / 4);
        if (this.d == this.f) {
            this.d = (this.d + 1);
        }
        java.util.Queue v0_4;
        this.e = v0_2;
        if (!p5.e()) {
            v0_4 = 0;
        } else {
            v0_4 = new ref.ReferenceQueue();
        }
        this.g = v0_4;
        if (p5.f()) {
            v1_0 = new ref.ReferenceQueue();
        }
        java.util.Queue v0_9;
        this.h = v1_0;
        if ((!p5.b()) && (!p5.d())) {
            v0_9 = com.a.b.d.qy.i();
        } else {
            v0_9 = new java.util.concurrent.ConcurrentLinkedQueue();
        }
        java.util.Queue v0_12;
        this.i = v0_9;
        if (!p5.b()) {
            v0_12 = com.a.b.d.qy.i();
        } else {
            v0_12 = new com.a.b.d.rp();
        }
        java.util.Queue v0_15;
        this.k = v0_12;
        if (!p5.c()) {
            v0_15 = com.a.b.d.qy.i();
        } else {
            v0_15 = new com.a.b.d.rs();
        }
        this.l = v0_15;
        return;
    }

    private static java.util.concurrent.atomic.AtomicReferenceArray a(int p1)
    {
        return new java.util.concurrent.atomic.AtomicReferenceArray(p1);
    }

    private void a(com.a.b.d.rz p3, long p4)
    {
        p3.a((this.a.w.a() + p4));
        return;
    }

    private void a(com.a.b.d.rz p3, Object p4)
    {
        p3.a(this.a.p.a(this, p3, p4));
        this.l();
        this.k.add(p3);
        if (this.a.c()) {
            java.util.Queue v0_9;
            if (!this.a.d()) {
                v0_9 = this.a.s;
            } else {
                v0_9 = this.a.r;
            }
            this.a(p3, v0_9);
            this.l.add(p3);
        }
        return;
    }

    private void a(java.util.concurrent.atomic.AtomicReferenceArray p3)
    {
        this.d = ((p3.length() * 3) / 4);
        if (this.d == this.f) {
            this.d = (this.d + 1);
        }
        this.e = p3;
        return;
    }

    private boolean a(com.a.b.d.rz p7, int p8, com.a.b.d.qq p9)
    {
        java.util.concurrent.atomic.AtomicReferenceArray v2 = this.e;
        int v3 = (p8 & (v2.length() - 1));
        int v0_3 = ((com.a.b.d.rz) v2.get(v3));
        com.a.b.d.rz v1_0 = v0_3;
        while (v1_0 != null) {
            if (v1_0 != p7) {
                v1_0 = v1_0.b();
            } else {
                this.c = (this.c + 1);
                this.a(v1_0.d(), v1_0.a().get(), p9);
                com.a.b.d.rz v1_2 = (this.b - 1);
                v2.set(v3, this.b(v0_3, v1_0));
                this.b = v1_2;
                int v0_4 = 1;
            }
            return v0_4;
        }
        v0_4 = 0;
        return v0_4;
    }

    private static boolean a(com.a.b.d.sr p2)
    {
        int v0 = 0;
        if ((!p2.b()) && (p2.get() == null)) {
            v0 = 1;
        }
        return v0;
    }

    private boolean a(Object p8)
    {
        try {
            int v0_1;
            if (this.b == 0) {
                this.a();
                v0_1 = 0;
            } else {
                java.util.concurrent.atomic.AtomicReferenceArray v3 = this.e;
                int v4 = v3.length();
                int v2 = 0;
                while (v2 < v4) {
                    int v0_3 = ((com.a.b.d.rz) v3.get(v2));
                    while (v0_3 != 0) {
                        boolean v5_0 = this.c(v0_3);
                        if ((!v5_0) || (!this.a.n.a(p8, v5_0))) {
                            v0_3 = v0_3.b();
                        } else {
                            this.a();
                            v0_1 = 1;
                        }
                    }
                    v2++;
                }
            }
        } catch (int v0_5) {
            this.a();
            throw v0_5;
        }
        return v0_1;
    }

    private com.a.b.d.rz b(int p3)
    {
        com.a.b.d.rz v0_0 = this.e;
        return ((com.a.b.d.rz) v0_0.get(((v0_0.length() - 1) & p3)));
    }

    private com.a.b.d.rz b(com.a.b.d.rz p5, com.a.b.d.rz p6)
    {
        this.k.remove(p6);
        this.l.remove(p6);
        int v2 = this.b;
        int v1_0 = p6.b();
        while (p5 != p6) {
            int v1_1;
            int v0_2 = this.a(p5, v1_0);
            if (v0_2 == 0) {
                this.e(p5);
                v1_1 = (v2 - 1);
                v0_2 = v1_0;
            } else {
                v1_1 = v2;
            }
            p5 = p5.b();
            v2 = v1_1;
            v1_0 = v0_2;
        }
        this.b = v2;
        return v1_0;
    }

    private void d(com.a.b.d.rz p3)
    {
        this.l();
        this.k.add(p3);
        if (this.a.c()) {
            java.util.Queue v0_6;
            if (!this.a.d()) {
                v0_6 = this.a.s;
            } else {
                v0_6 = this.a.r;
            }
            this.a(p3, v0_6);
            this.l.add(p3);
        }
        return;
    }

    private com.a.b.d.rz e(Object p4, int p5)
    {
        com.a.b.d.rz v0 = 0;
        com.a.b.d.rz v1 = this.a(p4, p5);
        if (v1 != null) {
            if ((!this.a.c()) || (!this.a.a(v1))) {
                v0 = v1;
            } else {
                this.m();
            }
        }
        return v0;
    }

    private void e()
    {
        if (this.tryLock()) {
            try {
                this.f();
                this.unlock();
            } catch (Throwable v0_1) {
                this.unlock();
                throw v0_1;
            }
        }
        return;
    }

    private void e(com.a.b.d.rz p2)
    {
        this.a(p2, com.a.b.d.qq.c);
        this.k.remove(p2);
        this.l.remove(p2);
        return;
    }

    private void f()
    {
        int v2 = 0;
        if (this.a.e()) {
            com.a.b.d.sr v1_0 = 0;
            while(true) {
                com.a.b.d.sr v0_3 = this.g.poll();
                if (v0_3 == null) {
                    break;
                }
                com.a.b.d.sr v0_4 = ((com.a.b.d.rz) v0_3);
                Object v3_0 = this.a;
                int v4_0 = v0_4.c();
                v3_0.a(v4_0).a(v0_4, v4_0);
                com.a.b.d.sr v0_5 = (v1_0 + 1);
                if (v0_5 == 16) {
                    break;
                }
                v1_0 = v0_5;
            }
        }
        if (this.a.f()) {
            do {
                com.a.b.d.sr v0_9 = this.h.poll();
                if (v0_9 == null) {
                    break;
                }
                com.a.b.d.sr v0_10 = ((com.a.b.d.sr) v0_9);
                com.a.b.d.sr v1_1 = this.a;
                Object v3_2 = v0_10.a();
                int v4_1 = v3_2.c();
                v1_1.a(v4_1).a(v3_2.d(), v4_1, v0_10);
                v2++;
            } while(v2 != 16);
        }
        return;
    }

    private void g()
    {
        int v1_0 = 0;
        while(true) {
            int v0_2 = this.g.poll();
            if (v0_2 == 0) {
                break;
            }
            int v0_3 = ((com.a.b.d.rz) v0_2);
            com.a.b.d.sa v2_0 = this.a;
            int v3 = v0_3.c();
            v2_0.a(v3).a(v0_3, v3);
            int v0_4 = (v1_0 + 1);
            if (v0_4 == 16) {
                break;
            }
            v1_0 = v0_4;
        }
        return;
    }

    private void h()
    {
        int v1_0 = 0;
        while(true) {
            int v0_2 = this.h.poll();
            if (v0_2 == 0) {
                break;
            }
            int v0_3 = ((com.a.b.d.sr) v0_2);
            com.a.b.d.sa v2_0 = this.a;
            Object v3_0 = v0_3.a();
            int v4 = v3_0.c();
            v2_0.a(v4).a(v3_0.d(), v4, v0_3);
            int v0_4 = (v1_0 + 1);
            if (v0_4 == 16) {
                break;
            }
            v1_0 = v0_4;
        }
        return;
    }

    private void i()
    {
        if (this.a.e()) {
            while (this.g.poll() != null) {
            }
        }
        if (this.a.f()) {
            while (this.h.poll() != null) {
            }
        }
        return;
    }

    private void j()
    {
        while (this.g.poll() != null) {
        }
        return;
    }

    private void k()
    {
        while (this.h.poll() != null) {
        }
        return;
    }

    private void l()
    {
        while(true) {
            com.a.b.d.rz v0_2 = ((com.a.b.d.rz) this.i.poll());
            if (v0_2 == null) {
                break;
            }
            if (this.k.contains(v0_2)) {
                this.k.add(v0_2);
            }
            if ((this.a.d()) && (this.l.contains(v0_2))) {
                this.l.add(v0_2);
            }
        }
        return;
    }

    private void m()
    {
        if (this.tryLock()) {
            try {
                this.n();
                this.unlock();
            } catch (Throwable v0_1) {
                this.unlock();
                throw v0_1;
            }
        }
        return;
    }

    private void n()
    {
        this.l();
        if (!this.l.isEmpty()) {
            long v2 = this.a.w.a();
            do {
                AssertionError v0_6 = ((com.a.b.d.rz) this.l.peek());
                if ((v0_6 != null) && (com.a.b.d.qy.a(v0_6, v2))) {
                }
            } while(this.a(v0_6, v0_6.c(), com.a.b.d.qq.d));
            throw new AssertionError();
        }
        return;
    }

    private boolean o()
    {
        if ((!this.a.b()) || (this.b < this.f)) {
            int v0_3 = 0;
        } else {
            this.l();
            int v0_6 = ((com.a.b.d.rz) this.k.remove());
            if (this.a(v0_6, v0_6.c(), com.a.b.d.qq.e)) {
                v0_3 = 1;
            } else {
                throw new AssertionError();
            }
        }
        return v0_3;
    }

    private void p()
    {
        java.util.concurrent.atomic.AtomicReferenceArray v7 = this.e;
        int v8 = v7.length();
        if (v8 < 1073741824) {
            int v5 = this.b;
            java.util.concurrent.atomic.AtomicReferenceArray v9 = com.a.b.d.sa.a((v8 << 1));
            this.d = ((v9.length() * 3) / 4);
            int v10 = (v9.length() - 1);
            int v6 = 0;
            while (v6 < v8) {
                int v2_0;
                int v0_8 = ((com.a.b.d.rz) v7.get(v6));
                if (v0_8 == 0) {
                    v2_0 = v5;
                } else {
                    int v2_1 = v0_8.b();
                    int v4_0 = (v0_8.c() & v10);
                    if (v2_1 != 0) {
                        int v1_1 = v0_8;
                        while (v2_1 != 0) {
                            com.a.b.d.rz v3_2 = (v2_1.c() & v10);
                            if (v3_2 == v4_0) {
                                v3_2 = v4_0;
                            } else {
                                v1_1 = v2_1;
                            }
                            v2_1 = v2_1.b();
                            v4_0 = v3_2;
                        }
                        v9.set(v4_0, v1_1);
                        com.a.b.d.rz v3_0 = v0_8;
                        v2_0 = v5;
                        while (v3_0 != v1_1) {
                            int v0_13;
                            int v4_1 = (v3_0.c() & v10);
                            int v0_12 = this.a(v3_0, ((com.a.b.d.rz) v9.get(v4_1)));
                            if (v0_12 == 0) {
                                this.e(v3_0);
                                v0_13 = (v2_0 - 1);
                            } else {
                                v9.set(v4_1, v0_12);
                                v0_13 = v2_0;
                            }
                            v3_0 = v3_0.b();
                            v2_0 = v0_13;
                        }
                    } else {
                        v9.set(v4_0, v0_8);
                        v2_0 = v5;
                    }
                }
                v6++;
                v5 = v2_0;
            }
            this.e = v9;
            this.b = v5;
        }
        return;
    }

    private void q()
    {
        if (this.b != 0) {
            this.lock();
            try {
                java.util.concurrent.atomic.AtomicReferenceArray v3 = this.e;
            } catch (com.a.b.d.rz v0_22) {
                this.unlock();
                this.d();
                throw v0_22;
            }
            if (this.a.t != com.a.b.d.qy.y) {
                int v2_1 = 0;
                while (v2_1 < v3.length()) {
                    com.a.b.d.rz v0_20 = ((com.a.b.d.rz) v3.get(v2_1));
                    while (v0_20 != null) {
                        if (!v0_20.a().b()) {
                            this.a(v0_20, com.a.b.d.qq.a);
                        }
                        v0_20 = v0_20.b();
                    }
                    v2_1++;
                }
            }
            com.a.b.d.rz v0_4 = 0;
            while (v0_4 < v3.length()) {
                v3.set(v0_4, 0);
                v0_4++;
            }
            if (this.a.e()) {
                while (this.g.poll() != null) {
                }
            }
            if (this.a.f()) {
                while (this.h.poll() != null) {
                }
            }
            this.k.clear();
            this.l.clear();
            this.j.set(0);
            this.c = (this.c + 1);
            this.b = 0;
            this.unlock();
            this.d();
        }
        return;
    }

    private void r()
    {
        this.c();
        return;
    }

    private void s()
    {
        this.d();
        return;
    }

    final com.a.b.d.rz a(com.a.b.d.rz p5, com.a.b.d.rz p6)
    {
        com.a.b.d.rz v0_0 = 0;
        if (p5.d() != null) {
            com.a.b.d.sr v1_1 = p5.a();
            Object v2 = v1_1.get();
            if ((v2 != null) || (v1_1.b())) {
                v0_0 = this.a.v.a(this, p5, p6);
                v0_0.a(v1_1.a(this.h, v2, v0_0));
            }
        }
        return v0_0;
    }

    final com.a.b.d.rz a(Object p4, int p5)
    {
        com.a.b.d.rz v0_3;
        if (this.b == 0) {
            v0_3 = 0;
        } else {
            com.a.b.d.rz v0_1 = this.e;
            v0_3 = ((com.a.b.d.rz) v0_1.get(((v0_1.length() - 1) & p5)));
            while (v0_3 != null) {
                if (v0_3.c() == p5) {
                    boolean v1_4 = v0_3.d();
                    if (v1_4) {
                        if (this.a.m.a(p4, v1_4)) {
                            return v0_3;
                        }
                    } else {
                        this.e();
                    }
                }
                v0_3 = v0_3.b();
            }
        }
        return v0_3;
    }

    final com.a.b.d.rz a(Object p2, int p3, com.a.b.d.rz p4)
    {
        return this.a.v.a(this, p2, p3, p4);
    }

    final Object a(Object p9, int p10, Object p11)
    {
        this.lock();
        try {
            this.c();
            java.util.concurrent.atomic.AtomicReferenceArray v4 = this.e;
            int v5 = (p10 & (v4.length() - 1));
            int v0_3 = ((com.a.b.d.rz) v4.get(v5));
            com.a.b.d.rz v3 = v0_3;
        } catch (int v0_9) {
            this.unlock();
            this.d();
            throw v0_9;
        }
        while (v3 != null) {
            Object v6 = v3.d();
            if ((v3.c() != p10) || ((v6 == null) || (!this.a.m.a(p9, v6)))) {
                v3 = v3.b();
            } else {
                com.a.b.d.qq v7_0 = v3.a();
                int v2_5 = v7_0.get();
                if (v2_5 != 0) {
                    this.c = (this.c + 1);
                    this.a(p9, v2_5, com.a.b.d.qq.b);
                    this.a(v3, p11);
                    this.unlock();
                    this.d();
                    int v0_4 = v2_5;
                } else {
                    if (com.a.b.d.sa.a(v7_0)) {
                        this.c = (this.c + 1);
                        this.a(v6, v2_5, com.a.b.d.qq.c);
                        int v2_7 = (this.b - 1);
                        v4.set(v5, this.b(v0_3, v3));
                        this.b = v2_7;
                    }
                    this.unlock();
                    this.d();
                    v0_4 = 0;
                }
            }
            return v0_4;
        }
        this.unlock();
        this.d();
        v0_4 = 0;
        return v0_4;
    }

    final Object a(Object p12, int p13, Object p14, boolean p15)
    {
        this.lock();
        try {
            this.c();
            int v1_0 = (this.b + 1);
        } catch (com.a.b.d.rz v0_29) {
            this.unlock();
            this.d();
            throw v0_29;
        }
        if (v1_0 > this.d) {
            java.util.concurrent.atomic.AtomicReferenceArray v7 = this.e;
            int v8 = v7.length();
            if (v8 < 1073741824) {
                int v1_1 = this.b;
                java.util.concurrent.atomic.AtomicReferenceArray v9 = com.a.b.d.sa.a((v8 << 1));
                this.d = ((v9.length() * 3) / 4);
                int v10 = (v9.length() - 1);
                int v6_0 = 0;
                while (v6_0 < v8) {
                    com.a.b.d.rz v0_23;
                    com.a.b.d.rz v0_22 = ((com.a.b.d.rz) v7.get(v6_0));
                    if (v0_22 == null) {
                        v0_23 = v1_1;
                    } else {
                        int v3_2 = v0_22.b();
                        boolean v5_2 = (v0_22.c() & v10);
                        if (v3_2 != 0) {
                            Object v4_3 = v0_22;
                            while (v3_2 != 0) {
                                Object v4_4;
                                com.a.b.d.rz v2_8;
                                com.a.b.d.rz v2_7 = (v3_2.c() & v10);
                                if (v2_7 == v5_2) {
                                    v2_8 = v4_3;
                                    v4_4 = v5_2;
                                } else {
                                    v4_4 = v2_7;
                                    v2_8 = v3_2;
                                }
                                v3_2 = v3_2.b();
                                v5_2 = v4_4;
                                v4_3 = v2_8;
                            }
                            v9.set(v5_2, v4_3);
                            com.a.b.d.rz v2_5 = v0_22;
                            while (v2_5 != v4_3) {
                                int v3_3 = (v2_5.c() & v10);
                                com.a.b.d.rz v0_27 = this.a(v2_5, ((com.a.b.d.rz) v9.get(v3_3)));
                                if (v0_27 == null) {
                                    this.e(v2_5);
                                    v1_1--;
                                } else {
                                    v9.set(v3_3, v0_27);
                                }
                                v2_5 = v2_5.b();
                            }
                        } else {
                            v9.set(v5_2, v0_22);
                            v0_23 = v1_1;
                        }
                    }
                    v6_0++;
                    v1_1 = v0_23;
                }
                this.e = v9;
                this.b = v1_1;
            }
            v1_0 = (this.b + 1);
        }
        int v3_0 = this.e;
        Object v4_0 = (p13 & (v3_0.length() - 1));
        com.a.b.d.rz v0_13 = ((com.a.b.d.rz) v3_0.get(v4_0));
        com.a.b.d.rz v2_0 = v0_13;
        while (v2_0 != null) {
            boolean v5_0 = v2_0.d();
            if ((v2_0.c() != p13) || ((!v5_0) || (!this.a.m.a(p12, v5_0)))) {
                v2_0 = v2_0.b();
            } else {
                int v3_1 = v2_0.a();
                com.a.b.d.rz v0_18 = v3_1.get();
                if (v0_18 != null) {
                    if (!p15) {
                        this.c = (this.c + 1);
                        this.a(p12, v0_18, com.a.b.d.qq.b);
                        this.a(v2_0, p14);
                        this.unlock();
                        this.d();
                    } else {
                        this.b(v2_0);
                        this.unlock();
                        this.d();
                    }
                } else {
                    this.c = (this.c + 1);
                    this.a(v2_0, p14);
                    if (v3_1.b()) {
                        if (this.o()) {
                            v1_0 = (this.b + 1);
                        }
                    } else {
                        this.a(p12, v0_18, com.a.b.d.qq.c);
                        v1_0 = this.b;
                    }
                    this.b = v1_0;
                    this.unlock();
                    this.d();
                    v0_18 = 0;
                }
            }
            return v0_18;
        }
        com.a.b.d.rz v0_16;
        this.c = (this.c + 1);
        com.a.b.d.rz v0_14 = this.a(p12, p13, v0_13);
        this.a(v0_14, p14);
        v3_0.set(v4_0, v0_14);
        if (!this.o()) {
            v0_16 = v1_0;
        } else {
            v0_16 = (this.b + 1);
        }
        this.b = v0_16;
        this.unlock();
        this.d();
        v0_18 = 0;
        return v0_18;
    }

    final void a()
    {
        if ((this.j.incrementAndGet() & 63) == 0) {
            this.b();
        }
        return;
    }

    final void a(com.a.b.d.rz p3)
    {
        if (this.a.d()) {
            this.a(p3, this.a.r);
        }
        this.i.add(p3);
        return;
    }

    final void a(com.a.b.d.rz p3, com.a.b.d.qq p4)
    {
        Object v0 = p3.d();
        p3.c();
        this.a(v0, p3.a().get(), p4);
        return;
    }

    final void a(Object p3, Object p4, com.a.b.d.qq p5)
    {
        if (this.a.t != com.a.b.d.qy.y) {
            this.a.t.offer(new com.a.b.d.qx(p3, p4, p5));
        }
        return;
    }

    final boolean a(com.a.b.d.rz p8, int p9)
    {
        this.lock();
        try {
            java.util.concurrent.atomic.AtomicReferenceArray v2 = this.e;
            int v3 = (p9 & (v2.length() - 1));
            int v0_3 = ((com.a.b.d.rz) v2.get(v3));
            com.a.b.d.rz v1_0 = v0_3;
        } catch (int v0_5) {
            this.unlock();
            this.d();
            throw v0_5;
        }
        while (v1_0 != null) {
            if (v1_0 != p8) {
                v1_0 = v1_0.b();
            } else {
                this.c = (this.c + 1);
                this.a(v1_0.d(), v1_0.a().get(), com.a.b.d.qq.c);
                com.a.b.d.rz v1_2 = (this.b - 1);
                v2.set(v3, this.b(v0_3, v1_0));
                this.b = v1_2;
                this.unlock();
                this.d();
                int v0_4 = 1;
            }
            return v0_4;
        }
        this.unlock();
        this.d();
        v0_4 = 0;
        return v0_4;
    }

    final boolean a(Object p8, int p9, com.a.b.d.sr p10)
    {
        this.lock();
        try {
            java.util.concurrent.atomic.AtomicReferenceArray v3 = this.e;
            int v4 = (p9 & (v3.length() - 1));
            int v0_3 = ((com.a.b.d.rz) v3.get(v4));
            com.a.b.d.rz v2 = v0_3;
        } catch (int v0_6) {
            this.unlock();
            if (!this.isHeldByCurrentThread()) {
                this.d();
            }
            throw v0_6;
        }
        while (v2 != null) {
            com.a.b.d.qq v5_0 = v2.d();
            if ((v2.c() != p9) || ((v5_0 == null) || (!this.a.m.a(p8, v5_0)))) {
                v2 = v2.b();
            } else {
                if (v2.a() != p10) {
                    this.unlock();
                    if (!this.isHeldByCurrentThread()) {
                        this.d();
                    }
                    int v0_5 = 0;
                } else {
                    this.c = (this.c + 1);
                    this.a(p8, p10.get(), com.a.b.d.qq.c);
                    int v1_6 = (this.b - 1);
                    v3.set(v4, this.b(v0_3, v2));
                    this.b = v1_6;
                    this.unlock();
                    if (!this.isHeldByCurrentThread()) {
                        this.d();
                    }
                    v0_5 = 1;
                }
            }
            return v0_5;
        }
        this.unlock();
        if (!this.isHeldByCurrentThread()) {
            this.d();
        }
        v0_5 = 0;
        return v0_5;
    }

    final boolean a(Object p9, int p10, Object p11, Object p12)
    {
        this.lock();
        try {
            this.c();
            java.util.concurrent.atomic.AtomicReferenceArray v3 = this.e;
            int v4 = (p10 & (v3.length() - 1));
            int v0_3 = ((com.a.b.d.rz) v3.get(v4));
            int v2_0 = v0_3;
        } catch (int v0_12) {
            this.unlock();
            this.d();
            throw v0_12;
        }
        while (v2_0 != 0) {
            Object v5 = v2_0.d();
            if ((v2_0.c() != p10) || ((v5 == null) || (!this.a.m.a(p9, v5)))) {
                v2_0 = v2_0.b();
            } else {
                com.a.b.d.qq v6_4 = v2_0.a();
                Object v7 = v6_4.get();
                if (v7 != null) {
                    if (!this.a.n.a(p11, v7)) {
                        this.b(v2_0);
                        this.unlock();
                        this.d();
                        int v0_4 = 0;
                    } else {
                        this.c = (this.c + 1);
                        this.a(p9, v7, com.a.b.d.qq.b);
                        this.a(v2_0, p12);
                        this.unlock();
                        this.d();
                        v0_4 = 1;
                    }
                } else {
                    if (com.a.b.d.sa.a(v6_4)) {
                        this.c = (this.c + 1);
                        this.a(v5, v7, com.a.b.d.qq.c);
                        int v2_2 = (this.b - 1);
                        v3.set(v4, this.b(v0_3, v2_0));
                        this.b = v2_2;
                    }
                    this.unlock();
                    this.d();
                    v0_4 = 0;
                }
            }
            return v0_4;
        }
        this.unlock();
        this.d();
        v0_4 = 0;
        return v0_4;
    }

    final Object b(Object p3, int p4)
    {
        try {
            Object v0_1;
            com.a.b.d.rz v1 = this.e(p3, p4);
        } catch (Object v0_2) {
            this.a();
            throw v0_2;
        }
        if (v1 != null) {
            v0_1 = v1.a().get();
            if (v0_1 == null) {
                this.e();
            } else {
                this.a(v1);
            }
            this.a();
        } else {
            this.a();
            v0_1 = 0;
        }
        return v0_1;
    }

    final void b()
    {
        this.c();
        this.d();
        return;
    }

    final void b(com.a.b.d.rz p3)
    {
        this.k.add(p3);
        if (this.a.d()) {
            this.a(p3, this.a.r);
            this.l.add(p3);
        }
        return;
    }

    final boolean b(Object p8, int p9, com.a.b.d.sr p10)
    {
        this.lock();
        try {
            java.util.concurrent.atomic.AtomicReferenceArray v3 = this.e;
            int v4 = (p9 & (v3.length() - 1));
            int v0_3 = ((com.a.b.d.rz) v3.get(v4));
            com.a.b.d.rz v2 = v0_3;
        } catch (int v0_5) {
            this.unlock();
            this.d();
            throw v0_5;
        }
        while (v2 != null) {
            com.a.b.d.sr v5_0 = v2.d();
            if ((v2.c() != p9) || ((v5_0 == null) || (!this.a.m.a(p8, v5_0)))) {
                v2 = v2.b();
            } else {
                if (v2.a() != p10) {
                    this.unlock();
                    this.d();
                    int v0_4 = 0;
                } else {
                    v3.set(v4, this.b(v0_3, v2));
                    this.unlock();
                    this.d();
                    v0_4 = 1;
                }
            }
            return v0_4;
        }
        this.unlock();
        this.d();
        v0_4 = 0;
        return v0_4;
    }

    final boolean b(Object p10, int p11, Object p12)
    {
        int v1 = 0;
        this.lock();
        try {
            this.c();
            java.util.concurrent.atomic.AtomicReferenceArray v4 = this.e;
            int v5 = (p11 & (v4.length() - 1));
            int v0_3 = ((com.a.b.d.rz) v4.get(v5));
            int v3_0 = v0_3;
        } catch (int v0_4) {
            this.unlock();
            this.d();
            throw v0_4;
        }
        while (v3_0 != 0) {
            Object v6 = v3_0.d();
            if ((v3_0.c() != p11) || ((v6 == null) || (!this.a.m.a(p10, v6)))) {
                v3_0 = v3_0.b();
            } else {
                com.a.b.d.qq v2_7;
                com.a.b.d.qq v2_5 = v3_0.a();
                Object v7 = v2_5.get();
                if (!this.a.n.a(p12, v7)) {
                    if (!com.a.b.d.sa.a(v2_5)) {
                        this.unlock();
                        this.d();
                        return v1;
                    } else {
                        v2_7 = com.a.b.d.qq.c;
                    }
                } else {
                    v2_7 = com.a.b.d.qq.a;
                }
                int v0_7;
                this.c = (this.c + 1);
                this.a(v6, v7, v2_7);
                int v3_2 = (this.b - 1);
                v4.set(v5, this.b(v0_3, v3_0));
                this.b = v3_2;
                if (v2_7 != com.a.b.d.qq.a) {
                    v0_7 = 0;
                } else {
                    v0_7 = 1;
                }
                this.unlock();
                this.d();
                v1 = v0_7;
            }
            return v1;
        }
        this.unlock();
        this.d();
        return v1;
    }

    final Object c(com.a.b.d.rz p4)
    {
        Object v0 = 0;
        if (p4.d() != null) {
            Object v1_2 = p4.a().get();
            if (v1_2 != null) {
                if ((!this.a.c()) || (!this.a.a(p4))) {
                    v0 = v1_2;
                } else {
                    this.m();
                }
            } else {
                this.e();
            }
        } else {
            this.e();
        }
        return v0;
    }

    final void c()
    {
        if (this.tryLock()) {
            try {
                this.f();
                this.n();
                this.j.set(0);
                this.unlock();
            } catch (Throwable v0_2) {
                this.unlock();
                throw v0_2;
            }
        }
        return;
    }

    final boolean c(Object p3, int p4)
    {
        int v0_0 = 0;
        try {
            if (this.b == 0) {
                this.a();
            } else {
                Object v1_1 = this.e(p3, p4);
                if (v1_1 != null) {
                    if (v1_1.a().get() != null) {
                        v0_0 = 1;
                    }
                    this.a();
                } else {
                    this.a();
                }
            }
        } catch (int v0_1) {
            this.a();
            throw v0_1;
        }
        return v0_0;
    }

    final Object d(Object p9, int p10)
    {
        this.lock();
        try {
            this.c();
            java.util.concurrent.atomic.AtomicReferenceArray v4 = this.e;
            int v5 = (p10 & (v4.length() - 1));
            com.a.b.d.qq v0_3 = ((com.a.b.d.rz) v4.get(v5));
            com.a.b.d.rz v3 = v0_3;
        } catch (com.a.b.d.qq v0_5) {
            this.unlock();
            this.d();
            throw v0_5;
        }
        while (v3 != null) {
            Object v6 = v3.d();
            if ((v3.c() != p10) || ((v6 == null) || (!this.a.m.a(p9, v6)))) {
                v3 = v3.b();
            } else {
                com.a.b.d.qq v1_1;
                int v7_0 = v3.a();
                Object v2_5 = v7_0.get();
                if (v2_5 == null) {
                    if (!com.a.b.d.sa.a(v7_0)) {
                        this.unlock();
                        this.d();
                        com.a.b.d.qq v0_4 = 0;
                        return v0_4;
                    } else {
                        v1_1 = com.a.b.d.qq.c;
                    }
                } else {
                    v1_1 = com.a.b.d.qq.a;
                }
                this.c = (this.c + 1);
                this.a(v6, v2_5, v1_1);
                com.a.b.d.qq v1_3 = (this.b - 1);
                v4.set(v5, this.b(v0_3, v3));
                this.b = v1_3;
                this.unlock();
                this.d();
                v0_4 = v2_5;
            }
            return v0_4;
        }
        this.unlock();
        this.d();
        v0_4 = 0;
        return v0_4;
    }

    final void d()
    {
        if (!this.isHeldByCurrentThread()) {
            while (((com.a.b.d.qx) this.a.t.poll()) != null) {
            }
        }
        return;
    }
}
