package com.a.b.d;
final class ro extends java.util.AbstractSet {
    final synthetic com.a.b.d.qy a;

    ro(com.a.b.d.qy p1)
    {
        this.a = p1;
        return;
    }

    public final void clear()
    {
        this.a.clear();
        return;
    }

    public final boolean contains(Object p5)
    {
        int v0 = 0;
        if ((p5 instanceof java.util.Map$Entry)) {
            boolean v1_1 = ((java.util.Map$Entry) p5).getKey();
            if (v1_1) {
                boolean v1_2 = this.a.get(v1_1);
                if ((v1_2) && (this.a.n.a(((java.util.Map$Entry) p5).getValue(), v1_2))) {
                    v0 = 1;
                }
            }
        }
        return v0;
    }

    public final boolean isEmpty()
    {
        return this.a.isEmpty();
    }

    public final java.util.Iterator iterator()
    {
        return new com.a.b.d.rn(this.a);
    }

    public final boolean remove(Object p5)
    {
        int v0 = 0;
        if ((p5 instanceof java.util.Map$Entry)) {
            boolean v1_1 = ((java.util.Map$Entry) p5).getKey();
            if ((v1_1) && (this.a.remove(v1_1, ((java.util.Map$Entry) p5).getValue()))) {
                v0 = 1;
            }
        }
        return v0;
    }

    public final int size()
    {
        return this.a.size();
    }
}
