package com.a.b.d;
public final class io extends com.a.b.d.ba {
    private static final int b = 2;
    private static final long c;
    transient int a;

    private io()
    {
        this(new java.util.HashMap());
        this.a = 2;
        return;
    }

    private io(int p2, int p3)
    {
        int v0_2;
        this(com.a.b.d.sz.a(p2));
        this.a = 2;
        if (p3 < 0) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        com.a.b.b.cn.a(v0_2);
        this.a = p3;
        return;
    }

    private io(com.a.b.d.vi p2)
    {
        this(com.a.b.d.sz.a(p2.p().size()));
        this.a = 2;
        this.a(p2);
        return;
    }

    private static com.a.b.d.io a(int p1, int p2)
    {
        return new com.a.b.d.io(p1, p2);
    }

    private void a(java.io.ObjectInputStream p3)
    {
        p3.defaultReadObject();
        this.a = p3.readInt();
        int v0_1 = p3.readInt();
        this.a(com.a.b.d.sz.a(v0_1));
        com.a.b.d.zz.a(this, p3, v0_1);
        return;
    }

    private void a(java.io.ObjectOutputStream p2)
    {
        p2.defaultWriteObject();
        p2.writeInt(this.a);
        com.a.b.d.zz.a(this, p2);
        return;
    }

    private static com.a.b.d.io b(com.a.b.d.vi p1)
    {
        return new com.a.b.d.io(p1);
    }

    public static com.a.b.d.io v()
    {
        return new com.a.b.d.io();
    }

    final java.util.Set a()
    {
        return com.a.b.d.aad.a(this.a);
    }

    public final bridge synthetic java.util.Set a(Object p2)
    {
        return super.a(p2);
    }

    public final bridge synthetic java.util.Set a(Object p2, Iterable p3)
    {
        return super.a(p2, p3);
    }

    public final bridge synthetic boolean a(com.a.b.d.vi p2)
    {
        return super.a(p2);
    }

    public final bridge synthetic boolean a(Object p2, Object p3)
    {
        return super.a(p2, p3);
    }

    public final bridge synthetic java.util.Map b()
    {
        return super.b();
    }

    public final bridge synthetic java.util.Set b(Object p2)
    {
        return super.b(p2);
    }

    public final bridge synthetic boolean b(Object p2, Object p3)
    {
        return super.b(p2, p3);
    }

    final synthetic java.util.Collection c()
    {
        return com.a.b.d.aad.a(this.a);
    }

    public final bridge synthetic boolean c(Object p2, Iterable p3)
    {
        return super.c(p2, p3);
    }

    public final bridge synthetic boolean c(Object p2, Object p3)
    {
        return super.c(p2, p3);
    }

    public final bridge synthetic boolean equals(Object p2)
    {
        return super.equals(p2);
    }

    public final bridge synthetic int f()
    {
        return super.f();
    }

    public final bridge synthetic boolean f(Object p2)
    {
        return super.f(p2);
    }

    public final bridge synthetic void g()
    {
        super.g();
        return;
    }

    public final bridge synthetic boolean g(Object p2)
    {
        return super.g(p2);
    }

    public final bridge synthetic int hashCode()
    {
        return super.hashCode();
    }

    public final bridge synthetic java.util.Collection i()
    {
        return super.i();
    }

    public final bridge synthetic boolean n()
    {
        return super.n();
    }

    public final bridge synthetic java.util.Set p()
    {
        return super.p();
    }

    public final bridge synthetic com.a.b.d.xc q()
    {
        return super.q();
    }

    public final bridge synthetic String toString()
    {
        return super.toString();
    }

    public final bridge synthetic java.util.Set u()
    {
        return super.u();
    }
}
