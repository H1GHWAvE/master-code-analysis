package com.a.b.d;
final class zz {

    private zz()
    {
        return;
    }

    private static int a(java.io.ObjectInputStream p1)
    {
        return p1.readInt();
    }

    static com.a.b.d.aab a(Class p3, String p4)
    {
        try {
            return new com.a.b.d.aab(p3.getDeclaredField(p4), 0);
        } catch (NoSuchFieldException v0_1) {
            throw new AssertionError(v0_1);
        }
    }

    static void a(com.a.b.d.vi p1, java.io.ObjectInputStream p2)
    {
        com.a.b.d.zz.a(p1, p2, p2.readInt());
        return;
    }

    static void a(com.a.b.d.vi p6, java.io.ObjectInputStream p7, int p8)
    {
        int v2 = 0;
        while (v2 < p8) {
            java.util.Collection v3 = p6.c(p7.readObject());
            int v4 = p7.readInt();
            int v0_1 = 0;
            while (v0_1 < v4) {
                v3.add(p7.readObject());
                v0_1++;
            }
            v2++;
        }
        return;
    }

    static void a(com.a.b.d.vi p3, java.io.ObjectOutputStream p4)
    {
        p4.writeInt(p3.b().size());
        java.util.Iterator v2 = p3.b().entrySet().iterator();
        while (v2.hasNext()) {
            java.util.Iterator v0_6 = ((java.util.Map$Entry) v2.next());
            p4.writeObject(v0_6.getKey());
            p4.writeInt(((java.util.Collection) v0_6.getValue()).size());
            java.util.Iterator v0_9 = ((java.util.Collection) v0_6.getValue()).iterator();
            while (v0_9.hasNext()) {
                p4.writeObject(v0_9.next());
            }
        }
        return;
    }

    static void a(com.a.b.d.xc p1, java.io.ObjectInputStream p2)
    {
        com.a.b.d.zz.a(p1, p2, p2.readInt());
        return;
    }

    static void a(com.a.b.d.xc p3, java.io.ObjectInputStream p4, int p5)
    {
        int v0 = 0;
        while (v0 < p5) {
            p3.a(p4.readObject(), p4.readInt());
            v0++;
        }
        return;
    }

    static void a(com.a.b.d.xc p3, java.io.ObjectOutputStream p4)
    {
        p4.writeInt(p3.a().size());
        java.util.Iterator v1 = p3.a().iterator();
        while (v1.hasNext()) {
            int v0_5 = ((com.a.b.d.xd) v1.next());
            p4.writeObject(v0_5.a());
            p4.writeInt(v0_5.b());
        }
        return;
    }

    static void a(java.util.Map p1, java.io.ObjectInputStream p2)
    {
        com.a.b.d.zz.a(p1, p2, p2.readInt());
        return;
    }

    static void a(java.util.Map p3, java.io.ObjectInputStream p4, int p5)
    {
        int v0 = 0;
        while (v0 < p5) {
            p3.put(p4.readObject(), p4.readObject());
            v0++;
        }
        return;
    }

    static void a(java.util.Map p3, java.io.ObjectOutputStream p4)
    {
        p4.writeInt(p3.size());
        java.util.Iterator v1 = p3.entrySet().iterator();
        while (v1.hasNext()) {
            Object v0_4 = ((java.util.Map$Entry) v1.next());
            p4.writeObject(v0_4.getKey());
            p4.writeObject(v0_4.getValue());
        }
        return;
    }
}
