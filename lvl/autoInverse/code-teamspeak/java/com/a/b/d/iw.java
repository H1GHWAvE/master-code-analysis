package com.a.b.d;
public final class iw extends com.a.b.d.gs implements com.a.b.d.ck, java.io.Serializable {
    private final com.a.b.d.jt a;

    private iw(com.a.b.d.jt p1)
    {
        this.a = p1;
        return;
    }

    synthetic iw(com.a.b.d.jt p1, byte p2)
    {
        this(p1);
        return;
    }

    private static com.a.b.d.iw a(java.util.Map p6)
    {
        com.a.b.d.iw v6_2;
        if (!(p6 instanceof com.a.b.d.iw)) {
            com.a.b.d.iy v2_1 = new com.a.b.d.iy();
            java.util.Iterator v3 = p6.entrySet().iterator();
            while (v3.hasNext()) {
                com.a.b.d.jt v0_6 = ((java.util.Map$Entry) v3.next());
                int v1_2 = ((Class) v0_6.getKey());
                v2_1.a.a(v1_2, com.a.b.l.z.a(v1_2).cast(v0_6.getValue()));
            }
            v6_2 = new com.a.b.d.iw(v2_1.a.a(), 0);
        } else {
            v6_2 = ((com.a.b.d.iw) p6);
        }
        return v6_2;
    }

    private static com.a.b.d.iy b()
    {
        return new com.a.b.d.iy();
    }

    public final Object a(Class p3)
    {
        return this.a.get(com.a.b.b.cn.a(p3));
    }

    public final Object a(Class p2, Object p3)
    {
        throw new UnsupportedOperationException();
    }

    protected final java.util.Map a()
    {
        return this.a;
    }

    protected final bridge synthetic Object k_()
    {
        return this.a;
    }
}
