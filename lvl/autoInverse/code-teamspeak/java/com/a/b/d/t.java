package com.a.b.d;
abstract class t implements java.util.Iterator {
    final java.util.Iterator b;
    Object c;
    java.util.Collection d;
    java.util.Iterator e;
    final synthetic com.a.b.d.n f;

    t(com.a.b.d.n p3)
    {
        this.f = p3;
        this.b = com.a.b.d.n.a(p3).entrySet().iterator();
        this.c = 0;
        this.d = 0;
        this.e = com.a.b.d.nj.b();
        return;
    }

    abstract Object a();

    public boolean hasNext()
    {
        if ((!this.b.hasNext()) && (!this.e.hasNext())) {
            int v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    public Object next()
    {
        if (!this.e.hasNext()) {
            Object v0_4 = ((java.util.Map$Entry) this.b.next());
            this.c = v0_4.getKey();
            this.d = ((java.util.Collection) v0_4.getValue());
            this.e = this.d.iterator();
        }
        return this.a(this.c, this.e.next());
    }

    public void remove()
    {
        this.e.remove();
        if (this.d.isEmpty()) {
            this.b.remove();
        }
        com.a.b.d.n.b(this.f);
        return;
    }
}
