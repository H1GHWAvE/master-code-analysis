package com.a.b.d;
abstract class bf implements com.a.b.d.adv {
    private transient java.util.Set a;
    private transient java.util.Collection b;

    bf()
    {
        return;
    }

    public Object a(Object p2, Object p3, Object p4)
    {
        return this.e(p2).put(p3, p4);
    }

    public java.util.Set a()
    {
        return this.m().keySet();
    }

    public void a(com.a.b.d.adv p5)
    {
        java.util.Iterator v1 = p5.e().iterator();
        while (v1.hasNext()) {
            Object v0_3 = ((com.a.b.d.adw) v1.next());
            this.a(v0_3.a(), v0_3.b(), v0_3.c());
        }
        return;
    }

    public boolean a(Object p2)
    {
        return com.a.b.d.sz.b(this.m(), p2);
    }

    public boolean a(Object p2, Object p3)
    {
        int v0_4;
        int v0_2 = ((java.util.Map) com.a.b.d.sz.a(this.m(), p2));
        if ((v0_2 == 0) || (!com.a.b.d.sz.b(v0_2, p3))) {
            v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    public Object b(Object p2, Object p3)
    {
        Object v0_3;
        Object v0_2 = ((java.util.Map) com.a.b.d.sz.a(this.m(), p2));
        if (v0_2 != null) {
            v0_3 = com.a.b.d.sz.a(v0_2, p3);
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public java.util.Set b()
    {
        return this.l().keySet();
    }

    public boolean b(Object p2)
    {
        return com.a.b.d.sz.b(this.l(), p2);
    }

    public Object c(Object p2, Object p3)
    {
        Object v0_3;
        Object v0_2 = ((java.util.Map) com.a.b.d.sz.a(this.m(), p2));
        if (v0_2 != null) {
            v0_3 = com.a.b.d.sz.c(v0_2, p3);
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public boolean c()
    {
        int v0_1;
        if (this.k() != 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public boolean c(Object p3)
    {
        java.util.Iterator v1 = this.m().values().iterator();
        while (v1.hasNext()) {
            if (((java.util.Map) v1.next()).containsValue(p3)) {
                int v0_3 = 1;
            }
            return v0_3;
        }
        v0_3 = 0;
        return v0_3;
    }

    public void d()
    {
        com.a.b.d.nj.i(this.e().iterator());
        return;
    }

    public java.util.Set e()
    {
        java.util.Set v0 = this.a;
        if (v0 == null) {
            v0 = this.f();
            this.a = v0;
        }
        return v0;
    }

    public boolean equals(Object p2)
    {
        return com.a.b.d.adx.a(this, p2);
    }

    java.util.Set f()
    {
        return new com.a.b.d.bh(this);
    }

    abstract java.util.Iterator g();

    public java.util.Collection h()
    {
        java.util.Collection v0 = this.b;
        if (v0 == null) {
            v0 = this.i();
            this.b = v0;
        }
        return v0;
    }

    public int hashCode()
    {
        return this.e().hashCode();
    }

    java.util.Collection i()
    {
        return new com.a.b.d.bi(this);
    }

    java.util.Iterator m_()
    {
        return new com.a.b.d.bg(this, this.e().iterator());
    }

    public String toString()
    {
        return this.m().toString();
    }
}
