package com.a.b.d;
public abstract class hk extends com.a.b.d.gs implements java.util.SortedMap {

    protected hk()
    {
        return;
    }

    private int b(Object p2, Object p3)
    {
        int v0_1;
        int v0_0 = this.comparator();
        if (v0_0 != 0) {
            v0_1 = v0_0.compare(p2, p3);
        } else {
            v0_1 = ((Comparable) p2).compareTo(p3);
        }
        return v0_1;
    }

    protected synthetic java.util.Map a()
    {
        return this.c();
    }

    protected java.util.SortedMap a(Object p3, Object p4)
    {
        java.util.SortedMap v0_1;
        if (this.b(p3, p4) > 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.a(v0_1, "fromKey must be <= toKey");
        return this.tailMap(p3).headMap(p4);
    }

    protected abstract java.util.SortedMap c();

    protected final boolean c(Object p3)
    {
        int v0 = 0;
        try {
            if (this.b(this.tailMap(p3).firstKey(), p3) == 0) {
                v0 = 1;
            }
        } catch (NullPointerException v1) {
        } catch (NullPointerException v1) {
        } catch (NullPointerException v1) {
        }
        return v0;
    }

    public java.util.Comparator comparator()
    {
        return this.c().comparator();
    }

    public Object firstKey()
    {
        return this.c().firstKey();
    }

    public java.util.SortedMap headMap(Object p2)
    {
        return this.c().headMap(p2);
    }

    protected synthetic Object k_()
    {
        return this.c();
    }

    public Object lastKey()
    {
        return this.c().lastKey();
    }

    public java.util.SortedMap subMap(Object p2, Object p3)
    {
        return this.c().subMap(p2, p3);
    }

    public java.util.SortedMap tailMap(Object p2)
    {
        return this.c().tailMap(p2);
    }
}
