package com.a.b.d;
public final class aeq extends com.a.b.d.bb {
    private static final long c;
    private transient java.util.Comparator a;
    private transient java.util.Comparator b;

    private aeq(java.util.Comparator p2, java.util.Comparator p3)
    {
        this(new java.util.TreeMap(p2));
        this.a = p2;
        this.b = p3;
        return;
    }

    private aeq(java.util.Comparator p1, java.util.Comparator p2, com.a.b.d.vi p3)
    {
        this(p1, p2);
        this.a(p3);
        return;
    }

    private java.util.Comparator A()
    {
        return this.a;
    }

    private java.util.NavigableMap B()
    {
        return ((java.util.NavigableMap) super.w());
    }

    private java.util.NavigableSet C()
    {
        return ((java.util.NavigableSet) super.x());
    }

    private java.util.NavigableSet D()
    {
        return new com.a.b.d.x(this, ((java.util.NavigableMap) super.w()));
    }

    private java.util.NavigableMap E()
    {
        return ((java.util.NavigableMap) super.v());
    }

    private java.util.NavigableMap F()
    {
        return new com.a.b.d.w(this, ((java.util.NavigableMap) super.w()));
    }

    private static com.a.b.d.aeq a(java.util.Comparator p3, java.util.Comparator p4)
    {
        return new com.a.b.d.aeq(((java.util.Comparator) com.a.b.b.cn.a(p3)), ((java.util.Comparator) com.a.b.b.cn.a(p4)));
    }

    private void a(java.io.ObjectInputStream p3)
    {
        p3.defaultReadObject();
        this.a = ((java.util.Comparator) com.a.b.b.cn.a(((java.util.Comparator) p3.readObject())));
        this.b = ((java.util.Comparator) com.a.b.b.cn.a(((java.util.Comparator) p3.readObject())));
        this.a(new java.util.TreeMap(this.a));
        com.a.b.d.zz.a(this, p3);
        return;
    }

    private void a(java.io.ObjectOutputStream p2)
    {
        p2.defaultWriteObject();
        p2.writeObject(this.a);
        p2.writeObject(this.d_());
        com.a.b.d.zz.a(this, p2);
        return;
    }

    private static com.a.b.d.aeq b(com.a.b.d.vi p3)
    {
        return new com.a.b.d.aeq(com.a.b.d.yd.d(), com.a.b.d.yd.d(), p3);
    }

    private java.util.NavigableSet j(Object p2)
    {
        return ((java.util.NavigableSet) super.h(p2));
    }

    private static com.a.b.d.aeq z()
    {
        return new com.a.b.d.aeq(com.a.b.d.yd.d(), com.a.b.d.yd.d());
    }

    final java.util.Collection a(Object p3, java.util.Collection p4)
    {
        return new com.a.b.d.af(this, p3, ((java.util.NavigableSet) p4), 0);
    }

    final java.util.Collection a(java.util.Collection p2)
    {
        return com.a.b.d.aad.a(((java.util.NavigableSet) p2));
    }

    final synthetic java.util.Set a()
    {
        return this.y();
    }

    public final synthetic java.util.Set a(Object p2)
    {
        return this.j(p2);
    }

    public final bridge synthetic boolean a(com.a.b.d.vi p2)
    {
        return super.a(p2);
    }

    public final bridge synthetic boolean a(Object p2, Object p3)
    {
        return super.a(p2, p3);
    }

    public final synthetic java.util.Map b()
    {
        return ((java.util.NavigableMap) super.v());
    }

    public final bridge synthetic boolean b(Object p2, Object p3)
    {
        return super.b(p2, p3);
    }

    final synthetic java.util.Collection c()
    {
        return this.y();
    }

    public final synthetic java.util.Collection c(Object p2)
    {
        return this.j(p2);
    }

    public final bridge synthetic boolean c(Object p2, Iterable p3)
    {
        return super.c(p2, p3);
    }

    public final bridge synthetic boolean c(Object p2, Object p3)
    {
        return super.c(p2, p3);
    }

    public final bridge synthetic java.util.SortedSet d(Object p2, Iterable p3)
    {
        return super.d(p2, p3);
    }

    public final java.util.Comparator d_()
    {
        return this.b;
    }

    final java.util.Collection e(Object p2)
    {
        if (p2 == null) {
            this.a.compare(p2, p2);
        }
        return super.e(p2);
    }

    final synthetic java.util.Map e()
    {
        return ((java.util.NavigableMap) super.w());
    }

    public final bridge synthetic boolean equals(Object p2)
    {
        return super.equals(p2);
    }

    public final bridge synthetic int f()
    {
        return super.f();
    }

    public final bridge synthetic boolean f(Object p2)
    {
        return super.f(p2);
    }

    public final bridge synthetic void g()
    {
        super.g();
        return;
    }

    public final bridge synthetic boolean g(Object p2)
    {
        return super.g(p2);
    }

    final synthetic java.util.Set h()
    {
        return new com.a.b.d.x(this, ((java.util.NavigableMap) super.w()));
    }

    public final synthetic java.util.SortedSet h(Object p2)
    {
        return this.j(p2);
    }

    public final bridge synthetic int hashCode()
    {
        return super.hashCode();
    }

    public final bridge synthetic java.util.Collection i()
    {
        return super.i();
    }

    public final bridge synthetic java.util.SortedSet i(Object p2)
    {
        return super.i(p2);
    }

    final synthetic java.util.Map m()
    {
        return new com.a.b.d.w(this, ((java.util.NavigableMap) super.w()));
    }

    public final bridge synthetic boolean n()
    {
        return super.n();
    }

    public final synthetic java.util.Set p()
    {
        return ((java.util.NavigableSet) super.x());
    }

    public final bridge synthetic com.a.b.d.xc q()
    {
        return super.q();
    }

    public final bridge synthetic String toString()
    {
        return super.toString();
    }

    public final bridge synthetic java.util.Set u()
    {
        return super.u();
    }

    public final bridge synthetic java.util.SortedMap v()
    {
        return ((java.util.NavigableMap) super.v());
    }

    final bridge synthetic java.util.SortedMap w()
    {
        return ((java.util.NavigableMap) super.w());
    }

    public final bridge synthetic java.util.SortedSet x()
    {
        return ((java.util.NavigableSet) super.x());
    }

    final java.util.SortedSet y()
    {
        return new java.util.TreeSet(this.b);
    }
}
