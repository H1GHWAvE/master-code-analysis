package com.a.b.d;
final class pk extends java.util.AbstractSequentialList implements java.io.Serializable {
    private static final long c;
    final java.util.List a;
    final com.a.b.b.bj b;

    pk(java.util.List p2, com.a.b.b.bj p3)
    {
        this.a = ((java.util.List) com.a.b.b.cn.a(p2));
        this.b = ((com.a.b.b.bj) com.a.b.b.cn.a(p3));
        return;
    }

    public final void clear()
    {
        this.a.clear();
        return;
    }

    public final java.util.ListIterator listIterator(int p3)
    {
        return new com.a.b.d.pl(this, this.a.listIterator(p3));
    }

    public final int size()
    {
        return this.a.size();
    }
}
