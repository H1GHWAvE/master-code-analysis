package com.a.b.d;
 class ul implements com.a.b.d.qj {
    final java.util.Map a;
    final java.util.Map b;
    final java.util.Map c;
    final java.util.Map d;

    ul(java.util.Map p2, java.util.Map p3, java.util.Map p4, java.util.Map p5)
    {
        this.a = com.a.b.d.sz.b(p2);
        this.b = com.a.b.d.sz.b(p3);
        this.c = com.a.b.d.sz.b(p4);
        this.d = com.a.b.d.sz.b(p5);
        return;
    }

    public final boolean a()
    {
        if ((!this.a.isEmpty()) || ((!this.b.isEmpty()) || (!this.d.isEmpty()))) {
            int v0_6 = 0;
        } else {
            v0_6 = 1;
        }
        return v0_6;
    }

    public java.util.Map b()
    {
        return this.a;
    }

    public java.util.Map c()
    {
        return this.b;
    }

    public java.util.Map d()
    {
        return this.c;
    }

    public java.util.Map e()
    {
        return this.d;
    }

    public boolean equals(Object p5)
    {
        int v0 = 1;
        if (p5 != this) {
            if (!(p5 instanceof com.a.b.d.qj)) {
                v0 = 0;
            } else {
                if ((!this.b().equals(((com.a.b.d.qj) p5).b())) || ((!this.c().equals(((com.a.b.d.qj) p5).c())) || ((!this.d().equals(((com.a.b.d.qj) p5).d())) || (!this.e().equals(((com.a.b.d.qj) p5).e()))))) {
                    v0 = 0;
                }
            }
        }
        return v0;
    }

    public int hashCode()
    {
        int v0_1 = new Object[4];
        v0_1[0] = this.b();
        v0_1[1] = this.c();
        v0_1[2] = this.d();
        v0_1[3] = this.e();
        return java.util.Arrays.hashCode(v0_1);
    }

    public String toString()
    {
        if ((!this.a.isEmpty()) || ((!this.b.isEmpty()) || (!this.d.isEmpty()))) {
            String v0_6 = 0;
        } else {
            v0_6 = 1;
        }
        String v0_9;
        if (v0_6 == null) {
            String v0_8 = new StringBuilder("not equal");
            if (!this.a.isEmpty()) {
                v0_8.append(": only on left=").append(this.a);
            }
            if (!this.b.isEmpty()) {
                v0_8.append(": only on right=").append(this.b);
            }
            if (!this.d.isEmpty()) {
                v0_8.append(": value differences=").append(this.d);
            }
            v0_9 = v0_8.toString();
        } else {
            v0_9 = "equal";
        }
        return v0_9;
    }
}
