package com.a.b.d;
public final class ael extends com.a.b.d.abu {
    private static final long d;
    final java.util.Comparator c;

    private ael(java.util.Comparator p3, java.util.Comparator p4)
    {
        this(new java.util.TreeMap(p3), new com.a.b.d.aeo(p4));
        this.c = p4;
        return;
    }

    private static com.a.b.d.ael a(com.a.b.d.ael p3)
    {
        com.a.b.d.ael v0_1 = new com.a.b.d.ael(super.l_().comparator(), p3.c);
        super.a(p3);
        return v0_1;
    }

    private static com.a.b.d.ael a(java.util.Comparator p1, java.util.Comparator p2)
    {
        com.a.b.b.cn.a(p1);
        com.a.b.b.cn.a(p2);
        return new com.a.b.d.ael(p1, p2);
    }

    private java.util.SortedMap f(Object p2)
    {
        return new com.a.b.d.aep(this, p2);
    }

    private static com.a.b.d.ael p()
    {
        return new com.a.b.d.ael(com.a.b.d.yd.d(), com.a.b.d.yd.d());
    }

    private java.util.Comparator q()
    {
        return super.l_().comparator();
    }

    private java.util.Comparator r()
    {
        return this.c;
    }

    public final bridge synthetic Object a(Object p2, Object p3, Object p4)
    {
        return super.a(p2, p3, p4);
    }

    public final synthetic java.util.Set a()
    {
        return super.l_();
    }

    public final bridge synthetic void a(com.a.b.d.adv p1)
    {
        super.a(p1);
        return;
    }

    public final bridge synthetic boolean a(Object p2)
    {
        return super.a(p2);
    }

    public final bridge synthetic boolean a(Object p2, Object p3)
    {
        return super.a(p2, p3);
    }

    public final bridge synthetic Object b(Object p2, Object p3)
    {
        return super.b(p2, p3);
    }

    public final bridge synthetic java.util.Set b()
    {
        return super.b();
    }

    public final bridge synthetic boolean b(Object p2)
    {
        return super.b(p2);
    }

    public final bridge synthetic Object c(Object p2, Object p3)
    {
        return super.c(p2, p3);
    }

    public final bridge synthetic boolean c()
    {
        return super.c();
    }

    public final bridge synthetic boolean c(Object p2)
    {
        return super.c(p2);
    }

    public final bridge synthetic java.util.Map d(Object p2)
    {
        return super.d(p2);
    }

    public final bridge synthetic void d()
    {
        super.d();
        return;
    }

    public final synthetic java.util.Map e(Object p2)
    {
        return new com.a.b.d.aep(this, p2);
    }

    public final bridge synthetic java.util.Set e()
    {
        return super.e();
    }

    public final bridge synthetic boolean equals(Object p2)
    {
        return super.equals(p2);
    }

    public final bridge synthetic java.util.Collection h()
    {
        return super.h();
    }

    public final bridge synthetic int hashCode()
    {
        return super.hashCode();
    }

    public final java.util.SortedMap j()
    {
        return super.j();
    }

    public final bridge synthetic int k()
    {
        return super.k();
    }

    public final bridge synthetic java.util.Map l()
    {
        return super.l();
    }

    public final java.util.SortedSet l_()
    {
        return super.l_();
    }

    public final synthetic java.util.Map m()
    {
        return super.j();
    }

    final java.util.Iterator o()
    {
        java.util.Comparator v0 = this.c;
        return new com.a.b.d.aen(this, com.a.b.d.nj.a(com.a.b.d.mq.a(this.a.values(), new com.a.b.d.aem(this)), v0), v0);
    }

    public final bridge synthetic String toString()
    {
        return super.toString();
    }
}
