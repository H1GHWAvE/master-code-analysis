package com.a.b.d;
public abstract class agj extends com.a.b.d.agi implements java.util.ListIterator {

    protected agj()
    {
        return;
    }

    public final void add(Object p2)
    {
        throw new UnsupportedOperationException();
    }

    public final void set(Object p2)
    {
        throw new UnsupportedOperationException();
    }
}
