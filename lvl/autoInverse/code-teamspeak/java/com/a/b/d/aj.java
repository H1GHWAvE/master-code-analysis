package com.a.b.d;
final class aj implements java.util.Iterator {
    java.util.Map$Entry a;
    final synthetic java.util.Iterator b;
    final synthetic com.a.b.d.ai c;

    aj(com.a.b.d.ai p1, java.util.Iterator p2)
    {
        this.c = p1;
        this.b = p2;
        return;
    }

    private com.a.b.d.xd a()
    {
        java.util.Map$Entry v0_2 = ((java.util.Map$Entry) this.b.next());
        this.a = v0_2;
        return new com.a.b.d.ak(this, v0_2);
    }

    public final boolean hasNext()
    {
        return this.b.hasNext();
    }

    public final synthetic Object next()
    {
        java.util.Map$Entry v0_2 = ((java.util.Map$Entry) this.b.next());
        this.a = v0_2;
        return new com.a.b.d.ak(this, v0_2);
    }

    public final void remove()
    {
        int v0_1;
        if (this.a == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.b(v0_1, "no calls to next() since the last call to remove()");
        com.a.b.d.ai.a(this.c, ((long) ((com.a.b.d.dv) this.a.getValue()).b(0)));
        this.b.remove();
        this.a = 0;
        return;
    }
}
