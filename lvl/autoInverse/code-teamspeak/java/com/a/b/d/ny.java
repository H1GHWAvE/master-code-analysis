package com.a.b.d;
final class ny extends com.a.b.d.agi {
    final java.util.Queue a;

    public ny(Iterable p4, java.util.Comparator p5)
    {
        this.a = new java.util.PriorityQueue(2, new com.a.b.d.nz(this, p5));
        java.util.Iterator v1_2 = p4.iterator();
        while (v1_2.hasNext()) {
            com.a.b.d.yi v0_4 = ((java.util.Iterator) v1_2.next());
            if (v0_4.hasNext()) {
                this.a.add(com.a.b.d.nj.j(v0_4));
            }
        }
        return;
    }

    public final boolean hasNext()
    {
        int v0_2;
        if (this.a.isEmpty()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final Object next()
    {
        com.a.b.d.yi v0_2 = ((com.a.b.d.yi) this.a.remove());
        Object v1 = v0_2.next();
        if (v0_2.hasNext()) {
            this.a.add(v0_2);
        }
        return v1;
    }
}
