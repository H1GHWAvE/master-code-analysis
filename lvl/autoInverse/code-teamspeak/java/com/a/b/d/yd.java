package com.a.b.d;
public abstract class yd implements java.util.Comparator {
    static final int c = 1;
    static final int d = 255;

    public yd()
    {
        return;
    }

    private int a(java.util.List p2, Object p3)
    {
        return java.util.Collections.binarySearch(p2, p3, this);
    }

    private int a(Object[] p4, int p5, int p6, int p7)
    {
        Object v1 = p4[p7];
        p4[p7] = p4[p6];
        p4[p6] = v1;
        int v0_1 = p5;
        while (p5 < p6) {
            if (this.compare(p4[p5], v1) < 0) {
                com.a.b.d.yc.a(p4, v0_1, p5);
                v0_1++;
            }
            p5++;
        }
        com.a.b.d.yc.a(p4, p6, v0_1);
        return v0_1;
    }

    private static com.a.b.d.yd a(com.a.b.d.yd p1)
    {
        return ((com.a.b.d.yd) com.a.b.b.cn.a(p1));
    }

    private static varargs com.a.b.d.yd a(Object p2, Object[] p3)
    {
        return new com.a.b.d.fh(new com.a.b.d.pa(p2, p3));
    }

    public static com.a.b.d.yd a(java.util.Comparator p1)
    {
        com.a.b.d.cu v1_1;
        if (!(p1 instanceof com.a.b.d.yd)) {
            v1_1 = new com.a.b.d.cu(p1);
        } else {
            v1_1 = ((com.a.b.d.yd) p1);
        }
        return v1_1;
    }

    private static com.a.b.d.yd a(java.util.List p1)
    {
        return new com.a.b.d.fh(p1);
    }

    private java.util.List a(Iterable p9, int p10)
    {
        if ((!(p9 instanceof java.util.Collection)) || (((long) ((java.util.Collection) p9).size()) > (2 * ((long) p10)))) {
            java.util.List v0_4 = this.a(p9.iterator(), p10);
        } else {
            java.util.List v0_6 = ((Object[]) ((java.util.Collection) p9).toArray());
            java.util.Arrays.sort(v0_6, this);
            if (v0_6.length > p10) {
                v0_6 = com.a.b.d.yc.b(v0_6, p10);
            }
            v0_4 = java.util.Collections.unmodifiableList(java.util.Arrays.asList(v0_6));
        }
        return v0_4;
    }

    private java.util.List a(java.util.Iterator p11, int p12)
    {
        java.util.List v0_7;
        com.a.b.b.cn.a(p11);
        com.a.b.d.cl.a(p12, "k");
        if ((p12 != 0) && (p11.hasNext())) {
            if (p12 < 1073741823) {
                int v7 = (p12 * 2);
                java.util.List v0_3 = new Object[v7];
                java.util.List v0_4 = ((Object[]) v0_3);
                int v2_0 = p11.next();
                v0_4[0] = v2_0;
                int v1_0 = 1;
                while ((v1_0 < p12) && (p11.hasNext())) {
                    int v4_2 = p11.next();
                    int v3_7 = (v1_0 + 1);
                    v0_4[v1_0] = v4_2;
                    v2_0 = this.b(v2_0, v4_2);
                    v1_0 = v3_7;
                }
                while (p11.hasNext()) {
                    int v4_0 = p11.next();
                    if (this.compare(v4_0, v2_0) < 0) {
                        int v3_3 = (v1_0 + 1);
                        v0_4[v1_0] = v4_0;
                        if (v3_3 != v7) {
                            v1_0 = v3_3;
                        } else {
                            int v4_1 = 0;
                            int v6 = (v7 - 1);
                            int v2_1 = 0;
                            while (v2_1 < v6) {
                                int v1_5 = (((v2_1 + v6) + 1) >> 1);
                                java.util.List v8 = v0_4[v1_5];
                                v0_4[v1_5] = v0_4[v6];
                                v0_4[v6] = v8;
                                int v1_6 = v2_1;
                                int v3_5 = v2_1;
                                while (v3_5 < v6) {
                                    if (this.compare(v0_4[v3_5], v8) < 0) {
                                        com.a.b.d.yc.a(v0_4, v1_6, v3_5);
                                        v1_6++;
                                    }
                                    v3_5++;
                                }
                                com.a.b.d.yc.a(v0_4, v6, v1_6);
                                if (v1_6 <= p12) {
                                    if (v1_6 >= p12) {
                                        break;
                                    }
                                    v2_1 = Math.max(v1_6, (v2_1 + 1));
                                    v4_1 = v1_6;
                                } else {
                                    v6 = (v1_6 - 1);
                                }
                            }
                            v2_0 = v0_4[v4_1];
                            int v1_7 = (v4_1 + 1);
                            while (v1_7 < p12) {
                                v2_0 = this.b(v2_0, v0_4[v1_7]);
                                v1_7++;
                            }
                            v1_0 = p12;
                        }
                    }
                }
                java.util.Arrays.sort(v0_4, 0, v1_0, this);
                v0_7 = java.util.Collections.unmodifiableList(java.util.Arrays.asList(com.a.b.d.yc.b(v0_4, Math.min(v1_0, p12))));
            } else {
                java.util.List v0_8 = com.a.b.d.ov.a(p11);
                java.util.Collections.sort(v0_8, this);
                if (v0_8.size() > p12) {
                    v0_8.subList(p12, v0_8.size()).clear();
                }
                v0_8.trimToSize();
                v0_7 = java.util.Collections.unmodifiableList(v0_8);
            }
        } else {
            v0_7 = com.a.b.d.jl.d();
        }
        return v0_7;
    }

    private com.a.b.d.yd b(java.util.Comparator p3)
    {
        return new com.a.b.d.cy(this, ((java.util.Comparator) com.a.b.b.cn.a(p3)));
    }

    private java.util.List b(Iterable p9, int p10)
    {
        java.util.List v0_4;
        int v1_0 = this.a();
        if ((!(p9 instanceof java.util.Collection)) || (((long) ((java.util.Collection) p9).size()) > (2 * ((long) p10)))) {
            v0_4 = v1_0.a(p9.iterator(), p10);
        } else {
            java.util.List v0_6 = ((Object[]) ((java.util.Collection) p9).toArray());
            java.util.Arrays.sort(v0_6, v1_0);
            if (v0_6.length > p10) {
                v0_6 = com.a.b.d.yc.b(v0_6, p10);
            }
            v0_4 = java.util.Collections.unmodifiableList(java.util.Arrays.asList(v0_6));
        }
        return v0_4;
    }

    private java.util.List b(java.util.Iterator p2, int p3)
    {
        return this.a().a(p2, p3);
    }

    public static com.a.b.d.yd d()
    {
        return com.a.b.d.xz.a;
    }

    public static com.a.b.d.yd e()
    {
        return com.a.b.d.agl.a;
    }

    private static com.a.b.d.yd e(Iterable p1)
    {
        return new com.a.b.d.cy(p1);
    }

    private static com.a.b.d.yd f()
    {
        return com.a.b.d.bj.a;
    }

    private boolean f(Iterable p4)
    {
        Object v0_2;
        java.util.Iterator v2 = p4.iterator();
        if (!v2.hasNext()) {
            v0_2 = 1;
        } else {
            Object v0_1 = v2.next();
            while (v2.hasNext()) {
                Object v1_1 = v2.next();
                if (this.compare(v0_1, v1_1) <= 0) {
                    v0_1 = v1_1;
                } else {
                    v0_2 = 0;
                }
            }
        }
        return v0_2;
    }

    private static com.a.b.d.yd g()
    {
        return com.a.b.d.yg.a;
    }

    private boolean g(Iterable p4)
    {
        Object v0_2;
        java.util.Iterator v2 = p4.iterator();
        if (!v2.hasNext()) {
            v0_2 = 1;
        } else {
            Object v0_1 = v2.next();
            while (v2.hasNext()) {
                Object v1_1 = v2.next();
                if (this.compare(v0_1, v1_1) < 0) {
                    v0_1 = v1_1;
                } else {
                    v0_2 = 0;
                }
            }
        }
        return v0_2;
    }

    private com.a.b.d.yd h()
    {
        return this.a(com.a.b.d.sz.a());
    }

    private com.a.b.d.yd i()
    {
        return new com.a.b.d.ob(this);
    }

    public com.a.b.d.yd a()
    {
        return new com.a.b.d.zx(this);
    }

    public final com.a.b.d.yd a(com.a.b.b.bj p2)
    {
        return new com.a.b.d.ch(p2, this);
    }

    public Object a(Object p2, Object p3)
    {
        if (this.compare(p2, p3) > 0) {
            p2 = p3;
        }
        return p2;
    }

    public varargs Object a(Object p5, Object p6, Object p7, Object[] p8)
    {
        Object v1 = this.a(this.a(p5, p6), p7);
        int v2 = p8.length;
        int v0_1 = 0;
        while (v0_1 < v2) {
            v1 = this.a(v1, p8[v0_1]);
            v0_1++;
        }
        return v1;
    }

    public Object a(java.util.Iterator p3)
    {
        Object v0 = p3.next();
        while (p3.hasNext()) {
            v0 = this.a(v0, p3.next());
        }
        return v0;
    }

    public java.util.List a(Iterable p2)
    {
        java.util.ArrayList v0_1 = ((Object[]) com.a.b.d.mq.c(p2));
        java.util.Arrays.sort(v0_1, this);
        return com.a.b.d.ov.a(java.util.Arrays.asList(v0_1));
    }

    public com.a.b.d.jl b(Iterable p5)
    {
        com.a.b.d.jl v0_1 = ((Object[]) com.a.b.d.mq.c(p5));
        int v2 = v0_1.length;
        int v1 = 0;
        while (v1 < v2) {
            com.a.b.b.cn.a(v0_1[v1]);
            v1++;
        }
        java.util.Arrays.sort(v0_1, this);
        return com.a.b.d.jl.b(v0_1);
    }

    public com.a.b.d.yd b()
    {
        return new com.a.b.d.ya(this);
    }

    public Object b(Object p2, Object p3)
    {
        if (this.compare(p2, p3) < 0) {
            p2 = p3;
        }
        return p2;
    }

    public varargs Object b(Object p5, Object p6, Object p7, Object[] p8)
    {
        Object v1 = this.b(this.b(p5, p6), p7);
        int v2 = p8.length;
        int v0_1 = 0;
        while (v0_1 < v2) {
            v1 = this.b(v1, p8[v0_1]);
            v0_1++;
        }
        return v1;
    }

    public Object b(java.util.Iterator p3)
    {
        Object v0 = p3.next();
        while (p3.hasNext()) {
            v0 = this.b(v0, p3.next());
        }
        return v0;
    }

    public com.a.b.d.yd c()
    {
        return new com.a.b.d.yb(this);
    }

    public Object c(Iterable p2)
    {
        return this.a(p2.iterator());
    }

    public abstract int compare();

    public Object d(Iterable p2)
    {
        return this.b(p2.iterator());
    }
}
