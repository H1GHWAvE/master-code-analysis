package com.a.b.d;
abstract class il implements java.util.Iterator {
    int b;
    com.a.b.d.ia c;
    com.a.b.d.ia d;
    int e;
    final synthetic com.a.b.d.hy f;

    il(com.a.b.d.hy p3)
    {
        this.f = p3;
        this.b = 0;
        this.c = 0;
        this.d = 0;
        this.e = com.a.b.d.hy.a(this.f);
        return;
    }

    private void a()
    {
        if (com.a.b.d.hy.a(this.f) == this.e) {
            return;
        } else {
            throw new java.util.ConcurrentModificationException();
        }
    }

    abstract Object a();

    public boolean hasNext()
    {
        int v0 = 1;
        this.a();
        if (this.c == null) {
            while (this.b < com.a.b.d.hy.b(this.f).length) {
                if (com.a.b.d.hy.b(this.f)[this.b] == null) {
                    this.b = (this.b + 1);
                } else {
                    int v1_8 = com.a.b.d.hy.b(this.f);
                    int v2_4 = this.b;
                    this.b = (v2_4 + 1);
                    this.c = v1_8[v2_4];
                }
            }
            v0 = 0;
        }
        return v0;
    }

    public Object next()
    {
        this.a();
        if (this.hasNext()) {
            Object v0_1 = this.c;
            this.c = v0_1.c;
            this.d = v0_1;
            return this.a(v0_1);
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public void remove()
    {
        int v0_1;
        this.a();
        if (this.d == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.b(v0_1, "no calls to next() since the last call to remove()");
        com.a.b.d.hy.a(this.f, this.d);
        this.e = com.a.b.d.hy.a(this.f);
        this.d = 0;
        return;
    }
}
