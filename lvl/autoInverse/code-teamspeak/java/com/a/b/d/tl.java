package com.a.b.d;
abstract class tl extends com.a.b.d.uj {
    final java.util.Map a;
    final com.a.b.b.co b;

    tl(java.util.Map p1, com.a.b.b.co p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    final boolean b(Object p3, Object p4)
    {
        return this.b.a(com.a.b.d.sz.a(p3, p4));
    }

    final java.util.Collection c_()
    {
        return new com.a.b.d.ui(this, this.a, this.b);
    }

    public boolean containsKey(Object p2)
    {
        if ((!this.a.containsKey(p2)) || (!this.b(p2, this.a.get(p2)))) {
            int v0_5 = 0;
        } else {
            v0_5 = 1;
        }
        return v0_5;
    }

    public Object get(Object p3)
    {
        int v0_1 = this.a.get(p3);
        if ((v0_1 == 0) || (!this.b(p3, v0_1))) {
            v0_1 = 0;
        }
        return v0_1;
    }

    public boolean isEmpty()
    {
        return this.entrySet().isEmpty();
    }

    public Object put(Object p2, Object p3)
    {
        com.a.b.b.cn.a(this.b(p2, p3));
        return this.a.put(p2, p3);
    }

    public void putAll(java.util.Map p4)
    {
        java.util.Iterator v1 = p4.entrySet().iterator();
        while (v1.hasNext()) {
            java.util.Map v0_4 = ((java.util.Map$Entry) v1.next());
            com.a.b.b.cn.a(this.b(v0_4.getKey(), v0_4.getValue()));
        }
        this.a.putAll(p4);
        return;
    }

    public Object remove(Object p2)
    {
        int v0_1;
        if (!this.containsKey(p2)) {
            v0_1 = 0;
        } else {
            v0_1 = this.a.remove(p2);
        }
        return v0_1;
    }
}
