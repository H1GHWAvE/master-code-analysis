package com.a.b.d;
 class acq extends com.a.b.d.uj {
    final synthetic com.a.b.d.abx b;

    acq(com.a.b.d.abx p1)
    {
        this.b = p1;
        return;
    }

    private java.util.Map a(Object p2)
    {
        int v0_2;
        if (!this.b.a(p2)) {
            v0_2 = 0;
        } else {
            v0_2 = this.b.e(p2);
        }
        return v0_2;
    }

    private java.util.Map b(Object p2)
    {
        java.util.Map v0_3;
        if (p2 != null) {
            v0_3 = ((java.util.Map) this.b.a.remove(p2));
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    protected final java.util.Set a()
    {
        return new com.a.b.d.acr(this);
    }

    public boolean containsKey(Object p2)
    {
        return this.b.a(p2);
    }

    public synthetic Object get(Object p2)
    {
        int v0_2;
        if (!this.b.a(p2)) {
            v0_2 = 0;
        } else {
            v0_2 = this.b.e(p2);
        }
        return v0_2;
    }

    public bridge synthetic Object remove(Object p2)
    {
        java.util.Map v0_3;
        if (p2 != null) {
            v0_3 = ((java.util.Map) this.b.a.remove(p2));
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }
}
