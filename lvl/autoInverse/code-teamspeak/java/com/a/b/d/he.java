package com.a.b.d;
public abstract class he extends com.a.b.d.hp implements java.util.NavigableSet {

    protected he()
    {
        return;
    }

    private Object a(Object p3)
    {
        return com.a.b.d.nj.d(this.headSet(p3, 0).descendingIterator(), 0);
    }

    private java.util.NavigableSet a(Object p2, boolean p3, Object p4, boolean p5)
    {
        return this.tailSet(p2, p3).headSet(p4, p5);
    }

    private Object d(Object p3)
    {
        return com.a.b.d.nj.d(this.headSet(p3, 1).descendingIterator(), 0);
    }

    private Object e()
    {
        return com.a.b.d.nj.h(this.iterator());
    }

    private Object e(Object p3)
    {
        return com.a.b.d.nj.d(this.tailSet(p3, 1).iterator(), 0);
    }

    private Object f()
    {
        return com.a.b.d.nj.h(this.descendingIterator());
    }

    private Object f(Object p3)
    {
        return com.a.b.d.nj.d(this.tailSet(p3, 0).iterator(), 0);
    }

    private java.util.SortedSet g(Object p2)
    {
        return this.headSet(p2, 0);
    }

    private Object h()
    {
        return this.iterator().next();
    }

    private java.util.SortedSet h(Object p2)
    {
        return this.tailSet(p2, 1);
    }

    private Object i()
    {
        return this.descendingIterator().next();
    }

    protected synthetic java.util.Set a()
    {
        return this.d();
    }

    protected final java.util.SortedSet a(Object p3, Object p4)
    {
        return this.subSet(p3, 1, p4, 0);
    }

    protected synthetic java.util.Collection b()
    {
        return this.d();
    }

    protected synthetic java.util.SortedSet c()
    {
        return this.d();
    }

    public Object ceiling(Object p2)
    {
        return this.d().ceiling(p2);
    }

    protected abstract java.util.NavigableSet d();

    public java.util.Iterator descendingIterator()
    {
        return this.d().descendingIterator();
    }

    public java.util.NavigableSet descendingSet()
    {
        return this.d().descendingSet();
    }

    public Object floor(Object p2)
    {
        return this.d().floor(p2);
    }

    public java.util.NavigableSet headSet(Object p2, boolean p3)
    {
        return this.d().headSet(p2, p3);
    }

    public Object higher(Object p2)
    {
        return this.d().higher(p2);
    }

    protected synthetic Object k_()
    {
        return this.d();
    }

    public Object lower(Object p2)
    {
        return this.d().lower(p2);
    }

    public Object pollFirst()
    {
        return this.d().pollFirst();
    }

    public Object pollLast()
    {
        return this.d().pollLast();
    }

    public java.util.NavigableSet subSet(Object p2, boolean p3, Object p4, boolean p5)
    {
        return this.d().subSet(p2, p3, p4, p5);
    }

    public java.util.NavigableSet tailSet(Object p2, boolean p3)
    {
        return this.d().tailSet(p2, p3);
    }
}
