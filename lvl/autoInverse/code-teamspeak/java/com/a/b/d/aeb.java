package com.a.b.d;
final class aeb extends com.a.b.d.bf {
    final com.a.b.d.adv a;
    final com.a.b.b.bj b;

    aeb(com.a.b.d.adv p2, com.a.b.b.bj p3)
    {
        this.a = ((com.a.b.d.adv) com.a.b.b.cn.a(p2));
        this.b = ((com.a.b.b.bj) com.a.b.b.cn.a(p3));
        return;
    }

    private com.a.b.b.bj n()
    {
        return new com.a.b.d.aec(this);
    }

    public final Object a(Object p2, Object p3, Object p4)
    {
        throw new UnsupportedOperationException();
    }

    public final java.util.Set a()
    {
        return this.a.a();
    }

    public final void a(com.a.b.d.adv p2)
    {
        throw new UnsupportedOperationException();
    }

    public final boolean a(Object p2, Object p3)
    {
        return this.a.a(p2, p3);
    }

    public final Object b(Object p3, Object p4)
    {
        int v0_1;
        if (!this.a(p3, p4)) {
            v0_1 = 0;
        } else {
            v0_1 = this.b.e(this.a.b(p3, p4));
        }
        return v0_1;
    }

    public final java.util.Set b()
    {
        return this.a.b();
    }

    public final Object c(Object p3, Object p4)
    {
        int v0_1;
        if (!this.a(p3, p4)) {
            v0_1 = 0;
        } else {
            v0_1 = this.b.e(this.a.c(p3, p4));
        }
        return v0_1;
    }

    public final java.util.Map d(Object p3)
    {
        return com.a.b.d.sz.a(this.a.d(p3), this.b);
    }

    public final void d()
    {
        this.a.d();
        return;
    }

    public final java.util.Map e(Object p3)
    {
        return com.a.b.d.sz.a(this.a.e(p3), this.b);
    }

    final java.util.Iterator g()
    {
        return com.a.b.d.nj.a(this.a.e().iterator(), new com.a.b.d.aec(this));
    }

    final java.util.Collection i()
    {
        return com.a.b.d.cm.a(this.a.h(), this.b);
    }

    public final int k()
    {
        return this.a.k();
    }

    public final java.util.Map l()
    {
        return com.a.b.d.sz.a(this.a.l(), new com.a.b.d.aee(this));
    }

    public final java.util.Map m()
    {
        return com.a.b.d.sz.a(this.a.m(), new com.a.b.d.aed(this));
    }
}
