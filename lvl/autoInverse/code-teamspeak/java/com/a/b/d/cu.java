package com.a.b.d;
final class cu extends com.a.b.d.yd implements java.io.Serializable {
    private static final long b;
    final java.util.Comparator a;

    cu(java.util.Comparator p2)
    {
        this.a = ((java.util.Comparator) com.a.b.b.cn.a(p2));
        return;
    }

    public final int compare(Object p2, Object p3)
    {
        return this.a.compare(p2, p3);
    }

    public final boolean equals(Object p3)
    {
        int v0_1;
        if (p3 != this) {
            if (!(p3 instanceof com.a.b.d.cu)) {
                v0_1 = 0;
            } else {
                v0_1 = this.a.equals(((com.a.b.d.cu) p3).a);
            }
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final int hashCode()
    {
        return this.a.hashCode();
    }

    public final String toString()
    {
        return this.a.toString();
    }
}
