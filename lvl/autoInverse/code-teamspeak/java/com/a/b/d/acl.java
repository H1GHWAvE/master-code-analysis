package com.a.b.d;
final class acl extends com.a.b.d.vb {
    final synthetic com.a.b.d.aci a;

    acl(com.a.b.d.aci p1)
    {
        this.a = p1;
        this(p1);
        return;
    }

    public final boolean remove(Object p4)
    {
        java.util.Iterator v2 = this.a.entrySet().iterator();
        while (v2.hasNext()) {
            int v0_5 = ((java.util.Map$Entry) v2.next());
            if (((java.util.Map) v0_5.getValue()).equals(p4)) {
                com.a.b.d.abx.a(this.a.a, v0_5.getKey());
                int v0_3 = 1;
            }
            return v0_3;
        }
        v0_3 = 0;
        return v0_3;
    }

    public final boolean removeAll(java.util.Collection p5)
    {
        com.a.b.b.cn.a(p5);
        int v0_0 = 0;
        java.util.Iterator v1_5 = com.a.b.d.ov.a(this.a.a.b().iterator()).iterator();
        while (v1_5.hasNext()) {
            Object v2_1 = v1_5.next();
            if (p5.contains(this.a.a.d(v2_1))) {
                com.a.b.d.abx.a(this.a.a, v2_1);
                v0_0 = 1;
            }
        }
        return v0_0;
    }

    public final boolean retainAll(java.util.Collection p5)
    {
        com.a.b.b.cn.a(p5);
        int v0_0 = 0;
        java.util.Iterator v1_5 = com.a.b.d.ov.a(this.a.a.b().iterator()).iterator();
        while (v1_5.hasNext()) {
            Object v2_1 = v1_5.next();
            if (!p5.contains(this.a.a.d(v2_1))) {
                com.a.b.d.abx.a(this.a.a, v2_1);
                v0_0 = 1;
            }
        }
        return v0_0;
    }
}
