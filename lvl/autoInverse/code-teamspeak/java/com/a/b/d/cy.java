package com.a.b.d;
final class cy extends com.a.b.d.yd implements java.io.Serializable {
    private static final long b;
    final com.a.b.d.jl a;

    cy(Iterable p2)
    {
        this.a = com.a.b.d.jl.a(p2);
        return;
    }

    cy(java.util.Comparator p2, java.util.Comparator p3)
    {
        this.a = com.a.b.d.jl.a(p2, p3);
        return;
    }

    public final int compare(Object p5, Object p6)
    {
        int v3 = this.a.size();
        int v2 = 0;
        while (v2 < v3) {
            int v0_1 = ((java.util.Comparator) this.a.get(v2)).compare(p5, p6);
            if (v0_1 == 0) {
                v2++;
            }
            return v0_1;
        }
        v0_1 = 0;
        return v0_1;
    }

    public final boolean equals(Object p3)
    {
        int v0_1;
        if (p3 != this) {
            if (!(p3 instanceof com.a.b.d.cy)) {
                v0_1 = 0;
            } else {
                v0_1 = this.a.equals(((com.a.b.d.cy) p3).a);
            }
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final int hashCode()
    {
        return this.a.hashCode();
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        return new StringBuilder((v0_2.length() + 19)).append("Ordering.compound(").append(v0_2).append(")").toString();
    }
}
