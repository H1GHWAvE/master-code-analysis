package com.a.b.d;
public abstract class hm extends com.a.b.d.gy implements com.a.b.d.abn {

    protected hm()
    {
        return;
    }

    private com.a.b.d.abn b(Object p2, com.a.b.d.ce p3, Object p4, com.a.b.d.ce p5)
    {
        return this.c(p2, p3).d(p4, p5);
    }

    private com.a.b.d.xd e()
    {
        com.a.b.d.xd v0_5;
        com.a.b.d.xd v0_1 = this.a().iterator();
        if (v0_1.hasNext()) {
            com.a.b.d.xd v0_3 = ((com.a.b.d.xd) v0_1.next());
            v0_5 = com.a.b.d.xe.a(v0_3.a(), v0_3.b());
        } else {
            v0_5 = 0;
        }
        return v0_5;
    }

    private com.a.b.d.xd q()
    {
        com.a.b.d.xd v0_6;
        com.a.b.d.xd v0_2 = this.m().a().iterator();
        if (v0_2.hasNext()) {
            com.a.b.d.xd v0_4 = ((com.a.b.d.xd) v0_2.next());
            v0_6 = com.a.b.d.xe.a(v0_4.a(), v0_4.b());
        } else {
            v0_6 = 0;
        }
        return v0_6;
    }

    private com.a.b.d.xd r()
    {
        com.a.b.d.xd v0_5;
        java.util.Iterator v1 = this.a().iterator();
        if (v1.hasNext()) {
            com.a.b.d.xd v0_3 = ((com.a.b.d.xd) v1.next());
            v0_5 = com.a.b.d.xe.a(v0_3.a(), v0_3.b());
            v1.remove();
        } else {
            v0_5 = 0;
        }
        return v0_5;
    }

    private com.a.b.d.xd s()
    {
        com.a.b.d.xd v0_6;
        java.util.Iterator v1 = this.m().a().iterator();
        if (v1.hasNext()) {
            com.a.b.d.xd v0_4 = ((com.a.b.d.xd) v1.next());
            v0_6 = com.a.b.d.xe.a(v0_4.a(), v0_4.b());
            v1.remove();
        } else {
            v0_6 = 0;
        }
        return v0_6;
    }

    public final com.a.b.d.abn a(Object p2, com.a.b.d.ce p3, Object p4, com.a.b.d.ce p5)
    {
        return this.c().a(p2, p3, p4, p5);
    }

    protected final synthetic java.util.Collection b()
    {
        return this.c();
    }

    protected abstract com.a.b.d.abn c();

    public final com.a.b.d.abn c(Object p2, com.a.b.d.ce p3)
    {
        return this.c().c(p2, p3);
    }

    public java.util.Comparator comparator()
    {
        return this.c().comparator();
    }

    public final com.a.b.d.abn d(Object p2, com.a.b.d.ce p3)
    {
        return this.c().d(p2, p3);
    }

    public final java.util.NavigableSet e_()
    {
        return ((java.util.NavigableSet) super.n_());
    }

    protected final synthetic com.a.b.d.xc f()
    {
        return this.c();
    }

    public final com.a.b.d.xd h()
    {
        return this.c().h();
    }

    public final com.a.b.d.xd i()
    {
        return this.c().i();
    }

    public final com.a.b.d.xd j()
    {
        return this.c().j();
    }

    public final com.a.b.d.xd k()
    {
        return this.c().k();
    }

    protected final synthetic Object k_()
    {
        return this.c();
    }

    public final com.a.b.d.abn m()
    {
        return this.c().m();
    }

    public final synthetic java.util.SortedSet n()
    {
        return ((java.util.NavigableSet) super.n_());
    }

    public final bridge synthetic java.util.Set n_()
    {
        return ((java.util.NavigableSet) super.n_());
    }
}
