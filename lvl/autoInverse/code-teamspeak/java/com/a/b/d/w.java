package com.a.b.d;
final class w extends com.a.b.d.z implements java.util.NavigableMap {
    final synthetic com.a.b.d.n c;

    w(com.a.b.d.n p1, java.util.NavigableMap p2)
    {
        this.c = p1;
        this(p1, p2);
        return;
    }

    private java.util.Map$Entry a(java.util.Iterator p4)
    {
        java.util.Map$Entry v0_4;
        if (p4.hasNext()) {
            java.util.Map$Entry v0_2 = ((java.util.Map$Entry) p4.next());
            java.util.Collection v2 = this.c.c();
            v2.addAll(((java.util.Collection) v0_2.getValue()));
            p4.remove();
            v0_4 = com.a.b.d.sz.a(v0_2.getKey(), this.c.a(v2));
        } else {
            v0_4 = 0;
        }
        return v0_4;
    }

    private java.util.NavigableMap a(Object p2)
    {
        return this.headMap(p2, 0);
    }

    private java.util.NavigableMap a(Object p3, Object p4)
    {
        return this.subMap(p3, 1, p4, 0);
    }

    private java.util.NavigableMap b(Object p2)
    {
        return this.tailMap(p2, 1);
    }

    private java.util.NavigableMap f()
    {
        return ((java.util.NavigableMap) super.d());
    }

    private java.util.NavigableSet g()
    {
        return ((java.util.NavigableSet) super.c());
    }

    private java.util.NavigableSet h()
    {
        return new com.a.b.d.x(this.c, ((java.util.NavigableMap) super.d()));
    }

    final synthetic java.util.SortedSet b()
    {
        return this.h();
    }

    public final bridge synthetic java.util.SortedSet c()
    {
        return ((java.util.NavigableSet) super.c());
    }

    public final java.util.Map$Entry ceilingEntry(Object p2)
    {
        java.util.Map$Entry v0_3;
        java.util.Map$Entry v0_2 = ((java.util.NavigableMap) super.d()).ceilingEntry(p2);
        if (v0_2 != null) {
            v0_3 = this.a(v0_2);
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public final Object ceilingKey(Object p2)
    {
        return ((java.util.NavigableMap) super.d()).ceilingKey(p2);
    }

    final bridge synthetic java.util.SortedMap d()
    {
        return ((java.util.NavigableMap) super.d());
    }

    public final java.util.NavigableSet descendingKeySet()
    {
        return this.descendingMap().navigableKeySet();
    }

    public final java.util.NavigableMap descendingMap()
    {
        return new com.a.b.d.w(this.c, ((java.util.NavigableMap) super.d()).descendingMap());
    }

    final synthetic java.util.Set e()
    {
        return this.h();
    }

    public final java.util.Map$Entry firstEntry()
    {
        java.util.Map$Entry v0_3;
        java.util.Map$Entry v0_2 = ((java.util.NavigableMap) super.d()).firstEntry();
        if (v0_2 != null) {
            v0_3 = this.a(v0_2);
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public final java.util.Map$Entry floorEntry(Object p2)
    {
        java.util.Map$Entry v0_3;
        java.util.Map$Entry v0_2 = ((java.util.NavigableMap) super.d()).floorEntry(p2);
        if (v0_2 != null) {
            v0_3 = this.a(v0_2);
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public final Object floorKey(Object p2)
    {
        return ((java.util.NavigableMap) super.d()).floorKey(p2);
    }

    public final java.util.NavigableMap headMap(Object p4, boolean p5)
    {
        return new com.a.b.d.w(this.c, ((java.util.NavigableMap) super.d()).headMap(p4, p5));
    }

    public final bridge synthetic java.util.SortedMap headMap(Object p2)
    {
        return this.headMap(p2, 0);
    }

    public final java.util.Map$Entry higherEntry(Object p2)
    {
        java.util.Map$Entry v0_3;
        java.util.Map$Entry v0_2 = ((java.util.NavigableMap) super.d()).higherEntry(p2);
        if (v0_2 != null) {
            v0_3 = this.a(v0_2);
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public final Object higherKey(Object p2)
    {
        return ((java.util.NavigableMap) super.d()).higherKey(p2);
    }

    public final synthetic java.util.Set keySet()
    {
        return ((java.util.NavigableSet) super.c());
    }

    public final java.util.Map$Entry lastEntry()
    {
        java.util.Map$Entry v0_3;
        java.util.Map$Entry v0_2 = ((java.util.NavigableMap) super.d()).lastEntry();
        if (v0_2 != null) {
            v0_3 = this.a(v0_2);
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public final java.util.Map$Entry lowerEntry(Object p2)
    {
        java.util.Map$Entry v0_3;
        java.util.Map$Entry v0_2 = ((java.util.NavigableMap) super.d()).lowerEntry(p2);
        if (v0_2 != null) {
            v0_3 = this.a(v0_2);
        } else {
            v0_3 = 0;
        }
        return v0_3;
    }

    public final Object lowerKey(Object p2)
    {
        return ((java.util.NavigableMap) super.d()).lowerKey(p2);
    }

    public final java.util.NavigableSet navigableKeySet()
    {
        return ((java.util.NavigableSet) super.c());
    }

    public final java.util.Map$Entry pollFirstEntry()
    {
        return this.a(this.entrySet().iterator());
    }

    public final java.util.Map$Entry pollLastEntry()
    {
        return this.a(this.descendingMap().entrySet().iterator());
    }

    public final java.util.NavigableMap subMap(Object p4, boolean p5, Object p6, boolean p7)
    {
        return new com.a.b.d.w(this.c, ((java.util.NavigableMap) super.d()).subMap(p4, p5, p6, p7));
    }

    public final bridge synthetic java.util.SortedMap subMap(Object p3, Object p4)
    {
        return this.subMap(p3, 1, p4, 0);
    }

    public final java.util.NavigableMap tailMap(Object p4, boolean p5)
    {
        return new com.a.b.d.w(this.c, ((java.util.NavigableMap) super.d()).tailMap(p4, p5));
    }

    public final bridge synthetic java.util.SortedMap tailMap(Object p2)
    {
        return this.tailMap(p2, 1);
    }
}
