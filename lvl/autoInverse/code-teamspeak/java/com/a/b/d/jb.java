package com.a.b.d;
public abstract class jb {
    static final int c = 4;

    jb()
    {
        return;
    }

    static int a(int p2, int p3)
    {
        if (p3 >= 0) {
            int v0_2 = (((p2 >> 1) + p2) + 1);
            if (v0_2 < p3) {
                v0_2 = (Integer.highestOneBit((p3 - 1)) << 1);
            }
            if (v0_2 < 0) {
                v0_2 = 2147483647;
            }
            return v0_2;
        } else {
            throw new AssertionError("cannot store more than MAX_VALUE elements");
        }
    }

    public abstract com.a.b.d.iz a();

    public com.a.b.d.jb a(Iterable p3)
    {
        java.util.Iterator v0 = p3.iterator();
        while (v0.hasNext()) {
            this.b(v0.next());
        }
        return this;
    }

    public com.a.b.d.jb a(java.util.Iterator p2)
    {
        while (p2.hasNext()) {
            this.b(p2.next());
        }
        return this;
    }

    public varargs com.a.b.d.jb a(Object[] p4)
    {
        int v1 = p4.length;
        int v0 = 0;
        while (v0 < v1) {
            this.b(p4[v0]);
            v0++;
        }
        return this;
    }

    public abstract com.a.b.d.jb b();
}
