package com.a.b.d;
abstract class aej implements java.util.Iterator {
    final java.util.Iterator c;

    aej(java.util.Iterator p2)
    {
        this.c = ((java.util.Iterator) com.a.b.b.cn.a(p2));
        return;
    }

    abstract Object a();

    public final boolean hasNext()
    {
        return this.c.hasNext();
    }

    public final Object next()
    {
        return this.a(this.c.next());
    }

    public final void remove()
    {
        this.c.remove();
        return;
    }
}
