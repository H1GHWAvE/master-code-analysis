package com.a.b.d;
final class nx implements java.util.Iterator {
    final synthetic int a;
    final synthetic java.util.Iterator b;
    private int c;

    nx(int p1, java.util.Iterator p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    public final boolean hasNext()
    {
        if ((this.c >= this.a) || (!this.b.hasNext())) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public final Object next()
    {
        if (this.hasNext()) {
            this.c = (this.c + 1);
            return this.b.next();
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public final void remove()
    {
        this.b.remove();
        return;
    }
}
