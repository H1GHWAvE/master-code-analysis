package com.a.b.d;
final class wf extends com.a.b.d.uj {
    final com.a.b.d.vi a;

    wf(com.a.b.d.vi p2)
    {
        this.a = ((com.a.b.d.vi) com.a.b.b.cn.a(p2));
        return;
    }

    static synthetic com.a.b.d.vi a(com.a.b.d.wf p1)
    {
        return p1.a;
    }

    private void a(Object p2)
    {
        this.a.p().remove(p2);
        return;
    }

    private java.util.Collection b(Object p2)
    {
        int v0_1;
        if (!this.containsKey(p2)) {
            v0_1 = 0;
        } else {
            v0_1 = this.a.c(p2);
        }
        return v0_1;
    }

    private java.util.Collection c(Object p2)
    {
        int v0_1;
        if (!this.containsKey(p2)) {
            v0_1 = 0;
        } else {
            v0_1 = this.a.d(p2);
        }
        return v0_1;
    }

    protected final java.util.Set a()
    {
        return new com.a.b.d.wg(this);
    }

    public final void clear()
    {
        this.a.g();
        return;
    }

    public final boolean containsKey(Object p2)
    {
        return this.a.f(p2);
    }

    public final synthetic Object get(Object p2)
    {
        int v0_1;
        if (!this.containsKey(p2)) {
            v0_1 = 0;
        } else {
            v0_1 = this.a.c(p2);
        }
        return v0_1;
    }

    public final boolean isEmpty()
    {
        return this.a.n();
    }

    public final java.util.Set keySet()
    {
        return this.a.p();
    }

    public final synthetic Object remove(Object p2)
    {
        int v0_1;
        if (!this.containsKey(p2)) {
            v0_1 = 0;
        } else {
            v0_1 = this.a.d(p2);
        }
        return v0_1;
    }

    public final int size()
    {
        return this.a.p().size();
    }
}
