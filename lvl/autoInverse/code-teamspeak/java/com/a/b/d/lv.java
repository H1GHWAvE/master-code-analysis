package com.a.b.d;
final class lv extends com.a.b.d.yw implements com.a.b.d.aay {

    lv(com.a.b.d.me p1, com.a.b.d.jl p2)
    {
        this(p1, p2);
        return;
    }

    private com.a.b.d.me i()
    {
        return ((com.a.b.d.me) super.b());
    }

    final bridge synthetic com.a.b.d.iz b()
    {
        return ((com.a.b.d.me) super.b());
    }

    final com.a.b.d.jl b(int p4, int p5)
    {
        return new com.a.b.d.zq(super.b(p4, p5), this.comparator()).f();
    }

    public final java.util.Comparator comparator()
    {
        return ((com.a.b.d.me) super.b()).comparator();
    }

    public final boolean contains(Object p2)
    {
        int v0_1;
        if (this.indexOf(p2) < 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final int indexOf(Object p3)
    {
        int v0_2 = ((com.a.b.d.me) super.b()).c(p3);
        if ((v0_2 < 0) || (!this.get(v0_2).equals(p3))) {
            v0_2 = -1;
        }
        return v0_2;
    }

    public final int lastIndexOf(Object p2)
    {
        return this.indexOf(p2);
    }
}
