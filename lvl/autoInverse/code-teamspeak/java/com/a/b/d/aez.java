package com.a.b.d;
final class aez extends com.a.b.d.xo {
    final Object a;
    int b;
    int c;
    long d;
    com.a.b.d.aez e;
    com.a.b.d.aez f;
    com.a.b.d.aez g;
    com.a.b.d.aez h;
    private int i;

    aez(Object p6, int p7)
    {
        int v0;
        if (p7 <= 0) {
            v0 = 0;
        } else {
            v0 = 1;
        }
        com.a.b.b.cn.a(v0);
        this.a = p6;
        this.b = p7;
        this.d = ((long) p7);
        this.c = 1;
        this.i = 1;
        this.e = 0;
        this.f = 0;
        return;
    }

    private com.a.b.d.aez a(com.a.b.d.aez p5)
    {
        com.a.b.d.aez v0_7;
        if (this.e != null) {
            this.e = this.e.a(p5);
            this.c = (this.c - 1);
            this.d = (this.d - ((long) p5.b));
            v0_7 = this.g();
        } else {
            v0_7 = this.f;
        }
        return v0_7;
    }

    private static synthetic com.a.b.d.aez a(com.a.b.d.aez p0, com.a.b.d.aez p1)
    {
        p0.h = p1;
        return p1;
    }

    private static synthetic com.a.b.d.aez a(com.a.b.d.aez p1, java.util.Comparator p2, Object p3)
    {
        return p1.a(p2, p3);
    }

    private com.a.b.d.aez a(Object p5, int p6)
    {
        this.f = new com.a.b.d.aez(p5, p6);
        com.a.b.d.aer.a(this, this.f, this.h);
        this.i = Math.max(2, this.i);
        this.c = (this.c + 1);
        this.d = (this.d + ((long) p6));
        return this;
    }

    private com.a.b.d.aez b(com.a.b.d.aez p5)
    {
        com.a.b.d.aez v0_7;
        if (this.f != null) {
            this.f = this.f.b(p5);
            this.c = (this.c - 1);
            this.d = (this.d - ((long) p5.b));
            v0_7 = this.g();
        } else {
            v0_7 = this.e;
        }
        return v0_7;
    }

    private static synthetic com.a.b.d.aez b(com.a.b.d.aez p0, com.a.b.d.aez p1)
    {
        p0.g = p1;
        return p1;
    }

    private static synthetic com.a.b.d.aez b(com.a.b.d.aez p1, java.util.Comparator p2, Object p3)
    {
        return p1.b(p2, p3);
    }

    private com.a.b.d.aez b(Object p5, int p6)
    {
        this.e = new com.a.b.d.aez(p5, p6);
        com.a.b.d.aer.a(this.g, this.e, this);
        this.i = Math.max(2, this.i);
        this.c = (this.c + 1);
        this.d = (this.d + ((long) p6));
        return this;
    }

    private int c(java.util.Comparator p3, Object p4)
    {
        int v0 = 0;
        while(true) {
            com.a.b.d.aez v1_1 = p3.compare(p4, this.a);
            if (v1_1 >= null) {
                if (v1_1 <= null) {
                    break;
                }
                if (this.f != null) {
                    this = this.f;
                }
            } else {
                if (this.e != null) {
                    this = this.e;
                }
            }
            return v0;
        }
        v0 = this.b;
        return v0;
    }

    private static long c(com.a.b.d.aez p2)
    {
        long v0;
        if (p2 != null) {
            v0 = p2.d;
        } else {
            v0 = 0;
        }
        return v0;
    }

    private com.a.b.d.aez c()
    {
        com.a.b.d.aez v0_1;
        com.a.b.d.aez v0_0 = this.b;
        this.b = 0;
        com.a.b.d.aer.a(this.g, this.h);
        if (this.e != null) {
            if (this.f != null) {
                if (this.e.i < this.f.i) {
                    com.a.b.d.aez v1_6 = this.h;
                    v1_6.f = this.f.a(v1_6);
                    v1_6.e = this.e;
                    v1_6.c = (this.c - 1);
                    v1_6.d = (this.d - ((long) v0_0));
                    v0_1 = v1_6.g();
                } else {
                    com.a.b.d.aez v1_7 = this.g;
                    v1_7.e = this.e.b(v1_7);
                    v1_7.f = this.f;
                    v1_7.c = (this.c - 1);
                    v1_7.d = (this.d - ((long) v0_0));
                    v0_1 = v1_7.g();
                }
            } else {
                v0_1 = this.e;
            }
        } else {
            v0_1 = this.f;
        }
        return v0_1;
    }

    private static int d(com.a.b.d.aez p1)
    {
        int v0;
        if (p1 != null) {
            v0 = p1.i;
        } else {
            v0 = 0;
        }
        return v0;
    }

    private void d()
    {
        this.c = ((com.a.b.d.aer.a(this.e) + 1) + com.a.b.d.aer.a(this.f));
        this.d = ((((long) this.b) + com.a.b.d.aez.c(this.e)) + com.a.b.d.aez.c(this.f));
        return;
    }

    private static synthetic int e(com.a.b.d.aez p1)
    {
        return p1.b;
    }

    private void e()
    {
        this.i = (Math.max(com.a.b.d.aez.d(this.e), com.a.b.d.aez.d(this.f)) + 1);
        return;
    }

    private static synthetic long f(com.a.b.d.aez p2)
    {
        return p2.d;
    }

    private void f()
    {
        this.c = ((com.a.b.d.aer.a(this.e) + 1) + com.a.b.d.aer.a(this.f));
        this.d = ((((long) this.b) + com.a.b.d.aez.c(this.e)) + com.a.b.d.aez.c(this.f));
        this.e();
        return;
    }

    private static synthetic int g(com.a.b.d.aez p1)
    {
        return p1.c;
    }

    private com.a.b.d.aez g()
    {
        switch (this.h()) {
            case -2:
                if (this.f.h() > 0) {
                    this.f = this.f.j();
                }
                this = this.i();
                break;
            case 2:
                if (this.e.h() < 0) {
                    this.e = this.e.i();
                }
                this = this.j();
                break;
            default:
                this.e();
        }
        return this;
    }

    private int h()
    {
        return (com.a.b.d.aez.d(this.e) - com.a.b.d.aez.d(this.f));
    }

    private static synthetic Object h(com.a.b.d.aez p1)
    {
        return p1.a;
    }

    private com.a.b.d.aez i()
    {
        void v0_1;
        if (this.f == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.b(v0_1);
        void v0_2 = this.f;
        this.f = v0_2.e;
        v0_2.e = this;
        v0_2.d = this.d;
        v0_2.c = this.c;
        this.f();
        return v0_2.e();
    }

    private static synthetic com.a.b.d.aez i(com.a.b.d.aez p1)
    {
        return p1.e;
    }

    private com.a.b.d.aez j()
    {
        void v0_1;
        if (this.e == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.b(v0_1);
        void v0_2 = this.e;
        this.e = v0_2.f;
        v0_2.f = this;
        v0_2.d = this.d;
        v0_2.c = this.c;
        this.f();
        return v0_2.e();
    }

    private static synthetic com.a.b.d.aez j(com.a.b.d.aez p1)
    {
        return p1.f;
    }

    private static synthetic com.a.b.d.aez k(com.a.b.d.aez p1)
    {
        return p1.h;
    }

    private static synthetic com.a.b.d.aez l(com.a.b.d.aez p1)
    {
        return p1.g;
    }

    final com.a.b.d.aez a(java.util.Comparator p2, Object p3)
    {
        while(true) {
            com.a.b.d.aez v0_1 = p2.compare(p3, this.a);
            if (v0_1 >= null) {
                if (v0_1 != null) {
                    if (this.f == null) {
                        break;
                    }
                    this = this.f;
                }
            } else {
                if (this.e != null) {
                    this = ((com.a.b.d.aez) com.a.b.b.ca.a(this.e.a(p2, p3), this));
                }
            }
            return this;
        }
        this = 0;
        return this;
    }

    final com.a.b.d.aez a(java.util.Comparator p8, Object p9, int p10, int p11, int[] p12)
    {
        int v0_1 = p8.compare(p9, this.a);
        if (v0_1 >= 0) {
            if (v0_1 <= 0) {
                p12[0] = this.b;
                if (p10 == this.b) {
                    if (p11 != 0) {
                        this.d = (this.d + ((long) (p11 - this.b)));
                        this.b = p11;
                    } else {
                        this = this.c();
                    }
                }
            } else {
                int v0_6 = this.f;
                if (v0_6 != 0) {
                    this.f = v0_6.a(p8, p9, p10, p11, p12);
                    if (p12[0] == p10) {
                        if ((p11 != 0) || (p12[0] == 0)) {
                            if ((p11 > 0) && (p12[0] == 0)) {
                                this.c = (this.c + 1);
                            }
                        } else {
                            this.c = (this.c - 1);
                        }
                        this.d = (this.d + ((long) (p11 - p12[0])));
                    }
                    this = this.g();
                } else {
                    p12[0] = 0;
                    if ((p10 == 0) && (p11 > 0)) {
                        this = this.a(p9, p11);
                    }
                }
            }
        } else {
            int v0_17 = this.e;
            if (v0_17 != 0) {
                this.e = v0_17.a(p8, p9, p10, p11, p12);
                if (p12[0] == p10) {
                    if ((p11 != 0) || (p12[0] == 0)) {
                        if ((p11 > 0) && (p12[0] == 0)) {
                            this.c = (this.c + 1);
                        }
                    } else {
                        this.c = (this.c - 1);
                    }
                    this.d = (this.d + ((long) (p11 - p12[0])));
                }
                this = this.g();
            } else {
                p12[0] = 0;
                if ((p10 == 0) && (p11 > 0)) {
                    this = this.b(p9, p11);
                }
            }
        }
        return this;
    }

    final com.a.b.d.aez a(java.util.Comparator p7, Object p8, int p9, int[] p10)
    {
        int v0_0 = 0;
        com.a.b.d.aez v1_1 = p7.compare(p8, this.a);
        if (v1_1 >= null) {
            if (v1_1 <= null) {
                p10[0] = this.b;
                if ((((long) this.b) + ((long) p9)) <= 2147483647) {
                    v0_0 = 1;
                }
                com.a.b.b.cn.a(v0_0);
                this.b = (this.b + p9);
                this.d = (this.d + ((long) p9));
            } else {
                com.a.b.d.aez v1_5 = this.f;
                if (v1_5 != null) {
                    long v2_3 = v1_5.i;
                    this.f = v1_5.a(p7, p8, p9, p10);
                    if (p10[0] == 0) {
                        this.c = (this.c + 1);
                    }
                    this.d = (this.d + ((long) p9));
                    if (this.f.i != v2_3) {
                        this = this.g();
                    }
                } else {
                    p10[0] = 0;
                    this = this.a(p8, p9);
                }
            }
        } else {
            com.a.b.d.aez v1_7 = this.e;
            if (v1_7 != null) {
                long v2_4 = v1_7.i;
                this.e = v1_7.a(p7, p8, p9, p10);
                if (p10[0] == 0) {
                    this.c = (this.c + 1);
                }
                this.d = (this.d + ((long) p9));
                if (this.e.i != v2_4) {
                    this = this.g();
                }
            } else {
                p10[0] = 0;
                this = this.b(p8, p9);
            }
        }
        return this;
    }

    public final Object a()
    {
        return this.a;
    }

    public final int b()
    {
        return this.b;
    }

    final com.a.b.d.aez b(java.util.Comparator p2, Object p3)
    {
        while(true) {
            com.a.b.d.aez v0_1 = p2.compare(p3, this.a);
            if (v0_1 <= null) {
                if (v0_1 != null) {
                    if (this.e == null) {
                        break;
                    }
                    this = this.e;
                }
            } else {
                if (this.f != null) {
                    this = ((com.a.b.d.aez) com.a.b.b.ca.a(this.f.b(p2, p3), this));
                }
            }
            return this;
        }
        this = 0;
        return this;
    }

    final com.a.b.d.aez b(java.util.Comparator p6, Object p7, int p8, int[] p9)
    {
        long v0_1 = p6.compare(p7, this.a);
        if (v0_1 >= 0) {
            if (v0_1 <= 0) {
                p9[0] = this.b;
                if (p8 < this.b) {
                    this.b = (this.b - p8);
                    this.d = (this.d - ((long) p8));
                } else {
                    this = this.c();
                }
            } else {
                long v0_8 = this.f;
                if (v0_8 != 0) {
                    this.f = v0_8.b(p6, p7, p8, p9);
                    if (p9[0] > 0) {
                        if (p8 < p9[0]) {
                            this.d = (this.d - ((long) p8));
                        } else {
                            this.c = (this.c - 1);
                            this.d = (this.d - ((long) p9[0]));
                        }
                    }
                    this = this.g();
                } else {
                    p9[0] = 0;
                }
            }
        } else {
            long v0_18 = this.e;
            if (v0_18 != 0) {
                this.e = v0_18.b(p6, p7, p8, p9);
                if (p9[0] > 0) {
                    if (p8 < p9[0]) {
                        this.d = (this.d - ((long) p8));
                    } else {
                        this.c = (this.c - 1);
                        this.d = (this.d - ((long) p9[0]));
                    }
                }
                if (p9[0] != 0) {
                    this = this.g();
                }
            } else {
                p9[0] = 0;
            }
        }
        return this;
    }

    final com.a.b.d.aez c(java.util.Comparator p5, Object p6, int p7, int[] p8)
    {
        int v0_1 = p5.compare(p6, this.a);
        if (v0_1 >= 0) {
            if (v0_1 <= 0) {
                p8[0] = this.b;
                if (p7 != 0) {
                    this.d = (this.d + ((long) (p7 - this.b)));
                    this.b = p7;
                } else {
                    this = this.c();
                }
            } else {
                int v0_5 = this.f;
                if (v0_5 != 0) {
                    this.f = v0_5.c(p5, p6, p7, p8);
                    if ((p7 != 0) || (p8[0] == 0)) {
                        if ((p7 > 0) && (p8[0] == 0)) {
                            this.c = (this.c + 1);
                        }
                    } else {
                        this.c = (this.c - 1);
                    }
                    this.d = (this.d + ((long) (p7 - p8[0])));
                    this = this.g();
                } else {
                    p8[0] = 0;
                    if (p7 > 0) {
                        this = this.a(p6, p7);
                    }
                }
            }
        } else {
            int v0_15 = this.e;
            if (v0_15 != 0) {
                this.e = v0_15.c(p5, p6, p7, p8);
                if ((p7 != 0) || (p8[0] == 0)) {
                    if ((p7 > 0) && (p8[0] == 0)) {
                        this.c = (this.c + 1);
                    }
                } else {
                    this.c = (this.c - 1);
                }
                this.d = (this.d + ((long) (p7 - p8[0])));
                this = this.g();
            } else {
                p8[0] = 0;
                if (p7 > 0) {
                    this = this.b(p6, p7);
                }
            }
        }
        return this;
    }

    public final String toString()
    {
        return com.a.b.d.xe.a(this.a, this.b).toString();
    }
}
