package com.a.b.d;
public abstract class gs extends com.a.b.d.hg implements java.util.Map {

    public gs()
    {
        return;
    }

    private Object a(Object p4)
    {
        java.util.Iterator v1 = this.entrySet().iterator();
        while (v1.hasNext()) {
            Object v0_4 = ((java.util.Map$Entry) v1.next());
            if (com.a.b.b.ce.a(v0_4.getKey(), p4)) {
                Object v0_2 = v0_4.getValue();
                v1.remove();
            }
            return v0_2;
        }
        v0_2 = 0;
        return v0_2;
    }

    private void a(java.util.Map p1)
    {
        com.a.b.d.sz.a(this, p1);
        return;
    }

    private boolean b(Object p2)
    {
        return com.a.b.d.sz.e(this, p2);
    }

    private void d()
    {
        com.a.b.d.nj.i(this.entrySet().iterator());
        return;
    }

    private boolean d(Object p2)
    {
        return com.a.b.d.sz.f(this, p2);
    }

    private boolean e()
    {
        int v0_3;
        if (this.entrySet().iterator().hasNext()) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    private int f()
    {
        return com.a.b.d.aad.a(this.entrySet());
    }

    private String h()
    {
        return com.a.b.d.sz.a(this);
    }

    public abstract java.util.Map a();

    protected boolean c(Object p2)
    {
        return com.a.b.d.sz.d(this, p2);
    }

    public void clear()
    {
        this.a().clear();
        return;
    }

    public boolean containsKey(Object p2)
    {
        return this.a().containsKey(p2);
    }

    public boolean containsValue(Object p2)
    {
        return this.a().containsValue(p2);
    }

    public java.util.Set entrySet()
    {
        return this.a().entrySet();
    }

    public boolean equals(Object p2)
    {
        if ((p2 != this) && (!this.a().equals(p2))) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public Object get(Object p2)
    {
        return this.a().get(p2);
    }

    public int hashCode()
    {
        return this.a().hashCode();
    }

    public boolean isEmpty()
    {
        return this.a().isEmpty();
    }

    public synthetic Object k_()
    {
        return this.a();
    }

    public java.util.Set keySet()
    {
        return this.a().keySet();
    }

    public Object put(Object p2, Object p3)
    {
        return this.a().put(p2, p3);
    }

    public void putAll(java.util.Map p2)
    {
        this.a().putAll(p2);
        return;
    }

    public Object remove(Object p2)
    {
        return this.a().remove(p2);
    }

    public int size()
    {
        return this.a().size();
    }

    public java.util.Collection values()
    {
        return this.a().values();
    }
}
