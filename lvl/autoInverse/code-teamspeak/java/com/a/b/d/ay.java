package com.a.b.d;
abstract class ay implements com.a.b.d.yr {

    ay()
    {
        return;
    }

    public void a(com.a.b.d.yl p2)
    {
        throw new UnsupportedOperationException();
    }

    public boolean a()
    {
        return this.g().isEmpty();
    }

    public boolean a(com.a.b.d.yr p3)
    {
        java.util.Iterator v1 = p3.g().iterator();
        while (v1.hasNext()) {
            if (!this.c(((com.a.b.d.yl) v1.next()))) {
                int v0_2 = 0;
            }
            return v0_2;
        }
        v0_2 = 1;
        return v0_2;
    }

    public boolean a(Comparable p2)
    {
        int v0_1;
        if (this.b(p2) == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public abstract com.a.b.d.yl b();

    public void b()
    {
        this.b(com.a.b.d.yl.c());
        return;
    }

    public void b(com.a.b.d.yl p2)
    {
        throw new UnsupportedOperationException();
    }

    public void b(com.a.b.d.yr p3)
    {
        java.util.Iterator v1 = p3.g().iterator();
        while (v1.hasNext()) {
            this.a(((com.a.b.d.yl) v1.next()));
        }
        return;
    }

    public void c(com.a.b.d.yr p3)
    {
        java.util.Iterator v1 = p3.g().iterator();
        while (v1.hasNext()) {
            this.b(((com.a.b.d.yl) v1.next()));
        }
        return;
    }

    public abstract boolean c();

    public boolean equals(Object p3)
    {
        int v0_1;
        if (p3 != this) {
            if (!(p3 instanceof com.a.b.d.yr)) {
                v0_1 = 0;
            } else {
                v0_1 = this.g().equals(((com.a.b.d.yr) p3).g());
            }
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final int hashCode()
    {
        return this.g().hashCode();
    }

    public final String toString()
    {
        return this.g().toString();
    }
}
