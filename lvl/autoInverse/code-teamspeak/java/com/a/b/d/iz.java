package com.a.b.d;
public abstract class iz extends java.util.AbstractCollection implements java.io.Serializable {
    private transient com.a.b.d.jl a;

    iz()
    {
        return;
    }

    int a(Object[] p4, int p5)
    {
        java.util.Iterator v1 = this.iterator();
        while (v1.hasNext()) {
            int v0_1 = (p5 + 1);
            p4[p5] = v1.next();
            p5 = v0_1;
        }
        return p5;
    }

    public final boolean add(Object p2)
    {
        throw new UnsupportedOperationException();
    }

    public final boolean addAll(java.util.Collection p2)
    {
        throw new UnsupportedOperationException();
    }

    public abstract com.a.b.d.agi c();

    public final void clear()
    {
        throw new UnsupportedOperationException();
    }

    public boolean contains(Object p2)
    {
        if ((p2 == null) || (!super.contains(p2))) {
            int v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public com.a.b.d.jl f()
    {
        com.a.b.d.jl v0 = this.a;
        if (v0 == null) {
            v0 = this.l();
            this.a = v0;
        }
        return v0;
    }

    Object g()
    {
        return new com.a.b.d.jp(this.toArray());
    }

    abstract boolean h_();

    public synthetic java.util.Iterator iterator()
    {
        return this.c();
    }

    com.a.b.d.jl l()
    {
        com.a.b.d.jl v0_3;
        switch (this.size()) {
            case 0:
                v0_3 = com.a.b.d.jl.d();
                break;
            case 1:
                v0_3 = com.a.b.d.jl.a(this.c().next());
                break;
            default:
                v0_3 = new com.a.b.d.yw(this, this.toArray());
        }
        return v0_3;
    }

    public final boolean remove(Object p2)
    {
        throw new UnsupportedOperationException();
    }

    public final boolean removeAll(java.util.Collection p2)
    {
        throw new UnsupportedOperationException();
    }

    public final boolean retainAll(java.util.Collection p2)
    {
        throw new UnsupportedOperationException();
    }

    public final Object[] toArray()
    {
        Object[] v0_1;
        Object[] v0_0 = this.size();
        if (v0_0 != null) {
            v0_1 = new Object[v0_0];
            this.a(v0_1, 0);
        } else {
            v0_1 = com.a.b.d.yc.a;
        }
        return v0_1;
    }

    public final Object[] toArray(Object[] p3)
    {
        com.a.b.b.cn.a(p3);
        int v0_0 = this.size();
        if (p3.length >= v0_0) {
            if (p3.length > v0_0) {
                p3[v0_0] = 0;
            }
        } else {
            p3 = com.a.b.d.yc.a(p3, v0_0);
        }
        this.a(p3, 0);
        return p3;
    }
}
