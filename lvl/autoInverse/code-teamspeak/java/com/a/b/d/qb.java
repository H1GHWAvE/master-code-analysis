package com.a.b.d;
 class qb extends com.a.b.d.gs {
    private final java.util.Map a;
    final com.a.b.d.pn b;
    private transient java.util.Set c;

    qb(java.util.Map p2, com.a.b.d.pn p3)
    {
        this.a = ((java.util.Map) com.a.b.b.cn.a(p2));
        this.b = ((com.a.b.d.pn) com.a.b.b.cn.a(p3));
        return;
    }

    protected java.util.Map a()
    {
        return this.a;
    }

    public java.util.Set entrySet()
    {
        java.util.Set v0_0 = this.c;
        if (v0_0 == null) {
            v0_0 = com.a.b.d.po.a(this.a.entrySet(), this.b);
            this.c = v0_0;
        }
        return v0_0;
    }

    protected synthetic Object k_()
    {
        return this.a();
    }

    public Object put(Object p2, Object p3)
    {
        this.b.a(p2, p3);
        return this.a.put(p2, p3);
    }

    public void putAll(java.util.Map p7)
    {
        java.util.Map v1 = this.a;
        com.a.b.d.pn v2 = this.b;
        java.util.LinkedHashMap v3_1 = new java.util.LinkedHashMap(p7);
        java.util.Iterator v4 = v3_1.entrySet().iterator();
        while (v4.hasNext()) {
            Object v0_3 = ((java.util.Map$Entry) v4.next());
            v2.a(v0_3.getKey(), v0_3.getValue());
        }
        v1.putAll(v3_1);
        return;
    }
}
