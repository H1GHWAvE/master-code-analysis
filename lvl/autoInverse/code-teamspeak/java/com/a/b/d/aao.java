package com.a.b.d;
final class aao extends java.util.AbstractSet {
    final com.a.b.d.jt a;

    aao(java.util.Set p8)
    {
        Integer v4_0 = com.a.b.d.jt.l();
        java.util.Iterator v5 = ((java.util.Set) com.a.b.b.cn.a(p8)).iterator();
        int v0_2 = 0;
        while (v5.hasNext()) {
            String v2_3 = (v0_2 + 1);
            v4_0.a(v5.next(), Integer.valueOf(v0_2));
            v0_2 = v2_3;
        }
        int v0_6;
        this.a = v4_0.a();
        if (this.a.size() > 30) {
            v0_6 = 0;
        } else {
            v0_6 = 1;
        }
        Object[] v3_1 = new Object[1];
        v3_1[0] = Integer.valueOf(this.a.size());
        com.a.b.b.cn.a(v0_6, "Too many elements to create power set: %s > 30", v3_1);
        return;
    }

    public final boolean contains(Object p2)
    {
        int v0_1;
        if (!(p2 instanceof java.util.Set)) {
            v0_1 = 0;
        } else {
            v0_1 = this.a.g().containsAll(((java.util.Set) p2));
        }
        return v0_1;
    }

    public final boolean equals(Object p3)
    {
        boolean v0_1;
        if (!(p3 instanceof com.a.b.d.aao)) {
            v0_1 = super.equals(p3);
        } else {
            v0_1 = this.a.equals(((com.a.b.d.aao) p3).a);
        }
        return v0_1;
    }

    public final int hashCode()
    {
        return (this.a.g().hashCode() << (this.a.size() - 1));
    }

    public final boolean isEmpty()
    {
        return 0;
    }

    public final java.util.Iterator iterator()
    {
        return new com.a.b.d.aap(this, this.size());
    }

    public final int size()
    {
        return (1 << this.a.size());
    }

    public final String toString()
    {
        String v0_2 = String.valueOf(String.valueOf(this.a));
        return new StringBuilder((v0_2.length() + 10)).append("powerSet(").append(v0_2).append(")").toString();
    }
}
