package com.a.b.d;
final class bh extends java.util.AbstractSet {
    final synthetic com.a.b.d.bf a;

    bh(com.a.b.d.bf p1)
    {
        this.a = p1;
        return;
    }

    public final void clear()
    {
        this.a.d();
        return;
    }

    public final boolean contains(Object p5)
    {
        int v0_1;
        if (!(p5 instanceof com.a.b.d.adw)) {
            v0_1 = 0;
        } else {
            int v0_5 = ((java.util.Map) com.a.b.d.sz.a(this.a.m(), ((com.a.b.d.adw) p5).a()));
            if ((v0_5 == 0) || (!com.a.b.d.cm.a(v0_5.entrySet(), com.a.b.d.sz.a(((com.a.b.d.adw) p5).b(), ((com.a.b.d.adw) p5).c())))) {
                v0_1 = 0;
            } else {
                v0_1 = 1;
            }
        }
        return v0_1;
    }

    public final java.util.Iterator iterator()
    {
        return this.a.g();
    }

    public final boolean remove(Object p5)
    {
        int v0_1;
        if (!(p5 instanceof com.a.b.d.adw)) {
            v0_1 = 0;
        } else {
            int v0_5 = ((java.util.Map) com.a.b.d.sz.a(this.a.m(), ((com.a.b.d.adw) p5).a()));
            if ((v0_5 == 0) || (!com.a.b.d.cm.b(v0_5.entrySet(), com.a.b.d.sz.a(((com.a.b.d.adw) p5).b(), ((com.a.b.d.adw) p5).c())))) {
                v0_1 = 0;
            } else {
                v0_1 = 1;
            }
        }
        return v0_1;
    }

    public final int size()
    {
        return this.a.k();
    }
}
