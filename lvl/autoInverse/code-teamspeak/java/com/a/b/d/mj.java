package com.a.b.d;
public final class mj {
    private final java.util.List a;
    private java.util.Comparator b;
    private java.util.Comparator c;

    public mj()
    {
        this.a = new java.util.ArrayList();
        return;
    }

    private com.a.b.d.mi a()
    {
        com.a.b.d.aax v0_5;
        switch (this.a.size()) {
            case 0:
                v0_5 = com.a.b.d.mi.p();
                break;
            case 1:
                v0_5 = new com.a.b.d.aax(((com.a.b.d.adw) com.a.b.d.mq.b(this.a)));
                break;
            default:
                v0_5 = com.a.b.d.zr.a(this.a, this.b, this.c);
        }
        return v0_5;
    }

    private com.a.b.d.mj a(com.a.b.d.adv p6)
    {
        java.util.Iterator v1 = p6.e().iterator();
        while (v1.hasNext()) {
            com.a.b.d.adw v0_3 = ((com.a.b.d.adw) v1.next());
            if (!(v0_3 instanceof com.a.b.d.aea)) {
                this.a.add(com.a.b.d.mi.b(v0_3.a(), v0_3.b(), v0_3.c()));
            } else {
                com.a.b.b.cn.a(v0_3.a());
                com.a.b.b.cn.a(v0_3.b());
                com.a.b.b.cn.a(v0_3.c());
                this.a.add(v0_3);
            }
        }
        return this;
    }

    private com.a.b.d.mj a(com.a.b.d.adw p5)
    {
        if (!(p5 instanceof com.a.b.d.aea)) {
            this.a.add(com.a.b.d.mi.b(p5.a(), p5.b(), p5.c()));
        } else {
            com.a.b.b.cn.a(p5.a());
            com.a.b.b.cn.a(p5.b());
            com.a.b.b.cn.a(p5.c());
            this.a.add(p5);
        }
        return this;
    }

    private com.a.b.d.mj a(Object p3, Object p4, Object p5)
    {
        this.a.add(com.a.b.d.mi.b(p3, p4, p5));
        return this;
    }

    private com.a.b.d.mj a(java.util.Comparator p2)
    {
        this.b = ((java.util.Comparator) com.a.b.b.cn.a(p2));
        return this;
    }

    private com.a.b.d.mj b(java.util.Comparator p2)
    {
        this.c = ((java.util.Comparator) com.a.b.b.cn.a(p2));
        return this;
    }
}
