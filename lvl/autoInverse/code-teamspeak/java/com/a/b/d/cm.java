package com.a.b.d;
public final class cm {
    static final com.a.b.b.bv a;

    static cm()
    {
        com.a.b.d.cm.a = com.a.b.b.bv.a(", ").b("null");
        return;
    }

    private cm()
    {
        return;
    }

    static String a(java.util.Collection p3)
    {
        String v0_2 = com.a.b.d.cm.a(p3.size()).append(91);
        com.a.b.d.cm.a.a(v0_2, com.a.b.d.mq.a(p3, new com.a.b.d.cn(p3)));
        return v0_2.append(93).toString();
    }

    static StringBuilder a(int p6)
    {
        com.a.b.d.cl.a(p6, "size");
        return new StringBuilder(((int) Math.min((((long) p6) * 8), 1073741824)));
    }

    static java.util.Collection a(Iterable p0)
    {
        return ((java.util.Collection) p0);
    }

    private static java.util.Collection a(Iterable p1, java.util.Comparator p2)
    {
        return new com.a.b.d.cp(p1, p2);
    }

    public static java.util.Collection a(java.util.Collection p1, com.a.b.b.bj p2)
    {
        return new com.a.b.d.ct(p1, p2);
    }

    public static java.util.Collection a(java.util.Collection p3, com.a.b.b.co p4)
    {
        com.a.b.d.co v0_3;
        if (!(p3 instanceof com.a.b.d.co)) {
            v0_3 = new com.a.b.d.co(((java.util.Collection) com.a.b.b.cn.a(p3)), ((com.a.b.b.co) com.a.b.b.cn.a(p4)));
        } else {
            v0_3 = new com.a.b.d.co(((com.a.b.d.co) p3).a, com.a.b.b.cp.a(((com.a.b.d.co) p3).b, p4));
        }
        return v0_3;
    }

    static synthetic boolean a(long p2)
    {
        if ((p2 < 0) || (p2 > 2147483647)) {
            int v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    static boolean a(java.util.Collection p2, Object p3)
    {
        com.a.b.b.cn.a(p2);
        try {
            boolean v0 = p2.contains(p3);
        } catch (NullPointerException v1) {
        } catch (NullPointerException v1) {
        }
        return v0;
    }

    static boolean a(java.util.Collection p1, java.util.Collection p2)
    {
        return com.a.b.d.mq.e(p2, com.a.b.b.cp.a(p1));
    }

    static synthetic boolean a(java.util.List p2, java.util.List p3)
    {
        boolean v0_2;
        if (p2.size() == p3.size()) {
            v0_2 = com.a.b.d.ip.a(p2).equals(com.a.b.d.ip.a(p3));
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    private static java.util.Collection b(Iterable p2)
    {
        return new com.a.b.d.cp(p2, com.a.b.d.yd.d());
    }

    private static java.util.Collection b(java.util.Collection p2)
    {
        return new com.a.b.d.cr(com.a.b.d.jl.a(p2));
    }

    private static boolean b(long p2)
    {
        if ((p2 < 0) || (p2 > 2147483647)) {
            int v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    static boolean b(java.util.Collection p2, Object p3)
    {
        com.a.b.b.cn.a(p2);
        try {
            boolean v0 = p2.remove(p3);
        } catch (NullPointerException v1) {
        } catch (NullPointerException v1) {
        }
        return v0;
    }

    private static boolean b(java.util.List p2, java.util.List p3)
    {
        boolean v0_2;
        if (p2.size() == p3.size()) {
            v0_2 = com.a.b.d.ip.a(p2).equals(com.a.b.d.ip.a(p3));
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }
}
