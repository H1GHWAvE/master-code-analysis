package com.a.b.d;
public final class ql extends com.a.b.d.ht {
    static final int b = 255;
    private static final int n = 16;
    private static final int o = 4;
    private static final int p;
    boolean c;
    int d;
    int e;
    int f;
    com.a.b.d.sh g;
    com.a.b.d.sh h;
    long i;
    long j;
    com.a.b.d.qq k;
    com.a.b.b.au l;
    com.a.b.b.ej m;

    public ql()
    {
        this.d = -1;
        this.e = -1;
        this.f = -1;
        this.i = -1;
        this.j = -1;
        return;
    }

    private com.a.b.d.ht a(com.a.b.d.qw p3)
    {
        com.a.b.d.qw v0_1;
        if (this.a != null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        com.a.b.b.cn.b(v0_1);
        this.a = ((com.a.b.d.qw) com.a.b.b.cn.a(p3));
        this.c = 1;
        return this;
    }

    private void e(long p12, java.util.concurrent.TimeUnit p14)
    {
        int v0_1;
        if (this.i != -1) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        int v0_3;
        Object[] v4_1 = new Object[1];
        v4_1[0] = Long.valueOf(this.i);
        com.a.b.b.cn.b(v0_1, "expireAfterWrite was already set to %s ns", v4_1);
        if (this.j != -1) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        int v0_5;
        Object[] v4_3 = new Object[1];
        v4_3[0] = Long.valueOf(this.j);
        com.a.b.b.cn.b(v0_3, "expireAfterAccess was already set to %s ns", v4_3);
        if (p12 < 0) {
            v0_5 = 0;
        } else {
            v0_5 = 1;
        }
        Object[] v4_6 = new Object[2];
        v4_6[0] = Long.valueOf(p12);
        v4_6[1] = p14;
        com.a.b.b.cn.a(v0_5, "duration cannot be negative: %s %s", v4_6);
        return;
    }

    private com.a.b.b.au j()
    {
        return ((com.a.b.b.au) com.a.b.b.ca.a(this.l, this.i().a()));
    }

    private com.a.b.d.ql k()
    {
        return this.a(com.a.b.d.sh.c);
    }

    private com.a.b.d.ql l()
    {
        return this.b(com.a.b.d.sh.c);
    }

    private com.a.b.d.ql m()
    {
        return this.b(com.a.b.d.sh.b);
    }

    private com.a.b.d.sh n()
    {
        return ((com.a.b.d.sh) com.a.b.b.ca.a(this.h, com.a.b.d.sh.a));
    }

    private long o()
    {
        long v0_2;
        if (this.i != -1) {
            v0_2 = this.i;
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    private long p()
    {
        long v0_2;
        if (this.j != -1) {
            v0_2 = this.j;
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    private com.a.b.b.ej q()
    {
        return ((com.a.b.b.ej) com.a.b.b.ca.a(this.m, com.a.b.b.ej.b()));
    }

    public final bridge synthetic com.a.b.d.ht a()
    {
        return this.a(com.a.b.d.sh.c);
    }

    public final synthetic com.a.b.d.ht a(int p2)
    {
        return this.d(p2);
    }

    final synthetic com.a.b.d.ht a(long p2, java.util.concurrent.TimeUnit p4)
    {
        return this.c(p2, p4);
    }

    final synthetic com.a.b.d.ht a(com.a.b.b.au p2)
    {
        return this.b(p2);
    }

    public final com.a.b.d.ql a(com.a.b.d.sh p7)
    {
        com.a.b.d.sh v0_1;
        int v2 = 0;
        if (this.g != null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        Object[] v4 = new Object[1];
        v4[0] = this.g;
        com.a.b.b.cn.b(v0_1, "Key strength was already set to %s", v4);
        this.g = ((com.a.b.d.sh) com.a.b.b.cn.a(p7));
        if (this.g != com.a.b.d.sh.b) {
            v2 = 1;
        }
        com.a.b.b.cn.a(v2, "Soft keys are not supported");
        if (p7 != com.a.b.d.sh.a) {
            this.c = 1;
        }
        return this;
    }

    final java.util.concurrent.ConcurrentMap a(com.a.b.b.bj p2)
    {
        java.util.concurrent.ConcurrentMap v0_2;
        if (this.k != null) {
            v0_2 = new com.a.b.d.qo(this, p2);
        } else {
            v0_2 = new com.a.b.d.qn(this, p2);
        }
        return ((java.util.concurrent.ConcurrentMap) v0_2);
    }

    public final bridge synthetic com.a.b.d.ht b()
    {
        return this.b(com.a.b.d.sh.c);
    }

    final synthetic com.a.b.d.ht b(int p2)
    {
        return this.e(p2);
    }

    final synthetic com.a.b.d.ht b(long p2, java.util.concurrent.TimeUnit p4)
    {
        return this.d(p2, p4);
    }

    final com.a.b.d.ql b(com.a.b.b.au p7)
    {
        com.a.b.b.au v0_1;
        if (this.l != null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        Object[] v4 = new Object[1];
        v4[0] = this.l;
        com.a.b.b.cn.b(v0_1, "key equivalence was already set to %s", v4);
        this.l = ((com.a.b.b.au) com.a.b.b.cn.a(p7));
        this.c = 1;
        return this;
    }

    public final com.a.b.d.ql b(com.a.b.d.sh p7)
    {
        com.a.b.d.sh v0_1;
        if (this.h != null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        Object[] v4 = new Object[1];
        v4[0] = this.h;
        com.a.b.b.cn.b(v0_1, "Value strength was already set to %s", v4);
        this.h = ((com.a.b.d.sh) com.a.b.b.cn.a(p7));
        if (p7 != com.a.b.d.sh.a) {
            this.c = 1;
        }
        return this;
    }

    public final synthetic com.a.b.d.ht c()
    {
        return this.b(com.a.b.d.sh.b);
    }

    public final synthetic com.a.b.d.ht c(int p2)
    {
        return this.f(p2);
    }

    final com.a.b.d.ql c(long p4, java.util.concurrent.TimeUnit p6)
    {
        this.e(p4, p6);
        this.i = p6.toNanos(p4);
        if ((p4 == 0) && (this.k == null)) {
            this.k = com.a.b.d.qq.d;
        }
        this.c = 1;
        return this;
    }

    public final com.a.b.d.ql d(int p7)
    {
        int v0_1;
        int v1 = 1;
        if (this.d != -1) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        Object[] v4 = new Object[1];
        v4[0] = Integer.valueOf(this.d);
        com.a.b.b.cn.b(v0_1, "initial capacity was already set to %s", v4);
        if (p7 < 0) {
            v1 = 0;
        }
        com.a.b.b.cn.a(v1);
        this.d = p7;
        return this;
    }

    final com.a.b.d.ql d(long p4, java.util.concurrent.TimeUnit p6)
    {
        this.e(p4, p6);
        this.j = p6.toNanos(p4);
        if ((p4 == 0) && (this.k == null)) {
            this.k = com.a.b.d.qq.d;
        }
        this.c = 1;
        return this;
    }

    final com.a.b.d.ql e(int p7)
    {
        com.a.b.d.qq v0_1;
        int v2 = 0;
        if (this.f != -1) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        Object[] v4 = new Object[1];
        v4[0] = Integer.valueOf(this.f);
        com.a.b.b.cn.b(v0_1, "maximum size was already set to %s", v4);
        if (p7 >= 0) {
            v2 = 1;
        }
        com.a.b.b.cn.a(v2, "maximum size must not be negative");
        this.f = p7;
        this.c = 1;
        if (this.f == 0) {
            this.k = com.a.b.d.qq.e;
        }
        return this;
    }

    public final java.util.concurrent.ConcurrentMap e()
    {
        java.util.concurrent.ConcurrentMap v0_5;
        if (this.c) {
            java.util.concurrent.ConcurrentMap v0_3;
            if (this.k != null) {
                v0_3 = new com.a.b.d.qp(this);
            } else {
                v0_3 = new com.a.b.d.qy(this);
            }
            v0_5 = ((java.util.concurrent.ConcurrentMap) v0_3);
        } else {
            v0_5 = new java.util.concurrent.ConcurrentHashMap(this.g(), 1061158912, this.h());
        }
        return v0_5;
    }

    public final com.a.b.d.ql f(int p7)
    {
        int v0_1;
        int v1 = 1;
        if (this.e != -1) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        Object[] v4 = new Object[1];
        v4[0] = Integer.valueOf(this.e);
        com.a.b.b.cn.b(v0_1, "concurrency level was already set to %s", v4);
        if (p7 <= 0) {
            v1 = 0;
        }
        com.a.b.b.cn.a(v1);
        this.e = p7;
        return this;
    }

    final com.a.b.d.qy f()
    {
        return new com.a.b.d.qy(this);
    }

    final int g()
    {
        int v0_1;
        if (this.d != -1) {
            v0_1 = this.d;
        } else {
            v0_1 = 16;
        }
        return v0_1;
    }

    final int h()
    {
        int v0_1;
        if (this.e != -1) {
            v0_1 = this.e;
        } else {
            v0_1 = 4;
        }
        return v0_1;
    }

    final com.a.b.d.sh i()
    {
        return ((com.a.b.d.sh) com.a.b.b.ca.a(this.g, com.a.b.d.sh.a));
    }

    public final String toString()
    {
        String v0_0 = com.a.b.b.ca.a(this);
        if (this.d != -1) {
            v0_0.a("initialCapacity", this.d);
        }
        if (this.e != -1) {
            v0_0.a("concurrencyLevel", this.e);
        }
        if (this.f != -1) {
            v0_0.a("maximumSize", this.f);
        }
        if (this.i != -1) {
            v0_0.a("expireAfterWrite", new StringBuilder(22).append(this.i).append("ns").toString());
        }
        if (this.j != -1) {
            v0_0.a("expireAfterAccess", new StringBuilder(22).append(this.j).append("ns").toString());
        }
        if (this.g != null) {
            v0_0.a("keyStrength", com.a.b.b.e.a(this.g.toString()));
        }
        if (this.h != null) {
            v0_0.a("valueStrength", com.a.b.b.e.a(this.h.toString()));
        }
        if (this.l != null) {
            v0_0.a("keyEquivalence");
        }
        if (this.a != null) {
            v0_0.a("removalListener");
        }
        return v0_0.toString();
    }
}
