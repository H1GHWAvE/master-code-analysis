package com.a.b.j;
public final class c {
    static final int a = 170;
    static final double[] b = None;
    private static final double c = 49632;
    private static final double d = 72430328479680;
    private static final double e = 50144;
    private static final double f = 17376;
    private static final double g;

    static c()
    {
        com.a.b.j.c.g = Math.log(2.0);
        double[] v0_3 = new double[11];
        v0_3 = {0, 0, 0, 0, 0, 0, -16, 63, 0, 0, -128};
        com.a.b.j.c.b = v0_3;
        return;
    }

    private c()
    {
        return;
    }

    private static double a(int p6)
    {
        double v0_6;
        com.a.b.j.k.b("n", p6);
        if (p6 <= 170) {
            double v2 = 1.0;
            double v0_3 = ((p6 & -16) + 1);
            while (v0_3 <= p6) {
                v2 *= ((double) v0_3);
                v0_3++;
            }
            v0_6 = (com.a.b.j.c.b[(p6 >> 4)] * v2);
        } else {
            v0_6 = inf;
        }
        return v0_6;
    }

    private static double a(Iterable p6)
    {
        com.a.b.j.e v1_1 = new com.a.b.j.e(0);
        java.util.Iterator v2 = p6.iterator();
        while (v2.hasNext()) {
            v1_1.a(((Number) v2.next()).doubleValue());
        }
        return v1_1.a();
    }

    private static double a(java.util.Iterator p4)
    {
        com.a.b.j.e v1_1 = new com.a.b.j.e(0);
        while (p4.hasNext()) {
            v1_1.a(((Number) p4.next()).doubleValue());
        }
        return v1_1.a();
    }

    private static varargs double a(double[] p6)
    {
        double v0_0 = 0;
        com.a.b.j.e v1_1 = new com.a.b.j.e(0);
        int v2 = p6.length;
        while (v0_0 < v2) {
            v1_1.a(p6[v0_0]);
            v0_0++;
        }
        return v1_1.a();
    }

    private static varargs double a(int[] p6)
    {
        double v0_0 = 0;
        com.a.b.j.e v1_1 = new com.a.b.j.e(0);
        int v2 = p6.length;
        while (v0_0 < v2) {
            v1_1.a(((double) p6[v0_0]));
            v0_0++;
        }
        return v1_1.a();
    }

    private static varargs double a(long[] p6)
    {
        double v0_0 = 0;
        com.a.b.j.e v1_1 = new com.a.b.j.e(0);
        int v2 = p6.length;
        while (v0_0 < v2) {
            v1_1.a(((double) p6[v0_0]));
            v0_0++;
        }
        return v1_1.a();
    }

    public static java.math.BigInteger a(double p8, java.math.RoundingMode p10)
    {
        long v2_3;
        java.math.BigInteger v0_0 = 1;
        double v4 = com.a.b.j.c.b(p8, p10);
        if ((-9.223372036854776e+18 - v4) >= 1.0) {
            v2_3 = 0;
        } else {
            v2_3 = 1;
        }
        if (v4 >= 9.223372036854776e+18) {
            v0_0 = 0;
        }
        java.math.BigInteger v0_4;
        if ((v0_0 & v2_3) == 0) {
            v0_4 = java.math.BigInteger.valueOf(com.a.b.j.f.a(v4)).shiftLeft((Math.getExponent(v4) - 52));
            if (v4 < 0) {
                v0_4 = v0_4.negate();
            }
        } else {
            v0_4 = java.math.BigInteger.valueOf(((long) v4));
        }
        return v0_4;
    }

    private static boolean a(double p2)
    {
        if ((p2 <= 0) || ((!com.a.b.j.f.b(p2)) || (!com.a.b.j.i.a(com.a.b.j.f.a(p2))))) {
            int v0_5 = 0;
        } else {
            v0_5 = 1;
        }
        return v0_5;
    }

    private static boolean a(double p4, double p6, double p8)
    {
        if (p8 >= 0) {
            if ((Math.copySign((p4 - p6), 1.0) > p8) && ((p4 != p6) && ((!Double.isNaN(p4)) || (!Double.isNaN(p6))))) {
                int v0_7 = 0;
            } else {
                v0_7 = 1;
            }
            return v0_7;
        } else {
            int v0_9 = String.valueOf(String.valueOf("tolerance"));
            throw new IllegalArgumentException(new StringBuilder((v0_9.length() + 40)).append(v0_9).append(" (").append(p8).append(") must be >= 0").toString());
        }
    }

    private static double b(double p4)
    {
        return (Math.log(p4) / com.a.b.j.c.g);
    }

    private static double b(double p8, java.math.RoundingMode p10)
    {
        if (com.a.b.j.f.b(p8)) {
            switch (com.a.b.j.d.a[p10.ordinal()]) {
                case 1:
                    com.a.b.j.k.a(com.a.b.j.c.c(p8));
                    break;
                case 2:
                    if ((p8 >= 0) || (com.a.b.j.c.c(p8))) {
                    } else {
                        p8 -= 1.0;
                    }
                    break;
                case 3:
                    if ((p8 <= 0) || (com.a.b.j.c.c(p8))) {
                    } else {
                        p8 += 1.0;
                    }
                case 4:
                    break;
                case 5:
                    if (com.a.b.j.c.c(p8)) {
                    } else {
                        p8 += Math.copySign(1.0, p8);
                    }
                    break;
                case 6:
                    p8 = Math.rint(p8);
                    break;
                case 7:
                    double v0_4 = Math.rint(p8);
                    if (Math.abs((p8 - v0_4)) != 0.5) {
                        p8 = v0_4;
                    } else {
                        p8 += Math.copySign(0.5, p8);
                    }
                    break;
                case 8:
                    double v0_3 = Math.rint(p8);
                    if (Math.abs((p8 - v0_3)) == 0.5) {
                    } else {
                        p8 = v0_3;
                    }
                    break;
                default:
                    throw new AssertionError();
            }
            return p8;
        } else {
            throw new ArithmeticException("input is infinite or NaN");
        }
    }

    private static int b(double p6, double p8, double p10)
    {
        int v0_0 = 0;
        if (p10 >= 0) {
            if ((Math.copySign((p6 - p8), 1.0) > p10) && ((p6 != p8) && ((!Double.isNaN(p6)) || (!Double.isNaN(p8))))) {
                IllegalArgumentException v2_7 = 0;
            } else {
                v2_7 = 1;
            }
            if (v2_7 == null) {
                if (p6 >= p8) {
                    if (p6 <= p8) {
                        v0_0 = com.a.b.l.a.a(Double.isNaN(p6), Double.isNaN(p8));
                    } else {
                        v0_0 = 1;
                    }
                } else {
                    v0_0 = -1;
                }
            }
            return v0_0;
        } else {
            boolean v1_3 = String.valueOf(String.valueOf("tolerance"));
            throw new IllegalArgumentException(new StringBuilder((v1_3.length() + 40)).append(v1_3).append(" (").append(p10).append(") must be >= 0").toString());
        }
    }

    private static int c(double p8, java.math.RoundingMode p10)
    {
        int v2_2;
        int v0_0 = 1;
        double v4 = com.a.b.j.c.b(p8, p10);
        if (v4 <= -2147483649.0) {
            v2_2 = 0;
        } else {
            v2_2 = 1;
        }
        if (v4 >= 2147483648.0) {
            v0_0 = 0;
        }
        com.a.b.j.k.b((v0_0 & v2_2));
        return ((int) v4);
    }

    private static boolean c(double p2)
    {
        if ((!com.a.b.j.f.b(p2)) || ((p2 != 0) && ((52 - Long.numberOfTrailingZeros(com.a.b.j.f.a(p2))) > Math.getExponent(p2)))) {
            int v0_6 = 0;
        } else {
            v0_6 = 1;
        }
        return v0_6;
    }

    private static long d(double p8, java.math.RoundingMode p10)
    {
        int v2_3;
        int v0_0 = 1;
        double v4 = com.a.b.j.c.b(p8, p10);
        if ((-9.223372036854776e+18 - v4) >= 1.0) {
            v2_3 = 0;
        } else {
            v2_3 = 1;
        }
        if (v4 >= 9.223372036854776e+18) {
            v0_0 = 0;
        }
        com.a.b.j.k.b((v0_0 & v2_3));
        return ((long) v4);
    }

    private static int e(double p8, java.math.RoundingMode p10)
    {
        int v0_2;
        int v1 = 1;
        if ((p8 <= 0) || (!com.a.b.j.f.b(p8))) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        int v0_11;
        com.a.b.b.cn.a(v0_2, "x must be positive and finite");
        int v3_1 = Math.getExponent(p8);
        if (com.a.b.j.f.c(p8)) {
            switch (com.a.b.j.d.a[p10.ordinal()]) {
                case 1:
                    com.a.b.j.k.a(com.a.b.j.c.a(p8));
                case 2:
                    v1 = 0;
                    break;
                case 3:
                    if (!com.a.b.j.c.a(p8)) {
                    } else {
                        v1 = 0;
                    }
                    break;
                case 4:
                    int v0_8;
                    if (v3_1 >= 0) {
                        v0_8 = 0;
                    } else {
                        v0_8 = 1;
                    }
                    if (com.a.b.j.c.a(p8)) {
                        v1 = 0;
                    }
                    v1 &= v0_8;
                    break;
                case 5:
                    int v0_7;
                    if (v3_1 < 0) {
                        v0_7 = 0;
                    } else {
                        v0_7 = 1;
                    }
                    if (com.a.b.j.c.a(p8)) {
                        v1 = 0;
                    }
                    v1 &= v0_7;
                    break;
                case 6:
                case 7:
                case 8:
                    if ((com.a.b.j.f.d(p8) * com.a.b.j.f.d(p8)) > 2.0) {
                    } else {
                        v1 = 0;
                    }
                    break;
                default:
                    throw new AssertionError();
            }
            if (v1 == 0) {
                v0_11 = v3_1;
            } else {
                v0_11 = (v3_1 + 1);
            }
        } else {
            v0_11 = (com.a.b.j.c.e((4503599627370496.0 * p8), p10) - 52);
        }
        return v0_11;
    }
}
