package com.a.b.e;
public final class l {
    public char a;
    public char b;
    public String c;
    private final java.util.Map d;

    private l()
    {
        this.d = new java.util.HashMap();
        this.a = 0;
        this.b = 65535;
        this.c = 0;
        return;
    }

    synthetic l(byte p1)
    {
        return;
    }

    private com.a.b.e.l a(String p1)
    {
        this.c = p1;
        return this;
    }

    private static synthetic String a(com.a.b.e.l p1)
    {
        return p1.c;
    }

    private com.a.b.e.l b()
    {
        this.a = 0;
        this.b = 65533;
        return this;
    }

    public final com.a.b.e.g a()
    {
        return new com.a.b.e.m(this, this.d, this.a, this.b);
    }

    public final com.a.b.e.l a(char p3, String p4)
    {
        com.a.b.b.cn.a(p4);
        this.d.put(Character.valueOf(p3), p4);
        return this;
    }
}
