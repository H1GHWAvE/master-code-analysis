package com.a.b.e;
public abstract class a extends com.a.b.e.d {
    private final char[][] a;
    private final int b;
    private final char c;
    private final char d;

    private a(com.a.b.e.b p2, char p3, char p4)
    {
        com.a.b.b.cn.a(p2);
        this.a = p2.a;
        this.b = this.a.length;
        if (p4 < p3) {
            p4 = 0;
            p3 = 65535;
        }
        this.c = p3;
        this.d = p4;
        return;
    }

    protected a(java.util.Map p2, char p3, char p4)
    {
        this(com.a.b.e.b.a(p2), p3, p4);
        return;
    }

    public final String a(String p4)
    {
        com.a.b.b.cn.a(p4);
        int v0 = 0;
        while (v0 < p4.length()) {
            char v1_1 = p4.charAt(v0);
            if (((v1_1 >= this.b) || (this.a[v1_1] == null)) && ((v1_1 <= this.d) && (v1_1 >= this.c))) {
                v0++;
            } else {
                p4 = this.a(p4, v0);
                break;
            }
        }
        return p4;
    }

    protected abstract char[] a();

    protected final char[] a(char p2)
    {
        int v0_2;
        if (p2 >= this.b) {
            if ((p2 < this.c) || (p2 > this.d)) {
                v0_2 = this.a();
            } else {
                v0_2 = 0;
            }
        } else {
            v0_2 = this.a[p2];
            if (v0_2 == 0) {
            }
        }
        return v0_2;
    }
}
