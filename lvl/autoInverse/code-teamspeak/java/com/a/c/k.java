package com.a.c;
public final class k {
    static final boolean a = False;
    private static final String d = ")]}'
";
    final com.a.c.u b;
    final com.a.c.ad c;
    private final ThreadLocal e;
    private final java.util.Map f;
    private final java.util.List g;
    private final com.a.c.b.f h;
    private final boolean i;
    private final boolean j;
    private final boolean k;
    private final boolean l;

    public k()
    {
        this(com.a.c.b.s.a, com.a.c.d.a, java.util.Collections.emptyMap(), 0, 0, 0, 1, 0, 0, com.a.c.ah.a, java.util.Collections.emptyList());
        return;
    }

    k(com.a.c.b.s p5, com.a.c.j p6, java.util.Map p7, boolean p8, boolean p9, boolean p10, boolean p11, boolean p12, boolean p13, com.a.c.ah p14, java.util.List p15)
    {
        java.util.List v0_20;
        this.e = new ThreadLocal();
        this.f = java.util.Collections.synchronizedMap(new java.util.HashMap());
        this.b = new com.a.c.l(this);
        this.c = new com.a.c.m(this);
        this.h = new com.a.c.b.f(p7);
        this.i = p8;
        this.k = p10;
        this.j = p11;
        this.l = p12;
        java.util.ArrayList v1_1 = new java.util.ArrayList();
        v1_1.add(com.a.c.b.a.z.Q);
        v1_1.add(com.a.c.b.a.n.a);
        v1_1.add(p5);
        v1_1.addAll(p15);
        v1_1.add(com.a.c.b.a.z.x);
        v1_1.add(com.a.c.b.a.z.m);
        v1_1.add(com.a.c.b.a.z.g);
        v1_1.add(com.a.c.b.a.z.i);
        v1_1.add(com.a.c.b.a.z.k);
        if (p14 != com.a.c.ah.a) {
            v0_20 = new com.a.c.p(this);
        } else {
            v0_20 = com.a.c.b.a.z.n;
        }
        java.util.List v0_23;
        v1_1.add(com.a.c.b.a.z.a(Long.TYPE, Long, v0_20));
        if (!p13) {
            v0_23 = new com.a.c.n(this);
        } else {
            v0_23 = com.a.c.b.a.z.p;
        }
        java.util.List v0_26;
        v1_1.add(com.a.c.b.a.z.a(Double.TYPE, Double, v0_23));
        if (!p13) {
            v0_26 = new com.a.c.o(this);
        } else {
            v0_26 = com.a.c.b.a.z.o;
        }
        v1_1.add(com.a.c.b.a.z.a(Float.TYPE, Float, v0_26));
        v1_1.add(com.a.c.b.a.z.r);
        v1_1.add(com.a.c.b.a.z.t);
        v1_1.add(com.a.c.b.a.z.z);
        v1_1.add(com.a.c.b.a.z.B);
        v1_1.add(com.a.c.b.a.z.a(java.math.BigDecimal, com.a.c.b.a.z.v));
        v1_1.add(com.a.c.b.a.z.a(java.math.BigInteger, com.a.c.b.a.z.w));
        v1_1.add(com.a.c.b.a.z.D);
        v1_1.add(com.a.c.b.a.z.F);
        v1_1.add(com.a.c.b.a.z.J);
        v1_1.add(com.a.c.b.a.z.O);
        v1_1.add(com.a.c.b.a.z.H);
        v1_1.add(com.a.c.b.a.z.d);
        v1_1.add(com.a.c.b.a.e.a);
        v1_1.add(com.a.c.b.a.z.M);
        v1_1.add(com.a.c.b.a.w.a);
        v1_1.add(com.a.c.b.a.u.a);
        v1_1.add(com.a.c.b.a.z.K);
        v1_1.add(com.a.c.b.a.a.a);
        v1_1.add(com.a.c.b.a.z.R);
        v1_1.add(com.a.c.b.a.z.b);
        v1_1.add(new com.a.c.b.a.c(this.h));
        v1_1.add(new com.a.c.b.a.l(this.h, p9));
        v1_1.add(new com.a.c.b.a.g(this.h));
        v1_1.add(new com.a.c.b.a.q(this.h, p6, p5));
        this.g = java.util.Collections.unmodifiableList(v1_1);
        return;
    }

    private com.a.c.an a(com.a.c.ah p2)
    {
        com.a.c.p v0_2;
        if (p2 != com.a.c.ah.a) {
            v0_2 = new com.a.c.p(this);
        } else {
            v0_2 = com.a.c.b.a.z.n;
        }
        return v0_2;
    }

    private com.a.c.an a(boolean p2)
    {
        com.a.c.n v0_1;
        if (!p2) {
            v0_1 = new com.a.c.n(this);
        } else {
            v0_1 = com.a.c.b.a.z.p;
        }
        return v0_1;
    }

    private com.a.c.d.e a(java.io.Writer p4)
    {
        if (this.k) {
            p4.write(")]}\'\n");
        }
        com.a.c.d.e v0_3 = new com.a.c.d.e(p4);
        if (this.l) {
            if ("  ".length() != 0) {
                v0_3.a = "  ";
                v0_3.b = ": ";
            } else {
                v0_3.a = 0;
                v0_3.b = ":";
            }
        }
        v0_3.e = this.i;
        return v0_3;
    }

    private com.a.c.w a(Object p2)
    {
        com.a.c.w v0_1;
        if (p2 != null) {
            v0_1 = this.a(p2, p2.getClass());
        } else {
            v0_1 = com.a.c.y.a;
        }
        return v0_1;
    }

    private Object a(com.a.c.w p3, Class p4)
    {
        return com.a.c.b.ap.a(p4).cast(this.a(p3, p4));
    }

    private Object a(java.io.Reader p3, Class p4)
    {
        Object v0_1 = new com.a.c.d.a(p3);
        Object v1 = this.a(v0_1, p4);
        com.a.c.k.a(v1, v0_1);
        return com.a.c.b.ap.a(p4).cast(v1);
    }

    private Object a(java.io.Reader p3, reflect.Type p4)
    {
        com.a.c.d.a v0_1 = new com.a.c.d.a(p3);
        Object v1 = this.a(v0_1, p4);
        com.a.c.k.a(v1, v0_1);
        return v1;
    }

    private Object a(String p3, Class p4)
    {
        Object v0_2;
        if (p3 != null) {
            Class v1_1 = new com.a.c.d.a(new java.io.StringReader(p3));
            v0_2 = this.a(v1_1, p4);
            com.a.c.k.a(v0_2, v1_1);
        } else {
            v0_2 = 0;
        }
        return com.a.c.b.ap.a(p4).cast(v0_2);
    }

    private Object a(String p3, reflect.Type p4)
    {
        Object v0_2;
        if (p3 != null) {
            com.a.c.d.a v1_1 = new com.a.c.d.a(new java.io.StringReader(p3));
            v0_2 = this.a(v1_1, p4);
            com.a.c.k.a(v0_2, v1_1);
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    private String a(com.a.c.w p2)
    {
        String v0_1 = new java.io.StringWriter();
        this.a(p2, v0_1);
        return v0_1.toString();
    }

    static synthetic void a(double p4)
    {
        if ((!Double.isNaN(p4)) && (!Double.isInfinite(p4))) {
            return;
        } else {
            throw new IllegalArgumentException(new StringBuilder().append(p4).append(" is not a valid double value as per JSON specification. To override this behavior, use GsonBuilder.serializeSpecialFloatingPointValues() method.").toString());
        }
    }

    private void a(com.a.c.w p6, com.a.c.d.e p7)
    {
        boolean v1 = p7.c;
        p7.c = 1;
        boolean v2 = p7.d;
        p7.d = this.j;
        boolean v3 = p7.e;
        p7.e = this.i;
        try {
            com.a.c.b.aq.a(p6, p7);
            p7.c = v1;
            p7.d = v2;
            p7.e = v3;
            return;
        } catch (java.io.IOException v0_4) {
            p7.c = v1;
            p7.d = v2;
            p7.e = v3;
            throw v0_4;
        } catch (java.io.IOException v0_3) {
            throw new com.a.c.x(v0_3);
        }
    }

    private void a(com.a.c.w p7, Appendable p8)
    {
        try {
            RuntimeException v1_0 = this.a(com.a.c.b.aq.a(p8));
            boolean v2 = v1_0.c;
            v1_0.c = 1;
            boolean v3 = v1_0.d;
            v1_0.d = this.j;
            boolean v4 = v1_0.e;
            v1_0.e = this.i;
            try {
                com.a.c.b.aq.a(p7, v1_0);
            } catch (java.io.IOException v0_5) {
                v1_0.c = v2;
                v1_0.d = v3;
                v1_0.e = v4;
                throw v0_5;
            } catch (java.io.IOException v0_4) {
                throw new com.a.c.x(v0_4);
            }
            v1_0.c = v2;
            v1_0.d = v3;
            v1_0.e = v4;
            return;
        } catch (java.io.IOException v0_6) {
            throw new RuntimeException(v0_6);
        }
    }

    public static void a(Object p2, com.a.c.d.a p3)
    {
        try {
            if ((p2 == null) || (p3.f() == com.a.c.d.d.j)) {
                return;
            } else {
                throw new com.a.c.x("JSON document was not fully consumed.");
            }
        } catch (com.a.c.x v0_3) {
            throw new com.a.c.x(v0_3);
        } catch (com.a.c.x v0_4) {
            throw new com.a.c.ag(v0_4);
        }
    }

    private void a(Object p2, Appendable p3)
    {
        if (p2 == null) {
            this.a(com.a.c.y.a, p3);
        } else {
            this.a(p2, p2.getClass(), p3);
        }
        return;
    }

    private void a(Object p6, reflect.Type p7, com.a.c.d.e p8)
    {
        java.io.IOException v0_1 = this.a(com.a.c.c.a.a(p7));
        boolean v1 = p8.c;
        p8.c = 1;
        boolean v2_1 = p8.d;
        p8.d = this.j;
        boolean v3_1 = p8.e;
        p8.e = this.i;
        try {
            v0_1.a(p8, p6);
            p8.c = v1;
            p8.d = v2_1;
            p8.e = v3_1;
            return;
        } catch (java.io.IOException v0_3) {
            p8.c = v1;
            p8.d = v2_1;
            p8.e = v3_1;
            throw v0_3;
        } catch (java.io.IOException v0_2) {
            throw new com.a.c.x(v0_2);
        }
    }

    private com.a.c.an b(boolean p2)
    {
        com.a.c.o v0_1;
        if (!p2) {
            v0_1 = new com.a.c.o(this);
        } else {
            v0_1 = com.a.c.b.a.z.o;
        }
        return v0_1;
    }

    private String b(Object p3)
    {
        String v0_0 = p3.getClass();
        java.io.StringWriter v1_1 = new java.io.StringWriter();
        this.a(p3, v0_0, v1_1);
        return v1_1.toString();
    }

    private String b(Object p2, reflect.Type p3)
    {
        String v0_1 = new java.io.StringWriter();
        this.a(p2, p3, v0_1);
        return v0_1.toString();
    }

    private static void b(double p4)
    {
        if ((!Double.isNaN(p4)) && (!Double.isInfinite(p4))) {
            return;
        } else {
            throw new IllegalArgumentException(new StringBuilder().append(p4).append(" is not a valid double value as per JSON specification. To override this behavior, use GsonBuilder.serializeSpecialFloatingPointValues() method.").toString());
        }
    }

    public final com.a.c.an a(com.a.c.ap p4, com.a.c.c.a p5)
    {
        String v2_0 = this.g.iterator();
        int v1_1 = 0;
        while (v2_0.hasNext()) {
            int v0_5 = ((com.a.c.ap) v2_0.next());
            if (v1_1 != 0) {
                int v0_6 = v0_5.a(this, p5);
                if (v0_6 != 0) {
                    return v0_6;
                }
            } else {
                if (v0_5 == p4) {
                    v1_1 = 1;
                }
            }
        }
        throw new IllegalArgumentException(new StringBuilder("GSON cannot serialize ").append(p5).toString());
    }

    public final com.a.c.an a(com.a.c.c.a p6)
    {
        AssertionError v0_2 = ((com.a.c.an) this.f.get(p6));
        if (v0_2 == null) {
            int v2;
            AssertionError v0_5 = ((java.util.Map) this.e.get());
            ThreadLocal v1_0 = 0;
            if (v0_5 != null) {
                v2 = v0_5;
            } else {
                ThreadLocal v1_2 = new java.util.HashMap();
                this.e.set(v1_2);
                v2 = v1_2;
                v1_0 = 1;
            }
            v0_2 = ((com.a.c.q) v2.get(p6));
            if (v0_2 == null) {
                try {
                    java.util.Map v3_1 = new com.a.c.q();
                    v2.put(p6, v3_1);
                    com.a.c.an v4_0 = this.g.iterator();
                } catch (AssertionError v0_17) {
                    v2.remove(p6);
                    if (v1_0 != null) {
                        this.e.remove();
                    }
                    throw v0_17;
                }
                while (v4_0.hasNext()) {
                    v0_2 = ((com.a.c.ap) v4_0.next()).a(this, p6);
                    if (v0_2 != null) {
                        if (v3_1.a == null) {
                            v3_1.a = v0_2;
                            this.f.put(p6, v0_2);
                            v2.remove(p6);
                            if (v1_0 != null) {
                                this.e.remove();
                            }
                        } else {
                            throw new AssertionError();
                        }
                    }
                }
                throw new IllegalArgumentException(new StringBuilder("GSON cannot handle ").append(p6).toString());
            }
        }
        return v0_2;
    }

    public final com.a.c.an a(Class p2)
    {
        return this.a(com.a.c.c.a.a(p2));
    }

    public final com.a.c.w a(Object p2, reflect.Type p3)
    {
        com.a.c.w v0_1 = new com.a.c.b.a.j();
        this.a(p2, p3, v0_1);
        return v0_1.a();
    }

    public final Object a(com.a.c.d.a p4, reflect.Type p5)
    {
        boolean v2 = p4.b;
        p4.b = 1;
        try {
            p4.f();
            int v0_2 = this.a(com.a.c.c.a.a(p5)).a(p4);
            p4.b = v2;
        } catch (int v0_5) {
            if (0 == 0) {
                throw new com.a.c.ag(v0_5);
            } else {
                p4.b = v2;
                v0_2 = 0;
            }
        } catch (int v0_6) {
            p4.b = v2;
            throw v0_6;
        } catch (int v0_4) {
            throw new com.a.c.ag(v0_4);
        }
        return v0_2;
    }

    public final Object a(com.a.c.w p2, reflect.Type p3)
    {
        Object v0_2;
        if (p2 != null) {
            v0_2 = this.a(new com.a.c.b.a.h(p2), p3);
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public final void a(Object p3, reflect.Type p4, Appendable p5)
    {
        try {
            this.a(p3, p4, this.a(com.a.c.b.aq.a(p5)));
            return;
        } catch (java.io.IOException v0_2) {
            throw new com.a.c.x(v0_2);
        }
    }

    public final String toString()
    {
        return new StringBuilder("{serializeNulls:").append(this.i).append("factories:").append(this.g).append(",instanceCreators:").append(this.h).append("}").toString();
    }
}
