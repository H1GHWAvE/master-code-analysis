package com.a.c;
public final class af implements java.util.Iterator {
    private final com.a.c.d.a a;
    private final Object b;

    private af(java.io.Reader p3)
    {
        this.a = new com.a.c.d.a(p3);
        this.a.b = 1;
        this.b = new Object();
        return;
    }

    private af(String p2)
    {
        this(new java.io.StringReader(p2));
        return;
    }

    private com.a.c.w a()
    {
        if (this.hasNext()) {
            try {
                return com.a.c.b.aq.a(this.a);
            } catch (java.util.NoSuchElementException v0_6) {
                throw new com.a.c.aa("Failed parsing JSON source to Json", v0_6);
            } catch (java.util.NoSuchElementException v0_3) {
                if ((v0_3.getCause() instanceof java.io.EOFException)) {
                    v0_3 = new java.util.NoSuchElementException();
                }
                throw v0_3;
            } catch (java.util.NoSuchElementException v0_5) {
                throw new com.a.c.aa("Failed parsing JSON source to Json", v0_5);
            }
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public final boolean hasNext()
    {
        try {
            int v0_4;
            if (this.a.f() == com.a.c.d.d.j) {
                v0_4 = 0;
            } else {
                v0_4 = 1;
            }
        } catch (int v0_5) {
            throw v0_5;
        } catch (int v0_2) {
            throw new com.a.c.x(v0_2);
        }
        return v0_4;
    }

    public final synthetic Object next()
    {
        return this.a();
    }

    public final void remove()
    {
        throw new UnsupportedOperationException();
    }
}
