package com.a.c;
final class n extends com.a.c.an {
    final synthetic com.a.c.k a;

    n(com.a.c.k p1)
    {
        this.a = p1;
        return;
    }

    private static void a(com.a.c.d.e p2, Number p3)
    {
        if (p3 != null) {
            com.a.c.k.a(p3.doubleValue());
            p2.a(p3);
        } else {
            p2.f();
        }
        return;
    }

    private static Double b(com.a.c.d.a p2)
    {
        Double v0_2;
        if (p2.f() != com.a.c.d.d.i) {
            v0_2 = Double.valueOf(p2.l());
        } else {
            p2.k();
            v0_2 = 0;
        }
        return v0_2;
    }

    public final synthetic Object a(com.a.c.d.a p3)
    {
        Double v0_2;
        if (p3.f() != com.a.c.d.d.i) {
            v0_2 = Double.valueOf(p3.l());
        } else {
            p3.k();
            v0_2 = 0;
        }
        return v0_2;
    }

    public final synthetic void a(com.a.c.d.e p3, Object p4)
    {
        if (((Number) p4) != null) {
            com.a.c.k.a(((Number) p4).doubleValue());
            p3.a(((Number) p4));
        } else {
            p3.f();
        }
        return;
    }
}
