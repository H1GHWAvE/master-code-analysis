package com.a.c;
public final class r {
    private com.a.c.b.s a;
    private com.a.c.ah b;
    private com.a.c.j c;
    private final java.util.Map d;
    private final java.util.List e;
    private final java.util.List f;
    private boolean g;
    private String h;
    private int i;
    private int j;
    private boolean k;
    private boolean l;
    private boolean m;
    private boolean n;
    private boolean o;

    public r()
    {
        this.a = com.a.c.b.s.a;
        this.b = com.a.c.ah.a;
        this.c = com.a.c.d.a;
        this.d = new java.util.HashMap();
        this.e = new java.util.ArrayList();
        this.f = new java.util.ArrayList();
        this.i = 2;
        this.j = 2;
        this.m = 1;
        return;
    }

    private com.a.c.r a()
    {
        this.o = 1;
        return this;
    }

    private com.a.c.r a(double p2)
    {
        com.a.c.b.s v0_1 = this.a.a();
        v0_1.b = p2;
        this.a = v0_1;
        return this;
    }

    private com.a.c.r a(int p2)
    {
        this.i = p2;
        this.h = 0;
        return this;
    }

    private com.a.c.r a(int p2, int p3)
    {
        this.i = p2;
        this.j = p3;
        this.h = 0;
        return this;
    }

    private com.a.c.r a(com.a.c.ah p1)
    {
        this.b = p1;
        return this;
    }

    private com.a.c.r a(com.a.c.ap p2)
    {
        this.e.add(p2);
        return this;
    }

    private com.a.c.r a(com.a.c.b p4)
    {
        this.a = this.a.a(p4, 1, 0);
        return this;
    }

    private com.a.c.r a(com.a.c.d p1)
    {
        this.c = p1;
        return this;
    }

    private com.a.c.r a(com.a.c.j p1)
    {
        this.c = p1;
        return this;
    }

    private com.a.c.r a(Class p4, Object p5)
    {
        if ((!(p5 instanceof com.a.c.ae)) && ((!(p5 instanceof com.a.c.v)) && (!(p5 instanceof com.a.c.an)))) {
            java.util.List v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        com.a.c.b.a.a(v0_3);
        if (((p5 instanceof com.a.c.v)) || ((p5 instanceof com.a.c.ae))) {
            this.f.add(0, com.a.c.ak.a(p4, p5));
        }
        if ((p5 instanceof com.a.c.an)) {
            this.e.add(com.a.c.b.a.z.b(p4, ((com.a.c.an) p5)));
        }
        return this;
    }

    private com.a.c.r a(String p1)
    {
        this.h = p1;
        return this;
    }

    private com.a.c.r a(reflect.Type p3, Object p4)
    {
        if ((!(p4 instanceof com.a.c.ae)) && ((!(p4 instanceof com.a.c.v)) && ((!(p4 instanceof com.a.c.s)) && (!(p4 instanceof com.a.c.an))))) {
            java.util.List v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        com.a.c.b.a.a(v0_4);
        if ((p4 instanceof com.a.c.s)) {
            this.d.put(p3, ((com.a.c.s) p4));
        }
        if (((p4 instanceof com.a.c.ae)) || ((p4 instanceof com.a.c.v))) {
            this.e.add(com.a.c.ak.b(com.a.c.c.a.a(p3), p4));
        }
        if ((p4 instanceof com.a.c.an)) {
            this.e.add(com.a.c.b.a.z.a(com.a.c.c.a.a(p3), ((com.a.c.an) p4)));
        }
        return this;
    }

    private varargs com.a.c.r a(int[] p6)
    {
        int v0 = 0;
        com.a.c.b.s v1_1 = this.a.a();
        v1_1.c = 0;
        int v2 = p6.length;
        while (v0 < v2) {
            v1_1.c = (p6[v0] | v1_1.c);
            v0++;
        }
        this.a = v1_1;
        return this;
    }

    private varargs com.a.c.r a(com.a.c.b[] p6)
    {
        int v1 = p6.length;
        int v0 = 0;
        while (v0 < v1) {
            this.a = this.a.a(p6[v0], 1, 1);
            v0++;
        }
        return this;
    }

    private static void a(String p3, int p4, int p5, java.util.List p6)
    {
        if ((p3 == null) || ("".equals(p3.trim()))) {
            if ((p4 != 2) && (p5 != 2)) {
                com.a.c.a v0_3 = new com.a.c.a(p4, p5);
                p6.add(com.a.c.ak.a(com.a.c.c.a.a(java.util.Date), v0_3));
                p6.add(com.a.c.ak.a(com.a.c.c.a.a(java.sql.Timestamp), v0_3));
                p6.add(com.a.c.ak.a(com.a.c.c.a.a(java.sql.Date), v0_3));
            }
        } else {
            v0_3 = new com.a.c.a(p3);
        }
        return;
    }

    private com.a.c.r b()
    {
        com.a.c.b.s v0_1 = this.a.a();
        v0_1.e = 1;
        this.a = v0_1;
        return this;
    }

    private com.a.c.r b(com.a.c.b p4)
    {
        this.a = this.a.a(p4, 0, 1);
        return this;
    }

    private com.a.c.r c()
    {
        this.g = 1;
        return this;
    }

    private com.a.c.r d()
    {
        this.k = 1;
        return this;
    }

    private com.a.c.r e()
    {
        com.a.c.b.s v0_1 = this.a.a();
        v0_1.d = 0;
        this.a = v0_1;
        return this;
    }

    private com.a.c.r f()
    {
        this.n = 1;
        return this;
    }

    private com.a.c.r g()
    {
        this.m = 0;
        return this;
    }

    private com.a.c.r h()
    {
        this.l = 1;
        return this;
    }

    private com.a.c.k i()
    {
        com.a.c.a v0_5;
        java.util.ArrayList v11_1 = new java.util.ArrayList();
        v11_1.addAll(this.e);
        java.util.Collections.reverse(v11_1);
        v11_1.addAll(this.f);
        com.a.c.c.a v1_0 = this.h;
        com.a.c.j v2_0 = this.i;
        java.util.Map v3_0 = this.j;
        if ((v1_0 == null) || ("".equals(v1_0.trim()))) {
            if ((v2_0 != 2) && (v3_0 != 2)) {
                v0_5 = new com.a.c.a(v2_0, v3_0);
                v11_1.add(com.a.c.ak.a(com.a.c.c.a.a(java.util.Date), v0_5));
                v11_1.add(com.a.c.ak.a(com.a.c.c.a.a(java.sql.Timestamp), v0_5));
                v11_1.add(com.a.c.ak.a(com.a.c.c.a.a(java.sql.Date), v0_5));
            }
        } else {
            v0_5 = new com.a.c.a(v1_0);
        }
        return new com.a.c.k(this.a, this.c, this.d, this.g, this.k, this.o, this.m, this.n, this.l, this.b, v11_1);
    }
}
