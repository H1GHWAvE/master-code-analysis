package com.a.c;
public abstract class an {

    public an()
    {
        return;
    }

    private com.a.c.an a()
    {
        return new com.a.c.ao(this);
    }

    private Object a(com.a.c.w p3)
    {
        try {
            return this.a(new com.a.c.b.a.h(p3));
        } catch (java.io.IOException v0_3) {
            throw new com.a.c.x(v0_3);
        }
    }

    private Object a(java.io.Reader p2)
    {
        return this.a(new com.a.c.d.a(p2));
    }

    private Object a(String p3)
    {
        return this.a(new com.a.c.d.a(new java.io.StringReader(p3)));
    }

    private void a(java.io.Writer p2, Object p3)
    {
        this.a(new com.a.c.d.e(p2), p3);
        return;
    }

    private String b(Object p3)
    {
        String v0_1 = new java.io.StringWriter();
        this.a(new com.a.c.d.e(v0_1), p3);
        return v0_1.toString();
    }

    public final com.a.c.w a(Object p3)
    {
        try {
            java.io.IOException v0_1 = new com.a.c.b.a.j();
            this.a(v0_1, p3);
            return v0_1.a();
        } catch (java.io.IOException v0_3) {
            throw new com.a.c.x(v0_3);
        }
    }

    public abstract Object a();

    public abstract void a();
}
