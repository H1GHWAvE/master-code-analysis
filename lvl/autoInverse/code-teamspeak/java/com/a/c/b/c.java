package com.a.c.b;
final class c implements java.io.Serializable, java.lang.reflect.GenericArrayType {
    private static final long b;
    private final reflect.Type a;

    public c(reflect.Type p2)
    {
        this.a = com.a.c.b.b.a(p2);
        return;
    }

    public final boolean equals(Object p2)
    {
        if ((!(p2 instanceof reflect.GenericArrayType)) || (!com.a.c.b.b.a(this, ((reflect.GenericArrayType) p2)))) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final reflect.Type getGenericComponentType()
    {
        return this.a;
    }

    public final int hashCode()
    {
        return this.a.hashCode();
    }

    public final String toString()
    {
        return new StringBuilder().append(com.a.c.b.b.c(this.a)).append("[]").toString();
    }
}
