package com.a.c.b;
public final class f {
    private final java.util.Map a;

    public f(java.util.Map p1)
    {
        this.a = p1;
        return;
    }

    private com.a.c.b.ao a(Class p3)
    {
        try {
            com.a.c.b.l v0_1 = new Class[0];
            reflect.Constructor v1 = p3.getDeclaredConstructor(v0_1);
        } catch (com.a.c.b.l v0) {
            com.a.c.b.l v0_5 = 0;
            return v0_5;
        }
        if (!v1.isAccessible()) {
            v1.setAccessible(1);
        }
        v0_5 = new com.a.c.b.l(this, v1);
        return v0_5;
    }

    private com.a.c.b.ao a(reflect.Type p4, Class p5)
    {
        com.a.c.b.h v0_4;
        if (!java.util.Collection.isAssignableFrom(p5)) {
            if (!java.util.Map.isAssignableFrom(p5)) {
                v0_4 = 0;
            } else {
                if (!java.util.SortedMap.isAssignableFrom(p5)) {
                    if ((!(p4 instanceof reflect.ParameterizedType)) || (String.isAssignableFrom(com.a.c.c.a.a(((reflect.ParameterizedType) p4).getActualTypeArguments()[0]).a))) {
                        v0_4 = new com.a.c.b.i(this);
                    } else {
                        v0_4 = new com.a.c.b.h(this);
                    }
                } else {
                    v0_4 = new com.a.c.b.r(this);
                }
            }
        } else {
            if (!java.util.SortedSet.isAssignableFrom(p5)) {
                if (!java.util.EnumSet.isAssignableFrom(p5)) {
                    if (!java.util.Set.isAssignableFrom(p5)) {
                        if (!java.util.Queue.isAssignableFrom(p5)) {
                            v0_4 = new com.a.c.b.q(this);
                        } else {
                            v0_4 = new com.a.c.b.p(this);
                        }
                    } else {
                        v0_4 = new com.a.c.b.o(this);
                    }
                } else {
                    v0_4 = new com.a.c.b.n(this, p4);
                }
            } else {
                v0_4 = new com.a.c.b.m(this);
            }
        }
        return v0_4;
    }

    private com.a.c.b.ao b(reflect.Type p2, Class p3)
    {
        return new com.a.c.b.j(this, p3, p2);
    }

    public final com.a.c.b.ao a(com.a.c.c.a p6)
    {
        com.a.c.b.h v0_6;
        reflect.Type v1 = p6.b;
        com.a.c.b.k v2_0 = p6.a;
        com.a.c.b.h v0_2 = ((com.a.c.s) this.a.get(v1));
        if (v0_2 == null) {
            com.a.c.b.h v0_5 = ((com.a.c.s) this.a.get(v2_0));
            if (v0_5 == null) {
                v0_6 = this.a(v2_0);
                if (v0_6 == null) {
                    if (!java.util.Collection.isAssignableFrom(v2_0)) {
                        if (!java.util.Map.isAssignableFrom(v2_0)) {
                            v0_6 = 0;
                        } else {
                            if (!java.util.SortedMap.isAssignableFrom(v2_0)) {
                                if ((!(v1 instanceof reflect.ParameterizedType)) || (String.isAssignableFrom(com.a.c.c.a.a(((reflect.ParameterizedType) v1).getActualTypeArguments()[0]).a))) {
                                    v0_6 = new com.a.c.b.i(this);
                                } else {
                                    v0_6 = new com.a.c.b.h(this);
                                }
                            } else {
                                v0_6 = new com.a.c.b.r(this);
                            }
                        }
                    } else {
                        if (!java.util.SortedSet.isAssignableFrom(v2_0)) {
                            if (!java.util.EnumSet.isAssignableFrom(v2_0)) {
                                if (!java.util.Set.isAssignableFrom(v2_0)) {
                                    if (!java.util.Queue.isAssignableFrom(v2_0)) {
                                        v0_6 = new com.a.c.b.q(this);
                                    } else {
                                        v0_6 = new com.a.c.b.p(this);
                                    }
                                } else {
                                    v0_6 = new com.a.c.b.o(this);
                                }
                            } else {
                                v0_6 = new com.a.c.b.n(this, v1);
                            }
                        } else {
                            v0_6 = new com.a.c.b.m(this);
                        }
                    }
                    if (v0_6 == null) {
                        v0_6 = new com.a.c.b.j(this, v2_0, v1);
                    }
                }
            } else {
                v0_6 = new com.a.c.b.k(this, v0_5, v1);
            }
        } else {
            v0_6 = new com.a.c.b.g(this, v0_2, v1);
        }
        return v0_6;
    }

    public final String toString()
    {
        return this.a.toString();
    }
}
