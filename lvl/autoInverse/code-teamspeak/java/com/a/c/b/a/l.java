package com.a.c.b.a;
public final class l implements com.a.c.ap {
    private final com.a.c.b.f a;
    private final boolean b;

    public l(com.a.c.b.f p1, boolean p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    private static com.a.c.an a(com.a.c.k p1, reflect.Type p2)
    {
        if ((p2 != Boolean.TYPE) && (p2 != Boolean)) {
            com.a.c.an v0_3 = p1.a(com.a.c.c.a.a(p2));
        } else {
            v0_3 = com.a.c.b.a.z.f;
        }
        return v0_3;
    }

    static synthetic boolean a(com.a.c.b.a.l p1)
    {
        return p1.b;
    }

    public final com.a.c.an a(com.a.c.k p9, com.a.c.c.a p10)
    {
        com.a.c.b.a.m v0_7;
        com.a.c.b.a.m v0_0 = p10.b;
        if (java.util.Map.isAssignableFrom(p10.a)) {
            com.a.c.an v4;
            com.a.c.b.a.l v1_3 = com.a.c.b.b.b(v0_0, com.a.c.b.b.b(v0_0));
            com.a.c.b.a.m v0_1 = v1_3[0];
            if ((v0_1 != Boolean.TYPE) && (v0_1 != Boolean)) {
                v4 = p9.a(com.a.c.c.a.a(v0_1));
            } else {
                v4 = com.a.c.b.a.z.f;
            }
            v0_7 = new com.a.c.b.a.m(this, p9, v1_3[0], v4, v1_3[1], p9.a(com.a.c.c.a.a(v1_3[1])), this.a.a(p10));
        } else {
            v0_7 = 0;
        }
        return v0_7;
    }
}
