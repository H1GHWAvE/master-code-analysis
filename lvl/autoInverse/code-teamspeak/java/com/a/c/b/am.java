package com.a.c.b;
abstract class am implements java.util.Iterator {
    com.a.c.b.an b;
    com.a.c.b.an c;
    int d;
    final synthetic com.a.c.b.ag e;

    private am(com.a.c.b.ag p2)
    {
        this.e = p2;
        this.b = this.e.e.d;
        this.c = 0;
        this.d = this.e.d;
        return;
    }

    synthetic am(com.a.c.b.ag p1, byte p2)
    {
        this(p1);
        return;
    }

    final com.a.c.b.an a()
    {
        java.util.ConcurrentModificationException v0_0 = this.b;
        if (v0_0 != this.e.e) {
            if (this.e.d == this.d) {
                this.b = v0_0.d;
                this.c = v0_0;
                return v0_0;
            } else {
                throw new java.util.ConcurrentModificationException();
            }
        } else {
            throw new java.util.NoSuchElementException();
        }
    }

    public final boolean hasNext()
    {
        int v0_1;
        if (this.b == this.e.e) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final void remove()
    {
        if (this.c != null) {
            this.e.a(this.c, 1);
            this.c = 0;
            this.d = this.e.d;
            return;
        } else {
            throw new IllegalStateException();
        }
    }
}
