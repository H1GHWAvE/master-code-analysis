package com.a.c.b.a;
public final class h extends com.a.c.d.a {
    private static final java.io.Reader c;
    private static final Object d;
    public final java.util.List a;

    static h()
    {
        com.a.c.b.a.h.c = new com.a.c.b.a.i();
        com.a.c.b.a.h.d = new Object();
        return;
    }

    public h(com.a.c.w p2)
    {
        this(com.a.c.b.a.h.c);
        this.a = new java.util.ArrayList();
        this.a.add(p2);
        return;
    }

    private Object q()
    {
        return this.a.remove((this.a.size() - 1));
    }

    private void r()
    {
        this.a(com.a.c.d.d.e);
        String v0_4 = ((java.util.Map$Entry) ((java.util.Iterator) this.g()).next());
        this.a.add(v0_4.getValue());
        this.a.add(new com.a.c.ac(((String) v0_4.getKey())));
        return;
    }

    public final void a()
    {
        this.a(com.a.c.d.d.a);
        this.a.add(((com.a.c.t) this.g()).iterator());
        return;
    }

    public final void a(com.a.c.d.d p4)
    {
        if (this.f() == p4) {
            return;
        } else {
            throw new IllegalStateException(new StringBuilder("Expected ").append(p4).append(" but was ").append(this.f()).toString());
        }
    }

    public final void b()
    {
        this.a(com.a.c.d.d.b);
        this.q();
        this.q();
        return;
    }

    public final void c()
    {
        this.a(com.a.c.d.d.c);
        this.a.add(((com.a.c.z) this.g()).a.entrySet().iterator());
        return;
    }

    public final void close()
    {
        this.a.clear();
        this.a.add(com.a.c.b.a.h.d);
        return;
    }

    public final void d()
    {
        this.a(com.a.c.d.d.d);
        this.q();
        this.q();
        return;
    }

    public final boolean e()
    {
        int v0_1;
        int v0_0 = this.f();
        if ((v0_0 == com.a.c.d.d.d) || (v0_0 == com.a.c.d.d.b)) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public final com.a.c.d.d f()
    {
        while (!this.a.isEmpty()) {
            AssertionError v0_2 = this.g();
            if (!(v0_2 instanceof java.util.Iterator)) {
                if (!(v0_2 instanceof com.a.c.z)) {
                    if (!(v0_2 instanceof com.a.c.t)) {
                        if (!(v0_2 instanceof com.a.c.ac)) {
                            if (!(v0_2 instanceof com.a.c.y)) {
                                if (v0_2 != com.a.c.b.a.h.d) {
                                    throw new AssertionError();
                                } else {
                                    throw new IllegalStateException("JsonReader is closed");
                                }
                            } else {
                                AssertionError v0_7 = com.a.c.d.d.i;
                            }
                        } else {
                            AssertionError v0_8 = ((com.a.c.ac) v0_2);
                            if (!(v0_8.a instanceof String)) {
                                if (!(v0_8.a instanceof Boolean)) {
                                    if (!(v0_8.a instanceof Number)) {
                                        throw new AssertionError();
                                    } else {
                                        v0_7 = com.a.c.d.d.g;
                                    }
                                } else {
                                    v0_7 = com.a.c.d.d.h;
                                }
                            } else {
                                v0_7 = com.a.c.d.d.f;
                            }
                        }
                    } else {
                        v0_7 = com.a.c.d.d.a;
                    }
                } else {
                    v0_7 = com.a.c.d.d.c;
                }
            } else {
                String v1_13 = (this.a.get((this.a.size() - 2)) instanceof com.a.c.z);
                AssertionError v0_13 = ((java.util.Iterator) v0_2);
                if (!v0_13.hasNext()) {
                    if (v1_13 == null) {
                        v0_7 = com.a.c.d.d.b;
                    } else {
                        v0_7 = com.a.c.d.d.d;
                    }
                } else {
                    if (v1_13 == null) {
                        this.a.add(v0_13.next());
                    } else {
                        v0_7 = com.a.c.d.d.e;
                    }
                }
            }
            return v0_7;
        }
        v0_7 = com.a.c.d.d.j;
        return v0_7;
    }

    public final Object g()
    {
        return this.a.get((this.a.size() - 1));
    }

    public final String h()
    {
        this.a(com.a.c.d.d.e);
        String v0_4 = ((java.util.Map$Entry) ((java.util.Iterator) this.g()).next());
        this.a.add(v0_4.getValue());
        return ((String) v0_4.getKey());
    }

    public final String i()
    {
        String v0_0 = this.f();
        if ((v0_0 == com.a.c.d.d.f) || (v0_0 == com.a.c.d.d.g)) {
            return ((com.a.c.ac) this.q()).b();
        } else {
            throw new IllegalStateException(new StringBuilder("Expected ").append(com.a.c.d.d.f).append(" but was ").append(v0_0).toString());
        }
    }

    public final boolean j()
    {
        this.a(com.a.c.d.d.h);
        return ((com.a.c.ac) this.q()).l();
    }

    public final void k()
    {
        this.a(com.a.c.d.d.i);
        this.q();
        return;
    }

    public final double l()
    {
        String v0_0 = this.f();
        if ((v0_0 == com.a.c.d.d.g) || (v0_0 == com.a.c.d.d.f)) {
            String v0_3 = ((com.a.c.ac) this.g()).c();
            if ((this.b) || ((!Double.isNaN(v0_3)) && (!Double.isInfinite(v0_3)))) {
                this.q();
                return v0_3;
            } else {
                throw new NumberFormatException(new StringBuilder("JSON forbids NaN and infinities: ").append(v0_3).toString());
            }
        } else {
            throw new IllegalStateException(new StringBuilder("Expected ").append(com.a.c.d.d.g).append(" but was ").append(v0_0).toString());
        }
    }

    public final long m()
    {
        String v0_0 = this.f();
        if ((v0_0 == com.a.c.d.d.g) || (v0_0 == com.a.c.d.d.f)) {
            String v0_3 = ((com.a.c.ac) this.g()).g();
            this.q();
            return v0_3;
        } else {
            throw new IllegalStateException(new StringBuilder("Expected ").append(com.a.c.d.d.g).append(" but was ").append(v0_0).toString());
        }
    }

    public final int n()
    {
        String v0_0 = this.f();
        if ((v0_0 == com.a.c.d.d.g) || (v0_0 == com.a.c.d.d.f)) {
            String v0_3 = ((com.a.c.ac) this.g()).h();
            this.q();
            return v0_3;
        } else {
            throw new IllegalStateException(new StringBuilder("Expected ").append(com.a.c.d.d.g).append(" but was ").append(v0_0).toString());
        }
    }

    public final void o()
    {
        if (this.f() != com.a.c.d.d.e) {
            this.q();
        } else {
            this.h();
        }
        return;
    }

    public final String toString()
    {
        return this.getClass().getSimpleName();
    }
}
