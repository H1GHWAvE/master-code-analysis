package com.a.c.b.a;
public final class j extends com.a.c.d.e {
    private static final java.io.Writer f;
    private static final com.a.c.ac g;
    private final java.util.List h;
    private String i;
    private com.a.c.w j;

    static j()
    {
        com.a.c.b.a.j.f = new com.a.c.b.a.k();
        com.a.c.b.a.j.g = new com.a.c.ac("closed");
        return;
    }

    public j()
    {
        this(com.a.c.b.a.j.f);
        this.h = new java.util.ArrayList();
        this.j = com.a.c.y.a;
        return;
    }

    private void a(com.a.c.w p3)
    {
        if (this.i == null) {
            if (!this.h.isEmpty()) {
                IllegalStateException v0_3 = this.g();
                if (!(v0_3 instanceof com.a.c.t)) {
                    throw new IllegalStateException();
                } else {
                    ((com.a.c.t) v0_3).a(p3);
                }
            } else {
                this.j = p3;
            }
        } else {
            if ((!(p3 instanceof com.a.c.y)) || (this.e)) {
                ((com.a.c.z) this.g()).a(this.i, p3);
            }
            this.i = 0;
        }
        return;
    }

    private com.a.c.w g()
    {
        return ((com.a.c.w) this.h.get((this.h.size() - 1)));
    }

    public final com.a.c.d.e a(double p4)
    {
        if ((this.c) || ((!Double.isNaN(p4)) && (!Double.isInfinite(p4)))) {
            this.a(new com.a.c.ac(Double.valueOf(p4)));
            return this;
        } else {
            throw new IllegalArgumentException(new StringBuilder("JSON forbids NaN and infinities: ").append(p4).toString());
        }
    }

    public final com.a.c.d.e a(long p4)
    {
        this.a(new com.a.c.ac(Long.valueOf(p4)));
        return this;
    }

    public final com.a.c.d.e a(Number p4)
    {
        if (p4 != null) {
            if (!this.c) {
                IllegalArgumentException v0_1 = p4.doubleValue();
                if ((Double.isNaN(v0_1)) || (Double.isInfinite(v0_1))) {
                    throw new IllegalArgumentException(new StringBuilder("JSON forbids NaN and infinities: ").append(p4).toString());
                }
            }
            this.a(new com.a.c.ac(p4));
        } else {
            this = this.f();
        }
        return this;
    }

    public final com.a.c.d.e a(String p2)
    {
        if ((!this.h.isEmpty()) && (this.i == null)) {
            if (!(this.g() instanceof com.a.c.z)) {
                throw new IllegalStateException();
            } else {
                this.i = p2;
                return this;
            }
        } else {
            throw new IllegalStateException();
        }
    }

    public final com.a.c.d.e a(boolean p3)
    {
        this.a(new com.a.c.ac(Boolean.valueOf(p3)));
        return this;
    }

    public final com.a.c.w a()
    {
        if (this.h.isEmpty()) {
            return this.j;
        } else {
            throw new IllegalStateException(new StringBuilder("Expected one JSON element but was ").append(this.h).toString());
        }
    }

    public final com.a.c.d.e b()
    {
        com.a.c.t v0_1 = new com.a.c.t();
        this.a(v0_1);
        this.h.add(v0_1);
        return this;
    }

    public final com.a.c.d.e b(String p2)
    {
        if (p2 != null) {
            this.a(new com.a.c.ac(p2));
        } else {
            this = this.f();
        }
        return this;
    }

    public final com.a.c.d.e c()
    {
        if ((!this.h.isEmpty()) && (this.i == null)) {
            if (!(this.g() instanceof com.a.c.t)) {
                throw new IllegalStateException();
            } else {
                this.h.remove((this.h.size() - 1));
                return this;
            }
        } else {
            throw new IllegalStateException();
        }
    }

    public final void close()
    {
        if (this.h.isEmpty()) {
            this.h.add(com.a.c.b.a.j.g);
            return;
        } else {
            throw new java.io.IOException("Incomplete document");
        }
    }

    public final com.a.c.d.e d()
    {
        com.a.c.z v0_1 = new com.a.c.z();
        this.a(v0_1);
        this.h.add(v0_1);
        return this;
    }

    public final com.a.c.d.e e()
    {
        if ((!this.h.isEmpty()) && (this.i == null)) {
            if (!(this.g() instanceof com.a.c.z)) {
                throw new IllegalStateException();
            } else {
                this.h.remove((this.h.size() - 1));
                return this;
            }
        } else {
            throw new IllegalStateException();
        }
    }

    public final com.a.c.d.e f()
    {
        this.a(com.a.c.y.a);
        return this;
    }

    public final void flush()
    {
        return;
    }
}
