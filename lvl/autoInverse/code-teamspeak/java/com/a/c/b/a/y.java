package com.a.c.b.a;
final class y extends com.a.c.an {
    private final com.a.c.k a;
    private final com.a.c.an b;
    private final reflect.Type c;

    y(com.a.c.k p1, com.a.c.an p2, reflect.Type p3)
    {
        this.a = p1;
        this.b = p2;
        this.c = p3;
        return;
    }

    private static reflect.Type a(reflect.Type p1, Object p2)
    {
        if ((p2 != null) && ((p1 == Object) || (((p1 instanceof reflect.TypeVariable)) || ((p1 instanceof Class))))) {
            p1 = p2.getClass();
        }
        return p1;
    }

    public final Object a(com.a.c.d.a p2)
    {
        return this.b.a(p2);
    }

    public final void a(com.a.c.d.e p4, Object p5)
    {
        boolean v1_0 = this.b;
        com.a.c.an v0_0 = this.c;
        if ((p5 != null) && ((v0_0 == Object) || (((v0_0 instanceof reflect.TypeVariable)) || ((v0_0 instanceof Class))))) {
            v0_0 = p5.getClass();
        }
        com.a.c.an v0_1;
        if (v0_0 == this.c) {
            v0_1 = v1_0;
        } else {
            v0_1 = this.a.a(com.a.c.c.a.a(v0_0));
            if (((v0_1 instanceof com.a.c.b.a.s)) && (!(this.b instanceof com.a.c.b.a.s))) {
                v0_1 = this.b;
            }
        }
        v0_1.a(p4, p5);
        return;
    }
}
