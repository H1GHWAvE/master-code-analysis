package com.a.c.b.a;
final class av implements com.a.c.ap {
    final synthetic Class a;
    final synthetic com.a.c.an b;

    av(Class p1, com.a.c.an p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    public final com.a.c.an a(com.a.c.k p3, com.a.c.c.a p4)
    {
        int v0_1;
        if (p4.a != this.a) {
            v0_1 = 0;
        } else {
            v0_1 = this.b;
        }
        return v0_1;
    }

    public final String toString()
    {
        return new StringBuilder("Factory[type=").append(this.a.getName()).append(",adapter=").append(this.b).append("]").toString();
    }
}
