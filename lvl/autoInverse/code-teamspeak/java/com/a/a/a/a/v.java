package com.a.a.a.a;
public final class v implements com.a.a.a.a.s {
    private static final String a = "ServerManagedPolicy";
    private static final String b = "com.android.vending.licensing.ServerManagedPolicy";
    private static final String f = "lastResponse";
    private static final String g = "validityTimestamp";
    private static final String h = "retryUntil";
    private static final String i = "maxRetries";
    private static final String j = "retryCount";
    private static final String k = "rawData";
    private static final String l = "0";
    private static final String m = "0";
    private static final String n = "0";
    private static final String o = "0";
    private static final long p = 60000;
    private long q;
    private long r;
    private long s;
    private long t;
    private long u;
    private int v;
    private com.a.a.a.a.t w;
    private String x;

    public v(android.content.Context p4, com.a.a.a.a.r p5)
    {
        this.u = 0;
        this.w = new com.a.a.a.a.t(p4.getSharedPreferences("com.android.vending.licensing.ServerManagedPolicy", 0), p5);
        this.x = this.w.b("rawData", "");
        this.v = Integer.parseInt(this.w.b("lastResponse", Integer.toString(291)));
        this.q = Long.parseLong(this.w.b("validityTimestamp", "0"));
        this.r = Long.parseLong(this.w.b("retryUntil", "0"));
        this.s = Long.parseLong(this.w.b("maxRetries", "0"));
        this.t = Long.parseLong(this.w.b("retryCount", "0"));
        return;
    }

    private void a(int p4)
    {
        this.u = System.currentTimeMillis();
        this.v = p4;
        this.w.a("lastResponse", Integer.toString(p4));
        return;
    }

    private void a(long p4)
    {
        this.t = p4;
        this.w.a("retryCount", Long.toString(p4));
        return;
    }

    private void a(String p3)
    {
        this.w.a("rawData", p3);
        return;
    }

    private void b(String p5)
    {
        try {
            com.a.a.a.a.t v0_1 = Long.valueOf(Long.parseLong(p5));
        } catch (com.a.a.a.a.t v0) {
            android.util.Log.w("ServerManagedPolicy", "License validity timestamp (VT) missing, caching for a minute");
            v0_1 = Long.valueOf((System.currentTimeMillis() + 60000));
            p5 = Long.toString(v0_1.longValue());
        }
        this.q = v0_1.longValue();
        this.w.a("validityTimestamp", p5);
        return;
    }

    private long c()
    {
        return this.t;
    }

    private void c(String p3)
    {
        try {
            com.a.a.a.a.t v0_1 = Long.valueOf(Long.parseLong(p3));
        } catch (com.a.a.a.a.t v0) {
            android.util.Log.w("ServerManagedPolicy", "License retry timestamp (GT) missing, grace period disabled");
            p3 = "0";
            v0_1 = Long.valueOf(0);
        }
        this.r = v0_1.longValue();
        this.w.a("retryUntil", p3);
        return;
    }

    private long d()
    {
        return this.q;
    }

    private void d(String p3)
    {
        try {
            com.a.a.a.a.t v0_1 = Long.valueOf(Long.parseLong(p3));
        } catch (com.a.a.a.a.t v0) {
            android.util.Log.w("ServerManagedPolicy", "Licence retry count (GR) missing, grace period disabled");
            p3 = "0";
            v0_1 = Long.valueOf(0);
        }
        this.s = v0_1.longValue();
        this.w.a("maxRetries", p3);
        return;
    }

    private long e()
    {
        return this.r;
    }

    private static java.util.Map e(String p7)
    {
        int v0_0 = 0;
        java.util.HashMap v1_1 = new java.util.HashMap();
        try {
            String v2_1 = p7.split("&");
            int v3 = v2_1.length;
        } catch (int v0) {
            android.util.Log.w("ServerManagedPolicy", "Invalid syntax error while decoding extras data from server.");
            return v1_1;
        }
        while (v0_0 < v3) {
            String v4_1 = v2_1[v0_0].split("=");
            v1_1.put(v4_1[0], v4_1[1]);
            v0_0++;
        }
        return v1_1;
    }

    private long f()
    {
        return this.s;
    }

    public final void a(int p5, com.a.a.a.a.u p6)
    {
        if (p5 == 291) {
            this.a((this.t + 1));
        } else {
            this.a(0);
        }
        if (p5 != 256) {
            if (p5 == 561) {
                this.b("0");
                this.c("0");
                this.d("0");
                this.a("");
            }
        } else {
            String v1_0 = com.a.a.a.a.v.e(p6.g);
            this.v = p5;
            this.b(((String) v1_0.get("VT")));
            this.c(((String) v1_0.get("GT")));
            this.d(((String) v1_0.get("GR")));
        }
        this.u = System.currentTimeMillis();
        this.v = p5;
        this.w.a("lastResponse", Integer.toString(p5));
        this.w.a();
        return;
    }

    public final void a(com.a.a.a.a.u p5, String p6, String p7)
    {
        com.a.a.a.a.t v0_1 = new com.a.a.a.a.w(p5, p6, p7);
        com.a.c.k v1_1 = new com.a.c.k();
        Class v2 = v0_1.getClass();
        java.io.StringWriter v3_1 = new java.io.StringWriter();
        v1_1.a(v0_1, v2, v3_1);
        this.a(v3_1.toString());
        this.w.a();
        return;
    }

    public final boolean a()
    {
        int v0 = 0;
        long v2_0 = System.currentTimeMillis();
        if (this.v != 256) {
            if ((this.v != 291) || (v2_0 >= (this.u + 60000))) {
                v0 = 0;
            } else {
                if ((v2_0 > this.r) && (this.t > this.s)) {
                    v0 = 0;
                }
            }
        } else {
            if (v2_0 > this.q) {
            }
        }
        return v0;
    }

    public final com.a.a.a.a.w b()
    {
        com.a.a.a.a.w v0_2;
        com.a.a.a.a.w v0_1 = new com.a.c.k();
        Class v1_0 = this.x;
        if (v1_0 != null) {
            Class v1_2 = new com.a.c.d.a(new java.io.StringReader(v1_0));
            v0_2 = v0_1.a(v1_2, com.a.a.a.a.w);
            com.a.c.k.a(v0_2, v1_2);
        } else {
            v0_2 = 0;
        }
        return ((com.a.a.a.a.w) com.a.c.b.ap.a(com.a.a.a.a.w).cast(v0_2));
    }
}
