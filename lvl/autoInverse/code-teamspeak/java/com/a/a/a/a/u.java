package com.a.a.a.a;
public final class u {
    public int a;
    public int b;
    public String c;
    public String d;
    public String e;
    public long f;
    public String g;

    public u()
    {
        return;
    }

    private static com.a.a.a.a.u a(String p5)
    {
        long v0_3;
        long v0_1 = p5.indexOf(58);
        if (-1 != v0_1) {
            String v1_1 = p5.substring(0, v0_1);
            if (v0_1 < p5.length()) {
                v0_3 = p5.substring((v0_1 + 1));
                p5 = v1_1;
            } else {
                v0_3 = "";
                p5 = v1_1;
            }
        } else {
            v0_3 = "";
        }
        String v1_4 = android.text.TextUtils.split(p5, java.util.regex.Pattern.quote("|"));
        if (v1_4.length >= 6) {
            com.a.a.a.a.u v2_3 = new com.a.a.a.a.u();
            v2_3.g = v0_3;
            v2_3.a = Integer.parseInt(v1_4[0]);
            v2_3.b = Integer.parseInt(v1_4[1]);
            v2_3.c = v1_4[2];
            v2_3.d = v1_4[3];
            v2_3.e = v1_4[4];
            v2_3.f = Long.parseLong(v1_4[5]);
            return v2_3;
        } else {
            throw new IllegalArgumentException("Wrong number of fields.");
        }
    }

    public final String toString()
    {
        Object[] v1_1 = new Object[6];
        v1_1[0] = Integer.valueOf(this.a);
        v1_1[1] = Integer.valueOf(this.b);
        v1_1[2] = this.c;
        v1_1[3] = this.d;
        v1_1[4] = this.e;
        v1_1[5] = Long.valueOf(this.f);
        return android.text.TextUtils.join("|", v1_1);
    }
}
