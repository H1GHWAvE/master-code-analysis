package com.a.a.a.a;
final class l extends com.a.a.a.a.f {
    private static final int e = 257;
    private static final int f = 258;
    private static final int g = 259;
    final synthetic com.a.a.a.a.k b;
    private final com.a.a.a.a.p c;
    private Runnable d;

    public l(com.a.a.a.a.k p5, com.a.a.a.a.p p6)
    {
        this.b = p5;
        this.c = p6;
        this.d = new com.a.a.a.a.m(this, p5);
        android.util.Log.i("LicenseChecker", "Start monitoring timeout.");
        com.a.a.a.a.k.c(this.b).postDelayed(this.d, 10000);
        return;
    }

    static synthetic com.a.a.a.a.p a(com.a.a.a.a.l p1)
    {
        return p1.c;
    }

    private void a()
    {
        android.util.Log.i("LicenseChecker", "Start monitoring timeout.");
        com.a.a.a.a.k.c(this.b).postDelayed(this.d, 10000);
        return;
    }

    private void b()
    {
        android.util.Log.i("LicenseChecker", "Clearing timeout.");
        com.a.a.a.a.k.c(this.b).removeCallbacks(this.d);
        return;
    }

    static synthetic void b(com.a.a.a.a.l p2)
    {
        android.util.Log.i("LicenseChecker", "Clearing timeout.");
        com.a.a.a.a.k.c(p2.b).removeCallbacks(p2.d);
        return;
    }

    public final void a(int p3, String p4, String p5)
    {
        com.a.a.a.a.k.c(this.b).post(new com.a.a.a.a.n(this, p3, p4, p5));
        return;
    }
}
