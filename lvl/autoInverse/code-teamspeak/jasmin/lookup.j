.bytecode 50.0
.class public synchronized lookup
.super java/lang/Object

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/String;Lorg/xbill/DNS/Lookup;)V
iconst_0
istore 3
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 0
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc ":"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
aload 1
invokevirtual org/xbill/DNS/Lookup/getResult()I
ifeq L0
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuffer
dup
ldc " "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 1
invokevirtual org/xbill/DNS/Lookup/getErrorString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
L0:
getstatic java/lang/System/out Ljava/io/PrintStream;
invokevirtual java/io/PrintStream/println()V
aload 1
invokevirtual org/xbill/DNS/Lookup/getAliases()[Lorg/xbill/DNS/Name;
astore 0
aload 0
arraylength
ifle L1
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "# aliases: "
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
iconst_0
istore 2
L2:
iload 2
aload 0
arraylength
if_icmpge L3
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
iload 2
aaload
invokevirtual java/io/PrintStream/print(Ljava/lang/Object;)V
iload 2
aload 0
arraylength
iconst_1
isub
if_icmpge L4
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc " "
invokevirtual java/io/PrintStream/print(Ljava/lang/String;)V
L4:
iload 2
iconst_1
iadd
istore 2
goto L2
L3:
getstatic java/lang/System/out Ljava/io/PrintStream;
invokevirtual java/io/PrintStream/println()V
L1:
aload 1
invokevirtual org/xbill/DNS/Lookup/getResult()I
ifne L5
aload 1
invokevirtual org/xbill/DNS/Lookup/getAnswers()[Lorg/xbill/DNS/Record;
astore 0
iload 3
istore 2
L6:
iload 2
aload 0
arraylength
if_icmpge L5
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
iload 2
aaload
invokevirtual java/io/PrintStream/println(Ljava/lang/Object;)V
iload 2
iconst_1
iadd
istore 2
goto L6
L5:
return
.limit locals 4
.limit stack 4
.end method

.method public static main([Ljava/lang/String;)V
iconst_2
istore 1
aload 0
arraylength
iconst_2
if_icmple L0
aload 0
iconst_0
aaload
ldc "-t"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
aload 0
iconst_1
aaload
invokestatic org/xbill/DNS/Type/value(Ljava/lang/String;)I
istore 3
iload 3
istore 2
iload 3
ifge L1
new java/lang/IllegalArgumentException
dup
ldc "invalid type"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
iload 1
aload 0
arraylength
if_icmpge L2
new org/xbill/DNS/Lookup
dup
aload 0
iload 1
aaload
iload 2
invokespecial org/xbill/DNS/Lookup/<init>(Ljava/lang/String;I)V
astore 4
aload 4
invokevirtual org/xbill/DNS/Lookup/run()[Lorg/xbill/DNS/Record;
pop
aload 0
iload 1
aaload
aload 4
invokestatic lookup/a(Ljava/lang/String;Lorg/xbill/DNS/Lookup;)V
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
return
L0:
iconst_0
istore 1
iconst_1
istore 2
goto L1
.limit locals 5
.limit stack 4
.end method
