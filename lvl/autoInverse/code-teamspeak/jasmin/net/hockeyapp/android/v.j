.bytecode 50.0
.class final synchronized net/hockeyapp/android/v
.super android/os/AsyncTask

.field final synthetic 'a' Landroid/graphics/Bitmap;

.field final synthetic 'b' Landroid/content/Context;

.method <init>(Landroid/graphics/Bitmap;Landroid/content/Context;)V
aload 0
aload 1
putfield net/hockeyapp/android/v/a Landroid/graphics/Bitmap;
aload 0
aload 2
putfield net/hockeyapp/android/v/b Landroid/content/Context;
aload 0
invokespecial android/os/AsyncTask/<init>()V
return
.limit locals 3
.limit stack 2
.end method

.method private transient a([Ljava/io/File;)Ljava/lang/Boolean;
.catch java/lang/Exception from L0 to L1 using L2
L0:
new java/io/FileOutputStream
dup
aload 1
iconst_0
aaload
invokespecial java/io/FileOutputStream/<init>(Ljava/io/File;)V
astore 1
aload 0
getfield net/hockeyapp/android/v/a Landroid/graphics/Bitmap;
getstatic android/graphics/Bitmap$CompressFormat/JPEG Landroid/graphics/Bitmap$CompressFormat;
bipush 100
aload 1
invokevirtual android/graphics/Bitmap/compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
pop
aload 1
invokevirtual java/io/FileOutputStream/close()V
L1:
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
areturn
L2:
astore 1
ldc "HockeyApp"
ldc "Could not save screenshot."
aload 1
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
areturn
.limit locals 2
.limit stack 4
.end method

.method private a(Ljava/lang/Boolean;)V
aload 1
invokevirtual java/lang/Boolean/booleanValue()Z
ifne L0
aload 0
getfield net/hockeyapp/android/v/b Landroid/content/Context;
ldc "Screenshot could not be created. Sorry."
sipush 2000
invokestatic android/widget/Toast/makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;
invokevirtual android/widget/Toast/show()V
L0:
return
.limit locals 2
.limit stack 3
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
aload 0
aload 1
checkcast [Ljava/io/File;
invokespecial net/hockeyapp/android/v/a([Ljava/io/File;)Ljava/lang/Boolean;
areturn
.limit locals 2
.limit stack 2
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
aload 1
checkcast java/lang/Boolean
invokevirtual java/lang/Boolean/booleanValue()Z
ifne L0
aload 0
getfield net/hockeyapp/android/v/b Landroid/content/Context;
ldc "Screenshot could not be created. Sorry."
sipush 2000
invokestatic android/widget/Toast/makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;
invokevirtual android/widget/Toast/show()V
L0:
return
.limit locals 2
.limit stack 3
.end method
