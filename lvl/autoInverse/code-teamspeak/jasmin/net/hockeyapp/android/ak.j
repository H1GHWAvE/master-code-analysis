.bytecode 50.0
.class public final synchronized net/hockeyapp/android/ak
.super java/lang/Object

.field private static final 'a' Ljava/lang/String; = "startTime"

.field private static final 'b' Ljava/lang/String; = "usageTime"

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/app/Activity;)V
invokestatic java/lang/System/currentTimeMillis()J
lstore 1
aload 0
ifnonnull L0
return
L0:
aload 0
ldc "HockeyApp"
iconst_0
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
astore 3
aload 3
new java/lang/StringBuilder
dup
ldc "startTime"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/Object/hashCode()I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
lload 1
invokeinterface android/content/SharedPreferences$Editor/putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor; 3
pop
aload 3
invokestatic net/hockeyapp/android/e/n/a(Landroid/content/SharedPreferences$Editor;)V
return
.limit locals 4
.limit stack 4
.end method

.method public static a(Landroid/content/Context;)Z
getstatic net/hockeyapp/android/a/b Ljava/lang/String;
ifnonnull L0
aload 0
invokestatic net/hockeyapp/android/a/a(Landroid/content/Context;)V
getstatic net/hockeyapp/android/a/b Ljava/lang/String;
ifnonnull L0
iconst_0
ireturn
L0:
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static b(Landroid/content/Context;)J
aload 0
invokestatic net/hockeyapp/android/ak/a(Landroid/content/Context;)Z
ifne L0
lconst_0
lreturn
L0:
aload 0
ldc "HockeyApp"
iconst_0
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
new java/lang/StringBuilder
dup
ldc "usageTime"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
getstatic net/hockeyapp/android/a/b Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
lconst_0
invokeinterface android/content/SharedPreferences/getLong(Ljava/lang/String;J)J 3
ldc2_w 1000L
ldiv
lreturn
.limit locals 1
.limit stack 4
.end method

.method private static b(Landroid/app/Activity;)V
invokestatic java/lang/System/currentTimeMillis()J
lstore 1
aload 0
ifnonnull L0
L1:
return
L0:
aload 0
invokestatic net/hockeyapp/android/ak/a(Landroid/content/Context;)Z
ifeq L1
aload 0
ldc "HockeyApp"
iconst_0
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
astore 7
aload 7
new java/lang/StringBuilder
dup
ldc "startTime"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/Object/hashCode()I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
lconst_0
invokeinterface android/content/SharedPreferences/getLong(Ljava/lang/String;J)J 3
lstore 3
aload 7
new java/lang/StringBuilder
dup
ldc "usageTime"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
getstatic net/hockeyapp/android/a/b Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
lconst_0
invokeinterface android/content/SharedPreferences/getLong(Ljava/lang/String;J)J 3
lstore 5
lload 3
lconst_0
lcmp
ifle L1
aload 7
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
astore 0
aload 0
new java/lang/StringBuilder
dup
ldc "usageTime"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
getstatic net/hockeyapp/android/a/b Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
lload 1
lload 3
lsub
lload 5
ladd
invokeinterface android/content/SharedPreferences$Editor/putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor; 3
pop
aload 0
invokestatic net/hockeyapp/android/e/n/a(Landroid/content/SharedPreferences$Editor;)V
return
.limit locals 8
.limit stack 6
.end method

.method private static c(Landroid/content/Context;)Landroid/content/SharedPreferences;
aload 0
ldc "HockeyApp"
iconst_0
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
areturn
.limit locals 1
.limit stack 3
.end method
