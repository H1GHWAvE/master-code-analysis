.bytecode 50.0
.class public final synchronized net/hockeyapp/android/f/j
.super android/widget/LinearLayout

.field public static final 'a' I = 12289


.field public static final 'b' I = 12290


.field public static final 'c' I = 12291


.field public static final 'd' I = 12292


.field public static final 'e' I = 12293


.field private 'f' Landroid/widget/LinearLayout;

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
iconst_0
invokespecial net/hockeyapp/android/f/j/<init>(Landroid/content/Context;B)V
return
.limit locals 2
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;B)V
aload 0
aload 1
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 3
aload 0
iconst_m1
invokevirtual net/hockeyapp/android/f/j/setBackgroundColor(I)V
aload 0
aload 3
invokevirtual net/hockeyapp/android/f/j/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
new android/widget/LinearLayout
dup
aload 1
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
putfield net/hockeyapp/android/f/j/f Landroid/widget/LinearLayout;
aload 0
getfield net/hockeyapp/android/f/j/f Landroid/widget/LinearLayout;
sipush 12289
invokevirtual android/widget/LinearLayout/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
iconst_m1
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 3
iconst_1
ldc_w 20.0F
aload 0
invokevirtual net/hockeyapp/android/f/j/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
aload 3
bipush 49
putfield android/widget/LinearLayout$LayoutParams/gravity I
aload 0
getfield net/hockeyapp/android/f/j/f Landroid/widget/LinearLayout;
aload 3
invokevirtual android/widget/LinearLayout/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield net/hockeyapp/android/f/j/f Landroid/widget/LinearLayout;
iload 2
iload 2
iload 2
iload 2
invokevirtual android/widget/LinearLayout/setPadding(IIII)V
aload 0
getfield net/hockeyapp/android/f/j/f Landroid/widget/LinearLayout;
iconst_1
invokevirtual android/widget/LinearLayout/setOrientation(I)V
aload 0
aload 0
getfield net/hockeyapp/android/f/j/f Landroid/widget/LinearLayout;
invokevirtual net/hockeyapp/android/f/j/addView(Landroid/view/View;)V
new android/widget/TextView
dup
aload 1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
astore 3
aload 3
sipush 12290
invokevirtual android/widget/TextView/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 4
aload 4
iconst_0
iconst_0
iconst_0
iconst_1
ldc_w 30.0F
aload 0
invokevirtual net/hockeyapp/android/f/j/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 3
aload 4
invokevirtual android/widget/TextView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 3
sipush 1280
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 3
ldc_w -7829368
invokevirtual android/widget/TextView/setTextColor(I)V
aload 3
iconst_2
ldc_w 18.0F
invokevirtual android/widget/TextView/setTextSize(IF)V
aload 3
aconst_null
iconst_0
invokevirtual android/widget/TextView/setTypeface(Landroid/graphics/Typeface;I)V
aload 0
getfield net/hockeyapp/android/f/j/f Landroid/widget/LinearLayout;
aload 3
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
new android/widget/EditText
dup
aload 1
invokespecial android/widget/EditText/<init>(Landroid/content/Context;)V
astore 3
aload 3
sipush 12291
invokevirtual android/widget/EditText/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 4
aload 4
iconst_0
iconst_0
iconst_0
iconst_1
ldc_w 30.0F
aload 0
invokevirtual net/hockeyapp/android/f/j/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 3
aload 4
invokevirtual android/widget/EditText/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 3
sipush 1282
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/widget/EditText/setHint(Ljava/lang/CharSequence;)V
aload 3
iconst_5
invokevirtual android/widget/EditText/setImeOptions(I)V
aload 3
bipush 33
invokevirtual android/widget/EditText/setInputType(I)V
aload 3
ldc_w -7829368
invokevirtual android/widget/EditText/setTextColor(I)V
aload 3
iconst_2
ldc_w 15.0F
invokevirtual android/widget/EditText/setTextSize(IF)V
aload 3
aconst_null
iconst_0
invokevirtual android/widget/EditText/setTypeface(Landroid/graphics/Typeface;I)V
aload 3
ldc_w -3355444
invokevirtual android/widget/EditText/setHintTextColor(I)V
aload 1
aload 3
invokestatic net/hockeyapp/android/f/j/a(Landroid/content/Context;Landroid/widget/EditText;)V
aload 0
getfield net/hockeyapp/android/f/j/f Landroid/widget/LinearLayout;
aload 3
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
new android/widget/EditText
dup
aload 1
invokespecial android/widget/EditText/<init>(Landroid/content/Context;)V
astore 3
aload 3
sipush 12292
invokevirtual android/widget/EditText/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 4
aload 4
iconst_0
iconst_0
iconst_0
iconst_1
ldc_w 30.0F
aload 0
invokevirtual net/hockeyapp/android/f/j/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 3
aload 4
invokevirtual android/widget/EditText/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 3
sipush 1283
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/widget/EditText/setHint(Ljava/lang/CharSequence;)V
aload 3
iconst_5
invokevirtual android/widget/EditText/setImeOptions(I)V
aload 3
sipush 128
invokevirtual android/widget/EditText/setInputType(I)V
aload 3
invokestatic android/text/method/PasswordTransformationMethod/getInstance()Landroid/text/method/PasswordTransformationMethod;
invokevirtual android/widget/EditText/setTransformationMethod(Landroid/text/method/TransformationMethod;)V
aload 3
ldc_w -7829368
invokevirtual android/widget/EditText/setTextColor(I)V
aload 3
iconst_2
ldc_w 15.0F
invokevirtual android/widget/EditText/setTextSize(IF)V
aload 3
aconst_null
iconst_0
invokevirtual android/widget/EditText/setTypeface(Landroid/graphics/Typeface;I)V
aload 3
ldc_w -3355444
invokevirtual android/widget/EditText/setHintTextColor(I)V
aload 1
aload 3
invokestatic net/hockeyapp/android/f/j/a(Landroid/content/Context;Landroid/widget/EditText;)V
aload 0
getfield net/hockeyapp/android/f/j/f Landroid/widget/LinearLayout;
aload 3
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
new android/widget/Button
dup
aload 1
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 1
aload 1
sipush 12293
invokevirtual android/widget/Button/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 3
aload 3
iconst_0
iconst_0
iconst_0
iconst_1
ldc_w 30.0F
aload 0
invokevirtual net/hockeyapp/android/f/j/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 1
aload 3
invokevirtual android/widget/Button/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 1
aload 0
invokespecial net/hockeyapp/android/f/j/getButtonSelector()Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/Button/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 1
sipush 1284
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 1
iconst_m1
invokevirtual android/widget/Button/setTextColor(I)V
aload 1
iconst_2
ldc_w 15.0F
invokevirtual android/widget/Button/setTextSize(IF)V
aload 0
getfield net/hockeyapp/android/f/j/f Landroid/widget/LinearLayout;
aload 1
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
return
.limit locals 5
.limit stack 7
.end method

.method private a()V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 1
aload 0
iconst_m1
invokevirtual net/hockeyapp/android/f/j/setBackgroundColor(I)V
aload 0
aload 1
invokevirtual net/hockeyapp/android/f/j/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
return
.limit locals 2
.limit stack 4
.end method

.method private a(Landroid/content/Context;)V
aload 0
new android/widget/LinearLayout
dup
aload 1
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
putfield net/hockeyapp/android/f/j/f Landroid/widget/LinearLayout;
aload 0
getfield net/hockeyapp/android/f/j/f Landroid/widget/LinearLayout;
sipush 12289
invokevirtual android/widget/LinearLayout/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
iconst_m1
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 1
iconst_1
ldc_w 20.0F
aload 0
invokevirtual net/hockeyapp/android/f/j/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
aload 1
bipush 49
putfield android/widget/LinearLayout$LayoutParams/gravity I
aload 0
getfield net/hockeyapp/android/f/j/f Landroid/widget/LinearLayout;
aload 1
invokevirtual android/widget/LinearLayout/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield net/hockeyapp/android/f/j/f Landroid/widget/LinearLayout;
iload 2
iload 2
iload 2
iload 2
invokevirtual android/widget/LinearLayout/setPadding(IIII)V
aload 0
getfield net/hockeyapp/android/f/j/f Landroid/widget/LinearLayout;
iconst_1
invokevirtual android/widget/LinearLayout/setOrientation(I)V
aload 0
aload 0
getfield net/hockeyapp/android/f/j/f Landroid/widget/LinearLayout;
invokevirtual net/hockeyapp/android/f/j/addView(Landroid/view/View;)V
return
.limit locals 3
.limit stack 5
.end method

.method private static a(Landroid/content/Context;Landroid/widget/EditText;)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmpge L0
aload 0
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/density F
ldc_w 10.0F
fmul
f2i
istore 2
new android/graphics/drawable/ShapeDrawable
dup
new android/graphics/drawable/shapes/RectShape
dup
invokespecial android/graphics/drawable/shapes/RectShape/<init>()V
invokespecial android/graphics/drawable/ShapeDrawable/<init>(Landroid/graphics/drawable/shapes/Shape;)V
astore 3
aload 3
invokevirtual android/graphics/drawable/ShapeDrawable/getPaint()Landroid/graphics/Paint;
astore 4
aload 4
iconst_m1
invokevirtual android/graphics/Paint/setColor(I)V
aload 4
getstatic android/graphics/Paint$Style/FILL_AND_STROKE Landroid/graphics/Paint$Style;
invokevirtual android/graphics/Paint/setStyle(Landroid/graphics/Paint$Style;)V
aload 4
fconst_1
invokevirtual android/graphics/Paint/setStrokeWidth(F)V
aload 3
iload 2
iload 2
iload 2
iload 2
invokevirtual android/graphics/drawable/ShapeDrawable/setPadding(IIII)V
aload 0
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/density F
f2d
ldc2_w 1.5D
dmul
d2i
istore 2
new android/graphics/drawable/ShapeDrawable
dup
new android/graphics/drawable/shapes/RectShape
dup
invokespecial android/graphics/drawable/shapes/RectShape/<init>()V
invokespecial android/graphics/drawable/ShapeDrawable/<init>(Landroid/graphics/drawable/shapes/Shape;)V
astore 0
aload 0
invokevirtual android/graphics/drawable/ShapeDrawable/getPaint()Landroid/graphics/Paint;
astore 4
aload 4
ldc_w -12303292
invokevirtual android/graphics/Paint/setColor(I)V
aload 4
getstatic android/graphics/Paint$Style/FILL_AND_STROKE Landroid/graphics/Paint$Style;
invokevirtual android/graphics/Paint/setStyle(Landroid/graphics/Paint$Style;)V
aload 4
fconst_1
invokevirtual android/graphics/Paint/setStrokeWidth(F)V
aload 0
iconst_0
iconst_0
iconst_0
iload 2
invokevirtual android/graphics/drawable/ShapeDrawable/setPadding(IIII)V
aload 1
new android/graphics/drawable/LayerDrawable
dup
iconst_2
anewarray android/graphics/drawable/Drawable
dup
iconst_0
aload 0
aastore
dup
iconst_1
aload 3
aastore
invokespecial android/graphics/drawable/LayerDrawable/<init>([Landroid/graphics/drawable/Drawable;)V
invokevirtual android/widget/EditText/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
L0:
return
.limit locals 5
.limit stack 7
.end method

.method private b(Landroid/content/Context;)V
new android/widget/TextView
dup
aload 1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
astore 1
aload 1
sipush 12290
invokevirtual android/widget/TextView/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 2
aload 2
iconst_0
iconst_0
iconst_0
iconst_1
ldc_w 30.0F
aload 0
invokevirtual net/hockeyapp/android/f/j/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 1
aload 2
invokevirtual android/widget/TextView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 1
sipush 1280
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 1
ldc_w -7829368
invokevirtual android/widget/TextView/setTextColor(I)V
aload 1
iconst_2
ldc_w 18.0F
invokevirtual android/widget/TextView/setTextSize(IF)V
aload 1
aconst_null
iconst_0
invokevirtual android/widget/TextView/setTypeface(Landroid/graphics/Typeface;I)V
aload 0
getfield net/hockeyapp/android/f/j/f Landroid/widget/LinearLayout;
aload 1
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
return
.limit locals 3
.limit stack 7
.end method

.method private c(Landroid/content/Context;)V
new android/widget/EditText
dup
aload 1
invokespecial android/widget/EditText/<init>(Landroid/content/Context;)V
astore 2
aload 2
sipush 12291
invokevirtual android/widget/EditText/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 3
aload 3
iconst_0
iconst_0
iconst_0
iconst_1
ldc_w 30.0F
aload 0
invokevirtual net/hockeyapp/android/f/j/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 2
aload 3
invokevirtual android/widget/EditText/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 2
sipush 1282
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/widget/EditText/setHint(Ljava/lang/CharSequence;)V
aload 2
iconst_5
invokevirtual android/widget/EditText/setImeOptions(I)V
aload 2
bipush 33
invokevirtual android/widget/EditText/setInputType(I)V
aload 2
ldc_w -7829368
invokevirtual android/widget/EditText/setTextColor(I)V
aload 2
iconst_2
ldc_w 15.0F
invokevirtual android/widget/EditText/setTextSize(IF)V
aload 2
aconst_null
iconst_0
invokevirtual android/widget/EditText/setTypeface(Landroid/graphics/Typeface;I)V
aload 2
ldc_w -3355444
invokevirtual android/widget/EditText/setHintTextColor(I)V
aload 1
aload 2
invokestatic net/hockeyapp/android/f/j/a(Landroid/content/Context;Landroid/widget/EditText;)V
aload 0
getfield net/hockeyapp/android/f/j/f Landroid/widget/LinearLayout;
aload 2
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
return
.limit locals 4
.limit stack 7
.end method

.method private d(Landroid/content/Context;)V
new android/widget/EditText
dup
aload 1
invokespecial android/widget/EditText/<init>(Landroid/content/Context;)V
astore 2
aload 2
sipush 12292
invokevirtual android/widget/EditText/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 3
aload 3
iconst_0
iconst_0
iconst_0
iconst_1
ldc_w 30.0F
aload 0
invokevirtual net/hockeyapp/android/f/j/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 2
aload 3
invokevirtual android/widget/EditText/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 2
sipush 1283
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/widget/EditText/setHint(Ljava/lang/CharSequence;)V
aload 2
iconst_5
invokevirtual android/widget/EditText/setImeOptions(I)V
aload 2
sipush 128
invokevirtual android/widget/EditText/setInputType(I)V
aload 2
invokestatic android/text/method/PasswordTransformationMethod/getInstance()Landroid/text/method/PasswordTransformationMethod;
invokevirtual android/widget/EditText/setTransformationMethod(Landroid/text/method/TransformationMethod;)V
aload 2
ldc_w -7829368
invokevirtual android/widget/EditText/setTextColor(I)V
aload 2
iconst_2
ldc_w 15.0F
invokevirtual android/widget/EditText/setTextSize(IF)V
aload 2
aconst_null
iconst_0
invokevirtual android/widget/EditText/setTypeface(Landroid/graphics/Typeface;I)V
aload 2
ldc_w -3355444
invokevirtual android/widget/EditText/setHintTextColor(I)V
aload 1
aload 2
invokestatic net/hockeyapp/android/f/j/a(Landroid/content/Context;Landroid/widget/EditText;)V
aload 0
getfield net/hockeyapp/android/f/j/f Landroid/widget/LinearLayout;
aload 2
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
return
.limit locals 4
.limit stack 7
.end method

.method private e(Landroid/content/Context;)V
new android/widget/Button
dup
aload 1
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 1
aload 1
sipush 12293
invokevirtual android/widget/Button/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 2
aload 2
iconst_0
iconst_0
iconst_0
iconst_1
ldc_w 30.0F
aload 0
invokevirtual net/hockeyapp/android/f/j/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 1
aload 2
invokevirtual android/widget/Button/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 1
aload 0
invokespecial net/hockeyapp/android/f/j/getButtonSelector()Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/Button/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 1
sipush 1284
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 1
iconst_m1
invokevirtual android/widget/Button/setTextColor(I)V
aload 1
iconst_2
ldc_w 15.0F
invokevirtual android/widget/Button/setTextSize(IF)V
aload 0
getfield net/hockeyapp/android/f/j/f Landroid/widget/LinearLayout;
aload 1
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
return
.limit locals 3
.limit stack 7
.end method

.method private static f(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
aload 0
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/density F
ldc_w 10.0F
fmul
f2i
istore 1
new android/graphics/drawable/ShapeDrawable
dup
new android/graphics/drawable/shapes/RectShape
dup
invokespecial android/graphics/drawable/shapes/RectShape/<init>()V
invokespecial android/graphics/drawable/ShapeDrawable/<init>(Landroid/graphics/drawable/shapes/Shape;)V
astore 2
aload 2
invokevirtual android/graphics/drawable/ShapeDrawable/getPaint()Landroid/graphics/Paint;
astore 3
aload 3
iconst_m1
invokevirtual android/graphics/Paint/setColor(I)V
aload 3
getstatic android/graphics/Paint$Style/FILL_AND_STROKE Landroid/graphics/Paint$Style;
invokevirtual android/graphics/Paint/setStyle(Landroid/graphics/Paint$Style;)V
aload 3
fconst_1
invokevirtual android/graphics/Paint/setStrokeWidth(F)V
aload 2
iload 1
iload 1
iload 1
iload 1
invokevirtual android/graphics/drawable/ShapeDrawable/setPadding(IIII)V
aload 0
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/density F
f2d
ldc2_w 1.5D
dmul
d2i
istore 1
new android/graphics/drawable/ShapeDrawable
dup
new android/graphics/drawable/shapes/RectShape
dup
invokespecial android/graphics/drawable/shapes/RectShape/<init>()V
invokespecial android/graphics/drawable/ShapeDrawable/<init>(Landroid/graphics/drawable/shapes/Shape;)V
astore 0
aload 0
invokevirtual android/graphics/drawable/ShapeDrawable/getPaint()Landroid/graphics/Paint;
astore 3
aload 3
ldc_w -12303292
invokevirtual android/graphics/Paint/setColor(I)V
aload 3
getstatic android/graphics/Paint$Style/FILL_AND_STROKE Landroid/graphics/Paint$Style;
invokevirtual android/graphics/Paint/setStyle(Landroid/graphics/Paint$Style;)V
aload 3
fconst_1
invokevirtual android/graphics/Paint/setStrokeWidth(F)V
aload 0
iconst_0
iconst_0
iconst_0
iload 1
invokevirtual android/graphics/drawable/ShapeDrawable/setPadding(IIII)V
new android/graphics/drawable/LayerDrawable
dup
iconst_2
anewarray android/graphics/drawable/Drawable
dup
iconst_0
aload 0
aastore
dup
iconst_1
aload 2
aastore
invokespecial android/graphics/drawable/LayerDrawable/<init>([Landroid/graphics/drawable/Drawable;)V
areturn
.limit locals 4
.limit stack 6
.end method

.method private getButtonSelector()Landroid/graphics/drawable/Drawable;
new android/graphics/drawable/StateListDrawable
dup
invokespecial android/graphics/drawable/StateListDrawable/<init>()V
astore 1
new android/graphics/drawable/ColorDrawable
dup
ldc_w -16777216
invokespecial android/graphics/drawable/ColorDrawable/<init>(I)V
astore 2
aload 1
iconst_1
newarray int
dup
iconst_0
ldc_w -16842919
iastore
aload 2
invokevirtual android/graphics/drawable/StateListDrawable/addState([ILandroid/graphics/drawable/Drawable;)V
new android/graphics/drawable/ColorDrawable
dup
ldc_w -12303292
invokespecial android/graphics/drawable/ColorDrawable/<init>(I)V
astore 2
aload 1
iconst_2
newarray int
dup
iconst_0
ldc_w -16842919
iastore
dup
iconst_1
ldc_w 16842908
iastore
aload 2
invokevirtual android/graphics/drawable/StateListDrawable/addState([ILandroid/graphics/drawable/Drawable;)V
new android/graphics/drawable/ColorDrawable
dup
ldc_w -7829368
invokespecial android/graphics/drawable/ColorDrawable/<init>(I)V
astore 2
aload 1
iconst_1
newarray int
dup
iconst_0
ldc_w 16842919
iastore
aload 2
invokevirtual android/graphics/drawable/StateListDrawable/addState([ILandroid/graphics/drawable/Drawable;)V
aload 1
areturn
.limit locals 3
.limit stack 5
.end method
