.bytecode 50.0
.class public synchronized net/hockeyapp/android/f/a
.super android/view/ViewGroup

.field static final synthetic 'a' Z

.field private 'b' I

.method static <clinit>()V
ldc net/hockeyapp/android/f/a
invokevirtual java/lang/Class/desiredAssertionStatus()Z
ifne L0
iconst_1
istore 0
L1:
iload 0
putstatic net/hockeyapp/android/f/a/a Z
return
L0:
iconst_0
istore 0
goto L1
.limit locals 1
.limit stack 1
.end method

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
invokespecial android/view/ViewGroup/<init>(Landroid/content/Context;)V
return
.limit locals 2
.limit stack 2
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
aload 1
instanceof android/view/ViewGroup$LayoutParams
ireturn
.limit locals 2
.limit stack 1
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
new android/view/ViewGroup$LayoutParams
dup
iconst_1
iconst_1
invokespecial android/view/ViewGroup$LayoutParams/<init>(II)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public getAttachments()Ljava/util/ArrayList;
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 2
iconst_0
istore 1
L0:
iload 1
aload 0
invokevirtual net/hockeyapp/android/f/a/getChildCount()I
if_icmpge L1
aload 2
aload 0
iload 1
invokevirtual net/hockeyapp/android/f/a/getChildAt(I)Landroid/view/View;
checkcast net/hockeyapp/android/f/b
invokevirtual net/hockeyapp/android/f/b/getAttachmentUri()Landroid/net/Uri;
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 2
areturn
.limit locals 3
.limit stack 3
.end method

.method protected onLayout(ZIIII)V
aload 0
invokevirtual net/hockeyapp/android/f/a/getChildCount()I
istore 9
aload 0
invokevirtual net/hockeyapp/android/f/a/getPaddingLeft()I
istore 5
aload 0
invokevirtual net/hockeyapp/android/f/a/getPaddingTop()I
istore 3
iconst_0
istore 7
L0:
iload 7
iload 9
if_icmpge L1
aload 0
iload 7
invokevirtual net/hockeyapp/android/f/a/getChildAt(I)Landroid/view/View;
astore 12
iload 3
istore 6
iload 5
istore 8
aload 12
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L2
aload 12
invokevirtual android/view/View/invalidate()V
aload 12
invokevirtual android/view/View/getMeasuredWidth()I
istore 10
aload 12
invokevirtual android/view/View/getMeasuredHeight()I
istore 11
aload 12
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
astore 13
iload 3
istore 6
iload 5
istore 8
iload 5
iload 10
iadd
iload 4
iload 2
isub
if_icmple L3
aload 0
invokevirtual net/hockeyapp/android/f/a/getPaddingLeft()I
istore 8
iload 3
aload 0
getfield net/hockeyapp/android/f/a/b I
iadd
istore 6
L3:
aload 12
iload 8
iload 6
iload 8
iload 10
iadd
iload 11
iload 6
iadd
invokevirtual android/view/View/layout(IIII)V
aload 13
getfield android/view/ViewGroup$LayoutParams/width I
istore 3
iload 8
aload 12
checkcast net/hockeyapp/android/f/b
invokevirtual net/hockeyapp/android/f/b/getGap()I
iload 10
iload 3
iadd
iadd
iadd
istore 8
L2:
iload 7
iconst_1
iadd
istore 7
iload 6
istore 3
iload 8
istore 5
goto L0
L1:
return
.limit locals 14
.limit stack 6
.end method

.method protected onMeasure(II)V
iconst_0
istore 5
getstatic net/hockeyapp/android/f/a/a Z
ifne L0
iload 1
invokestatic android/view/View$MeasureSpec/getMode(I)I
ifne L0
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L0:
iload 1
invokestatic android/view/View$MeasureSpec/getSize(I)I
istore 7
aload 0
invokevirtual net/hockeyapp/android/f/a/getChildCount()I
istore 8
aload 0
invokevirtual net/hockeyapp/android/f/a/getPaddingLeft()I
istore 3
aload 0
invokevirtual net/hockeyapp/android/f/a/getPaddingTop()I
istore 1
iconst_0
istore 6
iconst_0
istore 4
L1:
iload 6
iload 8
if_icmpge L2
aload 0
iload 6
invokevirtual net/hockeyapp/android/f/a/getChildAt(I)Landroid/view/View;
astore 10
aload 10
checkcast net/hockeyapp/android/f/b
astore 11
aload 11
invokevirtual net/hockeyapp/android/f/b/getEffectiveMaxHeight()I
aload 11
invokevirtual net/hockeyapp/android/f/b/getPaddingTop()I
iadd
istore 5
aload 10
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L3
aload 10
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
astore 11
aload 10
iload 7
ldc_w -2147483648
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
iload 5
ldc_w -2147483648
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
invokevirtual android/view/View/measure(II)V
aload 10
invokevirtual android/view/View/getMeasuredWidth()I
istore 9
iload 4
aload 10
invokevirtual android/view/View/getMeasuredHeight()I
aload 11
getfield android/view/ViewGroup$LayoutParams/height I
iadd
invokestatic java/lang/Math/max(II)I
istore 4
iload 3
iload 9
iadd
iload 7
if_icmple L4
aload 0
invokevirtual net/hockeyapp/android/f/a/getPaddingLeft()I
istore 3
iload 1
iload 4
iadd
istore 1
L5:
iload 3
aload 11
getfield android/view/ViewGroup$LayoutParams/width I
iload 9
iadd
iadd
istore 3
L6:
iload 6
iconst_1
iadd
istore 6
goto L1
L2:
aload 0
iload 4
putfield net/hockeyapp/android/f/a/b I
iload 2
invokestatic android/view/View$MeasureSpec/getMode(I)I
ifne L7
iload 1
iload 4
iadd
aload 0
invokevirtual net/hockeyapp/android/f/a/getPaddingBottom()I
iadd
istore 3
L8:
aload 0
iload 7
iload 3
invokevirtual net/hockeyapp/android/f/a/setMeasuredDimension(II)V
return
L7:
iload 5
istore 3
iload 2
invokestatic android/view/View$MeasureSpec/getMode(I)I
ldc_w -2147483648
if_icmpne L8
iload 5
istore 3
iload 1
iload 4
iadd
aload 0
invokevirtual net/hockeyapp/android/f/a/getPaddingBottom()I
iadd
iload 5
if_icmpge L8
iload 1
iload 4
iadd
aload 0
invokevirtual net/hockeyapp/android/f/a/getPaddingBottom()I
iadd
istore 3
goto L8
L4:
goto L5
L3:
goto L6
.limit locals 12
.limit stack 4
.end method
