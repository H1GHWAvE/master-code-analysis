.bytecode 50.0
.class public final synchronized enum net/hockeyapp/android/c/a
.super java/lang/Enum

.field public static final enum 'a' Lnet/hockeyapp/android/c/a;

.field public static final enum 'b' Lnet/hockeyapp/android/c/a;

.field public static final enum 'c' Lnet/hockeyapp/android/c/a;

.field private static final synthetic 'e' [Lnet/hockeyapp/android/c/a;

.field private final 'd' I

.method static <clinit>()V
new net/hockeyapp/android/c/a
dup
ldc "CrashManagerUserInputDontSend"
iconst_0
iconst_0
invokespecial net/hockeyapp/android/c/a/<init>(Ljava/lang/String;II)V
putstatic net/hockeyapp/android/c/a/a Lnet/hockeyapp/android/c/a;
new net/hockeyapp/android/c/a
dup
ldc "CrashManagerUserInputSend"
iconst_1
iconst_1
invokespecial net/hockeyapp/android/c/a/<init>(Ljava/lang/String;II)V
putstatic net/hockeyapp/android/c/a/b Lnet/hockeyapp/android/c/a;
new net/hockeyapp/android/c/a
dup
ldc "CrashManagerUserInputAlwaysSend"
iconst_2
iconst_2
invokespecial net/hockeyapp/android/c/a/<init>(Ljava/lang/String;II)V
putstatic net/hockeyapp/android/c/a/c Lnet/hockeyapp/android/c/a;
iconst_3
anewarray net/hockeyapp/android/c/a
dup
iconst_0
getstatic net/hockeyapp/android/c/a/a Lnet/hockeyapp/android/c/a;
aastore
dup
iconst_1
getstatic net/hockeyapp/android/c/a/b Lnet/hockeyapp/android/c/a;
aastore
dup
iconst_2
getstatic net/hockeyapp/android/c/a/c Lnet/hockeyapp/android/c/a;
aastore
putstatic net/hockeyapp/android/c/a/e [Lnet/hockeyapp/android/c/a;
return
.limit locals 0
.limit stack 5
.end method

.method private <init>(Ljava/lang/String;II)V
aload 0
aload 1
iload 2
invokespecial java/lang/Enum/<init>(Ljava/lang/String;I)V
aload 0
iload 3
putfield net/hockeyapp/android/c/a/d I
return
.limit locals 4
.limit stack 3
.end method

.method private a()I
aload 0
getfield net/hockeyapp/android/c/a/d I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public static valueOf(Ljava/lang/String;)Lnet/hockeyapp/android/c/a;
ldc net/hockeyapp/android/c/a
aload 0
invokestatic java/lang/Enum/valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
checkcast net/hockeyapp/android/c/a
areturn
.limit locals 1
.limit stack 2
.end method

.method public static values()[Lnet/hockeyapp/android/c/a;
getstatic net/hockeyapp/android/c/a/e [Lnet/hockeyapp/android/c/a;
invokevirtual [Lnet/hockeyapp/android/c/a;/clone()Ljava/lang/Object;
checkcast [Lnet/hockeyapp/android/c/a;
areturn
.limit locals 0
.limit stack 1
.end method
